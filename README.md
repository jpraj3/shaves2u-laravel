# **Shaves2U**
***

## Link:
***
   * [https://shaves2u.com](https://shaves2u.com)
   * [https://ba.shaves2u.com](https://ba.shaves2u.com)

## Source: 
***
    - Ecommerce (Shaves2U REST API & Ecommerce Platform)
    - BA-Website (Onground Sales App)
    - Admin Management Site

## Techstack: 
***  
    - Laravel v5.8.17
    - Bitbucket & Git
    - Laragon

## Setup Guide:
***
    - composer install

##### or

    - composer update
    - composer dump-autoload

***
## Development Guide:
***

***
#### To standardize naming convention throughout the project, these are some of the things to follow: 

    * For new features - branch name as follows: 
        - feature/[date]_[ecommerce/ba/admin]_[feature-name]  -> e.g : feature/20190613_ecommerce_ba_payment_gateway
    * Naming of files/folders/variables/functions 
        - as short as possible while understandable. 
    * Proper comments to understand the code you are building. with proper steps
    * Any updates to the current README.md should be added following the existing format. (please update regularly as required)  
    * New features should be considered as a *New Module* unless the feature requires the use of existing modules to be modified.
    * Routing should be made with proper process flow in mind
        - E.g: Login -> Dashboard -> View Products -> Create / Edit / Delete Products 
    * Any functions that are to be used in more than a single file 
        - Ensure that it is converted into a helper (global function)
    * Minimize Code Redundacy
    * Practice Clean Code
    * Never re-invent the wheel
***

###### Development should be separated by modules of global & non-global. Global modules would be duplicated across all 3 project folders via script. Script can only be launched via Ecommerce project folder, therefore, all global module developments should be strictly done via ** *Ecommerce project folder ONLY* **

#### Global Modules
***
    * App\Http\Controllers\Global\
    * App\Models
    * App\Mail
    * App\Notification
    * App\Helpers
    * App\Services
    * config\
    * database\
    * composer.json
    * composer.lock
    * package.json

#### Non-global Modules
***
    * App\Console\
    * App\Exceptions\
    * App\Console\
    * App\Helpers
    * App\Http\Controllers\Ecommerce\
    * App\Http\Controllers\Admin\
    * App\Http\Controllers\BaWebsite\
    * App\Http\Middleware\
    * App\Providers
    * App\Services
    * App\Traits

    *** 

    * bootstrap\
    * public\
    * resources\
    * routes\
    * storage\
    * tests\
***

## PHP Artisan Commands: 
***
    - key:generate
    - config:cache 
    - view:clear
    - route:list
    - api:routes
    - optimize
    - db:seed --class={{class_name}}
    - migrate / migrate:fresh / migrate:refresh / migrate:rollback
    - migrate --path=./database/migrations/{{filename}}.php
    - php artisan queue:table

## Libraries:
***
* LaraCast Generators       - composer require laracasts/generators --dev
* CORS                      - composer require barryvdh/laravel-cors
* Stripe                    - composer require stripe/stripe-php
* Cashier                   - composer require laravel/cashier (possible-usage)
* Aftership                 - composer require aftership/aftership-php-sdk
* Redis                     - composer require predis/predis
* SEOTools                  - composer require artesaos/seotools
* Laravel Cascading Config  - composer require machaven/laravel-cascading-config
* Credit Card Validation    - composer require laravel-validation-rules/credit-card
* Guzzle                    - composer require guzzlehttp/guzzle
* Browser Agent             - composer require jenssegers/agent
* Social Share				- composer require jorenvanhocht/laravel-share
* Bitly URL Shortener       - composer require shivella/laravel-bitly
* Laravel CORS              - composer require barryvdh/laravel-cors
* phpoffice/phpspreadsheet  - composer require phpoffice/phpspreadsheet
* maatwebsite/excel         - composer require maatwebsite/excel
* zanysoft/laravel-zip		- composer require zanysoft/laravel-zip

***

## Integration
***
* Stripe            - [https://github.com/stripe/stripe-php](https://github.com/stripe/stripe-php)
* Aftership         - [https://github.com/AfterShip/aftership-sdk-php ](https://github.com/AfterShip/aftership-sdk-php )
* IpStack           - [https://ipstack.com/product](https://ipstack.com/product)
* SEOTools          - [https://github.com/artesaos/seotools](https://github.com/artesaos/seotools)
***

## Future Integration
***
* Login(FB,GOOGLE,KAKAO,NAVER)  - [https://socialiteproviders.netlify.com/)
* Aftership         - [https://github.com/AfterShip/aftership-sdk-php ](https://github.com/AfterShip/aftership-sdk-php )
* IpStack           - [https://ipstack.com/product](https://ipstack.com/product)
***

## Links for Tutorials:
***
* [IpStack](https://scotch.io/tutorials/personalize-your-ux-by-location-using-ipstacks-geolocation)
* [Stripe #1](https://appdividend.com/2018/12/05/laravel-stripe-payment-gateway-integration-tutorial-with-example/)
* [Stripe #2](https://itsolutionstuff.com/post/stripe-payment-gateway-integration-in-laravel-58example.html)
* [Unit Testing #1 ](https://blog.pusher.com/tests-laravel-applications/)
* [Unit Testing #2 ](https://phpunit.readthedocs.io/en/7.3/index.html)
* [Laravel - Detect Language & Auto Change #1](https://brysemeijer.com/blog/2/laravel-detect-language-set-locale)
* [Laravel - Detect Language & Auto Change #2](https://laracasts.com/discuss/channels/general-discussion/where-to-setlocale-in-laravel-5-on-multilingual-multidomain-app)
* [Redis](https://laravel.com/docs/5.8/redis)
* [Queue](https://laravel.com/docs/5.1/queues)
* [Authorization](https://laravel.com/docs/5.8/authorization)
* [Validation](https://laravel.com/docs/5.8/validation)
* [Providers](https://laravel.com/docs/5.8/providers)
* [https://github.com/Xethron/migrations-generator](https://github.com/Xethron/migrations-generator)
* [https://medium.com/@CristianLLanos/eloquent-models-from-my-database-5d74c632e03c](https://medium.com/@CristianLLanos/eloquent-models-from-my-database-5d74c632e03c)
* [SEOTools](https://github.com/artesaos/seotools)
* [Javascript PHP LangCode Localization](https://pineco.de/using-laravels-localization-js/)
***

## Unit Testing: 
***
    1. Create unit / feature testing using 
        * php artisan make: test {{test - name}} --{{Unit / Feature}}
    2. Run all unit tests in command-line from document root
        * vendor\ bin\ phpunit
    
***

### Email Structure
The Email feature consist of 3 sections.

- Email Helper. Contains module to Build Email template & Send Email. (app\Helpers\Email\EmailHelper.php)

- Email Templates. Consist of base.blade.php (Header & Footer), Used for all email templates; and template.blade.php, contains Email body content,separated by modules. (resources\views\email-templates)

Email Templates folder structure:

    email-template/
            {module-name}/
                template.blade.php
    base.blade.php

- Email Translation Files. Consist email.php, which contains Translation Strings for Email Body Content, filtered by Language (resources\lang)

Email Translation folder structure:

    resource/
            lang/
                {lang-code}/
                        email.php

#### Sending Email with Email Helper + Mandrill API

##### Initial Setup
- Install [Guzzle](https://github.com/guzzle/guzzle)
```
    composer require guzzlehttp/guzzle
    composer install
```

- Setting up .env. Add the following to the .env file in your project root. Values in [Square Brackets] required modification.

```
    MANDRILL_DRIVER=mandrill
    MANDRILL_HOST=smtp.mandrillapp.com
    MANDRILL_PORT=587
    MANDRILL_USERNAME= [Your Mandrill Username]
    MANDRILL_PASSWORD= [Your Mandrill API Key]
    MANDRILL_SECRET= [Your Mandrill API Key]
    MANDRILL_ENCRYPTION=tls
    MANDRILL_ADDRESS= [Email From Address]
    MANDRILL_NAME= [Email From Name]
```

#### Calling Email Helper From Contoller
- Import Email Helper in your controller
```php
    use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
```

- Create new email Instance in the Controller construct, with Request parameter.
```php
    public function __construct()
    {
        $this->email = new EmailQueueHelper($request);
    }
```
- Call the email helper buildEmailTemplate in your controller function
```php
    public function yourEmailFunction()
    {
        $this->email->buildEmailTemplate($moduleName, $moduleData, $CountryAndLocaleData)
    }
```
The buildEmailTemplate function requires 3 parameters:  

```
$moduleName -> This determines the path of the template file, body content and Email Subject Title.  

$moduleData -> Variables to be used in email template should be passed here. The data formats accepted are String and Array.  

$CountryAndLocaleData -> Array, with Country Data in JSON & Locale Data in string
```

###### Example A (Calling EmailHelper to send welcome email in ArbitraryController.php)
**ArbitaryController.php**

```php
namespace App\Http\Controllers\ArbitraryController;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;

class ArbitraryController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->request = $request;
        $this->email = new EmailQueueHelper();
    }

    /**
     * Arbitary ControllerSend Email Function.
     */
    protected function sendArbitaryEmail()
    {
        // All email Related variables should be passed here.
        $arbitaryModuleData = [ 
            'name' => 'foo',
            'email' => 'bar@example.com'
        ];

        // Get Country Data (From session)
        $countryData = session()->get('currentCountry');

        // Get Locale Data
        $localeData = app()->getLocale();

        // Pass Country and Locale Data into array
        $countryAndLocaleData = array($countryData, $localeData)

        /**
         * Calling The EmailHelper
         * Param 1 passes the email module name. For this example module name is welcome
         * Param 2 passes the Variables required in your template.blade.php
         * Param 3 Determines the Email Footer Text and email language
         */
        $this->email->buildEmailTemplate('welcome', $arbitaryModuleData, $countryAndLocaleData);
    }
}
```
Variables in Param 2 can be accessed from template.blade.php using $moduleData array.  

eg: From Example A, ```$moduleData['name']``` returns **foo**  

#### Email Templating
All Email template.blade.php structure is build with HTML & translated with Laravel ```@lang``` directive.

##### Creating Translation string for Email content
1. Create the translation strings in resources/lang/{lang-code}/email.php

2. Create a ```@lang``` directive to call the translation under email-template/{module-name}/template.blade.php

###### Example B (Templating with welcome email, with en as langcode)

**template.blade.html**

```html
<td style="padding:0;margin:0;text-align:left;" class="row-padding">
    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
        <tr>
            <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                @lang('email.content.body.welcome.intro')
            </td>
        </tr>
    </table>
</td>
```

**email.php**

```php
return [
    'content' => [
        'body' => [
            'welcome' => [
                'intro' => 'You\'ve taken the first step to end retail razor rip-offs. We\'re thrilled that you have chosen Shaves2U, and hope you\'ll enjoy our products as much as we did crafting them for you.',
            ]
        ],
    ]
];
```

**template.blade.html Expected Output**

```html
<td style="padding:0;margin:0;text-align:left;" class="row-padding">
    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
        <tr>
            <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                You've taken the first step to end retail razor rip-offs. We're thrilled that you have chosen Shaves2U, and hope you'll enjoy our products as much as we did crafting them for you.
            </td>
        </tr>
    </table>
</td>
```

##### Binding Variables to Email Content
Variables Passed from EmailHelper's $moduleData parameter be accessed from template.blade.php using $moduleData array.

1. Create the translation strings in resources/lang/{lang-code}/email.php with ```:placeholder``` string.

2. Create ```@lang``` directive under email-template/{module-name}/template.blade.php. Assign a second arguement to ```@lang``` directive to map $moduleData variables to the defined ```:placeholder``` string

###### Example C (Variable Binding with welcome email, with en as langcode)

**ArbitaryController.php (From Example A)**

```php
namespace App\Http\Controllers\ArbitraryController;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;

class ArbitraryController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->request = $request;
        $this->email = new EmailQueueHelper();
    }

    /**
     * Arbitary ControllerSend Email Function.
     */
    protected function sendArbitaryEmail()
    {
        // All email Related variables should be passed here.
        $arbitaryModuleData = [ 
            'name' => 'foo',
            'email' => 'bar@example.com'
        ];

        // Get Country Data (From session)
        $countryData = session()->get('currentCountry');

        // Get Locale Data
        $localeData = app()->getLocale();

        // Pass Country and Locale Data into array
        $countryAndLocaleData = array($countryData, $localeData)

        /**
         * Calling The EmailHelper
         * Param 1 passes the email module name. For this example module name is welcome
         * Param 2 passes the Variables required in your template.blade.php
         * Param 3 Determines the Email Footer Text and email language
         */
        $this->email->buildEmailTemplate('welcome', $arbitaryModuleData, $countryAndLocaleData);
    }
}

```

**template.blade.html**

```html
<td style="padding:0;margin:0;text-align:left;" class="row-padding">
    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
        <tr>
            <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                @lang('email.content.body.welcome.hello', ['name' => $moduleData['name']])
            </td>
        </tr>
    </table>
</td>
```

**email.php**

```php
return [
    'content' => [
        'body' => [
            'welcome' => [
                'hello' => 'Hey :name,',
            ]
        ],
    ]
];
```

**template.blade.html Expected Output**
```html
<td style="padding:0;margin:0;text-align:left;" class="row-padding">
    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
        <tr>
            <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                Hey foo
            </td>
        </tr>
    </table>
</td>
```
----
remove from staging producttypedetails for BA alacarte
'126', '3', '34', NULL, '2019-07-03 00:50:41', '2019-07-03 00:50:41'
'128', '4', '1010', NULL, '2019-07-03 00:50:41', '2019-07-03 00:50:41'
'129', '4', '1011', NULL, '2019-07-03 00:50:41', '2019-07-03 00:50:41'
'130', '4', '1012', NULL, '2019-07-03 00:50:41', '2019-07-03 00:50:41'
'113', '5', '1019', NULL, '2019-07-03 08:50:41', '2019-07-03 08:50:41'
'127', '5', '121', NULL, '2019-07-03 00:50:41', '2019-07-03 00:50:41'
'92', '1', '121', NULL, '2019-07-03 08:50:40', '2019-07-03 08:50:40'
'6', '1', '6', NULL, '2019-07-03 08:50:37', '2019-07-03 08:50:37'
### Tables/Columns to take note for Old Site Migration Script
- Table : [orderdetails]
    Note: New column - PlanId (replaces PlanCountryId in the future)
    Note: New column - isFreeProduct
    Note: New column - isAddon 
- Table : [subscriptions] 
    Note: New column - currentCycle (to determine the products to be sent for each cycle)
- Table : [subscription_productaddon] 
    Note: New column - isWithTrialKit (to determine product is together with plan - eg. shave cream)