<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::post('/ajax/session', 'Globals\Utilities\SessionController@session')->name('ba.session');
Route::post('/login', 'Auth\LoginController@login')->name('ba.attempt-login');
Route::post('/check', 'Auth\LoginController@checkAgentExists')->name('ba.verify-agent');
Route::post('/logout', 'Auth\LoginController@logout')->name('ba.logout');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('ba.login');
// Locale
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'PostLogin\PostLoginController@redirectToHome')->name('ba.locale.region.redirect');
    Route::get('lang-select/{langCodeList}', 'Globals\GeoLocation\LocaleController@changeActiveLanguage')->name('ba.locale.changeActiveLanguage');
    Route::group(['prefix' => '{langCode}-{countryCode}', 'middleware' => ['locale', 'auth']], function () {
        // Authenticated Users here ---
        Route::get('/', 'PostLogin\PostLoginController@index')->name('ba.locale.landing');
        Route::get('/dashboard', 'PostLogin\PostLoginController@dashboard')->name('ba.locale.dashboard');
        Route::get('/products', 'PostLogin\PostLoginController@products')->name('ba.locale.products');
        Route::get('/saleshistory', 'PostLogin\PostLoginController@saleshistory')->name('ba.locale.saleshistory');
        Route::get('/profile', 'PostLogin\PostLoginController@profile')->name('ba.locale.profile');

        //Thank You
        Route::post('/order/success', 'Globals\ThankYou\ThankYouController@redirectToThankYou')->name('ba.locale.cash.order.success');
        Route::get('/thankyou/{id}', 'Globals\ThankYou\ThankYouController@index')->name('ba.locale.thankyou');

        Route::group(['prefix' => 'products'], function () {
            Route::post('/product-types', 'PostLogin\PostLoginController@productsType')->name('ba.locale.product-types');
            Route::get('/select-category', 'PostLogin\PostLoginController@showProductsType')->name('ba.locale.show.product-types');
            Route::get('/custom-plan', 'PostLogin\PostLoginController@showCustomPlans')->name('ba.locale.show.custom-plans');

        });

        Route::group(['prefix' => 'alacarte'], function () {
            Route::post('/select', 'PostLogin\PostLoginController@showProducts')->name('ba.locale.show.alacarte.select');
            Route::get('/select', 'PostLogin\PostLoginController@showProducts')->name('ba.locale.show.alacarte.select');
            Route::post('/select-payment-method', 'PostLogin\PostLoginController@showAlacarteSPM')->name('ba.locale.show.alacarte.spm');
            Route::get('/select-payment-method', 'PostLogin\PostLoginController@showAlacarteSPM')->name('ba.locale.show.alacarte.spm');
            Route::post('/_checkout', 'PostLogin\PostLoginController@AlacarteCheckout')->name('ba.locale.alacarte.checkout');
            Route::post('/checkout-cash', 'PostLogin\PostLoginController@showAlacarteCheckoutCash')->name('ba.locale.show.alacarte.checkout-cash');
            Route::post('/checkout-card', 'PostLogin\PostLoginController@showAlacarteCheckoutCard')->name('ba.locale.show.alacarte.checkout-card');
            Route::get('/checkout-card', 'PostLogin\PostLoginController@showAlacarteCheckoutCard')->name('ba.locale.show.alacarte.checkout-card');
        });

        // ba trial plan selection & checkout journey
        Route::group(['prefix' => 'trial'], function () {
            Route::get('/selection', 'PostLogin\PostLoginController@returnTrialSteps')->name('ba.locale.show.trial-plans.step-1');
            // Route::post('/step-2', 'PostLogin\PostLoginController@returnTrialSteps')->name('ba.locale.show.trial-plans.step-2');
            // Route::post('/step-3', 'PostLogin\PostLoginController@returnTrialSteps')->name('ba.locale.show.trial-plans.step-3');
            // Route::get('/selection', 'PostLogin\PostLoginController@showTrialPlansStep1')->name('ba.locale.show.trial-plans.step1');
            //     Route::post('/select-refill', 'PostLogin\PostLoginController@showTrialPlansStep2')->name('ba.locale.show.trial-plans.step2');
            //     Route::post('/select-frequency', 'PostLogin\PostLoginController@showTrialPlansStep3')->name('ba.locale.show.trial-plans.step3');
            Route::post('/_checkout', 'PostLogin\PostLoginController@TrialPlanCheckout')->name('ba.locale.trial-plans.checkout');
            Route::post('/checkout', 'PostLogin\PostLoginController@showTrialPlanCheckout')->name('ba.locale.show.trial-plans.checkout');
            Route::get('/checkout', 'PostLogin\PostLoginController@showTrialPlanCheckout')->name('ba.locale.show.trial-plans.checkout');
        });

        Route::group(['prefix' => 'stripe'], function () {
            Route::get('/redirect-from-stripe', 'PostLogin\PostLoginController@postPaymentRedirect')->name(('api.locale.region.user.stripe.redirect2'));
        });

    });
});
