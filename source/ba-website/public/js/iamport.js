
function nicepayAlacarte(orderData, URL, redirect) {
  console.log(orderData);
  var loading = document.getElementById("loading");
  if (loading) {
    loading.style.display = "block";
  }
  var errormsg = document.getElementById("error_msg_alacarte");
  if(errormsg){
    errormsg.innerHTML = "";
  }
  var type = orderData.type;
  //IMP.init("imp43733045");   //live
  //var amount = orderData.amount;

  IMP.init("imp99992948");
  var amount = orderData.amount;
  var email = orderData.email;
  var name = orderData.name;
  var phone = orderData.phone;
  var merchant_uid = 'ko_product_' + new Date().getTime();


if(amount <= 0){
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type: 'POST',
    url: URL,
    data: {
      type: type,
      status: true,
      resultpayment : "success",
      isFree: 1,
      imp_uid: "zero price",
      merchant_uid: "zero price",
      card_name: "zero price",
    },
    cache: false,
    success: function (data) {
      if(loading){
        loading.style.display = "none"; 
        }
        if (data.status) {
          if (data.status == "success") {
              window.location.href = redirect+data.orderid;
          }
      }
    },
    error: function (jqXHR) {
      if(loading){
        loading.style.display = "none"; 
        }
        if(jqXHR){
                errormsg.innerHTML = jqXHR.responseJSON.message;
              }
    }
  });
}else{

  IMP.request_pay({ // param
    pg: "nice",
    pay_method: "card",
    merchant_uid: merchant_uid,
    // customer_uid: "shaves2u_0003_1234", // 카드(빌링키)와 1:1로 대응하는 값
    language: "ko",
    name: name,
    amount: amount,
    //  amount: 300,
    buyer_email: email,
    buyer_name: name,
    buyer_tel: phone
  }, function (rsp) { // callback
    console.log("rsprsprsp + " + rsp);
    console.log("rsprsprsp1 + " + JSON.stringify(rsp));
    if (rsp.success) {
      // return rsp.success;
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: URL,
        data: {
          type: type,
          status: rsp.success,
          resultpayment : "success",
          isFree: 0,
          imp_uid: rsp.imp_uid,
          merchant_uid: rsp.merchant_uid,
          card_name: rsp.card_name,
        },
        cache: false,
        success: function (data) {
          // if(loading){
          //   loading.style.display = "none"; 
          //   }
            if (data.status) {
              if (data.status == "success") {
                  window.location.href = redirect+data.orderid;
              }
          }
        },
        error: function (jqXHR) {
          if(loading){
            loading.style.display = "none"; 
            }
            if(jqXHR){
                    errormsg.innerHTML = jqXHR.responseJSON.message;
                  }
        }
      });
    } else {
      // return rsp.error_msg.message;
      if(errormsg){
        errormsg.innerHTML = rsp.error_msg;
      }
      if (loading) {
        loading.style.display = "none";
      }
    }
  });
}
}