// Promotion
function isJSON(str) {
    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
}

function Promotion(usertype, type, sessiontype, idget="") {
    let promo_code_id = document.getElementById("promo_code");
    let promo_code = promo_code_id.value ? promo_code_id.value : '';
    session_data = session_data;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: API_URL + '/ba/user/check/promotion',
        method: "POST",
        cache: false,
        data: {
            "promo_code": promo_code,
            "CountryId": countryid,
            "langCode": langCode,
            "urllang": urllangCode,
            "user_id": user_id,
            "usertype": 'baWebsite',
        },
    }).done(function (response) {
        // console.log("promotion response", response.payload);
        if (response) {
            $("#error-promotion").text("");
            $("#success-promotion").text("");
            let result = response.payload;
            let eshipping = document.getElementById("c-shipping");
            let esubtotal = document.getElementById("c-subtotal");
            let ediscount = document.getElementById("c-discount");
            let etotal = document.getElementById("c-total");
            let ensubtotal = document.getElementById("c-nsubtotal");
            let entotal = document.getElementById("c-ntotal");
            let efreeproduct = document.getElementById("c-free-product");
            let enfreeproduct = document.getElementById("c-nfree-product");
            let efreeexistproduct = document.getElementById("c-free-exist-product");
            let etax = document.getElementById("c-tax");
            let shippingfee;
            let shippingfeeget;
            let subtotal;
            let discount;
            let oridiscount;
            let total;
            let ntotal;
            let minSpend = 0;
            let nminSpend = 0;
            let maxdic;
            let cTaxA = ctaxAmount;
            let taxrate = taxRate;
            let taxa = 0;
            // Promotion free product
            let freeproductshow = "";
            let freeproductcopy = "";

            // Promotion free exist product
            let freeexistproductshow = "";
            let freeexistproductcopy = "";
            let freeexistresultproduct = [];
            let sessionproductcountry = [];
            let ablefreeexistproduct = 0;
            let freeexistresultproductprice = 0.00;

            if (esubtotal) {
                subtotal = esubtotal.innerText;
            }
            if (ensubtotal) {
                ensubtotal = ensubtotal.innerText;
            }

            if (eshipping) {
                shippingfee = shipping_fee;
            }
            // if (ediscount) {
            //     discount = discount.value;
            // }\

            // Promotion free product
            if (result.freeproductcid) {
                if (efreeproduct && result.freeproductcid.length >= 1) {
                    for (let i = 0; i < result.freeproductcid.length; i++) {
                        if (i == 0) {
                            freeproductshow = result.freeproductname[i];
                        } else {
                            freeproductshow = freeproductshow + ',' + result.freeproductname[i];
                        }
                    }
                }
            }
            // Promotion free exist product
            if (result.freeexistproductcid) {
                // console.log(`resized == ${JSON.stringify(session_data)}`);
                if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                    if (type == "trial-plan" && result.promotionType != "Instant") {
                        let hanlde;
                        let blade;
                        let addon;
                        if (session_data["selection"]["step1"]["selected_blade"]) {
                            blade = session_data["selection"]["step1"]["selected_blade"];
                            sessionproductcountry.push(Number(blade.productcountriesid));
                        }
                        if (session_data["selection"]["step2"]["selected_addon_list"]) {
                            addon = session_data["selection"]["step2"]["selected_addon_list"];
                            addon.forEach((function (h) {
                                sessionproductcountry.push(Number(h.productcountriesid));
                            }));
                        }

                            hanlde = "";
                            sessionproductcountry.push(Number(hanlde.productcountriesid));


                    }
                    else if (type == "custom-plan") {
                        let hanlde;
                        let blade;
                        let addon;
                        if (session_data["selection"]["step1"]["selected_handle"]) {
                            hanlde = session_data["selection"]["step1"]["selected_handle"];
                            sessionproductcountry.push(Number(hanlde.productcountriesid));
                        }
                        if (session_data["selection"]["step2"]["selected_blade"]) {
                            blade = session_data["selection"]["step2"]["selected_blade"];
                            sessionproductcountry.push(Number(blade.productcountriesid));
                        }
                        if (session_data["selection"]["step4"]["selected_addon_list"]) {
                            addon = session_data["selection"]["step4"]["selected_addon_list"];
                            addon.forEach((function (h) {
                                sessionproductcountry.push(Number(h.productcountriesid));
                            }));
                        }
                    }
                    else if (type == "awesome-shave-kits") {
                        let product;
                        let pcountryid;
                        if (session_data["selection"]["step1"]) {
                            product = session_data["selection"]["step1"];
                            product.forEach((function (h) {
                                pcountryid = h.productcountryid;
                                sessionproductcountry.push(Number(pcountryid));
                            }));
                        }
                    }
                    let countfreeexistproduct = 0;
                    for (let i = 0; i < result.freeexistproductcid.length; i++) {
                        if (sessionproductcountry.includes(result.freeexistproductcid[i])) {
                            ablefreeexistproduct = 1;
                            freeexistresultproductprice = parseFloat(freeexistresultproductprice) + parseFloat(result.freeexistproductsellprice[i]);
                            freeexistresultproduct.push(result.freeexistproductcid[i]);
                            if (countfreeexistproduct == 0) {
                                freeexistproductshow = result.freeexistproductname[i];
                                countfreeexistproduct = countfreeexistproduct + 1;
                            } else {
                                freeexistproductshow = freeexistproductshow + ',' + result.freeexistproductname[i];
                                countfreeexistproduct = countfreeexistproduct + 1;
                            }
                        }
                    }
                }
            }

            if (freeexistresultproductprice && freeexistresultproductprice != 0 && freeexistresultproductprice != 0.00 ) {
                freeexistresultproductprice = parseFloat(freeexistresultproductprice).toFixed(2);
            } else {
                freeexistresultproductprice = "0.00";
            }

            if (result.promotionType === "Instant") {

                // Promotion free product
                if (result.freeproductcid) {
                    if (efreeproduct && result.freeproductcid.length >= 1) {
                        freeproductcopy = ",";
                    }
                }

                // Promotion free exist product
                if (ablefreeexistproduct == 1) {
                    if (result.freeexistproductcid) {
                        if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                            freeexistproductcopy = "Free Exist Product : ";
                        }
                    }
                }

                // Promotion discount
                if (shipping_fee && shipping_fee != 0  && shipping_fee != 0.00) {
                    eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                } else {
                    eshipping.innerHTML = "0.00";
                }
                if (result.isFreeShipping) {
                    if (result.isFreeShipping === 1) {
                        shippingfee = 0.00;
                        shippingfeeget = "0.00";
                        eshipping.innerHTML = shippingfeeget;
                    }
                }
                if (etotal) {
                    total = parseFloat(subtotal);
                    if (ablefreeexistproduct == 1 && type != "trial-plan") {
                        if (freeexistresultproduct && (freeexistresultproductprice > 0)) {
                            total = total - parseFloat(freeexistresultproductprice);
                        }
                    }
                    discount = (((parseFloat(total)) * result.discount) / 100);
                    oridiscount = result.discount + "%";

                    if (result.minSpend) {
                        if (result.minSpend > total) {
                            minSpend = 1;
                        }
                    }
                    if (result.maxDiscount) {
                        if (result.maxDiscount < discount) {
                            discount = result.maxDiscount;
                            oridiscount = result.maxDiscount;
                        }
                    }
                    total = total - discount;
                    total = total + parseFloat(shippingfee)
                    if (type != "trial-plan") {
                        taxa = taxrate / 100 * (total);
                        total = total + taxa;
                    }
                    if (total < 0) {
                        total = 0;
                    }
                }
                if (minSpend == 0) {

                    if(discount && discount != 0 && discount != 0.00){
                        discount = parseFloat(discount).toFixed(2);
                    }else{
                        discount = "0.00";
                    }

                    ediscount.innerHTML = discount;
                    if(total && total != 0 && total != 0.00){
                        total = parseFloat(total).toFixed(2);
                    }else{
                        total = "0.00";
                    }

                    if(ntotal && ntotal != 0 && ntotal != 0.00){
                        ntotal = parseFloat(ntotal).toFixed(2);
                    }else{
                        ntotal = "0.00";
                    }

                    if(taxa && taxa != 0 && taxa != 0.00){
                        taxa = parseFloat(taxa).toFixed(2);
                    }else{
                        taxa = "0.00";
                    }

                    etotal.innerHTML = total;
                    ntotal = next_price;
                    if (entotal) {
                        entotal.innerHTML = ntotal;
                    }
                    etax.innerHTML = taxa;
                    // Promotion free product
                    if (result.freeproductcid) {
                        if (efreeproduct && result.freeproductcid.length >= 1) {
                            efreeproduct.innerHTML = freeproductcopy + freeproductshow;
                        }
                        else {
                            efreeproduct.innerHTML = "";
                        }
                    } else {
                        efreeproduct.innerHTML = "";
                    }

                    if (enfreeproduct){
                        enfreeproduct.innerHTML = "";
                    }

                    // Promotion free exist product
                    if (ablefreeexistproduct == 1) {
                        if (result.freeexistproductcid) {
                            if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                                efreeexistproduct.innerHTML = freeexistproductcopy + freeexistproductshow;
                            }
                            else {
                                efreeexistproduct.innerHTML = "";
                            }
                        } else {
                            efreeexistproduct.innerHTML = "";
                        }
                    } else {
                        efreeexistproduct.innerHTML = "";
                    }

                    //seperatesession for custom plan
                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionData(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice);
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "success");
                    }
                } else {
                    ediscount.innerHTML = "0.00";
                    if(current_price && current_price != 0 && current_price != 0.00){
                        current_price = parseFloat(current_price).toFixed(2);
                    }else{
                        current_price = "0.00";
                    }
                    if(next_price && next_price != 0 && next_price != 0.00){
                        next_price = parseFloat(next_price).toFixed(2);
                    }else{
                        next_price = "0.00";
                    }
                    if(cTaxA && cTaxA != 0 && cTaxA != 0.00){
                        cTaxA = parseFloat(cTaxA).toFixed(2);
                    }else{
                        cTaxA = "0.00";
                    }

                    etotal.innerHTML = current_price;
                    entotal.innerHTML = next_price;
                    if (shipping_fee && shipping_fee != 0 && shipping_fee != 0.00) {
                        eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                    } else {
                        eshipping.innerHTML = "0.00";
                    }
                    etax.innerHTML = cTaxA;
                    // Promotion free product
                    efreeproduct.innerHTML = "";
                    if (enfreeproduct){
                        enfreeproduct.innerHTML = "";
                    }
                    // Promotion free exist product
                    efreeexistproduct.innerHTML = "";

                    $("#error-promotion").text("Minimum Spend : " + result.minSpend);
                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionErrorData();
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "error");
                    }
                }
                $("#success-promotion").text(trans('validation.custom.validation.promo.valid'));
            }
            else if (result.promotionType == "Upcoming" && type != "awesome-shave-kits") {

                // Promotion free product
                if (result.freeproductcid) {
                    if (enfreeproduct && result.freeproductcid.length >= 1) {
                        freeproductcopy = ",";
                    }
                }

                // Promotion free exist product
                if (ablefreeexistproduct == 1) {
                    if (result.freeexistproductcid) {
                        if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                            freeexistproductcopy = "Next Month Free Exist Product : ";
                        }
                    }
                }

                // Promotion discount
                if (shipping_fee && shipping_fee != 0 && shipping_fee != 0.00) {
                    eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                } else {
                    eshipping.innerHTML = "0.00";
                }
                if (result.isFreeShipping) {
                    if (result.isFreeShipping === 1) {
                        shippingfee = 0.00;
                        shippingfeeget = "0.00";
                        eshipping.innerHTML = shippingfeeget;
                    }
                }
                if (entotal) {
                    ntotal = parseFloat(ensubtotal);
                    if (ablefreeexistproduct == 1) {
                        if (freeexistresultproduct && (freeexistresultproductprice > 0)) {
                            ntotal = ntotal - parseFloat(freeexistresultproductprice);
                        }
                    }
                    discount = (((parseFloat(ntotal)) * result.discount) / 100);

                    total = parseFloat(subtotal);
                    oridiscount = result.discount + "%";
                    if (result.minSpend) {
                        if (result.minSpend > total) {
                            nminSpend = 1;
                        }
                    }
                    total = "";
                    if (result.maxDiscount) {
                        if (result.maxDiscount < discount) {
                            discount = result.maxDiscount;
                            oridiscount = result.maxDiscount;
                        }
                    }
                    ntotal = ntotal - discount;
                    ntotal = ntotal + parseFloat(shippingfee);

                    taxa = taxrate / 100 * (ntotal);
                    ntotal = ntotal + taxa;

                    if (ntotal < 0) {
                        ntotal = 0;
                    }
                }

                if (nminSpend == 0) {

                    if(discount && discount != 0 && discount != 0.00){
                        discount = parseFloat(discount).toFixed(2);
                    }else{
                        discount = "0.00";
                    }
                    ediscount.innerHTML = discount;

                    if(ntotal && ntotal != 0 && ntotal != 0.00){
                        ntotal = parseFloat(ntotal).toFixed(2);
                    }else{
                        ntotal = "0.00";
                    }

                    if(total && total != 0 && total != 0.00){
                        total = parseFloat(total).toFixed(2);
                    }else{
                        total = "0.00";
                    }

                    if(taxa && taxa != 0 && taxa != 0.00){
                        taxa = parseFloat(taxa).toFixed(2);
                    }else{
                        taxa = "0.00";
                    }


                    entotal.innerHTML = ntotal;
                    total = current_price;
                    etotal.innerHTML = total;
                    etax.innerHTML = taxa;
                    // Promotion free product
                    if (result.freeproductcid) {
                        if (enfreeproduct && result.freeproductcid.length >= 1) {
                            enfreeproduct.innerHTML = freeproductcopy + freeproductshow;
                        }
                    }
                    else {
                        enfreeproduct.innerHTML = "";
                    }
                    if (efreeproduct){
                    efreeproduct.innerHTML = "";
                    }
                    // Promotion free exist product
                    if (ablefreeexistproduct == 1) {
                        if (result.freeexistproductcid) {
                            if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                                efreeexistproduct.innerHTML = freeexistproductcopy + freeexistproductshow;
                            }
                        }
                        else {
                            efreeexistproduct.innerHTML = "";
                        }
                    } else {
                        efreeexistproduct.innerHTML = "";
                    }

                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionData(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice);
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "success");
                    }
                } else {
                    if(current_price && current_price != 0 && current_price != 0.00 ){
                        current_price = parseFloat(current_price).toFixed(2);
                    }else{
                        current_price = "0.00";
                    }
                    if(next_price && next_price != 0 && next_price != 0.00 ){
                        next_price = parseFloat(next_price).toFixed(2);
                    }else{
                        next_price = "0.00";
                    }
                    if(cTaxA && cTaxA != 0 && cTaxA != 0.00 ){
                        cTaxA = parseFloat(cTaxA).toFixed(2);
                    }else{
                        cTaxA = "0.00";
                    }

                    ediscount.innerHTML = "0.00";
                    etotal.innerHTML = current_price;
                    entotal.innerHTML = next_price;
                    if (shipping_fee && shipping_fee != 0 && shipping_fee != 0.00 ) {
                        eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                    } else {
                        eshipping.innerHTML = "0.00";
                    }
                    efreeproduct.innerHTML = "";
                    if (enfreeproduct){
                        enfreeproduct.innerHTML = "";
                    }
                    efreeexistproduct.innerHTML = "";
                    etax.innerHTML = cTaxA;
                    $("#error-promotion").text("Minimum Spend : " + result.minSpend);
                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionErrorData();
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "error");
                    }
                }
                $("#success-promotion").text(trans('validation.custom.validation.promo.valid'));
            }
            else if (result.promotionType == "Recurring") {

                // Promotion free product
                if (result.freeproductcid) {
                    if (result.freeproductcid.length >= 1) {
                        freeproductcopy = ",";
                    }
                }

                // Promotion free exist product
                if (ablefreeexistproduct == 1) {
                    if (result.freeexistproductcid) {
                        if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                            freeexistproductcopy = "Recurring Free Exist Product : ";
                        }
                    }
                }

                // Promotion discount
                if (shipping_fee && shipping_fee != 0 && shipping_fee != 0.00) {
                    eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                } else {
                    eshipping.innerHTML = "0.00";
                }
                if (etotal) {
                    if (result.isFreeShipping) {
                        if (result.isFreeShipping === 1) {
                            shippingfee = 0.00;
                            shippingfeeget = "0.00";
                            eshipping.innerHTML = shippingfeeget;
                        }
                    }
                    total = parseFloat(subtotal);
                    if (ablefreeexistproduct == 1 && type != "trial-plan") {
                        if (freeexistresultproduct && (freeexistresultproductprice > 0)) {
                            total = total - parseFloat(freeexistresultproductprice);
                        }
                    }
                    if (result.minSpend) {
                        if (result.minSpend > total) {
                            minSpend = 1;
                        }
                    }
                    discount = (((parseFloat(total)) * result.discount) / 100);
                    oridiscount = result.discount + "%";

                    if (result.maxDiscount) {
                        if (result.maxDiscount < discount) {
                            discount = result.maxDiscount;
                            oridiscount = result.maxDiscount;
                        }
                    }
                    if (minSpend == 0) {
                        total = total - discount;
                        total = total + parseFloat(shippingfee);
                        if (type != "trial-plan") {
                            taxa = taxrate / 100 * (total);
                            total = total + taxa;
                        }
                    }
                    else {
                        total = current_price;
                    }
                    if (total < 0) {
                        total = 0;
                    }
                }
                if (entotal && type != "awesome-shave-kits") {
                    if (result.isFreeShipping) {
                        if (result.isFreeShipping === 1) {
                            shippingfee = 0.00;
                            shippingfeeget = "0.00";
                            eshipping.innerHTML = shippingfeeget;
                        }
                    }
                    ntotal = parseFloat(ensubtotal);
                    if (ablefreeexistproduct == 1) {
                        if (freeexistresultproduct && (freeexistresultproductprice > 0)) {
                            ntotal = ntotal - parseFloat(freeexistresultproductprice);
                        }
                    }
                    discount = (((parseFloat(ntotal)) * result.discount) / 100);

                    oridiscount = result.discount + "%";
                    if (result.maxDiscount) {
                        if (result.maxDiscount < discount) {
                            discount = result.maxDiscount;
                            oridiscount = result.maxDiscount;
                        }
                    }
                    if (minSpend == 0) {
                        ntotal = ntotal - discount;
                        ntotal = ntotal + parseFloat(shippingfee);

                        taxa = taxrate / 100 * (ntotal);
                        ntotal = ntotal + taxa;

                    }
                    else {
                        ntotal = next_price;
                    }
                    if (ntotal < 0) {
                        ntotal = 0;
                    }
                }

                if (minSpend == 0) {

                    if(discount && discount != 0 && discount != 0.00){
                        discount = parseFloat(discount).toFixed(2);
                    }else{
                        discount = "0.00";
                    }

                    ediscount.innerHTML = discount;
                    if(total && total != 0 && total != 0.00){
                        total = parseFloat(total).toFixed(2);
                    }else{
                        total = "0.00";
                    }

                    if(ntotal && ntotal != 0 && ntotal != 0.00){
                        ntotal = parseFloat(ntotal).toFixed(2);
                    }else{
                        ntotal  = "0.00";
                    }

                    if(taxa && taxa != 0 && taxa != 0.00){
                        taxa = parseFloat(taxa).toFixed(2);
                    }else{
                        taxa = "0.00";
                    }


                    etotal.innerHTML = total;
                    if (entotal) {
                        entotal.innerHTML = ntotal;
                    }
                    etax.innerHTML = taxa;
                    // Promotion free product
                    if (result.freeproductcid) {
                        if (efreeproduct && result.freeproductcid.length >= 1) {
                            efreeproduct.innerHTML = freeproductcopy + freeproductshow;
                        }
                        else {
                            efreeproduct.innerHTML = "";
                        }
                    } else {
                        efreeproduct.innerHTML = "";
                    }

                    if (result.freeproductcid) {
                        if (enfreeproduct && result.freeproductcid.length >= 1) {
                            enfreeproduct.innerHTML = freeproductcopy + freeproductshow;
                        }
                        else {
                            enfreeproduct.innerHTML = "";
                        }
                    } else {
                        enfreeproduct.innerHTML = "";
                    }
                    // Promotion free exist product
                    if (ablefreeexistproduct == 1) {
                        if (result.freeexistproductcid) {
                            if (efreeexistproduct && result.freeexistproductcid.length >= 1) {
                                efreeexistproduct.innerHTML = freeexistproductcopy + freeexistproductshow;
                            }
                            else {
                                efreeexistproduct.innerHTML = "";
                            }
                        } else {
                            efreeexistproduct.innerHTML = "";
                        }
                    } else {
                        efreeexistproduct.innerHTML = "";
                    }
                    if (ntotal) {
                        ntotal = ntotal;
                    }
                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionData(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice);
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "success");
                    }
                }
                else {
                    ediscount.innerHTML = "0.00";

                    if(current_price && current_price != 0 && current_price != 0.00){
                        current_price = parseFloat(current_price).toFixed(2);
                    }else{
                        current_price = "0.00";
                    }
                    if(next_price && next_price != 0 && next_price != 0.00){
                        next_price = parseFloat(next_price).toFixed(2);
                    }else{
                        next_price = "0.00";
                    }
                    if(cTaxA && cTaxA != 0 && cTaxA != 0.00){
                        cTaxA = parseFloat(cTaxA).toFixed(2);
                    }else{
                        cTaxA = "0.00";
                    }

                    etotal.innerHTML = current_price;
                    if (entotal) {
                        entotal.innerHTML = next_price;
                    }
                    if (shipping_fee && shipping_fee != 0  && shipping_fee != 0.00) {
                        eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                    } else {
                        eshipping.innerHTML = "0.00";
                    }
                    efreeproduct.innerHTML = "";
                    if (enfreeproduct){
                        enfreeproduct.innerHTML = "";
                    }
                    efreeexistproduct.innerHTML = "";
                    etax.innerHTML = cTaxA;
                    $("#error-promotion").text("Minimum Spend : " + result.minSpend);
                    if (sessiontype == "seperatesession") {
                        UpdateSessionPromotionErrorData();
                    }
                    else {
                        onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "error");
                    }
                }
                $("#success-promotion").text(trans('validation.custom.validation.promo.valid'));
            }
            else {
                ediscount.innerHTML = "0.00";

                if(current_price && current_price != 0  && current_price != 0.00){
                    current_price = parseFloat(current_price).toFixed(2);
                }else{
                    current_price = "0.00";
                }

                if(next_price && next_price != 0  && next_price != 0.00){
                    next_price = parseFloat(next_price).toFixed(2);
                }else{
                    next_price = "0.00";
                }

                if(cTaxA && cTaxA != 0  && cTaxA != 0.00){
                    cTaxA = parseFloat(cTaxA).toFixed(2);
                }else{
                    cTaxA = "0.00";
                }

                etotal.innerHTML = current_price;
                if (entotal) {
                    entotal.innerHTML = next_price;
                }
                if (shipping_fee && shipping_fee != 0  && shipping_fee != 0.00) {
                    eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
                } else {
                    eshipping.innerHTML = "0.00";
                }
                etax.innerHTML = cTaxA;
                efreeproduct.innerHTML = "";
                if (enfreeproduct){
                    enfreeproduct.innerHTML = "";
                }
                efreeexistproduct.innerHTML = "";
                $("#error-promotion").text(trans('validation.custom.validation.promo.invalid'));
                if (sessiontype == "seperatesession") {
                    UpdateSessionPromotionErrorData();
                }
                else {
                    onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "error");
                }
            }
            var loading = document.getElementById("loading");
            if (loading) {
                loading.style.display = "none";
            }
        } else {
            var loading = document.getElementById("loading");
            if (loading) {
                loading.style.display = "none";
            }
            return 0;
        }
    }).fail(function (jqXHR, textStatus, error) {
        if (error) {
            let result = "";
            let ediscount = document.getElementById("c-discount");
            let etotal = document.getElementById("c-total");
            let total = current_price;
            let entotal = document.getElementById("c-ntotal");
            let ntotal = next_price;
            let eshipping = document.getElementById("c-shipping");
            let efreeproduct = document.getElementById("c-free-product");
            let enfreeproduct = document.getElementById("c-nfree-product");
            let efreeexistproduct = document.getElementById("c-free-exist-product");
            let etax = document.getElementById("c-tax");
            let freeexistresultproduct = [];
            let ablefreeexistproduct = 0;
            let freeexistresultproductprice = 0.00;
            let cTaxA = ctaxAmount;
            etax.innerHTML = parseFloat(cTaxA).toFixed(2);
            ediscount.innerHTML = "0.00";
            etotal.innerHTML = parseFloat(total).toFixed(2);
            if (entotal) {
                entotal.innerHTML = parseFloat(ntotal).toFixed(2);
            }
            if (shipping_fee && shipping_fee != 0  && shipping_fee != 0.00) {
                eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
            } else {
                eshipping.innerHTML = "0.00";
            }

            efreeproduct.innerHTML = "";
            if (enfreeproduct){
                enfreeproduct.innerHTML = "";
            }
            efreeexistproduct.innerHTML = "";

            $("#error-promotion").text(trans('validation.custom.validation.promo.invalid'));
            if (sessiontype == "seperatesession") {
                UpdateSessionPromotionErrorData();
            }
            else {
                onApplyPromo(result, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, "error");
            }
            var loading = document.getElementById("loading");
            if (loading) {
                loading.style.display = "none";
            }
            return 0;
        }
    });

}

// Promotion
function checkPromotion(promo_code, session, data, type) {
    session_data = JSON.parse(session);
    // console.log(promo_code, session, data, type);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/promotion/check',
        method: "POST",
        cache: false,
        data: {
            "promo_code": promo_code,
            "country": data.user.CountryId,
            "lang": data.langCode,
            "urllang": data.urllangCode,
            "user": data.user,
            "session_data": session_data,
            "type": data.promotion_type,
            "usertype": data.usertype,
        },
    }).done(function (response) {
        if (type === 'normal') {
            pre_applyPromoCheck(response);
        }

        if (type === 'onchange') {
            pre_selectPromoToBeApplied(response, 'normal');
        }
    })
}







// Promotion with next subtotal minspend
// function Promotion(usertype, type, sessiontype) {
//   let promo_code_id = document.getElementById("promo_code");
//   let promo_code = promo_code_id.value ? promo_code_id.value : '';
//   $.ajax({
//     headers: {
//       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     },
//     url: GLOBAL_URL + '/promotion/check',
//     method: "POST",
//     cache: false,
//     data: {
//       "promo_code": promo_code,
//       "country": countryid,
//       "lang": langCode,
//       "urllang": urllangCode,
//       "user": user,
//       "session_data": session_data,
//       "type": type,
//       "usertype": usertype,
//     },
//   }).done(function (response) {
//     if (response) {
//       console.log("dsa + " + JSON.stringify(response));
//       $("#error-promotion").text("");
//       let result = response;
//       let eshipping = document.getElementById("c-shipping");
//       let esubtotal = document.getElementById("c-subtotal");
//       let ediscount = document.getElementById("c-discount");
//       let etotal = document.getElementById("c-total");
//       let ensubtotal = document.getElementById("c-nsubtotal");
//       let entotal = document.getElementById("c-ntotal");
//       let efreeproduct = document.getElementById("c-free-product");
//       let shippingfee;
//       let shippingfeeget;
//       let subtotal;
//       let discount;
//       let oridiscount;
//       let total;
//       let ntotal;
//       let minSpend = 0;
//       let nminSpend = 0;
//       let maxdic;
//       let freeproductshow = "";
//       let freeproductcopy = "";
//       if (esubtotal) {
//         subtotal = esubtotal.innerText;
//       }
//       if (ensubtotal) {
//         ensubtotal = ensubtotal.innerText;
//       }
//       if (eshipping) {
//         shippingfee = shipping_fee;
//       }
//       // if (ediscount) {
//       //     discount = discount.value;
//       // }\

//       // Promotion free product
//       if (efreeproduct && result.freeproductcid.length >= 1) {
//         for (let i = 0; i < result.freeproductcid.length; i++) {
//           if (i == 0) {
//             freeproductshow = result.freeproductsku[i];
//           } else {
//             freeproductshow = freeproductshow + ',' + result.freeproductsku[i];
//           }
//         }
//       }

//       if (result.promotionType == "Instant") {

//         // Promotion free product
//         if (efreeproduct && result.freeproductcid.length >= 1) {
//           freeproductcopy = "Free Product : ";
//         }

//         // Promotion discount
//         eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//         if (result.isFreeShipping) {
//           if (result.isFreeShipping === 1) {
//             shippingfee = 0.00;
//             shippingfeeget = 0.00;
//             eshipping.innerHTML = shippingfeeget;
//           }
//         }
//         if (etotal) {
//           discount = (((parseFloat(subtotal) + parseFloat(shippingfee)) * result.discount) / 100);
//           oridiscount = result.discount + "%";
//           total = parseFloat(subtotal) + parseFloat(shippingfee);
//           if (result.minSpend) {
//             if (result.minSpend > total) {
//               minSpend = 1;
//             }
//           }
//           if (result.maxDiscount) {
//             if (result.maxDiscount < discount) {
//               discount = result.maxDiscount;
//               oridiscount = result.maxDiscount;
//             }
//           }
//           total = total - discount;
//         }
//         if (minSpend == 0) {
//           ediscount.innerHTML = oridiscount;
//           etotal.innerHTML = parseFloat(total).toFixed(2);
//           ntotal = next_price;
//           entotal.innerHTML = parseFloat(ntotal).toFixed(2);

//           // Promotion free product
//           if (efreeproduct && result.freeproductcid.length >= 1) {
//             efreeproduct.innerHTML = freeproductcopy + freeproductshow;
//           }
//           else {
//             efreeproduct.innerHTML = "";
//           }

//           //seperatesession for custom plan
//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionData(result, total.toFixed(2), ntotal.toFixed(2));
//           }
//           else {
//             onApplyPromo(result, total.toFixed(2), ntotal.toFixed(2), "success");
//           }
//         } else {
//           ediscount.innerHTML = "";
//           etotal.innerHTML = parseFloat(current_price).toFixed(2);
//           entotal.innerHTML = parseFloat(next_price).toFixed(2);
//           eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);

//           // Promotion free product
//           efreeproduct.innerHTML = "";

//           $("#error-promotion").text("Minimum Spend : " + result.minSpend);
//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionErrorData();
//           }
//           else {
//             onApplyPromo(result, total, "error");
//           }
//         }
//       }
//       else if (result.promotionType == "Upcoming") {

//         // Promotion free product
//         if (efreeproduct && result.freeproductcid.length >= 1) {
//           freeproductcopy = "Next Month Free Product : ";
//         }

//         // Promotion discount
//         eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//         if (result.isFreeShipping) {
//           if (result.isFreeShipping === 1) {
//             shippingfee = 0.00;
//             shippingfeeget = 0.00;
//             eshipping.innerHTML = shippingfeeget;
//           }
//         }
//         if (entotal) {
//           discount = (((parseFloat(ensubtotal) + parseFloat(shippingfee)) * result.discount) / 100);
//           ntotal = parseFloat(ensubtotal) + parseFloat(shippingfee);
//           oridiscount = result.discount + "%";
//           if (result.minSpend) {
//             if (result.minSpend > ntotal) {
//               nminSpend = 1;
//             }
//           }
//           if (result.maxDiscount) {
//             if (result.maxDiscount < discount) {
//               discount = result.maxDiscount;
//               oridiscount = result.maxDiscount;
//             }
//           }
//           ntotal = ntotal - discount;
//         }

//         if (nminSpend == 0) {
//           ediscount.innerHTML = oridiscount;
//           entotal.innerHTML = parseFloat(ntotal).toFixed(2);
//           total = current_price;
//           etotal.innerHTML = parseFloat(total).toFixed(2);

//           // Promotion free product
//           if (efreeproduct && result.freeproductcid.length >= 1) {
//             efreeproduct.innerHTML = freeproductcopy + freeproductshow;
//           }

//           else {
//             efreeproduct.innerHTML = "";
//           }
//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionData(result, total.toFixed(2), ntotal.toFixed(2));
//           }
//           else {
//             onApplyPromo(result, total.toFixed(2), ntotal.toFixed(2), "success");
//           }
//         } else {
//           ediscount.innerHTML = "";
//           etotal.innerHTML = parseFloat(current_price).toFixed(2);
//           entotal.innerHTML = parseFloat(next_price).toFixed(2);
//           eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//           efreeproduct.innerHTML = "";
//           $("#error-promotion").text("Minimum Spend : " + result.minSpend);
//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionErrorData();
//           }
//           else {
//             onApplyPromo(result, total, "error");
//           }
//         }
//       }
//       else if (result.promotionType == "Recurring") {

//         // Promotion free product
//         if (efreeproduct && result.freeproductcid.length >= 1) {
//           freeproductcopy = "Recurring Free Product : ";
//         }

//         // Promotion discount
//         eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//         if (etotal) {
//           if (result.isFreeShipping) {
//             if (result.isFreeShipping === 1) {
//               shippingfee = 0.00;
//               shippingfeeget = 0.00;
//               eshipping.innerHTML = shippingfeeget;
//             }
//           }
//           discount = (((parseFloat(subtotal) + parseFloat(shippingfee)) * result.discount) / 100);
//           oridiscount = result.discount + "%";
//           total = parseFloat(subtotal) + parseFloat(shippingfee);
//           if (result.minSpend) {
//             if (result.minSpend > total) {
//               minSpend = 1;
//             }
//           }
//           if (result.maxDiscount) {
//             if (result.maxDiscount < discount) {
//               discount = result.maxDiscount;
//               oridiscount = result.maxDiscount;
//             }
//           }
//           if (minSpend == 0) {
//             total = total - discount;
//           }
//           else {
//             total = current_price;
//           }
//         }
//         if (entotal) {
//           if (result.isFreeShipping) {
//             if (result.isFreeShipping === 1) {
//               shippingfee = 0.00;
//               shippingfeeget = 0.00;
//               eshipping.innerHTML = shippingfeeget;
//             }
//           }
//           discount = (((parseFloat(ensubtotal) + parseFloat(shippingfee)) * result.discount) / 100);
//           ntotal = parseFloat(ensubtotal) + parseFloat(shippingfee);
//           oridiscount = result.discount + "%";
//           if (result.minSpend) {
//             if (result.minSpend > ntotal) {
//               nminSpend = 1;
//             }
//           }
//           if (result.maxDiscount) {
//             if (result.maxDiscount < discount) {
//               discount = result.maxDiscount;
//               oridiscount = result.maxDiscount;
//             }
//           }
//           if (nminSpend == 0) {
//             ntotal = ntotal - discount;
//           }
//           else {
//             ntotal = next_price;
//           }
//         }

//         if (nminSpend == 0 || minSpend == 0) {
//           ediscount.innerHTML = oridiscount;
//           etotal.innerHTML = parseFloat(total).toFixed(2);
//           entotal.innerHTML = parseFloat(ntotal).toFixed(2);

//           // Promotion free product
//           if (efreeproduct && result.freeproductcid.length >= 1) {
//             efreeproduct.innerHTML = freeproductcopy + freeproductshow;
//           }
//           else {
//             efreeproduct.innerHTML = "";
//           }

//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionData(result, total.toFixed(2), ntotal.toFixed(2));
//           }
//           else {
//             onApplyPromo(result, total.toFixed(2), ntotal.toFixed(2), "success");
//           }
//         }
//         else {
//           ediscount.innerHTML = "";
//           etotal.innerHTML = parseFloat(current_price).toFixed(2);
//           entotal.innerHTML = parseFloat(next_price).toFixed(2);
//           eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//           efreeproduct.innerHTML = "";
//           $("#error-promotion").text("Minimum Spend : " + result.minSpend);
//           if (sessiontype == "seperatesession") {
//             UpdateSessionPromotionErrorData();
//           }
//           else {
//             onApplyPromo(result, total, "error");
//           }
//         }
//       }

//     } else {
//       return 0;
//     }
//   }).fail(function (jqXHR, textStatus, error) {
//     if (error) {
//       let result = "";
//       let ediscount = document.getElementById("c-discount");
//       let etotal = document.getElementById("c-total");
//       let total = current_price;
//       let entotal = document.getElementById("c-ntotal");
//       let ntotal = next_price;
//       let eshipping = document.getElementById("c-shipping");
//       ediscount.innerHTML = "";
//       etotal.innerHTML = parseFloat(total).toFixed(2);
//       entotal.innerHTML = parseFloat(ntotal).toFixed(2);
//       eshipping.innerHTML = parseFloat(shipping_fee).toFixed(2);
//       $("#error-promotion").text(jqXHR.responseJSON.message);
//       if (sessiontype == "seperatesession") {
//         UpdateSessionPromotionErrorData();
//       }
//       else {
//         onApplyPromo(result, total, "error");
//       }
//       return 0;
//     }
//   });

// }
