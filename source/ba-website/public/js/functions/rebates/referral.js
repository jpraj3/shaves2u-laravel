

// check for referral params
function checkReferralParams() {
    // console.log("referral params");
    if (window.location.pathname.includes('/shave-plans/trial-plan/selection') === true) {
        // if(window.location.pathname.search('/shave-plans/trial-plan') === "/")
        if (getUrlParam(null, 'ref', '') !== undefined && getUrlParam(null, 'src', '') === "link" || getUrlParam(null, 'ref', '') !== undefined && getUrlParam(null, 'src', '') === "email") {
            _referral_unique_code = getUrlParam(null, 'ref', '');
            _src = getUrlParam(null, 'src', '');
            // console.log(_referral_unique_code, _src);
            if (_referral_unique_code) {
                $.ajax({
                    url: GLOBAL_URL + '/referral/get-referrer-info',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    method: "POST",
                    cache: false,
                    data: { "_referral_unique_code": _referral_unique_code, "_src": _src }
                })
                    .done(function (response) {
                        let referral_program_info = {
                            "code": _referral_unique_code,
                            "src": _src,
                            "referrer": response,
                            "referee": { "current_country" : window.currentCountryData}
                        };
                        // console.log(referral_program_info);
                        window.addStorage("referral_program_info", JSON.stringify(referral_program_info));
                    })
            }
        }
    }
}



// ======================================== ON LOAD =======================================================
$(function () {
    // startup functions to run
    checkReferralParams();



    $("#submit_referral_cash_withdrawals").on("submit", function (event) {
        event.preventDefault();
        let bank_name = $("#bank_name").val();
        let bank_account_no = $("#bank_account_no").val();
        let cashout_name = $("#withdrawal_name").val();
        $.ajax({
            url: GLOBAL_URL + '/user/referrals/withdraw-cash',
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            method: "POST",
            cache: false,
            data: { "bank_name": bank_name, "bank_account_no": bank_account_no, "name": cashout_name }
        })
            .done(function (response) {
                // console.log(response);
            })
            .fail(function (jqXHR, textStatus, error) {
                // console.log(error);
            });
    });

    $("#send_referral_invitation_email").on("submit", function (event) {
        event.preventDefault();
        let referral_email_lists = $("#referral_email_lists").val();
        let invite_link = $("#referrals_unique_link").html();
        // console.log(referral_email_lists, invite_link, getUrlParam('ref', ''));
        $.ajax({
            url: GLOBAL_URL + '/user/referrals/send-invitation-email',
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            method: "POST",
            cache: false,
            data: { "referral_email_lists": referral_email_lists, "invite_link": invite_link, "invite_code": getUrlParam(invite_link, 'ref', '') }
        })
            .done(function (response) {
                // console.log(response);
            })
            .fail(function (jqXHR, textStatus, error) {
                // console.log(error);
            });
    });
});
