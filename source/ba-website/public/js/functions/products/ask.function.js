let selected_blade;

// Function: Select blade 
function SelectBlade(productcountriesid) {
    return function () {
        $("#blade-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        $("#blade-tick-" + productcountriesid).removeClass('d-none');
        for (let i = 0; i < 3; i++) {
            if (i != productcountriesid) {
                $("#blade-" + i).removeClass('item-selected').addClass('item-unselected');
                $("#blade-tick-" + i).addClass('d-none');
            }
        }

        if (productcountriesid == 0) {
            $('#selection_price').text('RM 70.00');
        }
        else if (productcountriesid == 1) {
            $('#selection_price').text('RM 85.00');
        }
        else if (productcountriesid == 2) {
            $('#selection_price').text('RM 100.00');
        }
    };
};

// On page load
$(function () {

    //On select blade
    for (let i = 0; i < 3; i++) {
        $("#blade-" + i).click(SelectBlade(i));

        if (i == 0) {
            $("#blade-" + i).click();
        }
    }
})


$("#blade-1").click(function(){
    $("#box-change").html("Test")
});
