
let current_step = 1;

//Function: Update step
function UpdateStep(current_step) {

    // Only show selected panel and hide others
    for (let i = 1; i <= 5; i++) {
        var z = current_step - 1;
        if (i == current_step) {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-unselected').addClass('panel-title-selected');
            $("#step" + i + '-heading.topnav-' + i + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
            $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
            $("#collapse" + i).addClass('show');
            //console.log("a");
        } else {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-selected').addClass('panel-title-unselected');
            $("#collapse" + i).removeClass('show');
            //console.log("b");
        }
    }

    // Do something according to current_step id
    switch (current_step) {
        case 1:
            {
                //do nothing now
            }
            break;
        case 2:
            {
                //do nothing now
            }
            break;
        case 3:
            {
                $(".button-next").removeClass('d-none');
                $("#form-checkout").addClass('d-none');
                $(".checkout-title-1").removeClass('d-none');
                $(".checkout-title-2").addClass('d-none');
                $("#step1-heading").removeClass('d-none');
                $("#step2-heading").removeClass('d-none');
                $("#step3-heading").removeClass('d-none');
            }
            break;
        case 4:
            {
                $(".button-next").addClass('d-none');
                $("#form-checkout").removeClass('d-none');
                $(".checkout-title-1").addClass('d-none');
                $(".checkout-title-2").removeClass('d-none');
                $("#step1-heading").addClass('d-none');
                $("#step2-heading").addClass('d-none');
                $("#step3-heading").addClass('d-none');
            }
            break;
        default:
            break;
    }
}

// Function: Select panel
function SelectPanel(panel_id) {
    return function () {
        //Current step is ahead of the selected panel id
        if (panel_id <= current_step) {
            // Show or hide selected panel  
            var z = current_step - 1;
            //console.log()
            if (!$("#collapse" + panel_id).hasClass('show')) {
                $("#step1-heading.mobile_heading1").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step1-title.mobile_title1").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse1").removeClass('show');
                $("#step2-heading.mobile_heading2").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step2-title.mobile_title2").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse2").removeClass('show');
                $("#step3-heading.mobile_heading3").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step3-title.mobile_title3").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse3").removeClass('show');
                $("#step4-title.mobile_title4").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse4").removeClass('show');
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-unselected').addClass('panel-title-selected');
                $("#step" + panel_id + '-heading.topnav-' + panel_id + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
                $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
                $("#collapse" + panel_id).addClass('show');
                //console.log("trgger A")
            } else {
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse" + panel_id).removeClass('show');
                //console.log("d");
            }
        }
    }
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_GET, null).done(function (data) {
        //console.log("Successfully retrieved session: " + data);

        // Do something with the session data
        if (data) {
            let session_data = JSON.parse(data);
            let previous_step = session_data["selection"]["current_step"];
            current_step = previous_step;
            UpdateStep(previous_step);
            if (previous_step >= 1) {
                if (previous_step >= 2) {
                    if (previous_step >= 3) {
                        if (previous_step >= 4) {
                        }
                    }
                }
            }
        }

    });
}

/*
// Function: Update session data
function UpdateSessionData() {
    let session_data = {};
    session_data["selection"] = {};
    session_data["selection"]["step1"] = { "selected_handle": selected_handle };
    session_data["selection"]["step2"] = { "selected_blade": selected_blade };
    session_data["selection"]["step3"] = { "selected_frequency": selected_frequency };
    session_data["selection"]["step4"] = { "selected_addon_list": selected_addon_list };
    session_data["selection"]["step5"] = { "planId": selected_plan_id };
    session_data["selection"]["current_step"] = current_step;
    session_data["selection"]["plan_type"] = "trial-plan";
    session_data["selection"]["journey_type"] = "selection";
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_SET, JSON.stringify(session_data)).done(function () {
        //console.log("Session successfully stored.");
    });
}

// Function: Clear session data
function ClearSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_CLEAR, null).done(function () {
        //console.log("Successfully cleared session.");
    });
}
*/

// On page load
$(function () {

    // Restore previous (if any)
    // Comment this first for testing: GetSessionData();

    //On select panel
    for (let i = 1; i <= 5; i++) {
        $("#step" + i + "-heading.mobile_heading" + i).click(SelectPanel(i));
        $("#step" + i + "-heading.topnav-" + i + "-heading").click(SelectPanel(i));
    }
    
    //On click next
    $(".button-next").click(function () {
        if (current_step != 5) {
            current_step++;
            UpdateStep(current_step);
        }
        UpdateSessionData();
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step != 1) {
            current_step--;
            UpdateStep(current_step);
        }
    });

    $("#check").click(function () {

    });

    $("#clear").click(function () {
    });
})