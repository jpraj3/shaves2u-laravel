let saved_session_data = {};
let user_exists_loggedin = JSON.parse(
    window.localStorage.getItem("current_user")
);
let current_step = 1;
let productid;
let productcountryId;
let quantity;
let journey_type;
let current_session_data;
let _selection = [];
let _reinitialize_selection = [];
let _reinitialize_checkout = [];
let session_data_for_selection = {};
let session_data_for_checkout = {};
let isfirstclick_selection = false;
let isfirstclick_checkout = false;
let get_selection_session;
let get_checkout_session;
let payment_intent_next_update_type;
let total_price = 0.0;
let gender = JSON.parse(localStorage.getItem("gender_selected"));
//elements for checkout-trial-plan
let element_collapse_1 = document.getElementById("collapse1");
let element_collapse_2 = document.getElementById("collapse2");
let element_collapse_3 = document.getElementById("collapse3");
let element_collapse_4 = document.getElementById("collapse4");
let element_main_4 = document.getElementById("account-header");
let element_main_5 = document.getElementById("shipping-address-header");
let element_main_6 = document.getElementById("payment-header");
let element_1 = document.getElementById("shipping-address-show");
let element_2 = document.getElementById("shipping-address-edit");
let element_3 = document.getElementById("shipping-address-add");
let element_4 = document.getElementById("enable-address-edit");
let element_5 = document.getElementById("enable-address-add");
let element_6 = document.getElementById("cancel-address-edit");
let element_7 = document.getElementById("billing-address-block_for_edit");
let element_7_1 = document.getElementById("billing-address-block_for_add");
let element_8 = document.getElementById("show_card_details_container");
let element_9 = document.getElementById("card_selection_list");
let element_10 = document.getElementById("add_additional_card");
let element_11 = document.getElementById("enableBillingEditCheckBox_for_edit");
let element_11_1 = document.getElementById("enableBillingEditCheckBox_for_add");
let element_12 = document.getElementById("delivery_address_id");
let element_13 = document.getElementById("billing_address_id");
let element_14 = document.getElementById("payment_intent_id");
let element_15 = document.getElementById("payment_intent_next_update_type");
let element_16 = document.getElementById("appendAddress");
let element_17 = document.getElementById("product_country_id");
let element_18 = document.getElementById("appendCard");
let element_19 = document.getElementById("appendCardList");
let element_20 = document.getElementById("add-card");

//input for add & edit addresses
let add_delivery_name = document.getElementById("add_delivery_name");
let add_SSN = document.getElementById("add_SSN");
let add_delivery_address = document.getElementById("add_delivery_address");
let add_delivery_city = document.getElementById("add_delivery_city");
let add_delivery_state = document.getElementById("add_delivery_state");
let add_delivery_postcode = document.getElementById("add_delivery_postcode");
let add_delivery_phone = document.getElementById("add_delivery_phone");
let add_delivery_phoneext = document.getElementById("add_delivery_phoneext");

let add_billing_address = document.getElementById("add_billing_address");
let edit_SSN = document.getElementById("edit_SSN");
let add_billing_city = document.getElementById("add_billing_city");
let add_billing_state = document.getElementById("add_billing_state");
let add_billing_postcode = document.getElementById("add_billing_postcode");
let add_billing_phone = document.getElementById("add_billing_phone");
let add_billing_phoneext = document.getElementById("add_billing_phoneext");

let edit_delivery_name = document.getElementById("edit_delivery_name");
let edit_delivery_address = document.getElementById("edit_delivery_address");
let edit_delivery_city = document.getElementById("edit_delivery_city");
let edit_delivery_state = document.getElementById("edit_delivery_state");
let edit_delivery_postcode = document.getElementById("edit_delivery_postcode");
let edit_delivery_phone = document.getElementById("edit_delivery_phone");
let edit_delivery_phoneext = document.getElementById("edit_delivery_phoneext");

let edit_billing_address = document.getElementById("edit_billing_address");
let edit_billing_city = document.getElementById("edit_billing_city");
let edit_billing_state = document.getElementById("edit_billing_state");
let edit_billing_postcode = document.getElementById("edit_billing_postcode");
let edit_billing_phone = document.getElementById("edit_billing_phone");
let edit_billing_phoneext = document.getElementById("edit_billing_phoneext");

// korean address -------------
let add_delivery_name1 = document.getElementById("add_delivery_name1");
let add_delivery_address1 = document.getElementById("add_delivery_address1");
let add_delivery_city1 = document.getElementById("add_delivery_city1");
let add_delivery_state1 = document.getElementById("add_delivery_state1");
let add_delivery_postcode1 = document.getElementById("add_delivery_postcode1");
let add_delivery_phone1 = document.getElementById("add_delivery_phone1");
let add_delivery_phoneext1 = document.getElementById("add_delivery_phoneext1");

let add_billing_address1 = document.getElementById("add_billing_address1");
let add_billing_city1 = document.getElementById("add_billing_city1");
let add_billing_state1 = document.getElementById("add_billing_state1");
let add_billing_postcode1 = document.getElementById("add_billing_postcode1");
let add_billing_phone1 = document.getElementById("add_billing_phone1");
let add_billing_phoneext1 = document.getElementById("add_billing_phoneext1");

let edit_delivery_name1 = document.getElementById("edit_delivery_name1");
let edit_delivery_address1 = document.getElementById("edit_delivery_address1");
let edit_delivery_city1 = document.getElementById("edit_delivery_city1");
let edit_delivery_state1 = document.getElementById("edit_delivery_state1");
let edit_delivery_postcode1 = document.getElementById(
    "edit_delivery_postcode1"
);
let edit_delivery_phone1 = document.getElementById("edit_delivery_phone1");
let edit_delivery_phoneext1 = document.getElementById(
    "edit_delivery_phoneext1"
);

let edit_billing_address1 = document.getElementById("edit_billing_address1");
let edit_billing_city1 = document.getElementById("edit_billing_city1");
let edit_billing_state1 = document.getElementById("edit_billing_state1");
let edit_billing_postcode1 = document.getElementById("edit_billing_postcode1");
let edit_billing_phone1 = document.getElementById("edit_billing_phone1");
let edit_billing_phoneext1 = document.getElementById("edit_billing_phoneext1");

function onUpdateCard(data) {
    // console.log("onUpdateCard", data);
    $(".append_selected_cardnumber").empty();
    $(".append_selected_cardexpiry").empty();

    // update new card last 4 digits
    let last4digits = data.cardNumber ? data.cardNumber : "1234";
    $(".cc_last4digits").html(last4digits);
    let expirymy =
        data.expiredMonth && data.expiredYear
            ? data.expiredMonth + "/" + data.expiredYear.slice(-2)
            : "xx/xx";
    $(".cc_expmy").html(expirymy);
    ChangeCardBackground(data.branchName);
    $(".append_selected_cardnumber").html("xxxx xxxx xxxx " + data.cardNumber);
    $(".append_selected_cardexpiry").html(
        data.expiredMonth + " / " + data.expiredYear
    );
}

async function updateCardSelectionList(user_id) {
    // console.log("updateCardSelectionList: => " + JSON.stringify(user_id));

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: API_URL + "/ba/stripe/card",
        method: "POST",
        cache: false,
        data: { id: user_id }
    })
        .done(function (response) {
            // console.log("updateCardSelectionList: => " + JSON.stringify(response));
            if (response) {
                if (
                    document.body.contains(
                        document.getElementById("show_card_details_container")
                    )
                ) {
                    document.getElementById("appendCardList").innerHTML = "";
                    response.forEach(function (el) {
                        // console.log("el: => " + JSON.stringify(el));
                        elChild = document.createElement("div");
                        let x =
                            '<div class="container col-12 individual-card-selection" style="border: 2px solid black; margin:1%;padding:10px;cursor: pointer;" onClick="onUpdateCardSelection(' +
                            el.id +
                            ')">' +
                            '<div class="form-group">' +
                            '<label for="">Card Name</label>' +
                            "<p> " +
                            el.cardName +
                            "</p>" +
                            "</div>" +
                            '<div class="form-group">' +
                            '<label for="">Card Number</label>' +
                            "<p> XXXX XXXX XXXX " +
                            el.cardNumber +
                            "</p>" +
                            "</div>" +
                            '<div class="form-group">' +
                            '<label for="">Card Number</label>' +
                            "<p> " +
                            el.expiredMonth +
                            " / " +
                            el.expiredYear +
                            "</p>" +
                            "</div>" +
                            "</div>";
                        elChild.innerHTML = x;
                        document
                            .getElementById("appendCardList")
                            .appendChild(elChild);
                    });
                }
            } else {
                return (card_list = 0);
            }
        })
        .fail(function (jqXHR, textStatus, error) {
            if (error) {
                // console.log(error);
                return (card_list = 0);
            }
        });
}

async function CheckSessionTKData() {
    var data = await session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null);
    if (data) {
        return JSON.parse(data);
    } else {
        return data;
    }
}

function UpdateEvent(channel_type, event_location_code) {
    let checkout_data_event = {
        journey_type: "checkout",
        update_type: "update_event",
        event: {
            ba_channel_type: channel_type,
            ba_event_location_code: event_location_code
        }
    };
    UpdateSessionData(checkout_data_event);
}

function UpdateIsDirectTrial(isDirectTrial) {
    let checkout_data_isdirecttrial = {
        journey_type: "checkout",
        update_type: "isDirectTrial",
        isDirectTrial: isDirectTrial
    };

    return UpdateSessionData(checkout_data_isdirecttrial).done(function (response) {
        return response
    });
}

// on proceed payment button
function onProceedPayment(
    session_data,
    session_checkout_data,
    payment_intent,
    data
) {
    event_data = {
        ba_channel_type: $("#ba_channel_type").val(),
        ba_event_location_code: $("#ba_event_location_code").val()
    };

    let isDirectTrial = $("#ba_is_direct_trial").is(":checked");

    UpdateIsDirectTrial(isDirectTrial).done(function () {
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
        // console.log(event_data, isDirectTrial);
        // START

        // Check if the user have already subscribed for trial plan before
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + "/ba/user/check/trial",
            data: { userid: user.id },
            success: function (data) {
                if (data.payload === "exist") {
                    $("#loading").css("display", "none");
                    $("#notification_trial_plan_exist").modal("show");
                    // session(SESSION_USER_INFO, SESSION_CLEAR, null).done(
                    //     function () {
                    //         setTimeout(function () {
                    //             window.location.href =
                    //                 GLOBAL_URL + "/products/select-category";
                    //         }, 500);
                    //     }
                    // );
                } else {
                    // START CONFIRM
                    $("#loading").css("display", "block");
                    var type = "trial-plan";
                    let payment_intent_id = payment_intent.id;
                    let card_id = payment_intent.payment_method;
                    // console.log("CARD ID =>>>>>>> ", payment_intent);
                    let CONFIRM_PURCHASE_URL =
                        API_URL + "/ba/plan/confirm-purchase";
                    let OTP_URL = API_URL + "/ba/stripe/confirm";
                    let RETURN_URL_WITH_OTP =
                        window.location.origin +
                        GLOBAL_URL +
                        `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=1`;
                    let RETURN_URL_WITHOUT_OTP =
                        window.location.origin +
                        GLOBAL_URL +
                        `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=0&payment_intent=${payment_intent_id}`;
                    let RETURN_URL_WITHOUT_OTP_FREE_PURCHASE =
                        window.location.origin +
                        GLOBAL_URL +
                        `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=0&is_free_purchase=1`;
                    // console.log("seller", window.localStorage.getItem("seller"));
                    $.ajax({
                        url: CONFIRM_PURCHASE_URL,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                                "content"
                            )
                        },
                        method: "POST",
                        cache: false,
                        data: {
                            type: type,
                            payment_intent: payment_intent_id,
                            session_selection: JSON.parse(session_data),
                            session_checkout: session_checkout_data,
                            appType: "baWebsite",
                            country_id: countryid,
                            country_code: country_id,
                            user_id: user_id,
                            langCode: langCode,
                            urllangCode: urllangCode,
                            seller: window.localStorage.getItem("seller"),
                            event_data: event_data,
                            isDirectTrial: isDirectTrial,
                            gender: gender
                        }
                    }).done(function (data) {
                        // console.clear();
                        // console.log("build items: >>>>>>>", data);
                        RETURN_URL_WITH_OTP =
                            window.location.origin +
                            GLOBAL_URL +
                            `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=1&orderid=${data.metadata.orderid}&subscriptionid=${data.metadata.subscriptionid}&receiptid=${data.metadata.receiptid}&countryid=${countryid}&gender=${gender}`;
                        RETURN_URL_WITHOUT_OTP =
                            window.location.origin +
                            GLOBAL_URL +
                            `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=0&payment_intent=${payment_intent_id}&orderid=${data.metadata.orderid}&subscriptionid=${data.metadata.subscriptionid}&receiptid=${data.metadata.receiptid}&countryid=${countryid}&gender=${gender}`;
                        RETURN_URL_WITHOUT_OTP_FREE_PURCHASE =
                            window.location.origin +
                            GLOBAL_URL +
                            `/stripe/redirect-from-stripe?type=${type}&card_id=${card_id}&otp=0&is_free_purchase=1&orderid=${data.metadata.orderid}&subscriptionid=${data.metadata.subscriptionid}&receiptid=${data.metadata.receiptid}&countryid=${countryid}&gender=${gender}`;
                        // AFTER CONFIRM START
                        // console.log(JSON.stringify(data));
                        if (data.id) {
                            // If success creating subscription, orders and receipt, OR order id already created before, then proceed to stripe payment
                            $.ajax({
                                url: OTP_URL,
                                headers: {
                                    "X-CSRF-TOKEN": $(
                                        'meta[name="csrf-token"]'
                                    ).attr("content")
                                },
                                method: "POST",
                                cache: false,
                                data: {
                                    payment_intent_id: payment_intent_id,
                                    return_url: RETURN_URL_WITH_OTP,
                                    country_id: countryid
                                }
                            })
                                .done(function (data) {
                                    // console.clear();
                                    // console.log(data);
                                    // console.log(data);
                                    if (data.next_action != null) {
                                        if (data.status == "requires_source_action" || data.status == "requires_action") {
                                            // If got OTP
                                            window.location = data.next_action.redirect_to_url.url;
                                        } else {
                                            // If no OTP
                                            window.location = RETURN_URL_WITHOUT_OTP;
                                        }
                                    } else {
                                        // If no OTP
                                        window.location = RETURN_URL_WITHOUT_OTP;
                                    }
                                    // $("#loading").css("display", "none");
                                })
                                .fail(function (jqXHR, textStatus, error) {
                                    window.location = RETURN_URL_WITHOUT_OTP;
                                    // $("#loading").css("display", "none");
                                });
                        } else if (data == "isFree") {
                            window.location = RETURN_URL_WITHOUT_OTP_FREE_PURCHASE;
                            // $("#loading").css("display", "none");
                        } else {
                            // console.log("Failed to confirm purchase.");
                            $("#loading").css("display", "none");
                        }
                        // FINISH CONFIRM START
                    })
                        .fail(function (jqXHR, textStatus, error) {
                            $("#loading").css("display", "none");
                            // console.log("Failed to confirm purchase.");
                        });
                }
                // FINISH CONFIRM
            },
            error: function (jqXHR) {
                $("#loading").css("display", "none");
                location.reload();
            }
        });
        // END
    });
}

// Function: Get session data
function GetSessionData() {
    let sessionABC = [];
    session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).done(function (
        data
    ) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            return data;
        } else {
        }
    });
    return sessionABC;
}

// Function: Get session data
function GetSessionDataV2() {
    let sessionABC = [];
    session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).done(function (
        data
    ) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            sessionABC[0] = get_checkout_session;
        } else {
        }
    });
    return sessionABC;
}

function onUpdateCardSelection(card_id) {
    $("#loading").css("display", "block");
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: API_URL + "/ba/stripe/card/each",
        method: "POST",
        cache: false,
        data: { id: card_id }
    })
        .done(function (response) {
            if (response) {
                // console.log(countryid, card_id, response, payment_intent);
                $.ajax({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    url: API_URL + "/ba/stripe/card/select",
                    method: "POST",
                    cache: false,
                    data: {
                        id: card_id,
                        type: "trial-plan",
                        payment_intent: payment_intent.id,
                        CountryId: countryid
                    }
                }).done(function (response2) {
                    // console.log(response2);
                    payment_intent.id = response2.id;
                    // console.log("latest payment intent: ", payment_intent);
                    checkout_data_card = {
                        journey_type: "checkout",
                        update_type: "selected_card",
                        card_id: card_id,
                        card: response.payload
                    };

                    onUpdateCard(response.payload);
                    UpdateSessionData(checkout_data_card);
                    CardBack();

                    // update new card last 4 digits
                    let last4digits = response.payload.cardNumber
                        ? response.payload.cardNumber
                        : "1234";
                    $(".cc_last4digits").html(last4digits);
                    let expirymy =
                        response.payload.expiredMonth && response.payload.expiredYear
                            ? response.payload.expiredMonth +
                            "/" +
                            response.payload.expiredYear.slice(-2)
                            : "xx/xx";
                    $(".cc_expmy").html(expirymy);
                    ChangeCardBackground(response.payload.branchName);
                });
            } else {
                return 0;
            }
        })
        .fail(function (jqXHR, textStatus, error) {
            $("#loading").css("display", "none");
            if (error) {
                // console.log(error);
                return 0;
            }
        });
}

function onAddCard(card_id, card, payment_intent_id) {
    // console.log(card_id, card);
    checkout_data_card = {
        journey_type: "checkout",
        update_type: "selected_card",
        card_id: card_id,
        card: card,
        payment_intent_id: payment_intent_id
    };
    UpdateSessionData(checkout_data_card);
    CardBack();
}

function onUpdateDeliverySelection(addressUpdates, type) {
    if (type === "default") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
    if (type === "update") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        $("#shipping-address-add").hide();
        $("#shipping-address-edit").show();
        AddressBack(type);
    }
    if (type === "address_update_only") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
}

// Function: Update payment intent
function onUpdatePaymentIntent(payment_intent_data, next_update_type) {
    if (next_update_type === "first-update") {
        payment_intent_next_update_type = "first-update";
    } else if (next_update_type === "price-update") {
        payment_intent_next_update_type = "price-update";
    } else if (next_update_type === "card-update") {
        payment_intent_next_update_type = "card-update";
    }

    return payment_intent_next_update_type;
}

// Function: Update session data
function UpdateSessionData(data) {
    // console.log("UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> 3333");
    // session selection
    // session checkout
    let _session_checkout = {};
    let _session_checkout_user = {};
    let _session_checkout_promo = {};
    let _session_checkout_selected_card = {};
    let _session_checkout_selected_delivery_address = {};
    let _session_checkout_event = {};
    let _session_checkout_summary = {};

    var journey_type = data.journey_type;
    if (journey_type === "selection") {
        if (data.update_type === "update_annual_suggestion") {
            _session_selection_switch_plans_update = {
                isSwitched: data.switch_plans.isSwitched,
                _normal: data.switch_plans._normal,
                _annual: data.switch_plans._annual,
                isSelected: data.switch_plans.isSelected
            };
            _session_selection_summary_update = {
                current_price: data.summary.current_price,
                next_price: data.summary.next_price,
                planId: data.summary.planId,
                shipping_fee: data.summary.shipping_fee
            };
            let ___session_data = JSON.parse(session_data);
            ___session_data.selection.switch_plans = _session_selection_switch_plans_update;
            ___session_data.selection.summary = _session_selection_summary_update;
            session_data_for_selection = ___session_data;
            session_data = ___session_data;
            // console.log("latest updated session_data", session_data);
            return session(
                SESSION_SELECTION_TRIAL_PLAN,
                SESSION_SET,
                JSON.stringify(session_data)
            ).done(function (response) {
                session(SESSION_SELECTION_TRIAL_PLAN, SESSION_GET, null).done(
                    function (response) {
                        session_data = response;
                    }
                );
            });
        }
    } else if (journey_type === "checkout") {
        // console.log("UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> 444");
        if (isfirstclick_checkout === false) {
            // console.log("UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> 5555", data);
            isfirstclick_checkout = true;
            _session_checkout = {
                checkout: {
                    user: {},
                    selected_card: {
                        id: "",
                        risk_level: "",
                        payment_intent_id: "",
                        type: "",
                        customer_id: ""
                    },
                    selected_address: {},
                    promo: {},
                    event: {},
                    summary: {},
                    payment_data: {},
                    switch_plans: {
                        isSwitched: false,
                        _normal: null,
                        _annual: null,
                        isSelected: "normal"
                    }
                }
            };
            session_data_for_checkout = _session_checkout;

            // console.log(data);
            if (user_exists_loggedin !== null) {
                _session_checkout_user = {
                    id: user_exists_loggedin.id,
                    email: user_exists_loggedin.email,
                    defaultLanguage: user_exists_loggedin.defaultLanguage,
                    IsActive: user_exists_loggedin.isActive,
                    CountryId: user_exists_loggedin.CountryId
                };
                session_data_for_checkout.checkout.user = _session_checkout_user;
            }

            if (data.update_type === "default") {
                // console.log(
                //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> 2222",
                //     data
                // );
                if (data.user && data.user.length !== 0) {
                    // console.log(
                    //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 1"
                    // );
                    _session_checkout_user = {
                        id: data.user.id,
                        email: data.user.email,
                        defaultLanguage: data.user.defaultLanguage,
                        IsActive: data.user.isActive,
                        CountryId: data.user.CountryId
                    };
                    session_data_for_checkout.checkout.user = _session_checkout_user;
                }

                if (data.promo && data.promo.length !== 0) {
                    // console.log(
                    //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 2"
                    // );
                    _session_checkout_promo = data.session_checkout.promo;
                    session_data_for_checkout.checkout.promo = _session_checkout_promo;
                }

                if (
                    data.delivery_address &&
                    data.delivery_address.length !== 0
                ) {
                    // console.log(
                    //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 3"
                    // );
                    _session_checkout_selected_delivery_address = {
                        delivery_address: data.delivery_address
                            ? data.delivery_address.id
                            : "",
                        billing_address: data.billing_address
                            ? data.billing_address.id
                            : ""
                    };
                    session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                }

                if (data.default_card && data.default_card.length !== 0) {
                    // console.log(
                    //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 4"
                    // );
                    _session_checkout_selected_card = {
                        id: data.default_card[0]
                            ? data.default_card[0].id
                            : null,
                        payment_intent_id: data.default_card[0]
                            ? data.default_card[0].pymt_intent_tk
                            : null,
                        customer_id: data.default_card
                            ? data.default_card.customerId
                            : null,
                        type: "trial-plan",
                        risk_level: data.default_card
                            ? data.default_card.risk_level
                            : null
                    };
                    session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
                }

                if (
                    data.session_checkout.event &&
                    data.session_checkout.event.length !== 0
                ) {
                    _session_checkout_event = data.session_checkout.event;
                    session_data_for_checkout.checkout.event = _session_checkout_event;
                }

                if (
                    data.session_checkout.summary &&
                    data.session_checkout.summary.length !== 0
                ) {
                    _session_checkout_summary = data.session_checkout.summary;
                    session_data_for_checkout.checkout.summary = _session_checkout_summary;
                }

                if (data.checkout_details.length !== 0) {
                    // console.log(
                    //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 5"
                    // );
                    _session_checkout_payment_data = data.checkout_details;
                    session_data_for_checkout.checkout.payment_data = _session_checkout_payment_data;
                }
            }

            if (data.update_type === "selected_user") {
                _session_checkout_user = { test: "test" };
                session_data_for_checkout.checkout.user.push(
                    _session_checkout_user
                );
            }
            if (data.update_type === "selected_promo") {
                if (data.status === "error") {
                    // console.log(data);
                    _session_checkout_promo = {};
                } else {
                    _session_checkout_promo = {
                        promo_id: data.data.pid,
                        promo_code: data.data.code,
                        promo_isGeneric: data.data.isGeneric,
                        promo_discount: data.data.discount,
                        promo_free_product_id: data.data.freeProductCountryIds,
                        promo_free_exist_product_id:
                            data.data.freeExistProductCountryIds,
                        promo_ablefreeexistproduct: data.ablefreeexistproduct
                            ? data.ablefreeexistproduct
                            : "0",
                        promo_freeexistresultproduct: data.freeexistresultproduct
                            ? data.freeexistresultproduct
                            : "",
                        promo_freeexistresultproductprice:
                            data.freeexistresultproductprice,
                        promo_product_bundle_id: data.data.ProductBundleId,
                        promo_promotionType: data.data.promotionType,
                        promo_minSpend: data.data.minSpend,
                        promo_maxDiscount: data.data.maxDiscount,
                        promo_isFreeShipping: data.data.isFreeShipping,
                        promo_timePerUser: data.data.timePerUser,
                        promo_total_price: data.total,
                        promo_next_total_price: data.ntotal
                    };
                }
                session_data_for_checkout.checkout.promo = _session_checkout_promo;
            }
            if (data.update_type === "selected_address") {
                _session_checkout_selected_delivery_address = {
                    delivery_address: data.delivery_address_id,
                    billing_address: data.billing_address_id
                };
                session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
            }
            if (data.update_type === "selected_card") {
                _session_checkout_selected_card = {
                    id: data.card.id,
                    payment_intent_id: data.payment_intent_id,
                    customer_id: data.card.customerId,
                    type: data.card.payment_method,
                    risk_level: data.card.risk_level
                };
                session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
            }
            if (data.update_type === "update_event") {
                _session_checkout_event = {
                    ba_channel_type: data.event.ba_channel_type,
                    ba_event_location_code: data.event.ba_event_location_code
                };
                session_data_for_checkout.checkout.event = _session_checkout_event;
            }
            if (data.update_type === "isDirectTrial") {
                _session_checkout_summary = {
                    isDirectTrial: data.isDirectTrial
                };
                session_data_for_checkout.checkout.summary = _session_checkout_summary;
            }

            // console.log(
            //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> VVVVV 6",
            //     session_data_for_checkout
            // );
            return session(
                SESSION_CHECKOUT_TRIAL_PLAN,
                SESSION_SET,
                JSON.stringify(session_data_for_checkout)
            ).done(function () {
                session_checkout_data = session_data_for_checkout;
                $("#loading").css("display", "none");
                currentSession();
                return true;
            });
        } else {
            return session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).done(
                function (response) {
                    // Do something with the session data
                    if (response) {
                        get_checkout_session = response;
                        session_data_for_checkout = JSON.parse(response);
                        // console.log(data);
                        // console.log(
                        //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>> 111"
                        // );
                        if (data.update_type === "default") {
                            // console.log(
                            //     "UPDATE CHECKOUT SESSION ==== >>>>>>>>>>>>"
                            // );
                            if (data.user && data.user.length !== 0) {
                                _session_checkout_user = {
                                    id: data.user.id,
                                    email: data.user.email,
                                    defaultLanguage: data.user.defaultLanguage,
                                    IsActive: data.user.isActive,
                                    CountryId: data.user.CountryId
                                };
                                session_data_for_checkout.checkout.user = _session_checkout_user;
                            }

                            if (data.promo && data.promo.length !== 0) {
                                _session_checkout_promo =
                                    data.session_checkout.promo;
                                session_data_for_checkout.checkout.promo = _session_checkout_promo;
                            }

                            if (
                                data.delivery_address &&
                                data.delivery_address.length !== 0
                            ) {
                                _session_checkout_selected_delivery_address = {
                                    delivery_address: data.delivery_address
                                        ? data.delivery_address.id
                                        : "",
                                    billing_address: data.billing_address
                                        ? data.billing_address.id
                                        : ""
                                };
                                session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                            }

                            if (
                                data.default_card &&
                                data.default_card.length !== 0
                            ) {
                                _session_checkout_selected_card = {
                                    id: data.default_card[0]
                                        ? data.default_card[0].id
                                        : null,
                                    payment_intent_id: data.default_card[0]
                                        ? data.default_card[0].pymt_intent_tk
                                        : null,
                                    customer_id: data.default_card
                                        ? data.default_card.customerId
                                        : null,
                                    type: "trial-plan",
                                    risk_level: data.default_card
                                        ? data.default_card.risk_level
                                        : null
                                };
                                session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
                            }

                            if (
                                data.session_checkout.event &&
                                data.session_checkout.event.length !== 0
                            ) {
                                _session_checkout_event =
                                    data.session_checkout.event;
                                session_data_for_checkout.checkout.event = _session_checkout_event;
                            }

                            if (
                                data.session_checkout.summary &&
                                data.session_checkout.summary.length !== 0
                            ) {
                                _session_checkout_summary =
                                    data.session_checkout.summary;
                                session_data_for_checkout.checkout.summary = _session_checkout_summary;
                            }

                            if (data.checkout_details.length !== 0) {
                                _session_checkout_payment_data =
                                    data.checkout_details;
                                session_data_for_checkout.checkout.payment_data = _session_checkout_payment_data;
                            }
                        }

                        if (data.update_type === "selected_user") {
                            _session_checkout_user = { test: "test" };
                            session_data_for_checkout.checkout.user.push(
                                _session_checkout_user
                            );
                        }

                        if (data.update_type === "selected_promo") {
                            if (data.status === "error") {
                                _session_checkout_promo = {};
                            } else {
                                _session_checkout_promo = {
                                    promo_id: data.data.pid,
                                    promo_code: data.data.code,
                                    promo_isGeneric: data.data.isGeneric,
                                    promo_discount: data.data.discount,
                                    promo_free_product_id:
                                        data.data.freeProductCountryIds,
                                    promo_free_exist_product_id:
                                        data.data.freeExistProductCountryIds,
                                    promo_ablefreeexistproduct: data.ablefreeexistproduct
                                        ? data.ablefreeexistproduct
                                        : "0",
                                    promo_freeexistresultproduct: data.freeexistresultproduct
                                        ? data.freeexistresultproduct
                                        : "",
                                    promo_freeexistresultproductprice:
                                        data.freeexistresultproductprice,
                                    promo_product_bundle_id:
                                        data.data.ProductBundleId,
                                    promo_promotionType:
                                        data.data.promotionType,
                                    promo_planIds: data.data.planIds,
                                    promo_minSpend: data.data.minSpend,
                                    promo_maxDiscount: data.data.maxDiscount,
                                    promo_isFreeShipping:
                                        data.data.isFreeShipping,
                                    promo_timePerUser: data.data.timePerUser,
                                    promo_total_price: data.total,
                                    promo_next_total_price: data.ntotal
                                };
                            }
                            session_data_for_checkout.checkout.promo = _session_checkout_promo;
                        }
                        if (data.update_type === "selected_address") {
                            _session_checkout_selected_delivery_address = {
                                delivery_address: data.delivery_address_id,
                                billing_address: data.billing_address_id
                            };
                            session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                        }
                        if (data.update_type === "selected_card") {
                            _session_checkout_selected_card = {
                                id: data.card.id,
                                payment_intent_id: data.payment_intent_id,
                                customer_id: data.card.customerId,
                                type: data.card.payment_method,
                                risk_level: data.card.risk_level
                            };
                            session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
                        }
                        if (data.update_type === "update_event") {
                            _session_checkout_event = {
                                ba_channel_type: data.event.ba_channel_type,
                                ba_event_location_code:
                                    data.event.ba_event_location_code
                            };
                            session_data_for_checkout.checkout.event = _session_checkout_event;
                        }
                        if (data.update_type === "update_annual_suggestion") {
                            _session_checkout_switch_plans = {
                                isSwitched: data.switch_plans.isSwitched,
                                _normal: data.switch_plans._normal,
                                _annual: data.switch_plans._annual,
                                isSelected: data.switch_plans.isSelected
                            };

                            session_data_for_checkout.checkout.switch_plans = _session_checkout_switch_plans;
                            session_data_for_checkout.checkout.payment_data =
                                data.payment_data;
                        }
                        if (data.update_type === "isDirectTrial") {
                            _session_checkout_summary = {
                                isDirectTrial: data.isDirectTrial
                            };
                            session_data_for_checkout.checkout.summary = _session_checkout_summary;
                        }
                        return session(
                            SESSION_CHECKOUT_TRIAL_PLAN,
                            SESSION_SET,
                            JSON.stringify(session_data_for_checkout)
                        ).done(function () {
                            session_checkout_data = session_data_for_checkout;
                            $("#loading").css("display", "none");
                            currentSession();
                            return true;
                        });
                    } else {
                        return true;
                    }
                }
            );
        }
    }
}

async function currentSession() {
    // console.log("currentSession start");
    let result = await GetSessionDataV2();
    // console.log(result);
}

// Function: get session data for checkout
function setSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_SET, null).done(function () {
        // console.log(
        //     "SESSION_CHECKOUT_TRIAL_PLAN | Successfully set checkout session."
        // );
    });
}

// Function: get session data for checkout
function getSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).done(function (
        response
    ) {
        // console.log(
        //     "SESSION_CHECKOUT_TRIAL_PLAN | Successfully get checkout session. ",
        //     response
        // );
    });
}

// Function: Clear session data
function ClearSessionData_CHECKOUT() {
    return session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_CLEAR, null).done(
        function (response) {
            if (response) {
                // console.log(
                //     "SESSION_CHECKOUT_TRIAL_PLAN | Successfully cleared checkout session."
                // );
                return response;
            }
        }
    );
}

async function ClearUpdateSessionData_CHECKOUT(data) {
    var clear = await session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_CLEAR, null);
    if (clear == "success") {
        // console.log(
        //     "SESSION_CHECKOUT_TRIAL_PLAN | Successfully cleared checkout session. "
        // );
        // console.log("update checkout session on load");
        return UpdateSessionData(data);
    }
}

//Function: Update step
function UpdateStep(current_step) {
    if (current_step == 1) {
        $("#button-next").text("CONFIRM ACCOUNT");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "none";
        }

        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 2) {
        $("#button-next").text(confirmsatext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "none";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 3) {
        // trigger click add/edit address submit
        $("#button-next").text(confirmcardtext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 4) {
        $("#button-next").text(confirmeventtext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 5) {
        $("#button-next").text("CONFIRM EVENT LOCATION");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 6) {
        $("#button-next").text("PROCEED TO PAY");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
}

function steps() {
    //On click next
    $("#button-next").click(function () {
        // console.log("current_step", current_step);
        if (current_step == 6) {
            //do nothing
        } else {
            current_step++;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            if (current_step == 2) {
                shippingaddress();
                UpdateStep(current_step);
            }
            if (current_step == 3) {
                if (!$("#add-addresses-block" + name).length == 0) {
                    if ($("#add-addresses-block").valid() === true) {
                        $("#add-addresses-block").submit();
                        payment();
                        UpdateStep(current_step);
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text(confirmsatext);
                    }
                }
                if (!$("#edit-addresses-block" + name).length == 0) {
                    if ($("#edit-addresses-block").valid() === true) {
                        $("#edit-addresses-block").trigger("submit");
                        payment();
                        UpdateStep(current_step);
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text(confirmsatext);
                    }
                }
            }
            if (current_step == 4) {
                if (!$("#form_card" + name).length == 0) {
                    if ($("#form_card").valid() === true) {
                        $("#form_card").trigger("submit");
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text(confirmdetailtext);
                    }
                }
            }
            if (current_step == 5) {
                if (!$("#event_location_form" + name).length == 0) {
                    validateEvents("event_location_form");
                    if ($("#event_location_form").valid() === true) {
                        eventLocation();
                        UpdateStep(current_step);
                        $("#button-next").addClass("display-none");
                        $("#form-pay").removeClass("hidden");
                        $("#form-pay-kr").removeClass("hidden");
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text("CONFIRM EVENT LOCATION");
                        $("#button-next").removeClass("display-none");
                        $("#form-pay").addClass("hidden");
                        $("#form-pay-kr").addClass("hidden");
                    }
                }

                if (!$("#ba_annual_plan").length == 0) {
                    if (country_id === "sg") {
                        $("#ba_is_direct_trial").trigger('click')
                        $("#select_annual_plan").trigger('click');
                    }
                }
            }
            UpdateStep(current_step);
        }
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step == 1) {
            let selection_url =
                window.location.origin +
                GLOBAL_URL +
                "/trial/selection?gender=" + gender;
            window.location = selection_url;
        } else {
            current_step--;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            if (current_step == 2) {
                shippingaddress();
                UpdateStep(current_step);
            }
            if (current_step == 3) {
                payment();
                UpdateStep(current_step);
            }
        }
    });
}

// Begin: Functions related to shipping address
function account() {
    current_step = 1;
    $("#button-next").removeClass("display-none");
    //$("#form-pay").addClass('display-none');
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "none";
    }
    if (document.getElementById("collapse1").offsetParent !== null) {
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.add("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function shippingaddress() {
    // console.log("shippingaddress");
    current_step = 2;
    $("#button-next").removeClass("display-none");
    //$("#form-pay").addClass('display-none');
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "none";
    }
    // console.log(document.getElementById("collapse2").offsetParent !== null);
    if (document.getElementById("collapse2").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.add("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function editAddress() {
    if (
        document.getElementById("shipping-address-show").offsetParent !== null
    ) {
        document.getElementById("shipping-address-show").style.display = "none";

        if (document.getElementById("shipping-address-edit")) {
            document.getElementById("shipping-address-edit").style.display =
                "block";
        } else if (document.getElementById("shipping-address-add")) {
            document.getElementById("shipping-address-add").style.display =
                "block";
        }

        document.getElementById("enable-address-edit").style.display = "none";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "block";
    } else {
        document.getElementById("shipping-address-show").style.display =
            "block";

        if (document.getElementById("shipping-address-edit")) {
            document.getElementById("shipping-address-edit").style.display =
                "none";
        } else if (document.getElementById("shipping-address-add")) {
            document.getElementById("shipping-address-add").style.display =
                "none";
        }

        document.getElementById("enable-address-edit").style.display = "block";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "none";
    }
}

function onAddDeliveryAddress(data) {
    edit_delivery_name.value = data.delivery_address_fullname;
    if (edit_SSN) {
        edit_SSN.value = data.delivery_address_SSN;
    }
    edit_delivery_address.value = data.delivery_address_address;
    edit_delivery_city.value = data.delivery_address_city;
    edit_delivery_state.value = data.delivery_address_state;
    edit_delivery_postcode.value = data.delivery_address_portalCode;
    edit_delivery_phone.value = data.delivery_address_contactNumber.replace(
        "+" + userPhoneExt,
        ""
    );
    edit_billing_address.value = data.billing_address_address;
    edit_billing_city.value = data.billing_address_city;
    edit_billing_state.value = data.billing_address_state;
    edit_billing_postcode.value = data.billing_address_portalCode;
    edit_billing_phone.value = data.billing_address_contactNumber.replace(
        "+" + userPhoneExt,
        ""
    );

    if (edit_delivery_address1) {
        edit_delivery_address1.value = data.delivery_address_address;
    }
    if (edit_delivery_city1) {
        edit_delivery_city1.value = data.delivery_address_city;
    }
    if (edit_delivery_state1) {
        edit_delivery_state1.value = data.delivery_address_state;
    }
    if (edit_delivery_postcode1) {
        edit_delivery_postcode1.value = data.delivery_address_portalCode;
    }
    if (edit_delivery_phone1) {
        edit_delivery_phone1.value = data.delivery_address_contactNumber.replace(
            "+" + userPhoneExt,
            ""
        );
    }
    if (edit_billing_address1) {
        edit_billing_address1.value = data.billing_address_address;
    }
    if (edit_billing_city1) {
        edit_billing_city1.value = data.billing_address_city;
    }
    if (edit_billing_state1) {
        edit_billing_state1.value = data.billing_address_state;
    }
    if (edit_billing_postcode1) {
        edit_billing_postcode1.value = data.billing_address_portalCode;
    }
    if (edit_billing_phone1) {
        edit_billing_phone1.value = data.billing_address_contactNumber.replace(
            "+" + userPhoneExt,
            ""
        );
    }
}

function cancelEditAddress() {
    if (document.getElementById("enable-address-edit")) {
        if (
            document.getElementById("enable-address-edit").style.display ===
            "block"
        ) {
            if (
                document.getElementById("shipping-address-show")
                    .offsetParent !== null
            ) {
                document.getElementById("shipping-address-show").style.display =
                    "none";
                document.getElementById("shipping-address-edit").style.display =
                    "block";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "none";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "block";
            } else {
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "none";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "none";
            }
        } else {
            if (
                document.getElementById("shipping-address-show")
                    .offsetParent !== null
            ) {
                document.getElementById("shipping-address-show").style.display =
                    "none";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "block";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "block";
            } else {
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "block";
                document.getElementById("enable-address-add").style.display =
                    "block";
                document.getElementById("cancel-address-edit").style.display =
                    "none";
            }
        }
    } else {
        if (
            document.getElementById("shipping-address-show").offsetParent !==
            null
        ) {
            document.getElementById("shipping-address-show").style.display =
                "none";
            document.getElementById("shipping-address-edit").style.display =
                "block";
            document.getElementById("shipping-address-add").style.display =
                "block";
            document.getElementById("enable-address-edit").style.display =
                "block";
            document.getElementById("enable-address-add").style.display =
                "block";
            document.getElementById("cancel-address-edit").style.display =
                "block";
        } else {
            document.getElementById("shipping-address-show").style.display =
                "block";
            document.getElementById("shipping-address-edit").style.display =
                "none";
            document.getElementById("shipping-address-add").style.display =
                "block";
            document.getElementById("enable-address-edit").style.display =
                "block";
            document.getElementById("enable-address-add").style.display =
                "block";
            document.getElementById("cancel-address-edit").style.display =
                "none";
        }
    }
}

function addAddress() {
    add_delivery_name.value = "";
    if (add_SSN) {
        add_SSN.value = "";
    }
    add_delivery_address.value = "";
    add_delivery_city.value = "";
    add_delivery_state.value = "";
    add_delivery_postcode.value = "";
    add_delivery_phone.value = "";
    add_billing_address.value = "";
    add_billing_city.value = "";
    add_billing_state.value = "";
    add_billing_postcode.value = "";
    add_billing_phone.value = "";

    if (document.getElementById("shipping-address-add").offsetParent !== null) {
        document.getElementById("shipping-address-add").style.display = "none";
        document.getElementById("enable-address-edit").style.display = "block";
        document.getElementById("enable-address-add").style.display = "block";
        document.getElementById("cancel-address-edit").style.display = "none";
        document.getElementById("shipping-address-show").style.display =
            "block";
    } else {
        document.getElementById("shipping-address-add").style.display = "block";
        document.getElementById("enable-address-edit").style.display = "none";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "block";
        document.getElementById("shipping-address-show").style.display = "none";
    }
}

function enableBilling() {
    if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_edit")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
        true
    ) {
        document.getElementById(
            "billing-address-block_for_edit"
        ).style.display = "block";
    } else if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_edit")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
        false
    ) {
        document.getElementById(
            "billing-address-block_for_edit"
        ).style.display = "none";
    }

    if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_add")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_add").checked ==
        true
    ) {
        document.getElementById("billing-address-block_for_add").style.display =
            "block";
    } else if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_add")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_add").checked ==
        false
    ) {
        document.getElementById("billing-address-block_for_add").style.display =
            "none";
    }
}
// End: Functions related to shipping address

function payment() {
    current_step = 3;
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse3").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.add("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function paymentSummary() {
    current_step = 4;
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse4").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.add("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function eventLocation() {
    let channel_type = $("#ba_channel_type").val();
    let event_location_code = $("#ba_event_location_code").val();
    UpdateEvent(channel_type, event_location_code);

    $("#button-next").addClass("display-none");
    $("#form-pay").removeClass("display-none");
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse5").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.add("show");
    }
    event_data = $("#event_location_form").serialize();
    // console.log(event_data);
}

//Card Functions
function editCard() {
    if (
        document.body.contains(
            document.getElementById("show_card_details_container")
        ) &&
        document.body.contains(
            document.getElementById("card_selection_list")
        ) &&
        document.body.contains(document.getElementById("add_additional_card"))
    ) {
        if (
            document.getElementById("show_card_details_container")
                .offsetParent !== null
        ) {
            document.getElementById(
                "show_card_details_container"
            ).style.display = "none";
            document.getElementById("card_selection_list")
                ? (document.getElementById(
                    "card_selection_list"
                ).style.display = "block")
                : null;
            document.getElementById("add_additional_card")
                ? (document.getElementById(
                    "add_additional_card"
                ).style.display = "none")
                : null;
        } else {
            document.getElementById(
                "show_card_details_container"
            ).style.display = "block";
            document.getElementById("card_selection_list")
                ? (document.getElementById(
                    "card_selection_list"
                ).style.display = "none")
                : null;
            document.getElementById("add_additional_card")
                ? (document.getElementById(
                    "add_additional_card"
                ).style.display = "none")
                : null;
        }
    } else if (
        document.body.contains(element_8_1) &&
        document.body.contains(element_9_1) &&
        document.body.contains(element_10_1)
    ) {
        if (element_8_1.offsetParent !== null) {
            element_8_1.style.display = "block";
            element_9_1 ? (element_9_1.style.display = "block") : null;
            element_10_1 ? (element_10_1.style.display = "block") : null;
        } else {
            element_8_1.style.display = "block";
            element_9_1 ? (element_9_1.style.display = "block") : null;
            element_10_1 ? (element_10_1.style.display = "block") : null;
        }
    }
}

function selectionAddCard() {
    if (
        document.getElementById("show_card_details_container").offsetParent !==
        null
    ) {
        document.getElementById("show_card_details_container").style.display =
            "none";
        document.getElementById("card_selection_list")
            ? (document.getElementById("card_selection_list").style.display =
                "none")
            : null;
        document.getElementById("add_additional_card")
            ? (document.getElementById("add_additional_card").style.display =
                "block")
            : null;
    } else {
        document.getElementById("show_card_details_container").style.display =
            "none";
        document.getElementById("card_selection_list")
            ? (document.getElementById("card_selection_list").style.display =
                "none")
            : null;
        document.getElementById("add_additional_card")
            ? (document.getElementById("add_additional_card").style.display =
                "block")
            : null;
    }
}

function CardBack() {
    if (document.getElementById("show_card_details_container")) {
        if (
            document.getElementById("show_card_details_container")
                .offsetParent == null
        ) {
            document.getElementById(
                "show_card_details_container"
            ).style.display = "block";
            document.getElementById("card_selection_list")
                ? (document.getElementById(
                    "card_selection_list"
                ).style.display = "none")
                : null;
            document.getElementById("add_additional_card")
                ? (document.getElementById(
                    "add_additional_card"
                ).style.display = "none")
                : null;
        }
    }
}

function AddressBack(type) {
    // console.log("AddressBack type: " + type);
    if (type === "default") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent ==
                null
            ) {
                // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }

            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }
        }
    }
    if (type === "update") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            } else {
                // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "block")
                    : null;
            }
        }
    }
    if (type === "address_update_only") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                //document.getElementById("shipping-address-edit") ? document.getElementById("shipping-address-edit").style.display = "none" : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            } else {
                // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                //document.getElementById("shipping-address-edit") ? document.getElementById("shipping-address-edit").style.display = "none" : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }
        }
    }
}

function paymentKR() {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: GLOBAL_URL + "/shave-plans/checkout/nicepay/confirm-purchase",
        data: { type: "trial-plan" },
        json: true,
        cache: false,
        success: function (data) {
            // console.log(data);
            if (data.status) {
                if (data.status == "success") {
                    window.location.href =
                        GLOBAL_URL + "/thankyou/" + data.orderid;
                }
            }
        },
        error: function (jqXHR) {
            $("#error-card").text(jqXHR.responseJSON.message);
        }
    });
}

// Function: Update session data
async function UpdateSessionCardDataKR(data, pi, type, callback) {
    let get_session = await CheckSessionTKData();
    let session_data = {};
    session_data["checkout"] = {};
    if (get_session) {
        session_data["checkout"]["user"] = get_session["checkout"]["user"];
        session_data["checkout"]["selected_address"] =
            get_session["checkout"]["selected_address"];
        if (type == "nicepay") {
            session_data["checkout"]["selected_card"] = {
                id: data["id"],
                customer_id: data["cus_id"]
            };
        } else if (type == "stripe") {
            session_data["checkout"]["selected_card"] = {
                id: data["card_from_db"]["id"],
                customer_id: data["card_from_db"]["customerId"],
                risk_level: data["card_from_db"]["risk_level"],
                payment_intent_id: pi,
                type: "trial-plan"
            };
        } else {
            session_data["checkout"]["selected_card"] = {};
        }
        session_data["checkout"]["promo"] = get_session["checkout"]["promo"];
        session_data["checkout"]["payment_data"] =
            get_session["checkout"]["payment_data"];
    } else {
        session_data["checkout"]["user"] = {};
        session_data["checkout"]["selected_address"] = {
            delivery_address: "",
            billing_address: ""
        };
        if (type == "nicepay") {
            session_data["checkout"]["selected_card"] = {
                id: data["id"],
                customer_id: data["cus_id"]
            };
        } else if (type == "stripe") {
            session_data["checkout"]["selected_card"] = {
                id: data["card_from_db"]["id"],
                customer_id: data["card_from_db"]["customerId"],
                risk_level: data["card_from_db"]["risk_level"],
                payment_intent_id: pi,
                type: "trial-plan"
            };
        } else {
            session_data["checkout"]["selected_card"] = {};
        }

        session_data["checkout"]["promo"] = {};
        session_data["checkout"]["payment_data"] = {};
    }
    session(
        SESSION_CHECKOUT_TRIAL_PLAN,
        SESSION_SET,
        JSON.stringify(session_data)
    ).done(function (test) {
        // console.log("Session successfully stored. " + JSON.stringify(test));
        callback();
    });
}

// Promotion
function onApplyPromo(
    data,
    total,
    ntotal,
    ablefreeexistproduct,
    freeexistresultproduct,
    freeexistresultproductprice,
    status
) {
    let checkout_data_promo = {
        journey_type: "checkout",
        update_type: "selected_promo",
        data: data,
        total: total,
        ntotal: ntotal,
        ablefreeexistproduct: ablefreeexistproduct,
        freeexistresultproduct: freeexistresultproduct,
        freeexistresultproductprice: freeexistresultproductprice,
        status: status
    };
    UpdateSessionData(checkout_data_promo);
}

// Promotion
function applyPromotion() {
    var loading = document.getElementById("loading");
    if (loading) {
        loading.style.display = "block";
    }
    Promotion("web", "trial-plan", "combinesession");
}

function summaryChoosePlan(type, alldata, annual_data) {
    // console.log(type, alldata, annual_data);
    // console.log(trial_switch_plans);
    if (type === "normal") {
        let _session_data = JSON.parse(alldata.session_data);
        let _session_checkout_data = session_checkout_data;

        $("#radio-select-normal-plan").addClass("radio-selected");

        $("#radio-select-annual-plan").removeClass("radio-selected");

        // console.log(_session_checkout_data);
        let selection_data_switch_plans = {
            journey_type: "selection",
            update_type: "update_annual_suggestion",
            switch_plans: {
                isSwitched: true,
                _normal: _session_data,
                _annual: null,
                isSelected: "normal"
            },
            summary: {
                current_price: _session_data.selection.summary.current_price,
                next_price: _session_data.selection.summary.next_price,
                planId: _session_data.selection.summary.planId,
                shipping_fee: _session_data.selection.summary.shipping_fee
            }
        };
        UpdateSessionData(selection_data_switch_plans).done(function () {
            let checkout_data_switch_plans = {
                journey_type: "checkout",
                update_type: "update_annual_suggestion",
                switch_plans: {
                    isSwitched: true,
                    _normal: _session_data,
                    _annual: null,
                    isSelected: "normal"
                },
                payment_data:
                    alldata.payment_intent.checkout_details
            };
            UpdateSessionData(checkout_data_switch_plans);
        });
    } else if (type === "annual") {
        let _session_data = JSON.parse(alldata.session_data);
        let _session_checkout_data = session_checkout_data;
        // console.log(_session_checkout_data);
        // console.log("annual data", annual_data);

        $("#radio-select-annual-plan").addClass("radio-selected");

        $("#radio-select-normal-plan").removeClass("radio-selected");

        let selection_data_switch_plans = {
            journey_type: "selection",
            update_type: "update_annual_suggestion",
            switch_plans: {
                isSwitched: true,
                _normal: _session_data,
                _annual: annual_data,
                isSelected: "annual"
            },
            summary: {
                current_price: annual_data.trial_price,
                next_price: annual_data.sales_price,
                planId: annual_data.planid,
                shipping_fee: _session_data.selection.summary.shipping_fee
            }
        };
        UpdateSessionData(selection_data_switch_plans).done(function () {
            let checkout_data_switch_plans = {
                journey_type: "checkout",
                update_type: "update_annual_suggestion",
                switch_plans: {
                    isSwitched: true,
                    _normal: alldata,
                    _annual: annual_data,
                    isSelected: "annual"
                },
                payment_data: annual_data
            };
            UpdateSessionData(checkout_data_switch_plans);
        });

        // window.location.reload();
    }
}

// ========================================================================================================
// CHECKOUT PAGE ON LOAD ==================================================================================
// ========================================================================================================
$(function () {
    ClearSessionData_CHECKOUT().then(function (response) {
        if (response) {
            // Check Current Steps and do following function
            steps();

            if (current_step == 1) {
                var link = document.getElementById("account-header");
                if (link) {
                    link.click();
                    $("#button-next").text("NEXT");
                }
            }

            if (document.getElementById("shipping-address-add")) {
                if (
                    document.getElementById("shipping-address-add").offset !==
                    null
                ) {
                    document.getElementById(
                        "enable-address-edit"
                    ).style.display = "none";
                    document.getElementById(
                        "enable-address-add"
                    ).style.display = "none";
                    document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none";
                }
            }

            // Enable billing if check box is checked
            if (
                document.body.contains(
                    document.getElementById(
                        "enableBillingEditCheckBox_for_edit"
                    )
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_edit")
                    .checked == true
            ) {
                document.getElementById("cancel-address-edit").style.display =
                    "block";
            }
            if (
                document.body.contains(
                    document.getElementById(
                        "enableBillingEditCheckBox_for_edit"
                    )
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_edit")
                    .checked == false
            ) {
                document.getElementById("cancel-address-edit").style.display =
                    "none";
            }
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_add")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_add")
                    .checked == true
            ) {
                document.getElementById(
                    "billing-address-block_for_add"
                ).style.display = "block";
            }
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_add")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_add")
                    .checked == false
            ) {
                document.getElementById(
                    "billing-address-block_for_add"
                ).style.display = "none";
            }

            // console.log("INITIAL PAGE LOAD => ", checkout_details);
            // console.log("INITIAL PAGE LOAD => ", payment_intent);
            // console.log("INITIAL PAGE LOAD => ", user_id);
            // console.log("INITIAL PAGE LOAD => ", country_id);
            // console.log("INITIAL PAGE LOAD => ", user);
            // console.log("INITIAL PAGE LOAD => ", delivery_address);
            // console.log("INITIAL PAGE LOAD => ", billing_address);
            // console.log("INITIAL PAGE LOAD => ", default_card);
            // console.log("INITIAL PAGE LOAD => ", session_data);

            // ADD All Default Data to Checkout Session
            if (
                !(
                    typeof checkout_details === "undefined" &&
                    typeof payment_intent === "undefined" &&
                    typeof user_id === "undefined" &&
                    typeof country_id === "undefined" &&
                    typeof user === "undefined" &&
                    typeof default_delivery_address === "undefined" &&
                    typeof default_billing_address === "undefined" &&
                    typeof default_card === "undefined" &&
                    typeof session_data === "undefined" &&
                    typeof saved_session_checkout === "undefined"
                )
            ) {
                // console.log("update checkout session on load 2");
                let xe = checkout_details ? checkout_details : "";
                let xf = payment_intent ? payment_intent : "";
                let xg = user_id ? user_id : "";
                let xh = country_id ? country_id : "";
                let xi = user ? user : "";
                let xj = default_delivery_address
                    ? default_delivery_address
                    : "";
                let xk = default_billing_address ? default_billing_address : "";
                let xl = default_card ? default_card : "";
                let xm = session_data ? session_data : "";
                let xn = saved_session_checkout ? saved_session_checkout : "";

                // console.log("1", xe);
                // console.log("2", xf);
                // console.log("3", xg);
                // console.log("4", xh);
                // console.log("5", xi);
                // console.log("6", xj);
                // console.log("7", xk);
                // console.log("8", xl);
                // console.log("9", xm);
                if (xe && xf && xg && xh && xi) {
                    // console.log("FOUND 2");
                    let data = {
                        checkout_details: xe,
                        payment_intents: xf,
                        user_id: xg,
                        country_id: xh,
                        user: xi,
                        delivery_address: xj,
                        billing_address: xk,
                        default_card: xl,
                        session_data: xm,
                        session_checkout: xn,
                        journey_type: "checkout",
                        update_type: "default"
                    };
                    // console.log("update checkout session on load 1");
                    // console.log("default items", data);
                    // ClearUpdateSessionData_CHECKOUT(data);
                    UpdateSessionData(data).done(function () {

                        if (saved_session_checkout) {
                            if (saved_session_checkout.event) {
                                $("#ba_channel_type option[value=" + saved_session_checkout.event.ba_channel_type + "]").prop("selected", true);
                                $("#ba_event_location_code").val(saved_session_checkout.event.ba_event_location_code);

                                UpdateEvent(saved_session_checkout.event.ba_channel_type, saved_session_checkout.event.ba_event_location_code);
                            }
                            if (saved_session_checkout.summary) {
                                if (saved_session_checkout.summary.isDirectTrial) {
                                    $("#ba_is_direct_trial").prop("checked", saved_session_checkout.summary.isDirectTrial);
                                }

                                UpdateIsDirectTrial(saved_session_checkout.summary.isDirectTrial);
                            }
                        }

                        // // Save default delivery & billing address to session checkout
                        if (
                            document.getElementById("delivery_address_id") &&
                            document.getElementById("billing_address_id")
                        ) {
                            if (
                                document.getElementById("delivery_address_id")
                                    .value &&
                                document.getElementById("billing_address_id")
                                    .value
                            ) {
                                let addressUpdates = {
                                    selected_delivery_address_id: document.getElementById(
                                        "delivery_address_id"
                                    ).value
                                        ? document.getElementById(
                                            "delivery_address_id"
                                        ).value
                                        : "",
                                    selected_billing_address_id: document.getElementById(
                                        "billing_address_id"
                                    ).value
                                        ? document.getElementById(
                                            "billing_address_id"
                                        ).value
                                        : ""
                                };

                                if (
                                    delivery_address[0].address !==
                                    billing_address[0].address
                                ) {
                                    $(
                                        "#enableBillingEditCheckBox_for_edit"
                                    ).prop("checked", true);
                                    document.getElementById(
                                        "cancel-address-edit"
                                    ).style.display = "block";
                                } else {
                                    $(
                                        "#enableBillingEditCheckBox_for_edit"
                                    ).prop("checked", false);
                                    document.getElementById(
                                        "cancel-address-edit"
                                    ).style.display = "none";
                                }

                                onUpdateDeliverySelection(
                                    addressUpdates,
                                    "address_update_only"
                                );
                            }
                        }

                        // PaymentIntent | Get [ID] & [UPDATE_TYPE]
                        if (
                            document.getElementById("payment_intent_id") &&
                            document.getElementById(
                                "payment_intent_next_update_type"
                            )
                        ) {
                            let paymentIntent_updates = {
                                payment_intent_id: document.getElementById(
                                    "payment_intent_id"
                                ).value
                                    ? document.getElementById(
                                        "payment_intent_id"
                                    ).value
                                    : "",
                                next_update_type: document.getElementById(
                                    "payment_intent_next_update_type"
                                ).value
                                    ? document.getElementById(
                                        "payment_intent_next_update_type"
                                    ).value
                                    : ""
                            };
                            onUpdatePaymentIntent(
                                paymentIntent_updates,
                                document.getElementById(
                                    "payment_intent_next_update_type"
                                ).value
                            );
                        }
                    });
                }
            }
        }
    });

    v_checkout_e_address("edit-addresses-block");
    // Action: On submit edit address
    $("#edit-addresses-block").on("submit", function (event) {
        event.preventDefault();
        // v_checkout_e_address("edit-addresses-block");
        // if ($("#edit-addresses-block").valid() === true) {
        $("#loading").css("display", "block");
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + "/ba/user/address/edit/" + user_id,
            data: $(this).serialize(),
            cache: false,
            success: function (data) {
                // console.log(data);
                document.getElementById("shipping-address-show").innerHTML = "";
                // All countries except korea
                if (country_id != "kr") {
                    if (data["delivery_address"]) {
                        // All countries except taiwan
                        if (country_id != "tw") {
                            document.getElementById(
                                "shipping-address-show"
                            ).innerHTML =
                                '<div class="panel-body">' +
                                '<div class="box-white" id="appendAddress">' +
                                "<br>" +
                                '<label style="font-weight: bold">Delivery Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.fullName +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.portalCode +
                                ", " +
                                data.delivery_address.city +
                                ", " +
                                data.delivery_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.contactNumber +
                                "</p><br>" +
                                "<br>" +
                                '<label style="font-weight: bold">Billing Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.portalCode +
                                ", " +
                                data.billing_address.city +
                                ", " +
                                data.billing_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.contactNumber +
                                "</p><br>" +
                                "</div>" +
                                "<div><button id='enable-address-edit' class='btn btn-load-more col-12' onclick='editAddress()'>EDIT ADDRESS</button></div>" +
                                "</div>";
                        } else {
                            document.getElementById(
                                "shipping-address-show"
                            ).innerHTML =
                                '<div class="panel-body">' +
                                '<div class="box-white" id="appendAddress">' +
                                "<br>" +
                                '<label style="font-weight: bold">Delivery Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.fullName +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.SSN +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.portalCode +
                                ", " +
                                data.delivery_address.city +
                                ", " +
                                data.delivery_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.contactNumber +
                                "</p><br>" +
                                "<br>" +
                                '<label style="font-weight: bold">Billing Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.portalCode +
                                ", " +
                                data.billing_address.city +
                                ", " +
                                data.billing_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.contactNumber +
                                "</p><br>" +
                                "</div>" +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                "</div>";
                        }
                    }

                    // append address values into edit-form
                    let updateEditForm = {
                        delivery_address_fullname:
                            data.delivery_address.fullName,
                        delivery_address_SSN: data.delivery_address.SSN,
                        delivery_address_address: data.delivery_address.address,
                        delivery_address_portalCode:
                            data.delivery_address.portalCode,
                        delivery_address_city: data.delivery_address.city,
                        delivery_address_state: data.delivery_address.state,
                        delivery_address_contactNumber:
                            data.delivery_address.contactNumber,
                        billing_address_address: data.billing_address.address,
                        billing_address_portalCode:
                            data.billing_address.portalCode,
                        billing_address_city: data.billing_address.city,
                        billing_address_state: data.billing_address.state,
                        billing_address_contactNumber:
                            data.billing_address.contactNumber
                    };

                    // onAddDeliveryAddress(updateEditForm);
                    let addressUpdates = {
                        selected_delivery_address_id: data.delivery_address
                            ? data.delivery_address.id
                            : "",
                        selected_billing_address_id: data.billing_address
                            ? data.billing_address.id
                            : ""
                    };
                    onUpdateDeliverySelection(addressUpdates, "update");

                    if (
                        updateEditForm.delivery_address_address !==
                        updateEditForm.billing_address_address
                    ) {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            true
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "block";
                    } else {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            false
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                    $("#loading").css("display", "none");
                } else if (country_id == "kr") {
                    if (data["delivery_address"]) {
                        document.getElementById(
                            "shipping-address-show"
                        ).innerHTML =
                            '<div class="panel-body">' +
                            '<div class="box-white" id="appendAddress">' +
                            "<br>" +
                            '<label style="font-weight: bold">Delivery Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.fullName +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.portalCode +
                            ", " +
                            data.delivery_address.city +
                            ", " +
                            data.delivery_address.state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.contactNumber +
                            "</p><br>" +
                            "<br>" +
                            '<label style="font-weight: bold">Billing Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.portalCode +
                            ", " +
                            data.billing_address.city +
                            ", " +
                            data.billing_address.state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.contactNumber +
                            "</p><br>" +
                            "</div>" +
                            '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                            "</div>";
                    }

                    // append address values into edit-form
                    let updateEditForm = {
                        delivery_address_fullname:
                            data.delivery_address.fullName,
                        delivery_address_SSN: data.delivery_address.SSN,
                        delivery_address_address: data.delivery_address.address,
                        delivery_address_portalCode:
                            data.delivery_address.portalCode,
                        delivery_address_city: data.delivery_address.city,
                        delivery_address_state: data.delivery_address.state,
                        delivery_address_contactNumber:
                            data.delivery_address.contactNumber,
                        billing_address_address: data.billing_address.address,
                        billing_address_portalCode:
                            data.billing_address.portalCode,
                        billing_address_city: data.billing_address.city,
                        billing_address_state: data.billing_address.state,
                        billing_address_contactNumber:
                            data.billing_address.contactNumber
                    };

                    // onAddDeliveryAddress(updateEditForm);
                    let addressUpdates = {
                        selected_delivery_address_id: data.delivery_address
                            ? data.delivery_address.id
                            : "",
                        selected_billing_address_id: data.billing_address
                            ? data.billing_address.id
                            : ""
                    };
                    onUpdateDeliverySelection(addressUpdates, "update");

                    if (
                        updateEditForm.delivery_address_address !==
                        updateEditForm.billing_address_address
                    ) {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            true
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "block";
                    } else {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            false
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                    $("#loading").css("display", "none");
                }
            },
            error: function () {
                $("#loading").css("display", "none");
            }
        });
        // }
    });

    v_checkout_address("add-addresses-block");
    // Action: On submit add address
    $("#add-addresses-block").on("submit", function (event) {
        event.preventDefault();
        // v_checkout_address("add-addresses-block");
        // if ($("#add-addresses-block").valid() === true) {
        $("#loading").css("display", "block");
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + "/ba/user/address/add",
            data: $(this).serialize(),
            cache: false,
            success: function (data) {
                // console.log("address data: => " + JSON.stringify(data));
                document.getElementById("shipping-address-show").innerHTML = "";

                // All countries except korea
                if (country_id != "kr") {
                    if (data["delivery_address"] && data["billing_address"]) {
                        // All countries except taiwan
                        if (data["delivery_address"]) {
                            if (country_id != "tw") {
                                document.getElementById(
                                    "shipping-address-show"
                                ).innerHTML =
                                    '<div class="panel-body">' +
                                    '<div class="box-white" id="appendAddress">' +
                                    "<br>" +
                                    '<label style="font-weight: bold">Delivery Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].fullName +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].portalCode +
                                    ", " +
                                    data.delivery_address[0].city +
                                    ", " +
                                    data.delivery_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].contactNumber +
                                    "</p><br>" +
                                    "<br>" +
                                    '<label style="font-weight: bold">Billing Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].portalCode +
                                    ", " +
                                    data.billing_address[0].city +
                                    ", " +
                                    data.billing_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].contactNumber +
                                    "</p><br>" +
                                    "</div>" +
                                    "<div><button id='enable-address-edit' class='btn btn-load-more col-10' onclick='editAddress()'>EDIT ADDRESS</button></div>" +
                                    "</div>";
                            } else {
                                document.getElementById(
                                    "shipping-address-show"
                                ).innerHTML =
                                    '<div class="panel-body">' +
                                    '<div class="box-white" id="appendAddress">' +
                                    "<br>" +
                                    '<label style="font-weight: bold">Delivery Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].fullName +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].SSN +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].portalCode +
                                    ", " +
                                    data.delivery_address[0].city +
                                    ", " +
                                    data.delivery_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].contactNumber +
                                    "</p><br>" +
                                    "<br>" +
                                    '<label style="font-weight: bold">Billing Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].portalCode +
                                    ", " +
                                    data.billing_address[0].city +
                                    ", " +
                                    data.billing_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].contactNumber +
                                    "</p><br>" +
                                    "</div>" +
                                    '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                    "</div>";
                            }
                        }

                        $("#edit_delivery_name").val(
                            data.delivery_address[0].fullName
                        );
                        $("#edit_SSN").val(data.delivery_address[0].SSN);
                        $("#edit_delivery_phone").val(
                            data.delivery_address[0].contactNumber.replace(
                                "+" + userPhoneExt,
                                ""
                            )
                        );
                        $("#edit_delivery_address").val(
                            data.delivery_address[0].address
                        );
                        // $("#edit_delivery_state").val(data.delivery_address[0].state);
                        $("#edit_delivery_city").val(
                            data.delivery_address[0].city
                        );
                        $("#edit_delivery_postcode").val(
                            data.delivery_address[0].portalCode
                        );
                        $("#edit_billing_phone").val(
                            data.billing_address[0].contactNumber.replace(
                                "+" + userPhoneExt,
                                ""
                            )
                        );
                        $("#edit_billing_address").val(
                            data.billing_address[0].address
                        );
                        // $("#edit_billing_state").val(data.billing_address[0].state);
                        $("#edit_billing_city").val(
                            data.billing_address[0].city
                        );
                        $("#edit_billing_postcode").val(
                            data.billing_address[0].portalCode
                        );

                        // append address values into edit-form
                        let updateEditForm = {
                            delivery_address_fullname:
                                data.delivery_address[0].fullName,
                            delivery_address_SSN: data.delivery_address[0].SSN,
                            delivery_address_address:
                                data.delivery_address[0].address,
                            delivery_address_portalCode:
                                data.delivery_address[0].portalCode,
                            delivery_address_city:
                                data.delivery_address[0].city,
                            delivery_address_state:
                                data.delivery_address[0].state,
                            delivery_address_contactNumber:
                                data.delivery_address[0].contactNumber,
                            billing_address_address:
                                data.billing_address[0].address,
                            billing_address_portalCode:
                                data.billing_address[0].portalCode,
                            billing_address_city: data.billing_address[0].city,
                            billing_address_state:
                                data.billing_address[0].state,
                            billing_address_contactNumber:
                                data.billing_address[0].contactNumber
                        };
                        // onAddDeliveryAddress(updateEditForm);
                        let addressUpdates = {
                            selected_delivery_address_id: data
                                .delivery_address[0].id
                                ? data.delivery_address[0].id
                                : "",
                            selected_billing_address_id: data.billing_address[0]
                                .id
                                ? data.billing_address[0].id
                                : ""
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        if (
                            updateEditForm.delivery_address_address !==
                            updateEditForm.billing_address_address
                        ) {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                true
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "block";
                        } else {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                false
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "none";
                        }
                        $("#loading").css("display", "none");
                    }

                    if (
                        document.getElementById("shipping-address-show") !==
                        "undefined" &&
                        document.getElementById("shipping-address-show") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-show"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-edit") !==
                        "undefined" &&
                        document.getElementById("shipping-address-edit") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-edit"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-edit") !==
                        "undefined" &&
                        document.getElementById("enable-address-edit") !== null
                    ) {
                        document.getElementById(
                            "enable-address-edit"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-add") !==
                        "undefined" &&
                        document.getElementById("shipping-address-add") !== null
                    ) {
                        document.getElementById(
                            "shipping-address-add"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-add") !==
                        "undefined" &&
                        document.getElementById("enable-address-add") !== null
                    ) {
                        document.getElementById(
                            "enable-address-add"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("cancel-address-edit") !==
                        "undefined" &&
                        document.getElementById("cancel-address-edit") !== null
                    ) {
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                } else if (country_id == "kr") {
                    if (data["delivery_address"] && data["billing_address"]) {
                        if (data["delivery_address"]) {
                            document.getElementById(
                                "shipping-address-show"
                            ).innerHTML =
                                '<div class="panel-body">' +
                                '<div class="box-white" id="appendAddress">' +
                                "<br>" +
                                '<label style="font-weight: bold">Delivery Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address[0].fullName +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address[0].address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address[0].portalCode +
                                ", " +
                                data.delivery_address[0].city +
                                ", " +
                                data.delivery_address[0].state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address[0].contactNumber +
                                "</p><br>" +
                                "<br>" +
                                '<label style="font-weight: bold">Billing Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address[0].address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address[0].portalCode +
                                ", " +
                                data.billing_address[0].city +
                                ", " +
                                data.billing_address[0].state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address[0].contactNumber +
                                "</p><br>" +
                                "</div>" +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                "</div>";
                        }

                        $("#edit_delivery_name").val(
                            data.delivery_address[0].fullName
                        );
                        $("#edit_SSN").val(data.delivery_address[0].SSN);
                        $("#edit_delivery_phone").val(
                            data.delivery_address[0].contactNumber
                        );

                        var getdda = data.delivery_address[0].address;
                        $("#edit_delivery_address").val(
                            data.delivery_address[0].address
                        );

                        if (getdda.includes(",")) {
                            var splitdda = getdda.split(",");
                            $("#edit_delivery_address").val(splitdda[0]);
                            $("#edit_delivery_flat").val(splitdda[1]);
                        } else {
                            $("#edit_delivery_address").val(getdda);
                        }
                        var addcheckbox = document.getElementById(
                            "enableBillingEditCheckBox_for_add"
                        );
                        if (addcheckbox.checked == true) {
                            addcheckbox.click();
                        }
                        $("#edit_delivery_city").val(
                            data.delivery_address[0].city
                        );
                        $("#edit_delivery_state").val(
                            data.delivery_address[0].state
                        );
                        $("#edit_delivery_postcode").val(
                            data.delivery_address[0].portalCode
                        );
                        $("#edit_delivery_city").val(
                            data.delivery_address[0].city
                        );

                        var getbba = data.billing_address[0].address;
                        $("#edit_billing_address").val(
                            data.billing_address[0].address
                        );

                        if (getbba.includes(",")) {
                            var splitbba = getbba.split(",");
                            $("#edit_billing_address1").val(splitbba[0]);
                            $("#edit_billing_flat").val(splitbba[1]);
                        } else {
                            $("#edit_billing_address1").val(getbba);
                        }
                        $("#edit_billing_city").val(
                            data.billing_address[0].city
                        );
                        $("#edit_billing_state").val(
                            data.billing_address[0].state
                        );
                        $("#edit_billing_postcode").val(
                            data.billing_address[0].portalCode
                        );
                        $("#edit_billing_phone").val(
                            data.billing_address[0].contactNumber.replace(
                                "+" + userPhoneExt,
                                ""
                            )
                        );

                        let updateEditForm = {
                            delivery_address_fullname:
                                data.delivery_address[0].fullName,
                            delivery_address_address:
                                data.delivery_address[0].address,
                            delivery_address_portalCode:
                                data.delivery_address[0].portalCode,
                            delivery_address_city:
                                data.delivery_address[0].city,
                            delivery_address_state:
                                data.delivery_address[0].state,
                            delivery_address_contactNumber:
                                data.delivery_address[0].contactNumber,
                            billing_address_address:
                                data.billing_address[0].address,
                            billing_address_portalCode:
                                data.billing_address[0].portalCode,
                            billing_address_city: data.billing_address[0].city,
                            billing_address_state:
                                data.billing_address[0].state,
                            billing_address_contactNumber:
                                data.billing_address[0].contactNumber
                        };
                        // onAddDeliveryAddress(updateEditForm);
                        let addressUpdates = {
                            selected_delivery_address_id: data
                                .delivery_address[0].id
                                ? data.delivery_address[0].id
                                : "",
                            selected_billing_address_id: data.billing_address[0]
                                .id
                                ? data.billing_address[0].id
                                : ""
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        if (
                            updateEditForm.delivery_address_address !==
                            updateEditForm.billing_address_address
                        ) {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                true
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "block";
                        } else {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                false
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "none";
                        }
                        $("#loading").css("display", "none");
                    }

                    if (
                        document.getElementById("shipping-address-show") !==
                        "undefined" &&
                        document.getElementById("shipping-address-show") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-show"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-edit") !==
                        "undefined" &&
                        document.getElementById("shipping-address-edit") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-edit"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-edit") !==
                        "undefined" &&
                        document.getElementById("enable-address-edit") !== null
                    ) {
                        document.getElementById(
                            "enable-address-edit"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-add") !==
                        "undefined" &&
                        document.getElementById("shipping-address-add") !== null
                    ) {
                        document.getElementById(
                            "shipping-address-add"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-add") !==
                        "undefined" &&
                        document.getElementById("enable-address-add") !== null
                    ) {
                        document.getElementById(
                            "enable-address-add"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("cancel-address-edit") !==
                        "undefined" &&
                        document.getElementById("cancel-address-edit") !== null
                    ) {
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }

                    // append address values into edit-form
                }
            },
            error: function () {
                $("#loading").css("display", "none");
            }
        });
        // }
    });

    v_checkout_cards("form_card");
    // On add card
    $("#form_card").on("submit", function (event) {
        event.preventDefault();
        // v_checkout_cards("form_card");
        // if ($("#form_card").valid() === true) {
        $("#loading").css("display", "block");
        let card_name = user.firstName + (user.lastName ? user.lastName : "");
        let card_number = $("#card-number")
            .val()
            .replace(/\s/g, "");
        let card_expiry_array = $("#card-expiry")
            .val()
            .split(" / ");
        let card_expiry_month = card_expiry_array[0];
        let card_expiry_year = card_expiry_array[1];
        let card_cvv = $("#card-cvv").val();

        if (
            card_number &&
            card_number.length == 16 &&
            card_expiry_month &&
            card_expiry_year &&
            card_cvv
        ) {
            let stripeData = {
                card_name,
                card_number,
                card_expiry_month,
                card_expiry_year,
                card_cvv,
                user_id,
                countryid
            };

            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "POST",
                url: API_URL + "/ba/stripe/card/add",
                data: stripeData,
                dataType: "json",
                json: true,
                cache: false,
                success: function (data) {
                    // console.log("add stripe card success");
                    if (checkout_details) {
                        total_price = checkout_details.trialPrice;

                        let _checkout_details = {
                            total_price: total_price
                        };
                        if (data.card && data.customer) {
                            // update session for checkout - card_details
                            onUpdateCard(data.card_from_db);
                            updateCardSelectionList(data.card_from_db.UserId);
                            // console.log(
                            //     "before ajax update payment Intent: " +
                            //     JSON.stringify(data)
                            // );
                            // update paymentIntent if customerId has not been linked yet
                            $.ajax({
                                headers: {
                                    "X-CSRF-TOKEN": $(
                                        'meta[name="csrf-token"]'
                                    ).attr("content")
                                },
                                type: "POST",
                                url:
                                    API_URL +
                                    "/ba/stripe/payment-intent/update",
                                data: {
                                    payment_intent_type: "trial-plan",
                                    payment_intent_details: payment_intent,
                                    update_type: payment_intent_next_update_type,
                                    checkout_details: _checkout_details,
                                    data: data,
                                    additional_card: 0
                                },
                                dataType: "json",
                                json: true,
                                cache: false,
                                success: function (response) {
                                    // console.log(
                                    //     "update paymentIntent " +
                                    //     JSON.stringify(response)
                                    // );
                                    payment_intent = response;
                                    checkout_data_card = {
                                        journey_type: "checkout",
                                        update_type: "selected_card",
                                        card_id: data.card_from_db.id,
                                        card: data.card_from_db,
                                        payment_intent_id: response.id
                                    };

                                    //Update card selection lists
                                    UpdateCardSelectionList(
                                        checkout_data_card.card
                                    );

                                    UpdateSessionData(checkout_data_card);

                                    $("#edit-card-container").removeClass(
                                        "hidden"
                                    );
                                    $("#add-card-container").addClass("hidden");

                                    $("#error-payment-method").addClass(
                                        "hidden"
                                    );
                                    $("#error-payment-method").html("");
                                    $("#error-payment-method2").addClass(
                                        "hidden"
                                    );
                                    $("#error-payment-method2").html("");
                                    $("#loading").css("display", "none");

                                    ChangeCardBackground(
                                        data.card_from_db.branchName
                                    );

                                    paymentSummary();
                                    UpdateStep(current_step);
                                },
                                error: function (response) {
                                    current_step--;
                                    UpdateStep(current_step);
                                    // console.log(response);
                                    $("#loading").css("display", "none");
                                    $("#error-payment-method").removeClass(
                                        "hidden"
                                    );
                                    $("#error-payment-method2").removeClass(
                                        "hidden"
                                    );
                                    if (response.responseJSON.message == 'Server Error') {
                                        $("#error-payment-method").text(trans('website_contents.global.invalid_card'));
                                        $("#error-payment-method2").text(trans('website_contents.global.invalid_card'));
                                    } else {
                                        $("#error-payment-method").text(response.responseJSON.message);
                                        $("#error-payment-method2").text(response.responseJSON.message);
                                    }
                                }
                            });
                        } else {
                            $("#loading").css("display", "none");
                        }
                    } else {
                        console.log("22", data);
                        $("#loading").css("display", "none");
                    }
                },
                error: function (response) {
                    console.log("44", response,response.responseJSON.message);
                    current_step--;
                    UpdateStep(current_step);
                    // console.log(response);
                    $("#loading").css("display", "none");
                    $("#error-payment-method").removeClass("hidden");
                    $("#error-payment-method2").removeClass("hidden");
                    if (response.responseJSON.message == 'Server Error') {
                        $("#error-payment-method").html(trans('website_contents.global.invalid_card'));
                        $("#error-payment-method2").html(trans('website_contents.global.invalid_card'));
                    } else {
                        $("#error-payment-method").html(response.responseJSON.message);
                        $("#error-payment-method2").html(response.responseJSON.message);
                    }
                }
            });
        } else {
            // console.log("33", data);
            paymentSummary();
            UpdateStep(current_step);
            $("#loading").css("display", "none");
        }
        // }
    });

    $("#tos_subscription_agree").click(function (event) {
        if ($("#tos_subscription_agree").is(':checked')) {
            if ($("#tos_subscription_agree_form").hasClass('subs_agreement_tos_text_error')) { $("#tos_subscription_agree_form").removeClass('subs_agreement_tos_text_error'); }
            if ($("#tos_subscription_agree").hasClass('subs_agreement_tos_checkbox_error')) { $("#tos_subscription_agree").removeClass('subs_agreement_tos_checkbox_error'); }
        } else {
            if ($("#tos_subscription_agree_form").hasClass('subs_agreement_tos_text_error')) { $("#tos_subscription_agree_form").removeClass('subs_agreement_tos_text_error'); }
            if ($("#tos_subscription_agree").hasClass('subs_agreement_tos_checkbox_error')) { $("#tos_subscription_agree").removeClass('subs_agreement_tos_checkbox_error'); }
        }
    });
    // Proceed to Payment
    $("#form-pay").click(function (event) {
        event.preventDefault();
        if (!$("#tos_subscription_agree").is(':checked')) {
            if (!$("#tos_subscription_agree_form").hasClass('subs_agreement_tos_text_error')) { $("#tos_subscription_agree_form").addClass('subs_agreement_tos_text_error'); }
            if (!$("#tos_subscription_agree").hasClass('subs_agreement_tos_checkbox_error')) { $("#tos_subscription_agree").addClass('subs_agreement_tos_checkbox_error'); }
        } else {
            var loading = document.getElementById("loading");
            if (loading) {
                loading.style.display = "block";
            }
            let data = "";
            // console.log(payment_intent);
            // console.log(session_checkout_data);
            onProceedPayment(
                session_data,
                session_checkout_data,
                payment_intent,
                data
            );
        }

    });

    //kr card
    $("#form-pay-kr").click(function (event) {
        if (!$("#tos_subscription_agree").is(':checked')) {
            if (!$("#tos_subscription_agree_form").hasClass('subs_agreement_tos_text_error')) { $("#tos_subscription_agree_form").addClass('subs_agreement_tos_text_error'); }
            if (!$("#tos_subscription_agree").hasClass('subs_agreement_tos_checkbox_error')) { $("#tos_subscription_agree").addClass('subs_agreement_tos_checkbox_error'); }
        } else {
            event.preventDefault();
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "POST",
                url: GLOBAL_URL + "/nicepay/card/add",
                data: $("#add-card-details-kr").serialize(),
                json: true,
                cache: false,
                success: function (data) {
                    // console.log("rteast" + JSON.stringify(data));
                    if (data["status"] == "success") {
                        // console.log("rteast1");
                        $("#error-card").text("");
                        UpdateSessionCardDataKR(data, "", "nicepay", paymentKR);
                    } else {
                        // console.log("rteast2");
                        $("#error-card").text(data["data"]["message"]);
                    }
                },
                error: function (jqXHR) {
                    $("#error-card").text(jqXHR.responseJSON.message);
                }
            });
        }
    });


    $(".d_phone").keyup(function (value) {

        var d_phoneext = $('.d_phoneext').val();
        var d_phone = this.value;
        if (d_phone.length >= 8) {
            var combinephone = d_phoneext + d_phone;
            // console.log(combinephone);
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "POST",
                url: API_URL + '/ba/user/check/phone',
                data: { cphone: combinephone, appType: 'baWebsite' },
                json: true,
                cache: false,
                success: function (data) {
                    let status = data.payload.used;
                    if (status == 'Yes') {
                        $("#notification_phone_exist").modal("show");
                    }
                },
                error: function (jqXHR) {

                }
            });
        }

    });

    $(".d_phoneext").keyup(function (value) {
        var d_phoneext = this.value;
        var d_phone = $('.d_phone').val();
        if (d_phone.length >= 8) {
            var combinephone = d_phoneext + d_phone;
            $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                type: "POST",
                url: API_URL + '/ba/user/check/phone',
                data: { cphone: combinephone, appType: 'baWebsite' },
                json: true,
                cache: false,
                success: function (data) {
                    let status = data.payload.used;
                    if (status == 'Yes') {
                        $("#notification_phone_exist").modal("show");
                    }
                },
                error: function (jqXHR) {

                }
            });
        }

    });

    $("#phoneexistcancel").click(function () {
        window.location = GLOBAL_URL;
    });

    // $("#tester").click(function(){
    //     session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).done(function (data) {
    //         // Do something with the session data
    //         if (data) {
    //             console.log(data);
    //         } else {
    //         }
    //     });
    // });
});
