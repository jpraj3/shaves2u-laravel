let current_step = 1;

//Set selected variables
let selected_handle;
let selected_blade;
let selected_frequency;
let selected_addon_list = [];
let selected_addon1;
let selected_addon2;
let selected_plan_id;
let current_shipping_image = "";
let next_billing_image = "";
let hasShaveCreamNextBilling = false;
let current_p;
let next_p;
let shipping;
let gender = JSON.parse(localStorage.getItem("gender_selected"));
// console.log(gender);
let page_url = window.location.pathname;
let page_trial_step_1 = page_url.includes("trial/steps/select-blade");
let page_trial_step_2 = page_url.includes("/trial/steps/select-refill");
let page_trial_step_3 = page_url.includes("/trial/steps/select-frequency");
// On page load
$(function () {
    // Initialize the url has to #step1
    history.replaceState(undefined, undefined, "#step1");


    handle_products.forEach(handle => {
        if (plan_type_based_on_handle && plan_type_based_on_handle.includes(handle.productcountriesid)) {
            selected_handle = handle;
        } else {
            selected_handle = handle_products[0];
        }
    });
    // Select default handle
    // if (handle_products.length > 1) {
    //     selected_handle = handle_products[1];
    // } else {
    //     selected_handle = handle_products[0];
    // }

    // Always select free shave cream
    if (addon_products.length > 0) {
        selected_addon_list.push(addon_products[0]);
    } else {
        selected_addon_list = [];
    }

    //On select panel
    // for (let i = 1; i <= 5; i++) {
    //     $("#step" + i + "-heading.mobile_heading" + i).click(SelectPanel(i));
    //     $("#step" + i + "-heading.topnav-" + i + "-heading").click(SelectPanel(i));
    // }

    //On select handle
    for (let i = 0; i < handle_products.length; i++) {
        let productcountriesid = handle_products[i].productcountriesid;
        $("#handle-" + productcountriesid).click(
            SelectHandle(productcountriesid)
        );

        if (i == 0) {
            $("#handle-" + productcountriesid).click();
        }
    }

    //On select blade
    for (let i = 0; i < blade_products.length; i++) {
        let productcountriesid = blade_products[i].productcountriesid;
        if(blade_products[i].isOffline == 1){
        $(".blade-" + productcountriesid).click(
            SelectBlade(productcountriesid)
        );
        
        if (gender === "female") {
            
                $(".blade-" + productcountriesid).click();
            
        } else {
           
                $(".blade-" + productcountriesid).click();
            
        }
    }
    }

    //On select refill
    for (let i = 0; i < 3; i++) {
        $(".refill-" + i).click(SelectRefill(i));

        if (gender === "female") {
            if (i == 0) {
                $(".refill-" + i).click();
            }
        } else {
            if (i == 0) {
                $(".refill-" + i).click();
            }
        }
    }

    //On select frequency`
    for (let i = 0; i < frequency_list.length; i++) {
        let frequency_id = frequency_list[i].duration;
        $(".frequency-" + frequency_id).click(SelectFrequency(frequency_id));

        if (i == 0) {
            $(".frequency-" + frequency_id).click();
        }
    }

    //On select addon
    // for (let i = 0; i < addon_products.length; i++) {
    //     let productcountriesid = addon_products[i].productcountriesid;
    //     $("#addon-" + productcountriesid).click(SelectAddon(addon_products[i]));

    //     if (productcountriesid == free_shave_cream_id) {
    //         selected_addon_list.push(addon_products[i]);
    //         $("#addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
    //         $("#check_addon_always_deliver").change(function() {
    //             if(this.checked) {
    //                 hasShaveCreamNextBilling = true;
    //                 selected_addon1 = addon_products[i];
    //             }
    //             else {
    //                 hasShaveCreamNextBilling = false;
    //                 selected_addon1 = null;
    //             }
    //         });
    //     }

    //     if (i == 0) {
    //         $("#addon-" + productcountriesid).click(SelectAddon(addon_products[i]));
    //     }
    // }

    //On click next
    $(".button-next").click(function () {
        // If no user id, skip the ajax
        if (user_id === null) {
            if (current_step != 3) {
                current_step++;
                history.replaceState(
                    undefined,
                    undefined,
                    `#step${current_step}`
                );
                UpdateStep(current_step);
                // if (current_step == 2) {
                //     goToTrialPlanViewStep2();
                // }

                // if (current_step == 3) {
                //     goToTrialPlanViewStep3();
                // }
            }

            if (current_step != 1) {
                $("#button-back").removeClass("hidden");
            }

            if (current_step == 3) {
                GetSessionData();
                // goToTrialPlanViewStep3();
            }
        } else {
            if (user_id !== null) {
                // Check if the user have already subscribed for trial plan before
                $.ajax({
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    type: "GET",
                    url:
                        GLOBAL_URL +
                        "/shave-plans/trial-plan/check-trial-plan-exist/" +
                        user_id,
                    data: null,
                    json: true,
                    cache: false,
                    success: function (data) {
                        if (data === "exist") {
                            $("#notification_trial_plan_exist").modal("show");
                        } else {
                            if (current_step != 3) {
                                current_step++;
                                UpdateStep(current_step);
                            }

                            if (current_step != 1) {
                                $("#button-back").removeClass("hidden");
                            }

                            if (current_step == 3) {
                                GetSessionData();
                            }
                        }
                    },
                    error: function (jqXHR) { }
                });
            }
        }
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step != 1) {
            current_step--;
            history.replaceState(undefined, undefined, `#step${current_step}`);
            UpdateStep(current_step);
        }

        if (current_step == 1) {
            history.replaceState(undefined, undefined, `#step1`);
            $("#button-back").addClass("hidden");
        }
    });

    $("#check").click(function () { });

    $("#clear").click(function () { });

    $("#button-proceed-checkout").click(function () {
        UpdateSessionData(true);
    });

    // if Blade Refill selected
    var hash = window.location.hash;
    if (hash == ".refill") {
        $(".button-next").click();
    }
});

// Function: Select handle
function SelectHandle(productcountriesid) {
    return function () {
        $("#handle-" + productcountriesid)
            .removeClass("item-unselected")
            .addClass("item-selected");
        $("#handle-tick-" + productcountriesid).removeClass("d-none");
        handle_products.forEach(product => {
            if (product.productcountriesid != productcountriesid) {
                $("#handle-" + product.productcountriesid)
                    .removeClass("item-selected")
                    .addClass("item-unselected");
                $("#handle-tick-" + product.productcountriesid).addClass(
                    "d-none"
                );
            } else {
                selected_handle = product;
            }
        });
    };
}

// Function: Select blade
function SelectBlade(productcountriesid) {
    return function () {
        $("#selected_trial_blade").val(parseInt(productcountriesid));
        $(".blade-" + productcountriesid)
            .removeClass("item-unselected")
            .addClass("item-selected");
        $("#blade-tick-" + productcountriesid).removeClass("d-none");
        blade_products.forEach(product => {
            if (product.productcountriesid != productcountriesid) {
                $(".blade-" + product.productcountriesid)
                    .removeClass("item-selected")
                    .addClass("item-unselected");
                $("#blade-tick-" + product.productcountriesid).addClass(
                    "d-none"
                );
            } else {
                selected_blade = product;

                // Update refill page data
                $(".refill-0-title").html(
                    `${selected_blade.producttranslatesname}<br><br>`
                );
                if (selected_addon_list.length > 0) {
                    $(".refill-1-title").html(
                        `${selected_blade.producttranslatesname},<br>${addon_products[0].producttranslatesname}<br>`
                    );
                    $(".refill-2-title").html(
                        `${selected_blade.producttranslatesname},<br>${addon_products[0].producttranslatesname},<br> ${addon_products[1].producttranslatesname}`
                    );
                }

                $(".refill-0-price").html(
                    `${currency} ${Number(selected_blade.sellPrice).toFixed(2)}`
                );

                if (selected_addon_list.length > 0) {
                    $(".refill-1-price").html(
                        `${currency} ${(
                            Number(selected_blade.sellPrice) +
                            Number(addon_products[0].sellPrice)
                        ).toFixed(2)}`
                    );
                    $(".refill-2-price").html(
                        `${currency} ${(
                            Number(selected_blade.sellPrice) +
                            Number(addon_products[0].sellPrice) +
                            Number(addon_products[1].sellPrice)
                        ).toFixed(2)}`
                    );
                }


                if (gender == "female") {
                    $(".refill-0-image").attr(
                        "src",
                        GLOBAL_URL_V2 +
                        "/images/common/checkoutAssetsWomen/women-5-blade.jpg"
                    );
                } else {
                    $(".refill-0-image").attr(
                        "src",
                        GLOBAL_URL_V2 +
                        "/images/common/checkoutAssets/checkout-package1-" +
                        selected_blade.bladeCount +
                        "blade.png"
                    );
                }

                if (selected_addon_list.length > 0) {
                    $(".refill-1-image").attr(
                        "src",
                        GLOBAL_URL_V2 +
                        "/images/common/checkoutAssets/checkout-package2-" +
                        selected_blade.bladeCount +
                        "blade.png"
                    );
                    $(".refill-2-image").attr(
                        "src",
                        GLOBAL_URL_V2 +
                        "/images/common/checkoutAssets/checkout-package3-" +
                        selected_blade.bladeCount +
                        "blade.png"
                    );
                }

                if (gender == "female") {
                    $(".package-image").attr(
                        "src",
                        GLOBAL_URL_V2 +
                        "/images/common/checkoutAssetsWomen/TrialKit-Women.jpg"
                    );
                    current_shipping_image =
                        "/images/common/checkoutAssetsWomen/TrialKit-Women.jpg";

                    let blade_type_show = parseInt(selected_blade.bladeCount);
                    for (var i in trial_saving_price_list.female) {
                        if (i == blade_type_show) {
                            $("#changetrialsavingprice").html(trial_saving_price_list.female[i]);
                        }
                    }
                } else {
                    if (country_handles == "H3") {
                        $(".package-image").attr(
                            "src",
                            GLOBAL_URL_V2 +
                            "/images/common/checkoutAssets/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png"
                        );
                        current_shipping_image =
                            "/images/common/checkoutAssets/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png";

                        let blade_type_show = parseInt(selected_blade.bladeCount);
                        for (var i in trial_saving_price_list.male) {
                            if (i == blade_type_show) {
                                $("#changetrialsavingprice").html(trial_saving_price_list.male[i]);
                            }
                        }
                    } else if (country_handles == "H1") {
                        $(".package-image").attr(
                            "src",
                            GLOBAL_URL_V2 +
                            "/images/common/premium/product/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png"
                        );
                        current_shipping_image =
                            "/images/common/premium/product/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png";

                        let blade_type_show = parseInt(selected_blade.bladeCount);
                        for (var i in trial_saving_price_list.male) {
                            if (i == blade_type_show) {
                                $("#changetrialsavingprice").html(trial_saving_price_list.male[i]);
                            }
                        }
                    } else {
                        $(".package-image").attr(
                            "src",
                            GLOBAL_URL_V2 +
                            "/images/common/checkoutAssets/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png"
                        );
                        current_shipping_image =
                            "/images/common/checkoutAssets/TrialKit-" +
                            selected_blade.bladeCount +
                            "blade.png";

                        let blade_type_show = parseInt(selected_blade.bladeCount);
                        for (var i in trial_saving_price_list.male) {
                            if (i == blade_type_show) {
                                $("#changetrialsavingprice").html(trial_saving_price_list.male[i]);
                            }
                        }
                    }

                }

                GetPlanIdAndPrice();
            }
        });
    };
}

// Function: Select refill
function SelectRefill(position, bladeCount) {
    return function () {
        $(".refill-" + position)
            .removeClass("item-unselected")
            .addClass("item-selected");
        $("#refill-tick-" + position).removeClass("d-none");
        var list_refill_position = [0, 1, 2];
        list_refill_position.forEach(num => {
            if (position != num) {
                $(".refill-" + num)
                    .removeClass("item-selected")
                    .addClass("item-unselected");
                $("#refill-tick-" + num).addClass("d-none");
            }
        });

        if (position == 0) {
            hasShaveCreamNextBilling = false;
            selected_addon_list = selected_addon_list.filter(
                (word, index, arr) => {
                    return index == 0;
                }
            );
            next_billing_image =
                "/images/common/checkoutAssets/checkout-package1-" +
                bladeCount +
                "blade-small.png";
        } else if (position == 1) {
            hasShaveCreamNextBilling = true;
            selected_addon_list = selected_addon_list.filter(
                (word, index, arr) => {
                    return index == 0;
                }
            );
            next_billing_image =
                "/images/common/checkoutAssets/checkout-package2-" +
                bladeCount +
                "blade-small.png";
        } else if (position == 2) {
            hasShaveCreamNextBilling = true;
            selected_addon_list.push(addon_products[1]);
            next_billing_image =
                "/images/common/checkoutAssets/checkout-package3-" +
                bladeCount +
                "blade-small.png";
        }

        GetPlanIdAndPrice();
    };
}

// Function: Select frequency
function SelectFrequency(frequency_id) {
    // console.log("START SELECT FREQUENCY", frequency_id);
    $(".frequency-" + frequency_id)
        .removeClass("item-unselected")
        .addClass("item-selected");
    $(".frequency-" + frequency_id + " .frequency-p").html(
        trans(
            "website_contents.authenticated.trial_plan.selection.select_freq_desc",
            { frequency_id: frequency_id }
        )
    );
    $("#frequency-tick-" + frequency_id).removeClass("d-none");
    let imagepath = $(".frequency-" + frequency_id + " img").attr("src");
    var res = imagepath.split(".png");
    var res1 = res[0].replace("-over", "");
    $(".frequency-" + frequency_id + " img").attr("src", res1 + "-over.png");
    frequency_list.forEach(frequency => {
        var duration = frequency.duration;
        // console.log(frequency.duration);
        if (duration != frequency_id) {

            let imagepath = $(".frequency-" + duration + " img").attr("src");
            var res = imagepath.split(".png");
            var res1 = res[0].replace("-over", "");
            $(".frequency-" + duration + " img").attr("src", res1 + ".png");
            $(".frequency-" + duration)
                .removeClass("item-selected")
                .addClass("item-unselected");
            $(".frequency-" + duration + " .frequency-p").html(trans('website_contents.authenticated.frequency.2.detailsText', { months: duration }));
            $("#frequency-tick-" + duration).addClass("d-none");
        } else {
      
            selected_frequency = duration;
        }
    });
    GetPlanIdAndPrice();
}

// Function: Select addon
function SelectAddon(addon) {
    return function () {
        let productcountriesid = addon.productcountriesid;

        if (productcountriesid != free_shave_cream_id) {
            if ($("#addon-" + productcountriesid).hasClass("item-unselected")) {
                $("#addon-" + productcountriesid)
                    .removeClass("item-unselected")
                    .addClass("item-selected");
                $("#addon-tick-" + productcountriesid).removeClass("d-none");
                selected_addon_list.push(addon);
                selected_addon2 = addon;
            } else {
                $("#addon-" + productcountriesid)
                    .removeClass("item-selected")
                    .addClass("item-unselected");
                $("#addon-tick-" + productcountriesid).addClass("d-none");
                selected_addon_list = selected_addon_list.filter(function (
                    value,
                    index,
                    arr
                ) {
                    return value != addon;
                });
                selected_addon2 = null;
            }
        }
    };
}

//Function: Update step
function UpdateStep(current_step) {
    // Only show selected panel and hide others
    for (let i = 1; i <= 5; i++) {
        var z = current_step - 1;
        if (i == current_step) {
            $("#step" + i + "-heading.mobile_heading" + i)
                .removeClass("panel-heading-unselected")
                .addClass("panel-heading-selected");
            $("#step" + i + "-title.mobile_title" + i)
                .removeClass("panel-title-unselected")
                .addClass("panel-title-selected");
            $("#step" + i + "-heading.topnav-" + i + "-heading")
                .removeClass("bord-thick-white")
                .addClass("bord-thick");
            $("#step" + z + "-title.topnav-" + z + "-arrow")
                .removeClass("bord-thick-white")
                .addClass("bord-thick");
            $("#collapse" + i).addClass("show");
        } else {
            $("#step" + i + "-heading.mobile_heading" + i)
                .removeClass("panel-heading-selected")
                .addClass("panel-heading-unselected");
            $("#step" + i + "-title.mobile_title" + i)
                .removeClass("panel-title-selected")
                .addClass("panel-title-unselected");
            $("#collapse" + i).removeClass("show");
        }
    }

    // Do something according to current_step id
    switch (current_step) {
        case 1:
            {
                //do nothing now
            }
            break;
        case 2:
            {
                //do nothing now
            }
            break;
        case 3:
            {
                //do nothing now
            }
            break;
        case 4:
            {
                $(".button-next").removeClass("display-none");
                $("#form-checkout").addClass("display-none");
            }
            break;
        case 5:
            {
                $(".button-next").addClass("display-none");
                $("#form-checkout").removeClass("display-none");
                DisplaySelectedItems();
                GetPlanIdAndPrice();
            }
            break;
        default:
            break;
    }
    GetSessionData();
}

//Function: Display Selected Handle,Blade,Frequency and Addons
function DisplaySelectedItems() {
    $(".text_selected_handle").text(selected_handle["producttranslatesname"]);
    $(".text_selected_blade").text(selected_blade["producttranslatesname"]);
    $(".text_selected_frequency").text(selected_frequency + " months");
    if (selected_addon1 != null) {
        $(".selected-addon1").removeClass("d-none");
        $(".text_selected_addon1").text(
            selected_addon1["producttranslatesname"]
        );
    }
    if (selected_addon2 != null) {
        $(".selected-addon2").removeClass("d-none");
        $(".text_selected_addon2").text(
            selected_addon2["producttranslatesname"]
        );
    }
}

//Function: Calculate summary of plan price
function GetPlanIdAndPrice() {
    let today_subtotal_price = 0.0;
    let next_month_subtotal_price = 0.0;
    let shipping_price = 0.0;
    let payment_today_price = 0.0;

    let ProductCountriesId = [];
    let ProductCountriesIdGet = "";
    let count1 = 0;
    let count2 = 0;
    let planCombineCountry = "";

    let totalprice = 0.0;
    let nextmonthprice = 0.0;
    let shippingfee = 0.0;
    let currentprice = 0.0;
    let planpriceget = 0.0;
    let ProductSellPrice;
    let ProductCountriesIdAddOn = [];
    let planskuget = "";
    let planidget = "";

    // Proceed only if all handle, blade and frequency is selected
    if (selected_handle && selected_blade && selected_frequency) {
        // console.log(selected_handle, selected_blade, selected_frequency);
        // Generate combinations of selected product countries id in ascending order
        ProductCountriesId.push(selected_handle["productcountriesid"]);
        ProductCountriesId.push(selected_blade["productcountriesid"]);
        if (ProductCountriesId.length > 1) {
            ProductCountriesId.sort(function (a, b) {
                return a - b;
            });
        }
        ProductCountriesId.forEach(function (productCountryId) {
            if (count1 == 0) {
                ProductCountriesIdGet =
                    ProductCountriesIdGet + productCountryId + "";
            } else {
                ProductCountriesIdGet =
                    ProductCountriesIdGet + "," + productCountryId + "";
            }
            count1++;
        });

        // Loop through all Trial Plans
        if (allplan) {
            allplan.forEach(function (plan) {
                planCombineCountry = "";
                // Filter selected plan duration (frequency)
                if (plan.planduration == selected_frequency) {
                    // Generate combinations of product countries id from selected plan
                    plan.productCountryId.forEach(function (productCountryId) {
                        if (count2 == 0) {
                            planCombineCountry =
                                planCombineCountry + productCountryId;
                        } else {
                            planCombineCountry =
                                planCombineCountry + "," + productCountryId;
                        }
                        count2++;
                    });

                    // If selected product combinations matches with the plan product combinations, get the plan id, sku and price
                    if (ProductCountriesIdGet == planCombineCountry) {
                        planidget = plan.planid;
                        selected_plan_id = planidget;
                        planpriceget = parseFloat(plan.trialPrice);
                        planskuget = plan.plansku;
                    }
                }
                count2 = 0;
            });
        }

        // Calculate price for plan summary
        ProductSellPrice = parseFloat(selected_blade["sellPrice"]);
        nextmonthprice = nextmonthprice + ProductSellPrice;

        // Add price for premium addons
        selected_addon_list.forEach(function (data) {
            ProductCountriesIdAddOn.push(data["productcountriesid"]);
            if (data["productcountriesid"] == free_shave_cream_id) {
                if (hasShaveCreamNextBilling) {
                    nextmonthprice =
                        nextmonthprice + parseFloat(data["sellPrice"]);
                }
            } else {
                //planpriceget = planpriceget + parseFloat(data["sellPrice"]);
                nextmonthprice = nextmonthprice + parseFloat(data["sellPrice"]);
            }
        });

        // Show price in summary details
        $(".today-subtotal").text(planpriceget.toFixed(2) + "");
        $(".next-month-subtotal").text(nextmonthprice.toFixed(2) + "");
        $(".shipping-total").text(shippingfee.toFixed(2) + "");
        $(".today-payment-total").text(planpriceget.toFixed(2) + "");
        $(".next-month-payment-total").text(nextmonthprice.toFixed(2) + "");

        current_p = planpriceget.toFixed(2) + "";
        next_p = nextmonthprice.toFixed(2) + "";
        shipping = shippingfee.toFixed(2) + "";

        UpdateSessionData(false);
    }
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_GET, null).done(function (
        data
    ) {
        if (data) {
            // console.log(data);
            //let session_data = JSON.parse(data);
            //let previous_step = session_data["selection"]["current_step"];
            //current_step = previous_step;
            //UpdateStep(previous_step);
        }
    });
}

// Function: Update session data
function UpdateSessionData(isProceedCheckout) {
    let session_data = {};

    // console.log("selected_blade", selected_blade);
    // console.log("selected_addon_list", selected_addon_list);
    // console.log("selected_frequency", selected_frequency);
    // console.log("selected_plan_id", selected_plan_id);
    // console.log("current_p", current_p);
    // console.log("shipping", shipping);
    // console.log("next_p", next_p);
    // console.log("hasShaveCreamNextBilling", hasShaveCreamNextBilling);
    // console.log("current_step", current_step);
    // console.log("next_billing_image", next_billing_image);
    // console.log("current_shipping_image", current_shipping_image);

    // New session
    session_data["selection"] = {};
    session_data["selection"]["step1"] = { selected_blade: selected_blade };
    session_data["selection"]["step2"] = {
        selected_addon_list: selected_addon_list
    };
    session_data["selection"]["step3"] = {
        selected_frequency: selected_frequency
    };
    session_data["selection"]["summary"] = {
        planId: selected_plan_id,
        current_price: current_p,
        shipping_fee: shipping,
        next_price: next_p
    };
    session_data["selection"][
        "has_shave_cream_next_billing"
    ] = hasShaveCreamNextBilling;
    session_data["selection"]["current_step"] = current_step;
    session_data["selection"]["plan_type"] = "trial-plan";
    session_data["selection"]["journey_type"] = "selection";
    session_data["selection"]["next_billing_image"] = next_billing_image;
    session_data["selection"][
        "current_shipping_image"
    ] = current_shipping_image;
    session_data["selection"]["switch_plans"] = {
        isSwitched: false,
        _normal: null,
        _annual: null,
        isSelected: "normal"
    };
    session(
        SESSION_SELECTION_TRIAL_PLAN,
        SESSION_SET,
        JSON.stringify(session_data)
    ).done(function () {
        if (isProceedCheckout) {
            $("#loading").css("display", "block");
            let data = {
                handle_products: handle_products,
                blade_products: blade_products,
                frequency_list: frequency_list,
                addon_products: addon_products,
                free_shave_cream_id: free_shave_cream_id,
                allplan: allplan,
                country_id: country_id,
                currency: currency,
                trial_price: trial_price,
                trial_saving_price: trial_saving_price,
                next_refill_date: next_refill_date,
                user_id: user_id,
                selected_handle: selected_handle,
                selected_blade: selected_blade,
                selected_frequency: selected_frequency,
                selected_addon_list: selected_addon_list,
                selected_addon1: selected_addon1,
                selected_addon2: selected_addon2,
                selected_plan_id: selected_plan_id,
                current_shipping_image: current_shipping_image,
                next_billing_image: next_billing_image,
                hasShaveCreamNextBilling: hasShaveCreamNextBilling,
                current_p: current_p,
                next_p: next_p,
                shipping: shipping,
                session_data: session_data,
                langCode: langCode,
                currentCountryIso: country.codeIso,
                urllangCode: langCode + "-" + country.codeIso.toLowerCase(),
                gender: gender
            };

            session(
                TRIAL_CHECKOUT_PAYMENT_FAILED,
                "set",
                JSON.stringify(data)
            ).done(function () {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    url: API_URL + "/ba/products/trial/start-checkout",
                    data: { data: data },
                    success: function (response) {
                        // console.log(response);
                        if (response.success === true) {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    "X-CSRF-TOKEN": $(
                                        'meta[name="csrf-token"]'
                                    ).attr("content")
                                },
                                url: GLOBAL_URL + "/trial/_checkout",
                                data: { data: response.payload },
                                success: function (response) {
                                    // console.log(response);
                                    if (response.url) {
                                        $("#goToSelectedView").attr(
                                            "action",
                                            response.url
                                        );
                                        $("#trial_data").val(
                                            JSON.stringify(response.data)
                                        );
                                        $("#goToSelectedView").submit();
                                    }
                                },
                                error: function (data) {
                                    console.log(data);
                                    $("#loading").css("display", "none");
                                },
                                failure: function (data) {
                                    console.log(data);
                                    $("#loading").css("display", "none");
                                }
                            });
                        } else {
                            $("#loading").css("display", "none");
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        $("#loading").css("display", "none");
                    },
                    failure: function (data) {
                        console.log(data);
                        $("#loading").css("display", "none");
                    }
                });
            });
        }
    });
}

// Function: Clear session data
function ClearSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_CLEAR, null).done(function () {
        //console.log("Successfully cleared session.");
    });
}

// Function: Select panel
function SelectPanel(panel_id) {
    return function () {
        //Current step is ahead of the selected panel id
        if (panel_id <= current_step) {
            // Show or hide selected panel
            var z = current_step - 1;
            if (!$("#collapse" + panel_id).hasClass("show")) {
                $("#step1-heading.mobile_heading1")
                    .removeClass("panel-heading-selected")
                    .addClass("panel-heading-unselected");
                $("#step1-title.mobile_title1")
                    .removeClass("panel-title-selected")
                    .addClass("panel-title-unselected");
                $("#collapse1").removeClass("show");
                $("#step2-heading.mobile_heading2")
                    .removeClass("panel-heading-selected")
                    .addClass("panel-heading-unselected");
                $("#step2-title.mobile_title2")
                    .removeClass("panel-title-selected")
                    .addClass("panel-title-unselected");
                $("#collapse2").removeClass("show");
                $("#step3-heading.mobile_heading3")
                    .removeClass("panel-heading-selected")
                    .addClass("panel-heading-unselected");
                $("#step3-title.mobile_title3")
                    .removeClass("panel-title-selected")
                    .addClass("panel-title-unselected");
                $("#collapse3").removeClass("show");
                // $("#step4-heading.mobile_heading4").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                // $("#step4-title.mobile_title4").removeClass('panel-title-selected').addClass('panel-title-unselected');
                // $("#collapse4").removeClass('show');
                // $("#step5-heading.mobile_heading5").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                // $("#step5-title.mobile_title5").removeClass('panel-title-selected').addClass('panel-title-unselected');
                // $("#collapse5").removeClass('show');
                $("#step" + panel_id + "-heading.mobile_heading" + panel_id)
                    .removeClass("panel-heading-unselected")
                    .addClass("panel-heading-selected");
                $("#step" + panel_id + "-title.mobile_title" + panel_id)
                    .removeClass("panel-title-unselected")
                    .addClass("panel-title-selected");
                $(
                    "#step" +
                    panel_id +
                    "-heading.topnav-" +
                    panel_id +
                    "-heading"
                )
                    .removeClass("bord-thick-white")
                    .addClass("bord-thick");
                $("#step" + z + "-title.topnav-" + z + "-arrow")
                    .removeClass("bord-thick-white")
                    .addClass("bord-thick");
                $("#collapse" + panel_id).addClass("show");
            } else {
                $("#step" + panel_id + "-heading.mobile_heading" + panel_id)
                    .removeClass("panel-heading-selected")
                    .addClass("panel-heading-unselected");
                $("#step" + panel_id + "-title.mobile_title" + panel_id)
                    .removeClass("panel-title-selected")
                    .addClass("panel-title-unselected");
                $("#collapse" + panel_id).removeClass("show");
            }
        }
    };
}
