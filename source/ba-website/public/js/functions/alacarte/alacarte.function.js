let selected_handle_list = [];
let selected_blade_list = [];
let selected_addon_list = [];
let selected_ask_list = [];
let current_price = null;
let shipping = 0.00.toFixed(2) + "";

function GetTotalPrice() {
    current_price = 0;
    if (selected_handle_list.length >= 1) {
        selected_handle_list.forEach(selection => {
            current_price += (selection.Price * selection.Quantity);
        });
    }
    if (selected_blade_list.length >= 1) {
        selected_blade_list.forEach(selection => {
            current_price += (selection.Price * selection.Quantity);
        });
    }
    if (selected_addon_list.length >= 1) {
        selected_addon_list.forEach(selection => {
            current_price += (selection.Price * selection.Quantity);
        });
    }
    if (selected_ask_list.length >= 1) {
        selected_ask_list.forEach(selection => {
            current_price += (selection.Price * selection.Quantity);
        });
    }
    current_price = current_price.toFixed(2) + "";
    $(".price-display").html(" - " + currency + " " + current_price );

    if (selected_handle_list.length != 0 || selected_blade_list.length != 0 || selected_addon_list.length != 0 || selected_ask_list.length != 0) {
        $(".button-proceed-checkout").prop('disabled', false);
    }
    else {
        $(".button-proceed-checkout").prop('disabled', true);
    }
}

function SelectHandle(productcountriesid) {
    return function (e) {
        var handle = {};
        handle_products.forEach(product => {
            if (product.ProductCountryId == productcountriesid) {
                handle["ProductCountryId"] = product.ProductCountryId;
                handle["ProductName"] = product.name;
                handle["Quantity"] = product.order_qty;
                handle["Price"] = product.sellPrice;
            }
        });
        var handle_selection = $("#handle-" + productcountriesid);
        var handle_qty = $("#handle_qty_" + productcountriesid);
        // console.log(e.target.id);

        if ( e.target.nodeName != "BUTTON" && e.target.nodeName != "INPUT" && handle_selection.hasClass("item-selected") ) {
            handle_selection.removeClass('item-selected').addClass('item-unselected');
            handle_qty.val(0);
            // $("#handle_price_" + productcountriesid).html( (0).toFixed(2) );

            selected_handle_list = selected_handle_list.filter(function(value,index,arr) {
                return value["ProductCountryId"] != handle["ProductCountryId"];
            });
        }
        else if ( e.target.id === "handle_remove_" + productcountriesid) {
            // console.log(handle_qty.val())
            if (handle_qty.val() == "0" || handle_qty.val() == "" ) {
                handle_selection.removeClass('item-selected').addClass('item-unselected');
                handle_qty.val(0);
                handle_qty.trigger('change');
                // $("#handle_price_" + productcountriesid).html( (0).toFixed(2) );

                selected_handle_list = selected_handle_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != handle["ProductCountryId"];
                });
            }
            else {
                selected_handle_list = selected_handle_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != handle["ProductCountryId"];
                });
                selected_handle_list.push(handle);
            }
        }
        else if ( e.target.id != "handle_remove_" + productcountriesid)  {
            handle_selection.removeClass('item-unselected').addClass('item-selected');
            if (handle_qty.val() == "0" || handle_qty.val() == "" ) {
                handle_selection.removeClass('item-unselected').addClass('item-selected');
                handle_qty.val(1);
                handle_qty.trigger('change');
                // $("#handle_price_" + productcountriesid).html(handle["Price"]);

                selected_handle_list = selected_handle_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != handle["ProductCountryId"];
                });
                selected_handle_list.push(handle);
            }
            else {
                selected_handle_list = selected_handle_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != handle["ProductCountryId"];
                });
                selected_handle_list.push(handle);
            }
        }
        GetTotalPrice();
    };
}

// Function: Select blade
function SelectBlade(productcountriesid) {
    return function (e) {
        var blade = {};
        blade_products.forEach(product => {
            if (product.ProductCountryId == productcountriesid) {
                blade["ProductCountryId"] = product.ProductCountryId;
                blade["ProductName"] = product.name;
                blade["Quantity"] = product.order_qty;
                blade["Price"] = product.sellPrice;
            }
        });
        var blade_selection = $("#blade-" + productcountriesid);
        var blade_qty = $("#blade_qty_" + productcountriesid);

        if ( e.target.nodeName != "BUTTON" && e.target.nodeName != "INPUT" && blade_selection.hasClass("item-selected") ) {
            blade_selection.removeClass('item-selected').addClass('item-unselected');
            blade_qty.val(0);
            // $("#blade_price_" + productcountriesid).html( (0).toFixed(2) );

            selected_blade_list = selected_blade_list.filter(function(value,index,arr) {
                return value["ProductCountryId"] != blade["ProductCountryId"];
            });
        }
        else if ( e.target.id === "blade_remove_" + productcountriesid) {
            if (blade_qty.val() == "0" || blade_qty.val() == "" ) {
                blade_selection.removeClass('item-selected').addClass('item-unselected');
                blade_qty.val(0);
                blade_qty.trigger('change');
                // $("#blade_price_" + productcountriesid).html( (0).toFixed(2) );

                selected_blade_list = selected_blade_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != blade["ProductCountryId"];
                });
            }
            else {
                selected_blade_list = selected_blade_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != blade["ProductCountryId"];
                });
                selected_blade_list.push(blade);
            }
        }
        else if (e.target.id == ("blade_add_" + productcountriesid) || e.target.id == ("blade-" + productcountriesid)) {
            blade_selection.removeClass('item-unselected').addClass('item-selected');
            if (blade_qty.val() == "0" || blade_qty.val() == "" ) {
                blade_qty.val(1);
                blade_qty.trigger('change');
                // $("#blade_price_" + productcountriesid).html(blade["Price"]);
            }
            else {
                selected_blade_list = selected_blade_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != blade["ProductCountryId"];
                });
                selected_blade_list.push(blade);
            }
        }
        GetTotalPrice();
    };
}

function SelectAddon(productcountriesid) {
    return function (e) {
        var addon = {};
        addon_products.forEach(product => {
            if (product.ProductCountryId == productcountriesid) {
                addon["ProductCountryId"] = product.ProductCountryId;
                addon["ProductName"] = product.name;
                addon["Quantity"] = product.order_qty;
                addon["Price"] = product.sellPrice;
            }
        });
        var addon_selection = $("#addon-" + productcountriesid);
        var addon_qty = $("#addon_qty_" + productcountriesid);

        if ( e.target.nodeName != "BUTTON" && e.target.nodeName != "INPUT" && addon_selection.hasClass("item-selected") ) {
            addon_selection.removeClass('item-selected').addClass('item-unselected');
            addon_qty.val(0);
            // $("#addon_price_" + productcountriesid).html( (0).toFixed(2) );

            selected_addon_list = selected_addon_list.filter(function(value,index,arr) {
                return value["ProductCountryId"] != addon["ProductCountryId"];
            });
        }
        else if ( e.target.id === "addon_remove_" + productcountriesid) {
            if (addon_qty.val() == "0" || addon_qty.val() == "" ) {
                addon_selection.removeClass('item-selected').addClass('item-unselected');
                addon_qty.val(0);
                addon_qty.trigger('change');
                // $("#addon_price_" + productcountriesid).html( (0).toFixed(2) );

                selected_addon_list = selected_addon_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != addon["ProductCountryId"];
                });
            }
            else {
                selected_addon_list = selected_addon_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != addon["ProductCountryId"];
                });
                selected_addon_list.push(addon);
            }
        }
        else if (e.target.id == ("addon_add_" + productcountriesid) || e.target.id == ("addon-" + productcountriesid)) {
            addon_selection.removeClass('item-unselected').addClass('item-selected');
            if (addon_qty.val() == "0" || addon_qty.val() == "" ) {
                addon_qty.val(1);
                addon_qty.trigger('change');
                // $("#addon_price_" + productcountriesid).html(addon["Price"]);
            }
            else {
                selected_addon_list = selected_addon_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != addon["ProductCountryId"];
                });
                selected_addon_list.push(addon);
            }
        }
        GetTotalPrice();
    };
}

function SelectASK(productcountriesid) {
    return function (e) {
        var ask = {};
        ask_products.forEach(product => {
            if (product.ProductCountryId == productcountriesid) {
                ask["ProductCountryId"] = product.ProductCountryId;
                ask["ProductName"] = product.name;
                ask["Quantity"] = product.order_qty;
                ask["Price"] = product.sellPrice;
            }
        });
        var ask_selection = $("#ask-" + productcountriesid);
        var ask_qty = $("#ask_qty_" + productcountriesid);

        if ( e.target.nodeName != "BUTTON" && e.target.nodeName != "INPUT" && ask_selection.hasClass("item-selected") ) {
            ask_selection.removeClass('item-selected').addClass('item-unselected');
            ask_qty.val(0);
            // $("#ask_price_" + productcountriesid).html( (0).toFixed(2) );

            selected_ask_list = selected_ask_list.filter(function(value,index,arr) {
                return value["ProductCountryId"] != ask["ProductCountryId"];
            });
        }
        else if ( e.target.id === "ask_remove_" + productcountriesid) {
            if (ask_qty.val() == "0" || ask_qty.val() == "" ) {
                ask_selection.removeClass('item-selected').addClass('item-unselected');
                ask_qty.val(0);
                ask_qty.trigger('change');
                // $("#ask_price_" + productcountriesid).html( (0).toFixed(2) );

                selected_ask_list = selected_ask_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != ask["ProductCountryId"];
                });
            }
            else {
                selected_ask_list = selected_ask_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != ask["ProductCountryId"];
                });
                selected_ask_list.push(ask);
            }
        }
        else if (e.target.id == ("ask_add_" + productcountriesid) || e.target.id == ("ask-" + productcountriesid)) {
            ask_selection.removeClass('item-unselected').addClass('item-selected');
            if (ask_qty.val() == "0" || ask_qty.val() == "" ) {
                ask_qty.val(1);
                ask_qty.trigger('change');
                // $("#ask_price_" + productcountriesid).html(ask["Price"]);
            }
            else {
                selected_ask_list = selected_ask_list.filter(function(value,index,arr) {
                    return value["ProductCountryId"] != ask["ProductCountryId"];
                });
                selected_ask_list.push(ask);
            }
        }
        GetTotalPrice();
    };
}

function UpdateStep(new_step) {
    current_step = new_step;
    // Only show selected panel and hide others
    for (let i = 1; i <= 4; i++) {
        var z = current_step - 1;
        if (i == current_step) {
            $("#collapse" + i).addClass('show');
            $("#collapse" + (i - 1)).removeClass('show');
        } else {
            $("#collapse" + i).removeClass('show');
        }
    }

    // Do something according to current_step id
    switch (current_step) {
        case 1:
            {
                if (handle_products == null) {
                    UpdateStep(2);
                }
                window.location.hash = "#handle";
                $(".button-next").removeClass("d-none");
                $(".button-proceed-checkout").removeClass("d-none").addClass("d-none");
            }
            break;
        case 2:
            {
                if (blade_products == null) {
                    UpdateStep(3);
                }
                window.location.hash = "#blade";
                $(".button-next").removeClass("d-none");
                $(".button-proceed-checkout").removeClass("d-none").addClass("d-none");
            }
            break;
        case 3:
            {
                if (addon_products == null) {
                    UpdateStep(4);
                }
                window.location.hash = "#addon";
                $(".button-next").removeClass("d-none");
                $(".button-proceed-checkout").removeClass("d-none").addClass("d-none");
            }
            break;
        case 4:
            {
                window.location.hash = "#ask";
                $(".button-next").removeClass("d-none").addClass("d-none");
                $(".button-proceed-checkout").removeClass("d-none");
            }
            break;
        default:
            break;
    }
    UpdateSessionData(false);
    GetSessionData()
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_SELECTION_ALACARTE_BA, SESSION_GET, null).done(function (data) {
        if (data) {
            // console.log(data);
            //let session_data = JSON.parse(data);
            //let previous_step = session_data["selection"]["current_step"];
            //current_step = previous_step;
            //UpdateStep(previous_step);
        }
    });
}

function UpdateSessionData(isProceedCheckout) {
    let session_data = {};

    session_data = {
        "selection": {
            "selected_handle_list": selected_handle_list,
            "selected_blade_list": selected_blade_list,
            "selected_addon_list": selected_addon_list,
            "selected_ask_list": selected_ask_list,
            "summary": {
                "current_price": current_price,
                "shipping_fee": shipping,
            },
            "current_step": { current_step },
        },
    };

    session(SESSION_SELECTION_ALACARTE_BA, SESSION_SET, JSON.stringify(session_data)).done(function () {
        if (isProceedCheckout) {
            $("#loading").css("display", "block");

            let data = {
                handle_products: handle_products,
                blade_products: blade_products,
                addon_products: addon_products,
                ask_products: ask_products,
                country_id: country_id,
                currency: currency,
                user_id: user_id,
                selected_handle_list: selected_handle_list,
                selected_blade_list: selected_blade_list,
                selected_addon_list: selected_addon_list,
                selected_ask_list: selected_ask_list,
                session_data: session_data,
                langCode: langCode,
                currentCountryIso: country.codeIso,
                urllangCode: langCode + "-" + country.codeIso.toLowerCase()
            };

            $.ajax({
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                url: API_URL + "/ba/products/alacarte/start-checkout",
                data: { data: data },
                success: function(response) {
                    // console.log(response);
                    if (response.success === true) {
                        $("#goToSelectedView").attr(
                            "action",
                            GLOBAL_URL + "/alacarte/select-payment-method"
                        );
                        $("#product_data").val(
                            JSON.stringify(response.payload)
                        );
                        $("#goToSelectedView").submit();
                    }
                },
                error: function(data) {},
                failure: function(data) {}
            });
        }
    });
}

// Function: Clear session data
function ClearSessionData() {
    session(SESSION_SELECTION_ALACARTE_BA, SESSION_CLEAR, null).done(function () {
        //console.log("Successfully cleared session.");
    });
}

// On page load
$(function () {
    handle_products.forEach(product => {
        $("#handle-" + product.ProductCountryId).click(SelectHandle(product.ProductCountryId));
        $("#handle_qty_" + product.ProductCountryId).change( function () {
            var qty = $("#handle_qty_" + product.ProductCountryId).val()
            var total = $(this).val() * product.sellPrice;
            if (qty >= 0 ) {
                // $("#handle_price_" + product.ProductCountryId).html( total.toFixed(2) );
                product.order_qty = qty;
            }
            else {
                $("#handle_qty_" + product.ProductCountryId).val(0);
            }
            SelectHandle(product.ProductCountryId);
        });

        $("#handle_add_" + product.ProductCountryId).click( function () {
            var qty = $("#handle_qty_" + product.ProductCountryId).val()
            if (qty < 9) {
                $("#handle_qty_" + product.ProductCountryId).val( ++qty );
                $("#handle_qty_" + product.ProductCountryId).trigger('change');
            }
        });

        $("#handle_remove_" + product.ProductCountryId).click( function () {
            var qty = $("#handle_qty_" + product.ProductCountryId).val()
            $("#handle_qty_" + product.ProductCountryId).val( --qty );
            $("#handle_qty_" + product.ProductCountryId).trigger('change');
        });
    });

    blade_products.forEach(product => {
        $("#blade-" + product.ProductCountryId).click(SelectBlade(product.ProductCountryId));

        $("#blade_qty_" + product.ProductCountryId).change( function () {
            var total = $(this).val() * product.sellPrice;
            var qty = $("#blade_qty_" + product.ProductCountryId).val()

            if (qty >= 0) {
                // $("#blade_price_" + product.ProductCountryId).html( total.toFixed(2) );
                product.order_qty = qty;
            }
            else {
                $("#blade_qty_" + product.ProductCountryId).val(0);
            }
            SelectBlade(product.ProductCountryId);
        });

        $("#blade_add_" + product.ProductCountryId).click( function () {
            var qty = $("#blade_qty_" + product.ProductCountryId).val()
            if (qty < 9) {
                $("#blade_qty_" + product.ProductCountryId).val( ++qty );
                $("#blade_qty_" + product.ProductCountryId).trigger('change');
            }
        });

        $("#blade_remove_" + product.ProductCountryId).click( function () {
            var qty = $("#blade_qty_" + product.ProductCountryId).val()
            $("#blade_qty_" + product.ProductCountryId).val( --qty );
            $("#blade_qty_" + product.ProductCountryId).trigger('change');
        });
    });

    addon_products.forEach(product => {
        $("#addon-" + product.ProductCountryId).click(SelectAddon(product.ProductCountryId));

        $("#addon_qty_" + product.ProductCountryId).change( function () {
            var total = $(this).val() * product.sellPrice;
            var qty = $("#addon_qty_" + product.ProductCountryId).val()

            if (qty >= 0 ) {
                // $("#addon_price_" + product.ProductCountryId).html( total.toFixed(2) );
                product.order_qty = qty;
            }
            else {
                $("#addon_qty_" + product.ProductCountryId).val(0);
            }
            SelectAddon(product.ProductCountryId);
        });

        $("#addon_add_" + product.ProductCountryId).click( function () {
            var qty = $("#addon_qty_" + product.ProductCountryId).val()
            if (qty < 9) {
                $("#addon_qty_" + product.ProductCountryId).val( ++qty );
                $("#addon_qty_" + product.ProductCountryId).trigger('change');
            }
        });

        $("#addon_remove_" + product.ProductCountryId).click( function () {
            var qty = $("#addon_qty_" + product.ProductCountryId).val()
            $("#addon_qty_" + product.ProductCountryId).val( --qty );
            $("#addon_qty_" + product.ProductCountryId).trigger('change');
        });
    });

    ask_products.forEach(product => {
        $("#ask-" + product.ProductCountryId).click(SelectASK(product.ProductCountryId));

        $("#ask_qty_" + product.ProductCountryId).change( function () {
            var total = $(this).val() * product.sellPrice;
            var qty = $("#ask_qty_" + product.ProductCountryId).val()

            if (qty >= 0 ) {
                // $("#ask_price_" + product.ProductCountryId).html( total.toFixed(2) );
                product.order_qty = qty;
            }
            else if (qty >= 9) {
                $("#ask_qty_" + product.ProductCountryId).val(9);
            }
            else {
                $("#ask_qty_" + product.ProductCountryId).val(0);
            }
            SelectASK(product.ProductCountryId);
        });

        $("#ask_add_" + product.ProductCountryId).click( function () {
            var qty = $("#ask_qty_" + product.ProductCountryId).val()
            if (qty < 9) {
                $("#ask_qty_" + product.ProductCountryId).val( ++qty );
                $("#ask_qty_" + product.ProductCountryId).trigger('change');
            }
        });

        $("#ask_remove_" + product.ProductCountryId).click( function () {
            var qty = $("#ask_qty_" + product.ProductCountryId).val()
            $("#ask_qty_" + product.ProductCountryId).val( --qty );
            $("#ask_qty_" + product.ProductCountryId).trigger('change');
        });
    });

    if (saved_selection != "" && saved_selection != null) {
        saved_selection.selection.selected_handle_list.forEach(selection => {
            // console.log(selection);
            $("#handle-" + selection.ProductCountryId).click();
            $("#handle_qty_" + selection.ProductCountryId).val(selection.Quantity);
            // $("#handle_price_" + selection.ProductCountryId).html( (selection.Price * selection.Quantity).toFixed(2) );

        });
        selected_handle_list = saved_selection.selection.selected_handle_list;

        saved_selection.selection.selected_blade_list.forEach(selection => {
            // console.log(selection);
            $("#blade-" + selection.ProductCountryId).click();
            $("#blade_qty_" + selection.ProductCountryId).val(selection.Quantity);
            // $("#blade_price_" + selection.ProductCountryId).html( (selection.Price * selection.Quantity).toFixed(2) );
        });
        selected_blade_list = saved_selection.selection.selected_blade_list;

        saved_selection.selection.selected_addon_list.forEach(selection => {
            // console.log(selection);
            $("#addon-" + selection.ProductCountryId).click();
            $("#addon_qty_" + selection.ProductCountryId).val(selection.Quantity);
            // $("#addon_price_" + selection.ProductCountryId).html( (selection.Price * selection.Quantity).toFixed(2) );
        });
        selected_addon_list = saved_selection.selection.selected_addon_list;

        saved_selection.selection.selected_ask_list.forEach(selection => {
            // console.log(selection);
            $("#ask-" + selection.ProductCountryId).click();
            $("#ask_qty_" + selection.ProductCountryId).val(selection.Quantity);
            // $("#ask_price_" + selection.ProductCountryId).html( (selection.Price * selection.Quantity).toFixed(2) );
        });
        selected_ask_list = saved_selection.selection.selected_ask_list;
    }

    GetTotalPrice();

    // if (saved_selection.selection.selected_handle != null) {
    //      var handle = saved_selection.selection.selected_handle
    //     $("#handle-" + handle.ProductCountryId).click();
    //     $("#handle_qty_" + handle.ProductCountryId).val(handle.SelectedQuantity);
    //     $("#handle_price_" + handle.ProductCountryId).html(handle.SelectedPrice);

    //     selected_handle.order_qty = handle.SelectedQuantity ;
    // }
    // if (saved_selection.selection.selected_blade != null) {
    //     var blade = saved_selection.selection.selected_blade
    //     $("#blade-" + blade.ProductCountryId).click();
    //     $("#blade_qty_" + blade.ProductCountryId).val(blade.SelectedQuantity);
    //     $("#blade_price_" + blade.ProductCountryId).html(blade.SelectedPrice);

    //     selected_blade.order_qty = blade.SelectedQuantity ;
    // }
    // if (saved_selection.selection.selected_addon != null) {
    //     var addon = saved_selection.selection.selected_addon
    //     $("#addon-" + addon.ProductCountryId).click();
    //     $("#addon_qty_" + addon.ProductCountryId).val(addon.SelectedQuantity);
    //     $("#addon_price_" + addon.ProductCountryId).html(addon.SelectedPrice);

    //     selected_addon.order_qty = addon.SelectedQuantity ;
    // }
    // if (saved_selection.selection.selected_ask != null) {
    //     var ask = saved_selection.selection.selected_ask
    //     $("#ask-" + ask.ProductCountryId).click();
    //     $("#ask_qty_" + ask.ProductCountryId).val(ask.SelectedQuantity);
    //     $("#ask_price_" + ask.ProductCountryId).html(ask.SelectedPrice);

    //     selected_ask.order_qty = ask.SelectedQuantity ;
    // }

    if (window.location.hash === "#handle") {
        UpdateStep(1);
    }
    else if (window.location.hash === "#blade") {
        UpdateStep(2);
    }
    else if (window.location.hash === "#addon") {
        UpdateStep(3);
    }
    else if (window.location.hash === "#ask") {
        UpdateStep(4);
    }
    else {
        UpdateStep(1);
    }

    //On click next
    $(".button-next").click(function () {
        current_step++;
        UpdateStep(current_step);
    });

    $(".button-proceed-checkout").click(function() {
        UpdateSessionData(true);
    });

    $("#collapse1").on('shown.bs.collapse', function () {
        UpdateStep(1);
    });

    $("#collapse2").on('shown.bs.collapse', function () {
        UpdateStep(2);
    });

    $("#collapse3").on('shown.bs.collapse', function () {
        UpdateStep(3);
    });

    $("#collapse4").on('shown.bs.collapse', function () {
        UpdateStep(4);
    });
});
