
$(document).ready(function () {
    $("#edit-addresses-block").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: GLOBAL_URL + '/delivery-address/edit/' + user_id,
            data: $(this).serialize(),
            cache: false,
            success: function (data) {

                var x = document.getElementById("appendAddress");

                x.innerHTML = '';
                x.innerHTML = '<label style="font-weight: bold">Address</label><br>' +
                    '<p style="margin-bottom:0;">' + data.delivery_address.address + '</p>' +
                    '<p style="margin-bottom:0;">' + data.delivery_address.portalCode + ', ' + data.delivery_address.city + ', ' + data.delivery_address.state + '</p>' +
                    '<p style="margin-bottom:0;">' + data.delivery_address.contactNumber + '</p><br>';
                if (data["delivery_address"] && data["billing_address"]) {
                    var dfn = document.getElementById("edit_delivery_name");
                    dfn.value = data["delivery_address"]["fullName"];

                    var dda = document.getElementById("edit_delivery_address");
                    if (country_id == 'kr') {
                        var getdda = data["delivery_address"]["address"];
                        dda.value = data["delivery_address"]["address"];
                        var dda1 = document.getElementById("edit_delivery_address1");
                        var ddf = document.getElementById("edit_delivery_flat");
                        if (getdda.includes(",")) {
                            var splitdda = getdda.split(",");
                            dda1.value = splitdda[0];
                            ddf.value = splitdda[1];
                        } else {
                            dda1.value = getdda;
                        }
                    }

                    var ddc = document.getElementById("edit_delivery_city");
                    ddc.value = data["delivery_address"]["city"];

                    var dds = document.getElementById("edit_delivery_state");
                    dds.value = data["delivery_address"]["state"];

                    var dpc = document.getElementById("edit_delivery_postcode");
                    dpc.value = data["delivery_address"]["portalCode"];

                    var ddp = document.getElementById("edit_delivery_phone");
                    ddp.value = data["delivery_address"]["contactNumber"];

                    var bba = document.getElementById("edit_billing_address");
                    if (country_id == 'kr') {
                        var getbba = data["billing_address"]["address"];
                        bba.value = data["billing_address"]["address"];
                        var bba1 = document.getElementById("edit_billing_address1");
                        var bbf = document.getElementById("edit_billing_flat");
                        if (getbba.includes(",")) {
                            var splitbba = getbba.split(",");
                            bba1.value = splitbba[0];
                            bbf.value = splitbba[1];
                        } else {
                            bba1.value = getbba;
                        }
                    }
                    else {
                        bba.value = data["billing_address"]["address"];
                    }

                    var bbc = document.getElementById("edit_billing_city");
                    bbc.value = data["billing_address"]["city"];

                    var bbs = document.getElementById("edit_billing_state");
                    bbs.value = data["billing_address"]["state"];

                    var bpc = document.getElementById("edit_billing_postcode");
                    bpc.value = data["billing_address"]["portalCode"];

                    var bbp = document.getElementById("edit_billing_phone");
                    bbp.value = data["billing_address"]["contactNumber"];

                    var edb = document.getElementById("enableBillingEditCheckBox");
                    if (edb) {
                        if (edb.checked) {
                            if (data["diff"] == 0) {
                                edb.click();
                            } else {

                            }
                        } else {
                            if (data["diff"] == 0) {

                            } else {
                                edb.click();
                            }
                        }
                    }
                }
                cancelEditAddress();
            },
            error: function () {

            }
        });
    });

    $("#add-addresses-block").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: GLOBAL_URL + '/delivery-address/add',
            data: $(this).serialize(),
            cache: false,
            success: function (data) {

                var x = document.getElementById("shipping-address-show");
                var e = document.getElementById("enable-address-edit2");
                var a = document.getElementById("enable-address-add");
                e.style.visibility = "visible";


                a.style.visibility = "hidden";

                x.innerHTML = '';
                if (data["delivery_address"]) {
                    x.innerHTML = ' <div class="panel-body"><div class="box-white" id="appendAddress"><label style="font-weight: bold">Address</label><br>' +
                        '<p style="margin-bottom:0;">' + data["delivery_address"][0]["address"] + '</p>' +
                        '<p style="margin-bottom:0;">' + data["delivery_address"][0]["portalCode"] + ', ' + data["delivery_address"][0]["city"] + ', ' + data["delivery_address"][0]["state"] + '</p>' +
                        '<p style="margin-bottom:0;">' + data["delivery_address"][0]["contactNumber"] + '</p><br> </div></div>';
                }
                if (data["delivery_address"] && data["billing_address"]) {
                    var dfn = document.getElementById("edit_delivery_name");
                    dfn.value = data["delivery_address"][0]["fullName"];
                    var dda = document.getElementById("edit_delivery_address");
                    if (country_id == 'kr') {
                        var getdda = data["delivery_address"][0]["address"];
                        dda.value = data["delivery_address"][0]["address"];
                        var dda1 = document.getElementById("edit_delivery_address1");
                        var ddf = document.getElementById("edit_delivery_flat");
                        if (getdda.includes(",")) {
                            var splitdda = getdda.split(",");
                            dda1.value = splitdda[0];
                            ddf.value = splitdda[1];
                        } else {
                            dda1.value = getdda;
                        }
                    }
                    else {
                        dda.value = data["delivery_address"][0]["address"];
                    }
                    var ddc = document.getElementById("edit_delivery_city");
                    ddc.value = data["delivery_address"][0]["city"];

                    var dds = document.getElementById("edit_delivery_state");
                    dds.value = data["delivery_address"][0]["state"];

                    var dpc = document.getElementById("edit_delivery_postcode");
                    dpc.value = data["delivery_address"][0]["portalCode"];

                    var ddp = document.getElementById("edit_delivery_phone");
                    ddp.value = data["delivery_address"][0]["contactNumber"];

                    var bba = document.getElementById("edit_billing_address");
                    if (country_id == 'kr') {
                        var getbba = data["billing_address"][0]["address"];
                        bba.value = data["billing_address"][0]["address"];
                        var bba1 = document.getElementById("edit_billing_address1");
                        var bbf = document.getElementById("edit_billing_flat");
                        if (getbba.includes(",")) {
                            var splitbba = getbba.split(",");
                            bba1.value = splitbba[0];
                            bbf.value = splitbba[1];
                        } else {
                            bba1.value = getbba;
                        }
                    }
                    else {
                        bba.value = data["billing_address"][0]["address"];
                    }


                    var bbc = document.getElementById("edit_billing_city");
                    bbc.value = data["billing_address"][0]["city"];

                    var bbs = document.getElementById("edit_billing_state");
                    bbs.value = data["billing_address"][0]["state"];

                    var bpc = document.getElementById("edit_billing_postcode");
                    bpc.value = data["billing_address"][0]["portalCode"];

                    var bbp = document.getElementById("edit_billing_phone");
                    bbp.value = data["billing_address"][0]["contactNumber"];

                    document.getElementById("enableBillingEditCheckBox").click();
                }


                var x = document.getElementById("shipping-address-show");
                var y = document.getElementById("shipping-address-edit");
                var z = document.getElementById("enable-address-edit");
                var v = document.getElementById("enable-address-edit2");
                var v2 = document.getElementById("enable-address-add")
                var a = document.getElementById("shipping-address-add-edit");
                var a2 = document.getElementById("shipping-address-add");
                var z2 = document.getElementById("cancel-address-edit");
                x.style.display = "block";
                a.style.display = "none";
                v.style.display = "block";
                a2.style.display = "none";
                v2.style.display = "none";
                z2.style.display = "none";

            },
            error: function () {

            }
        });
    });
});



function shippingaddress() {
    var x = document.getElementById("account-body");
    var y = document.getElementById("shipping-address-body");
    var z = document.getElementById("payment-body");
    if (y.offsetParent !== null) {
        x.style.display = "none";
        z.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
    }
}

function editAddress() {
    var x = document.getElementById("shipping-address-show");
    var y = document.getElementById("shipping-address-edit");
    var z = document.getElementById("enable-address-edit");
    var z2 = document.getElementById("cancel-address-edit");
    z.disabled = true;
    z2.disabled = false;


    var ea = document.getElementById("btn_edit_address");
    var aa = document.getElementById("btn_add_address");
    if (ea) {
        ea.disabled = false;
    }
    if (aa) {
        aa.disabled = false;
    }
    if (x.offsetParent !== null) {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
        z2.style.display = "block";
    } else {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
        z2.style.display = "none";
    }
}





function cancelEditAddress() {
    var x = document.getElementById("shipping-address-show");
    var y = document.getElementById("shipping-address-edit");
    var z = document.getElementById("enable-address-edit");
    var v = document.getElementById("enable-address-edit2");
    var v2 = document.getElementById("enable-address-add")
    var a = document.getElementById("shipping-address-add-edit");
    var a2 = document.getElementById("shipping-address-add");
    var z2 = document.getElementById("cancel-address-edit");

    z2.disabled = true;
    if (z) {
        z.disabled = false;
    }
    if (v) {
        v.disabled = false;
    }
    if (v2) {
        v2.disabled = false;
    }

    var ea = document.getElementById("btn_edit_address");
    var aa = document.getElementById("btn_add_address");
    if (ea) {
        ea.disabled = false;
    }
    if (aa) {
        aa.disabled = false;
    }

    var checkthis = document.getElementById('enable-address-edit2')
    if (checkthis) {

        if ($('#enable-address-edit2').css('visibility') === 'hidden') {
            if (x.offsetParent !== null) {
                x.style.display = "none";
                a2.style.display = "block";
                v2.style.display = "none";
                a.style.display = "none";
                v.style.display = "none";
                z2.style.display = "block";
            } else {
                x.style.display = "block";
                a2.style.display = "none";
                v2.style.display = "block";
                a.style.display = "none";
                v.style.display = "none";
                z2.style.display = "none";
            }
        } else {
            if (x.offsetParent !== null) {
                x.style.display = "none";
                a.style.display = "block";
                v.style.display = "block";
                a2.style.display = "none";
                v2.style.display = "none";
                z2.style.display = "block";
            } else {
                x.style.display = "block";
                a.style.display = "none";
                v.style.display = "block";
                a2.style.display = "none";
                v2.style.display = "none";
                z2.style.display = "none";
            }

        }
    } else {
        if (x.offsetParent !== null) {
            x.style.display = "none";
            y.style.display = "block";
            z.style.display = "block";
            z2.style.display = "block";
        } else {
            x.style.display = "block";
            y.style.display = "none";
            z.style.display = "block";
            z2.style.display = "none";
        }
    }
}

function editAddress2() {
    var x = document.getElementById("shipping-address-show");
    var y = document.getElementById("shipping-address-add-edit");
    var z = document.getElementById("enable-address-edit2");
    var z2 = document.getElementById("cancel-address-edit");

    z.disabled = true;

    z2.disabled = false;

    var ea = document.getElementById("btn_edit_address");
    var aa = document.getElementById("btn_add_address");
    if (ea) {
        ea.disabled = false;
    }
    if (aa) {
        aa.disabled = false;
    }

    if (x.offsetParent !== null) {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
        z2.style.display = "block";
    } else {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
        z2.style.display = "none";
    }
}

function addAddress() {
    var x = document.getElementById("shipping-address-show");
    var y = document.getElementById("shipping-address-add");
    var z = document.getElementById("enable-address-add");
    var z2 = document.getElementById("cancel-address-edit");

    z.disabled = true;
    z2.disabled = false;

    var ea = document.getElementById("btn_edit_address");
    var aa = document.getElementById("btn_add_address");
    if (ea) {
        ea.disabled = false;
    }
    if (aa) {
        aa.disabled = false;
    }

    if (y.offsetParent !== null) {
        y.style.display = "none";
        z.style.display = "block";
        z2.style.display = "none";
        x.style.display = "block";
    } else {
        y.style.display = "block";
        z.style.display = "none";
        z2.style.display = "block";
        x.style.display = "none";
    }
}


function enableBilling() {
    var x = document.getElementById("billing-address-block");
    if (x.offsetParent !== null) {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

function enableBilling1() {
    var x = document.getElementById("billing-address-block1");
    if (x.offsetParent !== null) {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

