let saved_session_data = {};
let current_step = 1;
let productid;
let productcountryId;
let quantity;
let journey_type;
let current_session_data;
let _selection = [];
let _reinitialize_selection = [];
let _reinitialize_checkout = [];
let session_data_for_selection = {};
let session_data_for_checkout = {};
let isfirstclick_selection = false;
let isfirstclick_checkout = false;
let get_selection_session;
let get_checkout_session;
let total_price = 0.0;

let element_collapse_1 = document.getElementById("collapse1");
let element_collapse_2 = document.getElementById("collapse2");
let element_collapse_3 = document.getElementById("collapse3");
let element_collapse_4 = document.getElementById("collapse4");
let element_main_4 = document.getElementById("account-header");
let element_main_5 = document.getElementById("shipping-address-header");
let element_main_6 = document.getElementById("payment-header");
let element_1 = document.getElementById("shipping-address-show");
let element_2 = document.getElementById("shipping-address-edit");
let element_3 = document.getElementById("shipping-address-add");
let element_4 = document.getElementById("enable-address-edit");
let element_5 = document.getElementById("enable-address-add");
let element_6 = document.getElementById("cancel-address-edit");
let element_7 = document.getElementById("billing-address-block_for_edit");
let element_7_1 = document.getElementById("billing-address-block_for_add");
let element_11 = document.getElementById("enableBillingEditCheckBox_for_edit");
let element_11_1 = document.getElementById("enableBillingEditCheckBox_for_add");
let element_12 = document.getElementById("delivery_address_id");
let element_13 = document.getElementById("billing_address_id");
let element_16 = document.getElementById("appendAddress");
let element_17 = document.getElementById("product_country_id");

//input for add & edit addresses
let add_delivery_name = document.getElementById("add_delivery_name");
let add_SSN = document.getElementById("add_SSN");
let add_delivery_address = document.getElementById("add_delivery_address");
let add_delivery_city = document.getElementById("add_delivery_city");
let add_delivery_state = document.getElementById("add_delivery_state");
let add_delivery_postcode = document.getElementById("add_delivery_postcode");
let add_delivery_phone = document.getElementById("add_delivery_phone");
let add_delivery_phoneext = document.getElementById("add_delivery_phoneext");

let add_billing_address = document.getElementById("add_billing_address");
let add_billing_city = document.getElementById("add_billing_city");
let add_billing_state = document.getElementById("add_billing_state");
let add_billing_postcode = document.getElementById("add_billing_postcode");
let add_billing_phone = document.getElementById("add_billing_phone");
let add_billing_phoneext = document.getElementById("add_billing_phoneext");

let edit_delivery_name = document.getElementById("edit_delivery_name");
let edit_SSN = document.getElementById("edit_SSN");
let edit_delivery_address = document.getElementById("edit_delivery_address");
let edit_delivery_city = document.getElementById("edit_delivery_city");
let edit_delivery_state = document.getElementById("edit_delivery_state");
let edit_delivery_postcode = document.getElementById("edit_delivery_postcode");
let edit_delivery_phone = document.getElementById("edit_delivery_phone");
let edit_delivery_phoneext = document.getElementById("edit_delivery_phoneext");

let edit_billing_address = document.getElementById("edit_billing_address");
let edit_billing_city = document.getElementById("edit_billing_city");
let edit_billing_state = document.getElementById("edit_billing_state");
let edit_billing_postcode = document.getElementById("edit_billing_postcode");
let edit_billing_phone = document.getElementById("edit_billing_phone");
let edit_billing_phoneext = document.getElementById("edit_billing_phoneext");

// korean address -------------
let add_delivery_name1 = document.getElementById("add_delivery_name1");
let add_delivery_address1 = document.getElementById("add_delivery_address1");
let add_delivery_city1 = document.getElementById("add_delivery_city1");
let add_delivery_state1 = document.getElementById("add_delivery_state1");
let add_delivery_postcode1 = document.getElementById("add_delivery_postcode1");
let add_delivery_phone1 = document.getElementById("add_delivery_phone1");
let add_delivery_phoneext1 = document.getElementById("add_delivery_phoneext1");

let add_billing_address1 = document.getElementById("add_billing_address1");
let add_billing_city1 = document.getElementById("add_billing_city1");
let add_billing_state1 = document.getElementById("add_billing_state1");
let add_billing_postcode1 = document.getElementById("add_billing_postcode1");
let add_billing_phone1 = document.getElementById("add_billing_phone1");
let add_billing_phoneext1 = document.getElementById("add_billing_phoneext1");

let edit_delivery_name1 = document.getElementById("edit_delivery_name1");
let edit_delivery_address1 = document.getElementById("edit_delivery_address1");
let edit_delivery_city1 = document.getElementById("edit_delivery_city1");
let edit_delivery_state1 = document.getElementById("edit_delivery_state1");
let edit_delivery_postcode1 = document.getElementById(
    "edit_delivery_postcode1"
);
let edit_delivery_phone1 = document.getElementById("edit_delivery_phone1");
let edit_delivery_phoneext1 = document.getElementById("edit_delivery_phoneext1");

let edit_billing_address1 = document.getElementById("edit_billing_address1");
let edit_billing_city1 = document.getElementById("edit_billing_city1");
let edit_billing_state1 = document.getElementById("edit_billing_state1");
let edit_billing_postcode1 = document.getElementById("edit_billing_postcode1");
let edit_billing_phone1 = document.getElementById("edit_billing_phone1");
let edit_billing_phoneext1 = document.getElementById("edit_billing_phoneext1");

async function CheckSessionTKData() {
    var data = await session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null);
    if (data) {
        return JSON.parse(data);
    } else {
        return data;
    }
}

// Function: Get session data
function GetSessionData() {
    let sessionABC = [];
    session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null).done(function (
        data
    ) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            return data;
        } else {
        }
    });
    return sessionABC;
}

// Function: Get session data
function GetSessionDataV2() {
    let sessionABC = [];
    session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null).done(function (
        data
    ) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            sessionABC[0] = get_checkout_session;
        } else {
        }
    });
    return sessionABC;
}

function onUpdateDeliverySelection(addressUpdates, type) {
    if (type === "default") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
    if (type === "update") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        $("#shipping-address-add").hide();
        $("#shipping-address-edit").show();
        AddressBack(type);
    }
    if (type === "address_update_only") {
        let checkout_data_address = {
            journey_type: "checkout",
            update_type: "selected_address",
            delivery_address_id: addressUpdates.selected_delivery_address_id
                ? addressUpdates.selected_delivery_address_id
                : null,
            billing_address_id: addressUpdates.selected_billing_address_id
                ? addressUpdates.selected_billing_address_id
                : null
        };
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
}

// Function: Update session data
function UpdateSessionData(data) {
    // session selection
    let _session_selection = {};
    let _session_selection_step1_add_products = {};

    // session checkout
    let _session_checkout = {};
    let _session_checkout_user = {};
    let _session_checkout_promo = {};
    let _session_checkout_selected_delivery_address = {};
    let _session_checkout_event = {};
    let _session_checkout_summary = {};

    var journey_type = data.journey_type;
    if (journey_type === "selection") {
        var product_country_id = data.productcountryId.toString();
        var quantity = data.quantity;
        var product_type = data.product_type;

        if (!isfirstclick_selection) {
            isfirstclick_selection = true;
            _session_selection = {
                selection: {
                    step1: [
                        {
                            productcountryid: product_country_id,
                            quantity: quantity
                        }
                    ],
                    current_step: { current_step },
                    journey_type: { journey_type },
                    product_type: { product_type }
                }
            };
            session_data_for_selection = _session_selection;
        } else {
            _session_selection_step1_add_products = {
                productcountryid: product_country_id,
                quantity: quantity
            };
            session_data_for_selection.selection.step1.push(
                _session_selection_step1_add_products
            );
        }

        return session(
            SESSION_SELECTION_ALACARTE_BA,
            SESSION_SET,
            JSON.stringify(session_data_for_selection)
        ).done(function () { return true; });
    } else if (journey_type === "checkout") {
        if (isfirstclick_checkout === false) {
            isfirstclick_checkout = true;
            _session_checkout = {
                checkout: {
                    user: {},
                    selected_address: {},
                    promo: {},
                    event: {},
                    summary: {},
                    payment_data: {}
                }
            };
            session_data_for_checkout = _session_checkout;

            // // console.log(data);
            if (data.update_type === "default") {
                if (data.user && data.user.length !== 0) {
                    _session_checkout_user = {
                        id: data.user.id,
                        email: data.user.email,
                        defaultLanguage: data.user.defaultLanguage,
                        IsActive: data.user.isActive,
                        CountryId: data.user.CountryId
                    };
                    session_data_for_checkout.checkout.user = _session_checkout_user;
                }

                if (
                    data.delivery_address &&
                    data.delivery_address.length !== 0
                ) {
                    _session_checkout_selected_delivery_address = {
                        delivery_address: data.delivery_address
                            ? data.delivery_address.id
                            : "",
                        billing_address: data.billing_address
                            ? data.billing_address.id
                            : ""
                    };
                    session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                }

                if (
                    data.session_checkout.event &&
                    data.session_checkout.event.length !== 0
                ) {
                    _session_checkout_event = data.session_checkout.event;
                    session_data_for_checkout.checkout.event = _session_checkout_event;
                }

                if (
                    data.session_checkout.promo &&
                    data.session_checkout.promo.length !== 0
                ) {
                    _session_checkout_promo = data.session_checkout.promo;
                    session_data_for_checkout.checkout.promo = _session_checkout_promo;
                }

                if (
                    data.session_checkout.summary &&
                    data.session_checkout.summary.length !== 0
                ) {
                    _session_checkout_summary = data.session_checkout.summary;
                    session_data_for_checkout.checkout.summary = _session_checkout_summary;
                }

                if (data.checkout_details.length !== 0) {
                    _session_checkout_payment_data = data.checkout_details;
                    session_data_for_checkout.checkout.payment_data = _session_checkout_payment_data;
                }
            }

            if (data.update_type === "selected_user") {
                _session_checkout_user = { test: "test" };
                session_data_for_checkout.checkout.user.push(
                    _session_checkout_user
                );
            }
            if (data.update_type === "selected_promo") {
                if (data.status === "error") {
                    // // console.log(data);
                    _session_checkout_promo = {};
                } else {
                    _session_checkout_promo = {
                        promo_id: data.data.pid,
                        promo_code: data.data.code,
                        promo_isGeneric: data.data.isGeneric,
                        promo_discount: data.data.discount,
                        promo_free_product_id: data.data.freeProductCountryIds,
                        promo_free_exist_product_id: data.data.freeExistProductCountryIds,
                        promo_ablefreeexistproduct: data.ablefreeexistproduct  ? data.ablefreeexistproduct : '0',
                        promo_freeexistresultproduct: data.freeexistresultproduct  ? data.freeexistresultproduct : '',
                        promo_freeexistresultproductprice: data.freeexistresultproductprice,
                        promo_product_bundle_id: data.data.ProductBundleId,
                        promo_promotionType: data.data.promotionType,
                        promo_minSpend: data.data.minSpend,
                        promo_maxDiscount: data.data.maxDiscount,
                        promo_isFreeShipping: data.data.isFreeShipping,
                        promo_timePerUser: data.data.timePerUser,
                        promo_total_price: data.total,
                        promo_next_total_price: data.ntotal
                    };
                }
                session_data_for_checkout.checkout.promo = _session_checkout_promo;
            }
            if (data.update_type === "selected_address") {
                _session_checkout_selected_delivery_address = {
                    delivery_address: data.delivery_address_id,
                    billing_address: data.billing_address_id
                };
                session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
            }
            if (data.update_type === "selected_card") {
                _session_checkout_selected_card = {
                    id: data.card.id,
                    payment_intent_id: data.payment_intent_id,
                    customer_id: data.card.customerId,
                    type: data.card.payment_method,
                    risk_level: data.card.risk_level
                };
                session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
            }
            if (data.update_type === "update_event") {
                _session_checkout_event = {
                    ba_channel_type: data.event.ba_channel_type,
                    ba_event_location_code: data.event.ba_event_location_code
                };
                session_data_for_checkout.checkout.event = _session_checkout_event;
            }
            if (data.update_type === "isDirectTrial") {
                _session_checkout_summary = {
                    isDirectTrial: data.isDirectTrial
                };
                session_data_for_checkout.checkout.summary = _session_checkout_summary;
            }

            return session(
                SESSION_CHECKOUT_ALACARTE_BA,
                SESSION_SET,
                JSON.stringify(session_data_for_checkout)
            ).done(function () {
                session_checkout_data = session_data_for_checkout;
                $("#loading").css("display", "none");
                currentSession();
                return true;
            });
        } else {
            return session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null).done(
                function (response) {
                    // Do something with the session data
                    if (response) {
                        get_checkout_session = response;
                        session_data_for_checkout = JSON.parse(response);
                        if (data.update_type === "selected_user") {
                            _session_checkout_user = { test: "test" };
                            session_data_for_checkout.checkout.user.push(
                                _session_checkout_user
                            );
                        }
                        if (data.update_type === "selected_promo") {
                            if (data.status === "error") {
                                _session_checkout_promo = {};
                            } else {
                                _session_checkout_promo = {
                                    promo_id: data.data.pid,
                                    promo_code: data.data.code,
                                    promo_isGeneric: data.data.isGeneric,
                                    promo_discount: data.data.discount,
                                    promo_free_product_id: data.data.freeProductCountryIds,
                                    promo_free_exist_product_id: data.data.freeExistProductCountryIds,
                                    promo_ablefreeexistproduct: data.ablefreeexistproduct  ? data.ablefreeexistproduct : '0',
                                    promo_freeexistresultproduct: data.freeexistresultproduct  ? data.freeexistresultproduct : '',
                                    promo_freeexistresultproductprice:data.freeexistresultproductprice,
                                    promo_product_bundle_id:data.data.ProductBundleId,
                                    promo_promotionType:data.data.promotionType,
                                    promo_planIds: data.data.planIds,
                                    promo_minSpend: data.data.minSpend,
                                    promo_maxDiscount: data.data.maxDiscount,
                                    promo_isFreeShipping: data.data.isFreeShipping,
                                    promo_timePerUser: data.data.timePerUser,
                                    promo_total_price: data.total,
                                    promo_next_total_price: data.ntotal
                                };
                            }
                            session_data_for_checkout.checkout.promo = _session_checkout_promo;
                        }
                        if (data.update_type === "selected_address") {
                            _session_checkout_selected_delivery_address = {
                                delivery_address: data.delivery_address_id,
                                billing_address: data.billing_address_id
                            };
                            session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                        }
                        if (data.update_type === "update_event") {
                            _session_checkout_event = {
                                ba_channel_type: data.event.ba_channel_type,
                                ba_event_location_code:
                                    data.event.ba_event_location_code
                            };
                            session_data_for_checkout.checkout.event = _session_checkout_event;
                        }
                        if (data.update_type === "isDirectTrial") {
                            _session_checkout_summary = {
                                isDirectTrial: data.isDirectTrial
                            };
                            session_data_for_checkout.checkout.summary = _session_checkout_summary;
                        }
                        return session(
                            SESSION_CHECKOUT_ALACARTE_BA,
                            SESSION_SET,
                            JSON.stringify(session_data_for_checkout)
                        ).done(function () {
                            session_checkout_data = session_data_for_checkout;
                            $("#loading").css("display", "none");
                            currentSession();
                            return true;
                        });
                    } else {
                        return true;
                    }
                }
            );
        }
    }
}

async function currentSession() {
    // // console.log("currentSession start");
    let result = await GetSessionDataV2();
    // // console.log(result);
}

// Function: get session data for checkout
function setSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_SET, null).done(function () {
        
    });
}

// Function: get session data for checkout
function getSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null).done(function (
        response
    ) {
        // console.log(response);
    });
}

// Function: Clear session data
function ClearSessionData_CHECKOUT() {
    return session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_CLEAR, null).done(function(response) {
        if (response) {
            return response;
        }
    });
}

async function ClearUpdateSessionData_CHECKOUT(data) {
    var clear = await session(
        SESSION_CHECKOUT_ALACARTE_BA,
        SESSION_CLEAR,
        null
    );
    // // console.log("ClearSession: " + clear);
    if (clear === "success") {
        return UpdateSessionData(data);
    }
}

//Function: Update step
function UpdateStep(current_step) {
    if (current_step == 1) {
        $("#button-next").text("CONFIRM ACCOUNT");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "none";
        }

        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 2) {
        $("#button-next").text(confirmsatext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "none";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 3) {
        // trigger click add/edit address submit
        $("#button-next").text(confirmpaymenttext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 4) {
        $("#button-next").text(confirmeventtext);
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 5) {
        $("#button-next").text("CONFIRM EVENT LOCATION");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
    if (current_step == 6) {
        $("#button-next").text("PROCEED TO PAY");
        if (country_id == "kr") {
            let formpaykr = document.getElementById("form-pay-kr");
            formpaykr.style.display = "block";
        }
        $(".step1-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step1-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step2-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step2-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step3-heading")
            .removeClass("panel-heading-selected")
            .addClass("panel-heading-unselected");
        $(".step3-title")
            .removeClass("panel-title-selected")
            .addClass("panel-title-unselected");
        $(".step" + current_step + "-heading")
            .removeClass("panel-heading-unselected")
            .addClass("panel-heading-selected");
        $(".step" + current_step + "-title")
            .removeClass("panel-title-unselected")
            .addClass("panel-title-selected");
    }
}

function steps() {
    //On click next
    $("#button-next").click(function () {
        // // console.log("current_step", current_step);
        if (current_step == 6) {
            //do nothing
        } else {
            current_step++;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            if (current_step == 2) {
                shippingaddress();
                UpdateStep(current_step);
            }
            if (current_step == 3) {
                if (!$("#add-addresses-block" + name).length == 0) {
                    if ($("#add-addresses-block").valid() === true) {
                        $("#add-addresses-block").submit();
                        payment();
                        UpdateStep(current_step);
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text("CONFIRM SHIPPING ADDRESS");
                    }
                }
                if (!$("#edit-addresses-block" + name).length == 0) {
                    if ($("#edit-addresses-block").valid() === true) {
                        $("#edit-addresses-block").trigger("submit");
                        payment();
                        UpdateStep(current_step);
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text("CONFIRM SHIPPING ADDRESS");
                    }
                }
            }
            if (current_step == 4) {
                paymentSummary();
                UpdateStep(current_step);
            }
            if (current_step == 5) {
                if (!$("#event_location_form" + name).length == 0) {
                    validateEvents("event_location_form");
                    if ($("#event_location_form").valid() === true) {
                        eventLocation();
                        UpdateStep(current_step);
                        $("#button-next").addClass("display-none");
                        $("#form-pay").removeClass("hidden");
                        $("#form-pay-kr").removeClass("hidden");
                    } else {
                        current_step--;
                        UpdateStep(current_step);
                        $("#button-next").text("CONFIRM EVENT LOCATION");
                        $("#button-next").removeClass("display-none");
                        $("#form-pay").addClass("hidden");
                        $("#form-pay-kr").addClass("hidden");
                    }
                }
            }

            UpdateStep(current_step);
        }
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step == 1) {
            let selection_url =
            window.location.origin +
            GLOBAL_URL +
            "/alacarte/select-payment-method";
            window.location = selection_url;
        } else {
            current_step--;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            if (current_step == 2) {
                shippingaddress();
                UpdateStep(current_step);
            }
            if (current_step == 3) {
                payment();
                UpdateStep(current_step);
            }
        }
    });
}

// Begin: Functions related to shipping address
function account() {
    current_step = 1;
    $("#button-next").removeClass("display-none");
    //$("#form-pay").addClass('display-none');
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "none";
    }
    if (document.getElementById("collapse1").offsetParent !== null) {
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.add("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function shippingaddress() {
    // // console.log("shippingaddress");
    current_step = 2;
    $("#button-next").removeClass("display-none");
    //$("#form-pay").addClass('display-none');
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "none";
    }
    // // console.log(document.getElementById("collapse2").offsetParent !== null);
    if (document.getElementById("collapse2").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.add("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function editAddress() {
    if (
        document.getElementById("shipping-address-show").offsetParent !== null
    ) {
        document.getElementById("shipping-address-show").style.display = "none";

        if (document.getElementById("shipping-address-edit")) {
            document.getElementById("shipping-address-edit").style.display = "block";
    }else if(document.getElementById("shipping-address-add")) {
        document.getElementById("shipping-address-add").style.display = "block";
    }

        document.getElementById("enable-address-edit").style.display = "none";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "block";
    } else {
        document.getElementById("shipping-address-show").style.display = "block";

        if (document.getElementById("shipping-address-edit")) {
        document.getElementById("shipping-address-edit").style.display = "none";
    }else if(document.getElementById("shipping-address-add")) {
        document.getElementById("shipping-address-add").style.display = "none";
        }

        document.getElementById("enable-address-edit").style.display = "block";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "none";
    }
}

function onAddDeliveryAddress(data) {
    edit_delivery_name.value = data.delivery_address_fullname;
    if (edit_SSN) {
        edit_SSN.value = data.delivery_address_SSN;
    }
    edit_delivery_address.value = data.delivery_address_address;
    edit_delivery_city.value = data.delivery_address_city;
    edit_delivery_state.value = data.delivery_address_state;
    edit_delivery_postcode.value = data.delivery_address_portalCode;
    edit_delivery_phone.value = data.delivery_address_contactNumber.replace(("+" + userPhoneExt), "");
    edit_billing_address.value = data.billing_address_address;
    edit_billing_city.value = data.billing_address_city;
    edit_billing_state.value = data.billing_address_state;
    edit_billing_postcode.value = data.billing_address_portalCode;
    edit_billing_phone.value = data.billing_address_contactNumber.replace(("+" + userPhoneExt), "");

    if (edit_delivery_address1) {
        edit_delivery_address1.value = data.delivery_address_address;
    }
    if (edit_delivery_city1) {
        edit_delivery_city1.value = data.delivery_address_city;
    }
    if (edit_delivery_state1) {
        edit_delivery_state1.value = data.delivery_address_state;
    }
    if (edit_delivery_postcode1) {
        edit_delivery_postcode1.value = data.delivery_address_portalCode;
    }
    if (edit_delivery_phone1) {
        edit_delivery_phone1.value = data.delivery_address_contactNumber.replace(("+" + userPhoneExt), "");
    }
    if (edit_billing_address1) {
        edit_billing_address1.value = data.billing_address_address;
    }
    if (edit_billing_city1) {
        edit_billing_city1.value = data.billing_address_city;
    }
    if (edit_billing_state1) {
        edit_billing_state1.value = data.billing_address_state;
    }
    if (edit_billing_postcode1) {
        edit_billing_postcode1.value = data.billing_address_portalCode;
    }
    if (edit_billing_phone1) {
        edit_billing_phone1.value = data.billing_address_contactNumber.replace(("+" + userPhoneExt), "");
    }
}

function cancelEditAddress() {
    if (document.getElementById("enable-address-edit")) {
        if (
            document.getElementById("enable-address-edit").style.display ===
            "block"
        ) {
            if (
                document.getElementById("shipping-address-show")
                    .offsetParent !== null
            ) {
                document.getElementById("shipping-address-show").style.display =
                    "none";
                document.getElementById("shipping-address-edit").style.display =
                    "block";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "none";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "block";
            } else {
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "none";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "none";
            }
        } else {
            if (
                document.getElementById("shipping-address-show")
                    .offsetParent !== null
            ) {
                document.getElementById("shipping-address-show").style.display =
                    "none";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "block";
                document.getElementById("enable-address-add").style.display =
                    "none";
                document.getElementById("cancel-address-edit").style.display =
                    "block";
            } else {
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit").style.display =
                    "none";
                document.getElementById("shipping-address-add").style.display =
                    "none";
                document.getElementById("enable-address-edit").style.display =
                    "block";
                document.getElementById("enable-address-add").style.display =
                    "block";
                document.getElementById("cancel-address-edit").style.display =
                    "none";
            }
        }
    } else {
        if (
            document.getElementById("shipping-address-show").offsetParent !==
            null
        ) {
            document.getElementById("shipping-address-show").style.display =
                "none";
            document.getElementById("shipping-address-edit").style.display =
                "block";
            document.getElementById("shipping-address-add").style.display =
                "block";
            document.getElementById("enable-address-edit").style.display =
                "block";
            document.getElementById("enable-address-add").style.display =
                "block";
            document.getElementById("cancel-address-edit").style.display =
                "block";
        } else {
            document.getElementById("shipping-address-show").style.display =
                "block";
            document.getElementById("shipping-address-edit").style.display =
                "none";
            document.getElementById("shipping-address-add").style.display =
                "block";
            document.getElementById("enable-address-edit").style.display =
                "block";
            document.getElementById("enable-address-add").style.display =
                "block";
            document.getElementById("cancel-address-edit").style.display =
                "none";
        }
    }
}

function addAddress() {
    add_delivery_name.value = "";
    if (add_SSN) {
        add_SSN.value = "";
    }
    add_delivery_address.value = "";
    add_delivery_city.value = "";
    add_delivery_state.value = "";
    add_delivery_postcode.value = "";
    add_delivery_phone.value = "";
    add_billing_address.value = "";
    add_billing_city.value = "";
    add_billing_state.value = "";
    add_billing_postcode.value = "";
    add_billing_phone.value = "";

    if (document.getElementById("shipping-address-add").offsetParent !== null) {
        document.getElementById("shipping-address-add").style.display = "none";
        document.getElementById("enable-address-edit").style.display = "block";
        document.getElementById("enable-address-add").style.display = "block";
        document.getElementById("cancel-address-edit").style.display = "none";
        document.getElementById("shipping-address-show").style.display = "block";
    } else {
        document.getElementById("shipping-address-add").style.display = "block";
        document.getElementById("enable-address-edit").style.display = "none";
        document.getElementById("enable-address-add").style.display = "none";
        document.getElementById("cancel-address-edit").style.display = "block";
        document.getElementById("shipping-address-show").style.display = "none";
    }
}

function enableBilling() {
    if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_edit")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
        true
    ) {
        document.getElementById(
            "billing-address-block_for_edit"
        ).style.display = "block";
    } else if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_edit")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
        false
    ) {
        document.getElementById(
            "billing-address-block_for_edit"
        ).style.display = "none";
    }

    if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_add")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_add").checked == true
    ) {
        document.getElementById("billing-address-block_for_add").style.display = "block";
    } else if (
        document.body.contains(
            document.getElementById("enableBillingEditCheckBox_for_add")
        ) &&
        document.getElementById("enableBillingEditCheckBox_for_add").checked == false
    ) {
        document.getElementById("billing-address-block_for_add").style.display = "none";
    }
}
// End: Functions related to shipping address

function payment() {
    current_step = 3;
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse3").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.add("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function paymentSummary() {
    current_step = 4;
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse4").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse5").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.add("show");
        document.getElementById("collapse5").classList.remove("show");
    }
}

function eventLocation() {
    let channel_type = $("#ba_channel_type").val();
    let event_location_code = $("#ba_event_location_code").val();
    UpdateEvent(channel_type, event_location_code);

    current_step = 4;
    $("#button-next").addClass("display-none");
    $("#form-pay").removeClass("display-none");
    if (country_id == "kr") {
        let formpaykr = document.getElementById("form-pay-kr");
        formpaykr.style.display = "block";
    }
    if (document.getElementById("collapse5").offsetParent !== null) {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
    } else {
        document.getElementById("collapse1").classList.remove("show");
        document.getElementById("collapse2").classList.remove("show");
        document.getElementById("collapse3").classList.remove("show");
        document.getElementById("collapse4").classList.remove("show");
        document.getElementById("collapse5").classList.add("show");
    }
}
function AddressBack(type) {
    // // console.log("AddressBack type: " + type);
    if (type === "default") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent ==
                null
            ) {
                // // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }

            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }
        }
    }
    if (type === "update") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            } else {
                // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                document.getElementById("shipping-address-edit")
                    ? (document.getElementById(
                        "shipping-address-edit"
                    ).style.display = "none")
                    : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "block")
                    : null;
            }
        }
    }
    if (type === "address_update_only") {
        if (document.getElementById("shipping-address-show")) {
            if (
                document.getElementById("shipping-address-show").offsetParent !=
                null
            ) {
                // console.log("AddressBack 2 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                //document.getElementById("shipping-address-edit") ? document.getElementById("shipping-address-edit").style.display = "none" : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            } else {
                // console.log("AddressBack 1 ");
                document.getElementById("shipping-address-show").style.display =
                    "block";
                //document.getElementById("shipping-address-edit") ? document.getElementById("shipping-address-edit").style.display = "none" : null;
                document.getElementById("shipping-address-add")
                    ? (document.getElementById(
                        "shipping-address-add"
                    ).style.display = "none")
                    : null;
                document.getElementById("enable-address-edit")
                    ? (document.getElementById(
                        "enable-address-edit"
                    ).style.display = "block")
                    : null;
                document.getElementById("enable-address-add")
                    ? (document.getElementById(
                        "enable-address-add"
                    ).style.display = "block")
                    : null;
                document.getElementById("cancel-address-edit")
                    ? (document.getElementById(
                        "cancel-address-edit"
                    ).style.display = "none")
                    : null;
            }
        }
    }
}

function paymentKR() {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: GLOBAL_URL + "/shave-plans/checkout/nicepay/confirm-purchase",
        data: { type: "alacarte" },
        json: true,
        cache: false,
        success: function (data) {
            // console.log(data);
            if (data.status) {
                if (data.status == "success") {
                    window.location.href =
                        GLOBAL_URL + "/thankyou/" + data.orderid;
                }
            }
        },
        error: function (jqXHR) {
            $("#error-card").text(jqXHR.responseJSON.message);
        }
    });
}

// Promotion
function onApplyPromo(
    data,
    total,
    ntotal,
    ablefreeexistproduct,
    freeexistresultproduct,
    freeexistresultproductprice,
    status
) {
    let checkout_data_promo = {
        journey_type: "checkout",
        update_type: "selected_promo",
        data: data,
        total: total,
        ntotal: ntotal,
        ablefreeexistproduct: ablefreeexistproduct,
        freeexistresultproduct: freeexistresultproduct,
        freeexistresultproductprice: freeexistresultproductprice,
        status: status
    };
    UpdateSessionData(checkout_data_promo);
}

// Promotion
function applyPromotion() {
    var loading = document.getElementById("loading");
    if (loading) {
        loading.style.display = "block";
    }
    Promotion("web", "alacarte", "combinesession");
}

function UpdateEvent(channel_type, event_location_code) {
    let checkout_data_event = {
        journey_type: "checkout",
        update_type: "update_event",
        event: {
            ba_channel_type: channel_type,
            ba_event_location_code: event_location_code
        }
    };
    UpdateSessionData(checkout_data_event);
}

function UpdateIsDirectTrial(isDirectTrial) {
    let checkout_data_isdirecttrial = {
        journey_type: "checkout",
        update_type: "isDirectTrial",
        isDirectTrial: isDirectTrial,
    };

    return UpdateSessionData(checkout_data_isdirecttrial).done(function (response) {
        return response
    });
}

// on proceed payment button
function onProceedPayment(
    session_data,
    session_checkout_data,
    payment_intent,
    data
) {
    event_data = {
        ba_channel_type: $("#ba_channel_type").val(),
        ba_event_location_code: $("#ba_event_location_code").val()
    };

    let isDirectTrial = $("#ba_is_direct_trial").is(":checked");

    UpdateIsDirectTrial(isDirectTrial).done(function () {
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
        // console.log(event_data, isDirectTrial);

        // START CONFIRM
        $("#loading").css("display", "block");
        var type = "ala-carte";
        let CONFIRM_PURCHASE_URL =
            API_URL + "/ba/products/complete-purchase";
        $.ajax({
            url: CONFIRM_PURCHASE_URL,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            method: "POST",
            cache: false,
            data: {
                type: type,
                session_selection: JSON.parse(session_data),
                session_checkout: session_checkout_data,
                appType: "baWebsite",
                country_id: countryid,
                country_code: country_id,
                user_id: user_id,
                langCode: langCode,
                urllangCode: urllangCode,
                seller: window.localStorage.getItem("seller"),
                event_data: event_data,
                isDirectTrial: isDirectTrial
            }
        }).done(function (data) {
            // console.clear();
            if (data) {
                $.ajax({
                    headers: {
                        "X-CSRF-TOKEN": $(
                            'meta[name="csrf-token"]'
                        ).attr("content")
                    },
                    type: "POST",
                    url: GLOBAL_URL + "/order/success",
                    data: { data: data, id: data.orderid },
                    success: function (data) {
                        window.location.replace(data);
                        // $("#loading").css("display", "none");
                    }
                }).fail(function(jqXHR, textStatus, error) {
                    $("#loading").css("display", "none");
                    // console.log("Failed to confirm purchase.");
                });
            }else{
                $("#loading").css("display", "none");
            }
        }).fail(function(jqXHR, textStatus, error) {
            $("#loading").css("display", "none");
            // console.log("Failed to confirm purchase.");
        });
        // FINISH CONFIRM
    });
}

// ========================================================================================================
// CHECKOUT PAGE ON LOAD ==================================================================================
// ========================================================================================================
$(function () {
    ClearSessionData_CHECKOUT().then(function(response) {
        if (response) {

            // Check Current Steps and do following function
            steps();

            if (current_step == 1) {
                var link = document.getElementById("account-header");
                if (link) {
                    link.click();
                    $("#button-next").text("NEXT");
                }
            }

            if (document.getElementById("shipping-address-add")) {
                if (document.getElementById("shipping-address-add").offset !== null) {
                    document.getElementById("enable-address-edit").style.display =
                        "none";
                    document.getElementById("enable-address-add").style.display =
                        "none";
                    document.getElementById("cancel-address-edit").style.display =
                        "none";
                }
            }

            // Enable billing if check box is checked
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_edit")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
                true
            ) {
                document.getElementById("cancel-address-edit").style.display = "block";
            }
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_edit")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_edit").checked ==
                false
            ) {
                document.getElementById("cancel-address-edit").style.display = "none";
            }
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_add")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_add").checked ==
                true
            ) {
                document.getElementById("billing-address-block_for_add").style.display =
                    "block";
            }
            if (
                document.body.contains(
                    document.getElementById("enableBillingEditCheckBox_for_add")
                ) &&
                document.getElementById("enableBillingEditCheckBox_for_add").checked ==
                false
            ) {
                document.getElementById("billing-address-block_for_add").style.display =
                    "none";
            }

            // console.log(checkout_details);
            // console.log(user_id);
            // console.log(country_id);
            // console.log(user);
            // console.log(delivery_address);
            // console.log(billing_address);
            // console.log(session_data);

            // ADD All Default Data to Checkout Session
            if (
                !(
                    typeof checkout_details === "undefined" &&
                    typeof user_id === "undefined" &&
                    typeof country_id === "undefined" &&
                    typeof user === "undefined" &&
                    typeof default_delivery_address === "undefined" &&
                    typeof default_billing_address === "undefined" &&
                    typeof session_data === "undefined" &&
                    typeof saved_session_checkout === "undefined"
                )
            ) {
                let xe = checkout_details ? checkout_details : "";
                let xg = user_id ? user_id : "";
                let xh = country_id ? country_id : "";
                let xi = user ? user : "";
                let xj = default_delivery_address ? default_delivery_address : "";
                let xk = default_billing_address ? default_billing_address : "";
                let xm = session_data ? session_data : "";
                let xn = saved_session_checkout ? saved_session_checkout : "";

                if (xe && xg && xh && xi) {
                    let data = {
                        checkout_details: xe,
                        user_id: xg,
                        country_id: xh,
                        user: xi,
                        delivery_address: xj,
                        billing_address: xk,
                        session_data: xm,
                        session_checkout: xn,
                        journey_type: "checkout",
                        update_type: "default"
                    };

                    // console.log("default items", data);
                    UpdateSessionData(data).done(function() {
                        // Save default delivery & billing address to session checkout
                        if (
                            document.getElementById("delivery_address_id") &&
                            document.getElementById("billing_address_id")
                        ) {
                            if (
                                document.getElementById("delivery_address_id").value &&
                                document.getElementById("billing_address_id").value
                            ) {
                                let addressUpdates = {
                                    selected_delivery_address_id: document.getElementById(
                                        "delivery_address_id"
                                    ).value
                                        ? document.getElementById("delivery_address_id").value
                                        : "",
                                    selected_billing_address_id: document.getElementById(
                                        "billing_address_id"
                                    ).value
                                        ? document.getElementById("billing_address_id").value
                                        : ""
                                };

                                if (delivery_address[0].address !== billing_address[0].address) {
                                    $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                    document.getElementById("cancel-address-edit").style.display =
                                        "block";
                                } else {
                                    $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                    document.getElementById("cancel-address-edit").style.display =
                                        "none";
                                }

                                onUpdateDeliverySelection(addressUpdates, "address_update_only");
                            }
                        }
                    });
                }
            }
        }
    });

    v_checkout_e_address("edit-addresses-block");
    // Action: On submit edit address
    $("#edit-addresses-block").on("submit", function (event) {
        event.preventDefault();
        // v_checkout_e_address("edit-addresses-block");
        // if ($("#edit-addresses-block").valid() === true) {
        $("#loading").css("display", "block");
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + "/ba/user/address/edit/" + user_id,
            data: $(this).serialize(),
            cache: false,
            success: function (data) {
                // console.log(data);
                document.getElementById("shipping-address-show").innerHTML = "";
                // All countries except korea
                if (country_id != "kr") {
                    // All countries except taiwan
                    if (country_id != "tw") {
                        if (data["delivery_address"]) {
                            document.getElementById(
                                "shipping-address-show"
                            ).innerHTML =
                                '<div class="panel-body">' +
                                '<div class="box-white" id="appendAddress">' +
                                "<br>" +
                                '<label style="font-weight: bold">Delivery Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.fullName +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.portalCode +
                                ", " +
                                data.delivery_address.city +
                                ", " +
                                data.delivery_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.contactNumber +
                                "</p><br>" +
                                "<br>" +
                                '<label style="font-weight: bold">Billing Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.portalCode +
                                ", " +
                                data.billing_address.city +
                                ", " +
                                data.billing_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.contactNumber +
                                "</p><br>" +
                                "</div>" +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                "</div>";
                        }
                    }
                    else {
                        if (data["delivery_address"]) {
                            document.getElementById(
                                "shipping-address-show"
                            ).innerHTML =
                                '<div class="panel-body">' +
                                '<div class="box-white" id="appendAddress">' +
                                "<br>" +
                                '<label style="font-weight: bold">Delivery Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.fullName +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.SSN +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.portalCode +
                                ", " +
                                data.delivery_address.city +
                                ", " +
                                data.delivery_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.delivery_address.contactNumber +
                                "</p><br>" +
                                "<br>" +
                                '<label style="font-weight: bold">Billing Address</label><br>' +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.address +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.portalCode +
                                ", " +
                                data.billing_address.city +
                                ", " +
                                data.billing_address.state +
                                "</p>" +
                                '<p style="margin-bottom:0;">' +
                                data.billing_address.contactNumber +
                                "</p><br>" +
                                "</div>" +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                "</div>";
                        }
                    }

                    // append address values into edit-form
                    let updateEditForm = {
                        delivery_address_fullname:
                            data.delivery_address.fullName,
                        delivery_address_address: data.delivery_address.address,
                        delivery_address_portalCode:
                            data.delivery_address.portalCode,
                        delivery_address_city: data.delivery_address.city,
                        delivery_address_state: data.delivery_address.state,
                        delivery_address_contactNumber:
                            data.delivery_address.contactNumber,
                        billing_address_address: data.billing_address.address,
                        billing_address_portalCode:
                            data.billing_address.portalCode,
                        billing_address_city: data.billing_address.city,
                        billing_address_state: data.billing_address.state,
                        billing_address_contactNumber:
                            data.billing_address.contactNumber
                    };

                    // onAddDeliveryAddress(updateEditForm);
                    let addressUpdates = {
                        selected_delivery_address_id: data.delivery_address
                            ? data.delivery_address.id
                            : "",
                        selected_billing_address_id: data.billing_address
                            ? data.billing_address.id
                            : ""
                    };
                    onUpdateDeliverySelection(addressUpdates, "update");

                    if (
                        updateEditForm.delivery_address_address !==
                        updateEditForm.billing_address_address
                    ) {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            true
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "block";
                    } else {
                        $("#enableBillingEditCheckBox_for_edit").prop(
                            "checked",
                            false
                        );
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                    $("#loading").css("display", "none");
                } else if (country_id == "kr") {
                    if (data["delivery_address"]) {
                        document.getElementById(
                            "shipping-address-show"
                        ).innerHTML =
                            '<div class="panel-body">' +
                            '<div class="box-white" id="appendAddress">' +
                            "<br>" +
                            '<label style="font-weight: bold">Delivery Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.fullName +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.portalCode +
                            ", " +
                            data.delivery_address.city +
                            ", " +
                            data.delivery_address.state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address.contactNumber +
                            "</p><br>" +
                            "<br>" +
                            '<label style="font-weight: bold">Billing Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.portalCode +
                            ", " +
                            data.billing_address.city +
                            ", " +
                            data.billing_address.state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address.contactNumber +
                            "</p><br>" +
                            "</div>" +
                            '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                            "</div>";
                    }

                    if (data["delivery_address"] && data["billing_address"]) {
                        var ada1 = document.getElementById(
                            "add_delivery_address1"
                        );
                        var ada = document.getElementById(
                            "add_delivery_address"
                        );
                        var adf = document.getElementById("add_delivery_flat");
                        ada1.value = "";
                        ada.value = "";
                        adf.value = "";

                        var aba1 = document.getElementById(
                            "add_billing_address1"
                        );
                        var aba = document.getElementById(
                            "add_billing_address"
                        );
                        var abf = document.getElementById("add_billing_flat");
                        aba1.value = "";
                        aba.value = "";
                        abf.value = "";

                        var dfn = document.getElementById("edit_delivery_name");
                        dfn.value = data["delivery_address"]["fullName"];
                        var dda = document.getElementById(
                            "edit_delivery_address"
                        );

                        var dss = document.getElementById("edit_SSN");
                        if (dss) {
                            dss.value = data["delivery_address"]["SSN"];
                        }

                        var getdda = data["delivery_address"]["address"];
                        dda.value = data["delivery_address"]["address"];
                        var dda1 = document.getElementById(
                            "edit_delivery_address1"
                        );
                        var ddf = document.getElementById("edit_delivery_flat");
                        if (getdda.includes(",")) {
                            var splitdda = getdda.split(",");
                            dda1.value = splitdda[0];
                            ddf.value = splitdda[1];
                        } else {
                            dda1.value = getdda;
                        }
                        var addcheckbox = document.getElementById(
                            "enableBillingEditCheckBox_for_add"
                        );
                        if (addcheckbox.checked == true) {
                            addcheckbox.click();
                        }
                        var ddc = document.getElementById("edit_delivery_city");
                        ddc.value = data["delivery_address"]["city"];

                        var dds = document.getElementById(
                            "edit_delivery_state"
                        );
                        dds.value = data["delivery_address"]["state"];

                        var dpc = document.getElementById(
                            "edit_delivery_postcode"
                        );
                        dpc.value = data["delivery_address"]["portalCode"];

                        var ddp = document.getElementById(
                            "edit_delivery_phone"
                        );
                        ddp.value = data["delivery_address"]["contactNumber"].replace(("+" + userPhoneExt), "");

                        var bba = document.getElementById(
                            "edit_billing_address"
                        );

                        var getbba = data["billing_address"]["address"];
                        bba.value = data["billing_address"]["address"];
                        var bba1 = document.getElementById(
                            "edit_billing_address1"
                        );
                        var bbf = document.getElementById("edit_billing_flat");
                        if (getbba.includes(",")) {
                            var splitbba = getbba.split(",");
                            bba1.value = splitbba[0];
                            bbf.value = splitbba[1];
                        } else {
                            bba1.value = getbba;
                        }

                        var bbc = document.getElementById("edit_billing_city");
                        bbc.value = data["billing_address"]["city"];

                        var bbs = document.getElementById("edit_billing_state");
                        bbs.value = data["billing_address"]["state"];

                        var bpc = document.getElementById(
                            "edit_billing_postcode"
                        );
                        bpc.value = data["billing_address"]["portalCode"];

                        var bbp = document.getElementById("edit_billing_phone");
                        bbp.value = data["billing_address"]["contactNumber"].replace(("+" + userPhoneExt), "");

                        // append address values into edit-form

                        let addressUpdates = {
                            selected_delivery_address_id: data.delivery_address
                                ? data.delivery_address.id
                                : "",
                            selected_billing_address_id: data.billing_address
                                ? data.billing_address.id
                                : ""
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        $("#loading").css("display", "none");
                    }

                    if (
                        document.getElementById("shipping-address-show") !==
                        "undefined" &&
                        document.getElementById("shipping-address-show") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-show"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-edit") !==
                        "undefined" &&
                        document.getElementById("shipping-address-edit") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-edit"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-edit") !==
                        "undefined" &&
                        document.getElementById("enable-address-edit") !== null
                    ) {
                        document.getElementById(
                            "enable-address-edit"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-add") !==
                        "undefined" &&
                        document.getElementById("shipping-address-add") !== null
                    ) {
                        document.getElementById(
                            "shipping-address-add"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-add") !==
                        "undefined" &&
                        document.getElementById("enable-address-add") !== null
                    ) {
                        document.getElementById(
                            "enable-address-add"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("cancel-address-edit") !==
                        "undefined" &&
                        document.getElementById("cancel-address-edit") !== null
                    ) {
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                }
            },
            error: function () {
                $("#loading").css("display", "none");
            }
        });
        // }
    });

    v_checkout_address("add-addresses-block");
    // Action: On submit add address
    $("#add-addresses-block").on("submit", function (event) {
        event.preventDefault();
        // v_checkout_address("add-addresses-block");
        // if ($("#add-addresses-block").valid() === true) {
        $("#loading").css("display", "block");
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + "/ba/user/address/add",
            data: $(this).serialize(),
            cache: false,
            success: function (data) {
                // console.log("address data: => " + JSON.stringify(data));
                document.getElementById("shipping-address-show").innerHTML = "";

                // All countries except korea
                if (country_id != "kr") {
                    if (data["delivery_address"] && data["billing_address"]) {
                        if (data["delivery_address"]) {
                            if (country_id != "tw") {
                                document.getElementById(
                                    "shipping-address-show"
                                ).innerHTML =
                                    '<div class="panel-body">' +
                                    '<div class="box-white" id="appendAddress">' +
                                    "<br>" +
                                    '<label style="font-weight: bold">Delivery Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].fullName +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].portalCode +
                                    ", " +
                                    data.delivery_address[0].city +
                                    ", " +
                                    data.delivery_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].contactNumber +
                                    "</p><br>" +
                                    "<br>" +
                                    '<label style="font-weight: bold">Billing Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].portalCode +
                                    ", " +
                                    data.billing_address[0].city +
                                    ", " +
                                    data.billing_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].contactNumber +
                                    "</p><br>" +
                                    "</div>" +
                                    "<div><button id='enable-address-edit' class='btn btn-load-more col-10' onclick='editAddress()'>EDIT ADDRESS</button></div>" +
                                    "</div>";
                            }
                            else {
                                document.getElementById(
                                    "shipping-address-show"
                                ).innerHTML =
                                    '<div class="panel-body">' +
                                    '<div class="box-white" id="appendAddress">' +
                                    "<br>" +
                                    '<label style="font-weight: bold">Delivery Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].fullName +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].SSN +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].portalCode +
                                    ", " +
                                    data.delivery_address[0].city +
                                    ", " +
                                    data.delivery_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.delivery_address[0].contactNumber +
                                    "</p><br>" +
                                    "<br>" +
                                    '<label style="font-weight: bold">Billing Address</label><br>' +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].address +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].portalCode +
                                    ", " +
                                    data.billing_address[0].city +
                                    ", " +
                                    data.billing_address[0].state +
                                    "</p>" +
                                    '<p style="margin-bottom:0;">' +
                                    data.billing_address[0].contactNumber +
                                    "</p><br>" +
                                    "</div>" +
                                    '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                                    "</div>";
                            }
                        }
                        $("#edit_delivery_name").val(
                            data.delivery_address[0].fullName
                        );
                        $("#edit_SSN").val(
                            data.delivery_address[0].SSN
                        );
                        $("#edit_delivery_phone").val(
                            data.delivery_address[0].contactNumber.replace(("+" + userPhoneExt), "")
                        );
                        $("#edit_delivery_address").val(
                            data.delivery_address[0].address
                        );
                        // $("#edit_delivery_state").val(data.delivery_address[0].state);
                        $("#edit_delivery_city").val(
                            data.delivery_address[0].city
                        );
                        $("#edit_delivery_postcode").val(
                            data.delivery_address[0].portalCode
                        );
                        $("#edit_billing_phone").val(
                            data.billing_address[0].contactNumber.replace(("+" + userPhoneExt), "")
                        );
                        $("#edit_billing_address").val(
                            data.billing_address[0].address
                        );
                        // $("#edit_billing_state").val(data.billing_address[0].state);
                        $("#edit_billing_city").val(
                            data.billing_address[0].city
                        );
                        $("#edit_billing_postcode").val(
                            data.billing_address[0].portalCode
                        );

                        // append address values into edit-form
                        let updateEditForm = {
                            delivery_address_fullname:
                                data.delivery_address[0].fullName,
                            delivery_address_address:
                                data.delivery_address[0].address,
                            delivery_address_portalCode:
                                data.delivery_address[0].portalCode,
                            delivery_address_city:
                                data.delivery_address[0].city,
                            delivery_address_state:
                                data.delivery_address[0].state,
                            delivery_address_contactNumber:
                                data.delivery_address[0].contactNumber,
                            billing_address_address:
                                data.billing_address[0].address,
                            billing_address_portalCode:
                                data.billing_address[0].portalCode,
                            billing_address_city: data.billing_address[0].city,
                            billing_address_state:
                                data.billing_address[0].state,
                            billing_address_contactNumber:
                                data.billing_address[0].contactNumber
                        };
                        // onAddDeliveryAddress(updateEditForm);
                        let addressUpdates = {
                            selected_delivery_address_id: data
                                .delivery_address[0].id
                                ? data.delivery_address[0].id
                                : "",
                            selected_billing_address_id: data.billing_address[0]
                                .id
                                ? data.billing_address[0].id
                                : ""
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        if (
                            updateEditForm.delivery_address_address !==
                            updateEditForm.billing_address_address
                        ) {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                true
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "block";
                        } else {
                            $("#enableBillingEditCheckBox_for_edit").prop(
                                "checked",
                                false
                            );
                            document.getElementById(
                                "cancel-address-edit"
                            ).style.display = "none";
                        }
                        $("#loading").css("display", "none");
                    }

                    if (
                        document.getElementById("shipping-address-show") !==
                        "undefined" &&
                        document.getElementById("shipping-address-show") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-show"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-edit") !==
                        "undefined" &&
                        document.getElementById("shipping-address-edit") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-edit"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-edit") !==
                        "undefined" &&
                        document.getElementById("enable-address-edit") !== null
                    ) {
                        document.getElementById(
                            "enable-address-edit"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-add") !==
                        "undefined" &&
                        document.getElementById("shipping-address-add") !== null
                    ) {
                        document.getElementById(
                            "shipping-address-add"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-add") !==
                        "undefined" &&
                        document.getElementById("enable-address-add") !== null
                    ) {
                        document.getElementById(
                            "enable-address-add"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("cancel-address-edit") !==
                        "undefined" &&
                        document.getElementById("cancel-address-edit") !== null
                    ) {
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }
                } else if (country_id == "kr") {
                    if (data["delivery_address"] && data["billing_address"]) {
                        document.getElementById(
                            "shipping-address-show"
                        ).innerHTML =
                            '<div class="panel-body">' +
                            '<div class="box-white" id="appendAddress">' +
                            "<br>" +
                            '<label style="font-weight: bold">Delivery Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address[0].fullName +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address[0].address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address[0].portalCode +
                            ", " +
                            data.delivery_address[0].city +
                            ", " +
                            data.delivery_address[0].state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.delivery_address[0].contactNumber +
                            "</p><br>" +
                            "<br>" +
                            '<label style="font-weight: bold">Billing Address</label><br>' +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address[0].address +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address[0].portalCode +
                            ", " +
                            data.billing_address[0].city +
                            ", " +
                            data.billing_address[0].state +
                            "</p>" +
                            '<p style="margin-bottom:0;">' +
                            data.billing_address[0].contactNumber +
                            "</p><br>" +
                            "</div>" +
                            '<div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">EDIT ADDRESS</button></div>' +
                            "</div>";

                        var ada1 = document.getElementById(
                            "add_delivery_address1"
                        );
                        var ada = document.getElementById(
                            "add_delivery_address"
                        );
                        var adf = document.getElementById("add_delivery_flat");
                        ada1.value = "";
                        ada.value = "";
                        adf.value = "";

                        var aba1 = document.getElementById(
                            "add_billing_address1"
                        );
                        var aba = document.getElementById(
                            "add_billing_address"
                        );
                        var abf = document.getElementById("add_billing_flat");
                        aba1.value = "";
                        aba.value = "";
                        abf.value = "";

                        var dfn = document.getElementById("edit_delivery_name");
                        dfn.value = data["delivery_address"][0]["fullName"];
                        var dda = document.getElementById(
                            "edit_delivery_address"
                        );

                        var dss = document.getElementById("edit_SSN");
                        if (dss) {
                            dss.value = data["delivery_address"][0]["SSN"];
                        }

                        var getdda = data["delivery_address"][0]["address"];
                        dda.value = data["delivery_address"][0]["address"];
                        var dda1 = document.getElementById(
                            "edit_delivery_address1"
                        );
                        var ddf = document.getElementById("edit_delivery_flat");
                        if (getdda.includes(",")) {
                            var splitdda = getdda.split(",");
                            dda1.value = splitdda[0];
                            ddf.value = splitdda[1];
                        } else {
                            dda1.value = getdda;
                        }
                        var addcheckbox = document.getElementById(
                            "enableBillingEditCheckBox_for_add"
                        );
                        if (addcheckbox.checked == true) {
                            addcheckbox.click();
                        }
                        var ddc = document.getElementById("edit_delivery_city");
                        ddc.value = data["delivery_address"][0]["city"];

                        var dds = document.getElementById(
                            "edit_delivery_state"
                        );
                        dds.value = data["delivery_address"][0]["state"];

                        var dpc = document.getElementById(
                            "edit_delivery_postcode"
                        );
                        dpc.value = data["delivery_address"][0]["portalCode"];

                        var ddp = document.getElementById(
                            "edit_delivery_phone"
                        );
                        ddp.value =
                            data["delivery_address"][0]["contactNumber"].replace(("+" + userPhoneExt), "");

                        var bba = document.getElementById(
                            "edit_billing_address"
                        );

                        var getbba = data["billing_address"][0]["address"];
                        bba.value = data["billing_address"][0]["address"];
                        var bba1 = document.getElementById(
                            "edit_billing_address1"
                        );
                        var bbf = document.getElementById("edit_billing_flat");
                        if (getbba.includes(",")) {
                            var splitbba = getbba.split(",");
                            bba1.value = splitbba[0];
                            bbf.value = splitbba[1];
                        } else {
                            bba1.value = getbba;
                        }

                        var bbc = document.getElementById("edit_billing_city");
                        bbc.value = data["billing_address"][0]["city"];

                        var bbs = document.getElementById("edit_billing_state");
                        bbs.value = data["billing_address"][0]["state"];

                        var bpc = document.getElementById(
                            "edit_billing_postcode"
                        );
                        bpc.value = data["billing_address"][0]["portalCode"];

                        var bbp = document.getElementById("edit_billing_phone");
                        bbp.value = data["billing_address"][0]["contactNumber"].replace(("+" + userPhoneExt), "");
                        // let updateEditForm = {
                        //     "delivery_address_fullname": data.delivery_address[0].fullName,
                        //     "delivery_address_address": data.delivery_address[0].address,
                        //     "delivery_address_portalCode": data.delivery_address[0].portalCode,
                        //     "delivery_address_city": data.delivery_address[0].city,
                        //     "delivery_address_state": data.delivery_address[0].state,
                        //     "delivery_address_contactNumber": data.delivery_address[0].contactNumber,
                        //     "billing_address_address": data.billing_address[0].address,
                        //     "billing_address_portalCode": data.billing_address[0].portalCode,
                        //     "billing_address_city": data.billing_address[0].city,
                        //     "billing_address_state": data.billing_address[0].state,
                        //     "billing_address_contactNumber": data.billing_address[0].contactNumber,
                        // };
                        UpdateSessionCPAddressData(
                            data["delivery_address"],
                            data["billing_address"]
                        );
                        // onAddDeliveryAddress(updateEditForm);
                        let addressUpdates = {
                            selected_delivery_address_id: data
                                .delivery_address[0].id
                                ? data.delivery_address[0].id
                                : "",
                            selected_billing_address_id: data.billing_address[0]
                                .id
                                ? data.billing_address[0].id
                                : ""
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        $("#loading").css("display", "none");
                    }

                    if (
                        document.getElementById("shipping-address-show") !==
                        "undefined" &&
                        document.getElementById("shipping-address-show") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-show"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-edit") !==
                        "undefined" &&
                        document.getElementById("shipping-address-edit") !==
                        null
                    ) {
                        document.getElementById(
                            "shipping-address-edit"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-edit") !==
                        "undefined" &&
                        document.getElementById("enable-address-edit") !== null
                    ) {
                        document.getElementById(
                            "enable-address-edit"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("shipping-address-add") !==
                        "undefined" &&
                        document.getElementById("shipping-address-add") !== null
                    ) {
                        document.getElementById(
                            "shipping-address-add"
                        ).style.display = "none";
                    }
                    if (
                        document.getElementById("enable-address-add") !==
                        "undefined" &&
                        document.getElementById("enable-address-add") !== null
                    ) {
                        document.getElementById(
                            "enable-address-add"
                        ).style.display = "block";
                    }
                    if (
                        document.getElementById("cancel-address-edit") !==
                        "undefined" &&
                        document.getElementById("cancel-address-edit") !== null
                    ) {
                        document.getElementById(
                            "cancel-address-edit"
                        ).style.display = "none";
                    }

                    // append address values into edit-form
                }
            },
            error: function () {
                $("#loading").css("display", "none");
            }
        });
        // }
    });

    // Proceed to Payment
    $("#form-pay").click(function (event) {
        event.preventDefault();
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
        let data = "";
        // console.log(session_checkout_data);
        onProceedPayment(session_data, session_checkout_data, data);
    });

    //kr card
    $("#form-pay-kr").click(function (event) {
        event.preventDefault();
    });
});
