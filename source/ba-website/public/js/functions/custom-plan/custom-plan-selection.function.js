//Set selected variables
let selected_blade;
let selected_frequency;
let selected_addon_list = [];
let selected_plan_id;
let current_step = 1;
let current_p;
let next_p;
let shipping;
// // Function: Select handle 
function SelectHandle(addon) {
    return function () {

        let productcountriesid = addon.productcountriesid;

        if ($(".handle-" + productcountriesid).hasClass('item-unselected')) {
            $(".handle-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".handle-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            //$('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list.push(addon);
        } else {
            $(".handle-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
            let productsku = $(".handle-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'hidden');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-", "-off");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                return value != addon;
            });
        }
    };
};

function SelectHandleClick(addon, clicktype, btntype) {
    let productcountriesid = addon.productcountriesid;
    if (clicktype == "submit") {
        let getlist = selected_addon_list;
        selected_addon_list = [];
        getlist.forEach(p => {
            if (p.ProductId == addon.ProductId) {

                if (btntype == "every") {
                    p["cycle"] = "all";
                } else if (btntype == "one") {
                    p["cycle"] = "1";
                }
            }

            selected_addon_list.push(p);
        });
    }
    else {
        if ($(".handle-" + productcountriesid).hasClass('item-unselected')) {
            $(".handle-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".handle-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list.push(addon);

        } else {
            $(".handle-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
            let productsku = $(".handle-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'hidden');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-", "-off");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                return value != addon;
            });
        }
    }
}

// // Function: Select bag 
function SelectBag(addon) {
    return function () {

        let productcountriesid = addon.productcountriesid;
        if ($(".bag-" + productcountriesid).hasClass('item-unselected')) {
            $(".bag-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".bag-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list.push(addon);
        } else {
            $(".bag-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
            let productsku = $(".bag-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'hidden');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-", "-off");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                return value != addon;
            });
        }
    };
};

function SelectBagClick(addon, clicktype, btntype) {
    let productcountriesid = addon.productcountriesid;
    if (clicktype == "submit") {
        let getlist = selected_addon_list;
        selected_addon_list = [];
        getlist.forEach(p => {
            if (p.ProductId == addon.ProductId) {

                if (btntype == "every") {
                    p["cycle"] = "all";
                } else if (btntype == "one") {
                    p["cycle"] = "1";
                }
            }

            selected_addon_list.push(p);
        });
    }
    else {
        if ($(".bag-" + productcountriesid).hasClass('item-unselected')) {
            $(".bag-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".bag-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            if (clicktype == "submit") {

            }
            selected_addon_list.push(addon);
        } else {
            $(".bag-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
            let productsku = $(".bag-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'hidden');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-", "-off");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                return value != addon;
            });
        }
    }
};


// Function: Select blade 
function SelectBlade(productcountriesid) {
    return function () {
        $(".blade-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        let imagepath = $(".blade-" + productcountriesid + " img").attr('src');
        imageget = imagepath.split("/").pop(-1);
        $(".custom-blade-img").attr('src',imageassetspath + "/customcheck_"+imageget);

        blade_products.forEach(product => {
            if (product.productcountriesid != productcountriesid) {
                $(".blade-" + product.productcountriesid).removeClass('item-selected').addClass('item-unselected');
            } else {
                selected_blade = product;
            }
        });
    };
};

// Function: Select frequency 
function SelectFrequency(frequency_id) {
    return function () {
        $(".frequency-" + frequency_id).removeClass('item-unselected').addClass('item-selected');
        $(".frequency-" + frequency_id + " .frequency-p").html("Nice! Looking forward to<br>seeing you every <b>" + frequency_id + " months</b>.");
        $("#frequency-tick-" + frequency_id).removeClass('d-none');
        let imagepath= $(".frequency-" + frequency_id + " img").attr('src');
        var res = imagepath.split(".png");
        var res1 = res[0].replace("-over", "");
        $(".frequency-" + frequency_id + " img").attr('src',res1+"-over.png");
        frequency_list.forEach(frequency => {
            var duration = frequency.duration;
            if (duration != frequency_id) {
                let imagepath= $(".frequency-" + duration + " img").attr('src');
                var res = imagepath.split(".png");
                var res1 = res[0].replace("-over", "");
                $(".frequency-" + duration + " img").attr('src',res1+".png");
                $(".frequency-" + duration).removeClass('item-selected').addClass('item-unselected');
            
                $("#frequency-tick-" + duration).addClass('d-none');
            } else {
                selected_frequency = duration;

            }
        });
        getPlanIdAndPrice();
    };
};

// Function: Select addon 
function SelectAddon(addon) {
    return function () {
        let productcountriesid = addon.productcountriesid;
        if ($(".addon-" + productcountriesid).hasClass('item-unselected')) {
            $(".addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".addon-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list.push(addon);
        } else {
            $(".addon-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
            let productsku = $(".addon-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'hidden');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-", "-off");
            $('.custom-' + productsku +"-img").attr('src',imagepath);

            selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                return value != addon;
            });
        }

    };
};


function SelectAddonPre(addon) {
        let productcountriesid = addon.productcountriesid;
        if ($(".addon-" + productcountriesid).hasClass('item-unselected')) {
            $(".addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
            let productsku = $(".addon-" + productcountriesid + " .productsku").first().text();
            productsku = productsku.replace("/", "");
            // $('.custom-' + productsku +"-img").css('visibility', 'visible');
            let imagepath= $('.custom-' + productsku +"-img").attr('src');
            imagepath = imagepath.replace("-off", "-");
            $('.custom-' + productsku +"-img").attr('src',imagepath);
            selected_addon_list.push(addon);
        } 
};

function getPlanIdAndPrice() {
    let totalprice = 0.00;
    let nextmonthprice = 0.00;
    let currentprice = 0.00;
    let planpriceget = 0.00;
    let ProductSellPrice;
    let ProductCountriesId = [];
    let ProductCountriesIdAddOn = [];
    let ProductCountriesIdGet = "";
    let plancombinecontry = "";
    let planskuget = "";
    let planidget = "";
    let count = 0;
    let count1 = 0;

    if (selected_blade && selected_frequency) {

        //save all product country id
        ProductCountriesId.push(selected_blade["productcountriesid"]);
        if (ProductCountriesId.length > 1) {
            ProductCountriesId.sort();
        }

        ProductCountriesId.forEach(function (productCountryId) {
            if (count1 == 0) {
                ProductCountriesIdGet = ProductCountriesIdGet + productCountryId + '';
            } else {
                ProductCountriesIdGet = ProductCountriesIdGet + "," + productCountryId + '';
            }
            count1++;
        });
        if (allplan) {
            allplan.forEach(function (plan) {
                plancombinecontry = "";
                if (plan.planduration == selected_frequency) {
                    plan.productCountryId.forEach(function (productCountryId) {
                        if (count == 0) {
                            plancombinecontry = plancombinecontry + productCountryId;
                        } else {
                            plancombinecontry = plancombinecontry + "," + productCountryId;
                        }
                        count++;
                    });

                    if (ProductCountriesIdGet == plancombinecontry) {
                        planidget = plan.planid;
                        planpriceget = parseFloat(plan.sellPrice);
                        planskuget = plan.plansku;
                    }
                }
                count = 0;
            });
        }
        ProductSellPrice = parseFloat(selected_blade["sellPrice"]);
        nextmonthprice = nextmonthprice + ProductSellPrice;
        currentprice = nextmonthprice;


        selected_addon_list.forEach(function (data) {
            ProductCountriesIdAddOn.push(data["productcountriesid"]);
            nextmonthprice = nextmonthprice + parseFloat(data["sellPrice"]);
        });

        totalprice = totalprice + nextmonthprice + shippingfee;

        if (planidget) {
            if (currentprice == planpriceget) {
                insertPrice(totalprice, nextmonthprice, shippingfee, currentprice, planidget);
            }
        } else {
            let plantype = plantypeconfig;
            let plangroup = plangroupconfig;
            let bladetype = selected_blade["sku"].charAt(1);
            let imagesurl = "";
            var pc = document.getElementById("proceed-checkout");
            if (pc) {
                pc.disabled = true;
            }
            let planidget = autoCreatePlan(ProductCountriesId, currentprice, bladetype, plangroup, plantype, country_id, imagesurl, selected_frequency);
            insertPrice(totalprice, nextmonthprice, shippingfee, currentprice, planidget);

        }

    }
}

function autoCreatePlan(ProductCountriesId, currentprice, bladetype, plangroup, plantype, country_id, imagesurl, selected_frequency) {
    // $.ajax({
    //     url: GLOBAL_URL_V2 + "/ajax/autoCreatePlan",
    //     headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    //     method: "POST",
    //     data: { ProductCountriesId, currentprice, bladetype, plangroup, plantype, country_id, imagesurl, selected_frequency },
    //     success: function (response) {
    //         selected_plan_id = response;
    //         var pc = document.getElementById("proceed-checkout");
    //         if (pc) {
    //             pc.disabled = false;
    //         }
    //         return response;
    //     }
    // });
}

function insertPrice(totalprice, nextmonthprice, shippingfee, currentprice, planidget) {
    selected_plan_id = planidget;
    // document.getElementById("today_subtotal").innerHTML = nextmonthprice.toFixed(2) + '';
    // document.getElementById("next_month_subtotal").innerHTML = nextmonthprice.toFixed(2) + '';
    // document.getElementById("shipping_total").innerHTML = shippingfee.toFixed(2) + '';
    // document.getElementById("payment_total").innerHTML = totalprice.toFixed(2) + '';
    current_p = nextmonthprice.toFixed(2) + '';
    next_p = nextmonthprice.toFixed(2) + '';
    shipping = shippingfee.toFixed(2) + '';
    UpdateSessionData();
}

//Function: Update step
function UpdateStep(current_step) {

    // Only show selected panel and hide others
    for (let i = 1; i <= 5; i++) {
        var z = current_step - 1;
        if (i == current_step) {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-unselected').addClass('panel-title-selected');
            $("#step" + i + '-heading.topnav-' + i + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
            $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
            $("#collapse" + i).addClass('show');
            $("#collapseTitle" + i).addClass('show');
            $("#collapse-mobile-" + i).addClass('show');
            $("#collapseTitle-mobile-" + i).addClass('show');
            //console.log("a");
        } else {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-selected').addClass('panel-title-unselected');
            $("#collapse" + i).removeClass('show');
            $("#collapseTitle" + i).removeClass('show');
            $("#collapse-mobile-" + i).removeClass('show');
            $("#collapseTitle-mobile-" + i).removeClass('show');
            //console.log("b");
        }
    }

    // Do something according to current_step id
    switch (current_step) {
        case 1:
            {
                //do nothing now
            }
            break;
        case 2:
            {
                //do nothing now
            }
            break;
        case 3:
            {
                $(".button-next").removeClass('d-none');
                $("#form-checkout").addClass('d-none');
                $(".checkout-title-1").removeClass('d-none');
                $(".checkout-title-2").addClass('d-none');
                $("#step1-heading").removeClass('d-none');
                $("#step2-heading").removeClass('d-none');
                $("#step3-heading").removeClass('d-none');
            }
            break;
        case 4:
            {
                $(".button-next").addClass('d-none');
                $("#form-checkout").removeClass('d-none');
                $(".checkout-title-1").addClass('d-none');
                $(".checkout-title-2").removeClass('d-none');
                $("#step1-heading").addClass('d-none');
                $("#step2-heading").addClass('d-none');
                $("#step3-heading").addClass('d-none');
            }
            break;
        default:
            break;
    }
}

//Function: Display Selected Handle,Blade,Frequency and Addons
function DisplaySelectedItems() {
    $("#text_selected_blade").text(selected_blade['producttranslatesname']);
    $("#text_selected_frequency").text(selected_frequency + " months");
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_SELECTION_CUSTOM_PLAN, SESSION_GET, null).done(function (data) {


        // Do something with the session data
        if (data) {
            let session_data = JSON.parse(data);
            let previous_step = session_data["selection"]["current_step"];
            current_step = previous_step;
            UpdateStep(previous_step);
            if (previous_step >= 1) {
                if (previous_step >= 2) {
                    if (previous_step >= 3) {
                        if (previous_step >= 4) {
                        }
                    }
                }
            }
        }

    });
}

// Function: Update session data
function UpdateSessionData() {
    let session_data = {};
    session_data["selection"] = {};
    session_data["selection"]["step1"] = { "selected_handle": '' };
    session_data["selection"]["step2"] = { "selected_blade": selected_blade };
    session_data["selection"]["step3"] = { "selected_frequency": selected_frequency };
    session_data["selection"]["step4"] = { "selected_addon_list": selected_addon_list };
    session_data["selection"]["step5"] = { "planId": selected_plan_id, "current_price": current_p, "shipping_fee": shipping, "next_price": next_p };
    session_data["selection"]["current_step"] = current_step;
    session_data["selection"]["plan_type"] = "custom-plan";
    session_data["selection"]["journey_type"] = "selection";
    // session_data["selection"]["countryid"] = country_id;
    // session_data["selection"]["langcode"] = langCode;
    // console.log("session_data + " + JSON.stringify(session_data));
    session(SESSION_SELECTION_CUSTOM_PLAN, SESSION_SET, JSON.stringify(session_data)).done(function () {

    });
}

// Function: Clear session data
function ClearSessionData() {
    session(SESSION_SELECTION_CUSTOM_PLAN, SESSION_CLEAR, null).done(function () {

    });
}

// Function: Select panel
function SelectPanel(panel_id) {
    return function () {
        //Current step is ahead of the selected panel id
        if (panel_id < current_step) {
            // Show or hide selected panel  
            if (!$("#collapse" + panel_id).hasClass('show') && !!$("#collapseTitle" + panel_id).hasClass('show') && !$("#collapse-mobile-" + panel_id).hasClass('show') && !!$("#collapseTitle-mobile-" + panel_id).hasClass('show')) {
                $("#step1-heading.mobile_heading1").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step1-title.mobile_title1").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse1").removeClass('show');
                $("#collapseTitle1").removeClass('show');
                $("#collapse-mobile-1").removeClass('show');
                $("#collapseTitle-mobile-1").removeClass('show');
                $("#step2-heading.mobile_heading2").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step2-title.mobile_title2").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse2").removeClass('show');
                $("#collapse-mobile-2").removeClass('show');
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-unselected').addClass('panel-title-selected');
                $("#step" + panel_id + '-heading.topnav-' + panel_id + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
                $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
                $("#collapse" + panel_id).addClass('show');
                $("#collapseTitle" + panel_id).addClass('show');
            } else {
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse" + panel_id).removeClass('show');
            }
        }
    }
}

function addonCycle(addonID, addonName, addonImage, addontype) {
    let addoncycleimg = document.getElementById("img-addon-cycle-modal");
    if (addoncycleimg) {
        addoncycleimg.src = "";
        addoncycleimg.src = addonImage;
    }
    let addoncyclename = document.getElementById("name-addon-cycle-modal");
    if (addoncyclename) {
        addoncyclename.innerHTML = "";
        addoncyclename.innerHTML = addonName;
    }
    let addonidhidden = document.getElementById("addonid-hidden");
    if (addonidhidden) {
        addonidhidden.value = "";
        addonidhidden.value = addonID;
    }
    let addontypehidden = document.getElementById("addontype-hidden");
    if (addontypehidden) {
        addontypehidden.value = "";
        addontypehidden.value = addontype;
    }
    if ($("." + addontype + addonID).hasClass('item-unselected')) {
        $('#addon-cycle-modal').modal('show');
    }
    // console.log(addonID, addonName, addonImage);
}

function addonCycleClose() {

    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }
    let addontypehidden = document.getElementById("addontype-hidden");
    let addontypehiddenget = "";
    if (addontypehidden) {
        addontypehiddenget = addontypehidden.value;
    }

    if ($("." + addontypehiddenget + addonidhiddenget).hasClass('item-selected')) {
        //On select handle
        if (addontypehiddenget == "handle-") {
            for (let i = 0; i < handle_products.length; i++) {
                let productcountriesid = handle_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectHandleClick(handle_products[i], "close", "close");
                }

            }
        }
        else if (addontypehiddenget == "bag-") {
            for (let i = 0; i < bag_products.length; i++) {
                let productcountriesid = bag_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectBagClick(bag_products[i], "close", "close")
                }
            }
        }
    }
}

function addonCycleEvery() {
    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }
    let addontypehidden = document.getElementById("addontype-hidden");
    let addontypehiddenget = "";
    if (addontypehidden) {
        addontypehiddenget = addontypehidden.value;
    }

    if ($("." + addontypehiddenget + addonidhiddenget).hasClass('item-selected')) {
        //On select handle
        if (addontypehiddenget == "handle-") {
            for (let i = 0; i < handle_products.length; i++) {
                let productcountriesid = handle_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectHandleClick(handle_products[i], "submit", "every");
                }

            }
        }
        else if (addontypehiddenget == "bag-") {
            for (let i = 0; i < bag_products.length; i++) {
                let productcountriesid = bag_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectBagClick(bag_products[i], "submit", "every")
                }
            }
        }
    }
}

function addonCycleOne() {

    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }
    let addontypehidden = document.getElementById("addontype-hidden");
    let addontypehiddenget = "";
    if (addontypehidden) {
        addontypehiddenget = addontypehidden.value;
    }

    if ($("." + addontypehiddenget + addonidhiddenget).hasClass('item-selected')) {
        //On select handle
        if (addontypehiddenget == "handle-") {
            for (let i = 0; i < handle_products.length; i++) {
                let productcountriesid = handle_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectHandleClick(handle_products[i], "submit", "one");
                }

            }
        }
        else if (addontypehiddenget == "bag-") {
            for (let i = 0; i < bag_products.length; i++) {
                let productcountriesid = bag_products[i].productcountriesid;
                if (productcountriesid == addonidhiddenget) {
                    SelectBagClick(bag_products[i], "submit", "one")
                }
            }
        }
    }
}

// On page load
$(function () {

    let current_step = 1;
    //On select blade
    for (let i = 0; i < blade_products.length; i++) {
        let productcountriesid = blade_products[i].productcountriesid;
        $(".blade-" + productcountriesid).click(SelectBlade(productcountriesid))

        if (i == 2) {
            $(".blade-" + productcountriesid).click();
        }
    }

    //On select frequency
    for (let i = 0; i < frequency_list.length; i++) {
        let frequency_id = frequency_list[i].duration;
        $(".frequency-" + frequency_id).click(SelectFrequency(frequency_id));

        if (i == 0) {
            $(".frequency-" + frequency_id).click();
        }
    }

    //On select handle
    for (let i = 0; i < handle_products.length; i++) {
        let productcountriesid = handle_products[i].productcountriesid;
        // $("#handle-" + productcountriesid).click(SelectHandle(productcountriesid))

        // if(i == 0) {
        //     $("#handle-" + productcountriesid).click();
        // }

        // $("#handle-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        //selected_addon_list.push(handle_products[i]);

        $(".handle-" + productcountriesid).click(SelectHandle(handle_products[i]))
    }

    //On select bag
    for (let i = 0; i < bag_products.length; i++) {
        let productcountriesid = bag_products[i].productcountriesid;
        // $("#handle-" + productcountriesid).click(SelectHandle(productcountriesid))

        // if(i == 0) {
        //     $("#handle-" + productcountriesid).click();
        // }

        // $("#handle-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        //selected_addon_list.push(handle_products[i]);

        $(".bag-" + productcountriesid).click(SelectBag(bag_products[i]))
    }

    // //On select addon
    for (let i = 0; i < addon_products.length; i++) {
        let productcountriesid = addon_products[i].productcountriesid;

        // if(productcountriesid == free_shave_cream_id) {
        // $("#addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        // selected_addon_list.push(addon_products[i]);
        // }
        SelectAddonPre(addon_products[i]);
        $(".addon-" + productcountriesid).click(SelectAddon(addon_products[i]))
    }

    $("#proceed-checkout").click(function () {
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
    });

    //On click next
    $(".button-next").click(function () {
        if (current_step != 5) {
            current_step++;
            getPlanIdAndPrice();
            UpdateStep(current_step);
        }
        let topnav1 = document.getElementById("step1-heading");
        if(topnav1){
            topnav1.style.color = "black";
        }
        let topnav2 = document.getElementById("step2-heading");
        if(topnav2){
            topnav2.style.color = "#ff5001";
        }
        UpdateSessionData();
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step != 1) {
            current_step--;
            UpdateStep(current_step);
        }
    });

    $("#check").click(function () {

    });

    $("#clear").click(function () {
    });

    $("#addoncycleclose").click(function () {


    });
})