let login_form = $('#ba_login_form');

// BA Sellers Authentication
function authenticate() {
    checkBadgeId().then(function (data) {
        let attempt = attemptLogin(data);
    }).catch(function (err) {
        // Run this when promise was rejected via reject()
    })

}

function checkBadgeId() {
    return new Promise(function (resolve, reject) {
        let email = document.getElementById('login_email').value;
        let password = document.getElementById('login_password').value;
        let msg = [];
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: GLOBAL_URL_V2 + "/check",
            data: { badgeId: email, password: password },
            success: function (data) {
                console.log(data);
                resolve(data);
            },
            error: function (data) {
                reject(data);
            },
            failure: function (data) {
                reject(data);
            }
        });
    })
}


function attemptLogin(data) {
    console.log(data);
    let badgeId = data.badgeId;
    let password = document.getElementById('login_password').value;
    if (data !== "") {
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL_V2 + "/login",
                data: { badgeId: badgeId, password: password },
                success: function (data) {
                    if (data.redirect_url) {
                        window.addStorage("seller", JSON.stringify(data.seller));
                        window.addStorage("current_country", JSON.stringify(data.country));
                        // window.addStorage("current_lang", JSON.stringify(data.country.defaultLang));
                        window.location.replace(data.redirect_url);
                    }
                    // session(SESSION_SELLER_INFO, SESSION_SET, data.payload)
                    resolve(data);
                },
                error: function (data) {
                    let login_data_error = document.getElementById('login_data_error');
                    if(login_data_error){
                        if(data.responseJSON.message == "The given data was invalid."){
                            let login_data_errorget = document.getElementById('login_data_errorget'); 
                            if(login_data_errorget){
                            login_data_error.innerHTML = login_data_errorget.textContent;
                            }
                       
                          }
                    }
                    
                    reject(data.responseJSON.message);
                },
                failure: function (data) {
                    reject(data);
                }
            });
        })
    } else {
        return false;
    }
}


function logout() {
    // console.log(window.location.origin);
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: 'POST',
            url: window.location.origin + GLOBAL_URL_V2 + "/logout",
            data: {},
            success: function (data) {
                window.localStorage.clear('current_user');
                window.localStorage.clear('seller');
                window.localStorage.clear('current_lang');
                window.localStorage.clear('current_country');
                window.localStorage.clear('gender_selected');
                // window.location.reload();
                window.location.replace(window.location.origin + GLOBAL_URL_V2);
                resolve(data);
            },
            error: function (data) {
                reject(data);
            },
            failure: function (data) {
                reject(data);
            }
        });
    })
}


// on load
