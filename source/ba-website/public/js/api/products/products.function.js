let api_trial_plan_data = null;
let api_custom_plan_data = null;
let api_products_data = null;

// Gender Selection
function onGenderSelect(element_name) {
    var loading = document.getElementById("loading");
    if (loading) {
        loading.style.display = "block";
    }
    switch (element_name) {
        case "pg_male":
            document.getElementById("pg_select").value = "male";
            break;
        case "pg_female":
            document.getElementById("pg_select").value = "female";
            break;
        default:
            document.getElementById("pg_select").value = null;
    }

    let langCode = JSON.parse(window.localStorage.getItem("current_lang"));
    let country = JSON.parse(window.localStorage.getItem("current_country"));
    if (document.getElementById("pg_select").value !== null) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                url: API_URL + "/ba/products/list",
                data: {
                    gender: document.getElementById("pg_select").value,
                    CountryId: country.id,
                    langCode: langCode,
                    appType: "baWebsite"
                },
                success: function (data) {
                    console.log(data);
                    window.addStorage(
                        "gender_selected",
                        JSON.stringify(data.payload.gender)
                    );
                    redirectToProductTypes(data);
                    resolve(data);
                    if (loading) {
                        loading.style.display = "none";
                    }
                },
                error: function (data) {
                    if (loading) {
                        loading.style.display = "none";
                    }
                    reject(data);
                },
                failure: function (data) {
                    if (loading) {
                        loading.style.display = "none";
                    }
                    reject(data);
                }
            });
        });
    }
    if (loading) {
        loading.style.display = "none";
    }
}

function redirectToProductTypes(data) {
    let t_tk =
        data.payload.filtered_tk_plans !== null
            ? data.payload.filtered_tk_plans.active_plans
            : null;
    let t_cp =
        data.payload.filtered_cp_plans !== null
            ? data.payload.filtered_cp_plans.active_plans
            : null;
    let t_prdts =
        data.payload.filtered_products !== null
            ? data.payload.filtered_products.active_products
            : null;
    let gender = window.localStorage.getItem("gender");
    if (!(t_tk <= 0 && t_cp <= 0 && t_prdts <= 0)) {
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            url: GLOBAL_URL + "/products/product-types",
            data: { gender: gender },
            success: function (data) {
                console.log(data);
                if (data) {
                    window.location.replace(data);
                }
            },
            error: function (err) {
                console.log(err);
                // show popup with error messages
            },
            failure: function (data) {
                console.log(data);
            }
        });
    } else {
        // Sweet alert popup -> msg -> no product or plans found
        Swal.fire("No Plans or Products found.").then(result => { });
        // alert('No Plans or Products found.');
    }
}

function loadProductTypes() {
    let country = JSON.parse(window.localStorage.getItem("current_country"));
    let langCode = JSON.parse(window.localStorage.getItem("current_lang"));
    let gender = JSON.parse(window.localStorage.getItem("gender_selected"));
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            url: API_URL + "/ba/products/get-all-types-list",
            data: {
                gender: gender,
                CountryId: country.id,
                langCode: langCode,
                appType: "baWebsite"
            },
            success: function (data) {
                console.log(langCode);
                console.log(data);
                let details = data.payload;
                let group_tk = document.getElementById("TK_Lists");
                let group_cp = document.getElementById("CP_Lists");
                let group_prdts = document.getElementById("Prdts_Lists");
                let category_name = "";
                let products_name = "";
                if (
                    details.filtered_tk_plans !== "" ||
                    details.filtered_tk_plans !== null
                ) {
                    if (
                        details.filtered_tk_plans.name === "Women Trial Plan" &&
                        details.request_options.CountryId !== 1
                    ) {
                    } else {
                        if (details.filtered_tk_plans.name === "Trial Plan") {
                            if (langCode.toUpperCase() == "ZH-HK") {
                                category_name = "試用禮盒";
                            } else {
                                category_name = "Starter Kit";
                            }
                        }

                        if (
                            details.filtered_tk_plans.name ===
                            "Women Trial Plan"
                        ) {
                            if (langCode.toUpperCase() == "ZH-HK") {
                                category_name = "試用禮盒";
                            } else {
                                category_name = "Starter Kit";
                            }
                        }
                        if ((country.id === 1 || country.id === 7) && details.gender !== "female") {
                            let append_trial_type =
                            '<div class="col-10 d-inline-flex item-unselected cursorPointer">' +
                            '<div class="col-6 m-auto"><img class="productImgSelection" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/PlansBaMale-premium.png" /></div>' +
                            '<div class="col-6 m-auto" style="padding:0px;"><h3 style="font-size: 20px;font-weight: 700;" class="text-center m-auto">' +
                            category_name +
                            "</h3></div>" +
                            "</div>";
                        group_tk.innerHTML = append_trial_type;

                        } else {
                            let append_trial_type =
                            '<div class="col-10 d-inline-flex item-unselected cursorPointer">' +
                            '<div class="col-6 m-auto"><img class="productImgSelection" src="' +
                            details.filtered_tk_plans.translate_url +
                            '" /></div>' +
                            '<div class="col-6 m-auto" style="padding:0px;"><h3 style="font-size: 20px;font-weight: 700;" class="text-center m-auto">' +
                            category_name +
                            "</h3></div>" +
                            "</div>";
                        group_tk.innerHTML = append_trial_type;

                        }


                    }
                }
                if (details.gender !== "female") {
                    if (
                        details.filtered_cp_plans !== "" ||
                        details.filtered_cp_plans !== null
                    ) {
                        let append_custom_type =
                            '<div class="col-10 d-inline-flex item-unselected cursorPointer">' +
                            '<div class="col-6 m-auto"><img class="productImgSelection" style="" src="' +
                            details.filtered_cp_plans.translate_url +
                            '" /></div>' +
                            '<div class="col-6 m-auto" style="padding:0px;"><h3 style="font-size: 20px;font-weight: 700;" class="text-center m-auto">' +
                            details.filtered_cp_plans.name +
                            "</h3></div>" +
                            "</div>";

                        group_cp.innerHTML = append_custom_type;
                    }
                }

                let imageUrl = '';
                if (details.gender !== "female") {
                    if (country.id === 1 || country.id === 7) {
                        imageUrl = "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png";
                    } else {
                        imageUrl = "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png";
                    }
                } else {
                    if (country.id === 1 || country.id === 7) {
                        imageUrl = "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png";
                    } else {
                        imageUrl = "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png";
                    }

                }
                if (country.id !== 7 && country.id !== 2) {
                    if (details.filtered_products.length > 0) {
                        if (langCode.toUpperCase() == "ZH-HK") {
                            products_name = "剃鬚產品";
                        } else {
                            products_name = "Products";
                        }
                        let append_product_type =
                            '<div class="col-10 d-inline-flex item-unselected cursorPointer">' +
                            '<div class="col-6 m-auto"><img class="productImgSelection" style="" src="' +
                            imageUrl +
                            '" /></div>' +
                            '<div class="col-6 m-auto" style="padding:0px;"><h3 style="font-size: 20px;font-weight: 700;" class="text-center m-auto">' +
                            products_name +
                            "</h3></div>" +
                            "</div>";
                        group_prdts.innerHTML = append_product_type;
                    }
                }
                // }
            },
            error: function (data) { },
            failure: function (data) { }
        });
    });
}

function loadTrialPlans() {
    let country = JSON.parse(window.localStorage.getItem("current_country"));
    let langCode = JSON.parse(window.localStorage.getItem("current_lang"));
    let gender = JSON.parse(window.localStorage.getItem("gender_selected"));
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            url: API_URL + "/ba/products/trial/list",
            data: {
                gender: gender,
                CountryId: country.id,
                langCode: langCode,
                appType: "baWebsite"
            },
            success: function (data) {
                console.log(data);
                handle_products = data.payload.handle_products;
                blade_products = data.payload.blade_products;
                frequency_list = data.payload.frequency_list;
                addon_products = data.payload.addon_products;
                free_shave_cream_id = data.payload.free_shave_cream_id;
                allplan = data.payload.allplan;
                country_id = data.payload.country.id;
                let country = data.payload.country;
                currency = data.payload.country.currencyCode;
                trial_price = data.payload.trial_price;
                trial_saving_price = data.payload.trial_saving_price;
                next_refill_date = data.payload.next_refill_date;
                user_id = null;

                let _trialPriceGenerate = {
                    currencyDisplay: country.currencyDisplay,
                    trialprice: trial_price,
                    trial_saving_price: trial_saving_price
                };
                generateHTMLElements(
                    "trial_purchase",
                    "trial",
                    "step-1",
                    "trial_price",
                    _trialPriceGenerate
                );

                let _bladeGenerate = [];
                blade_products.forEach(function (x) {
                    _bladeGenerate.push({
                        img: x.productDefaultImage,
                        sku: x.sku,
                        ProductCountryId: x.productcountriesid
                    });
                });
                generateHTMLElements(
                    "trial_purchase",
                    "trial",
                    "step-1",
                    "blades",
                    _bladeGenerate
                );
                resolve(data);
            },
            error: function (data) {
                reject(data);
            },
            failure: function (data) {
                reject(data);
            }
        });
    });
}

function loadCustomPlans() { }

function loadAlacarteProducts() { }

function goToTrialPlanView() {
    let country = JSON.parse(window.localStorage.getItem("current_country"));
    let langCode = JSON.parse(window.localStorage.getItem("current_lang"));
    let gender = JSON.parse(window.localStorage.getItem("gender_selected"));
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: API_URL + "/ba/products/trial/list",
        data: {
            gender: gender,
            CountryId: country.id,
            langCode: langCode,
            appType: "baWebsite"
        },
        success: function (data) {
            if (data) {
                handle_products = data.payload.handle_products;
                blade_products = data.payload.blade_products;
                frequency_list = data.payload.frequency_list;
                addon_products = data.payload.addon_products;
                free_shave_cream_id = data.payload.free_shave_cream_id;
                allplan = data.payload.allplan;
                country_id = data.payload.country.id;
                let country = data.payload.country;
                currency = data.payload.country.currencyCode;
                trial_price = data.payload.trial_price;
                trial_saving_price = data.payload.trial_saving_price;
                next_refill_date = data.payload.next_refill_date;
                user_id = null;
                console.log($("#selected_trial_blade").val());

                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    url: GLOBAL_URL + "/trial/step-1",
                    data: { steps: "select-blade", data: data },
                    success: function (response) {
                        console.log(response);
                        if (response.url) {
                            $("#goToSelectedView").attr("action", response.url);
                            $("#trial_data").val(JSON.stringify(data));
                            $("#goToSelectedView").submit();
                        }
                    },
                    error: function (data) { },
                    failure: function (data) { }
                });
            }
        }
    });
}

function goToTrialPlanViewStep2() {
    blade_products.forEach(function (x) {
        console.log(
            x.productcountriesid,
            parseInt($("#selected_trial_blade").val())
        );
        if (
            x.productcountriesid === parseInt($("#selected_trial_blade").val())
        ) {
            selected_blade = x;
        }
    });

    console.log(selected_blade);

    let data = {
        handle_products: handle_products,
        blade_products: blade_products,
        frequency_list: frequency_list,
        addon_products: addon_products,
        free_shave_cream_id: free_shave_cream_id,
        allplan: allplan,
        country_id: country_id,
        currency: currency,
        trial_price: trial_price,
        trial_saving_price: trial_saving_price,
        next_refill_date: next_refill_date,
        user_id: user_id,
        selected_handle: null,
        selected_blade: selected_blade,
        selected_frequency: null,
        selected_addon_list: null,
        selected_addon1: null,
        selected_addon2: null,
        selected_plan_id: null,
        current_shipping_image: null,
        next_billing_image: null,
        hasShaveCreamNextBilling: null,
        current_p: null,
        next_p: null,
        shipping: null
    };

    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: GLOBAL_URL + "/trial/step-2",
        data: { steps: "select-refill", selected_blade: selected_blade },
        success: function (response) {
            console.log(response);
            if (response.url) {
                $("#goToSelectedView").attr("action", response.url);
                data.selected_blade = response.selected_blade;
                $("#trial_data").val(JSON.stringify(data));
                $("#goToSelectedView").submit();
            }
        },
        error: function (data) { },
        failure: function (data) { }
    });
}

function goToTrialPlanViewStep3() {
    let data = {
        handle_products: handle_products,
        blade_products: blade_products,
        frequency_list: frequency_list,
        addon_products: addon_products,
        free_shave_cream_id: free_shave_cream_id,
        allplan: allplan,
        country_id: country_id,
        currency: currency,
        trial_price: trial_price,
        trial_saving_price: trial_saving_price,
        next_refill_date: next_refill_date,
        user_id: user_id,
        selected_handle: null,
        selected_blade: selected_blade,
        selected_frequency: null,
        selected_addon_list: null,
        selected_addon1: null,
        selected_addon2: null,
        selected_plan_id: null,
        current_shipping_image: null,
        next_billing_image: null,
        hasShaveCreamNextBilling: null,
        current_p: null,
        next_p: null,
        shipping: null
    };

    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: GLOBAL_URL + "/trial/step-3",
        data: { steps: "select-frequency", selected_blade: selected_blade },
        success: function (response) {
            console.log(response);
            console.log(window);
            if (response.url) {
                $("#goToSelectedView").attr("action", response.url);
                $("#trial_data").val(JSON.stringify(data));
                $("#goToSelectedView").submit();
            }
        },
        error: function (data) { },
        failure: function (data) { }
    });
}

function proceedToBATrialCheckout() {
    UpdateSessionData(true);
}

function goToProductView() {
    let country = JSON.parse(window.localStorage.getItem("current_country"));
    let langCode = JSON.parse(window.localStorage.getItem("current_lang"));
    let gender = JSON.parse(window.localStorage.getItem("gender_selected"));

    $.ajax({
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        url: API_URL + "/ba/products/alacarte/list",
        data: {
            gender: gender,
            CountryId: country.id,
            langCode: langCode,
            appType: "baWebsite"
        },
        success: function (data) {
            console.log(data);
            if (data) {
                //determine starting step based on empty objects
                let start_step = "";
                if (data.payload.filtered_products.handle_products.length > 0) {
                    start_step = "#handle";
                }
                else if  (data.payload.filtered_products.blade_products.length > 0) {
                    start_step = "#blade";
                }
                else if  (data.payload.filtered_products.addon_products.length > 0) {
                    start_step = "#addon";
                }
                else if  (data.payload.filtered_products.ask_products.length > 0) {
                    start_step = "#ask";
                }
                $("#goToSelectedView").attr(
                    "action",
                    GLOBAL_URL + "/alacarte/select" + start_step
                );
                $("#product_data").val(JSON.stringify(data));
                $("#goToSelectedView").submit();
            }
        }
    });
}

function generateHTMLElements(page, type, subtype, group, data) {
    // element lists
    let trialprice_1 = $("#trialprice_1");
    let trialprice_2 = $("#trialprice_2");
    let trialprice_3 = $("#trialprice_3");
    let trialprice_4 = $("#trialprice_4");
    let trialprice_5 = $("#trialprice_5");
    let trialprice_6 = $("#trialprice_6");
    let trialpricesaving_1 = $("#trialpricesaving_1");
    let trialpricesaving_2 = $("#trialpricesaving_2");

    let selectyourbladetype_1 = $("#selectyourbladetype_1");
    let selectyourbladetype_2 = $("#selectyourbladetype_2");
    switch (page) {
        case "trial_purchase":
            switch (type) {
                case "trial":
                    switch (subtype) {
                        case "step-1":
                            switch (group) {
                                case "trial_price":
                                    let _trialprice =
                                        data.currencyDisplay + data.trialprice;
                                    let _trialsavingsprice =
                                        data.currencyDisplay +
                                        data.trial_saving_price;
                                    trialprice_1.html(_trialprice);
                                    trialprice_2.html(_trialprice);
                                    trialprice_3.html(_trialprice);
                                    trialprice_4.html(_trialprice);
                                    trialprice_5.html(_trialprice);
                                    trialprice_6.html(_trialprice);
                                    trialpricesaving_1.html(_trialsavingsprice);
                                    trialpricesaving_2.html(_trialsavingsprice);
                                    break;
                                case "blades":
                                    data.forEach(function (x, y) {
                                        if (x.sku === "S6/2018") {
                                            let blades =
                                                '<div onClick="SelectBlade(' +
                                                x.ProductCountryId +
                                                ')" class="blade_' +
                                                x.ProductCountryId +
                                                ' col-4 mr-3 item-selected">' +
                                                '<input type="hidden" class="blade_' +
                                                x.ProductCountryId +
                                                '" value="' +
                                                x.ProductCountryId +
                                                '"/>' +
                                                '<img src="' +
                                                x.img +
                                                '" class="img-fluid" />' +
                                                '<h6 class="text-center"><b>' +
                                                x.sku +
                                                "</b></h6>" +
                                                "</div>";
                                            selectyourbladetype_1.append(
                                                blades
                                            );
                                            selectyourbladetype_2.append(
                                                blades
                                            );
                                        } else {
                                            let blades =
                                                '<div onClick="SelectBlade(' +
                                                x.ProductCountryId +
                                                ')" class="blade_' +
                                                x.ProductCountryId +
                                                ' col-4 mr-3 item-unselected">' +
                                                '<input type="hidden" class="blade_' +
                                                x.ProductCountryId +
                                                '" value="' +
                                                x.ProductCountryId +
                                                '"/>' +
                                                '<img src="' +
                                                x.img +
                                                '" class="img-fluid" />' +
                                                '<h6 class="text-center"><b>' +
                                                x.sku +
                                                "</b></h6>" +
                                                "</div>";
                                            selectyourbladetype_1.append(
                                                blades
                                            );
                                            selectyourbladetype_2.append(
                                                blades
                                            );
                                        }
                                    });
                                    break;
                            }
                            break;
                    }
                    break;
            }
            break;
    }
}

// on load

$(function () {
    let page_url = window.location.pathname;
    let page_product_type_list = page_url.includes("products/select-category");

    if (page_product_type_list === true) {
        loadProductTypes();
    }
});
