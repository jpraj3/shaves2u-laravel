
function addNewAddress(type) {
    let _validation_result = '';
    let form_data = '';
    let template = '';
    $.ajax({
        type: 'GET',
        url: "/templates/popup",
        dataType: "html",
        data: { template_name: 'address', data: null },
        async: false,
        cache: false,
        success: function (data) {
            template = data;
            if (template) {
                Swal.fire({
                    title: '<div class="col-12" style="padding: 60px 30px 0px 30px;"><h1 class="MuliBold fs-20-sm fs-24-md">ADDRESS DETAILS</h1></div>',
                    html: template,
                    width: 'auto',
                    height: '90%',
                    heightAuto: false,
                    backdrop: true,
                    background: 'white',
                    position: 'center',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: 'Save',
                    customClass: {
                        confirmButton: 'btn btn-load-more',
                    },
                    preConfirm: () => {
                        let _check = popup_address_Validation();
                        _validation_result = _check;
                        form_data = _check[2]
                        return _check[0];
                    }
                }).then((result) => {
                    // console.log(_validation_result);
                    if (result.value) {
                        if (_validation_result[1] !== null && _validation_result[1] === "delivery and shipping") {
                            p_address_actions_popup("save", "delivery and shipping", form_data);
                        } else if (_validation_result[1] !== null && _validation_result[1] === "delivery only") {
                            // console.log("222");
                            p_address_actions_popup("save", "delivery only", form_data);
                        } else {
                            return 0;
                        }

                        // Swal.fire(
                        //     'Address added!',
                        //     'success'
                        // )
                    }
                })
            }
        }
    });
}

function defaultcardupdated(data) {
    Swal.fire(
        'XXXX XXXX XXXX ' + data.cardNumber,
        'has been set as default.',
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function defaultaddressupdated(data) {
    Swal.fire(
        'Address',
        'has been set as default.',
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function carddeleted_popup(data) {
    Swal.fire(
        'Card deleted!',
        '',
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function addNewCard_Popup(data, success) {

    if (success == 1) {
        Swal.fire(
            'XXXX XXXX XXXX ' + data.cardNumber + ' has been added.',
            '',
            'success'
        ).then((result) => {
            window.location.reload();
        })
    } else if (success == 0) {
        Swal.fire(
            'Add Card failed.',
            '',
            'error'
        ).then((result) => {
            window.location.reload();
        })
    }

}


