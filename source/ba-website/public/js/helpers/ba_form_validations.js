// Initialize Elements

let _register_checkout_valid = false;
let _register_valid = false;
let _login_checkout_valid = false;
let _login_valid = false;
let _user_profile_valid = false;
let _user_profile_delivery_valid = false;
let _user_profile_billing_valid = false;
let _user_profile_cards_valid = false;
let _user_profile_delivery_valid_popup = false;
let _user_profile_billing_valid_popup = false;

$.fn.extend({
    trackChanges: function () {
        $(":input", this).change(function () {
            $(this.form).data("changed", true);
        });
    }
    ,
    isChanged: function () {
        return this.data("changed");
    }
});

function validateRegistration(element_name) {
    let birthday_rules = {
        required: true,
    };

    $('#' + element_name).validate({
        groups: {
            birth_date: "day-of-birth month-of-birth year-of-birth",
            terms_consent: "tos_agree privacy_policy_agree personal_info_agree min_age_agree"
        },
        rules: {
            name: {
                required: true,
                maxlength: 100
            },
            password: {
                required: true,
                minlength: 8,
            },
            email: {
                required: true,
                email: true,
                remote: function () {
                    remote_data = {
                        url: API_URL + '/check/unique',
                        type: "post"
                    };
                    if (typeof formMode !== 'undefined') {
                        if (formMode === "register")  {
                            return remote_data;
                        }
                        else {
                            return true;
                        }
                    } 
                    else {
                        return remote_data;
                    }
                }
            },
            "day-of-birth": birthday_rules,
            "month-of-birth": birthday_rules,
            "year-of-birth": birthday_rules,
            tos_agree: {
                required: true
            },
            privacy_policy_agree: {
                required: true
            },
            personal_info_agree: {
                required: true
            },
            min_age_agree: {
                required: true
            },
        },

        messages: {
            name:
            {
                required: trans('validation.custom.validation.name.required', {}),
                maxlength: trans('validation.custom.validation.name.maxlength', {attribute: "name", length: 100})
            },
            email: {
                required: trans('validation.custom.validation.email.required', {}),
                email: trans('validation.custom.validation.email.required', {}),
                maxlength: trans('validation.custom.validation.email.maxlength', {attribute: "email", length: 100}),
                remote: trans('validation.custom.validation.email.email_exists', {}),
            },
            password: {
                required: trans('validation.custom.validation.password.required', {}),
                maxlength: trans('validation.custom.validation.password.maxlength', {attribute: "password", length: 100})
            },
            "day-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            "month-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            "year-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            tos_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            privacy_policy_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            personal_info_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            min_age_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "day-of-birth" || element.attr("name") == "month-of-birth" || element.attr("name") == "year-of-birth") {
                error.appendTo('#birthday_error');
            } else if (element.attr("name") == "name") {
                error.appendTo('#name_error');
            } else if (element.attr("name") == "password") {
                error.appendTo('#password_error');
            } else if (element.attr("name") == "email") {
                error.appendTo('#email_error');
            } else if (element.attr("name") == "tos_agree" || element.attr("name") == "privacy_policy_agree" || element.attr("name") == "personal_info_agree" ||  element.attr("name") == "min_age_agree") {
                error.appendTo('#required_checked_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function validateLogin(element_name) {
    $('#' + element_name).validate({
        rules: {
            password: {
                required: true,
                minlength: 8,
            },
            email: {
                required: true,
                email: true,
            },
        },
        messages: {
            email: {
                required: trans('validation.custom.validation.email.required', {}),
                email: trans('validation.custom.validation.email.required', {})
            },
            password: {
                required: trans('validation.custom.validation.password.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "password") {
                error.appendTo('#password_error');
            } else if (element.attr("name") == "email") {
                error.appendTo('#email_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function validateBALogin(element_name) {
    $('#' + element_name).validate({
        rules: {
            badgeId: {
                required: true,
            },
            icNumber: {
                required: true,
            },
        },
        messages: {
            badgeId: {
                required: trans('validation.custom.validation.badgeId.required', {})
            },
            icNumber: {
                required: trans('validation.custom.validation.icNumber.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "badgeId") {
                error.appendTo('#badgeId_error');
            } else if (element.attr("name") == "icNumber") {
                error.appendTo('#icNumber_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_user(element_name) {
    let birthday_rules = {
        required: true,
    };

    $('#' + element_name).validate({
        groups: {
            birth_date: "pu_e_birthday_day pu_e_birthday_month pu_e_birthday_year"
        },
        rules: {
            pu_e_password: {
                required: function () {
                    return $('#pu_e_password').val().length > 0;
                },
                minlength: function () {
                    if ($('#pu_e_password').val().length > 0) {
                        return 8;
                    }
                },
            },
            pu_e_fullname: {
                required: true,
                maxlength: 100
            },
            pu_e_phone: {
                required: true,
            },
            pu_e_birthday_day: birthday_rules,
            pu_e_birthday_month: birthday_rules,
            pu_e_birthday_year: birthday_rules
        },

        messages: {
            pu_e_fullname:
            {
                required: trans('validation.custom.validation.name.required', {}),
                maxlength: trans('validation.custom.validation.name.maxlength', { attribute: name, max: 100 })
            },
            pu_e_password: {
                required: trans('validation.custom.validation.password.required', {}),
                minlength: trans('validation.custom.validation.password.minlength', { minlength: 8 })
            },
            pu_e_phone: {
                required: trans('validation.custom.validation.contact_number.required', {}),
            },
            pu_e_birthday_day: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            pu_e_birthday_month: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            pu_e_birthday_year: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pu_e_birthday_day" || element.attr("name") == "pu_e_birthday_month" || element.attr("name") == "pu_e_birthday_year") {
                error.appendTo('#pu_e_birthday_error');
            } else if (element.attr("name") == "pu_e_fullname") {
                error.appendTo('#pu_e_fullname_error');
            } else if (element.attr("name") == "pu_e_phone") {
                error.appendTo('#pu_e_phone_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_delivery(element_name) {
    $('#' + element_name).validate({
        rules: {
            pd_e_ssn: {
                required: true
            },
            pd_e_address: {
                required: true
            },
            pd_e_city: {
                required: true
            },
            pd_e_portalCode: {
                required: true,
                maxlength: 6
            },
            pd_e_state: {
                required: true
            },
        },

        messages: {
            pd_e_ssn:
            {
                required: trans('validation.custom.validation.address.ssn.required', { attribute: "SSN" }),
            },
            pd_e_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            pd_e_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            pd_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            pd_e_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pd_e_address") {
                error.appendTo('#pd_e_address_error');
            } else if (element.attr("name") == "pd_e_city") {
                error.appendTo('#pd_e_city_error');
            } else if (element.attr("name") == "pd_e_portalCode") {
                error.appendTo('#pd_e_portalCode_error');
            } else if (element.attr("name") == "pd_e_state") {
                error.appendTo('#pd_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_billing(element_name) {
    $('#' + element_name).validate({
        rules: {
            pb_e_address: {
                required: true
            },
            pb_e_city: {
                required: true
            },
            pb_e_portalCode: {
                required: true,
                maxlength: 6
            },
            pb_e_state: {
                required: true
            },
        },

        messages: {
            pb_e_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            pb_e_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            pb_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            pb_e_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pb_e_address") {
                error.appendTo('#pb_e_address_error');
            } else if (element.attr("name") == "pb_e_city") {
                error.appendTo('#pb_e_city_error');
            } else if (element.attr("name") == "pb_e_portalCode") {
                error.appendTo('#pb_e_portalCode_error');
            } else if (element.attr("name") == "pb_e_state") {
                error.appendTo('#pb_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_delivery_popup(element_name) {
    $('#' + element_name).validate({
        rules: {
            pd_e_ssn_popup: {
                required: true
            },
            pd_e_address_popup: {
                required: true
            },
            pd_e_city_popup: {
                required: true
            },
            pd_e_portalCode_popup: {
                required: true,
                maxlength: 6
            },
            pd_e_state_popup: {
                required: true
            },
        },

        messages: {
            pd_e_ssn_popup:
            {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            pd_e_address_popup:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            pd_e_city_popup: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            pd_e_portalCode_popup: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            pd_e_state_popup: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pd_e_address_popup") {
                error.appendTo('#pd_e_address_popup_error');
            } else if (element.attr("name") == "pd_e_city_popup") {
                error.appendTo('#pd_e_city_popup_error');
            } else if (element.attr("name") == "pd_e_portalCode_popup") {
                error.appendTo('#pd_e_portalCode_popup_error');
            } else if (element.attr("name") == "pd_e_state_popup") {
                error.appendTo('#pd_e_state_popup_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_billing_popup(element_name) {
    $('#' + element_name).validate({
        rules: {
            pb_e_address_popup: {
                required: true
            },
            pb_e_city_popup: {
                required: true
            },
            pb_e_portalCode_popup: {
                required: true,
                maxlength: 6
            },
            pb_e_state_popup: {
                required: true
            },
        },

        messages: {
            pb_e_address_popup:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            pb_e_city_popup: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            pb_e_portalCode_popup: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            pb_e_state_popup: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pb_e_address_popup") {
                error.appendTo('#pb_e_address_popup_error');
            } else if (element.attr("name") == "pb_e_city_popup") {
                error.appendTo('#pb_e_city_popup_error');
            } else if (element.attr("name") == "pb_e_portalCode_popup") {
                error.appendTo('#pb_e_portalCode_popup_error');
            } else if (element.attr("name") == "pb_e_state_popup") {
                error.appendTo('#pb_e_state_popup_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_cards(element_name) {
    $('#' + element_name).validate({
        rules: {
            pc_e_cardnumber_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#pc_e_cardNumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            pc_e_expiry: {
                required: true,
                minlength: 3
            },
            pc_e_cvv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            pc_e_cardnumber_masked:
            {
                required: trans('validation.custom.validation.card.cardnumber.required', {}),
                minlength: function() {
                    var cardInit = $("#pc_e_cardNumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                    }
                    else {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                    }
                }
            },
            pc_e_expiry: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            pc_e_cvv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pc_e_cardnumber") {
                error.appendTo('#pc_e_cardnumber_error');
            } else if (element.attr("name") == "pc_e_expiry") {
                error.appendTo('#pc_e_expiry_error');
            } else if (element.attr("name") == "pc_e_cvv") {
                error.appendTo('#pc_e_cvv_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_cards(element_name) {
    // console.log(element_name);
    $('#' + element_name).validate({
        rules: {
            spc_e_cardNumber_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#spc_e_cardNumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            spc_e_expirydate: {
                required: true,
                minlength: 4
            },
            spc_e_ccv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            spc_e_cardNumber_masked:
            {
                required: trans('validation.custom.validation.card.cardnumber.required', {}),
                minlength: function() {
                    var cardInit = $("#spc_e_cardNumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                    }
                    else {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                    }
                }
            },
            spc_e_expirydate: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            spc_e_ccv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "spc_e_cardNumber") {
                error.appendTo('#spc_e_cardNumber_error');
            } else if (element.attr("name") == "spc_e_expirydate") {
                error.appendTo('#spc_e_expirydate_error');
            } else if (element.attr("name") == "spc_e_ccv") {
                error.appendTo('#spc_e_ccv_error2');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_delivery(element_name) {
    $('#' + element_name).validate({
        rules: {
            spd_e_ssn: {
                required: true
            },
            spd_e_address: {
                required: true
            },
            spd_e_city: {
                required: true
            },
            spd_e_portalCode: {
                required: true,
                maxlength: 6
            },
            spd_e_state: {
                required: true
            },
        },

        messages: {
            spd_e_ssn: 
            {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            spd_e_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            spd_e_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            spd_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            spd_e_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "spd_e_address") {
                error.appendTo('#spd_e_address_error');
            } else if (element.attr("name") == "spd_e_city") {
                error.appendTo('#spd_e_city_error');
            } else if (element.attr("name") == "spd_e_portalCode") {
                error.appendTo('#spd_e_portalCode_error');
            } else if (element.attr("name") == "spd_e_state") {
                error.appendTo('#spd_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_billing(element_name) {
    $('#' + element_name).validate({
        rules: {
            spb_e_address: {
                required: true
            },
            spb_e_city: {
                required: true
            },
            spb_e_portalCode: {
                required: true,
                maxlength: 6
            },
            spb_e_state: {
                required: true
            },
        },

        messages: {
            spb_e_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "address" }),
            },
            spb_e_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "city" }),
            },
            spb_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            spb_e_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "spb_e_address") {
                error.appendTo('#spb_e_address_error');
            } else if (element.attr("name") == "spb_e_city") {
                error.appendTo('#spb_e_city_error');
            } else if (element.attr("name") == "spb_e_portalCode") {
                error.appendTo('#spb_e_portalCode_error');
            } else if (element.attr("name") == "spb_e_state") {
                error.appendTo('#spb_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_checkout_address(element_name) {

    let mindigits = 0;
    let maxdigits = 100;
    let allowz = true;
    let allowposcode = true;

    checkCountryId = userCountryId ? userCountryId : sellerCountryId;

    if (checkCountryId) {
        if (checkCountryId == 1) {
            mindigits = 9;
            maxdigits = 10;
        } else if (checkCountryId == 2 || userCountryId == 7) {
            mindigits = 8;
            maxdigits = 8;
        } else if (checkCountryId == 8) {
            mindigits = 11;
            maxdigits = 11;
        } else if (checkCountryId == 9) {
            mindigits = 9;
            maxdigits = 9;
        }

        if (checkCountryId == 8) {
            allowz = true;
        }
        else {
            allowz = false;
        }
        if (checkCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }

    $.validator.addMethod(
        "allowzero",
        function(value,element) {
            return (value.charAt(0) != '0');
        }, trans('validation.custom.validation.address.phone.allowzero', {})
    );

    $('#' + element_name).validate({
        rules: {
            delivery_name: {
                required: true,
                maxlength: 100
            },
            SSN: {
                required: true
            },
            delivery_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            delivery_address: {
                required: true,
                maxlength: 100
            },
            delivery_city: {
                required: true,
                maxlength: 100
            },
            delivery_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            delivery_state: {
                required: true
            },
            billing_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            billing_address: {
                required: true,
                maxlength: 100
            },
            billing_city: {
                required: true,
                maxlength: 100
            },
            billing_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            billing_state: {
                required: true
            },
        },

        messages: {
            delivery_name:
            {
                required: trans('validation.custom.validation.address.name.required', { attribute: "recipient name" }),
                maxlength: trans('validation.custom.validation.address.name.maxlength', {attribute: "recipient name", length: 100})
            },
            SSN: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            delivery_phone: {
                required: trans('validation.custom.validation.address.phone.required', { attribute: "phone" }),
                minlength: trans('validation.custom.validation.address.phone.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.address.phone.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.address.phone.allowzero', {})
            },
            delivery_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "Unit No./Street/Area" }),
                maxlength: trans('validation.custom.validation.address.address.maxlength', {attribute: "Unit No./Street/Area", length: 100})
            },
            delivery_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "town" }),
                maxlength: trans('validation.custom.validation.address.city.maxlength', {attribute: "town", length: 100})
            },
            delivery_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "postcode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "postcode" })
            },
            delivery_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            },
            billing_phone: {
                required: trans('validation.custom.validation.address.phone.required', { attribute: "phone" }),
                minlength: trans('validation.custom.validation.address.phone.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.address.phone.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.address.phone.allowzero', {})
            },
            billing_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "Unit No./Street/Area" }),
                maxlength: trans('validation.custom.validation.address.address.maxlength', {attribute: "Unit No./Street/Area", length: 100})
            },
            billing_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "town" }),
                maxlength: trans('validation.custom.validation.address.city.maxlength', {attribute: "town", length: 100})
            },
            billing_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "postcode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "postcode" })
            },
            billing_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "delivery_name") {
                error.appendTo('#pd_name_error');
            } else if (element.attr("name") == "delivery_phone") {
                error.appendTo('#pd_phone_error');
            } else if (element.attr("name") == "delivery_address") {
                error.appendTo('#pd_address_error');
            } else if (element.attr("name") == "delivery_city") {
                error.appendTo('#pd_city_error');
            } else if (element.attr("name") == "delivery_postcode") {
                error.appendTo('#pd_portalCode_error');
            } else if (element.attr("name") == "delivery_state") {
                error.appendTo('#pd_state_error');
            } else if (element.attr("name") == "billing_phone") {
                error.appendTo('#pb_phone_error');
            } else if (element.attr("name") == "billing_address") {
                error.appendTo('#pb_address_error');
            } else if (element.attr("name") == "billing_city") {
                error.appendTo('#pb_city_error');
            } else if (element.attr("name") == "billing_postcode") {
                error.appendTo('#pb_portalCode_error');
            } else if (element.attr("name") == "billing_state") {
                error.appendTo('#pb_state_error');
            } else {
                alert(error);
            }
        }
    });
}

function v_checkout_e_address(element_name) {

    let mindigits = 0;
    let maxdigits = 100;
    let allowz = true;
    let allowposcode = true;

    checkCountryId = userCountryId ? userCountryId : sellerCountryId

    if (checkCountryId) {
        if (checkCountryId == 1) {
            mindigits = 9;
            maxdigits = 10;
        } else if (checkCountryId == 2 || userCountryId == 7) {
            mindigits = 8;
            maxdigits = 8;
        } else if (checkCountryId == 8) {
            mindigits = 11;
            maxdigits = 11;
        } else if (checkCountryId == 9) {
            mindigits = 9;
            maxdigits = 9;
        }

        if (checkCountryId == 8) {
            allowz = true;
        }
        else {
            allowz = false;
        }

        if (checkCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }

    $.validator.addMethod(
        "allowzero",
        function(value,element) {
            return (value.charAt(0) != '0');
        }, trans('validation.custom.validation.address.phone.allowzero', {})
    );

    $('#' + element_name).validate({
        rules: {
            delivery_name: {
                required: true,
                maxlength: 100
            },
            SSN: {
                required: true,
            },
            delivery_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            delivery_address: {
                required: true,
                maxlength: 100
            },
            delivery_city: {
                required: true,
                maxlength: 100
            },
            delivery_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            delivery_state: {
                required: true
            },
            billing_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            billing_address: {
                required: true,
                maxlength: 100
            },
            billing_city: {
                required: true,
                maxlength: 100
            },
            billing_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            billing_state: {
                required: true
            }
        },

        messages: {
            delivery_name:
            {
                required: trans('validation.custom.validation.address.name.required', { attribute: "recipient name" }),
                maxlength: trans('validation.custom.validation.address.name.maxlength', {attribute: "recipient name", length: 100})
            },
            SSN: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            delivery_phone: {
                required: trans('validation.custom.validation.address.phone.required', { attribute: "phone" }),
                minlength: trans('validation.custom.validation.address.phone.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.address.phone.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.address.phone.allowzero', {})
            },
            delivery_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "Unit No./Street/Area" }),
                maxlength: trans('validation.custom.validation.address.address.maxlength', {attribute: "Unit No./Street/Area", length: 100})
            },
            delivery_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "town" }),
                maxlength: trans('validation.custom.validation.address.city.maxlength', {attribute: "town", length: 100})
            },
            delivery_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "postcode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "postcode" })
            },
            delivery_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            },
            billing_phone: {
                required: trans('validation.custom.validation.address.phone.required', { attribute: "phone" }),
                minlength: trans('validation.custom.validation.address.phone.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.address.phone.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.address.phone.allowzero', {})
            },
            billing_address:
            {
                required: trans('validation.custom.validation.address.address.required', { attribute: "Unit No./Street/Area" }),
                maxlength: trans('validation.custom.validation.address.address.maxlength', {attribute: "Unit No./Street/Area", length: 100})
            },
            billing_city: {
                required: trans('validation.custom.validation.address.city.required', { attribute: "town" }),
                maxlength: trans('validation.custom.validation.address.city.maxlength', {attribute: "town", length: 100})
            },
            billing_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', { attribute: "postcode" }),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "postcode" })
            },
            billing_state: {
                required: trans('validation.custom.validation.address.state.required', { attribute: "state" })
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "delivery_name") {
                error.appendTo('#pd_name_e_error');
            } else if (element.attr("name") == "delivery_phone") {
                error.appendTo('#pd_phone_e_error');
            } else if (element.attr("name") == "delivery_address") {
                error.appendTo('#pd_address_e_error');
            } else if (element.attr("name") == "delivery_city") {
                error.appendTo('#pd_city_e_error');
            } else if (element.attr("name") == "delivery_postcode") {
                error.appendTo('#pd_portalCode_e_error');
            } else if (element.attr("name") == "delivery_state") {
                error.appendTo('#pd_state_e_error');
            } else if (element.attr("name") == "billing_phone") {
                error.appendTo('#pb_phone_e_error');
            } else if (element.attr("name") == "billing_address") {
                error.appendTo('#pb_address_e_error');
            } else if (element.attr("name") == "billing_city") {
                error.appendTo('#pb_city_e_error');
            } else if (element.attr("name") == "billing_postcode") {
                error.appendTo('#pb_portalCode_e_error');
            } else if (element.attr("name") == "billing_state") {
                error.appendTo('#pb_state_e_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function validateEvents(element_name) {
    $('#' + element_name).validate({
        rules: {
            ba_event_location_code: {
                required: true
            }
        },
        messages: {
            ba_event_location_code: {
                required:  trans('validation.custom.validation.event.locationcode.required')
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "ba_event_location_code") {
                error.appendTo('#event_code_error');
            } else if (element.attr("name") == "ba_channel_type") {
                error.appendTo('#channel_code_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_checkout_cards(element_name) {
    // console.log(element_name);
    $('#' + element_name).validate({
        rules: {
            card_number_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#card-number").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            card_expiry_date: {
                required: true,
                minlength: 7
            },
            card_cvv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            card_number_masked:
            {
                required: trans('validation.custom.validation.card.cardnumber.required', {}),
                minlength: function() {
                    var cardInit = $("#card-number").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                    }
                    else {
                        return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                    }
                }
            },
            card_expiry_date: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            card_cvv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "card_number") {
                error.appendTo('#c_cardNumber_error');
            } else if (element.attr("name") == "card_expiry_date") {
                error.appendTo('#c_expirydate_error');
            } else if (element.attr("name") == "card_cvv") {
                error.appendTo('#c_ccv_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}


////////////////////////////////// ON LOAD FUNCTIONS ///////////////////////////////////
$(function() {
    // Display error message if any
    // DisplayPaymentError();

    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    if (typeof(loginStatus) !== 'undefined' && loginStatus === "post-login") {
        // Show orange tick
        // $(".orange-tick-personal").removeClass("hidden");

        // $(".orange-tick-address").removeClass("hidden");

        if (cards.length > 0) {
            $("#edit-card-container").removeClass("hidden");
            let cardOptionList = [];
            cards.forEach(card => {

                var selected = '';
                if (card.isDefault == 1) {
                    selected = "selected";
                    $("#view-expiry-date").val(card.expiredMonth + " / " + card.expiredYear);
                    $("#view-cvv").val(". . .");
                    ChangeCardBackground(card.branchName);
                }
                cardOptionList.push("<option value='" + JSON.stringify(card) + "' " + selected + ">xxxx xxxx xxxx " + card.cardNumber + "</option>");
            });
            $("#card-dropdown").html(cardOptionList.join());
        } else {
            $("#add-card-container").removeClass("hidden");
        }
    }


    $("#card-dropdown").change(function() {
        var selectedCard = JSON.parse(this.value);
        $("#view-expiry-date").val(selectedCard.expiredMonth + " / " + selectedCard.expiredYear);
        onUpdateCardSelection(selectedCard.id);
    });

    $('#btn-create-account-desktop').click(function() {
        // validateRegistration("form_register");
        onLoginOrRegister();
    });

    $('.btn-sign-in').click(function() {
        onChangeLoginOrRegisterForm();
    })

    $('.btn-create-one').click(function() {
        onChangeLoginOrRegisterForm();
    })

    $('#card-number-masked').keydown(function (event) {
        $(this).val(CreditCardFormat($('#card-number').val()));
    });

    $('#card-number-masked').on('input',function (event) {
        $('#card-number').val(CreditCardFormat($(this).val()));
        $(this).val(CreditCardFormatMasked($('#card-number').val()));
    });

    $('#card-number-masked').blur(function (event) {
        // deselectd control
        $(this).val(CreditCardMaskAll($(this).val()));
    });
    
    $('.card_expiry_date').keyup(function() {
        $(this).val(ExpiryDateFormat($(this).val()));
    })

    $(".number-only").keypress(function(value) {
        if ((value.which < 48 || value.which > 57) && (value.which !== 8) && (value.which !== 0)) {
            return false;
        }
        return true;
    });

    $("#button-add-new-card").click(function() {
        $("#edit-card-container").addClass("hidden");
        $("#add-card-container").removeClass("hidden");
        $("#button-cancel-add-new-card").removeClass("hidden");
    });

    $("#button-cancel-add-new-card").click(function() {
        $("#edit-card-container").removeClass("hidden");
        $("#add-card-container").addClass("hidden");
        $("#button-cancel-add-new-card").addClass("hidden");
    });
})
