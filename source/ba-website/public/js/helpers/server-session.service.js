
// Define methods
var SESSION_SET = "set";
var SESSION_GET = "get";
var SESSION_CLEAR = "clear";

// Define keys
var SESSION_SELECTION_TRIAL_PLAN = "selection_trial_plan";
var SESSION_CHECKOUT_TRIAL_PLAN = "checkout_trial_plan";
var SESSION_SELECTION_CUSTOM_PLAN = "selection_custom_plan";
var SESSION_CHECKOUT_CUSTOM_PLAN = "checkout_custom_plan";
var SESSION_CHECKOUT_ASK = "checkout_ask";
var SESSION_SELECTION_ASK = "checkout_selection_ask";
var SESSION_CHECKOUT_ALACARTE_BA = "checkout_alacarte_ba";
var SESSION_URL_PARAMETERS = "url_parameters";
var SESSION_REFERRAL_PROGRAM_INFO = "referral_program_info";
var SESSION_LOGIN = "isLoggedIn";
var SESSION_EDIT_SHAVE_PLANS = "session_edit_shave_plans";
var SESSION_SELLER_INFO = "seller";
var SESSION_SELECTION_ALACARTE_BA = "selection_alacarte_ba";
var SESSION_USER_INFO = "user";
var SESSION_GENDER_SELECTED = "gender_selected";
var SESSION_SWITCH_PLANS = "trial_switch_plans";
var SESSION_ALACARTE_PRODUCT_DATA = "alacarte_product_data";
var SESSION_SELECTION_PRODUCT_DATA = "selection_product_data";
var TRIAL_CHECKOUT_PAYMENT_FAILED = "trial_checkout_payment_failed";
var ALACARTE_CHECKOUT_PAYMENT_FAILED = "alacarte_checkout_payment_failed";
var SESSION_LOGIN_FAIL = "loginerror";
function session(session_key, method, json_data) {
    return $.ajax({
        url: GLOBAL_URL_V2 + "/ajax/session",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        cache: false,
        data: {
            "session_key": session_key,
            "method": method,
            "data": json_data
        },
    })
}
