
// ===================================================================================================================================
// STORAGE SERVICE
// ===================================================================================================================================

window.addStorage = function (key, value) {
    window.localStorage.setItem(key, value);
}

window.getStorage = function (key) {
    return window.localStorage.getItem(key);
}

window.removeStorage = function (key) {
    window.localStorage.removeItem(key);
}





