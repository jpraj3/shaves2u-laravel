function foldDaumPostcode() {
    // iframe을 넣은 element를 안보이게 한다.
    if(document.getElementById('daum-postcode-wrap-add-delivery')){
        document.getElementById('daum-postcode-wrap-add-delivery').style.display = 'none';
    }
    if(document.getElementById('daum-postcode-wrap-add-billing')){
        document.getElementById('daum-postcode-wrap-add-billing').style.display = 'none';
    }
    if(document.getElementById('daum-postcode-wrap-edit-delivery')){
        document.getElementById('daum-postcode-wrap-edit-delivery').style.display = 'none';
    }
    if(document.getElementById('daum-postcode-wrap-edit-billing')){
        document.getElementById('daum-postcode-wrap-edit-billing').style.display = 'none';
    }
}


function koreanDeliveryAddressPrefill(type) {
    let currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    let docid;
    if(type =="add"){
        docid =   document.getElementById('daum-postcode-wrap-add-delivery');
    }else{
        docid =   document.getElementById('daum-postcode-wrap-edit-delivery');
    }
    let UnitControlsget = '';


    new daum.Postcode({
        oncomplete: function(data) {

            var fullRoadAddr = data.roadAddress;
            var extraRoadAddr = '';


            if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                extraRoadAddr += data.bname;
            }

            if (data.buildingName !== '' && data.apartment === 'Y') {
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }

            if (extraRoadAddr !== '') {
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }

            if (fullRoadAddr !== '') {
                fullRoadAddr += extraRoadAddr;
            }
            
            // if (UnitControls.value) {
            //     UnitControlsget = "," + UnitControls.value;
            // }

            //roadname
            var a_flat ="";
            var e_flat ="";
            var dda = document.getElementById("edit_delivery_address");
            if(dda){
             var edf = document.getElementById("edit_delivery_flat");
           if(edf){
               if(edf.value){
            e_flat = "," + edf.value; 
               }
           }
           var dda1 = document.getElementById("edit_delivery_address1");
           dda1.value = fullRoadAddr;
           dda.value = fullRoadAddr + e_flat;

             var ddc = document.getElementById("edit_delivery_city");
             ddc.value = data.sido;
 
             var dds = document.getElementById("edit_delivery_state");
             dds.value = data.sigungu;
 
             var dpc = document.getElementById("edit_delivery_postcode");
             dpc.value = data.zonecode;
            }
 
           var adda = document.getElementById("add_delivery_address");
            if(adda){
             var adf = document.getElementById("add_delivery_flat");
             if(adf){
                if(adf.value){
              a_flat = "," + adf.value; 
                }
             }
             var adda1 = document.getElementById("add_delivery_address1");
             adda1.value = fullRoadAddr;
             adda.value = fullRoadAddr + a_flat;
             var addc = document.getElementById("add_delivery_city");
             addc.value = data.sido;
 
             var adds = document.getElementById("add_delivery_state");
             adds.value = data.sigungu;
 
             var adpc = document.getElementById("add_delivery_postcode");
             adpc.value = data.zonecode;
            }
   
 
            // iframe을 넣은 element를 안보이게 한다.
            // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
            if(type =="add"){
                docid.style.display = 'none';
            }else{
                docid.style.display = 'none';
            }
         

            // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
            document.body.scrollTop = currentScroll;
        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize: function(size) {
            if(type =="add"){
                docid.style.height = size.height + 'px';
            }else{
                docid.style.height = size.height + 'px';
            }
        },
        width: '100%',
        height: '100%'
    }).embed(docid);

    // iframe을 넣은 element를 보이게 한다.
    docid.style.display = 'block';
}

function koreanBillingAddressPrefill(type) {
    let currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);

    let docid;
    if(type =="add"){
        docid =   document.getElementById('daum-postcode-wrap-add-billing');
    }else{
        docid =   document.getElementById('daum-postcode-wrap-edit-billing');
    }
    let UnitControlsget = '';


    new daum.Postcode({
        oncomplete: function(data) {

            var fullRoadAddr = data.roadAddress;
            var extraRoadAddr = '';


            if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                extraRoadAddr += data.bname;
            }

            if (data.buildingName !== '' && data.apartment === 'Y') {
                extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }

            if (extraRoadAddr !== '') {
                extraRoadAddr = ' (' + extraRoadAddr + ')';
            }

            if (fullRoadAddr !== '') {
                fullRoadAddr += extraRoadAddr;
            }
            // if (UnitControls.value) {
            //     UnitControlsget = "," + UnitControls.value;
            // }

            //roadname
            var a_flat ="";
            var e_flat ="";
            var bba = document.getElementById("edit_billing_address");
           if(bba){
            var ebf = document.getElementById("edit_billing_flat");
            if(ebf){
                if(ebf.value){
             e_flat = "," + ebf.value; 
                }
            }
            var bba1 = document.getElementById("edit_billing_address1");
            bba1.value = fullRoadAddr;
            bba.value = fullRoadAddr + e_flat;

            var bbc = document.getElementById("edit_billing_city");
            bbc.value = data.sido;

            var bbs = document.getElementById("edit_billing_state");
            bbs.value = data.sigungu;

            var bpc = document.getElementById("edit_billing_postcode");
            bpc.value = data.zonecode;
           }

           var abba = document.getElementById("add_billing_address");
           if(abba){
            var abf = document.getElementById("add_billing_flat");
            if(abf){
                if(abf.value){
             a_flat = "," + abf.value; 
                }
            }
            var abba1 = document.getElementById("add_billing_address1");
            abba1.value = fullRoadAddr;
            abba.value = fullRoadAddr  + a_flat;
            var abbc = document.getElementById("add_billing_city");
            abbc.value = data.sido;

            var abbs = document.getElementById("add_billing_state");
            abbs.value = data.sigungu;

            var abpc = document.getElementById("add_billing_postcode");
            abpc.value = data.zonecode;
           }
  


            // iframe을 넣은 element를 안보이게 한다.
            // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
            docid.style.display = 'none';

            // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
            document.body.scrollTop = currentScroll;
        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize: function(size) {
            docid.style.height = size.height + 'px';
        },
        width: '100%',
        height: '100%'
    }).embed(docid);

    // iframe을 넣은 element를 보이게 한다.
    docid.style.display = 'block';
}

function onChangeDeliveryAddressUnitNumber(event) {

    var bba1 = document.getElementById("edit_delivery_address1");
    var abba1 = document.getElementById("add_delivery_address1");
    if(bba1){
        var bba = document.getElementById("edit_delivery_address");
        if(bba1.value){
            if(event)
            {
                bba.value = bba1.value + "," + event;
            }
            else{
                bba.value = bba1.value;
            }
          
        }else{
            if(event)
            {
                bba.value =  "," + event;
            }
        }
     }

     if(abba1){
        var abba = document.getElementById("add_delivery_address");
        if(abba1.value){
            if(event)
            {
                abba.value = abba1.value + "," + event;
            }
            else{
                abba.value = abba1.value;
            }
          
        }else{
            if(event)
            {
                abba.value =  "," + event;
            }
        }
     }

  }

  function onChangeBillingAddressUnitNumber(event) {
    var bba1 = document.getElementById("edit_billing_address1");
    var abba1 = document.getElementById("add_billing_address1");
    if(bba1){
        var bba = document.getElementById("edit_billing_address");
        if(bba1.value){
            if(event)
            {
                bba.value = bba1.value + "," + event;
            }
            else{
                bba.value = bba1.value;
            }
          
        }else{
            if(event)
            {
                bba.value =  "," + event;
            }
        }
     }

     if(abba1){
        var abba = document.getElementById("add_billing_address");
        if(abba1.value){
            if(event)
            {
                abba.value = abba1.value + "," + event;
            }
            else{
                abba.value = abba1.value;
            }
          
        }else{
            if(event)
            {
                abba.value =  "," + event;
            }
        }
     }
  }