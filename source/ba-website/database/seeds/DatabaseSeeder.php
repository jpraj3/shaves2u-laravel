<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Admin 
        $this->call(AdminTblSeeder::class);
        // Ba Website 
        $this->call(SellerUserTblSeeder::class);
        // Ecommerce
        $this->call(UserTblSeeder::class);
        // Products
        $this->call(ProductTblSeeder::class);

        $seeder = [
            ProductTblSeeder::class,
            SellerUserTblSeeder::class
        ];

        foreach ($seeder as $seed) {
            $this->call($seed);
            $this->command->info("");
        }
    }
}
