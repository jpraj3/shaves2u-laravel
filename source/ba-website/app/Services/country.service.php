<?php

namespace App\Services;

use App\Helpers\LaravelHelper;

use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\SupportedLang;
use App\Models\GeoLocation\States;
use DB;

class BACountryService
{
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();

        $this->country = "";
        $this->country_id = "";
        $this->country_iso = "";
        $this->default_lang = "";
        $this->_supportedlangs = "";

        if (session()->get('currentCountry') !=null && session()->get('currentCountry')) {
            if (is_array(session()->get('currentCountry'))) {
                $this->country = json_decode(session()->get('currentCountry'), true);
                $this->country_id = $this->country['id'];
                $this->country_iso = strtoupper($this->country['codeIso']);
                $this->default_lang = strtoupper($this->country['defaultLang']);
            } else {
                $this->country = session()->get('currentCountry');
                $this->country_id = $this->country->id;
                $this->country_iso = strtoupper($this->country->codeIso);
                $this->default_lang = strtoupper($this->country->defaultLang);
            }
        }
        $this->_supportedlangs = $this->supportedLangs($this->country_id);
    }

    public function getAllCountryLang($auth)
    {
        $allcountry = [];
        if($auth){
        $countryDataUrl = Countries::where('countries.isActive', 1)->where('countries.id', $auth->CountryId)
            ->select('countries.*', 'countries.name as cname', 'countries.defaultLang as urlLang', DB::raw('(SELECT sublanguageCode FROM languagedetails WHERE languagedetails.mainlanguageCode = countries.defaultLang and  languagedetails.CountryId = countries.id) as defaultLang'))
            ->get()->toArray();
        $suppcountryDataUrl = SupportedLang::join('countries', 'countries.id', 'supportedlanguages.CountryId')
            ->select('countries.*', 'supportedlanguages.languageCode as urlLang', DB::raw('(SELECT sublanguageCode FROM languagedetails WHERE languagedetails.mainlanguageCode = supportedlanguages.languageCode and  languagedetails.CountryId = supportedlanguages.CountryId) as defaultLang'))
            ->where('countries.isActive', 1)
            ->where('countries.id', $auth->CountryId)
            ->get()->toArray();
     
        foreach ($countryDataUrl as $c) {
            if ($c["defaultLang"]) {
                array_push($allcountry, $c);
                foreach ($suppcountryDataUrl as $s) {
                    if ($c["id"] == $s["id"]) {
                        if ($s["defaultLang"]) {
                            array_push($allcountry, $s);
                        }
                    }
                }
            }
        }
    }
        return $allcountry;
    }

    public function getCountryPhoneExt($countryId)
    {
        // return phone number extension based on countryid
        $countryPhoneData = Countries::where('countries.isActive', 1)
            ->where('countries.id', $countryId)
            ->pluck('countries.callingCode')
            ->first();

        return $countryPhoneData;
    }

    public function supportedLangs($CountryId)
    {
        $_list = SupportedLang::where('CountryId', $CountryId)->get();

        return $_list;
    }

    public function getAllStates($CountryId)
    {
        $_list = States::where('CountryId', $CountryId)->orderByRaw("isDefault desc", "name asc")->get();

        return $_list;
    }

    public function getCurrency()
    {
        return Countries::where('id', $this->country_id)->pluck('currencyCode')->first();
    }

    public function getCurrencyDisplay()
    {
        return Countries::where('id', $this->country_id)->pluck('currencyDisplay')->first();
    }

    public function getCountryAndLangCode()
    {
        $country_details = $this->laravelHelper->ConvertArraytoObject($this->getCountryfromDB($this->country_id));
        $countryInfo = [
            'country_id' => $this->country_id,
            'country_iso' => $this->country_iso,
            'default_lang' => $this->default_lang,
            'supported_langs' => $this->_supportedlangs,
            'data_from_db' => $country_details,
        ];

        return $countryInfo;
    }

    public function getCountryfromDB($id)
    {
        $country_from_db = Countries::where('id', $id)->first();

        return $country_from_db;
    }

    public function shippingFee($countryid, $lang)
    {
        $result = Countries::where('id', $countryid)->select('shippingFee')->first();
        $data = [
            'shippingfee' => $result->shippingFee,
        ];
        return $data;
    }

    public function shippingFeeTrial($countryid, $lang)
    {
        $result = Countries::where('id', $countryid)->select('shippingTrialFee')->first();
        $data = [
            'shippingfee' => $result->shippingFee,
        ];
        return $data;
    }

    public function tax($countryid)
    {
        $result = Countries::where('id', $countryid)->select('taxRate', 'taxName', 'taxCode', 'includedTaxToProduct', 'taxNumber')->first();
        $data = [
            'taxRate' => $result->taxRate,
            'taxName' => $result->taxName,
            'taxCode' => $result->taxCode,
            'includedTaxToProduct' => $result->includedTaxToProduct,
            'taxNumber' => $result->taxNumber,
        ];
        return $data;
    }
}
