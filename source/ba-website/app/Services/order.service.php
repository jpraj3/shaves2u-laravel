<?php

namespace App\Services;

use App\Helpers\LaravelHelper;
use App\Models\GeoLocation\Countries;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class OrderService
{

    public $country;
    public $country_id;
    public $country_iso;

    public function __construct()
    {
        $this->ORDER_LENGTH = 9;
        $this->BULK_ORDER_LENGTH = 8;
        $this->laravelHelper = new LaravelHelper();
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatOrderNumber($order, $country, $isFull)
    {
        if (is_array(session()->get('currentCountry'))) {
            $country = json_decode(session()->get('currentCountry'), true);
            $country_id = $country['id'];
            $country_iso = strtoupper($country['codeIso']);
        } else {
            $country = session()->get('currentCountry');

            $country_id = $country->id;
            $country_iso = strtoupper($country->codeIso);
        }

        $code = $country ? $country->code : 'NaN';
        $countryCode = $country ? $country->codeIso : 'NaN';
        $orderId = $order ? $order->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($order->created_at)));
        }
    }

    public static function formatOrderNumberV2($order, $country, $isFull)
    {
        if (is_array(session()->get('currentCountry'))) {
            $country = json_decode(session()->get('currentCountry'), true);
            $country_id = $country['id'];
            $country_iso = strtoupper($country['codeIso']);
        } else {
            $country = session()->get('currentCountry');

            $country_id = $country->id;
            $country_iso = strtoupper($country->codeIso);
        }

        $ORDER_LENGTH = 9;
        if ($country === null) {
            $country = Countries::where('id', $order->CountryId)->first();
            $code = $country ? $country->code : 'NaN';
            $countryCode = $country ? $country->codeIso : 'NaN';
            $orderId = $order ? $order->id : 'NaN';
            $returnValue = '';
            for ($i = 0; $i < $ORDER_LENGTH - strlen($orderId); $i++) {
                $returnValue = $returnValue . '0';
            }

            if (!$isFull) {
                return $countryCode . $returnValue . $orderId;
            } else {
                return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', Carbon::parse($order->created_at)->format('Ymd')));
            }
        } else {
            $code = $country ? $country->code : 'NaN';
            $countryCode = $country ? $country->codeIso : 'NaN';
            $orderId = $order ? $order->id : 'NaN';
            $returnValue = '';
            for ($i = 0; $i < $ORDER_LENGTH - strlen($orderId); $i++) {
                $returnValue = $returnValue . '0';
            }

            if (!$isFull) {
                return $countryCode . $returnValue . $orderId;
            } else {
                return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', Carbon::parse($order->created_at)->format('Ymd')));
            }
        }
    }

    public function formatDate($datetime)
    {
        $isArray = is_array($datetime);

        if (!$isArray) {
            $datetime = Carbon::parse($datetime)->format('Ymd');

        } else {
            $datetime = Carbon::parse($datetime)->format('Ymd');
        }

        return $datetime;
    }

    /**
     * Extract order number for display format
     * Example MY000000001-20171127 -> 000000001
     * @param {*} orderNumber
     */
    public function getOrderNumber($orderNumber)
    {
        Log::info('getOrderNumber start');
        Log::info('param: orderNumber = ' . $orderNumber);
        return substr($orderNumber, 2, $this->ORDER_LENGTH);
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatInvoiceNumber($order)
    {
        Log::info('formatInvoiceNumber start');
        $code = $order->Country ? $order->Country->code : $order->region;
        $countryCode = $order->Country ? $order->Country->code : $order->region;
        $orderId = $order->orderId ? $order->orderId : $order->id;
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        return 'S2U-' . $countryCode . '-' . $returnValue . $orderId;
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatBulkOrderNumber($bulkorder, $country, $isFull)
    {
        Log::info('formatBulkOrderNumber start');
        $countryCode = $country ? $country->codeIso : 'NaN';
        $orderId = $bulkorder->id ? $bulkorder->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->BULK_ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . 'B' . $returnValue . $orderId;
        } else {
            return $countryCode . 'B' . $returnValue . $orderId . '-' . $this->formatDate($bulkorder->created_at);
        }
    }

    /**
     * Extract order number for display format
     * Example MY000000001-20171127 -> 000000001
     * @param {*} orderNumber
     */
    public function getBulkOrderNumber($orderNumber)
    {
        Log::info('getBulkOrderNumber start');
        Log::info('param: orderNumber = $orderNumber');
        if (strpos($orderNumber, "-") !== false) {
            return substr($orderNumber, 3, strpos($orderNumber, "-") - 3);
        } else {
            return substr($orderNumber, 3, $this->BULK_ORDER_LENGTH);
        }
    }

    /**
     * Check type of order base on the order number
     * MY0000000001-20170101 -> order
     * MYB000000001-20170101 -> bulkOrder
     * @param {*} orderNumber
     */
    public function checkOrderNumberType($orderNumber)
    {
        Log::info('checkOrderNumberType start');
        Log::info('param: orderNumber =' . $orderNumber);
        if ($orderNumber[2] === 'B') {
            return 'bulkOrder';
        } else {
            return 'order';
        }
    }
}
