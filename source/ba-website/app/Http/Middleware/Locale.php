<?php

namespace App\Http\Middleware;

use App;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\LanguageDetails;
use App\Models\Plans\TrialHandleTypes;
use App\Models\Products\Product;
use App\Services\BACountryService;
use Auth;
use Closure;
use Config;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Input;
use Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
        // $this->country_service = new BACountryService();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }

    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $url = explode("/", $_SERVER['REQUEST_URI']);
            $spliturl = '';
            $urllang = '';
            $urlcountry = '';
            $check = 'true';
            $countryget = '';
            if ($url) {

                if (count($url) >= 2) {
                    if ($_SERVER["SERVER_NAME"] === 'dcistaging.com') {
                        $spliturl = $url[3];
                    } else {

                        $spliturl = $url[1];
                    }

                    if ($spliturl) {
                        if (strpos($spliturl, '-') !== false) {
                            $splitLangAndCountry = explode("-", $spliturl);
                            $urllang = strtolower($splitLangAndCountry[0]);
                            $urlcountry = strtolower($splitLangAndCountry[1]);
                        } else {
                            // return redirect('/');
                        }
                    }
                }
            }

            // Check if current country session Exist
            $countrySession = session()->get('currentCountry');

            // If Country session data does not exist
            if (!isset($countrySession)) {

                // Getting Client Public IP
                $languageDetailsData = '';
                $countryDataUrl = Countries::leftJoin('supportedlanguages', 'supportedlanguages.CountryId', 'countries.id')
                    ->where('countries.id', Auth::user()->CountryId)
                    ->where('countries.isActive', 1)
                    ->select('countries.id as countryid', 'countries.*', 'supportedlanguages.*')
                    ->first();

                if ($countryDataUrl) {
                    if ($countryDataUrl->languageCode) {
                        if (strtolower($countryDataUrl->languageCode) == $urllang) {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->languageCode)
                                ->first();
                        } else {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                                ->first();
                        }
                    } else {
                        $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                            ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                            ->first();
                    }

                    if ($languageDetailsData) {
                        $sessiondata = [
                            'id' => $countryDataUrl->countryid,
                            'name' => $countryDataUrl->name,
                            'code' => $countryDataUrl->code,
                            'codeIso' => $countryDataUrl->codeIso,
                            'defaultLang' => $languageDetailsData->sublanguageCode,
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                        $clientCountryJson = json_encode($sessiondata);
                        $clientCountryArray = json_decode($clientCountryJson, true);
                        $countryLang = $languageDetailsData->sublanguageCode;
                    }
                    // Store url parameters in session
                    // // $this->checkUTMSources();
                    $clientCountryJson = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode($clientCountryJson));

                    // Store Client Country JSON Data to Session Storage
                    session()->put('currentCountry', $clientCountryJson);

                    // Store Client Locale in Session Storage
                    session()->put('currentLocale', $countryLang);

                    // Set Client Locale based on $countryLang
                    App::setLocale(strtolower($countryLang));
                } else {
                    session()->forget('currentCountry');
                    session()->forget('currentLocale');
                    $this->guard()->logout();
                }
            } // If Country session data does exist
            else {
                // Pass Session Data into variable
                if (is_array(session()->all())) {
                    $clientCountryArray = (session()->all()['currentCountry']);
                } else {
                    $clientCountryArray = session()->all()->currentCountry;
                }

                if (is_array($clientCountryArray)) {
                    $getcodeIso = $clientCountryArray['codeIso'];
                    $geturllang = $clientCountryArray['urlLang'];
                    $getid = $clientCountryArray['id'];
                } else {
                    $getcodeIso = $clientCountryArray->codeIso;
                    $geturllang = $clientCountryArray->urlLang;
                    $getid = $clientCountryArray->id;
                }
                $countryLang = strtolower(session()->get('currentLocale'));
                $mainlang = strtolower($geturllang);

                if (($urllang && $urlcountry) && (($mainlang != $urllang) || (strtolower($getcodeIso) != $urlcountry))) {

                    $countryDataUrl = Countries::leftJoin('supportedlanguages', 'supportedlanguages.CountryId', 'countries.id')
                        ->where(function ($query) use ($urllang) {
                            $query->orWhereRaw('LOWER(countries.defaultLang) = ?', $urllang)
                                ->orWhereRaw('LOWER(supportedlanguages.languageCode) = ?', $urllang);
                        })
                        ->whereRaw('LOWER(countries.codeIso) = ?', $urlcountry)
                        ->where('countries.id', Auth::user()->CountryId)
                        ->where('countries.isActive', 1)
                        ->select('countries.id as countryid', 'countries.*', 'supportedlanguages.*')
                        ->first();

                    if ($countryDataUrl) {
                        if ($countryDataUrl->languageCode) {
                            if (strtolower($countryDataUrl->languageCode) == $urllang) {
                                $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                    ->where('mainlanguageCode', $countryDataUrl->languageCode)
                                    ->first();
                            } else {
                                $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                    ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                                    ->first();
                            }
                        } else {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                                ->first();
                        }

                        if ($languageDetailsData) {
                            $sessiondata = [
                                'id' => $countryDataUrl->countryid,
                                'name' => $countryDataUrl->name,
                                'code' => $countryDataUrl->code,
                                'codeIso' => $countryDataUrl->codeIso,
                                'defaultLang' => $languageDetailsData->sublanguageCode,
                                'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                            ];
                            $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                            // Store url parameters in session
                            // // $this->checkUTMSources();
                            $sessiondata = \App\Helpers\LaravelHelper::ConvertArraytoObject($sessiondata);

                            session()->put('currentCountry', $sessiondata);
                            session()->put('currentLocale', $languageDetailsData->sublanguageCode);

                            if (is_array(session()->all())) {
                                $clientCountryArray = (session()->all()['currentCountry']);
                            } else {
                                $clientCountryArray = session()->all()->currentCountry;
                            }

                            $countryLang = $languageDetailsData->sublanguageCode;
                        } else {
                            $check = 'false';
                        }
                    } else {
                        $check = 'false';
                    }
                }
                // Get country Language based on locale
                App::setLocale(strtolower($countryLang));
            }

            // Get SEO Info for Current Country
            // $clientCountrySeoData = json_decode(json_encode(MetaTags::where('CountryId', $clientCountryArray['id'])->first()), true);

            // Remove Default Title from SEO tools (Library Bug workaround)
            config(['seotools.meta.defaults.title' => false]);
            $country_service = new BACountryService();
            $allcountry = $country_service->getAllCountryLang(Auth::user());
            $callcode = $country_service->getCountryPhoneExt(Auth::user()->CountryId);

            // Set Meta Information
            // SEOMeta::setTitle($clientCountrySeoData['metatitle']);
            // SEOMeta::setDescription($clientCountrySeoData['metaDescription']);

            // // Set og Meta Info
            // OpenGraph::setDescription($clientCountrySeoData['metaDescription']);
            // OpenGraph::setTitle($clientCountrySeoData['ogTitle']);
            // OpenGraph::setUrl($clientCountrySeoData['ogUrl']);
            // OpenGraph::addProperty('type', $clientCountrySeoData['ogType']);
            // OpenGraph::addImage($clientCountrySeoData['ogImage']);
            if (is_array($clientCountryArray)) {
                $getnewcodeIso = $clientCountryArray['codeIso'];
            } else {
                $getnewcodeIso = $clientCountryArray->codeIso;
            }

            $this->initSalesNotificationProductList($countryLang);
            view()->share('currentCountryIso', strtolower($getnewcodeIso));
            view()->share('url', strtolower($urllang));
            view()->share('langCode', strtolower($countryLang));
            view()->share('allcountry', $allcountry);
            view()->share('callcode', $callcode);
            //allow BACountryService methods to be used in blade
            view()->share('country_service', $country_service);
            // gather plan handle types
            if (is_array($clientCountryArray)) {
                $country_handles = TrialHandleTypes::join('products', 'products.id', 'trial_handle_types.ProductIds')
                    ->where('trial_handle_types.CountryId', $clientCountryArray['id'])
                    ->where('trial_handle_types.gender', 'male')
                    ->first();
            } else {
                $country_handles = TrialHandleTypes::join('products', 'products.id', 'trial_handle_types.ProductIds')
                    ->where('trial_handle_types.CountryId', $clientCountryArray->id)
                    ->where('trial_handle_types.gender', 'male')
                    ->first();
            }

            $country_handles ? session()->put('country_handles', $country_handles) : '';
            if ($check == "false") {
                return redirect('/');
            }
        }
        // $this->checkUTMSources();
        return $next($request);
    }

    public function checkUTMSources()
    {
        // get utm_medium
        $params = Input::all();
        $test2 = "https://shaves2u.com/users/reset-password?email=kkshore@hotmail.com&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjIwMTktMDgtMjJUMDk6NDU6MzIuMDU1WiIsImVtYWlsIjoia2tzaG9yZUBob3RtYWlsLmNvbSIsImlhdCI6MTU2NjM4MDczMn0.QFQ2tCKvmv9aQaLrtRYMmV7DyCUGDszo2ak0EPRRFd4&isActive=true&nextPage=/user/subscriptions&utm_source=mandrill&utm_medium=email&utm_campaign=_sgp_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web";

        if (count($params) > 0) {
            $session_params = (object) array();

            foreach ($params as $key => $param) {
                $session_params->$key = $param;
            }
        } else {
            $session_params = $params;
        }
        session()->put('url_parameters', json_encode($session_params));
    }

    public function initSalesNotificationProductList($countryLang)
    {
        // If Session Data for SalesNotificationProductList is not null
        if (!is_null(session()->get('SalesNotificationProductList'))) {
            //Do Nothing
        } else {
            // Else, Query dB to obtain Required Data
            $salesNotificationList = Product::join('producttranslates', 'producttranslates.ProductId', 'products.id') // products JOIN producttranslates
                ->join('productimages', 'productimages.ProductId', 'products.id') // products JOIN productimages
                ->select('producttranslates.name', 'productimages.url') // Select Name & URL columns
                ->where(['products.status' => 'active', 'producttranslates.langCode' => $countryLang]) // Where product status = active & producttranslate = current $CountryLang
                ->inRandomOrder() // Randomise
                ->limit(10) // Limit 10 records
                ->get();

            // Save Query results into session
            session()->put('SalesNotificationProductList', json_encode($salesNotificationList));
        }
    }
}
