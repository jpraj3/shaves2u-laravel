<?php

namespace App\Http\Middleware;

use App\Models\BaWebsite\SellerUser;
use Auth;
use Closure;

class BaAuth
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->session()->has('seller')) {
            $seller = new SellerUser($request->session()->get('seller'));
            Auth::login($seller);
            return $next($request);
        } else {
            return redirect()->route('ba.login');
        }
    }
}
