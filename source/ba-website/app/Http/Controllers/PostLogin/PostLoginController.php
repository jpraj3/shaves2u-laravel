<?php

namespace App\Http\Controllers\PostLogin;

use App;
use App\Http\Controllers\Controller;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\States;
use App\Models\Orders\Orders;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use OpenGraph;
use SEOMeta;

class PostLoginController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->middleware('auth');
        $this->app = $app;
        $this->request = $request;
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
    }

    public function redirectToHome()
    {
        if (Auth::check()) {
            // Get Current Country Data (in JSON) From Session Storage
            $currentCountryData = "";
            $currentCountryIso = "";
            $langurl = "";
            if (session()->get('currentCountry') != null && session()->get('currentCountry')) {
                if (is_array(session()->get('currentCountry'))) {
                    $currentCountryData = json_decode(session()->get('currentCountry'), true);
                    // Get Redirect(Country Code) Prefix from Current country data
                    $currentCountryIso = strtolower($currentCountryData['codeIso']);
                    $langurl = strtolower($currentCountryData['urlLang']);
                } else {
                    $currentCountryData = session()->get('currentCountry');
                    // Get Redirect(Country Code) Prefix from Current country data
                    $currentCountryIso = strtolower($currentCountryData->codeIso);
                    $langurl = strtolower($currentCountryData->urlLang);
                }
            }

            // Get Current Locale
            $currentLocale = strtolower(app()->getLocale());
            return redirect()->route('ba.locale.landing', [$langurl, $currentCountryIso]);
        } else {
            return redirect()->route('ba.login');
        }
    }

    public function index($langCode, $countryCode)
    {
        if (Auth::check()) {
            // If country Code param exist
            if ($countryCode) {
                $sessionCountryData = "";
                $sessionCountryIso = "";
                $sessionUrl = "";
                if (session()->get('currentCountry') != null && session()->get('currentCountry')) {
                    if (is_array(session()->get('currentCountry'))) {
                        $sessionCountryData = json_decode(session()->get('currentCountry'), true);
                        $sessionCountryIso = strtolower($sessionCountryData['codeIso']);
                        $sessionUrl = strtolower($sessionCountryData['urlLang']);
                    } else {
                        $sessionCountryData = session()->get('currentCountry');
                        $sessionCountryIso = strtolower($sessionCountryData->codeIso);
                        $sessionUrl = strtolower($sessionCountryData->urlLang);
                    }
                }

                // If Country ISO code for request URL different from session ISO
                if ($countryCode !== $sessionCountryIso) {
                    // Destroy Current Session
                    session()->forget('currentCountry');

                    // Get New country Info based on $countryCode
                    $activeCountry = Countries::where([['codeIso', $countryCode], ['isActive', true]])->first();
                    // If country in database exist and isActive
                    if (isset($activeCountry)) {
                        $languageDetailsData = LanguageDetails::where('CountryId', $activeCountry->id)
                            ->where('mainlanguageCode', $activeCountry->defaultLang)
                            ->first();
                        if (isset($languageDetailsData)) {
                            // Convert stdClass Data to JSON
                            $sessiondata = [
                                'id' => $activeCountry->id,
                                'name' => $activeCountry->name,
                                'code' => $activeCountry->code,
                                'codeIso' => $activeCountry->codeIso,
                                'defaultLang' => strtolower($languageDetailsData->sublanguageCode),
                                'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                            ];
                            $sessionUrl = strtolower($languageDetailsData->mainlanguageCode);
                            $activeCountryJson = json_encode($sessiondata);
                        } else {

                            // Else get default fallback values from config
                            $defaultCountryInfo = $this->default_country;
                            $defaultCountryIso = strtolower($defaultCountryInfo['codeIso']);
                            $sessionUrl = strtolower($defaultCountryInfo['urlLang']);
                            // Redirect back to default country url
                            return redirect()->route('locale.region', [$defaultCountryIso]);
                        }
                    } else {

                        // Else get default fallback values from config
                        $defaultCountryInfo = $this->default_country;
                        $defaultCountryIso = strtolower($defaultCountryInfo['codeIso']);

                        // Redirect back to default country url
                        return redirect()->route('locale.region', [$defaultCountryIso]);
                    }

                    // Create new session Based on new country info
                    session()->put('currentCountry', $activeCountryJson);

                    // Assign new locale value in session storage
                    session()->put('currentLocale', json_decode($activeCountryJson, true)['defaultLang']);

                    // Set Locale based on Current Country default lang
                    $this->app->setLocale(json_decode($activeCountryJson, true)['defaultLang']);
                } else {
                    // Do nothing if Country iso for request URL is same as session
                }

                // Get new country info from session
                if (is_array(session()->get('currentCountry'))) {
                    $currentCountryData = json_decode(session()->get('currentCountry'), true);
                } else {
                    $currentCountryData = session()->get('currentCountry');
                }

                // Store new redirect URL
                if (is_array($currentCountryData)) {
                    $currentCountryIso = $currentCountryData['codeIso'];
                } else {
                    $currentCountryIso = $currentCountryData->codeIso;
                }
            } else {

                //  If no country code param exist Use Fallback values
                $currentCountryData = config('global.default.country');
                if (is_array($currentCountryData)) {
                    $currentCountryIso = $currentCountryData['codeIso'];
                } else {
                    $currentCountryIso = $currentCountryData->codeIso;
                }
            }
            $langCode = strtolower(app()->getLocale());
            // $currentCountrySeoData = json_decode(json_encode(MetaTags::where('CountryId', $currentCountryData['id'])->first()), true);

            // Remove Default Title from SEO tools (Library Bug workaround)
            config(['seotools.meta.defaults.title' => false]);

            // Set Meta Information
            // SEOMeta::setTitle($currentCountrySeoData['metatitle']);
            // SEOMeta::setDescription($currentCountrySeoData['metaDescription']);

            // Set og Meta Info
            // OpenGraph::setDescription($currentCountrySeoData['metaDescription']);
            // OpenGraph::setTitle($currentCountrySeoData['ogTitle']);
            // OpenGraph::setUrl($currentCountrySeoData['ogUrl']);
            // OpenGraph::addProperty('type', $currentCountrySeoData['ogType']);
            // OpenGraph::addImage($currentCountrySeoData['ogImage']);
            view()->share('currentCountryIso', $currentCountryIso);
            view()->share('url', $sessionUrl);
            view()->share('langCode', $langCode);
            return view('postlogin.products')->with('currentCountryIso', $currentCountryIso)->with('langCode', $langCode);
        } else {
            return redirect()->route('ba.login');
        }
    }

    public function dashboard()
    {
        return view('postlogin.dashboard');
    }

    public function profile()
    {
        $subs_ids = [];
        $total_trial_kits_sold = 0;
        $total_custom_plans_sold = 0;
        $total_ala_carte_orders = 0;

        if (Auth::check()) {
            $seller = Auth::user();
            $marketing_office_info = MarketingOffice::where('id', $seller->MarketingOfficeId)->first();

            if (is_array(session()->get('currentCountry'))) {
                $currentCountryData = json_decode(session()->get('currentCountry'), true);
                $currentCountryIso = strtolower($currentCountryData['codeIso']);
            } else {
                $currentCountryData = session()->get('currentCountry');
                $currentCountryIso = strtolower($currentCountryData->codeIso);
            }

            $currentLocale = strtolower(app()->getLocale());
            $total_subscription_orders = Orders::where('SellerUserId', $seller->id)->get();
            if (!empty($total_subscription_orders)) {
                foreach ($total_subscription_orders as $tso) {
                    array_push($subs_ids, (int) $tso->subscriptionIds);
                }
                $total_trial_kits_sold = Subscriptions::whereIn('id', $subs_ids)->where('isTrial', 1)->where('isCustom', 0)->whereDate('created_at', Carbon::today())->count();
                $total_custom_plans_sold = Subscriptions::whereIn('id', $subs_ids)->where('isCustom', 1)->where('isTrial', 0)->whereDate('created_at', Carbon::today())->count();
            }
            $total_ala_carte_orders = Orders::where('SellerUserId', $seller->id)->where('subscriptionIds', null)->whereDate('created_at', Carbon::today())->count();
            return view('postlogin.profile', compact('currentLocale', 'countryCode', 'seller', 'marketing_office_info', 'total_trial_kits_sold', 'total_custom_plans_sold', 'total_ala_carte_orders'));
        } else {
            return redirect()->route('ba.login');
        }
    }

    public function saleshistory()
    {
        if (Auth::check()) {
            $seller = Auth::user();

            if (is_array(session()->get('currentCountry'))) {
                $currentCountryData = json_decode(session()->get('currentCountry'), true);
                $currentCountryIso = strtolower($currentCountryData['codeIso']);
            } else {
                $currentCountryData = session()->get('currentCountry');
                $currentCountryIso = strtolower($currentCountryData->codeIso);
            }

            $currentLocale = strtolower(app()->getLocale());
            $weekly = \Carbon\Carbon::today()->subDays(3);
            $orders = Orders::leftJoin('subscriptions', 'subscriptions.id', 'orders.subscriptionIds')
                ->where('orders.SellerUserId', $seller->id)
                ->where('orders.created_at', '>=', $weekly)
                ->orderBy('orders.created_at', 'DESC')
                ->select('orders.email', 'orders.created_at', 'orders.id', 'orders.status', 'orders.subscriptionIds', 'orders.isDirectTrial', 'orders.CountryId', 'subscriptions.isTrial', 'subscriptions.isCustom')
                ->get()
                ->groupBy(function ($item) {
                    return $item->created_at->format('Y-m-d');
                });

            return view('postlogin.saleshistory', compact('currentLocale', 'countryCode', 'orders'));
        } else {
            return redirect()->route('ba.login');
        }
    }

    public function products()
    {
        if (Auth::check()) {
            return view('postlogin.products');
        } else {
            return redirect()->route('ba.login');
        }
    }

    public function productsType(Request $request)
    {
        try {

            if (is_array(session()->get('currentCountry'))) {
                $current_country = json_decode(session()->get('currentCountry'), true);
                $country_code = strtolower($current_country['codeIso']);
                $urlLang = strtolower($current_country['urlLang']);
            } else {
                $current_country = session()->get('currentCountry');
                $country_code = strtolower($current_country->codeIso);
                $urlLang = strtolower($current_country->urlLang);
            }

            $current_locale = App::getLocale();
            $url = route('ba.locale.show.product-types', ['langCode' => $urlLang, 'countryCode' => $country_code]);
            return $url;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function showProductsType()
    {
        return view('postlogin.products.product-type-list');
    }

    public function returnTrialSteps(Request $request)
    {

        $locale = strtolower(App::getLocale());
        if (is_array(session()->get('currentCountry'))) {
            $sessionCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        } else {
            $sessionCountryData = session()->get('currentCountry');
            $currentCountryIso = strtolower($sessionCountryData->codeIso);
        }

        $country = Countries::where('id', $sessionCountryData->id)->first();
        $url = '';
        $data = [];
        // Set Meta Information
        SEOMeta::setTitle('Shaves2U | Try now for only ' . $country->currencyDisplay . config('global.all.general_trial_price.' . strtolower($country->codeIso)));
        SEOMeta::setDescription('Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.');
        // SEOMeta::setCanonical($url);

        // Set og Meta Info
        OpenGraph::setDescription('Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.');
        OpenGraph::setTitle('Shaves2U | Try now for only ' . $country->currencyDisplay . config('global.all.general_trial_price.' . strtolower($country->codeIso)));
        // OpenGraph::setUrl($url);
        OpenGraph::addImage('https://shaves2u.com/assets/images/S2U-Referal-Image-MY.jpg');

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $data = [
            "gender" => $request->gender,
            "CountryId" => $country->id,
            "langCode" => App::getLocale(),
            "appType" => "baWebsite",
        ];

        $gender = isset($request->gender) ? $request->gender : 'male';
        $url = config('environment.apiUrl') . "/ba/products/trial/list";
        $res = $client->postAsync($url, ['body' => json_encode($data)]);
        $res = $res->wait();
        $data = json_decode($res->getBody(), true);

        if ($data["success"] === true) {
            $detailsV2 = $data["payload"];

            // remove blades that is not associated with any plans
            $bladeLists = [];
            $frequencyExists = [];
            $planExists = [];
            foreach ($detailsV2["blade_products"] as $key => $blade_type) {
                foreach ($detailsV2["allplan"] as $key => $plan) {
                    if (in_array((int) $blade_type["productcountriesid"], $plan["productCountryId"])) {
                        array_push($planExists, $plan);
                        array_push($bladeLists, (int) $blade_type["productcountriesid"]);
                    }
                }
            }
            $detailsV2["allplan"] = $planExists;
            $bladeLists = array_unique($bladeLists);
            foreach ($detailsV2["blade_products"] as $key => $blade_type) {
                if (!in_array($blade_type["productcountriesid"], $bladeLists)) {
                    unset($detailsV2["blade_products"][$key]);
                }
            }
            $detailsV2["blade_products"] = array_values($detailsV2["blade_products"]);

            // remove frequencies not associated with any plans
            foreach ($detailsV2["frequency_list"] as $key => $frequency_type) {
                foreach ($detailsV2["allplan"] as $key => $plan) {
                    if ((int) $frequency_type["duration"] == (int) $plan["planduration"]) {
                        if (!in_array((int) $frequency_type["duration"], $frequencyExists)) {
                            array_push($frequencyExists, (int) $frequency_type["duration"]);
                        }
                    }
                }
            }

            foreach ($detailsV2["frequency_list"] as $key => $fE) {
                if (!in_array($fE["duration"], $frequencyExists)) {
                    unset($detailsV2["frequency_list"][$key]);
                }
            }
            $detailsV2["frequency_list"] = array_values($detailsV2["frequency_list"]);
            $details = \App\Helpers\LaravelHelper::ConvertArraytoObject($detailsV2);

            return view('postlogin.products.trial-plans.start-journey.step-1-blade-selection', compact('details', 'detailsV2', 'gender'));
        } else {
            return redirect()->back();
        }
    }

    public function showTrialPlansStep1(Request $request)
    {
        $data = json_decode($request->trial_data);
        $details = $data->payload;
        return view('postlogin.products.trial-plans.start-journey.step-1-blade-selection', compact('details'));
    }

    public function showTrialPlansStep2(Request $request)
    {
        $details = json_decode($request->trial_data);
        $selected_blade = $details->selected_blade;
        return view('postlogin.products.trial-plans.start-journey.step-2-plan-type-selection', compact('details', 'selected_blade'));
    }

    public function showTrialPlansStep3(Request $request)
    {
        $details = json_decode($request->trial_data);
        return view('postlogin.products.trial-plans.start-journey.step-3-frequency-selection', compact('details'));
    }

    public function TrialPlanCheckout(Request $request)
    {
        $locale = strtolower(App::getLocale());
        if (is_array(session()->get('currentCountry'))) {
            $sessionCountryData = json_decode(session()->get('currentCountry'));
            $currentCountryIso = strtolower($sessionCountryData['codeIso']);
            $urlLang = strtolower($sessionCountryData['urlLang']);
        } else {
            $sessionCountryData = session()->get('currentCountry');
            $currentCountryIso = strtolower($sessionCountryData->codeIso);
            $urlLang = strtolower($sessionCountryData->urlLang);
        }
        $country = Countries::where('id', $sessionCountryData->id)->first();
        $data = [];
        $url = route('ba.locale.show.trial-plans.checkout', ['langCode' => $urlLang, 'countryCode' => strtolower($country->codeIso)]);
        $data = array(
            "url" => $url,
            "data" => $request->data,
        );

        return $data;
    }

    public function showTrialPlanCheckout(Request $request)
    {
        return $this->_showTrialPlanCheckout($request->trial_data, null);
    }

    public function _showTrialPlanCheckout($trial_data, $payment_fail, $gender = null)
    {
        $selection = json_decode(session()->get('selection_trial_plan'), true);
        $checkout = json_decode(session()->get('checkout_trial_plan'), true);
        $trial_switch_plans_session = session()->get('trial_switch_plans');
        $gender = isset(json_decode($trial_data, true)["gender"]) ? json_decode($trial_data, true)["gender"] : ($gender !== null ? $gender : 'male');
        if (!empty($trial_switch_plans_session)) {
            $trial_switch_plans_session = session()->get('trial_switch_plans');
        } else {
            $trial_switch_plans_session = (object) array(
                "_old_payment_intent_data" => null,
                "_new_payment_intent_data" => null,
            );

            session()->put('trial_switch_plans', json_encode($trial_switch_plans_session));
        }

        // logic for annual plan selection
        if ($selection["selection"]["switch_plans"]["isSwitched"] === true) {
            if ($selection["selection"]["switch_plans"]["isSelected"] === "annual") {
                // generate new paymentIntent

            }
            // if annual plan was selected and then switched to normal plan selection
            if ($selection["selection"]["switch_plans"]["isSelected"] === "normal") {}
        }

        $details = "";
        if ($trial_data) {
            $details = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode($trial_data, true));
            session()->put('trial_checkout_payment_failed', json_encode($trial_data));
        } else {
            if (json_decode(session()->get('trial_checkout_payment_failed', true)) !== "") {
                $details = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode(json_decode(session()->get('trial_checkout_payment_failed', true), true)));
            } else {}
        }

        $userdetails = (object) array();
        $_user = json_decode(session()->get('user'));
        $session_selection = json_decode(session()->get('selection_trial_plan'));

        if (is_array(session()->get('currentCountry'))) {
            $current_country = json_decode(session()->get('currentCountry'));
        } else {
            $current_country = session()->get('currentCountry');
        }

        if ($current_country) {
            $states = States::where('CountryId', $current_country->id)->get();
        } else {
            $states = States::all();
        }

        $userdetails->current_country = $current_country;
        $userdetails->states = $states;
        $user = null;
        if (isset($_user)) {
            $user = User::findorfail($_user->id);
        }
        if ($user !== null) {
            $userdetails->user = $user;
            $d_card = Cards::where('isDefault', 1)->where('UserId', $user->id)->orderBy('created_at', 'DESC')->first();
            $d_billing_address = DeliveryAddresses::where('id', $user->defaultBilling)->first();
            $d_delivery_address = DeliveryAddresses::where('id', $user->defaultShipping)->first();
            $userdetails->defaults = (object) array(
                "card" => $d_card,
                "delivery_address" => $d_delivery_address,
                "billing_address" => $d_billing_address,
            );
            $cards = Cards::where('UserId', $user->id)->orderBy('created_at', 'DESC')->get();
            $userdetails->cards = $cards;
            $billing_addresses = DeliveryAddresses::where('id', $user->defaultBilling)->get();
            $userdetails->billing_addresses = $billing_addresses;
            $delivery_addresses = DeliveryAddresses::where('id', $user->defaultShipping)->get();
            $userdetails->delivery_addresses = $delivery_addresses;

            // generate new paymentIntent
            if ($current_country->id != 8 && $userdetails->defaults->card !== null) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);

                $data = [
                    "userdetails" => $userdetails,
                    "current_country" => session()->get('currentCountry'),
                    "langCode" => strtolower(App::getLocale()),
                    "session_selection" => session()->get('selection_trial_plan'),
                    "gender" => $gender,
                ];

                $url = config('environment.apiUrl') . '/ba/stripe/regenerate-payment-intents';
                $res = $client->postAsync($url, ['body' => json_encode($data)]);
                $res = $res->wait();
                $payment_intent = json_decode($res->getBody(), true);

                $generate_payment_intent = $payment_intent["generate_payment_intent"];
                $checkout_details = $payment_intent["plandetails"];

                //first time, no annual plans were selected
                if ($selection["selection"]["switch_plans"]["isSwitched"] === false) {

                    if (is_string($trial_switch_plans_session) === true) {
                        $trial_switch_plans_session = json_decode($trial_switch_plans_session, true);
                    }
                    $trial_switch_plans_session["_old_payment_intent_data"] = $payment_intent;
                    $trial_switch_plans_session["_new_payment_intent_data"] = null;
                    session()->put('trial_switch_plans', $trial_switch_plans_session);
                } else {
                    if (is_string($trial_switch_plans_session) === true) {
                        $trial_switch_plans_session = json_decode($trial_switch_plans_session, true);
                    }
                    $trial_switch_plans_session["_new_payment_intent_data"] = $payment_intent;
                    session()->put('trial_switch_plans', $trial_switch_plans_session);
                }
            } else {
                $payment_intent = "";
                $generate_payment_intent = "";
                $checkout_details = "";
            }
        } else {
            $userdetails->user = null;
            $userdetails->defaults = (object) array(
                "card" => null,
                "delivery_address" => null,
                "billing_address" => null,
            );
            $userdetails->cards = [];
            $userdetails->billing_addresses = null;
            $userdetails->delivery_addresses = null;
            $payment_intent = "";
            $generate_payment_intent = "";
            $checkout_details = "";
        }

        session()->put('gender_selected', $gender);
        $related_annual_plan = $this->getRelatedAnnualPlan($details, $gender);
        $addon_selected = isset(json_decode($details->session_data, true)["selection"]["step2"]) ? json_decode($details->session_data, true)["selection"]["step2"]["selected_addon_list"] : null;
        // trial_switch_plans
        $has_shave_cream_next_billing = isset(json_decode($details->session_data, true)["selection"]["has_shave_cream_next_billing"]) ? json_decode($details->session_data, true)["selection"]["has_shave_cream_next_billing"] : false;
        $payment_fail = \App\Helpers\LaravelHelper::ConvertArraytoObject($payment_fail);

        return view('postlogin.products.trial-plans.checkout-journey.trial-plan-checkout-ba', compact('has_shave_cream_next_billing','gender', 'details', 'userdetails', 'states', 'generate_payment_intent', 'related_annual_plan', 'addon_selected', 'payment_fail'));
    }

    public function getRelatedAnnualPlan($details, $gender)
    {

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);
        if (is_array(session()->get('currentCountry'))) {
            $current_country = json_decode(session()->get('currentCountry'), true);
            $current_country_id = $current_country['id'];
        } else {
            $current_country = session()->get('currentCountry');
            $current_country_id = $current_country->id;
        }
        $data = [
            "langCode" => strtolower(App::getLocale()),
            "CountryId" => $current_country_id,
            "existing_plan_info" => $details,
            "gender" => $gender,
        ];

        $url = config('environment.apiUrl') . '/ba/products/trial/related-annual-plan';
        $res = $client->postAsync($url, ['body' => json_encode($data)]);
        $res = $res->wait();
        $res = json_decode($res->getBody(), true);
        return $res["payload"];
    }

    public function showCustomPlans()
    {
        return view('postlogin.products.custom-plans.start-journey.custom-plan-list');
    }

    public function showProducts(Request $request)
    {
        try {
            if ($request->product_data) {
                $data = json_decode($request->product_data);
                $details = $data->payload->filtered_products;

                session()->put('alacarte_product_data', $request->product_data);
            } else if (session()->get('alacarte_product_data')) {
                $data = json_decode(session()->get('alacarte_product_data'));
                $details = $data->payload->filtered_products;
            } else {
                return redirect()->back();
            }

            return view('postlogin.products.alacarte.alacarte-list', compact('details'));
        } catch (Exception $e) {
            return redirect()->back();
        }
    }

    public function showAlacarteSPM(Request $request)
    {
        try {
            if ($request->product_data) {
                $data = json_decode($request->product_data);
                session()->put('selection_product_data', $request->product_data);
            } else if (session()->get('selection_product_data')) {
                $data = json_decode(session()->get('selection_product_data'));
            } else {
                return redirect()->back();
            }

            return view('postlogin.products.alacarte.alacarte-select-payment', compact('data'));
        } catch (Exception $e) {
            return redirect()->back();
        }
    }

    public function AlacarteCheckout(Request $request)
    {
        $locale = strtolower(App::getLocale());
        if (is_array(session()->get('currentCountry'))) {
            $sessionCountryData = json_decode(session()->get('currentCountry'));
            $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
            $urlLang = strtolower($sessionCountryData['urlLang']);
        } else {
            $sessionCountryData = session()->get('currentCountry');
            $currentCountryIso = strtoupper($sessionCountryData->codeIso);
            $urlLang = strtolower($sessionCountryData->urlLang);
        }

        $country = Countries::where('id', $sessionCountryData->id)->first();
        $data = [];
        $method = $request->method;

        $url = route('ba.locale.show.alacarte.checkout-' . $method, ['langCode' => $urlLang, 'countryCode' => strtolower($country->codeIso)]);
        $data = array(
            "url" => $url,
            "data" => $request->data,
        );

        return $data;
    }

    public function showAlacarteCheckoutCash(Request $request)
    {
        $details = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode($request->product_data, true));
        $userdetails = (object) array();
        $_user = json_decode(session()->get('user'));

        if (is_array(session()->get('currentCountry'))) {
            $current_country = json_decode(session()->get('currentCountry'));
        } else {
            $current_country = session()->get('currentCountry');
        }

        if ($current_country) {
            $states = States::where('CountryId', $current_country->id)->get();
        } else {
            $states = States::all();
        }

        $userdetails->current_country = $current_country;
        $userdetails->states = $states;

        $user = null;
        if (isset($_user)) {
            $user = User::findorfail($_user->id);
        }
        if ($user !== null) {
            $userdetails->user = $user;
            $d_card = Cards::where('isDefault', 1)->where('UserId', $user->id)->orderBy('created_at', 'DESC')->first();
            $d_billing_address = DeliveryAddresses::where('id', $user->defaultBilling)->first();
            $d_delivery_address = DeliveryAddresses::where('id', $user->defaultShipping)->first();
            $userdetails->defaults = (object) array(
                "card" => $d_card,
                "delivery_address" => $d_delivery_address,
                "billing_address" => $d_billing_address,
            );
            $cards = Cards::where('UserId', $user->id)->orderBy('created_at', 'DESC')->get();
            $userdetails->cards = $cards;
            $billing_addresses = DeliveryAddresses::where('id', $user->defaultBilling)->get();
            $userdetails->billing_addresses = $billing_addresses;
            $delivery_addresses = DeliveryAddresses::where('id', $user->defaultShipping)->get();
            $userdetails->delivery_addresses = $delivery_addresses;
        } else {
            $userdetails->user = null;
            $userdetails->defaults = (object) array(
                "card" => null,
                "delivery_address" => null,
                "billing_address" => null,
            );
            $userdetails->cards = [];
            $userdetails->billing_addresses = null;
            $userdetails->delivery_addresses = null;
        }
        $countriesext = Countries::pluck('callingCode')->all();

        return view('postlogin.products.alacarte.alacarte-checkout-cash', compact('details', 'userdetails', 'states', 'countriesext'));
    }

    public function showAlacarteCheckoutCard(Request $request)
    {
        return $this->_showAlacarteCheckoutCard($request->product_data, null);
    }

    public function _showAlacarteCheckoutCard($product_data, $payment_fail)
    {
        $details = "";

        if (is_array(session()->get('currentCountry'))) {
            $current_country = json_decode(session()->get('currentCountry'));
        } else {
            $current_country = session()->get('currentCountry');
        }

        if ($product_data) {
            $details = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode($product_data, true));
            session()->put('alacarte_checkout_payment_failed', json_encode($product_data));
        } else {
            if (session()->has('alacarte_checkout_payment_failed')) {
                $details = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode(json_decode(session()->get('alacarte_checkout_payment_failed', true))));
            } else {
                $langCode = strtolower(app()->getLocale());

                if (is_array(session()->get('currentCountry'))) {
                    $currentCountryIso = strtolower($current_country['codeIso']);
                    $urlLang = strtolower($current_country['urlLang']);
                } else {
                    $currentCountryIso = strtolower($current_country->codeIso);
                    $urlLang = strtolower($current_country->urlLang);
                }

                return redirect()->route('ba.locale.landing', ['langCode' => $urlLang, 'countryCode' => $currentCountryIso]);
            }
        }

        $userdetails = (object) array();
        $_user = json_decode(session()->get('user'));
        if ($current_country) {
            $states = States::where('CountryId', $current_country->id)->get();
        } else {
            $states = States::all();
        }
        $countriesext = Countries::pluck('callingCode')->all();

        $userdetails->current_country = $current_country;
        $userdetails->states = $states;

        $user = null;
        if (isset($_user)) {
            $user = User::findorfail($_user->id);
        }
        if ($user !== null) {
            $userdetails->user = $user;
            $d_card = Cards::where('isDefault', 1)->where('UserId', $user->id)->orderBy('created_at', 'DESC')->first();
            $d_billing_address = DeliveryAddresses::where('id', $user->defaultBilling)->first();
            $d_delivery_address = DeliveryAddresses::where('id', $user->defaultShipping)->first();
            $userdetails->defaults = (object) array(
                "card" => $d_card,
                "delivery_address" => $d_delivery_address,
                "billing_address" => $d_billing_address,
            );
            $cards = Cards::where('UserId', $user->id)->orderBy('created_at', 'DESC')->get();
            $userdetails->cards = $cards;
            $billing_addresses = DeliveryAddresses::where('id', $user->defaultBilling)->get();
            $userdetails->billing_addresses = $billing_addresses;
            $delivery_addresses = DeliveryAddresses::where('id', $user->defaultShipping)->get();
            $userdetails->delivery_addresses = $delivery_addresses;

            // generate new paymentIntent
            if ($current_country->id != 8 && $userdetails->defaults->card !== null) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);

                $data = [
                    "userdetails" => $userdetails,
                    "current_country" => session()->get('currentCountry'),
                    "langCode" => strtolower(App::getLocale()),
                    "session_selection" => session()->get('selection_alacarte_ba'),
                    "session_checkout" => session()->get('checkout_alacarte_ba'),
                ];

                $api_url = config('environment.apiUrl');

                $res = $client->postAsync($api_url . '/ba/stripe/regenerate-alacarte-payment-intents', ['body' => json_encode($data)]);
                $res = $res->wait();
                $payment_intent = json_decode($res->getBody(), true);
                $generate_payment_intent = $payment_intent["generate_payment_intent"];
                $checkout_details = $payment_intent["productcountries"];
            } else {
                $payment_intent = "";
                $generate_payment_intent = "";
                $checkout_details = "";
            }
        } else {
            $userdetails->user = null;
            $userdetails->defaults = (object) array(
                "card" => null,
                "delivery_address" => null,
                "billing_address" => null,
            );
            $userdetails->cards = [];
            $userdetails->billing_addresses = null;
            $userdetails->delivery_addresses = null;
            $payment_intent = "";
            $generate_payment_intent = "";
            $checkout_details = "";
        }

        $payment_fail = \App\Helpers\LaravelHelper::ConvertArraytoObject($payment_fail);
        return view('postlogin.products.alacarte.alacarte-checkout-card', compact('details', 'userdetails', 'states', 'generate_payment_intent', 'payment_fail'));
    }

    public function postPaymentRedirect(Request $request)
    {
        $gender = session()->get('gender_selected');
        // try {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        if ($request->get('type') === 'trial-plan') {
            $session_selection = session()->get('selection_trial_plan');
            $session_checkout = session()->get('checkout_trial_plan');
        } else if ($request->get('type') === 'alacarte') {
            $session_selection = session()->get('selection_alacarte_ba');
            $session_checkout = session()->get('checkout_alacarte_ba');
        }

        $data = [
            "stripe_response" => $request->all(),
            "current_country" => session()->get('currentCountry'),
            "seller_info" => session()->get('seller'),
            "session_selection" => $session_selection,
            "session_checkout" => $session_checkout,
            "lang_code" => strtolower(app()->getLocale())
        ];

        $url = config('environment.apiUrl') . '/ba/stripe/redirect';
        $res = $client->postAsync($url, ['body' => json_encode($data)]);
        $res = $res->wait();
        $res = json_decode($res->getBody(), true);
        // $result = json_decode($res->getBody(), true);
        // echo $request;
        // echo '<br>';
        // echo $res->getBody();

        // dd($res);

        if ($res["status"] === 'success') {
            $langCode = strtolower(app()->getLocale());
            if (is_array(session()->get('currentCountry'))) {
                $current_country = json_decode(session()->get('currentCountry'));
                $currentCountryIso = strtolower($current_country['codeIso']);
                $urlLang = strtolower($current_country['urlLang']);
            } else {
                $current_country = session()->get('currentCountry');
                $currentCountryIso = strtolower($current_country->codeIso);
                $urlLang = strtolower($current_country->urlLang);
            }

            // remove all sessions related to checkout
            session()->forget('selection_trial_plan');
            session()->forget('checkout_trial_plan');
            session()->forget('alacarte_product_data');
            session()->forget('selection_alacarte_ba');
            session()->forget('selection_product_data');
            session()->forget('checkout_alacarte_ba');
            session()->forget('trial_switch_plans');
            session()->forget('gender_selected');
            session()->forget('user');
            return redirect()->route('ba.locale.thankyou', ['langCode' => $urlLang, 'countryCode' => $currentCountryIso, 'id' => $res["order_id"]])->with('data', $res);
        } else {
            $payment_fail = array();
            if ($request->get('type') === 'trial-plan') {
                $payment_fail["payment_fail"] = true;
                // $payment_fail["payment_fail_message"] = $res["error"]["payment_error"];
                $payment_fail["payment_fail_message"] = "Sorry, your payment was unsuccessful. Please update your credit card information and try again.";
                return $this->_showTrialPlanCheckout(null, $payment_fail, $gender);
            } else if ($request->get('type') === 'alacarte') {
                $payment_fail["payment_fail"] = true;
                // $payment_fail["payment_fail_message"] = $res["error"]["payment_error"];
                $payment_fail["payment_fail_message"] = "Sorry, your payment was unsuccessful. Please update your credit card information and try again.";
                return $this->_showAlacarteCheckoutCard(null, $payment_fail);
            }
        }
    }
}
