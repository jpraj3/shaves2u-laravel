<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\BaWebsite\SellerUser;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\SupportedLang;
use Auth;
use Exception;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use \Illuminate\Validation\ValidationException as ValidationException;
use Carbon\Carbon;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Application $app, Request $request)
    {
        $this->middleware('guest')->except('logout');
        $this->app = $app;
        $this->request = $request;
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
        $this->session = new SessionController();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $guard = null;
        // Get new country info from session
        $currentCountryData = "";
        $currentCountryIso = "";
        if (session()->get('currentCountry') != null && session()->get('currentCountry')) {
            if (is_array(session()->get('currentCountry'))) {
                $currentCountryData = json_decode(session()->get('currentCountry'), true);
                $currentCountryIso = strtolower($currentCountryData['codeIso']);
            } else {
                $currentCountryData = session()->get('currentCountry');
                $currentCountryIso = strtolower($currentCountryData->codeIso);
            }
        }
        $langCode = strtolower(app()->getLocale());


        $path = $langCode . '-' . $currentCountryIso;
        $url_parameters = json_encode(session()->get('url_parameters'));
        if (Auth::guard($guard)->check()) {
            return redirect('/' . $langCode . '-' . $currentCountryIso . '/');
        } else {
            return view('auth.login');
        }
    }

    public function checkAgentExists(Request $request)
    {
        $exists = SellerUser::where('badgeId', $request->badgeId)
            ->where('isActive', 1)
            ->first();

        return response()
            ->json($exists)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $seller = $this->guard()->getLastAttempted();

            // Make sure the user is active
            if ($seller->isActive && $this->attemptLogin($request)) {
                // Send the normal successful login response
                return $this->sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);
                return redirect()
                    ->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors(['active' => 'The email must be activated to login.']);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function api_login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $seller = $this->guard()->getLastAttempted();

            // Make sure the user is active
            if ($this->attemptLogin($request)) {
                // Send the normal successful login response
                return $this->api_sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);

                $response = [
                    "email" => $request->only($this->username(), 'remember'),
                    "error" => 'Email Address does not exist',
                ];

                return $response;
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->api_sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    protected function api_sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->api_authenticated($request, $this->guard()->user())
            ?: null;
    }

    protected function authenticated(Request $request, $seller)
    {
        // set new country session
        if ($seller) {
            //update last_login_at

            SellerUser::where('id', $seller->id)->update(["lastLoginAt" => Carbon::now()]);
            // get seller country & langcode
            $country = Countries::where('id', $seller->CountryId)->first();
            // $supported_languages = SupportedLang::where('CountryId', $seller->CountryId)->get();
            // App::setLocale($country->defaultLang);
            // session()->put('current_locale', $country->defaultLang);
            // session()->put('currentCountry', $country);
            // session()->put('supported_langs', $supported_languages);
            session()->put('seller', $seller);
            $redirect_url = route('ba.locale.landing', ['langCode' => strtolower($country->defaultLang), 'countryCode' => strtolower($country->codeIso)]);
            return ["seller" => $seller, "country" => $country, "redirect_url" => $redirect_url];
        }
    }

    protected function api_authenticated(Request $request, $seller)
    {
        // Get new country info from session
        $langCode = strtolower(app()->getLocale());

        if (is_array(session()->get('currentCountry'))) {
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
        } else {
            $currentCountryData = session()->get('currentCountry');
            $currentCountryIso = strtolower($currentCountryData->codeIso);
        }

        $this->session->_session('isLoggedIn', 'set', $seller);
        return $seller;
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function api_sendFailedLoginResponse(Request $request)
    {
        return 0;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'badgeId';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function authenticate($countryCode)
    {
        $country = Countries::where('code', $countryCode)->first();
        return view('auth.login')->withCookie(cookie()->forever('country', $country))->with('country', $country);
    }
}
