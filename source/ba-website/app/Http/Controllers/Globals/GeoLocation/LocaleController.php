<?php

namespace App\Http\Controllers\Globals\GeoLocation;

use App;
use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\MetaTags;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\LanguageDetails;
use App\Models\GeoLocation\SupportedLang;
use App\Models\Plans\TrialHandleTypes;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use SEOMeta;
use Auth;
class LocaleController extends Controller
{

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
        \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Current Country Data (in JSON) From Session Storage
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        // Get Redirect(Country Code) Prefix from Current country data
        $countryCode = strtolower($currentCountryData['codeIso']);
        // Get Current Locale
        $langCode = strtolower(app()->getLocale());

        if (Auth::check()) {
            // Redirect function
            return redirect()->route('ba.locale.landing', ['langCode'=> $langCode, 'countryCode' => $countryCode]);
        } else {
        }
    }

    public function region($langCode, $countryCode)
    {

        // If country Code param exist
        if ($countryCode) {
            $sessionCountryData = json_decode(session()->get('currentCountry'), true);
            $sessionCountryIso = strtolower($sessionCountryData['codeIso']);
            $sessionUrl = strtolower($sessionCountryData['urlLang']);
            // If Country ISO code for request URL different from session ISO
            if ($countryCode !== $sessionCountryIso) {
                // Destroy Current Session
                session()->forget('currentCountry');

                // Get New country Info based on $countryCode
                $activeCountry = Countries::where([['codeIso', $countryCode], ['isActive', true]])->first();
                // If country in database exist and isActive
                if (isset($activeCountry)) {
                    $languageDetailsData = LanguageDetails::where('CountryId', $activeCountry->id)
                        ->where('mainlanguageCode', $activeCountry->defaultLang)
                        ->first();
                    if (isset($languageDetailsData)) {
                        // Convert stdClass Data to JSON
                        $sessiondata = [
                            'id' => $activeCountry->id,
                            'code' => $activeCountry->code,
                            'codeIso' => $activeCountry->codeIso,
                            'defaultLang' => strtolower($languageDetailsData->sublanguageCode),
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $sessionUrl = strtolower($languageDetailsData->mainlanguageCode);
                        $activeCountryJson = json_encode($sessiondata);
                    } else {

                        // Else get default fallback values from config
                        $defaultCountryInfo = $this->default_country;
                        $defaultCountryIso = strtolower($defaultCountryInfo['codeIso']);
                        $sessionUrl = strtolower($defaultCountryInfo['urlLang']);
                        // Redirect back to default country url
                        return redirect()->route('locale.region', [$defaultCountryIso]);
                    }
                } else {

                    // Else get default fallback values from config
                    $defaultCountryInfo = $this->default_country;
                    $defaultCountryIso = strtolower($defaultCountryInfo['codeIso']);

                    // Redirect back to default country url
                    return redirect()->route('locale.region', [$defaultCountryIso]);
                }

                // Create new session Based on new country info
                session()->put('currentCountry', $activeCountryJson);

                // Assign new locale value in session storage
                session()->put('currentLocale', json_decode($activeCountryJson, true)['defaultLang']);

                // Set Locale based on Current Country default lang
                $this->app->setLocale(json_decode($activeCountryJson, true)['defaultLang']);
            } else {
                // Do nothing if Country iso for request URL is same as session
            }

            // Get new country info from session
            $currentCountryData = json_decode(session()->get('currentCountry'), true);

            // Store new redirect URL
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
        } else {

            //  If no country code param exist Use Fallback values
            $currentCountryData = config('global.default.country');
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
        }
        $langCode = strtolower(app()->getLocale());
        $currentCountrySeoData = json_decode(json_encode(MetaTags::where('CountryId', $currentCountryData['id'])->first()), true);

        // Remove Default Title from SEO tools (Library Bug workaround)
        config(['seotools.meta.defaults.title' => false]);

        $currentCountry = Countries::where('id', $currentCountryData["id"])->first();
        // Set Meta Information
        SEOMeta::setTitle($currentCountrySeoData['metatitle']);
        SEOMeta::setDescription($currentCountrySeoData['metaDescription']);

         // gather plan handle types
         $country_handles = TrialHandleTypes::join('products', 'products.id', 'trial_handle_types.ProductIds')
         ->where('trial_handle_types.CountryId', $currentCountry->id)
         ->where('trial_handle_types.gender', 'male')
         ->first();
         $country_handles ? session()->put('country_handles', $country_handles) : '';
        // Set og Meta Info
        // OpenGraph::setDescription($currentCountrySeoData['metaDescription']);
        // OpenGraph::setTitle($currentCountrySeoData['ogTitle']);
        // OpenGraph::setUrl($currentCountrySeoData['ogUrl']);
        // OpenGraph::addProperty('type', $currentCountrySeoData['ogType']);
        // OpenGraph::addImage($currentCountrySeoData['ogImage']);
        view()->share('currentCountryIso', $currentCountryIso);
        view()->share('url', $sessionUrl);
        view()->share('langCode', $langCode);
        return view('_guest.landing')->with('currentCountryIso', $currentCountryIso)->with('langCode', $langCode);
    }

    public function authenticated()
    {}

    public function APICountries()
    {

        $countries = Countries::get();

        return response()
            ->json($countries)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function changeActiveLanguage($langCode)
    {
        // $supportedLang = SupportedLang::where('id',$id)->first();
        // $langCode = $supportedLang->languageCode;
        // $country = Countries::where('id',$supportedLang->CountryId)->first();
        // $countryCode = $country->codeIso;

        App::setLocale(strtolower($langCode));
        session()->put('current_locale', $langCode);
        // session()->put('currentCountry', $country);

        return redirect()->route('ba.locale.landing', ['langCode'=> strtolower(app()->getLocale()), 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]);
    }
}
