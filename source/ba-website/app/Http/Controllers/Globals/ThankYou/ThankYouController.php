<?php

namespace App\Http\Controllers\Globals\ThankYou;

use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanCategory;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Services\OrderService;
use DB;
// Models
use Carbon\Carbon;
use Illuminate\Foundation\Application;

// Services
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class ThankYouController extends Controller
{

    public function __construct(Application $app, Request $request)
    {
        $this->middleware(['locale']);
        $this->app = $app;
        $this->request = $request;
        $this->orderService = new OrderService;

    }

    public function index($lang, $country, $id)
    {
        $langCode = strtolower(app()->getLocale());
        if (is_array(session()->get('currentCountry'))) {
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
        } else {
            $currentCountryData = session()->get('currentCountry');
            $currentCountryIso = strtolower($currentCountryData->codeIso);
        }
        $deliveryDateFrom = Carbon::now("Asia/Kuala_Lumpur")->addDays(1)->format('Y-m-d');
        $deliveryDateTo = Carbon::now("Asia/Kuala_Lumpur")->addDays(7)->format('Y-m-d');

        $orderInfo = (Object) array();
        // retrieve order info
        $_order = Orders::findorfail($id);
        $orderInfo->order = $_order;
        $type = null;
        $plantype = null;
        $orderInfo->planGender = null;
        $orderInfo->user = User::where('id', $_order->UserId)->first();
        $bladeName = '';
        if ($_order->subscriptionIds !== null) {
            $type = 'plans';
            $orderInfo->subscription = Subscriptions::where('id', $_order->subscriptionIds)->first();
            $planCountry = Plans::where('id', $orderInfo->subscription->PlanId)->first();
            $plansku = PlanSKU::where('id', $planCountry->PlanSkuId)->first();
            // $bladeNames = explode('-',$plansku->sku);
            // $bladeName = in_array("TK3", $bladeNames) ? '3' : (in_array("TK5", $bladeNames) ? '5' : ((in_array("TK6", $bladeNames) ? '6' : '3')));

            // $bladeName = $bladeName . ' ' . Lang::get('website_contents.authenticated.trial_plan.blade_cartridge');
            if(strtoupper(substr($plansku->sku, -2)) === '-W'){
                $orderInfo->planGender = 'female';
            }else{
                $orderInfo->planGender = 'male';
            }
            if ($orderInfo->subscription) {
                if ($orderInfo->subscription->isTrial == 1 && $orderInfo->subscription->isCustom == 0) {
                    $plantype = 'trial-plan';
                } else if ($orderInfo->subscription->isCustom == 1 && $orderInfo->subscription->isTrial == 0) {
                    $plantype = 'custom-plan';
                } else {
                    $plantype = null;
                }

            }
        } else {
            $type = 'products';
        }

        $orderInfo->receipt = Receipts::where('OrderId', $_order->id)->first();
        $orderInfo->plantype = $plantype;
        $orderInfo->type = $type;
        // retrieve country info
        $_country = Countries::findorfail($_order->CountryId);
        $orderInfo->currency = $_country->currencyDisplay;
        session()->forget('user');
        session()->forget('selection_trial_plan');
        session()->forget('checkout_trial_plan');
        session()->forget('selection_alacarte_ba');
        session()->forget('checkout_alacarte_ba');
        session()->forget('trial_checkout_payment_failed');
        session()->forget('alacarte_checkout_payment_failed');

        return view('postlogin.thankyou.thankyou')
            ->with('orderid', $this->orderService->formatOrderNumber($_order, $_country, false))
            ->with('currentCountryIso', $currentCountryIso)
            ->with('langCode', $langCode)
            ->with('deliveryDateFrom', $deliveryDateFrom)
            ->with('deliveryDateTo', $deliveryDateTo)
            ->with('orderInfo', $orderInfo)
            ->with('bladeName',$bladeName);
    }

    public function redirectToThankYou(Request $request){
        if (is_array(session()->get('currentCountry'))) {
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
            $urlLang = strtolower($currentCountryData['urlLang']);
        } else {
            $currentCountryData = session()->get('currentCountry');
            $currentCountryIso = strtolower($currentCountryData->codeIso);
            $urlLang = strtolower($currentCountryData->urlLang);
        }

        $url = route('ba.locale.thankyou', ['langCode' => $urlLang , 'countryCode' => strtolower($currentCountryIso), 'id' => $request->id]);
        return $url;
    }

}
