<?php

namespace App\Http\Controllers\BaWebsite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalesAgent  $salesAgent
     * @return \Illuminate\Http\Response
     */
    public function show(SalesAgent $salesAgent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalesAgent  $salesAgent
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesAgent $salesAgent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SalesAgent  $salesAgent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesAgent $salesAgent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesAgent  $salesAgent
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesAgent $salesAgent)
    {
        //
    }
}
