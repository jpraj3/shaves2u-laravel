<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class LaravelHelper
{

    public static function ConvertArraytoObject($array)
    {
        return json_decode(json_encode($array, JSON_FORCE_OBJECT));
    }

    public static function ConvertToNDecimalPoints($number, $total_decimal_points)
    {
        return number_format((float) ($number), $total_decimal_points, '.', '');
    }

    public static function GenerateUUID()
    {
        return Str::uuid()->toString();
    }

    public static function uniqueCombination($in, $minLength = 1, $max = 2000)
    {
        $count = count($in);
        $members = pow(2, $count);
        $return = array();
        for ($i = 0; $i < $members; $i++) {
            $b = sprintf("%0" . $count . "b", $i);
            $out = array();
            for ($j = 0; $j < $count; $j++) {
                $b{$j} == '1' and $out[] = $in[$j];
            }

            count($out) >= $minLength && count($out) <= $max and $return[] = $out;
        }
        return $return;
    }

    public static function isJson($string) {
        json_decode($string);
        return json_last_error() == JSON_ERROR_NONE;
    }

}
