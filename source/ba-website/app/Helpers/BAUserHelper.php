<?php

namespace App\Helpers;

use App\Models\Ecommerce\User;

class BAUserHelper
{

    public static function BAUserInfos($user_id)
    {
        $userdetails = (Object) array();

        if ($user_id) {
            $user = User::where('id', $user_id)->first();
            if ($user) {
                $d_card = Cards::where('isDefault', 1)->where('UserId', $user->id)->orderBy('created_at', 'DESC')->first();
                $d_billing_address = DeliveryAddresses::where('id', $user->defaultBilling)->first();
                $d_delivery_address = DeliveryAddresses::where('id', $user->defaultShipping)->first();
                $userdetails->defaults = (Object) array(
                    "card" => $d_card,
                    "delivery_address" => $d_delivery_address,
                    "billing_address" => $d_billing_address,
                );
                $cards = Cards::where('UserId', $user->id)->orderBy('created_at', 'DESC')->first();
                $userdetails->cards = $cards;
                $billing_addresses = DeliveryAddresses::where('id', $user->defaultBilling)->get();
                $userdetails->billing_addresses = $billing_addresses;
                $delivery_addresses = DeliveryAddresses::where('id', $user->defaultShipping)->get();
                $userdetails->delivery_addresses = $delivery_addresses;
            }
        }

        return $userdetails;
    }
}
