<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductBundleTranslates extends Model
{
    protected $table = 'productbundletranslates';
    //
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'langCode',
        'CountryId',
        'ProductBundleId',
        'created_at',
        'updated_at',
    ];
}
