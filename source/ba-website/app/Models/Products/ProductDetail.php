<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model {
    public $timestamps = false;
    protected $table = 'productdetails';
    //
    protected $fillable = [
        'type',
        'imageUrl',
        'title',
        'details',
        'langCode',
        'ProductId'
    ];

}
