<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductVariantOption extends Model {
    public $timestamps = false;
    protected $table = 'productvariantoptions';
    //
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'ProductVariantId',
        'ProductId'
    ];

}
