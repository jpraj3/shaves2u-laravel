<?php

namespace App\Models\Promotions;

use Illuminate\Database\Eloquent\Model;

class Promotions extends Model
{
    protected $table = 'promotions';
    //
    protected $fillable = [
        'discount',
        'bannerUrl',
        'emailTemplateId',
        'emailTemplateOptions',
        'expiredAt',
        'activeAt',
        'minSpend',
        'allowMinSpendForTotalAmount',
        'maxDiscount',
        'planIds',
        'productCountryIds',
        'planCountryIds',
        'freeProductCountryIds',
        'freeExistProductCountryIds',
        'ProductBundleId',
        'isGeneric',
        'timePerUser',
        'totalUsage',
        'appliedTo',
        'isFreeShipping',
        'CountryId',
        'isBaApp',
        'isOnline',
        'isOffline',
        'promotionType'
    ];
}
