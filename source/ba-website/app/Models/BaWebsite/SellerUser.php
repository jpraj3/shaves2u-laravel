<?php

namespace App\Models\BaWebsite;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class SellerUser extends Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    protected $table = 'sellerusers';

    protected $guard = 'bawebsite';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'badgeId',
        'icNumber',
        'password',
        'agentName',
        'startDate',
        'campaign',
        'email',
        'latestUpdatedDate',
        'CountryId',
        'isActive',
        'channelType',
        'eventLocationCode',
        'MarketingOfficeId',
        'lastLoginAt',
        'api_token',
    ];

    protected $hidden = [
        'password',
    ];

    // public function getAuthPassword()
    // {
    //     return $this->icNumber;
    // }

    // public function getAuthIdentifierName()
    // {
    //     return $this->getKeyName();
    // }

    // public function getAuthIdentifier()
    // {
    //     return $this->getKey();
    // }

}
