<?php

namespace App\Models\FileUploads;

use Illuminate\Database\Eloquent\Model;

class FileUploads extends Model {
    protected $table = 'fileuploads';
    //
    protected $fillable = [
        'name',
        'isReport',
        'reportType',
        'status',
        'url',
        'orderId',
        'bulkOrderId',
        'isDownloaded',
        'AdminId',
        'file_id'
    ];
}
