<?php

namespace App\Models\Rebates;

use Illuminate\Database\Eloquent\Model;

class RewardsIn extends Model {
    protected $table = 'rewardsins';
    //
    protected $fillable = [
        'CountryId',
        'UserId',
        'value',
        'rewardscurrencyId',
        'OrderId'
    ];

}
