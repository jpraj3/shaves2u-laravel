<?php

namespace App\Models\GeoLocation;

use Illuminate\Database\Eloquent\Model;

class SupportedLang extends Model
{
    public $timestamps = false;
    // Table Name
    protected $table = 'supportedlanguages';
    // Table Column
    protected $fillable = [
        'languageCode',
        'languageName',
        'createdAt',
        'updatedAt',
        'CountryId',
    ];
}
