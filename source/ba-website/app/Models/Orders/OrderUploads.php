<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderUploads extends Model {
    protected $table = 'orderuploads';
    //
    protected $fillable = [
        'doFileName',
        'wareHouseFileName',
        'orderNumber',
        'orderId',
        'bulkOrderId',
        'sku',
        'qty',
        'deliveryId',
        'carrierAgent',
        'status',
        'region',
        'created_at',
        'updated_at',
    ];

}
