<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model {
    protected $table = 'orderdetails';
    //
    protected $fillable = [
        'qty',
        'price',
        'OrderId',
        'currency',
        'startDeliverDate',
        'created_at',
        'updated_at',
        'OrderId',
        'PlanCountryId',
    ];

}
