<?php

namespace App\Models\BulkOrders;

use Illuminate\Database\Eloquent\Model;

class BulkOrders extends Model
{
    protected $table = 'bulkorders';
    //
    protected $fillable = [
        'status',
        'deliveryId',
        'email',
        'phone',
        'fullName',
        'SSN',
        'paymentType',
        'promoCode',
        'subscriptionIds',
        'taxRate',
        'taxName',
        'carrierKey',
        'carrierAgent',
        'vendor',
        'DeliveryAddressId',
        'BillingAddressId',
        'CountryId',
        'taxInvoiceNo',
    ];

}
