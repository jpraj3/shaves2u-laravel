<?php

namespace App\Models\BulkOrders;

use Illuminate\Database\Eloquent\Model;

class BulkOrderDetails extends Model
{
    protected $table = 'bulkorderdetails';
    //
    protected $fillable = [
        'qty',
        'price',
        'BulkOrderId',
        'currency',
        'startDeliverDate',
        'OrderId',
        'PlanId',
        'ProductCountryId',
    ];

}
