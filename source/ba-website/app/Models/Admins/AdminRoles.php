<?php

namespace App\Models\Admins;

use Illuminate\Database\Eloquent\Model;

class AdminRoles extends Model
{
    protected $table = 'adminroles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
