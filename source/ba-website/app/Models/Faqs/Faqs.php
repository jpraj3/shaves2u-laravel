<?php

namespace App\Models\Faqs;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    protected $table = 'faqs';
    protected $fillable = [
        'type',
        'title',
        'subtitle',
        'isSubscription',
        'isAlaCarte',
        'langCode',
        'CountryId',
    ];
}
