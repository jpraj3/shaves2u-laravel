<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class MetaTags extends Model
{
    public $timestamps = false;
    // Table Name
    protected $table = 'metatags';
    // Table Column
    protected $fillable = [
        'CountryId',
        'metatitle',
        'metaDescription',
        'ogTitle',
        'ogUrl',
        'ogDescription',
        'ogImage',
        'ogType',
        'pageType',
        'pageId',
    ];
}
