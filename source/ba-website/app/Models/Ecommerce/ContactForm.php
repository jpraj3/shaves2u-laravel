<?php

namespace App\Models\Ecommerce;

use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
    // Table Name
    protected $table = 'contactform';
    // Table Column
    protected $fillable = [
        'topic',
        'name',
        'email',
        'metaDescription',
        'contactNo',
        'enquiryMessage',
    ];
}
