<?php

namespace App\Models\Audits;

use Illuminate\Database\Eloquent\Model;

class EmailAudits extends Model
{
    protected $table = 'email_audits';
    //
    protected $fillable = [
        'email_key',
        'email_type',
        'email_data',
        'UserId',
        'SubscriptionId',
        'OrderId',
        'error',
    ];
}