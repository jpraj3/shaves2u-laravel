<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanTrialProducts extends Model
{
    protected $table = 'plantrialproducts';
    //
    protected $fillable = [
        'qty',
        'PlanId',
        'ProductCountryId',
    ];
}
