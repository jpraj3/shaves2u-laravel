<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanFrequency extends Model
{
    protected $table = 'plan_frequency';
    //
    protected $fillable = [
        'duration',
        'durationType',
        'durationName',
        'checkout_image_url',
        'checkout_duration_text',
        'checkout_details_text',
        'checkout_selected_details_text',
        'showInCheckoutPage',
        'planCategoryId',
        'countryId',
        'langCode'
    ];
}