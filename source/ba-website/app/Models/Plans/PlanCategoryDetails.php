<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanCategoryDetails extends Model
{
    protected $table = 'plan_category_details';
    //
    protected $fillable = [
        'PlanSkuId',
        'PlanCategoryId'
    ];

}
