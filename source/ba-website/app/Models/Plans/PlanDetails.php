<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanDetails extends Model
{
    protected $table = 'plan_details';
    //
    protected $fillable = [
        'PlanId',
        'ProductCountryId',
        'qty',
        'cycle',
        'isAnnual',
        'duration',
        'totalCycle'
    ];
}
