<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanSKU extends Model
{
    protected $table = 'plansku';
    //
    protected $fillable = [
        'sku',
        'rating',
        'isFeatured',
        'status',
        'slug',
        'order',
        'duration',
        'isAnnual'
    ];
}
