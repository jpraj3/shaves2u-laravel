<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class TranslationServiceProvider extends ServiceProvider
{
    /**
     * The path to the current lang files.
     *
     * @var string
     */
    protected $langPath;

    /**
     * Create a new service provider instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->langPath = resource_path('lang/' . strtolower(App::getLocale()));

    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Defers booting until locale is set.
        view()->composer('*', function ($view) 
        {
            $this->langPath = resource_path('lang/' . strtolower(App::getLocale()));
            Cache::forget('translations');
            Cache::rememberForever('translations', function () {
                return collect(File::allFiles($this->langPath))->flatMap(function ($file) {
                    return [
                        ($translation = $file->getBasename('.php')) => trans($translation),
                    ];
                })->toJson();
            });
        });
    }
}
