<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Sales App - Shaves2U</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fdb918">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{ asset('validator/jquery-validation-1.19.1/dist/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="shortcut icon" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <!-- sweetalert2 popup -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2.v8/sweetalert2.min.css') }}">
    <!-- Toastr Notification Plugin -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr-v2.1.4/toastr.min.css')}}"/>
    <script src="{{ asset('plugins/toastr-v2.1.4/toastr.min.js') }}"></script>   <!-- End Toastr Notification Plugin -->
    <script src="{{ asset('plugins/sweetalert2.v8/sweetalert2.all.min.js') }}"></script>
  
    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHXJ3CB');</script>
    <!-- End Google Tag Manager -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M7SX4VH');</script>
    <!-- End Google Tag Manager -->
    @endif

    @include('layouts.scripts.mainscripts')
    @include('layouts.global.styles')
    <style>
            input[type="text"]:disabled {
                background-color: white !important;
            }

            input[type="number"]:disabled {
                background-color: white !important;
            }

            .status-inactive {
                background-color: #ffc107 !important;
            }

            .status-active {
                background-color: #06cb7c !important;
            }

            .status-onhold {
                background-color: #6c757d !important;
            }

            select:disabled {
                background-color: white !important;
            }
        </style>
</head>

<body>
    
    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHXJ3CB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7SX4VH"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
        
    <div id="app">
        <header>
            @include('layouts.header.header')
            <header>
                <main class="py-2">
                @include('loading.loading')
                    @yield('content')
                </main>
    </div>
</body>

</html>
