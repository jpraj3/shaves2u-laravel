@section('javascript')
<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<!-- GLOBAL URLs for ease of access -->
<script>
    <?php  if (Auth::check()) { ?>
    if (window.location.hostname == 'dcistaging.com') {
        var GLOBAL_URL = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2] + '/' + '<?php 
        if (is_array(session()->get('currentCountry'))) {
            echo strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']); 
        } else {
        echo strtolower(session()->get('currentCountry')->urlLang); 
        }
        ?>' + '-' + '<?php 
        if (is_array(session()->get('currentCountry'))) {
        echo strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']); 
        } else {
        echo strtolower(session()->get('currentCountry')->codeIso); 
        }
        ?>';
        var GLOBAL_URL_V2 = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2];
    } else {
        var GLOBAL_URL = '/' + '<?php 
        if (is_array(session()->get('currentCountry'))) {
         echo strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']); 
        } else {
         echo strtolower(session()->get('currentCountry')->urlLang); 
        }
        ?>' + '-' + '<?php
        if (is_array(session()->get('currentCountry'))) {
        echo strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']); 
        } else {
        echo strtolower(session()->get('currentCountry')->codeIso); 
        }
        ?>';
        var GLOBAL_URL_V2 = '';
    }
    <?php } else {?>
        if (window.location.hostname == 'dcistaging.com') {
        var GLOBAL_URL = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2] + '/';
        var GLOBAL_URL_V2 = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2];
    } else {
        var GLOBAL_URL = '/';
        var GLOBAL_URL_V2 = '';
    }
    <?php } ?>
</script>
<script>let API_URL = "<?php echo config('environment.apiUrl'); ?>";
</script>
<!-- Nicepay -->
<script language="javascript">var IMP = window.IMP; // 생략해도 괜찮습니다.</script>
<!-- Storage services -->
<script src="{{ asset('js/helpers/storage.service.js') }}"></script>
<!-- Korean Address API -->
<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script src="{{ asset('js/helpers/laravel-helper.js') }}"></script>
<!-- Server Session -->
<script src="{{ asset('js/helpers/server-session.service.js') }}"></script>
<script>
all_sessions = @php echo json_encode(session() -> all()) @endphp;console.log(all_sessions);
</script>
<script>  window.translations = {!! Cache::get('translations') !!};</script>
<script>
    console.log(window.translations);
    function trans(key, replace = {}) {
        let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations);
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
        return translation;
    }
    function trans_choice(key, count = 1, replace = {}){
    let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations).split('|');
    translation = count > 1 ? translation[1] : translation[0];
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
    return translation;
    }
    // console.log(trans('validation.custom.validation.email.email', {attribute: 'email'}));
    // console.log(trans_choice('validation.custom.validation.email.plural_test', 1,{attempts: 1}));
</script>
<script>
let current_localeV2 = "<?php echo strtoupper(app()->getLocale()) ?>";
let current_locale = "<?php echo strtolower(app()->getLocale()) ?>";
window.localStorage.setItem('currentLocale', current_locale);
window.localStorage.setItem('current_lang', JSON.stringify(current_locale.toUpperCase()));
</script>
@show
<script src="{{ asset('js/auth/ba.auth.js') }}"></script>

<script>
    @if (Auth::check())
        let sellerCountryId = <?php echo Auth::user()->CountryId; ?>;
        let sellerPhoneExt = <?php echo view()->shared('callcode'); ?>;
    @endif
</script>
