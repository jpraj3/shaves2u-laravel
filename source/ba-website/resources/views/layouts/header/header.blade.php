<style>
    .topLogo-main {
        width: 59px;
        position: relative;
        left: -20px
    }

    .topLogo-login {
        width: 59px;
        position: relative;
    }


    .nav-link:hover {
        text-decoration: none;
        color: #f64d00 !important;
    }
</style>
<div class="col-xs-12">
    <nav class="navbar navbar-light bg-white shadow-sm bold-lg p-0">
        <div class="container-fluid">

            @if(Auth::check())
            @php
            // Get Data from session
            $sessionCountryData = session()->get('currentCountry');

            // If Data from session exist
            if (!empty($sessionCountryData)) {
            // Set Current country info based on session Data
            if (is_array($sessionCountryData)) {
            $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
            $urlLang = strtolower($sessionCountryData['urlLang']);
            } else {
            $currentCountryIso = strtolower($sessionCountryData->codeIso);
            $urlLang = strtolower($sessionCountryData->urlLang);
            }
            }
            // If Data from Session Storage does not exist
            else {
            // Redirect back to / where session is set
            redirect()->route('ba.locale.region.redirect');
            }
            $currentLocale = strtolower(app()->getLocale());
            @endphp
            <button class="navbar-toggler border-0 collapsed p-0 ml-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_left" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon" style="background-image: none;">
                    <i class="fa fa-bars color-orange" style="font-size: 25px"></i>
                </span>
            </button>
            <a class="navbar-brand mx-auto menu-header topLogo-main" href="{{ route('ba.locale.landing', ['langCode' => $urlLang, 'countryCode' => strtolower($currentCountryIso)]) }}">
                <img src="{{ asset('images/common/logo/main_logo.svg') }}" />
            </a>
            @else
            <a class="navbar-brand mx-auto menu-header topLogo-login" href="{{ route('ba.login') }}">
                <img src="{{ asset('images/common/logo/main_logo.svg') }}" />
            </a>
            @endif
        </div>
    </nav>
    @guest
    @else
    <div class="collapse navbar-collapse" id="navbarSupportedContent_left" style="background-color:grey;">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto" style="text-transform: uppercase;">
            <li class="nav-item text-uppercase text-center border-bottom pt-2" style="font-size:16px; text-decoration:none;">
                <a class="nav-link" href="{{ route('ba.locale.products', ['langCode' => $urlLang, 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]) }}" style=" color: white;">@lang('website_contents.global.header.products')</a>
            </li>
            <li class="nav-item text-uppercase text-center border-bottom" style="font-size:16px;">
                <a class="nav-link" href="{{ route('ba.locale.profile', ['langCode' => $urlLang, 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]) }}" style=" color: white;">@lang('website_contents.global.header.profile')</a>
            </li>
            <li class="nav-item text-uppercase text-center border-bottom" style="font-size:16px;">
                <a class="nav-link" href="{{ route('ba.locale.saleshistory', ['langCode' => $urlLang, 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]) }}" style=" color: white;">@lang('website_contents.global.header.sales_history')</a>
            </li>
            <li class="nav-item text-uppercase text-center border-bottom mb-2" style="font-size:16px;">
                <a class="nav-link" onclick="logout()" style=" color: white;">@lang('website_contents.global.header.log_out')</a>
            </li>
            <li class="nav-item text-uppercase text-center" style="font-size:16px;  color: white;">
                @if(session()->get('currentCountry'))
                @php($current_country = session()->get('currentCountry'))
                <a style="font-size: 15px;padding-top: 0px; color: white; text-decoration:none;" class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="col-12">
                        <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.strtolower($current_country->codeIso).'.png')}}" alt="">
                        {{$current_country->name}} ({{ $current_country->urlLang }})
                    </span>
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdownMenuLink" style="font-size:15px;background-color: grey;">
                    @if(view()->shared('allcountry'))
                    @foreach (view()->shared('allcountry') as $u)
                    @if(strtolower($u['codeIso']) == strtolower(view()->shared('currentCountryIso')) )


                    <a style="font-size: 15px;padding-top: 0px; color: white; text-decoration:none;" class="nav-link pt-0 pb-0" href="{{ route('ba.locale.landing', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=> strtolower($u['codeIso'])]) }}">
                    <span class="col-12">
                        <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.strtolower($current_country->codeIso).'.png')}}" alt="">
                        {{ $u['name']}} ({{  $u['urlLang'] }})
                    </span>
                    </a>


                    @endif
                    @endforeach
                    @endif
                </div>
                @endif
            </li>
        </ul>
    </div>
    @endguest
</div>