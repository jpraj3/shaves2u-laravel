<link rel="dns-prefetch"href="//fonts.gstatic.com"><link href="https://fonts.googleapis.com/css?family=Nunito"rel="stylesheet"><link href="{{ asset('css/app.css') }}"rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css"rel="stylesheet"

type='text/css'><style>@font-face {
    font-family: "Muli";
    src: url('{{asset("/fonts/Muli.ttf")}}');
}

@font-face {
    font-family: "Muli-Bold";
    src: url('{{asset("/fonts/Muli-Bold.ttf")}}');
}

@font-face {
    font-family: "Muli-ExtraBold";
    src: url('{{asset("/fonts/Muli-ExtraBold.ttf")}}');
}

@font-face {
    font-family: "Muli-Light";
    src: url('{{asset("/fonts/Muli-Light.ttf")}}');
}

@font-face {
    font-family: "Muli-SemiBold";
    src: url('{{asset("/fonts/Muli-SemiBold.ttf")}}');
}

@media (min-width: 992px) {
    main {
        min-height: 737px;
    }
}

a:hover {
    text-decoration: none;
}

.MuliPlain {
    font-family: "Muli";
}

.MuliBold {
    font-family: "Muli-Bold";
}

.MuliExtraBold {
    font-family: "Muli-ExtraBold";
}

.MuliLight {
    font-family: "Muli-Light";
}

.MuliSemiBold {
    font-family: "Muli-SemiBold";
}

main {
    background-color: #fff;
}

.text-overflow-center {
    margin-left: -100%;
    margin-right: -100%;
    text-align: center;
}

#footer {
    width: 100%;
    background: black;
    clear: both;
    position: relative;
    bottom: 0;
    display: block;
}

.footer-padding {
    padding: 20px 60px;
}

.inlineB {
    display: inline-block;
}

.row-center {
    display: contents;
    -webkit-box-align: center;
    align-items: center;
    -webkit-box-pack: center;
    justify-content: center;
}

.border-orange {
    border-color: #FE5000 !important;
}

.btn-custom {
    margin-bottom: 15px;
    font-size: 20px;
    width: 200px;
}

.btn-load-more-orange {
    border: solid 2px #FE5000 !important;
    border-radius: 5px;
    background: #FE5000;
    padding: 14px 28px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: white !important;
    margin: auto;
}

.btn-load-more-fb {
    border: solid 2px #367AC9 !important;
    border-radius: 5px;
    background: white;
    padding: 14px 28px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: #1C327A !important;
    margin: auto;
}

.btn-load-more {
    border: solid 2px #FE5000 !important;
    border-radius: 5px;
    background: rgba (255, 255, 255, 0);
    padding: 14px 28px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: #FE5000 !important;
    margin: auto;
}

.btn-load-more-white {
    border: solid 2px white;
    border-radius: 5px;
    background: rgba (255, 255, 255, 0);
    padding: 14px 28px;
    cursor: pointer;
    text-align: center;
    font-size: 16px;
    color: white;
    margin: auto;
}

.btn-load-section {
    display: block;
    border: none;
    background-color: #FE5000;
    cursor: pointer;
    text-align: center;
    font-size: 16px;
    color: white;
    margin: auto;
    width: 100%
}

.btn-black {
    display: block;
    border: none;
    background-color: black;
    padding: 2% 4% !important;
    cursor: pointer;
    text-align: center;
    font-size: 16px;
    color: white;
    margin: auto;
    width: 100%
}

.card,
.card-header {
    background-color: white !important;
    border-left: none !important;
    border-right: none !important;
}

.card-header {
    border-bottom: none !important;
}

.card-footer {
    background-color: white !important;
    margin-left: 33%;
    margin-right: 33%;
    border-top: none !important;
    border-bottom: 1px solid black;
}

@media (min-width: 1200px) {
    .container {
        max-width: 1040px !important;
    }
}

@media (max-width: 568px) {

    .card,
    .card-header {
        border: 0px !important;
    }
}

.carousel-indicators li {
    border-radius: 50%;
    width: 12px !important;
    height: 12px !important;
    background-color: #7f7f7f !important;
    margin: 10px !important;
    opacity: 1;
}

.carousel-indicators li.active {
    background-color: #FE5000 !important;
}

@media (max-width: 991px) {
    .border-md-bottom {
        border-bottom: 1px solid #dee2e6 !important;
    }

    .border-md-top {
        border-top: 1px solid #dee2e6 !important;
    }

    .btn-fixed {
        position: fixed;
        bottom: 0px;
        width: 100%;
        z-index: 999;
    }
}

@media (min-width: 992px) {
    .border-lg {
        border: 1px solid black;
    }

    .border-lg-right {
        border-right: 1px solid #dee2e6 !important;
    }

    .center-lg {
        text-align: center;
    }
}

.color-white {
    color: white !important;
}

.color-black {
    color: black !important;
}

.color-orange {
    color: #ff5001 !important;
}

.color-grey {
    color: #cccccc !important;
}

.color-darkgrey {
    color: #888888 !important;
}

.fs-8 {
    font-size: 8px !important;
}

.fs-12 {
    font-size: 12px !important;
}

.fs-14 {
    font-size: 14px !important;
}

.fs-16 {
    font-size: 16px !important;
}

.fs-18 {
    font-size: 18px !important;
}

.fs-20 {
    font-size: 20px !important;
}

.fs-25 {
    font-size: 25px !important;
}

.fs-30 {
    font-size: 30px !important;
}

.fs-40 {
    font-size: 40px !important;
}

.fs-48 {
    font-size: 48px !important;
}

.fs-80 {
    font-size: 80px !important;
}

.fs-96 {
    font-size: 96px !important;
}

.btn-start {
    display: block;
    width: 30%;
    border: none !important;
    background-color: #FE5000 !important;
    padding: 14px 28px !important;
    font-size: 16px !important;
    cursor: pointer;
    text-align: center;
    color: white !important;
    margin: auto;
}

.fa-angle-up,
.fa-angle-down,
.fa-angle-right {
    font-size: 3rem;
    color: #FE5000;
}

.navbar-light .navbar-nav .nav-link {
    color: rgba(0, 0, 0, 1);
}

.padd5 {
    padding: 5px;
}

.padd10 {
    padding: 10px;
}

.padd20 {
    padding: 20px;
}

.paddTB20 {
    padding-top: 20px;
    padding-bottom: 20px;
}

.paddTB40 {
    padding-top: 40px;
    padding-bottom: 40px;
}

.rotate-icon.fa-angle-down {
    -webkit-transform: rotate(-180deg);
    -moz-transform: rotate(-180deg);
    -o-transform: rotate(-180deg);
    -ms-transform: rotate(-180deg);
    transform: rotate(-180deg);
    transition-duration: 0.4s;
}

.rotate-icon.fa-angle-right {
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    transition-duration: 0.4s;
}

.rounded-10 {
    border-radius: 10px;
}

.rounded-t-10 {
    border-top-right-radius: 10px;
    border-top-left-radius: 10px;
}

.rounded-b-10 {
    border-bottom-right-radius: 10px;
    border-bottom-left-radius: 10px;
}

.collapsed .rotate-icon.fa-angle-down {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    transform: rotate(0deg);
}

.collapsed .rotate-icon.fa-angle-right {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    transform: rotate(0deg);
}

.bg-banner {
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}

.shadow-md {
    box-shadow: 0 .25rem .25rem rgba(0, 0, 0, .15) !important;
}

.tab-content {
    min-width: 100%;
}

.text-orng {
    color: #FF5001 !important;
}

.ico.icon-country {
    cursor: pointer;
    max-width: 22px;
    height: 22px;
    border-radius: 50%;
    border: 1px solid #d5d5d5;
}

a {
    background-color: transparent !important;
}

.bg-black {
    background-color: black;
}

.mt-1p {
    margin-top: 1%;
}

.mt-2p {
    margin-top: 2%;
}

.mt-5p {
    margin-top: 5%;
}

.mt-10p {
    margin-top: 10%;
}

.mb-1p {
    margin-bottom: 1%;
}

.mb-2p {
    margin-bottom: 2%;
}

.mb-5p {
    margin-bottom: 5%;
}

.mb-10p {
    margin-bottom: 10%;
}

.bg-orange {
    background-color: #FF5001;
}

.bg-green {
    background-color: #06cb7c;
}

.bg-grey {
    background-color: #cecece;
}

.custom-checkbox {
    margin-bottom: 0;
}

.custom-checkbox input {
    display: none;
}

.custom-checkbox span {
    background-color: white;
    border: 1px solid black;
    border-radius: 0px;
    float: right;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
}

.custom-checkbox #check_addon_always_deliver:checked+span:before {
    content: "✔";
    color: black;
    font-size: 2rem;
}

.custom-checkbox #check_disclaimer:checked+span:before {
    content: "✔";
    color: black;
    font-size: 1rem;
}

.pointer {
    cursor: pointer;
}

#navbarSupportedContent_left .dropdown.show .fa.fa-caret-down {
    -webkit-transform: rotate(-180deg);
    -moz-transform: rotate(-180deg);
    -o-transform: rotate(-180deg);
    -ms-transform: rotate(-180deg);
    transform: rotate(-180deg);
    transition-duration: 0.4s;
}

#navbarSupportedContent_left .dropdown:not(.show) .fa.fa-caret-down {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    transform: rotate(0deg);
    transition-duration: 0.4s;
}

.navbar-toggler:focus {
    outline: none !important;
}

#app .navbar-toggler:not(.collapsed) .fa.fa-bars {
    -webkit-text-stroke: 4px white;
    font-size: 36px;
    margin-top: -5px;
}

#app .navbar-toggler:not(.collapsed) .fa.fa-bars:before {
    content: "\f00d";
}

#app .navbar-toggler.collapsed .fa.fa-bars {
    font-size: 30px;
}

#app .navbar-toggler.collapsed .fa.fa-bars:before {
    content: "\f0c9";
}

.lh-25px {
    line-height: 25px;
}

.lh-40px {
    line-height: 40px;
}

.w-500px {
    width: 500px;
}

.w-60 {
    width: 60%;
}

.w-70 {
    width: 70%;
}

.w-100 {
    width: 100%;
}

.w-130 {
    width: 130%;
}

.w-150 {
    width: 150%;
}

}

);
background-repeat: no-repeat;
background-size: cover;
}

.pAbsolute {
    position: absolute;
}

.b-8px {
    bottom: 8px;
}

.d-none {
    display: none;
}

.truncate {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

</style>
