@extends('layouts.app')
@section('content')
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
<!--
<div id="thankYouPage" class="container-fluid" >
    <div class="page-inner">
        <div class="box cart-container">
            <div class="box-header box-flex">
                <h2 class="text-center text-uppercase">Thank you!</h2>
            </div>
            <div class="box-body">
                <p class="order-number text-uppercase" >your order number is {{$orderid}}</p>

                <p class="estimated-delivery" >The estimated delivery date is <strong>{{$deliveryDateFrom}}–{{$deliveryDateTo}}</strong>.</br>Email us at <a href="mailto:help.my@shaves2u.com">help.my@shaves2u.com</a> with any questions or suggestions.</p>
            </div>
        </div>
    </div>
</div>-->


<div id="thankYouPage" class="container-fluid pb-5 p-0 m-0" style="background-color: #eaeaea !important ;">
  <div class="panel panel-default" style="margin: 0px !important ;background: white;">
    <div class="mobile_heading1">

      <p class="fs-22 MuliBold panel-title  mobile_title1 m-0" style="padding-top: 15px; padding-bottom: 15px; text-align: center; font-size: 22px; color: #08cc7d;"><b>
          @lang('website_contents.authenticated.thankyou.order_confirmed')
        </b></p>
    </div>
  </div>
  <div class="page-inner">
    <div class="row p-0 m-0">
      <div class="col-12 rounded-10 pl-3 pr-3" style="background-color: #eaeaea !important ;">
        <div class="row p-0 m-0 mr-0 ml-0">
          <div class="col-12 pt-4 pb-1 pl-0 pr-0">
            <div class="col-12 rounded-t-10 bg-white shadow-sm pt-3">
              <div class="row p-0 m-0 mr-0 ml-0">
                <h4 class="MuliExtraBold">@lang('website_contents.authenticated.thankyou.order_summary')</h4>
                <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                  <div class="row p-0 m-0">
                    <div class="col-5 p-0 d-flex align-items-center">
                        @if($orderInfo->type === 'plans')
                        @if($orderInfo->planGender !== null && $orderInfo->planGender === 'female')
                        <img class="img-fluid" src="{{asset('/images/common/checkoutAssetsWomen/TrialKit-Women.jpg')}}" />
                        @else
                        <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}" />
                        @endif
                        @else
                        @if($m_h_sku == 'H1')
                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png" />
                        @elseif($m_h_sku == 'H3')
                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                        @else
                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                        @endif
                        @endif
                    </div>
                    <div class="col-7 p-0 d-flex align-items-center">
                      <div class="col-12 p-0">
                        @if($orderInfo->plantype === "trial-plan")
                        <h3 class="MuliBold">@lang('website_contents.authenticated.products.starter_kit')</h3>
                        <p class="fs-18 Muli"{{ $bladeName }}<span id="c-nfree-product"></span></p>
                        @elseif($orderInfo->plantype === "custom-plan")
                        <h3 class="MuliBold">@lang('website_contents.authenticated.thankyou.shave_plan')</h3>
                        <p class="fs-18 Muli">{{ $bladeName }}<span id="c-nfree-product"></span></p>
                        @else
                        <h3 class="MuliBold">@lang('website_contents.authenticated.products.products')</h3>
                        <p class="fs-18 Muli">@lang('website_contents.authenticated.alacarte.checkout.alacarte')<span id="c-nfree-product"></span></p>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row p-0 m-0 pt-2">
                  <div class="col-12" style="font-size: 18px;">
                    <label class="p-0 m-0">@lang('website_contents.authenticated.thankyou.order_no')</label>
                    <p class="color-orange MuliBold p-0 mb-3">#{{ $orderid }}</p>
                  </div>
                  <div class="col-12" style="font-size: 18px;">
                    <label class="p-0 m-0">@lang('website_contents.authenticated.thankyou.date')</label>
                    <p class="color-orange MuliBold p-0 mb-3">{{ \Carbon\Carbon::parse($orderInfo->order->created_at)->format('F d, Y, h:i A') }}</p>
                  </div>
                  <div class="col-12" style="font-size: 18px;">
                    <label class="p-0 m-0">@lang('website_contents.authenticated.thankyou.status')</label>
                    <p class="color-orange MuliBold p-0 mb-3">{{ $orderInfo->order->status }}</p>
                  </div>
                  <div class="col-12" style="font-size: 18px;">
                    <label class="p-0 m-0">@lang('website_contents.authenticated.thankyou.direct_delivery')</label>
                    <p class="color-orange MuliBold p-0 mb-3">{{ $orderInfo->order->isDirectTrial === 1 ? 'Yes' : 'No' }}</p>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-12 p-0" style="border-top: 10px black solid; background-color: white;">
                  <div class="col-12 d-inline-flex p-0" style="font-size: 20px;">
                    <div class="col-8 pl-4 pt-2 pr-0">
                      <label><b>@lang('website_contents.authenticated.thankyou.grand_total')</b></label>
                    </div>
                    <div class="col-4 pt-2 pl-0 pr-0">
                      <p class="MuliBold">{{$orderInfo->currency}} {{ $orderInfo->receipt->totalPrice }}</p>
                    </div>
                  </div>
                </div>
          </div>
        </div>
      </div>

      <?php
            $urllang = view()->shared('url');
            $iso = view()->shared('currentCountryIso');
            $url = url("/" . $urllang . "-" . $iso);
            ?>

    <div id="mobile-button" class="col-12 text-center pl-0 pr-0" style="position: fixed;  top: 93%;">
      <a class="button-next button-proceed-checkout-mobile MuliExtraBold" href="{{ $url }}" style="background-color: #ff5001  !important ;
    color: white;
    border: none;
    padding-top: 15px;
    padding-bottom: 15px;
    width: 100%;
    display: block;
    ">@lang('website_contents.authenticated.thankyou.done')</a>
    </div>
  </div>

</div>

<script>
  window.localStorage.removeItem('gender_selected');
  window.localStorage.removeItem('current_user');
</script>
@endsection
