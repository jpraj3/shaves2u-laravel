@extends('layouts.app')

@section('content')
<!-- PHP START -->
@php($user = $userdetails->user !== null ?  $userdetails->user : null)
@php($user_id = $userdetails->user !== null ?  $userdetails->user->id : null)
@php($delivery_address = $userdetails->defaults->delivery_address !== null ?  $userdetails->defaults->delivery_address : null)
@php($billing_address = $userdetails->defaults->billing_address !== null ?  $userdetails->defaults->billing_address : null)
@php($cards = $userdetails->cards)
@php($default_card = $userdetails->defaults->card !== null ?  $userdetails->defaults->card : null)
@php($session = $details->session_data)
@php($checkout_details = isset($details->checkout_details) ? $details->checkout_details : null )
@php($checkout_session = json_decode(session()->get('checkout_alacarte_ba')) ? json_decode(session()->get('checkout_alacarte_ba'))->checkout : null )
@php($phoneExt = $country_service->getCountryPhoneExt($userdetails->user ? $userdetails->user->CountryId : '') ? $country_service->getCountryPhoneExt($userdetails->user ? $userdetails->user->CountryId : '') : $callcode)
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
@php($sessionLoginError = session()->get('loginerror'))
<?php session()->forget('loginerror'); ?>
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/alacarte/alacarte-checkout.css') }}">
<style>
    input.mask_content {
        -webkit-text-security: disc;
    }

    .error {
        color: red;
    }

    .error-promotion {
        color: red;
    }

    .success-promotion {
        color: green;
    }

    .card-index {background-color: transparent !important;}
    .proceed-checkout {margin-bottom: 0px !important;}
    .hasError{
        padding:0px;color:red;margin-top:10px;
    }
    .notification {
        height: 40px;
        position: sticky;
        top: 0;
        width: 100%;
        display: table;
        background: #f4f8fc;
        opacity: 1;
        z-index: 10000;
    }

    .notification > span {
        text-align: center;
        margin: auto;
        display: table-cell;
        vertical-align: middle;
    }

    .click-able {
        color: #ed594a;
        cursor: pointer;
    }
</style>

<div class="grey-bg">

    <!-- Hidden Inputs for Checkout -->
    <input type="hidden" name="payment_intent_next_update_type" id="payment_intent_next_update_type" value="first-update" />
    <input type="hidden" name="payment_intent_id" id="payment_intent_id" value="" />
    <!-- END -->

    <!-- Begin: Mobile Layout -->
    <div class="row p-0 m-0">
        <div class="checkout-content-mobile col-12 pl-0 pr-0">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default" style="margin: 0px !important ;background: white;">
                    <div class="mobile_heading1 col-12">
                                <button id="button-back" class="button-back" style="position:absolute; bottom: -3px;">
                                    @lang('website_contents.global.content.back')
                                </button>
                        <div class="row mx-auto text-center">
                            <div class="fs-22 MuliBold panel-title mobile_title1 m-0 pt-0 mx-auto pb-2"><b>
                                @lang('website_contents.authenticated.checkout.content.payment')
                            </b>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="panel panel-default border-top" style="margin: 0px !important">
                    <div id="step1-heading" class="panel-heading panel-heading-selected mobile_heading1 " data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                        <h5 id="step1-title" class="panel-title panel-title-selected mobile_title1" style="padding-top: 15px; padding-bottom: 15px; text-align: center;">
                            @lang('website_contents.authenticated.checkout.account.account')
                        </h5>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse show">
                        <div class="panel-body">
                        <div class="col-12 pt-3 pb-4 pl-4 pr-4">
                                <div class="col-12 rounded-10 bg-white shadow-sm padd15">
                                    <div class="container-create-account">
                                        @if($userdetails->user === null)
                                        <!-- <div id="form_title" class="row ml-0 mr-0 pt-2">
                                          <i class="fa fa-check-circle ml-auto fs-30 text-orng"></i>
                                        </div> -->
                                        <form id="form_register" method="POST" action="" onsubmit="return false">
                                            @csrf
                                            <div class="col-lg-12 padd20 pt-0 pl-0 pr-0">
                                                <div class="col-lg-12 padd20 pl-0 pl-3 pr-0">
                                                    <div class="register-group form-group">
                                                        <label class="color-grey pb-0">@lang('website_contents.authenticated.checkout.account.name')</label>
                                                        <div class="input-group pr-4">
                                                            <input id="d-name-tk" name="name" type="text" class="name padd0 form-control MuliBold @error('name') is-invalid @enderror" required /> @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                        </div>
                                                        <div id="name_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="color-grey">@lang('website_contents.authenticated.checkout.account.email_address')</label>
                                                        <div class="input-group pr-4">
                                                            <input id="d-email-tk" name="email" type="email" class="email p-0 form-control MuliBold @error('email') is-invalid @enderror" required /> @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                        </div>
                                                        <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="hidden login-group form-group">
                                                        <label for="password" class="color-grey">@lang('website_contents.authenticated.checkout.account.password')</label>
                                                        <div class="input-group pr-4">
                                                            <input id="d-password-tk" name="password" type="password" class="password p-0 form-control MuliBold @error('password') is-invalid @enderror" required autocomplete="new-password"><span style="position:absolute; right: 0;" toggle="#d-password-tk" class="fa fa-eye field-icon toggle-password"></span>
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                        </div>
                                                        <div id="password_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                    </div>
                                                    <!-- <div class="register-group form-group">
                                                        <label class="color-grey pb-0">@lang('website_contents.authenticated.checkout.account.phone')</label>
                                                        <div class="input-group pr-4">
                                                            <input id="d-phone-tk" name="phone" type="text" class="phone p-0 form-control MuliBold @error('phone') is-invalid @enderror" required /> @error('phone');
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                            <div id="phone_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div> -->
                                                    <div class="error error_password"></div>
                                                </div>
                                                <div class="register-group form-group">
                                                    <div class="row">
                                                        <label for="date-of-birth" class="col-12 col-form-label text-left color-grey">@lang('website_contents.authenticated.checkout.account.date_of_birth')</label>
                                                        <div class="col-4 pl-3 MuliBold">
                                                            <select id="d-day-of-birth-tk" class="day-of-birth form-control custom-select @error('name') is-invalid @enderror" name="day-of-birth" required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.dd')</option>
                                                                @for($i=1;$i
                                                                <32;$i++) @switch($i) @case($i) @if($i < 10) <option value="{{sprintf(" %02d ", $i)}}">
                                                                    {{sprintf("%02d", $i)}}</option>
                                                                    @else
                                                                    <option value="{{ $i }}">{{$i}}</option>
                                                                    @endif @break @default @endswitch @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-4 pl-1 MuliBold">
                                                            <select id="d-month-of-birth-tk" class="month-of-birth form-control custom-select" name="month-of-birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.mm')</option>
                                                                @for($i=1;$i<13;$i++) @switch($i) @case(1) <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jan')</option>
                                                                @break @case(2)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.feb')</option>
                                                                @break @case(3)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.mar')</option>
                                                                @break @case(4)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.apr')</option>
                                                                @break @case(5)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.may')</option>
                                                                @break @case(6)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jun')</option>
                                                                @break @case(7)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jul')</option>
                                                                @break @case(8)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.aug')</option>
                                                                @break @case(9)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.sep')</option>
                                                                @break @case(10)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.oct')</option>
                                                                @break @case(11)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.nov')</option>
                                                                @break @case(12)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.dec')</option>
                                                                @break
                                                                @default
                                                                @endswitch
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-4 pr-3 MuliBold birth-year">
                                                            <select id="d-year-of-birth-tk" class="year-of-birth form-control custom-select" name="year-of-birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.yyyy')</option>
                                                                @for($i=30;$i
                                                                <100;$i++) @switch($i) @case($i) @if($i < 10) <option value="19{{sprintf(" %02d ", $i)}}">
                                                                    19{{sprintf("%02d", $i)}}</option>
                                                                    @else
                                                                    @if($i==80)
                                                                    <option value="19{{ $i }}" selected>19{{$i}}</option>
                                                                    @else
                                                                    <option value="19{{ $i }}">19{{$i}}</option>
                                                                    @endif
                                                                    @endif @break @default @endswitch @endfor
                                                                    @for($i=0;$i<(int)substr(date("Y"), -2) + 1;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf(" %02d ", $i)}}">
                                                                        20{{sprintf("%02d", $i)}}</option>
                                                                        @else
                                                                        <option value="20{{ $i }}">20{{$i}}</option>
                                                                        @endif @break @default @endswitch @endfor
                                                            </select>
                                                        </div>
                                                        @error('date-of-birth')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span> @enderror
                                                        <div id="birthday_error" class="hasError col-12 col-form-label text-left pl-4"></div>
                                                    </div>
                                                </div>

                                                @if(strtolower($currentCountryIso) == 'kr')
                                                    <div class="form-group register-group">
                                                        <div class="row text-left">
                                                            <div class="input-group">
                                                                <input required id="tos_agree" name="tos_agree" type="checkbox" class="col-1 @error('tos_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0"><u>@lang('website_contents.authenticated.checkout.account.tos_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="privacy_policy_agree" name="privacy_policy_agree" type="checkbox" class="col-1 @error('privacy_policy_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0"><u>@lang('website_contents.authenticated.checkout.account.privacy_policy_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="personal_info_agree" name="personal_info_agree" type="checkbox" class="col-1 @error('personal_info_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0"><u>@lang('website_contents.authenticated.checkout.account.personal_info_transfer_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="min_age_agree" name="min_age_agree" type="checkbox" class="col-1 @error('min_age_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0">@lang('website_contents.authenticated.checkout.account.minimum_age_agreement')</p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input id="email_sub_agree" name="email_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0">@lang('website_contents.authenticated.checkout.account.email_sub_agreement')</p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input id="sms_sub_agree" name="sms_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0">@lang('website_contents.authenticated.checkout.account.sms_sub_agreement')</p>
                                                            </div>
                                                            <ul class="col-11 offset-1">
                                                                <li>@lang('website_contents.authenticated.checkout.account.sms_sub_agreement_point1')</li>
                                                                <li>@lang('website_contents.authenticated.checkout.account.sms_sub_agreement_point2')</li>
                                                            </ul>
                                                        </div>
                                                        @error('required_checked')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                        <div id="required_checked_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                @else
                                                    <div class="form-group register-group">
                                                        <div class="row">
                                                            <div class="input-group">
                                                                <input id="marketing_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authenticated.checkout.account.marketing_sub_agreement')</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-lg-12 pl-3 pr-0 pt-1">
                                                    <h6 class="text-sign-in">@lang('website_contents.authenticated.checkout.account.have_account')</h6>
                                                    <h6 class="text-create-one hidden">@lang('website_contents.authenticated.checkout.account.no_account')</h6>
                                                </div>
                                                <div class="col-12 p-0" style="bottom:0; border-radius: 0% !important;position: fixed; left:0px">
                                                    <div class="col text-center pt-lg-5 pl-0 pr-0">
                                                        <button id="btn-create-account-desktop" style="color:white !important;margin-bottom: 0px !important;border-radius: 0% !important;" class="btn btn-create-account button-proceed-checkout MuliExtraBold" type="submit">@lang('website_contents.authenticated.checkout.account.confirm_account')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        @else
                                        <!-- if user has logged in or registered before -> session is still active -->
                                        <div class="container-profile-account">

                                            <div class="col-lg-12 padd20 pl-0 pr-0 pb-0">

                                                <div class="col-lg-12 pl-0 pr-0">
                                                    <div class="form-group">
                                                        <label class="color-grey pl-2">@lang('website_contents.authenticated.checkout.account.name')</label>
                                                        <div class="input-group">
                                                            <label class="MuliBold form-control">{{ $userdetails->user->firstName }}</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="color-grey pl-2">@lang('website_contents.authenticated.checkout.account.email')</label>
                                                        <div class="input-group">
                                                            <label class="MuliBold form-control">{{ $userdetails->user->email }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 pl-2 pr-0 pt-1 pb-3" onClick="logoutfromCheckout()">
                                                <h6 class="text-sign-in">@lang('website_contents.authenticated.checkout.account.switch_account')</h6>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div id="step2-heading" class="panel-heading panel-heading-unselected mobile_heading2 " data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                        <h5 id="step2-title" class="panel-title panel-title-unselected mobile_title2 pl-2" style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
                            @lang('website_contents.authenticated.checkout.address.shipping_address')
                        </h5>
                    </div>
                    @if($userdetails->user !== null)
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-12 pt-3 pb-4 pl-4 pr-4">
                                <div class="rounded-10 bg-white pt-2 pb-3">
                                    @if(count($userdetails->delivery_addresses) > 0)
                                    @if(strtolower($details->currentCountryIso) == 'kr')
                                    @include('postlogin.products.trial-plans.checkout-journey.checkout-modules.delivery-address-kr-edit')
                                    @else
                                    @include('postlogin.products.trial-plans.checkout-journey.checkout-modules.delivery-address-edit')
                                    @endif
                                    @else
                                    @if(strtolower($details->currentCountryIso) == 'kr')
                                    @include('postlogin.products.trial-plans.checkout-journey.checkout-modules.delivery-address-kr-add')
                                    @else
                                    @include('postlogin.products.trial-plans.checkout-journey.checkout-modules.delivery-address-add')
                                    @endif
                                    @endif
                                    <div class="col-12 pt-4 pl-0 pr-0" hidden>
                                        @if(count($userdetails->delivery_addresses) > 0)
                                        <div class="row p-0 m-0 mx-0">
                                            <div class="col-6 pl-0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">@lang('website_contents.authenticated.checkout.address.edit_address')</button>
                                            </div>
                                            <div class="col-6 pr-0">
                                                <button id="enable-address-add" class="btn btn-load-more col-10" onclick="addAddress()">@lang('website_contents.authenticated.checkout.address.add_address')</button>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row p-0 m-0 mx-0">
                                            <div class="col-6 p-0">
                                                <button id="enable-address-add" class="btn btn-load-more col-10" onclick="addAddress()">@lang('website_contents.authenticated.checkout.address.add_address')</button>
                                            </div>
                                            <div class="col-6 p-0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()" style="display:none;">@lang('website_contents.authenticated.checkout.address.edit_address')</button>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-6 p-0">
                                            <button id="cancel-address-edit" class="btn btn-load-more col-12" onclick="cancelEditAddress()" style="display:none;">@lang('website_contents.global.content.cancel')</button>
                                            <label class="pt-4" id="estimated-delivery-date" style="font-weight: bold; font-size: 13px">@lang('website_contents.authenticated.checkout.address.edit_address', ['date1' => Carbon\Carbon::now()->addDays(7)->format('M d'), 'date2' => Carbon\Carbon::now()->addDays(14)->format('M d')])</label>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End: Mobile Shipping -->
                    @endif
                </div>
            </div>

            <div class="panel panel-default">
                <div id="step3-heading" class="panel-heading panel-heading-unselected mobile_heading3 " data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step3-title" class="panel-title panel-title-unselected mobile_title3" style="padding-top: 20px; padding-bottom: 15px; text-align: center;">
                        @lang('website_contents.authenticated.checkout.payment_method.payment_method')
                    </h5>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-12 pt-2 pb-2">
                            <div class="col-12 rounded-10 bg-white shadow-sm">
                                <div class="row p-0 m-0 mr-0 ml-0 pt-2">
                                    <!-- <h4 class="MuliExtraBold">PAYMENT METHOD</h4> -->
                                    @if(count($userdetails->cards) === 0)
                                    <!-- Add card -->
                                    <div id="add-card-container" class="">
                                    <form id="form_card">
                                        <div class="row p-0 m-0 mr-0 ml-0">
                                            {{-- <div class="col-12 pl-0 pb-0 pr-0">
                                                <div class="col-lg-12 pl-0 pr-0 bg-card pt-1" style="background-image: url({{asset('/images/cards/visa_card.png')}});">
                                                    <div class="card-index ml-auto mr-4 text-center fs-12 mb-5" style="background-color: transparent !important;">

                                                    </div>
                                                    <div class="card-number text-center text-white mt-2 mb-5">
                                                        <span class="append_selected_cardnumber">XXXX XXXX XXXX <span class="cc_last4digits">1234</span></span>
                                                    </div>
                                                    <div class="card-expiry mr-4 text-right text-white fs-12 pb-4">
                                                        <p class="append_selected_cardexpiry">09 / 20</p>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-12 pl-0 pr-0 pb-0">
                                                <div class="row p-0 m-0 mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <div class="form-group pl-0 pr-0">
                                                            <label>@lang('website_contents.authenticated.checkout.payment_method.card_number')</label>
                                                            <input id="card-number-masked" name="card_number_masked" type="text" class="card_number_masked number-only fs-20 form-control pl-0" maxlength="19" />
                                                            <input id="card-number" name="card_number" type="hidden" class="credit-card-logo card_number number-only fs-20 pl-0" maxlength="19" />
                                                            <div id="c_cardNumber_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 padd20 pl-0 pt-0 pb-0">
                                                        <div class="form-group pl-0 pr-0 pb-0">
                                                            <label>@lang('website_contents.authenticated.checkout.payment_method.expiry_date')</label>
                                                            <input required id="card-expiry" name="card_expiry_date" type="text" class="card_expiry_date number-only fs-20 form-control" maxlength="7" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 padd20 pr-0 pt-0 pb-0">
                                                        <div class="form-group pl-0 pr-0 pb-0">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.cvv')</label>
                                                            <input required id="card-cvv" name="card_cvv" type="text" class="card_cvv number-only fs-20 form-control mask_content"  maxlength="4" />
                                                            <div id="c_ccv_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div>
                                                    <div id="c_expirydate_error" class="hasError col-12 col-form-label text-left"></div>
                                                    <div id="c_ccv_error" class="hasError col-12 col-form-label text-left"></div>
                                                    <div id="error-payment-method" class="error MuliPlain hidden padd20 pr-0 pt-0"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <div class="row p-0 m-0">
                                                    <div class="col-12">
                                                        <button id="button-add-card" class="btn btn-load-more w-100 hidden" type="button"><b>@lang('website_contents.authenticated.checkout.payment_method.save_card')</b></button>
                                                    </div>
                                                    <div class="col-12">
                                                        <button id="button-cancel-add-new-card" class="btn btn-load-more hidden w-100" type="button"><b>@lang('website_contents.global.content.cancel')</b></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 pl-0 pr-0 pl-2">
                                                <p>
                                                    @lang('website_contents.authenticated.checkout.payment_method.terms_of_service_agree')
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div id="edit-card-container" class="hidden">
                                        <div class="row  p-0 m-0 mr-0 ml-0">
                                            {{-- <div class="col-12 pl-0 pb-0 pr-0">
                                                <div class="col-lg-12 pl-0 pr-0 bg-card pt-1" style="background-image: url({{asset('/images/cards/visa_card.png')}});">
                                                    <div class="card-index ml-auto mr-4 text-center fs-12 mb-5" style="background-color: transparent !important;">

                                                    </div>
                                                    <div class="card-number text-center text-white mt-2 mb-5">
                                                        <span class="append_selected_cardnumber">XXXX XXXX XXXX <span class="cc_last4digits">1234</span></span>
                                                    </div>
                                                    <div class="card-expiry mr-4 text-right text-white fs-12 pb-4">
                                                        <p class="append_selected_cardexpiry">09 / 20</p>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-12 pl-0 pr-0">
                                                <div class="row  p-0 m-0 mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.selected_card')</label>
                                                            <select id="card-dropdown" class="MuliBold form-control">
                                                                @foreach($userdetails->cards as $card)
                                                                    <option value="{{ json_encode($card) }}">xxxx xxxx xxxx {{  $card->cardNumber }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 padd20 pl-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.expiry_date')</label>
                                                            <input id="view-expiry-date" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="12 / 24" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 padd20 pr-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.cvv')</label>
                                                            <input id="view-cvv" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="xxx" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center justify-content-start">
                                                <div class="col-12 p-0 m-0 pl-0 pb-2">
                                                    <button id="button-add-new-card" class="btn btn-load-more w-100 uppercase"><b>@lang('website_contents.authenticated.checkout.payment_method.add_new_card')</b></button>
                                                </div>
                                            </div>
                                            <div class="col-12 pr-0 pl-2">
                                                <p>
                                                    @lang('website_contents.authenticated.checkout.payment_method.terms_of_service_agree')
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <!-- Edit card -->
                                    <div id="edit-card-container" class="">
                                    <div class="row  p-0 m-0 mr-0 ml-0">
                                            {{-- <div class="col-12 pl-0 pb-0 pr-0">
                                                <div class="col-lg-12 pl-0 pr-0 bg-card credit-card-bg pt-1">
                                                    <div class="card-index ml-auto mr-4 text-center fs-12 mb-5" style="background-color: transparent !important;">

                                                    </div>
                                                    <div class="card-number text-center text-white mt-2 mb-5">
                                                        <span class="append_selected_cardnumber">XXXX XXXX XXXX <span class="cc_last4digits">1234</span></span>
                                                    </div>
                                                    <div class="card-expiry mr-4 text-right text-white fs-12 pb-4">
                                                        <p class="append_selected_cardexpiry">09 / 20</p>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-12 pl-0 pr-0">
                                            <div class="row  p-0 m-0 mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.selected_card')</label>
                                                            <select id="card-dropdown" class="MuliBold form-control">
                                                                @foreach($userdetails->cards as $card)
                                                                    <option value="{{ json_encode($card) }}">xxxx xxxx xxxx {{  $card->cardNumber }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-8 padd20 pl-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.expiry_date')</label>
                                                            <input id="view-expiry-date" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="12 / 24" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 padd20 pr-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.authenticated.checkout.payment_method.cvv')</label>
                                                            <input id="view-cvv" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="xxx" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center justify-content-start">
                                            <div class="col-12 p-0 m-0 pl-0 pb-2">
                                                    <button id="button-add-new-card" class="btn btn-load-more w-100 uppercase"><b>@lang('website_contents.authenticated.checkout.payment_method.add_new_card')</b></button>
                                                </div>
                                            </div>
                                            <div class="col-12 pr-0 pl-2">
                                                <p>
                                                    @lang('website_contents.authenticated.checkout.payment_method.terms_of_service_agree')
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="add-card-container" class="hidden">
                                            <form id="form_card">
                                            <div class="row  p-0 m-0 mr-0 ml-0">
                                                    {{-- <div class="col-12 pl-0 pb-0 pr-0">
                                                        <div class="col-lg-12 pl-0 pr-0 bg-card credit-card-bg pt-1">
                                                            <div class="card-index ml-auto mr-4 text-center fs-12 mb-5" style="background-color: transparent !important;">

                                                            </div>
                                                            <div class="card-number text-center text-white mt-2 mb-5">
                                                                <span class="append_selected_cardnumber">XXXX XXXX XXXX <span class="cc_last4digits">1234</span></span>
                                                            </div>
                                                            <div class="card-expiry mr-4 text-right text-white fs-12 pb-4">
                                                                <p class="append_selected_cardexpiry">09 / 20</p>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-12 pl-0 pr-0 pb-0">
                                                    <div class="row p-0 m-0 mr-0 ml-0">
                                                            <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                                <div class="form-group pl-0 pr-0">
                                                                    <label>@lang('website_contents.authenticated.checkout.payment_method.card_number')</label>
                                                                    <input id="card-number-masked" name="card_number_masked" type="text" class="card_number_masked number-only fs-20 form-control pl-0" maxlength="19" />
                                                                    <input id="card-number" name="card_number" type="hidden" class="credit-card-logo card_number number-only fs-20 pl-0" maxlength="19" />
                                                                    <div id="c_cardNumber_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-8 padd20 pl-0 pt-0 pb-0">
                                                                <div class="form-group pl-0 pr-0 pb-0">
                                                                    <label>@lang('website_contents.authenticated.checkout.payment_method.expiry_date')</label>
                                                                    <input required id="card-expiry" name="card_expiry_date" type="text" class="card_expiry_date number-only fs-20 form-control" placeholder="xx / xx" maxlength="7" />
                                                                </div>
                                                            </div>
                                                            <div class="col-4 padd20 pr-0 pt-0 pb-0">
                                                                <div class="form-group pl-0 pr-0 pb-0">
                                                                    <label>@lang('website_contents.authenticated.checkout.payment_method.cvv')</label>
                                                                    <input required id="card-cvv" name="card_cvv" type="text" class="card_cvv number-only fs-20 form-control mask_content" placeholder="xxx" maxlength="4" />
                                                                </div>
                                                            </div>
                                                            <div id="c_expirydate_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                            <div id="c_ccv_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                            <div id="error-payment-method2" class="error MuliPlain hidden padd20 pr-0 pt-0"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 text-center pb-2">
                                                        <div class="row">
                                                            <div class="col-12 pb-2">
                                                                <button id="button-add-card" class="btn btn-load-more w-100 hidden" type="button"><b>@lang('website_contents.authenticated.checkout.payment_method.save_card')</b></button>
                                                            </div>
                                                            <div class="col-12">
                                                                <button id="button-cancel-add-new-card" class="btn btn-load-more hidden w-100" type="button"><b>@lang('website_contents.global.content.cancel')</b></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 pl-0 pr-0 pl-2">
                                                        <p>
                                                            @lang('website_contents.authenticated.checkout.payment_method.terms_of_service_agree')
                                                        </p>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>

                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: Mobile Payment Method -->

            </div>

            <div class="panel panel-default">
                <div id="ste4-heading" class="panel-heading panel-heading-unselected mobile_heading4 " data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step4-title" class="panel-title panel-title-unselected mobile_title4" style="padding-top: 20px; padding-bottom: 15px; text-align: center;">
                            @lang('website_contents.authenticated.checkout.event.event_location')
                    </h5>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-12 pt-2 pb-3">
                            <div class="col-12 rounded-10 bg-white shadow-sm ">
                            <div class="row  p-0 m-0 mr-0 ml-0">

                                    <div class="col-12 pt-2 pb-1 pl-0 pr-0">

                                    <div class="col-12 rounded-t-10 bg-white pt-3 pb-0 pl-0 pr-0">
                                    <div class="row  p-0 m-0 mr-0 ml-0">
                                                <div class="col-lg-12 padd20 pb-0 pl-0 pr-0 pt-0">
                                                    <div class="col-lg-12 padd20 pb-2 pl-0 pr-0 pt-0">
                                                        <form id="event_location_form">
                                                            <div class="form-group">
                                                                <label class="color-grey">@lang('website_contents.authenticated.checkout.event.channel_type')</label>
                                                                <div class="input-group">
                                                                    <select required placeholder="@lang('website_contents.authenticated.checkout.event.location_code')" id="ba_channel_type" name="ba_channel_type" class="MuliBold form-control">
                                                                        <option value="Event" selected>@lang('website_contents.authenticated.checkout.event.event')</option>
                                                                        <option value="Streets">@lang('website_contents.authenticated.checkout.event.streets')</option>
                                                                        <option value="B2B">@lang('website_contents.authenticated.checkout.event.b2b')</option>
                                                                        <option value="RES">@lang('website_contents.authenticated.checkout.event.res')</option>
                                                                        @if($userdetails->current_country->name === 'Malaysia')
                                                                        <option value="Bazar">@lang('website_contents.authenticated.checkout.event.bazar')</option>
                                                                        <option value="Truck">@lang('website_contents.authenticated.checkout.event.truck')</option>
                                                                        <option value="Roadshow">@lang('website_contents.authenticated.checkout.event.roadshow')</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div id="channel_code_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="color-grey">@lang('website_contents.authenticated.checkout.event.location_code')</label>
                                                                <div class="input-group">
                                                                    <input required type="text" id="ba_event_location_code" name="ba_event_location_code" class="MuliBold form-control"/>
                                                                </div>
                                                                <div id="event_code_error" class="hasError col-12 col-form-label text-left pb-0"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div id="step5-heading" class="panel-heading panel-heading-unselected mobile_heading5 " data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                        <h5 id="step5-title" class="panel-title panel-title-unselected mobile_title5" style="padding-top: 20px; padding-bottom: 15px; text-align: center;">
                            @lang('website_contents.authenticated.checkout.payment_summary.total_summary')
                        </h5>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-12 pt-2 pb-5">
                                <div class="col-12 rounded-10 bg-white shadow-sm pl-0 pr-0 pt-3">
                                    <div class="row mr-0 ml-0">
                                        <div class="col-12 padd20 pl-0 pr-0 pl-4 pb-0">
                                            <div class="row p-0 m-0">
                                                <div class="col-5 p-0 d-flex align-items-center">
                                                        @if($m_h_sku == 'H1')
                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png" />
                                                        @elseif($m_h_sku == 'H3')
                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                                                        @else
                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                                                        @endif
                                                </div>
                                                <div class="col-7 p-0 d-flex align-items-center">
                                                    <div class="col-12 p-0">
                                                        <h3 class="MuliBold">@lang('website_contents.authenticated.products.products')</h3>
                                                        <p class="fs-18 Muli">@lang('website_contents.authenticated.alacarte.checkout.alacarte')<span id="c-nfree-product"></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pt-2 pb-1 pl-0 pr-0">
                                            <div class="col-12 rounded-t-10 bg-white shadow-sm pt-3 pb-0 pl-2 pr-2">
                                                <div class="row mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 border-bottom border-top fs-18">
                                                        <div class="row mr-0 ml-0">
                                                            <div class="col-12 padd0">
                                                                <h6 class="col-12 padd0">@lang('website_contents.authenticated.checkout.payment_summary.subtotal') <span class="pull-right color-orange">{{ $details->currency }} <span id="c-subtotal">{{ $details->current_price }}</span></span></h6>
                                                                <h6 class="col-12 padd0">@lang('website_contents.authenticated.checkout.payment_summary.shipping') <span class="pull-right color-orange">{{ $details->currency }} <span id="c-shipping">{{ $details->shipping_fee}}</span></span></h6>
                                                                <h6 class="col-12 padd0">@lang('website_contents.authenticated.checkout.payment_summary.discount') <span class="pull-right color-orange">{{ $details->currency }} <span id="c-discount">0.00</span></span></h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <p id="c-tax" class="d-none">{{ $details->taxAmount}}</span>
                                                        <p class="d-none" id="c-free-product"></p>
                                                        <p class="d-none" id="c-free-exist-product"></p>
                                                        <p class="fs-18 pl-4">@lang('website_contents.authenticated.checkout.payment_summary.promo_code')</p>

                                                        <div class="row mr-0 ml-0">
                                                            <div class="col-7 padd0">
                                                                    <div class="form-group">
                                                                        <input id="promo_code" type="text" class="form-control" style="font-size: 14px" placeholder="@lang('website_contents.authenticated.checkout.payment_summary.enter_promo_code')" />
                                                                    </div>
                                                            </div>
                                                            <div class="col-5 padd0">
                                                                <button class="btn btn-load-more pt-1 pb-1" onclick="applyPromotion()"><b>@lang('website_contents.authenticated.checkout.payment_summary.apply')</b></button>
                                                            </div>
                                                            <p class="error-promotion" id="error-promotion"></p>
                                                            <p class="success-promotion" id="success-promotion"></p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 bg-dark shadow-sm padd20 text-white">
                                                <div class="row mr-0 ml-0">
                                                    <div class="col-7 padd0">
                                                        <h5 class="MuliExtraBold mb-0">@lang('website_contents.authenticated.checkout.payment_summary.today_total')</h5>
                                                    </div>
                                                    <div class="col-5 padd0">
                                                        <h5 class="MuliExtraBold mb-0 text-right">{{ $details->currency }}
                                                            <span id="c-total">{{App\Helpers\LaravelHelper::ConvertToNDecimalPoints($details->current_price + $details->shipping_fee,2)}}</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form id="isDirectTrial_form" class="col-12 d-inline-flex rounded-10 border-direct mt-3">
                                    <div class="col-12 d-inline-flex">
                                        <div class="col-10">
                                            <p class="mt-3"><b>@lang('website_contents.authenticated.alacarte.checkout.collect_products')</b></p>
                                        </div>
                                        <div class="col-2">
                                            <input type="checkbox" id="ba_is_direct_trial" name="ba_is_direct_trial" class="MuliBold form-control mt-2"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(strtolower($details->currentCountryIso) == 'kr')
            <div id="form-pay-kr" class="hidden">
                {{ csrf_field() }}
                <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.authenticated.checkout.content.pay_now')</button>
            </div>
            @else
            <div id="form-pay"  class="col-12 text-center pl-0 pr-0 hidden" style="position: fixed;  top: 93%;">
                {{ csrf_field() }}
                <button class="button-next button-proceed-checkout-mobile MuliExtraBold" style="background-color: #ff5001  !important ;
    color: white;
    border: none;
    padding-top: 11px;
    padding-bottom: 15px;
    width: 100%;
    display: block;"type="submit" style="width: 100%">@lang('website_contents.authenticated.checkout.content.pay_now')</button>
            </div>
            @endif

        </div>
        <div class="panel panel-default ">
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-12 pt-2 pb-5">
                        <div class="col-12 rounded-t-10 bg-white shadow-sm padd15">
                            <div class="row mr-0 ml-0">
                                <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                                    <div class="row">
                                        <div class="col-5 padd0 d-flex align-items-center">
                                                @if($m_h_sku == 'H1')
                                                <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png" />
                                                @elseif($m_h_sku == 'H3')
                                                <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                                                @else
                                                <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png" />
                                                @endif
                                        </div>
                                        <div class="col-7 padd0 d-flex align-items-center">
                                            <div class="col-12 padd0">
                                                <h3 class="MuliBold">@lang('website_contents.authenticated.products.products')</h3>
                                                <p class="fs-18 Muli">@lang('website_contents.authenticated.alacarte.checkout.alacarte')</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                    <div class="row mr-0 ml-0">
                                        <div class="col-6 padd0">
                                            <p>@lang('website_contents.authenticated.checkout.payment_summary.subtotal')</p>
                                            <p>@lang('website_contents.authenticated.checkout.payment_summary.shipping')</p>
                                            <p>@lang('website_contents.authenticated.checkout.payment_summary.discount')</p>
                                        </div>
                                        <div class="col-6 padd0 text-right text-orng">
                                            <p>{{ $details->currency }} <span> {{ $details->current_price }}</span></p>
                                            <p>{{ $details->currency }} {{ $details->shipping_fee}}</p>
                                            <p>{{ $details->currency }} <span>0.00</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 padd20 pl-0 pr-0">
                                    <div class="row mr-0 ml-0">
                                        <div class="col-7 padd0">
                                            <p class="fs-18">@lang('website_contents.authenticated.checkout.payment_summary.promo_code')</p>
                                            <div class="form-group">
                                                <label>@lang('website_contents.authenticated.checkout.payment_summary.enter_promo_code')</label>
                                                <input type="text" class="fs-20 form-control" placeholder="123456" />
                                            </div>
                                        </div>
                                        <div class="col-5 padd0">
                                            <button class="btn btn-load-more" style="width: 100%; position: absolute; bottom: 20px;"><b>@lang('website_contents.authenticated.checkout.payment_summary.apply')</b></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 bg-dark shadow-sm padd15 text-white">
                            <div class="row mr-0 ml-0">
                                <div class="col-6 padd0">
                                    <p class="MuliExtraBold fs-18 mb-0">@lang('website_contents.authenticated.checkout.payment_summary.today_total')</p>
                                </div>
                                <div class="col-6 padd0 text-right">
                                    <p class="MuliExtraBold fs-18 mb-0">{{ $details->currency }} <span>{{App\Helpers\LaravelHelper::ConvertToNDecimalPoints($details->current_price + $details->shipping_fee,2)}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End: Mobile Payment Summary -->

        </div>
    </div>
    <!-- End: Mobile Layout -->
    <br>
    @if($userdetails->user !== null)
    <!--Proceed!-->
    <div class="col-12 p-0" style="bottom: -23px;border-radius: 0% !important;position: fixed;">
        <div class="col text-center pt-lg-5 pl-0 pr-0">
            <button id="button-next" style="margin-bottom: 0px !important;border-radius: 0% !important;" class="btn button-proceed-checkout MuliExtraBold" type="submit">@lang('website_contents.authenticated.checkout.account.confirm_account')</button>
        </div>
        <form id="form-checkout" class="d-none" method="GET" action="">
            {{ csrf_field() }}
            <div class="col text-center padd0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                <button id="proceed-pay" style="margin-bottom: 0px !important;border-radius: 0% !important;" class="btn button-proceed-checkout" type="submit">@lang('website_contents.authenticated.checkout.payment_summary.proceed_checkout')</button>
            </div>
        </form>
        <br>
        <button class="button-proceed-checkout" id="check" style="display:none">CHECK</button>
        <button class="button-proceed-checkout" id="clear" style="display:none">CLEAR SESSION</button>
    </div>
    @endif
    <!-- Payment Failed Popup -->
    <div class="modal fade" id="notification_payment_failed" tabindex="-1" role="dialog" aria-labelledby="notification_payment_failed" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="col-12 text-center">
                        <h5 id="notification_payment_failed_text"></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Notification trial plan exist -->
    {{-- <div class="modal fade" id="notification_trial_plan_exist" tabindex="-1" role="dialog" aria-labelledby="notification_trial_plan_exist" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-trial-plan-exist">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5" style="padding-top: 0 !important">
                    <div class="col-12 text-center">
                        <h5 class="modal-trial-plan-text">Oops. It seems like you’ve already signed up for a Starter Kit. Click <a href="#" class="modal-trial-plan-text-a">here<a> to check on your subscription status.</h5>
                        <br>
                        <h5 class="modal-trial-plan-text">If you’ve already cancelled your shave plan but would like to restart, click <a href="#" class="modal-trial-plan-text-a">here<a> to customize your new shave plan.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

</div>
<script src="{{ asset('js/helpers/ba_form_validations.js') }}"></script>
<script src="{{ asset('js/functions/alacarte/alacarte-checkout-card.function.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
<script>
    let event_data = {};
    let country_id = "<?php echo $details->currentCountryIso; ?>";
    let countryid = {!! json_encode($details->currentCountryid)!!};
    let langCode = {!!json_encode($details->langCode)!!};
    let urllangCode = {!! json_encode($details->urllangCode)!!};
    let user = {!!json_encode($userdetails->user)!!} !== null ? {!!json_encode($userdetails->user)!!} : null;
    let user_id = user !== null ? user.id : null;
    let delivery_address ={!!json_encode($userdetails->delivery_addresses)!!} !== null ? {!!json_encode($userdetails->delivery_addresses)!!} : null;
    let billing_address ={!!json_encode($userdetails->billing_addresses)!!} !== null ? {!!json_encode($userdetails->billing_addresses)!!} : null;
    let default_delivery_address = {!!json_encode($userdetails->defaults->delivery_address)!!} !== null ? {!!json_encode($userdetails->defaults->delivery_address)!!} : null;
    let default_billing_address = {!!json_encode($userdetails->defaults->billing_address)!!} !== null ? {!!json_encode($userdetails->defaults->billing_address)!!} : null;
    let default_card = {!!json_encode($userdetails->defaults->card)!!} !== null ? {!!json_encode($userdetails->defaults->card)!!} : null;
    let session_data = {!! json_encode($details->session_data)!!};
    let payment_intent = {!! json_encode($generate_payment_intent) !!} !== "" ? {!! json_encode($generate_payment_intent) !!} : ({!!json_encode($details->payment_intent)!!} !== null ? {!!json_encode($details->payment_intent)!!} : null);
    let checkout_details = {!!json_encode($details->checkout_details)!!} !== null ? {!!json_encode($details->checkout_details)!!} : null;
    let current_price = {!!json_encode($details->current_price)!!};
    let next_price = 0.0;
    let shipping_price = {!!json_encode($details->shipping_price)!!};
    let shipping_fee = {!!json_encode($details->shipping_fee)!!};
    let taxRate  = <?php echo $details->taxRate; ?>;
    let ctaxAmount  = <?php echo $details->taxAmount; ?>;
    let cards = {!!json_encode($userdetails->cards)!!} !== null ? {!!json_encode($userdetails->cards)!!} : null;
    let loginStatus = false;
    let formMode = "register";
    let session_checkout_data = null;
    let payment_failed = {!! json_encode($payment_fail) !!} !== null ? {!! json_encode($payment_fail) !!} : null;
    let saved_session_checkout = {!! json_encode($checkout_session)!!};
    let userCountryId = {!!json_encode($userdetails->user ? $userdetails->user->CountryId : null)!!};
    let userPhoneExt = {!!json_encode($phoneExt)!!};
    let signintext  = "@lang('website_contents.global.content.signin')";
    let confirmaccounttext  = "@lang('website_contents.authenticated.checkout.account.confirm_account')";
    let confirmsatext  =  "@lang('website_contents.global.content.confirmsa')";
    let confirmdetailtext  =  "@lang('website_contents.global.content.confirmdetail')";
    let confirmcardtext  =  "@lang('website_contents.global.content.confirmcard')";
    let confirmpaymenttext  =  "@lang('website_contents.global.content.confirmpayment')";
    let confirmeventtext  =  "@lang('website_contents.global.content.confirmevent')";
    let sessionLoginError  = <?php echo "'".$sessionLoginError."'"; ?>;
    // console.log("PAYMENT INTENT >>>>>>> ", payment_intent);
    async function DisplayPaymentError() {
        await session(SESSION_CHECKOUT_ALACARTE_BA, SESSION_GET, null).then(data => {
            if (data) {
                let dataParsed = JSON.parse(data);
                if (dataParsed.payment_error) {
                    $("#notification_payment_failed_text").html(dataParsed.payment_error);
                    $("#notification_payment_failed").modal("show");
                }
            }
        });
    }

    function logoutfromCheckout(){
        $("#loading").css("display", "block");
        session(SESSION_USER_INFO, SESSION_CLEAR, null).done(
            function() {
                //set items to storage
                window.localStorage.removeItem('current_user');
                window.location.reload();
            }
        );
    }

    function onChangeLoginOrRegisterForm() {
        if (formMode === "register") {
            formMode = "login";
            $(".title-create-account").html(signintext);
            $(".btn-create-account").html(signintext);
            $('.text-sign-in').addClass("hidden");
            $('.text-create-one').removeClass("hidden");
            $(".register-group").addClass("hidden");
            $(".login-group").removeClass("hidden");
            $(".name").removeAttr('required');
            $(".d-password-tk").removeAttr('required');
            $(".day-of-birth").removeAttr('required');
            $(".month-of-birth").removeAttr('required');
            $(".year-of-birth").removeAttr('required');
            $(".error_password").html("");
        } else {
            formMode = "register"
            $(".title-create-account").html(confirmaccounttext);
            $(".btn-create-account").html(confirmaccounttext);
            $('.text-sign-in').removeClass("hidden");
            $('.text-create-one').addClass("hidden");
            $(".register-group").removeClass("hidden");
            $(".login-group").addClass("hidden");
            $(".name").attr("required", true);
            $(".day-of-birth").attr("required", true);
            $(".month-of-birth").attr("required", true);
            $(".year-of-birth").attr("required", true);
            $(".error_password").html("");
        }
    }

    function onLoginOrRegister() {
        let email = $("#d-email-tk").val();
        let password = $("#d-password-tk").val();
        let name = $("#d-name-tk").val();
        let dob = $("#d-day-of-birth-tk").val();
        let mob = $("#d-month-of-birth-tk").val();
        let yob = $("#d-year-of-birth-tk").val();
        let birthday = yob + "-" + mob + "-" + dob;
        birthday = birthday.replace(/\s/g, '');
        let marketing_sub_agree = $('#marketing_sub_agree').is(':checked') ? $('#marketing_sub_agree').val() : null;
        let email_sub_agree = $('#email_sub_agree').is(':checked') ? $('#email_sub_agree').val() : null;
        let sms_sub_agree = $('#sms_sub_agree').is(':checked') ? $('#sms_sub_agree').val() : null;
        if ($('#form_register').valid() === true) {
            if (formMode === "register") {
                if (email && email.includes("@") && name && dob && mob && yob && birthday) {
                    RegisterCustomer(email, password, name, birthday, formMode, marketing_sub_agree, email_sub_agree, sms_sub_agree);
                }
            } else {
                if (email && password && email.includes("@")) {
                    // console.log(email)
                    // console.log(password)
                    LoginCustomer(email, password, formMode);
                }
            }
        }
    }

    function RegisterCustomer(email, password, name, birthday, type, marketing_sub_agree, email_sub_agree, sms_sub_agree) {

        let url = API_URL + '/ba/user/check/email';
        let method = "POST";
        let data = {
            password: password,
            email: email,
            type: type,
            appType: 'baWebsite'
        };

        $("#loading").css("display", "block");
        AJAX(url, method, data).done(response => {
            // console.log(response,type);
            if (type === "register") {
                let gender = JSON.parse(window.localStorage.getItem('gender_selected'));
                let current_country = JSON.parse(window.localStorage.getItem('current_country'));
                let current_locale = JSON.parse(window.localStorage.getItem('current_lang'));
                let seller_info = JSON.parse(window.localStorage.getItem('seller'));

                if (response.payload.user_data === null) {
                    let url = API_URL + '/ba/user/register';
                    let method = "POST";
                    let data = {
                        email: email,
                        name: name,
                        birthday: birthday,
                        gender: gender,
                        current_country: current_country,
                        current_locale : current_locale,
                        seller_info : seller_info,
                        marketing_sub_agree: marketing_sub_agree,
                        email_sub_agree: email_sub_agree,
                        sms_sub_agree: sms_sub_agree,
                    };

                    AJAX(url, method, data).done(function(response2) {
                        $("#loading").css("display", "none");
                        // console.log(response2);
                        let user_data = response2.payload;
                        if(user_data !== null){
                            session(SESSION_USER_INFO, SESSION_SET, JSON.stringify(user_data)).done(
                                function() {
                                    //set items to storage
                                    window.localStorage.setItem('current_user', JSON.stringify(user_data));
                                    window.location.reload();
                                }
                            );
                        } else {
                            $("#loading").css("display", "none");
                            let exists = user_data !== null && user_data.isActive !== null ? true : false;
                            let isActive = user_data !== null ? true : false;
                            if (exists && isActive) {
                                location.reload();
                            } else if (exists && !isActive) {
                                // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                                // $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                            } else if (exists && isActive) {
                                // $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                            }
                        }
                        // location.reload();
                    })
                } else {
                    $("#loading").css("display", "none");
                    let exists = response.payload.user_data !== null && response.payload.isActive !== null ? true : false;
                    let isActive = response.payload.user_data !== null && response.payload.isActive === 1 ? true : false;
                    if (exists && isActive) {
                        location.reload();
                    } else if (exists && !isActive) {
                        // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                        // $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                    } else if (exists && isActive) {
                        // $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                    }
                    // $(".error_password").html(trans('validation.custom.validation.user.used_email'));
                }
            }
        })
    }


    function LoginCustomer(email, password, type) {

        let url = API_URL + '/ba/user/check/email';
        let method = "POST";
        let data = {
            password: password,
            email: email,
            type: type,
            appType: 'baWebsite'
        };

        $("#loading").css("display", "block");
        AJAX(url, method, data).done(response => {
            $("#loading").css("display", "none");
            // console.log(response);
            let user_data = response.payload.user_data;
            if (type === "login") {
                if (response.success === false) {
                            session(SESSION_LOGIN_FAIL, SESSION_SET, response.error).done(
                                    function() {
                                        // $("#loading").css("display", "none");
                                        window.location.reload();
                                    //    $("#password_error").html(data.error);
                                    }
                                );
   
                        }else{
                if(user_data !== null){
                    let exists = response.payload.user_data !== null && response.payload.isActive !== null ? true : false;
                    let isActive = response.payload.user_data !== null && response.payload.isActive === 1 ? true : false;
                    if (exists && isActive) {
                        session(SESSION_USER_INFO, SESSION_SET, JSON.stringify(user_data)).done(
                            function() {
                                //set items to storage
                                window.localStorage.setItem('current_user', JSON.stringify(user_data));
                                window.location.reload();
                            }
                        );
                    } else if (exists && !isActive) {
                        // // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                        $("#email_error").html(trans('validation.custom.validation.email.email_not_active', {}))
                    } else if (!exists && !isActive) {
                        $("#email_error").html(trans('validation.custom.validation.email.email_not_exists', {}))
                    }
                } else {
                    // return user does not exists, please register
                    $("#loading").css("display", "none");
                    let exists = response.payload.user_data !== null && response.payload.isActive !== null ? true : false;
                    let isActive = response.payload.user_data !== null && response.payload.isActive === 1 ? true : false;
                    if (exists && isActive) {
                        location.reload();
                    } else if (exists && !isActive) {
                        // // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                        $("#email_error").html(trans('validation.custom.validation.email.email_not_active', {}))
                    } else if (!exists && !isActive) {
                        $("#email_error").html(trans('validation.custom.validation.email.email_not_exists', {}))
                    }
                    // $(".error_password").html(trans('validation.custom.validation.user.not_exists'));
                }
            }
            }
        })
    }

    //CREDIT CARD INPUT MASK
    function CreditCardFormatMasked(value) {
        var v_masked = value.replace(/\s/g, '').replace(/.(?!$)/gi, '•');
        var matches_masked = v_masked.match(/.{4,16}/g);
        var match_masked = matches_masked && matches_masked[0] || '';
        var parts_masked = [];

        for (i=0, len=match_masked.length; i<len; i+=4) {
            parts_masked.push(match_masked.substring(i, i+4));
        }

        if (parts_masked.length) {
            return parts_masked.join(' ');
        } else {
            return v_masked;
        }
    }

    function CreditCardMaskAll(value) {
        var v_masked = value.replace(/\s/g, '').replace(/./gi, '•');
        var matches_masked = v_masked.match(/.{4,16}/g);
        var match_masked = matches_masked && matches_masked[0] || '';
        var parts_masked = [];

        for (i=0, len=match_masked.length; i<len; i+=4) {
            parts_masked.push(match_masked.substring(i, i+4));
        }

        if (parts_masked.length) {
            return parts_masked.join(' ');
        } else {
            return v_masked;
        }
    }

    function CreditCardFormat(value, key) {
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

        if (key === "Backspace") {
            v = v.slice(0, -1); //simulate backspace by remove last char
        }
        else if ($.isNumeric(key)) {
            v = v + key; //append key if is numeric
        }

        var matches = v.match(/.{4,16}/g);
        var match = matches && matches[0] || '';
        var parts = [];

        for (i=0, len=match.length; i<len; i+=4) {
            parts.push(match.substring(i, i+4));
        }

        if (parts.length) {
            return parts.join('');
        } else {
            return v;
        }
    }

    function ExpiryDateFormat(value) {
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
        var matches = v.match(/\d{2,4}/g);
        var match = matches && matches[0] || ''
        var parts = []

        for (i = 0, len = match.length; i < len; i += 2) {
            parts.push(match.substring(i, i + 2))
        }

        if (parts.length) {
            return parts.join(' / ')
        } else {
            return value
        }
    }

    function UpdateCardSelectionList(newCard) {
        let cardOptionList = [];
        cards.push(newCard);
        cards.forEach(card => {
            var selected = '';
            if (card.id === newCard.id) {
                selected = "selected";
                $("#view-expiry-date").val(card.expiredMonth + " / " + card.expiredYear);
                $("#view-cvv").val(". . .");
            }
            cardOptionList.push("<option value='" + JSON.stringify(card) + "' " + selected + ">xxxx xxxx xxxx " + card.cardNumber + "</option>");
        });

        $("#card-dropdown").html(cardOptionList.join());
    }

    function ChangeCardBackground(branchName) {
        // console.log("branchName", branchName);
        if (branchName === "Visa") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/visa_card.png')");
        } else if (branchName === "MasterCard") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/mastercard_card.png')");
        } else if (branchName === "American Express") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/amex_card.png')");
        } else {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/visa_card.png')");
        }
    }

    $(function(){
        if(sessionLoginError){
        $("#password_error").html(sessionLoginError);
        onChangeLoginOrRegisterForm();
        }
        // console.log(payment_failed);
        if(payment_failed !== null){
            if(payment_failed.payment_fail){
                $("#notification_payment_failed_text").html(payment_failed.payment_fail_message);
                $("#notification_payment_failed").modal('show');

                $("#notification_payment_failed").on('hidden.bs.modal', function() {
                    window.history.replaceState(null, null, GLOBAL_URL + "/alacarte/checkout-card");
                    // window.location.reload();
                });
            }
        }
        validateRegistration("form_register");
    })

</script>
@endsection
