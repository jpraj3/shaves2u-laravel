@extends('layouts.app')

@section('content')

<style>
    .button-back {
        color: tomato;
        background-color: transparent;
        border: 0px;
        font-size: 15px;
        margin-top: 14px;
        margin-bottom: 14px;
    }
</style>

<div class="container text-center">
    <div class="row p-0 m-0">
        <div class="col-2 d-flex align-items-center">
            <button id="button-back" class="button-back">
                    @lang('website_contents.global.content.back')
            </button>
        </div>
        <div class="col-8 text-center">
            <h1 style="margin: 10% 15%;font-size: 25px;font-weight: 700;"><b>@lang('website_contents.authenticated.alacarte.select_method.select_method')</b></h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-12 d-inline-flex text-center vh-50 mx-auto" id="productCategories">
        <div class="col-6 mx-auto text-center pr-2">
        <button style="background: transparent !important;border: 1px solid lightgrey; min-height:50vw;" class="col-12 mx-auto btn btn-primary" id="pm_card" onClick="onMethodSelect('pm_card')" style="margin:auto;">
            <div class="col-12 mx-auto text-center mb-5">
                <h1 style="color: black;font-size: 18px; margin: 5%;font-weight: 400;"><b>@lang('website_contents.authenticated.alacarte.select_method.card')</b><h1>
            </div>
            <div class="col-12 mx-auto text-center">
                <img src="{{ asset('images/common/ba-creditcard.png') }}" class="img img-fluid" />
            </div>
        </button>
        </div>
        <div class="col-6 mx-auto text-center pl-2">
        <button style="background: transparent !important;border: 1px solid lightgrey; min-height:50vw;" class="col-12 mx-auto btn btn-primary" id="pm_cash" onClick="onMethodSelect('pm_cash')" style="margin:auto;">
            <div class="col-12 mx-auto text-center mb-5">
                <h1 style="color: black;font-size: 18px; margin: 5%;font-weight: 400;"><b>@lang('website_contents.authenticated.alacarte.select_method.cash')</b><h1>
            </div>
            <div class="col-12 mx-auto text-center">
                    <img src="{{ asset('images/common/ba-cash.png') }}" class="img img-fluid" />
            </div>
        </button>
        </div>
    </div>
    <form id="pm_form" method="POST">
            @csrf
        <input type="hidden" name="pm_select" id="pm_select" value="" />
        <input type="hidden" name="product_data" id="product_data" value="" />
    </form>
</div>

<script>
    let data = {!! json_encode($data)!!};
    let pm = null;

    function onMethodSelect(element_name) {
        switch (element_name) {
            case "pm_cash":
                document.getElementById("pm_select").value = "cash";
                break;
            case "pm_card":
                document.getElementById("pm_select").value = "card";
                break;
            default:
                document.getElementById("pm_select").value = null;
        }

        pm = document.getElementById("pm_select").value ;

        $.ajax({
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $(
                    'meta[name="csrf-token"]'
                ).attr("content")
            },
            url: GLOBAL_URL + "/alacarte/_checkout",
            data: { data: data.payload, method: pm },
            success: function(response) {
                // console.log(response);
                if (response.url) {
                    $("#pm_form").attr(
                        "action",
                        response.url
                    );
                    $("#product_data").val(
                        JSON.stringify(data)
                    );
                    $("#pm_form").submit();
                }
            },
            error: function(data) {},
            failure: function(data) {}
        });
    }
    
    $(function() {
        $("#button-back").click(function() {
            let selection_url =
            window.location.origin +
            GLOBAL_URL +
            "/alacarte/select#handle";
            window.location = selection_url;
        });
    });
</script>
@endsection
