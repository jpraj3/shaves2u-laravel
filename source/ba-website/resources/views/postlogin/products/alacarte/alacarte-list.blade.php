@extends('layouts.app')
<link rel="stylesheet" href="{{ asset('css/alacarte/alacarte-list.css') }}">

<link rel="stylesheet" href="{{ asset('css/alacarte/alacarte-selection.css') }}">
<style>
    @media screen and (max-width: 991px) {
        .item-selected {
            /* background: linear-gradient(-135deg, #ff5001 40px, #ececec 0); */
            color: black;
        }
    }

    .item-selected {
        border: 2px #ff5001 solid;
        border-radius: 10px;
        color: #ff5001;
        position: relative;
    }

    .main-heading-font{
        font-size:22px;
        font-weight: 700;
        text-decoration: none !important;
    color: white !important;
    }
    .sub-heading-font{
        font-size:16px;
    }
    .sub-content-1{
        margin:auto;
    }
    .margin-auto{
        margin:auto;
    }
    .sub-content-2{
        margin:auto;
        padding: 5% 0 5% 0;
        left: -20px;
    }
    .sub-content-2-header-font{
        font-size: 16px;
        font-weight: 700;
    }
    .sub-content-2-description-font{
        line-height: 1.3em;
        font-size: 12px;
    }
    .sub-content-2-price-font{
        padding: 5% 0 5% 0;
        font-size: 14px;
    }
    .sub-content-quantity-group{
        margin: 0 20%;
    }
    .sub-content-quantity-field{
        margin: auto;
        max-width: 50px;
    }
    .sub-content-minus-quantity-btn{
        background: #fe5000 !important;
        box-shadow: none !important;
        margin: auto !important;
        border-radius: 20% !important;
        color: white !important;
        clear: both !important;
        padding: 8% 40% !important;
    }
    .sub-content-plus-quantity-btn{
        background: #fe5000 !important;
        box-shadow: none !important;
        margin: auto !important;
        border-radius: 20% !important;
        color: white !important;
        clear: both !important;
        padding: 8% 40% !important;
    }
    .arrow-group{
        float: right;
        margin: 0px;
        padding: 2% 0.5%;
    }
    .button-proceed-checkout-mobile {
    background-color: #ff5001;
    color: white;
    border: none;
    padding-top: 15px;
    padding-bottom: 15px;
    width: 100%;
    }
    .main-header-style-on{
        border-top: 1px solid white !important;
        border-bottom: 1px solid white!important;
        text-align: center;
    }
    .main-header-style-off{
        border-bottom: 1px solid lightgrey!important;
        text-align: center;
    }
    .btn-link-on{
        color: white !important;
    }
    .btn-link-off{
        color: black !important;
    }
    .product-image-container {
        margin: auto;
    }
    .product-image-container>.product-image {
        width: 80%;
    }
</style>
@section('content')
<div class="container-fluid">
    <div id="accordion">
        <div class="card" {{ count($details->handle_products) > 0 ? '' : 'hidden' }}>
            <div class="top-selected main-header-style-on border-bottom" id="heading1">
                <h4 class="mb-0 main-heading-font">
                    <button class="btn btn-link btn-link-on" style="color: black !important;text-decoration: none !important;" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        @lang('website_contents.authenticated.alacarte.selection.handles')
                    </button>
                    {{-- <span id="heading1-arrow" class="arrow-group"><i class="fa fa-chevron-up"></i></span> --}}
                </h4>
            </div>

            <div id="collapse1" class="row panel-collapse collapse show" data-parent="#accordion">
                <div class="card-body">
                    {{-- <h3 class="text-center sub-heading-font"><b>Select a handle</b></h3> --}}
                    <div class="col-md-12">
                        @foreach ($details->handle_products as $handle)
                            <div id="handle-{{ $handle->ProductCountryId }}" class="text-left m-2 pl-2 pt-2 item-unselected">
                                <div class="row">
                                    <div class="col-4 sub-content-1 product-image-container">
                                        <img src="{{ $handle->product_default_image }}" class="img-fluid product-image">
                                    </div>
                                    <div class="col-8 text-center sub-content-2">
                                        <h3 class="sub-content-2-header-font">{{ $handle->name }}</h3>
                                        {{-- <span class="sub-content-2-description-font">{{ $handle->shortDescription }}</span> --}}
                                        <h4 class="sub-content-2-price-font">{{ $details->currency }} <span id="handle_price_{{ $handle->ProductCountryId }}">{{ $handle->sellPrice }}</span></h4>
                                        <div class="d-inline-flex sub-content-quantity-group">
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-minus-quantity-btn" id="handle_remove_{{ $handle->ProductCountryId }}">-</button>
                                            </div>
                                            <div class="col-4 p-0 sub-content-quantity-field">
                                                <input class="input-box text-center" id="handle_qty_{{ $handle->ProductCountryId }}" type="text" value="0" pattern="\d*" maxlength="1"/>
                                            </div>
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-plus-quantity-btn" id="handle_add_{{ $handle->ProductCountryId }}">+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="card" {{ count($details->blade_products) > 0 ? '' : 'hidden' }}>
                <div class="main-header-style-off" id="heading2">
                        <h4 class="mb-0 main-heading-font">
                    <button class="btn btn-link btn-link-off" style="text-decoration: none !important;" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        @lang('website_contents.authenticated.alacarte.selection.blades')
                    </button>
                    {{-- // 2-arrow" class="arrow-group"><i class="fa fa-chevron-down"></i></span> --}}
                </h4>
            </div>
            <div id="collapse2" class="row panel-collapse collapse {{ count($details->handle_products) > 0 ? '' : 'show' }}" data-parent="#accordion">
                <div class="card-body">
                    {{-- <h3 class="text-center sub-heading-font"><b>Select blade cartridge</b></h3> --}}
                    <div class="col-md-12">
                        @foreach ($details->blade_products as $blade)
                            <div id="blade-{{ $blade->ProductCountryId }}" class="text-left m-2 pl-2 pt-2 item-unselected">
                                <div class="row">
                                    <div class="col-4 sub-content-1 product-image-container">
                                        <img src="{{ $blade->product_default_image }}" class="img-fluid product-image">
                                    </div>
                                    <div class="col-8 text-center sub-content-2">
                                        <h3 class="sub-content-2-header-font">{{ $blade->name }}</h3>
                                        {{-- <span class="sub-content-2-description-font">{{ $blade->shortDescription }}</span> --}}
                                        <h4 class="sub-content-2-price-font">{{ $details->currency }} <span id="blade_price_{{ $blade->ProductCountryId }}">{{ $blade->sellPrice }}</span></h4>
                                        <div class="d-inline-flex sub-content-quantity-group">
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-minus-quantity-btn" id="blade_remove_{{ $blade->ProductCountryId }}" >-</button>
                                            </div>
                                            <div class="col-4 p-0 sub-content-quantity-field">
                                                <input class="input-box text-center" id="blade_qty_{{ $blade->ProductCountryId }}" type="text" value="0" pattern="\d*" maxlength="1"/>
                                            </div>
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-plus-quantity-btn" id="blade_add_{{ $blade->ProductCountryId }}" >+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="card" {{ count($details->addon_products) > 0 ? '' : 'hidden' }}>
                <div class="main-header-style-off" id="heading3">
                        <h4 class="mb-0 main-heading-font">
                    <button class="btn btn-link btn-link-off"  style="text-decoration: none !important;"  data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                        @lang('website_contents.authenticated.alacarte.selection.creams')
                    </button>
                    {{-- <span id="heading3-arrow" class="arrow-group"><i class="fa fa-chevron-down"></i></span> --}}
                </h5>
            </div>

            <div id="collapse3" class="row panel-collapse collapse {{ count($details->handle_products) > 0 || count($details->blade_products) > 0 ? '' : 'show' }}" data-parent="#accordion">
                <div class="card-body">
                    {{-- <h3 class="text-center sub-heading-font"><b>Select a cream</b></h3> --}}
                    <div class="col-md-12">
                        @foreach ($details->addon_products as $addon)
                            <div id="addon-{{ $addon->ProductCountryId }}" class="text-left m-2 pl-2 pt-2 item-unselected">
                                <div class="row">
                                    <div class="col-4 sub-content-1 product-image-container">
                                        <img src=" {{ $addon->product_default_image }}" class="img-fluid product-image">
                                    </div>
                                    <div class="col-8 text-center sub-content-2">
                                        <h3 class="sub-content-2-header-font">{{ $addon->name }}</h3>
                                        {{-- <span class="sub-content-2-description-font">{{ $addon->shortDescription }}</span> --}}
                                        <h4 class="sub-content-2-price-font">{{ $details->currency }} <span id="addon_price_{{ $addon->ProductCountryId }}">{{ $addon->sellPrice }}</span></h4>
                                        <div class="d-inline-flex sub-content-quantity-group">
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-minus-quantity-btn" id="addon_remove_{{ $addon->ProductCountryId }}" >-</button>
                                            </div>
                                            <div class="col-4 p-0 sub-content-quantity-field">
                                                <input class="input-box text-center" id="addon_qty_{{ $addon->ProductCountryId }}" type="text" value="0" pattern="\d*" maxlength="1"/>
                                            </div>
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-plus-quantity-btn" id="addon_add_{{ $addon->ProductCountryId }}" >+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="card" {{ count($details->ask_products) > 0 ? '' : 'hidden' }}>
                <div class="main-header-style-off" id="heading4">
                        <h4 class="mb-0 main-heading-font">
                    <button class="btn btn-link btn-link-off"  style="text-decoration: none !important;" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                        @lang('website_contents.authenticated.alacarte.selection.ask')
                    </button>
                    {{-- <span id="heading4-arrow" class="arrow-group"><i class="fa fa-chevron-down"></i></span> --}}
                </h5>
            </div>

            <div id="collapse4" class="row panel-collapse collapse {{ count($details->handle_products) > 0 || count($details->blade_products) > 0 || count($details->addon_products) > 0 ? '' : 'show' }}" data-parent="#accordion">
                <div class="card-body">
                    {{-- <h3 class="text-center sub-heading-font"><b>Gift awesomeness</b></h3> --}}
                    <div class="col-md-12">
                        @foreach ($details->ask_products as $ask)
                            <div id="ask-{{ $ask->ProductCountryId }}" class="text-left m-2 pl-2 pt-2 item-unselected">
                                <div class="row">
                                    <div class="col-4 sub-content-1 product-image-container">
                                    <?php
    
                                $img =  $ask->product_default_image ;
                                if ($ask->sku) {
                                    if (strpos($ask->sku, 'ASK3') !== false) {
                                   
                                        $img = asset('/images/common/3blade.png');
                                    } else if (strpos($ask->sku, 'ASK5') !== false) {
  
                                        $img = asset('/images/common/5blade.png');
                                    } else if (strpos($ask->sku, 'ASK6') !== false) {
     
                                        $img = asset('/images/common/6blade.png');
                                    }
                                }
                                ?>
                                        <img src="{{  $img }}" class="img-fluid product-image">
                                    </div>
                                    <div class="col-8 text-center sub-content-2">
                                        <h3 class="sub-content-2-header-font">{{ $ask->name }}</h3>
                                        {{-- <span class="sub-content-2-description-font">{{ $ask->shortDescription }}</span> --}}
                                        <h4 class="sub-content-2-price-font">{{ $details->currency }} <span id="ask_price_{{ $ask->ProductCountryId }}">{{ $ask->sellPrice }}</span></h4>
                                        <div class="d-inline-flex sub-content-quantity-group">
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-minus-quantity-btn" id="ask_remove_{{ $ask->ProductCountryId }}" >-</button>
                                            </div>
                                            <div class="col-4 p-0 sub-content-quantity-field">
                                                <input class="input-box text-center" id="ask_qty_{{ $ask->ProductCountryId }}" type="text" value="0" pattern="\d*" maxlength="1"/>
                                            </div>
                                            <div class="col-3 p-0">
                                                <button type="button" class="btn sub-content-plus-quantity-btn" id="ask_add_{{ $ask->ProductCountryId }}" >+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <form id="goToSelectedView" method="POST" action="">
        @csrf
        <input type="hidden" name="product_data" id="product_data" value="" />
    </form>
</div>
<br><br><br><br><br>
<div class="col-12 text-center" style="padding:0px;bottom:0;position:fixed;">
    <button class="button-proceed-checkout-mobile button-next text-uppercase" type="submit">@lang('website_contents.global.content.next')<span class="price-display"></span></button>
    <button class="button-proceed-checkout-mobile button-proceed-checkout MuliExtraBold text-uppercase" type="button" style="width: 100%">@lang('website_contents.global.content.next')<span class="price-display"></button>
</div>

<script>
    let data = {!! json_encode($details) !!};
    let handle_products = {!! json_encode($details->handle_products) !!};
    let blade_products = {!! json_encode($details->blade_products) !!};
    let addon_products = {!! json_encode($details->addon_products) !!};
    let ask_products = {!! json_encode($details->ask_products) !!};
    let country_id = {!! json_encode($details->country_id) !!};
    let currency = {!! json_encode($details->currency)!!};
    let current_step = 1;
    // console.log(data);
    let saved_selection = JSON.parse({!! json_encode(Session::get('selection_alacarte_ba')) !!});
    let user_id = null;
    let country = {!! json_encode(Session::get('currentCountry')) !!};
    let langCode = {!! json_encode(strtolower(app()->getLocale())) !!};
</script>
<script type="text/javascript">
    $(document).ready(function(){

        // $('#heading1').click(function(){
        //     $('#heading4-arrow').
        // });

        // $('#heading2').click(function(){
        //     if(document.getElementById('heading2-arrow').classList.contains('fa fa-chevron-down')){
        //         $('#heading2-arrow').html('<i class="fa fa-chevron-up"></i>');
        //     } else {
        //         $('#heading2-arrow').html('<i class="fa fa-chevron-down"></i>');
        //     }
        // });

        // $('#heading3').click(function(){
        //     if(document.getElementById('heading3-arrow').classList.contains('fa fa-chevron-down')){
        //         $('#heading3-arrow').html('<i class="fa fa-chevron-up"></i>');
        //     } else {
        //         $('#heading3-arrow').html('<i class="fa fa-chevron-down"></i>');
        //     }
        // });

        // $('#heading4').click(function(){
        //     if(document.getElementById('heading4-arrow').classList.contains('fa fa-chevron-down')){
        //         $('#heading4-arrow').html('<h5 class="mb-0 main-heading-font"><button class="btn btn-link" style="text-decoration: none !important;" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse1">Awesome Shave Kits</button><i class="fa fa-chevron-up"></i></h5>');
        //     } else {
        //         $('#heading4-arrow').html('<h5 class="mb-0 main-heading-font"><button class="btn btn-link" style="text-decoration: none !important;" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse1">Awesome Shave Kits</button><i class="fa fa-chevron-down"></i></h5>');
        //     }
        // });
    });
</script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
<script src="{{ asset('js/functions/alacarte/alacarte.function.js') }}"></script>
@endsection
