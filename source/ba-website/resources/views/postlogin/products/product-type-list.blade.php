@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/product/product.css') }}">
<style>
    .item-unselected{
        height: 110px;
    }
</style>
<div class="container text-center">
       <h1 style="margin: 5% 10%;font-size: 25px;font-weight: 700;"><b>@lang('website_contents.authenticated.products.select_product')</b></h1>
</div>


<div class="container pt-3">
    <input type="hidden" name="gender_selected" id="gender_selected" />
    <div class="col-12 text-center vh-50 mx-auto" id="productCategories">
        <a href="{{ route('ba.locale.show.trial-plans.step-1', ['langCode' => strtolower(session()->get('currentCountry')->urlLang), 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]) }}" class="col-12 mb-4" id="TK_Lists"></a>
        <div hidden onClick="goToTrialPlanView();" class="col-12 mb-4" id="CP_Lists"></div>
        <div onClick="goToProductView();" class="col-12 text-center vh-50 mx-auto mt-4 p-0" id="Prdts_Lists"></div>
    </div>

    <form id="goToSelectedView" method="POST" action="">
        @csrf
        <input type="hidden" name="trial_data" id="trial_data" value="" />
        <input type="hidden" name="custom_data" id="custom_data" value="" />
        <input type="hidden" name="product_data" id="product_data" value="" />
    </form>
</div>

<script src="{{ asset('js/api/products/products.function.js') }}"></script>

<script>
    $(function () {
        if(window.localStorage.getItem('gender_selected')){
            let gender_selected = window.localStorage.getItem('gender_selected');
          $("#gender_selected").val(gender_selected);

          let old_trial_link = $("#TK_Lists").attr("href");
          $("#TK_Lists").attr("href", old_trial_link + '?gender='+gender_selected.replace(/['"]+/g, ''))
          // console.log($("#TK_Lists").attr("href"));
        }
    })
</script>
@endsection
