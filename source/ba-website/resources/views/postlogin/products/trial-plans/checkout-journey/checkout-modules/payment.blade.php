<style>
    .individual-card-selection:hover {
        background: green;
        color: white;
    }
</style>
@if($default_card !== null && count($default_card) > 0)
<div id="show_card_details_container" class="panel-body">
    <div class="rounded-10 bg-white padd20" id="appendCard">
        <div class="form-group">
            <label for="">Card Name</label>
            <p>{{$default_card[0]->cardName}}</p>
        </div>
        <div class="form-group">
            <label for="">Card Number</label>
            <p>XXXX XXXX XXXX {{$default_card[0]->cardNumber}}</p>
        </div>
        <div class="form-group">
            <label for="">Card Expiry</label>
            <p>{{$default_card[0]->expiredMonth}}/{{$default_card[0]->expiredYear}}</p>
        </div>
        <div class="form-group" id="add-card" style="text-align:center;">
            <button type="submit" class="btn btn-primary" onClick="editCard()">Edit Card</button>
        </div>
    </div>
</div>
<div id="card_selection_list" class="panel-body" style="display:none;">
    @include('checkout.subscriptions.checkout-modules.card.card-selection')
</div>
<div id="add_additional_card" class="panel-body" style="display:none;">
    @include('checkout.subscriptions.checkout-modules.card.card-add')
</div>
@else
<div id="show_card_details_container" class="panel-body" style="display:none;">
    <div class="rounded-10 bg-white padd20" id="appendCard">
    </div>
    <div class="form-group" id="add-card" style="text-align:center;">
        <button type="submit" class="btn btn-primary" onClick="editCard()">Change Card</button>
    </div>
</div>
<div id="card_selection_list" class="panel-body" style="display:none;">
    <div id="card_selection" class="panel-body">
        <div class="rounded-10 bg-white padd20" id="appendCardList">
        </div>
        <br> <br>
        <p class="error-card"  id="error-card"></p>
        <div class="form-group" id="add-card" style="text-align:center;">
            <button class="btn btn-primary" onClick="CardBack()">Back</button>
            <button type="submit" class="btn btn-primary" onClick="selectionAddCard()">Add Card</button>
        </div>
    </div>
</div>
<div id="add_additional_card" class="panel-body">
    @include('checkout.subscriptions.checkout-modules.card.card-add')
</div>
@endif