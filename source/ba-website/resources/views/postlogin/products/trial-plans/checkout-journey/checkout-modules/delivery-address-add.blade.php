<div id="shipping-address-show" style="display: none;">
    <input type="hidden" id="delivery_address_id" name="delivery_address_id" value="" />
    <input type="hidden" id="billing_address_id" name="billing_address_id" value="" />
</div>

<div id="shipping-address-add" class="pl-0 pt-0 MuliPlain">

    <form id="add-addresses-block" method="POST" action="">
    <!-- <h4 class="color-orange MuliBold  mb-3">Address</h4> -->
        @csrf
        <input type="hidden" name="user_id" value="{{ $userdetails->user !== "" ? $userdetails->user->id : '' }}">
        <div class="box-white" id="shipping-address-block">
            <div class="col-12 padd form-group pt-2 pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="Full Name" class="">@lang('website_contents.authenticated.checkout.address.recipient_name')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_name" name="delivery_name" class="">
                <div id="pd_name_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @if(strtolower($details->currentCountryIso) === 'tw')
            <div class="col-12 padd form-group pt-2 pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="SSN" class="">@lang('website_contents.authenticated.checkout.address.ssn')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_ssn" name="SSN" class="">
                <div id="pd_ssn_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @endif
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="phone" class="">@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <div class="input-group">
                    <input required class="padd0 mr-3 form-control MuliBold d_phoneext" id="add_delivery_phoneext" name="delivery_phoneext" value="+{{ $phoneExt }}" style="max-width: 60px; background-color: white;" readonly>
                    <input required class="col-12 padd0 form-control MuliBold d_phone" id="add_delivery_phone" name="delivery_phone" class="" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" >
                </div>
                <div id="pd_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="address" class="">@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_address" name="delivery_address" class="">
                <div id="pd_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="state" class="">@lang('website_contents.authenticated.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="add_delivery_state" name="delivery_state">
                    @if($userdetails->states)
                 @foreach ($userdetails->states as $s)
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                  @endforeach
                @endif
                </select>
                <div id="pd_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="city" class="">@lang('website_contents.authenticated.checkout.address.town')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_city" name="delivery_city" class="">
                <div id="pd_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="postcode" class="">@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input required class="col-12 padd0 form-control MuliBold number-only" id="add_delivery_postcode" name="delivery_postcode" class="">
                <div id="pd_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_add col-12">
            <div class="row pl-4 pr-0">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox form-control" type="checkbox" id="enableBillingEditCheckBox_for_add" name="isDifferentBilling" onclick="enableBilling()">
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">@lang('website_contents.authenticated.checkout.address.different_address')</h5>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_add" style="display:none;">
            <div class="col-12 padd0 form-group pt-3">
                <b>@lang('website_contents.authenticated.checkout.address.billing_address')</b>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <div class="input-group">
                    <input required class="padd0 mr-3 form-control MuliBold" id="add_billing_phoneext" name="billing_phoneext" value="+{{ $phoneExt }}"  style="max-width: 60px; background-color: white;" readonly>
                    <input class="col-12 padd0 form-control MuliBold" id="add_billing_phone" name="billing_phone" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" >
                </div>
                <div id="pb_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_address" name="billing_address">
                <div id="pb_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">@lang('website_contents.authenticated.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="add_billing_state" name="billing_state">
                @if($states)
                 @foreach ($states as $s)
                   @if($billing_address)
                    @if($billing_address[0]["state"] === $s["name"])
                    <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                    @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                    @endif
                   @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                   @endif
                  @endforeach
                @endif
                </select>
                <div id="pb_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">@lang('website_contents.authenticated.checkout.address.town')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_city" name="billing_city">
                <div id="pb_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input class="col-12 padd0 form-control MuliBold number-only" id="add_billing_postcode" name="billing_postcode">
                <div id="pb_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <button hidden class="btn btn-load-more col-6 uppercase mt-3" style="font-size: 12px" type="submit">@lang('website_contents.authenticated.checkout.address.save_address')</button>
    </form>
</div>

