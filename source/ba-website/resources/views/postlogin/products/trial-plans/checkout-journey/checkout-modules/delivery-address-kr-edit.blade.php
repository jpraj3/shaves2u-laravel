<div id="shipping-address-show">
    <div class="panel-body">
        <div class="box-white" id="appendAddress">
            <br>
            <label style="font-weight: bold">@lang('website_contents.authenticated.checkout.address.delivery_address')</label><br>
            <p style="margin-bottom:0;">{{ $userdetails->delivery_addresses ?  $userdetails->delivery_addresses[0]["fullName"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $userdetails->delivery_addresses ?  $userdetails->delivery_addresses[0]["address"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["portalCode"] : ''}}, {{ $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["city"]: ''}}, {{ $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["state"] : ''}}</p>
            <p style="margin-bottom:0;">{{ $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["contactNumber"] : '' }}</p><br>
            <br>
            <label style="font-weight: bold">@lang('website_contents.authenticated.checkout.address.billing_address')</label><br>
            <p style="margin-bottom:0;">{{ $userdetails->billing_addresses ?  $userdetails->billing_addresses[0]["address"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["portalCode"] : ''}}, {{ $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["city"]: ''}}, {{ $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["state"] : ''}}</p>
            <p style="margin-bottom:0;">{{ $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["contactNumber"] : '' }}</p><br>
            <br>
        </div>
        <input type="hidden" id="delivery_address_id" name="delivery_address_id" value="{{ $userdetails->delivery_addresses ?  $userdetails->delivery_addresses[0]["id"] : '' }}" />
        <input type="hidden" id="billing_address_id" name="billing_address_id" value="{{ $userdetails->billing_addresses ?  $userdetails->billing_addresses[0]["id"] : '' }}" />

        <div><button id="enable-address-edit" class="btn btn-load-more col-10" onclick="editAddress()">@lang('website_contents.authenticated.checkout.address.edit_address')</button></div>
    </div>
</div>

<div id="shipping-address-edit" class="pl-0 pt-0 MuliPlain" style="display:none;">

    <form id="edit-addresses-block" method="POST" action="">
    <!-- <h4 class="color-orange MuliBold  mb-3">Address</h4> -->
        @csrf
        <input type="hidden" name="user_id" value="{{ $userdetails->user !== "" ? $userdetails->user->id : '' }}">
        <div class="box-white" id="shipping-address-block">
            <div class="col-12 padd0 form-group pt-2 pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="Full Name" >@lang('website_contents.authenticated.checkout.address.recipient_name')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_name" name="delivery_name"  value="{{  $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["fullName"] : '' }}">
                <div id="pd_name_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="phone" >@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <div class="input-group">
                    <input required class="padd0 mr-3 form-control MuliBold d_phoneext" id="edit_delivery_phoneext" name="delivery_phoneext" value="+{{ $phoneExt }}" style="max-width: 60px; background-color: white;" readonly>
                    <input required class="col-12 padd0 form-control MuliBold d_phone" id="edit_delivery_phone" name="delivery_phone" class="" value="{{  $userdetails->delivery_addresses ? str_replace(("+".$phoneExt), "", $userdetails->delivery_addresses[0]["contactNumber"]) : ''}}" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" >
                </div>
                <div id="pd_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanDeliveryAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-delivery" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="address" >@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <?php
                $delivery_address1=$delivery_address ? $delivery_address->address : '';
                $flat = '';
                $showaddress='' ;
                if( strpos($delivery_address1, ',') !== false){
                    $seperate = explode(",", $delivery_address1);
                    $showaddress= $seperate[0];
                    $flat= $seperate[1];
                }else{
                    $showaddress= $delivery_address1;
                }
                ?>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_address1" name="delivery_address1"  value="{{$showaddress}}" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="edit_delivery_address" name="delivery_address" value="{{ $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["address"] : ''}}" readonly="readonly">
                <div id="pd_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="Flat" >@lang('website_contents.authenticated.checkout.address.flat')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_flat" name="delivery_flat"  value="{{$flat}}" onchange="onChangeDeliveryAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="city" >@lang('website_contents.authenticated.checkout.address.town')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_city" name="delivery_city"  value="{{  $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["city"] : ''}}" readonly="readonly">
                <div id="pd_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="state" >@lang('website_contents.authenticated.checkout.address.state')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_state" name="delivery_state"  value="{{  $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["state"] : ''}}" readonly="readonly">
                <div id="pd_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="postcode" >@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_delivery_postcode" name="delivery_postcode"  value="{{  $userdetails->delivery_addresses ? $userdetails->delivery_addresses[0]["portalCode"]: '' }}" readonly="readonly">
                <div id="pd_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_edit col-12">
            <div class="row pl-4 pr-0">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox" type="checkbox" id="enableBillingEditCheckBox_for_edit" name="isDifferentBilling" onclick="enableBilling()" <?php echo ($userdetails->billing_addresses ? 'checked' : ''); ?>>
                </div>
                <div class="col-11 pl-0">
                    <h6 class="MuliPlain">@lang('website_contents.authenticated.checkout.address.different_address')</h6>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_edit" class="box-white" style="display:none;">
            <div class="col-12 padd0 form-group pr-0 pl-3 pt-2">
                <b>@lang('website_contents.authenticated.checkout.address.different_address')</b>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="phone">@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <div class="input-group col-12">
                    <input required class="padd0 mr-3 form-control MuliBold" id="edit_billing_phoneext" name="billing_phoneext" value="+{{ $phoneExt }}"  style="max-width: 60px; background-color: white;" readonly>
                    <input class="col-12 padd0 form-control MuliBold" id="edit_billing_phone" name="billing_phone" value="{{  $userdetails->billing_addresses ? str_replace(("+".$phoneExt), "", $userdetails->billing_addresses[0]["contactNumber"]) : ''}}" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" >
                </div>
                <div id="pb_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanBillingAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-billing" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="address">@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <?php
                $billing_address1=$billing_address ? $billing_address->address : '';
                $flatb = '';
                $showbaddress='' ;
                if( strpos($billing_address1, ',') !== false){
                    $seperate = explode(",", $billing_address1);
                    $showbaddress= $seperate[0];
                    $flatb= $seperate[1];
                }else{
                    $showbaddress= $billing_address1;
                }
                ?>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_billing_address1" name="billing_address1" value="{{ $showbaddress }}"  readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="edit_billing_address" name="billing_address" value="{{  $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["address"] : ''}}" readonly="readonly">
                <div id="pb_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="Flat">@lang('website_contents.authenticated.checkout.address.flat')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_billing_flat" name="billing_flat" value="{{ $flatb }}">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="city">@lang('website_contents.authenticated.checkout.address.town')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_billing_city" name="billing_city" value="{{  $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["city"] : ''}}" readonly="readonly">
                <div id="pb_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="state">@lang('website_contents.authenticated.checkout.address.state')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_billing_state" name="billing_state" value="{{  $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["state"] : ''}}" readonly="readonly">
                <div id="pb_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey" for="postcode">@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="edit_billing_postcode" name="billing_postcode" value="{{  $userdetails->billing_addresses ? $userdetails->billing_addresses[0]["portalCode"] : ''}}" readonly="readonly">
                <div id="pb_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>
        <button hidden class="btn btn-load-more col-6 uppercase mt-3" style="font-size: 12px" type="submit"><b>@lang('website_contents.authenticated.checkout.address.save_address')</b></button>
    </form>
</div>
