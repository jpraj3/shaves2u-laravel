<div id="shipping-address-show" hidden>
    <input type="hidden" id="delivery_address_id" name="delivery_address_id" value="" />
    <input type="hidden" id="billing_address_id" name="billing_address_id" value="" />
</div>

<div id="shipping-address-add" class="pl-0 pt-0 MuliPlain">

    <form id="add-addresses-block" method="POST" action="">
    <!-- <h4 class="color-orange MuliBold  mb-3">Address</h4> -->
        @csrf
        <input type="hidden" name="user_id" value="{{ $userdetails->user !== "" ? $userdetails->user->id : '' }}">
        <div class="box-white" id="shipping-address-block">
            <div class="col-12 padd form-group pt-2 pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="Full Name" >@lang('website_contents.authenticated.checkout.address.recipient_name')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_name" name="delivery_name" >
                <div id="pd_name_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="phone" >@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <div class="input-group">
                    <input required class="padd0 mr-3 form-control MuliBold d_phoneext" id="add_delivery_phoneext" name="delivery_phoneext" value="+{{ $phoneExt }}" style="max-width: 60px; background-color: white;" readonly>
                    <input required class="col-12 padd0 form-control MuliBold d_phone" id="add_delivery_phone" name="delivery_phone" class="" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" >
                </div>
                <div id="pd_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanDeliveryAddressPrefill('add')" class="text-center text-danger"
                style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-add-delivery"
                style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap"
                    style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()"
                    alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="address" >@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_address1" name="delivery_address1" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="add_delivery_address" name="delivery_address" readonly="readonly">
                <div id="pd_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="Flat" >@lang('website_contents.authenticated.checkout.address.flat')</label>
                <input class="col-12" id="add_delivery_flat" name="delivery_flat" onchange="onChangeDeliveryAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="city" >@lang('website_contents.authenticated.checkout.address.town')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_city" name="delivery_city" readonly="readonly">
                <div id="pd_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="state" >@lang('website_contents.authenticated.checkout.address.state')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_state" name="delivery_state" readonly="readonly">
                <div id="pd_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pl-0 pr-0" for="postcode" >@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_delivery_postcode" name="delivery_postcode" readonly="readonly">
                <div id="pd_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_add col-12">
            <div class="row pl-4 pr-0">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox form-control" type="checkbox" id="enableBillingEditCheckBox_for_add" name="isDifferentBilling" onclick="enableBilling()">
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">@lang('website_contents.authenticated.checkout.address.different_address')</h5>
                </div>
            </div>
        </div>
        <div class="box-white" id="billing-address-block_for_add" style="display:none;">
            <div class="col-12 padd0 form-group pt-3 pr-0 pl-0">
                <b>Billing Address</b>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="phone">@lang('website_contents.authenticated.checkout.address.contact_no')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_phone" name="billing_phone">
                <div id="pb_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>

            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
            <a href="javascript:;" onclick="koreanBillingAddressPrefill('add')" class="text-center text-danger"
                style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-add-billing"
                style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap"
                    style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()"
                    alt="접기 버튼">
            </div>
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="address">@lang('website_contents.authenticated.checkout.address.unit_details')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_address1" name="billing_address1" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="add_billing_address" name="billing_address" readonly="readonly">
                <div id="pb_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="Flat">@lang('website_contents.authenticated.checkout.address.flat')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_flat" name="billing_flat" onchange="onChangeBillingAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="city">@lang('website_contents.authenticated.checkout.address.town')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_city" name="billing_city" readonly="readonly">
                <div id="pb_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="state">@lang('website_contents.authenticated.checkout.address.state')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_state" name="billing_state" readonly="readonly">
                <div id="pb_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group pr-0 pl-0" id="">
                <label class="col-12 padd0 color-grey pr-0 pl-0" for="postcode">@lang('website_contents.authenticated.checkout.address.postcode')</label>
                <input required class="col-12 padd0 form-control MuliBold" id="add_billing_postcode" name="billing_postcode" readonly="readonly">
                <div id="pb_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <button hidden class="btn btn-load-more col-6 uppercase mt-3" style="font-size: 12px" type="submit">@lang('website_contents.authenticated.checkout.address.save_address')</button>
    </form>
</div>

