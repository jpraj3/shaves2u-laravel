<div id="add_card_details_container" class="panel-body">
    <div class="rounded-10 bg-white padd20">
        <div class="payment">
            <form id="add-card-details" method="POST" action="">
                <div class="form-group">
                    <label for="">Card Name</label>
                    <input type="text" name="card_name" class="form-control" id="card_name">
                </div>
                <div class="form-group" id="card-number-field">
                    <label for="cardNumber">Card Number</label>
                    <input type="text" name="card_number" class="form-control" id="card_number">
                </div>
                <div class="form-group CVV">
                    <label for="cvv">CVV</label>
                    <input type="text" name="card_cvv" class="form-control" id="card_cvv">
                </div>
                <div class="form-group pb-4" id="expiration-date">
                    <label>Expiration Date</label>
                    <div class="input-group" id="expiration-date">
                        <select class="col-6 form-control" name="card_expiry_month" id="card_expiry_month">
                            <option value="01">January</option>
                            <option value="02">February </option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select class="col-6 form-control" name="card_expiry_year" id="card_expiry_year">
                            <option value="19"> 2019</option>
                            <option value="20"> 2020</option>
                            <option value="21"> 2021</option>
                            <option value="22"> 2022</option>
                            <option value="23"> 2023</option>
                            <option value="24"> 2024</option>
                            <option value="25"> 2025</option>
                            <option value="26"> 2026</option>
                            <option value="27"> 2027</option>
                            <option value="28"> 2028</option>
                            <option value="29"> 2029</option>
                            <option value="30"> 2030</option>
                            <option value="31"> 2031</option>
                            <option value="32"> 2032</option>
                        </select>
                </div>
                <div class="form-group" id="credit_cards">
                    <img src="{{ asset('images/cards/visa.jpg') }}" id="visa">
                    <img src="{{ asset('images/cards/mastercard.jpg') }}" id="mastercard">
                    <img src="{{ asset('images/cards/amex.jpg') }}" id="amex">
                </div>
                <p class="error-card"  id="error-card1"></p>
                <div class="col-12 padd20 pl-0 pr-0">
                    <p>By making a purchase, you accept the <u><a href="{{ route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" style="color: black">Terms of Service</a></u> (update effective as of 11/17/17). Your subscription will automatically renew and your credit card will be charged the subscription fee. You can cancel or modify your plan at any time from your profile page.</p>
                </div>
                <div class="form-group" id="add-card" style="text-align:center;">
                    <button class="btn btn-load-more" onClick="CardBack()">Back</button>
                    <button type="submit" class="btn btn-load-more" id="confirm-purchase">Add Card</button>
                </div>
            </form>
        </div>
    </div>
</div>