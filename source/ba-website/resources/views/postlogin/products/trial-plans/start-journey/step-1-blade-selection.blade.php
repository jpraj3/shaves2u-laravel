@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">
@php ($trial_saving_price_list = $details->trial_saving_price_list)
@php($current_country = session()->get('currentCountry'))
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
{{-- <div class="container mb-5">
    <div class="row">
        <!-- <div class="col-md-12 text-center pr-0 pl-0 pb-2">
            <h2 class="mt-2 pl-1 pr-1 w-100">
                <b>Get started with the<br>Shaves2U Starter Kit</b>
            </h2>
        </div> -->
        <!-- <div class="col-md-12 text-center">
            <h4 class="trial-sub-title">A better shave, delivered.<br> Start with a shaver and a<br>shave cream for just {{ $details->country->currencyDisplay }}{{ $details->trial_price }}</h4>
</div> -->
<div class="col-md-12 text-center m-auto">
    <div class="col-xs-12">
        <p class="fs-22 MuliBold color-orange p-0 m-0"><b>Starter Kit | {{ $details->country->currencyDisplay }}{{ $details->trial_price }}</b></p>
        <p class="fs-16 MuliBold"><b>Save RM 18.00</b></p>
    </div>
</div>
<div class="col-md-12 pb-3 pt-3">
<div class="row justify-content-center"  style="display:block;margin:auto;width: 300px">
<img class="package-image" style="width: 100%" />
 </div>
</div>
<div class="col-md-12 p-0 pt-3 border-top">
    <h4><b>Select your blade type</b></h4>
    <div class="row justify-content-center">
        @foreach($details->blade_products as $blade)
        <div class="blade-{{$blade->productcountriesid}} col-xs-4 mr-3 item-unselected">
            <img src="{{URL::asset($blade->smallBladeImageUrl)}}" class="img-fluid" />
            <h6 class="text-center"><b>{{$blade->bladeName}}</b></h6>
        </div>
        @endforeach
    </div>
</div>
<div class="m-auto pt-3">
    <div class="col-md-12 pt-3 pr-0 pl-0">
        <h5><b>What's included in the Starter Kit</b></h5>
        <ul class="pr-0 pl-3">
            <li>
                <h5>Swivel Handle</h5>
            </li>
            <li>
                <h5>Blade Cartridge</h5>
            </li>
            <li>
                <h5>Shave Cream</h5>
            </li>
            <li>
                <h5>Free Shipping</h5>
            </li>
        </ul>
    </div>
    <div class="row pl-3">
        <span class="w-100">
            <h5><img src="{{URL::asset('/images/common/checkoutAssets/thumbsup.png')}}" class="img-fluid" /><b> Quality Guaranteed </b></h5>
        </span>
    </div>
</div>
</div>
</div> --}}


<div>
    <!--header
<div class="accordion md-accordion" id="accordionshaveplan" role="tablist" aria-multiselectable="true">
    <div class="card-header bg-none" role="tab" id="heading3">
     <a data-toggle="collapse" data-parent="#accordionshaveplan" href="#shaveplancollapse3" aria-expanded="false" aria-controls="shaveplancollapse3" class="collapsed">
      <h5 class="mb-0 MuliBold color-black">
      Why do I need a payment method to start my Trial Kit? <i class="fa fa-angle-right rotate-icon" style="float: right;"></i>
      </h5>
     </a>
    </div>
    <div id="shaveplancollapse3" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#accordionshaveplan">
      <div class="card-body pt-0">
       <p>Our business is providing our subscribers with Shave Plans that offer savings, convenience and high quality products.</p>
       <p>Shave Plans require recurring billing, which is not supported by online payment transfer.</p>
      </div>
    </div>
   <hr>
</div>
header-->
    <div class="container">

        <!-- Section: BACK BUTTON -->
        <div class="row">
            <div class="col-md-12 d-block pr-0 pl-0 ml-2">
                <button id="button-back" class="button-back hidden text-uppercase">
                    @lang('website_contents.global.content.back') </button> </div>
        </div> <!-- Section: GET STARTED -->
        <div id="collapse1" class="row panel-collapse collapse show">
            <div class="row row-footer m-0 -p-0">
                <!-- <div class="col-md-12 text-center pr-0 pl-0">
                                <h2 class="mt-2 pl-1 pr-1 w-100 pb-2">
                                    <b>Get started with the<br>Shaves2U Starter Kit</b>
                                </h2>
                               </div> -->
                <!-- <div class="col-md-12 text-center">
                                <h4 class="trial-sub-title"> A better shave, delivered.<br> Start with a shaver and a<br>shave cream for just {{ $details->country->currencyDisplay }}{{ $details->trial_price }}</h4>
                               </div> -->
                <div class="col-md-12 text-center m-0 -p-0">
                    <div class="col-xs-12">
                        <p class="fs-22 MuliBold p-0 m-0"><b>@lang('website_contents.authenticated.products.starter_kit') | <span class="color-grey"><strike>{{$details->country->currencyDisplay}}<span id="changetrialsavingprice">{{ $details->trial_saving_price }}</span></strike></span> <span class=" color-orange ">{{ $details->country->currencyDisplay }}{{ $details->trial_price }}</span></b></p>
                        <!-- <p class="fs-16 MuliBold"><b>Save RM 18.00</b></p> -->
                    </div>
                </div>
                <div class="col-md-12 pb-3 pt-2">
                    <div class="row justify-content-center"  style="display:block;margin:auto;width: 200px">
                        <img class="package-image" style="width: 100%" />
                    </div>
                </div>
                <div class="col-md-12 p-0 pt-3 border-top">
                    <h5 class="row justify-content-start p-0 pb-3 m-0 ml-4"><b>@lang('website_contents.authenticated.trial_plan.selection.select_blade')</b></h5>
                    <div class="row justify-content-center  p-0 pb-1 m-0  ml-3">
                    @php($countisoffline =  count((array)$details->blade_products))
                        @foreach($details->blade_products as $blade)
                        @if($countisoffline > 0)
                        @php($countisoffline =  count((array)$blade->isOffline))
                        @endif
                        @if($blade->isOffline === 1)
                        <div class="blade-{{$blade->productcountriesid}} col-xs-4 mr-3 item-unselected item-unselected-step-1">
                            @if($gender === 'female')
                            <img src="{{ asset('images/common/checkoutAssetsWomen/5blades-w.png') }}" class="img-fluid" />
                            @else
                            <img src="{{URL::asset($blade->smallBladeImageUrl)}}" class="img-fluid" />
                            @endif
                            <h6 class="text-center"><b>@lang('website_contents.global.blade',['blade' => $gender === 'female' ? 5 : $blade->bladeCount])</b></h6>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="pt-3">
                    <div class="col-md-12 pt-3 pr-0 pl-4">
                        <h5><b>@lang('website_contents.authenticated.trial_plan.selection.included_kit')</b></h5>
                        <ul class="pr-0 pl-3">
                            <li>
                                @if($gender === 'female')
                                <h6>@lang('website_contents.authenticated.trial_plan.selection.razor_handle')</h6>
                                @else
                                @if(strtoupper($current_country->codeIso) === "MY" || strtoupper($current_country->codeIso) === "SG")
                                <h6>@if(strtoupper($current_country->codeIso) === "SG") 1 x @endif Premium Handle</h6>
                                @else
                                <h6>@if(strtoupper($current_country->codeIso) === "SG") 1 x @endif @lang('website_contents.authenticated.trial_plan.selection.swivel_handle')</h6>
                                @endif
                                @endif
                            </li>
                            <li>
                                <h6>@lang('website_contents.authenticated.trial_plan.selection.blade_cartridge')</h6>
                            </li>
                            @if($gender === 'male')
                            <li>
                                <h6>@lang('website_contents.authenticated.trial_plan.selection.shave_cream')</h6>
                            </li>
                            @endif
                            <li>
                                <h6>@lang('website_contents.authenticated.trial_plan.selection.free_shipping')</h6>
                            </li>
                        </ul>
                    </div>
                    <div class="row" style="padding-left: 2rem">
                        <span class="w-100">
                            <h5><img src="{{URL::asset('/images/common/checkoutAssets/thumbsup.png')}}" class="img-fluid pb-2" /><b> @lang('website_contents.authenticated.trial_plan.selection.quality_guaranteed') </b></h5>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row m-auto pl-0 pr-0" id="mobile-button">
                <div class="col-12 text-center pl-0 pr-0">
                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-uppercase" type="submit">@lang('website_contents.authenticated.trial_plan.selection.start_now')</button>
                </div>
            </div>
        </div>

        <!-- Section: ONGOING PRODUCTS -->
        <div id="collapse2" class="row panel-collapse collapse">
            <div class="d-none d-sm-block" style="padding-bottom: 6.45rem;margin:auto;">
                <div class="row p-0 m-0 pb-2 ">
                    <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                        <p class="fs-22 MuliBold p-0 m-0">
                            <b>@lang('website_contents.authenticated.trial_plan.selection.choose_refill')</b>
                        <p>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <p class="fs-16 MuliPlain mt-3">
                            @lang('website_contents.authenticated.trial_plan.selection.refill_ships_date',['date' => $details->next_refill_date])
                        </p>
                    </div>
                </div>

                <!-- No addons -->
                <div class="row p-0 m-0">
                    @if($gender !== 'female')
                    <div class='col-12 d-flex'>
                            <div class="col-12 refill-0 text-center pt-4 pb-4 item-unselected pointer">
                                <h5 class="refill-0-title Muliplain orange">
                                    :blade_count: Blade Pack
                                </h5>
                                <h4 class="refill-0-price MuliExtraBold orange">
                                    :price:
                                </h4>
                                <img src="" class="refill-0-image w-30 pt-4" />
                                <h6 class="MuliPlain pt-3">
                                    @lang('website_contents.authenticated.trial_plan.selection.solo_pack_desc')
                                </h6>
                            </div>
                        </div>
                    {{-- <div class='col-md-4 d-flex'>
                        <div class="refill-0 text-center pt-4 pb-4 item-unselected pointer">
                            <h5 class="refill-0-title Muliplain orange">
                                :blade_count: Blade Pack
                            </h5>
                            <h4 class="refill-0-price MuliExtraBold orange">
                                :price:
                            </h4>
                            <img src="" class="refill-0-image w-30 pt-4" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.authenticated.trial_plan.selection.solo_pack_desc')
                            </h6>
                        </div>
                    </div> --}}
                    <!-- Plus shave cream -->
                    {{-- <div class='col-md-4 d-flex'>
                        <div class="refill-1 text-center pt-4 item-unselected pointer" style="padding-bottom: 40px">
                            <h5 class="refill-1-title Muliplain orange">
                                :blade_count: Blade, 1 Shave Cream
                            </h5>
                            <h4 class="refill-1-price MuliExtraBold orange">
                                :price:
                            </h4>
                            <img src="" class="refill-1-image w-34 pt-4" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.authenticated.trial_plan.selection.duo_pack_desc')
                            </h6>
                        </div>
                    </div> --}}
                    {{-- <div class='col-md-4 d-flex'>
                        <!-- Plus after shave cream -->
                        <div class="refill-2 text-center pt-4 item-unselected pointer" style="padding-bottom: 5px">
                            <h5 class="refill-2-title Muliplain orange">
                                :blade_count: Blade, 1 Shave Cream <br> 1 After Shave Cream
                            </h5>
                            <h4 class="refill-2-price MuliExtraBold orange">
                                :price:
                            </h4>
                            <img src="" class="refill-2-image" style="width: 48%" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.authenticated.trial_plan.selection.combo_pack_desc')
                            </h6>
                        </div>
                    </div> --}}
                    @else
                    @if($gender === 'female')
                    <div class='col-12 d-flex'>
                        <div class="col-12 refill-0 text-center pt-4 pb-4 item-unselected pointer">
                            <h5 class="refill-0-title Muliplain orange">
                                :blade_count: Blade Pack
                            </h5>
                            <h4 class="refill-0-price MuliExtraBold orange">
                                :price:
                            </h4>
                            <img src="" class="refill-0-image w-30 pt-4" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.authenticated.trial_plan.selection.solo_pack_desc')
                            </h6>
                        </div>
                    </div>
                    @endif
                    @endif
                </div>
                <div class="row m-auto">
                    <div id="mobile-button" class="col-12 text-center pt-5 pl-0 pr-0">
                        <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-uppercase" type="submit">@lang('website_contents.global.content.next')</button>
                    </div>
                </div>
            </div>

            <div class="d-block d-sm-none">
                <div class="row p-0 m-0 pb-2">
                    <div class="col-md-12 text-center p-0 pl-0 pr-0 step1-heading">
                        <p class="fs-22 MuliBold p-0 m-0">
                            <b>@lang('website_contents.authenticated.trial_plan.selection.choose_refill')</b>
                        </p>
                    </div>
                    <div class="col-md-12 text-center p-0 pl-0 pr-0">
                        <p class="fs-14 MuliPlain mt-3">
                            @lang('website_contents.authenticated.trial_plan.selection.refill_ships_date',['date' => $details->next_refill_date])
                        </p>
                    </div>
                </div>

                <div class="row p-0 m-0 ">
                    @if($gender !== 'female')
                    <div class='col-md-12'>
                        <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-7 box-detail">
                                    <p class="fs-16 refill-0-title Muliplain pt-1 select-line-height orange">
                                        :blade_count: Blade Cartridge Pack
                                    </p>
                                    <h4 class="refill-0-price MuliExtraBold orange">
                                        :price:
                                    </h4>
                                    <p class="fs-14 Muliplain pt-2 orange">
                                            @lang('website_contents.authenticated.trial_plan.selection.solo_pack_desc')
                                    </p>
                                </div>
                                <div class="col-5">
                                    <img src="" class="refill-0-image w-80 pb-10 pl-30 box-detail-img" />
                                    <!-- <span class="check-sign check-sign-select">&check;</span> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Plus shave cream -->
                    {{-- <div class='col-md-12'>
                        <div class="refill-1 m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-7 box-detail">
                                    <p class="fs-16 refill-1-title Muliplain pt-1 select-line-height orange">
                                        :blade_count: Blades,<br>Shave Cream
                                    </p>
                                    <h4 class="refill-1-price MuliExtraBold orange">
                                        :price:
                                    </h4>
                                    <p class="fs-14 Muliplain pt-2 orange">
                                        @lang('website_contents.authenticated.trial_plan.selection.duo_pack_desc')
                                    </p>
                                </div>
                                <div class="col-5">
                                    <img src="" class="refill-1-image w-60 ml-4 box-detail-img" />
                                    <!-- <span class="check-sign-1 check-sign-select">&check;</span> -->
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class='col-md-12' style="padding-bottom:20vw">
                        <!-- Plus after shave cream -->
                        <div class="refill-2 m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-7 box-detail">
                                    <p class="fs-16 refill-2-title Muliplain pt-1 select-line-height orange">
                                        :blade_count: Blades,<br>Shave Cream,<br>After Shave Cream
                                    </p>
                                    <h4 class="refill-2-price MuliExtraBold orange">
                                        :price:
                                    </h4>
                                    <p class="fs-14 Muliplain pt-2 orange">
                                        @lang('website_contents.authenticated.trial_plan.selection.combo_pack_desc')
                                    </p>
                                </div>
                                <div class="col-5">
                                    <img src="" class="refill-2-image w-100 pr-3 pb-2 box-detail-img" />
                                    <!-- <span class="check-sign-2 check-sign-select">&check;</span> -->
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    @else
                    @if($gender === 'female')
                    <div class='col-md-12'>
                            <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                                <div class="row">
                                    <div class="col-7 box-detail">
                                        <p class="fs-16 refill-0-title Muliplain pt-1 select-line-height orange">
                                            :blade_count: Blade Cartridge Pack
                                        </p>
                                        <h4 class="refill-0-price MuliExtraBold orange">
                                            :price:
                                        </h4>
                                        <p class="fs-14 Muliplain pt-2 orange">
                                                @lang('website_contents.authenticated.trial_plan.selection.solo_pack_desc')
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <img src="" class="refill-0-image w-80 pb-10 pl-30 box-detail-img" />
                                        <!-- <span class="check-sign check-sign-select">&check;</span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endif
                </div>
                <div class="row d-block d-sm-none m-auto">
                    <div id="mobile-button" class="col-12 text-center pt-5 pl-0 pr-0">
                        <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-uppercase" type="submit">@lang('website_contents.global.content.next')</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section: SHIPPING FREQUENCY -->
        <div id="collapse3" class="row panel-collapse collapse pb-3">
            <div class="row row-center">
                <div class="row p-0 m-0 pb-2">
                    <div class="col-md-12 text-center p-0 m-0 pl-0 pr-0">
                        <p class="fs-22 MuliBold p-0 m-0">
                            <b>@lang('website_contents.authenticated.trial_plan.selection.shave_frequency')</b>
                        </p>
                    </div>
                    <div class="col-md-12 text-center pl-3 pr-3">
                        <p class="fs-14 MuliPlain mt-3">
                            @lang('website_contents.authenticated.trial_plan.selection.change_frequency')
                       </p>
                    </div>
                </div>

                <!-- @foreach($details->frequency_list as $key => $frequency)
                    <div class="col-12 mb-3" onClick="SelectFrequency({{ $frequency->duration }})">
                        <div class="row frequency-{{$frequency->duration}} text-center pt-4 item-unselected">
                            <div class="col-6 orange">
                                <h4 class="MuliExtraBold pt-4">
                                {!! $frequency->durationText !!}
                            </h4>
                                <h5 class="frequency-{{$frequency->duration}}-details MuliPlain pt-4 frequency-p">
                                {!! $frequency->detailsText !!}<br><br>
                            </h5>
                            </div>
                            <div class="col-6">
                                <img src="{{URL::asset($frequency->image)}}" class="img-fluid pb-3" />
                            </div>
                        </div>
                    </div>
                    @endforeach -->

                <div class="col-md-12 d-block p-0" style="padding-bottom: 8vw !important;">
                    @foreach($details->frequency_list as $key => $frequency)
                    <div class="col-12 mb-3" onClick="SelectFrequency({{ $frequency->duration }})">
                        <div class="row frequency-{{$frequency->duration}} m-2 pl-2 pt-2 item-unselected">
                            <div class="col-8 frequency-title-top orange">
                                <h4 class="MuliExtraBold">
                                    {!! $frequency->durationText !!}
                                </h4>
                                <p class="fs-13 frequency-{{$frequency->duration}}-details MuliPlain pt-2 frequency-p select-line-height">
                                    {!! $frequency->detailsText !!}<br><br>
                                </p>
                            </div>
                            <div class="col-4">
                                <img src="{{URL::asset($frequency->image)}}" class="img-fluid pb-3" />
                                <!-- <span class="check-sign-3 check-sign-select">&check;</span> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- <div class="row">
                            <div id="mobile-button" class="col-12 pl-0 pr-0">
                                <form id="form-proceed-checkout1" method="GET" action="#">
                                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-center text-uppercase" type="submit">Review Order</button>
                                </form>
                            </div>
                        </div> -->

                    <!-- <div  id="mobile-button" class="col-12 text-center pt-5 pb-5">
                        <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">NEXT STEP</button>
                    </div> -->
                </div>

            </div>

            <div id="mobile-button" class="col-12 pl-0 pr-0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                <form id="form-proceed-checkout" method="GET" action="#">
                    <button id="button-proceed-checkout" class="button-next button-proceed-checkout-mobile MuliExtraBold text-center text-uppercase" type="button">@lang('website_contents.authenticated.selection.review_order')</button>
                </form>
            </div>

        </div>

        <!-- Modal: Notification trial plan exist -->
        <div class="modal fade" id="notification_trial_plan_exist" tabindex="-1" role="dialog" aria-labelledby="notification_trial_plan_exist" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-trial-plan-exist">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-5" style="padding-top: 0 !important">
                        <div class="col-12 text-center">
                            <h5 class="modal-trial-plan-text">@lang('website_contents.authenticated.trial_plan.selection.already_signed_up')</h5>
                            <br>
                            <h5 class="modal-trial-plan-text">@lang('website_contents.authenticated.trial_plan.selection.restart_plan')</h5>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row m-auto pl-0 pr-0 mb-5" id="mobile-button">
            {{-- <div class="col-12 text-center pl-0 pr-0">
                    <button class="button-next button-proceed-checkout-mobile text-uppercase" type="submit">Start Now</button>
                </div> --}}
            <input type="hidden" id="selected_trial_blade" value="" />
            <form id="goToSelectedView" method="POST" action="">
                @csrf
                <input type="hidden" name="trial_data" id="trial_data" value="" />
            </form>
        </div>
    </div>

    <script type="text/javascript">
             let handle_products = {!! json_encode($detailsV2["handle_products"])!!};
    let plan_type_based_on_handle = {!!json_encode($detailsV2["plan_type_based_on_handle"]) !!};
    let blade_products = {!! json_encode($detailsV2["blade_products"])!!};
    // console.log(blade_products);
    let frequency_list = {!! json_encode($detailsV2["frequency_list"])!!};
    let addon_products = {!! json_encode($detailsV2["addon_products"])!!};
    let free_shave_cream_id = {!! json_encode($detailsV2["free_shave_cream_id"])!!};
    let allplan = {!! json_encode($detailsV2["allplan"])!!};
    let allannualplan = {!! json_encode($detailsV2["allannualplan"])!!};
    let country_id = <?php echo $details->country->id; ?>;
    let currency = {!! json_encode($detailsV2["currency"])!!};
    let trial_price = {!! json_encode($detailsV2["trial_price"])!!};
    let trial_saving_price = {!! json_encode($detailsV2["trial_saving_price"])!!};
    let next_refill_date = {!! json_encode($detailsV2["next_refill_date"])!!};
    let user_id = null;
    let country = <?php echo json_encode(Session::get('currentCountry')) ?>;
    let langCode = <?php echo json_encode(strtolower(app()->getLocale())) ?>;
    let trial_saving_price_list = {!! json_encode($detailsV2["trial_saving_price_list"])!!};
    let country_handles = {!!json_encode($m_h_sku) !!};
    let countisoffline = <?php echo $countisoffline; ?>;
    // console.log(handle_products,blade_products,frequency_list,addon_products,free_shave_cream_id,allplan,allannualplan);
    </script>
    <script src="{{ asset('js/api/products/products.function.js') }}"></script>
    <script src="{{ asset('js/functions/trial-plan/trial-plan-selection.function.js') }}"></script>
    @endsection
