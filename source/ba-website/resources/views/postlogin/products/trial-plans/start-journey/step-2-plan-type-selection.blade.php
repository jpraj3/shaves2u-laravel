@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">

<!-- <div style="background: linear-gradient(to bottom, white 40%, #ECECEC 60%);">
    <div class="container">

        
        <div class="row">
            <div class="col-md-12 d-none d-sm-block pr-0 pl-0">
                <button id="button-back" class="button-back hidden">
                    < BACK </button>
            </div>
        </div>


         <div class="row ">
            <div class="d-none d-sm-block">
                <div class="row pb-5">
                    <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                        <h1 class="mt-5 MuliExtraBold trial-kit-title step1-title">
                        Choose your next refill
                    </h1>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h4 class="mt-3 MuliPlain">
                        First refil ships <b>{{$details->next_refill_date}} (not charged today).</b> Cancel anytime.
                    </h4>
                    </div>
                </div>

                <div class="row">
                        @foreach($details->blade_products as $blade)
                        @if((int) $blade->productcountriesid === (int) $details->selected_blade->productcountriesid)
                        @php($addon_all_price = $blade->sellPrice)
                    <div class='col-md-4' onClick="SelectRefill(0,{{ $blade->bladeCount }})">
                        <div class="refill-0 text-center pt-5 pb-5 item-unselected">

                            <h5 class="refill-0-title Muliplain">
                                   {{ $blade->bladeName }}
                        </h5>
                            <h4 class="refill-0-price MuliExtraBold">
                            {{ number_format((float)$addon_all_price, 2, '.', '') }}
                        </h4>
                            <img src="{{ asset('images/common/checkoutAssets/checkout-package1-'.$blade->bladeCount.'blade.png') }}" class="refill-0-image w-30" />
                            <h6 class="MuliPlain pt-3">
                            {{ $blade->title }}
                        </h6>
                        </div>
                    </div>

                    @foreach($details->addon_products as $addon_product)
                    @if($addon_product->slug === 'shave_cream_24')
                    @php($addon_all_price = $addon_all_price + $addon_product->sellPrice)
                    <div class='col-md-4' onClick="SelectRefill(1,{{ $blade->bladeCount }})">
                        <div class="refill-1 text-center pt-5 pb-5 item-unselected">
                            <h5 class="refill-1-title Muliplain">
                                    {{ $blade->bladeName }} Pack, 1 Shave Cream
                        </h5>
                            <h4 class="refill-1-price MuliExtraBold">
                            {{ number_format((float)$addon_all_price, 2, '.', '') }}
                        </h4>
                            <img src="{{ asset('images/common/checkoutAssets/checkout-package2-'.$blade->bladeCount.'blade.png') }}" class="refill-1-image w-34" />
                            <h6 class="MuliPlain pt-2">
                            Helps make every shave<br>smoother and more comfortable.
                        </h6>
                        </div>
                    </div>
                    @endif

                    @if($addon_product->slug === 'after_shave_cream_11')
                    @php($addon_all_price = $addon_all_price + $addon_product->sellPrice)
                    <div class='col-md-4' onClick="SelectRefill(2,{{ $blade->bladeCount }})">

                        <div class="refill-2 text-center pt-4 item-unselected" style="padding-bottom: 47px">
                            <h5 class="refill-2-title Muliplain">
                                    {{ $blade->bladeName }} Pack, 1 Shave Cream <br> 1 After Shave Cream
                        </h5>
                            <h4 class="refill-2-price MuliExtraBold">
                            {{ number_format((float)$addon_all_price, 2, '.', '') }}
                        </h4>
                            <img src="{{ asset('images/common/checkoutAssets/checkout-package3-'.$blade->bladeCount.'blade.png') }}" class="refill-2-image" style="width: 48%" />
                            <h6 class="MuliPlain pt-3">
                            Leaves you feeling refreshed and<br>smelling good.
                        </h6>
                        </div>
                    </div>
                    @endif
                    @endforeach

                    @endif
                    @endforeach
                </div>
                <div class="col-12 text-center pt-5 pb-5">
                    <button class="button-next button-proceed-checkout MuliExtraBold" style="width: 30%" type="submit">NEXT STEP</button>
                </div>
            </div>

            <div class="d-block d-sm-none">
                <div class="row pb-5">
                    <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                        <h1 class="mt-5 MuliExtraBold trial-kit-title step1-title">
                        Choose your next refill
                    </h1>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h4 class="mt-3 MuliPlain">
                        First refil ships <br><b>{{ $details->next_refill_date }} (not charged today).</b><br>Cancel anytime.
                    </h4>
                    </div>
                </div>

                <div class="row">
                    <div class='col-md-12'>
                        <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="refill-0-title Muliplain">
                                    Blade Cartridge Pack
                                </h5>
                                    <h4 class="refill-0-price MuliExtraBold">
                                    :price:
                                </h4>
                                    <h5 class="Muliplain pt-3">
                                    Makes you feeling refreshed and smelling good
                                </h5>
                                </div>
                                <div class="col-6">
                                    <img src="" class="refill-0-image w-60 pb-10 pl-30" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="refill-1 m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="refill-1-title Muliplain">
                                    Blade Pack, 1 Shave Cream
                                </h5>
                                    <h4 class="refill-1-price MuliExtraBold">
                                    :price:
                                </h4>
                                    <h4 class="mt-3 MuliPlain">
                                    First refil ships <br><b>{{$details->next_refill_date}} (not charged today).</b><br>Cancel anytime.
                                </h4>
                                </div>
                                <div class="col-6">
                                    <img src="" class="refill-1-image w-60 ml-4" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12' style="padding-bottom:20vw">

                        <div class="refill-2 m-2 pl-2 pt-2 item-unselected">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="refill-2-title Muliplain">
                                    Blade Pack, 1 Shave Cream, 1 After Shave Cream
                                </h5>
                                    <h4 class="refill-2-price MuliExtraBold">
                                    :price:
                                </h4>
                                    <h5 class="Muliplain pt-3">
                                Leaves you feeling refreshed and smelling good
                                </h5>
                                </div>
                                <div class="col-6">
                                    <img src="" class="refill-2-image w-100 pr-3 pb-2" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-block d-sm-none m-auto">
                    <div id="mobile-button" class="col-12 text-center pt-5 pl-0 pr-0">
                        <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">NEXT STEP</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="goToSelectedView" method="POST" action="">
        @csrf
        <input type="hidden" name="trial_data" id="trial_data" value="" />
    </form>
</div> -->

<div class="container">
    <div class="row pb-2">
        <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
            <h1 class="mt-2">
            Choose Your Ongoing Refill
        </h2>
        </div>
        <div class="col-md-12 text-center pl-0 pr-0">
            <p>
                First refil ships <b>{{$details->next_refill_date}} (not charged today).</b> Cancel anytime.
            </p>
        </div>
    </div>

    <div class="row">
        @foreach($details->blade_products as $blade)
        @if((int) $blade->productcountriesid === (int) $details->selected_blade->productcountriesid)
        @php($addon_all_price = $blade->sellPrice)
        <div class='col-md-12' onClick="SelectRefill(0,{{ $blade->bladeCount }})">
            <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                <div class="row align-items-center paddTB20">
                    <div class="col-6 pl-5">
                        <h5 class="refill-0-title Muliplain">
                        {{ $blade->bladeName }}
                    </h5>
                        <h4 class="refill-0-price MuliExtraBold">
                        {{ number_format((float)$addon_all_price, 2, '.', '') }}
                    </h4>
                        <h5 class="Muliplain pt-3">
                        Makes you feeling refreshed and smelling good
                    </h5>
                    </div>
                    <div class="col-6 text-right pr-5">
                        <img src="{{ asset('images/common/checkoutAssets/checkout-package1-'.$blade->bladeCount.'blade.png') }}" class="refill-0-image" style="width: 60px" />
                    </div>
                </div>
            </div>
        </div>

        <!-- Plus shave cream -->
        @foreach($details->addon_products as $addon_product)
        @if($addon_product->slug === 'shave_cream_24')
        @php($addon_all_price = $addon_all_price + $addon_product->sellPrice)
        <div class='col-md-12' onClick="SelectRefill(1,{{ $blade->bladeCount }})">
            <div class="refill-1 m-2 pl-2 pt-2 item-unselected">
                <div class="row align-items-center paddTB20">
                    <div class="col-6 pl-5">
                        <h5 class="refill-1-title Muliplain">
                        {{ $blade->bladeName }} Blade Pack, 1 Shave Cream
                    </h5>
                        <h4 class="refill-1-price MuliExtraBold">
                        {{ number_format((float)$addon_all_price, 2, '.', '') }}
                    </h4>
                        <h4 class="mt-3 MuliPlain">
                        First refil ships <br><b>{{$details->next_refill_date}} (not charged today).</b><br>Cancel anytime.
                    </h4>
                    </div>
                    <div class="col-6 text-right pr-5">
                        <img src="{{ asset('images/common/checkoutAssets/checkout-package2-'.$blade->bladeCount.'blade.png') }}" class="refill-1-image" style="width: 80px" />
                    </div>
                </div>
            </div>
        </div>


        <!-- Plus after shave cream -->
        @endif
        @if($addon_product->slug === 'after_shave_cream_11')
        @php($addon_all_price = $addon_all_price + $addon_product->sellPrice)
        <div class='col-md-12' onClick="SelectRefill(2,{{ $blade->bladeCount }})">
            <div class="refill-2 m-2 pl-2 pt-2 item-unselected">
                <div class="row align-items-center paddTB20">
                    <div class="col-6 pl-5">
                        <h5 class="refill-2-title Muliplain">
                        {{ $blade->bladeName }} Blade Pack, 1 Shave Cream, 1 After Shave Cream
                    </h5>
                        <h4 class="refill-2-price MuliExtraBold">
                        {{ number_format((float)$addon_all_price, 2, '.', '') }}
                    </h4>
                        <h5 class="Muliplain pt-3">
                    Leaves you feeling refreshed and smelling good
                    </h5>
                    </div>
                    <div class="col-6 text-right pr-5">
                        <img src="{{ asset('images/common/checkoutAssets/checkout-package3-'.$blade->bladeCount.'blade.png') }}" class="refill-2-image" style="width: 100px" />
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach

        @endif
        @endforeach
    </div>
    
    <form id="goToSelectedView" method="POST" action="">
        @csrf
        <input type="hidden" name="trial_data" id="trial_data" value="" />
    </form>
</div>

<div id="mobile-button" class="col-12 text-center pl-0 pr-0">
    <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">NEXT STEP</button>
</div>


<script type="text/javascript">
//Set selected variables
    let selected_blade = {!! json_encode($details->selected_blade)!!};
    let current_step = 2;
    let handle_products = {!! json_encode($details->handle_products)!!};
    let blade_products = {!! json_encode($details->blade_products)!!};
    let frequency_list = {!! json_encode($details->frequency_list)!!};
    let addon_products = {!! json_encode($details->addon_products)!!};
    let free_shave_cream_id = {!! json_encode($details->free_shave_cream_id)!!};
    let allplan = {!! json_encode($details->allplan)!!};
    let country_id = <?php echo $details->country_id; ?>;
    let currency = {!! json_encode($details->currency)!!};
    let trial_price = {!! json_encode($details->trial_price)!!};
    let trial_saving_price = {!! json_encode($details->trial_saving_price)!!};
    let next_refill_date = {!! json_encode($details->next_refill_date)!!};
    let user_id = null;
</script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
<script src="{{ asset('js/functions/trial-plan/trial-plan-selection.function.js') }}"></script>
@endsection
