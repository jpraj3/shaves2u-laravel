@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">

<div style="background: linear-gradient(to bottom, white 40%, #ECECEC 60%);">
    <div class="container">

        <!-- Section: BACK BUTTON -->
        <div class="row">
            <div class="col-md-12 d-none d-sm-block pr-0 pl-0">
                <button id="button-back" class="button-back hidden">
                    < BACK </button>
            </div>
        </div>

    <!-- Section: SHIPPING FREQUENCY -->
    <div class="row">
            <div class="row row-center">
                <div class="row pb-5">
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h1 class="mt-5 MuliExtraBold trial-kit-title">
                        How often do you shave?
                    </h1>
                    </div>
                    <div class="col-md-12 text-center pl-1 pr-1">
                        <h5 class="mt-3 MuliPlain pl-3 pr-3">
                            We'll send your ongoing shipments based on how often you shave. It's easy to change your shipping frequency at any time.
                        </h5>
                    </div>
                </div>

                <div class="col-md-12 pb-5">
                    @foreach($details->frequency_list as $key => $frequency)
                    <div class="col-12 mb-3" onClick="SelectFrequency({{ $frequency->duration }})">
                        <div class="row frequency-{{$frequency->duration}} text-center pt-4 item-unselected">
                            <div class="col-6">
                                <h4 class="MuliExtraBold pt-4">
                                {!! $frequency->durationText !!}
                            </h4>
                                <h5 class="frequency-{{$frequency->duration}}-details MuliPlain pt-4 frequency-p">
                                {!! $frequency->detailsText !!}<br><br>
                            </h5>
                            </div>
                            <div class="col-6">
                                <img src="{{URL::asset($frequency->image)}}" class="img-fluid pb-3" />
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div id="mobile-button" class="col-12 text-center pl-0 pr-0">
        {{-- <form id="form-proceed-checkout" method="GET" action=""> --}}
            <button onClick="proceedToBATrialCheckout();" class="button-proceed-checkout-mobile MuliExtraBold" type="button">REVIEW ORDER</button>
        {{-- </form> --}}
    </div>

    <form id="goToSelectedView" method="POST" action="">
        @csrf
        <input type="hidden" name="trial_data" id="trial_data" value="" />
    </form>
</div>



<script type="text/javascript">
    let selected_blade = {!! json_encode($details->selected_blade)!!};
    let current_step = 3;
    let handle_products = {!! json_encode($details->handle_products)!!};
    let blade_products = {!! json_encode($details->blade_products)!!};
    let frequency_list = {!! json_encode($details->frequency_list)!!};
    let addon_products = {!! json_encode($details->addon_products)!!};
    let free_shave_cream_id = {!! json_encode($details->free_shave_cream_id)!!};
    let allplan = {!! json_encode($details->allplan)!!};
    let country_id = <?php echo $details->country_id; ?>;
    let currency = {!! json_encode($details->currency)!!};
    let trial_price = {!! json_encode($details->trial_price)!!};
    let trial_saving_price = {!! json_encode($details->trial_saving_price)!!};
    let next_refill_date = {!! json_encode($details->next_refill_date)!!};
    let user_id = null;
    let country = <?php echo json_encode(Session::get('currentCountry')) ?>;
    let langCode = <?php echo json_encode(strtolower(app()->getLocale())) ?>;
</script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
<script src="{{ asset('js/functions/trial-plan/trial-plan-selection.function.js') }}"></script>
@endsection
