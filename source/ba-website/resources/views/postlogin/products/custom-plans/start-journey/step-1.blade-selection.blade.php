

@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">

<div class="container">
        <div style="background: linear-gradient(to bottom, white 40%, #ECECEC 60%);">
                <div class="container">
                    <!-- Section: BACK BUTTON -->
                    <div class="row">
                        <div class="col-md-12 d-none d-sm-block pr-0 pl-0">
                            <button id="button-back" class="button-back hidden">
                                < BACK </button>
                        </div>
                    </div>
                    <!-- Section: GET STARTED -->
                    <div id="collapse1" class="row panel-collapse collapse show">
                        <div class="row">
                            <div class="col-md-12 text-center pr-0 pl-0">
                                <h1 class="mt-5 pl-1 pr-1 MuliExtraBold trial-kit-title w-100">
                                Get started with the Shaves2U Starter Kit
                            </h1>
                                <h5 class="col-md-12 d-none d-sm-block MuliPlain w-100">
                                A better shave, delivered. Start with a shaver and a shave cream for just currencytrial_price
                            </h5>
                                <h5 class="col-xs-12 d-block d-sm-none MuliPlain">
                                A better shave, delivered.<br>Start with a shaver and a<br>shave cream for just currencytrial_price
                            </h5>
                            </div>
                            <div class="d-block d-sm-none text-center m-auto pt-3">
                                <div class="col-xs-12 color-orange" id="trialprice_1">
                                    <h2><b>Starter Kit | currencytrial_price</b></h2>
                                </div>
                                <div class="col-xs-12 pt-1" id="trialpricesaving_1">
                                    <h5><b>Save currencytrial_saving_price</b></h5>
                                </div>
                            </div>
                            <div class="col-md-6 pr-0 pl-0 text-center">
                                <img src="" class="package-image w-70" />
                            </div>
                            <div class="col-xs-12 pt5 m-auto d-block d-sm-none">
                                <hr>
                                <h4><b>Select your blade type</b></h4>
                                <div class="row pt-2 d-inline-flex" id="selectyourbladetype_1">
                                </div>
                            </div>
                            <div class="d-block d-sm-none m-auto pt-3">
                                <div class="col-md-12 pt-3 pr-0 pl-0">
                                    <h4><b>What's included in the Starter Kit</b></h4>
                                    <ul class="pr-0 pl-3">
                                        <li>
                                            <h5>Swivel Handle</h5></li>
                                        <li>
                                            <h5>Blade Cartridge</h5></li>
                                        <li>
                                            <h5>Shave Cream</h5></li>
                                        <li>
                                            <h5>Free Shipping</h5></li>
                                    </ul>
                                </div>
                                <div class="row pl-3">
                                    <span class="w-100">
                                    <h5><img src="" class="img-fluid" /><b> Quality Guaranteed </b></h5>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-6 pr-0 pl-0">
                                <div class="col-md-12 pt-5 d-none d-sm-block">
                                    <div class="row ">
                                        <div class="col-md-7 color-orange">
                                            <h2><b>Starter Kit&nbsp | &nbspcurrencytrial_price</b></h2>
                                        </div>
                                        <div class="col-md-5 pt-1">
                                            <h5>Save currencytrial_saving_price</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-12 pt-3 pr-0 pl-0">
                                        <h4><b>What's included in the Starter Kit</b></h4>
                                        <ul class="pr-0 pl-3">
                                            <li>
                                                <h5>Swivel Handle</h5></li>
                                            <li>
                                                <h5>Blade Cartridge</h5></li>
                                            <li>
                                                <h5>Shave Cream</h5></li>
                                            <li>
                                                <h5>Free Shipping</h5></li>
                                        </ul>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-md-12 pt5 pr-0 d-none d-sm-block">
                                    <h4><b>Select your blade type</b></h4>
                                    <div class="row pt-2 d-inline-flex" id="selectyourbladetype_2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center pt-5 d-none d-sm-block ">
                            <button id="get_started-next" class="button-next button-proceed-checkout MuliExtraBold" style="width: 30%">START NOW</button>
                        </div>
                        <div class="col-md-12 text-center pl-0 pr-0 d-none d-sm-block">
                            <div class="row">
                                <span class="w-100">
                            <h5><img src="" class="img-fluid pr-2" /> <b> Quality Guaranteed </b></h5>
                            </span>
                            </div>
                        </div>

                        <div class="col-xs-12 d-block d-sm-none pt-5">
                            <div class="row">
                                <div class="col-xs-12 text-center m-auto pr-0 pl-0">
                                    <h1 class="MuliExtraBold trial-kit-title">
                                    How it works
                                </h1>
                                </div>
                                <div class="col-xs-6 text-center m-auto pr-3 pl-3" style="color: #828282;">
                                    <h5 class="MuliPlain">
                                    Shaves2U brings you a unique shaving experience tailored for your grooming needs. Get started in just three simple steps!
                                </h5>
                                </div>
                            </div>
                            <div class="row pr-0 pl-0 row-center">
                                <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                                    <img src="" class="w-50" />
                                    <h5 class="MuliPlain color-orange">
                                    Step 1
                                </h5>
                                    <h2 class="MuliSemiBold">
                                    Build your plan
                                </h2>
                                    <h5 class="MuliPlain">
                                    Choose your frequency of delivery<br>you desire.
                                </h5>
                                </div>
                                <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                                    <img src="" class="w-50" />
                                    <h5 class="MuliPlain color-orange">
                                    Step 2
                                </h5>
                                    <h2 class="MuliSemiBold">
                                    Try for 2 weeks
                                </h2>
                                    <h5 class="MuliPlain">
                                    Start by selecting the ideal blade<br>type of your shaving needs.
                                </h5>
                                </div>
                                <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                                    <img src="" class="w-50" />
                                    <h5 class="MuliPlain color-orange">
                                    Step 3
                                </h5>
                                    <h2 class="MuliSemiBold">
                                    Be in Control
                                </h2>
                                    <h5 class="MuliPlain">
                                    Change or stop your plan at any<br>time from your profile page.
                                </h5>
                                </div>
                            </div>
                            <div class="row pr-0 pl-0 pt-5">
                                <div class="col-xs-12 pr-0 pl-0">
                                    <img src="" class="w-100" />
                                </div>
                                <div class="col-xs-12 text-center pr-3 pl-3">
                                    <h2 class="MuliExtraBold">
                                    Crafted for control.
                                </h2>
                                    <h5 class="MultiPlain">
                                    Our Swivel Handle is designed with a comfortable rubberised grip and weighted metal body for optimal control, topped with a striking chrome finish. For a shave with substance and style.
                                </h5>
                                </div>
                                <div class="col-xs-12 pr-0 pl-0 pt-5">
                                    <img src="" class="w-100" />
                                </div>
                                <div class="col-xs-12 text-center pr-4 pl-4">
                                    <h2 class="MuliExtraBold">
                                    Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.
                                </h2>
                                    <h5 class="MultiPlain" style="color: #828282;">
                                    Our Blade Cartridges have everything you need for a superior shave: presicion-honed carbon steel blades, a moisturising lubrication strip, and an anti-dog design.
                                </h5>
                                </div>
                                <div class="col-xs-12 text-center pr-3 pl-3 pt-5">
                                    <div class="col-xs-12 pr-0 pl-0">
                                        <img src="" class="w-100" />
                                    </div>
                                    <h2 class="MuliExtraBold">
                                    Good for your skin.
                                </h2>
                                    <h5 class="MultiPlain" style="color: #828282;">
                                    Our shave cream cushions and conditions your facial hair for a gentler, smoother shave Enriched with anti-inflammatory ingredients to help reduce post-shave redness.
                                </h5>
                                </div>
                            </div>
                            <div class="row d-block d-sm-none m-auto pl-0 pr-0" id="mobile-button">
                                <div class="col-12 text-center pl-0 pr-0">
                                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-uppercase" type="submit">Start Now</button>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center pr-0 pl-0 pt-5 d-none d-sm-block">
                                <h1 class="mt-5 MuliExtraBold trial-kit-title">
                                How it works
                            </h1>
                            </div>
                            <div class="col-md-12 text-center pr-0 pl-0 pb-5 d-none d-sm-block" style="color: #828282;">
                                <h4 class="mt-4 MuliPlain">
                                Shaves2U brings you a unique shaving experience tailored for your grooming needs.
                            </h4>
                                <h4 class="MuliPlain">
                            Get started in just three simple steps!
                            </h4>
                            </div>
                        </div>
                        <div class="row pr-0 pl-0 row-center">
                            <div class="col-md-4 text-center pr-0 pl-0 d-none d-sm-block">
                                <img src="" class="img-fluid" />
                                <h5 class="MuliPlain color-orange pb-3">
                                Step 1
                            </h5>
                                <h2 class="MuliSemiBold pb-3">
                                Build your plan
                            </h2>
                                <h5 class="MuliPlain" style="color: #828282;">
                                Choose your frequency of delivery<br>you desire.
                            </h5>
                            </div>
                            <div class="col-md-4 text-center pr-0 pl-0 d-none d-sm-block">
                                <img src="" class="img-fluid" />
                                <h5 class="MuliPlain color-orange pb-3">
                                Step 2
                            </h5>
                                <h2 class="MuliSemiBold pb-3">
                                Try for 2 weeks
                            </h2>
                                <h5 class="MuliPlain" style="color: #828282;">
                                Start by selecting the ideal blade<br>type of your shaving needs.
                            </h5>
                            </div>
                            <div class="col-md-4 text-center pr-0 pl-0 d-none d-sm-block">
                                <img src="" class="img-fluid" />
                                <h5 class="MuliPlain color-orange pb-3">
                                Step 3
                            </h5>
                                <h2 class="MuliSemiBold pb-3">
                                Be in Control
                            </h2>
                                <h5 class="MuliPlain" style="color: #828282;">
                                Change or stop your plan at any<br>time from your profile page.
                            </h5>
                            </div>
                        </div>
                        <div class="row pr-0 pl-0 pt-150">
                            <div class="col-lg-4 text-center pr-0 pl-0 d-none d-sm-block">
                                <h1 class="MuliExtraBold pt-100 w-500px">
                                Crafted for control.
                            </h1>
                                <h4 class="MultiPlain lh-40px w-500px">
                                Our Swivel Handle is designed with a comfortable<br>rubberised grip and weighted metal body for<br>optimal control, topped with a striking chrome<br>finish. For a shave with substance and style.
                            </h4>
                            </div>
                            <div class="col-lg-8 pr-0 pl-5 d-none d-sm-block">
                                <img src="" class="w-100" />
                            </div>
                        </div>
                        <div class="row pr-0 pl-0 pt-150">
                            <div class="col-lg-6 pr-0 pl-0 d-none d-sm-block">
                                <img src="" class="w-100" />
                            </div>
                            <div class="col-lg-6 text-center pr-0 pl-0 d-none d-sm-block">
                                <h1 class="MuliExtraBold pt-100">
                                Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.
                            </h1>
                                <h4 class="MultiPlain lh-40px">
                                Our Blade Cartridges have everything you<br>need for a superior shave: presicion-honed<br>carbon steel blades, a moisturising<br>lubrication strip, and an anti-clog design.
                            </h4>
                            </div>
                        </div>
                        <div class="row pr-0 pl-0 pt-5">
                            <div class="col-lg-6 text-center pr-0 pl-0 d-none d-sm-block">
                                <h1 class="MuliExtraBold pt-100">
                                Good for your skin.
                            </h1>
                                <h4 class="MultiPlain lh-40px w-500px">
                                Our shave cream cushions and conditions your<br>facial hair for a gentler, smoother shave.<br>Enriched with anti-inflammatory ingredients to<br>help reduce post-shave redness.
                            </h4>
                            </div>
                            <div class="col-lg-6 pr-0 pl-0 d-none d-sm-block pb-5">
                                <img src="" class="w-130" />
                            </div>
                        </div>
                    </div>

                    <!-- Section: ONGOING PRODUCTS -->
                    <div id="collapse2" class="row panel-collapse collapse">
                        <div class="d-none d-sm-block">
                            <div class="row pb-5">
                                <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                                    <h1 class="mt-5 MuliExtraBold trial-kit-title step1-title">
                                    Choose your next refill
                                </h1>
                                </div>
                                <div class="col-md-12 text-center pl-0 pr-0">
                                    <h4 class="mt-3 MuliPlain">
                                    First refil ships <b>next_refill_date(not charged today).</b> Cancel anytime.
                                </h4>
                                </div>
                            </div>

                            <!-- No addons -->
                            <div class="row">
                                <div class='col-md-4'>
                                    <div class="refill-0 text-center pt-5 pb-5 item-unselected">

                                        <h5 class="refill-0-title Muliplain">
                                        :blade_count: Blade Cartridge Pack
                                    </h5>
                                        <h4 class="refill-0-price MuliExtraBold">
                                        :price:
                                    </h4>
                                        <img src="" class="refill-0-image w-30" />
                                        <h6 class="MuliPlain pt-3">
                                        Leaves you feeling refreshed and<br>smelling good.
                                    </h6>
                                    </div>
                                </div>
                                <!-- Plus shave cream -->
                                <div class='col-md-4'>
                                    <div class="refill-1 text-center pt-5 pb-5 item-unselected">
                                        <h5 class="refill-1-title Muliplain">
                                        :blade_count: Blade, 1 Shave Cream
                                    </h5>
                                        <h4 class="refill-1-price MuliExtraBold">
                                        :price:
                                    </h4>
                                        <img src="" class="refill-1-image w-34" />
                                        <h6 class="MuliPlain pt-2">
                                        Helps make every shave<br>smoother and more comfortable.
                                    </h6>
                                    </div>
                                </div>
                                <div class='col-md-4'>
                                    <!-- Plus after shave cream -->
                                    <div class="refill-2 text-center pt-4 item-unselected" style="padding-bottom: 47px">
                                        <h5 class="refill-2-title Muliplain">
                                        :blade_count: Blade, 1 Shave Cream <br> 1 After Shave Cream
                                    </h5>
                                        <h4 class="refill-2-price MuliExtraBold">
                                        :price:
                                    </h4>
                                        <img src="" class="refill-2-image" style="width: 48%" />
                                        <h6 class="MuliPlain pt-3">
                                        Leaves you feeling refreshed and<br>smelling good.
                                    </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center pt-5 pb-5">
                                <button class="button-next button-proceed-checkout MuliExtraBold" style="width: 30%" type="submit">NEXT STEP</button>
                            </div>
                        </div>

                        <div class="d-block d-sm-none">
                            <div class="row pb-5">
                                <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                                    <h1 class="mt-5 MuliExtraBold trial-kit-title step1-title">
                                    Choose your next refill
                                </h1>
                                </div>
                                <div class="col-md-12 text-center pl-0 pr-0">
                                    <h4 class="mt-3 MuliPlain">
                                    First refil ships <br><b>next_refill_date(not charged today).</b><br>Cancel anytime.
                                </h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class='col-md-12'>
                                    <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                                        <div class="row">
                                            <div class="col-6">
                                                <h5 class="refill-0-title Muliplain">
                                                :blade_count: Blade Cartridge Pack
                                            </h5>
                                                <h4 class="refill-0-price MuliExtraBold">
                                                :price:
                                            </h4>
                                                <h5 class="Muliplain pt-3">
                                                Makes you feeling refreshed and smelling good
                                            </h5>
                                            </div>
                                            <div class="col-6">
                                                <img src="" class="refill-0-image w-60 pb-10 pl-30" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Plus shave cream -->
                                <div class='col-md-12'>
                                    <div class="refill-1 m-2 pl-2 pt-2 item-unselected">
                                        <div class="row">
                                            <div class="col-6">
                                                <h5 class="refill-1-title Muliplain">
                                                :blade_count: Blade Pack, 1 Shave Cream
                                            </h5>
                                                <h4 class="refill-1-price MuliExtraBold">
                                                :price:
                                            </h4>
                                                <h4 class="mt-3 MuliPlain">
                                                First refil ships <br><b>next_refill_date(not charged today).</b><br>Cancel anytime.
                                            </h4>
                                            </div>
                                            <div class="col-6">
                                                <img src="" class="refill-1-image w-60 ml-4" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-md-12' style="padding-bottom:20vw">
                                    <!-- Plus after shave cream -->
                                    <div class="refill-2 m-2 pl-2 pt-2 item-unselected">
                                        <div class="row">
                                            <div class="col-6">
                                                <h5 class="refill-2-title Muliplain">
                                                :blade_count: Blade Pack, 1 Shave Cream, 1 After Shave Cream
                                            </h5>
                                                <h4 class="refill-2-price MuliExtraBold">
                                                :price:
                                            </h4>
                                                <h5 class="Muliplain pt-3">
                                            Leaves you feeling refreshed and smelling good
                                            </h5>
                                            </div>
                                            <div class="col-6">
                                                <img src="" class="refill-2-image w-100 pr-3 pb-2" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row d-block d-sm-none m-auto">
                                <div id="mobile-button" class="col-12 text-center pt-5 pl-0 pr-0">
                                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">NEXT STEP</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Section: SHIPPING FREQUENCY -->
                    <div id="collapse3" class="row panel-collapse collapse">
                        <div class="row row-center">
                            <div class="row pb-5">
                                <div class="col-md-12 text-center pl-0 pr-0">
                                    <h1 class="mt-5 MuliExtraBold trial-kit-title">
                                    How often do you shave?
                                </h1>
                                </div>
                                <div class="col-md-12 text-center pl-1 pr-1">
                                    <h5 class="mt-3 MuliPlain pl-3 pr-3">
                                        We'll send your ongoing shipments based on how often you shave. It's easy to change your shipping frequency at any time.
                                    </h5>
                                </div>
                            </div>

                            <div class="col-md-3 mb-3 d-none d-sm-block">
                                <div class="frequency-frequency['duration']text-center pt-4 item-unselected">
                                    <img src="" class="img-fluid" />
                                    <h4 class="MuliExtraBold pt-4">

                                    </h4>
                                    <h6 class="frequency-frequency['duration']-details MuliPlain pt-4 pb-3 frequency-p">

                                    </h6>
                                </div>
                            </div>

                            <div class="col-md-12 d-block d-sm-none" style="padding-bottom: 20vw;">
                                <div class="col-12 mb-3">
                                    <div class="row frequency-frequency['duration']text-center pt-4 item-unselected">
                                        <div class="col-6">
                                            <h4 class="MuliExtraBold pt-4">
                                        </h4>
                                            <h5 class="frequency-frequency['duration']-details MuliPlain pt-4 frequency-p">
                                        </h5>
                                        </div>
                                        <div class="col-6">
                                            <img src="" class="img-fluid pb-3" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mobile-button" class="col-12 pl-0 pr-0">
                                        <form id="form-proceed-checkout" method="GET" action="">
                                            <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-center text-uppercase" type="submit">Review Order</button>
                                        </form>
                                    </div>
                                </div>

                                <!-- <div  id="mobile-button" class="col-12 text-center pt-5 pb-5">
                                <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">NEXT STEP</button>
                            </div> -->
                            </div>

                            <div class="col px-0 text-center pt-5 pb-5 d-none d-sm-block">
                                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                                <form id="form-proceed-checkout" method="GET" action="">
                                    <button id="button-proceed-checkout" class="button-proceed-checkout MuliExtraBold" type="button" style="width: 30%">REVIEW ORDER</button>
                                </form>
                            </div>

                        </div>
                    </div>

                <!-- Modal: Notification trial plan exist -->
                <div class="modal fade" id="notification_trial_plan_exist" tabindex="-1" role="dialog" aria-labelledby="notification_trial_plan_exist" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header modal-header-trial-plan-exist">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <div class="modal-body p-5" style="padding-top: 0 !important">
                                <div class="col-12 text-center">
                                <h5 class="modal-trial-plan-text">Oops. It seems like you’ve already signed up for a Starter Kit. Click <a href="" class="modal-trial-plan-text-a">here<a> to check on your subscription status.</h5>
                                <br>
                                <h5 class="modal-trial-plan-text">If you’ve already cancelled your shave plan but would like to restart, click <a href="" class="modal-trial-plan-text-a">here<a> to customize your new shave plan.</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</div>
<script type="text/javascript">
    let handle_products = null;
    let blade_products = null;
    let frequency_list = null;
    let addon_products = null;
    let free_shave_cream_id = null;
    let allplan = null;
    let country_id = null;
    let currency = null;
    let trial_price = null;
    let trial_saving_price = null;
    let next_refill_date = null;
    let user_id = null;
</script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
<script src="{{ asset('js/functions/trial-plan/trial-plan-selection.function.js') }}"></script>
<script src="{{ asset('js/functions/rebates/referral.js') }}"></script>
@endsection

<!-- Authenticate Sellers -->
