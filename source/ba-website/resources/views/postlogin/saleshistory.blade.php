@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/salesHistory/salesHistory.css') }}">

<div class="container text-center">
       <h1 style="font-size: 25px;margin: 5% 0;"><b>@lang('website_contents.global.header.sales_history')</b></h1>
</div>
@if(!empty($orders))
@php($orderService = new \App\Services\OrderService )
@foreach($orders as $key => $ods)
    <section style="background-color:grey">
            <div class="container">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <h4 style="color: white;font-size: 16px;margin: 4% 0;"><b>{{ substr(\Carbon\Carbon::parse($key)->format('r'), 0, strpos(\Carbon\Carbon::parse($key)->format('r'),'00:00:00')) }}</b></h4>
                    </div>
                </div>
            </div>
    </section>
    @foreach($ods as $od)
    <div class="background-lightGrey">
        <section class="pt-4 p-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 px-3 pb-3 border-bottom">
                        <div class="row padd30">
                            <div class="col-8 col-lg-9">
                                <p class="fs-16 mb-1"><strong>@lang('website_contents.authenticated.saleshistory.order_no', ['orderNo' => $orderService->formatOrderNumber($od, session()->get('currentCountry'), false)])</strong></p>
                                <p class="fs-16 mb-1">{{ $od->email }}</p>
                                <p class="fs-16 mb-1">{{ $od->subscriptionIds !== null ? ( $od->isTrial === 1 ? app('translator')->getFromJson('website_contents.authenticated.products.starter_kit') : ( $od->isCustom === 1 ? app('translator')->getFromJson('website_contents.authenticated.products.shave_plans') : '')) : app('translator')->getFromJson('website_contents.authenticated.products.products') }}</p>
                                <p class="fs-16 mb-1">{{ $od->isDirectTrial === true ? app('translator')->getFromJson('website_contents.authenticated.saleshistory.direct_delivery') : '' }}</p>
                            </div>
                            <div class="col-4 col-lg-3">
                                @if($od->status === 'Pending' || $od->status === 'On Hold' || $od->status === 'Unrealized' || $od->status === 'Payment Failure' || $od->status === 'Cancelled')
                                <div class="status-onhold status-tag col-12 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                    @if ($od->status === 'Pending')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.pending')</span>
                                    @elseif ($od->status === 'On Hold')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.on_hold')</span>
                                    @elseif ($od->status === 'Unrealized')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.unrealized')</span>
                                    @elseif ($od->status === 'Payment Failure')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.payment_failure')</span>
                                    @elseif ($od->status === 'Cancelled')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.cancelled')</span>
                                    @endif
                                </div>
                                @elseif($od->status === 'Processing' || $od->status === 'Delivering' || $od->status === 'Completed' || $od->status === 'Payment Received')
                                <div class="status-active status-tag col-12 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                    @if ($od->status === 'Processing')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.processing')</span>
                                    @elseif ($od->status === 'Delivering')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.delivering')</span>
                                    @elseif ($od->status === 'Completed')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.completed')</span>
                                    @elseif ($od->status === 'Payment Received')
                                        <span class="mx-auto" style="font-size:12px;text-transform:uppercase;">@lang('website_contents.authenticated.saleshistory.payment_received')</span>
                                    @endif
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endforeach
@endforeach
@else
<section style="background-color:grey">
        <div class="container">
            <div class="col-12">
                <div class="row justify-content-center pt-2">
                    <h4 style="color: white;"><b>{{ \Carbon\Carbon::now()->format('Y-m-d') }}</b></h4>
                </div>
            </div>
        </div>
        <div class="background-lightGrey">
                <section class="pt-5 p-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 px-5 border-bottom">
                                <div class="row padd30">
                                    <div class="col-7 col-lg-9">
                                        <p class="fs-16 mb-0">@lang('website_contents.authenticated.saleshistory.no_orders')</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
</section>
@endif
{{--
<section class="orderhistory-load-more-mobile d-lg-none bg-orange">
        <button id="orderhistory-load-more-btn" class="btn-load-section paddTB20">
            Load More
        </button>
</section> --}}

@endsection
