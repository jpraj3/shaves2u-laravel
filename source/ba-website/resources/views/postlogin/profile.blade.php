@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/profile/profile.css') }}">

<div class="container text-center">
       <h1 style="font-size: 25px;margin: 5% 0;"><b>@lang('website_contents.global.header.profile')</b></h1>
</div>

<div class="background-lightGrey">
    <section class="dashboard-content mb-5 pt-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 padd30">
                    <div class="rounded-10 bg-white shadow padd20">

                        <div class="row justify-content-center">
                            <div class="col-12">
                                <p class="fs-20 text-uppercase"><b>@lang('website_contents.authenticated.profile.sales_today')</b></h4>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="row padd15">
                                    <div class="col-6">
                                        <p class="fs-18 mb-0">@lang('website_contents.authenticated.products.starter_kit')</p>
                                    </div>
                                    <div class="col-6 text-right color-orange">
                                        <p class="fs-18 mb-0"><b>{{ $total_trial_kits_sold }}</b></p>
                                    </div>
                                    <div class="col-6">
                                        <p class="fs-18 mb-0">@lang('website_contents.authenticated.products.shave_plans')</p>
                                    </div>
                                    <div class="col-6 text-right color-orange">
                                        <p class="fs-18 mb-0"><b>{{ $total_custom_plans_sold }}</b></p>
                                    </div>
                                    <div class="col-6">
                                        <p class="fs-18 mb-0">@lang('website_contents.authenticated.products.products')</p>
                                    </div>
                                    <div class="col-6 text-right color-orange">
                                        <p class="fs-18 mb-0"><b>{{ $total_ala_carte_orders }}</b></p>
                                    </div>
                                </div>
                                <div class="row padd15" style="margin-top: 5%;">
                                    <div class="col-12 text-center text-uppercase">
                                        <a style="font-size: 16px !important;font-weight: bolder;padding: 10px 10px !important;" class="btn btn-load-more w-100" href="{{ route('ba.locale.saleshistory', ['langCode' => strtolower(session()->get('currentCountry')->urlLang), 'countryCode' => strtolower(session()->get('currentCountry')->codeIso)]) }}"><b>@lang('website_contents.authenticated.profile.view_sales')</b></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard-content mb-5" hidden>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 padd30">
                    <div class="rounded-10 bg-white shadow padd20">

                        <div class="row justify-content-center">
                            <div class="col-10">
                                <p class="fs-20 text-uppercase"><b>@lang('website_contents.authenticated.profile.unique_code')</b></h4>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <div class="row padd15">
                                    <div class="col-12">
                                        <p class="fs-20 mb-0">@lang('website_contents.authenticated.profile.scan_qr')</p>
                                    </div>
                                    <div class="col-12 text-center">
                                        <img style="width: 40%" src="{{ asset('images/common/qr-code.png') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard-content mb-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 padd30">
                    <div class="rounded-10 bg-white shadow padd20">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <p class="fs-20 text-uppercase"><b>@lang('website_contents.authenticated.profile.personal_details')</b></h4>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                                    <label class="fs-16 mb-0 fs-14-sm color-grey">@lang('website_contents.authenticated.profile.name')</label>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 border-bottom pb-1 fs-14-sm">
                                    <span class="fs-16 mb-0 fs-14-sm">{{ $seller->agentName }}</span>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                                    <label class="fs-16 mb-0 fs-14-sm color-grey">@lang('website_contents.authenticated.profile.email')</label>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 border-bottom fs-14-sm">
                                    <span class="fs-16 mb-0 fs-14-sm ">{{ $seller->email }}</span>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                                    <label class="fs-16 mb-0 fs-14-sm color-grey">@lang('website_contents.authenticated.profile.id')</label>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 border-bottom fs-14-sm">
                                    <span class="fs-16 mb-0 fs-14-sm ">{{ $seller->badgeId }}</span>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                                    <label class="fs-16 mb-0 fs-14-sm color-grey">@lang('website_contents.authenticated.profile.organisation')</label>
                                </div>
                                <div class="col-lg-12 pl-0 pr-0 pb-1 border-bottom fs-14-sm">
                                    <span class="fs-16 mb-0 fs-14-sm ">{{ $marketing_office_info->organization }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

<!-- Authenticate Sellers -->
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
