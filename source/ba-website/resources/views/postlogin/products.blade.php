@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/gender/selection.css') }}">
<div class="container text-center">
       <h1 style="margin: 10% 10%;font-size: 25px;font-weight: 700;"><b>@lang('website_contents.authenticated.products.select_gender')</b></h1>
</div>

<div class="container pt-3">
    <div class="col-12 d-inline-flex text-center vh-50 mx-auto" id="productCategories">
        @if(session()->get('currentCountry')->id === 1)
        <div class="col-6 mx-auto p-0" id="pg_male">
            <div class="col-12 mx-auto text-right p-0">
                @if(strtolower(session()->get('currentCountry')->defaultLang) == "zh-hk")
                <img onClick="onGenderSelect('pg_male')" src="{{ asset('images/common/gender/btn-male-chi.png') }}" class="gender-img pointer" />
                @else
                <img onClick="onGenderSelect('pg_male')" src="{{ asset('images/common/gender/btn-male.png') }}" class="gender-img pointer" />
                @endif
            </div>
        </div>
        <div class="col-6 mx-auto p-0" id="pg_female" style="margin:auto;">
            <div class="col-12 mx-auto text-left p-0">
            @if(strtolower(session()->get('currentCountry')->defaultLang) == "zh-hk")
            <img onClick="onGenderSelect('pg_female')" src="{{ asset('images/common/gender/btn-female-chi.png') }}" class="gender-img pointer" />
                @else
                <img onClick="onGenderSelect('pg_female')"  src="{{ asset('images/common/gender/btn-female.png') }}" class="gender-img pointer" />
                @endif
            </div>
        </div>
        @else
        <div class="col-12 mx-auto p-0" id="pg_male">
                <div class="col-12 mx-auto p-0">
                    @if(strtolower(session()->get('currentCountry')->defaultLang) == "zh-hk")
                    <img onClick="onGenderSelect('pg_male')" src="{{ asset('images/common/gender/btn-male-chi.png') }}" class="gender-img pointer" />
                    @else
                    <img onClick="onGenderSelect('pg_male')" src="{{ asset('images/common/gender/btn-male.png') }}" class="gender-img pointer" />
                    @endif
                </div>
            </div>
        @endif
    </div>
    <form id="pg_form">
        <input type="hidden" name="pg_select" id="pg_select" value="" />
    </form>
</div>
<!-- Authenticate Sellers -->
<script>let country_ = {!! json_encode(session()->get('currentCountry'))!!};</script>
<script>
session(SESSION_USER_INFO, SESSION_CLEAR, null).done(
    function() {
        //set items to storage
        window.localStorage.removeItem('current_user');
    }
);
</script>
<script src="{{ asset('js/api/products/products.function.js') }}"></script>
@endsection
