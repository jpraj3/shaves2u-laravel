@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('css/authentication/login.css') }}">
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <div class="row justify-content-center text-weight-bold">
                <h1><b>@lang('website_contents.login.welcome')</b></h1>
            </div>
            <div class="row justify-content-center">
                <p>@lang('website_contents.login.enter_details')</p>
            </div>
            <form id="ba_login_form">
            @csrf
                <div class="form-group row">
                    <label for="badgeId" class="col-md-12 col-form-label text-md-left text-weight-bold"><b>@lang('website_contents.login.badge_id')</b></label>

                    <div class="col-md-12">
                        <input id="login_email" type="text" class="form-control" name="badgeId" value="" required>
                    </div>
                    <div id="badgeId_error" class="hasError col-12 col-form-label text-left"></div>
                </div>
                <div class="form-group row">
                    <label for="icNumber" class="col-md-12 col-form-label text-md-left"><b>@lang('website_contents.login.password')</b></label>

                    <div class="col-md-12">
                        <input id="login_password" type="password" class="form-control" name="icNumber" value=""  required>
                    </div>
                    <div id="icNumber_error" class="hasError col-12 col-form-label text-left"></div>
                    <div id="login_data_errorget" class="hidden">@lang('website_contents.login.invaliddata')</div>
                    <div id="login_data_error" class="error hasError col-12 col-form-label text-left"></div>
                    <div id="login_data_errorget" class="d-none error hasError col-12 col-form-label text-left">@lang('website_contents.login.invaliddata')</div>
                </div>
               
                <div class="form-group row pt-3">
                    <div class="col-md-12">
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <button onClick="login_btn()" type="button" class="btn login-button w-100">
                                    @lang('website_contents.login.login')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/helpers/ba_form_validations.js') }}"></script>
<script>
    function validateOnLoadLogin() {
        validateBALogin("ba_login_form");
        let _login_btn = document.getElementById('_login_btn');
        if ($('#ba_login_form').valid() === true) {
            // console.log("pass validation");
            _login_btn.removeAttribute("disabled");
            _login_valid = true;
        } else {
            // console.log("fail validation");
            _login_btn.setAttribute("disabled", null);
            _login_valid = false;
        }
    }

    function login_btn() {
        validateBALogin("ba_login_form");
        if ($('#ba_login_form').valid() === true) { // Calling validation function
            authenticate(); //form submission
            _login_valid = true;
        }else{
            _login_valid = false;
        }
    }
</script>
@endsection