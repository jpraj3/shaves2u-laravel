<?php
$a = dirname(__FILE__);
$b = realpath($a);
$c = explode('\\', $b);
$d = end($c);
$DIR_NAME = $d;
$API_KEY_PATH = config('app.env') !== $DIR_NAME ? '' : 'D:/laragon/www/dev/s2u-laravel-config/config/' . config('app.appType') . '/config.' . config('app.env') . '.json';
$GET_CONFIG_CONTENTS = $API_KEY_PATH === '' ? '' : json_decode(file_get_contents($API_KEY_PATH), true);
return [
    'webUrl' => 'http://localhost:8000/',
    'apiUrl' => 'http://192.168.2.236:99/api',
    'emailUrl' => 'http://192.168.2.236:99/',
    'CUSTOM_CONFIG_PATH' => $API_KEY_PATH,
    'CUSTOM_CONFIG_DATA' => $GET_CONFIG_CONTENTS,
    'IpStack' => [
        'accesskey' => '2c878ac336a0a28ab63caed03dc92cc1',
        'url' => 'http://api.ipstack.com/',
        'example' => 'http://api.ipstack.com/175.138.185.1?access_key=2c878ac336a0a28ab63caed03dc92cc1',
    ],
    'aws' => [
        'settings' => [
            'keyPath' => 'D:/laragon/www/dev/s2u-laravel-config/config/aws-key.json',
            'userAvatarAlbum' => 'user-avatars',
            'version' => '2012-10-17',
            'bucketName' => 'shaves2u-dci',
            'testUpload' => 'shaves2u-laravel-test',
            'planPanelAlbum' => 'plan-panel',
            'productImagesAlbum' => 'product-images',
            'planImagesAlbum' => 'plan-images',
            'promotionImagesAlbum' => 'promotion-images',
            'filesUploadAlbum' => 'files-upload-local',
            'csvReportsUpload' => 'csv-reports-upload-local',
            'localDownloadFolder' => 'D:/laragon/www/dev/s2u-laravel-config/config/aws-output-files',
            'localCSVReportFolder' => 'D:/laragon/www/dev/s2u-laravel-config/config/aws-output-files/csv-report-files',
            'limitDownload' => 2000,
        ],
    ],

    'taxInvoice' => [
        'taxInvoicePath' => 'D:/laragon/www/dev/s2u-laravel-config/config/taxInvoice-upload-files',
    ],

    'warehouse-interface' => [
        'folderName' => 'warehouse-interface-',
        'orderCancel' => 'order-cancel-outbound',
        'orderConfirmation' => 'order-confirmation-outbound',
        'orderInbound' => 'order-inbound-outbound',
    ],

    'warehouse_paths' => [
        'MYS' => [
            'local_upload_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-my-upload-files',
            'local_download_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-my-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-my-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-my-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'HKG' => [
            'local_upload_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-hk-upload-files',
            'local_download_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-hk-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-hk-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-hk-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'SGP' => [
            'local_upload_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/urbanfox-upload-files',
            'local_download_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/urbanfox-output-files',
        ],
        'KOR' => [
            'local_upload_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-kr-upload-files',
            'local_download_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-kr-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-kr-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-kr-ftp-test/inbox/new-site-test',
            'pre_convert' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-kr-output-files/pre_convert',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'TWN' => [
            'local_upload_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-tw-upload-files',
            'local_download_folder' => 'D:/laragon/www/dev/s2u-laravel-config/config/warehouse-tw-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-tw-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-tw-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
    ],

    'aftership' => [
        'trackingUrl' => 'https://s2ustaging.aftership.com',
    ],

    'urbanFox' => [
        'host' => 'https://ims.urbanfox.asia/graphiql',
        'trackingUrl' => 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number=',
    ],

    'deliverDateForKorea' => '2018-10-01',

    'trialPlan' => [
        'baFirstDelivery' => '13 days',
        'webFirstDelivery' => '20 days',
        'startFirstDelivery' => '13 days',
        'followUpEmail1' => '10 days',
        'followUpEmail2' => '7 days',
        'followUpEmail5Days' => '5 days',
    ],
];
