<?php
return [
    'default' => [
        'country' => [
            'id' => 1,
            'code' => 'MYS',
            'codeIso' => 'MY',
            'defaultLang' => 'en',
            'urlLang' => 'en',
        ],
    ],
    'all' => [
        'email-type' => [
            'subscription_recharge_email_report',
            'subscription_recharge_nicepay_email_report',
            'password_reset',
            'password_updated',
            'receipt_order_confirmation',
            'receipt_order_delivery',
            'receipt_order_completed',
            'receipt_subscription_payment_failure',
            'referral_active_credits',
            'referral_email_invite',
            'referral_inactive_credits',
            'referral_subscription_invite',
            'reminder_promo_3day',
            'reminder_promo_7day',
            'subscription_cancellation',
            'subscription_modified',
            'subscription_renewal',
            'subscription_renewal_annual',
            'order_cancellation',
            'order_cancelled',
            'welcome',
            'welcome_ask',
            'card_expiry',
            'reactivate',
            'goodbye',
            'ordershipped'
        ],
    ]
    ]
?>