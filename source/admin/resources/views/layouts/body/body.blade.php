
<div class="row">
    @if(Auth::check())
        <div class="col-2 pr-0">
            <div class="col-12">
                <nav id="sidebar">
                    <ul class="list-unstyled components">
                        <li>
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.orders') }}">Orders</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.bulkorders') }}">Bulk Orders</a>
                        </li>
                        <li>
                            <a href="#subscriptionsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Subscriptions</a>
                            <ul class="collapse list-unstyled" id="subscriptionsSubmenu">
                                <li>
                                    <a href="#">Subscribers</a>
                                </li>
                                <li>
                                    <a href="#">Recharge</a>
                                </li>
                                <li>
                                    <a href="#">Cancellation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Referral</a>
                        </li>
                        <li>
                            <a href="#salesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Sales Report</a>
                            <ul class="collapse list-unstyled" id="usersSubmenu">
                                <li>
                                    <a href="#">Sales Report</a>
                                </li>
                                <li>
                                    <a href="#">Appco Sales Report</a>
                                </li>
                                <li>
                                    <a href="#">Marketing Office Sales Report</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Promotion</a>
                        </li>
                        <li>
                            <a href="#">Product</a>
                        </li>
                        <li>
                            <a href="#">Plan</a>
                        </li>
                        <li>
                            <a href="#">Countries</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-10 pl-0">
            @yield('content')
        </div>
    @else
        <div class="col-12">
            @yield('content')
        </div>
    @endif
</div>