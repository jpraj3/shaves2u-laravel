
<style>
    .chartjs-size-monitor {
        position: absolute;
        left: 0px;
        top: 0px;
        right: 0px;
        bottom: 0px;
        overflow: hidden;
        pointer-events: none;
        visibility: hidden;
        z-index: -1;
    }

    .chartjs-size-monitor-expand {
        position:absolute;
        left:0;
        top:0;
        right:0;
        bottom:0;
        overflow:hidden;
        pointer-events:none;
        visibility:hidden;
        z-index:-1;
    }

    .chartjs-size-monitor-shrink {
        position:absolute;
        left:0;
        top:0;
        right:0;
        bottom:0;
        overflow:hidden;
        pointer-events:none;
        visibility:hidden;
        z-index:-1;
    }

    .chartjs-render-monitor {
        display: block;
        width: 1247px;
        height: 400px;
    }

    .revenue-chart {
        position: relative;
        padding: 15px;
    }

    .revenue-chart .price {
        position: absolute;
        top: -12px;
        left: 38px;
    }

    .revenue-chart .date {
        position: absolute;
        bottom: 0px;
        right: 46px;
    }

    div.dataTables_wrapper {
        width: 100% !important;
    }

    div.dataTables_wrapper > div {
        width: 100% !important;
    }

    .error {
        color: red;
    }
</style>

<link rel="stylesheet" href="{{ asset('plugins/one-ui/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/one-ui/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/one-ui/plugins/datatables/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/one-ui/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">
