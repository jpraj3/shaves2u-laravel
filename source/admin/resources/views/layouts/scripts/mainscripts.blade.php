<!-- Page JS Plugins -->
<script src="{{ asset('plugins/one-ui/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('plugins/one-ui/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- Page JS Helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Inputs + Ion Range Slider plugins) -->
<script>jQuery(function(){ One.helpers(['datepicker','select2']); });</script>
<script>
    $("#filterByDateTo").datepicker().datepicker('setDate', "0d");
    $("#filterByDateFrom").datepicker().datepicker('setDate', "-1d");
</script>
<script>
    all_sessions = @php echo json_encode(session()->all()) @endphp;
    console.log(all_sessions);
</script>
<script>
    jQuery(function(){
        $(".js-datepicker-enabled input").keydown(function () {
            return false;
        });

        // Init DataTable with Buttons
        $('.js-dataTable-buttons').dataTable({
            pageLength: 50,
            lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
            autoWidth: false,
            searching: false,
            dom: "<l<t>ip>",
            order: [],
        });
    });
</script>
<script>
    let appType = "{!! config('app.appType') !!}";
    let API_URL = "{!! config('environment.apiUrl') !!}";
    let GLOBAL_URL = "{{ url('/') }}";
</script>
<script>
    function showLoading() {
        jQuery("#loading").css("display", "block");
    };

    function hideLoading() {
        jQuery("#loading").css("display", "none");
    };
</script>
<script src="{{ asset('js/functions/global/export-report.function.js') }}"></script>
<script src="{{ asset('js/functions/global/export-tax-invoice.function.js') }}"></script>
