<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <meta name="description" content="OneUI - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/common/logo/logo-mobile.svg') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href="{{ asset('plugins/one-ui/css/oneui.css') }}">
        <!-- sweetalert2 popup -->
        <link rel="stylesheet" href="{{ asset('plugins/sweetalert2.v8/sweetalert2.min.css') }}">
        <!-- wow animation -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <!-- Toastr Notification Plugin -->
        <link rel="stylesheet" href="{{ asset('plugins/toastr-v2.1.4/toastr.min.css')}}"/>
        <script src="{{ asset('plugins/toastr-v2.1.4/toastr.min.js') }}"></script>   <!-- End Toastr Notification Plugin -->
        <script src="{{ asset('plugins/sweetalert2.v8/sweetalert2.all.min.js') }}"></script>
        <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="{{ asset('plugins/one-ui/css/themes/amethyst.css') }}"> -->

        @include('layouts.styles.styles')
        @yield('css_after')

        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-dark'                              Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Light themed Header
            'page-header-dark'                          Dark themed Header

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->

        @guest

        <!-- Header -->
        <header id="page-header">
            @include('layouts.header.header')
        </header>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            @yield('content')
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        <footer id="page-footer" class="bg-body-light">
            @include('layouts.footer.footer')
        </footer>
        <!-- END Footer -->

        @else
        <div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed">
            <!-- Side Overlay-->
            <aside id="side-overlay" class="font-size-sm">
                <!-- Side Header -->
                <div class="content-header border-bottom">
                    <!-- User Avatar -->
                    <a class="img-link mr-1" href="javascript:void(0)">
                        <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar10.jpg') }}" alt="">
                    </a>
                    <!-- END User Avatar -->

                    <!-- User Info -->
                    <div class="ml-2">
                        <a class="link-fx text-dark font-w600" href="javascript:void(0)">{{ Auth::user()->firstName }} {{ Auth::user()->lastName }}</a>
                    </div>
                    <!-- END User Info -->

                    <!-- Close Side Overlay -->
                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                    <a class="ml-auto btn btn-sm btn-dual" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_close">
                        <i class="fa fa-fw fa-times text-danger"></i>
                    </a>
                    <!-- END Close Side Overlay -->
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="content-side">
                    <p>
                        Content..
                    </p>
                </div>
                <!-- END Side Content -->
            </aside>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Sidebar Mini Mode - Display Helper classes

                Adding 'smini-hide' class to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding 'smini-show' class to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition animation, make sure to also add the 'no-transition' class to your element

                Adding 'smini-hidden' to an element will hide it when the sidebar is in mini mode
                Adding 'smini-visible' to an element will show it (display: inline-block) only when the sidebar is in mini mode
                Adding 'smini-visible-block' to an element will show it (display: block) only when the sidebar is in mini mode
            -->
            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header -->
                <div class="content-header bg-white-5">
                    <!-- Logo -->
                    <a class="font-w600 text-dual" href="/">
                        <i class="fa fa-circle-notch text-primary"></i>
                        <span class="smini-hide">
                            <span class="font-w700 font-size-h5">{{ config('app.name', 'Laravel') }}</span>
                        </span>
                    </a>
                    <!-- END Logo -->

                    <!-- Options -->
                    <div>
                        <!-- Color Variations -->
                        <div class="dropdown d-inline-block ml-3">
                            <a class="text-dual font-size-sm" id="sidebar-themes-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="si si-drop"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right font-size-sm smini-hide border-0" aria-labelledby="sidebar-themes-dropdown">
                                <!-- Color Themes -->
                                <!-- Layout API, functionality initialized in Template._uiHandleTheme() -->
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="default" href="#">
                                    <span>Default</span>
                                    <i class="fa fa-circle text-default"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="{{ asset('css/themes/amethyst.css') }}" href="#">
                                    <span>Amethyst</span>
                                    <i class="fa fa-circle text-amethyst"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="{{ asset('css/themes/city.css') }}" href="#">
                                    <span>City</span>
                                    <i class="fa fa-circle text-city"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="{{ asset('css/themes/flat.css') }}" href="#">
                                    <span>Flat</span>
                                    <i class="fa fa-circle text-flat"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="{{ asset('css/themes/modern.css') }}" href="#">
                                    <span>Modern</span>
                                    <i class="fa fa-circle text-modern"></i>
                                </a>
                                <a class="dropdown-item d-flex align-items-center justify-content-between" data-toggle="theme" data-theme="{{ asset('css/themes/smooth.css') }}" href="#">
                                    <span>Smooth</span>
                                    <i class="fa fa-circle text-smooth"></i>
                                </a>
                                <!-- END Color Themes -->

                                <div class="dropdown-divider"></div>

                                <!-- Sidebar Styles -->
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <a class="dropdown-item" data-toggle="layout" data-action="sidebar_style_light" href="#">
                                    <span>Sidebar Light</span>
                                </a>
                                <a class="dropdown-item" data-toggle="layout" data-action="sidebar_style_dark" href="#">
                                    <span>Sidebar Dark</span>
                                </a>
                                <!-- Sidebar Styles -->

                                <div class="dropdown-divider"></div>

                                <!-- Header Styles -->
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <a class="dropdown-item" data-toggle="layout" data-action="header_style_light" href="#">
                                    <span>Header Light</span>
                                </a>
                                <a class="dropdown-item" data-toggle="layout" data-action="header_style_dark" href="#">
                                    <span>Header Dark</span>
                                </a>
                                <!-- Header Styles -->
                            </div>
                        </div>
                        <!-- END Themes -->

                        <!-- Close Sidebar, Visible only on mobile screens -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="d-lg-none text-dual ml-3" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                            <i class="fa fa-times"></i>
                        </a>
                        <!-- END Close Sidebar -->
                    </div>
                    <!-- END Options -->
                </div>
                <!-- END Side Header -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.dashboard') ? ' active' : '' }}" href="{{ route('admin.dashboard') }}">
                                <i class="nav-main-link-icon si si-speedometer"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>
                        @canView('orders')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.orders*') ? ' active' : '' }}" href="{{ route('admin.orders') }}">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Orders</span>
                            </a>
                        </li>
                        @endcanView
                        @canView('bulk-orders')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.bulkorders*') ? ' active' : '' }}" href="{{ route('admin.bulkorders') }}">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Bulk Orders</span>
                            </a>
                        </li>
                        @endcanView
                        @permissions(['subscription/subscribers','subscription/recharge','subscription/cancellation','subscription/pauseplan'],'canView')
                        <li class="nav-main-item{{ request()->routeIs('admin.subscription*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Subscription</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @canView('subscription/subscribers')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.subscription.subscribers*') ? ' active' : '' }}" href="{{ route('admin.subscription.subscribers') }}">
                                        <span class="nav-main-link-name">Subscribers</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('subscription/recharge')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.subscription.recharge*') ? ' active' : '' }}" href="{{ route('admin.subscription.recharge') }}">
                                        <span class="nav-main-link-name">Recharge</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('subscription/cancellation')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.subscription.cancellation*') ? ' active' : '' }}" href="{{ route('admin.subscription.cancellation') }}">
                                        <span class="nav-main-link-name">Cancellation</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('subscription/pauseplan')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.subscription.pauseplan*') ? ' active' : '' }}" href="{{ route('admin.subscription.pauseplan') }}">
                                        <span class="nav-main-link-name">Pause Plan</span>
                                    </a>
                                </li>
                                @endcanView
                            </ul>
                        </li>
                        @endpermissions
                        @canView('referral')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.referral*') ? ' active' : '' }}" href="{{ route('admin.referral') }}">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Referral</span>
                            </a>
                        </li>
                        @endcanView
                        @canView('referral/cashout')
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.referral-cash-out*') ? ' active' : '' }}" href="{{ route('admin.referral-cash-out') }}">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Referral Cash-out</span>
                            </a>
                        </li>
                        @endcanView
                        @permissions(['users/customers'],'canView')
                        <li class="nav-main-item{{ request()->is('users/*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Customer Management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @canView('users/customers')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('users/customers') ? ' active' : '' }}" href="{{ route('users.customers') }}">
                                        <span class="nav-main-link-name">Customer List</span>
                                    </a>
                                </li>
                                @endpermissions
                                {{-- <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('users/customer-audit') ? ' active' : '' }}" href="{{ route('users.customers.details') }}">
                                        <span class="nav-main-link-name">Customer Audits</span>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        @endpermissions
                        {{-- <li class="nav-main-heading">Various</li> --}}
                        @permissions(['reports/master','reports/appco','reports/mo'],'canView')
                        <li class="nav-main-item{{ request()->is('reports/*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Sales Report</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @canView('reports/master')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.reports.master*') ? ' active' : '' }}" href="{{ route('admin.reports.master') }}">
                                        <span class="nav-main-link-name">Master Report</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('reports/appco')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.reports.appco*') ? ' active' : '' }}" href="{{ route('admin.reports.appco') }}">
                                        <span class="nav-main-link-name">Appco Sales Report</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('reports/mo')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.reports.mo*') ? ' active' : '' }}" href="{{ route('admin.reports.mo') }}">
                                        <span class="nav-main-link-name">MO Sales Report</span>
                                    </a>
                                </li>
                                @endcanView
                            </ul>
                        </li>
                        @endpermissions
                        {{-- <li class="nav-main-item">
                            <a class="nav-main-link" href="">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Promotion</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Product</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Plan</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Countries</span>
                            </a>
                        </li> --}}
                        {{-- <li class="nav-main-item{{ request()->is('examples/*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Website Content Management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="">
                                        <span class="nav-main-link-name"></span>
                                    </a>
                                </li>
                            </ul>
                        </li> --}}
                        {{-- <li class="nav-main-item{{ request()->is('examples/*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Customer Management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="">
                                        <span class="nav-main-link-name"></span>
                                    </a>
                                </li>
                            </ul>
                        </li> --}}
                        @permissions(['ba/generateMO','ba/generateBA','ba'],'canView')
                        <li class="nav-main-item{{ request()->routeIs('admin.ba*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">BA Management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @canView('ba/generateMO')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.ba.generateMO') ? ' active' : '' }}" href="{{ route('admin.ba.generateMO') }}">
                                        <span class="nav-main-link-name">Generate MO</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('ba/generateBA')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.ba.generateBA') ? ' active' : '' }}" href="{{ route('admin.ba.generateBA') }}"">
                                        <span class="nav-main-link-name">Generate BA</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('ba')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.ba.list') ? ' active' : '' }}" href="{{ route('admin.ba.list') }}">
                                        <span class="nav-main-link-name">BA List</span>
                                    </a>
                                </li>
                                @endcanView
                                {{-- <li class="nav-main-item">
                                    <a class="nav-main-link" href="">
                                        <span class="nav-main-link-name">BA Audit Trail</span>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        @endpermissions
                        @permissions(['admins','admins/roles','admins/paths'],'canView')
                        <li class="nav-main-item{{ request()->routeIs('admin.admins*') ? ' open' : '' }}">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Admin Management</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @canView('admins')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.admins') ? ' active' : '' }}" href="{{ route('admin.admins') }}">
                                        <span class="nav-main-link-name">Admin List</span>
                                    </a>
                                </li>
                                @endcanView
                                @canView('admins/roles')
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.admins.roles*') ? ' active' : '' }}" href="{{ route('admin.admins.roles') }}">
                                        <span class="nav-main-link-name">Admin Roles</span>
                                    </a>
                                </li>
                                @endcanView
                                {{-- Do not show this --}}
                                {{-- <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->routeIs('admin.admins.paths*') ? ' active' : '' }}" href="{{ route('admin.admins.paths') }}">
                                        <span class="nav-main-link-name">Admin Paths</span>
                                    </a>
                                </li> --}}
                                {{-- <li class="nav-main-item">
                                    <a class="nav-main-link" href="">
                                        <span class="nav-main-link-name">Admin Audit Trail</span>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        @endpermissions
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->routeIs('admin.export*') ? ' active' : '' }}" href="{{ route('admin.export') }}">
                                <i class="nav-main-link-icon si si-bars"></i>
                                <span class="nav-main-link-name">Export</span>
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- END Side Navigation -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                @include('layouts.header.header')
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @include('loading.loading')
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light">
                @include('layouts.footer.footer')
            </footer>
            <!-- END Footer -->

        </div>
        <!-- END Page Container -->
        @endguest

        <!-- OneUI Core JS -->
        <script src="{{ asset('plugins/one-ui/oneui.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        <script src="{{ asset('plugins/one-ui/laravel.app.js') }}"></script>

        @include('layouts.scripts.mainscripts')
        @yield('js_after')
        <script src="{{ asset('js/functions/auth/logout.function.js') }}"></script>
    </body>
</html>
