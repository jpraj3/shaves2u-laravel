@extends('layouts.app')

@section('js_after')
<script>
var apiurl = "{!! config('app.ecommerce_url', $_SERVER['REQUEST_URI']) !!}";
var siteurl = "{{ url('/') }}";
</script>

<script src="{{ asset('js/functions/auth/login.function.js') }}"></script>
@endsection

@section('content')
<div class="bg-image" style="background-image: url({{ asset('media/photos/photo6@2x.jpg') }});">
    <div class="hero-static bg-white-95">
        <div class="content">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4"><!-- Sign In Block -->
                    <div class="block block-themed block-fx-shadow mb-0">
                        <div class="block-header">
                            <h3 class="block-title">Sign In</h3>
                            <div class="block-options">
                                {{-- <a class="btn-block-option font-size-sm" href="{{ route('admin.password.forgot') }}">Forgot Password?</a> --}}
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="p-sm-3 px-lg-4 py-lg-5">
                                <h1 class="mb-2">{{ config('app.name', 'Laravel') }}</h1>
                                <p>Welcome, please login.</p>
                            <form id="login_form" class="js-validation-signin" method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="py-3">
                                        <div class="form-group">
                                            <input id="email" type="email" name="email" required="required" autocomplete="current-password" class="form-control form-control-alt form-control-lg" placeholder="Email address">
                                        </div>

                                        <div class="form-group">
                                            <input id="password" type="password" name="password" required="required" autocomplete="current-password" class="form-control form-control-alt form-control-lg" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-xl-5">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
