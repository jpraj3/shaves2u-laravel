@extends('layouts.app')

@php($adminId = Auth::user()->id)
@php($canDownload = $permissionsHelper->check('users/customers','canDownload'))
@php($canEdit = $permissionsHelper->check('users/customers','canEdit'))
@php($canViewDetails = $permissionsHelper->check('users/customers/details','canView'))

{{-- Page Specific CSS --}}
@section('css_before')
@endsection

<script>
    const adminId = {{ $adminId }};
    const canDownload = {{ json_encode($canDownload) }};
    const canEdit = {{ json_encode($canEdit) }};
    const canViewDetails = {{ json_encode($canViewDetails) }};
</script>

@section('content')
    <div class="content content-narrow">
        <div class="row">
            <div class="col-12">
                <div class="block block-rounded block-mode-loading-oneui">
                    <div class="block-header">
                        <h3 class="block-title">Customers</h3>
                        @canDownload('users/customers')
                        <button onClick="exportCSV('customers')" type="button" class="btn csv-download">Download Report</button>
                        @endcanDownload
                    </div>
                    <div hidden class="col-12" id="csv-queue-notification">
                        <p style="color: green; margin-right: 3%; text-align: right; margin-bottom: 0px;">
                            Your export has been queued.
                            <a href="{{ route('admin.export') }}">View progress.</a>
                        </p>
                    </div>
                    <div class="block-content block-content-full">
                        <form>
                           {{-- <div class="row mx-0 mb-3">
                               <div class="col-md-3 offset-md-6">
                                   <label for="">Filter by Channel</label>
                                   <select class="selectpicker form-control filterOrders" name="filterByChannel" id="filterByChannel">
                                       <option value="all">All Channels</option>
                                       <option value="seller">BA</option>
                                       <option value="customer">Online / E-Commerce</option>
                                   </select>
                               </div>
                               <div class="col-md-3">
                                   <label for="">Filter by Country</label>
                                   <select class="selectpicker form-control filterOrders" name="filterByCountry" id="filterByCountry">
                                       <option value="all">All Countries</option>
                                   </select>
                               </div>
                            </div> --}}
                            <div class="row mx-0 mb-3">
                                <div class="col-md-3 filter-field mt-3">
                                    <label for="">Search</label>
                                    <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by Name, Email" type="text">
                                </div>
                                <div class="col-md-3 mt-3">
                                    <label for="">Filter by Country</label>
                                    <select class="selectpicker form-control filterCustomers" name="filterByCountry"
                                            id="filterByCountry">
                                        <option value="all">All Countries</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mt-3">
                                    <label for="">Filter by Status</label>
                                    <select class="selectpicker form-control filterCustomers" name="filterByStatus" id="filterByStatus">
                                        <option value="all">All Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mt-3 form-group">
                                    <label for="filterByDateFrom">Filter by Date</label>
                                    <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                        <input type="text" class="form-control filterCustomers" id="filterByDateFrom" name="filterByDateFrom" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                        <div class="input-group-prepend input-group-append">
                                        <span class="input-group-text font-w600">
                                            <i class="fa fa-fw fa-arrow-right"></i>
                                        </span>
                                        </div>
                                        <input type="text" class="form-control filterCustomers" id="filterByDateTo" name="filterByDateTo" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                            <button type="button" id="start_filter" class="btn btn-primary">Filter</button>
                        </div>
                        </form>
                        <div class="row row-deck">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                                <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Consent</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="customers_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Page Specific JS --}}
@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/functions/users/customerUsers.function.js') }}"></script>
@endsection