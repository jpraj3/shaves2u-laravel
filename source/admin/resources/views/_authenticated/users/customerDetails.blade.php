@extends('layouts.app')

@php($canEdit = $permissionsHelper->check('users/customers/details', 'canEdit'))
@php($canCreate = $permissionsHelper->check('users/customers/details', 'canCreate'))
@php($canViewSubs = $permissionsHelper->check('subscription/subscribers/details', 'canView'))

@section('css_before')
<style>
    .detail-address {
        border: 1px solid #111;
        padding: 20px;
        border-radius: 5px;
        max-width: 400px;
        max-height: 240px;
        margin-bottom: 20px;
    }
    .card-content {
        border: 1px solid #111;
        padding: 20px;
        border-radius: 5px;
        max-width: 350px;
        margin-bottom: 25px;
        max-height: 200px;
    }

    p {
        margin-bottom: 5px !important;
    }

    h6, .btn-sm {
        margin-bottom: 10px !important;
    }
    
    .crud-control {
        margin-right: 10px !important;
    }
</style>
@endsection
<script>
    const canViewSubs = {{ json_encode($canViewSubs) }};
</script>
@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Customer Details<button id="switch_edit_profile" class="btn btn-sm edit-hide"><i class="fa fa-edit"></i></button></h3>
                </div>
                <form id="form_edit_profile" method="POST" action="" onsubmit="return false">
                    <div class="block-content block-content-full">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-2">
                                    <strong>Email Address: </strong>
                                </div>
                                <div class="col-10">
                                    <span id="email" class="edit-hide"></span>
                                    <input required id="customer_email" name="customer_email" type="text" class="form-control edit-show d-none" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-2">
                                    <strong>Name: </strong>
                                </div>
                                <div class="col-10">
                                    <span id="full_name" class="edit-hide"></span>
                                    <input required id="customer_name" name="customer_name" type="text" class="form-control edit-show d-none" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="row">
                            <div class="col-2">
                                <strong>Contact: </strong>
                            </div>
                            <div class="col-10">
                                <span id="phone" class="edit-hide"></span>
                                <input id="customer_phone" name="customer_phone" type="text" class="form-control edit-show d-none" value="" />
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Country: </strong>
                            </div>
                            <div class="col-10">
                                <span id="country"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Consent: </strong>
                            </div>
                            <div class="col-10">
                                <span id="consent"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Channel: </strong>
                            </div>
                            <div class="col-10">
                                <span id="channel"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Subscription ID: </strong>
                            </div>
                            <div class="col-10">
                                <span id="subscriptionId"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Date/Time of Registration: </strong>
                            </div>
                            <div class="col-10">
                                <span id="register_datetime"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Date/Time of Last Login: </strong>
                            </div>
                            <div class="col-10">
                                <span id="last_login"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <strong>Order History: </strong>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-4">
                                        <strong>Date </strong>
                                    </div>
                                    <div class="col-6">
                                        <strong>Description/Status</strong>
                                    </div>
                                </div>
                                <div id="order_log">
                                </div>
                            </div>
                        </div>
                        <div class="row edit-show d-none">
                            <button type="button" id="cancel_edit_profile" class="btn btn-secondary mt-3 ml-3 edit-show">Cancel</button>
                            <button type="submit" id="save_profile" class="btn btn-primary mt-3 ml-3 edit-show">Save</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-6">
                            <h3>Shipping Address</h3>
                            <div id="delivery_address">
                            </div>
                        </div>
                        <div class="col-6">
                            <h3>Billing Address</h3>
                            <div id="billing_address">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="address_list">
                    </div>
                    <div class="row">
                        {{-- <button type="button" class="btn btn-primary mt-3 ml-3 float-right" data-toggle="modal" data-target="#modal_address_add" data-original-title="Add Address" data-backdrop="static">Add New Address</button> --}}
                    </div>
                </div>
                <hr>
                <div class="block-content block-content-full">
                    <h3>Card Details</h3>
                    <div class="row" id="card_list">
                    </div>
                    <div class="row">
                        {{-- <button type="button" class="btn btn-primary mt-3 ml-3 float-right" data-toggle="modal" data-target="#modal_card_add" data-original-title="Add Card" data-backdrop="static">Add New Card</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="modal_address_add" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Add New Address</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form id="add_address_form" method="POST" action="" onsubmit="return false">
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label>Unit No./Street/Area <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" id="add_address_detail" name="add_address_detail" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group tw_only d-none">
                                <label>SSN <span class="text-danger">*</span></label>
                                <input required type="hidden" class="form-control" id="add_ssn" name="add_ssn" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group">
                                <label>Town/City <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" id="add_city" name="add_city" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group">
                                <label>Postal Code <span class="text-danger">*</span></label>
                                <input required type="number" class="form-control" id="add_portalcode" name="add_portalcode" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group">
                                <label>State <span class="text-danger">*</span></label>
                                <select required class="form-control states-select" id="add_state" name="add_state" {{ $canCreate ? "" : "disabled" }}>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full border-top">
                        <button type="submit" id="add_address" class="btn btn-sm btn-primary">Save</button>
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="modal" id="modal_address_edit" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true" data-address-id="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Add New Address</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Unit No./Street/Area <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="edit_address_detail" name="edit_address_detail" {{ $canEdit ? "" : "disabled" }}/>
                        </div>
                        <div class="col-12 form-group tw_only d-none">
                            <label>SSN <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="edit_ssn" name="edit_ssn" {{ $canEdit ? "" : "disabled" }}/>
                        </div>
                        <div class="col-12 form-group">
                            <label>Town/City <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="edit_city" name="edit_city" {{ $canEdit ? "" : "disabled" }}/>
                        </div>
                        <div class="col-12 form-group">
                            <label>Postal Code <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="edit_portalcode" name="edit_portalcode" {{ $canEdit ? "" : "disabled" }}/>
                        </div>
                        <div class="col-12 form-group">
                            <label>State <span class="text-danger">*</span></label>
                            <select class="form-control states-select" id="edit_state" name="edit_state" {{ $canEdit ? "" : "disabled" }}>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full border-top">
                    <button type="submit" id="edit_address" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal" id="modal_address_select" tabindex="-1" aria-labelledby="modal-block-vcenter" aria-hidden="true" data-address-id="">
    <div class="modal modal-centered">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Change Default Address</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="col-12 form-group">
                        <label>Please select shipping or billing</label>
                    </div>
                </div>
                <div class="block-content block-content-full border-top">
                    <button type="button" id="set_default_shipping" class="btn btn-sm btn-primary">Set as default shipping</button>
                    <button type="button" id="set_default_billing" class="btn btn-sm btn-primary">Set as default billing</button>
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="modal_card_add" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Add New Card</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <form id="add_card_form" method="POST" action="" onsubmit="return false">
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label>Card Number <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control card_number_masked number-only" id="add_card_number_masked"  maxlength="19" {{ $canCreate ? "" : "disabled" }}/>
                                <input id="add_card_number" name="add_card_number" type="hidden" class="card_number number-only" maxlength="19" />
                            </div>
                            <div class="col-7 form-group">
                                <label>Expiry Date (MM/YY) <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control number-only" id="add_card_expiry" name="add_card_expiry" maxlength="7" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                            <div class="col-5 form-group">
                                <label>CVV <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control number-only" id="add_card_cvv" name="add_card_cvv" maxlength="4" {{ $canCreate ? "" : "disabled" }}/>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full border-top">
                        <button type="submit" id="add_card" class="btn btn-sm btn-primary">Save</button>
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> --}}
<script>
</script>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/users/customerDetails.function.js') }}"></script>
<script>
    var customerid = "{{ collect(request()->segments())->last() }}";
</script>
@endsection