@extends('layouts.app')

@section('content')
@php($adminId = Auth::user()->id)
<script>let adminId = {{ $adminId }}</script>
<div class="content content-narrow">
        <div class="row">
            <div class="col-12">
                <div class="block block-rounded block-mode-loading-oneui">
                    <div class="block-header">
                        <h3 class="block-title">Exports</h3>
                    </div>
                    <div class="block-content block-content-full" id="bo-list-section">
                        <form>
                            <div class="row mx-0 mb-4">
                                <div class="col-md-4 filter-field mt-3">
                                    <label for="">Search</label>
                                    <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by File Name" type="text">
                                </div>
                                <div class="col-md-4 mt-3">
                                    <label for="">Filter by Status</label>
                                    <select class="selectpicker form-control filterExport" name="filterByStatus" id="filterByStatus">
                                        <option value="all">All Status</option>
                                        <option value="Processsing">Processing</option>
                                        <option value="Processsing">Completed</option>
                                        <option value="Cancelled">Cancelled</option>
                                    </select>
                                </div>
                                <div class="col-md-4 mt-3 form-group">
                                    <label for="filterByDateFrom">Filter by Date</label>
                                    <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                        <input type="text" class="form-control filterExport" id="filterByDateFrom" name="filterByDateFrom" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                        <div class="input-group-prepend input-group-append">
                                            <span class="input-group-text font-w600">
                                                <i class="fa fa-fw fa-arrow-right"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control filterExport" id="filterByDateTo" name="filterByDateTo" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row row-deck">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                                <thead class="thead-dark">
                                    <tr class="text-uppercase">
                                        <th>Filename</th>
                                        <th>Type</th>
                                        <th>Details</th>
                                        <th>Date</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="exports_list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/export/export-list.function.js') }}"></script>
@endsection
