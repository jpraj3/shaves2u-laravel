@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('admins/roles','canCreate'))
@php($canViewDetails = $permissionsHelper->check('admins/roles/details','canView'))

@section('css_before')
@endsection

@section('js_after')
<!-- Page JS Plugins -->
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <div class="row row-deck w-100">
                        <div class="col-6">
                            <h3 class="block-title">Manage Roles</h3>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('admin.admins.roles.add') }}" class="btn btn-primary ml-auto" {{ $canCreate ? "" : "disabled" }}>Add Role</a>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody id="roles_list">
                                @foreach ($data as $d)
                                <tr>
                                    <td>{{ $d->roleName }}</td>
                                    <td>{{ $d->description }}</td>
                                    <td class="text-center"><a class="btn btn-sm btn-light js-tooltip-enabled" href="{{ route('admin.admins.roles.details', $d->id) }}" data-original-title="Edit Role" {{ $canViewDetails ? "" : "disabled" }}><i class="fa fa-edit"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection