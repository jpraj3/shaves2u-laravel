@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('admins', 'canCreate'))
@php($canEdit = $permissionsHelper->check('admins', 'canEdit'))

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <div class="row row-deck w-100">
                        <div class="col-6">
                            <h3 class="block-title">Admins</h3>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary ml-auto" data-toggle="modal" data-target="#modal_add_admin" {{ $canCreate? "" : "disabled" }}>Add Admin</button>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <form>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-3 filter-field mt-3">
                                <label for="">Search</label>
                                <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by Order No, Tax Invoice No, Email" type="text">
                            </div>
                        </div>
                    </form>
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Country</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="admins_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_add_admin" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="add_admin" method="POST" action="">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Add Admin</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-6 form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="Enter first name" id="add_admin_firstname" name="add_admin_firstname" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-6 form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Enter last name" id="add_admin_lastname" name="add_admin_lastname" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" placeholder="Enter phone number" id="add_admin_phone" name="add_admin_phone" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-12 form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Email address" id="add_admin_email" name="add_admin_email" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-6 form-group">
                                <label>Create New Password <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" placeholder="Create New Password" id="add_admin_password" name="add_admin_password" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-6 form-group">
                                <label>Re-enter Password <span class="text-danger">*</span></label>
                                <input type="password" class="form-control" placeholder="Re-enter Password" id="confirm_admin_password" name="confirm_admin_password" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-6 form-group">
                                <label>Role</label>
                                <select class="form-control role-select" id="add_admin_role" name="add_admin_role" {{ $canCreate? "" : "disabled" }}>
                                    <option value="" selected disabled>Select a role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->roleName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6 form-group">
                                <label>Country <span class="text-danger">*</span></label>
                                <select class="form-control country-select" id="add_admin_country" name="add_admin_country" {{ $canCreate? "" : "disabled" }}>
                                        <option value="" selected disabled>Select Country</option>
                                </select>
                            </div>
                            <div class="col-6 form-group">
                                <label>Marketing Office</label>
                                <select class="form-control mo-select" id="add_admin_mo" name="add_admin_mo" {{ $canCreate? "" : "disabled" }}>
                                    <option value="" selected disabled>Select Marketing Office</option>
                                </select>
                            </div>
                            <div class="col-12 pl-4 form-group">
                                <label class="form-check-label">
                                    <input class="form-check-input" id="add_admin_viewall" name="add_admin_viewall" type="checkbox"  value="1">
                                     Can view data of all countries
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canCreate? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_admin" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="edit_admin" method="POST" action="">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Edit Admin</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <input type="hidden" class="form-control" id="edit_admin_id" name="edit_admin_id"/>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label>Role</label>
                                <select class="form-control role-select" id="edit_admin_role" name="edit_role" {{ $canEdit? "" : "disabled" }}>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->roleName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6 form-group">
                                <label>Country <span class="text-danger">*</span></label>
                                <select class="form-control country-select" id="edit_admin_country" name="edit_country" {{ $canEdit? "" : "disabled" }}>
                                </select>
                            </div>
                            <div class="col-6 form-group">
                                <label>Marketing Office</label>
                                <select class="form-control mo-select" id="edit_admin_mo" name="edit_mo" {{ $canEdit? "" : "disabled" }}>
                                    <option value="" selected disabled>Select Marketing Office</option>
                                </select>
                            </div>
                            <div class="col-12 pl-4 form-group">
                                <label class="form-check-label">
                                    <input class="form-check-input" id="edit_admin_viewall" name="edit_viewall" type="checkbox"  value="1">
                                     Can view data of all countries
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canEdit? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/admin/admins.function.js') }}"></script>
<script>
    let canEdit = {{ $canEdit }}
    let canCreate = {{ $canEdit }}
</script>
@endsection