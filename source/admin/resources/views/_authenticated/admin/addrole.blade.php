@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('admins/roles/add', 'canCreate'))

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <form method="POST" action="{{ route('admin.admins.roles.create') }}">
                    @csrf
                    <div class="block-header">
                        <h3 class="block-title">Add Role</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="row">
                            <div class="col-12 form-group">
                                <label>Role Name <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" id="role_name" name="role_name" value=""/>
                            </div>
                            <div class="col-12 form-group">
                                <label>Description <span class="text-danger">*</span></label>
                                <textarea required class="form-control" id="role_desc" name="role_desc"></textarea>
                            </div>
                            <div class="col-12 form-group">
                                <label>Path</label>
                                <div class="row mx-0">
                                    @foreach ($data as $d)
                                        <div class="col-4">
                                            <label for="{{ $d->id }}_all_actions">
                                                <input type="checkbox" id="{{ $d->id }}_all_actions" name="permission[{{ $d->id }}][all_actions]" class="check-all" data-role-id="{{ $d->id }}" value="all_actions"> {{ $d->pathValue }}
                                            </label>
                                            <div class="col-12">
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_view">
                                                        <input type="checkbox" id="{{ $d->id }}_can_view" name="permission[{{ $d->id }}][can_view]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_view"> Can View
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_create">
                                                        <input type="checkbox" id="{{ $d->id }}_can_create" name="permission[{{ $d->id }}][can_create]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_create"> Can Create
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_edit">
                                                        <input type="checkbox" id="{{ $d->id }}_can_edit" name="permission[{{ $d->id }}][can_edit]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_edit"> Can Edit
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_upload">
                                                        <input type="checkbox" id="{{ $d->id }}_can_upload" name="permission[{{ $d->id }}][can_upload]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_upload"> Can Upload
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_download">
                                                        <input type="checkbox" id="{{ $d->id }}_can_download" name="permission[{{ $d->id }}][can_download]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_download"> Can Download
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_delete">
                                                        <input type="checkbox" id="{{ $d->id }}_can_delete" name="permission[{{ $d->id }}][can_delete]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_delete"> Can Delete
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="{{ $d->id }}_can_reactive">
                                                        <input type="checkbox" id="{{ $d->id }}_can_reactive" name="permission[{{ $d->id }}][can_reactive]" class="check-sub role_{{ $d->id }}" data-role-id="{{ $d->id }}"  value="can_reactive"> Can Reactive
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <button class="btn btn-sm btn-secondary" type="button">Cancel</button>
                                <button class="btn btn-sm btn-primary" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<!-- Page JS Plugins -->

<script>
    $(document).ready(function () {
        $(".check-all").change(function () {
            var role_id = $(this).attr('data-role-id');
            if  ($(this).is(':checked')) {
                $(".role_" + role_id).prop( "checked", true );
            }
            else {
                $(".role_" + role_id).prop( "checked", false );
            }   
        });

        $(".check-sub").change(function () {
            var role_id = $(this).attr('data-role-id');
            if  ($(".role_" + role_id).not(':checked').length == 0) {
                $("#" + role_id + "_all_actions").prop( "checked", true );
            }
            else {
                $("#" + role_id + "_all_actions").prop( "checked", false );
            }   
        });
    });
</script>
@endsection