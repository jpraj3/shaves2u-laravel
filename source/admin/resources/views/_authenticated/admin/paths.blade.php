@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('admins/paths', 'canCreate'))
@php($canEdit = $permissionsHelper->check('admins/paths', 'canEdit'))

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <div class="row row-deck w-100">
                        <div class="col-6">
                            <h3 class="block-title">Manage Paths</h3>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary ml-auto" data-toggle="modal" data-target="#modal_addpath" {{ $canCreate? "" : "disabled" }}>Add Path</button>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Name</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody id="roles_list">
                                @foreach ($data as $d)
                                <tr>
                                    <td>{{ $d->pathValue }}</td>
                                <td class="text-center"><button class="btn btn-sm btn-light js-tooltip-enabled edit-path" data-path-id="{{ $d->id }}" data-path-value="{{ $d->pathValue }}" data-toggle="modal" data-target="#modal_editpath" data-original-title="Edit Role" {{ $canEdit? "" : "disabled" }}><i class="fa fa-edit"></i></button></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_addpath" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="add_path" method="POST" action="{{ route('admin.admins.paths.create') }}">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Add Path</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="col-12 form-group">
                            <label>Path Value <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="add_path_value" name="add_path_value" {{ $canCreate? "" : "disabled" }}/>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canCreate? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_editpath" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="edit_path" method="POST" action="{{ route('admin.admins.paths.edit') }}">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Edit Path</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <input type="hidden" class="form-control" id="edit_path_id" name="edit_path_id"/>
                        <div class="col-12 form-group">
                            <label>Path Value <span class="text-danger">*</span></label>
                            <input required type="text" class="form-control" id="edit_path_value" name="edit_path_value" {{ $canEdit? "" : "disabled" }}/>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canEdit? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@section('js_after')
<!-- Page JS Plugins -->
<script>
    $(document).ready(function() {
        jQuery('.edit-path').click(function(e) {
            $("#edit_path_id").val( $(this).attr('data-path-id') );
            $("#edit_path_value").val( $(this).attr('data-path-value') );
        });
    });
</script>
@endsection
@endsection