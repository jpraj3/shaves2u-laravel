@extends('layouts.app')

@php($canViewDetails = $permissionsHelper->check('referral/cashout/details', 'canView'))
@php($canEdit = $permissionsHelper->check('referral/cashout/details', 'canEdit'))

@if (!$canViewDetails)
    <style>
        .view-details {
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
@endif

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Referral Cash-Out Details</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-3">
                            <strong>Email Address: </strong>
                        </div>
                        <div class="col-9">
                            <span id="email"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>User Country: </strong>
                        </div>
                        <div class="col-9">
                            <span id="country"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>Cash-out Amount: </strong>
                        </div>
                        <div class="col-9">
                            <span id="amount"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>Bank Name: </strong>
                        </div>
                        <div class="col-9">
                            <span id="bank_name"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>Full Name as per Bank Account: </strong>
                        </div>
                        <div class="col-9">
                            <span id="full_name"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>Bank Account No.: </strong>
                        </div>
                        <div class="col-9">
                            <span id="bank_acc_no"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <strong>Request date/time: </strong>
                        </div>
                        <div class="col-9">
                            <span id="created_date"></span>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-3">
                                    <strong>Referral Cash-out Status: </strong>
                                </div>
                                <div class="col-3">
                                    <select class="form-control w-50" id="change_status" {{ $canEdit ? "" : "disabled" }}>
                                        <option value="" disabled hidden selected></option>
                                        <option value="active" disabled hidden>Active</option>
                                        <option value="in_process" disabled hidden>In Process</option>
                                        <option value="withdrawn">Withdrawn</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modal_status_confirm" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Withdraw Referral Cash-out</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="col-12 form-group">
                        <label> Confirm status? </label>
                    </div>
                </div>
                <div class="block-content block-content-full border-top">
                    <button type="button" id="referral_cashout_status_confirmed" class="btn btn-sm btn-primary">Confirm</button>
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/referralsCashOut/referralcashoutDetails.function.js') }}"></script>
<script>
    var referralid = "{{ collect(request()->segments())->last() }}";
</script>
@endsection