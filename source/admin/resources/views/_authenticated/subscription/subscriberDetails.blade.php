@extends('layouts.app')

@php($canEdit = $permissionsHelper->check('subscription/subscribers/details', 'canEdit'))

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Subscriber Details</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-2">
                            <strong>Email Address: </strong>
                        </div>
                        <div class="col-10">
                            <span id="email"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Subscription ID: </strong>
                        </div>
                        <div class="col-10">
                            <span id="subscription_id"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Subscription Status: </strong>
                        </div>
                        <div class="col-10">
                            <span id="subscription_status"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Plan Type: </strong>
                        </div>
                        <div class="col-10">
                            <span id="plan_type"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Frequency: </strong>
                        </div>
                        <div class="col-10">
                            <span id="frequency"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Order No: </strong>
                        </div>
                        <div class="col-10" id="order_no">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Channel: </strong>
                        </div>
                        <div class="col-10">
                            <span id="channel"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Next Charge Date: </strong>
                        </div>
                        <div class="col-10">
                            <span id="next_charge_date"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Recharge History: </strong>
                        </div>
                        <div class="col-10" id="recharge_history">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Created Date: </strong>
                        </div>
                        <div class="col-10" id="created_date">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Last Updated: </strong>
                        </div>
                        <div class="col-10" id="last_updated">
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-6">
                                    <strong>Subscription Status: </strong>
                                </div>
                                <div class="col-6">
                                    <select class="form-control w-50" id="change_status">
                                        <option value="Pending">Pending</option>
                                        <option value="Processing">Processing</option>
                                        <option value="Cancelled">Cancelled</option>
                                        {{-- <option value="Payment Failure">Payment Failure</option> --}}
                                        <option value="On Hold">On Hold</option>
                                        <option value="Unrealized">Unrealized</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <strong>Date</strong>
                                </div>
                                <div class="col-4">
                                    <strong>Status</strong>
                                </div>
                                <div class="col-6">
                                    <strong>Description</strong>
                                </div>
                            </div>
                            <div id="subscriber_log">
                            </div>
                            <div class="row">
                                <div class="col-4" style="    margin-top: 3%;">
                                    <button id="open_slm" class="btn btn-primary" {{ $canEdit ? "" : "disabled" }}>Add Comment</button>
                                </div>
                            </div>
                            <div id="subscriber_log_add_comment" class="d-none border rounded p-3">
                                <div>
                                    Add Comment
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <textarea id="subscriber_log_message" name="subscriber_log_message" class="w-100" placeholder="Write your comment here!"></textarea>
                                            <button id="submit_slm" class="btn btn-primary pull-right" type="button" {{ $canEdit ? "" : "disabled" }}>Add</button>
                                            <button id="cancel_slm" class="btn btn-secondary pull-right mr-1" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <style>
                        .vl {
                            border-left: 1px solid lightgrey;
                            height: 20vh;
                        }
                        </style>
                        
                        <div class="col-1 vl"></div>
                        <div class="col-5">
                            <div class="row">
                                <div class="col-4">
                                    <strong>Payment Method: </strong>
                                </div>
                            </div>
                            <div id="payment_method">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-6">
                            <p><strong>Shipping Details:</strong></p>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="col-11">
                                    <span id="shipping_name"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="col-11">
                                    <span id="shipping_contact"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="col-11">
                                    <span id="shipping_address"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <p><strong>Plan Details:</strong></p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>SKU</th>
                                        {{-- <th>Item</th> --}}
                                        <th class="text-right">Price</th>
                                    </tr>
                                </thead>
                                <tbody id="plandetails">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full d-none">
                    <p><strong>Tax Invoice</strong></p>
                    <div class="table-responsive push">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Price</th>
                                    <th class="text-right">Total Price</th>
                                </tr>
                            </thead>
                            <tbody id="invoice">
                                <tr>
                                    <td colspan="2" class="font-w600 text-right">Sub Total</td>
                                    <td class="text-right" id="sub_total"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right">Discount</td>
                                    <td class="text-right" id="discount"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right">Processing Fee</td>
                                    <td class="text-right" id="processing_fee"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right">Grand Total</td>
                                    <td class="text-right" id="grand_total"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="font-w600 text-right">Cash Rebate</td>
                                    <td class="text-right" id="cash_rebate"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w700 text-uppercase text-right bg-body-light">Total Payable (Incl. Tax)</td>
                                    <td class="font-w700 text-right bg-body-light" id="total_payable"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
</script>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/subscriptions/subscriberDetails.function.js') }}"></script>
<script>
    var subscriberid = "{{ collect(request()->segments())->last() }}";
    var handleTypes = {!! json_encode(config('global.all.handle_types.trial.skus'))!!};
</script>
@endsection