@extends('layouts.app')

@php($canDownload = $permissionsHelper->check('subscription/subscribers', 'canDownload'))
@php($canViewDetails = $permissionsHelper->check('subscription/subscribers/details', 'canView'))
@php($adminId = Auth::user()->id)

@if (!$canViewDetails)
    <style>
        .view-details {
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
@endif

@section('css_before')
@endsection

@section('content')
{{-- {{ dd($permissionsHelper->CheckPermissionsByPathByAction($userRole, 'orders', 'canView')) }} --}}
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Subscribers</h3>
    <button id="csv-report-trigger-2" onClick="exportCSV('subscriberssummary')" type="button" class="btn csv-download">Download historical data since day 1</button>
    <button id="csv-report-trigger" onClick="exportCSV('subscribers')" type="button" class="btn csv-download">Download live report</button>
                </div>
                <div hidden class="col-12" id="csv-queue-notification">
                    <p style="color: green; margin-right: 3%; text-align: right; margin-bottom: 0px;">
                        Your export has been queued.
                        <a href="{{ route('admin.export') }}">View progress.</a>
                    </p>
                </div>
                <div class="block-content block-content-full">
                    <form>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-3 offset-md-6">
                                <label for="">Filter by Country</label>
                                <select class="selectpicker form-control filterSubscribers" name="filterByCountry" id="filterByCountry">
                                    <option value="all">All Countries</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Filter by Plan Type</label>
                                <select class="selectpicker form-control filterSubscribers" name="filterByPlanType" id="filterByPlanType">
                                    <option value="all">All Plan Types</option>
                                    <option value="trial">Trial</option>
                                    <option value="custom">Custom</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-3 filter-field mt-3">
                                <label for="">Search</label>
                                <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by Email" type="text">
                            </div>
                            <div class="col-md-3 mt-3">
                                <label for="">Filter by SKU</label>
                                <select class="selectpicker form-control filterSubscribers" name="filterBySKU" id="filterBySKU">
                                    <option value="all">All SKUs</option>
                                </select>
                            </div>
                            <div class="col-md-3 mt-3">
                                <label for="">Filter by Status</label>
                                <select class="selectpicker form-control filterSubscribers" name="filterByStatus" id="filterByStatus">
                                    <option value="all">All Status</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Processing">Processing</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Payment Failure">Payment Failure</option>
                                    <option value="On Hold">On Hold</option>
                                    <option value="Unrealized">Unrealized</option>
                                </select>
                            </div>
                            <div class="col-md-3 mt-3 form-group">
                                <label for="filterByDateFrom">Filter by Date</label>
                                <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <input type="text" class="form-control filterSubscribers" id="filterByDateFrom" name="filterByDateFrom" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <div class="input-group-prepend input-group-append">
                                        <span class="input-group-text font-w600">
                                            <i class="fa fa-fw fa-arrow-right"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control filterSubscribers" id="filterByDateTo" name="filterByDateTo" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="button" id="start_filter" class="btn btn-primary">Filter</button>
                        </div>
                    </form>
                    <div class="row row-deck">
                        <table id="subscribers_table" class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Email</th>
                                    <th>Country</th>
                                    <th>Created Date</th>
                                    <th>Last Updated</th>
                                    <th>Frequency</th>
                                    <th>SKU</th>
                                    <th>Plan Type</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="subscribers_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/subscriptions/subscribers.function.js') }}"></script>
<script src="{{ asset('js/functions/global/export-report.function.js') }}"></script>
<script>
    const adminId = {{ json_encode($adminId) }};
    const canDownload = {!! json_encode($canDownload) !!};
    const canViewDetails = {!! json_encode($canViewDetails) !!};
</script>
@endsection