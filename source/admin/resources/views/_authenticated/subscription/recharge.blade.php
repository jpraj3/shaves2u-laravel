@extends('layouts.app')

@php($canDownload = $permissionsHelper->check('subscription/recharge', 'canDownload'))
@php($canViewDetails = $permissionsHelper->check('subscription/recharge/details', 'canView'))

@if (!$canViewDetails)
    <style>
        .view-details {
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
@endif

@section('css_before')
@endsection
@php($adminId = Auth::user()->id)
<script>let adminId = {{ json_encode($adminId) }}</script>
@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Recharge History</h3>
                    @canDownload('subscription/recharge')
                    <div class="block-header">
                        <button id="csv-report-trigger" onClick="exportCSV('recharges')" type="button" class="btn csv-download">Download Report</button>
                    </div>
                    @endcanDownload
                </div>
                <div hidden class="col-12" id="csv-queue-notification">
                    <p style="color: green; margin-right: 3%; text-align: right; margin-bottom: 0px;">
                        Your export has been queued.
                        <a href="{{ route('admin.export') }}">View progress.</a>
                    </p>
                </div>
                <div class="block-content block-content-full">
                    <form>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-3 filter-field mt-3">
                                <label for="">Search</label>
                                <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by Email" type="text">
                            </div>
                            <div class="col-md-3 mt-3">
                                <label for="">Filter by Status</label>
                                <select class="selectpicker form-control filterRecharges" name="filterByStatus" id="filterByStatus">
                                    <option value="all">All Status</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Processing">Processing</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Payment Failure">Payment Failure</option>
                                    <option value="On Hold">On Hold</option>
                                    <option value="Unrealized">Unrealized</option>
                                </select>
                            </div>
                            <div class="col-md-3 mt-3 form-group">
                                <label for="filterByDateFrom">Filter by Date</label>
                                <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <input type="text" class="form-control filterRecharges" id="filterByDateFrom" name="filterByDateFrom" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <div class="input-group-prepend input-group-append">
                                        <span class="input-group-text font-w600">
                                            <i class="fa fa-fw fa-arrow-right"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control filterRecharges" id="filterByDateTo" name="filterByDateTo" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="button" id="start_filter" class="btn btn-primary">Filter</button>
                        </div>
                    </form>
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Email</th>
                                    <th>Recharge Date</th>
                                    <th>Total Recharges</th>
                                    <th>Status</th>
                                    <th>Realized</th>
                                </tr>
                            </thead>
                            <tbody id="recharges_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/recharge/recharge.function.js') }}"></script>
@endsection