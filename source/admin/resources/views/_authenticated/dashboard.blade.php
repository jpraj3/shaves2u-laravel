@extends('layouts.app')

@php($ordersCanDownload = $permissionsHelper->check('orders','canDownload'))
@php($ordersCanViewDetails = $permissionsHelper->check('orders/details','canView'))
@php($usersCanDownload = $permissionsHelper->check('users','canDownload'))
@php($usersCanViewDetails = $permissionsHelper->check('users/details','canView'))

@section('css_before')
@endsection

@php($adminId = Auth::user()->id)
<script>
const adminId = {{ $adminId }};
const ordersCanDownload = {{ json_encode($ordersCanDownload) }};
const ordersCanViewDetails = {{ json_encode($ordersCanViewDetails) }};
const usersCanDownload = {{ json_encode($usersCanDownload) }};
const usersCanViewDetails = {{ json_encode($usersCanViewDetails) }};
</script>

@section('content')
<div class="bg-image overflow-hidden" style="background-image: url({{ asset('media/photos/photo3@2x.jpg') }});">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 js-appear-enabled animated fadeIn" data-toggle="appear">Dashboard</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="250">Welcome {{ Auth::user()->firstName }}</h2>
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Sales <!----></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content p-0 pt-3 bg-body-light text-center">
                    <div class="row mx-0">
                        <div class="col-sm-2 form-group">
                            <select id="country_select" name="country_select" class="form-control">
                            </select>
                        </div>
                        <div class="col-sm-7 offset-sm-3 form-group">
                            <div aria-label="Basic example" class="btn-group float-right reloadChart" data-target="barChart" role="group">
                                <button class="btn btn-secondary active" type="button" value="day">Past Day</button>
                                <button class="btn btn-secondary" type="button" value="week">7 days</button>
                                <button class="btn btn-secondary" type="button" value="month">Last Month</button>
                                <button class="btn btn-secondary" type="button" value="year">This Year</button>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4>Revenue chart</h4>
                        </div>
                        <div class="col-sm-12 form-group">
                            <div class="text-right">
                                <span>Total Amount: <span id="currency"></span> <span id="total_amount"></span></span>
                                <strong></strong>
                            </div>
                        </div>
                    </div>
                    <div class="revenue-chart">
                        <span class="price">RM</span>
                        <span class="date">Date</span>
                        <div>
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                    </div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0">
                                    </div>
                                </div>
                            </div>
                            <canvas id="barChart" class="chartjs-render-monitor" height="400" width="1247">

                            </canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-12 mt-2">
            <div class="block block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">10 most recent orders</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead class="thead-dark">
                            <tr class="text-uppercase">
                                <th>Order No.</th>
                                <th>Tax Invoice No</th>
                                <th class="table-sort th-email">Created Date</th>
                                <th class="table-sort">Email</th>
                                <th class="">SKU</th>
                                <th class="table-sort" width="130">Grand Total</th>
                                <th class="table-sort th-channel" width="120">Channel</th>
                                <th class="th-status">Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="orders_list">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-deck">
        <div class="col-12 mt-2">
            <div class="block block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">10 most recent customer registrations</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                            <i class="si si-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm mb-0">
                        <thead class="thead-dark">
                            <tr class="text-uppercase">
                                <th>Email</th>
                                <th>Full name</th>
                                <th>Register Date</th>
                                <th>Phone No.</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="users_list">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('plugins/one-ui/plugins/chart.js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('js/functions/dashboard.function.js') }}"></script>
@endsection
