@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('ba/generateMO','canCreate'))
@php($canEdit = $permissionsHelper->check('ba/generateMO','canEdit'))

@if (!$canEdit)
    <style>
        .edit-mo {
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
@endif

@section('css_before')
@endsection

@section('content')
{{-- {{ dd($permissionsHelper->CheckPermissionsByPathByAction($userRole, 'orders', 'canView')) }} --}}
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <div class="row row-deck w-100">
                        <div class="col-6">
                            <h3 class="block-title">Marketing Offices</h3>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary ml-auto" data-toggle="modal" data-target="#modal_add_mo" {{ $canCreate? "" : "disabled" }}>Add MO</button>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="mo_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_add_mo" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="add_mo" method="POST" action="">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Add Marketing Office</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>Code <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Code" id="add_mo_code" name="add_mo_code" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                            <div class="col-9 form-group">
                                <label>Organization <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Organization" id="add_mo_org" name="add_mo_org" {{ $canCreate? "" : "disabled" }}/>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canCreate? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_mo" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="edit_mo" method="POST" action="">
                @csrf
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Edit Marketing Office</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>Code <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Code" id="edit_mo_code" name="edit_mo_code" {{ $canEdit? "" : "disabled" }}/>
                            </div>
                            <div class="col-9 form-group">
                                <label>Organization <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Organization" id="edit_mo_org" name="edit_mo_org" {{ $canEdit? "" : "disabled" }}/>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="submit" {{ $canEdit? "" : "disabled" }}>Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/ba/molist.function.js') }}"></script>
<script>
    let canEdit = {{ $canEdit }}
    let canCreate = {{ $canEdit }}
</script>
@endsection