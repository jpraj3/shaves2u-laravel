@extends('layouts.app')

@php($canCreate = $permissionsHelper->check('ba/generateBA','canCreate'))

@section('css_before')
@endsection


@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <div class="row row-deck w-100">
                        <div class="col-4">
                            <h3 class="block-title">Generate BA</h3>
                        </div>
                        <div class="col-8">
                            @canUpload('ba/generateBA')
                            <form id="submit_csv" method="POST">
                                <div class="form-group row">
                                    <div class="col-8">
                                        <input id="csv_file" name="csv_file" type="file" class="form-control"/>
                                    </div>
                                    <div class="col-4">
                                        <button class="btn btn-primary float-right mr-2" id="upload-csv" type="submit" >Add CSV</button>
                                    </div>
                                </div>
                            </form>
                            @endcanUpload
                        </div>
                    </div>
                </div>
                <div hidden class="col-12" id="upload-error">
                    <p id="error_msg" style="color: red; margin-right: 3%; text-align: right; margin-bottom: 0px;"></p>
                </div>
                <div class="block-content block-content-full">
                    <form id="form_add_ba" method="POST" action=""  onsubmit="return false">
                        <div class="row mx-0 mb-3">
                            <div class="col-12 mt-3 form-group">
                                <label>Badge ID <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Badge ID" id="badge_id" name="badge_id"/>
                            </div>
                            <div class="col-12 mt-3 form-group">
                                <label>IC Number <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="IC Number" id="ic_number" name="ic_number"/>
                            </div>
                            <div class="col-12 mt-3 form-group">
                                <label>Name <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Name" id="agent_name" name="agent_name"/>
                            </div>
                            <div class="col-12 mt-3 form-group">
                                <label>Campaign <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Campaign" id="campaign" name="campaign"/>
                            </div>
                            <div class="col-12 mt-3 form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" placeholder="Email Address" id="email" name="email"/>
                            </div>
                            <div class="col-6 mt-3 form-group">
                                <label>Country <span class="text-danger">*</span></label>
                                <select required class="form-control country-select" id="country" name="country">
                                    <option value="" selected disabled>Select Country</option>
                                </select>
                            </div>
                            <div class="col-6 mt-3 form-group">
                                <label>Marketing Office <span class="text-danger">*</span></label>
                                <select required class="form-control mo-select" id="marketing_office" name="marketing_office">
                                    <option value="" selected disabled>Select Marketing Office</option>
                                </select>
                            </div>
                            <button class="btn btn-sm btn-primary" type="submit">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
</script>
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{ asset('js/functions/ba/generateBA.function.js') }}"></script>
@endsection