@extends('layouts.app')

@php($canViewDetails = $permissionsHelper->check('referral/details', 'canView'))

@if (!$canViewDetails)
    <style>
        .view-details {
            pointer-events: none;
            cursor: not-allowed;
            opacity: 0.65;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }
    </style>
@endif

@section('css_before')
@endsection

@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Referral Details</h3>
                </div>
                <div class="block-content block-content-full">
                <p>User Email: <span id="ruser-email"></span></p>
                <p>Refer to</p>
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Email</th>
                                    <th>Order Numbers</th>
                                    <th>Channel</th>
                                    <th>Status</th>
                                    <th>Credit</th>
                                    <th>Date Of Registration</th>
                                </tr>
                            </thead>
                            <tbody id="referrals_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/referrals/referralDetails.function.js') }}"></script>
<script>
    var userid = "{{ collect(request()->segments())->last() }}";
</script>
@endsection