@extends('layouts.app')

@php($canDownload = $permissionsHelper->check('reports/appco', 'canDownload'))

@section('css_before')
@endsection
@php($adminId = Auth::user()->id)
<script>let adminId = {{ $adminId }}</script>
@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
            <div class="block-header">
                    <h3 class="block-title">SALES REPORTS (APPCO)</h3>
                    <div class="block-header">
                            @canDownload('reports/appco')
                            <button id="csv-report-trigger" onClick="exportCSV('reports/appco')" type="button" class="btn csv-download">Download Report</button>
                            @endcanDownload
                    </div>
                </div>
                <div hidden class="col-12" id="csv-queue-notification">
                    <p style="color: green; margin-right: 3%; text-align: right; margin-bottom: 0px;">
                        Your export has been queued.
                        <a href="{{ route('admin.export') }}">View progress.</a>
                    </p>
                </div>
                <div class="block-content block-content-full">
                    <form>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-3 offset-md-6">
                            <!-- <label for="">Filter by Channel</label>
                                <select class="selectpicker form-control filterReportAppco" name="filterByChannel" id="filterByChannel">
                                    <option value="all">All Channels</option>
                                    <option value="seller">BA</option>
                                    <option value="customer">Online / E-Commerce</option>
                                    <option value="bulk">Bulk Order</option>
                                </select> -->
                            </div>
                            <div class="col-md-3">
                            <label for="">Filter by Country</label>
                                <select class="selectpicker form-control filterReportAppco" name="filterByCountry" id="filterByCountry">
                                    <option value="all">All Countries</option>
                                </select>
                            </div>

                        </div>
                        <div class="row mx-0 mb-3">
                            <div class="col-md-5 filter-field mt-3">
                            <label for="">Search</label>
                                <input class="form-control" id="filterBySearch" name="filterBySearch" placeholder="Search by Order No, Tax Invoice No, Product SKU" type="text">
                            </div>
                            <div class="col-md-3 mt-3">
                                <label for="">Filter by Status</label>
                                <select class="selectpicker form-control filterReportAppco" name="filterByStatus" id="filterByStatus">
                                    <option value="all">All Status</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Payment Received">Payment Received</option>
                                    <option value="Processing">Processing</option>
                                    <option value="Delivering">Delivering</option>
                                    <option value="Completed">Completed</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Payment Failure">Payment Failure</option>
                                    <option value="Returned">Returned</option>
                                </select>
                            </div>
                            <div class="col-md-4 mt-3 form-group">
                                <label for="filterByDateFrom">Filter by Date</label>
                                <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <input type="text" class="form-control filterReportAppco" id="filterByDateFrom" name="filterByDateFrom" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    <div class="input-group-prepend input-group-append">
                                        <span class="input-group-text font-w600">
                                            <i class="fa fa-fw fa-arrow-right"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control filterReportAppco" id="filterByDateTo" name="filterByDateTo" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="button" id="start_filter" class="btn btn-primary">Filter</button>
                        </div>
                    </form>
                    <div class="row row-deck">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-buttons">
                            <thead class="thead-dark">
                                <tr class="text-uppercase">
                                    <th>Order No.</th>
                                    <th>Badge Id</th>
                                    <th>Tax Invoice No</th>
                                    <th>Channel Type</th>
                                    <th>Product SKU</th>
                                    <th>Date</th>
                                    <th>Location Code</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="reports_appco_list">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')
<script src="{{ asset('js/functions/reports/appco.function.js') }}"></script>
@endsection