@extends('layouts.app')

@php($canEdit = $permissionsHelper->check('bulk-orders/details','canEdit'))

@section('css_before')
@endsection


@section('content')
<div class="content content-narrow">
    <div class="row">
        <div class="col-12">
            <div class="block block-rounded block-mode-loading-oneui">
                <div class="block-header">
                    <h3 class="block-title">Bulk Order Details</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-2">
                            <strong>Name: </strong>
                        </div>
                        <div class="col-10">
                            <span id="full_name"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Email Address: </strong>
                        </div>
                        <div class="col-10">
                            <span id="email"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Subscription ID: </strong>
                        </div>
                        <div class="col-10">
                            <span id="subscription_id"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Order No.: </strong>
                        </div>
                        <div class="col-10">
                            <span id="order_no"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Tax Invoice No.: </strong>
                        </div>
                        <div class="col-10">
                            <span id="tax_invoice_no"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Charge ID: </strong>
                        </div>
                        <div class="col-10">
                            <span id="charge_id"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Channel: </strong>
                        </div>
                        <div class="col-10">
                            <span id="channel"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Tracking No.: </strong>
                        </div>
                        <div class="col-10">
                            <span id="tracking_no"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Payment Method: </strong>
                        </div>
                        <div class="col-10">
                            <span class="text-capitalize" id="payment_method"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <strong>Created Date: </strong>
                        </div>
                        <div class="col-10">
                            <span id="created_date"></span>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-4">
                                    <strong>Order Status: </strong>
                                </div>
                                <div class="col-6">
                                    <select class="form-control w-50" id="change_status" {{ $canEdit ? "" : "disabled" }}>
                                        <option value="Pending">Pending</option>
                                        <option value="Payment Received">Payment Received</option>
                                        <option value="Processing">Processing</option>
                                        <option value="Delivering">Delivering</option>
                                        <option value="Completed">Completed</option>
                                        <option value="Cancelled">Cancelled</option>
                                        {{-- <option value="Payment Failure">Payment Failure</option> --}}
                                        <option value="Returned">Returned</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <strong>Date </strong>
                                </div>
                                <div class="col-6">
                                    <strong>Description/Status</strong>
                                </div>
                            </div>
                            <div id="order_log">
                            </div>
                            <div class="row">
                                <div class="col-6 offset-4">
                                    <button id="open_olm" class="btn btn-primary" {{ $canEdit ? "" : "disabled" }}>Add Comment</button>
                                </div>
                            </div>
                            <div id="order_log_add_comment" class="d-none border rounded p-3">
                                <div>
                                    Add Comment
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <textarea id="order_log_message" name="order_log_message" class="w-100" placeholder="Write your comment here!"></textarea>
                                            <button id="submit_olm" class="btn btn-primary pull-right" type="button" {{ $canEdit ? "" : "disabled" }}>Add</button>
                                            <button id="cancel_olm" class="btn btn-secondary pull-right mr-1" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="col-6">
                            <form>
                                <div class="row">
                                    <div class="col-4">
                                        <strong>Payment Status: </strong>
                                    </div>
                                    <div class="col-6">
                                        <span id="status"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <strong>Date </strong>
                                    </div>
                                    <div class="col-6">
                                        <strong>Description/Status</strong>
                                    </div>
                                </div>
                                <div id="payment_log">
                                </div>
                                <div class="row">
                                    <div class="col-6 offset-4">
                                        <button class="btn btn-primary">Add Comment</button>
                                    </div>
                                </div>
                                <div id="payment_log_add_comment" class="d-none">
                                    <div>
                                        Add Comment
                                    </div>
                                    <div class="col-md-12">
                                        <div class="panel panel-info">
                                            <div class="panel-body">
                                                <textarea id="payment_log_message" name="payment_log_message" placeholder="Write your comment here!"></textarea>
                                                <button id="submit_plm" class="btn btn-primary pull-right" type="button">Add</button>
                                                <button id="cancel_plm" class="btn btn-secondary pull-right mr-1" type="button">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        -->
                    </div>
                </div>
                <div id="tracking_div" class="d-none block-content block-content-full">
                    <p><strong>Tracking Number</strong></p>
                    <div class="row">
                        <div class="col-4">
                            <input id="tracking_number" name="tracking_number" type="text" class="form-control tracking-control"/>
                        </div>
                        <div class="col-2">
                            <button id="update_tracking" class="btn btn-primary tracking-control" type="button" {{ $canEdit ? "" : "disabled" }}>Update</button>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <p><strong>Shipping Details</strong></p>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="col-11">
                            <span id="shipping_name"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="col-11">
                            <span id="shipping_contact"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="col-11">
                            <span id="shipping_address"></span>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <p><strong>Tax Invoice</strong></p>
                    <div class="table-responsive push">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Price</th>
                                    <th class="text-right">Total Price</th>
                                </tr>
                            </thead>
                            <tbody id="invoice">
                                <tr>
                                    <td colspan="3" class="font-w600 text-right">Sub Total</td>
                                    <td class="text-right" id="sub_total"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w600 text-right">Discount</td>
                                    <td class="text-right" id="discount"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w600 text-right">Processing Fee</td>
                                    <td class="text-right" id="processing_fee"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w600 text-right">Grand Total</td>
                                    <td class="text-right" id="grand_total"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w600 text-right">Cash Rebate</td>
                                    <td class="text-right" id="cash_rebate"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-w700 text-uppercase text-right bg-body-light">Total Payable (Incl. Tax)</td>
                                    <td class="font-w700 text-right bg-body-light" id="total_payable"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modal_status_confirm" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Deliver Bulk Order</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content font-size-sm">
                    <div class="col-12 form-group">
                        <label> Confirm status?. </label>
                    </div>
                </div>
                <div class="block-content block-content-full border-top">
                    <button type="button" id="bulkorder_status_confirmed" class="btn btn-sm btn-primary">Confirm</button>
                    <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
</script>
@endsection

@section('js_after')
<!-- Page JS Plugins -->
<script src="{{ asset('js/functions/bulkorders/bulkorderDetails.function.js') }}"></script>

<script>
    let bulkorderid = "{{ collect(request()->segments())->last() }}";
</script>
@endsection