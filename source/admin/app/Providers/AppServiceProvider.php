<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Admin\RoleDetails;
use App\Models\Admin\Paths;
use App\Helpers\PermissionsHelper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Blade::if('canView', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canView');
        });
        Blade::if('canCreate', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canCreate');
        });
        Blade::if('canEdit', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canEdit');
        });
        Blade::if('canDelete', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canDelete');
        });
        Blade::if('canUpload', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canUpload');
        });
        Blade::if('canDownload', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canDownload');
        });
        Blade::if('canReactive', function($path){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($path, 'canReactive');
        });
        Blade::if('permissions', function($paths, $action){
            $permissionsHelper = new PermissionsHelper();
            return $permissionsHelper->check($paths, $action);
        });
        
        //Allow pagination to be performed on collections
        Collection::macro('paginate', function($perPage, $page = null, $total = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }
}
