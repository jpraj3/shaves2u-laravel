<?php

namespace App\Http\Controllers;

use App\Exports\OrdersExport;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\FileUploads\FileUploads;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Promotions\PromotionCodes;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Excel;

class ExportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orders(Request $request)
    {
        try {
            $options = [];
            if (!empty($request->result)) {

                //format filters
                $filter_status = $request->filters["status"] !== "all" ? strtolower($request->filters["status"]) . "-orders-report-" : "orders-report-";
                $filter_fromdate = $request->filters["fromdate"] !== null ? $request->filters["fromdate"] : "for-all";
                $filter_todate = $request->filters["todate"] !== null ? $request->filters["todate"] : "";
                $date_text = $request->filters["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text .'-'.Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 0,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => Auth::user()->id,
                ]);

                foreach ($request->result as $order_) {
                    // Initialize variables
                    $order = null;
                    $delivery_address = null;
                    $billing_address = null;
                    $order_details = null;
                    $receipt = null;
                    $country = null;
                    $promotion = null;
                    $seller = null;
                    $marketing_office = null;
                    $cancellationReason = null;
                    $cancelledDate = null;

                    // build data
                    $order = Orders::where('id', $order_["id"])->first();
                    $delivery_address = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                    $billing_address = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
                    $order_details = OrderDetails::where('OrderId', $order->id)->get();
                    $orderhistories = OrderHistories::where('OrderId', $order->id)->get();
                    $receipt = Receipts::where('OrderId', $order->id)->first();
                    $country = Countries::where('id', $order->CountryId)->first();
                    if ($order->PromotionId !== null) {
                        $promotion = PromotionCodes::where('PromotionId', $order->PromotionId)->first();
                    }

                    // BA Info
                    if ($order->SellerUserId !== null) {
                        $seller = SellerUser::where('id', $order->SellerUserId)->first();
                        $marketing_office = MarketingOffice::where('id', $seller->MarketingOfficeId)->first();
                    }

                    // get cancellationReason if order was canceled from orderHistories
                    if ($order->status == 'Cancelled') {
                        for ($i = count($orderhistories) - 1; $i >= 0; $i--) {
                            $orderHistory = $orderhistories[$i];
                            // get the if lastest history was canceled
                            if ($i == count($orderhistories) - 1 && $orderHistory->message == 'Cancelled') {
                                $cancellationReason = $orderHistory->cancellationReason;
                                $cancelledDate = Carbon::parse($orderHistory->created_at)->format('YYYY-MM-DD');
                                break;
                            }
                        }
                    }

                    $e = [
                        "orderId" => $order !== null ? $order->id : null,
                        "badgeId" => $seller !== null ? $seller->badgeId : null,
                        "agentName" => $seller !== null ? $seller->agentName : null,
                        "channelType" => $order !== null ? $order->channelType : null,
                        "eventLocationCode" => $order !== null ? $order->eventLocationCode : null,
                        "moCode" => $marketing_office !== null ? $marketing_office->moCode : null,
                        "customerName" => $order !== null ? $order->fullName : null,
                        "email" => $order !== null ? $order->email : null,
                        "SSN" => $order !== null ? $order->SSN : null,
                        "deliveryAddress" => $delivery_address !== null ? $delivery_address->address . ',' . $delivery_address->portalCode . ',' . $delivery_address->city . ' ' . $delivery_address->state : null,
                        "deliveryContactNumber" => $delivery_address !== null ? $delivery_address->contactNumber : null,
                        "billingAddress" => $billing_address !== null ? $billing_address->address . ',' . $billing_address->portalCode . ',' . $billing_address->city . ' ' . $billing_address->state : null,
                        "billingContactNumber" => $billing_address !== null ? $billing_address->contactNumber : null,
                        "trackingNumber" => $order->deliveryId,
                        "promoCode" => $promotion !== null ? $promotion->code : null,
                        "discountPrice" => $receipt !== null ? $receipt->subTotalPrice - $receipt->discountAmount : $receipt->subTotalPrice,
                        "productName" => "test",
                        "sku" => "test",
                        "region" => $country !== null ? $country->CodeIso : null,
                        "category" => $order !== null &&  $seller !== null ? 'Sales app' : 'ecommerce',
                        "currency" => $country !== null ? $country->currencyDisplay : null,
                        "createdAt" => $order !== null ? $order->created_at : null,
                        "saleDate" => $order !== null ? $order->created_at : null,
                        "paymentType" => $order !== null ? $order->paymentType : null,
                        "status" => $order !== null ? $order->status : null,
                        "cancellationReason" => $cancellationReason,
                        "canceledDate" => $cancelledDate,
                        "totalPrice" => $receipt->totalPrice,
                        "subTotalPrice" => $receipt->subTotalPrice,
                        "discountAmount" => $receipt->discountAmount,
                        "taxAmount" => $receipt->taxAmount,
                        "shippingFee" => $receipt->shippingFee,
                        "grandTotalPrice" => $receipt->totalPrice,
                        "source" => $order !== null ? $order->source : null,
                        "medium" => $order !== null ? $order->medium : null,
                        "campaign" => $order !== null ? $order->campaign : null,
                        "term" => $order !== null ? $order->term : null,
                        "content" => $order !== null ? $order->content : null,
                        "isDirectTrial" => $order !== null && isset($order->isDirectTrial) ? ($order->isDirectTrial === 0 ? 'No' : 'Yes')  : null,
                    ];

                    array_push($options, $e);
                }

                Excel::store(new OrdersExport($options), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);
                $url = config('environment.aws.settings.bucket-url') . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }
}
