<?php

namespace App\Http\Controllers\Admin\Landing;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;

// Models
use App\Models\Admin\Admins;
use App\Models\Admin\Roles;
use App\Models\Admin\RoleDetails;
use App\Models\Admin\Paths;
use App\Models\Reports\QueueReport;

// Helpers
use App\Helpers\PermissionsHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Exception;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Auth;

class PagesController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->middleware('auth');
        // $this->middleware(function ($request, $next) {
        //     $this->userRole = null;
        //     $this->currentpath = request()->segments() ? request()->segments()[0] : "";

        //     if (Auth::guard()->check()) {
        //         $this->userRole = Auth::user()->RoleId;
        //         $this->permissionsHelper = new PermissionsHelper($this->userRole);
        //         $this->permissions = $this->permissionsHelper->GetPermissions($this->userRole);
        //         $this->pathpermissions = $this->permissionsHelper->GetPermissionsByPath($this->currentpath, $this->userRole);
        //     }
        //     else {
        //         $this->permissionsHelper = new PermissionsHelper();
        //         $this->permissions = $this->permissionsHelper->GetPermissions();
        //         $this->pathpermissions = $this->permissionsHelper->GetPermissionsByPath($this->currentpath);
        //     }

        //     view()->share('permissionsHelper', $this->permissionsHelper);
        //     view()->share('permissions', $this->permissions);
        //     view()->share('pathpermissions', $this->pathpermissions);
        //     return $next($request);
        // });
        // dd(session());
        $this->permissionsHelper = new PermissionsHelper();
        view()->share('permissionsHelper', $this->permissionsHelper);
    }

    public function index()
    {
        return view('_authenticated.dashboard');
    }

    public function resetPasswordView()
    {
        return view('auth.resetpassword');
    }

    public function orders()
    {
        if ($this->permissionsHelper->check('orders','canView')) {
            return view('_authenticated.orders.orders');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function orderDetails()
    {
        if ($this->permissionsHelper->check('orders/details','canView')) {
            return view('_authenticated.orders.orderDetails');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function bulkOrders()
    {
        if ($this->permissionsHelper->check('bulk-orders','canView')) {
            return view('_authenticated.orders.bulkOrders' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function bulkOrderDetails()
    {
        if ($this->permissionsHelper->check('bulk-orders/details','canView')) {
            return view('_authenticated.orders.bulkOrderDetails');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function subscribers()
    {
        if ($this->permissionsHelper->check('subscription/subscribers','canView')) {
            $countQueueReport = QueueReport::where(function ($query) {
                $query->orWhere('status',  'Pending')
                    ->orWhere('status', 'Processing');
            })
            ->where('type', 'subscribers')
            ->count();
            return view('_authenticated.subscription.subscribers' , compact('countQueueReport'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function subscriberDetails()
    {
        if ($this->permissionsHelper->check('subscription/subscribers/details','canView')) {
            return view('_authenticated.subscription.subscriberDetails' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function createMO()
    {
        if ($this->permissionsHelper->check('ba/generateMO','canView')) {
            return view('_authenticated.ba.generateMO' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function createBA()
    {
        if ($this->permissionsHelper->check('ba/generateBA','canView')) {
            return view('_authenticated.ba.generateBA' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function BAList()
    {
        if ($this->permissionsHelper->check('ba','canView')) {
            return view('_authenticated.ba.balist' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function BADetails()
    {
        if ($this->permissionsHelper->check('ba/details','canView')) {
            return view('_authenticated.ba.badetails' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function adminList()
    {
        if ($this->permissionsHelper->check('admins','canView')) {
            $roles = Roles::get()->all();
            return view('_authenticated.admin.admins')->with('roles', $roles);
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function adminRoles()
    {
        if ($this->permissionsHelper->check('admins/roles','canView')) {
            $data = Roles::get()->all();
            return view('_authenticated.admin.roles')->with('data', $data);
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function adminRoleDetails($id)
    {
        if ($this->permissionsHelper->check('admins/roles/details','canView')) {
            $data = RoleDetails::where('RoleId', $id)
            ->leftJoin('paths', 'roledetails.PathId', '=', 'paths.id')
            ->select('roledetails.*','paths.pathValue')
            ->get();

            $role = Roles::find($id);

            return view('_authenticated.admin.roledetails', compact('data', 'role'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function addAdminRole()
    {
        if ($this->permissionsHelper->check('admins/roles','canCreate')) {
            $data = Paths::get()->all();

            return view('_authenticated.admin.addrole', compact('data'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function createAdminRole(Request $request)
    {
        if ($this->permissionsHelper->check('admins/roles','canCreate')) {
            $validatedData = $request->validate([
                'role_name' => 'required|max:50',
                'role_desc' => 'required',
            ]);
            $input = $request->all();

            $role = new Roles();
            $role->roleName = $input['role_name'];
            $role->description = $input['role_desc'];
            $role->created_at = Carbon::now();
            $role->updated_at = Carbon::now();
            $role->save();
            $roleid = $role->id;

            //create RoleDetail for each existing path
            foreach ($input['permission'] as $pathid => $pathpermissions) {
                $roledetail = new RoleDetails();
                $roledetail->RoleId = $roleid;
                $roledetail->PathId = $pathid;

                if (in_array('can_view',$pathpermissions)) {
                    $roledetail->canView = 1;
                }
                else {
                    $roledetail->canView = 0;
                }

                if (in_array('can_create',$pathpermissions)) {
                    $roledetail->canCreate = 1;
                }
                else {
                    $roledetail->canCreate = 0;
                }

                if (in_array('can_edit',$pathpermissions)) {
                    $roledetail->canEdit = 1;
                }
                else {
                    $roledetail->canEdit = 0;
                }

                if (in_array('can_upload',$pathpermissions)) {
                    $roledetail->canUpload = 1;
                }
                else {
                    $roledetail->canUpload = 0;
                }

                if (in_array('can_download',$pathpermissions)) {
                    $roledetail->canDownload = 1;
                }
                else {
                    $roledetail->canDownload = 0;
                }

                if (in_array('can_delete',$pathpermissions)) {
                    $roledetail->canDelete = 1;
                }
                else {
                    $roledetail->canDelete = 0;
                }

                if (in_array('can_reactive',$pathpermissions)) {
                    $roledetail->canReactive = 1;
                }
                else {
                    $roledetail->canReactive = 0;
                }

                $roledetail->save();
            }

            return redirect()->route('admin.admins.roles');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function editAdminRole(Request $request)
    {
        if ($this->permissionsHelper->check('admins/roles','canEdit')) {
            $validatedData = $request->validate([
                'role_name' => 'required|max:50',
                'role_desc' => 'required',
            ]);

            $input = $request->all();

            $role = Roles::find($input['role_id']);
            $role->roleName = $input['role_name'];
            $role->description = $input['role_desc'];
            $role->updated_at = Carbon::now();
            $role->save();

            //create RoleDetail for each existing path
            foreach ($input['permission'] as $pathid => $pathpermissions) {
                $roledetail = RoleDetails::find($pathid);

                if (in_array('can_view',$pathpermissions)) {
                    $roledetail->canView = 1;
                }
                else {
                    $roledetail->canView = 0;
                }

                if (in_array('can_create',$pathpermissions)) {
                    $roledetail->canCreate = 1;
                }
                else {
                    $roledetail->canCreate = 0;
                }

                if (in_array('can_edit',$pathpermissions)) {
                    $roledetail->canEdit = 1;
                }
                else {
                    $roledetail->canEdit = 0;
                }

                if (in_array('can_upload',$pathpermissions)) {
                    $roledetail->canUpload = 1;
                }
                else {
                    $roledetail->canUpload = 0;
                }

                if (in_array('can_download',$pathpermissions)) {
                    $roledetail->canDownload = 1;
                }
                else {
                    $roledetail->canDownload = 0;
                }

                if (in_array('can_delete',$pathpermissions)) {
                    $roledetail->canDelete = 1;
                }
                else {
                    $roledetail->canDelete = 0;
                }

                if (in_array('can_reactive',$pathpermissions)) {
                    $roledetail->canReactive = 1;
                }
                else {
                    $roledetail->canReactive = 0;
                }

                $roledetail->save();
            }

            return redirect()->route('admin.admins.roles');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function adminPaths()
    {
        if ($this->permissionsHelper->check('admins/paths','canView')) {
            $data = Paths::get()->all();
            return view('_authenticated.admin.paths')->with('data', $data);
        }
    }

    public function createAdminPath(Request $request)
    {
        if ($this->permissionsHelper->check('admins/paths','canCreate')) {
            $validatedData = $request->validate([
                'add_path_value' => 'required|max:50',
            ]);

            $input = $request->all();
            $path = new Paths();
            $path->pathValue = $input['add_path_value'];
            $path->created_at = Carbon::now();
            $path->updated_at = Carbon::now();
            $path->save();
            $pathid = $path->id;

            $roleids = Roles::pluck('id')->all();

            //create RoleDetail for each existing role
            foreach ($roleids as $roleid) {
                $roledetail = new RoleDetails();
                $roledetail->RoleId = $roleid;
                $roledetail->PathId = $pathid;

                $roledetail->canView = 0;
                $roledetail->canCreate = 0;
                $roledetail->canEdit = 0;
                $roledetail->canUpload = 0;
                $roledetail->canDownload = 0;
                $roledetail->canDelete = 0;
                $roledetail->canReactive = 0;

                $roledetail->save();
            }

            return redirect()->route('admin.admins.paths');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function editAdminPath(Request $request)
    {
        if ($this->permissionsHelper->check('admins/paths','canEdit')) {
            $validatedData = $request->validate([
                'edit_path_value' => 'required|max:50',
            ]);

            $input = $request->all();

            $path = Paths::find($input['edit_path_id']);
            $path->pathValue = $input['edit_path_value'];
            $path->updated_at = Carbon::now();
            $path->save();
            $pathid = $path->id;

            $roleids = Roles::pluck('id')->all();

            //create RoleDetail if does not already exist
            foreach ($roleids as $roleid) {
                $roledetail = RoleDetails::where('RoleId', $roleid)->where('PathId', $pathid)->first();

                if (!$roledetail) {
                    $roledetail = new RoleDetails();
                    $roledetail->RoleId = $roleid;
                    $roledetail->PathId = $pathid;

                    $roledetail->canView = 0;
                    $roledetail->canCreate = 0;
                    $roledetail->canEdit = 0;
                    $roledetail->canUpload = 0;
                    $roledetail->canDownload = 0;
                    $roledetail->canDelete = 0;
                    $roledetail->canReactive = 0;

                    $roledetail->save();
                }
            }

            return redirect()->route('admin.admins.paths');
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function getCustomerUsers()
    {
        if ($this->permissionsHelper->check('users/customers','canView')) {
            return view('_authenticated.users.customerUsers' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function getCustomerDetails()
    {
        if ($this->permissionsHelper->check('users/customers/details','canView')) {
            return view('_authenticated.users.customerDetails' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function cancellation()
    {
        if ($this->permissionsHelper->check('subscription/cancellation','canView')) {
            return view('_authenticated.cancellation.cancellations' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function referral()
    {
        if ($this->permissionsHelper->check('referral','canView')) {
            return view('_authenticated.referral.referrals' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function referralDetails()
    {
        if ($this->permissionsHelper->check('referral/details','canView')) {
            return view('_authenticated.referral.referralDetails' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function referralcashout()
    {
        if ($this->permissionsHelper->check('referral/cashout','canView')) {
            return view('_authenticated.referral-cash-out.referralsCashOut' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function referralcashoutDetails()
    {
        if ($this->permissionsHelper->check('referral/cashout/details','canView')) {
            return view('_authenticated.referral-cash-out.referralCashOutDetails' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function exportList(){
        // $allowed = $this->permissionsHelper->CheckPermissionsByPathByAction($this->userRole, 'referral/details', 'canView');
        // if ($allowed) {
        //     return view('_authenticated.referral.referralDetails' );
        // }
        // else {
            return view('_authenticated.export.export-list');
        // }
    }

    public function recharge()
    {
        if ($this->permissionsHelper->check('subscription/recharge','canView')) {
            return view('_authenticated.subscription.recharge' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function pauseplan()
    {
        if ($this->permissionsHelper->check('subscription/pauseplan','canView')) {
            return view('_authenticated.pauseplan.pauseplans' );
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function reportmaster()
    {
        if ($this->permissionsHelper->check('reports/master','canView')) {
            $countQueueReport = QueueReport::where(function ($query) {
                $query->orWhere('status',  'Pending')
                    ->orWhere('status', 'Processing');
            })
            ->where('type', 'reportsmaster')
            ->count();
            return view('_authenticated.reports.master'  , compact('countQueueReport'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function reportappco()
    {
        if ($this->permissionsHelper->check('reports/appco','canView')) {
            $countQueueReport = QueueReport::where(function ($query) {
                $query->orWhere('status',  'Pending')
                    ->orWhere('status', 'Processing');
            })
            ->where('type', 'reportappco')
            ->count();
            return view('_authenticated.reports.appco'  , compact('countQueueReport'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }

    public function reportmo()
    {
        if ($this->permissionsHelper->check('reports/mo','canView')) {
            $countQueueReport = QueueReport::where(function ($query) {
                $query->orWhere('status',  'Pending')
                    ->orWhere('status', 'Processing');
            })
            ->where('type', 'reportmo')
            ->count();
            return view('_authenticated.reports.mo'  , compact('countQueueReport'));
        }
        else {
            return redirect()->route('admin.dashboard');
        }
    }
}