<?php

namespace App\Http\Controllers;

use App\Admin\AdminRoles;
use Illuminate\Http\Request;

class AdminRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function show(AdminRoles $adminRoles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminRoles $adminRoles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminRoles $adminRoles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminRoles $adminRoles)
    {
        //
    }
}
