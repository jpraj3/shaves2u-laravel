<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrdersExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "orderId","badgeId","agentName","channelType","eventLocationCode","moCode","customerName",
            "email","SSN","deliveryAddress","deliveryContactNumber","billingAddress","billingContactNumber","trackingNumber","promoCode","discountPrice",
            "productName","sku","region","category","currency","createdAt","saleDate","paymentType",
            "status", "cancellationReason","canceledDate","totalPrice","subTotalPrice","discountAmount","taxAmount","shippingFee","grandTotalPrice","source","medium","campaign",
            "term","content","isDirectTrial"
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //     ];
    // }


    public function array(): array
    {
        return $this->options;
    }

}
