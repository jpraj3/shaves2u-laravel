<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admins extends Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    protected $table = 'admins';

    protected $guard = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'email',
        'password',
        'firstName',
        'lastName',
        'staffId',
        'isActive',
        'viewAllCountry',
        'phone',
        'AdminRoleId',
        'CountryId',
        'RoleId',
        'MarketingOfficeId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
