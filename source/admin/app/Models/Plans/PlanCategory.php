<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanCategory extends Model
{
    protected $table = 'plan_categories';
    //
    protected $fillable = [
        'name',
        'type',
    ];

}
