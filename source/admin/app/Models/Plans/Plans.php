<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';
    //
    protected $fillable = [
        'price',
        'sellPrice',
        'trialPrice',
        'description',
        'discountPercent',
        'discountAmount',
        'tax',
        'taxAmount',
        'plantype',
        'isOnline',
        'isOffline',
        'rating',
        'order',
        'PlanSkuId',
        'CountryId'
    ];
}
