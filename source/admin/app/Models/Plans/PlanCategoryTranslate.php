<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanCategoryTranslate extends Model
{
    protected $table = 'plan_category_translates';
    //
    protected $fillable = [
        'langCode',
        'translate',
        'PlanCategoryId',
    ];

}
