<?php

namespace App\Models\Rebates;

use Illuminate\Database\Eloquent\Model;

class RewardsOut extends Model {
    protected $table = 'rewardsouts';
    //
    protected $fillable = [
        'CountryId',
        'UserId',
        'value',
        'rewardscurrencyId',
    ];

}
