<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductCountry extends Model {
    public $timestamps = false;
    protected $table = 'productcountries';
    //
    protected $fillable = [
        'basedPrice',
        'sellPrice',
        'qty',
        'maxQty',
        'isOnline',
        'isOffline',
        'created_at',
        'updated_at',
        'CountryId',
        'ProductId',
        'order'
    ];

}
