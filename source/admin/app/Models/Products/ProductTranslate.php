<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductTranslate extends Model {
    public $timestamps = false;
    protected $table = 'producttranslates';
    //
    protected $fillable = [
        'name',
        'description',
        'shortDescription',
        'langCode',
        'ProductId',
        'seoTitle'
    ];

}
