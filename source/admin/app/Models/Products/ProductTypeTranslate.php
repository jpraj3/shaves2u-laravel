<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductTypeTranslate extends Model {
    public $timestamps = false;
    protected $table = 'producttypetranslates';
    //
    protected $fillable = [
        'name',
        'langCode',
        'created_at',
        'updated_at',
        'ProductTypeId'
    ];

}
