<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {
    public $timestamps = false;
    protected $table = 'productimages';
    //
    protected $fillable = [
        'url',
        'isDefault',
        'ProductId'
    ];

}
