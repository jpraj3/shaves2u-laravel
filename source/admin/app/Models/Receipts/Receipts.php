<?php

namespace App\Models\Receipts;

use Illuminate\Database\Eloquent\Model;

class Receipts extends Model {
    protected $table = 'receipts';
    //
    protected $fillable = [
        'chargeId',
        'last4',
        'branchName',
        'expiredYear',
        'expiredMonth',
        'chargeFee',
        'chargeCurrency',
        'subTotalPrice',
        'discountAmount',
        'shippingFee',
        'taxAmount',
        'totalPrice',
        'currency',
        'originPrice',
        'OrderId',
        'SubscriptionId'
    ];

}
