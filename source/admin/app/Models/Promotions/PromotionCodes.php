<?php

namespace App\Models\Promotions;

use Illuminate\Database\Eloquent\Model;

class PromotionCodes extends Model
{
    protected $table = 'promotioncodes';
    //
    protected $fillable = [
        'code',
        'isValid',
        'email',
        'PromotionId',
        'promotion_type',
    ];
}
