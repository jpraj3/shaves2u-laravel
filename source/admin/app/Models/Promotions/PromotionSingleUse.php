<?php

namespace App\Models\Promotions;

use Illuminate\Database\Eloquent\Model;

class PromotionSingleUse extends Model
{
    protected $table = 'promotion_single_use';
    //
    protected $fillable = [
        'PromotionId',
        'startDate',
        'endDate',
        'UserId',
        'AllowedUsage',
        'isUsed',
    ];
}
