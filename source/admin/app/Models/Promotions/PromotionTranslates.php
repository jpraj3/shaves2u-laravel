<?php

namespace App\Models\Promotions;

use Illuminate\Database\Eloquent\Model;

class PromotionTranslates extends Model
{
    public $timestamps = false;
    protected $table = 'promotiontranslates';
    //
    protected $fillable = [
        'name',
        'description',
        'langCode',
        'PromotionId',
    ];
}
