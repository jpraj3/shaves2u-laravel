<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class PaymentHistories extends Model {
    protected $table = 'paymenthistories';
    //
    protected $fillable = [
        'message',
        'isRemark',
        'OrderId',
        'reason',
        'created_by',
        'updated_by',
    ];

}
