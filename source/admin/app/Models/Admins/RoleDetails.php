<?php

namespace App\Models\Admins;

use Illuminate\Database\Eloquent\Model;

class RoleDetails extends Model
{
    protected $table = 'roledetails';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'canView',
        'canCreate',
        'canEdit',
        'canDelete',
        'canUpload',
        'canDownload',
        'canReactive',
        'PathId',
        'RoleId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
