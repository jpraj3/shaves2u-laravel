<?php

namespace App\Models\Admins;

use Illuminate\Database\Eloquent\Model;

class Paths extends Model
{
    protected $table = 'paths';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'pathValue',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
