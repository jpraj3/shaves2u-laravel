<?php

namespace App\Models\GeoLocation;

use Illuminate\Database\Eloquent\Model;

class LanguageDetails extends Model
{
    // Table Name
    protected $table = 'languagedetails';
    // Table Column
    protected $fillable = [
        'CountryId',
        'mainlanguageCode',
        'sublanguageCode',
        'sublanguageName'
    ];
}
