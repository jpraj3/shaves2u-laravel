<?php

namespace App\Models\GeoLocation;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    public $timestamps = false;
    // Table Name
    protected $table = 'countries';
    // Table Column
    protected $fillable = [
        'code',
        'codeIso',
        'name',
        'currencyCode',
        'currencyDisplay',
        'defaultLang',
        'callingCode',
        'isActive',
        'companyName',
        'companyRegistrationNumber',
        'companyAddress',
        'taxRate',
        'taxName',
        'taxCode',
        'includedTaxToProduct',
        'taxNumber',
        'email',
        'phone',
        'isBaEcommerce',
        'isBaSubscription',
        'isBaAlaCarte',
        'isBaStripe',
        'isWebEcommerce',
        'isWebSubscription',
        'isWebAlaCarte',
        'isWebStripe',
        'shippingMinAmount',
        'shippingFee',
        'shippingTrialFee',
        'order',
    ];
}
