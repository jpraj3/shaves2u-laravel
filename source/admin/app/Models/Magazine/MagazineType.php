<?php

namespace App\Models\Magazine;

use Illuminate\Database\Eloquent\Model;

class MagazineType extends Model
{
    protected $table = 'magazinetypes';
    //
    protected $fillable = [
        'id',
        'type',
        'langCode',
        'CountryId'
    ];
}
