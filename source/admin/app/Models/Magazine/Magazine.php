<?php

namespace App\Models\Magazine;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    protected $table = 'magazines';
    //
    protected $fillable = [
        'bannerUrl',
        'bannerAlt',
        'hashTag',
        'views',
        'title',
        'content',
        'postBy',
        'ArticleTypeId',
        'isFeatured',
        'URL',
        'META_description',
    ];
}
