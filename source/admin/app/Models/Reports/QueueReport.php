<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class QueueReport extends Model
{
    protected $table = 'queue_reports';
    protected $fillable = [
        'id',
        'type',
        'status',
        'fileuploadsId',
        'total',
        'current'
    ];
}
