<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class DeliveryAddresses extends Model
{
    protected $table = 'deliveryaddresses';
    //
    protected $fillable = [
        'id', 
        'firstName', 
        'lastName', 
        'fullName', 
        'contactNumber', 
        'SSN', 
        'state', 
        'address', 
        'portalCode', 
        'city', 
        'company', 
        'isRemoved', 
        'isBulkAddress', 
        'CountryId', 
        'UserId', 
        'SellerUserId', 
        'createdBy', 
        'updateBy', 
        'created_at', 
        'updated_at',
    ];
}
