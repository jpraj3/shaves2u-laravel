<?php

namespace App\Models\BulkOrders;

use Illuminate\Database\Eloquent\Model;

class BulkOrderHistories extends Model
{
    protected $table = 'bulkorderhistories';
    //
    protected $fillable = [
        'message',
        'isRemark',
        'BulkOrderId',
    ];

}
