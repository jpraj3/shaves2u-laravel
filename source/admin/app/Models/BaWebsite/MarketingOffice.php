<?php

namespace App\Models\BaWebsite;

use Illuminate\Database\Eloquent\Model;

class MarketingOffice extends Model
{
    protected $table = 'marketingoffices';
    //
    protected $fillable = [
        'moCode',
        'organization',
    ];
}
