<?php

namespace App\Models\Privacies;

use Illuminate\Database\Eloquent\Model;

class Privacies extends Model
{
    protected $table = 'privacies';
     public $timestamps = false;
    protected $fillable = [
        'content',
        'langCode',
        'CountryId',
    ];
}
