<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SubscriptionsProductAddOns extends Model
{
      //
 protected $table = 'subscriptions_productaddon';
 //public $timestamps = false;
    protected $fillable = [
        'subscriptionId',
        'ProductCountryId',
        'differentprice',
        'description',
        'qty',
        'cycle',
        'skipFirstCycle',
        'isAnnual',
        'duration',
        'totalCycle',
        'isWithTrialKit'
    ];
}
