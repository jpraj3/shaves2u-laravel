<?php

namespace App\Models\Audits;

use Illuminate\Database\Eloquent\Model;

class Audits extends Model
{
    protected $table = 'audits';
    //
    protected $fillable = [
        'user_type',
        'user_id',
        'event',
        'auditable_type',
        'audit_title',
        'audit_description',
        'old_values',
        'new_values',
        'ip_address',
        'url',
        'user_agent'
    ];
}
