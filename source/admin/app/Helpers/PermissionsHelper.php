<?php

namespace App\Helpers;

use App\Models\Admin\Admins;
use App\Models\Admin\Roles;
use App\Models\Admin\RoleDetails;
use App\Models\Admin\Paths;
use Auth;

class PermissionsHelper
{
    public function __construct() {
        $this->userRole = null;
    }

    public function GetPermissions($roleId = null)
    {
        $this->userRole = Auth::check() ? Auth::user()->RoleId : null;

        $pathpermissions = [];
        $permissions = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $this->userRole ? $this->userRole : ($roleId ? $roleId : ""))
                        ->get();
        
        if ($permissions!= null) {
            foreach ($permissions as $permission) {
                $actions['canView'] = $permission->canView;
                $actions['canCreate'] = $permission->canCreate;
                $actions['canEdit'] = $permission->canEdit;
                $actions['canDelete'] = $permission->canDelete;
                $actions['canUpload'] = $permission->canUpload;
                $actions['canDownload'] = $permission->canDownload;
                $actions['canReactive'] = $permission->canReactive;
                $pathpermissions[$permission->pathValue] = $actions;
            }
        }
        else {
            // if permissions not found, return all false
            $paths = Paths::all()->get();
            foreach ($paths as $path) {
                $actions['canView'] = 0;
                $actions['canCreate'] = 0;
                $actions['canEdit'] = 0;
                $actions['canDelete'] = 0;
                $actions['canUpload'] = 0;
                $actions['canDownload'] = 0;
                $actions['canReactive'] = 0;
                $pathpermissions[$path->pathValue] = $actions;
            }
        }

        return $pathpermissions;
    }

    public function GetPermissionsByPath($pathname, $roleId = null)
    {
        $this->userRole = Auth::check() ? Auth::user()->RoleId : null;

        $pathpermissions = [];
        $permissions = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $this->userRole ? $this->userRole : ($roleId ? $roleId : ""))
                        ->where('paths.pathValue', 'LIKE', $pathname.'%' )
                        ->get();

        if ($permissions!= null) {
            foreach ($permissions as $permission) {
                $actions['canView'] = $permission->canView;
                $actions['canCreate'] = $permission->canCreate;
                $actions['canEdit'] = $permission->canEdit;
                $actions['canDelete'] = $permission->canDelete;
                $actions['canUpload'] = $permission->canUpload;
                $actions['canDownload'] = $permission->canDownload;
                $actions['canReactive'] = $permission->canReactive;
                $pathpermissions[$permission->pathValue] = $actions;
            }
        }
        else {
            // if permissions not found, return all false
            $paths = Paths::all()->get();
            foreach ($paths as $path) {
                $actions['canView'] = 0;
                $actions['canCreate'] = 0;
                $actions['canEdit'] = 0;
                $actions['canDelete'] = 0;
                $actions['canUpload'] = 0;
                $actions['canDownload'] = 0;
                $actions['canReactive'] = 0;
                $pathpermissions[$path->pathValue] = $actions;
            }
        }

        return $pathpermissions;
    }

    //check if has permission for path exactly matching $pathname
    public function check($pathname, $action, $roleId = null)
    {
        $this->userRole = Auth::check() ? Auth::user()->RoleId : null;

        $permission = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $this->userRole ? $this->userRole : ($roleId ? $roleId : ""))
                        ->where('paths.pathValue', $pathname)
                        ->pluck('roledetails.'.$action)
                        ->first();

        if ($permission) {
            if ($permission === 1 || $permission === "1") {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    //check if has permission for any path beginning with $pathname
    public function checkRoot($pathname, $action, $roleId = null)
    {
        $this->userRole = Auth::check() ? Auth::user()->RoleId : null;

        $permission = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $this->userRole ? $this->userRole : ($roleId ? $roleId : ""))
                        ->where('paths.pathValue', 'LIKE', $pathname.'%')
                        ->where('roledetails.'.$action, "1")
                        ->get();

        if ($permission->isNotEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public function checkMult($paths, $action, $roleId = null)
    {
        $this->userRole = Auth::check() ? Auth::user()->RoleId : null;

        $permission = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $this->userRole ? $this->userRole : ($roleId ? $roleId : ""))
                        ->whereIn('paths.pathValue', $paths)
                        ->where('roledetails.'.$action, "1")
                        ->get();

        if ($permission->isNotEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public function CheckPermissionsByPathByAction($roleId, $pathname, $action)
    {
        $permission = RoleDetails::leftJoin('paths','roledetails.PathId', '=', 'paths.id')
                        ->where('roledetails.RoleId', $roleId)
                        ->where('paths.pathValue', $pathname)
                        ->pluck('roledetails.'.$action)
                        ->first();

        if ($permission) {
            if ($permission === 1 || $permission === "1") {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
