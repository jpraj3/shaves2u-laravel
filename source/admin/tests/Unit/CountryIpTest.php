<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountryIpTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetIp()
    {
        $client_ip = file_get_contents("http://ipecho.net/plain");
        echo "\n";
        echo " ========= IP Address Detected: \n" . $client_ip . "\n";
        // Curl Init (With IP & Access Key)
        $ip_stack_request = curl_init('http://api.ipstack.com/' . $client_ip . '?access_key=2c878ac336a0a28ab63caed03dc92cc1');
        curl_setopt($ip_stack_request, CURLOPT_RETURNTRANSFER, true);
        // Storing Data (in JSON)
        $ip_stack_response = curl_exec($ip_stack_request);
        curl_close($ip_stack_request);
        // Decode JSON Response
        $ip_stack_results = json_decode($ip_stack_response, true);
        // View Results
        echo "\n";
        echo " ========= Country Detected: \n" . json_encode($ip_stack_results) . "\n";
        echo "\n";
    }

    
}
