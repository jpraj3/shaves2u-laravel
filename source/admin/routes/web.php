<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
Route::get('/resetpassword', 'Admin\Landing\PagesController@resetPasswordView')->name('admin.password.forgot');
Route::post('/resetpassword', 'Admin\AdminController@resetPassword')->name('admin.users.password.reset');

Route::group(['middleware' => 'auth'], function () {
    // Reports & Exports
    Route::group(['prefix' => 'export'], function () {
        Route::post('/orders', 'ExportsController@orders')->name('export.admin.orders');
        Route::get('/test', 'ExportsController@orders')->name('test.export');
    });

    Route::get('/', 'Admin\Landing\PagesController@index')->name('admin.dashboard');

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'Admin\Landing\PagesController@orders')->name('admin.orders');
        Route::get('/details/{id}', 'Admin\Landing\PagesController@orderDetails')->name('admin.orders.details');
    });

    Route::group(['prefix' => 'bulkorders'], function () {
        Route::get('/', 'Admin\Landing\PagesController@bulkOrders')->name('admin.bulkorders');
        Route::get('/details/{id}', 'Admin\Landing\PagesController@bulkOrderDetails')->name('admin.bulkorders.details');
    });

    Route::group(['prefix' => 'subscription'], function () {
        Route::group(['prefix' => 'subscribers'], function () {
            Route::get('/', 'Admin\Landing\PagesController@subscribers')->name('admin.subscription.subscribers');
            Route::get('/details/{id}', 'Admin\Landing\PagesController@subscriberDetails')->name('admin.subscription.subscribers.details');
        });
        
        Route::group(['prefix' => 'recharge'], function () {
            Route::get('/', 'Admin\Landing\PagesController@recharge')->name('admin.subscription.recharge');
        });
        
        Route::group(['prefix' => 'cancellation'], function () {
            Route::get('/', 'Admin\Landing\PagesController@cancellation')->name('admin.subscription.cancellation');
        });

        Route::group(['prefix' => 'pauseplan'], function () {
            Route::get('/', 'Admin\Landing\PagesController@pauseplan')->name('admin.subscription.pauseplan');
        });
    });

    Route::group(['prefix' => 'referral'], function () {
        Route::get('/', 'Admin\Landing\PagesController@referral')->name('admin.referral');
        Route::get('/details/{id}', 'Admin\Landing\PagesController@referralDetails')->name('admin.referral.details');
        Route::get('/cashout', 'Admin\Landing\PagesController@referralcashout')->name('admin.referral-cash-out');
        Route::get('/cashout/details/{id}', 'Admin\Landing\PagesController@referralcashoutDetails')->name('admin.referral-cash-out.details');
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::group(['prefix' => 'master'], function () {
            Route::get('/', 'Admin\Landing\PagesController@reportmaster')->name('admin.reports.master');
        });
        Route::group(['prefix' => 'appco'], function () {
            Route::get('/', 'Admin\Landing\PagesController@reportappco')->name('admin.reports.appco');
        });
        
        Route::group(['prefix' => 'mo'], function () {
            Route::get('/', 'Admin\Landing\PagesController@reportmo')->name('admin.reports.mo');
        });
    });

    Route::group(['prefix' => 'ba'], function () {
        Route::get('/generateMO', 'Admin\Landing\PagesController@createMO')->name('admin.ba.generateMO');
        Route::get('/generateBA', 'Admin\Landing\PagesController@createBA')->name('admin.ba.generateBA');
        Route::get('/', 'Admin\Landing\PagesController@BAList')->name('admin.ba.list');
    });

    Route::group(['prefix' => 'admins'], function () {
        Route::get('/', 'Admin\Landing\PagesController@adminList')->name('admin.admins');

        Route::group(['prefix' => 'roles'], function () {
            Route::get('/', 'Admin\Landing\PagesController@adminRoles')->name('admin.admins.roles');
            Route::get('/details/{id}', 'Admin\Landing\PagesController@adminRoleDetails')->name('admin.admins.roles.details');
            Route::get('/add', 'Admin\Landing\PagesController@addAdminRole')->name('admin.admins.roles.add');
            Route::post('/create', 'Admin\Landing\PagesController@createAdminRole')->name('admin.admins.roles.create');
            Route::post('/edit', 'Admin\Landing\PagesController@editAdminRole')->name('admin.admins.roles.edit');
        });

        Route::group(['prefix' => 'paths'], function () {
            Route::get('/', 'Admin\Landing\PagesController@adminPaths')->name('admin.admins.paths');
            Route::post('/create', 'Admin\Landing\PagesController@createAdminPath')->name('admin.admins.paths.create');
            Route::post('/edit', 'Admin\Landing\PagesController@editAdminPath')->name('admin.admins.paths.edit');
        });
    });

    Route::get('/export-list', 'Admin\Landing\PagesController@exportList')->name('admin.export');

    Route::get('/bulkorders', 'Admin\Landing\PagesController@bulkorders')->name('admin.bulkorders');

    Route::group(['prefix' => 'users'], function () {
        Route::get('/customers', 'Admin\Landing\PagesController@getCustomerUsers')->name('users.customers');
        Route::get('/customers/details/{id}', 'Admin\Landing\PagesController@getCustomerDetails')->name('users.customers.details');
    });
});
