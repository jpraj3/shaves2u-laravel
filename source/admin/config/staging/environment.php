<?php
$a = dirname(__FILE__);
$b = realpath($a);
$c = explode('/',$b);
$d = end($c);
$DIR_NAME = $d;
$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
$API_KEY_PATH = config('app.env') !== $DIR_NAME ? '' : '/home/dcistaging/shaves2u/s2u-laravel-config/config/'.config('app.appType') . '/config.' . config('app.env') . '.json';
$GET_CONFIG_CONTENTS = $API_KEY_PATH === '' ? '' : json_decode(file_get_contents($API_KEY_PATH), true);
return [
    'webUrl' => $protocol . '://dcistaging.com/shaves2u/bawebsite',
    'apiUrl' => $protocol . '://dcistaging.com/shaves2u/ecommerce/api',
    'emailUrl' => $protocol . '://dcistaging.com/shaves2u/ecommerce/',
    'CUSTOM_CONFIG_PATH' => $API_KEY_PATH,
    'CUSTOM_CONFIG_DATA' => $GET_CONFIG_CONTENTS,
    'IpStack' => [
        'accesskey' => '2c878ac336a0a28ab63caed03dc92cc1',
        'url' => 'http://api.ipstack.com/',
        'example' => 'http://api.ipstack.com/175.138.185.1?access_key=2c878ac336a0a28ab63caed03dc92cc1',
    ],
    'aws' => [
        'settings' => [
            'bucket-url' => 'https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/',
            'keyPath' => '/home/dcistaging/shaves2u/s2u-laravel-config/aws-key.json',
            'userAvatarAlbum' => 'user-avatars',
            'version' => '2012-10-17',
            'bucketName' => 'shaves2u-dci',
            'testUpload' => 'shaves2u-laravel-test',
            'planPanelAlbum' => 'plan-panel',
            'productImagesAlbum' => 'product-images',
            'planImagesAlbum' => 'plan-images',
            'promotionImagesAlbum' => 'promotion-images',
            'filesUploadAlbum' => 'files-upload-local',
            'csvReportsUpload' => 'csv-reports-upload-local',
            'localDownloadFolder' => '/home/dcistaging/shaves2u/s2u-laravel-config/aws-output-files',
            'localCSVReportFolder' => '/home/dcistaging/shaves2u/s2u-laravel-config/aws-output-files/csv-report-files',
            'limitDownload' => 2000,
        ],
    ],

    'taxInvoice' => [
        'taxInvoicePath' => '/home/dcistaging/shaves2u/s2u-laravel-config/taxInvoice-upload-files',
    ],

    'warehouse-interface' => [
        'folderName' => 'warehouse-interface-',
        'orderCancel' => 'order-cancel-outbound',
        'orderConfirmation' => 'order-confirmation-outbound',
        'orderInbound' => 'order-inbound-outbound',
    ],

    'warehouse_paths' => [
        'MYS' => [
            'local_upload_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-my-upload-files',
            'local_download_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-my-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-my-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-my-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'HKG' => [
            'local_upload_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-hk-upload-files',
            'local_download_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-hk-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-hk-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-hk-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'SGP' => [
            'local_upload_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/urbanfox-upload-files',
            'local_download_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/urbanfox-output-files',
        ],
        'KOR' => [
            'local_upload_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-kr-upload-files',
            'local_download_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-kr-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-kr-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-kr-ftp-test/inbox/new-site-test',
            'pre_convert' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-kr-output-files/pre_convert',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'TWN' => [
            'local_upload_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-tw-upload-files',
            'local_download_folder' => '/home/dcistaging/shaves2u/s2u-laravel-config/warehouse-tw-output-files',
            'ftp_outbox_folder' => '/shaves2u-dci-tw-ftp-test/outbox/new-site-test',
            'ftp_inbox_folder' => '/shaves2u-dci-tw-ftp-test/inbox/new-site-test',
            'retry' => 5,
            'maxFiles' => 10,
        ],
    ],

    'aftership' => [
        'trackingUrl' => 'https://s2ustaging.aftership.com',
    ],

    'urbanFox' => [
        'host' => 'https://ims.urbanfox.asia/graphiql',
        'trackingUrl' => 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number=',
    ],

    'deliverDateForKorea' => '2018-10-01',

    'trialPlan' => [
        'baFirstDelivery' => '13 days',
        'webFirstDelivery' => '20 days',
        'startFirstDelivery' => '13 days',
        'followUpEmail1' => '10 days',
        'followUpEmail2' => '7 days',
        'followUpEmail5Days' => '5 days',
    ],
];
