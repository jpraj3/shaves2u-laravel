let reportappmo = [];
let countries = null;
let currentpage = null;
let pages = null;

function ajaxGetCountries(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

// function ajaxGetSKUs(handleResponse) {
//     try {
//         $.ajax ({
//             url: API_URL + '/admin/skus/list',
//             type: "POST",
//             data: {
//                 appType: appType
//             },
//             dataType: "json",
//             success: function(response){
//                 console.log(response);
//                 handleResponse(response);
//             }
//         });
//     }
//     catch (err) {
//         //
//     }
// }

function ajaxGetReportsMO() {
    reportappmo = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/reports/mo/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                // channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                // sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetReportsMOBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetReportsMOBlock() {
    $.ajax ({
        url: API_URL + '/admin/reports/mo',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            // channel: $("#filterByChannel").val(),
            country: $("#filterByCountry").val(),
            // sku: $("#filterBySKU").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReportsMO(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetReportsMOBlock(currentpage);
            }
        }
    });
}


function ajaxSearchReportsMO() {
    reportappmo = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/reports/mo/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchReportsMOBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchReportsMOBlock() {
    $.ajax ({
        url: API_URL + '/admin/reports/mo',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReportsMO(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchReportsMOBlock(currentpage);
            }
        }
    });
}

function refreshReportsMO(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        reportappmo = reportappmo.filter(obj => obj.id !== value.id);

        reportappmo.push(value);
    });
    reportappmo = reportappmo.sort(function(a, b) {
        return b.id - a.id;
    });

    generateReportsMOTable();
}

function generateReportsMOTable() {
    $("#reports_mo_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#reports_mo_list tr").remove();

    var resultsCount = 0;
    if (reportappmo.length >= 1) {
        $.each( reportappmo, function( key, value ) {
            let order_no = value['order_no'] == null ? "" : value['order_no'];
            let badgeId = value['badgeId'] == null ? "" : value['badgeId'];
            let eventLocationCode = value['eventLocationCode'] == null ? "" : value['eventLocationCode'];
            let channelType = value['channelType'] == null ? "" : value['eventLocationCode'];
            let taxInvoiceNo = value['taxInvoiceNo'] == null ? "" : value['channelType'];
            let created_at = value['created_at'] == null ? "" : value['created_at'];
            let sku = value['sku'] == null ? "" : value['sku'];
            let status = value['status'] == null ? "" : value['status'];

            if (order_no.indexOf($("#filterBySearch").val()) != -1 || taxInvoiceNo.indexOf($("#filterBySearch").val()) != -1 || sku.indexOf($("#filterBySearch").val()) != -1 ) {
                $("#reports_mo_list").append('<tr><td>' + order_no + 
                '</td><td>' + badgeId + 
                '</td><td>' + taxInvoiceNo + 
                '</td><td>' + channelType + 
                '</td><td>' + sku + 
                '</td><td>' + created_at + 
                '</td><td>' + eventLocationCode + 
                '</td><td>' + status + 
                '</td></tr>');
                resultsCount++;
            }
        });
        if (resultsCount < 1) {
            $("#reports_mo_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount)
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#reports_mo_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

$(document).ready(function() {
    
    ajaxGetCountries(function(output){
        $.each(output, function( key, value ) {
            $("#filterByCountry").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });
    
    // ajaxGetSKUs(function(output){
    //     $.each(output, function( key, value ) {
    //         $("#filterBySKU").append('<option value="' + value['sku'] + '">' + value['sku'] + '</option>');
    //     });
    // });

    // ajaxGetReportsMO();

    $("#start_filter").on('click',function () {
        ajaxGetReportsMO();
    });

    // $(".filterReportMO").change(function() {
    //     ajaxGetReportsMO();
    // });

    // $("#filterBySearch").change(function() {
    //     generateReportsMOTable();
    //     // ajaxSearchReportsMO();
    // });
});