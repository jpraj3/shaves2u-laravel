let reportmaster = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;
function ajaxGetCountries(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

// function ajaxGetSKUs(handleResponse) {
//     try {
//         $.ajax ({
//             url: API_URL + '/admin/skus/list',
//             type: "POST",
//             data: {
//                 appType: appType
//             },
//             dataType: "json",
//             success: function(response){
//                 console.log(response);
//                 handleResponse(response);
//             }
//         });
//     }
//     catch (err) {
//         //
//     }
// }

function ajaxGetReportsMaster() {
    reportmaster = [];

    try {
        $.ajax({
            url: API_URL + '/admin/reports/master/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                // sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetReportsMasterBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetReportsMasterBlock(pagenum) {
    $.ajax({
        url: API_URL + '/admin/reports/master',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            channel: $("#filterByChannel").val(),
            country: $("#filterByCountry").val(),
            // sku: $("#filterBySKU").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: pagenum,
        },
        dataType: "json",
        success: function (response) {
            if (response) {
                if (response.length > 0) {
                    refreshReportsMaster(response);
                    currentpage = currentpage + 1;
                    console.log(currentpage);
                    ajaxGetReportsMasterBlock(currentpage);
                }
            }
        }
    });
}

function ajaxSearchReportsMaster() {
    reportmaster = [];

    try {
        $.ajax({
            url: API_URL + '/admin/reports/master/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchReportsMasterBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchReportsMasterBlock() {
    $.ajax({
        url: API_URL + '/admin/reports/master',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function (response) {
            refreshReportsMaster(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchReportsMasterBlock(currentpage);
            }
        }
    });
}

function refreshReportsMaster(data) {
    $.each(data, function (key, value) {
        // remove duplicate data according to id
        reportmaster = reportmaster.filter(obj => obj.id !== value.id);

        reportmaster.push(value);
    });
    reportmaster = reportmaster.sort(function (a, b) {
        return b.id - a.id;
    });

    // let table = $('#reports_table').DataTable({
    //     bDestroy: true,
    //     pageLength: 50,
    //     lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
    //     autoWiDth: false,
    //     searching: false,
    //     dom: "<l<t>ip>",
    //     order: [],
    // });

    generateReportsMasterTable();
}


function generateReportsMasterTable(table) {
    $("#reports_master_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#reports_master_list tr").remove();

    var resultsCount = 0;
    if (reportmaster.length >= 1) {
        $.each(reportmaster, function (key, value) {
            let order_no = value['order_no'] == null ? "" : value['order_no'];
            let taxInvoiceNo = value['taxInvoiceNo'] == null ? "" : value['taxInvoiceNo'];
            let created_at = value['created_at'] == null ? "" : value['created_at'];
            let category = value['category'] == null ? "" : value['category'];
            let sku = value['sku'] == null ? "" : value['sku'];
            let currency = value['currency'] == null ? "" : value['currency'];
            let region = value['region'] == null ? "" : value['region'];
            let status = value['status'] == null ? "" : value['status'];

            category = category === "sales app" ? "BA" : (category === "client" ? "Online / Ecommerce" : (category === "Bulk Order" ? "Bulk Order" : ""));
            if (order_no.indexOf($("#filterBySearch").val()) != -1 || taxInvoiceNo.indexOf($("#filterBySearch").val()) != -1 || sku.indexOf($("#filterBySearch").val()) != -1) {
                $("#reports_master_list").append('<tr><td>' + order_no +
                    '</td><td>' + taxInvoiceNo +
                    '</td><td>' + category +
                    '</td><td>' + sku +
                    '</td><td>' + created_at +
                    '</td><td>' + region +
                    '</td><td>' + status +
                    '</td></tr>');
                resultsCount++;
            }
        });
        if (resultsCount < 1) {
            $("#reports_master_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('#reports_table').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#reports_master_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

function initReportsMaster() {
    showLoading();
    $('#reports_table').dataTable().fnDestroy();

    $('#reports_table').DataTable({
        pageLength: 50,
        lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
        autoWidth: false,
        searching: false,
        dom: "<l<t>ip>",
        order: [],
        bProcessing: true,
        bServerSide: true,
        sPaginationType: "full_numbers",
        ajax: {
            url: API_URL + '/admin/reports/master',
            type: "POST",
            data: {
                appType: appType,
                search: function () { return $("#filterBySearch").val() },
                channel: function () { return $("#filterByChannel").val() },
                country: function () { return $("#filterByCountry").val() },
                status: function () { return $("#filterByStatus").val() },
                fromdate: function () { return $("#filterByDateFrom").val() },
                todate: function () { return $("#filterByDateTo").val() },
            },
            dataType: "json",
            dataSrc: "data"
        },
        columns: [
            { data: 'order_no' },
            { data: 'taxInvoiceNo' },
            { data: 'category' },
            { data: 'sku' },
            { data: 'created_at' },
            { data: 'region' },
            { data: 'status' },
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return (data === "sales app" ? "BA" : (data === "client" ? "Online / Ecommerce" : (data === "Bulk Order" ? "Bulk Order" : "")));
                },
                targets: 2
            },
        ]
    });

    $('#reports_table').on('xhr.dt', function (e, settings, json, xhr) {
        // act on data
        hideLoading();
    });
}

$(document).ready(function () {

    ajaxGetCountries(function (output) {
        $.each(output, function (key, value) {
            $("#filterByCountry").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });

    // ajaxGetSKUs(function(output){
    //     $.each(output, function( key, value ) {
    //         $("#filterBySKU").append('<option value="' + value['sku'] + '">' + value['sku'] + '</option>');
    //     });
    // });

    // ajaxGetReportsMasterBlock(1);
    // initReportsMaster()
    
    $("#start_filter").on('click',function () {
        // ajaxGetReportsMasterBlock(1);
        showLoading();
        if (hasInitialized == 0) {
            hasInitialized = 1;
            initReportsMaster();
        } else {
            $('#reports_table').DataTable().ajax.reload();
        }
    });

    // $(".filterReportMaster").change(function () {
    //     // ajaxGetReportsMasterBlock(1);
    //     showLoading();
    //     if (hasInitialized == 0) {
    //         initReportsMaster();
    //     } else {
    //         $('#reports_table').DataTable().ajax.reload();
    //     }
    // });

    // var typingBuffer;
    // $("#filterBySearch").on('input', function () {
    //     // generateReportsMasterTable();
    //     // ajaxSearchReportsMaster();
    //     clearTimeout(typingBuffer);
    //     typingBuffer = setTimeout(function () {
    //         showLoading();
    //         if (hasInitialized == 0) {
    //             initReportsMaster();
    //         } else {
    //             $('#reports_table').DataTable().ajax.reload();
    //         }
    //     }, 500);
    // });
});