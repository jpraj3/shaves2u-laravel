function ajaxGetOrderDetails(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/details',
            type: "POST",
            data: {
                apptype: appType,
                id: bulkorderid,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        console.log(err);
    }
}

function ajaxChangeOrderStatus(handleResponse) {
    console.log("Initial Response: " + handleResponse);
    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: bulkorderid,
                status: $("#change_status").children("option:selected").val(),
                isRemark: 0,
            },
            dataType: "json",
            success: function (response) {
                console.log("Updated Response: " + handleResponse);
                handleResponse(response);
                $("#modal_status_confirm").modal('hide');
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxAddComment(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: bulkorderid,
                status: $("#order_log_message").val(),
                isRemark: 1,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxDownloadPDF(bulkorderid) {
    downloadurl = API_URL + '/admin/bulkorders/taxinvoice/download';
    try {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            var a;
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                // Trick for making downloadable link
                a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhttp.response);
                console.log(xhttp.response);
                // Give filename you wish to download
                a.download = xhttp.getResponseHeader('X-File-Name');
                a.style.display = 'none';
                document.body.appendChild(a);
                a.click();
            }
        };
        // Post data to URL which handles post request
        xhttp.open("POST", downloadurl);
        xhttp.setRequestHeader("Content-Type", "application/json");
        // You should set responseType as blob for binary responses
        xhttp.responseType = 'blob';
        xhttp.send({ id: bulkorderid });
    }
    catch (err) {
        //
    }
}

function ajaxUpdateTracking(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/tracking/update',
            type: "POST",
            data: {
                apptype: appType,
                id: bulkorderid,
                trackingNumber: $("#tracking_number").val(),
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.success) {
                    handleResponse(response.payload);
                }
                else {
                    $(".tracking-control").prop('disabled', false);
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshOrderDetails(data) {
    let fullname = data['fullName'] == null ? "" : data['fullName'];
    let email = data['email'] == null ? "" : data['email'];
    let subscriptionIds = data['subscriptionIds'] == null ? "" : data['subscriptionIds'];
    let order_no = data['order_no'] == null ? "" : data['order_no'];
    let taxInvoiceNo = data['taxInvoiceNo'] == null ? "" : data['taxInvoiceNo'];
    let taxInvoiceUrl = data['taxInvoiceUrl'] == null ? "" : data['taxInvoiceUrl'];
    let price = data['price'] == null ? "" : data['price'];
    let currency = data['currency'] == null ? "" : data['currency'];
    let subTotal = data['subTotal'] == null ? "" : data['subTotal'];
    let discountAmount = data['discountAmount'] == null ? "" : data['discountAmount'];
    let chargeFee = data['chargeFee'] == null ? "" : data['chargeFee'];
    let totalprice = data == null ? "" : data['totalprice'];
    let SellerUserId = data['SellerUserId'];
    let deliveryId = data['deliveryId'] == null ? "" : data['deliveryId'];
    let carrierAgent = data['carrierAgent'] == null ? "" : data['carrierAgent'];
    let paymentType = data['paymentType'] == null ? "" : data['paymentType'];
    let created_at_date = data['created_at_date'] == null ? "" : data['created_at_date'];
    let status = data['status'] == null ? "" : data['status'];
    let bulkorderlog = data['bulkorderlog'] == null ? "" : data['bulkorderlog'];
    let deliverydetails = data['deliverydetails'] == null ? "" : data['deliverydetails'];
    let firstName = deliverydetails == null ? "" : deliverydetails['firstName'];
    let contactNumber = deliverydetails == null ? "" : deliverydetails['contactNumber'];
    let address = deliverydetails == null ? "" : deliverydetails['address'];

    $("#full_name").text(fullname);
    $("#email").html('<a href="">' + email + '</a>');
    $("#subscription_id").html('<a href="">' + subscriptionIds + '</a>');
    $("#order_no").text(order_no);
    $("#tax_invoice_no").html('<a href="' + taxInvoiceUrl + '">' + taxInvoiceNo + '</a>');

    if (data['SellerUserId'] != null)
        $("#channel").text('BA');
    else
        $("#channel").text('Online / E-Commerce');

    if (deliveryId !== null && deliveryId !== "") {
        $("#tracking_div").remove();
    }
    else {
        $("#tracking_div").removeClass("d-none");
    }

    $("#tracking_no").html('<a href="https://track.aftership.com/' + carrierAgent + '/' + deliveryId + '">' + deliveryId + '</a>');

    if (paymentType === 'stripe') {
        $("#payment_method").text(branchName + " **** **** **** " + last4);
    }
    else {
        $("#payment_method").text(paymentType);
    }
    $("#created_date").text(created_at_date);

    $("#change_status").val(status);

    if (bulkorderlog != null) {
        $.each(bulkorderlog, function (key, value) {
            $("#order_log").append('<div class="row"> \
                                        <div class="col-4"> \
                                            <span>' + value['created_at_date'] + '</span> \
                                        </div> \
                                        <div class="col-6"> \
                                            <span>' + value['message'] + '</span> \
                                        </div> \
                                    </div>');
        });
    }

    $("#shipping_name").text(firstName);
    $("#shipping_contact").text(contactNumber);
    $("#shipping_address").text(address);


    $.each(data['bulkorderdetails'], function (key, value) {
        $("#invoice").prepend('<tr><td>' + value['sku'] +
            '</td><td class="text-right">' + value['qty'] +
            '</td><td class="text-right">' + currency + ' ' + value['unit_price'] +
            '</td><td class="text-right">' + currency + ' ' + value['price'] +
            '</td></tr>');
    });

    $("#sub_total").text(currency + ' ' + subTotal);
    $("#discount").text(currency + ' ' + discountAmount);
    $("#processing_fee").text(currency + ' 0.00');
    $("#grand_total").text(currency + ' ' + totalprice);
    $("#cash_rebate").text(currency + ' 0.00');
    $("#total_payable").text(currency + ' ' + totalprice);
}

function refreshOrderLog(data) {
    $("#order_log span").remove();
    $.each(data, function (key, value) {
        $("#order_log").append('<div class="row"> \
                                    <div class="col-4"> \
                                        <span>' + value['created_at_date'] + '</span> \
                                    </div> \
                                    <div class="col-6"> \
                                        <span>' + value['message'] + '</span> \
                                    </div> \
                                </div>');
    });
}

$(document).ready(function () {
    ajaxGetOrderDetails(function (output) {
        refreshOrderDetails(output);
    });

    $("#change_status").change(function () {
        if ($(this).val() === "Delivering" || $(this).val() === "Completed" || $(this).val() === "Cancelled") {
            $("#modal_status_confirm").modal('show');
        }
        else {
            ajaxChangeOrderStatus(function (output) {
                refreshOrderLog(output);
            });
        }
    });

    $("#bulkorder_status_confirmed").click(function () {
        ajaxChangeOrderStatus(function (output) {
            refreshOrderLog(output);
        });
        $("#modal_refund_confirm").modal('hide');
    });

    $("#open_olm").click(function () {
        $("#order_log_add_comment").removeClass('d-none');
        $("#open_olm").addClass('d-none');
    });

    $("#cancel_olm").click(function () {
        $("#order_log_add_comment").addClass('d-none');
        $("#open_olm").removeClass('d-none');
    });

    $("#submit_olm").click(function () {
        if ($("#order_log_message").val().length >= 1) {
            ajaxAddComment(function (output) {
                refreshOrderLog(output);
            });
        }
        $("#order_log_add_comment").addClass('d-none');
        $("#open_olm").removeClass('d-none');
    });

    $("#update_tracking").click(function () {
        if ($("#tracking_number").val().length >= 1) {
            $(".tracking-control").prop('disabled', true);
            ajaxUpdateTracking(function (output) {
                $("#tracking_div").remove();
                $("#tracking_no").html('<a href="https://track.aftership.com/' + carrierAgent + '/' + output.trackingNumber + '">' + output.trackingNumber + '</a>');
                $("#change_status").val("Delivering");
                refreshOrderLog(output.bulkorderlog);
            });
        }
    });
});