let orderdata = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;

function ajaxGetCountries(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetSKUs(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/skus/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetOrders() {
    orderdata = [];

    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetOrderBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetOrderBlock() {
    $.ajax({
        url: API_URL + '/admin/bulkorders/list',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            country: $("#filterByCountry").val(),
            sku: $("#filterBySKU").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function (response) {
            refreshOrders(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetOrderBlock(currentpage);
            }
        }
    });
}

function ajaxSearchOrders() {
    orderdata = [];

    try {
        $.ajax({
            url: API_URL + '/admin/bulkorders/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchOrderBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchOrderBlock() {
    $.ajax({
        url: API_URL + '/admin/bulkorders/list',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function (response) {
            refreshOrders(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                // ajaxSearchOrderBlock(currentpage);
            }
            hideLoading();
        }
    });
}

function ajaxDownloadPDF(orderid) {
    downloadurl = API_URL + '/admin/bulkorders/taxinvoice/download';
    try {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            var a;
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                // Trick for making downloadable link
                a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhttp.response);
                console.log(xhttp.response);
                // Give filename you wish to download
                a.download = xhttp.getResponseHeader('X-File-Name');
                a.style.display = 'none';
                document.body.appendChild(a);
                a.click();
            }
        };
        // Post data to URL which handles post request
        xhttp.open("POST", downloadurl);
        xhttp.setRequestHeader("Content-Type", "application/json");
        // You should set responseType as blob for binary responses
        xhttp.responseType = 'blob';
        xhttp.send({ id: orderid });
    }
    catch (err) {
        //
    }
}

function refreshOrders(data) {
    $.each(data, function (key, value) {
        // remove duplicate data according to id
        orderdata = orderdata.filter(obj => obj.id !== value.id);

        orderdata.push(value);
    });
    orderdata = orderdata.sort(function (a, b) {
        return b.id - a.id;
    });

    generateOrdersTable();
}


function generateOrdersTable() {
    $("#orders_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#orders_list tr").remove();

    var resultsCount = 0;
    if (orderdata.length >= 1) {
        $.each(orderdata, function (key, value) {
            let bulkorder_no = value['bulkorder_no'] == null ? "" : value['bulkorder_no'];
            let taxInvoiceNo = value['taxInvoiceNo'] == null ? "" : value['taxInvoiceNo'];
            let created_at = value['created_at'] == null ? "" : value['created_at'];
            let email = value['email'] == null ? "" : value['email'];
            let skus = value['skus'] == null ? "" : value['skus'];
            let currency = value['currency'] == null ? "" : value['currency'];
            let totalPrice = value['totalPrice'] == null ? "" : value['totalPrice'];
            let status = value['status'] == null ? "" : value['status'];
            let taxinvoiceurl = value['taxinvoiceurl'] == null ? "" : value['taxinvoiceurl'];
            let canViewDetailsBtn = canViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/bulkorders/details/' + value['id'] + '" data-original-title="View Details"><i class="fa fa-search"></i></a>';
            let canDownloadBtn = canDownload === false || taxinvoiceurl == null ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled pdf-download" href="' + taxinvoiceurl + '"  data-original-title="Download"><i class="fa fa-download"></i></a>';

                $("#orders_list").append('<tr><td>' + bulkorder_no +
                    '</td><td>' + taxInvoiceNo +
                    '</td><td>' + created_at +
                    '</td><td><a href="">' + email +
                    '</td><td>' + skus +
                    '</td><td>' + currency + ' ' + totalPrice +
                    '</td><td>' + status +
                    '</td><td class="text-center">' + canViewDetailsBtn + canDownloadBtn + '</td></tr>');
                resultsCount++;
            
        });
        if (resultsCount < 1) {
            $("#orders_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#orders_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}



function bulkorderupload() {
    event.preventDefault();
    var form = $('#bulkorderupload')[0];

    // Create an FormData object 
    var data = new FormData(form);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: API_URL + '/admin/bulkorders/upload/bulkorders',
        data: data,
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function (data) {
            console.log(data);

            if (data.status == "fail") {
                Swal.fire(
                    '',
                    data.reason,
                    'error'
                ).then((result) => {
                 location.reload();
                })
            } else {
                 location.reload();
            }
        },
        error: function () {
            $("#loading").css("display", "none");
        }
    });
};

$(document).ready(function () {

    ajaxGetCountries(function (output) {
        $.each(output, function (key, value) {
            $("#filterByCountry").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });

    ajaxGetSKUs(function (output) {
        $.each(output, function (key, value) {
            $("#filterBySKU").append('<option value="' + value['sku'] + '">' + value['sku'] + '</option>');
        });
    });

    $("#start_filter").on('click', function () {
        showLoading();

            ajaxGetOrders();
     
    });
//     $(".filterOrders").change(function () {
//         ajaxGetOrders();
//     });


// let typingTimer;               
// let doneSearchTypingInterval = 5000;  
// let searchInput = document.getElementById('filterBySearch');

// searchInput.addEventListener('keyup', () => {
//     clearTimeout(typingTimer);
//     if (searchInput.value) {
//         typingTimer = setTimeout(doneSearchTyping, doneSearchTypingInterval);
//     }
// });

// function doneSearchTyping () {
//     showLoading();
//     ajaxSearchOrders();
// }

    // $("#filterBySearch").keyup(function () {
    //     ajaxGetOrders();
    //     // generateOrdersTable();
    //     // ajaxSearchOrders();
    // });

    $("#bo-upload-btn").click(function () {
        let csvsection = document.getElementById("bo-csv-section");
        csvsection.style.display = "block";
        let csvbtn = document.getElementById("bo-upload-btn");
        csvbtn.style.display = "none";
        let bolist = document.getElementById("bo-list-section");
        bolist.style.display = "none";
    });

    $("#bo-csv-cancel-btn").click(function () {
        let csvsection = document.getElementById("bo-csv-section");
        csvsection.style.display = "none";
        let csvbtn = document.getElementById("bo-upload-btn");
        csvbtn.style.display = "block";
        let bolist = document.getElementById("bo-list-section");
        bolist.style.display = "block";
    });
});