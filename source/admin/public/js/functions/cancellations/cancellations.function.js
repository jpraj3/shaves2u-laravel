let cancellationsdata = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;

function ajaxGetCancellations(handleResponse) {
    cancellationsdata = [];

    try {
        $.ajax ({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: API_URL + '/admin/subscriptions/cancellations/count',
            type: "POST",
            data: {
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                // country: $("#filterByCountry").val(),
                // plantype: $("#filterByPlanType").val(),
                // sku: $("#filterBySKU").val(),
                // status: $("#filterByStatus").val(),
                search: $("#filterBySearch").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetCancellationsBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetCancellationsBlock() {
    $.ajax ({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: API_URL + '/admin/subscriptions/cancellations',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            // country: $("#filterByCountry").val(),
            // plantype: $("#filterByPlanType").val(),
            // sku: $("#filterBySKU").val(),
            // status: $("#filterByStatus").val(),
            search: $("#filterBySearch").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshCancellations(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetCancellationsBlock(currentpage);
            }
        }
    });
}

function ajaxSearchCancellations(handleResponse) {
    cancellationsdata = [];

    try {
        $.ajax ({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: API_URL + '/admin/subscriptions/cancellations/count',
            type: "POST",
            data: {
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchCancellationsBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchCancellationsBlock() {
    $.ajax ({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: API_URL + '/admin/subscriptions/cancellations',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshCancellations(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchCancellationsBlock(currentpage);
            }
        }
    });
}

function refreshCancellations(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        cancellationsdata = cancellationsdata.filter(obj => obj.id !== value.id);

        cancellationsdata.push(value);
    });
    cancellationsdata = cancellationsdata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateCancellationsTable();
}


function generateCancellationsTable() {
    $("#cancellations_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#cancellations_list tr").remove();

    var resultsCount = 0;
    if (cancellationsdata.length >= 1) {
        $.each( cancellationsdata, function( key, value ) {
            let email = value['email'] ? value['email'] : "";
            let defaultContent = value['defaultContent'] ? value['defaultContent'] : "-";
            let Canceled = value['hasCancelled'] ? value['hasCancelled'] : "-";
            if(Canceled == 1){
                Canceled = "Yes";
            }
            else{
                Canceled = "-";
            }
            let OtherReason = value['OtherReason'] ? value['OtherReason'] : "-";
            if(defaultContent == "-"){
                defaultContent = OtherReason;
            }

            let created_date = value['created_date'] ? value['created_date'] : "";
            let updated_date = value['updated_date'] ? value['updated_date'] : "";
            let planType = value['planType'] ? value['planType'] : "";
            let planname = value['subscriptionName'] ? value['subscriptionName'] : "";
            let userActionText = value['userActionText'] ? value['userActionText'] : "";
            let plantype = "";

            if (value['email'] != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#cancellations_list").append('<tr><td>' + email + 
                    '</td><td>' + defaultContent + 
                    '</td><td>' + Canceled + 
                    '</td><td>' + userActionText + 
                    '</td><td>' + planType + ": " + planname +
                    '</td><td>' + created_date + 
                    '</tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#cancellations_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#cancellations_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

$(document).ready(function() {
    $("#start_filter").on('click', function () {
        showLoading();

        ajaxGetCancellations();
     
    });
    // ajaxGetCancellations();

    // $(".filterCancellations").change(function() {
    //     ajaxGetCancellations();
    // });

    // $("#filterBySearch").change(function() {
    //     generateCancellationsTable();
    //     // ajaxSearchCancellations();
    // });
});