let pauseplandata = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;

function ajaxGetPausePlans(handleResponse) {
    pauseplandata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/subscriptions/pauseplans/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                fmonth: $("#filterByMonth").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetPausePlanBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetPausePlanBlock() {
    $.ajax ({
        url: API_URL + '/admin/subscriptions/pauseplans',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            fmonth: $("#filterByMonth").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshPausePlans(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetPausePlanBlock(currentpage);
            }
        }
    });
}

function ajaxSearchPausePlans(handleResponse) {
    pauseplandata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/subscriptions/pauseplans/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchPausePlanBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchPausePlanBlock() {
    $.ajax ({
        url: API_URL + '/admin/subscriptions/pauseplans',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshPausePlans(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchPausePlanBlock(currentpage);
            }
        }
    });
}

function refreshPausePlans(data) {
    $.each(data, function( key, value ) {
        pauseplandata.push(value);
    });
    pauseplandata = pauseplandata.sort(function(a, b) {
        return b.id - a.id;
    });

    generatePausePlansTable();
}


function generatePausePlansTable() {
    $("#pauseplans_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#pauseplans_list tr").remove();

    var resultsCount = 0;
    if (pauseplandata.length >= 1) {
        $.each( pauseplandata, function( key, value ) {
            let email = value['email'] ? value['email'] : "";
            let sid = value['sid'] ? value['sid'] : "";
            let originaldate = value['originaldate'] ? value['originaldate'] : "";
            let resumedate = value['resumedate'] ? value['resumedate'] : "";
            let pausemonth = value['pausemonth'] ? value['pausemonth'] : "";
            let created_date = value['created_date'] ? value['created_date'] : "";

            if (value['email'] != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#pauseplans_list").append('<tr><td>' + email + 
                    '</td><td>' + sid + 
                    '</td><td>' + originaldate + 
                    '</td><td>' + resumedate + 
                    '</td><td>' + pausemonth + " months" +
                    '</td><td>' + created_date + 
                     '</tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#pauseplans_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#pauseplans_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

$(document).ready(function() {
    
    $("#start_filter").on('click', function () {
        showLoading();

        ajaxGetPausePlans();
     
    });
    // ajaxGetPausePlans();

    // $(".filterPausePlan").change(function() {
    //     ajaxGetPausePlans();
    // });

    // $("#filterBySearch").change(function() {
    //     generatePausePlansTable();
    //     // ajaxSearchPausePlans();
    // });
});