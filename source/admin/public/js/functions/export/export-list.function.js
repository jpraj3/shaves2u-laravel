let exportdata = [];
let currentpage = null;
let pages = null;

function ajaxGetExports() {
    exportdata = [];
    console.log("<?php echo Auth::user()->id; ?>");
    try {
        $.ajax({
            url: API_URL + "/admin/export/count",
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
                adminId: adminId
            },
            dataType: "json",
            success: function(response) {
                console.log(response);
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetExportsBlock();
            }
        });
    } catch (err) {
        //
    }
}

function ajaxGetExportsBlock() {
    $.ajax({
        url: API_URL + "/admin/export/list",
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
            adminId: adminId
        },
        dataType: "json",
        success: function(response) {
            refreshExports(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage);
                ajaxGetExportsBlock(currentpage);
            }
        }
    });
}

function refreshExports(data) {
    console.log(data);
    $.each(data, function(key, value) {
        exportdata.push(value);
    });
    exportdata = exportdata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateExportsTable();
}

function generateExportsTable() {
    $("#exports_list tr").remove();
    $(".js-dataTable-buttons")
        .dataTable()
        .fnDestroy();
    $("#exports_list tr").remove();

    var resultsCount = 0;
    if (exportdata.length >= 1) {
        console.log(exportdata);
        $.each(exportdata, function(key, value) {
            let filename = value["name"] == null ? "" : value["name"];
            let type = value["isReport"] == 1 ? "Report" : "Lists";
            let details = value["reportType"] == null ? "" : value["reportType"];
            let date = value["created_date"] == null ? "" : value["created_date"];
            let url = value["url"] == null ? "" : value["url"];
            let status = value["status"] == null ? "" : value["status"];

            if(status == "Completed"){
                if (value["name"] != null) {
                    if (filename.indexOf($("#filterBySearch").val()) != -1) {
                        $("#exports_list").append(
                            "<tr><td>" +
                                filename +
                                "</td><td>" +
                                type +
                                "</td><td>" +
                                details +
                                "</td><td>" +
                                date +
                                '</td><td class="text-center"> \
                                    <a class="btn btn-sm btn-success js-tooltip-enabled" href="' +
                                url +
                                '">'+status+'</a> \
                        </td></tr>'
                        );
                        resultsCount++;
                    }
                }
            }else{
                if (value["name"] != null) {
                    if (filename.indexOf($("#filterBySearch").val()) != -1) {
                        $("#exports_list").append(
                            "<tr><td>" +
                                filename +
                                "</td><td>" +
                                type +
                                "</td><td>" +
                                details +
                                "</td><td>" +
                                date +
                                '</td><td class="text-center"> \
                                    <a class="btn btn-sm btn-light js-tooltip-enabled disabled" href="">'+status+'</a> \
                        </td></tr>'
                        );
                        resultsCount++;
                    }
                }
            }

        });

        console.log(resultsCount);

        if (resultsCount < 1) {
            $("#exports_list").append(
                '<tr><td class="text-center" colspan="8">No Items</td></tr>'
            );
        } else {
            console.log(resultsCount);
            $(".js-dataTable-buttons").DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: []
            });
        }
    } else {
        $("#exports_list").append(
            '<tr><td class="text-center" colspan="8">No Items</td></tr>'
        );
    }
}

$(document).ready(function() {
    ajaxGetExports();

    $(".filterExport").change(function() {
        ajaxGetExports();
    });

    $("#filterBySearch").change(function() {
        generateExportsTable();
    });
});
