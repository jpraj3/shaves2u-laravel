let terms = null;

function ajaxGetCountries(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetSales(handleResponse) {
    console.log($("#country_select").val());
    try {
        $.ajax ({
            url: API_URL + '/admin/sales/list',
            type: "POST",
            data: { 
                appType:appType,
                timeperiod: $(".reloadChart .active").val(),
                countryid: $("#country_select").val()
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetOrders(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/orders/list',
            type: "POST",
            data: { 
                appType:appType,
                maxresults: 10,
                datesort: 'DESC'
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetUsers(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/user/list',
            type: "POST",
            data: { 
                appType:appType,
                maxresults: 10 
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxDownloadPDF(orderid) {
    downloadurl = API_URL + '/admin/admin/orders/taxinvoice/download';
    try {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            var a;
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                // Trick for making downloadable link
                a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhttp.response);
                console.log(xhttp.response);
                // Give filename you wish to download
                a.download = xhttp.getResponseHeader('X-File-Name');
                a.style.display = 'none';
                document.body.appendChild(a);
                a.click();
            }
        };
        // Post data to URL which handles post request
        xhttp.open("POST", downloadurl);
        xhttp.setRequestHeader("Content-Type", "application/json");
        // You should set responseType as blob for binary responses
        xhttp.responseType = 'blob';
        xhttp.send({id:orderid});
    }
    catch (err) {
        //
    }
}

function refreshOrders(data) {
    $("#orders_list tr").remove();
    if (data.length >= 1) {
        $.each( data, function( key, value ) {
            let order_no = value['order_no'] == null ? "" : value['order_no'];
            let taxInvoiceNo = value['taxInvoiceNo'] == null ? "" : value['taxInvoiceNo'];
            let created_at = value['created_at'] == null ? "" : value['created_at'];
            let email = value['email'] == null ? "" : value['email'];
            let skus = value['skus'] == null ? "" : value['skus'];
            let currency = value['currency'] == null ? "" : value['currency'];
            let totalPrice = value['totalPrice'] == null ? "" : value['totalPrice'];
            let status = value['status'] == null ? "" : value['status'];
            let taxinvoiceurl = value['taxinvoiceurl'] == null ? "" : value['taxinvoiceurl'];
            let canDownloadBtn = ordersCanDownload === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled pdf-download" href="' + taxinvoiceurl + '"  data-original-title="Download" ><i class="fa fa-download"></i></a>';
            let canViewDetailsBtn = ordersCanViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/orders/details/' + value['id'] +'" data-original-title="View Details"><i class="fa fa-search"></i></a>';

            var channel = null;
            if (value['SellerUserId'] != null) {
                channel = 'BA';
            }
            else {
                channel = 'Online / Ecommerce';
            }

            $("#orders_list").append('<tr><td>' + order_no + 
            '</td><td>' + taxInvoiceNo + 
            '</td><td>' + created_at + 
            '</td><td>' + email + 
            '</td><td>' + skus + 
            '</td><td>' + currency + ' ' + totalPrice + 
            '</td><td>' + channel + 
            '</td><td>' + status + 
            '</td><td class="text-center">'+canViewDetailsBtn+canDownloadBtn+'</td></tr>');
        });
    }
    else {
        $("#orders_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

function refreshUsers(data) {
    $("#users_list tr").remove();
    
    var resultsCount = 0;
    if (data.length >= 1) {
        $.each( data, function( key, value ) {
            let email = value['email'] == null ? "" : value['email'];
            let firstName = value['firstName'] == null ? "" : value['firstName'];
            let lastName = value['lastName'] == null ? "" : value['lastName'];
            let created_at = value['created_at'] == null ? "" : value['created_at'];
            let phone = value['phone'] == null ? "" : value['phone'];
            // let canDownloadBtn = usersCanDownload === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled pdf-download" data-original-title="Download""><i class="fa fa-download"></i></a>';
            let canViewDetailsBtn = usersCanViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" data-original-title="View Details"><i class="fa fa-search"></i></a>';
            
            $("#users_list").append('<tr><td>' + email + 
            '</td><td>' + firstName + lastName +
            '</td><td>' + created_at + 
            '</td><td>' + phone + 
            '</td><td class="text-center">'+canViewDetailsBtn+'</td></tr>');
            resultsCount++;
        });
    }
    else {
        $("#users_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

function updateChart(chart, data) {
    chart.data.labels = [];
    chart.data.datasets.forEach((dataset) => {
        dataset.data = [];
        dataset.backgroundColor = [];
    });

    let currency = data.currency;
    let sales = data.sales;
    let total_amount = 0;

    $.each( sales, function( key, value ) {
        total_amount = total_amount + parseFloat(value['total_day_sales']);

        chart.data.labels.push(value['date_group']);
        chart.data.datasets.forEach((dataset) => {
            dataset.data.push(value['total_day_sales']);
            dataset.backgroundColor.push('rgba(0, 128, 104, 1)');
        });
    });
    chart.update();
    $("#currency").html(currency);
    $("#total_amount").html(total_amount.toFixed(2));
}

$(document).ready(function() {
    ajaxGetCountries(function(countries){
        $.each(countries, function( key, value ) {
            $("#country_select").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    
        ajaxGetSales(function(sales){
            updateChart(myBarChart, sales);
        });
    });

    $("#country_select option:first-child").attr("selected", "selected");

    var ctxB = document.getElementById("barChart").getContext('2d');
    var myBarChart = new Chart(ctxB, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                data: [],
                backgroundColor: []
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    $('.reloadChart > button').on('click', function (e) {
        $('.reloadChart > button').removeClass('active');
        this.classList.add('active');
        ajaxGetSales(function(output){
            updateChart(myBarChart, output);
        });
    });

    $("#country_select").change(function() {
        ajaxGetSales(function(output){
            updateChart(myBarChart, output);
        });
    });

    ajaxGetOrders(function(output){
        refreshOrders(output);
    });

    ajaxGetUsers(function(output){
        refreshUsers(output);
    });
});