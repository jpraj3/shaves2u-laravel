let referraldata = [];
let countries = null;
let currentpage = null;
let pages = null;

function ajaxGetReferrals(handleResponse) {
    referraldata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/referrals/details/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                id: userid,
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetReferralBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetReferralBlock() {
    $.ajax ({
        url: API_URL + '/admin/referrals/details',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            id: userid,
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReferrals(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetReferralBlock(currentpage);
            }
        }
    });
}

function refreshReferrals(data) {
    $.each(data, function( key, value ) {
        referraldata.push(value);
    });
    referraldata = referraldata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateReferralsTable();
}


function generateReferralsTable() {
    $("#referrals_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#referrals_list tr").remove();

    var resultsCount = 0;
    if (referraldata.length >= 1) {
        $.each( referraldata, function( key, value ) {
          
            let remail = value['remail'] ? value['remail'] : "";
            let femail = value['femail'] ? value['femail'] : "";
            let orderid = value['OrderId'] ? value['OrderId'] : "-";
            let currencyDisplay = value['currencyDisplay'] ? value['currencyDisplay'] : ""; 
            let channel = value['source'] ? value['source'] : "-";
            let status = value['newstatus'] ? value['newstatus'] : "-";
            let credit = value['rewvalue'] ? currencyDisplay+value['rewvalue'] : currencyDisplay+"0"; 
            let date = value['created_at'] ? value['created_at'] : "-";

            if (value['remail'] != null && value['femail'] != null ) {
               
                    $("#ruser-email").html(remail);
                    $("#referrals_list").append('<tr><td>' + femail + 
                    '</td><td>' + orderid + 
                    '</td><td>' + channel + 
                    '</td><td>' + status + 
                    '</td><td>' + credit +
                    '</td><td>' + date + 
                    '</td></tr>');
                    resultsCount++;
                
            }
        });
        if (resultsCount < 1) {
            $("#referrals_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount)
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#referrals_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

$(document).ready(function() {
   
    ajaxGetReferrals();

    $(".filterReferrals").change(function() {
        ajaxGetReferrals(function(output){
            refreshReferrals(output);
        });
    });
});