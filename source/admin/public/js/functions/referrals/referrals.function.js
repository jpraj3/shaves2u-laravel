let referraldata = [];
let countries = null;
let currentpage = null;
let pages = null;

function ajaxGetReferrals(handleResponse) {
    referraldata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/referrals/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetReferralBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetReferralBlock() {
    $.ajax ({
        url: API_URL + '/admin/referrals',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReferrals(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetReferralBlock(currentpage);
            }
        }
    });
}

function ajaxSearchReferrals(handleResponse) {
    referraldata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/referrals/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchReferralBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchReferralBlock() {
    $.ajax ({
        url: API_URL + '/admin/referrals',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReferrals(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchReferralBlock(currentpage);
            }
        }
    });
}

function refreshReferrals(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        referraldata = referraldata.filter(obj => obj.rid !== value.rid);

        referraldata.push(value);
    });
    referraldata = referraldata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateReferralsTable();
}


function generateReferralsTable() {
    $("#referrals_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#referrals_list tr").remove();

    var resultsCount = 0;
    if (referraldata.length >= 1) {
        $.each( referraldata, function( key, value ) {
            let email = value['email'] ? value['email'] : "";
            let currencyDisplay = value['currencyDisplay'] ? value['currencyDisplay'] : "";
            let conversion = value['conversion'] ? value['conversion'] : currencyDisplay+"0";
         
            let countrycode = value['countrycode'] ? value['countrycode'] : "";
            let cashoutmoney = value['cashoutmoney'] ? value['cashoutmoney'] : 0; 
            let currentcredit = value['rewvalue'] ? (value['rewvalue'] - cashoutmoney) : "0";
            let totalcredit = value['rewvalue'] ? value['rewvalue'] : "0"; 
          

            if (value['email'] != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#referrals_list").append('<tr><td>' + email + 
                    '</td><td>' + countrycode + 
                    '</td><td>' + conversion + 
                    '</td><td>' +  currencyDisplay + Number(currentcredit).toFixed(2) + 
                    '</td><td>' +  currencyDisplay + Number(totalcredit).toFixed(2) +
                    '</td><td class="text-center"> \
                    <a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/referral/details/' + value['rid'] +'" data-original-title="View Details"><i class="fa fa-search"></i></a> \
                    </td></tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#referrals_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#referrals_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

$(document).ready(function() {
    $("#start_filter").on('click', function () {
        showLoading();

        ajaxGetReferrals();
     
    });
    // ajaxGetReferrals();

    // $(".filterReferrals").change(function() {
    //     ajaxGetReferrals();
    // });

    // $("#filterBySearch").keyup(function() {
    //     generateReferralsTable();
    //     // ajaxSearchReferrals();
    // });
});