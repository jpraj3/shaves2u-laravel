function ajaxGetOrderDetails(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/details',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxChangeOrderStatus(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
                status: $("#change_status").children("option:selected").val(),
                isRemark: 0,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
                $("#modal_status_confirm").modal('hide');
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxAddStatusComment(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
                status: $("#order_log_message").val(),
                isRemark: 1,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxChangePaymentStatus(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/status/paymenthistoriesupdate',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
                status: $("#change_payment_status").children("option:selected").val(),
                isRemark: 0,
                adminId: adminId,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxAddPaymentStatusComment(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/status/paymenthistoriesupdate',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
                status: $("#payment_log_message").val(),
                isRemark: 1,
                adminId: adminId
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxDownloadPDF(orderid) {
    downloadurl = API_URL + '/admin/orders/taxinvoice/download';
    try {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            var a;
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                // Trick for making downloadable link
                a = document.createElement('a');
                a.href = window.URL.createObjectURL(xhttp.response);
                console.log(xhttp.response);
                // Give filename you wish to download
                a.download = xhttp.getResponseHeader('X-File-Name');
                a.style.display = 'none';
                document.body.appendChild(a);
                a.click();
            }
        };
        // Post data to URL which handles post request
        xhttp.open("POST", downloadurl);
        xhttp.setRequestHeader("Content-Type", "application/json");
        // You should set responseType as blob for binary responses
        xhttp.responseType = 'blob';
        xhttp.send({ id: orderid });
    }
    catch (err) {
        //
    }
}

function ajaxUpdateTracking(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/orders/tracking/update',
            type: "POST",
            data: {
                apptype: appType,
                id: orderid,
                trackingNumber: $("#tracking_number").val(),
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.success) {
                    handleResponse(response.payload);
                }
                else {
                    $(".tracking-control").prop('disabled', false);
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshOrderDetails(data) {
    let fullname = data['fullName'] == null && data['fullName'] == 0 ? "" : data['fullName'];
    let email = data['email'] == null ? "" : data['email'];
    let subscriptionIds = data['subscriptionIds'] == null ? "" : data['subscriptionIds'];
    let order_no = data['order_no'] == null ? "" : data['order_no'];
    let taxInvoiceNo = data['taxInvoiceNo'] == null ? "" : data['taxInvoiceNo'];
    let taxInvoiceUrl = data['taxInvoiceUrl'] == null ? "" : data['taxInvoiceUrl'];
    let invoice = data['invoice'] == null ? null : data['invoice'];
    let chargeId = invoice == null ? "" : invoice['chargeId'];
    let branchName = invoice == null ? "" : invoice['branchName'];
    let last4 = invoice == null ? "" : invoice['last4'];
    let currency = invoice == null ? "" : invoice['currency'];
    let originPrice = invoice == null ? "" : invoice['originPrice'];
    let subTotalPrice = invoice == null ? "" : invoice['subTotalPrice'];
    let discountAmount = invoice == null ? "" : invoice['discountAmount'];
    let shippingFee = invoice == null ? "" : invoice['shippingFee'];
    let totalPrice = invoice == null ? "" : invoice['totalPrice'];
    let SellerUserId = data['SellerUserId'];
    let deliveryId = data['deliveryId'] == null ? "" : data['deliveryId'];
    let carrierAgent = data['carrierAgent'] == null ? "" : data['carrierAgent'];
    let paymentType = data['paymentType'] == null ? "" : data['paymentType'];
    let created_at_date = data['created_at_date'] == null ? "" : data['created_at_date'];
    let status = data['status'] == null ? "" : data['status'];
    let payment_status = data['payment_status'] == null ? "" : data['payment_status'];
    let orderlog = data['orderlog'];
    let paymentlog = data['paymentlog'];
    let deliverydetails = data['deliverydetails'];
    let firstName = deliverydetails == null ? "" : deliverydetails['firstName'];
    let lastName = deliverydetails == null ? "" : " " + deliverydetails['lastName'];
    fullname = fullname === "" ? firstName + lastName : "";
    let contactNumber = data['phone'] == null ? deliverydetails == "" ? "" : deliverydetails['contactNumber'] : data['phone'];
    let address = deliverydetails == null ? "" : deliverydetails['address'];

    $("#full_name").text(fullname);
    $("#email").html('' + email + '');
    $("#subscription_id").html('<a href="' + GLOBAL_URL + '/subscription/subscribers/details/' + subscriptionIds + '" target="_blank">' + subscriptionIds + '</a>');
    $("#order_no").text(order_no);
    $("#tax_invoice_no").html('<a href="' + taxInvoiceUrl + '">' + taxInvoiceNo + '</a>');
    if (paymentType === 'stripe') {
        $("#charge_id").html('<a href="https://dashboard.stripe.com/search?query=' + chargeId + '" target="_blank">' + chargeId + '</a>');
    }
    else {
        $("#charge_id").html(chargeId);
    }

    if (SellerUserId != null)
        $("#channel").text('BA');
    else
        $("#channel").text('Online / E-Commerce');

    if (deliveryId !== null && deliveryId !== "") {
        $("#tracking_div").remove();
    }
    else {
        $("#tracking_div").removeClass("d-none");
    }

    $("#tracking_no").html('<a href="https://track.aftership.com/' + carrierAgent + '/' + deliveryId + '">' + deliveryId + '</a>');

    if (paymentType === 'stripe') {
        $("#payment_method").text(branchName + " **** **** **** " + last4);
    }
    else {
        $("#payment_method").text(paymentType);
    }
    $("#created_date").text(created_at_date);

    $("#change_status").val(status);
    // console.log(payment_status);

    if (paymentType !== 'cash' || status !== "Cancelled") {
        $("#change_payment_status").val(payment_status);
        if (payment_status === "Refunded" || payment_status === "Cash") {
            $("#change_payment_status").prop('disabled', true);
        }
    }
    else {
        $("#change_payment_status").prop('disabled', true);
        $("#change_payment_status").val('');
    }

    if (orderlog != null) {
        $.each(orderlog, function (key, value) {
            $("#order_log").append('<div class="row"> \
                                        <div class="col-4"> \
                                            <span>' + value['created_at_date'] + '</span> \
                                        </div> \
                                        <div class="col-6"> \
                                            <span>' + value['message'] + '</span> \
                                        </div> \
                                    </div>');

        });
    }

    if (paymentlog != null) {
        $.each(paymentlog, function (key, value) {
            $("#payment_log").append('<div class="row"> \
                                        <div class="col-4"> \
                                            <span>' + value['created_at_date'] + '</span> \
                                        </div> \
                                        <div class="col-6"> \
                                            <span>' + value['message'] + '</span> \
                                        </div> \
                                    </div>');
        });
    }

    $("#shipping_name").text(firstName + lastName);
    $("#shipping_contact").text(contactNumber);
    $("#shipping_address").text(address);


    $.each(data['orderdetails'], function (key, value) {
        $("#invoice").prepend('<tr><td>' + value['sku'] +
            // '</td><td>' + value['name'] +
            '</td><td class="text-right">' + value['qty'] +
            '</td><td class="text-right">' + currency + ' ' + value['price'] +
            '</td><td class="text-right">' + currency + ' ' + value['total_price'] +
            '</td></tr>');
    });

    $("#sub_total").text(currency + ' ' + subTotalPrice);
    $("#discount").text(currency + ' ' + discountAmount);
    $("#processing_fee").text(currency + ' ' + shippingFee);
    $("#grand_total").text(currency + ' ' + originPrice);
    $("#cash_rebate").text(currency + ' 0.00');
    $("#total_payable").text(currency + ' ' + totalPrice);
}

function refreshOrderLog(data) {
    $("#order_log span").remove();
    $.each(data, function (key, value) {
        $("#order_log").append('<div class="row"> \
                                    <div class="col-4"> \
                                        <span>' + value['created_at_date'] + '</span> \
                                    </div> \
                                    <div class="col-6"> \
                                        <span>' + value['message'] + '</span> \
                                    </div> \
                                </div>');
    });
}

function refreshPaymentLog(data) {
    $("#payment_log span").remove();
    $.each(data, function (key, value) {
        $("#payment_log").append('<div class="row"> \
                                    <div class="col-4"> \
                                        <span>' + value['created_at_date'] + '</span> \
                                    </div> \
                                    <div class="col-6"> \
                                        <span>' + value['message'] + '</span> \
                                    </div> \
                                </div>');
    });
}

$(document).ready(function () {
    ajaxGetOrderDetails(function (output) {
        refreshOrderDetails(output);
    });

    $("#change_status").change(function () {
        if ($(this).val() === "Delivering" || $(this).val() === "Completed" || $(this).val() === "Cancelled") {
            $("#modal_status_confirm").modal('show');
        }
        else {
            ajaxChangeOrderStatus(function (output) {
                refreshOrderLog(output);
            });
        }
    });

    $("#order_status_confirmed").click(function () {
        ajaxChangeOrderStatus(function (output) {
            refreshOrderLog(output);
        });
        $("#modal_status_confirm").modal('hide');
    });

    $("#change_payment_status").change(function () {
        if ($(this).val() === "Refunded") {
            $("#modal_refund_confirm").modal('show');
        }
        else {
            if ($(this).val() === "Cash") {
            } else {
                ajaxChangePaymentStatus(function (output) {
                    refreshPaymentLog(output);
                });
            }
        }
    });

    $("#order_refund_confirmed").click(function () {
        ajaxChangePaymentStatus(function (output) {
            refreshPaymentLog(output);
        });
        $("#modal_refund_confirm").modal('hide');
    });

    $("#open_olm").click(function () {
        $("#order_log_add_comment").removeClass('d-none');
        $("#open_olm").addClass('d-none');
    });

    $("#cancel_olm").click(function () {
        $("#order_log_add_comment").addClass('d-none');
        $("#open_olm").removeClass('d-none');
    });

    $("#submit_olm").click(function () {
        if ($("#order_log_message").val().length >= 1) {
            ajaxAddStatusComment(function (output) {
                refreshOrderLog(output);
            });
        }
        $("#order_log_add_comment").addClass('d-none');
        $("#open_olm").removeClass('d-none');
    });

    $("#open_plm").click(function () {
        $("#payment_log_add_comment").removeClass('d-none');
        $("#open_plm").addClass('d-none');
    });

    $("#cancel_plm").click(function () {
        $("#payment_log_add_comment").addClass('d-none');
        $("#open_plm").removeClass('d-none');
    });

    $("#submit_plm").click(function () {
        if ($("#payment_log_message").val().length >= 1) {
            ajaxAddPaymentStatusComment(function (output) {
                refreshPaymentLog(output);
            });
        }
        $("#payment_log_add_comment").addClass('d-none');
        $("#open_plm").removeClass('d-none');
    });

    $("#update_tracking").click(function () {
        if ($("#tracking_number").val().length >= 1) {
            $(".tracking-control").prop('disabled', true);
            ajaxUpdateTracking(function (output) {
                $("#tracking_div").remove();
                $("#tracking_no").html('<a href="https://track.aftership.com/' + carrierAgent + '/' + output.trackingNumber + '">' + output.trackingNumber + '</a>');
                $("#change_status").val("Delivering");
                refreshOrderLog(output.orderlog);
            });
        }
    });
});