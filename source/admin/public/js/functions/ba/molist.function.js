let modata = [];

function ajaxGetMarketingOffices(handleResponse) {
    modata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/mo/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshMOs(data) {
    modata = data;

    generateMOTable();
}


function generateMOTable() {
    $("#mo_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#mo_list tr").remove();

    var resultsCount = 0;
    if (modata.length >= 1) {
        $.each( modata, function( key, value ) {
            let id = value['id'] == null ? "" : value['id'];
            let code = value['moCode'] == null ? "" : value['moCode'];
            let organization = value['organization'] == null ? "" : value['organization'];

            $("#mo_list").append('<tr><td>' + code + 
            '</td><td>' + organization + 
            '</td><td class="text-center"> \
            <button class="btn btn-sm btn-light js-tooltip-enabled edit-mo" data-mo-id="' + id +'" data-mo-code="' + code + '" data-mo-org="' + organization + '" data-toggle="modal" data-target="#modal_edit_mo" data-original-title="Edit MO"' + (canEdit ? '' : ' disabled') + '><i class="fa fa-edit"></i></button> \
            </td></tr>');
            resultsCount++;
        });
        if (resultsCount < 1) {
            $("#mo_list").append('<tr><td class="text-center" colspan="6">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
            
            $('.edit-mo').click(function(e) {
                let posturl = API_URL + '/admin/mo/update/' + $(this).attr('data-mo-id');
                console.log(posturl);
                $("#edit_mo").attr('action', posturl)
                $("#edit_mo_code").val( $(this).attr('data-mo-code') );
                $("#edit_mo_org").val( $(this).attr('data-mo-org') );
            });
        }
    }
    else {
        $("#mo_list").append('<tr><td class="text-center" colspan="6">No Items</td></tr>');
    }
}

function createNewMO() {
    $('button[type="submit"]').prop("disabled",true);
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: API_URL + '/admin/mo/create',
        data: $("#add_mo").serialize(),
        cache: false,
        success: function(data) {
            console.log(data);
            $('#modal_add_mo').modal('hide');
            $('button[type="submit"]').prop("disabled",false);
            ajaxGetMarketingOffices(function(output) {
                refreshMOs(output);
            });
        },
        error: function(jqXHR) {
            console.log(jqXHR);
        }
    });
}

function updateMO() {
    $('button[type="submit"]').prop("disabled",true);
    
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: $("#edit_mo").attr('action'),
        data: $("#edit_mo").serialize(),
        cache: false,
        success: function(data) {
            console.log(data);
            $('#modal_edit_mo').modal('hide');
            $('button[type="submit"]').prop("disabled",false);
            ajaxGetMarketingOffices(function(output) {
                refreshMOs(output);
            });
        },
        error: function(jqXHR) {
            console.log(jqXHR);
        }
    });
}

$(document).ready(function() {
    $("#add_mo").on("submit", function(event) {
        event.preventDefault();
        createNewMO();
    });

    $("#edit_mo").on("submit", function(event) {
        event.preventDefault();
        updateMO();
    });

    ajaxGetMarketingOffices(function(output){
        refreshMOs(output);
    })
});