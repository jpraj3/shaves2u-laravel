function ajaxGetCountries(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetMarketingOffices(handleResponse) {
    modata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/mo/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

$(document).ready(function() {
    $("#form_add_ba").on("submit", function(event) {
        event.preventDefault();
        $('button[type="submit"]').prop("disabled",true);
        
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + '/admin/ba/create',
            data: $("#form_add_ba").serialize(),
            cache: false,
            success: function(data) {
                console.log(data);
                window.location.href = GLOBAL_URL + '/ba';
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });
    $("#submit_csv").on("submit", function(event) {
        event.preventDefault();
        $("#submit_csv").attr('disabled', true);
        var form = $('#submit_csv')[0];
    
        // Create an FormData object 
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: API_URL + '/admin/ba/upload',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  // Important!
            contentType: false,
            cache: false,
            success: function (response) {
                console.log(response);
                if (response.success === "success") {
                    window.location.href = GLOBAL_URL + '/ba';
                }
                if (response.error) {
                    $("#upload-error").prop("hidden", false);
                    $("#error_msg").html(response.error);
                }
            },
            error: function () {
                $("#loading").css("display", "none");
                $("#submit_csv").attr('disabled', true);
            }
        });
    });

    ajaxGetCountries(function(output){
        $.each(output, function( key, value ) {
            $(".country-select").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });

    ajaxGetMarketingOffices(function(output){
        $.each(output, function( key, value ) {
            $(".mo-select").append('<option value="' + value['id'] + '">' + value['moCode'] + '</option>');
        });
    })
});