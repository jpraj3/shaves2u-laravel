let badata = [];
let countries = null;
let currentpage = null;
let pages = null;

function ajaxGetSellerUsers() {
    badata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/ba/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetSellerUserBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetSellerUserBlock() {
    $.ajax ({
        url: API_URL + '/admin/ba/list',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshBAs(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetSellerUserBlock(currentpage);
            }
        }
    });
}

function refreshBAs(data) {
    $.each(data, function( key, value ) {
        badata.push(value);
    });
    badata = badata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateBATable();
}


function generateBATable() {
    $("#ba_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#ba_list tr").remove();

    var resultsCount = 0;
    if (badata.length >= 1) {
        $.each( badata, function( key, value ) {
            let id = value['id'] == null ? "" : value['id'];
            let badgeid = value['badgeId'] == null ? "" : value['badgeId'];
            let name = value['agentName'] == null ? "" : value['agentName'];
            let country = value['country'] == null ? "" : value['country'];
            let email = value['email'] == null ? "" : value['email'];
            let status = value['isActive'] == 1 ? "Active" : "Inactive";
            let statusChange = value['isActive'] == 1 ? 'data-status="0" data-original-title="Disable"><i class="fa fa-times-circle"></i>' : 'data-status="1" data-original-title="Enable"><i class="fa fa-check-circle"></i>';
            let statusBtn = canEdit === false ? '' : '<button id="change_status_' + id + '" class="btn btn-sm btn-light js-tooltip-enabled status-change" data-ba-id="' + id + '" ' + statusChange + '</button>';

            if (email != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#ba_list").append('<tr><td>' + name + 
                    '</td><td>' + country + 
                    '</td><td>' + email + 
                    '</td"><td id="status_' + id + '">' + status + 
                    '</td><td class="text-center" id="change_status_cell_' + id + '">' + statusBtn + '</td></tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#ba_list").append('<tr><td class="text-center" colspan="6">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
    
            $(".status-change").click(function(event) {
                $(this).prop("disabled",true);
                changeStatus($(this).attr("data-ba-id"), $(this).attr("data-status"));
            });
        }
    }
    else {
        $("#ba_list").append('<tr><td class="text-center" colspan="6">No Items</td></tr>');
    }
}

function changeStatus(id, status) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: API_URL + "/admin/ba/status/update/" + id,
        data: {isActive: status},
        cache: false,
        success: function(data) {
            console.log(data);
            let statusChange = data.payload.isActive === "1" ? 'data-status="0" data-original-title="Disable"><i class="fa fa-times-circle"></i>' : 'data-status="1" data-original-title="Enable"><i class="fa fa-check-circle"></i>';
            let statusBtn = canEdit === false ? '' : '<button id="change_status_' + data.payload.id + '" class="btn btn-sm btn-light js-tooltip-enabled status-change" data-ba-id="' + data.payload.id + '" ' + statusChange + '</button>';

            $('#change_status_cell_' + data.payload.id).html(statusBtn);
            $('#change_status_' + data.payload.id).click(function(event) {
                $(this).prop("disabled",true);
                changeStatus($(this).attr("data-ba-id"), $(this).attr("data-status"));
            });
            $('#status_' + data.payload.id).html(data.payload.isActive === "1" ? "Active" : "Inactive");
        },
        error: function(jqXHR) {
            console.log(jqXHR);
        }
    });
}

$(document).ready(function() {
    ajaxGetSellerUsers();

    $(".filterBAs").change(function() {
        ajaxGetSellerUsers();
    });

    $("#filterBySearch").change(function() {
        generateBATable();
    });
});