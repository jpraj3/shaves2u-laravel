let rechargedata = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;

function ajaxGetRecharges() {
    rechargedata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/subscriptions/recharges/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetRechargeBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetRechargeBlock() {
    $.ajax ({
        url: API_URL + '/admin/subscriptions/recharges',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshRecharges(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetRechargeBlock(currentpage);
            }
        }
    });
}

function ajaxSearchRecharges() {
    rechargedata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/subscriptions/recharges/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchRechargeBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchRechargeBlock() {
    $.ajax ({
        url: API_URL + '/admin/subscriptions/recharges',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshRecharges(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchRechargeBlock(currentpage);
            }
        }
    });
}

function refreshRecharges(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        rechargedata = rechargedata.filter(obj => obj.id !== value.id);

        rechargedata.push(value);
    });
    rechargedata = rechargedata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateRechargesTable();
}


function generateRechargesTable() {
    $("#recharges_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#recharges_list tr").remove();

    var resultsCount = 0;
    if (rechargedata.length >= 1) {
        $.each( rechargedata, function( key, value ) {
            let email = value['email'] == null ? "" : value['email'];
            let recharge_date = value['recharge_date'] == null ? "" : value['recharge_date'];
            let total_recharge = value['total_recharge'] == null ? "" : value['total_recharge'];
            let status = value['status'] == null ? "" : value['status'];
            let realized = value['unrealizedCust'] == null ? "No" : "Yes";

            if (email != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#recharges_list").append('<tr><td>' + email +
                    '</td><td>' + recharge_date +
                    '</td><td>' + total_recharge +
                    '</td><td><a href="">' + status +
                    '</td><td>' + realized +
                    '</td></tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#recharges_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWidth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#recharges_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

$(document).ready(function() {
    $("#start_filter").on('click', function () {
        showLoading();

            ajaxGetRecharges();
 
    });
    // ajaxGetRecharges();

    // $(".filterRecharges").change(function() {
    //     ajaxGetRecharges();
    // });

    // $("#filterBySearch").keyup(function() {
    //     generateRechargesTable();
    //     // ajaxSearchRecharges();
    // });
});
