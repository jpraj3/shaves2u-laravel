function generateCSVData(type, data) {
    console.log(type, data);
    try {
        $.ajax({
            // url: API_URL + "/admin/" + type + "/list",
            url: API_URL + "/admin/download/" + type + "/list",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            crossDomain: true,
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response !== "") {
                    if (type == "referralsCashout" || type == "referrals") {
                        console.log("dsdsds");
                        $("#csv-queue-notification-1").attr("hidden",true);
                    }

                    $("#csv-queue-notification").removeAttr('hidden');
                    if (type == "subscribers" || type == "reports/master" || type == "reports/appco" || type == "reports/mo" || type == "reports/mastersummary" || type == "subscriberssummary") {
                        document.getElementById("csv-report-trigger").disabled = true;
                        document.getElementById('csv-report-trigger').innerText = 'Report is generating';
                        document.getElementById("csv-report-trigger-2").disabled = true;
                        document.getElementById('csv-report-trigger-2').innerText = 'Report is generating';
                    }

                    if (response.url == null) {

                        // $("#csv-queue-notification").removeAttr('hidden');
                        // Swal.fire(
                        //     'No ' + type + ' data found.',
                        //     '',
                        //     'info'
                        // )
                    }
                } else {

                }
            },
            error: function (response) {
                console.log(response);
                if (response.url == null) {
                    Swal.fire(
                        'No ' + type + ' data found.',
                        '',
                        'info'
                    )
                }
                $("#csv-queue-notification").removeAttr('hidden');
            }
        });
    }
    catch (err) {
        //
        console.log(err);
    }
}

// function generateExcel(type, data, filters) {
//     $.ajax({
//         url: GLOBAL_URL + "/export/" + type,
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         type: "POST",
//         data: {result:data,filters:filters},
//         dataType: "application/pdf",
//         success: function(response) {

//         }
//     });
// }

function exportCSV(type) {
    $("#csv-queue-notification").attr('hidden');
    switch (type) {
        case "orders":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "bulkorders":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "cancellations":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "referrals":
            data = {
                appType: appType,
                maxresults: null,
                pagenum: null,
                adminId: adminId
            };
            $("#csv-queue-notification-1").removeAttr('hidden');
            generateCSVData(type, data);
            break;
        case "referralsCashout":
            data = {
                appType: appType,
                maxresults: null,
                pagenum: null,
                adminId: adminId,
                search: $("#filterBySearch").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
            };
            $("#csv-queue-notification-1").removeAttr('hidden');
            generateCSVData(type, data);
            break;
        case "reports/master":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                // sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "reports/mastersummary":
                data = {
                    appType: appType,
                    maxresults: null,
                    search: $("#filterBySearch").val(),
                    channel: $("#filterByChannel").val(),
                    country: $("#filterByCountry").val(),
                    // sku: $("#filterBySKU").val(),
                    status: $("#filterByStatus").val(),
                    fromdate: $("#filterByDateFrom").val(),
                    todate: $("#filterByDateTo").val(),
                    pagenum: null,
                    adminId: adminId
                };
                generateCSVData(type, data);
        break;
        case "reports/appco":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                // sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "reports/mo":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                // sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "subscribers":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                plantype: $("#filterByPlanType").val(),
                sku: $("#filterBySKU").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "subscriberssummary":
            data = {
                    appType: appType,
                    maxresults: null,
                    search: $("#filterBySearch").val(),
                    country: $("#filterByCountry").val(),
                    plantype: $("#filterByPlanType").val(),
                    sku: $("#filterBySKU").val(),
                    fromdate: $("#filterByDateFrom").val(),
                    todate: $("#filterByDateTo").val(),
                    pagenum: null,
                    adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "customers":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "recharges":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        case "pauseplans":
            data = {
                appType: appType,
                maxresults: null,
                search: $("#filterBySearch").val(),
                fmonth: $("#filterByMonth").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateCSVData(type, data);
            break;
        default:
        // code block
    }
}

function exportTaxInvoice(type) {
    // $("#csv-queue-notification").attr('hidden');
    switch (type) {
        case "order":
            data = {
                appType: appType,
                type: type,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateTaxInvoiceData(type, data);
            break;
        case "bulkorder":
            data = {
                appType: appType,
                type: type,
                maxresults: null,
                search: $("#filterBySearch").val(),
                channel: $("#filterByChannel").val(),
                country: $("#filterByCountry").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: null,
                adminId: adminId
            };
            generateTaxInvoiceData(type, data);
            break;
        default:
        // code block
    }
}

function generateTaxInvoiceData(type, data) {
    console.log(type, data);
    try {
        $.ajax({
            url: API_URL + "/admin/download/tax-invoice/" + type,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            crossDomain: true,
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response !== "") {
                    // $("#csv-queue-notification").removeAttr('hidden');
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
    catch (err) {
        //
        console.log(err);
    }
}
