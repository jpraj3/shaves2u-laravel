function ajaxGetCountries(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetMOs(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin/mo/list',
            type: "POST",
            data: {appType:appType},
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetAdmins(handleResponse) {
    try {
        $.ajax ({
            url: API_URL + '/admin',
            type: "POST",
            data: {
                appType:appType,
            },
            dataType: "json",
            success: function(response){
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshAdmins(data) {
    admindata = data;

    generateAdminsTable();
}

function generateAdminsTable() {
    $("#admins_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#admins_list tr").remove();

    var resultsCount = 0;
    if (admindata.length >= 1) {
        $.each( admindata, function( key, value ) {
            let id = value['id'] == null ? "" : value['id'];
            let firstname = value['firstName'] == null ? "" : value['firstName'];
            let lastname = value['lastName'] == null ? "" : value['lastName'];
            let email = value['email'] == null ? "" : value['email'];
            let phone = value['phone'] == null ? "" : value['phone'];
            let country = value['country'] == null ? "" : value['country'];
            let role = value['role'] == null ? "" : value['role'];
            let status = value['isActive'] == 1 ? "Active" : "Inactive";
            let roleId = value['RoleId'] == null ? "" : value['RoleId'];
            let countryId = value['CountryId'] == null ? "" : value['CountryId'];
            let marketingOfficeId = value['MarketingOfficeId'] == null ? "" : value['MarketingOfficeId'];
                
            if (email != null) {
                console.log("test");
                if (email.indexOf($("#filterBySearch").val()) != -1) {
                    $("#admins_list").append('<tr><td>' + firstname + ' ' + lastname +
                    '</td><td>' + email + 
                    '</td><td>' + phone + 
                    '</td><td>' + country + 
                    '</td><td>' + role + 
                    '</td><td>' + status +
                    '</td><td class="text-center"> \
                    <button class="btn btn-sm btn-light js-tooltip-enabled edit-admin" data-admin-id="' + id +'" data-admin-role="' + roleId + '" data-admin-mo="' + marketingOfficeId + '" data-admin-country="' + countryId + '" data-toggle="modal" data-target="#modal_edit_admin" data-original-title="Edit Admin"' + (canEdit ? '' : ' disabled') + '><i class="fa fa-edit"></i></button> \
                    </td></tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#admins_list").append('<tr><td class="text-center" colspan="7">No Items</td></tr>');
        }
        else {
            console.log(resultsCount)
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
            
            $('.edit-admin').click(function(e) {
                let posturl = API_URL + '/admin/update/' + $(this).attr('data-admin-id');
                console.log(posturl);
                $("#edit_admin").attr('action', posturl)
                $("#edit_admin_id").val( $(this).attr('data-admin-id') );
                $("#edit_admin_role").val( $(this).attr('data-admin-role') );
                $("#edit_admin_role  option[value=" + $(this).attr('data-admin-role') + "]").prop('selected', true);
                $("#edit_admin_country").val( $(this).attr('data-admin-country') );
                $("#edit_admin_country  option[value=" + $(this).attr('data-admin-country') + "]").prop('selected', true);
                $("#edit_admin_mo").val( $(this).attr('data-admin-mo') );
                $("#edit_admin_mo  option[value=" + $(this).attr('data-admin-mo') + "]").prop('selected', true);
            });
        }
    }
    else {
        $("#admins_list").append('<tr><td class="text-center" colspan="7">No Items</td></tr>');
    }
}

$(document).ready(function() {
    $("#add_admin").on("submit", function(event) {
        event.preventDefault();
        $('button[type="submit"]').prop("disabled",true);
        
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: API_URL + '/admin/create',
            data: $("#add_admin").serialize(),
            cache: false,
            success: function(data) {
                console.log(data);
                $('#modal_add_admin').modal('hide');
                $('button[type="submit"]').prop("disabled",false);
                ajaxGetAdmins(function(output) {
                    refreshAdmins(output);
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });

    $("#edit_admin").on("submit", function(event) {
        event.preventDefault();
        $('button[type="submit"]').prop("disabled",true);
        
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            type: "POST",
            url: $("#edit_admin").attr('action'),
            data: $("#edit_admin").serialize(),
            cache: false,
            success: function(data) {
                console.log(data);
                $('#modal_edit_admin').modal('hide');
                $('button[type="submit"]').prop("disabled",false);
                ajaxGetAdmins(function(output) {
                    refreshAdmins(output);
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });

    ajaxGetCountries(function(output){
        $.each(output, function( key, value ) {
            $(".country-select").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });

    ajaxGetMOs(function(output){
        $.each(output, function( key, value ) {
            $(".mo-select").append('<option value="' + value['id'] + '">' + value['moCode'] + '</option>');
        });
    });

    ajaxGetAdmins(function(output) {
        refreshAdmins(output);
    });

    $("#filterBySearch").change(function() {
        generateOrdersTable();
    });
});