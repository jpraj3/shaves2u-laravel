let subscriberdata = [];
let countries = null;
let currentpage = null;
let pages = null;
let hasInitialized = 0;
function ajaxGetCountries(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetPlanSKUs(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/planskus/list',
            type: "POST",
            data: {
                appType: appType
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetSubscribers(handleResponse) {
    subscriberdata = [];

    try {
        $.ajax({
            url: API_URL + '/admin/subscriptions/subscribers/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                plantype: $("#filterByPlanType").val(),
                sku: $("#filterBySKU").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetSubscribersBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetSubscribersBlock(pagenum) {
    $.ajax({
        url: API_URL + '/admin/subscriptions/subscribers',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            country: $("#filterByCountry").val(),
            plantype: $("#filterByPlanType").val(),
            sku: $("#filterBySKU").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: pagenum,
        },
        dataType: "json",
        success: function (response) {
            refreshSubscribers(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetSubscribersBlock(currentpage);
            }
        }
    });
}

function ajaxSearchSubscribers(handleResponse) {
    subscriberdata = [];

    try {
        $.ajax({
            url: API_URL + '/admin/subscriptions/subscribers/count',
            type: "POST",
            data: {
                appType: appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchSubscriberBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchSubscriberBlock() {
    $.ajax({
        url: API_URL + '/admin/subscriptions/subscribers',
        type: "POST",
        data: {
            appType: appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function (response) {
            refreshSubscribers(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchSubscriberBlock(currentpage);
            }
        }
    });
}

function refreshSubscribers(data) {
    $.each(data, function (key, value) {
        // remove duplicate data according to id
        subscriberdata = subscriberdata.filter(obj => obj.id !== value.id);

        subscriberdata.push(value);
    });
    subscriberdata = subscriberdata.sort(function (a, b) {
        return b.id - a.id;
    });

    generateSubscribersTable();
}


function generateSubscribersTable() {
    $("#subscribers_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#subscribers_list tr").remove();

    var resultsCount = 0;
    if (subscriberdata.length >= 1) {
        $.each(subscriberdata, function (key, value) {
            let email = value['email'] ? value['email'] : "";
            let countryName = value['countryName'] ? value['countryName'] : "";
            let created_date = value['created_date'] ? value['created_date'] : "";
            let updated_date = value['updated_date'] ? value['updated_date'] : "";
            let duration = value['duration'] ? value['duration'] : "";
            let sku = value['sku'] ? value['sku'] : "";
            let status = value['status'] ? value['status'] : "";
            let plantype = value['plantype'] ? value['plantype'] : "";

            if (email.indexOf($("#filterBySearch").val()) != -1) {
                $("#subscribers_list").append('<tr><td>' + email +
                    '</td><td>' + countryName +
                    '</td><td>' + created_date +
                    '</td><td>' + updated_date +
                    '</td><td>' + duration + " months" +
                    '</td><td>' + sku +
                    '</td><td>' + plantype +
                    '</td><td>' + status +
                    '</td><td class="text-center"> \
                <a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/subscription/subscribers/details/' + value['id'] + '" data-original-title="View Details"><i class="fa fa-search"></i></a> \
                </td></tr>');
                resultsCount++;
            }
        });
        if (resultsCount < 1) {
            $("#subscribers_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#subscribers_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
}

function initSubscribers() {
    $('#subscribers_table').dataTable().fnDestroy();

    $('#subscribers_table').DataTable({
        pageLength: 50,
        lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
        autoWidth: false,
        searching: false,
        dom: "<l<t>ip>",
        order: [],
        bProcessing: true,
        bServerSide: true,
        sPaginationType: "full_numbers",
        ajax: {
            url: API_URL + '/admin/subscriptions/subscribers',
            type: "POST",
            data: {
                appType: appType,
                search: function () { return $("#filterBySearch").val() },
                country: function () { return $("#filterByCountry").val() },
                plantype: function () { return $("#filterByPlanType").val() },
                sku: function () { return $("#filterBySKU").val() },
                status: function () { return $("#filterByStatus").val() },
                fromdate: function () { return $("#filterByDateFrom").val() },
                todate: function () { return $("#filterByDateTo").val() },
            },
            dataType: "json",
            dataSrc: "data"
        },
        columns: [
            { data: 'email' },
            { data: 'countryName' },
            { data: 'created_date' },
            { data: 'updated_date' },
            { data: 'duration' },
            { data: 'sku' },
            { data: 'plantype' },
            { data: 'status' },
            {
                class: "text-center",
                data: 'id'
            },
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    hideLoading();
                    return canViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/subscription/subscribers/details/' + data + '" data-original-title="View Details"><i class="fa fa-search"></i></a>';
                },
                targets: 8
            },
        ]
    });

}

$(document).ready(function () {
    ajaxGetCountries(function (output) {
        $.each(output, function (key, value) {
            $("#filterByCountry").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });

    ajaxGetPlanSKUs(function (output) {
        $.each(output, function (key, value) {
            $("#filterBySKU").append('<option value="' + value['id'] + '">' + value['sku'] + '</option>');
        });
    });

    // ajaxGetSubscribersBlock();
    // initSubscribers();
    $("#start_filter").on('click', function () {
        showLoading();
        if (hasInitialized == 0) {
            hasInitialized = 1;
            initSubscribers();
        } else {
            $('#subscribers_table').DataTable().ajax.reload();
        }
    });
    // $(".filterSubscribers").change(function() {
    //     // ajaxGetSubscribersBlock();
    //     $('#subscribers_table').DataTable().ajax.reload();
    // });

    // $("#filterBySearch").keyup(function() {
    //     // generateSubscribersTable();
    //     // ajaxSearchSubscribers();
    //     $('#subscribers_table').DataTable().ajax.reload();
    // });
});