function ajaxGetSubscriberDetails(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/subscriptions/subscribers/details',
            type: "POST",
            data: {
                apptype: appType,
                id: subscriberid,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxChangeSubscriberStatus(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/subscriptions/subscribers/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: subscriberid,
                status: $("#change_status").children("option:selected").val(),
                isRemark: 0,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxAddComment(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/subscriptions/subscribers/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: subscriberid,
                status: "Admin Update",
                comment: $("#subscriber_log_message").val(),
                isRemark: 1,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshSubscriberDetails(data) {
    let email = data['email'] != null ? data['email'] : "";
    let subscription_id = data['id'] != null ? data['id'] : "";
    let status = data['status'] != null ? data['status'] : "";
    let planType = data['isTrial'] == 1 ? 'Trial Plan' : data['isCustom'] == 1 ? "Custom Plan" : "";
    let duration = data['duration'] != null ? data['duration'] : "";
    let channel = data['SellerUserId'] != null ? 'BA' : 'Online / E-Commerce';
    let branchName = data['card']['branchName'] != null ? data['card']['branchName'] : "-";
    let cardType = data['card']['cardType'] != null ? data['card']['cardType'] : "-";
    let expiryDate = data['card']['expiredMonth'] != null && data['card']['expiredYear'] != null ? data['card']['expiredMonth'] + ' / ' + data['card']['expiredYear'] : "-";
    let cardNumber = data['card']['cardNumber'] != null ? "**** **** **** " + data['card']['cardNumber'] : "-";
    let paymentType = data['orders'][0]['paymentType'] != null ? data['orders'][0]['paymentType'] : "";
    let created_at = data['created_at'] != null ? data['created_at'] : "";
    let updated_at = data['updated_at'] != null ? data['updated_at'] : "";
    let nextChargeDate = data['nextChargeDate'] != null ? data['nextChargeDate'] : "";
    let currency = data['invoice'] == null ? "" : data['invoice']['currency'];
    let isAnnual = data['isAnnual'] != null ? data['isAnnual'] : 0;
    if (isAnnual == 1) {
        duration = 12;
    }
    let message = "";
    let detail = "";

    $("#email").html('' + email + '');
    $("#subscription_id").text(subscription_id);
    $("#subscription_status").text(status);
    $("#plan_type").text(planType);
    $("#frequency").text(duration + ' months');
    $.each(data['orders'], function (key, value) {
        $("#order_no").append('<span class="col-4 px-0"><a href="' + GLOBAL_URL + '/orders/details/' + value["id"] + '" target="_blank">' + value['order_no'] + '</a></span>&nbsp;');
    });
    $("#channel").text(channel);

    $("#next_charge_date").text(nextChargeDate);

    $.each(data['rechargehistory'], function (key, value) {
        if (key % 10 == 0) {
            $("#recharge_history").append('<span class="col-4 px-0">' + (key + 1) + 'st attempt at ' + value['created_at_date'] + '</span>');
        }
        else if (key % 10 == 1) {
            $("#recharge_history").append('<span class="col-4 px-0">' + (key + 1) + 'nd attempt at ' + value['created_at_date'] + '</span>');
        }
        else if (key % 10 == 2) {
            $("#recharge_history").append('<span class="col-4 px-0">' + (key + 1) + 'rd attempt at ' + value['created_at_date'] + '</span>');
        }
        else {
            $("#recharge_history").append('<span class="col-4 px-0">' + (key + 1) + 'th attempt at ' + value['created_at_date'] + '</span>');
        }
    });
    console.log();

    if (paymentType === 'stripe') {
        $("#payment_method").html('\
            <div class="row"><div class="col-4"> <strong>Card Brand:</strong></div><div class="col-6"><span>' + branchName + '</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Card Type:</strong></div><div class="col-6"><span>' + cardType + '</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Expiry Date:</strong></div><div class="col-6"><span>' + expiryDate + '</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Card Number:</strong></div><div class="col-6"><span>' + cardNumber + '</span></div></div>'
        );
    }
    else if (paymentType === 'nicepay') {
        $("#payment_method").html('\
            <div class="row"><div class="col-4"> <strong>Card Brand:</strong></div><div class="col-6"><span>-</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Card Type:</strong></div><div class="col-6"><span>-</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Expiry Date:</strong></div><div class="col-6"><span>-</span></div></div>\
            <div class="row"><div class="col-4"> <strong>Card Number:</strong></div><div class="col-6"><span>-</span></div></div>'
        );
    }
    else {
        $("#payment_method").text(paymentType);
    }

    $("#created_date").text(created_at);
    $("#last_updated").text(updated_at);

    $("#change_status").val(status);

    $.each(data['history'], function (key, value) {
        message = value['message'] != null ? value['message'] : "";
        detail = value['detail'] != null ? value['detail'] : "";

        $("#subscriber_log").append('<div class="row"> \
                                    <div class="col-2"> \
                                        <span>' + value['created_at_date'] + '</span> \
                                    </div> \
                                    <div class="col-4"> \
                                        <span>' + message + '</span> \
                                    </div> \
                                    <div class="col-6"> \
                                        <span>' + detail + '</span> \
                                    </div> \
                                </div>');
    });

    $("#shipping_name").text(data['deliverydetails']['firstName']);
    $("#shipping_contact").text(data['deliverydetails']['contactNumber']);
    $("#shipping_address").text(data['deliverydetails']['address']);

    let total_price = 0;
    if (planType == 'Trial Plan') {
        $.each(data['plandetails'], function (key, value) {
            if (!handleTypes.includes(value['sku'])) {
                $("#plandetails").append('<tr><td>' + value['sku'] +
                    // '</td><td class="text-right">' + value['name'] +
                    '</td><td class="text-right">' + value['currencyDisplay'] + ' ' + parseFloat(value['sellPrice'] * value['qty']).toFixed(2) +
                    '</td></tr>');
                total_price = parseFloat(total_price) + parseFloat(value['sellPrice'] * value['qty']);
            }

        });
    } else {
        $.each(data['plandetails'], function (key, value) {
            $("#plandetails").append('<tr><td>' + value['sku'] +
                // '</td><td class="text-right">' + value['name'] +
                '</td><td class="text-right">' + value['currencyDisplay'] + ' ' + value['sellPrice'] +
                '</td></tr>');
            total_price = parseFloat(total_price) + parseFloat(value['sellPrice']);

        });
    }


    if (data['addons']) {
        console.log(data['addons']);
        $.each(data['addons'], function (key, value) {
            $("#plandetails").append('<tr><td>' + value['sku'] +
                // '</td><td class="text-right">' + value['name'] +
                '</td><td class="text-right">' + value['currencyDisplay'] + ' ' + value['sellPrice'] +
                '</td></tr>');
            total_price = parseFloat(total_price) + parseFloat(value['sellPrice']);
        });
        $("#plandetails").append('<tr><td></td><td class="text-right">' + currency + ' ' + parseFloat(total_price).toFixed(2) +
            '</td></tr>');
    }



    $.each(data['subscriberdetails'], function (key, value) {
        $("#invoice").prepend('<tr><td>' + value['name'] +
            '</td><td class="text-right">' + value['qty'] +
            '</td><td class="text-right">' + data['invoice']['currency'] + ' ' + value['price'] +
            '</td><td class="text-right">' + data['invoice']['currency'] + ' ' + value['total_price'] +
            '</td></tr>');
    });

    $("#sub_total").text(data['invoice']['currency'] + ' ' + data['invoice']['originPrice']);
    $("#discount").text(data['invoice']['currency'] + ' ' + data['invoice']['discountAmount']);
    $("#processing_fee").text(data['invoice']['currency'] + ' ' + data['invoice']['chargeFee']);
    $("#grand_total").text(data['invoice']['currency'] + ' ' + data['invoice']['totalPrice']);
    $("#cash_rebate").text(data['invoice']['currency'] + ' 0.00');
    $("#total_payable").text(data['invoice']['currency'] + ' ' + data['invoice']['totalPrice']);
}

function refreshSubscriberLog(data) {
    $("#subscriber_log span").remove();
    let message = "";
    let detail = "";

    $.each(data, function (key, value) {
        message = value['message'] != null ? value['message'] : "";
        detail = value['detail'] != null ? value['detail'] : "";

        $("#subscriber_log").append('<div class="row"> \
                                    <div class="col-2"> \
                                        <span>' + value['created_at_date'] + '</span> \
                                    </div> \
                                    <div class="col-4"> \
                                        <span>' + message + '</span> \
                                    </div> \
                                    <div class="col-6"> \
                                        <span>' + detail + '</span> \
                                    </div> \
                                </div>');
    });
}

$(document).ready(function () {
    ajaxGetSubscriberDetails(function (output) {
        refreshSubscriberDetails(output);
    });

    $("#change_status").change(function () {
        ajaxChangeSubscriberStatus(function (output) {
            refreshSubscriberLog(output);
        });
    });

    $("#open_slm").click(function () {
        $("#subscriber_log_add_comment").removeClass('d-none');
        $("#open_slm").addClass('d-none');
    });

    $("#cancel_slm").click(function () {
        $("#subscriber_log_add_comment").addClass('d-none');
        $("#open_slm").removeClass('d-none');
    });

    $("#submit_slm").click(function () {
        if ($("#subscriber_log_message").val().length >= 1) {
            ajaxAddComment(function (output) {
                refreshSubscriberLog(output);
            });
        }
        $("#subscriber_log_add_comment").addClass('d-none');
        $("#open_slm").removeClass('d-none');
    });
});