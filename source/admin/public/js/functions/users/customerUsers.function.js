let customerdata = [];
let countries = null;

function ajaxGetCountries(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/countries/list',
            type: "POST",
            data: null,
            dataType: "json",
            success: function (response) {
                console.log(response);
                countries = response;
                handleResponse(response);
            }
        });
    } catch (err) {
        //
    }
}

function ajaxGetCustomers(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                country: $("#filterByCountry").val(),
                status: $("#filterByStatus").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function (response) {
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetCustomerBlock();
            }
        });
    } catch (err) {
        //
    }
}

function ajaxGetCustomerBlock() {
    $.ajax ({
        url: API_URL + '/admin/user/customer/list',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            country: $("#filterByCountry").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshCustomers(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetCustomerBlock(currentpage);
            }
        }
    });
}

function ajaxSearchCustomers() {
    customerdata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/user/customer/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchCustomerBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchCustomerBlock() {
    $.ajax ({
        url: API_URL + '/admin/user/customer/list',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshCustomers(response);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                // ajaxSearchCustomerBlock(currentpage);
            }
        }
    });
}

function refreshCustomers(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        customerdata = customerdata.filter(obj => obj.id !== value.id);

        customerdata.push(value);
    });
    customerdata = customerdata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateCustomersTable();
}

function generateCustomersTable() {
    $("#customers_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#customers_list tr").remove();

    let resultsCount = 0;
    if (customerdata.length >= 1) {
        $.each(customerdata, function (key, value) {
            let id = value['id'] == null ? "" : value['id'];
            let firstName = value['firstName'] ? value['firstName'] : "";
            let lastName = value['lastName'] ? value['lastName'] : "";
            let countryName = "";
            for (let i = 0; i < countries.length; i++) {
                if (countries[i]['id'] === value['CountryId']) {
                    countryName = countries[i]['name'];
                    // return;
                }
            }
            let consent = (value['Email_Sub'] == true && value['Sms_Sub'] == true) ? "Email & SMS" : (value['Email_Sub'] == true) ? "Email" : (value['Sms_Sub'] == true) ? "SMS" : (value['HKG_Marketing_Sub'] == true) ? "HK Marketing" : "None";
            let phone = value['phone'] ? value['phone'] : "";
            let email = value['email'] ? value['email'] : "";
            let status = value['isActive'] ? "Active" : "Inactive";
            let statusChange = value['isActive'] == 1 ? 'data-status="0" data-original-title="Disable"><i class="fa fa-times-circle"></i>' : 'data-status="1" data-original-title="Enable"><i class="fa fa-check-circle"></i>';
            let statusBtn = canEdit === false ? '' : '<button id="change_status_' + id + '" class="btn btn-sm btn-light js-tooltip-enabled status-change" data-ba-id="' + id + '" ' + statusChange + '</button>';
            let viewDetailsBtn = canViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/users/customers/details/' + value['id'] +'" data-original-title="View Details"><i class="fa fa-search"></i></a>';

                $("#customers_list").append('<tr><td>' + firstName + lastName +
                    '</td><td>' + countryName +
                    '</td><td>' + consent +
                    '</td><td>' + phone +
                    '</td><td>' + email +
                    '</td><td id="status_' + id + '">' + status +
                    '</td><td class="text-center" id="action_cell_' + id + '">'+statusBtn+viewDetailsBtn+'</td></tr>');
                resultsCount++
            
        });
        if (resultsCount < 1) {
            $("#customers_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
    
            $(".status-change").click(function(event) {
                $(this).prop("disabled",true);
                changeStatus($(this).attr("data-ba-id"), $(this).attr("data-status"));
            });
        }
    }
    else {
        $("#customers_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

function changeStatus(id, status) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        type: "POST",
        url: API_URL + "/admin/user/customer/status/update/" + id,
        data: {isActive: status},
        cache: false,
        success: function(data) {
            console.log(data);
            let statusChange = data.payload.isActive === "1" ? 'data-status="0" data-original-title="Disable"><i class="fa fa-times-circle"></i>' : 'data-status="1" data-original-title="Enable"><i class="fa fa-check-circle"></i>';
            let statusBtn = canEdit === false ? '' : '<button id="change_status_' + data.payload.id + '" class="btn btn-sm btn-light js-tooltip-enabled status-change" data-ba-id="' + data.payload.id + '" ' + statusChange + '</button>';
            let viewDetailsBtn = canViewDetails === false ? '' : '<a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/users/customers/' + data.payload.id +'" data-original-title="View Details"><i class="fa fa-search"></i></a>';

            $('#action_cell_' + data.payload.id).html(statusBtn + viewDetailsBtn);
            $('#change_status_' + data.payload.id).click(function(event) {
                $(this).prop("disabled",true);
                changeStatus($(this).attr("data-ba-id"), $(this).attr("data-status"));
            });
            $('#status_' + data.payload.id).html(data.payload.isActive === "1" ? "Active" : "Inactive");
        },
        error: function(jqXHR) {
            console.log(jqXHR);
        }
    });
}

$(document).ready(function () {

    ajaxGetCountries(function (output) {
        $.each(output, function (key, value) {
            $("#filterByCountry").append('<option value="' + value['id'] + '">' + value['name'] + '</option>');
        });
    });
    $("#start_filter").on('click', function () {
        showLoading();

        ajaxGetCustomers();
     
    });
    // ajaxGetCustomers();

    // $(".filterCustomers").change(function () {
    //     ajaxGetCustomers();
    // });
    // let typingTimer;               
    // let doneSearchTypingInterval = 5000;  
    // let searchInput = document.getElementById('filterBySearch');
    
    // searchInput.addEventListener('keyup', () => {
    //     clearTimeout(typingTimer);
    //     if (searchInput.value) {
    //         typingTimer = setTimeout(doneSearchTyping, doneSearchTypingInterval);
    //     }
    // });
    
    // function doneSearchTyping () {
    //     ajaxSearchCustomers();
    // }
    // $("#filterBySearch").keyup(function() {
    //     generateCustomersTable();
    //     // ajaxSearchCustomers();
    // });
});
