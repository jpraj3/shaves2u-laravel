function ajaxGetCustomerDetails(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/details',
            type: "POST",
            data: {
                apptype: appType,
                id: customerid,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function refreshCustomerDetails(data) {
    let firstname = data['firstName'] == null ? "" : data['firstName'];
    let lastname = data['lastName'] == null ? "" : " " + data['lastName'];
    let email = data['email'] == null ? "" : data['email'];
    let phone = data['phone'] == null ? "" : data['phone'];
    let country = data['country'] == null ? "" : data['country']['code'];
    let lang = data['country'] === "HK" ? data['defaultLanguage'] === "EN" ? " - E" : " - C" : "";
    let consent = (data['Email_Sub'] == true && data['Sms_Sub'] == true) ? "Email & SMS" : (data['Email_Sub'] == true) ? "Email" : (data['Sms_Sub'] == true) ? "SMS" : (data['HKG_Marketing_Sub'] == true) ? "HK Marketing" : "None";
    let channel = data['badgeId'] == null ? "Online / E-Commerce" : "BA";
    let subscriptionId = data['subscription'] == null ? "" : data['subscription']['SubscriptionId'];
    let subscriptionLink = canViewSubs ? '<a href="' + GLOBAL_URL + '/subscription/subscribers/details/' + subscriptionId + '">' + subscriptionId + '</a>' : subscriptionId;
    let registered = data['registered_date'] == null ? "" : data['registered_date'];
    let last_login = data['last_login_date'] == null ? "" : data['last_login_date'];
    let orderlog = data['orderHistories'] == null ? "" : data['orderHistories'];
    let states = data['states'] == null ? "" : data['states'];
    
    $("#full_name").html(firstname + lastname);
    $("#customer_name").val(firstname + lastname);
    $("#email").html('<a href="mailto:">' + email + '</a>');
    $("#customer_email").val(email);
    $("#phone").html(phone);
    $("#customer_phone").val(phone);
    $("#country").html(country + lang);
    $("#consent").html(consent);
    $("#channel").html(channel);
    $("#subscriptionId").html(subscriptionLink);
    $("#register_datetime").html(registered);
    $("#last_login").html(last_login);

    if (country === "TWN") {
        $(".tw_only").removeClass("d-none");
        $("#add_ssn").attr('type','text');
    }
    else {
        $(".tw_only").addClass("d-none");
        $("#add_ssn").attr('type','hidden');
    }

    if (orderlog != null) {
        $("#order_log").html("");
        $.each(orderlog, function (key, value) {
            $("#order_log").append('<div class="row"> \
                                        <div class="col-4"> \
                                            <span>' + value['created_at_date'] + '</span> \
                                        </div> \
                                        <div class="col-6"> \
                                            <span>' + value['message'] + '</span> \
                                        </div> \
                                    </div>');

        });
    }

    let d_address_details = data['d_address_details'];

    if (d_address_details != null) {
        let d_address_id = d_address_details['id'] == null ? "" : d_address_details['id'];
        let d_address = d_address_details['address'] == null ? "" : d_address_details['address'];
        let d_city = d_address_details['city'] == null ? "" : d_address_details['city'];
        let d_state = d_address_details['state'] == null ? "" : d_address_details['state'];
        let d_postcode = d_address_details['portalCode'] == null ? "" : " " + d_address_details['portalCode'];
        // let editAddressBtn = '<button data-address-id="' + d_address_id + '" class="btn btn-sm btn-primary edit-address"><i class="fa fa-edit py-1"></i></button>';

        $("#delivery_address").html('<div class="detail-address"><h6>(Primary)</h6><h4>Address 1</h4> \
                    <p>' + d_address + '</p><p>' + d_city + '</p><p>' + d_state + '</p><p>' + d_postcode + '</p></div>');
    }
    else {
        $("#delivery_address").html('<p>Not Available</p>');
    }

    let b_address_details = data['b_address_details'];

    if (b_address_details != null) {
        let b_address_id = b_address_details['id'] == null ? "" : b_address_details['id'];
        let b_address = b_address_details['address'] == null ? "" : b_address_details['address'];
        let b_city = b_address_details['city'] == null ? "" : b_address_details['city'];
        let b_state = b_address_details['state'] == null ? "" : b_address_details['state'];
        let b_postcode = b_address_details['portalCode'] == null ? "" : " " + b_address_details['portalCode'];
        // let editAddressBtn = '<button data-address-id="' + b_address_id + '" class="btn btn-sm btn-primary edit-address"><i class="fa fa-edit py-1"></i></button>';

        $("#billing_address").html('<div class="detail-address"><h6>(Primary)</h6><h4>Address 1</h4> \
                    <p>' + b_address + '</p><p>' + b_city + '</p><p>' + b_state + '</p><p>' + b_postcode + '</p></div>');
    }
    else {
        $("#billing_address").html('<p>Not Available</p>');
    }

    if (b_address_details != null) {
    }

    let addresses = data['addresses'];
    if (addresses.length >= 1) {
        let address_id = null;
        let address = null;
        let city = null;
        let state = null;
        let postcode = null;
        let editAddressBtn = null;

        $("#address_list").html("");
        $.each(addresses, function( key, value ) {
            address_id = value['id'] == null ? "" : value['id'];
            address = value['address'] == null ? "" : value['address'];
            city = value['city'] == null ? "" : value['city'];
            state = value['state'] == null ? "" : value['state'];
            postcode = value['portalCode'] == null ? "" : " " + value['portalCode'];
            selectAddressBtn = '<button data-address-id="' + address_id + '" class="btn btn-sm btn-primary set-default-address crud-control">Set as default</button>'
            // editAddressBtn = '<button data-address-id="' + address_id + '" class="btn btn-sm btn-primary edit-address crud-control"><i class="fa fa-edit py-1"></i></button>';
            removeAddressBtn = '<button data-address-id="' + address_id + '" class="btn btn-sm btn-primary remove-address crud-control"><i class="fa fa-trash py-1"></i></button>';

            $("#address_list").append('<div class="col-6"><div class="detail-address">' + selectAddressBtn + removeAddressBtn + 
                        '<h4 class="title">Address ' + (key + 2) + '</h4><p>' + address + '</p><p>' + city + '</p><p>' + state + '</p><p>' + postcode + '</p></div></div>');
        });
    }
            
    $('.set-default-address').click(function(e) {
        $(this).prop('disabled', true);
        $("#modal_address_select").attr('data-address-id', $(this).attr('data-address-id'));
        $("#modal_address_select").modal('show');
    });
            
    $('.edit-address').click(function(e) {
        $(this).prop('disabled', true);
        $("#modal_address_edit").attr('data-address-id', $(this).attr('data-address-id'));
        $("#modal_address_edit").modal('show');
    });
            
    $('.remove-address').click(function(e) {
        $(this).prop('disabled', true);
        ajaxRemoveAddress($(this).attr('data-address-id'));
    });
    
    let card_details = data['card_details'];
    let cards = data['cards'];

    $("#card_list").html("");
    if (card_details != null || cards.length >= 1) {
        if (card_details != null) {
            let cardid = card_details['id'] == null ? "" : card_details['id'];
            let cardNumber = card_details['cardNumber'] == null ? "" : card_details['cardNumber'];
            let expiry_date = card_details['expiredMonth'] == null && card_details['expiredYear'] == null ? "" : card_details['expiredMonth'] + " / " + card_details['expiredYear'];

            $("#card_list").append('<div class="col-6"><div class="card-content"> \
                                        <h6>(Primary) \
                                        </h6> \
                                        <span class="country"></span> \
                                        <p class="title">XXXX XXXX XXXX <span>' + cardNumber + '</span></p> \
                                        <div class="card-bottom"> \
                                            <div class="expiry"> \
                                                <p class="valid-thru">Valid thru</p> \
                                                <p class="date">' + expiry_date + '</p> \
                                            </div> \
                                        </div> \
                                    </div></div>');
        }
        if (cards.length >= 1) {
            let cardNumber = null;
            let expiry_date = null;

            $.each(cards, function( key, value ) {
                cardid = value['id'] == null ? "" : value['id'];
                cardNumber = value['cardNumber'] == null ? "" : value['cardNumber'];
                expiry_date = value['expiredMonth'] == null && value['expiredYear'] == null ? "" : value['expiredMonth'] + " / " + value['expiredYear'];
                selectCardBtn = '<button data-card-id="' + cardid + '" class="btn btn-sm btn-primary set-default-card crud-control">Set as default</button>';
                removeCardBtn = '<button data-card-id="' + cardid + '" class="btn btn-sm btn-primary remove-card crud-control"><i class="fa fa-trash py-1"></i></button>';
    
                $("#card_list").append('<div class="col-6"><div class="card-content">' + selectCardBtn + removeCardBtn + 
                            '<p class="title">XXXX XXXX XXXX <span>' + cardNumber + '</span></p><div class="card-bottom"><div class="expiry"> \
                            <p class="valid-thru">Valid thru</p><p class="date">' + expiry_date + '</p></div></div></div></div>');
            });
        }
    }
    else {
        $("#card_details").html('<p>Not Available</p>');
    }
            
    $('.set-default-card').click(function(e) {
        $('.set-default-card').prop('disabled', true);
        ajaxSelectCard($(this).attr('data-card-id'));
    });
            
    $('.remove-card').click(function(e) {
        $(this).prop('disabled', true);
        ajaxRemoveCard($(this).attr('data-card-id'));
    });

    $(".states-select").html("");
    if (states.length >= 1) {
        $.each(states, function( key, value ) {
            $(".states-select").append('<option value="' + value['name'] + '">' + value['name'] +'</option>');
            if (value['isDefault'] == 1) {
                $(".states-select").val(value['name']);
            }
        });
    }
}

function ajaxSelectAddress(id, type) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/address/select/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                address_id: id,
                address_type: type,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('.set-default-address').prop('disabled', false);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxAddAddress() {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/address/create/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                address_detail: $("#add_address_detail").val(),
                city: $("#add_city").val(),
                SSN: $("#add_ssn").val(),
                portalCode: $("#add_portalcode").val(),
                state: $("#add_state").val(),
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('.add-address').prop('disabled', false);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxEditAddress(id) {
    $.ajax({
        type: "POST",
        url: API_URL + "/admin/user/customer/profile/update/" + customerid,
        data: {
            apptype: appType,
            address_id: $("#modal_address_edit").attr('data-address-id'),
            address_detail: $("#edit_address_detail").val(),
            city: $("#edit_city").val(),
            SSN: $("#edit_ssn").val(),
            portalCode: $("#edit_portalcode").val(),
            state: $("#edit_state").val(),
        },
        dataType: "json",
        success: function (data) {
            if (data && data.success === "success") {
                $(".edit-show").prop('disabled', false);
                $(".edit-hide").removeClass('d-none');
                $(".edit-show").addClass('d-none');
                
                $("#full_name").html(data.payload.firstName);
                $("#email").html('<a href="mailto:">' + data.payload.email + '</a>');
                $("#phone").html(data.payload.phone);
            }
            else {
                $(".edit-show").prop('disabled', false);
                $(".edit-hide").removeClass('d-none');
                $(".edit-show").addClass('d-none');
            }
        },
        error: function (response) {
            $(".edit-show").prop('disabled', false);
        }
    });
}

function ajaxRemoveAddress(id) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/address/remove/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                address_id: id,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSelectCard(id) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/card/select/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                card_id: id,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('.set-default-card').prop('disabled', false);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}
function ajaxAddCard(id) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/card/create/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                card_number: $("#add_card_number").val(),
                card_expiry_month: $("#add_expiry").val(),
                card_expiry_year: $("#add_expiry").val(),
                card_cvv: $("#add_cvv").val(),
                card_name: $("#customer_name").val(),
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('.set-default-card').prop('disabled', false);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxRemoveCard(id) {
    try {
        $.ajax({
            url: API_URL + '/admin/user/customer/card/remove/' + customerid,
            type: "POST",
            data: {
                apptype: appType,
                card_id: id,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                ajaxGetCustomerDetails(function (output) {
                    refreshCustomerDetails(output);
                });
            }
        });
    }
    catch (err) {
        //
    }
}
//CREDIT CARD INPUT MASK
// function CreditCardFormatMasked(value) {
//     var v_masked = value.replace(/\s/g, '').replace(/.(?!$)/gi, '•');
//     var matches_masked = v_masked.match(/.{4,16}/g);
//     var match_masked = matches_masked && matches_masked[0] || '';
//     var parts_masked = [];

//     for (i=0, len=match_masked.length; i<len; i+=4) {
//         parts_masked.push(match_masked.substring(i, i+4));
//     }

//     if (parts_masked.length) {
//         return parts_masked.join(' ');
//     } else {
//         return v_masked;
//     }
// }

// function CreditCardMaskAll(value) {
//     var v_masked = value.replace(/\s/g, '').replace(/./gi, '•');
//     var matches_masked = v_masked.match(/.{4,16}/g);
//     var match_masked = matches_masked && matches_masked[0] || '';
//     var parts_masked = [];

//     for (i=0, len=match_masked.length; i<len; i+=4) {
//         parts_masked.push(match_masked.substring(i, i+4));
//     }

//     if (parts_masked.length) {
//         return parts_masked.join(' ');
//     } else {
//         return v_masked;
//     }
// }

// function CreditCardFormat(value) {
//     var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

//     var matches = v.match(/.{4,16}/g);
//     var match = matches && matches[0] || '';
//     var parts = [];

//     for (i=0, len=match.length; i<len; i+=4) {
//         parts.push(match.substring(i, i+4));
//     }

//     if (parts.length) {
//         return parts.join('');
//     } else {
//         return v;
//     }
// }

// function ExpiryDateFormat(value) {
//     var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
//     // if (country_id == 'kr') {
//     // var matches = v.match(/\d{2,6}/g);
//     // }else{
//         var matches = v.match(/\d{2,4}/g);
//     // }
//     var match = matches && matches[0] || ''
//     var parts = []
//     var ExpiryDateFormatCount =0;
//     // if (country_id == 'kr') {
//     //  for (i = 0, len = match.length; i < len; i += 2) {
//     //     if(ExpiryDateFormatCount == 0){
//     //     parts.push(match.substring(i, i + 2))
//     //     }else{
//     //         parts.push(match.substring(i, i + 4))
//     //         i=i+2;
//     //     }
//     //     ExpiryDateFormatCount++;
//     //  }
//     // }
//     // else{
//      for (i = 0, len = match.length; i < len; i += 2) {
//         parts.push(match.substring(i, i + 2))
//      }
//     // }
//     if (parts.length) {
//         return parts.join(' / ')
//     } else {
//         return value
//     }
// }

$(document).ready(function () {
    ajaxGetCustomerDetails(function (output) {
        refreshCustomerDetails(output);
    });

    $('#form_edit_profile').validate({
        rules: {
            customer_name: {
                required: true,
                maxlength: 100
            },
            customer_email: {
                required: true,
                maxlength: 100,
                email: true,
            },
        }
    });

    $('#add_address_form').validate({
        rules: {
            add_address_detail: {
                required: true,
            },
            add_ssn: {
                required: true,
            },
            add_city: {
                required: true,
            },
            add_portalcode: {
                required: true,
            },
            add_state: {
                required: true,
            },
        }
    });

    $("#switch_edit_profile").on("click", function (event) {
        $(".edit-hide").addClass('d-none');
        $(".edit-show").removeClass('d-none');
    });

    $("#cancel_edit_profile").on("click", function (event) {
        $(".edit-hide").removeClass('d-none');
        $(".edit-show").addClass('d-none');
    });

    $("#form_edit_profile").submit(function (event) {
        event.preventDefault();
        
        if ($("#form_edit_profile").valid() === true) {
            $(".edit-show").prop('disabled', true);
            let form = $("#form_edit_profile");
            let formData = form.serialize();

            
        }
    });

    $("#set_default_shipping").on("click", function (event) {
        ajaxSelectAddress($("#modal_address_select").attr('data-address-id'), "shipping");
        $("#modal_address_select").modal('hide');
    });

    $("#set_default_billing").on("click", function (event) {
        ajaxSelectAddress($("#modal_address_select").attr('data-address-id'), "billing");
        $("#modal_address_select").modal('hide');
    });

    $("#add_address").on("click", function (event) {
        event.preventDefault();

        if ($("#add_address_form").valid() === true) {
            ajaxAddAddress($("#modal_address_add").attr('data-address-id'));
            $("#modal_address_add").modal('hide');
        }
    });

    $("#modal_address_select").on('hide.bs.modal', function (event) {
        console.log("test");
        $(".set-default-address").prop('disabled', false);
    });

    // $('#add_card_number_masked').keydown(function (event) {

    //     $(this).val(CreditCardFormat($('#add_card_number').val()));
    // });

    // $('#add_card_number_masked').on('input',function (event) {
    //     $('#add_card_number').val(CreditCardFormat($(this).val()));
    //     $(this).val(CreditCardFormatMasked($('#add_card_number').val()));
    // });

    // $('#add_card_number_masked').blur(function (event) {
    //     // deselectd control
    //     $(this).val(CreditCardMaskAll($(this).val()));
    // });

    // $('#add_card_expiry').keyup(function() {
    //     $(this).val(ExpiryDateFormat($(this).val()));
    // })

    // $(".number-only").on('input', function(value) {
    //     if ((value.which < 48 || value.which > 57) && (value.which !== 8) && (value.which !== 0)) {
    //         return false;
    //     }
    //     return true;
    // });
});