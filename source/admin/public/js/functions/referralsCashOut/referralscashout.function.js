let referraldata = [];
let countries = null;
let currentpage = null;
let pages = null;

function ajaxGetReferralsCashOut(handleResponse) {
    referraldata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/referrals/cashout/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                fromdate: $("#filterByDateFrom").val(),
                todate: $("#filterByDateTo").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxGetReferralCashOutBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxGetReferralCashOutBlock() {
    $.ajax ({
        url: API_URL + '/admin/referrals/cashout',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            status: $("#filterByStatus").val(),
            fromdate: $("#filterByDateFrom").val(),
            todate: $("#filterByDateTo").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReferralsCashOut(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxGetReferralCashOutBlock(currentpage);
            }
        }
    });
}

function ajaxSearchReferralsCashOut(handleResponse) {
    referraldata = [];

    try {
        $.ajax ({
            url: API_URL + '/admin/referrals/cashout/count',
            type: "POST",
            data: {
                appType:appType,
                maxresults: $("select[name='DataTables_Table_0_length']").val(),
                search: $("#filterBySearch").val(),
                status: $("#filterByStatus").val(),
                pagenum: 1,
            },
            dataType: "json",
            success: function(response){
                // get number of pages
                console.log(response.pages);
                pages = response.pages;
                currentpage = 1;
                ajaxSearchReferralCashOutBlock();
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxSearchReferralCashOutBlock() {
    $.ajax ({
        url: API_URL + '/admin/referrals/cashout',
        type: "POST",
        data: {
            appType:appType,
            maxresults: $("select[name='DataTables_Table_0_length']").val(),
            search: $("#filterBySearch").val(),
            status: $("#filterByStatus").val(),
            pagenum: currentpage,
        },
        dataType: "json",
        success: function(response){
            refreshReferralsCashOut(response.data);
            if (currentpage < pages) {
                currentpage = currentpage + 1;
                console.log(currentpage)
                ajaxSearchReferralCashOutBlock(currentpage);
            }
        }
    });
}

function refreshReferralsCashOut(data) {
    $.each(data, function( key, value ) {
        // remove duplicate data according to id
        // referraldata = referraldata.filter(obj => obj.rid !== value.rid);

        referraldata.push(value);
    });
    referraldata = referraldata.sort(function(a, b) {
        return b.id - a.id;
    });

    generateReferralsCashOutTable();
}


function generateReferralsCashOutTable() {
    $("#referralscashout_list tr").remove();
    $('.js-dataTable-buttons').dataTable().fnDestroy();
    $("#referralscashout_list tr").remove();

    var resultsCount = 0;
    if (referraldata.length >= 1) {
        $.each( referraldata, function( key, value ) {
            let currencyDisplay = value['currencyDisplay'] ? value['currencyDisplay'] : "";
            let email = value['email'] ? value['email'] : "";
            let Country = value['cname'] ? value['cname'] : "";
            let totalcredit = value['availablemoney'] ? value['availablemoney'] : "0";
            let cashoutmoney = value['amount'] ? value['amount'] : "0"; 
            let date = value['created_at'] ? value['created_at'] : "0"; 
            let status = value['status'] ? value['status'] : ""; 

            if (value['email'] != null) {
                if (email.indexOf($("#filterBySearch").val()) != -1 ) {
                    $("#referralscashout_list").append('<tr><td>' + email + 
                    '</td><td>' + Country + 
                    '</td><td>' +  currencyDisplay + Number(totalcredit).toFixed(2) + 
                    '</td><td>' +  currencyDisplay + Number(cashoutmoney).toFixed(2) +
                    '</td><td>' + status +
                    '</td><td>' + date + 
                    '</td><td class="text-center"> \
                    <a class="btn btn-sm btn-light js-tooltip-enabled view-details" href="' + GLOBAL_URL + '/referral/cashout/details/' + value['id'] +'" data-original-title="View Details"><i class="fa fa-search"></i></a> \
                    </td></tr>');
                    resultsCount++;
                }
            }
        });
        if (resultsCount < 1) {
            $("#referralscashout_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
        }
        else {
            console.log(resultsCount);
            $('.js-dataTable-buttons').DataTable({
                bDestroy: true,
                pageLength: 50,
                lengthMenu: [[50, 100, 150, 200], [50, 100, 150, 200]],
                autoWiDth: false,
                searching: false,
                dom: "<l<t>ip>",
                order: [],
            });
        }
    }
    else {
        $("#referralscashout_list").append('<tr><td class="text-center" colspan="8">No Items</td></tr>');
    }
    hideLoading();
}

$(document).ready(function() {
    $("#start_filter").on('click', function () {
        showLoading();

        ajaxGetReferralsCashOut();
     
    });
    // ajaxGetReferrals();

    // $(".filterReferrals").change(function() {
    //     ajaxGetReferrals();
    // });

    // $("#filterBySearch").keyup(function() {
    //     generateReferralsTable();
    //     // ajaxSearchReferrals();
    // });
});