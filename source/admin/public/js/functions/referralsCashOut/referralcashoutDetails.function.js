let status = "";

function ajaxGetReferralCashoutDetails(handleResponse) {
    try {
        $.ajax({
            url: API_URL + '/admin/referrals/cashout/details',
            type: "POST",
            data: {
                apptype: appType,
                id: referralid,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                handleResponse(response);
            }
        });
    }
    catch (err) {
        //
    }
}

function ajaxChangeReferralCashoutStatus() {
    try {
        $.ajax({
            url: API_URL + '/admin/referrals/cashout/status/update',
            type: "POST",
            data: {
                apptype: appType,
                id: referralid,
                status: $("#change_status").children("option:selected").val(),
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                status = response;
                $("#modal_status_confirm").modal('hide');
            }
        });
    }
    catch (err) {
        //
    }
}


function refreshReferralCashoutDetails(data) {
    let email = data['email'] == null ? "" : data['email'];
    let countryname = data['country']['name'] == null ? "" : data['country']['name'];
    let currency = data['country']['currencyCode'] == null ? "" : data['country']['currencyCode'];
    let amount = data['amount'] == null ? "" : data['amount'];
    let bank_name = data['bank_name'] == null ? "" : data['bank_name'];
    let fullname = data['name'] == null ? "" : data['name'];
    let bank_acc_no = data['bank_account_no'] == null ? "" : data['bank_account_no'];
    let created_date = data['created_at_date'] == null ? "" : data['created_at_date'];
    status = data['status'] == null ? "" : data['status'];

    $("#email").text(email);
    $("#country").text(countryname);
    $("#amount").text(currency + " " + amount);
    $("#bank_name").text(bank_name);
    $("#full_name").text(fullname);
    $("#bank_acc_no").text(bank_acc_no);
    $("#created_date").text(created_date);
    $("#change_status").val(status);
    if (status == "withdrawn") {
        $("#change_status").prop('disabled', 'disabled');
    }
}

function refreshReferralCashoutStatus(data) {
    console.log("do");
    $("#change_status").val(data);
    if (data == "withdrawn") {
        $("#change_status").prop('disabled', 'disabled');
    }
}

$(document).ready(function() {
    ajaxGetReferralCashoutDetails(function (output) {
        refreshReferralCashoutDetails(output);
    });

    $("#change_status").change(function () {
        $("#modal_status_confirm").modal('show');
    });

    $("#referral_cashout_status_confirmed").click(function () {
        ajaxChangeReferralCashoutStatus();
    });

    $('#modal_status_confirm').on('hidden.bs.modal', function () {
        refreshReferralCashoutStatus(status);
    })
});