<?php

use Illuminate\Database\Seeder;
use App\Models\GeoLocation\LanguageDetails;

class LanguageDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $languagedetailArray = [
            ['1', 'EN', 'EN','English','Malaysia (EN)'],
            ['2', 'EN', 'EN','English', 'Hong Kong (EN)'],
            ['2', 'ZH', 'ZH-HK','Chinese', 'Hong Kong (ZH-HK)'],
            ['3', 'EN', 'EN','English', 'India (EN)'],
            ['4', 'EN', 'EN','English', 'Thailand (EN)'],
            ['5', 'EN', 'EN','English', 'Philippines (EN)'],
            ['6', 'EN', 'EN','English', 'Indonesia (EN)'],
            ['7', 'EN', 'EN','English', 'Singapore (EN)'],
            ['8', 'KO', 'KO','Korean', 'Korea (KO)'],
            ['9', 'EN', 'EN','English', 'Taiwan (EN)'],
            ['9', 'ZH', 'ZH-TW','Chinese', 'Taiwan (ZH-TW)'],
        ];

        LanguageDetails::truncate();
        foreach ( $languagedetailArray as $languagedetail) {
            $languagedetails = new LanguageDetails();
            $languagedetails->CountryId = $languagedetail[0];
            $languagedetails->mainlanguageCode = $languagedetail[1];
            $languagedetails->sublanguageCode = $languagedetail[2];
            $languagedetails->sublanguageName = $languagedetail[3];
            $languagedetails->languageName = $languagedetail[4];
            $languagedetails->save();
        }
    }
    public function command($msg)
    {
        $this->command->info($msg);
    }
}
