<?php

use Illuminate\Database\Seeder;
use App\Models\Products\ProductCountry;
use App\Models\Products\Product;
use App\Models\Products\ProductTypeDetail;
/*ProductTypeDetail*/
class ProductTypeDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("producttypedetails", $now);

        $this->command(__CLASS__ . " Done...");
    }

    public function addRecords($type, $now) {
    

        ProductTypeDetail::truncate();
        $productlist = Product::select('id', 'ProductTypeId')  // plan id
            ->orderByRaw("id asc")
            ->get()->toArray();
  
      
        foreach ($productlist as $p) {
            $this->command(__FUNCTION__ . "starting adding -> ".$p["id"]." ".$p["ProductTypeId"] );
            $productlist = new ProductTypeDetail();
            $productlist->ProductId = $p["id"];
            $productlist->ProductTypeId = $p["ProductTypeId"];
            $productlist->save();
        }


        $this->command(__FUNCTION__ . "  ===== Successfully added product records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }
    
    public function command($msg) {
        $this->command->info($msg);
    }

}
