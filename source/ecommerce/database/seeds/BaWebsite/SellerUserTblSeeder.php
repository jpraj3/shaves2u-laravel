<?php

use App\Models\BaWebsite\SellerUser;
use Illuminate\Database\Seeder;

class SellerUserTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " => Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("SellerUser", $now);

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type, $now)
    {
        $SellerUser = [
            ['1', 'BATESTMY', '123412341234', 'BATEST SELLER', '2016-09-23', 'SHAVES2U', 'ba-my@shaves2u.com', '2019-06-11', '2017-10-26 11:21:16', '2019-06-11 16:01:10', 1, 1, null, null, 1, '2019-04-24 10:42:44', '$2y$12$BCtWvmp1L5ZYqBRk6uQWD.zTFQn7YH4IWL1aqYarOsUESnxDyeiwa'],
            ['2', 'BATESTSG', '123412341234', 'BATEST SELLER', '2016-09-23', 'SHAVES2U', 'ba-sg@shaves2u.com', '2019-06-11', '2017-10-26 11:21:16', '2019-06-11 16:01:10', 7, 1, null, null, 1, '2019-04-24 10:42:44', '$2y$12$BCtWvmp1L5ZYqBRk6uQWD.zTFQn7YH4IWL1aqYarOsUESnxDyeiwa'],
            ['3', 'BATESTHK', '123412341234', 'BATEST SELLER', '2016-09-23', 'SHAVES2U', 'ba-hk@shaves2u.com', '2019-06-11', '2017-10-26 11:21:16', '2019-06-11 16:01:10', 2, 1, null, null, 1, '2019-04-24 10:42:44', '$2y$12$BCtWvmp1L5ZYqBRk6uQWD.zTFQn7YH4IWL1aqYarOsUESnxDyeiwa'],
            ['4', 'BATESTKR', '123412341234', 'BATEST SELLER', '2016-09-23', 'SHAVES2U', 'ba-kr@shaves2u.com', '2019-06-11', '2017-10-26 11:21:16', '2019-06-11 16:01:10', 8, 1, null, null, 1, '2019-04-24 10:42:44', '$2y$12$BCtWvmp1L5ZYqBRk6uQWD.zTFQn7YH4IWL1aqYarOsUESnxDyeiwa'],
            ['5', 'BATESTTW', '123412341234', 'BATEST SELLER', '2016-09-23', 'SHAVES2U', 'ba-tw@shaves2u.com', '2019-06-11', '2017-10-26 11:21:16', '2019-06-11 16:01:10', 9, 1, null, null, 1, '2019-04-24 10:42:44', '$2y$12$BCtWvmp1L5ZYqBRk6uQWD.zTFQn7YH4IWL1aqYarOsUESnxDyeiwa'],
        ];

        $this->command(__FUNCTION__ . " => SellerUser record has not been found. Adding new SellerUser....");
        foreach ($SellerUser as $sa) {
            $SellerUser = new SellerUser();
            $SellerUser->id = $sa[0];
            $SellerUser->badgeId = $sa[1];
            $SellerUser->icNumber = $sa[2];
            $SellerUser->agentName = $sa[3];
            $SellerUser->startDate = $sa[4];
            $SellerUser->campaign = $sa[5];
            $SellerUser->email = $sa[6];
            $SellerUser->latestUpdatedDate = $sa[7];
            $SellerUser->created_at = $sa[8];
            $SellerUser->updated_at = $sa[9];
            $SellerUser->CountryId = $sa[10];
            $SellerUser->isActive = $sa[11];
            $SellerUser->channelType = $sa[12];
            $SellerUser->eventLocationCode = $sa[13];
            $SellerUser->MarketingOfficeId = $sa[14];
            $SellerUser->lastLoginAt = $sa[15];
            $SellerUser->password = $sa[16];
            $SellerUser->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added SellerUser records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
