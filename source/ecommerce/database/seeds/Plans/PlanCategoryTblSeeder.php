<?php

use App\Models\Plans\PlanCategory;
use Illuminate\Database\Seeder;

class PlanCategoryTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " => Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("PlanCategory", $now);

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type, $now)
    {
        $PlanCategory = [
            ['1', 'Trial Plan', 'Trial Plan'],
            ['2', 'Custom Plan', 'Custom Plan'],
            ['3', 'Women Trial Plan', 'Women Trial Plan'],
            ['4', 'Women Custom Plan', 'Women Custom Plan'],
        ];

        $this->command(__FUNCTION__ . " => PlanCategory record has not been found. Adding new PlanCategory....");
        foreach ($PlanCategory as $sa) {
            $PlanCategory = new PlanCategory();
            $PlanCategory->id = $sa[0];
            $PlanCategory->name = $sa[1];
            $PlanCategory->type = $sa[2];
            $PlanCategory->created_at = $now;
            $PlanCategory->updated_at = $now;
            $PlanCategory->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added PlanCategory records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
