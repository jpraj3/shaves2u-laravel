<?php

use App\Models\Plans\PlanCategoryTranslate;
use Illuminate\Database\Seeder;

class PlanCategoryTranslatesTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " => Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("PlanCategoryTranslate", $now);

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type, $now)
    {
        $PlanCategoryTranslate = [
            ['EN', 'Trial Plan', '1', 'Trial Plan', 'Trial Plan', 'Trial Plan', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['KO', 'Trial Plan KO', '1', 'Trial Plan KO', 'Trial Plan KO', 'Trial Plan KO', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['ZH-HK', 'Trial Plan ZH-HK', '1', 'Trial Plan ZH-HK', 'Trial Plan ZH-HK', 'Trial Plan ZH-HK', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['ZH-TW', 'Trial Plan ZH-TW', '1', 'Trial Plan ZH-TW', 'Trial Plan ZH-TW', 'Trial Plan ZH-TW', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],

            ['EN', 'Custom Plan', '2', 'Custom Plan', 'Custom Plan', 'Custom Plan', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['KO', 'Custom Plan KO', '2', 'Custom Plan KO', 'Custom Plan KO', 'Custom Plan KO', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['ZH-HK', 'Custom Plan ZH-HK', '2', 'Custom Plan ZH-HK', 'Custom Plan ZH-HK', 'Custom Plan ZH-HK', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['ZH-TW', 'Custom Plan ZH-TW', '2', 'Custom Plan ZH-TW', 'Custom Plan ZH-TW', 'Custom Plan ZH-TW', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],

            ['EN', 'Women Trial Plan', '3', 'Women Trial Plan', 'Women Trial Plan', 'Women Trial Plan', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['KO', 'Women Trial Plan KO', '3', 'Women Trial Plan KO', 'Women Trial Plan KO', 'Women Trial Plan KO', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['ZH-HK', 'Women Trial Plan ZH-HK', '3', 'Women Trial Plan ZH-HK', 'Women Trial Plan ZH-HK', 'Women Trial Plan ZH-HK', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],
            ['ZH-TW', 'Women Trial Plan ZH-TW', '3', 'Women Trial Plan ZH-TW', 'Women Trial Plan ZH-TW', 'Women Trial Plan ZH-TW', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/4f6eab40-5da5-11e8-8c24-e53a0aa7dfcd.png'],

            ['EN', 'Women Custom Plan', '4', 'Women Custom Plan', 'Women Custom Plan', 'Women Custom Plan', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['KO', 'Women Custom Plan KO', '4', 'Women Custom Plan KO', 'Women Custom Plan KO', 'Women Custom Plan KO', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['ZH-HK', 'Women Custom Plan ZH-HK', '4', 'Women Custom Plan ZH-HK', 'Women Custom Plan ZH-HK', 'Women Custom Plan ZH-HK', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],
            ['ZH-TW', 'Women Custom Plan ZH-TW', '4', 'Women Custom Plan ZH-TW', 'Women Custom Plan ZH-TW', 'Women Custom Plan ZH-TW', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24228770-c87d-11e8-9865-d1b5ef115113.jpg'],

        ];

        PlanCategoryTranslate::truncate();
        $this->command(__FUNCTION__ . " => PlanCategoryTranslate record has not been found. Adding new PlanCategoryTranslate....");
        foreach ($PlanCategoryTranslate as $sa) {
            $PlanCategoryTranslate = new PlanCategoryTranslate();
            $PlanCategoryTranslate->langCode = $sa[0];
            $PlanCategoryTranslate->translate = $sa[1];
            $PlanCategoryTranslate->PlanCategoryId = $sa[2];
            $PlanCategoryTranslate->translate_title = $sa[3];
            $PlanCategoryTranslate->translate_subtitle = $sa[4];
            $PlanCategoryTranslate->translate_description = $sa[5];
            $PlanCategoryTranslate->translate_url = $sa[6];
            $PlanCategoryTranslate->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added PlanCategoryTranslate records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
