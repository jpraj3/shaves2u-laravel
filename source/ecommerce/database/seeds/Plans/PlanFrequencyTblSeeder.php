<?php

use App\Models\Plans\PlanFrequency;
use Illuminate\Database\Seeder;

class PlanFrequencyTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $rows = [

            // Malaysia
            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 1, 1, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 1, 1, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 1, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 1, 'EN'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 1, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 1, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 1, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 1, 'EN'],

            // Hong Kong
            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 2, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 2, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 2, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 2, 'EN'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 2, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 2, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 2, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 2, 'EN'],

            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 2, 'ZH-HK'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 2, 'ZH-HK'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 2, 'ZH-HK'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 2, 'ZH-HK'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 2, 'ZH-HK'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 2, 'ZH-HK'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 2, 'ZH-HK'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 2, 'ZH-HK'],

            // Singapore
            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 1, 7, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 1, 7, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 7, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 7, 'EN'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 7, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 7, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 7, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 7, 'EN'],

            // Korea
            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 1, 8, 'KO'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 1, 8, 'KO'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 8, 'KO'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 8, 'KO'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 8, 'KO'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 8, 'KO'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 8, 'KO'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 8, 'KO'],


            // Taiwan
            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 9, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 9, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 9, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 9, 'EN'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 9, 'EN'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 9, 'EN'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 9, 'EN'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 9, 'EN'],

            // Trial Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 9, 'ZH-TW'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 9, 'ZH-TW'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 1, 9, 'ZH-TW'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 1, 9, 'ZH-TW'],

            // Custom Plan
            [2, 'month', '2 months', '/images/common/checkoutAssets/icon-frequency-1.png', '5 - 7 days<br>per week', 'Delivery every <b>2 months</b>', 'Nice! Looking forward to <br> you every 2 months.', 1, 2, 9, 'ZH-TW'],
            [3, 'month', '3 months', '/images/common/checkoutAssets/icon-frequency-2.png', '2 - 4 days<br>per week', 'Delivery every <b>3 months</b>', 'Nice! Looking forward to <br> you every 3 months.', 1, 2, 9, 'ZH-TW'],
            [4, 'month', '4 months', '/images/common/checkoutAssets/icon-frequency-3.png', '1 day<br>per week<br>', 'Delivery every <b>4 months</b>', 'Nice! Looking forward to <br> you every 4 months.', 1, 2, 9, 'ZH-TW'],
            [6, 'month', '6 months', '/images/common/checkoutAssets/icon-frequency-4.png', 'Once every<br>two weeks<br>', 'Delivery every <b>6 months</b>', 'Nice! Looking forward to <br> you every 6 months.', 1, 2, 9, 'ZH-TW'],
        ];

        $this->command(__FUNCTION__ . " Truncating data...");
        PlanFrequency::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($rows as $row) {
            $planfrequency = new PlanFrequency();
            $planfrequency->duration = $row[0];
            $planfrequency->durationType = $row[1];
            $planfrequency->durationName = $row[2];
            $planfrequency->checkout_image_url = $row[3];
            $planfrequency->checkout_duration_text = $row[4];
            $planfrequency->checkout_details_text = $row[5];
            $planfrequency->checkout_selected_details_text = $row[6];
            $planfrequency->showInCheckoutPage = $row[7];
            $planfrequency->planCategoryId = $row[8];
            $planfrequency->countryId = $row[9];
            $planfrequency->langCode = $row[10];
            $planfrequency->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
