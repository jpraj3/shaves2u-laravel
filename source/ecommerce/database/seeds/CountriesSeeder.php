<?php

use Illuminate\Database\Seeder;
use App\Models\GeoLocation\Countries;
class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $countries = [
            ['MYS', 'MY', 'Malaysia', 'MYR', 'RM', 'EN', '60', 1, 'Shaves2u (M) Sdn. Bhd.', '(1037174-T)', 'Level 18, Axiata Tower, No. 9, Jalan Stesen Sentral 5, Kuala Lumpur Sentral, 50470, KL, Malaysia.', 0, 'GST', 'SR', 0, '12334', 'help@shaves2u.com', '+603 2262 2330', 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00', 1],
            ['HKG', 'HK', 'Hong Kong', 'HKD', 'HKD', 'EN', '852', 1, 'Shaves2u Malaysia', 'xxx', 'yyy', 0, 'GST', NULL, 0, '12334', 'help@shaves2u.com', '+841659612968', 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00', 2],
            ['IND', 'IN', 'India', 'INR', 'INR', 'EN', '91', 0, 'Shaves2u Malaysia', 'xxx', 'yyy', 5, 'GST', NULL, 1, '12334', 'help@shaves2u.com', '+841659612968', 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00', 3],
            ['THA', 'THA', 'Thailand', 'THB', 'THB', 'EN', '66', 0, 'Shaves2u Malaysia', 'xxx', 'yyy', 5, 'GST', NULL, 1, '12334', 'help@shaves2u.com', '+841659612968', 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00', 4],
            ['PHL', 'PHL', 'Philippines', 'PHP', '₱', 'EN', '63', 0, 'Shaves2u Malaysia', 'xxx', 'yyy', 5, 'GST', NULL, 1, '12334', 'help@shaves2u.com', '+841659612968', 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00', 5],
            ['IDN', 'IND', 'Indonesia', 'IDR', 'IDR', 'EN', '62', 0, 'Shaves2u Indonesia', 'xxx', 'yyy', 5, 'GST', NULL, 1, '12334', 'help@shaves2u.com', '+841659612968', 0, 0, 0, 0, 0, 0, 0, 0, '0.00', '0.00', '0.00', 6],
            ['SGP', 'SG', 'Singapore', 'SGD', 'S$', 'EN', '65', 1, 'Shaves2u Singapore', 'xxx', 'Singapore address line 1, line 2. Singapore', 7, 'GST', 'SR', 1, '12334', 'help@shaves2u.com', '+65 31630325', 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00', 7],
            ['KOR', 'KR', 'Korea', 'KRW', '₩', 'KO', '82', 1, 'Shaves2u Korean', 'xxx', 'yyy', 10, 'GST', 'SR', 1, '12334', 'help@shaves2u.com', '+841659612968', 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00', 8],
            ['TWN', 'TW', 'Taiwan', 'TWD', 'NT$', 'ZH', '886', 1, 'Shaves2u Taiwan', 'xxx', 'xxx', 0, 'VAT', NULL, 1, '12334', 'help.tw@shaves2u.com', '60327732882', 1, 1, 1, 1, 1, 1, 1, 1, '0.00', '0.00', '0.00', 9],
        ];
        $this->command(__FUNCTION__ . " Truncating data...");
        Countries::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($countries as $ctry) {
            $country = new Countries();
            $country->code = $ctry[0];
            $country->codeIso = $ctry[1];
            $country->name = $ctry[2];
            $country->currencyCode = $ctry[3];
            $country->currencyDisplay = $ctry[4];
            $country->defaultLang = $ctry[5];
            $country->callingCode = $ctry[6];
            $country->isActive = $ctry[7];
            $country->companyName = $ctry[8];
            $country->companyRegistrationNumber = $ctry[9];
            $country->companyAddress = $ctry[10];
            $country->taxRate = $ctry[11];
            $country->taxName = $ctry[12];
            $country->taxCode = $ctry[13];
            $country->includedTaxToProduct = $ctry[14];
            $country->taxNumber = $ctry[15];
            $country->email = $ctry[16];
            $country->phone = $ctry[17];
            $country->isBaEcommerce = $ctry[18];
            $country->isBaSubscription = $ctry[19];
            $country->isBaAlaCarte = $ctry[20];
            $country->isBaStripe = $ctry[21];
            $country->isWebEcommerce = $ctry[22];
            $country->isWebSubscription = $ctry[23];
            $country->isWebAlaCarte = $ctry[24];
            $country->isWebStripe = $ctry[25];
            $country->shippingMinAmount = $ctry[26];
            $country->shippingFee = $ctry[27];
            $country->shippingTrialFee = $ctry[28];
            $country->order = $ctry[29];
            $country->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
