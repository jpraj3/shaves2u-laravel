<?php

use Illuminate\Database\Seeder;
use App\Models\Orders\OrderDetails;

use Illuminate\Support\Facades\DB;
class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $orderdetails = $db_old->table('orderdetails')->get();
          // old site (plancountries id) => new site (plans id)
        $planlist = array(
            '59'=>'1',
            '60'=>'105',
            '89'=>'177',
            '61'=>'2',
            '62'=>'108',
            '90'=>'156',
            '63'=>'4',
            '64'=>'106',
            '91'=>'178',
            '65'=>'5',
            '66'=>'109',
            '93'=>'157',
            '67'=>'7',
            '68'=>'107',
            '92'=>'155',
            '69'=>'8',
            '70'=>'110',
            '94'=>'158',
            '83'=>'3',
            '84'=>'111',
            '95'=>'159',
            '87'=>'6',
            '88'=>'112',
            '96'=>'160',
            '85'=>'9',
            '86'=>'113',
            '97'=>'161',
            '146'=>'115',
            '147'=>'163',
            '149'=>'32',
            '150'=>'33',
            '151'=>'116',
            '152'=>'164',
            '154'=>'31',
            '155'=>'114',
            '156'=>'162',
            '200'=>'129',
            '230'=>'80',
            '201'=>'130',
            '231'=>'81',
            '202'=>'131',
            '232'=>'82',
            '203'=>'138',
            '233'=>'89',
            '204'=>'132',
            '234'=>'83',
            '220'=>'133',
            '235'=>'84',
            '206'=>'134',
            '236'=>'85',
            '207'=>'139',
            '237'=>'90',
            '208'=>'135',
            '238'=>'86',
            '209'=>'136',
            '239'=>'87',
            '210'=>'137',
            '240'=>'88',
            '211'=>'140',
            '241'=>'91',
            '107'=>'201',
            '108'=>'202',
            '127'=>'203',
            '221'=>'204',
            '199'=>'205',
            '101'=>'206',
            '102'=>'207',
            '124'=>'208',
            '224'=>'209',
            '214'=>'210',
            '113'=>'211',
            '114'=>'212',
            '119'=>'213',
            '227'=>'214',
            '217'=>'215',
            '109'=>'216',
            '110'=>'217',
            '126'=>'218',
            '222'=>'219',
            '212'=>'220',
            '103'=>'221',
            '104'=>'222',
            '123'=>'223',
            '225'=>'224',
            '215'=>'225',
            '115'=>'226',
            '116'=>'227',
            '120'=>'228',
            '228'=>'229',
            '218'=>'230',
            '111'=>'231',
            '112'=>'232',
            '125'=>'233',
            '223'=>'234',
            '213'=>'235',
            '105'=>'236',
            '106'=>'237',
            '122'=>'238',
            '226'=>'239',
            '272'=>'240',
            '117'=>'241',
            '118'=>'242',
            '121'=>'243',
            '229'=>'244',
            '219'=>'245',
            '161'=>'246',
            '158'=>'247',
            '159'=>'248',
            '162'=>'251',
            '163'=>'252',
            '164'=>'253',
            '271'=>'254',
            '166'=>'256',
            '167'=>'257',
            '168'=>'258',
            '255'=>'259',
            '75'=>'216',
            '76'=>'217',
            '77'=>'221',
            '78'=>'222',
            '79'=>'231',
            '80'=>'232',
            '81'=>'236',
            '82'=>'237',
            '1'=>'261',
            '2'=>'262',
            '3'=>'263',
            '4'=>'264',
            '5'=>'265',
            '6'=>'266',
            '7'=>'267',
            '8'=>'268',
            '9'=>'269',
            '10'=>'270',
            '12'=>'271',
            '13'=>'272',
            '15'=>'273',
            '16'=>'274',
            '17'=>'275',
            '18'=>'276',
            '19'=>'277',
            '21'=>'278',
            '23'=>'279',
            '25'=>'280',
            '27'=>'281',
            '29'=>'282',
            '31'=>'283',
            '33'=>'284',
            '35'=>'285',
            '36'=>'286',
            '37'=>'287',
            '38'=>'288',
            '39'=>'289',
            '40'=>'290',
            '41'=>'291',
            '42'=>'292',
            '43'=>'293',
            '44'=>'294',
            '45'=>'295',
            '46'=>'296',
            '47'=>'297',
            '48'=>'298',
            '49'=>'299',
            '50'=>'300',
            '51'=>'301',
            '52'=>'302',
            '53'=>'303',
            '54'=>'304',
            '55'=>'305',
            '56'=>'306',
            '57'=>'307',
            '58'=>'308',
            '71'=>'309',
            '73'=>'310',
            '258'=>'311',
            '259'=>'312',
            '260'=>'313',
            '261'=>'314',
            '262'=>'315',
            '263'=>'316',
            '264'=>'317',
            '268'=>'318',
            '269'=>'319',
            '270'=>'320',
            '242'=>'92',
            '243'=>'93',
            '244'=>'196',
            '245'=>'94',
            '246'=>'101',
            '247'=>'95',
            '248'=>'96',
            '249'=>'97',
            '250'=>'102',
            '251'=>'98',
            '252'=>'99',
            '253'=>'100',
            '254'=>'103',
            '256'=>'197',
            '257'=>'198'
        );
        // $this->command(__FUNCTION__ . " Truncating data...");
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($orderdetails as $od) {
            $ods = new OrderDetails();
            $ods->id = $od->id;
            $ods->qty = $od->qty;
            $ods->price = $od->price;
            $ods->currency = $od->currency;
            $ods->startDeliverDate = $od->startDeliverDate;
            $ods->created_at = $od->createdAt;
            $ods->updated_at = $od->updatedAt;
            $ods->OrderId = $od->OrderId;
            $ods->ProductCountryId = $od->ProductCountryId;

            if(!empty($od->PlanCountryId)){
                if(!empty($planlist[$od->PlanCountryId])){
                    $ods->PlanId = (int)$planlist[$od->PlanCountryId];
                    }else{
                        $this->command(__FUNCTION__ . " Empty Trial Plan, id: ".$od->PlanCountryId );
                    }
            }

            $ods->isFreeProduct = 0;
            if ($od->ProductCountryId==28 || $od->ProductCountryId==29 || $od->ProductCountryId==160 || $od->ProductCountryId==170 || $od->ProductCountryId==208 ) {
                $ods->isAddon = 1;
            }else{
                $ods->isAddon = 0;
            }

            $ods->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
