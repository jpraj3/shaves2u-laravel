<?php

use App\Models\Products\ProductTypeTranslate;
use Illuminate\Database\Seeder;

/*product*/
class ProductTypeTranslateTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("productTypeTranslate", $now);

        $this->command(__CLASS__ . " Done...");
    }

    public function addRecords($type, $now)
    {
        $producttypetranslate = [
            [1, 'Shave Kit', 'EN', 1]
            , [2, 'Starter Packs', 'EN', 2]
            , [3, 'Handles', 'EN', 3]
            , [4, 'Cartridges', 'EN', 4]
            , [5, 'Skin Care', 'EN', 5]
            , [6, 'Womens', 'EN', 6]
            , [7, '손잡이', 'KO', 3]
            , [8, '스타터 팩', 'KO', 2]
            , [9, '카트리지 팩', 'KO', 4]
            , [10, '스킨 케어', 'KO', 5]
            , [11, '여성용', 'KO', 6]
            , [12, 'A.S.K（超凡刮鬍禮盒）', 'ZH-TW', 1]
            , [13, '試用禮盒', 'ZH-TW', 2]
            , [14, '刮鬍刀', 'ZH-TW', 4]
            , [15, '刮鬍護理', 'ZH-TW', 5]
            , [16, 'Womens_TW', 'ZH-TW', 6]
            , [17, '刀架類型', 'ZH-TW', 3]
            , [18, 'A.S.K（超凡刮鬍禮盒）YU', 'ZH-HK', 1]
            , [19, 'Womens_YU', 'ZH-HK', 6]
            , [20, '刀架類型YU', 'ZH-HK', 3]
            , [21, '刮鬍刀YU', 'ZH-HK', 4]
            , [22, '刮鬍護理YU', 'ZH-HK', 5]
            , [23, '試用禮盒YU', 'ZH-HK', 2]

            // NEW MEN LINE
            , [24, 'Trial Plan Handle', 'EN', 7]
            , [25, 'Trial Plan Blade', 'EN', 8]
            , [26, 'Trial Plan Addons', 'EN', 9]
            , [27, 'Custom Plan Blade', 'EN', 10]
            , [28, 'Custom Plan Addons', 'EN', 11]
            , [29, 'Custom Plan Addons Handle', 'EN', 12]
            , [30, 'ASK', 'EN', 13]
            , [31, 'Custom Plan Addons Bag', 'EN', 14]
            , [32, 'Trial Plan Handle KO', 'KO', 7]
            , [33, 'Trial Plan Blade KO', 'KO', 8]
            , [34, 'Trial Plan Addons KO', 'KO', 9]
            , [35, 'Custom Plan Blade KO', 'KO', 10]
            , [36, 'Custom Plan Addons KO', 'KO', 11]
            , [37, 'Custom Plan Addons Handle KO', 'KO', 12]
            , [38, 'ASK KO', 'KO', 13]
            , [39, 'Custom Plan Addons Bag KO', 'KO', 14]
            , [40, 'Trial Plan Handle TW', 'ZH-TW', 7]
            , [41, 'Trial Plan Blade TW', 'ZH-TW', 8]
            , [42, 'Trial Plan Addons TW', 'ZH-TW', 9]
            , [43, 'Custom Plan Blade TW', 'ZH-TW', 10]
            , [44, 'Custom Plan Addons TW', 'ZH-TW', 11]
            , [45, 'Custom Plan Addons Handle TW', 'ZH-TW', 12]
            , [46, 'ASK TW', 'ZH-TW', 13]
            , [47, 'Custom Plan Addons Bag TW', 'ZH-TW', 14]
            , [48, 'Trial Plan Handle HK', 'ZH-HK', 7]
            , [49, 'Trial Plan Blade HK', 'ZH-HK', 8]
            , [50, 'Trial Plan Addons HK', 'ZH-HK', 9]
            , [51, 'Custom Plan Blade HK', 'ZH-HK', 10]
            , [52, 'Custom Plan Addons HK', 'ZH-HK', 11]
            , [53, 'Custom Plan Addons Handle HK', 'ZH-HK', 12]
            , [54, 'ASK HK', 'ZH-HK', 13]
            , [55, 'Custom Plan Addons Bag HK', 'ZH-HK', 14]

            // Women Line
            , [56, 'Womens Shave Kit', 'EN', 15]
            , [57, 'Womens Starter Packs', 'EN', 16]
            , [58, 'Womens Handles', 'EN', 17]
            , [59, 'Womens Cartridges', 'EN', 18]
            , [60, 'Womens Skin Care', 'EN', 19]
            , [61, 'Womens Trial Plan Handle', 'EN', 20]
            , [62, 'Womens Trial Plan Blade', 'EN', 21]
            , [63, 'Womens Trial Plan Addons', 'EN', 22]
            , [64, 'Womens Custom Plan Blade', 'EN', 23]
            , [65, 'Womens Custom Plan Addons', 'EN', 24]
            , [66, 'Womens Custom Plan Addons Handle', 'EN', 25]
            , [67, 'Womens ASK', 'EN', 26]
            , [68, 'Womens Custom Plan Addons Bag', 'EN', 27]

            , [69, 'Womens 여성용', 'KO', 15]
            , [70, 'Womens 스타터 팩', 'KO', 16]
            , [71, 'Womens 손잡이', 'KO', 17]
            , [72, 'Womens 카트리지 팩', 'KO', 18]
            , [73, 'Womens 스킨 케어', 'KO', 19]
            , [74, 'Womens Trial Plan Handle KO', 'KO', 20]
            , [75, 'Womens Trial Plan Blade KO', 'KO', 21]
            , [76, 'Womens Trial Plan Addons KO', 'KO', 22]
            , [77, 'Womens Custom Plan Blade KO', 'KO', 23]
            , [78, 'Womens Custom Plan Addons KO', 'KO', 24]
            , [79, 'Womens Custom Plan Addons Handle KO', 'KO', 25]
            , [80, 'Womens ASK KO', 'KO', 26]
            , [81, 'Womens Custom Plan Addons Bag KO', 'KO', 27]

            , [82, 'Womens A.S.K（超凡刮鬍禮盒）', 'ZH-TW', 15]
            , [83, 'Womens 試用禮盒', 'ZH-TW', 16]
            , [84, 'Womens 刀架類型', 'ZH-TW', 17]
            , [85, 'Womens 刮鬍刀', 'ZH-TW', 18]
            , [86, 'Womens 刮鬍護理', 'ZH-TW', 19]
            , [87, 'Womens Trial Plan Handle TW', 'ZH-TW', 20]
            , [88, 'Womens Trial Plan Blade TW', 'ZH-TW', 21]
            , [89, 'Womens Trial Plan Addons TW', 'ZH-TW', 22]
            , [90, 'Womens Custom Plan Blade TW', 'ZH-TW', 23]
            , [91, 'Womens Custom Plan Addons TW', 'ZH-TW', 24]
            , [92, 'Womens Custom Plan Addons Handle TW', 'ZH-TW', 25]
            , [93, 'Womens ASK TW', 'ZH-TW', 26]
            , [94, 'Womens Custom Plan Addons Bag TW', 'ZH-TW', 27]

            , [95, 'Womens A.S.K（超凡刮鬍禮盒）YU', 'ZH-HK', 15]
            , [96, 'Womens 試用禮盒YU', 'ZH-HK', 16]
            , [97, 'Womens 刀架類型YU', 'ZH-HK', 17]
            , [98, 'Womens 刮鬍刀YU', 'ZH-HK', 18]
            , [99, 'Womens 刮鬍護理YU', 'ZH-HK', 19]
            , [100, 'Womens Trial Plan Handle HK', 'ZH-HK', 20]
            , [101, 'Womens Trial Plan Blade HK', 'ZH-HK', 21]
            , [102, 'Womens Trial Plan Addons HK', 'ZH-HK', 22]
            , [103, 'Womens Custom Plan Blade HK', 'ZH-HK', 23]
            , [104, 'Womens Custom Plan Addons HK', 'ZH-HK', 24]
            , [105, 'Womens Custom Plan Addons Handle HK', 'ZH-HK', 25]
            , [106, 'Womens ASK HK', 'ZH-HK', 26]
            , [107, 'Womens Custom Plan Addons Bag HK', 'ZH-HK', 27]

        ];
        $ProductTypeTranslateAll = ProductTypeTranslate::all("id");
        $ProductTypeTranslateAllArray = array();
        foreach ($ProductTypeTranslateAll as $ptt) {
            $ProductTypeTranslateAllArray[] = $ptt->id;
        }
        $this->command(__FUNCTION__ . " => Product record has not been found. Adding new product type translate....");
        foreach ($producttypetranslate as $p) {
            if (!in_array($p[0], $ProductTypeTranslateAllArray)) {
                $this->command(__FUNCTION__ . "starting adding -> " . $p[0]);
                $producttypetranslate = new ProductTypeTranslate();
                $producttypetranslate->id = $p[0];
                $producttypetranslate->name = $p[1];
                $producttypetranslate->langCode = $p[2];
                $producttypetranslate->created_at = $now;
                $producttypetranslate->updated_at = $now;
                $producttypetranslate->ProductTypeId = $p[3];
                $producttypetranslate->save();
            }
        }

        $this->command(__FUNCTION__ . "  ===== Successfully added product type translate records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }

}
