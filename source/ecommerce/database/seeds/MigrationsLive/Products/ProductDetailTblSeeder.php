<?php

use Illuminate\Database\Seeder;
use App\Models\Products\ProductDetail;

/*product*/
class ProductDetailTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("productdetail", $now);

        $this->command(__CLASS__ . " Done...");
    }

    public function addRecords($type, $now) {
        $productdetail = [
            [2,'center','','','','EN',4],
            [3,'center','','','','EN',1],
            [4,'left','','','','EN',2],
            [5,'center','','','','EN',3],
            [6,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/2f24b380-5fd4-11e8-b1f0-6d94eb4e20df.png','Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.','Our blade cartridges have everything you need for a superior shave: precision-honed carbon steel blades, a moisturising lubrication strip, and an anti-clog design.','EN',6],
            [7,'center','','','','EN',5],
            [8,'center','','','','EN',7],
            [9,'center','','','','EN',29],
            [10,'center','','','','EN',33],
            [11,'center','','','','EN',10],
            [12,'center','','','','EN',11],
            [13,'center','','','','EN',12],
            [14,'center','','','','EN',34],
            [15,'center','','','','EN',8],
            [16,'center','','','','EN',1000],
            [17,'center','','','','EN',35],
            [18,'center','','','','KO',1],
            [19,'center','','','','KO',8],
            [20,'center','','','','KO',34],
            [21,'center','','','','KO',33],
            [22,'center','','','','KO',3],
            [23,'center','','','','KO',2],
            [24,'center','','','','KO',7],
            [25,'center','','','','EN',1002],
            [26,'center','','','','EN',1001],
            [28,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/a90ef980-5fcf-11e8-b1f0-6d94eb4e20df.png','Chisel with the classic.','Equipped with the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.','EN',1005],
            [29,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/086ae6a0-5fd0-11e8-b1f0-6d94eb4e20df.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','EN',1006],
            [30,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/55759420-5fd2-11e8-b1f0-6d94eb4e20df.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','EN',1007],
            [31,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/559af670-5fd2-11e8-b1f0-6d94eb4e20df.png','All-round control.','Armed with a dual-axis swivel mechanism that perfectly follows your facial contours, clean-shaven perfection is now within reach. Ideal for guys who just shave everything off. ','EN',1007],
            [32,'center','','','','KO',5],
            [33,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/fb10e740-5fd2-11e8-b1f0-6d94eb4e20df.png','Smooth shaves for sensitive skin.','Experience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubble while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','EN',1010],
            [34,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/0db49730-5fd2-11e8-b1f0-6d94eb4e20df.png','Chisel with the classic.','Equipped with the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.','EN',1011],
            [35,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/2a7b88b0-5fd2-11e8-b1f0-6d94eb4e20df.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','EN',1012],
            [36,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/657688e0-5fd5-11e8-b1f0-6d94eb4e20df.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','EN',1004],
            [37,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/a3b570d0-5fd5-11e8-b1f0-6d94eb4e20df.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','EN',1005],
            [38,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/cb8262c0-5fd6-11e8-b1f0-6d94eb4e20df.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','EN',1006],
            [39,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/35b41ff0-5fd5-11e8-b1f0-6d94eb4e20df.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','EN',6],
            [40,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/2f740c00-5fd4-11e8-b1f0-6d94eb4e20df.png','Good for your skin. ','Every product in our skincare range is made with quality ingredients that address specific shaving needs for that smooth shaving experience.','EN',6],
            [41,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/7345d120-9adf-11e8-b9c1-856908402141.png','최고의 면도를 위한 제품','최고로 깔끔하고 부드러운 면도를 할 수 있습니다. 완벽함을 위해 매일 면도하는 수염이 많은 분께도 적합합니다.','KO',1012],
            [42,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/bcb5dda0-9adf-11e8-b9c1-856908402141.png','클래식한 면도기','Accublade® 트리머로 풍성한 수염과 넓은 피부 표면을 매우 부드럽게 면도할 수 있습니다. 매일 면도하지 않는 분께 적합합니다.','KO',1011],
            [43,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/d8817f30-9adf-11e8-b9c1-856908402141.png','민감한 피부를 위한 부드러운 면도','입술 위와 턱을 부드럽게 면도할 수 있습니다. 피부 자극이 적어 얇은 수염에 적합합니다. 피부과 테스트를 받은 저자극 면도기입니다.','KO',1010],
            [44,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/2b4f16d0-9ae2-11e8-b9c1-856908402141.png','클래식한 면도기','Accublade® 트리머로 풍성한 수염과 넓은 피부 표면을 매우 부드럽게 면도할 수 있습니다. 매일 면도하지 않는 분께 적합합니다.','KO',1005],
            [45,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/2b80d530-9ae2-11e8-b9c1-856908402141.png','쉬운 컨트롤','손잡이에 고무 마감 되어 젖은 손으로도 잡기 편하고 컨트롤이 쉽습니다. 무게감 있는 금속이 추가돼 적절한 압력을 주고, 단일축 회전형으로 정밀하게 면도할 수 있습니다. 수염 모양을 내는 데 최고의 면도기입니다.','KO',1005],
            [46,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/01849540-9ae3-11e8-b9c1-856908402141.png','최고의 면도를 위한 제품','최고로 깔끔하고 부드러운 면도를 할 수 있습니다. 완벽함을 위해 매일 면도하는 수염이 많은 분께도 적합합니다.','KO',1006],
            [47,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/e0284f10-9ae0-11e8-b9c1-856908402141.png','민감한 피부를 위한 부드러운 면도','입술 위와 턱을 부드럽게 면도할 수 있습니다. 피부 자극이 적어 얇은 수염에 적합합니다. 피부과 테스트를 받은 저자극 면도기입니다.','KO',1004],
            [48,'right','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/d297af50-9ae3-11e8-b9c1-856908402141.png','최고의 면도를 위한 제품','최고로 깔끔하고 부드러운 면도를 할 수 있습니다. 완벽함을 위해 매일 면도하는 수염이 많은 분께도 적합합니다.','KO',1007],
            [49,'center','','','','KO',35],
            [50,'center','','','','KO',12],
            [51,'center','','','','EN',1003],
            [52,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/e0401cd0-9ae0-11e8-b9c1-856908402141.png','쉬운 컨트롤','손잡이에 고무 마감 되어 젖은 손으로도 잡기 편하고 컨트롤이 쉽습니다. 무게감 있는 금속이 추가돼 적절한 압력을 주고, 단일축 회전형으로 정밀하게 면도할 수 있습니다. 수염 모양을 내는 데 최고의 면도기입니다.','KO',1004],
            [53,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/01ad2be0-9ae3-11e8-b9c1-856908402141.png','쉬운 컨트롤','손잡이에 고무 마감 되어 젖은 손으로도 잡기 편하고 컨트롤이 쉽습니다. 무게감 있는 금속이 추가돼 적절한 압력을 주고, 단일축 회전형으로 정밀하게 면도할 수 있습니다. 수염 모양을 내는 데 최고의 면도기입니다.','KO',1006],
            [54,'left','https://smart-shaves-development.s3.ap-southeast-1.amazonaws.com/product-images/d2bfd0c0-9ae3-11e8-b9c1-856908402141.png','회전식 컨트롤','이중축 회전형으로 굴곡진 얼굴선을 따라 완벽하게 깔끔한 면도를 할 수 있습니다. 완벽하게 면도를 하고 싶어하는 분들께\r
             적합합니다.','KO',1007],
            [55,'center','','','','EN',1015],
            [56,'center','','','','EN',1014],
            [57,'center','','','','EN',1013],
            [58,'center','','','','KO',1015],
            [59,'center','','','','KO',1014],
            [60,'center','','','','KO',1013],
            [61,'center','','','','ZH-TW',1015],
            [62,'center','','','','ZH-TW',1014],
            [63,'center','','','','ZH-TW',1013],
            [64,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/d5611640-5203-11e9-a1d0-6b87e96359a2.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','ZH-TW',1012],
            [65,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/ff3edc40-5203-11e9-a1d0-6b87e96359a2.png','Chisel with the classic.','Equipped with the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.','ZH-TW',1011],
            [66,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/20e86a00-5204-11e9-a1d0-6b87e96359a2.png','Smooth shaves for sensitive skin.','Experience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubble while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','ZH-TW',1010],
            [67,'right','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/4cec9270-5204-11e9-a1d0-6b87e96359a2.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','ZH-TW',1007],
            [68,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/4d0b6510-5204-11e9-a1d0-6b87e96359a2.png','All-round control.','Armed with a dual-axis swivel mechanism that perfectly follows your facial contours, clean-shaven perfection is now within reach. Ideal for guys who just shave everything off. ','ZH-TW',1007],
            [69,'right','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/839afc30-5204-11e9-a1d0-6b87e96359a2.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','ZH-TW',1006],
            [70,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/83b980b0-5204-11e9-a1d0-6b87e96359a2.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','ZH-TW',1006],
            [71,'right','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/6a378b90-5205-11e9-a1d0-6b87e96359a2.png','Chisel with the classic.','Equipped with the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.','ZH-TW',1005],
            [72,'left','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/6a588110-5205-11e9-a1d0-6b87e96359a2.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','ZH-TW',1005],
            [73,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/bc81ad40-5205-11e9-a1d0-6b87e96359a2.png','Crafted for Control','Crafted with a comfortable rubberised grip for confident handling even when hands are wet., a weighted metal body for the ideal amount of pressure, and a single-axis swivel mechanism for precision. Perfect for shaping facial hair. ','ZH-TW',1004],
            [74,'center','','','','ZH-TW',1003],
            [75,'center','','','','ZH-TW',1002],
            [76,'center','','','','ZH-TW',1001],
            [77,'center','','','','ZH-TW',35],
            [78,'center','','','','ZH-TW',12],
            [79,'center','','','','EN',27],
            [80,'center','','','','EN',1016],
            [81,'center','','','','ZH-HK',1016],
            [82,'center','','','','ZH-HK',1015],
            [83,'center','','','','ZH-HK',1014],
            [84,'center','','','','ZH-HK',1013],
            [85,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/edb2add0-6d42-11e9-9708-b74c690cad70.png','Smooth shaves for sensitive skin.','Experience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubble while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','ZH-HK',1010],
            [86,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/18d43e70-6d43-11e9-9708-b74c690cad70.png','Chisel with the classic.','Equipped with the Accublade® Trimmer system, shave fuller beards and larger areas of skin for a smooth finish like no other. Ideal for full-bearded guys who don’t shave daily.','ZH-HK',1011],
            [87,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/37faa3c0-6d43-11e9-9708-b74c690cad70.png','For that shave above the rest.','Max out on smoothness with the cleanest shave ever. Ideal for even the hairiest of guys who shave daily for that clean-shaven perfection. ','ZH-HK',1012],
            [88,'center','','','','ZH-HK',35],
            [89,'center','','','','EN',1019],
            [90,'center','','','','ZH-TW',1019],

            [91,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d442670-cacb-11e9-8f65-b370d1492f02.png','Toiletry bag','Toiletry bag','EN',1025],
            [92,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/c4ef9c00-cacb-11e9-8f65-b370d1492f02.png','Rubber Sleeve','Rubber Sleeve','EN',1026],
            [93,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d442670-cacb-11e9-8f65-b370d1492f02.png','Toiletry bag','Toiletry bag','ZH-TW',1025],
            [94,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/c4ef9c00-cacb-11e9-8f65-b370d1492f02.png','Rubber Sleeve','Rubber Sleeve','ZH-TW',1026],
            [95,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d442670-cacb-11e9-8f65-b370d1492f02.png','Toiletry bag','Toiletry bag','ZH-HK',1025],
            [96,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/c4ef9c00-cacb-11e9-8f65-b370d1492f02.png','Rubber Sleeve','Rubber Sleeve','ZH-HK',1026],
            [97,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d442670-cacb-11e9-8f65-b370d1492f02.png','Toiletry bag','Toiletry bag','KO',1025],
            [98,'center','https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/c4ef9c00-cacb-11e9-8f65-b370d1492f02.png','Rubber Sleeve','Rubber Sleeve','KO',1026],
            [99,'center','','','','KO',1019],
            [100,'center','','','','ZH-HK',12],
            [101,'center','','','','ZH-HK',1],
            [102,'center','','','','ZH-TW',1],
            [103,'center','','','','ZH-HK',34],
            [104,'center','','','','ZH-TW',34],
            [105,'center','','','','ZH-HK',1003],
            [106,'center','','','','ZH-HK',1002],
            [107,'center','','','','ZH-HK',1001],
            [108,'center','','','','ZH-HK',1019],
            [109,'center','','','','ZH-TW',121],
            [110,'center','','','','KO',121],
            [111,'center','','','','EN',121],
            [112,'center','','','','ZH-HK',121],
            [123,'center','','','','EN',1020],
        ];
        $ProductDetailAll = ProductDetail::all("id");
        $ProductDetailAllArray = array();
        foreach ($ProductDetailAll as $pd) {
            $ProductDetailAllArray[] =  $pd->id;
        }
        $this->command(__FUNCTION__ . " => Product record has not been found. Adding new products details....");
        foreach ($productdetail as $p) {

                if (!in_array($p[0], $ProductDetailAllArray)) {
                $this->command(__FUNCTION__ . "starting adding -> ". $p[0] );
                $productdetail = new ProductDetail();
                $productdetail->id = $p[0];
                $productdetail->type = $p[1];
                $productdetail->imageUrl = $p[2];
                $productdetail->title = $p[3];
                $productdetail->details = $p[4];
                $productdetail->langCode = $p[5];
                $productdetail->ProductId = $p[6];
                $productdetail->save();
                }
        }

        $this->command(__FUNCTION__ . "  ===== Successfully added product details records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }
    
    public function command($msg) {
        $this->command->info($msg);
    }

}
