<?php

use App\Models\Products\ProductType;
use Illuminate\Database\Seeder;

/*product*/
class ProductTypeTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("productype", $now);

        $this->command(__CLASS__ . " Done...");
    }

    public function addRecords($type, $now)
    {
        $producttype = [
            [1, 'Awesome Shave Kit', null, '2019-05-02 09:58:03', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/37486920-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [2, 'Starter Packs', null, '2019-05-02 09:57:39', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/b876b9e0-6cc0-11e9-a2ca-0f282bf25f4d.png', 'active']
            , [3, 'Handle Pack', null, '2019-05-02 09:56:38', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24a76dc0-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [4, 'Cartridge Packs', null, '2019-05-02 09:56:54', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/1cf1b450-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [5, 'Skin Care', null, '2019-05-02 09:57:17', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/20af5d20-68ab-11e8-b9da-131ac7bca11b.png', 'active']
            , [6, 'Womens', null, '2019-05-02 09:56:14', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [7, 'Trial Plan Handle', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [8, 'Trial Plan Blade', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [9, 'Trial Plan Addons', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [10, 'Custom Plan Blade', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [11, 'Custom Plan Addons', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [12, 'Custom Plan Addons Handle', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [13, 'ASK', null, '2019-06-27 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [14, 'Custom Plan Addons Bag', null, '2019-08-30 00:00:00', 0, 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d23a620-cacb-11e9-8f65-b370d1492f02.png', 'active']
            , [15, 'Womens Awesome Shave Kit', null, '2019-05-02 09:58:03', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/37486920-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [16, 'Womens Starter Packs', null, '2019-05-02 09:57:39', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/b876b9e0-6cc0-11e9-a2ca-0f282bf25f4d.png', 'active']
            , [17, 'Womens Handle Pack', null, '2019-05-02 09:56:38', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/24a76dc0-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [18, 'Womens Cartridge Packs', null, '2019-05-02 09:56:54', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/1cf1b450-59c2-11e8-8fb5-8f1134168a52.png', 'active']
            , [19, 'Womens Skin Care', null, '2019-05-02 09:57:17', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/20af5d20-68ab-11e8-b9da-131ac7bca11b.png', 'active']
            , [20, 'Womens Trial Plan Handle', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [21, 'Womens Trial Plan Blade', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [22, 'Womens Trial Plan Addons', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [23, 'Womens Custom Plan Blade', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [24, 'Womens Custom Plan Addons', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [25, 'Womens Custom Plan Addons Handle', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [26, 'Womens ASK', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/52b49530-5daa-11e8-8c24-e53a0aa7dfcd.png', 'active']
            , [27, 'Womens Custom Plan Addons Bag', null, '2019-06-27 00:00:00', '0', 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/5d23a620-cacb-11e9-8f65-b370d1492f02.png', 'active'],

        ];
        $ProductTypeAll = ProductType::all("id");
        $ProductTypeAllArray = array();
        foreach ($ProductTypeAll as $pt) {
            $ProductTypeAllArray[] = $pt->id;
        }
        $this->command(__FUNCTION__ . " => Product record has not been found. Adding new products....");
        foreach ($producttype as $p) {
            if (!in_array($p[0], $ProductTypeAllArray)) {
                $this->command(__FUNCTION__ . "starting adding -> " . $p[0]);
                $producttype = new ProductType();
                $producttype->id = $p[0];
                $producttype->name = $p[1];
                $producttype->created_at = $p[2];
                $producttype->updated_at = $p[3];
                $producttype->order = $p[4];
                $producttype->url = $p[5];
                $producttype->status = $p[6];
                $producttype->save();
            }
        }

        $this->command(__FUNCTION__ . "  ===== Successfully added product records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }

}
