<?php

use Illuminate\Database\Seeder;
use App\Models\Products\ProductTranslate;

/*product*/
class ProductTranslateTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("producttranslate", $now);

        $this->command(__CLASS__ . " Done...");
    }

    public function addRecords($type, $now) {
        $producttranslate = [
            [1,'Men’s Starter Pack','Find your favourite shave\r\n\r\nTry out both the 3 and 5 blade cartridges to see which suits you better.\r\n \r\nThe Starter Pack includes:\r\n\r\n• 1x Men\'s Premium Handle \r\n• 1x Men’s 3 Blade Cartridge\r\n• 1x Men’s 5 Blade Cartridge','Starter Pack comes with a Premium Handle & 2 types of cartridges.','EN',4,'null'],
            [2,'Men\'s Premium Handle','Keep it simple, with this beautifully crafted, weighty metal handle.\r\n\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)\r\n• Soft rubberised grip with precise balance and great heft\r\n• Striking chrome finish','Soft rubberised handle that fits all Shaves2U cartridges.','EN',1,'null'],
            [3,'Men\'s 3 Blade Cartridge Pack','**What makes it good:**\r\n\r\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubbles while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','Simple and smooth.','EN',2,'null'],
            [4,'Men\'s 5 Blade Cartridge Pack','**What makes it good:**\r\n\r\nDouble-coated blades. Aloe lubricating strip. Anti-clog cartridges. And that’s not all — The classic 5 Blade Razor now features the Accublade® Trimmer system for your goatee and sideburn shaving needs. ','Chisel with the classic.','EN',3,'null'],
            [5,'Awesome Shave Kit','**What makes it good:**\r\n\r\nThis Awesome Shave Kit contains everything you need to truly appreciate the Shaves2U experience.\r\n\r\n• 1 x Premium Handle\r\n• 1 x 3Blade Cartridge Pack\r\n• 1 x 5 Blade Cartridge Pack\r\n• 1 x Shave Cream (100g)\r\n• 1 x After Shave Cream','The shave kit to rule them all.','EN',6,'null'],
            [6,'Women’s Starter Pack','**What makes it good:**\r\n\r\nThis Starter Pack includes:\r\n• 1 x Women\'s Razor Handle\r\n• 2 x Women’s 5 Blade Cartridges                                                                                                                                                        ','Experience the essentials.','EN',5,'null'],
            [7,'Women’s Premium Handle','**What makes it good:**\r\n\r\n• Soft rubberised grip with precision balance\r\n• Convenient push button release','Ergonomic handle for simple shaving.','EN',8,'null'],
            [8,'Women\'s 5 Blade Cartridge Pack','**What makes it good:**\r\n\r\n• 5 Blade razor with a larger aloe vera lubricating strip to soothe sensitive skin \r\n• 1 cartridge pack includes 4 cartridges\r\n• Ceramic-coated blades for cleaner and crisper shaves \r\n• Open-blade architecture design for easy rinsing and no clogging\r\n• Smooth pivoting head for an even, clean shave                                                                                                                                                                                                ','The simple way to cleaner shaves.','EN',7,'null'],
            [9,'Shave Soap','**What makes it good:**\r\n\r\n• Tallow-based soap that produces a luxurious lather\r\n\r\n• Economical in use – a little goes a long way\r\n\r\n• Light, clean scent that refreshes\r\n','The go-to pick for a lather-rich, classic shave.','EN',29,'null'],
            [10,'Shaving Foam','The foamy shaving necessity.\r\n• Enriched formula that moisturises, soothes and nourishes the skin \r\n• Continues working after shaving\r\n• Cool, fresh scent\r\n• Large 200ml can \r\n• Made in Turkey','Enriched formula that moisturises, soothes and nourishes the skin.','EN',10,'null'],
            [11,'Shaving Gel','Keep stubble at bay the gel way.\r\n• Enriched formula that moisturises, soothes and nourishes the skin \r\n• Continues working after shaving\r\n• Cool, fresh scent\r\n• Large 200ml can \r\n• Made in Turkey','Enriched formula that moisturises, soothes and nourishes the skin.','EN',11,'null'],
            [12,'After Shave Cream','**What makes it good:**\r\n\r\n• Formulated to cool, calm, and balance freshly shaven skin\r\n\r\n• Replenishes skin’s lost moisture with conditioning ingredients\r\n\r\n• Light, clean scent for a refreshing post-shave ritual\r\n','For an icy cool finishing touch that’s good for your skin.','EN',12,'null'],
            [13,'Men\'s Swivel Handle','Achieve perfect manoeuvrability with a rubberised grip.\r\n\r\n• Beautifully designed metal and chrome handle, with a stunning finish.\r\n• Swivel head allows smoother movement around facial contours.\r\n• Balanced ergonomic grip for added control\r\n• Push-button release and rubberised finger rest\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)','Swivel head that allows smoother movement and fits all Shaves2U cartridges.','EN',34,'null'],
            [14,'Men\'s 6 Blade Cartridge Pack','**What makes it good:**\r\n\r\nMade with our patented I.C.E. Diamond Coating for durability and sharpness, the 6 Blade also features the MicroSilver BG™ enhanced lubricating strip for a shave that’s above the rest.\r\n','A shave above the rest.','EN',33,'null'],
            [15,'Test product','Test for iPay88 (RM1)','Testing product for iPay88 (RM1)','EN',1000,NULL],
            [16,'Shave Cream','**What makes it good:**\r\n\r\n• Enriched with Vitamin E to combat post-shave redness\r\n• Rich, easy-to-lather cream that cushions skin from cuts\r\n• Light, clean scent that refreshes\r\n','Start every shave right with this rich shave cream.','EN',35,'null'],
            [17,'SHAVING SOAP','<style type=\"text/css\">\r\n.producttab {display:none;}\r\n.add-to-links {display:none;}\r\n</style>\r\n\r\n<ul>\r\n<li>An \'Old School\' classic.</li>\r\n<li>Light scented formula for a super comfortable shave.</li>\r\n<li>Excellent for avoiding shaving \'nicks\' or razor rash.</li>\r\n<li>Use a shaving brush or hand to lather up and apply to face.</li>\r\n<li>Made in Turkey.</li>\r\n</ul>',NULL,'EN',9,NULL],
            [18,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n</style>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><img src=\"{{media url=\"wysiwyg/Shaves2U_Website_Subscription_v14_MY_180116_S2U_Website_S3_Subscription.jpg\"}}\" alt=\"\" style=\"max-width:100%; padding-top:18px;\" /></div>\r\n\r\n<!--<div class=\"row-ui\">\r\n         <ul>\r\n  			<li>RM220 / year. (4 payments of  RMR55 (excl GST) / every 3 months).</li>\r\n  			<li>Normal cost: RM277 - Save RM57.</li>\r\n  			<li>8 x S3 cartridges, free handle and Shave Foam with first delivery.</li>\r\n  			<li>8 x S3 cartridges per subsequent deliveries (X 3).</li>\r\n                        <li>Requires PayPal account for subscription.</li>\r\n          </ul>\r\n</div>-->\r\n</div>',NULL,'EN',13,NULL],
            [19,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n        \r\n        <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><img src=\"http://www.shaves2u.com/test/media/wysiwyg/S5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\"></div>\r\n</div>',NULL,'EN',14,NULL],
            [20,'YEARLY SUBSCRIPTION FOR HER - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n                <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><img src=\"http://www.shaves2u.com/test/media/wysiwyg/F5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\"></div>\r\n</div>',NULL,'EN',15,NULL],
            [21,'AFTER-SHAVE CREAM','<ul>\r\n<li>• 	After-Shave Cream, 50ml</li>\r\n<li>• 	Restores moisture lost during shaving and nourishes skin with vitamins</li>\r\n<li>• 	Calms and soothes sensitive skin</li>\r\n<li>• 	Light, fresh scent</li>\r\n</ul>\r\n',NULL,'EN',24,NULL],
            [22,'Swivel Trial Kit','Swivel Trial Kit','Swivel Trial Kit','EN',27,'null'],
            [23,'YEARLY SUBSCRIPTION FOR (INITI','<div class=\"row\">        \r\n<!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"{{config path=\"web/unsecure/base_url\"}}media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><img src=\"http://www.shaves2u.com/test/media/wysiwyg/S3-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\"></div>\r\n</div>',NULL,'EN',28,NULL],
            [24,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"{{media url=\"wysiwyg/Shaves2U_Website_Subscription_v14_MY_180116_S2U_Website_S3_Subscription.jpg\"}}\" alt=\"\" style=\"max-width:100%; padding-top:18px;\" />--></div>\r\n\r\n<!--<div class=\"row-ui\">\r\n         <ul>\r\n  			<li>RM220 / year.</li>\r\n  			<li>Normal cost: RM277 - Save RM57.</li>\r\n  			<li>8 x S3 cartridges, free handle and Shave Foam.</li>\r\n  			<li>Free delivery.</li>\r\n          </ul>\r\n</div>-->\r\n</div>',NULL,'EN',30,NULL],
            [25,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n        \r\n        <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/S5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',31,NULL],
            [26,'YEARLY SUBSCRIPTION FOR HER - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n                <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/F5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',32,NULL],
            [27,'3 Blade Razors Cartridge (M) -','3 Blade Razors Cartridge (M) - 4 pcs W/O Packaging',NULL,'EN',36,NULL],
            [28,'5 Blade Razors Cartridge (M) -','5 Blade Razors Cartridge (M) - 4 pcs W/O Packaging',NULL,'EN',37,NULL],
            [29,'Less than 4 cartridges (3 blad','Less than 4 cartridges (3 blade razors) per cassette.',NULL,'EN',38,NULL],
            [30,'Less than 4 cartridges (5 blad','Less than 4 cartridges (5 blade razors) per cassette.',NULL,'EN',39,NULL],
            [31,'ASK (New Version)','ASK (New Version)',NULL,'EN',40,NULL],
            [32,'3 Blade Razors Cartrige (M) - ','3 Blade Razors Cartrige (M) - 8pcs',NULL,'EN',41,NULL],
            [33,'5 Blade Razors Cartrige (M) - ','5 Blade Razors Cartrige (M) - 8pcs',NULL,'EN',42,NULL],
            [34,'Retail Label - Starter Pack (F','Retail Label - Starter Pack (F)',NULL,'EN',43,NULL],
            [35,'Retail Label - 5 Blade Razors ','Retail Label - 5 Blade Razors Cartrige (F) - 4pcs',NULL,'EN',44,NULL],
            [36,'Retail Carton Label - ASK','Retail Carton Label - ASK',NULL,'EN',45,NULL],
            [37,'Retail Label - Starter Pack (M','Retail Label - Starter Pack (M)',NULL,'EN',46,NULL],
            [38,'Retail Label - S3-R4','Retail Label - S3-R4',NULL,'EN',47,NULL],
            [39,'Retail Label - S3-R8','Retail Label - S3-R8',NULL,'EN',48,NULL],
            [40,'Retail Label - S5-R4','Retail Label - S5-R4',NULL,'EN',49,NULL],
            [41,'Retail Label - S5-R8','Retail Label - S5-R8',NULL,'EN',50,NULL],
            [42,'Packaging - Outer Packaging fo','Packaging - Outer Packaging for H1',NULL,'EN',51,NULL],
            [43,'Packaging - Outer Packaging fo','Packaging - Outer Packaging for H2',NULL,'EN',52,NULL],
            [44,'Packaging - S3','Packaging - S3',NULL,'EN',53,NULL],
            [45,'Packaging - S5','Packaging - S5',NULL,'EN',54,NULL],
            [46,'Packaging - Outer Packaging fo','Packaging - Outer Packaging for F5',NULL,'EN',55,NULL],
            [47,'Packaging - Nesting for H2(F)','Packaging - Nesting for H2(F)',NULL,'EN',56,NULL],
            [48,'Packaging - Nesting for F5(F)','Packaging - Nesting for F5(F)',NULL,'EN',57,NULL],
            [49,'Packaging - Nesting for H1','Packaging - Nesting for H1',NULL,'EN',58,NULL],
            [50,'Packaging - Nesting for ASK','Packaging - Nesting for ASK',NULL,'EN',59,NULL],
            [51,'Packaging - ASK','Packaging - ASK',NULL,'EN',60,NULL],
            [52,'Packaging - Female Starter Pac','Packaging - Female Starter Pack Card Holder',NULL,'EN',61,NULL],
            [53,'Packaging - Female 4PKGF5 Blad','Packaging - Female 4PKGF5 Blade Card Holder',NULL,'EN',62,NULL],
            [54,'Packaging - Male Starter Pack ','Packaging - Male Starter Pack Card Holder',NULL,'EN',63,NULL],
            [55,'Packaging - Male 4PKGS3 Card H','Packaging - Male 4PKGS3 Card Holder',NULL,'EN',64,NULL],
            [56,'Packaging - Male 8PKGS3 Card H','Packaging - Male 8PKGS3 Card Holder ',NULL,'EN',65,NULL],
            [57,'Packaging - Male 4PKGS5 Card H','Packaging - Male 4PKGS5 Card Holder ',NULL,'EN',66,NULL],
            [58,'Packaging - Male 8PKGS5 Card H','Packaging - Male 8PKGS5 Card Holder ',NULL,'EN',67,NULL],
            [59,'Packaging - Female Starter Pac','Packaging - Female Starter Pack Blister',NULL,'EN',68,NULL],
            [60,'Packaging - Female 4PKGF5 Blad','Packaging - Female 4PKGF5 Blade Blister ',NULL,'EN',69,NULL],
            [61,'Packaging - Male Starter Pack ','Packaging - Male Starter Pack Blister',NULL,'EN',70,NULL],
            [62,'Packaging - Male 4PKGS3/S5 Bli','Packaging - Male 4PKGS3/S5 Blister',NULL,'EN',71,NULL],
            [63,'Packaging - Male 8PKGS3/S5 Bli','Packaging - Male 8PKGS3/S5 Blister',NULL,'EN',72,NULL],
            [64,'Packaging - ASK Blister','Packaging - ASK Blister',NULL,'EN',73,NULL],
            [65,'Packaging - Protector for S3','Packaging - Protector for S3',NULL,'EN',74,NULL],
            [66,'Packaging - Protector for S5','Packaging - Protector for S5',NULL,'EN',75,NULL],
            [67,'Sensormatic Tags','Sensormatic Tags',NULL,'EN',76,NULL],
            [68,'Retail Box ','Retail Box ',NULL,'EN',77,NULL],
            [69,'Retail Box for ASK','Retail Box for ASK',NULL,'EN',78,NULL],
            [70,'Nesting for 8PKGS3 & 8PKGS5','Nesting for 8PKGS3 & 8PKGS5',NULL,'EN',79,NULL],
            [71,'Mould for Starter Pack (F)','Mould for Starter Pack (F)',NULL,'EN',80,NULL],
            [72,'Mould for F5 (4PKGS3) (F)','Mould for F5 (4PKGS3) (F)',NULL,'EN',81,NULL],
            [73,'Mould for Starter Pack (M)','Mould for Starter Pack (M)',NULL,'EN',82,NULL],
            [74,'Sealing Machine','Sealing Machine',NULL,'EN',83,NULL],
            [75,'Mould for S3/S5 (4PKG) (M)','Mould for S3/S5 (4PKG) (M)',NULL,'EN',89,NULL],
            [76,'Mould for S3/S5 (8PKG) (M)','Mould for S3/S5 (8PKG) (M)',NULL,'EN',90,NULL],
            [77,'Carton Label - Corrosive','Carton Label - Corrosive',NULL,'EN',97,NULL],
            [78,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"{{media url=\"wysiwyg/Shaves2U_Website_Subscription_v14_MY_180116_S2U_Website_S3_Subscription.jpg\"}}\" alt=\"\" style=\"max-width:100%; padding-top:18px;\" />--></div>\r\n\r\n<!--<div class=\"row-ui\">\r\n         <ul>\r\n  			<li>RM190 / year.</li>\r\n  			<li>Normal cost: RM277 - Save RM57.</li>\r\n  			<li>8 x S3 cartridges, free handle and Shave Foam.</li>\r\n  			<li>Free delivery.</li>\r\n          </ul>\r\n</div>-->\r\n</div>',NULL,'EN',98,NULL],
            [79,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"{{media url=\"wysiwyg/Shaves2U_Website_Subscription_v14_MY_180116_S2U_Website_S3_Subscription.jpg\"}}\" alt=\"\" style=\"max-width:100%; padding-top:18px;\" />--></div>\r\n\r\n<!--<div class=\"row-ui\">\r\n         <ul>\r\n  			<li>RM160 / year.</li>\r\n  			<li>Normal cost: RM277 - Save RM57.</li>\r\n  			<li>8 x S3 cartridges, free handle and Shave Foam.</li>\r\n  			<li>Free delivery.</li>\r\n          </ul>\r\n</div>-->\r\n</div>',NULL,'EN',99,NULL],
            [80,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n        \r\n        <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/S5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',100,NULL],
            [81,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n        \r\n        <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/S5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',101,NULL],
            [82,'YEARLY SUBSCRIPTION FOR HER - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n                <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/F5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',103,NULL],
            [83,'YEARLY SUBSCRIPTION FOR HER - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n                <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/F5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',104,NULL],
            [84,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"{{media url=\"wysiwyg/Shaves2U_Website_Subscription_v14_MY_180116_S2U_Website_S3_Subscription.jpg\"}}\" alt=\"\" style=\"max-width:100%; padding-top:18px;\" />--></div>\r\n\r\n<!--<div class=\"row-ui\">\r\n         <ul>\r\n  			<li>RM220 / year.</li>\r\n  			<li>Normal cost: RM277 - Save RM57.</li>\r\n  			<li>8 x S3 cartridges, free handle and Shave Foam.</li>\r\n  			<li>Free delivery.</li>\r\n          </ul>\r\n</div>-->\r\n</div>',NULL,'EN',105,NULL],
            [85,'YEARLY SUBSCRIPTION FOR HIM - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n        \r\n        <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/S5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',107,NULL],
            [86,'YEARLY SUBSCRIPTION FOR HER - ','<style type=\"text/css\">\r\n.add-to-links {display:none;}\r\n.row6 {display:none;}\r\n.required {display:none;}\r\n.last {display:none;}\r\n.nav-tabs {display:none;}\r\n.producttab {display:none;}\r\n</style>\r\n<div class=\"row\">\r\n                <!--<div class=\"col-md-7 col-sm-7 col-lg-7 col-xs-7\"><img src=\"http://www.shaves2u.com/my/media/wysiwyg/S3-Callout.jpg\" alt=\"\" style=\"max-width:100%\"></div>\r\n    </div>-->\r\n<div class=\"col-md-12 col-sm-12 col-lg-12 col-xs-12\"><!--<img src=\"http://www.shaves2u.com/test/media/wysiwyg/F5-Subcription-Plan.jpg\" alt=\"\" style=\"max-width:100%; padding-top:18px;\">--></div>\r\n</div>',NULL,'EN',108,NULL],
            [87,'Transparent round stiker','Transparent round stiker',NULL,'EN',109,NULL],
            [88,'Packing-Outer Packing for ASK-','Packing-Outer Packing for ASK-2017',NULL,'EN',112,NULL],
            [89,'AWESOME SHAVE KIT','<style type=\"text/css\">\r\n.producttab {display:none;}\r\n.add-to-links {display:none;}\r\n</style>\r\nThis awesome kit contains everything you need to ensure that you never have a bad shave again, hand-picked, packed & delivered directly to you with love. When you need to refill, just revisit us here and enjoy prices more than 50% lower than leading retail brands. <br/>\r\nIncludes:\r\n<ul>\r\n<li>1 x Razor Handle.</li>\r\n<li>1 x 5-Blade Razor Pack (4 Cartridges).</li>\r\n<li>1 x Shaving Gel (200 ml).</li>\r\n<li>1 x After-Shave Cream.</li>\r\n<li>1 x Shaving Soap.</li>\r\n</ul>',NULL,'EN',117,NULL],
            [90,'3 Blade Razor Cartridge (M) - ','3 Blade Razor Cartridge (M) - 4 pcs',NULL,'EN',118,NULL],
            [91,'5 Blade Razors Cartrige (M) - ','5 Blade Razors Cartrige (M) - 4pcs',NULL,'EN',119,NULL],
            [92,'5 Blade Razors Cartrige (F) - ','5 Blade Razors Cartrige (F) - 4pcs',NULL,'EN',120,NULL],
            [93,'Shave Cream Cool (M)','Shave Cream Cool (M)',NULL,'EN',121,NULL],
            [94,'5 Blade Razors Cartridge (F)','5 Blade Razors Cartridge (F) ',NULL,'EN',122,NULL],
            [95,'Premium Handle (F) W/O Packagi','Premium Handle (F) W/O Packaging',NULL,'EN',123,NULL],
            [96,'Premium Handle (M) W/O Packagi','Premium Handle (M) W/O Packaging',NULL,'EN',124,NULL],
            [98,'남성 프리미엄 핸들','단순해져라. 아름답게 만들어진, 무게감 있는 핸들.\r\n\r\n• 모든 Shaves2U 카트리지 (3, 5, 혹은 6-면도날)에 적합\r\n• 정확한 균형과 우수한 무게감을 가진 부드러운 고무 그립 \r\n• 매력적인 크롬 마무리','부드러운 고무 소재의 핸들로 모든 Shaves2U 카트리지를 사용 할 수 있습니다','KO',1,'null'],
            [99,'남성 3 면도날 카트리지 팩','알맞은 가격의 날카로운 레이저.\r\n\r\n• 알로에 베라 윤활 스트립으로 된 3-면도날 레이저 \r\n• 1개의 카트리지 팩은 4개의 카트리지로 구성 \r\n• 더 깨끗하고 뽀득한 면도 세라믹 코팅의 면도날. \r\n• 오픈된 면도날 설계 디자인으로 간편한 행굼과 막힘 방지','1개의 카트리지 팩은 3 면도날 카트리지 4개로 구성되어 있습니다.','KO',2,'null'],
            [100,'남성 5 면도날 카트리지 팩','면도날이 많을 수록 더 낫다.\r\n\r\n• 알로에 베라 윤활 스트립으로 된 4-면도날 레이저 \r\n• 1개의 카트리지 팩은 4개의 카트리지로 구성 \r\n• 더 깨끗하고 뽀득한 면도 세라믹 코팅의 면도날. \r\n• 오픈된 면도날 설계 디자인으로 간편한 행굼과 막힘 방지','1개의 카트리지 팩은 5 면도날 카트리지 4 개로구성되어 있습니다.','KO',3,'null'],
            [102,'여성용 프리미엄 손잡이','**제품의 장점:** \r\n\r\n● 부드러운 고무가 있어 정확하게 조절할 수 있습니다. \r\n● 버튼을 눌러 간편하게 카트리지를 빼낼 수 있습니다.','간편하게 면도할 수 있는 인체공학적 손잡이입니다.','KO',8,'null'],
            [103,'여성용 5중날 카트리지 팩','**제품의 장점:** \r\n\r\n● 5중날에 커진 알로에 베라 윤활밴드가 있어 민감한 피부를 진정시킵니다. \r\n● 카트리지 팩 1개애 카트리지 4개가 있습니다. \r\n● 면도날이 세라믹으로 코팅돼 더 깔끔하고 매끄럽게 면도할 수 있습니다. \r\n● 오픈형 면도날 구조로 세척이 쉽고 막히지 않습니다. \r\n● 면도기가 부드럽게 돌아가 고르고 깔끔하게 면도할 수 있습니다.','깔끔한 면도를 더 간편하게 할 수 있습니다.','KO',7,'null'],
            [104,'남성 스위블 핸들','고무로된 그립으로 완벽한 움직임을 달성해보세요\r\n\r\n• 아름답게 디자인된 메탈과 크롬 핸들, 그리고 놀라운 마무리.\r\n• 얼굴 윤곽 주위를 부드럽게 움직일 수 있는 스위블 헤드.\r\n• 조절을 위한 균형잡힌 인체공학적 그립\r\n• 누름 버튼 교체 방식 및 고무로 된 그립\r\n• 모든 Shaves2U 카트리지 (3, 5, 혹은 6-면도날)에 적합','더 매끄럽게 움직 일 수 있게 해주는 회전 가능한 헤드로 모든 Shaves2U 카트리지를 사용 할 수 있습니다.','KO',34,'null'],
            [105,'남성 6 면도날 카트리지 팩','내구성 위한 다이아몬드 코팅.\r\n\r\n• 알로에 베라 윤활 스트립의 6-면도날 레이저 \r\n• 1개 카트리지 팩은 4개의 프리미엄 카트리지로 구성.\r\n• 미국에서 만든 다이아몬드 코팅 면도날 \r\n• 모든 카트리지 뒤쪽에 스타일링 트리머 장착','1개의 카트리지 팩은 6 면도날 카트리지 4개로 구성되어 있습니다.','KO',33,'null'],
            [106,'Trial Kit - 3 Blade','S3 Trial Kit','S3 Trial Kit','EN',1001,'null'],
            [107,'Trial Kit - 5 Blade','Trial Kit - 5 Blade','Trial Kit - 5 Blade','EN',1002,'null'],
            [108,'Trial Kit - 6 Blade','S6 Trial Kit','S6 Trial Kit','EN',1003,'null'],
            [109,'여성 스타터 팩','경험은 필수입니다.\r\n\r\n이 스타터 팩 구성품:\r\n• 1 x 여성 레이저 핸들\r\n• 2 x 여성 5-면도날 카트리지','스타터 팩은 핸들과 두 개의 카트리지로 구성되어 있습니다','KO',5,'null'],
            [110,'Premium Handle with 3 Blade ','**What makes it good:**\r\n\r\n• Ergonomic rubber grip keeps you in control, even when hands are wet.\r\n\r\n• Weighted metal body for ideal shaving pressure and premium hand-feel. \r\n\r\n• Single-axis swivel mechanism for high-precision shaving. \r\n\r\n• 3 Blade cartridge is gentler on skin, while still providing an excellent finish.','Shave with substance and style with the standard choice.','EN',1004,'null'],
            [111,'Premium Handle with 5 Blade ','**What makes it good:**\r\n\r\n• Great for shaving fuller beards and longer stubble,\r\n\r\n• Gives a smoother finish over larger skin areas, such as cheeks or neck. \r\n\r\n• Accublade® Trimmer system for better styling of goatee and sideburns.\r\n\r\n• Ideal for those who don’t shave daily. ','Shave with substance and style with the standard choice.','EN',1005,'null'],
            [112,'Premium Handle with 6 Blade ','**What makes it good:**\r\n\r\n• Rubberised grip provides comfortable control.\r\n\r\n• Weighted metal body for premium form and function.\r\n\r\n• Perfected with a striking chrome finish.\r\n\r\n• For the smoothest finish of the clean-shaven look.\r\n\r\n• Ideal for hairy guys who shave daily.  ','For the clean-shaven look, leaves the smoothest finish.','EN',1006,'null'],
            [113,'Swivel Handle with 6 Blade','**What makes it good:**\r\n\r\n• Dual-axis swivel mechanism that glides perfectly over facial contours for that perfect clean shave.\r\n\r\n• Metal body for an exceptional form.\r\n\r\n• For the smoothest finish of the clean-shaven look.\r\n\r\n• Ideal for hairy guys who shave daily.  ','For ultimate control over your shaving experience.','EN',1007,'null'],
            [114,'3 Blade Pack','**What makes it good:**\r\n\r\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubbles while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','Ideal for upper lip and chin.','EN',1010,'null'],
            [115,'5 Blade Pack','**What makes it good:**\r\n\r\nDouble-coated blades. Aloe lubricating strip. Anti-clog cartridges. And that’s not all — The classic 5 Blade Razor now features the Accublade® Trimmer system for your goatee and sideburn shaving needs. ','Great for fuller beards and longer stubble.','EN',1011,'null'],
            [116,'6 Blade Pack','**What makes it good:**\r\n\r\nMade with our patented I.C.E. Diamond Coating for durability and sharpness, the 6 Blade also features the MicroSilver BG™ enhanced lubricating strip for a shave that’s above the rest.\r\n','The ultimate smooth finish everyday.','EN',1012,'null'],
            [117,'6중날 세트','**제품의 장점: **\r\n\r\n특허 받은 I.C.E. 다이아몬드 코팅은 내구성과 정교함이 우수하며, 최고의 면도를 위한 MicroSilver BG™ 윤활밴드가 있습니다.','매일 최상의 마무리감을 즐기고 싶은 분께 추천','KO',1012,''],
            [118,'5중날 세트','**제품의 장점:**\r\n\r\n이중코팅 면도날, 알로에 윤활밴드, 막히지 않는 카트리지. 이뿐만 아니라 클래식한 5중날과 함께 Accublade® 트리머로 턱수염 및 구레나룻 스타일을 연출할 수 있습니다.','풍성한 수염과 긴 수염에 최적화된 면도날','KO',1011,''],
            [119,'3중날 세트','**제품의 장점:** \r\n\r\n입술 위 및 턱 부분을 부드럽게 면도할 수 있습니다. 피부 자극이 적고 얇은 수염에 적합합니다. 피부과 테스트를 받은 저자극 면도기입니다.','입술 위와 턱수염에 적합한 면도날','KO',1010,''],
            [120,'5중날 프리미엄 손잡이','**제품의 장점:** \r\n\r\n● 풍성한 수염과 긴 수염에 적합합니다. \r\n\r\n● 뺨이나 목 등 넓은 피부 표면을 부드럽게 면도할 수 있습니다. \r\n\r\n● Accublade® 트리머로 턱수염 및 구레나룻 스타일을 연출할 수 있습니다. \r\n\r\n● 매일 면도하지 않는 분께 적합합니다.','실용적이면서도 프리미엄 면도감을 주는 면도기를 만나보세요','KO',1005,''],
            [121,'6중날 프리미엄 손잡이','**제품의 장점:**\r\n\r\n● 손잡이에 고무가 있어 쉽게 조절할 수 있습니다. \r\n\r\n● 무게감 있는 금속 손잡이로 최고의 디자인과 기능을 선보입니다. \r\n\r\n● 크롬으로 완벽하게 마무리했습니다. \r\n\r\n● 깔끔하고 부드러운 면도를 할 수 있습니다. \r\n\r\n● 매일 면도하는 수염이 많은 분께 적합합니다.','깔끔하고 부드러운 면도를 보장합니다','KO',1006,''],
            [122,'3중날 프리미엄 손잡이','**제품의 장점:** \r\n\r\n● 인체공학적 고무 손잡이로 손에 물기가 있어도 조절하기 쉽습니다. \r\n\r\n● 금속으로 손잡이에 무게감을 줌으로써 압력과 손잡이 느낌이 최고입니다. \r\n\r\n● 단일축 회전형으로 정밀하게 면도할 수 있습니다. \r\n\r\n● 3중날 카트리지는 피부에 자극이 적으면서 깔끔한 면도를 할 수 있습니다.','실용적이면서도 프리미엄 면도감을 주는 면도기를 만나보세요','KO',1004,''],
            [123,'6중날 회전형 손잡이','**제품의 장점:**\r\n\r\n● 이중축 회전형 손잡이는 굴곡진 얼굴선을 따라 완벽한 면도를 할 수 있습니다. \r\n\r\n● 금속이 추가되어 디자인이 뛰어납니다. \r\n\r\n● 부드럽고 깔끔하게 면도할 수 있습니다. \r\n\r\n● 매일 면도하는 수염이 많은 분께 적합합니다.','최고의 컨트롤 감을 자랑합니다.','KO',1007,''],
            [124,'쉐이브 크림','**제품의 장점:**\r\n\r\n● 비타민 E가 함유돼 면도 후 얼굴이 붉어지는 현상을 방지합니다. \r\n● 풍성한 거품으로 인해 얼굴 상처를 줄입니다. \r\n● 가볍고 깔끔한 향이 나 상쾌합니다.','풍성한 쉐이브 크림으로 매일 부드럽게 면도해보세요.','KO',35,''],
            [126,'Awesome Shave Kit – 6 Blade','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 6 Blade Cartridge','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 6 Blade Cartridge','EN',1013,''],
            [127,'Awesome Shave Kit – 5 Blade','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 5 Blade Cartridge','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 5 Blade Cartridge','EN',1014,''],
            [128,'Awesome Shave Kit – 3 Blade','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 3 Blade Cartridge','1 unit of Swivel Handle, Shave Cream, After Shave Cream & 5 units of 3 Blade Cartridge','EN',1015,''],
            [129,'3중날 카트리지 (5개입)','Awesome Shave Kit - S3','Awesome Shave Kit - S3','KO',1015,''],
            [130,'5중날 카트리지 (5개입)','Awesome Shave Kit for S5','Awesome Shave Kit for S5','KO',1014,''],
            [131,'6중날 카트리지 (5개입)','Awesome Shave Kit for S6','Awesome Shave Kit for S6','KO',1013,''],
            [132,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀3','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀3','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀3','ZH-TW',1015,''],
            [133,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀5','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀5','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀5','ZH-TW',1014,''],
            [134,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀6','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀6','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀6','ZH-TW',1013,''],
            [135,'刮鬍刀6','*還沒決定訂購配套？**\n\n採用我們專利的I.C.E金剛石塗層刀，刮鬍阻力低，更耐用，刮鬍刀6刀片還採用了MicroSilver BG™ 增強型潤滑條，挑戰360度無死角區\n','適用於每日刮鬍，最順滑舒適的體驗。 ','ZH-TW',1012,''],
            [136,'刮鬍刀5','*還沒決定訂購配套？**\n\nDouble-coated blades. Aloe lubricating strip. Anti-clog cartridges. And that’s not all — The classic 5 Blade Razor now features the Accublade® Trimmer system for your goatee and sideburn shaving needs. ','適用於修理更濃密的鬍鬚及更長的鬍茬。 ','ZH-TW',1011,''],
            [137,'刮鬍刀3','*還沒決定訂購配套？**\n\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubbles while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','適用於上嘴唇及下巴。','ZH-TW',1010,''],
            [138,'Swivel Handle with 6 Blade','*還沒決定訂購配套？**\n\n• Dual-axis swivel mechanism that glides perfectly over facial contours for that perfect clean shave.\n\n• Metal body for an exceptional form.\n\n• For the smoothest finish of the clean-shaven look.\n\n• Ideal for hairy guys who shave daily.  ','For ultimate control over your shaving experience.','ZH-TW',1007,''],
            [139,'Premium Handle with 6 Blade ','*還沒決定訂購配套？**\n\n• Rubberised grip provides comfortable control.\n\n• Weighted metal body for premium form and function.\n\n• Perfected with a striking chrome finish.\n\n• For the smoothest finish of the clean-shaven look.\n\n• Ideal for hairy guys who shave daily.  ','For the clean-shaven look, leaves the smoothest finish.','ZH-TW',1006,''],
            [140,'Premium Handle with 5 Blade ','*還沒決定訂購配套？**\n\n• Great for shaving fuller beards and longer stubble,\n\n• Gives a smoother finish over larger skin areas, such as cheeks or neck. \n\n• Accublade® Trimmer system for better styling of goatee and sideburns.\n\n• Ideal for those who don’t shave daily. ','Shave with substance and style with the standard choice.','ZH-TW',1005,''],
            [141,'Premium Handle with 3 Blade ','*還沒決定訂購配套？**\n\n• Ergonomic rubber grip keeps you in control, even when hands are wet.\n\n• Weighted metal body for ideal shaving pressure and premium hand-feel. \n\n• Single-axis swivel mechanism for high-precision shaving. \n\n• 3 Blade cartridge is gentler on skin, while still providing an excellent finish.','Shave with substance and style with the standard choice.','ZH-TW',1004,''],
            [142,'Trial Kit - 6 Blade','S6 Trial Kit','S6 Trial Kit','ZH-TW',1003,''],
            [143,'Trial Kit - 5 Blade','Trial Kit - 5 Blade','Trial Kit - 5 Blade','ZH-TW',1002,''],
            [144,'Trial Kit - 3 Blade','Trial Kit - 3 Blade','Trial Kit - 3 Blade','ZH-TW',1001,''],
            [145,'刮鬍膏','*還沒決定訂購配套？**\n\n• Enriched with Vitamin E to combat post-shave redness\n• Rich, easy-to-lather cream that cushions skin from cuts\n• Light, clean scent that refreshes\n','使用豐盈泡沫的刮鬍膏開啟您的刮鬍體驗。','ZH-TW',35,''],
            [146,'After Shave Cream','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','ZH-TW',12,''],
            [147,'Trial Kit - 3 Blade (Swivel Handle)','Trial Kit - 3 Blade (Swivel Handle)','Trial Kit - 3 Blade (Swivel Handle)','EN',1016,''],
            [148,'Trial Kit - 刮鬍刀3 (旋轉刀架)','Trial Kit - 刮鬍刀3 (旋轉刀架)','Trial Kit - 刮鬍刀3 (旋轉刀架)','ZH-HK',1016,''],
            [149,'Trial Kit - 5 Blade (Swivel Handle)','Trial Kit - 5 Blade (Swivel Handle)','Trial Kit - 5 Blade (Swivel Handle)','EN',1017,''],
            [150,'Trial Kit - 刮鬍刀5 (旋轉刀架)','Trial Kit - 刮鬍刀5 (旋轉刀架)','Trial Kit - 刮鬍刀5 (旋轉刀架)','ZH-HK',1017,''],
            [151,'Trial Kit - 6 Blade (Swivel Handle)','Trial Kit - 6 Blade (Swivel Handle)','Trial Kit - 6 Blade (Swivel Handle)','EN',1018,''],
            [152,'Trial Kit - 刮鬍刀6 (旋轉刀架)','Trial Kit - 刮鬍刀6 (旋轉刀架)','Trial Kit - 刮鬍刀6 (旋轉刀架)','ZH-HK',1018,''],
            [153,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀3','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀3','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀3','ZH-HK',1015,''],
            [154,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀5','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀5','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀5','ZH-HK',1014,''],
            [155,'A.S.K 超凡刮鬍禮盒 - 刮鬍刀6','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀6','內含旋轉刀架，刮鬍膏，鬚後膏各一個與5 個刮鬍刀6','ZH-HK',1013,''],
            [156,'刮鬍刀3','*還沒決定訂購配套？**\n\nExperience gentler shaving that’s ideal even for upper lip and chin areas. Works great on thinner stubbles while keeping skin irritation at bay. Dermatologically tested and hypoallergenic.  ','適用於上嘴唇及下巴。','ZH-HK',1010,''],
            [158,'刮鬍刀5','*還沒決定訂購配套？**\n\nDouble-coated blades. Aloe lubricating strip. Anti-clog cartridges. And that’s not all — The classic 5 Blade Razor now features the Accublade® Trimmer system for your goatee and sideburn shaving needs. ','適用於修理更濃密的鬍鬚及更長的鬍茬。 ','ZH-HK',1011,''],
            [159,'刮鬍刀6','*還沒決定訂購配套？**\n\n採用我們專利的I.C.E金剛石塗層刀，刮鬍阻力低，更耐用，刮鬍刀6刀片還採用了MicroSilver BG™ 增強型潤滑條，挑戰360度無死角區\n','適用於每日刮鬍，最順滑舒適的體驗。 ','ZH-HK',1012,''],
            [160,'刮鬍膏','*還沒決定訂購配套？**\n\n• Enriched with Vitamin E to combat post-shave redness\n• Rich, easy-to-lather cream that cushions skin from cuts\n• Light, clean scent that refreshes\n','使用豐盈泡沫的刮鬍膏開啟您的刮鬍體驗。','ZH-HK',35,''],
            [161,'After Shave Cream','**What makes it good:**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','EN',1019,''],
            [162,'鬚後膏','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','ZH-TW',1019,'']
            ,[163,'Toiletry bag','Toiletry bag','Toiletry bag','EN',1025,'']
            ,[164,'Razor Protector','Shaver Sleeve','Shaver Sleeve','EN',1026,'']
            ,[171,'浴室包','Toiletry bag','Toiletry bag','ZH-HK',1025,'']
            ,[172,'收纳套','Shaver Sleeve','Shaver Sleeve','ZH-HK',1026,'']
            ,[173,'浴室包','Toiletry bag','Toiletry bag','ZH-TW',1025,'']
            ,[174,'收纳套','Shaver Sleeve','Shaver Sleeve','ZH-TW',1026,'']
            ,[175,'After Shave Cream','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','KO',12,'']
            ,[176,'애프터 쉐이브 크림','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','KO',1019,'']
            ,[177,'After Shave Cream','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','ZH-HK',12,'']
            ,[178,'Men\'s Premium Handle','Keep it simple, with this beautifully crafted, weighty metal handle.\r\n\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)\r\n• Soft rubberised grip with precise balance and great heft\r\n• Striking chrome finish','Soft rubberised handle that fits all Shaves2U cartridges.','ZH-HK',34,'null']
            ,[179,'Men\'s Premium Handle','Keep it simple, with this beautifully crafted, weighty metal handle.\r\n\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)\r\n• Soft rubberised grip with precise balance and great heft\r\n• Striking chrome finish','Soft rubberised handle that fits all Shaves2U cartridges.','ZH-TW',34,'null']
            ,[180,'旋轉刀架','Achieve perfect manoeuvrability with a rubberised grip.\r\n\r\n• Beautifully designed metal and chrome handle, with a stunning finish.\r\n• Swivel head allows smoother movement around facial contours.\r\n• Balanced ergonomic grip for added control\r\n• Push-button release and rubberised finger rest\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)','貼合每種臉型給予最平滑的剃鬚體驗。','ZH-HK',34,'null']
            ,[181,'旋轉刀架','Achieve perfect manoeuvrability with a rubberised grip.\r\n\r\n• Beautifully designed metal and chrome handle, with a stunning finish.\r\n• Swivel head allows smoother movement around facial contours.\r\n• Balanced ergonomic grip for added control\r\n• Push-button release and rubberised finger rest\r\n• Fits all Shaves2U cartridges (3, 5, or 6 Blade)','貼合每種臉型給予最平滑的剃鬚體驗。','ZH-TW',34,'null']
            ,[182,'세면백','Toiletry bag','Toiletry bag','ZH-TW',1025,'']
            ,[183,'면도기 커버','Shaver Sleeve','Shaver Sleeve','ZH-TW',1026,'']
            ,[184,'刮鬍膏','*還沒決定訂購配套？**\n\n• Enriched with Vitamin E to combat post-shave redness\n• Rich, easy-to-lather cream that cushions skin from cuts\n• Light, clean scent that refreshes\n','使用豐盈泡沫的刮鬍膏開啟您的刮鬍體驗。','ZH-TW',121,'']
            ,[185,'쉐이브 크림','**제품의 장점:**\r\n\r\n● 비타민 E가 함유돼 면도 후 얼굴이 붉어지는 현상을 방지합니다. \r\n● 풍성한 거품으로 인해 얼굴 상처를 줄입니다. \r\n● 가볍고 깔끔한 향이 나 상쾌합니다.','풍성한 쉐이브 크림으로 매일 부드럽게 면도해보세요.','KO',121,'']
            ,[186,'剃鬚膏','*還沒決定訂購配套？**\n\n• Enriched with Vitamin E to combat post-shave redness\n• Rich, easy-to-lather cream that cushions skin from cuts\n• Light, clean scent that refreshes\n','使用豐盈泡沫的刮鬍膏開啟您的刮鬍體驗。','ZH-HK',121,'']
            ,[187,'鬚後膏','*還沒決定訂購配套？**\n\n• Formulated to cool, calm, and balance freshly shaven skin\n\n• Replenishes skin’s lost moisture with conditioning ingredients\n\n• Light, clean scent for a refreshing post-shave ritual\n','For an icy cool finishing touch that’s good for your skin.','ZH-HK',1019,'']
            ,[188,'Women’s Starter Kit - 5 Blade','Women’s Starter Kit - 5 Blade','Women’s Starter Kit - 5 Blade','EN',1020,'']
        ];
        $ProductTranslateAll = ProductTranslate::all("id");
        $ProductTranslateAllArray = array();
        foreach ($ProductTranslateAll as $pc) {
            $ProductTranslateAllArray[] =  $pc->id;
        }
        $this->command(__FUNCTION__ . " => Product record has not been found. Adding new product translate....");
        foreach ($producttranslate as $p) {
                if (!in_array($p[0], $ProductTranslateAllArray)) {
                $this->command(__FUNCTION__ . "starting adding -> ". $p[0] );
                $producttranslate = new ProductTranslate();
                $producttranslate->id = $p[0];
                $producttranslate->name = $p[1];
                $producttranslate->description = $p[2];
                $producttranslate->shortDescription = $p[3];
                $producttranslate->langCode = $p[4];
                $producttranslate->ProductId = $p[5];
                $producttranslate->seoTitle = $p[6];
                $producttranslate->save();
                }
        }

        $this->command(__FUNCTION__ . "  ===== Successfully added product translate records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg) {
        $this->command->info($msg);
    }

}
