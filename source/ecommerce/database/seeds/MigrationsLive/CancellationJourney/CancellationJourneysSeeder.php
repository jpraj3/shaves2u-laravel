<?php

use Illuminate\Database\Seeder;
use App\Models\CancellationJourney\CancellationJourneys;
use Illuminate\Support\Facades\DB;
class CancellationJourneysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $cancellationjourney = $db_old->table('cancellationjourneys')->get();
 
        $this->command(__FUNCTION__ . " Truncating data...");
        // CancellationJourneys::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($cancellationjourney as $cj) {
            $cjs = new CancellationJourneys();
            $cjs->id = $cj->id;
            $cjs->email = $cj->email;
            $cjs->CancellationTranslatesId = $cj->MessagestranslatesId;
            $cjs->OtherReason = $cj->OtherReason;
            $cjs->UserId = $cj->UserId;
            $cjs->OrderId = $cj->OrderId;
            $cjs->SubscriptionId = $cj->SubscriptionId;
            $cjs->created_at = $cj->createdAt;
            $cjs->updated_at = $cj->updatedAt;
            $cjs->planType = $cj->planType;
            $cjs->subscriptionName = $cj->subscriptionName;
            $cjs->hasCancelled = $cj->hasCancelled;
            $cjs->hasChangedBlade = $cj->hasChangedCartridge; //hasChangedCartridge
            $cjs->modifiedBlade = $cj->selectedCartridgePlan; //selectedCartridgePlan
            $cjs->hasChangedFrequency = $cj->hasChangedPlan; //hasChangedPlan
            $cjs->modifiedFrequency = $cj->selectedPlanUsage; //selectedPlanUsage
            $cjs->hasGetFreeProduct = $cj->hasGetNewPackage; //hasGetNewPackage
            $cjs->hasGetDiscount = $cj->hasGetDiscount;
            $cjs->hasPausedSubscription = $cj->hasPausedSubscription;
            $cjs->discountPercent = $cj->discountPercent;

            $cjs->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
