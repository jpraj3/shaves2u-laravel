<?php

use Illuminate\Database\Seeder;
use App\Models\CancellationJourney\CancellationFollowUp;
use Illuminate\Support\Facades\DB;
class CancellationFollowUpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $cancellationjourneyfollowup = $db_old->table('followupcancels')->get();
  
        $this->command(__FUNCTION__ . " Truncating data...");
        // CancellationFollowUp::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($cancellationjourneyfollowup as $cjfu) {
            $cjfus = new CancellationFollowUp();
            $cjfus->id = $cjfu->id;
            $cjfus->SubscriptionId = $cjfu->SubscriptionId;
            $cjfus->UserId = $cjfu->UserId;
            $cjfus->CancelEdm1 = $cjfu->CancelEdm1;
            $cjfus->CancelEdm2 = $cjfu->CancelEdm2;
            $cjfus->CancelEdm3 = $cjfu->CancelEdm3;
            $cjfus->CancelEdm4 = $cjfu->CancelEdm4;
            $cjfus->CancelEdm5 = $cjfu->CancelEdm5;
            $cjfus->created_at = $cjfu->CreatedAt;
            $cjfus->updated_at = $cjfu->UpdatedAt;
            $cjfus->ReminderDate = $cjfu->ReminderDate;
            $cjfus->ReminderStatus = $cjfu->ReminderStatus;
            $cjfus->NextJourneyStatus = $cjfu->NextJourneyStatus;
            $cjfus->JourneyDesc = $cjfu->JourneyDesc;
            $cjfus->Status = $cjfu->Status;

            $cjfus->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
