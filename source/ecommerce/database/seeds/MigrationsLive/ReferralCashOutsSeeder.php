<?php

use Illuminate\Database\Seeder;
use App\Models\Rebates\ReferralCashOut;
use Illuminate\Support\Facades\DB;
class ReferralCashOutsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $rewardsouts = $db_old->table('rewardsouts')
        ->join('users', 'rewardsouts.UserId', '=', 'users.id')
        ->select('rewardsouts.id as rid', 'rewardsouts.CountryId as cid', 'rewardsouts.createdAt as rcreatedAt', 'rewardsouts.updatedAt as rupdatedAt','rewardsouts.*', 'users.*')
        ->get();

        // $this->command(__FUNCTION__ . " Truncating data...");
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($rewardsouts as $ro) {
            $rcos = new ReferralCashOut();
            $rcos->id = $ro->rid;
            $rcos->name = $ro->firstName." ".$ro->lastName;
            $rcos->email = $ro->email;
            $rcos->amount = $ro->value;
            $rcos->status = "withdrawn";
            $rcos->UserId = $ro->UserId;
            $rcos->CountryId = $ro->cid;
            $rcos->created_at = $ro->rcreatedAt;
            $rcos->updated_at = $ro->rupdatedAt;

            $rcos->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
