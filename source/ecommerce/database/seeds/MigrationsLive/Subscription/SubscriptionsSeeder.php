<?php

use Illuminate\Database\Seeder;
use App\Models\Subscriptions\Subscriptions;
use Illuminate\Support\Facades\DB;
class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $subscriptions = $db_old->table('subscriptions')->get();

        // $this->command(__FUNCTION__ . " Truncating data...");
        // Subscriptions::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($subscriptions as $sub) {
           $currentcycle  =  0;
           if (!empty($sub->PlanCountryId) && empty($sub->CustomPlanId)) {
            $currentcycle = $sub->currentChargeNumber + 1;
           }
           elseif(!empty($sub->CustomPlanId) && empty($sub->PlanCountryId)){
            $currentcycle = $sub->currentChargeNumber;
           }

            $subs = new Subscriptions();
            $subs->id = $sub->id;
            $subs->nextDeliverDate = $sub->nextDeliverDate;
            $subs->startDeliverDate = $sub->startDeliverDate;
            $subs->expiredDateAt = $sub->expiredDateAt;
            $subs->currentDeliverNumber = $sub->currentDeliverNumber;

            $subs->currentCycle = $currentcycle;

            $subs->totalDeliverTimes = $sub->totalDeliverTimes;
            $subs->nextChargeDate = $sub->nextChargeDate;
            $subs->currentChargeNumber = $sub->currentChargeNumber;
            $subs->totalChargeTimes = $sub->totalChargeTimes;
            $subs->recharge_date = $sub->recharge_date;
            $subs->total_recharge = $sub->total_recharge;
            $subs->unrealizedCust =  $sub->unrealizedCust;
            $subs->pricePerCharge = $sub->pricePerCharge;
            $subs->currentOrderTime = $sub->currentOrderTime;
            $subs->currentOrderId = $sub->currentOrderId;
      
            if($sub->status == "Canceled"){
                $subs->status = "Cancelled";
            }else{
                $subs->status = $sub->status;
            }
            
            $subs->fullname = $sub->fullname;
            $subs->email = $sub->email;
            $subs->phone = $sub->phone;
            $subs->discountPercent = $sub->discountPercent;
            $subs->qty = $sub->qty;
            $subs->created_at = $sub->createdAt;
            $subs->updated_at = $sub->updatedAt;
            $subs->CardId = $sub->CardId;
            $subs->PlanId = $sub->PlanId;
            $subs->SellerUserId = $sub->SellerUserId;
            $subs->SkuId = $sub->SkuId;
            $subs->UserId = $sub->UserId;
            $subs->DeliveryAddressId = $sub->DeliveryAddressId;
            $subs->BillingAddressId = $sub->BillingAddressId;
            $subs->PlanCountryId = $sub->PlanCountryId;
            $subs->lastDeliverydate = $sub->lastDeliverydate;
            $subs->isTrial = $sub->isTrial;
            $subs->isCustom = $sub->isCustom;
            $subs->price = $sub->price;
            $subs->convertTrial2Subs = $sub->convertTrial2Subs;
            $subs->hasSendReferralInvite = $sub->hasSendReferralInvite;
            $subs->hasSendFollowUp1 = $sub->hasSendFollowUp1;
            $subs->hasSendFollowUp2 = $sub->hasSendFollowUp2;
            $subs->hasSendFollowUp5Days = $sub->hasSendFollowUp5Days;
            $subs->hasSendCancelRequest = $sub->hasSendCancelRequest;
            $subs->hasFlag = $sub->hasFlag;
            $subs->hasRechargeFlag = $sub->hasRechargeFlag;
            $subs->isOffline = $sub->isOffline;
            $subs->channelType = $sub->channelType;
            $subs->eventLocationCode = $sub->eventLocationCode;
            $subs->MarketingOfficeId = $sub->MarketingOfficeId;
            $subs->cancellationReason = $sub->cancellationReason;
            $subs->CustomPlanId = $sub->CustomPlanId;
            $subs->PlanOptionId = $sub->PlanOptionId;
            $subs->promoCode = $sub->promoCode;
            $subs->promotionId = $sub->promotionId;
            $subs->isDirectTrial = $sub->isDirectTrial;
            $subs->imp_uid = $sub->imp_uid;
            $subs->merchant_uid = $sub->merchant_uid;
            $subs->temp_discountPercent = $sub->temp_discountPercent;
            $subs->hasTriedCancel = $sub->hasTriedCancel;
            $subs->hasMaximumTenure = $sub->hasMaximumTenure;


            $subs->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
