<?php

use Illuminate\Database\Seeder;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\Plans\PlanSKU;
use Illuminate\Support\Facades\DB;
class SubscriptionsCustomPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $subscriptions = $db_old->table('subscriptions')
        ->join('customplans', 'subscriptions.CustomPlanId', '=', 'customplans.id')
        ->join('plantypes', 'customplans.PlanTypeId', '=', 'plantypes.id')
        ->select('subscriptions.id as subid', 'customplans.id as cusid', 'customplans.CountryId as ctid','plantypes.subsequentDeliverDuration as subsequentDeliverDuration', DB::raw('(SELECT group_concat(customplandetails.ProductCountryId) FROM customplandetails WHERE customplans.id = customplandetails.CustomPlanId  GROUP BY customplandetails.CustomPlanId) as customproduct'))
        ->whereNotNull('subscriptions.CustomPlanId')
        ->get();

        // $this->command(__FUNCTION__ . " Truncating data...");
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($subscriptions as $sub) {
            $duration = substr($sub->subsequentDeliverDuration,0,1);
            $plist = explode(",", $sub->customproduct);
            $productaddon = "";
            $blade = "";
            $planlist = "";
            foreach ($plist as $p) {
                if ($p==28 || $p==29 || $p==160 || $p==170 || $p==208 ) {
                    $subs = new SubscriptionsProductAddOns();

                    $subs->subscriptionId = $sub->subid;
                    $subs->ProductCountryId = $p;
                    $subs->qty = 1;
                    $subs->cycle = "all";
                    $subs->isWithTrialKit =null;
                    $subs->created_at = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                    $subs->updated_at = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        
        
                    $subs->save();
                } else {
                    $blade =  $p;
                    $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
                    ->join('plan_details', 'plan_details.PlanId', 'plans.id')
                    ->select('plans.id')
                    ->where('plansku.duration', $duration)
                    ->where('plans.plantype', "custom") 
                    ->where('plans.CountryId', $sub->ctid) 
                    ->where('plan_details.ProductCountryId', $blade) 
                    ->first();
                }
            }
            $subs = Subscriptions::find($sub->subid);
            if(!empty($planlist)){
            $subs->PlanId =  $planlist->id;
            }else{
                $this->command(__FUNCTION__ . " Empty Custom Plan, durantion: ".$duration." ,countryid : ".$sub->ctid." ,ProductCountryId : ".$blade );
            }
            $subs->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
