<?php

use Illuminate\Database\Seeder;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use Illuminate\Support\Facades\DB;
class SubTrialAddonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $db_old = DB::connection('mysql_old');
        $subscriptions = $db_old->table('subscriptions')
        ->join('planoptions', 'subscriptions.PlanOptionId', '=', 'planoptions.id')
        ->join('planoptionproducts', 'planoptions.id', '=', 'planoptionproducts.PlanOptionId')
        ->select('subscriptions.id as subid', 'planoptionproducts.ProductCountryId as pcid', 'planoptionproducts.qty as pqty')
        ->where('subscriptions.isTrial', '=', 1)
        ->where('planoptions.hasShaveCream', '=', 1)
        ->where(function ($query)  {
            $query->orWhereRaw('planoptionproducts.ProductCountryId = 28')
            ->orWhereRaw('planoptionproducts.ProductCountryId = 29')
            ->orWhereRaw('planoptionproducts.ProductCountryId = 160')
            ->orWhereRaw('planoptionproducts.ProductCountryId = 170')
            ->orWhereRaw('planoptionproducts.ProductCountryId = 208');
        })
        ->whereNotNull('subscriptions.PlanOptionId')
        ->get();

        // $this->command(__FUNCTION__ . " Truncating data...");
        // SubscriptionsProductAddOns::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($subscriptions as $sub) {
            $subs = new SubscriptionsProductAddOns();

            $subs->subscriptionId = $sub->subid;
            $subs->ProductCountryId = $sub->pcid;
            $subs->qty = $sub->pqty;
            $subs->cycle = "all";
            $subs->isWithTrialKit =1;
            $subs->created_at = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
            $subs->updated_at = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');


            $subs->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
