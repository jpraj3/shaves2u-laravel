<?php

use Illuminate\Database\Seeder;
use App\Models\Promotions\Promotions;
use Illuminate\Support\Facades\DB;
/*promotion*/
class PromotionsLiveTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("promotion", $now);

        $this->command(__CLASS__ . " Done...");
    }
    // $promotion = [
    //     [336,10,'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/1f4fc550-ca77-11e7-af5b-972dbb5a8a31.png',NULL,NULL,'2099-12-31','2017-11-10',NULL,0,NULL,NULL,'all','all',NULL,1,1,NULL,'{77:1,20298:1,20340:1,20366:1,20380:1,20425:1,20445:1,20446:1,20466:1,20514:1,20650:1,20827:1,21528:1,21529:1,22362:1,22690:1,22967:1,23399:1,23673:1,23838:1,24308:1,25990:1,27592:1,27977:1,34565:1,35628:1,38534:1,39115:1,39917:1,40949:1,40962:1,41219:1,41745:1,42194:1,42806:1,43056:1,43417:1,43513:1,43544:1,45294:1,45462:1,45634:1,48961:1,49244:1,49278:1,49307:1,51280:1,52098:1,52292:1,52773:1,52822:1,54408:1,54719:1,54722:1,54726:1,54832:1,54841:1,55317:1,55404:1,55601:1,55608:1,55862:1,55945:1,56709:1,57103:1,57480:1,57623:1,57644:1,57809:1,57863:1,58140:1,58175:1,58529:1,58554:1,58700:1,59123:1,59232:1,59279:1,59476:1,59712:1,59728:1,59937:1,60132:1,60574:1,60755:1,60761:1,60781:1,60824:1,60971:1,61371:1,61630:1,61632:1,61633:1,61889:1,61893:1,62016:1,62017:1,62048:1,62159:1,62295:1,62296:1,62464:1,62492:1,62574:1,62658:1,62759:1,66149:1}',0,'2017-11-16 02:37:43','2019-07-22 08:04:52',7,0,1,1],
    //     [337,10,'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/484acc70-ca77-11e7-af5b-972dbb5a8a31.png',NULL,NULL,'2099-12-31','2017-11-10',NULL,0,NULL,NULL,'all','all',NULL,1,1,NULL,'{15172:1,15230:1,15237:1,20221:1,20242:1,20248:1,20397:1,20438:1,20452:1,20464:1,20470:1,20498:1,20545:1,20556:1,20614:1,20712:1,20760:1,20765:1,20860:1,21567:1,21648:1,21659:1,22015:1,22665:1,22689:1,22820:1,23170:1,23312:1,23354:1,23774:1,24461:1,24888:1,25096:1,25707:1,25928:1,25985:1,26017:1,26191:1,26269:1,26341:1,26701:1,26724:1,26797:1,27106:1,27879:1,28318:1,28362:1,28468:1,28607:1,28751:1,29400:1,30328:1,30393:1,30771:1,31020:1,31986:1,32044:1,32079:1,32130:1,32474:1,32486:1,33957:1,34181:1,34311:1,34489:1,34565:1,34708:1,35566:1,35589:1,36012:1,36048:1,36686:1,37238:1,38921:1,39113:1,39117:1,39711:1,39945:1,40900:1,40960:1,42178:1,43055:1,44293:1,44381:1,45076:1,45128:1,45555:1,45572:1,46113:1,49102:1,49454:1,49653:1,49854:1,50182:1,50243:1,50334:1,50753:1,51014:1,51109:1,51454:1,51540:1,51608:1,51649:1,51653:1,51887:1,51901:1,51970:1,52552:1,52705:1,52716:1,52844:1,52940:1,53068:1,53329:1,53346:1,53735:1,53900:1,54226:1,54287:1,54305:1,54422:1,54428:1,54468:1,54491:1,54602:1,54788:1,54810:1,54937:1,55414:1,55515:1,55524:1,55788:1,55860:1,55944:1,56107:1,56174:1,56231:1,56326:1,56657:1,56694:1,56799:1,57411:1,57682:1,57758:1,57902:1,57933:1,58103:1,58125:1,58237:1,58415:1,58535:1,58740:1,58865:1,58901:1,58992:1,58994:1,59032:1,59044:1,59077:1,59210:1,59267:1,59397:1,59436:1,59456:1,59500:1,59533:1,59603:1,59739:1,59756:1,59808:1,59930:1,60155:1,60166:1,60363:1,60412:1,60443:1,60463:1,60592:1,60611:1,60637:1,60880:1,60916:1,60983:1,61020:1,61073:1,61171:1,61175:1,61368:1,61391:1,61459:1,61475:1,61480:1,61481:1,61746:1,62147:1,62167:1,62240:1,62358:1,62547:1,62915:1,64817:1,65015:1,65048:1,65100:1,68094:1,[object SequelizeInstance:User]:1}',0,'2017-11-16 02:38:52','2019-07-19 06:37:25',1,0,1,1],
    //     [7918,10,'https://smart-shaves-development.s3-ap-southeast-1.amazonaws.com/product-images/1f4fc550-ca77-11e7-af5b-972dbb5a8a31.png',NULL,NULL,'2099-12-31','2017-11-10',NULL,0,NULL,NULL,'all','all',NULL,1,1,NULL,'{59213:1,59764:1,62758:1}',0,'2018-12-31 00:00:00','2019-01-27 04:34:46',8,0,1,1],
    // ];
    public function addRecords($type, $now) {
  
        $db_old = DB::connection('mysql_old');
        $promotion = $db_old->table('promotions')->get();
        $PromotionAll = Promotions::all("id");
        $PromotionAllArray = array();
        foreach ($PromotionAll as $pc) {
            $PromotionAllArray[] =  $pc->id;
        }
        $this->command(__FUNCTION__ . " => Promotion record has not been found. Adding new promotion....");
        foreach ($promotion as $p) {

                if (!in_array($p->id, $PromotionAllArray)) {
                $this->command(__FUNCTION__ . "starting adding -> ". $p->id );
                $promotion = new Promotions();
                $promotion->id = $p->id;
                $promotion->discount = $p->discount;
                $promotion->bannerUrl = $p->bannerUrl;
                $promotion->emailTemplateId = $p->emailTemplateId;
                $promotion->emailTemplateOptions = $p->emailTemplateOptions;
                $promotion->expiredAt = $p->expiredAt;
                $promotion->activeAt = $p->activeAt;

                $promotion->minSpend = $p->minSpend;
                $promotion->allowMinSpendForTotalAmount = $p->allowMinSpendForTotalAmount;
                $promotion->maxDiscount = $p->maxDiscount;
                $promotion->planIds = $p->planIds;
                $promotion->productCountryIds = $p->planIds;
                $promotion->planCountryIds = $p->planCountryIds;
                $promotion->freeProductCountryIds = $p->freeProductCountryIds;
                $promotion->isGeneric = $p->isGeneric;

                $promotion->timePerUser = $p->timePerUser;
                $promotion->totalUsage = $p->totalUsage;
                $promotion->appliedTo = $p->appliedTo;
                $promotion->isFreeShipping = $p->isFreeShipping;
                $promotion->created_at = $p->createdAt;
                $promotion->updated_at = $p->updatedAt;
                $promotion->CountryId = $p->CountryId;
                $promotion->isBaApp = $p->isBaApp;
                $promotion->isOnline = $p->isOnline;
                $promotion->isOffline = $p->isOffline;

                $promotion->save();
                }
        }

        $this->command(__FUNCTION__ . "  ===== Successfully added promotion records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }
    
    public function command($msg) {
        $this->command->info($msg);
    }

}
