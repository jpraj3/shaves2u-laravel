<?php

use App\Models\BaWebsite\SellerUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class SellerUserLiveTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " => Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("SellerUser", $now);

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type, $now)
    {
        $db_old = DB::connection('mysql_old');
        $SellerUser = $db_old->table('sellerusers')->get();

        $this->command(__FUNCTION__ . " => SellerUser record has not been found. Adding new SellerUser....");
        foreach ($SellerUser as $sa) {
            $SellerUser = new SellerUser();
            $SellerUser->id = $sa->id;
            $SellerUser->badgeId = $sa->badgeId;
            $SellerUser->icNumber = $sa->icNumber;
            $SellerUser->agentName = $sa->agentName;
            $SellerUser->startDate = $sa->startDate;
            $SellerUser->campaign = $sa->campaign;
            $SellerUser->email = $sa->email;
            $SellerUser->latestUpdatedDate = $sa->latestUpdatedDate;
            $SellerUser->created_at = $sa->createdAt;
            $SellerUser->updated_at = $sa->updatedAt;
            $SellerUser->CountryId = $sa->CountryId;
            $SellerUser->isActive = $sa->isActive;
            $SellerUser->channelType = $sa->channelType;
            $SellerUser->eventLocationCode = $sa->eventLocationCode;
            $SellerUser->MarketingOfficeId = $sa->MarketingOfficeId;
            $SellerUser->lastLoginAt = $sa->lastLoginAt;
            $SellerUser->password = Hash::make($sa->icNumber);
            $SellerUser->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added SellerUser records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
