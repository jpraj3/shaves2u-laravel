<?php

use Illuminate\Database\Seeder;
use App\Models\Admins\Admin;
/*admin*/
class AdminTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command(__CLASS__ . " => Running...");

        $this->addRecords("admin");

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type)
    {
        $admin = [
            [
                'admin@gmail.com', 'admin123', 'Administrator', 'Administrator', '1', '1', '1', '+60 12-3456 789', '1', '1', '1'

            ]
        ];
        Admin::truncate();
        $this->command(__FUNCTION__ . " => admin record has not been found. Adding new admin....");
        foreach ($admin as $a) {
            $admin = new Admin();
            $admin->email = $a[0];
            $admin->password = md5($a[1]);
            $admin->firstName = $a[2];
            $admin->lastName = $a[3];
            $admin->staffId = $a[4];
            $admin->isActive = $a[5];
            $admin->viewAllCountry = $a[6];
            $admin->phone = $a[7];
            $admin->CountryId = $a[8];
            $admin->RoleId = $a[9];
            $admin->MarketingOfficeId = $a[10];
            $admin->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added admin records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
