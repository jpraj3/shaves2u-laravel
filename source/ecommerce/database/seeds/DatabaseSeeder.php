<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $seeder = [

             // Promotions
            // PromotionsTblSeeder::class,
            // PromotionCodesTblSeeder::class,
            // PromotionTranslatesTblSeeder::class,

             // Ba Agents
            //  SellerUserTblSeeder::class,

            //   // Countries & Languages
            //  CountriesSeeder::class,
            //  LanguageDetailsSeeder::class,
            //  MetaTagsSeeder::class,
            //  SupportedLanguagesSeeder::class,
            //  StatesSeeder::class,

            //   // Cancellation Journey
            //  CancellationReasonTranslatesSeeder::class,

            //   // Customers
            //  UserTblSeeder::class,

            //   // Plans
            //  PlanFrequencyTblSeeder::class,

            //   // Admin
            //  AdminTblSeeder::class,

            //   // Referral
            //  RewardsCurrenciesTblSeeder::class,


             // Migration Live
            // SellerUserLiveTblSeeder::class,
              // Products
            // ProductCountryTblSeeder::class,
            // ProductTypeTblSeeder::class,
            // ProductTypeTranslateTblSeeder::class,
            // ProductTblSeeder::class,
            // ProductDetailTblSeeder::class,
            // ProductImageTblSeeder::class,
            // ProductTranslateTblSeeder::class,
            // ProductTypeDetailSeeder::class, //if product type detail empty

            // PromotionsLiveTblSeeder::class,
            
            // SubscriptionsSeeder::class,
            // SubTrialAddonSeeder::class,
            // SubscriptionsCustomPlanSeeder::class,
            // SubscriptionsTrialPlanSeeder::class,
            // OrderDetailSeeder::class,
            // ReferralCashOutsSeeder::class,
            // CancellationJourneysSeeder::class,
            // CancellationFollowUpSeeder::class,
            
        ];

        foreach ($seeder as $seed) {
            $this->call($seed);
            $this->command->info("");
        }
    }
}
