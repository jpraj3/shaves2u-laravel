<?php

use App\Models\Faqs\Faqs;
use Illuminate\Database\Seeder;

class FaqTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $data = [
             // Malaysia
            ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'EN', 1],
            ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'EN', 1],
            ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'EN', 1],
            ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'EN', 1],
            ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'EN', 1],
            ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'EN', 1],
            ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'EN', 1],
            ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'EN', 1],

             // Hong Kong
             ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'EN', 2],
             ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'EN', 2],
             ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'EN', 2],
             ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'EN', 2],
             ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'EN', 2],
             ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'EN', 2],
             ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'EN', 2],
             ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'EN', 2],

             ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'ZH-HK', 2],
             ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'ZH-HK', 2],
             ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'ZH-HK', 2],
             ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'ZH-HK', 2],
             ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'ZH-HK', 2],
             ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'ZH-HK', 2],
             ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'ZH-HK', 2],
             ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'ZH-HK', 2],

            // Singapore
            ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'EN', 7],
            ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'EN', 7],
            ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'EN', 7],
            ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'EN', 7],
            ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'EN', 7],
            ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'EN', 7],
            ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'EN', 7],
            ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'EN', 7],

            // Korea
            ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'KO', 8],
            ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'KO', 8],
            ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'KO', 8],
            ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'KO', 8],
            ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'KO', 8],
            ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'KO', 8],
            ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'KO', 8],
            ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'KO', 8],

             // Taiwan
             ['shaveplan', 'Shave Plan', '<h3><b>Shave Plan</b></h3>', 1, '0', 'ZH-TW', 9],
             ['deliveryshipping', 'Delivery<span class=\"d-lg-none\"> / </span><br>Shipping', '<h3><b>Delivery / Shipping</b></h3>', '0', '0', 'ZH-TW', 9],
             ['returnsrefunds', 'Returns<span class=\"d-lg-none\"> / </span><br>Refunds', '<h3><b>Returns / Refunds</b></h3>', '0', '0', 'ZH-TW', 9],
             ['payment', 'Payment', '<h3><b>Payment</b></h3>', '0', '0', 'ZH-TW', 9],
             ['technicalissues', 'Technical<br> Issues', '<h3><b>Technical Issues</b></h3>', '0', '0', 'ZH-TW', 9],
             ['account', 'Account', '<h3><b>Account</b></h3>', '0', '0', 'ZH-TW', 9],
             ['products', 'Products', '<h3><b>Products</b></h3>', '0', 1, 'ZH-TW', 9],
             ['companyquestions', 'Company<br> Questions', '<h3><b>Company Questions</b></h3>', '0', '0', 'ZH-TW', 9],

        ];
        $this->command(__FUNCTION__ . " Truncating data...");
        Faqs::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($data as $dt) {
            $faq = new Faqs();
            $faq->type = $dt[0];
            $faq->title = $dt[1];
            $faq->subtitle = $dt[2];
            $faq->isSubscription = $dt[3];
            $faq->isAlaCarte = $dt[4];
            $faq->langCode = $dt[5];
            $faq->CountryId = $dt[6];
            $faq->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
