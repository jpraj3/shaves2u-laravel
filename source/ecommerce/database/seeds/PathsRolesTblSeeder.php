<?php

use Illuminate\Database\Seeder;
use App\Models\Admins\Paths;
use App\Models\Admins\Roles;
use App\Models\Admins\RoleDetails;

class PathsRolesTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command(__CLASS__ . " => Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords()
    {
        $paths = [
            'admins/paths',
            'admins/roles',
            'admins/roles/details',
            'orders',
            'orders/details',
            'bulk-orders',
            'bulk-orders/details',
            'subscription/subscribers',
            'subscription/subscribers/details',
            'subscription/recharge',
            'subscription/cancellation',
            'ba',
            'ba/generateMO',
            'ba/generateBA',
            'users/customers',
            'users/customers/details',
            'referral',
            'referral/details',
            'subscription/pauseplan',
            'reports/master',
            'reports/appco',
            'reports/mo'
        ];
        $roles = [
            [
                'Superadmin', 'Perform all actions'
            ]
        ];
        $roledetails = [
            [
                '1', '1', '1', '1', '1', '1', '0', '1', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '2', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '3', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '4', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '5', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '6', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '7', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '8', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '9', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '10', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '11', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '12', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '13', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '14', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '15', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '16', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '17', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '18', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '19', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '20', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '21', '1'
            ],
            [
                '1', '1', '1', '1', '1', '1', '0', '22', '1'
            ]
        ];
        Paths::truncate();
        $this->command(__FUNCTION__ . " => paths record has not been found. Adding new paths....");
        foreach ($paths as $p) {
            $path = new Paths();
            $path->pathValue = $p;
            $path->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added paths records...");
        
        Roles::truncate();
        $this->command(__FUNCTION__ . " => roles record has not been found. Adding new roles....");
        foreach ($roles as $r) {
            $role = new Roles();
            $role->roleName = $r[0];
            $role->Description = $r[1];
            $role->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added roles records...");
        
        RoleDetails::truncate();
        $this->command(__FUNCTION__ . " => roledetails record has not been found. Adding new roledetails....");
        foreach ($roledetails as $rd) {
            $roledetail = new RoleDetails();
            $roledetail->canView = $rd[0];
            $roledetail->canCreate = $rd[1];
            $roledetail->canEdit = $rd[2];
            $roledetail->canDelete = $rd[3];
            $roledetail->canUpload = $rd[4];
            $roledetail->canDownload = $rd[5];
            $roledetail->canReactive = $rd[6];
            $roledetail->PathId = $rd[7];
            $roledetail->RoleId = $rd[8];
            $roledetail->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added roledetails records...");

        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
