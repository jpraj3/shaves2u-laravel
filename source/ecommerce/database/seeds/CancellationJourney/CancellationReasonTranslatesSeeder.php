<?php

use Illuminate\Database\Seeder;
use App\Models\CancellationJourney\CancellationReasonTranslates;
class CancellationReasonTranslatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $translates = [
            ['I didn’t know I was on a subscription.','I didn’t know I was on a subscription.',1,'EN',0,'option1',0,null,1],
            ['I had a problem with the product.','I had a problem with the product.',1,'EN',0,'option2',0,null,2],
            ['It’s too expensive.','It’s too expensive.',1,'EN',0,'option3',0,null,3],
            ['I don’t use it enough.','I don’t use it enough.',1,'EN',0,'option4',0,null,4],
            ['I prefer using an electric shaver.','I prefer using an electric shaver.',1,'EN',0,'option5',0,null,5],
            ['All I wanted was the Trial Kit.','All I wanted was the Trial Kit.',1,'EN',0,'option6',0,null,6],

            ['I had a problem with the product.','I had a problem with the product.',7,'EN',0,'option1',0,null,1],
            ['It’s too expensive.','It’s too expensive.',7,'EN',0,'option2',0,null,2],
            ['I don’t use it enough.','I don’t use it enough.',7,'EN',0,'option3',0,null,3],
            ['I prefer using an electric shaver.','I prefer using an electric shaver.',7,'EN',0,'option4',0,null,4],
            ['All I wanted was the Trial Kit.','All I wanted was the Trial Kit.',7,'EN',0,'option5',0,null,5],
            ['I didn’t know I was on a subscription.','I didn’t know I was on a subscription.',7,'EN',0,'option6',0,null,6],

            ['I didn’t know I was on a subscription.','I didn’t know I was on a subscription.',8,'EN',0,'option1',0,null,1],
            ['I had a problem with the product.','I had a problem with the product.',8,'EN',0,'option2',0,null,2],
            ['It’s too expensive.','It’s too expensive.',8,'EN',0,'option3',0,null,3],
            ['I don’t use it enough.','I don’t use it enough.',8,'EN',0,'option4',0,null,4],
            ['I prefer using an electric shaver.','I prefer using an electric shaver.',8,'EN',0,'option5',0,null,5],
            ['All I wanted was the Trial Kit.','All I wanted was the Trial Kit.',8,'EN',0,'option6',0,null,6],

            ['정기배송인지 몰랐어요.','I didn’t know I was on a subscription.',8,'KO',0,'option1',0,null,1],
            ['제품 불량이었어요.','I had a problem with the product.',8,'KO',0,'option2',0,null,2],
            ['너무 비싸요.','It’s too expensive.',8,'KO',0,'option3',0,null,3],
            ['자주 사용 안 해요.','I don’t use it enough.',8,'KO',0,'option4',0,null,4],
            ['전기 면도기를 더 선호합니다.','I prefer using an electric shaver.',8,'KO',0,'option5',0,null,5],
            ['체험상품만 쓰고싶었어요.','All I wanted was the Trial Kit.',8,'KO',0,'option6',0,null,6],

            ['The blades are too sharp.','The blades are too sharp.',1,'EN',0,'option2_1',1,2,1],
            ['The blades are too blunt or did not last.','The blades are too blunt or did not last.',1,'EN',0,'option2_2',1,2,2],

            ['The blades are too sharp.','The blades are too sharp.',7,'EN',0,'option2_1',1,7,1],
            ['The blades are too blunt or did not last.','The blades are too blunt or did not last.',7,'EN',0,'option2_2',1,7,2],

            ['The blades are too sharp.','The blades are too sharp.',8,'EN',0,'option2_1',1,15,1],
            ['The blades are too blunt or did not last.','The blades are too blunt or did not last.',8,'EN',0,'option2_2',1,15,2],

            ['날이 너무 날카로워요.','The blades are too sharp.',8,'KO',0,'option2_1',1,21,1],
            ['날이 너무 무디거나 절삭력이 금방 떨어져요.','The blades are too blunt or did not last.',8,'KO',0,'option2_2',1,21,2],

            ['I didn’t know I was on a subscription.','I didn’t know I was on a subscription.',2,'EN',0,'option1',0,null,1],
            ['I had a problem with the product.','I had a problem with the product.',2,'EN',0,'option2',0,null,2],
            ['It’s too expensive.','It’s too expensive.',2,'EN',0,'option3',0,null,3],
            ['I don’t use it enough.','I don’t use it enough.',2,'EN',0,'option4',0,null,4],
            ['I prefer using an electric shaver.','I prefer using an electric shaver.',2,'EN',0,'option5',0,null,5],
            ['All I wanted was the Trial Kit.','All I wanted was the Trial Kit.',2,'EN',0,'option6',0,null,6],
            ['The blades are too sharp.','The blades are too sharp.',2,'EN',0,'option1',1,35,1],
            ['The blades are too blunt or did not last.','The blades are too blunt or did not last.',2,'EN',0,'option2',1,35,2],

            ['I prefer another brand.','I prefer another brand.',1,'EN',0,'option7',0,null,7],
            ['I prefer another brand.','I prefer another brand.',7,'EN',0,'option7',0,null,7],
            ['I prefer another brand.','I prefer another brand.',2,'EN',0,'option7',0,null,7],
            ['다른 브랜드 제품이 더 좋아요..','I prefer another brand.',8,'KO',0,'option7',0,null,7],

            ['我不曉得這是個配套','I didn’t know I was on a subscription.',9,'ZH-TW',0,'option1',0,null,1],
            ['我對你們的產品有不好的體驗','I had a problem with the product.',9,'ZH-TW',0,'option2',0,null,2],
            ['價格太昂貴了','It’s too expensive.',9,'ZH-TW',0,'option3',0,null,3],
            ['我不常用','I don’t use it enough.',9,'ZH-TW',0,'option4',0,null,4],
            ['我更喜歡使用電動刮鬍刀','I prefer using an electric shaver.',9,'ZH-TW',0,'option5',0,null,5],
            ['我只想要試用禮盒','All I wanted was the Trial Kit.',9,'ZH-TW',0,'option6',0,null,6],
            ['刮鬍刀太鋒利了','The blades are too sharp.',9,'ZH-TW',0,'option2_1',1,47,1],
            ['我較傾向於其他牌子','The blades are too blunt or did not last.',9,'ZH-TW',0,'option2_2',1,47,2],

            ['我不曉得這是個配套。','I didn’t know I was on a subscription.',2,'ZH-HK',0,'option1',0,null,1],
            ['我對你們的產品有不好的體驗','I had a problem with the product.',2,'ZH-HK',0,'option2',0,null,2],
            ['價格太昂貴了。','It’s too expensive.',2,'ZH-HK',0,'option3',0,null,3],
            ['我不常用。','I don’t use it enough.',2,'ZH-HK',0,'option4',0,null,4],
            ['我更喜歡使用電動刮鬍刀。','I prefer using an electric shaver.',2,'ZH-HK',0,'option5',0,null,5],
            ['我只想要試用禮盒','All I wanted was the Trial Kit.',2,'ZH-HK',0,'option6',0,null,6],
            ['刮鬍刀太鋒利了。','The blades are too sharp.',2,'ZH-HK',0,'option2_1',1,56,1],
            ['刮鬍刀不鋒利亦不持久耐用。','The blades are too blunt or did not last.',2,'ZH-HK',0,'option2_2',1,56,2],
            ['I prefer another brand.','I prefer another brand.',2,'ZH-HK',0,'option7',0,null,7]

        ];
        $this->command(__FUNCTION__ . " Truncating data...");
        CancellationReasonTranslates::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($translates as $trans) {
            $reasontranslates = new CancellationReasonTranslates();
            $reasontranslates->content = $trans[0];
            $reasontranslates->defaultContent = $trans[1];
            $reasontranslates->countryId = $trans[2];
            $reasontranslates->langCode = $trans[3];
            $reasontranslates->isHidden = $trans[4];
            $reasontranslates->viewName = $trans[5];
            $reasontranslates->modalLevel = $trans[6];
            $reasontranslates->parentId = $trans[7];
            $reasontranslates->orderNumber = $trans[8];
            $reasontranslates->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
