<?php

use Illuminate\Database\Seeder;
use App\Models\Ecommerce\User;

class UserTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " => Running...");

        $now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords("user", $now);

        $this->command(__CLASS__ . " => Done...");
    }

    public function addRecords($type, $now)
    {
        $user = [
            [
                1,
                'superuser@shaves2u.com',
                '$2y$10$DLcww.BCiIWfKbO.0x.sAevvgAbPL31rDupqCmIdgdW8ev23nqmqG',
                'test',
                'test',
                'en',
                'yyyy/MM/dd',
                '[]', 1, 1, now(), now(), 1, 0, 0, 0, 0
            ]
        ];

        $this->command(__FUNCTION__ . " => user record has not been found. Adding new user....");
        foreach ($user as $u) {
            $user = new user();
            $user->id = $u[0];
            $user->email = $u[1];
            $user->password = $u[2];
            $user->firstName = $u[3];
            $user->lastName = $u[4];
            $user->defaultLanguage = $u[5];
            $user->defaultDateFormat = $u[6];
            $user->notificationSettings = $u[7];
            $user->isActive = $u[8];
            $user->hasReceiveOffer = $u[9];
            $user->CountryId = $u[12];
            $user->isGuest = $u[13];
            $user->Email_Sub = $u[14];
            $user->Sms_Sub = $u[15];
            $user->HKG_Marketing_Sub = $u[16];
            $user->created_at = $u[10];
            $user->updated_at = $u[11];
            $user->save();
        }
        $this->command(__FUNCTION__ . " => ===== Successfully added user records...");
        $this->command("exit " . __FUNCTION__ . " function");
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
