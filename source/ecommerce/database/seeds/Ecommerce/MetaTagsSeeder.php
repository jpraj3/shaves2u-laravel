<?php

use App\Models\Ecommerce\MetaTags;
use Illuminate\Database\Seeder;

class MetaTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $metaDatas = [
            ['1', 'EN', '/en-my', 'Shaves2U MY | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/my', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
            ['2', 'EN', '/en-hk', 'Shaves2U HK | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/hk', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
            ['2', 'ZH-HK', '/zh-hk', 'Shaves2U ZH-HK | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/hk', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
            ['7', 'EN', '/en-sg', 'Shaves2U SG | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/sg', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
            ['8', 'KO', '/ko-kr', 'Shaves2U KR | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/kr', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
            ['9', 'ZH-TW', '/zh-tw', 'Shaves2U ZH-TW | The Perfect Shave for the Everyday Man', 'Shop for a quality razors, shavers, shaving cream and subscribe shave plans for men and women. Sign up to get your free trial now with Shaves2U.', 'Shaves2U', 'https://shaves2u.com/tw', 'Stay sharp with a Shave Plan. Subscribe now to shave and save. Get started now with 3, 5 or even 6-blade razors for that perfect shave.', 'https://shaves2u.com/assets/images/opengraph_shaves2u_new.jpg', 'website', 'index'],
        ];

        $this->command(__FUNCTION__ . " Truncating data...");
        App\Models\Ecommerce\MetaTags::truncate();
        $this->command(__FUNCTION__ . " Begin adding Records");

        foreach ($metaDatas as $metaData) {
            $metaDatas = new MetaTags();
            $metaDatas->CountryId = $metaData[0];
            $metaDatas->langCode = $metaData[1];
            $metaDatas->pageUrl = $metaData[2];
            $metaDatas->metatitle = $metaData[3];
            $metaDatas->metaDescription = $metaData[4];
            $metaDatas->ogTitle = $metaData[5];
            $metaDatas->ogUrl = $metaData[6];
            $metaDatas->ogDescription = $metaData[7];
            $metaDatas->ogImage = $metaData[8];
            $metaDatas->ogType = $metaData[9];
            $metaDatas->pageType = $metaData[10];
            $metaDatas->pageId = null;
            $metaDatas->save();
        }
    }
    public function command($msg)
    {
        $this->command->info($msg);
    }
}
