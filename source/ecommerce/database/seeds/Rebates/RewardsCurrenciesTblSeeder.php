<?php

use App\Models\Rebates\RewardsCurrency;
use Illuminate\Database\Seeder;

class RewardsCurrenciesTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->now = Carbon\Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $rows = [
            ['1', '20', '1'],
            ['2', '10', '7'],
            ['3', '10000', '8'],
            ['4', '50', '2'],
            ['5', '250', '9'],
        ];

        $this->command(__FUNCTION__ . " RewardsCurrency | Truncating data...");
        RewardsCurrency::truncate();
        $this->command(__FUNCTION__ . " RewardsCurrency | Begin adding Records");

        foreach ($rows as $row) {
            $rewardscurrencies = new RewardsCurrency();
            $rewardscurrencies->rewardtype = $row[0];
            $rewardscurrencies->value = $row[1];
            $rewardscurrencies->CountryId = $row[2];
            $rewardscurrencies->save();
        }
    }

    public function command($msg)
    {
        $this->command->info($msg);
    }
}
