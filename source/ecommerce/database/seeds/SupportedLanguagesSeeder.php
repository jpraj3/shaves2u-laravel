<?php

use Illuminate\Database\Seeder;
use App\Models\GeoLocation\SupportedLang;

class SupportedLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command(__CLASS__ . " Running...");

        $this->addRecords();

        $this->command(__CLASS__ . " Done...");
    }
    public function addRecords()
    {
        $supportedLanguages = [
            ['ZH', '中文', '2019-04-29 08:33:47', '2019-05-02 12:37:27', 2],
        ];
        foreach ( $supportedLanguages as $supportedLanguage) {
            $supportedLanguages = new SupportedLang();
            $supportedLanguages->languageCode = $supportedLanguage[0];
            $supportedLanguages->languageName = $supportedLanguage[1];
            $supportedLanguages->createdAt = $supportedLanguage[2];
            $supportedLanguages->updatedAt = $supportedLanguage[3];
            $supportedLanguages->CountryId = $supportedLanguage[4];
            $supportedLanguages->save();
        }
    }
    public function command($msg)
    {
        $this->command->info($msg);
    }
}
