<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePausePlanHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pause_plan_histories');
        Schema::create('pause_plan_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscriptionIds');
            $table->date('originaldate');
            $table->date('resumedate');
            $table->integer('pausemonth');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pause_plan_histories');
    }
}
