<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('PlanId');
            $table->integer('ProductCountryId');
            $table->integer('qty')->default('1');
            $table->string('cycle')->nullable()->default('all'); // all = every cycle , 1 = first cycle
            $table->boolean('isAnnual')->default('0');
            $table->integer('duration')->nullable();
            $table->integer('totalCycle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_details');
    }
}
