<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bannerUrl', 200);
            $table->string('bannerAlt', 256)->nullable();
            $table->string('hashTag', 50);
            $table->integer('views')->nullable();
            $table->string('title', 256);
            $table->text('content', 65535);
            $table->integer('postBy')->nullable();
            $table->integer('ArticleTypeId')->nullable();
            $table->tinyInteger('isFeatured')->default(0);
            $table->string('URL', 200)->nullable();
            $table->string('SEO_title', 100)->nullable();
            $table->text('META_description', 65535);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
