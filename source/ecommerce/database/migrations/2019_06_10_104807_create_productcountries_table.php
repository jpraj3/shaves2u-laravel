<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductcountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productcountries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->decimal('basedPrice', 10);
			$table->decimal('sellPrice', 10);
			$table->integer('qty')->nullable()->default(0);
			$table->integer('maxQty')->nullable()->default(0);
			$table->boolean('isOnline')->nullable()->default(1);
			$table->boolean('isOffline')->nullable()->default(1);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('ProductId')->nullable()->index('ProductId');
			$table->integer('order')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productcountries');
	}

}
