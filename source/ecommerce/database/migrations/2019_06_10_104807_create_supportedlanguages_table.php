<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupportedlanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supportedlanguages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('languageCode', 2);
			$table->string('languageName', 50)->nullable();
			$table->dateTime('createdAt');
			$table->dateTime('updatedAt');
			$table->integer('CountryId')->nullable()->index('CountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supportedlanguages');
	}

}
