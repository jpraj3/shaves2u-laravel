<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMagazinetypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('magazinetypes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('type', 10);
			$table->string('langCode', 5)->default('EN');
			$table->integer('CountryId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('magazinetypes');
	}

}
