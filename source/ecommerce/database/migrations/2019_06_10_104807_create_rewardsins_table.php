<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRewardsinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rewardsins', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('CountryId');
			$table->integer('UserId');
			$table->decimal('value', 10);
			$table->integer('rewardscurrencyId')->nullable();
			$table->integer('OrderId')->nullable();
			$table->dateTime('created_at')->nullable();
			$table->dateTime('updated_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rewardsins');
	}

}
