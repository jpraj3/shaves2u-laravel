<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cartdetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('qty')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CartId')->nullable()->index('CartId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
			$table->integer('PlanCountryId')->nullable()->index('PlanCountryId');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('SkuId')->nullable()->index('SkuId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cartdetails');
	}

}
