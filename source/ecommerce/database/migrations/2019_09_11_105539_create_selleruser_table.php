<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sales_agents');
        Schema::dropIfExists('sellers');
        Schema::dropIfExists('sellerusers');
        Schema::create('sellerusers', function (Blueprint $table) {
            // $table->bigIncrements('id', true);
            $table->integer('id', true);
            $table->string('badgeId', 10)->unique('badgeId');
            $table->string('icNumber', 20);
            $table->string('agentName', 200);
            $table->date('startDate');
            $table->string('campaign', 50);
            $table->string('email', 50);
            $table->date('latestUpdatedDate')->nullable();
            $table->dateTime('created_at');
			$table->dateTime('updated_at');
            $table->integer('CountryId')->nullable()->index('CountryId');
            $table->boolean('isActive')->nullable()->default(1);
            $table->enum('channelType', array('Event', 'Streets', 'B2B', 'RES', 'Bazar', 'Truck', 'Roadshow'))->nullable();
            $table->text('eventLocationCode', 65535)->nullable();
            $table->integer('MarketingOfficeId')->nullable();
            $table->dateTime('lastLoginAt')->nullable();
            $table->string('api_token', 80)->unique()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_agent');
        Schema::dropIfExists('sellers');
        Schema::dropIfExists('sellerusers');
    }
}
