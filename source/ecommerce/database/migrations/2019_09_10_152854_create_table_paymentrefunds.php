<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentrefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentrefunds', function (Blueprint $table) {
			$table->integer('id', true);
			$table->decimal('amount', 10)->nullable();
			$table->decimal('adjustment', 10)->nullable();
			$table->boolean('partial_refund')->default(false);
			$table->integer('OrderId')->nullable();
			$table->integer('SubscriptionId')->nullable();
			$table->integer('CardId')->nullable();
			$table->integer('UserId')->nullable();
			$table->text('reason', 65536)->nullable();
			$table->string('type', 50);
			$table->string('paymentType', 50);
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentrefunds');
    }
}
