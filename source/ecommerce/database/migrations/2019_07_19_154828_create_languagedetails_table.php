<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languagedetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('CountryId');
            $table->string('mainlanguageCode',100);
            $table->string('sublanguageCode',100);
            $table->string('sublanguageName', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languagedetails');
    }
}
