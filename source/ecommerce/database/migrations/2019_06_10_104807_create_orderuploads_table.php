<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderuploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderuploads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('doFileName', 50)->nullable();
			$table->string('wareHouseFileName', 50)->nullable();
			$table->string('orderNumber', 50)->nullable();
			$table->integer('orderId')->unsigned()->nullable();
			$table->integer('bulkOrderId')->unsigned()->nullable();
			$table->string('sku', 64)->nullable();
			$table->integer('qty')->nullable()->default(0);
			$table->string('deliveryId', 30)->nullable();
			$table->string('carrierAgent', 30)->nullable();
			$table->enum('status', array('Pending','On Hold','Payment Received','Processing','Delivering','Completed','Canceled'))->nullable()->default('On Hold');
			$table->string('region', 5)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderuploads');
	}

}
