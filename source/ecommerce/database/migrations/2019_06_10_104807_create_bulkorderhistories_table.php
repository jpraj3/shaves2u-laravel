<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBulkorderhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bulkorderhistories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('message', 300);
			$table->boolean('isRemark')->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('BulkOrderId')->unsigned()->nullable()->index('BulkOrderId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bulkorderhistories');
	}

}
