<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metatags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('CountryId');
            $table->string('metatitle', 200);
            $table->string('metaDescription', 200);
            $table->string('ogTitle', 200);
            $table->string('ogUrl', 200);
            $table->string('ogDescription', 200);
            $table->string('ogImage', 200);
            $table->string('ogType', 200);
            $table->string('pageType', 200);
            $table->string('pageId', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metatags');
    }
}
