<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationFollowUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_follow_up', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('SubscriptionId');
            $table->integer('UserId');
            $table->integer('CancelEdm1')->default(0);
            $table->integer('CancelEdm2')->default(0);
            $table->integer('CancelEdm3')->default(0);
            $table->integer('CancelEdm4')->default(0);
            $table->integer('CancelEdm5')->default(0);
            $table->date('ReminderDate')->nullable();
            $table->integer('ReminderStatus')->default(0);
            $table->integer('NextJourneyStatus')->nullable();
            $table->text('JourneyDesc')->nullable();
            $table->integer('Status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_follow_up');
    }
}
