<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsWithTrialKitToSubscriptionsProductaddon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions_productaddon', function (Blueprint $table) {
            $table->integer('isWithTrialKit')->after('totalCycle')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions_productaddon', function (Blueprint $table) {
            //
        });
    }
}
