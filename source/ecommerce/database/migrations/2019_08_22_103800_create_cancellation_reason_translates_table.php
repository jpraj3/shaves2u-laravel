<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationReasonTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_reason_translates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('content',1000);
            $table->integer('countryId');
            $table->string('langCode',20);
            $table->integer('isHidden')->default(0);
            $table->string('viewName',50)->nullable();
            $table->integer('modalLevel')->default(0);
            $table->integer('parentId')->nullable();
            $table->integer('orderNumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_reason_translates');
    }
}
