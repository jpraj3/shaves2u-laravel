<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRechargehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rechargehistories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscriptionId');
            $table->integer('adminId');
            $table->string('adminEmail', 50)->nullable();
            $table->string('status', 100);
            $table->string('reactiveCancelReason', 250)->nullable();
            $table->dateTime('reactiveCancelDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rechargehistories');
    }
}
