<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanFrequencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_frequency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('duration');
            $table->enum('durationType', array('month','year'))->default('month');
            $table->string('durationName',100)->nullable();
            $table->string('checkout_image_url',200)->nullable();
            $table->string('checkout_duration_text',200)->nullable();
            $table->string('checkout_details_text',200)->nullable();
            $table->string('checkout_selected_details_text',200)->nullable();
            $table->integer('showInCheckoutPage')->default(0);
            $table->integer('planCategoryId');
            $table->integer('countryId');
            $table->string('langCode',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_frequency');
    }
}
