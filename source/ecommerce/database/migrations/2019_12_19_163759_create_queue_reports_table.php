<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queue_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 200)->default('-');
            $table->enum('status', array('Pending','Processing','Cancelled','Failure','Finished'))->default('Pending');
            $table->integer('fileuploadsId')->nullable()->index('fileuploadsId');
            $table->integer('total')->default(0);
            $table->integer('current')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue_reports');
    }
}
