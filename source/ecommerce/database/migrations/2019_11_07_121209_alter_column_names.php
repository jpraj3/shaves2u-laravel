<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supportedlanguages', function (Blueprint $table) {
            $table->renameColumn('createdAt', 'created_at');
            $table->renameColumn('updatedAt', 'updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supportedlanguages', function (Blueprint $table) {
            $table->renameColumn('createdAt', 'created_at');
            $table->renameColumn('updatedAt', 'updated_at');
        });
    }
}
