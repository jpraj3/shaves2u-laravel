<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBulkordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bulkorders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('status', array('Pending','Payment Failure','Payment Received','Processing','Delivering','Completed','Cancelled','Returned'))->nullable()->default('Pending');
			$table->string('deliveryId', 30)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('phone', 30)->nullable();
			$table->string('fullName', 200)->nullable();
			$table->string('SSN', 50)->nullable();
			$table->enum('paymentType', array('stripe','iPay88','cash'))->nullable();
			$table->string('promoCode', 50)->nullable();
			$table->text('subscriptionIds', 65535)->nullable();
			$table->integer('taxRate')->nullable()->default(0);
			$table->string('taxName', 20)->nullable()->default('GST');
			$table->string('carrierKey', 15)->nullable();
			$table->string('carrierAgent', 30)->nullable();
			$table->string('vendor', 50)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('DeliveryAddressId')->nullable()->index('DeliveryAddressId');
			$table->integer('BillingAddressId')->nullable()->index('BillingAddressId');
			$table->string('taxInvoiceNo', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bulkorders');
	}

}
