<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productbundledetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productbundledetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ProductCountryId');
            $table->string('ProductBundleId');
            $table->string('PromotionId');
            $table->string('image_url');
            $table->integer('qty');
            $table->datetime('startAt');
            $table->datetime('expireAt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productbundledetails');
    }
}
