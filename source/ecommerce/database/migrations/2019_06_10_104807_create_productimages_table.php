<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productimages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url', 200);
			$table->boolean('isDefault')->nullable()->default(0);
			$table->integer('ProductId')->nullable()->index('ProductId')->default(NULL);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productimages');
	}

}
