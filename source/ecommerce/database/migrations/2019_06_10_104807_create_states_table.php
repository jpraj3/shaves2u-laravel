<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('states', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 50);
			$table->boolean('isDefault')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('states');
	}

}
