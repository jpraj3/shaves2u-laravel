<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_audits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email_key',100)->nullable();
            $table->string('email_type',100)->nullable();
            $table->text('email_data')->nullable();
            $table->integer('UserId')->nullable();
            $table->integer('SubscriptionId')->nullable();
            $table->integer('OrderId')->nullable();
            $table->text('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_audits');
    }
}
