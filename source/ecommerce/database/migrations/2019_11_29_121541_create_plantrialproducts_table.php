<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTrialProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantrialproducts', function (Blueprint $table) {
            $table->integer('id', true);
			$table->integer('qty')->nullable()->default(0);
			$table->integer('PlanId')->nullable()->index('PlanId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantrialproducts');
    }
}
