<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSalesReportColumnStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salereports', function(Blueprint $table)
        {
            $table->dropColumn('states');
        });
        Schema::table('salereports', function (Blueprint $table) {
            $table->enum('status', array('Awaiting Payment','Payment Received','Processing','Delivering','Completed','Canceled','Payment Failure','Returned'))->nullable()->default('Awaiting Payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salereports', function (Blueprint $table) {
            //
        });
    }
}
