<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotioncodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',50);
            $table->tinyInteger('isValid')->default(1);
            $table->string('email',100)->nullable();
            $table->integer('PromotionId')->nullable();
            $table->integer('promotion_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotioncodes');
    }
}
