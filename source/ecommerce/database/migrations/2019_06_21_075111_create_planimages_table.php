<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planimages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url', 200);
			$table->boolean('isDefault')->nullable()->default(0);
			$table->integer('PlanId')->nullable()->index('PlanId')->default(NULL);
          //  $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planimages');
    }
}
