<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Audits', function (Blueprint $table) {
            // $table->bigIncrements('id', true);
            $table->integer('id', true);
            $table->string('user_type', 255);
            $table->integer('user_id');
            $table->string('event', 255);
            $table->string('auditable_type', 255);
            $table->text('audit_title', 65535)->nullable();
            $table->text('audit_description', 65535)->nullable();
            $table->text('old_values', 65535)->nullable();
            $table->text('new_values', 65535)->nullable();
            $table->string('ip_address', 45);
            $table->string('url', 255);
            $table->string('user_agent', 255);
            $table->timestamps();
        });
    }
}
