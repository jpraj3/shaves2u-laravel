<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToMetaTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metatags', function (Blueprint $table) {
            $table->string('langCode', 10)->after('CountryId');
            $table->string('pageUrl', 255)->after('langCode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metatags', function (Blueprint $table) {
            //
        });
    }
}
