<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChargejobChecking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chargejoblist', function (Blueprint $table) {
            $table->integer('id', true);
            $table->dateTime('charge_date')->nullable();
            $table->integer('total_to_charge')->nullable();
            $table->string('JobType')->nullable();
            $table->integer('subscriptionId');
            $table->string('email')->nullable();
            $table->integer('CountryId');
            $table->string('status')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chargejoblist', function (Blueprint $table) {
            //
        });
    }
}
