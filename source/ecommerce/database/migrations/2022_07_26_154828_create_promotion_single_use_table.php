<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionSingleUseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_single_use', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('PromotionId')->nullable();
            $table->date('startDate');
            $table->date('endDate')->nullable();
            $table->integer('UserId')->nullable();
            $table->integer('AllowedUsage')->nullable();
            $table->integer('isUsed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_single_use');
    }
}
