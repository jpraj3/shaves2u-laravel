<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('status', array('Pending','Payment Received','Processing','Delivering','Completed','Cancelled','Payment Failure','Returned'))->nullable()->default('Pending');
			$table->enum('payment_status', array('Charged','Refunded','Rejected','Partially Refund','Adjustment'))->nullable();
			$table->string('deliveryId', 30)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('phone', 30)->nullable();
			$table->string('fullName', 200)->nullable();
			$table->string('SSN', 50)->nullable();
			$table->enum('paymentType', array('stripe','iPay88','nicepay','cash'))->nullable();
			$table->string('promoCode', 50)->nullable();
			$table->text('subscriptionIds', 65535)->nullable();
			$table->integer('taxRate')->nullable()->default(0);
			$table->string('taxName', 20)->nullable()->default('GST');
			$table->string('carrierKey', 15)->nullable();
			$table->string('carrierAgent', 30)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->boolean('isBulkCreated')->nullable()->default(0);
			$table->string('vendor', 50)->nullable();
			$table->integer('DeliveryAddressId')->nullable()->index('DeliveryAddressId');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('BillingAddressId')->nullable()->index('BillingAddressId');
			$table->integer('PromotionId')->nullable()->index('PromotionId');
			$table->integer('UserId')->nullable()->index('UserId');
			$table->integer('SellerUserId')->nullable()->index('SellerUserId');
			$table->string('source', 50)->nullable()->default('direct');
			$table->string('medium', 50)->nullable()->default('none');
			$table->string('campaign', 50)->nullable()->default('none');
			$table->string('term', 50)->nullable()->default('none');
			$table->string('content', 50)->nullable()->default('none');
			$table->string('taxInvoiceNo', 20)->nullable();
			$table->enum('channelType', array('Event','Streets','B2B','RES','Bazar','Truck','Roadshow'))->nullable();
			$table->text('eventLocationCode', 65535)->nullable();
			$table->integer('MarketingOfficeId')->nullable();
			$table->boolean('isDirectTrial')->nullable()->default(0);
			$table->boolean('isSubsequentOrder')->nullable()->default(0);
			$table->string('imp_uid', 50)->nullable();
			$table->string('merchant_uid', 50)->nullable();
			$table->string('pymt_intent', 255)->nullable();
			$table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
