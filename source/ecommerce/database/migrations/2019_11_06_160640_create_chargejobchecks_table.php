<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargejobchecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chargejobchecks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscriptionIds');
            $table->integer('UserId');
            $table->string('email', 255)->nullable();
            $table->integer('CountryId');
            $table->string('JobType', 255)->nullable();
            $table->tinyInteger('ReferralDiscount')->default(0);
            $table->tinyInteger('ChargeFunction')->default(0);
            $table->tinyInteger('CreateReceiptSub')->default(0);
            $table->tinyInteger('CreateOrder')->default(0);
            $table->tinyInteger('UpdateReceiptSub')->default(0);
            $table->text('ErrorOccur', 65535)->nullable();
            $table->tinyInteger('ErrorCheck')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chargejobchecks');
    }
}
