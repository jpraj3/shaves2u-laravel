<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('email', 100)->unique('email');
			$table->string('password', 255)->nullable();
			$table->string('firstName', 100)->nullable();
			$table->string('lastName', 100)->nullable();
			$table->string('socialId', 50)->nullable();
			$table->date('birthday')->nullable();
			$table->enum('gender', array('male','female'))->nullable()->default('male');
			$table->string('SSN', 50)->nullable();
			$table->string('NRIC', 15)->nullable();
			$table->string('phoneHome', 30)->nullable();
			$table->string('phoneOffice', 30)->nullable();
			$table->string('phone', 30)->nullable();
			$table->string('defaultLanguage', 5)->nullable()->default('en');
			$table->string('defaultDateFormat', 20)->nullable()->default('yyyy/MM/dd');
			$table->string('avatarUrl', 200)->nullable();
			$table->string('notificationSettings')->nullable()->default('[]');
			$table->boolean('isActive')->nullable()->default(0);
			$table->integer('defaultShipping')->nullable();
			$table->integer('defaultBilling')->nullable();
			$table->boolean('hasReceiveOffer')->nullable()->default(1);
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('NotificationTypeId')->nullable()->index('NotificationTypeId');
			$table->string('badgeId', 10)->nullable();
			$table->boolean('isGuest')->nullable()->default(0);
			$table->integer('UserTypeId')->nullable()->index('UserTypeId');
			$table->integer('IsSG_old')->nullable();
			$table->string('createdBy', 50)->nullable();
			$table->string('updateBy', 50)->nullable();
			$table->boolean('Email_Sub')->nullable()->default(0);
			$table->boolean('Sms_Sub')->nullable()->default(0);
			$table->integer('referralId')->nullable();
			$table->string('inviteCode', 50)->nullable();
			$table->boolean('HKG_Marketing_Sub')->nullable()->default(0);
			$table->string('remember_token',255)->nullable();
			$table->integer('welcome_edm')->default(0);
			$table->dateTime('welcome_edm_time')->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('registered_at')->nullable();
			$table->dateTime('last_login_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
