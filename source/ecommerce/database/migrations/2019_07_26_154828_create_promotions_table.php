<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('discount')->default(0);
            $table->string('bannerUrl',200)->nullable();
            $table->string('emailTemplateId',50)->nullable();
            $table->text('emailTemplateOptions', 65535)->nullable();
            $table->date('expiredAt');
            $table->date('activeAt')->default('2017-10-26');
            $table->decimal('minSpend', 10)->nullable();
            $table->tinyInteger('allowMinSpendForTotalAmount')->default(0);
            $table->decimal('maxDiscount', 10)->nullable();
            $table->text('planIds', 65535)->nullable();
            $table->text('productCountryIds', 65535)->nullable();
            $table->text('planCountryIds', 65535)->nullable();
            $table->text('freeProductCountryIds', 65535)->nullable();
            $table->text('freeExistProductCountryIds', 65535)->nullable();
            $table->text('ProductBundleId', 65535)->nullable();
            $table->tinyInteger('isGeneric')->default(0);
            $table->integer('timePerUser')->default(1);
            $table->integer('totalUsage')->nullable();
            $table->text('appliedTo', 65535)->nullable();
            $table->tinyInteger('isFreeShipping')->default(0);
            $table->integer('CountryId')->nullable();
            $table->tinyInteger('isBaApp')->default(0);
            $table->tinyInteger('isOnline')->default(1);
            $table->tinyInteger('isOffline')->default(1);
			$table->enum('promotionType', array('Instant','Upcoming','Recurring'))->nullable()->default('Instant');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
