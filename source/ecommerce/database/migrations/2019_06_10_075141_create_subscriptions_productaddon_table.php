<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsProductAddonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {     // plan option 
        Schema::create('subscriptions_productaddon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscriptionId');
            $table->integer('ProductCountryId');
            $table->decimal('differentprice', 10)->nullable()->default(0.00); //if gt different price with the product
            $table->text('description', 65535)->nullable();
            $table->integer('qty')->default('1');
            $table->string('cycle')->nullable()->default('all'); // all = every cycle , 1 = first cycle
            $table->boolean('isAnnual')->default('0');
            $table->integer('duration')->nullable();
            $table->integer('totalCycle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions_productaddon');
    }
}
