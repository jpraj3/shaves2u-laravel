<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subscriptions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('nextDeliverDate')->nullable();
			$table->date('startDeliverDate')->nullable();
			$table->date('expiredDateAt')->nullable();
			$table->integer('currentDeliverNumber')->nullable()->default(0);
			$table->integer('totalDeliverTimes')->nullable();
			$table->date('nextChargeDate')->nullable();
			$table->integer('currentChargeNumber')->nullable()->default(0);
			$table->integer('totalChargeTimes')->nullable();
			$table->dateTime('recharge_date')->nullable();
			$table->boolean('total_recharge')->nullable()->default(0);
			$table->boolean('unrealizedCust')->nullable()->default(0);
			$table->decimal('pricePerCharge', 10);
			$table->dateTime('currentOrderTime')->nullable();
			$table->integer('currentOrderId')->nullable();
			$table->enum('status', array('Pending','Processing','Cancelled','Payment Failure','On Hold','Unrealized','Finished'))->nullable()->default('Pending');
			$table->string('fullname', 200)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('phone', 20)->nullable();
			$table->integer('discountPercent')->nullable()->default(0);
			$table->integer('qty')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CardId')->nullable()->index('CardId');
			$table->integer('PlanId')->nullable()->index('PlanId');
			$table->integer('SellerUserId')->nullable()->index('SellerUserId');
			$table->integer('SkuId')->nullable()->index('SkuId');
			$table->integer('UserId')->nullable()->index('UserId');
			$table->integer('DeliveryAddressId')->nullable()->index('DeliveryAddressId');
			$table->integer('BillingAddressId')->nullable()->index('BillingAddressId');
			$table->integer('PlanCountryId')->nullable()->index('PlanCountryId');
			$table->dateTime('lastDeliverydate')->nullable();
			$table->boolean('isTrial')->nullable()->default(0);
			$table->boolean('isCustom')->nullable()->default(0);
			$table->boolean('isCampaignCharge')->nullable()->default(0);
			$table->decimal('price', 10)->nullable();
			$table->date('convertTrial2Subs')->nullable();
			$table->boolean('hasSendReferralInvite')->nullable()->default(0);
			$table->boolean('hasSendFollowUp1')->nullable()->default(0);
			$table->boolean('hasSendFollowUp2')->nullable()->default(0);
			$table->boolean('hasSendFollowUp5Days')->nullable()->default(0);
			$table->boolean('hasSendCancelRequest')->nullable()->default(0);
			$table->boolean('hasFlag')->nullable();
			$table->boolean('hasRechargeFlag')->nullable();
			$table->boolean('isOffline')->nullable()->default(0);
			$table->enum('channelType', array('Event','Streets','B2B','RES','Bazar','Truck','Roadshow'))->nullable();
			$table->text('eventLocationCode', 65535)->nullable();
			$table->integer('MarketingOfficeId')->nullable();
			$table->text('cancellationReason', 65535)->nullable();
			$table->integer('CustomPlanId')->nullable();
			$table->integer('PlanOptionId')->nullable();
			$table->string('promoCode', 50)->nullable();
			$table->integer('promotionId')->nullable();
			$table->boolean('isDirectTrial')->nullable()->default(0);
			$table->string('imp_uid', 50)->nullable();
			$table->string('merchant_uid', 50)->nullable();
			$table->integer('temp_discountPercent')->nullable()->default(0);
			$table->integer('hasTriedCancel')->nullable()->default(0);
			$table->integer('hasMaximumTenure')->nullable()->default(0);
			$table->boolean('isAnnualDiscountIncludeAddon')->nullable()->default(0);
			$table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscriptions');
	}

}
