<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProducttypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('producttypes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 200);
			$table->dateTime('created_at')->nullable();
			$table->dateTime('updated_at')->nullable();
			$table->integer('order')->nullable()->default(0);
			$table->string('url', 200)->nullable();
			$table->enum('status', array('active','removed'))->nullable()->default('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('producttypes');
	}

}
