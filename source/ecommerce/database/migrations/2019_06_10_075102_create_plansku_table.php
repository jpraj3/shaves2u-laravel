<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanSKUTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { //plan
        Schema::create('plansku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku');
            $table->integer('rating')->default('0');
            $table->integer('isFeatured')->default('0');
            $table->string('status')->default('active');
            $table->string('slug');
            $table->integer('order')->default('0');
            $table->string('duration'); //PlanTypeId
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_s_k_u_s');
        Schema::dropIfExists('plansku');
    }
}
