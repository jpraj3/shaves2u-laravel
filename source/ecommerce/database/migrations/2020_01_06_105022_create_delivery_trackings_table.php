<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliverytrackings', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('OrderId', false)->nullable()->unsigned()->index('OrderId');
            $table->integer('BulkOrderId', false)->nullable()->unsigned()->index('BulkOrderId');
            $table->string('warehouse_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('deliveryId', 255)->nullable();
            $table->text('issue', 65535);
            $table->json('api_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverytrackings');
    }
}
