<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnusedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productvariants');
        Schema::drop('productvariantoptions');
        Schema::drop('productrelateds');
        Schema::drop('usedtokens');
        Schema::drop('productreviews');
        Schema::dropIfExists('planimages');
    }
}
