<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productbundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productbundles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bundle_name');
            $table->boolean('isOnline');
            $table->boolean('isOffline');
            $table->string('promotion');
            $table->string('PromotionId');
            $table->string('discountPercent');
            $table->string('bundle_price');
            $table->string('isActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productbundles');
    }
}
