<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryaddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deliveryaddresses', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('firstName', 100)->nullable();
			$table->string('lastName', 100)->nullable();
			$table->string('fullName', 200)->nullable();
			$table->string('contactNumber', 30)->nullable();
			$table->string('SSN', 50)->nullable();
			$table->string('state', 100)->nullable();
			$table->string('address', 150);
			$table->string('portalCode', 10);
			$table->string('city', 50);
			$table->string('company', 50)->nullable();
			$table->boolean('isRemoved')->nullable()->default(0);
			$table->boolean('isBulkAddress')->nullable()->default(0);
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('UserId')->nullable()->index('UserId');
			$table->integer('SellerUserId')->nullable()->index('SellerUserId');
			$table->string('createdBy', 50)->nullable();
			$table->string('updateBy', 50)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deliveryaddresses');
	}

}
