<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 5)->unique('Countries_code_unique');
			$table->string('codeIso', 5)->unique('Countries_codeIso_unique');
			$table->string('name', 20)->unique('Countries_name_unique');
			$table->string('currencyCode', 5);
			$table->string('currencyDisplay', 5);
			$table->string('defaultLang', 2);
			$table->string('callingCode', 5)->nullable();
			$table->boolean('isActive')->nullable()->default(1);
			$table->string('companyName', 50);
			$table->string('companyRegistrationNumber', 50);
			$table->string('companyAddress', 100);
			$table->integer('taxRate')->nullable()->default(0);
			$table->string('taxName', 20)->default('GST');
			$table->string('taxCode', 20)->nullable();
			$table->boolean('includedTaxToProduct')->nullable()->default(1);
			$table->string('taxNumber', 20);
			$table->string('email', 50)->nullable();
			$table->string('phone', 20)->nullable();
			$table->boolean('isBaEcommerce')->nullable()->default(0);
			$table->boolean('isBaSubscription')->nullable()->default(0);
			$table->boolean('isBaAlaCarte')->nullable()->default(0);
			$table->boolean('isBaStripe')->nullable()->default(0);
			$table->boolean('isWebEcommerce')->nullable()->default(0);
			$table->boolean('isWebSubscription')->nullable()->default(0);
			$table->boolean('isWebAlaCarte')->nullable()->default(0);
			$table->boolean('isWebStripe')->nullable()->default(0);
			$table->decimal('shippingMinAmount', 10)->nullable()->default(0.00);
			$table->decimal('shippingFee', 10)->nullable()->default(0.00);
			$table->decimal('shippingTrialFee', 10)->nullable()->default(0.00);
			$table->integer('order')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
