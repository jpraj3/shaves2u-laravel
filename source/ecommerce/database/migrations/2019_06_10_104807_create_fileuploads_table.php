<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFileuploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fileuploads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 200)->unique('name');
			$table->integer('isReport')->default(0);
			$table->string('reportType', 100)->nullable();
			$table->enum('status', array('Processing','Completed','Canceled'))->nullable()->default('Completed');
			$table->string('url', 400)->nullable();
			$table->integer('orderId')->unsigned()->nullable();
			$table->integer('bulkOrderId')->unsigned()->nullable();
			$table->boolean('isDownloaded')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('AdminId')->nullable()->index('AdminId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fileuploads');
	}

}
