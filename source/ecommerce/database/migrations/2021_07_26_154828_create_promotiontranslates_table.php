<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotiontranslates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',200);
            $table->text('description', 65535);
            $table->string('langCode', 2);
            $table->integer('PromotionId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotiontranslates');
    }
}
