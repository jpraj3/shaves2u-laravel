<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanCategoryTranslateAddTitleImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_category_translates', function (Blueprint $table) {
            $table->string('translate_title', 255)->nullable()->after('PlanCategoryId');
            $table->string('translate_subtitle', 255)->nullable()->after('translate_title');
            $table->string('translate_description', 255)->nullable()->after('translate_subtitle');
            $table->string('translate_url', 255)->nullable()->after('translate_description');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_category_translates', function (Blueprint $table) {
            //
        });
    }
}
