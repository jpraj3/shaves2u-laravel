<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummarySubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_subscribers', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('SubID', 200)->default('-');
            $table->string('UserID', 200)->default('-');
            // $table->string('SubscriptionId', 200)->default('-');
            $table->string('CustomerName', 200)->default('-');
            $table->string('Email', 200)->default('-');
            $table->string('Country', 200)->default('-');
            $table->string('Category', 200)->default('-');
            $table->string('MOCode', 200)->default('-');
            $table->string('BadgeID', 200)->default('-');
            $table->string('DeliveryAddress', 200)->default('-');
            $table->string('PlanType', 200)->default('-');
            $table->string('BladeType', 200)->default('-');
            $table->string('DeliveryInterval', 200)->default('-');
            $table->string('CassetteCycle', 200)->default('-');
            $table->string('CycleCount', 200)->default('-');
            $table->string('AfterShaveCream', 200)->default('-');
            $table->string('ShaveCream', 200)->default('-');
            $table->string('Pouch', 200)->default('-');
            $table->string('RubberHandle', 200)->default('-');
            $table->string('SignUp', 200)->default('-');
            $table->string('Conversion', 200)->default('-');
            $table->string('LastDelivery', 200)->default('-');
            $table->string('LastCharge', 200)->default('-');
            $table->string('NextCharge', 200)->default('-');
            $table->string('RechargeAttempt', 200)->default('-');
            $table->string('RechargeCount', 200)->default('-');
            $table->string('CancellationDate', 200)->default('-');
            $table->string('Status', 200)->default('-');
            $table->string('CancellationReason', 200)->default('-');
            $table->string('OtherReason', 200)->default('-');
            $table->string('PauseSubscription', 200)->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_subscribers');
    }
}
