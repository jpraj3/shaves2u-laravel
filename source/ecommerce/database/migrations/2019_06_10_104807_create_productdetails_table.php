<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productdetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->enum('type', array('left','center','right'))->nullable()->default('center');
			$table->string('imageUrl', 200);
			$table->text('title', 65535)->nullable();
			$table->text('details', 65535)->nullable();
			$table->string('langCode', 11);
			$table->integer('ProductId')->nullable()->index('ProductId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productdetails');
	}

}
