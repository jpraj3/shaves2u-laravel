<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTypeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producttypedetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ProductTypeId')->nullable()->index('ProductTypeId')->default(NULL);
            $table->integer('ProductId')->nullable()->index('ProductId')->default(NULL);
            $table->integer('Order')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producttypedetails');
    }
}
