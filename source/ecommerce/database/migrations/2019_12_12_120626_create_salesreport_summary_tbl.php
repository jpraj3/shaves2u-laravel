<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesreportSummaryTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_salesreport', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('OrderId', 200)->default('-');
            $table->string('BulkOrderId', 200)->default('-');
            $table->string('OrderNumber', 200)->default('-');
            $table->string('OrderStatus', 200)->default('-');
            $table->string('SaleDate', 200)->default('-');
            $table->string('CompletionDate', 200)->default('-');
            $table->string('Region', 200)->default('-');
            $table->string('Category', 200)->default('-');
            $table->string('MOCode', 200)->default('-');
            $table->string('BadgeId', 200)->default('-');
            $table->string('AgentName', 200)->default('-');
            $table->string('ChannelType', 200)->default('-');
            $table->string('Event/LocationCode', 200)->default('-');
            $table->string('DeliveryMethod', 200)->default('-');
            $table->string('TrackingNumber', 200)->default('-');
            $table->string('UserID', 200)->default('-');
            $table->string('CustomerName', 200)->default('-');
            $table->string('Email', 200)->default('-');
            $table->string('DeliveryAddress', 200)->default('-');
            $table->string('DeliveryPostcode', 200)->default('-');
            $table->string('DeliveryContactNumber', 200)->default('-');
            $table->string('BillingAddress', 200)->default('-');
            $table->string('BillingPostcode', 200)->default('-');
            $table->string('BillingContactNumber', 200)->default('-');
            $table->string('ProductCategory', 200)->default('-');
            $table->string('A1', 200)->default('-');
            $table->string('A2', 200)->default('-');
            $table->string('A2/2018', 200)->default('-');
            $table->string('A3', 200)->default('-');
            $table->string('A4-2017', 200)->default('-');
            $table->string('A5', 200)->default('-');
            $table->string('A5/2018', 200)->default('-');
            $table->string('ASK-2017', 200)->default('-');
            $table->string('ASK3/2018', 200)->default('-');
            $table->string('ASK5/2018', 200)->default('-');
            $table->string('ASK6/2018', 200)->default('-');
            $table->string('F5', 200)->default('-');
            $table->string('FK5/2019', 200)->default('-');
            $table->string('H1', 200)->default('-');
            $table->string('H1S3/2018', 200)->default('-');
            $table->string('H1S5/2018', 200)->default('-');
            $table->string('H1S6/2018', 200)->default('-');
            $table->string('H2', 200)->default('-');
            $table->string('H3', 200)->default('-');
            $table->string('H3S6/2018', 200)->default('-');
            $table->string('H3TK3/2018', 200)->default('-');
            $table->string('H3TK5/2018', 200)->default('-');
            $table->string('H3TK6/2018', 200)->default('-');
            $table->string('PF', 200)->default('-');
            $table->string('PS', 200)->default('-');
            $table->string('S3', 200)->default('-');
            $table->string('S3/2018', 200)->default('-');
            $table->string('S5', 200)->default('-');
            $table->string('S5/2018', 200)->default('-');
            $table->string('S6', 200)->default('-');
            $table->string('S6/2018', 200)->default('-');
            $table->string('TK3/2018', 200)->default('-');
            $table->string('TK5/2018', 200)->default('-');
            $table->string('TK6/2018', 200)->default('-');
            $table->string('POUCH/2019', 200)->default('-');
            $table->string('PTC-HDL', 200)->default('-');
            $table->string('UnitTotal', 200)->default('-');
            $table->string('PaymentType', 200)->default('-');
            $table->string('CardBrand', 200)->default('-');
            $table->string('CardType', 200)->default('-');
            $table->string('Currency', 200)->default('-');
            $table->string('PromoCode', 200)->default('-');
            $table->string('TotalPrice', 200)->default('-');
            $table->string('DiscountAmount', 200)->default('-');
            $table->string('CashRebate', 200)->default('-');
            $table->string('SubTotalPrice', 200)->default('-');
            $table->string('ProcessingFee', 200)->default('-');
            $table->string('GrandTotalWithTax', 200)->default('-');
            $table->string('GrandTotalWithoutTax', 200)->default('-');
            $table->string('TaxAmount', 200)->default('-');
            $table->string('PaymentStatus', 200)->default('-');
            $table->string('ChargeID', 200)->default('-');
            $table->string('TaxInvoicenumber', 200)->default('-');
            $table->string('Source', 200)->default('-');
            $table->string('Medium', 200)->default('-');
            $table->string('Campaign', 200)->default('-');
            $table->string('Term', 200)->default('-');
            $table->string('Content', 200)->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesreport_summary_tbl');
    }
}
