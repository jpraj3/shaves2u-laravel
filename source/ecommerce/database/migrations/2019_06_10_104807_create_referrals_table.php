<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referrals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('referralId');
			$table->integer('friendId');
			$table->integer('CountryId');
			$table->string('source', 20)->nullable();
			$table->integer('firstPurchase')->default(0);
			$table->integer('used')->default(0);
			$table->integer('usedAmount')->nullable()->default(0);
			$table->dateTime('usedTime')->nullable();
			$table->string('usedType', 20)->nullable();
			$table->integer('rewardId')->nullable();
			$table->integer('refereeDiscount')->nullable()->default(0);
			$table->dateTime('refereeDiscountApplied')->nullable();
			$table->enum('status', array('Active','Inactive','Redeemed'))->default('Inactive');
			$table->dateTime('created_at')->nullable();
			$table->dateTime('updated_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referrals');
	}

}
