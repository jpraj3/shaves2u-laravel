<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReactivehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactivehistories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscriptionId');
            $table->dateTime('recharge_date')->nullable();
            $table->tinyInteger('total_recharge')->nullable();
            $table->tinyInteger('unrealizedCust')->nullable();
            $table->string('cancelreason', 100)->nullable();
            $table->string('lastStatus', 100)->nullable();
            $table->dateTime('lastStatusDate')->nullable();
            $table->string('reason', 100)->nullable();
            $table->dateTime('canceldate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactivehistories');
    }
}
