<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->integer('id', true);
			$table->string('email', 50)->unique('email');
			$table->string('password', 255)->nullable();
			$table->string('firstName', 100);
			$table->string('lastName', 100);
			$table->string('staffId', 50)->nullable();
			$table->boolean('isActive')->nullable()->default(1);
			$table->boolean('viewAllCountry')->nullable()->default(0);
			$table->string('phone', 20)->nullable();
			$table->integer('AdminRoleId')->nullable()->index('AdminRoleId');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('RoleId')->nullable()->index('Admins_ibfk_3');
			$table->integer('MarketingOfficeId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
