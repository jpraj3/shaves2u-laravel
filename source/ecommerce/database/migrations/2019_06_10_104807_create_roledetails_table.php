<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoledetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roledetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('canView')->nullable()->default(0);
			$table->boolean('canCreate')->nullable()->default(0);
			$table->boolean('canEdit')->nullable()->default(0);
			$table->boolean('canDelete')->nullable()->default(0);
			$table->boolean('canUpload')->nullable()->default(0);
			$table->boolean('canDownload')->nullable()->default(0);
			$table->boolean('canReactive')->nullable()->default(0);
			$table->integer('PathId')->nullable()->index('PathId');
			$table->integer('RoleId')->nullable()->index('RoleId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roledetails');
	}

}
