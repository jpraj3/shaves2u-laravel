<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  //plancountries
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('price', 10)->default(0.00); //actualPrice and pricePerCharge
            $table->decimal('sellPrice', 10)->nullable();
            $table->decimal('trialPrice', 10)->nullable();
            $table->integer('discountPercent');
            $table->decimal('discountAmount'); //savePercent
            $table->text('description', 65535);
            $table->string('tax', 50)->nullable();
            $table->decimal('taxAmount', 10);
            $table->enum('plantype', array('trial','custom'));
            $table->boolean('isOnline')->default('1');
            $table->boolean('isOffline')->default('1');
            $table->integer('rating')->default('0');
            $table->integer('order')->default('0');
            $table->integer('PlanSkuId');
            $table->integer('CountryId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
