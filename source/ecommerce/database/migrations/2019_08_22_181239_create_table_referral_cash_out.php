<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReferralCashOut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referralcashouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100)->nullable();
            $table->string('email',100)->nullable();
            $table->integer('amount')->nullable();
            $table->enum('status', array('active','withdrawn','in_process'))->nullable()->default('active');
            $table->integer('UserId')->nullable();
            $table->integer('CountryId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referralcashouts', function (Blueprint $table) {
            Schema::dropIfExists('referralcashouts');
        });
    }
}
