<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subscriptionhistories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('message', 300);
			$table->text('detail', 65535)->nullable();
			$table->integer('subscriptionId')->index('subscriptionId_idx');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscriptionhistories');
	}

}
