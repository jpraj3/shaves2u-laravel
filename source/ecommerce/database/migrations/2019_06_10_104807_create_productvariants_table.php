<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductvariantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productvariants', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 30);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('ProductId')->nullable()->index('ProductId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productvariants');
	}

}
