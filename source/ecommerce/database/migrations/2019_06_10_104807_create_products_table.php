<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sku', 64)->unique('sku');
			$table->integer('rating')->nullable()->default(0);
			$table->boolean('isFeatured')->nullable()->default(0);
			$table->enum('status', array('active','removed'))->default('active');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('ProductTypeId')->nullable()->index('ProductTypeId');
			$table->string('slug', 50)->unique('slug_UNIQUE');
			$table->integer('order')->nullable()->default(0);
			$table->decimal('long', 10)->default(0.00);
			$table->decimal('width', 10)->default(0.00);
			$table->decimal('hight', 10)->default(0.00);
			$table->decimal('weight', 10)->default(0.00);
			$table->boolean('isFeaturedProduct')->nullable()->default(0);
			$table->boolean('isFeaturedAsk')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
