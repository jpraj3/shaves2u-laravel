<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBulkorderdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bulkorderdetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('qty')->nullable()->default(0);
			$table->decimal('price', 10)->nullable()->default(0.00);
			$table->string('currency', 5);
			$table->date('startDeliverDate')->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('BulkOrderId')->unsigned()->nullable()->index('BulkOrderId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
			$table->integer('PlanCountryId')->nullable()->index('PlanCountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bulkorderdetails');
	}

}
