<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationJourneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_journeys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email',200);
            $table->string('CancellationTranslatesId',50)->nullable();
            $table->string('OtherReason',100)->nullable();
            $table->integer('UserId');
            $table->integer('OrderId')->nullable();
            $table->integer('SubscriptionId');
            $table->string('planType',100)->nullable();
            $table->string('subscriptionName',200)->nullable();
            $table->integer('hasCancelled')->default(0)->nullable();
            $table->integer('hasChangedBlade')->default(0)->nullable();
            $table->string('modifiedBlade',200)->nullable();
            $table->integer('hasChangedFrequency')->default(0)->nullable();
            $table->string('modifiedFrequency',200)->nullable();
            $table->integer('hasGetFreeProduct')->default(0)->nullable();
            $table->integer('hasGetDiscount')->default(0)->nullable();
            $table->string('discountPercent',100)->nullable();
            $table->integer('hasPausedSubscription')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_journeys');
    }
}
