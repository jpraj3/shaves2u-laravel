<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignReactiveHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {     // plan option 
        Schema::create('campaign_reactive_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('UserId')->nullable()->index('UserId');
            $table->string('email', 50)->nullable();
            $table->integer('subscriptionId')->index('subscriptionId');
            $table->integer('CountryId')->nullable();
            $table->string('LanguageCode', 250)->nullable();
            $table->enum('status_Old', array('Pending','Processing','Cancelled','Payment Failure','On Hold','Unrealized','Finished'))->nullable();
            $table->enum('status_New', array('Pending','Processing','Cancelled','Payment Failure','On Hold','Unrealized','Finished'))->nullable();
            $table->integer('PlanId_Old')->nullable()->index('PlanId_Old');
            $table->integer('PlanId_New')->nullable()->index('PlanId_New');
            $table->text('AddOn_Old', 65535)->nullable();
            $table->text('AddOn_New', 65535)->nullable();
            $table->text('eventLocationCode', 65535)->nullable();
            $table->decimal('pricePerCharge_Old', 10)->nullable();
            $table->decimal('pricePerCharge_New', 10)->nullable();
            $table->integer('discountPercent')->nullable()->default(0);
            $table->boolean('isChangeCard')->nullable()->default(0);
            $table->dateTime('unrealized_Date');
            $table->date('nextDeliverDate_old')->nullable();
            $table->date('nextChargeDate_old')->nullable();
            $table->integer('currentCycle_old')->unsigned()->nullable();
			$table->dateTime('recharge_date_old')->nullable();
            $table->boolean('total_recharge_old')->nullable()->default(0);
            $table->date('nextDeliverDate_new')->nullable();
            $table->date('nextChargeDate_new')->nullable();
            $table->boolean('isChargeSuccess')->nullable()->default(0);
            $table->string('source', 50)->nullable()->default('direct');
			$table->string('medium', 50)->nullable()->default('none');
			$table->string('campaign', 50)->nullable()->default('none');
			$table->string('term', 50)->nullable()->default('none');
			$table->string('content', 50)->nullable()->default('none');
            $table->text('extra_info', 65535)->nullable();
            $table->string('campaign_type',250)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_reactive_histories');
    }
}
