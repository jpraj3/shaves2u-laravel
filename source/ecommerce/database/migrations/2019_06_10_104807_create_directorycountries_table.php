<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDirectorycountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('directorycountries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 5)->unique('code');
			$table->string('codeIso2', 5);
			$table->string('name', 200);
			$table->string('nativeName', 200);
			$table->string('currencyCode', 5);
			$table->string('languageCode', 2);
			$table->string('languageName', 50)->nullable();
			$table->text('flag', 65535)->nullable();
			$table->string('callingCode', 5)->nullable();
			$table->integer('CountryId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('directorycountries');
	}

}
