<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductrelatedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productrelateds', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->integer('ProductCountryId')->nullable();
			$table->integer('RelatedProductCountryId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productrelateds');
	}

}
