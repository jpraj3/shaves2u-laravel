<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cards', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('customerId', 50);
			$table->string('cardNumber', 4);
			$table->string('branchName', 20);
			$table->string('cardName', 200)->nullable();
			$table->string('expiredYear', 4);
			$table->string('expiredMonth', 2);
			$table->string('birth', 10)->nullable();
			$table->boolean('isDefault')->nullable()->default(1);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('UserId')->nullable()->index('UserId');
			$table->boolean('isRemoved')->default(0);
			$table->enum('type', array('website','baWeb'))->nullable()->default('website');
			$table->string('pymt_intent_tk', 255)->nullable();
			$table->string('pymt_intent_cp', 255)->nullable();
			$table->string('pymt_intent_ask', 255)->nullable();
			$table->string('pymt_intent_alacarte', 255)->nullable();
			$table->enum('risk_level', array('normal','elevated','highest'))->nullable()->default('normal');
			$table->enum('payment_method', array('stripe','nicepay','alipay','wechatpay','naverpay','kakaopay'))->nullable()->default('stripe');
			$table->integer('CountryId')->nullable()->index('Cards_ibfk_2');
			$table->string('createdBy', 50)->nullable();
			$table->string('updateBy', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cards');
	}

}
