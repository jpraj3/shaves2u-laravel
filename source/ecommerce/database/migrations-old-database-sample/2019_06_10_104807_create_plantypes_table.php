<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlantypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plantypes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 200)->unique('name');
			$table->integer('totalDeliverTimes')->nullable();
			$table->string('subsequentDeliverDuration', 15)->nullable()->default('1 months');
			$table->integer('totalChargeTimes')->nullable();
			$table->string('subsequentChargeDuration', 15)->nullable()->default('1 months');
			$table->string('prefix', 10)->default('');
			$table->integer('order')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plantypes');
	}

}
