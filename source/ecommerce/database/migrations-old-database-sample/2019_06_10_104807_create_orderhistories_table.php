<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderhistories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('message', 300);
			$table->boolean('isRemark')->nullable()->default(0);
			$table->integer('OrderId')->unsigned()->nullable()->index('OrderId_idx');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->text('cancellationReason', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orderhistories');
	}

}
