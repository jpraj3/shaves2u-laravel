<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('chargeId', 30)->nullable();
			$table->string('last4', 4)->nullable();
			$table->string('branchName', 20)->nullable();
			$table->string('expiredYear', 4)->nullable();
			$table->string('expiredMonth', 2)->nullable();
			$table->decimal('chargeFee', 10)->nullable()->default(0.00);
			$table->string('chargeCurrency', 5)->nullable();
			$table->decimal('subTotalPrice', 10)->nullable()->default(0.00);
			$table->decimal('discountAmount', 10)->nullable()->default(0.00);
			$table->decimal('shippingFee', 10)->nullable()->default(0.00);
			$table->decimal('taxAmount', 10)->nullable()->default(0.00);
			$table->decimal('totalPrice', 10)->nullable()->default(0.00);
			$table->string('currency', 5);
			$table->decimal('originPrice', 10)->nullable()->default(0.00);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('OrderId')->unsigned()->nullable()->index('OrderId');
			$table->integer('SubscriptionId')->nullable()->index('receipts_ibfk_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipts');
	}

}
