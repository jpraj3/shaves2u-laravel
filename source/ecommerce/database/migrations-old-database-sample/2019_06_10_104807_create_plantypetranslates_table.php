<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlantypetranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plantypetranslates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 200)->unique('name');
			$table->string('langCode', 2);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('PlanTypeId')->nullable()->index('PlanTypeId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plantypetranslates');
	}

}
