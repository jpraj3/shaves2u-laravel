<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('bannerUrl', 200);
			$table->string('bannerAlt', 256)->nullable();
			$table->string('hashTag', 50);
			$table->integer('views')->nullable()->default(0);
			$table->string('title', 256);
			$table->text('content', 65535);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('postBy')->nullable()->index('postBy');
			$table->integer('ArticleTypeId')->nullable()->index('ArticleTypeId');
			$table->boolean('isFeatured')->nullable()->default(0);
			$table->string('URL', 50)->nullable();
			$table->string('SEO_title', 100)->nullable();
			$table->text('META_description', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}
