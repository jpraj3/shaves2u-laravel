<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChargejobchecksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chargejobchecks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('subscriptionIds');
			$table->integer('UserId');
			$table->string('email')->nullable();
			$table->integer('CountryId');
			$table->string('JobType')->nullable();
			$table->boolean('ReferralDiscount')->default(0);
			$table->boolean('ChargeFunction')->default(0);
			$table->boolean('CreateReceiptSub')->default(0);
			$table->boolean('CreateOrder')->default(0);
			$table->boolean('UpdateReceiptSub')->default(0);
			$table->text('ErrorOccur', 65535)->nullable();
			$table->boolean('ErrorCheck')->default(0);
			$table->dateTime('created_at')->nullable();
			$table->dateTime('updated_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chargejobchecks');
	}

}
