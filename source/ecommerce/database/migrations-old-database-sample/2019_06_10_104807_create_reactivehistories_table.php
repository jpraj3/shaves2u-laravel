<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReactivehistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reactivehistories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('subscriptionId');
			$table->integer('adminId');
			$table->string('adminEmail', 50)->nullable();
			$table->string('status', 100);
			$table->string('reactiveCancelReason', 250)->nullable();
			$table->dateTime('reactiveCancelDate')->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reactivehistories');
	}

}
