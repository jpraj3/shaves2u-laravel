<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRechargehistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rechargehistories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('subscriptionId');
			$table->dateTime('recharge_date')->nullable();
			$table->boolean('total_recharge')->nullable();
			$table->boolean('unrealizedCust')->nullable();
			$table->string('cancelreason', 100)->nullable();
			$table->string('lastStatus', 100)->nullable();
			$table->dateTime('lastStatusDate')->nullable();
			$table->string('reason', 100)->nullable();
			$table->dateTime('canceldate')->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rechargehistories');
	}

}
