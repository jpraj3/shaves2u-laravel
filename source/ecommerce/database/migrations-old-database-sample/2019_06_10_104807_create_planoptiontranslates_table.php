<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanoptiontranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planoptiontranslates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('name', 65535);
			$table->string('langCode', 2);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('PlanOptionId')->nullable()->index('PlanOptionId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planoptiontranslates');
	}

}
