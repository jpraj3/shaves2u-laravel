<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomplandetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customplandetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('qty')->nullable()->default(0);
			$table->integer('CustomPlanId')->nullable()->index('CustomPlanId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customplandetails');
	}

}
