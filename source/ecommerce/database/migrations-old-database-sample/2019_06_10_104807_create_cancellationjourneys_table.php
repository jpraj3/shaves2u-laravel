<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCancellationjourneysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cancellationjourneys', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 250)->nullable();
			$table->string('MessagestranslatesId', 50)->nullable();
			$table->string('OtherReason', 250)->nullable();
			$table->integer('UserId')->index('UserId');
			$table->integer('OrderId')->unsigned()->nullable()->index('OrderId');
			$table->integer('SubscriptionId')->nullable()->index('SubscriptionId');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->string('planType', 50)->nullable();
			$table->string('subscriptionName', 500)->nullable();
			$table->integer('hasCancelled')->nullable();
			$table->integer('hasChangedCartridge')->nullable();
			$table->string('selectedCartridgePlan', 50)->nullable();
			$table->integer('hasChangedPlan')->nullable();
			$table->string('selectedPlanUsage', 50)->nullable();
			$table->integer('hasGetNewPackage')->nullable();
			$table->integer('hasGetDiscount')->nullable();
			$table->integer('hasPausedSubscription')->nullable();
			$table->integer('discountPercent')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cancellationjourneys');
	}

}
