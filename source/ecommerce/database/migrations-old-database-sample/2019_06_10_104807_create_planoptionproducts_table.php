<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanoptionproductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planoptionproducts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('qty')->nullable()->default(0);
			$table->integer('PlanOptionId')->nullable()->index('PlanOptionId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planoptionproducts');
	}

}
