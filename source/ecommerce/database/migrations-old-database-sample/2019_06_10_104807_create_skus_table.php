<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sku', 50);
			$table->string('unit', 30)->nullable();
			$table->decimal('basedPrice', 10, 0);
			$table->decimal('sellPrice', 10, 0);
			$table->string('currency', 5);
			$table->integer('rating')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('ProductId')->nullable()->index('ProductId');
			$table->integer('CountryId')->nullable()->index('CountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skus');
	}

}
