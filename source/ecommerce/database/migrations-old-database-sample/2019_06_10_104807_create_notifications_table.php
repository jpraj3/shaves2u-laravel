<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('content', 65535);
			$table->dateTime('happenedDate');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('NotificationTypeId')->nullable()->index('NotificationTypeId');
			$table->integer('UserId')->nullable()->index('UserId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
