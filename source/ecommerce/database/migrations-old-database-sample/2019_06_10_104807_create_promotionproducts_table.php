<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionproductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotionproducts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('planIds', 65535)->nullable();
			$table->text('productCountryIds', 65535)->nullable();
			$table->integer('PromotionId')->nullable()->index('PromotionId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotionproducts');
	}

}
