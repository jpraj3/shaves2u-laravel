<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFollowupcancelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('followupcancels', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('SubscriptionId')->index('	SubscriptionId');
			$table->integer('UserId');
			$table->integer('CancelEdm1')->default(0);
			$table->integer('CancelEdm2')->default(0);
			$table->integer('CancelEdm3')->default(0);
			$table->integer('CancelEdm4')->default(0);
			$table->boolean('CancelEdm5');
			$table->date('CreatedAt');
			$table->date('updated_at');
			$table->date('ReminderDate');
			$table->integer('ReminderStatus')->default(0);
			$table->integer('NextJourneyStatus')->nullable();
			$table->text('JourneyDesc', 65535);
			$table->integer('Status')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('followupcancels');
	}

}
