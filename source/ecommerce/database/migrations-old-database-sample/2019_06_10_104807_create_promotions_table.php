<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('discount')->nullable()->default(0);
			$table->string('bannerUrl', 200)->nullable();
			$table->string('emailTemplateId', 50)->nullable();
			$table->text('emailTemplateOptions', 65535)->nullable();
			$table->date('expiredAt');
			$table->date('activeAt')->default('2017-10-26');
			$table->decimal('minSpend', 10)->nullable();
			$table->boolean('allowMinSpendForTotalAmount')->nullable()->default(0);
			$table->decimal('maxDiscount', 10)->nullable();
			$table->text('planIds', 65535)->nullable();
			$table->text('productCountryIds', 65535)->nullable();
			$table->text('planCountryIds', 65535)->nullable();
			$table->text('freeProductCountryIds', 65535)->nullable();
			$table->boolean('isGeneric')->nullable()->default(0);
			$table->integer('timePerUser')->nullable()->default(1);
			$table->integer('totalUsage')->nullable();
			$table->text('appliedTo', 65535)->nullable();
			$table->boolean('isFreeShipping')->nullable()->default(0);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->boolean('isBaApp')->nullable()->default(0);
			$table->boolean('isOnline')->nullable()->default(1);
			$table->boolean('isOffline')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotions');
	}

}
