<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlancountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plancountries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->decimal('actualPrice', 10);
			$table->decimal('sellPrice', 10);
			$table->decimal('savePercent', 10);
			$table->boolean('isOnline')->nullable()->default(1);
			$table->boolean('isOffline')->nullable()->default(1);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('PlanId')->nullable()->index('PlanId');
			$table->decimal('pricePerCharge', 10)->default(0.00);
			$table->integer('order')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plancountries');
	}

}
