<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticletypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articletypes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('type', 10);
			$table->string('langCode', 2)->default('EN');
			$table->integer('CountryId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articletypes');
	}

}
