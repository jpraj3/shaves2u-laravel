<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sku', 50)->unique('sku');
			$table->integer('rating')->nullable()->default(0);
			$table->boolean('isFeatured')->nullable()->default(0);
			$table->enum('status', array('active','removed'))->default('active');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('PlanTypeId')->nullable()->index('PlanTypeId');
			$table->string('slug', 50)->unique('slug_UNIQUE');
			$table->integer('order')->nullable()->default(0);
			$table->boolean('isTrial')->nullable()->default(0);
			$table->integer('PlanGroupId')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans');
	}

}
