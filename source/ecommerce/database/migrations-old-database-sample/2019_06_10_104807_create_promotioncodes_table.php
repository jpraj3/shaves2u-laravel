<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotioncodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotioncodes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 50);
			$table->boolean('isValid')->nullable()->default(1);
			$table->string('email', 50)->nullable();
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('PromotionId')->nullable()->index('PromotionId');
			$table->integer('promotion_type')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promotioncodes');
	}

}
