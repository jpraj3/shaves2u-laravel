<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanoptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planoptions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('description', 65535)->nullable();
			$table->string('url', 200);
			$table->decimal('actualPrice', 10);
			$table->decimal('sellPrice', 10);
			$table->decimal('pricePerCharge', 10)->nullable()->default(0.00);
			$table->decimal('savePercent', 10);
			$table->boolean('hasShaveCream')->nullable()->default(1);
			$table->integer('PlanCountryId')->nullable()->index('PlanCountryId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planoptions');
	}

}
