<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomplansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customplans', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('description', 65535);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId')->nullable()->index('CountryId');
			$table->integer('UserId')->nullable()->index('UserId');
			$table->integer('PlanTypeId')->nullable()->index('PlanTypeId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customplans');
	}

}
