<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalereportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salereports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('badgeId', 10)->nullable();
			$table->string('agentName', 200)->nullable();
			$table->enum('channelType', array('Event','Streets','B2B','RES','Bazar','Truck','Roadshow'))->nullable();
			$table->text('eventLocationCode', 65535)->nullable();
			$table->enum('states', array('Pending','On Hold','Payment Received','Processing','Delivering','Completed','Canceled'))->nullable()->default('On Hold');
			$table->string('deliveryId', 30)->nullable();
			$table->string('carrierAgent', 30)->nullable();
			$table->string('customerName', 200)->nullable();
			$table->text('deliveryAddress', 65535)->nullable();
			$table->string('deliveryContact', 30)->nullable();
			$table->text('billingAddress', 65535)->nullable();
			$table->string('billingContact', 30)->nullable();
			$table->string('email', 50);
			$table->enum('productCategory', array('Ala Carte','Subscription'))->nullable();
			$table->text('sku', 65535);
			$table->text('qty', 65535);
			$table->text('productName', 65535);
			$table->string('currency', 5)->nullable();
			$table->string('paymentType', 20)->nullable();
			$table->string('region', 5);
			$table->enum('category', array('client','sales app','Bulk Order'))->nullable()->default('client');
			$table->date('saleDate');
			$table->integer('orderId')->unsigned()->nullable();
			$table->integer('bulkOrderId')->unsigned()->nullable();
			$table->string('promoCode', 50)->nullable();
			$table->decimal('totalPrice', 10)->nullable()->default(0.00);
			$table->decimal('subTotalPrice', 10)->nullable()->default(0.00);
			$table->decimal('discountAmount', 10)->nullable()->default(0.00);
			$table->decimal('shippingFee', 10)->nullable()->default(0.00);
			$table->decimal('taxAmount', 10)->nullable()->default(0.00);
			$table->decimal('grandTotalPrice', 10)->nullable()->default(0.00);
			$table->string('source', 50)->nullable()->default('direct');
			$table->string('medium', 50)->nullable()->default('none');
			$table->string('campaign', 50)->nullable()->default('none');
			$table->string('term', 50)->nullable()->default('none');
			$table->string('content', 50)->nullable()->default('none');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->integer('CountryId');
			$table->string('taxInvoiceNo', 20)->nullable();
			$table->integer('MarketingOfficeId')->nullable();
			$table->string('moCode', 5)->nullable();
			$table->integer('taxRate')->nullable()->default(0);
			$table->boolean('isDirectTrial')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salereports');
	}

}
