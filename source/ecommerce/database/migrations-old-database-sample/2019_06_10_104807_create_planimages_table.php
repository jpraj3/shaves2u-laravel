<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanimagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planimages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('url', 200);
			$table->boolean('isDefault')->nullable()->default(0);
			$table->integer('PlanId')->nullable()->index('PlanId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planimages');
	}

}
