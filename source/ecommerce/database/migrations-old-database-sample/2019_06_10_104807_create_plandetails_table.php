<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlandetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plandetails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('deliverPlan', 65535);
			$table->integer('PlanCountryId')->nullable()->index('PlanCountryId');
			$table->integer('ProductCountryId')->nullable()->index('ProductCountryId');
			$table->integer('SkuId')->nullable()->index('SkuId');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plandetails');
	}

}
