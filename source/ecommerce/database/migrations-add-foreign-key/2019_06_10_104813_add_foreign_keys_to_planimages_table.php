<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanimagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planimages', function(Blueprint $table)
		{
			$table->foreign('PlanId', 'PlanImages_ibfk_1')->references('id')->on('plans')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planimages', function(Blueprint $table)
		{
			$table->dropForeign('PlanImages_ibfk_1');
		});
	}

}
