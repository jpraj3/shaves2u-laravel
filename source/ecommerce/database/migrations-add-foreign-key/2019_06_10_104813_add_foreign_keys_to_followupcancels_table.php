<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFollowupcancelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('followupcancels', function(Blueprint $table)
		{
			$table->foreign('SubscriptionId', '	SubscriptionId')->references('id')->on('subscriptions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('followupcancels', function(Blueprint $table)
		{
			$table->dropForeign('	SubscriptionId');
		});
	}

}
