<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductvariantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productvariants', function(Blueprint $table)
		{
			$table->foreign('ProductId', 'ProductVariants_ibfk_1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productvariants', function(Blueprint $table)
		{
			$table->dropForeign('ProductVariants_ibfk_1');
		});
	}

}
