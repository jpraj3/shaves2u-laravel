<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNonecommercesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('nonecommerces', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'nonecommerces_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('nonecommerces', function(Blueprint $table)
		{
			$table->dropForeign('nonecommerces_ibfk_1');
		});
	}

}
