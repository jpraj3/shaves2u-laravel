<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArticletranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('articletranslates', function(Blueprint $table)
		{
			$table->foreign('ArticleId', 'ArticleTranslates_ibfk_1')->references('id')->on('articles')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('CountryId', 'ArticleTranslates_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articletranslates', function(Blueprint $table)
		{
			$table->dropForeign('ArticleTranslates_ibfk_1');
			$table->dropForeign('ArticleTranslates_ibfk_2');
		});
	}

}
