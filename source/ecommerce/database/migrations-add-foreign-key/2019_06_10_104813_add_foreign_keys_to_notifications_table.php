<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notifications', function(Blueprint $table)
		{
			$table->foreign('NotificationTypeId', 'Notifications_ibfk_1')->references('id')->on('notificationtypes')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserId', 'Notifications_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notifications', function(Blueprint $table)
		{
			$table->dropForeign('Notifications_ibfk_1');
			$table->dropForeign('Notifications_ibfk_2');
		});
	}

}
