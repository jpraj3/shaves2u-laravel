<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCustomplansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customplans', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'customplans_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserId', 'customplans_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanTypeId', 'customplans_ibfk_3')->references('id')->on('plantypes')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customplans', function(Blueprint $table)
		{
			$table->dropForeign('customplans_ibfk_1');
			$table->dropForeign('customplans_ibfk_2');
			$table->dropForeign('customplans_ibfk_3');
		});
	}

}
