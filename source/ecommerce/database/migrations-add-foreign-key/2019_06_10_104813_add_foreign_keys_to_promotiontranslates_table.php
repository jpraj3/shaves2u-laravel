<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPromotiontranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotiontranslates', function(Blueprint $table)
		{
			$table->foreign('PromotionId', 'PromotionTranslates_ibfk_1')->references('id')->on('promotions')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotiontranslates', function(Blueprint $table)
		{
			$table->dropForeign('PromotionTranslates_ibfk_1');
		});
	}

}
