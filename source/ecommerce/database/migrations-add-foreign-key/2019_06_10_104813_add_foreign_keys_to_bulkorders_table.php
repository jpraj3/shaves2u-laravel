<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBulkordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bulkorders', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'bulkorders_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('DeliveryAddressId', 'bulkorders_ibfk_2')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('BillingAddressId', 'bulkorders_ibfk_3')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bulkorders', function(Blueprint $table)
		{
			$table->dropForeign('bulkorders_ibfk_1');
			$table->dropForeign('bulkorders_ibfk_2');
			$table->dropForeign('bulkorders_ibfk_3');
		});
	}

}
