<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSubscriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subscriptions', function(Blueprint $table)
		{
			$table->foreign('CardId', 'Subscriptions_ibfk_1')->references('id')->on('cards')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanId', 'Subscriptions_ibfk_2')->references('id')->on('plans')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SellerUserId', 'Subscriptions_ibfk_3')->references('id')->on('sellerusers')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SkuId', 'Subscriptions_ibfk_4')->references('id')->on('skus')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserId', 'Subscriptions_ibfk_5')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('DeliveryAddressId', 'Subscriptions_ibfk_6')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('BillingAddressId', 'Subscriptions_ibfk_7')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanCountryId', 'Subscriptions_ibfk_8')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subscriptions', function(Blueprint $table)
		{
			$table->dropForeign('Subscriptions_ibfk_1');
			$table->dropForeign('Subscriptions_ibfk_2');
			$table->dropForeign('Subscriptions_ibfk_3');
			$table->dropForeign('Subscriptions_ibfk_4');
			$table->dropForeign('Subscriptions_ibfk_5');
			$table->dropForeign('Subscriptions_ibfk_6');
			$table->dropForeign('Subscriptions_ibfk_7');
			$table->dropForeign('Subscriptions_ibfk_8');
		});
	}

}
