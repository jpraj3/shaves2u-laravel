<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsedtokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usedtokens', function(Blueprint $table)
		{
			$table->foreign('OrderId', 'usedtokens_ibfk_1')->references('id')->on('orders')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usedtokens', function(Blueprint $table)
		{
			$table->dropForeign('usedtokens_ibfk_1');
		});
	}

}
