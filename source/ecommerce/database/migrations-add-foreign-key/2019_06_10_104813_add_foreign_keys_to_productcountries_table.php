<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductcountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productcountries', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'ProductCountries_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductId', 'ProductCountries_ibfk_2')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productcountries', function(Blueprint $table)
		{
			$table->dropForeign('ProductCountries_ibfk_1');
			$table->dropForeign('ProductCountries_ibfk_2');
		});
	}

}
