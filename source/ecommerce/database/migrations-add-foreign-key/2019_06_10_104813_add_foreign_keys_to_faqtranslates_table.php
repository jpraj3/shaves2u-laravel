<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFaqtranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faqtranslates', function(Blueprint $table)
		{
			$table->foreign('FaqId', 'faqtranslates_ibfk_1')->references('id')->on('faqs')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faqtranslates', function(Blueprint $table)
		{
			$table->dropForeign('faqtranslates_ibfk_1');
		});
	}

}
