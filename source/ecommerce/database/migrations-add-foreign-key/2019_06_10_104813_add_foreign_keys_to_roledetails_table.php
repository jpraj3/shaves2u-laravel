<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRoledetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('roledetails', function(Blueprint $table)
		{
			$table->foreign('PathId', 'roledetails_ibfk_1')->references('id')->on('paths')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('RoleId', 'roledetails_ibfk_2')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('roledetails', function(Blueprint $table)
		{
			$table->dropForeign('roledetails_ibfk_1');
			$table->dropForeign('roledetails_ibfk_2');
		});
	}

}
