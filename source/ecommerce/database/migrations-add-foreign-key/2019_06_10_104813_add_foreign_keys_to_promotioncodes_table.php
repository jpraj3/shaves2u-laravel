<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPromotioncodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotioncodes', function(Blueprint $table)
		{
			$table->foreign('PromotionId', 'PromotionCodes_ibfk_1')->references('id')->on('promotions')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotioncodes', function(Blueprint $table)
		{
			$table->dropForeign('PromotionCodes_ibfk_1');
		});
	}

}
