<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeliveryaddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('deliveryaddresses', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'DeliveryAddresses_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserId', 'DeliveryAddresses_ibfk_2')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SellerUserId', 'DeliveryAddresses_ibfk_3')->references('id')->on('sellerusers')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('deliveryaddresses', function(Blueprint $table)
		{
			$table->dropForeign('DeliveryAddresses_ibfk_1');
			$table->dropForeign('DeliveryAddresses_ibfk_2');
			$table->dropForeign('DeliveryAddresses_ibfk_3');
		});
	}

}
