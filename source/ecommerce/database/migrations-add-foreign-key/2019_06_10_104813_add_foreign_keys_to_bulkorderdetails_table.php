<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBulkorderdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bulkorderdetails', function(Blueprint $table)
		{
			$table->foreign('BulkOrderId', 'bulkorderdetails_ibfk_1')->references('id')->on('bulkorders')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'bulkorderdetails_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanCountryId', 'bulkorderdetails_ibfk_3')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bulkorderdetails', function(Blueprint $table)
		{
			$table->dropForeign('bulkorderdetails_ibfk_1');
			$table->dropForeign('bulkorderdetails_ibfk_2');
			$table->dropForeign('bulkorderdetails_ibfk_3');
		});
	}

}
