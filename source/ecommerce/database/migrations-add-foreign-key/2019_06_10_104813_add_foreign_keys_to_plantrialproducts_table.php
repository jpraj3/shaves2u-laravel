<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlantrialproductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plantrialproducts', function(Blueprint $table)
		{
			$table->foreign('PlanCountryId', 'plantrialproducts_ibfk_1')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'plantrialproducts_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plantrialproducts', function(Blueprint $table)
		{
			$table->dropForeign('plantrialproducts_ibfk_1');
			$table->dropForeign('plantrialproducts_ibfk_2');
		});
	}

}
