<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlangrouptranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plangrouptranslates', function(Blueprint $table)
		{
			$table->foreign('PlanGroupId', 'plangrouptranslates_ibfk_1')->references('id')->on('plangroups')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plangrouptranslates', function(Blueprint $table)
		{
			$table->dropForeign('plangrouptranslates_ibfk_1');
		});
	}

}
