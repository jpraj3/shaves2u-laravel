<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductvariantoptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productvariantoptions', function(Blueprint $table)
		{
			$table->foreign('ProductVariantId', 'ProductVariantOptions_ibfk_1')->references('id')->on('productvariants')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductId', 'ProductVariantOptions_ibfk_2')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productvariantoptions', function(Blueprint $table)
		{
			$table->dropForeign('ProductVariantOptions_ibfk_1');
			$table->dropForeign('ProductVariantOptions_ibfk_2');
		});
	}

}
