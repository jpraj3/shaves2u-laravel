<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCustomplandetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customplandetails', function(Blueprint $table)
		{
			$table->foreign('CustomPlanId', 'customplandetails_ibfk_1')->references('id')->on('customplans')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'customplandetails_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customplandetails', function(Blueprint $table)
		{
			$table->dropForeign('customplandetails_ibfk_1');
			$table->dropForeign('customplandetails_ibfk_2');
		});
	}

}
