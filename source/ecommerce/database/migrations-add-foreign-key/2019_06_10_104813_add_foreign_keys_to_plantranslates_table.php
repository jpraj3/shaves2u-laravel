<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlantranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plantranslates', function(Blueprint $table)
		{
			$table->foreign('PlanId', 'PlanTranslates_ibfk_1')->references('id')->on('plans')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plantranslates', function(Blueprint $table)
		{
			$table->dropForeign('PlanTranslates_ibfk_1');
		});
	}

}
