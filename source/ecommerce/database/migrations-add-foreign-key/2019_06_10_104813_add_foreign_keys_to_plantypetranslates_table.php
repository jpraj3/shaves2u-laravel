<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlantypetranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plantypetranslates', function(Blueprint $table)
		{
			$table->foreign('PlanTypeId', 'plantypetranslates_ibfk_1')->references('id')->on('plantypes')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plantypetranslates', function(Blueprint $table)
		{
			$table->dropForeign('plantypetranslates_ibfk_1');
		});
	}

}
