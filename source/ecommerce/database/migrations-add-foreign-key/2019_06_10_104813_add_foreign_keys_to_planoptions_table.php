<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanoptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planoptions', function(Blueprint $table)
		{
			$table->foreign('PlanCountryId', 'planoptions_ibfk_1')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planoptions', function(Blueprint $table)
		{
			$table->dropForeign('planoptions_ibfk_1');
		});
	}

}
