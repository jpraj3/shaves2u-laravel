<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSkusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('skus', function(Blueprint $table)
		{
			$table->foreign('ProductId', 'Skus_ibfk_1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('CountryId', 'Skus_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('skus', function(Blueprint $table)
		{
			$table->dropForeign('Skus_ibfk_1');
			$table->dropForeign('Skus_ibfk_2');
		});
	}

}
