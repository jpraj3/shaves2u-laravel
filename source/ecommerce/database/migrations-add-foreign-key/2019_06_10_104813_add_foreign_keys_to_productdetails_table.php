<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productdetails', function(Blueprint $table)
		{
			$table->foreign('ProductId', 'ProductDetails_ibfk_1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productdetails', function(Blueprint $table)
		{
			$table->dropForeign('ProductDetails_ibfk_1');
		});
	}

}
