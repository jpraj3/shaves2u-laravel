<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCancellationjourneysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cancellationjourneys', function(Blueprint $table)
		{
			$table->foreign('UserId', 'cancellationjourneys_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('OrderId', 'cancellationjourneys_ibfk_2')->references('id')->on('orders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('SubscriptionId', 'cancellationjourneys_ibfk_3')->references('id')->on('subscriptions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cancellationjourneys', function(Blueprint $table)
		{
			$table->dropForeign('cancellationjourneys_ibfk_1');
			$table->dropForeign('cancellationjourneys_ibfk_2');
			$table->dropForeign('cancellationjourneys_ibfk_3');
		});
	}

}
