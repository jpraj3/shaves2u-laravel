<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSupportedlanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('supportedlanguages', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'supportedlanguages_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('supportedlanguages', function(Blueprint $table)
		{
			$table->dropForeign('supportedlanguages_ibfk_1');
		});
	}

}
