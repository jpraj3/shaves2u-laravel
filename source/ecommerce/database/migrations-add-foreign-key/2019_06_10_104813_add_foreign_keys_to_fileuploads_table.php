<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFileuploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fileuploads', function(Blueprint $table)
		{
			$table->foreign('AdminId', 'fileuploads_ibfk_1')->references('id')->on('admins')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fileuploads', function(Blueprint $table)
		{
			$table->dropForeign('fileuploads_ibfk_1');
		});
	}

}
