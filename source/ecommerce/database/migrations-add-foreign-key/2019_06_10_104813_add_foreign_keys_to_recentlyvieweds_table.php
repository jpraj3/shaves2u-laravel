<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecentlyviewedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recentlyvieweds', function(Blueprint $table)
		{
			$table->foreign('UserId', 'RecentlyVieweds_ibfk_1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'RecentlyVieweds_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SkuId', 'RecentlyVieweds_ibfk_3')->references('id')->on('skus')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recentlyvieweds', function(Blueprint $table)
		{
			$table->dropForeign('RecentlyVieweds_ibfk_1');
			$table->dropForeign('RecentlyVieweds_ibfk_2');
			$table->dropForeign('RecentlyVieweds_ibfk_3');
		});
	}

}
