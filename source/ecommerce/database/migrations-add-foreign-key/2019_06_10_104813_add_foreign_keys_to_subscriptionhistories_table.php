<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSubscriptionhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subscriptionhistories', function(Blueprint $table)
		{
			$table->foreign('subscriptionId', 'foreign_key_subscriptionId')->references('id')->on('subscriptions')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subscriptionhistories', function(Blueprint $table)
		{
			$table->dropForeign('foreign_key_subscriptionId');
		});
	}

}
