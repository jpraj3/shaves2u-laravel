<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCartdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cartdetails', function(Blueprint $table)
		{
			$table->foreign('CartId', 'CartDetails_ibfk_1')->references('id')->on('carts')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'CartDetails_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanCountryId', 'CartDetails_ibfk_3')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('CountryId', 'CartDetails_ibfk_4')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SkuId', 'CartDetails_ibfk_5')->references('id')->on('skus')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cartdetails', function(Blueprint $table)
		{
			$table->dropForeign('CartDetails_ibfk_1');
			$table->dropForeign('CartDetails_ibfk_2');
			$table->dropForeign('CartDetails_ibfk_3');
			$table->dropForeign('CartDetails_ibfk_4');
			$table->dropForeign('CartDetails_ibfk_5');
		});
	}

}
