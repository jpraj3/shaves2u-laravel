<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProducttypetranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('producttypetranslates', function(Blueprint $table)
		{
			$table->foreign('ProductTypeId', 'producttypetranslates_ibfk_1')->references('id')->on('producttypes')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('producttypetranslates', function(Blueprint $table)
		{
			$table->dropForeign('producttypetranslates_ibfk_1');
		});
	}

}
