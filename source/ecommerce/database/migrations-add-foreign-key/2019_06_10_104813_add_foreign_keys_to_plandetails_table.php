<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlandetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plandetails', function(Blueprint $table)
		{
			$table->foreign('PlanCountryId', 'PlanDetails_ibfk_1')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'PlanDetails_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SkuId', 'PlanDetails_ibfk_3')->references('id')->on('skus')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plandetails', function(Blueprint $table)
		{
			$table->dropForeign('PlanDetails_ibfk_1');
			$table->dropForeign('PlanDetails_ibfk_2');
			$table->dropForeign('PlanDetails_ibfk_3');
		});
	}

}
