<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSellerusersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sellerusers', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'SellerUsers_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sellerusers', function(Blueprint $table)
		{
			$table->dropForeign('SellerUsers_ibfk_1');
		});
	}

}
