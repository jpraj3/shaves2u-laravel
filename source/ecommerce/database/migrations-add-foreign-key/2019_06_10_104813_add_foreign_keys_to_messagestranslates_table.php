<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessagestranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('messagestranslates', function(Blueprint $table)
		{
			$table->foreign('messageId', 'messagestranslates_ibfk_1')->references('id')->on('messages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('messagestranslates', function(Blueprint $table)
		{
			$table->dropForeign('messagestranslates_ibfk_1');
		});
	}

}
