<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->foreign('DeliveryAddressId', 'orders_ibfk_1')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('CountryId', 'orders_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('BillingAddressId', 'orders_ibfk_3')->references('id')->on('deliveryaddresses')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PromotionId', 'orders_ibfk_4')->references('id')->on('promotions')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserId', 'orders_ibfk_5')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('SellerUserId', 'orders_ibfk_6')->references('id')->on('sellerusers')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropForeign('orders_ibfk_1');
			$table->dropForeign('orders_ibfk_2');
			$table->dropForeign('orders_ibfk_3');
			$table->dropForeign('orders_ibfk_4');
			$table->dropForeign('orders_ibfk_5');
			$table->dropForeign('orders_ibfk_6');
		});
	}

}
