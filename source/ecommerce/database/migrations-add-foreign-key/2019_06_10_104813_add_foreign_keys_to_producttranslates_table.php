<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProducttranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('producttranslates', function(Blueprint $table)
		{
			$table->foreign('ProductId', 'ProductTranslates_ibfk_1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('producttranslates', function(Blueprint $table)
		{
			$table->dropForeign('ProductTranslates_ibfk_1');
		});
	}

}
