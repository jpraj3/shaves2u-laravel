<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlancountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plancountries', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'PlanCountries_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanId', 'PlanCountries_ibfk_2')->references('id')->on('plans')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plancountries', function(Blueprint $table)
		{
			$table->dropForeign('PlanCountries_ibfk_1');
			$table->dropForeign('PlanCountries_ibfk_2');
		});
	}

}
