<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orderdetails', function(Blueprint $table)
		{
			$table->foreign('OrderId', 'orderdetails_ibfk_1')->references('id')->on('orders')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'orderdetails_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('PlanCountryId', 'orderdetails_ibfk_3')->references('id')->on('plancountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orderdetails', function(Blueprint $table)
		{
			$table->dropForeign('orderdetails_ibfk_1');
			$table->dropForeign('orderdetails_ibfk_2');
			$table->dropForeign('orderdetails_ibfk_3');
		});
	}

}
