<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanoptiontranslatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planoptiontranslates', function(Blueprint $table)
		{
			$table->foreign('PlanOptionId', 'planoptiontranslates_ibfk_1')->references('id')->on('planoptions')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planoptiontranslates', function(Blueprint $table)
		{
			$table->dropForeign('planoptiontranslates_ibfk_1');
		});
	}

}
