<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admins', function(Blueprint $table)
		{
			$table->foreign('AdminRoleId', 'Admins_ibfk_1')->references('id')->on('adminroles')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('CountryId', 'Admins_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('RoleId', 'Admins_ibfk_3')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admins', function(Blueprint $table)
		{
			$table->dropForeign('Admins_ibfk_1');
			$table->dropForeign('Admins_ibfk_2');
			$table->dropForeign('Admins_ibfk_3');
		});
	}

}
