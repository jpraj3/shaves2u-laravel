<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('articles', function(Blueprint $table)
		{
			$table->foreign('postBy', 'Articles_ibfk_1')->references('id')->on('admins')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ArticleTypeId', 'Articles_ibfk_2')->references('id')->on('articletypes')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articles', function(Blueprint $table)
		{
			$table->dropForeign('Articles_ibfk_1');
			$table->dropForeign('Articles_ibfk_2');
		});
	}

}
