<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBulkorderhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bulkorderhistories', function(Blueprint $table)
		{
			$table->foreign('BulkOrderId', 'bulkorderhistories_ibfk_1')->references('id')->on('bulkorders')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bulkorderhistories', function(Blueprint $table)
		{
			$table->dropForeign('bulkorderhistories_ibfk_1');
		});
	}

}
