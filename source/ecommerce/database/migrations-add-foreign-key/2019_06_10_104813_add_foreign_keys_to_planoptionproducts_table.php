<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanoptionproductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('planoptionproducts', function(Blueprint $table)
		{
			$table->foreign('PlanOptionId', 'planoptionproducts_ibfk_1')->references('id')->on('planoptions')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('ProductCountryId', 'planoptionproducts_ibfk_2')->references('id')->on('productcountries')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('planoptionproducts', function(Blueprint $table)
		{
			$table->dropForeign('planoptionproducts_ibfk_1');
			$table->dropForeign('planoptionproducts_ibfk_2');
		});
	}

}
