<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPromotionproductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotionproducts', function(Blueprint $table)
		{
			$table->foreign('PromotionId', 'PromotionProducts_ibfk_1')->references('id')->on('promotions')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotionproducts', function(Blueprint $table)
		{
			$table->dropForeign('PromotionProducts_ibfk_1');
		});
	}

}
