<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->foreign('CountryId', 'Users_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('NotificationTypeId', 'Users_ibfk_2')->references('id')->on('notificationtypes')->onUpdate('CASCADE')->onDelete('SET NULL');
			$table->foreign('UserTypeId', 'Users_ibfk_3')->references('id')->on('usertypes')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('Users_ibfk_1');
			$table->dropForeign('Users_ibfk_2');
			$table->dropForeign('Users_ibfk_3');
		});
	}

}
