<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productimages', function(Blueprint $table)
		{
			$table->foreign('ProductId', 'ProductImages_ibfk_1')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productimages', function(Blueprint $table)
		{
			$table->dropForeign('ProductImages_ibfk_1');
		});
	}

}
