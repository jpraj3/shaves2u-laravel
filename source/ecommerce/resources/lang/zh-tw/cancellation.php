<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cancellation Journey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for cancellation journey content.
    |
    */

    "modal" => [

        "global" => [
            "any-other-reason" => "其他：",
            "cancel-shave-plan" => "取消配套",
            "cancel-subscription" => "取消配套",
            "close" => "關閉",
            "dont-cancel" => "返回",
            "get-new-cassete" => "馬上領取",
            "keep-shaving" => "太好了！",
            "ok-great" => "太好了！",
            "other-reason" => "其他原因：",
            "proceed-cancellation" => "取消配套",
            "proceed-next" => "下一步",
            "try-different-blade" => "更改刀頭類型"
        ],
        "main" => [
            "header" => "等等！您會告訴我們為什麼要取消嗎？",
            "subheader-1" => "很遺憾您對我們不滿，",
            "subheader-2" => "您願意花一點時間告訴我們原因嗎？",
        ],
        "option1" => [
            "header" => "抱歉！我們應該加強溝通方式！",
            "subheader-1" => "很遺憾您對刮鬍刀配套不滿意。",
            "subheader-2" => "您喜歡我們的刮鬍刀嗎？",
            "text-1" => "最受歡迎選項",
            "text-2" => "是的，非常滿意！",
            "text-3" => '不如，我們在您下一個結算週期提供<div class="discount">:discount %</div>折扣？"',
            "text-4" => "不！再見！"
        ],
        "option2" => [
            "header" => "請告訴我們更多",
            "subheader-1" => "抱歉我們的產品辜負了您!",
            "subheader-2" => "請告訴我們出了什麼問題?"
        ],
        "option2_1" => [
            "header" => "何不試試其他刀頭呢？",
            "subheader-1" => "我們提供不同的刀頭以滿足每個人的不同刮鬍需求。",
            "text-1" => " 我們的客戶可以隨時更改刀頭類型，為何不試試使用不同的刀頭呢？"
        ],
        "option2_2" => [
            "header" => "這不應該發生！",
            "subheader-1" => "讓我們立即替換一組新的刮鬍刀給您。",
            "text-1" => "抱歉我們的產品辜負了您! 我們立即替您更換新的刮鬍刀！"
        ],
        "option3" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "不如自行定制您專屬的刮鬍刀配套？",
            "text-1" => "您可以更改替補頻率以符合您的預算。",
            "text-2" => "替補頻率",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount %</div>折扣'
        ],
        "option3_pause" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "我們將您的配套暫停如何？",
            "text-1" => "由於您有過多的刮鬍刀，我們將您的配套暫停<label class=\"month\">:months</label>個月如何？",
            "text-2" => "我們也將在下一個結算週期讓您享有<label class=\"discount\">:discount%</label>的折扣。"
        ],
        "option4" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "不如自行定制您專屬的刮鬍刀配套？",
            "text-1" => "您可以更改替補頻率以符合您的預算。",
            "text-2" => "替補頻率",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount %</div>折扣'
        ],
        "option4_pause" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "我們將您的配套暫停如何？",
            "text-1" => "由於您有過多的刮鬍刀，我們將您的配套暫停<label class=\"month\">:months</label>個月如何？",
            "text-2" => "我們也將在下一個結算週期讓您享有<label class=\"discount\">:discount% </label>的折扣。"
        ],
        "option5" => [
            "header" => "別急著要走！",
            "text-1" => "我們將以較低的價格提供高品質的刮鬍刀，這樣您就不會超出預算了。",
            "text-2" => "您會留下來嗎？",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount%</div>折扣'
        ],
        "option6" => [
            "header" => "您說真的嗎？",
            "subheader-1" => "不如我們在您的下一個結算週期折扣，就這一次？",
            "text-1" => "最受歡迎選項",
            "text-2" => '下一個結算週期提供<div class="discount">:discount% </div>折扣',
            "text-3" => '不！再見！'
        ],
        "option7" => [
            "header" => "別急著要走！",
            "text-1" => "我們將以較低的價格提供高品質的刮鬍刀，這樣您就不會超出預算了。",
            "text-2" => "您會留下來嗎？",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount% </div>折扣'
        ],
        "cancelled-page" => [
            "header" => "很遺憾您要走了！",
            "subheader-1" => "感謝您選擇Shaves2U!",
            "subheader-2" => "如果你改變主意，我們就在這裡等你！"
        ],
        "blade-updated-page" => [
            "header" => "您剛更新了刮鬍配套！",
            "subheader-1" => "您已經更改了刀頭類型",
            "subheader-2" => "，希望這更適合您的刮鬍習慣。"
        ],
        "free-product-applied-page" => [
            "header" => "我們正在處理您的新刮鬍刀！",
            "subheader-1" => "你的滿意度對我們很重要，",
            "subheader-2" => "希望您會發現這新的刮鬍刀比之前更好。"
        ]

    ]
];
