<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id="social-link-facebook"><span class="fa fa-facebook-official"></span></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id="social-link-twitter"><span class="fa fa-twitter"></span></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id="social-link-linkedin"><span class="fa fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id="social-link-whatsapp"><span class="fa fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id="social-link-pintrest"><span class="fa fa-pinterest"></span></a></li>',
];
