<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cancellation Journey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for cancellation journey content.
    |
    */

    "modal" => [

        "global" => [
            "any-other-reason" => "다른 이유:",
            "cancel-shave-plan" => "쉐이브 플랜 해지하기",
            "cancel-subscription" => "해지하기",
            "close" => "CLOSE",
            "dont-cancel" => "취소",
            "get-new-cassete" => "면도날 교환받기",
            "keep-shaving" => "리필배송 할인 받기",
            "ok-great" => "확인",
            "other-reason" => "다른 이유",
            "proceed-cancellation" => "정기배송 해지하기",
            "proceed-next" => "다음 단계로",
            "try-different-blade" => "다른 면도날 사용하기"
        ],
        "main" => [
            "header" => "부디 취소하시는 이유를 알려주세요.",
            "subheader-1" => "쉐이브 플랜을 통해 만족스럽지 못한 서비스를 제공해드려 죄송합니다.",
            "subheader-2" => "서비스 품질 향상을 위해 취소사유를 알려주시기를 부탁드립니다.",
        ],
        "option1" => [
            "header" => "서비스 이용에 불편을 드려 죄송합니다",
            "subheader-1" => "쉐이브 플랜이 마음에 들지 않으셨다니 죄송합니다.",
            "subheader-2" => "면도날은 어떠셨나요?",
            "text-1" => "많이 선택한옵션",
            "text-2" => "면도날은 괜찮았어요.",
            "text-3" => '다음 번 리필 배송 비용을 20% 할인해드리는건 어떠세요?',
            "text-4" => "정기배송 해지할게요."
        ],
        "option2" => [
            "header" => "어떤 점이 불편했는지 알려주세요.",
            "subheader-1" => "제품 품질이 불편하셨다니 죄송합니다.",
            "subheader-2" => "어떤 점이 불편하셨는지 말씀해주세요."
        ],
        "option2_1" => [
            "header" => "다른 면도날을 써보는건 어떠세요?",
            "subheader-1" => "아직 피부에 딱 맞는 면도날을 만나지 못하신 걸수도 있어요.",
            "text-1" => "지금 사용 중이신 날이 고객님께 안 맞는다면 쉐이브투유에서 제공하는 다른 면도날을 사용해보는건 어떠신가요?"
        ],
        "option2_2" => [
            "header" => "고객님께 불편을 드려 죄송합니다.",
            "subheader-1" => "면도날을 새 것으로 교환해드리겠습니다.",
            "text-1" => "서비스 이용에 불편을 드려 죄송합니다. 저희가 새로 보내드릴 면도날로 보다 기분 좋은 면도하시기를 바랍니다."
        ],
        "option3" => [
            "header" => "면도날을 바꿔보세요",
            "subheader-1" => "쉐이브 플랜 옵션을 변경하는건 어떠세요?",
            "text-1" => "원하시는 가격대에 맞게 배송주기와 면도날 종류를 변경하실 수 있습니다.",
            "text-2" => "배송주기",
            "text-3" => '다음번 리필배송 <label class="discount">:discount% 할인도</label> 제공해드립니다.'
        ],
        "option3_pause" => [
            "header" => "면도날을 바꿔보세요",
            "subheader-1" => "잠시 정기배송을 멈추시는건 어떠세요?",
            "text-1" => "다음 리필배송을 기존 스케줄에서 두달 늦게 받는건 어떠신가요?",
            "text-2" => "배송 스케쥴을 변경하시는 경우 다음 번 리필배송을 <label class=\"discount\">:discount% 20% </label> 된 가격에 제공해드립니다. ."
        ],
        "option4" => [
            "header" => "면도날을 바꿔보세요",
            "subheader-1" => "쉐이브 플랜 옵션을 변경하는건 어떠세요?",
            "text-1" => "You can change the frequency of delivery to suit your usage.",
            "text-2" => "Suggestion: Deliver every",
            "text-3" => '다음번 리필배송 <label class="discount">:discount% 할인도</label> 제공해드립니다.'
        ],
        "option4_pause" => [
            "header" => "면도날을 바꿔보세요",
            "subheader-1" => "잠시 정기배송을 멈추시는건 어떠세요?",
            "text-1" => "다음 리필배송을 기존 스케줄에서 두달 늦게 받는건 어떠신가요?",
            "text-2" => "We'll also give you a <label class=\"discount\">:discount% DISCOUNT</label> off your next billing cycle when your Shave Plan resumes."
        ],
        "option5" => [
            "header" => "건식면도로 느끼기 힘든 습식면도만의 장점을 느껴보세요.",
            "text-1" => "전기면도기 편하지만 습식면도처럼 위생적이고 깔끔하게 면도하기는 힘들죠. 쉐이브투유로 습식면도기의 깔끔한 절삭력을 느껴보세요.",
            "text-2" => "조금만 더 쉐이브투유를 사용해보는건 어떠세요?",
            "text-3" => '다음번 리필배송 <label class="discount">:discount% 할인도</label> 제공해드립니다.'
        ],
        "option6" => [
            "header" => "정말요?",
            "subheader-1" => "다음번 리필배송에 한해 20%할인을 받아보는건 어떠세요?",
            "text-1" => "가장 많이 선택한 옵션",
            "text-2" => '<label class="discount">:discount% 할인도</label> 제공해드립니다.',
            "text-3" => '구독해지할래요.'
        ],
        "option7" => [
            "header" => "건식면도로 느끼기 힘든 습식면도만의 장점을 느껴보세요.",
            "text-1" => "전기면도기 편하지만 습식면도처럼 위생적이고 깔끔하게 면도하기는 힘들죠. 쉐이브투유로 습식면도기의 깔끔한 절삭력을 느껴보세요.",
            "text-2" => "조금만 더 쉐이브투유를 사용해보는건 어떠세요?",
            "text-3" => '다음번 리필배송 <label class="discount">:discount% 할인도</label> 제공해드립니다.'
        ],
        "cancelled-page" => [
            "header" => "건식면도로 느끼기 힘든 습식면도만의 장점을 느껴보세요.",
            "subheader-1" => "쉐이브투유를 사용해주셔서 감사합니다.",
            "subheader-2" => "다시 저희 서비스가 이용하고 싶어지시면 언제든 돌아오세요."
        ],
        "blade-updated-page" => [
            "header" => "쉐이브 플랜이 변경되었습니다!",
            "subheader-1" => "요청하신 면도날로 리필 배송옵션이 변경되었습니다.",
            "subheader-2" => "새 면도날은 고객님께 잘 맞기를 바랍니다."
        ],
        "free-product-applied-page" => [
            "header" => "새 면도날이 준비되었습니다!",
            "subheader-1" => "고객님의 만족을 위해 더욱 노력하겠습니다.",
            "subheader-2" => "새로 받으실 면도날로 보다 기분 좋은 면도하시기를 바랍니다."
        ]

    ]
];
