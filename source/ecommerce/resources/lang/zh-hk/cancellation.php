<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cancellation Journey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for cancellation journey content.
    |
    */
    
    "modal" => [

        "global" => [
            "any-other-reason" => "其他原因:",
            "cancel-shave-plan" => "取消計劃",
            "cancel-subscription" => "取消配套",
            "close" => "關閉",
            "dont-cancel" => "返回",
            "get-new-cassete" => "領取刮鬍刀",
            "keep-shaving" => "太好了！",
            "ok-great" => "太好了！",
            "other-reason" => "其他原因:",
            "proceed-cancellation" => "取消配套",
            "proceed-next" => "下一步",
            "try-different-blade" => "更改刀頭類型"
        ],
        "main" => [
            "header" => "等等！您會告訴我們為什麼要取消嗎？",
            "subheader-1" => "很遺憾您對我們不滿",
            "subheader-2" => "您願意花一點時間告訴我們原因嗎？",
        ],
        "option1" => [
            "header" => "抱歉！我們應該加強溝通方式！",
            "subheader-1" => "很遺憾您對刮鬍刀配套不滿意。",
            "subheader-2" => "您喜歡我們的刮鬍刀嗎？",
            "text-1" => "最受歡迎選項",
            "text-2" => "是的，非常滿意！",
            "text-3" => '不如，我們在您下一個結算週期提供<div class="discount">:discount% 折扣</div>？',
            "text-4" => "不！再見！"
        ],
        "option2" => [
            "header" => "請告訴我們更多",
            "subheader-1" => "抱歉我們的產品辜負了您!",
            "subheader-2" => "請告訴我們出了什麼問題?"
        ],
        "option2_1" => [
            "header" => "何不試試其他刀頭呢？",
            "subheader-1" => "我們提供不同的刀頭以滿足每個人的不同刮鬍需求。",
            "text-1" => "我們的客戶可以隨時更改刀頭類型，為何不試試使用不同的刀頭呢？"
        ],
        "option2_2" => [
            "header" => "這不應該發生！",
            "subheader-1" => "讓我們立即替換一組新的剃鬚刀給您。",
            "text-1" => "抱歉我們的產品辜負了您! 我們立即替您更換新的剃鬚刀！"
        ],
        "option3" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "不如自行定制您專屬的剃鬚刀計劃？",
            "text-1" => "您可以更改剃鬚刀類型或運輸頻率以符合您的預算。",
            "text-2" => "運輸頻率",
            "text-3" => 'You also get a <label class="discount">:discount% </label> on your next billing cycle.'
        ],
        "option3_pause" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "您可以更改運輸頻率以符合您的預算。",
            "text-1" => "Since you have too many blades in stock, how about we hold your next delivery for <label class=\"month\">:months months?</label>",
            "text-2" => "We'll also give you a <label class=\"discount\">:discount% </label> off your next billing cycle when your Shave Plan resumes."
        ],
        "option4" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "不如自行定制您專屬的剃鬚刀計劃？",
            "text-1" => "您可以更改運輸頻率以符合您的預算。",
            "text-2" => "運輸頻率",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount% </div> 折扣'
        ],
        "option4_pause" => [
            "header" => "讓我們一起解決問題",
            "subheader-1" => "How about pausing your Shave Plan?",
            "text-1" => "Since you have too many blades in stock, how about we hold your next delivery for <label class=\"month\">:months months?</label>",
            "text-2" => "We'll also give you a <label class=\"discount\">:discount% </label> off your next billing cycle when your Shave Plan resumes."
        ],
        "option5" => [
            "header" => "別急著走！",
            "text-1" => "我們將以較低的價錢提供高質素的剃鬚刀，這樣您就不會超出預算了。",
            "text-2" => "您會留下嗎？",
            "text-3" => '我們也會在您下一個結算頻率提供<div class="discount">:discount% </div>折扣'
        ],
        "option6" => [
            "header" => "真的嗎？",
            "subheader-1" => "不如我們在您的下一次結算頻率折扣好嗎？",
            "text-1" => "最受歡迎選項",
            "text-2" => '下一個結算頻率提供<div class="discount">:discount% </div>折扣',
            "text-3" => '太好了！'
        ],
        "option7" => [
            "header" => "別急著走！",
            "text-1" => "我們將以較低的價錢提供高質素的剃鬚刀，這樣您就不會超出預算了。",
            "text-2" => "您會留下嗎？",
            "text-3" => '我們也會在您下一個結算週期提供<div class="discount">:discount% </div> 折扣'
        ],
        "cancelled-page" => [
            "header" => "很遺憾您要走了！",
            "subheader-1" => "感謝您選擇Shaves2U!",
            "subheader-2" => "如果你改變主意，我們就在這裡等你！"
        ],
        "blade-updated-page" => [
            "header" => "您剛更新了剃鬚計劃！",
            "subheader-1" => "您已經更改了刀頭類型，",
            "subheader-2" => "希望這更適合您的剃鬚習慣。"
        ],
        "free-product-applied-page" => [
            "header" => "我們正在處理您的新剃鬚刀！",
            "subheader-1" => "你的滿意度對我們很重要，",
            "subheader-2" => "希望您會發現這新的剃鬚刀比之前更好。"
        ]

    ]
];
