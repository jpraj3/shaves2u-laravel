<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
     */
    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
     */

    'custom' => [
        'validation' => [
            "email" => [
                "required" => "請輸入有效的電郵地址。",
                "email" => '請輸入有效的電郵地址。',
                "plural_test" => 'Be careful, you have :attempts attempt left.|You still have :attempts attempts left.',
                "maxlength" => 'The email may not be more than :max characters.',
                'email_not_exists' => '此電郵不存在。',
                'email_not_active' => '請激活電郵以登入帳戶。',
                'email_exists' => '電郵已曾註冊，請登入帳戶。'
            ],
            "password" => [
                "required" => '請輸入密碼',
                // "minlength" => "必須包含數字，大寫及小寫英文字母。",
                // "maxlength" => 'The :attribute may not be more than :max characters.',
                // "validpassword" => '必須包含數字，大寫及小寫英文字母。',
                "minlength" => "您的密碼必須介於8到20個字符之間，至少包含一個數字，大寫字母和符號。",
                "maxlength" => '您的密碼必須介於8到20個字符之間，至少包含一個數字，大寫字母和符號。',
                "validpassword" => '您的密碼必須介於8到20個字符之間，至少包含一個數字，大寫字母和符號。',
            ],
            "name" => [
                "required" => "請確認您的姓名。",
                "maxlength" => 'The :attribute may not be more than :max characters.',
            ],
            "birthday" => [
                "required" => '請輸入您的生日日期。',
            ],
            "terms_consent" => [
                "required" => 'You must agree to the Terms of Use, Privacy Policy, Personal Information Transfer and minimum age.',
            ],
            "contact_number" => [
                "required" => '請輸入有效聯絡電話。',
                "minlength" => '聯絡電話不能多或少於:min位數。',
                "maxlength" => '聯絡電話不能多或少於:max位數。',
                "allowzero" => '電話號碼有誤。',
            ],
            "address" => [
                "name" => [
                    "required" => '請輸入有效地址。',
                    "maxlength" => 'The :attribute may not be more than :max characters.',
                ],
                "ssn" => [
                    "required" => '請輸入有效地址。',
                ],
                "phone" => [
                    "required" => '請輸入有效聯絡電話。',
                    "minlength" => '聯絡電話不能多或少於:min位數。',
                    "maxlength" => '聯絡電話不能多或少於:max位數。',
                    "allowzero" => '電話號碼有誤。',
                ],
                "address" => [
                    "required" => '請輸入有效地址。',
                    "maxlength" => 'The :attribute may not be more than :max characters.',
                ],
                "city" => [
                    "required" => '請輸入有效地址。',
                    "maxlength" => 'The :attribute may not be more than :max characters.',
                ],
                "portalCode" => [
                    "required" => '請輸入有效地址。',
                    "maxlength" => ':attribute must be less than :length',
                ],
                "state" => [
                    "required" => '請輸入有效地址。',
                ],
            ],
            "card" => [
                "cardnumber" => [
                    "required" => '請輸入付款卡號碼。',
                    "maxlength" => '付款卡號碼不能多於16位數。',
                    "minlength" => '付款卡號碼不能少於:minlength位數。'
                ],
                "cvv" => [
                    "required" => '請輸入CCV。',
                    "maxlength" => 'CVV不能多於4位數。',
                    "minlength" => 'CVV不能少於3位數。'
                ],
                "cardexpiry" => [
                    "required" => '請輸入付款卡的有效日期。',
                    "maxlength" => '付款卡的有效日期不能多於4位數。',
                    "minlength" => '付款卡的有效日期不能少於4位數。'
                ],
            ],
            "contact" => [
                "name" => [
                    "required" => '我們該如何稱呼您?',
                ],
                "email" => [
                    "required" => '請輸入有效的電郵地址。',
                ],
                "phone" => [
                    "required" => '請輸入有效的電話號碼。',
                ],
                "enquiry" => [
                    "required" => '有什麼你想讓我們知道的？',
                ],
            ],
            "plan" => [
                'frequency' => [
                    'required' => 'Please select frequency.',
                ],
            ],
            "promo" => [
                'invalid' => '請輸入有效優惠代碼',
                'valid' => '優惠代碼成功確認！',
            ],
            "referral" => [
                'bank_name' => [
                    "required" => '请输入收款銀行名稱。',
                ],
                'withdrawal_name' => [
                    "required" => '请输入銀行註冊姓名。',
                ],
                'bank_account_no' => [
                    "required" => '请输入銀行戶口號碼。',
                ],
            ],
        ],
    ],

    /*
|--------------------------------------------------------------------------
| Custom Validation Attributes
|--------------------------------------------------------------------------
|
| The following language lines are used to swap our attribute placeholder
| with something more reader friendly such as "E-Mail Address" instead
| of "email". This simply helps us make our message more expressive.
|
 */
];
