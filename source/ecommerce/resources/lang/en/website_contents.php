<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for front end
    | content.
    |
     */
    'referral_whatsapp_desc' => 'Check out the new shave plan that I just subscribed for only RM6.50. Never thought such premium shaves can save me so much!',
    'referral_twitter_desc' => 'Check out the new shave plan that I just subscribed for only RM6.50. Never thought such premium shaves can save me so much!',
    'helloworld' => 'Hello World',
    'language' => 'Language',
    'global' => [
        'card_action_messages' => [
            'set_as_default' => 'You have successfully set this card as default. All existing subscriptions will be moved automatically to your new default card.',
            'card_deleted' => 'You have successfully deleted your card.',
            'card_added' => 'You have successfully added a new card and set it as default. All existing subscriptions will be moved automatically to your new default card.',
            'card_added_subscriptions' => 'You have successfully added a new card. This subscription will be moved to your new card.',
            'card_add_failed' => 'Your attempt to add a new card was unsuccessful. Please try again.'
        ],
        'address_action_messages' => [
            'set_as_default_shipment' => 'You have successfully set this address as default shipment address.',
            'set_as_default_billing' => 'You have successfully set this address as default billing address.',
            'address_deleted' => 'You have successfully deleted your address.',
            'address_added' => 'You have successfully added a new address.',
            'address_add_failed' => 'Your attempt to add a new address was unsuccessful. Please try again.'
        ],
        'invalid_card' => 'Invalid Card',
        'link_copied' => 'Link copied!',
        'stripe_payment_fail' => 'Sorry, your payment was unsuccessful. Please update your credit card information and try again.',
        'include_all_deliveries' => 'Include for all deliveries',
        'close' => 'CLOSE',
        'not_country_origin_purchase' => 'Your purchase could not be completed as you are not in your registered country. <br/> Please make a purchase only in the country that you\'ve registered.',
        'not_country_origin_plan' => 'Your request to view/edit the plan could not be completed as the country of origin does not match the country of the plan.',
        'blade' => ':blade Blade',
        'bladeV2' => 'Blade',
        'dates' => [
            'settings' => [
                'dd' => 'DD',
                'mm' => 'MM',
                'yyyy' => 'YYYY'
            ],
            'months' => [
                'jan' => 'Jan',
                'feb' => 'Feb',
                'mar' => 'Mar',
                'apr' => 'Apr',
                'may' => 'May',
                'jun' => 'Jun',
                'jul' => 'Jul',
                'aug' => 'Aug',
                'sep' => 'Sep',
                'oct' => 'Oct',
                'nov' => 'Nov',
                'dec' => 'Dec',
            ],
        ],
        'reset_password' => [
            'got_mail' => 'You\'ve Got Mail',
            'got_mail_desc' => 'Check your inbox in a few moments and follow the simple instructions in our email to reset your password.',
            'got_mail_return' => 'RETURN TO SIGN IN PAGE',
            'confirm_pw' => 'Create a new password',
            'confirm_pw_desc' => 'Please include at least 1 upper case and numeric in your password.',
            'confirm_pw_new_pw' => 'New password',
            'confirm_pw_retype_pw' => 'Retype new password',
            'confirm_pw_save' => 'SAVE',
            'confirm_pw_password' => 'Retype New Password',
            'reset-password' => 'Reset Password',
        ],
        'trial_price' => ':currency:price',
        'trial_priceV2' => ':price:currency',
        'shave_plans' => [
            'what_like_do' => 'What would you like to do?',
            'start_trial_kit' => 'Start a Shave Plan with our Trial Kit',
            'get_blade_refills' => 'Get blade refills',
            'continue' => 'Continue',
        ],
        'header' => [
            'get_started' => 'Get Started',
            'products' => 'Products',
            'help' => 'Help',
            'referral' => 'Referral',
            'login' => 'Login',
            'magazine' => 'Magazine'
        ],
        'content' => [
            'start_now' => 'START NOW',
            'load_more' => 'LOAD MORE',
            'welcome' => 'Welcome',
            'back' => 'Back',
            'next' => 'Next',
            'save' => 'Save',
            'submit' => 'Submit',
            'cancel' => 'Cancel',
        ],
        'faq' => [
            'know_more' => 'Need to know more?',
            'curious' => 'Curious to know more about us?<br>Read on below ',
            'how_sp_work' => 'How does a Shave Plan work?',
            'how_sp_work_ans' => 'There are 2 ways to start a Shave Plan.<br><br>
                    If you’re new to Shaves2U, start with a Starter Kit. It comes with everything you need
                    to
                    get started, and a 14-day trial period.<br><br>
                    If you already have a Shaves2U handle, you can just order a refill of our amazing blades
                    instead.<br><br>
                    Choose your preferred type of blade (3 Blade, 5 Blade or 6 Blade) and how often you’ll
                    need
                    refills (2, 3, 4, 6 months) depending on your shaving needs. We’ll bill your credit card
                    before each delivery.<br><br>
                    You are always in control of your plan. Change or cancel any time.',
            'how_long_arrive' => 'How long will my order take to arrive?',
            'how_long_arrive_ans' => 'Your delivery will usually reach you within 5 working days.<br> <br>However, shipping may
                    take
                    up to 7 working days in certain regions. The moment your order leaves our warehouse, we
                    will
                    provide you with a tracking number that you can use to stay up to date with its status.',
            'which_locations' => 'Which locations do you ship to?',
            'which_locations_ans' => 'For now, we only ship to Malaysia, Singapore, Korea, Hong Kong and Taiwan. However, we&#39re
                    looking to expand our services to other countries in the near future. Want premium, yet
                    affordable shavers in your country? Let us know at <a href="mailto:marketing@shaves2u.com" style="color: #FE5000">marketing@shaves2u.com</a>.',
            'offer_free_delivery' => 'Do you offer free delivery?',
            'offer_free_delivery_ans' => 'Yes we do! You can enjoy FREE shipping for all orders.',
            'how_refer_friend' => 'How do I refer a friend?',
            'how_refer_friend_ans' => 'You can refer a friend by first clicking on the Referral option on the top menu bar. You will
                    then be directed to the Referral dashboard. Here you will be provided with the referral link
                    where you can share it with your friends and family. Anyone who gains access to the site
                    from your link will be automatically categorised as your referral.',
            'how_much_earn' => 'Is there a limit to how much I can earn?',
            'how_much_earn_ans' => 'No, there is no limit to how much you can earn. Refer more and gain as much as you can!',
            'how_keep_track' => 'How do I keep track of my earning?',
            'how_keep_track_ans' => 'You can keep track of your earning from the Referral page. In the event that you have
                    referred new subscribers to sign-up for our subscription plan (“Referral Program”), you
                    shall be entitled for a credit amount of :currency:amount per subscriber upon satisfaction of the
                    following conditions:</br></br>

                    1.	The referral code is provided by the new subscriber to us upon sign-up; and
                    2.	We have successfully managed to charge the new subscriber on his first billing cycle. </br></br>

                    Upon satisfaction of the abovementioned conditions, the said amount shall be credited
                    into your account and you may utilise the amount to offset against any fees due on your
                    next billing cycle (of which any remaining balances shall be settled by you).</br>
                    If any cancellations have been made by the new subscriber on his subscriptions before his
                    first billing cycle, you shall not be entitled to any credit amount under this Referral
                    Program.',
            'how_cash_out' => 'How do I cash out?',
            'how_cash_out_ans' => 'From the Referral page, you can access the Cash Out button to withdraw your earnings.
                    Upon clicking the button, you will have to fill in the forms which include your banking
                    details. If you have provided the required information, the button will change to ‘In
                    Process’ until your earnings have been credited to your account which will take up to 5
                    working days. ',
        ],
        'footer' => [
            'contact_us' => 'Contact Us',
            'terms_of_use' => 'Terms of Use',
            'privacy' => 'Privacy',
            'faq' => 'FAQ',
            'all_rights_reserved' => '© 2019 Shaves2U. All Rights Reserved.',
        ],
    ],
    'authentication' => [
        'content' => [
            'create_account' => 'Create an account for quick checkout,<br>tracking your orders and more.',
            'email' => 'Email',
            'email_address' => 'Email Address',
            'or' => 'OR',
        ],
        'login' => [
            'enter_details' => 'Log in to your account',
            'password' => 'Password',
            'login' => 'LOGIN',
            'forgot_password' => 'Forgot Password',
            'login_facebook' => 'Login with Facebook',
            'new_sign_up' => 'Are you new here? Sign up now!',
        ],
        'register' => [
            'register' => 'Register',
            'name' => 'Name',
            'create_password' => 'Create Password',
            'confirm_password' => 'Retype New Password',
            'date_of_birth' => 'Date of Birth',
            'create_account' => 'Create Account',
            'accept_disclaimer' => 'By creating an account, you accept our',
            'accept_disclaimer_terms' => 'Terms of Use',
            'accept_disclaimer_and' => ' and ',
            'accept_disclaimer_privacy' => 'Privacy Policy',
            'marketing_sub_agreement' => 'I would like to receive marketing materials from Shaves2U including for future events, promotions, updates and any other marketing activities regarding Shaves2U’s products, services and/or events.',
            'signup_facebook' => 'Sign up with Facebook',
            'have_login' => 'Have an account? Log in here',
        ],
        'forgot_password' => [
            'title' => 'Forgot your password? Don\'t worry!',
            'sub_title' => 'Enter your email to receive instructions on how to reset your password.',
            'instructions' => 'Enter your email to receive instructions on how to reset your password.',
            'reset_link' => 'Submit',
        ],
    ],
    'landing' => [
        'main' => [
            'how_it_works' => 'How it works',
            'how_it_works_sub_1' => 'Shaves2U brings you a unique shaving experience tailored for your grooming needs.',
            'how_it_works_sub_2' => 'Get started in just three simple steps!',
        ],
        'hero_section' => [
            'better_shave' => 'A better shave, delivered.',
            'start_shave' => 'Start your Shave Plan for just',
            'looking_blade_refills' => 'Looking for blade refills?',
            'landing_trial_price' => ':currency:price',
        ],
        'start_trial' => [
            'how_it_works' => 'How it works',
            'shaving_experience' => 'Shaves2U brings you a unique shaving experience tailored for your grooming needs.',
            'shaving_experience_mobile' => 'Shaves2U brings you a unique<br>shaving experience tailored<br>for your grooming needs.',
            'get_started_steps' => 'Get started in just three simple steps!',
            'step_1' => 'Step 1',
            'step_1_build_plan' => 'Build your plan',
            'step_1_desc' => 'Choose the frequency of delivery you desire.',
            'step_2' => 'Step 2',
            'step_2_try' => 'Try for 2 weeks',
            'step_2_desc' => 'Start by selecting the ideal blade type for your shaving needs.',
            'step_3' => 'Step 3',
            'step_3_control' => 'Be in Control',
            'step_3_desc' => 'Change or stop your plan at any time from your profile page.',
            'not_typical' => 'We&#39re not your typical Shaving Company',
            'quality' => 'Quality',
            'razor_blades' => 'Razor-sharp blades engineered to perfection in the USA and Germany.',
            'affordable' => 'Affordable',
            'savings_40_leading' => 'Savings up to 40% less than the leading brand, and FREE shipping.',
            'convenience' => 'Convenience',
            'control_products' => 'You’re in control. Any time you want, add and remove products, plus adjust how often you get restock products.',
            'stop_ripoff' => 'Stop the Razor Rip-Off',
            'sell_direct' => 'We sell our blades to you directly. No middleman, no hidden fees. Just quality blades at an honest price.',
            '5blade_pack' => '5-Blade Cartridge Pack (4pcs)',
            'average_price' => '*Average price per replacement cartridge pack for Gillette Fusion ProGlide online across Lazada, 11Street, Watsons and Guardian',
            'get_underway' => 'Get under way with our Starter Kit',
            'new_shaving_experience' => 'A new shaving experience with your own ergonomic handle, razor-sharp blade, and bubbly shave cream for only :currency:price',
            'free_text' => 'For the longest time, most shaving brands sell their products through retail stores or some form of
                    a middleman. To cover the hidden fees and costs, these shaving companies hike up the prices of their
                    products, which leads to expensive shaving supplies for the customers.
                    <br/><br/>
                    But Shaves2U isn&#39t your average shaving brand; we want to put a stop to this razor rip-off. Shaves2U
                    believes in providing high-quality shaving kit at affordable prices for our customers. By ordering
                    from Shaves2U, you&#39re getting the best shaver Malaysia has to offer and other shaving supplies
                    delivered straight to you at a low price with no extra charges.
                    <br/><br/>
                    When you subscribe to a Shaves2U shave plan, you can say goodbye to overpriced razors and save up to
                    40% less than the leading brand!
                    <br/><br/>
                    Are you interested in the Shaves2U shave plan but not sure how it works? Let us guide you in getting
                    a unique shaving experience that suits your grooming needs. Read our step-by-step guide to get a
                    shaver in Malaysia that’s made just for you!
                    <br/><br/>
                    The first step is to build your shave plan with the Starter Kit. You&#39ll have to choose between a
                    three-blade, five-blade, or the six-blade cartridge that fits the swivel handle. Did you know our
                    razor-sharp blades are engineered to perfection in the USA and Germany? This means our razors
                    challenges all the shaver in Malaysia. Besides the blade cartridges and the swivel handle, the
                    starter kit also comes with a tube of shave cream.
                    <br/><br/>
                    For the second step, you&#39ll have to choose your next refill shipment. Your refill can be done in
                    three ways. The first refill option is to get only refills for blade cartridges of your choice. The
                    second refill option includes a tube of shave cream and blade cartridges. For the last refill
                    option, you have an extra tube of after shave cream along with shave cream and blade cartridges.
                    After choosing your refill options, try for your shave plan for two weeks!
                    <br/><br/>
                    For the third and final step, you can decide the frequency of the delivery based on how often you
                    shave. Shaves2U delivers as frequent as every two months, but you can choose the frequency up to six
                    months. For example, for men that shaves every five to seven times per week, we&#39ll deliver your
                    products every two months to fit your shaving needs.
                    <br/><br/>
                    Not only are we aiming to make a high quality shaver in Malaysia low-cost, but we also want to make
                    the experience convenient for everyone. Once you subscribe to a Shaves2U shave plan, you are in full
                    control of your shave plan. Feel to modify or change your shave plan whenever you want. For example,
                    if you decide not to shave so often anymore, you can decrease the delivery frequency and switch to
                    the five-blade cartridge. You can also stop your subscription at any time.
                    <br/><br/>
                    With just the low price of RM6.50, you can get the Starter Kit and start your Shaves2U plan. No
                    delivery-fee included! As we don&#39t have retail stores at the moment, all Shaves2U product orders are
                    done online on our website. For payment, we accept all debit and credit cards. Experience the best
                    shaver in Malaysia with Shaves2U!',
        ],
        'referral' => [
            'referral_program' => 'Shaves2u<br>Referral Program',
            'good_things' => 'Good things in life are meant to be shared',
            'easy_commissions' => 'Earn easy commissions',
            'easy_commissions_desc' => 'Start earning cash instantly when your<br>friends register for a Shaves2u account.',
            'leverage_network' => 'Leverage your network',
            'leverage_network_desc' => 'Share your shaving story with your peers through Facebook, Whatsapp, Twittter or Email.',
            'make_money' => 'Make more money',
            'make_money_desc' => 'The sky&#39s the limit! The more you<br>share, the more you earn.',
            'how_it_works' => 'How it works',
            'get_started_steps' => 'Get started in just three simple steps!',
            'step_1' => 'Step 1',
            'step_1_signup' => 'Sign up for an account',
            'step_1_desc' => 'Begin your journey by easily signing up<br>for a Shaves2U account.',
            'step_2' => 'Step 2',
            'step_2_share' => 'Share your unique link',
            'step_2_desc' => 'Spread the good news by sharing<br>your unique referral link to your<br>friends and family.',
            'step_3' => 'Step 3',
            'step_3_earn' => 'Earn cash for each<br>successful sign up',
            'step_3_desc' => 'Get rewarded with :currency:amount in cash for anyone<br>who signs up through your link and completes<br>their first billing cycle.',
            'step_3_desc_mobile' => 'Get rewarded with :currency:amount in cash for anyone who signs upthrough your link and completes their first billing cycle.',
            'start_journey' => 'Start your journey and make<br>some extra cash today!',
            'start_journey_mobile' => 'Start your journey<br>and make some<br>extra cash today!',
        ],
        'terms_of_use' => [
            'terms_conditions' => 'TERMS & CONDITIONS',
            'overview' => 'OVERVIEW',
            'tnc_overview_p1' => 'This website is operated by Shaves2U Sdn Bhd. Throughout the site, the Terms “we”, “us” and “our” shall refer to Shaves2U Sdn Bhd. Shaves2U Sdn Bhd offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all Terms, conditions, policies and notices stated here.',
            'tnc_overview_p2' => 'By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.',
            'tnc_overview_p3' => 'Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to the Terms of Service, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.',
            'tnc_overview_p4' => 'Any new features or tools which are added to the current store shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.',
            'interpretation' => 'INTERPRETATION',
            'tnc_interpretation_p1' => '"<b>Contract</b>" shall mean the contract for the purchase and sale of Goods, howsoever formed or concluded.',
            'tnc_interpretation_p2' => '"<b>Goods</b>" shall mean the Goods which Shaves2U Sdn Bhd is to supply in accordance with the Contract.',
            'sect1_ost' => 'SECTION 1 - ONLINE STORE TERMS',
            'tnc_sect1_ost_p1' => 'By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.',
            'tnc_sect1_ost_p2' => 'You may not use our Goods for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).',
            'tnc_sect1_ost_p3' => 'You must not transmit any worms or viruses or any code of a destructive nature.',
            'tnc_sect1_ost_p4' => 'A breach or violation of any of the Terms of Service will result in an immediate termination of your Contract.',
            'sect2_gc' => 'SECTION 2 - GENERAL CONDITIONS',
            'tnc_sect2_gc_p1' => 'We reserve the right to refuse service to anyone for any reason at any time.',
            'tnc_sect2_gc_p2' => 'You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.',
            'tnc_sect2_gc_p3' => 'You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.',
            'tnc_sect2_gc_p4' => 'The headings used in this agreement are included for convenience only and will not limit or otherwise affect the Terms of Service.',
            'sect3_acti' => 'SECTION 3 - ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION',
            'tnc_sect3_acti_p1' => 'We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk.',
            'tnc_sect3_acti_p2' => 'This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.',
            'sect4_msp' => 'SECTION 4 - MODIFICATIONS TO THE SERVICE AND PRICES',
            'tnc_sect4_msp_p1' => 'Prices for our Goods are subject to change without notice.',
            'tnc_sect4_msp_p2' => 'We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.',
            'tnc_sect4_msp_p3' => 'We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.',
            'tnc_sect4_msp_p4' => '',
            'sect5_gs' => 'SECTION 5 - GOODS OR SERVICES (if applicable)',
            'tnc_sect5_goodserv_p1' => 'Certain Goods or services may be available exclusively online through the website. These Goods or services may have limited quantities and are subject to return or exchange only according to our Return Policy.',
            'tnc_sect5_goodserv_p2' => 'We have made every effort to display as accurately as possible the colours and images of our Goods that appear at the store. We cannot guarantee that your computer monitor&#39s display of any colour will be accurate.',
            'tnc_sect5_goodserv_p3' => 'We reserve the right, but are not obligated, to limit the sales of our Goods or services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any Goods or services that we offer. All descriptions of Goods or Goods pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right to discontinue any Goods at any time. Any offer for any Goods or service made on this site is void where prohibited.',
            'tnc_sect5_goodserv_p4' => 'We do not warrant that the quality of any Goods, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.',
            'sect6_price' => 'SECTION 6 - PRICE',
            'tnc_sect6_price_p1' => 'The price of the Goods shall be the price stated in our website at the time which you make your offer to purchase the Goods to us plus any delivery charges, any applicable Goods and services tax, value added tax or similar tax which you shall be liable to pay to us in addition to the price.',
            'sect7_orderspec' => 'SECTION 7 - ORDERS AND SPECIFICATIONS',
            'tnc_sect7_orderspec_p1' => 'Order acceptance and completion of the contract between you and us will only be completed upon us issuing a confirmation of dispatch of the Goods to you. For the avoidance of doubt, we shall be entitled to refuse or cancel any order without giving any reasons for the same to you prior to issue of the confirmation of dispatch. We shall furthermore be entitled to require you to furnish us with contact and other verification information, including but not limited to address, contact numbers prior to issuing a confirmation of dispatch.',
            'tnc_sect7_orderspec_p2' => 'No concluded contract may be modified or cancelled by you except with the agreement in writing from us.',
            'sect8_deliperf' => 'SECTION 8 – DELIVERY/PERFORMANCE',
            'tnc_sect8_deliperf_p1' => 'Delivery of the Goods shall be made to the address specified by you in your order.',
            'tnc_sect8_deliperf_p2' => 'We have the right at any time to sub-contract all or any of its obligations for the delivery of the Goods to any other party as it may from time to time decide without giving notice of the same to you.',
            'tnc_sect8_deliperf_p3' => 'Any dates quoted for delivery of the Goods are approximate only. The time for delivery/performance shall not be of the essence, and we shall not be liable for any delay in delivery or performance howsoever caused.',
            'tnc_sect8_deliperf_p4' => 'If we failed to deliver the Goods in accordance with the contract or within a reasonable time, you shall be entitled, by serving written notice on us, to demand performance within a specified time thereafter, which shall be at least 14 days. If we failed to do so within the specified time, you shall be entitled to terminate the contract in respect of the undelivered Goods and claim a refund.',
            'sect9_abai' => 'SECTION 9 - ACCURACY OF BILLING AND ACCOUNT INFORMATION',
            'tnc_sect9_abai_p1' => 'We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.',
            'tnc_sect9_abai_p2' => 'You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.',
            'tnc_sect9_abai_p3' => 'For more detail, please review our Returns Policy.',
            'sect10_termspay' => 'SECTION 10 - TERMS OF PAYMENT',
            'tnc_sect10_termspay_p1' => 'You shall be required to make payment for the Goods at the time of order.',
            'tnc_sect10_termspay_p2' => 'We accept all payments via Credit and Debit Card. When an order is placed via Credit Card on our website, the transaction shall be processed by Stripe. This system is certified and allows us to accept payments such as Visa, MasterCard and Amex. All credit card numbers shall be protected by means of industry-leading encryption standards.',
            'sect11_mbg' => 'SECTION 11 - MONEY BACK GUARANTEE',
            'tnc_sect11_mbg_p1' => 'If you are unhappy with any of our Goods for any reason, we will refund the amount paid for purchasing the item excluding the shipping fee / processing fee (if applicable). Refund requests must be made directly to us at ',
            'tnc_sect11_mbg_p2' => 'All refund requests must be made within thirty (30) days of the date of shipment by us. All refund requests must be accompanied by a return of the Goods and original receipt of purchase to:',
            'tnc_sect11_mbg_address' => 'LF Logistics, 43-44, Lengkuk Keluli 1, Kawasan Perindustrian Bukit Raja Selatan, Seksyen 7, 40000 Shah Alam, Selangor DE, Malaysia.',
            'tnc_sect11_mbg_p3' => 'We shall process the request for refund within fourteen (14) days of the receipt of the returned Goods.',
            'tnc_sect11_mbg_p4' => 'We are not liable for Goods that are damaged or lost in transit. We will credit the amount paid for the returned Goods (less any shipping and handling costs which are non-refundable) via the payment method you used to make the original purchase.',
            'tnc_sect11_mbg_p5' => 'We do not control when a payment merchant processes a refund. You are responsible for contacting your credit card’s issuing bank if you have questions about the status of the refund.',
            'tnc_sect11_mbg_p6' => 'A refund request received more than thirty (30) days after the date of original shipment will not be entertained. We reserve our right to deny any request to refund for any returned Goods we deem damaged due to misuse, lack of care, mishandling, accident, abuse or other abnormal use.',
            'sect12_freetrial' => 'SECTION 12 - FREE TRIAL',
            'tnc_sect12_freetrial_p1' => 'Promotions or Free Trials that provide access to a Paid Service (“Free Trial”) must be used within the stipulated time of the Free Trial. You will be required to have a valid payment method on file in order to start your Free Trial. If you wish to cancel your Free Trial, please do so one (1) day before the next billing cycle in order to avoid being charged for a Paid Service.',
            'tnc_sect12_freetrial_p2' => 'If you cancel prior to the end of the Free Trial period and are inadvertently charged for a Paid Service, please contact us at ',
            'tnc_sect12_freetrial_p3' => 'The Free Trial Pack is a one-time only offer for new customers and is limited to one per household. Additional terms and limitations may apply.',
            'sect13_refprog' => 'SECTION 13 - REFERRAL PROGRAM',
            'tnc_sect13_refprog_p1' => 'In the event that you have referred new subscribers to sign-up for our subscription plan (“Referral Program”), you shall be entitled for a credit amount of MYR20 per subscriber upon satisfaction of the following conditions:',
            'tnc_sect13_refprog_p1_li1' => 'The referral code is provided by the new subscriber to us upon sign-up; and',
            'tnc_sect13_refprog_p1_li2' => 'We have successfully managed to charge the new subscriber on his first billing cycle.',
            'tnc_sect13_refprog_p2' => 'Upon satisfaction of the abovementioned conditions, the said amount shall be credited into your account and you may utilise the amount to offset against any fees due on your next billing cycle (of which any remaining balances shall be settled by you).',
            'tnc_sect13_refprog_p3' => 'If any cancellations have been made by the new subscriber on his subscriptions before his first billing cycle, you shall not be entitled to any credit amount under this Referral Program.',
            'sect14_ucfos' => 'SECTION 14 - USER COMMENTS, FEEDBACK AND OTHER SUBMISSIONS',
            'tnc_sect14_ucfos_p1' => 'If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, Plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, &#39comments&#39), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.',
            'tnc_sect14_ucfos_p2' => 'We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libellous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service.',
            'tnc_sect14_ucfos_p3' => 'You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libellous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.',
            'sect15_personalinfo' => 'SECTION 15 - PERSONAL INFORMATION',
            'tnc_sect15_personalinfo_p1' => 'Your submission of personal information through the store is governed by our Privacy Policy. To view our Privacy Policy please click on the following link:',
            'sect16_eio' => 'SECTION 16 - ERRORS, INACCURACIES AND OMISSIONS',
            'tnc_sect16_eio_p1' => 'Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to Goods descriptions, pricing, Promotions, offers, Goods shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).',
            'tnc_sect16_eio_p2' => 'We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.',
            'sect17_prohibuses' => 'SECTION 17 - PROHIBITED USES',
            'tnc_sect17_prohibuses_p1' => 'In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.',
            'sect18_warrantremed' => 'SECTION 18 - WARRANTIES AND REMEDIES',
            'tnc_sect18_warrantremed_p1' => 'Subject as expressly provided in this Terms of Service, all other warranties conditions or terms, including those implied by statute or common law, are excluded to the fullest extent permitted by law.',
            'tnc_sect18_warrantremed_p2' => 'Subject to Section 8 - Delivery/Performance, we warrant that the Goods will correspond with their specification at the time of delivery, and agree to remedy any non-conformity therein for a period of 1 month commencing from the date on which the Goods are delivered or deemed to be delivered ("Warranty Period").',
            'tnc_sect18_warrantremed_p3' => 'We shall be under no liability in respect of any defect arising from unsuitable or improper use, defective installation or commissioning by you or any third parties, fair wear and tear, wilful damage, negligence, abnormal working conditions, defective or negligent handling, failure to follow our instructions (whether oral or in writing) misuse or alteration of the Goods without our approval.',
            'tnc_sect18_warrantremed_p4' => 'We shall be under no liability whatsoever in respect of any defect in the Goods arising after the expiry of the Warranty Period.',
            'tnc_sect18_warrantremed_p5' => 'Any claims made by you which is based on any defect in the quality or condition of the Goods or their failure to correspond with specification shall be notified to us within seven days from the date of receipt of the Goods. During usage, the Goods shall be monitored constantly with regards to its safety and defects. If there are even slight reservations concerning the suitability for use or the slightest reservations concerning safety, the Goods must not be used. You are required to provide us with immediate written notification, specifying the reservations or the defect.',
            'tnc_sect18_warrantremed_p6' => 'In the event that no notification is given to us on the defects, we shall have no liability for any defect or failure or for any consequences resulting therefrom. Where any valid claim in respect of any of the Goods which is based on any defect in the quality or condition of the Goods or their failure to meet a specification is notified to us, the non-conforming Goods (or part thereof) will be replaced free of charge as originally ordered or refunded in full.',
            'sect19_dwll' => 'SECTION 19 - DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY',
            'tnc_sect19_dwll_p1' => 'We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.',
            'tnc_sect19_dwll_p2' => 'We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.',
            'tnc_sect19_dwll_p3' => 'You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.',
            'tnc_sect19_dwll_p4' => 'You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all Goods and services delivered to you through the service are (except as expressly stated by us) provided &#39as is&#39 and &#39as available&#39 for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.',
            'tnc_sect19_dwll_p5' => 'In no case shall Shaves2U Sdn Bhd, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any Goods procured using the service, or for any other claim related in any way to your use of the service or any Goods, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or Goods) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.',
            'tnc_sect19_dwll_p6' => 'We shall be under no liability whatsoever where this arises from a reason beyond our reasonable control as provided in Section 20 - Risk and Title of the Goods or from an act or default by you.',
            'tnc_sect19_dwll_p7' => 'In no event shall we be liable for loss of profit or goodwill, loss of Goods or revenue or any type of special indirect or consequential loss whatsoever (including loss or damage suffered by you as a result of an action brought by a third party) even if such loss were reasonably foreseeable or we had been advised of the possibility of you incurring the same.',
            'tnc_sect19_dwll_p8' => 'The remedies set out in Section 8 – Delivery / Performance are your sole and exclusive remedies for non-conformity of or defects in the Goods.',
            'tnc_sect19_dwll_p9' => 'Our maximum and cumulative total liability (including any liability for acts and omissions of its employees’ agents and sub-contractors) in respect of any and all claims for defective performance, breach of contract, compensation, indemnity, tort, misrepresentation, negligence at law or equity and any other damages or losses which may arise in connection with its performance or non-performance under the Contract, shall not exceed the total purchase price.',
            'sect20_rtg' => 'SECTION 20 - RISK AND TITLE TO THE GOODS',
            'tnc_sect20_rtg_p1' => 'Risk of damage to or loss of the Goods shall pass to you at the time of delivery or if you wrongfully failed to receive delivery of the Goods, the time when we have tendered delivery of the Goods.',
            'tnc_sect20_rtg_p2' => 'Notwithstanding delivery and the passing of risk in the Goods or any other provision of this Terms of Service, the title in the Goods shall not pass to you until we have received payment in full.',
            'sect21_indemnity' => 'SECTION 21 – INDEMNITY',
            'tnc_sect21_indemnity_p1' => 'You agree to indemnify, defend and hold harmless Shaves2U Sdn Bhd and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.',
            'sect22_severability' => 'SECTION 22 - SEVERABILITY',
            'tnc_sect22_severability_p1' => 'In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.',
            'sect23_termination' => 'SECTION 23 - TERMINATION',
            'tnc_sect23_termination_p1' => 'The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.',
            'tnc_sect23_termination_p2' => 'These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our services, or when you cease using our site.',
            'tnc_sect23_termination_p3' => 'If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our services (or any part thereof).',
            'sect24_entireagreement' => 'SECTION 24 - ENTIRE AGREEMENT',
            'tnc_sect24_entireagreement_p1' => 'The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision.',
            'tnc_sect24_entireagreement_p2' => 'These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).',
            'tnc_sect24_entireagreement_p3' => 'Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.',
            'sect25_forcemajure' => 'SECTION 25 - FORCE MAJEURE',
            'tnc_sect25_forcemajure_p1' => 'We shall not be liable to you or be deemed to be in breach of our Contract by reason of any delay in performing or any failure to perform any of our obligations if the delay or failure was due to any cause beyond our reasonable control. Without prejudice to the generality of the foregoing the following shall be regarded as causes beyond our reasonable control:',
            'tnc_sect25_forcemajure_p1_li1' => 'Act of God, explosion, flood, tempest, fire or accident;',
            'tnc_sect25_forcemajure_p1_li2' => 'War or threat of war, sabotage, insurrection, civil disturbance or requisition;',
            'tnc_sect25_forcemajure_p1_li3' => 'Acts of restrictions, regulations, bye-laws, prohibitions or measures of any kind on the part of any governmental parliamentary or local authority;',
            'tnc_sect25_forcemajure_p1_li4' => 'Import or export regulations or embargoes;',
            'tnc_sect25_forcemajure_p1_li5' => 'Interruption of traffic, strikes, lock-outs, other industrial actions or trade disputes (whether involving our employees or of a third party);',
            'tnc_sect25_forcemajure_p1_li6' => 'Interruption of production or operation, difficulties in obtaining raw materials labour fuel parts or machinery;',
            'tnc_sect25_forcemajure_p1_li7' => 'Power failure or breakdown in machinery.',
            'tnc_sect25_forcemajure_p2' => 'Upon the happening of any one of the events set out in this Section 25, we may at our option:',
            'tnc_sect25_forcemajure_p2_li1' => 'Fully or partially suspend delivery/performance while such event or circumstances continues;',
            'tnc_sect25_forcemajure_p2_li2' => 'Terminate any Contract so affected with immediate effect by written notice to you and we shall not be liable for any loss or damage suffered by you as a result thereof.',
            'sect26_governinglaw' => 'SECTION 26 - GOVERNING LAW',
            'tnc_sect26_governinglaw_p1' => 'These Terms of Service and any separate agreements whereby we provide you services shall be governed by and construed in accordance with the laws of Malaysia.',
            'sect27_cts' => 'SECTION 27 - CHANGES TO TERMS OF SERVICE',
            'tnc_sect27_cts_p1' => 'You can review the most current version of the Terms of Service at any time at this page.',
            'tnc_sect27_cts_p2' => 'We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.',
            'sect28_contactinfo' => 'SECTION 28 - CONTACT INFORMATION',
            'tnc_sect28_contactinfo_p1' => 'Questions about the Terms of Service should be sent to us at ',
            'sect29_notices' => 'SECTION 29 - NOTICES',
            'tnc_sect29_notices_p1' => 'Any notice required or permitted to be given by either party to the other under these Terms of Service shall be in writing addressed, if to us, to our principal place of business and if to the you, to the address stipulated in the relevant offer to purchase.',
            'sect30_others' => 'SECTION 30 - OTHERS',
            'tnc_sect30_others_p1' => 'No person who is not a party to this Contract (including any employee officer agent representative or sub-contractor of either party) shall have any right under the Contracts (Rights of Third Parties) Act to enforce any terms of this Contract which expressly or by implication confers a benefit on that person without the express prior agreement in writing of the parties.',
            'tnc_sect30_others_p2' => 'Any dispute, controversy or claim arising out of or relating to this contract, or the breach, termination or invalidity thereof shall be settled by arbitration in accordance with the Rules for Asian International Arbitration Centre (AIAC). The arbitral tribunal shall consist of a sole arbitrator, to be appointed by the Chairman of the AIAC. The place of arbitration shall be Malaysia. Any award by the arbitration tribunal shall be final and binding upon the parties.',
            'tnc_sect30_others_p3' => 'We shall be entitled to commence legal proceedings for the purposes of protecting our intellectual property rights and confidential information of which the remedies entitled by us shall include, without limitation, damages, injunctive or other equitable relief.',
            'tnc_sect30_others_p4' => 'The United Nations Convention on Contracts for the International Sale of Goods shall not apply to any contract for the sale of Goods.',
        ],
        'privacy' => [
            'privacy_policy' => 'PRIVACY POLICY',
            'introduction' => 'INTRODUCTION',
            'introduction_li1' => 'This is the privacy policy (“Privacy Policy”) of Shaves2u Sdn. Bhd. (“ <strong>Company</strong>”, “ <strong>we</strong>”, “ <strong>us</strong>”, or “ <strong>our</strong>”).',
            'introduction_li2' => 'To process, administer and/or manage your relationship with us, we will necessarily need to collect, use, disclose and/or process your personal data. This Privacy Policy applies to personal data about you (our clients, directors, shareholders, vendors, distributors, suppliers, contractors, service providers, business partners, etc) and/or individuals provided by you, possessed by us or that we obtain about you, whether now or in the future. We will only process your personal data in accordance with the Personal Data Protection Act 2010, the applicable regulations, guidelines, orders made under the Personal Data Protection Act 2010 and any statutory amendments or re-enactments made of the Personal Data Protection Act 2010 from time to time (collectively referred to as the “ <strong>PDPA</strong>”) as well as this Privacy Policy.',
            'introduction_li3' => 'If you are a corporate entity/an organisation, references to the term “you” and “your” shall also include your employees, representatives and agents.',
            'introduction_li4' => 'The PDPA requires us to inform you of your rights in respect of your personal data that is being processed or that is to be collected and further processed by us and the purposes for the data processing. The PDPA also requires us to obtain your consent to the processing of your personal data. In light of the PDPA, we are committed to protecting and safeguarding your personal data.',
            'introduction_li5' => 'By providing your personal data to us and/or continuing access to our website (“Site”), you declare that you have read and understood this Privacy Policy and agree to us processing your personal data in accordance with the manner as set out in this Privacy Policy.',
            'introduction_li6' => 'We reserve the right to modify, update and/or amend this Privacy Policy from time to time with reasonable prior notice to you. We will notify you of any amendments via announcements on the Site or other appropriate means. Please check the Site from time to time to see if there are amendments to this Privacy Policy. Any amendments to this Privacy Policy will be effective upon notice to you. By continuing to communicate with us, use our services, and/or access to the Site after being notified of any amendments to this Privacy Policy, you will be treated as having agreed to and accepted those amendments.',
            'introduction_li7' => 'It is necessary for us to collect and process your personal data. If you do not provide us with your personal data, or do not consent to this Privacy Policy or any amendments to this Privacy Policy, we may not be able to render all services to you and you may be required to terminate your relevant agreement with us and/or stop accessing or using the Site.',
            'collection' => 'COLLECTION OF PERSONAL DATA',
            'collection_li1' => 'The term “ <strong>personal data</strong>” means any information in our possession or control that relates directly or indirectly to an individual to the extent that the individual can be identified or are identifiable from that and other information in our possession such as name, address, telephone number, NRIC No, date of birth, email address, etc. The types of personal data collected depend on the purpose of collection. We may “process” your personal data by way of collecting, recording, holding, storing, using and/or disclosing it.',
            'collection_li2' => 'Your personal data may be collected from you during your course of dealings with us in any way or manner including pursuant to any transactions and/or communications made from/with us. We may also collect your personal data from a variety of sources, including without limitation, at any meetings, events, seminars, conferences, talks, road shows, customer satisfaction surveys organised and/or sponsored by us, as well as from publicly available sources.',
            'collection_li3' => 'In addition, we may also receive, store and process your personal data which are provided or made available by any third parties, credit reference bodies, regulatory and law enforcement authorities, for reasons including delivery of our services, performance of conditions of agreements and/or to comply with our legal and regulatory obligations.',
            'purpose' => 'PURPOSE OF ACQUIRING AND PROCESSING YOUR PERSONAL DATA',
            'purpose_li1' => 'The personal data as provided/furnished by you to us or collected by us from you or through such other sources as may be necessary for the fulfilment of the purposes at the time it was sought or collected, may be processed for the following purposes (collectively referred to as the “ Purposes”):',
            'purpose_li1_b1' => 'to assess, process and provide services to you;',
            'purpose_li1_b2' => 'to facilitate, process, deal with, administer, manage and/or maintain your relationship with us;',
            'purpose_li1_b3' => 'to consider and/or process your application/transaction with us;',
            'purpose_li1_b4' => 'to respond to your enquiries or complaints or resolve any issues and disputes which may arise in connection with any dealings with us;',
            'purpose_li1_b5' => 'to administer and process any payments related to services requested by you;',
            'purpose_li1_b6' => 'to facilitate your participation in, and our administration of, any events organised by us;',
            'purpose_li1_b7' => 'to conduct credit reference checks and establish your credit worthiness, where necessary, in providing you with the services;',
            'purpose_li1_b8' => 'to carry out due diligence or other monitoring or screening activities (including background checks) in accordance with legal or regulatory obligations or risk management procedures that may be required by law or that may have been put in place by us;',
            'purpose_li1_b9' => 'to administer and give effect to your commercial transactions with us;',
            'purpose_li1_b10' => 'to process any payments related to your commercial transactions with us;',
            'purpose_li1_b11' => 'to provide you with information and/or updates on our services and/or events which may be of interest to you from time to time by SMS, phone call, email, fax, mail, social media and/or any other appropriate communication channels;',
            'purpose_li1_b12' => 'to send you seasonal greetings messages, gifts, newsletters from time to time;',
            'purpose_li1_b13' => 'to send you invitation to join our events;',
            'purpose_li1_b14' => 'to monitor, review and improve our services and/or events;',
            'purpose_li1_b15' => 'to process and analyse your personal data either individually or collectively with other individuals;',
            'purpose_li1_b16' => 'to conduct market research or surveys, internal marketing analysis, client profiling activities, analysis of client patterns and choices, planning and statistical and trend analysis in relation to our services;',
            'purpose_li1_b17' => 'to share any of your personal data with the auditor for our internal audit and reporting purposes;',
            'purpose_li1_b18' => 'to share any of your personal data pursuant to any agreement or document which you have duly entered with us for purposes of seeking legal and/or financial advice and/or for purposes of commencing legal action;',
            'purpose_li1_b19' => 'to share any of your personal data with a third party necessary for the preparation of legal documents or contract to be entered by you;',
            'purpose_li1_b20' => 'to share any of your personal data with our business partners to jointly develop services or launch marketing campaigns;',
            'purpose_li1_b21' => 'to share any of your personal data with insurance companies necessary for the purpose of applying and obtaining insurance policy(ies), if necessary;',
            'purpose_li1_b22' => 'to share any of your personal data with financial institutions necessary for the purpose of applying and obtaining credit facility(ies), if necessary;',
            'purpose_li1_b23' => 'to communicate with you and to maintain and improve client relationship;',
            'purpose_li1_b24' => 'to maintain and update internal record keeping, files and contact lists;',
            'purpose_li1_b25' => 'to detect, investigate and prevent any fraudulent, prohibited or illegal activity or omission or misconduct;',
            'purpose_li1_b26' => 'to enable us to perform our obligations and enforce our rights under any agreements or documents that we are a party to;',
            'purpose_li1_b27' => 'to transfer or assign our rights, interests and obligations under any agreements entered into with us;',
            'purpose_li1_b28' => 'to meet any applicable legal or regulatory requirements and making disclosure under the requirements of any applicable law, regulation, direction, court order, by-law, guideline, circular or code applicable to us;',
            'purpose_li1_b29' => 'to comply with or as required by any request or direction of any governmental authority; or responding to requests for information from public agencies, ministries, statutory bodies or other similar authorities;',
            'purpose_li1_b30' => 'to enforce or defend our rights and your rights under, and to comply with, our obligations under the applicable laws, legislation and regulations;',
            'purpose_li1_b31' => 'for direct marketing purposes via SMS, phone call, email, fax, mail, social media and/or any other appropriate communication channels;',
            'purpose_li1_b32' => 'for internal administrative purposes;',
            'purpose_li1_b33' => 'for audit, risk management and security purposes;',
            'purpose_li1_b34' => 'for registration for a user account with us;',
            'purpose_li1_b35' => 'for our storage, hosting back-up (whether for disaster recovery or otherwise) of your personal data, whether within or outside Malaysia; and/or',
            'purpose_li1_b36' => 'for other purposes required to operate, maintain and better manage our business and your relationship with us,',
            'purpose_li1_cont' => 'and you agree and consent to us using and processing your personal data for the Purposes in the manner as identified in this Privacy Policy. If you do not consent to us processing your personal data for one or more of the Purposes, please notify us at the contact details below.',
            'purpose_li2' => 'We will seek your separate consent for any other purposes which do not fall within the categories stated above.',
            'purpose_li3' => 'We may also be collecting from sources other than yourself, personal data about you, for one or more of the above Purposes, and thereafter using, disclosing and/or processing such personal data for one or more of the above Purposes.',
            'consequences' => 'CONSEQUENCES OF NOT CONSENTING TO THIS PRIVACY POLICY',
            'consequences_desc' => 'The collection of your personal data by us may be mandatory or voluntary in nature depending on the Purposes for which your personal data is collected. Where it is mandatory for you to provide us with your personal data, and you fail or choose not to provide us with such data, or do not consent to the above or this Privacy Policy, we will not be able to provide our services or otherwise deal with you, if at all.',
            'disclosure' => 'DISCLOSURE OF YOUR PERSONAL DATA',
            'disclosure_desc' => 'We will not sell, rent, transfer or disclose any of your personal data to any third party without your consent. However, we may disclose your personal data to the following third parties, for one or more of the above Purposes:',
            'disclosure_desc_b1' => 'the Company’s group of companies including the Company’s parent/holding company, related and/or associated companies;',
            'disclosure_desc_b2' => 'your immediate family members and/or emergency contact person as may be notified to us from time to time;',
            'disclosure_desc_b3' => 'successors in title to us;',
            'disclosure_desc_b4' => 'any person under a duty of confidentiality to which has undertaken to keep your personal data confidential which we have engaged to discharge our obligations to you;',
            'disclosure_desc_b5' => 'any party in relation to legal proceedings or prospective legal proceedings;',
            'disclosure_desc_b6' => 'our auditors, consultants, lawyers, accountants or other financial or professional advisers appointed in connection with our business on a strictly confidential basis, appointed by us to provide services to us;',
            'disclosure_desc_b7' => 'any party nominated or appointed by us either solely or jointly with other service providers, for purpose of establishing and maintaining a common database where we have a legitimate common interest;',
            'disclosure_desc_b8' => 'data centres and/or servers located within or outside Malaysia for data storage purposes;',
            'disclosure_desc_b9' => 'storage facility and records management service providers;',
            'disclosure_desc_b10' => 'payment channels including but not limited to banks and financial institutions for purpose of assessing, verifying, effectuating and facilitating payment of any amount due to us;',
            'disclosure_desc_b11' => 'government agencies, law enforcement agencies, courts, tribunals, regulatory/professional bodies, industry regulators, ministries, and/or statutory agencies or bodies, offices or municipality in any jurisdiction, if required or authorised to do so, to satisfy any applicable law, regulation, order or judgment of a court or tribunal or queries from the relevant authorities;',
            'disclosure_desc_b12' => 'our business partners, third party product and/or service providers, suppliers, vendors, distributors, contractors or agents, on a need to know basis, that provide related products and/or services in connection with our business, or discharge or perform one or more of the above Purposes and other purposes required to operate and maintain our business;',
            'disclosure_desc_b13' => 'insurance companies for the purpose of applying and obtaining insurance policy(ies), if necessary;',
            'disclosure_desc_b14' => 'financial institutions for the purpose of applying and obtaining credit facility(ies), if necessary;',
            'disclosure_desc_b15' => 'financial institutions, merchants and credit card organisations in connection with your commercial transactions with us;',
            'disclosure_desc_b16' => 'the general public when you become a winner in a contest, participate in our events, conferences, talks and seminars by publishing your name, photographs and other personal data without compensation for advertising and publicity purposes;',
            'disclosure_desc_b17' => 'any third party (and its advisers/representatives) in connection with any proposed or actual reorganization, merger, sale, consolidation, acquisition, joint venture, assignment, transfer, funding exercise or asset sale relating to any portion of the Company; and/or',
            'disclosure_desc_b18' => 'any other person reasonably requiring the same in order for us to operate and maintain our business or carry out the activities set out in the Purposes or as instructed by you.',
            'accuracy' => 'ACCURACY OF YOUR PERSONAL DATA',
            'accuracy_desc' => 'We take it that all personal data provided by you is accurate and complete, and that none of it is misleading or out of date. You will promptly update us in the event of any change to your personal data.',
            'rights' => 'YOUR RIGHTS',
            'rights_li1' => 'To the extent that the applicable law allows, you have the right to request for access to, request for a copy of, request to update or correct, your personal data held by us. We may charge a small fee (such amount as permitted by the PDPA) to cover the administration costs involved in processing your request to access your personal data. Notwithstanding the foregoing, we reserve our rights to rely on any statutory exemptions and/or exceptions to collect, use and disclose your personal data.',
            'rights_li2' => 'You have the right at any time to request us to limit the processing and use of your personal data (for example, requesting us to stop sending you any marketing and promotional materials or contacting you for marketing purposes).',
            'rights_li3' => 'In addition, you also have the right, by notice in writing, to inform us on your withdrawal (in full or in part) of your consent given previously to us subject to any applicable legal restrictions, contractual conditions and a reasonable duration of time for the withdrawal of consent to be effected. However, your withdrawal of consent could result in certain legal consequences arising from such withdrawal. In this regard, depending on the extent of your withdrawal of consent for us to process your personal data, it may mean that we will not be able to continue with your existing relationship with us or the contract that you have with us will have to be terminated.',
            'retention' => 'RETENTION OF YOUR PERSONAL DATA',
            'retention_desc' => 'Any of your personal data provided to us is retained for as long as the purposes for which the personal data was collected continues; your personal data is then destroyed or anonymised from our records and system in accordance with our retention policy in the event your personal data is no longer required for the said purposes unless its further retention is required to satisfy a longer retention period to meet our operational, legal, regulatory, tax or accounting requirements.',
            'security' => 'SECURITY OF YOUR PERSONAL DATA',
            'security_li1' => 'We are committed to ensuring that your personal data is stored securely. In order to prevent unauthorised access, disclosure or other similar risks, we endeavour, where practicable, to implement appropriate technical, physical, electronic and procedural security measures in accordance with the applicable laws and regulations and industry standard to safeguard against and prevent the unauthorised or unlawful processing of your personal data, and the destruction of, or accidental loss, damage to, alteration of, unauthorised disclosure of or access to your personal data.',
            'security_li2' => 'We will make reasonable updates to its security measures from time to time and ensure the authorised third parties only use your personal data for the Purposes set out in this Privacy Policy.',
            'security_li3' => 'The Internet is not a secure medium. However, we will put in place various security procedures with regard to the Site and your electronic communications with us. All our employees and data processors, who have access to, and are associated with the processing of your personal data, are obliged to respect the confidentiality of your personal data.',
            'security_li4' => 'Please be aware that communications over the Internet, such as emails/webmails are not secure unless they have been encrypted. Your communications may be routed through a number of countries before being delivered – this is the nature of the World Wide Web/Internet.',
            'security_li5' => 'We cannot and do not accept responsibility for any unauthorised access or interception or loss of personal data that is beyond our reasonable control.',
            'minors_others' => 'PERSONAL DATA FROM MINORS AND OTHER INDIVIDUALS',
            'minors_others_li1' => 'To the extent that you have provided (or will provide) personal data about your family members, spouse, other dependents (if you are an individual), directors, shareholders, employees, representatives, agents (if you are a corporate entity/an organisation) and/or other individuals, you confirm that you have explained (or will explain) to them that their personal data will be provided to, and processed by, us and you represent and warrant that you have obtained their consent to the processing (including disclosure and transfer) of their personal data in accordance with this Privacy Policy.',
            'minors_others_li2' => 'In respect of minors (i.e. individuals under 18 years of age) or individuals not legally competent to give consent, you confirm that you are the parent or guardian or person who has parental responsibility over them or the person appointed by court to manage their affairs or that they have appointed you to act for them, to consent on their behalf to the processing (including disclosure and transfer) of their personal data in accordance with this Privacy Policy.',
            'transfer' => 'TRANSFER OF YOUR PERSONAL DATA OUTSIDE OF MALAYSIA',
            'transfer_desc' => 'Our information technology storage facilities and servers may be located in other jurisdictions outside of Malaysia. This may include, but not limited to, instances where your personal data may be stored on servers located outside Malaysia. In addition, your personal data may be disclosed or transferred to entities located outside Malaysia or where you access the Site from countries outside Malaysia. Please note that these foreign entities may be established in countries that might not offer a level of data protection that is equivalent to that offered in Malaysia under the laws of Malaysia. You hereby expressly consent to us transferring your personal data outside of Malaysia for such purposes. We shall endeavour to ensure that reasonable steps are taken to procure that all such third parties outside of Malaysia shall not use your personal data other than for that part of the Purposes and to adequately protect the confidentiality and privacy of your personal data.',
            'website' => 'WEBSITE',
            'website_li1' => 'External links',
            'website_li1_a' => 'If any part of the Site links you to other websites, those websites do not operate under this Privacy Policy and we do not accept any responsibility or liability arising from those websites.',
            'website_li1_b' => 'Likewise, if you subscribe to an application, content or a service from our strategic partner and you subsequently provide your personal data directly to that third party, that personal data will be subject to that third party’s privacy/personal data protection policy (if they have such a policy) and not to this Privacy Policy.',
            'website_li1_c' => 'We recommend you to read and understand the privacy/personal data protection statement/policy posted on those other websites in order to understand their procedures for collecting, processing, using and disclosing personal data and before submitting your personal data to those websites.',
            'website_li2' => 'Cookies',
            'website_li2_a' => 'We employ an industry standard technology called “cookies”. The cookie is a small piece of information stored on the hard drive of your computer or device for record-keeping purposes, and is used by us to track your visits to the Site. Cookies may be used to save your preferences for your ease and convenience when using the Site. Third party advertising networks may issue their separate cookies to your hard drive when serving advertisements.',
            'website_li2_b' => 'The type of anonymous click stream data collected by us through the cookies may include your Internet Protocol address, web browser software, date and time of visit to the Site, and whether your requests (including search requests and clicking on links to parts of the Site) were met with successfully. All such information collected through cookies are not personal data and you cannot be identified from this information. Such information is only used for the purpose of managing and creating a better user experience, analysing the traffic on the Site and to identify areas for improvement on the Site.',
            'website_li2_c' => 'The use of cookies is now an industry standard, and you will find them used on most major websites. Most browsers are initially set up to accept cookies. If you prefer, you can reset your browser either to notify you when you have received a cookie, or to refuse to accept cookies. You should understand that certain features on the Site will not function properly if you set your browser to not accept cookies.',
        ],
        'faq' => [
            'content' => [
                'select_topic' => 'Select a topic',
                'faq_title' => 'Frequently Asked Questions',
                'looking' => 'Looking for something?',
                'profile_page' => 'Profile Page',
                'shave_plan' => 'Shave Plan',
                'need_help' => 'Need more help? Drop us your message and let&#39s get in touch',
            ],
        ],
        'contact' => [
            'enquiry_type' => [
                'billing_payment' => 'Billing/Payment',
                'plan' => 'My Plan',
                'account' => 'My Account',
                'returns' => 'Returns',
                'products' => 'Products',
                'delivery' => 'Delivery',
            ],
            'contact_us' => 'Contact Us',
            'what_need_help' => 'What do you need help with?',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Contact No.',
            'enquiry' => 'Enquiry Form',
            'popular_topics' => 'Popular help topics',
            'shave_plans' => 'Shave plans',
            'delivery_shipping' => 'Delivery/Shipping',
            'returns_refunds' => 'Returns/Refunds',
            'payments' => 'Payments',
            'technical_issues' => 'Technical issues',
            'account' => 'Account',
            'products' => 'Products',
            'company' => 'Company',
            'shaves2u_name' => 'Shaves2u Sdn Bhd',
            'shaves2u_phone' => '+603 2773 2882',
            'shaves2u_time' => 'Mon-Fri, 9am-6pm,<br>except on public holidays.',
            'submit' => 'Submit',
        ],
    ],
    'authenticated' => [
        'content' => [
            'logged_in_name' => 'HI :name',
            'dashboard' => 'DASHBOARD',
            'shave_plans' => 'SHAVE PLANS',
            'profile' => 'PROFILE',
            'order_history' => 'ORDER HISTORY',
            'referrals' => 'REFERRALS',
            'leaving_soon' => 'You\'re leaving so soon. Are you sure?',
            'sign_out_copy' => 'SIGN OUT',
            'sign_out' => 'Yes, sign me out',
            'stay' => 'No, I want to stay',
            'switch_account' => 'Wish to switch account? <u class="btn-sign-in">Logout</u>',
        ],
        'dashboard' => [
            'dashboard' => 'Dashboard',
            'welcome' => 'Welcome :name',
            'current_plan' => 'CURRENT PLAN',
            '3_blade_pack_2_months' => '3 Blade Pack every 2 Months',
            'next_billing' => 'Next Billing',
            'payment_method' => 'Payment Method',
            'ending_in' => 'Ending in ',
            'expiring_in' => 'Expiring in ',
            'plan_products' => 'Plan Products',
            'view_details' => 'FIND OUT MORE',
            'view_plan' => 'VIEW PLAN',
            'referrals' => 'REFERRALS',
            'total_credits' => 'Total Credits',
            'people_referred' => 'Sign-ups via your link',
            'successful_referees' => 'Completed 1st billing',
            'shaving_plans' => 'Your Shaving Plans',
            'earn_more_referrals' => 'EARN MORE REFERRALS',
            'view_profile' => 'VIEW PROFILE',
            'start_shave' => 'START YOUR SHAVE PLAN',
        ],
        'shaveplans' => [
            'not_on_plan' => 'It looks like you’re not on a Shave Plan.',
            'custom_plan' => 'If you’re happy with our products, check out how you can<br>save even more on shaving with a customisable Shave Plan.',
            'shaving_plans' => 'Your Shaving Plans',
            'current_plan' => 'CURRENT PLAN',
            'inactive' => 'Inactive',
            'on_hold' => 'On Hold',
            'active' => 'Active',
            'next_billing' => 'Next Billing',
            'payment_method' => 'Payment Method',
            'plan_products' => 'Plan Products',
            'edit_plan' => 'EDIT PLAN',
            'view_plan' => 'VIEW YOUR PLAN',
            'refill_now' => 'Ship my refill now',
            'sure_ship_now' => 'Are you sure you want to ship your plan product refills now?',
            'charged_instantly' => 'You will be charged <b>:currency :price</b> instantly for this delivery.',
            'added_items' => 'These items will be added to your shave plan subscription and will be delivered on your next shipping date.',
            'ship_now' => 'YES, SHIP NOW',
            'cancel' => 'CANCEL',
            'shave_plan_indexed' => 'SHAVE PLAN :index',
        ],
        'shaveplansedit' => [
            'plan_details' => 'Plan Details',
            'edit_shave_plan' => 'Edit Shave Plan',
            'add_new_product' => 'ADD NEW PRODUCT',
            'frequency' => 'Frequency',
            'promotion' => 'Promotion',
            'enter_promo_code' => 'Enter Promo Code',
            'promo_code' => 'Promo code',
            'voucher' => ':discount% OFF Voucher',
            'use' => 'USE',
            'card_information' => 'This card information will be set as your default card when making new purchases or subscriptions.',
            'address' => 'Address',
            'shipping_billing' => 'Shipping & Billing',
            'shipping' => 'shipping',
            'billing' => 'billing',
            'pause_plan' => 'Pause Plan',
            'cancel_subscription' => 'Cancel Subscription',
            'save_plan' => 'SAVE PLAN',
            'select_product_add' => 'Select a product you would like to add',
            'add_product' => 'ADD PRODUCT',
            'accepted_promo' => 'Your promo code is accepted!',
            'change_promo' => 'Do you want to change your<br>current plan promotion?',
            'current_promotion' => 'CURRENT PROMOTION',
            'promotion_cycle' => 'Promotion Cycle : :promo',
            'promotion_discount' => ':promo% OFF Discount',
            'to' => 'TO',
            'change_now' => 'YES, CHANGE NOW',
            'promo_next_cycle' => 'Promotions will only affect<br class="d-lg-none"> your next billing cycle.',
            'pause_subscription' => 'Pause plan subscription',
            'select_months' => 'You can pause your plan now and postpone your billing for a few months.<br class="d-lg-none"><br>Select how many months you would like to pause for:',
            '1month' => '1<br class="d-none d-lg-block"> Month',
            '2months' => '2<br class="d-none d-lg-block"> Months',
            '3months' => '3<br class="d-none d-lg-block"> Months',
            'resume_date' => 'Your plan will resume on',
            'resume_date2' => '.',
            'pause_my_plan' => 'PAUSE MY PLAN',
            'cancel_plan' => 'Wish to cancel your plan?<br class="d-lg-none"> <a class="start-cancellation-journey color-orange" data-dismiss="modal"><u>Click here</u></a>',
            'card_number' => 'Card Number',
            'expiry_date' => 'Expiry Date (MM/YY)',
            'cvc' => 'CVC',
            'card_birth' => 'Card Birth',
            'card_password' => 'Password',
            'pause_plan_success' => 'You have successfully paused your subscription for :month months',
            'pause_plan_successV2' => 'You have successfully paused your subscription for :month month',
            'new_promotion' => 'NEW PROMOTION'
        ],
        'profile' => [
            'edit_profile' => 'Edit Profile',
            'add_new_address' => 'Add New Address',
            'shipping_address_primary' => 'Shipping Address (Primary)',
            'billing_address_primary' => 'Billing Address (Primary)',
            'not_given_address' => 'It looks like you have not given us your address.',
            'update_address' => 'Please update your information for more amazing deals on our shaving products. ',
            'set_primary_card' => 'Set as primary card',
            'primary_card' => 'Primary Card',
            'not_given_card' => 'Uh-oh. You have not given us your credit card information.',
            'update_card_details' => 'Please update your credit card details to ensure the smoothness of your subscription process.',
            'valid_thru' => 'VALID THRU',
        ],
        'orderhistory' => [
            'order_history' => 'Order History',
            'order_no' => 'Order No. #:orderNo',
            'view_details' => 'View Details',
            'no_history' => 'No order history found.',
        ],
        'orderhistoryinfo' => [
            'email' => 'Email',
            'address' => 'Address',
            'placed_on' => 'Placed on :date',
            'account_details' => 'Account Details',
            'order_summary' => 'ORDER SUMMARY',
            'shave_plan' => 'Shave Plan',
            'plan' => 'Plan',
            'starter_kit' => 'Starter Kit',
            'alacarte_orders' => 'Products',
            '6_blade_cartridge' => '6 Blade Cartridge',
            '5_blade_cartridge' => '5 Blade Cartridge',
            '3_blade_cartridge' => '3 Blade Cartridge',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
            'shipping_cost' => 'Shipping Cost',
            'promo_code' => 'Promo Code',
            'none' => 'None',
            'grand_total' => 'Grand Total Paid<br>(Incl. tax)',
        ],
        'referrals' => [
            'referrals' => 'Referrals',
            'get_rewarded' => 'Get rewarded with <strong> :currency:value</strong> in cash for anyone who signs up and completes their 1<sup>st</sup> billing cycle.',
            'your_referrals' => 'YOUR REFERRALS',
            'cash_out' => 'CASH OUT',
            'processing' => 'PROCESSING',
            'sign_ups' => 'Sign-ups via your link',
            'completed_billing' => 'Completed 1<sup>st</sup> billing',
            'referral_cash' => 'Referral Cash',
            'fill_in_details' => 'Kindly fill in your details below<br />for the cash withdrawal',
            'full_name' => 'Full Name as per Bank Account: ',
            'bank' => 'Bank:',
            'account_number' => 'Bank Account Number:',
            'transaction_credited' => 'Your transaction will be credited within 3 working days',
            'share_invitation' => 'Share the awesome shaves with your friends!<br>Send your friends an invitation to sign up for a Shaves2U Starter Kit.',
            'email_friend' => 'Enter friend\'s email (10 max, separated by semicoma (;))',
            'send' => 'SEND',
            'thanks_bank' => 'Thanks for your bank details',
            'withdrawal_credited' => 'Your withdrawal will be credited to your account within 3 working days.',
            'close' => 'CLOSE',
            'start_sharing_desktop' => 'Start sharing your unique link<br>and earning cash today!',
            'start_sharing_mobile' => 'Start sharing your unique link and earning cash today!',
            'share_now' => 'Share now',
            'unique_link' => 'Your unique link: ',
            'share_via' => 'Share Via',
            'success_invite'=>'Your invitation has been successfully sent!',
        ],
    ],
    'products' => [
        'product' => [
            'starter_kit' => 'Starter Kit',
            'better_shaves' => 'Better shaves start here.',
            'start_plan' => 'Start your shave plan for only :currency:price',
            'premium_handle' => 'Premium Handle',
            'premium_handle_desc' => 'Maneuver your way around with a firm grip of our ergonomically designed handle.',
            'swivel_handle' => 'Swivel Handle',
            'swivel_handle_desc' => 'Maneuver your way around with a firm grip of our ergonomically designed handle.',
            'blade_packs' => 'Blade Packs',
            'blade_packs_desc' => 'Better shave starts with a mean and clean razor blade. Ours is the meanest and the cleanest.',
            'blade_packs_desc_mobile' => 'Better shave starts with a mean<br>and clean razor blade. Ours is the<br>meanest and the cleanest.',
            'shave_cream' => 'Shaving Cream',
            'shave_cream_desc' => 'Smoother glide and softer skin only with <br>our specially formulated cream.',
            'shave_cream_desc_mobile' => 'Comes with all you need to experience our <br class="d-none d-sm-block"/>quality for yourself. Trial period lasts for 14 days.',
            'after_shave_cream' => 'After Shave Cream',
            'after_shave_cream_desc' => 'Rejuvenate your skin with a cooling sensation and light, fragrant scent.',
            'after_shave_cream_desc_mobile' => 'Smoother glide and softer skin only with <br>our specially formulated cream.',
            'ask' => 'Awesome Shave Kit',
            'ask_desc' => 'Everything you need for the <br/>ultimate shaving experience.',
            'razor_protector' => 'Razor Protector',
            'razor_protector_desc' => 'Made from the best quality silicone, our shaver sleeve is specifically designed to protect your shaver from grime or dust.',
            'toiletry_bag' => 'Toiletry Bag',
            'toiletry_bag_desc' => 'With 4 compartments and a nifty hook, it is easier to carry your shaving kit in our portable and compact bag.',
            'select_plan' => 'Select the plan you want to proceed with:',
            '6_blades' => '6 Blade every 2 months',
            'not_shave_plan' => 'It looks like you&#39;re not on a Shave Plan. This product is only available with an existing shave plan. Please select a plan to get started.',
            'start_shave' => 'START YOUR SHAVE PLAN',
        ],
        'content' => [
            'looking_at_this' => ' people are looking at this item now',
            'bought_this' => ' people have <br/> bought this items',
            'bought_item' => '3 Blade Pack',
            'buy_now' => 'BUY NOW',
            'learn_more' => 'LEARN MORE',
            'available_existing' => 'Only available with an existing shave plan',
            'available_existing_mobile' => 'Only available with an<br>existing shave plan',
            'shave_on' => 'Get your shave on with our Starter Kit',
            'new_shaving_experience' => 'A new shaving experience with your own ergonomic handle, razor-sharp blade, and bubbly shave cream for only :currency:price',
            'free_text' => 'If you sign up for our Shaves2U shave plan or buy our Awesome Shave Kit, get ready for the ultimate
                    shaving experience! Not only does Shaves2U products cost at a lower price compared to other brands,
                    but the shaving kits are also of high-quality.
                    <br/></br>
                    Your customised shaving kit starts with a razor handle. Our swivel handle plays an essential part in
                    providing our customers with the best shaving experience. We had it ergonomically-designed for a
                    comfortable and firm grip. The swivel handle is also designed with a metal body for exceptional
                    durability and sleek design. With a beautiful design and a secure grip, shaving sessions are now
                    more precise and quick. The dual-axis swivel mechanism makes it easy to glide and reach all around
                    your face with little to no effort. All our blade packs such as the three-blade, five-blade and
                    six-blade cartridges fit on our swivel handle, so you’ll only need one for the rest of your shave
                    plan!
                    <br/><br/>
                    Shaves2U provides three different blade cartridges to our customers: the three-blade pack, the
                    five-blade pack and the six-blade pack. All Shaves2U blade types have extra sharpness and durability
                    with our patented I.C.E. Diamond Coating, so you can always expect a clean finish to your shaving
                    sessions. There is also the MicroSilver BG™ Lubricating strip fitted to our razor blades; it
                    contains Aloe, Vitamin E, Shea Butter, Cocoa Butter for decreased irritation during shaving.
                    <br/><br/>
                    Each blade type is made for different shaving purposes. The three-blade pack is ideal for the upper
                    lip and chin. The five-blade pack is an excellent choice for men with fuller beards and longer
                    stubble. If you prefer the clean-shaven look, the six-blade pack is perfect for the ultimate smooth
                    finish for daily use.
                    <br/><br/>
                    The ultimate shaving experience isn&#39t complete without our shave cream. ARKO MEN produces them, and
                    you can get them from both shaving kits by Shaves2U. Our shave cream is specifically formulated to
                    cushion your sensitive skin and give it a cooling sensation during your shaving session. It is also
                    enriched with Vitamin E to prevent post-shave redness. With our shave cream, you&#39ll leave with a
                    refreshing feeling and rejuvenating touch with a light and clean scent after every shave.
                    <br/><br/>
                    For the ultimate shaving experience, you must include the Shaves2U after shave cream in your shaving
                    kit. Just like the shaving cream, ARKO MEN produces this after shave cream too. After your shaving
                    session, applying our after shave cream will leave you feeling refreshed and smelling pleasant. With
                    its unique formula, the after shave cream will ensure your freshly shaven skin is cold and calm. It
                    will also replenish your skin&#39s lost moisture with conditioning ingredients. Not only will your skin
                    be feeling fresh after shave, but the after shave cream will also leave you smelling good for the
                    rest of your day.
                    <br/><br/>
                    To enjoy the complete set of Shaves2U products, which includes the swivel handle, blade cartridges,
                    shaving cream and after shave cream, you must get the Awesome Shave Kit! This shaving kit is
                    everything you need for the ultimate shaving experience, and you can get it starting from the price
                    of RM70.',
            'select_plan_edit' => 'Select Your Plan to Edit',
        ],
        'handles' => [
            'premium_handle' => 'Premium Handle',
            'premium_handle_shortdesc' => 'Ergonomic design for greater precision.',
            'premium_handle_desc' => 'Follows every contour for the smoothest finish.',
            'premium_handle_makes_good' => 'What makes our Swivel Handle good?',
            'premium_handle_price' => ':currency :price',
            'premium_handle_subdesc1' => 'Dual axis swivel mechanism to glide over your facial contours with little to no effort.',
            'premium_handle_subdesc2' => 'An exceptional durability and a sleek look with the mighty metal body.',
            'premium_handle_subdesc3' => 'For the smoothest finish of the clean-shaven look.',
            'swivel_handle' => 'Swivel Handle',
            'swivel_handle_shortdesc' => 'Ergonomic design for greater precision.',
            'swivel_handle_desc' => 'Follows every contour for the smoothest finish.',
            'swivel_handle_makes_good' => 'What makes our Swivel Handle good?',
            'swivel_handle_price' => ':currency :price',
            'swivel_handle_subdesc1' => 'Dual axis swivel mechanism to glide over your facial contours with little to no effort.',
            'swivel_handle_subdesc2' => 'An exceptional durability and a sleek look with the mighty metal body.',
            'swivel_handle_subdesc3' => 'For the smoothest finish of the clean-shaven look.',
            'swivel_handle_meta_text' =>
            'No shaving jobs can be done without our shaving handles, the Shaves2U swivel handle. It isn\'t your
            average razor handle; it is designed to make your shaving experience smoother and better. With
            flexibility and durability in mind, we made sure the design of the swivel handle is ergonomic and
            sleek for our users. Not only is the Shaves2U swivel handle comfortable to hold, but it also
            provides a firm and stable grip whenever you\'re shaving.
            <br/><br/>
            The swivel handle by Shaves2U is made with metal, but it is light on the hands. Our shaving handles
            are also durable, so you\'ll only need to get one for your entire Shaves2U experience. No matter what
            blade cartridge you choose, it can fit on our swivel handle. The blade cartridges Shaves2U offers
            include the three-blade, five-blade and six-blade cartridges. You can always replace the blade
            cartridges on our swivel handles with any type.
            <br/><br/>
            How is the Shaves2U swivel handle so flexible? Our shaving handles have a dual-axis swivel mechanism
            that makes it easy to rotate and glide the blade cartridges over your facial contours with little to
            no effort. With our swivel handle, you can easily shave hard to reach areas with ease. You can also
            expect a more precise shaving experience with our shaving handles.
            <br/><br/>
            Are you interested in owning a Shaves2U swivel handle? With the price of RM18, you can get them
            individually. But if you get our Shaves2U Starter Kit and start a shave plan with us, not only do
            you get our swivel handle, but you will also get blade cartridges and a tube of shaving cream! Our
            Starter Kit costs as low as RM6.50, and we can deliver it to you free.
            <br/><br/>
            You can also get the swivel handle by purchasing the Awesome Shave Kit. In this shaving kit, you can
            get swivel handle, blade cartridges of any choice, shaving cream and the after shave cream. However,
            unlike the Starter Kit, the Awesome Shave Kit isn\'t a monthly subscription plan. With the price of
            at least RM70, you can get the complete set of shaving supplies by Shaves2U for the ultimate shaving
            experience.
            <br/><br/>
            To be a proud owner of the Shaves2U swivel handle, you have to order for our products online on our
            website as we have no retail stores at the moment. All you need to do is choose the Starter Kit or
            the Awesome Shave Kit, and we\'ll deliver it to you straight to you at your doorstep. If you decide
            to start a Shaves2U shave plan, you\'ll have to select the frequency of the delivery as well. It\'s
            just that simple!
            <br/><br/>
            Once you get our swivel handle from the mail after waiting for a few days, you can expect a better
            shaving experience with it. Don\'t forget; our swivel handles are meant to pair with our blade
            cartridges such as the three-blade pack, the five-blade pack and the six-blade pack. Pair it along
            with the shaving cream, and you\'ll get the ultimate shaving experience with Shaves2U!',
        ],
        'blades' => [
            'blade_pack' => 'Blade Pack',
            'blade_pack_shortdesc' => 'Specifically made for those who want their blades to be
            sharp, durable and sensational.<br>We got you.',
            'blade_pack_desc' => 'Specifically made for those who want their blades to be
            sharp, durable and sensational.<br>We got you.',
            '3blades' => [
                'name' => '3 Blade Pack',
                'price' => ':currency :price',
                'desc' => 'Ideal for upper lip <br class="d-none d-xl-block"/> and chin.',
                'subdesc1' => 'Double Coated Blade Edge for a close, smooth shave you would be proud of.',
                'subdesc2' => 'Perfectly crafted for thinner stubbles<br>while gently keeping skin irritation at bay.',
                'subdesc3' => ' Equipped with Microsilver BG™ lubricating strip for a shave that is satisfying!',
                'blade_pack_makes_good' => 'What makes our 3 Blade razor good?',
            ],
            '5blades' => [
                'name' => '5 Blade Pack',
                'price' => ':currency :price',
                'desc' => 'Great for fuller beards <br class="d-none d-xl-block"/> and longer stubble.',
                'subdesc1' => 'Forged for a close, comfortable shave with<br>our patented I.C.E Diamond and a blade on the<br>back to help you reach those isolated areas.',
                'subdesc2' => 'Specialized anti-clog cartridges for<br>a quick easy rinse.',
                'subdesc3' => 'Glide over skin with enhanced lubricating<br>strip packed with Vitamin E & Aloe<br>to help reduce skin irritation.',
                'blade_pack_makes_good' => 'What makes our 5 Blade razor good?',
            ],
            '6blades' => [
                'name' => '6 Blade Pack',
                'price' => ':currency :price',
                'desc' => 'The ultimate smooth <br class="d-none d-xl-block"/> finish everyday.',
                'subdesc1' => 'Extra sharpness and durability with our patented<br>I.C.E Diamond Coating and a blade on the back to<br>help you reach those isolated areas.',
                'subdesc2' => '6 L-shaped Blades spaced close together<br>for comfort and easy rinsing.',
                'subdesc3' => 'Fitted with a MicroSilver BG™ Lubricating strip<br>that contains Aloe, Vitamin E, Shea Butter,<br>Cocoa Butter, for reduced irritation.',
                'blade_pack_makes_good' => 'What makes our 6 Blade razor good?',
            ],
            'blades_meta_text' => 'Looking good is vital to help you feel more confident in daily life. From working the boardroom to
                    networking after hours, you can benefit a lot from looking your best. When you don’t look your best,
                    you may start to feel insecure, and this could possibly affect everything else in your life. When
                    you don’t have physical insecurities, you can be at your best during significant life situations.
                    <br/>
                    <br/>
                    One of the ways for you to look good is by having a personal grooming routine. Not all men are sure
                    of how to even start a personal grooming routine. It’s not as difficult as it sounds. While women
                    may have comprehensive steps in their grooming habits, men can get away with doing just the
                    basics—one of them includes shaving your face.
                    <br/><br/>
                    A lot of people neglect certain key elements of a good shave. To make sure that your shaves are
                    slick and smooth, you should make sure that you have the best, sharpest shaving blades for your
                    shaver. A lot of men overlook the value of having sharp, efficient blades and try to make do with
                    the same razor for as long as six months, which is highly not recommended.
                    <br/><br/>
                    Shaving blades should be replaced regularly. How often you should be doing that depends on how thick
                    your beard is. As with most blades, even razors do eventually get dull and lose their efficiency.
                    Using dull blades for your face will irritate your skin. If you notice that you have been getting a
                    lot of nicks and cuts when you shave, that is definitely a sign that your blades need changing.
                    <br/><br/>
                    You may not be aware of this, but there are multiple options when it comes to choosing your shaving
                    blade. The number of blades in a single razor can differ from three, five or six blades. How many
                    should you get? This number will be based on your facial hair needs. Moustaches and regular stubbles
                    may need a lesser amount of blades, but with fuller and thicker beards, you may need up to six
                    blades.
                    <br/><br/>
                    Now that you know you have to change shaving blades regularly, it may sound like a hassle. Not
                    everyone has time to do a store run to get new blades. Luckily for you, Shaves2U has great shaving
                    plans to make this part of your personal grooming routine easier. Choose to subscribe, and you never
                    have to bother with getting new razors again as you can have them delivered to you regularly.
                    <br/><br/>
                    With the Shaves2U subscription plan, you can choose the frequency of having new blade refill
                    cartridges sent to you, as well as the number of blades. As mentioned earlier, the number of blades
                    you need depends on how much facial hair you will be shaving off. The Shaves2U blade refill
                    cartridges come with four sets of blades and are equipped with a moisturising lubrication strip as
                    well as an anti-clog design for reduced irritation and easy rinsing. Order your shaving blade
                    refills anytime, or set a subscription plan for yourself to make things easier!',
        ],
        'shave_creams' => [
            'shave_creams' => 'Shave Cream',
            'shave_creams_shortdesc' => 'Ideal for upper lip and chin.',
            'shave_creams_desc' => 'Helps make every shave<br/>smoother and more comfortable.',
            'shave_creams_makes_good' => 'What makes our Shave Cream good?',
            'shave_creams_price' => ':currency :price',
            'shave_creams_desc1' => 'Cooling comfort',
            'shave_creams_desc1_info' => 'If there’s one thing you shouldn’t tolerate, it’s irritated skin. Good thing our shave cream is formulated specifically to cushion your sensitive skin and give it a cooling sensation that will blow your mind.',
            'shave_creams_subdesc1' => 'Enriched with Vitamin E for you to bid farewell to <br>post-shave redness.',
            'shave_creams_subdesc2' => 'Cushioning your skin from even the tiniest <br class="d-block d-lg-none">cuts with our rich and easy-to-lather cream.',
            'shave_creams_subdesc3' => 'Refreshing and rejuvenating touch <br class="d-block d-lg-none">with a light, clean scent.',
            'shave_creams_meta_text' =>
            'A lot of men are now more savvy about taking care of how they look. This is a good thing as men
            should realise that personal grooming should apply to anyone regardless of gender. One of the
            easiest ways for men to start a personal grooming routine is by shaving their face.
            <br/><br/>
            You may not realise it, but shaving does wonders for the face. It can take years off your face for a
            youthful look and give a neat impression of yourself as a person. As much as the scruffy, lumberjack
            look can be popular with the ladies, shaving to let your beard grow anew is good for your beard.
            Think of how people get a haircut or trim to freshen up their look. The same concept can be applied
            to your facial hair.
            <br/><br/>
            Now that you know that shaving your facial hair can be good for your personal grooming habits, what
            should you keep in mind to make sure your shaves are sharp and smooth? Besides having sharp razor
            blades, an often overlooked part of shaving that is just as important is the usage of shaving cream.
            <br/><br/>
            Shaving without a lubricant can be really bad for your skin. You can get nicks and cuts on your skin
            when you shave directly on skin without a lubricant, gel or foam. There are many choices of shaving
            lubricant available in the market, but shaving cream is undoubtedly the best option for most people.
            We encourage everyone to use it to ensure a smooth and moisturising shave. If you need one, we
            recommend Shaves2U’s very own shaving cream.
            <br/><br/>
            Besides making the razor glide easily on your face, shaving cream also helps with hydrating your
            facial hair pre-shaving. As some facial hair can be thick and coarse, it does make it a less smooth
            process without the aid of lubrication. With cream, it helps to soften up the facial hairs to make
            shaving easier. It only makes sense to incorporate it in your shaving routine!
            <br/><br/>
            A lot of shaving lubricants also aims to help ease redness. It may not necessarily moisturise your
            face as heavily as a normal moisturiser, but because of how easy shaving can strip moisture off your
            skin, using it beforehand makes a difference. This is especially true if you have sensitive skin.
            The Shaves2U shaving cream is packed with Vitamin E for an extra moisturising effect. The refreshing
            scent also helps you to feel rejuvenated and carry on being confident throughout the day.
            <br/><br/>
            Sign up for a Shaves2U Starter Kit and get it together with a shaving handle and blade. With a
            shaving plan, you can customise how often you need to have your shaving blade refills as well as
            your next round of shaving cream. The process is so easy as everything can be done online, so you
            don’t have to go to the store to get them physically. If you can’t commit to a subscription plan,
            you can also buy it individually as and when you need it.',
        ],
        'aftershave_creams' => [
            'aftershave_creams' => 'After Shave Cream',
            'aftershave_creams_shortdesc' => 'Leaves you feeling refreshed<br/> and smelling good.',
            'aftershave_creams_desc' => 'Helps make every shave<br/>smoother and more comfortable.',
            'aftershave_creams_makes_good' => 'What makes our After Shave Cream good?',
            'aftershave_creams_price' => ':currency :price',
            'aftershave_creams_desc1' => 'A Refreshed Experience',
            'aftershave_creams_desc1_info' => 'For every sensational performance, there’s a firework ending to it. Give yours a great finale with a refreshing and rejuvenating touch of our fragrant aftershave.',
            'aftershave_creams_subdesc1' => 'Ensuring your freshly shaven skin is cool,<br>calm and balance with our special formula.',
            'aftershave_creams_subdesc2' => 'Replenishes skin\'s lost moisture<br>with conditioning ingredients',
            'aftershave_creams_subdesc3' => 'Light fragrant scent to give that refreshing<br>firework ending to your performance.',
            'aftershave_creams_meta_text' =>
            'Shaving is a normal process in many people\'s grooming routines. Whether we do it daily, once every
            few days or even weeks, this form of hair removal or shaping is preferred by many for good reasons.
            Shaving is easy, painless and reasonably affordable, making it accessible for all. However, without
            proper shaving aftercare, the skin could be compromised, and that may have lasting effects!
            <br/>
            <br/>
            After shaving, the skin can become inflamed and irritated from improper shaving. This can cause your
            face to not only be painful but also leave your skin looking red and tender. If you\'re presently
            suffering from this, you should try shaving with a sharp blade and going with the direction of hair
            growth. With this, you lessen the chances of cutting the skin from running the edge deeper or going
            over too many times just to get a close shave. Using the right techniques to shave according to the
            desired look is important. In the long run, it maintains your skin and also makes future shaves
            sessions easier.
            <br/>
            <br/>
            Practising shaving aftercare is also vital in maintaining the skin\'s condition. Shaving aftercare is
            essential as a form of immediate treatment of issues that are caused by shaving. There are many
            techniques, tips and products that are involved in this stage. Some people rub a cube of ice or
            splash cold water to their face, but these steps can only do so much in improving the skin\'s
            condition. Hence, one of the best products to keep in your routine is after shave cream.
            <br/>
            <br/>
            After shave is a product applied to the skin after shaving. In the past, men applied witch hazel and
            bay rum to their necks and chins after shaving. These ingredients are used due to their astringent
            properties, reducing the skin irritation from shaving so closely to the skin. After shave also acts
            as an antiseptic to prevent infections or cuts, so that rough bumps are minimised, making your skin
            remain smooth and clear.
            <br/>
            <br/>
            We highly recommend that you add this step to your shaving routine. If you\'ve decided to get an
            after shave for your next shaving session, try Shaves2U\'s very own after shave cream! With
            ingredients like aloe vera, this cooling cream restores your skin\'s moisture levels as well as
            reduce irritation and inflammation of the skin. Our skincare products go through strict
            dermatologist tests to assure you that all of them are safe for use.
            <br/><br/>
            The light fragrance of Shaves2U\'s after shave cream elevates your shaving experience, taking care of
            you and your skin, all in one routine. The consistency of this product ensures easy use and full
            moisturising effects when you apply the after shave cream. All you have to do is squeeze a small
            amount on your palm and massage it into freshly shaved skin. It\'s that simple! Shaves2U aims to take
            care of all your shaving needs, no matter how busy or occupied you are. Add a tube of our after
            shave cream to your shave plan today!',
        ],
        'ask' => [
            'title' => 'Awesome Shave Kit',
            'subtitle' => 'Everything you need for the ultimate shaving experience.',
            'start_customizing' => 'Start customising your Awesome Shave Kit now!',
            'select_blade_type' => 'Please select your blade type',
            'what_comes_in_box' => 'What comes in the box',
            'what_comes_in_box_detail' => 'Ergonomic swivel handle, razor-sharp blade, frothy fresh shave cream and fragrant after shave cream.',
            'blade' => [
                '3blades' => [
                    'name' => '3 Blade Cartridges (5 Pieces)',
                    'desc' => 'The ultimate smooth finish everyday',
                    'info_1' => 'What makes it good:',
                    'info_1_desc' => '•	Made with our patented I.C.E Diamond Coating for durability and sharpness, the 3 blade also features the Microsilver BG™ enhanced lubricating strip for a shave that\'s above the rest.',
                ],
                '5blades' => [
                    'name' => '5 Blade Cartridges (5 Pieces)',
                    'desc' => 'The ultimate smooth finish everyday',
                    'info_1' => 'What makes it good:',
                    'info_1_desc' => '•	Made with our patented I.C.E Diamond Coating for durability and sharpness, the 5 blade also features the Microsilver BG™ enhanced lubricating strip for a shave that\'s above the rest.',
                ],
                '6blades' => [
                    'name' => '6 Blade Cartridges (5 Pieces)',
                    'desc' => 'The ultimate smooth finish everyday',
                    'info_1' => 'What makes it good:',
                    'info_1_desc' => '•	Made with our patented I.C.E Diamond Coating for durability and sharpness, the 6 blade also features the Microsilver BG™ enhanced lubricating strip for a shave that\'s above the rest.',
                ],
            ],
            'handle' => [
                'name' => 'Swivel Handle',
                'desc' => 'Follows every contour for the smoothest finish',
                'info_1' => 'What makes it good:',
                'info_1_desc' => 'Dual axis swivel mechanism that glides perfectly over facial contours for that perfect clean shave.',
                'info_2_desc' => 'Metal body for an exceptional form',
                'info_3_desc' => 'For the smoothest finish of the clean-shaven look',
            ],
            'shave_cream' => [
                'name' => 'Shave Cream',
                'desc' => 'Helps make every shave smoother and more comfortable',
                'info_1' => 'What makes it good:',
                'info_1_desc' => 'Enriched with Vitamin E to combat post-shaven redness',
                'info_2_desc' => 'Rich and easy-to-lather cream that cushions skin from cuts',
                'info_3_desc' => 'Light, clean scent that refreshes',
            ],
            'after_shave_cream' => [
                'name' => 'After Shave Cream',
                'desc' => 'Leaves you feeling refreshed and smelling great!',
                'info_1' => 'What makes it good:',
                'info_1_desc' => 'Formulated to cool, calm, and balance freshly shaven skins',
                'info_2_desc' => 'Replenishes skin\'s lost moisture with conditioning ingredients',
                'info_3_desc' => 'Light, clean scent for a refreshing post-shave ritual',
            ],
            'free_delivery_door' => 'Free delivery to your door',
            'cartridges_fit_handles' => 'All our cartridges fit all our handles',
            'prefer_shave_plan' => 'Prefer a Shave Plan?',
            'enjoy_low_prices' => 'Enjoy the Shaves2U shaving experience at a lower price, every time.',
            'meta_text' => 'Some specific steps and products make a shaving session, an elevated experience. Each one of these products is essential to the grooming routine to achieve excellent results. Shaves2U’s Awesome Shave Kit is the shaving set you need for the ultimate grooming experience. This comprehensive kit includes high-quality shaving products curated just for you!
                            <br><br>
                            The Swivel Handle of the razor included in this shaving set is designed to give you comfort without compromising the performance of the blade. Dual-axis swivel mechanism that glides perfectly over facial contours allows you to achieve that perfect clean shave. The metal body offers some heft and a sleek look, matching the other items in the set. The handle also fits all the cartridges from Shaves2U, allowing you to switch between blades should you feel like changing your look.
                            <br><br>
                            The Awesome Shaves Kit also includes Shaves2U’s famous blade cartridges. Choose between cartridges with three, five or six blades to be included in this shaving set. Each one is recommended for different shaving needs. A 3-blade cartridge is an excellent option for less hairy guys since it is narrower and ideal for stubble on upper lip and chin areas. The 5-blade cartridge is best for full-bearded guys who don’t shave daily and is equipped with trimmer blade on the back for a precise line-up. For ones who shave daily, the 6-blade cartridge gives the closest, smoothest result that will put you a level above the rest.
                            <br><br>
                            Lubrication is vital during shaving as it minimises irritation and keeps the skin protected. For this reason, Shaves2U’s shaving cream is also a part of this complete shaving set for you. This cream aims to make every shave smoother for you. The luscious, creamy lather allows the blade to simply glide over skin, clearing out all the unwanted hair comfortably. Don’t worry about the product clogging up your blades as this cream rinses off easily with water, leaving no residue at all! The tube of shaving cream has been tested under strict dermatology regulations so you could count on the product being safe and efficient for you.
                            <br><br>
                            After shaving, it is crucial to introduce care and moisture back into the skin. Shaves2U’s After Shave Cream is a component of this shaving set to do just that! Formulated to cool, calm, and balance freshly shaven skin. The after shave contains conditioning ingredients to replenish the skin’s moisture levels so that you get an even smoother finish. The light scent that accompanies this after shave creams will have you feeling refreshed and elevated, ready to take on the day with your newly shaven face.
                            <br><br>
                            Shaves2U’s Awesome Shaves Kit is the complete shaving set you need to up your shaving and grooming game! Get a set today and have us deliver it straight to you so you could start enjoying the products immediately. Remember, we aim to take care of all your shaving needs and issues, regardless of what they are. Order the Awesome Shaves Kit today to experience the full Shaves2U experience.',
        ],
    ],
    'blade_pack' => 'Pack',
    'blade_packV2' => 'Blade Pack',
    'blade_packV3' => 'Blade Cartridge Pack',
    'blade_packV4' => 'Blade',
    'every' => 'every',
    'Months' => 'Months',
    'trial' => 'Trial Period',
    'under_trial' => ' Charges 14 days after you received the Starter Kit.',
    'frequency' => [
        '2' => [
            "durationText" => "5 - 7 days<br>per week",
            "detailsText" => "Delivery every <b>:months months</b>",
            "selectedDetailsText" => "Nice! Looking forward to <br> see you every <b>:months months.</b>",
        ],
        '3' => [
            "durationText" => "2 - 4 days<br>per week",
            "detailsText" => "Delivery every <b>:months months</b>",
            "selectedDetailsText" => "Nice! Looking forward to <br> see you every <b>:months months.</b>",
        ],
        '4' => [
            "durationText" => "1 day<br>per week<br>",
            "detailsText" => "Delivery every <b>:months months</b>",
            "selectedDetailsText" => "Nice! Looking forward to <br> see you every <b>:months months.</b>",
        ],
        '6' => [
            "durationText" => "Once every<br>two weeks<br>",
            "detailsText" => "Delivery every <b>:months months</b>",
            "selectedDetailsText" => "Nice! Looking forward to <br> you every :months months.",
        ],
    ],
    'trial_plan' => [
        'header' => [
            'ongoing_products' => 'Ongoing Products',
            'shipping_frequency' => 'Shipping Frequency',
        ],
        'selection' => [
            'step1' => [
                'best_deal' => 'Best shaving deal in town, exclusive for you.',
                'starter_kit' => 'Starter Kit',
                'included_kit' => 'What&#39s included in the Starter Kit',
                'swivel_handle' => 'Swivel Handle',
                'blade_cartridge' => 'Blade Cartridge',
                'shave_cream' => 'Shave Cream',
                'free_shipping' => 'Free Shipping',
                'quality_guaranteed' => 'Quality Guaranteed',
                'how_works' => 'How it works',
                'unique_experience' => 'Shaves2U brings you a unique shaving experience tailored for your grooming needs.<br>',
                'unique_experience_mobile' => 'Shaves2U brings you a unique<br>shaving experience tailored<br>for your grooming needs.<br>',
                'get_started' => 'Get started in just three simple steps!',
                'step1' => 'Step 1',
                'step1_title' => 'Build your plan',
                'step1_desc' => 'Choose your frequency of delivery<br>you desire.',
                'step2' => 'Step 2',
                'step2_title' => 'Try for 2 weeks',
                'step2_desc' => 'Start by selecting the ideal blade<br>type of your shaving needs.',
                'step3' => 'Step 3',
                'step3_title' => 'Be in Control',
                'step3_desc' => 'Change or stop your plan at any<br>time from your profile page.',
                'handle_title' => 'Crafted for control.',
                'handle_desc' => 'Our Swivel Handle is designed with a comfortable rubberised grip and weighted metal body for optimal control, topped with a striking chrome finish. For a shave with substance and style.',
                'blades_title' => 'Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.',
                'blades_desc' => 'Our Blade Cartridges have everything you need for a superior shave: precision-honed carbon steel blades, a moisturising lubrication strip, and an anti-clog design.',
                'cream_title' => 'Giving your skin the attention it deserves.',
                'cream_desc' => 'Our shave cream cushions and conditions your facial hair for a gentler, smoother shave enriched with anti-inflammatory ingredients to help reduce post-shave redness.',
                'select_blade' => 'Select your blade type',
            ],
            'step2' => [
                'choose_refill' => 'Choose your next refill',
                'refill_ships_date' => 'First refill ships <br><b> :date (not charged today).</b><br>Cancel anytime.',
                'solo_pack_products' => ':blade_count Blade Cartridge Pack',
                'solo_pack' => 'Solo Pack',
                'duo_pack_products' => 'Blade, 1 Shave Cream',
                'duo_pack' => 'Duo Pack',
                'combo_pack_products' => 'Blade, 1 Shave Cream <br> 1 After Shave Cream',
                'combo_pack' => 'Combo Pack',
                'next_step' => 'NEXT STEP',
            ],
            'step3' => [
                'shave_frequency' => 'How often do you shave?',
                'choose_refill' => 'Choose your next refill',
                'change_frequency' => 'We&#39ll send your ongoing shipments based on how often you shave.<br> It&#39s easy to change your shipping frequency at any time.',
                'review_order' => 'Review Order',
            ],
        ],
        'checkout' => [
            'trial_kit' => 'Trial Kit',
            '5blade_cartridge' => '5 Blade Cartridge',
        ],
    ],
    'custom_plan' => [
        'header' => [
            'design_shave_plan' => 'Design Shave Plan',
            'shipping_frequency' => 'Shipping Frequency',
        ],
        'selection' => [
            'design_own_plan' => 'Design your own shave plan',
            'customised_plan' => 'Customised Plan',
            'select_addons' => 'Select your add-on products',
            'add_more' => 'You may add more than one',
            'how_deliver' => 'How would you like this item delivered?',
            'add_all_shipments' => 'ADD FOR EVERY SHIPMENT',
            'add_this_shipment' => 'ADD FOR JUST THIS SHIPMENT',
        ],
        'step1' => [
            'best_deal' => 'Best shaving deal in town, exclusive for you.',
            'starter_kit' => 'Starter Kit',
            'included_kit' => 'What&#39s included in the Starter Kit',
            'swivel_handle' => 'Swivel Handle',
            'blade_cartridge' => 'Blade Cartridge',
            'shave_cream' => 'Shave Cream',
            'free_shipping' => 'Free Shipping',
            'quality_guaranteed' => 'Quality Guaranteed',
            'how_works' => 'How it works',
            'unique_experience' => 'Shaves2U brings you a unique shaving experience tailored for your grooming needs.',
            'get_started' => 'Get started in just three simple steps!',
            'step1' => 'Step 1',
            'step1_title' => 'Build your plan',
            'step1_desc' => 'Choose your frequency of delivery<br>you desire.',
            'step2' => 'Step 2',
            'step2_title' => 'Try for 2 weeks',
            'step2_desc' => 'Start by selecting the ideal blade<br>type of your shaving needs.',
            'step3' => 'Step 3',
            'step3_title' => 'Be in Control',
            'step3_desc' => 'Change or stop your plan at any<br>time from your profile page.',
            'handle_title' => 'Crafted for control.',
            'handle_desc' => 'Our Swivel Handle is designed with a comfortable rubberised grip and weighted metal body for optimal control, topped with a striking chrome finish. For a shave with substance and style.',
            'blades_title' => 'Precision-cut blades. Moisturising lubrication strip. Open-blade architecture.',
            'blades_desc' => 'Our Blade Cartridges have everything you need for a superior shave: precision-honed carbon steel blades, a moisturising lubrication strip, and an anti-clog design.',
            'cream_title' => 'Giving your skin the attention it deserves.',
            'cream_desc' => 'Our shave cream cushions and conditions your facial hair for a gentler, smoother shave enriched with anti-inflammatory ingredients to help reduce post-shave redness.',
            'select_blade' => 'Select your blade type',
        ],
        'step2' => [
            'choose_refill' => 'Choose your next refill',
            'refill_ships_date' => 'First refill ships <br><b> :date (not charged today).</b><br>Cancel anytime.',
            'solo_pack_products' => ':blade_count Blade Cartridge Pack',
            'solo_pack' => 'Solo Pack',
            'duo_pack_products' => 'Blade, 1 Shave Cream',
            'duo_pack' => 'Duo Pack',
            'combo_pack_products' => 'Blade, 1 Shave Cream <br> 1 After Shave Cream',
            'combo_pack' => 'Combo Pack',
        ],
        'step3' => [
            'shave_frequency' => 'How often do you shave?',
            'choose_refill' => 'Choose your next refill',
            'change_frequency' => 'We&#39ll send your ongoing shipments based on how often you shave.<br> It&#39s easy to change your shipping frequency at any time.',
            'review_order' => 'Review Order',
        ],
    ],
    'ask' => [],
    'selection' => [
        'select_blade' => 'Select your blade type',
        'next_step' => 'NEXT STEP',
        'shave_frequency' => 'How often do you shave?',
        'change_frequency' => 'We&#39ll send your ongoing shipments based on how often you shave.<br> It&#39s easy to change your shipping frequency at any time.',
        'review_order' => 'Review Order',
        'change-to-annual-model' => [
            'title' => 'Would you like to enjoy greater saving?',
            'sub-title' => 'Sign up now for an annual plan and we\'ll have you covered for the year ahead so you can rest easy. You even get to enjoy a 20% discount!',
            'cancel' => 'CANCEL',
            'submit' => 'SIGN ME UP!',
        ],
    ],
    'checkout' => [
        'free-product' => 'Free 1 x ',
        'agreement' => [
            'subscription_agreement' => 'By ticking this checkbox, I understand that I am subscribing to a shaving subscription plan and agree to the <a style="color: #ff5001 !important;" href=":link" target="_blank">terms and conditions</a>',
        ],
        'content' => [
            'account' => 'Account',
            'payment' => 'Payment',
            'total_summary' => 'Total Summary',
            'proceed_checkout' => 'Proceed to Checkout',
            'login_validation' => 'Please fill in valid information.',
            'already_signed_up' => 'Oops. It seems like you’ve already signed up for a Starter Kit. Click <a href=":url" class="modal-trial-plan-text-a">here<a> to check on your subscription status.',
            'restart_plan' => 'If you’ve already cancelled your shave plan but would like to restart, click <a href=":url" class="modal-trial-plan-text-a">here<a> to customize your new shave plan.',
            'pay_now' => 'Pay Now',
        ],
        'user' => [
            'create_account' => 'Create Account',
            'name' => 'Name',
            'email_address' => 'Email Address',
            'password' => 'Password',
            'date_of_birth' => 'Date of Birth',
            'sign_in_facebook' => 'Sign in with Facebook',
            'have_account' => 'Already have an account? <u class="btn-sign-in">Sign in</u>',
            'no_account' => 'Don’t have account? <u class="btn-create-one">Create one</u>',
            'personal_details' => 'Personal Details',
            'email' => 'Email',
            'sign_in' => 'Sign In',
            'm_next_btn' => 'CONFIRM ACCOUNT',
        ],
        'address' => [
            'address' => 'Address',
            'shipping_address' => 'Shipping Address',
            'delivery_address' => 'Delivery Address',
            'billing_address' => 'Billing Address',
            'address_details' => 'Address Details',
            'recipient_name' => 'Recipient Name',
            'phone' => 'Phone',
            'unit_details' => 'Unit No./Street/Area',
            'state' => 'State',
            'town' => 'Town',
            'town_city' => 'Town/City',
            'flat' => 'flat',
            'postcode' => 'Postcode',
            'postal_code' => 'Postal Code',
            'different_address' => 'Bill to a different address',
            'save_address' => 'Save Address',
            'edit_address' => 'Edit Address',
            'estimated_delivery' => 'Estimated delivery :date1 - :date2',
            'm_next_btn' => 'CONFIRM ADDRESS',
        ],
        'payment_method' => [
            'payment_method' => 'Payment Method',
            'card_number' => 'Card Number',
            'expiry_date' => 'Expiry Date (MM/YY)',
            'cvv' => 'CVV',
            'terms_of_service_agree' => 'By making a purchase, you accept the <u><a href=":url" style="color: #FE5000">Terms of Service</a></u> (update effective as of 11/17/17). Your subscription will automatically renew and your credit card will be charged the subscription fee. You can cancel or modify your plan at any time from your profile page.',
            'add_card' => 'Add Card',
            'save_card' => 'Save Card',
            'selected_card' => 'Selected Card Number',
            'add_new_card' => 'Add New Card',
            'cancel' => 'Cancel',
            'm_next_btn' => 'CONFIRM CARD',
        ],
        'payment_summary' => [
            'summary' => 'Summary',
            'total_summary' => 'Total Summary',
            'ships_today' => 'Ships Today',
            'subtotal' => 'Subtotal ',
            'shipping' => 'Shipping ',
            'discount' => 'Discount ',
            'subtotal1' => 'Subtotal <span class="pull-right">:currency <span id="c-subtotal">:current_price</span></span>',
            'shipping1' => 'Shipping <span class="pull-right">:currency <span id="c-shipping">:shipping_fee</span></span>',
            'discount1' => 'Discount <span class="pull-right">:currency <span id="c-discount">:discount</span></span>',
            'promo_code' => 'Promo Code',
            'enter_promo_code' => 'Enter Promo Code',
            'apply' => 'Apply',
            'today_total' => 'Today Total',
            'ships_charges' => 'Ships & Charges :nextBillingDate',
            'shave_plan' => 'Shave Plan',
            'total' => 'Total',
            'total_v2' => 'Total <span class="pull-right">:currency <span id="c-ntotal">:next_price</span></span>',
            'total1' => 'Total ',
            'deliver_frequency' => 'Deliver every :planFrequency months',
            'next_delivery' => 'Next Delivery',
        ],
    ],
    'thankyou' => [
        'thank_you' => 'Thank You',
        'order_number' => 'Your order number is',
        'estimated_delivery' => 'The estimated delivery date is <b>:date1 - :date2</b>.',
        'email_us' => 'Email us at <a href="mailto:help.my@shaves2u.com">help.my@shaves2u.com</a> with any questions or suggestions.',
        'email_us_sg' => 'Email us at <a href="mailto:help.sg@shaves2u.com">help.sg@shaves2u.com</a> with any questions or suggestions.',
        'email_us_hk' => 'Email us at <a href="mailto:help.hk@shaves2u.com">help.hk@shaves2u.com</a> with any questions or suggestions.',
        'back_to_home' => 'Back to home',
    ],
    'order_status' => [
        'Pending' => 'Pending',
        'Payment Failure' => 'Payment Failure',
        'Payment Received' => 'Payment Received',
        'Processing' => 'Processing',
        'Delivering' => 'Delivering',
        'Completed' => 'Completed',
        'Cancelled' => 'Cancelled',
        'Returned' => 'Returned',
    ],
];
