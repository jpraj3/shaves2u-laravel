<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cancellation Journey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for cancellation journey content.
    |
    */

    "modal" => [

        "global" => [
            "any-other-reason" => "Any other reason:",
            "cancel-shave-plan" => "CANCEL MY SHAVE PLAN",
            "cancel-subscription" => "CANCEL MY SUBSCRIPTION",
            "close" => "CLOSE",
            "dont-cancel" => "NO, DON'T CANCEL",
            "get-new-cassete" => "GET NEW CASSETE",
            "keep-shaving" => "KEEP SHAVING FOR LESS",
            "ok-great" => "OK, GREAT!",
            "other-reason" => "Other reason:",
            "proceed-cancellation" => "PROCEED WITH CANCELLATION",
            "proceed-next" => "PROCEED NEXT",
            "try-different-blade" => "TRY A DIFFERENT BLADE"
        ],
        "main" => [
            "header" => "Hold on, will you let us know why you're cancelling?",
            "subheader-1" => "We're sorry to hear that you're dissatisfied with your Shave Plan.",
            "subheader-2" => "Please take a minute to tell us why.",
        ],
        "option1" => [
            "header" => "We should have communicated better",
            "subheader-1" => "We're sorry you're unhappy with your Shave Plan.",
            "subheader-2" => "Do you like the blades though?",
            "text-1" => "MOST POPULAR",
            "text-2" => "YES, THE BLADES WERE GOOD",
            "text-3" => 'In that case, let us give you a <div class="discount">:discount% DISCOUNT</div> on your next billing cycle.',
            "text-4" => "No, I guess this is goodbye."
        ],
        "option2" => [
            "header" => "We're listening",
            "subheader-1" => "We're sorry about the experience you had with our product.",
            "subheader-2" => "Please take a minute to tell us what went wrong."
        ],
        "option2_1" => [
            "header" => "Why not try a different blade instead?",
            "subheader-1" => "Our variety of blades are made to cater to the different shaving requirements of each man.",
            "text-1" => "As a subsriber to our Shave Plan, you have the flexibility to change your preferred blade type anytime you want. Why not try a different blade instead?"
        ],
        "option2_2" => [
            "header" => "That shouldn't happen",
            "subheader-1" => "Let us make it up to you with a new pack of blades.",
            "text-1" => "We're sorry about the experience you had with out product. Let us make it up to you with a brand new pack of blades."
        ],
        "option3" => [
            "header" => "Let's work things out",
            "subheader-1" => "How about customising your Shave Plan?",
            "text-1" => "You can change the type of blade or frequency of delivery to suit your budget.",
            "text-2" => "deliver every",
            "text-3" => 'You also get a <label class="discount">:discount% DISCOUNT</label> on your next billing cycle.'
        ],
        "option3_pause" => [
            "header" => "Let's work things out",
            "subheader-1" => "How about pausing your Shave Plan?",
            "text-1" => "Since you have too many blades in stock, how about we hold your next delivery for <label class=\"month\">:months months?</label>",
            "text-2" => "We'll also give you a <label class=\"discount\">:discount% DISCOUNT</label> off your next billing cycle when your Shave Plan resumes."
        ],
        "option4" => [
            "header" => "Let's work things out",
            "subheader-1" => "How about customising your Shave Plan?",
            "text-1" => "You can change the frequency of delivery to suit your usage.",
            "text-2" => "Suggestion: Deliver every",
            "text-3" => 'You also get a <label class="discount">:discount% DISCOUNT</label> on you next billing cycle.'
        ],
        "option4_pause" => [
            "header" => "Let's work things out",
            "subheader-1" => "How about pausing your Shave Plan?",
            "text-1" => "Since you have too many blades in stock, how about we hold your next delivery for <label class=\"month\">:months months?</label>",
            "text-2" => "We'll also give you a <label class=\"discount\">:discount% DISCOUNT</label> off your next billing cycle when your Shave Plan resumes."
        ],
        "option5" => [
            "header" => "We're not ready to see you go!",
            "text-1" => "We commit to delivering high-quality products at lower prices, so that you don't pay more than you should.",
            "text-2" => "Can we persuade you to stay a while longer?",
            "text-3" => 'You also get a <div class="discount">:discount% DISCOUNT</div> on your next billing cycle.'
        ],
        "option6" => [
            "header" => "For real?",
            "subheader-1" => "How about we cut you a deal on your next bill, just this once?",
            "text-1" => "MOST POPULAR",
            "text-2" => '<div class="discount">:discount% DISCOUNT</div> on your next billing cycle',
            "text-3" => 'No, I guess this is a goodbye.'
        ],
        "option7" => [
            "header" => "We're not ready to see you go!",
            "text-1" => "We commit to delivering high-quality products at lower prices, so that you don't pay more than you should.",
            "text-2" => "Can we persuade you to stay a while longer?",
            "text-3" => 'You also get a <div class="discount">:discount% DISCOUNT</div> on your next billing cycle.'
        ],
        "cancelled-page" => [
            "header" => "We are sorry to see you go",
            "subheader-1" => "Thank you for choosing Shaves2U.",
            "subheader-2" => "If you ever change your mind, we will be right here waiting for you."
        ],
        "blade-updated-page" => [
            "header" => "Here's your updated Shave Plan!",
            "subheader-1" => "You have changed your blade type.",
            "subheader-2" => "We hope this works better for you."
        ],
        "free-product-applied-page" => [
            "header" => "Your new blades are on its way!",
            "subheader-1" => "Your satisfaction is important to us.",
            "subheader-2" => "We hope you'll find this new blades work better than the last one."
        ]

    ]
];
