<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cancellation Journey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used for cancellation journey content.
    |
    */

    "welcomebackpromo" => [
        "header" => "We've missed you",
        "header1" => "Hey there! It's been awhile since we've last heard from you. We missed you badly and have <br>prepared a great welcome back gift exclusively for you.",
        "title" => "Your Plan",
        "image_subtile" => "Upgrade to an annual plan and get:",
        "image_li1" => "One-time offer free After Shave Cream (x1)",
        "image_li2" => "20% discount for every subsequent purchase of blade cartidge pack",
        "image_li3" => "FREE Shipping",
        "select_blade" => "Select your preferred blade type",
        "select_quantity" => "Select your quantity",
        "cassette" => "units of cassette refills",
        "select_add-on" => "Select your add-on products",
        "select_add-on_sub" => "You may add more than one",
        "btn"  => "LET'S GO!",
        "redirect_card_title" => "Thank you for trusting us with </br>your grooming needs.",
        "redirect_card_plan_detail" => "Here are the details of your plan.",
        "redirect_card_free_after_shavecream" => "One-time offer free After Shave Cream (x1)",
        "redirect_card_price" => "Total Price",
        "redirect_card_date" => "Your plan starts tomorrow",
        "redirect_card_footer" => "Kindly take a moment to review and </br>update your credit card details if required.",
        "btn_yes"  => "YES",
        "btn_no"  => "NO",

        "thankyou_title"  => "Thank you for trusting us with your grooming needs.",
        "thankyou_button"  => "Back to home page."
    ]
];
