@section('javascript')

<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>

<script>
    if (window.location.hostname == 'dcistaging.com') {
        var GLOBAL_URL = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2] + '/' + '<?php echo strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']); ?>' + '-' + '<?php echo strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']); ?>';
        var GLOBAL_URL_V2 = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2];
        var GLOBAL_URL_V3 = '/' + window.location.pathname.split("/")[1] + '/' + window.location.pathname.split("/")[2] + '/';
    } else {
        var GLOBAL_URL = '/' + '<?php echo strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']); ?>' + '-' + '<?php echo strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']); ?>';
        var GLOBAL_URL_V2 = '';
        var GLOBAL_URL_V3 = '/';
    }

</script>
<script language="javascript">
    var IMP = window.IMP; // 생략해도 괜찮습니다.
</script>
<script>
let current_localeV2 = "<?php echo strtoupper(app()->getLocale()) ?>";
let current_locale = "<?php echo strtolower(app()->getLocale()) ?>";
let current_country = "<?php json_encode(session()->get('currentCountry')); ?>";
window.localStorage.setItem('currentLocale', current_locale);
window.localStorage.setItem('currentCountry', current_country);
</script>
<script src="{{ asset('js/helpers/storage.service.js') }}"></script>

<script type="text/javascript">
    // get env variables
    let apptype = @php echo json_encode(config('app.env'))@endphp;
    // Passing Session info into JS variable
    var currentCountryData = @php echo json_encode(session() -> get('currentCountry'));
    @endphp;

    // Passing Locale info into JS variable
    var currentLocale = "@php echo strtolower(app()->getLocale()); @endphp";

    // This main condition sets Country info and locale in Local Storage Based on session
    if (getStorage("currentCountry")) {
        // If local storage data exist, do nothing.
    }
    // If local storage data does not exist, fall back to session data
    else {
        // If Session Data exist
        if (currentCountryData) {
            // Set the Local storage Data based on session storage
            addStorage("currentCountry", currentCountryData);
            addStorage("currentLocale", currentLocale);
            addStorage("current_lang", currentLocale);
        } else {
            // Redirect to root if session & local data does not exist
            @php redirect() -> route('locale.index');
            @endphp
        }
    }
</script>

<!-- Korean Address API -->
<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
<script src="{{ asset('js/helpers/laravel-helper.js') }}"></script>
<!-- Server Session -->
<script src="{{ asset('js/helpers/server-session.service.js') }}"></script>
<script>
    all_sessions = @php echo json_encode(session() -> all()) @endphp;
    // console.log(all_sessions);
    url_parameters = @php echo json_encode(session() -> get('url_parameters')) @endphp;
    window.addStorage("url_parameters", JSON.stringify(url_parameters));
    utm_parameters = @php echo json_encode(session() -> get('utm_parameters')) @endphp;
    window.addStorage("utm_parameters", JSON.stringify(utm_parameters));
</script>
<script>  window.translations = {!! Cache:: get('translations')!!};</script>
<script>
    // console.log(window.translations);
    function trans(key, replace = {}) {
        let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations);
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
        return translation;
    }
    function trans_choice(key, count = 1, replace = {}){
    let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations).split('|');
    translation = count > 1 ? translation[1] : translation[0];
        for (var placeholder in replace) {
            translation = translation.replace(`:${placeholder}`, replace[placeholder]);
        }
    return translation;
    }
    // console.log(trans('validation.custom.validation.email.email', {attribute: 'email'}));
    // console.log(trans_choice('validation.custom.validation.email.plural_test', 1,{attempts: 1}));
</script>
<script>
    function switchAccounts(){
        let url = window.location.origin + GLOBAL_URL + '/logoutV2';
        let method = "POST";
        AJAX(url, method, null).done(function(response) {
            // console.log(response);
            if(response === 'success'){
            location.reload();
            }
        })
    };
</script>
<script>
$(document).ready(function(){
    $("#notification_trial_plan_exist").on('hide.bs.modal', function(){
        window.location.replace(GLOBAL_URL);
    });
});
</script>
<script>
    let userCountryId = <?php echo view()->shared('callcode')->id; ?>;
    let phoneExt = <?php echo view()->shared('callcode')->callingCode; ?>;
</script>
@show
