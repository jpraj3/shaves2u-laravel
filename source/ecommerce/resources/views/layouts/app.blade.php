<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="theme-color" content="#fdb918">
    <div id="manifest"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    {{--
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script> --}}
    <script src="{{ asset('validator/jquery-validation-1.19.1/dist/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.3/lottie.min.js"></script>
    <link rel="shortcut icon" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- sweetalert2 popup -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2.v8/sweetalert2.min.css') }}">
    <!-- wow animation -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <!-- Toastr Notification Plugin -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr-v2.1.4/toastr.min.css')}}"/>
    <script src="{{ asset('plugins/toastr-v2.1.4/toastr.min.js') }}"></script>   <!-- End Toastr Notification Plugin -->
    <script src="{{ asset('plugins/sweetalert2.v8/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/js/wow.min.js') }}"></script>
    <script src="{{ asset('/js/cleave.min.js') }}"></script>

    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHXJ3CB');</script>
    <!-- End Google Tag Manager -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M7SX4VH');</script>
    <!-- End Google Tag Manager -->
    @endif

    <!-- Global change disabled input background color -->
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
                -moz-appearance:textfield;
        }
        input[type="text"]:disabled {
            background-color: white !important;
        }

        input[type="number"]:disabled {
            background-color: white !important;
        }

        .status-inactive {
            background-color: #ffc107 !important;
        }

        .status-active {
            background-color: #06cb7c !important;
        }

        .status-onhold {
            background-color: #6c757d !important;
        }

        select:disabled {
            background-color: white !important;
        }
        .success-promotion{
            color: green !important;
        }
        .error-promotion{
            color: red !important;
        }
    </style>
    <meta property="fb:app_id" content="723177734789504"/>
    <!-- Laravel Social Sharing  -->
    <script src="{{ asset('js/share.js') }}"></script>
    @include('layouts.styles.styles')
    @include('layouts.scripts.mainscripts')
    @include('layouts.scripts.social-login')
    {!! SEO::generate() !!}
    {!! OpenGraph::generate() !!}
</head>

<body id="hideBody">

    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHXJ3CB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7SX4VH"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
 <!-- Google Analytics -->
 <script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-XXXXX-X']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
<!-- End Google Analytics -->
    <div id="app">
        <header id="header">
            @if (Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.trial-plan'  )
                @include('layouts.header.starter_header')
            @elseif(Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.checkout')
                @include('layouts.header.payment_header')
            @elseif(Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection')
                @include('layouts.header.custom_header')
            @else
                @include('layouts.header.header')
            @endif
        </header>
        <main id="content">
            @include('loading.loading')
            @yield('content')
            @if(Auth::check())
                @if (Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.trial-plan' || Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection' || Route::currentRouteName() == 'locale.region.alacarte.ask.checkout' || Route::currentRouteName() == 'locale.region.alacarte.ask.checkout.confirm-purchase' || Route::currentRouteName() == 'locale.region.alacarte.ask.nicepay.checkout.confirm-purchase' || Route::currentRouteName() == 'locale.region.alacarte.ask' )
                    <div></div>
                @else
                    <link rel="stylesheet" href="{{ asset('css/referral-banner/referral-banner.css') }}">
                    <a href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                        <div id="blurbImg"></div>
                    {{-- <img src="{{URL::asset('/images/common/blurb_RM20.png')}}" class="referral_banner_footer d-none d-sm-block"/> --}}
                    <img src="{{URL::asset('/images/common/referral_icon_mobile.png')}}" class="referral_banner_footer d-block d-sm-none img-fluid"/>
                    <a>
                @endif
            @endif
        </main>
        <footer>
            @if(Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.trial-plan')
                @include('layouts.footer.starter_footer')
            @elseif(Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection' || Route::currentRouteName() == 'locale.region.alacarte.ask.checkout' || Route::currentRouteName() == 'locale.region.alacarte.ask' )

            @else
                @include('layouts.footer.footer')
            @endif
        </footer>
    </div>
</body>
{{-- Render Zendesk widget on All but pages with "checkout" in URL --}}
@if (strpos(request()->path(), 'checkout') == false)
    <!-- Start of shaves-2u Zendesk Widget script -->
    <script id="ze-snippet"
            src="https://static.zdassets.com/ekr/snippet.js?key=b8bf9b95-b617-4f7d-89ab-24e2e6a581a5"></script>
    <!-- End of shaves-2u Zendesk Widget script -->
@endif

@php
    // Get Sales Notification List From Session
    $sessionProductList = session()->get('SalesNotificationProductList');
@endphp


<script>
        /* Sample function that returns boolean in case the browser is Internet Explorer*/
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
            return is_ie;
        }

        /* Create an alert to show if the browser is IE or not */
        if (isIE()){
            $("#hideBody").html("<style>*{font-family: 'Muli', sans-serif; }p{font: 18px;}</style><div style='text-align: center'><img style='width: 50px;' src='{{asset('/images/common/logo-mobile.svg')}}'/></div><div style='text-align: center; color:#fe5000;'><h1>You are using an unsupported browser.</h1></div><div style='text-align:center'><p>For your information, the official Shaves2U website<br>cannot be accessed through Internet Explorer (IE).</p></div><div style='text-align:center'><p>You may try to install one of our recommended browser:</p></div><div style='text-align:center;'><p><b>Google Chrome</b></p></div><div style='text-align:center'><a href='https://www.google.com/chrome/browser/desktop/index.html'>Google Chrome Browser</a></div><div style='text-align:center'><a href='https://www.google.com/chrome/browser/mobile/index.html'>Google Chrome Mobile App</a></div><hr style='width: 70%'/><div style='text-align:center;'><b>Mozilla Firefox</b></div><div style='text-align:center'><a href='https://www.mozilla.org/en-US/firefox/new/'>Mozilla Firefox Browser</a></div>");
        }
    </script>

<script>

// var manifestCountry = window.localStorage.getItem('currentCountry');
// var manifestLocale = window.localStorage.getItem('currentLocale');

// console.log(manifestLocale)
// if(JSON.parse(manifestCountry).codeIso == "MY" && manifestLocale == "en"){
//     ('#manifest').html("<link rel='manifest' href='{{asset('/html/manifest/my/en-manifest.json')}}'>")
// } else if(JSON.parse(manifestCountry).codeIso == "KR" && manifestLocale == "ko"){
//     ('#manifest').html('<link rel="manifest" href="{{asset("/html/manifest/my/kr-manifest.json")}}">')
// } else if(JSON.parse(manifestCountry).codeIso == "SG" && manifestLocale == "en"){
//     ('#manifest').html('<link rel="manifest" href="{{asset("/html/manifest/my/en-manifest.json")}}">')
// } else if(JSON.parse(manifestCountry).codeIso == "HK" && manifestLocale == "zh"){
//     ('#manifest').html('<link rel="manifest" href="{{asset("/html/manifest/my/zh-hk-manifest.json")}}">')
// } else if(JSON.parse(manifestCountry).codeIso == "HK" && manifestLocale == "en"){
//     ('#manifest').html('<link rel="manifest" href="{{asset("/html/manifest/my/en-manifest.json")}}">')
// } else if(JSON.parse(manifestCountry).codeIso == "TW" && manifestLocale == "zh"){
//     ('#manifest').html('<link rel="manifest" href="{{asset("/html/manifest/my/zh-tw-manifest.json")}}">')
// }

var currentCountry = window.localStorage.getItem('currentCountry')

if(JSON.parse(currentCountry).codeIso === "MY" && JSON.parse(currentCountry).defaultLang === "EN"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/blurb_RM20.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
} else if (JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "EN"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/Referral-Icon-hkd.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
} else if (JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "ZH-HK"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/Referral-Icon-hkd-chi.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
} else if (JSON.parse(currentCountry).codeIso === "SG" && JSON.parse(currentCountry).defaultLang === "EN"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/Referral-Icon-sgd.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
} else if (JSON.parse(currentCountry).codeIso === "KR" && JSON.parse(currentCountry).defaultLang === "KO"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/Referral-Icon-kr.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
} else if (JSON.parse(currentCountry).codeIso === "TW" && JSON.parse(currentCountry).defaultLang === "ZH-TW"){
    $("#blurbImg").html("<img src='{{URL::asset('/images/common/Referral-Icon-tw.png')}}' class='referral_banner_footer d-none d-sm-block'/>")
}


</script>
<script>

    function initSalesNotification(SalesNotificationProductList) {
        // Get the key length of the product list
        const SalesNotificationProductKeys = Object.keys(SalesNotificationProductList);
        // Initiate Random Interval Where Notification will repeat
        let randomInterval = Math.floor(Math.random() * (60000 - 15000) + 15000);
        window.setInterval(function () {
            let randSalesNotificationKey = Math.floor(Math.random() * SalesNotificationProductKeys.length);
            toastr.sales('<div class="w-30 d-none d-lg-block"><img src="' + SalesNotificationProductList[randSalesNotificationKey]['url'] + '" class="w-100 sales-image" /></div><div class="w-70 p-2 d-none d-lg-block"><div><h6 class="color-orange"><b>24 People Have Bought This Item</b></h6></div><div><p class="text-dark mb-0">' + SalesNotificationProductList[randSalesNotificationKey]['name'] + '</p></div></div>');
            // Reset Random Interval
            randomInterval = Math.floor(Math.random() * (60000 - 15000) + 15000);
        }, randomInterval);
    }

    // Pass PHP json data to JS const
    const SalesNotificationProductList = <?php echo $sessionProductList; ?> ;
    $(function () {
        console.log(window.location.pathname);
        // If Current relative url contains "product"
        if (window.location.pathname.indexOf("product") > -1 && $(window).width() > 500 ) {
            // Set up toastr notification plugin settings
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "rtl": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            // Initiate Sales Notification Plugin
            initSalesNotification(SalesNotificationProductList);
        }
    })
</script>
</html>
