<style>
/* .custom-sq input[type=checkbox] {
  display: none;
} */

/* .custom-sq {
  font-size: 16px;
  line-height: 20px;
}
.custom-sq label {
  cursor: pointer;
}
.custom-sq label:before {
  display: inline-block;
  width: 20px;
  margin-right: 5px;
  content: "\00a0";
  text-align: center;
  background: #eee;
}
.custom-sq label:hover::before{
  background: #aaa;
}

.custom-sq input:checked ~ label:before {
  content: "\2605";
  background: #3f6cb5;
  color: #fff;
} */
</style>
<div class="container">
    <div class="row padd30 pb-0 pt-0">
            <div class="col-lg-12 bg-white pb-0 pt-3 text-left">
                <h3 class="MuliBold color-orange fs-24-md">@lang('website_contents.checkout.address.address')</h3>
                <form id="form_pe_delivery_popup">
                    <input type="hidden" id="update_address_type_popup_delivery" name="update_address_type_popup_delivery" value="delivery" />
                    <input type="hidden" id="adding_method_popup_delivery" name="adding_method_popup_delivery" value="new delivery" />
                    <div class="col-lg-12 padd20 pl-0 pr-0 pb-0">
                        @if(strtolower($currentCountryIso) == 'tw')
                        <div class="form-group">
                            <label>@lang('website_contents.checkout.address.ssn')</label>
                            <input name="pd_e_ssn_popup" id="pd_e_ssn_popup" type="text"
                                class="fs-20 form-control" value="" />
                            <div id="pd_e_ssn_popup_error" class="hasError col-12 col-form-label text-left px-0">
                            </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <label>@lang('website_contents.checkout.address.unit_details')</label>
                            <input name="pd_e_address_popup" id="pd_e_address_popup" type="text"
                                class="fs-20 form-control" value="" />
                            <div id="pd_e_address_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>@lang('website_contents.checkout.address.town_city')</label>
                            <input name="pd_e_city_popup" id="pd_e_city_popup" type="text" class="fs-20 form-control"
                                value="" />
                            <div id="pd_e_city_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>@lang('website_contents.checkout.address.postal_code')</label>
                            <input name="pd_e_portalCode_popup" id="pd_e_portalCode_popup" type="number"
                                class="fs-20 form-control" value="" />
                            <div id="pd_e_portalCode_popup_error" class="hasError col-12 col-form-label text-left px-0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>@lang('website_contents.checkout.address.state')</label>
                            <select class="fs-20 padd0 form-control" id="pd_e_state_popup" name="pd_e_state_popup">
                                             @if($states)
                                              @foreach ($states as $s)

                                           <option value="{{$s['name']}}">{{$s["name"]}}</option>

                                          @endforeach
                                         @endif
                                           </select>
                            <div id="pd_e_state_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                    </div>
                </form>
            </div>
    </div>
    {{-- <div class="custom-sq"><input type="checkbox" name="popup_different_billing" id="popup_different_billing"
            onclick="popup_check_if_different_billing_checked('delivery')"><label>Different Billing</label></div> --}}
</div>
