<style>
/* .custom-sq input[type=checkbox] {
  display: none;
} */

/* .custom-sq {
  font-size: 16px;
  line-height: 20px;
}
.custom-sq label {
  cursor: pointer;
}
.custom-sq label:before {
  display: inline-block;
  width: 20px;
  margin-right: 5px;
  content: "\00a0";
  text-align: center;
  background: #eee;
}
.custom-sq label:hover::before{
  background: #aaa;
}

.custom-sq input:checked ~ label:before {
  content: "\2605";
  background: #3f6cb5;
  color: #fff;
} */
</style>
<div class="container" style="padding:0px;">
    <div class="row padd30 pb-0 pt-0" style="padding:0px;">
            <div class="col-lg-12 bg-white pb-0 pt-3 text-left">
                <h3 class="MuliBold color-orange fs-24-md">주소</h3>
                <form id="form_pe_delivery_popup">
                <a href="javascript:;" onclick="koreanAddProfileDeliveryAddressPrefill('add')" class="text-center text-danger"
                style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-add-delivery"
                style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap"
                    style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()"
                    alt="접기 버튼">
</div>
                    <input type="hidden" id="update_address_type_popup_delivery" name="update_address_type_popup_delivery" value="delivery" />
                    <input type="hidden" id="adding_method_popup_delivery" name="adding_method_popup_delivery" value="new delivery" />
                    <div class="col-lg-12 padd20 pl-0 pr-0 pb-0">
                        <div class="form-group">
                            <label>상세주소</label>
                            <input name="pd_e_address_popup1" id="pd_e_address_popup1" type="text"
                                class="fs-20 form-control" value=""  readonly="readonly"  />
                                <input name="pd_e_address_popup" id="pd_e_address_popup" type="hidden"
                                class="fs-20 form-control" value=""   readonly="readonly" />
                            <div id="pd_e_address_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>단지, 동, 호</label>
                            <input name="pd_e_flat_popup" id="pd_e_flat_popup" type="text" class="fs-20 form-control"
                                value=""   onchange="onChangeAddProfileDeliveryAddressUnitNumber(this.value)"/>
                            <div id="pd_e_flat_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>도시</label>
                            <input name="pd_e_city_popup" id="pd_e_city_popup" type="text" class="fs-20 form-control"
                                value=""  readonly="readonly"/>
                            <div id="pd_e_city_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>군/구</label>
                            <input name="pd_e_state_popup" id="pd_e_state_popup" type="text" class="fs-20 form-control"
                                value=""  readonly="readonly"/>
                            <div id="pd_e_state_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>우편주소</label>
                            <input name="pd_e_portalCode_popup" id="pd_e_portalCode_popup" type="number"
                                class="fs-20 form-control" value=""  readonly="readonly"/>
                            <div id="pd_e_portalCode_popup_error" class="hasError col-12 col-form-label text-left px-0">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        {{-- <div id="popup_a_b" style="display: none !important;" class="padd30 d-flex">
            <div class="bg-white rounded shadow padd-lg-50 padd-md-15">
                <h3 class="MuliExtraBold fs-24-md">BILLING ADDRESS</h3>
                <h3 class="MuliBold color-orange fs-24-md">Address</h3>
                <form id="form_pe_billing_popup">
                    <input type="hidden" id="update_address_type_popup_billing" name="update_address_type_popup_billing" value="billing" />
                    <input type="hidden" id="adding_method_popup_billing" name="adding_method_popup_billing" value="new billing" />
                    <div class="col-lg-12 padd20 pl-0 pr-0 d-none d-sm-block">
                        <div class="form-group">
                            <label>Unit No./Street/Area</label>
                            <input name="pb_e_address_popup" id="pb_e_address_popup" type="text"
                                class="fs-20 form-control" value="" />
                            <div id="pb_e_address_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>Town/City</label>
                            <input name="pb_e_city_popup" id="pb_e_city_popup" type="text" class="fs-20 form-control"
                                value="" />
                            <div id="pb_e_city_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                        <div class="form-group">
                            <label>Postal Code</label>
                            <input name="pb_e_portalCode_popup" id="pb_e_portalCode_popup" type="number"
                                class="fs-20 form-control" value="" />
                            <div id="pb_e_portalCode_popup_error" class="hasError col-12 col-form-label text-left px-0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>State</label>
                            <select class="fs-20 padd0 form-control" id="pb_e_state_popup" name="pb_e_state_popup">
                                             @if($states)
                                              @foreach ($states as $s)

                                           <option value="{{$s['name']}}">{{$s["name"]}}</option>

                                          @endforeach
                                         @endif
                                           </select>
                            <div id="pb_e_state_popup_error" class="hasError col-12 col-form-label text-left px-0"></div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center padd20 pl-0  pr-md-30" style="bottom: 0px;">
                    </div>
                </form>
            </div>
        </div> --}}
    </div>
    {{-- <div class="custom-sq"><input type="checkbox" name="popup_different_billing" id="popup_different_billing"
            onclick="popup_check_if_different_billing_checked('delivery')"><label>Different Billing</label></div> --}}
</div>
