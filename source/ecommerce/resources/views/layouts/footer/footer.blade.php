@php
// Get Data from session
$sessionCountryData = session()->get('currentCountry');

// If Data from session exist
if (!empty($sessionCountryData)) {
// Set Current country info based on session Data
$currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
}
// If Data from Session Storage does not exist
else {
// Redirect back to / where session is set
redirect()->route('locale.index');
}
$currentLocale = strtolower(app()->getLocale());
@endphp

<style>
    .footer-link a {
        text-decoration: none;
        color: white;
    }

    .footer-link a:hover {
        text-decoration: none;
        color: #f64d00 !important;
    }

    .footer-email {
        padding-left: 1%;
    }
</style>

<footer id="footer">
    <div class="container pt-4 pl-0 pr-0 d-none d-lg-block">
        @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
        <div class="footer bg-faded pt-3 pl-4">
            <div class="row pr-0 pl-0">
                <!-- footer kr 1 -->
                <div class="col-sm-12 col-md-4 pr-0 pl-0">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="{{ route('locale.region.contact', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.contact_us')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="{{ route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.terms_of_use')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="{{ route('locale.region.privpolicy', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.privacy')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">
                            <a href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.faq')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">
                            <div class="social-media-icons col-sm-12 mt-3">
                                <div class="row">
                                    <div class="col-3 footer-link pl-0 pr-0">
                                        <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ/" target="_blank"><i class="fa fa-youtube-play" style="font-size:25px;"></i></a>
                                    </div>
                                    <div class="col-3 footer-link pl-0 pr-0">
                                        <a href="https://www.instagram.com/shaves2u/" target="_blank"><i class="fa fa-instagram" style="font-size:25px;"></i></a>
                                    </div>
                                    <div class="col-3 footer-link pl-0 pr-0">
                                        <a href="https://twitter.com/shaves2uasia" target="_blank"><i class="fa fa-twitter" style="font-size:25px;"></i></a>
                                    </div>
                                    <div class="col-3 footer-link pl-0 pr-0">
                                        <a href="https://www.facebook.com/Shaves2u/" target="_blank"><i class="fa fa-facebook-f" style="font-size:25px;"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">
                            <div class="col-12 pr-0 pl-0 pt-3 pb-4 text-left">

                                <h6 class="fs-16-md fs-12-lg mb-0 color-white footer-email">@lang('website_contents.global.footer.all_rights_reserved')</h6>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer kr 2 -->
                <div class="col-sm-12 col-md-4 pr-0 pl-0">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">

                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col2_kr_1')
                            </h4>

                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">

                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col2_kr_2')
                            </h4>

                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">

                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col2_kr_3')
                            </h4>

                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">

                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col2_kr_4')
                            </h4>

                        </div>
                    </div>
                </div>
                <!-- footer kr 3 -->
                <div class="col-sm-12 col-md-4 pr-0 pl-5">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">

                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col3_kr_1')
                            </h4>

                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="mailto:@lang('website_contents.global.footer.col3_kr_2_email')">
                                <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                    @lang('website_contents.global.footer.col3_kr_2')@lang('website_contents.global.footer.col3_kr_2_email')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="tel:@lang('website_contents.global.footer.col3_kr_3_phone')">
                                <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                    @lang('website_contents.global.footer.col3_kr_3')@lang('website_contents.global.footer.col3_kr_3_phone')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">
                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col3_kr_4')
                            </h4>
                        </div>
                        <div class="col-md-12 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5">
                            <h4 class="MuliBold fs-24-md fs-16-lg color-white">
                                @lang('website_contents.global.footer.col3_kr_5')
                            </h4>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        @else
        <div class="footer bg-faded pt-3">
            <div class="row pr-0 pl-0">
                <div class="col-sm-12 col-md-8 pr-0 pl-0">
                    <div class="row text-center">
                        <div class="col-md-3 col-sm-12 pr-0 pl-0 m-0 footer-link" style="max-width: 20%">
                            <a href="{{ route('locale.region.contact', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.contact_us')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-12 pr-0 pl-0 m-0 footer-link" style="max-width: 20%">
                            <a href="{{ route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.terms_of_use')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-12 pr-0 pl-0 m-0 footer-link" style="max-width: 20%">
                            <a href="{{ route('locale.region.privpolicy', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.privacy')
                                </h4>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-12 pr-0 pl-0 m-0 footer-link pr-5" style="max-width: 20%">
                            <a href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-24-md fs-16-lg">
                                    @lang('website_contents.global.footer.faq')
                                </h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="social-media-icons col-sm-12 col-md-3 offset-md-1">
                    <div class="row justify-content-center text-center">
                        <div class="col-4 footer-link pl-0 pr-0">
                            <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ/" target="_blank"><i class="fa fa-youtube-play" style="font-size:25px;"></i></a>
                        </div>
                        <div class="col-4 footer-link pl-0 pr-0">
                            <a href="https://www.instagram.com/shaves2u/" target="_blank"><i class="fa fa-instagram" style="font-size:25px;"></i></a>
                        </div>
                        <!-- <div class="col-3 footer-link pl-0 pr-0">
                            <a href="https://twitter.com/shaves2uasia" target="_blank"><i class="fa fa-twitter" style="font-size:25px;"></i></a>
                        </div> -->
                        <div class="col-4 footer-link pl-0 pr-0">
                            <a href="https://www.facebook.com/Shaves2u/" target="_blank"><i class="fa fa-facebook-f" style="font-size:25px;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 pr-0 pl-0 pt-3 pb-4 text-left">

                <h6 class="fs-16-md fs-12-lg mb-0 color-white">@lang('website_contents.global.footer.all_rights_reserved')</h6>

            </div>
        </div>
        @endif

    </div>
    <div class="container pt-4 pl-0 pr-0 d-block d-lg-none">
        <div class="footer bg-faded">
            <div class="row pr-0 pl-0">
                <div class="col-sm-12 pr-0 pl-0">
                    <div class="row justify-content-center text-center">
                        <div class="col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <a href="{{ route('locale.region.contact', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-18">
                                    @lang('website_contents.global.footer.contact_us')
                                </h4>
                            </a>
                        </div>
                        <div class="col-sm-12 pr-0 pl-0 footer-link">
                            <a href="{{ route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-18">
                                    @lang('website_contents.global.footer.terms_of_use')
                                </h4>
                            </a>
                        </div>
                        <div class="col-sm-12 pr-0 pl-0 footer-link">
                            <a href="{{ route('locale.region.privpolicy', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-18">
                                    @lang('website_contents.global.footer.privacy')
                                </h4>
                            </a>
                        </div>
                        <div class="col-sm-12 pr-0 pl-0 footer-link">
                            <a href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <h4 class="MuliBold fs-18">
                                    @lang('website_contents.global.footer.faq')
                                </h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="social-media-icons col-md-12">
                    <div class="row text-center">
                    @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                    <div class="col-3 footer-link pl-3 pr-0">
                            <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ/" target="_blank"><i class="fa fa-youtube-play" style="font-size:25px;"></i></a>
                        </div>
                        <div class="col-3 footer-link pl-0 pr-0">
                            <a href="https://www.instagram.com/shaves2u/" target="_blank"><i class="fa fa-instagram" style="font-size:25px;"></i></a>
                        </div>
                        <div class="col-3 footer-link pl-0 pr-0">
                            <a href="https://twitter.com/shaves2uasia" target="_blank"><i class="fa fa-twitter" style="font-size:25px;"></i></a>
                        </div>
                        <div class="col-3 footer-link pl-0 pr-0">
                            <a href="https://www.facebook.com/Shaves2u/" target="_blank"><i class="fa fa-facebook-f" style="font-size:25px;"></i></a>
                        </div>
                    @else
                    <div class="col-4 footer-link pl-3 pr-0">
                            <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ/" target="_blank"><i class="fa fa-youtube-play" style="font-size:25px;"></i></a>
                        </div>
                        <div class="col-4 footer-link pl-0 pr-0">
                            <a href="https://www.instagram.com/shaves2u/" target="_blank"><i class="fa fa-instagram" style="font-size:25px;"></i></a>
                        </div>
                        <!-- <div class="col-3 footer-link pl-0 pr-0">
                            <a href="https://twitter.com/shaves2uasia" target="_blank"><i class="fa fa-twitter" style="font-size:25px;"></i></a>
                        </div> -->
                        <div class="col-4 footer-link pl-0 pr-0">
                            <a href="https://www.facebook.com/Shaves2u/" target="_blank"><i class="fa fa-facebook-f" style="font-size:25px;"></i></a>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
            <!-- <div class="row pr-0 pl-0" style="border-top: 1px solid white;"> -->
            <div class="row pr-0 pl-0">
                <div class="col-sm-12 pr-0 pl-0 pt-2">
                    <div class="row justify-content-center kr-footer-sec-2">
                        <div class="col-sm-12 pr-0 pl-0 m-0 footer-link">
                            <h4 class="MuliBold fs-16 color-white">
                                @lang('website_contents.global.footer.col2_kr_1')
                                <br>
                                @lang('website_contents.global.footer.col2_kr_2')
                                <br>
                                @lang('website_contents.global.footer.col2_kr_3')
                                <br>
                                @lang('website_contents.global.footer.col2_kr_4')
                            </h4>
                        </div>
                        <div class="col-sm-12 pr-0 pl-0 footer-link">

                            <h4 class="MuliBold fs-16 color-white">
                            @lang('website_contents.global.footer.col3_kr_1')
                            <br>
                            @lang('website_contents.global.footer.col3_kr_2')@lang('website_contents.global.footer.col3_kr_2_email')
                            <br>
                            @lang('website_contents.global.footer.col3_kr_3')@lang('website_contents.global.footer.col3_kr_3_phone')
                            <br>
                            @lang('website_contents.global.footer.col3_kr_4')
                            <br>
                            @lang('website_contents.global.footer.col3_kr_5')
                            </h4>

                        </div>

                    </div>
                </div>
            </div>
            @endif
            <div class="col-12 pr-0 pl-0 pt-3 pb-4 text-center">
                <h6 class="fs-16-md fs-12-lg mb-0 color-white">@lang('website_contents.global.footer.all_rights_reserved')</h6>
            </div>
        </div>
    </div>
</footer>