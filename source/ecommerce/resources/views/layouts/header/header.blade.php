@php
// Get Data from session
$sessionCountryData = session()->get('currentCountry');

// If Data from session exist
if (!empty($sessionCountryData)) {
// Set Current country info based on session Data
$currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
$urllang = strtolower(json_decode($sessionCountryData, true)['urlLang']);
}
// If Data from Session Storage does not exist
else {
// Redirect back to / where session is set
redirect()->route('locale.index');
}
$currentLocale = strtolower(app()->getLocale());

@endphp
<style>

.topLogo-desktop{
    width: 40px;
    height: 40px;
    margin-left: 70px;
    margin-right: 20px;
}

.topLogo-mobile{
    width:40px;
    height: 40px;
    margin: 0 auto;
}


.nav-link:hover {
    text-decoration: none;
    color: #f64d00 !important;
}
/*.navbar-brand {*/
/*    margin-right: unset;*/
/*}*/
    @media only screen and (max-width:575px) {
        .navbar-brand {
            margin-right:unset;
        }
    }



@media (min-width: 992px) {
    .dropdown .dropdown-menu {
        /* transform: translate3d(49px, 49px, 0px) !important; */
        width: 100%;
        /* background-color: rgb(77, 77, 77); */
        border-radius: 0px;
        border: none;
    }
}

@media (max-width: 991px) {
    .dropdown .dropdown-menu {
        transform: translate3d(0, 49px, 0) !important;
        width: 100%;
        background-color: rgb(77, 77, 77);
        border-radius: 0px;
        border: none;
    }
}
</style>


<!-- Desktop Header Start-->
<div class="col-lg-12 d-none d-lg-block p-0">
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm bold-lg padd0 pt-3 pb-3">
    <div class="container-fluid">
            <button class="navbar-toggler border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_left" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon" style="background-image: none;">
                    <i class="fa fa-bars color-orange"></i>
                </span>
            </button>
            <a class="navbar-brand topLogo-desktop d-none d-lg-block" href="{{ route('locale.region', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img src="{{asset('/images/common/logo-mobile.svg')}}"/>
             </a>
            <a class="navbar-brand topLogo-mobile d-block d-lg-none" href="{{ route('locale.region', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <img src="{{asset('/images/common/logo-mobile.svg')}}"/>
            </a>
            <a class="nav-link font-weight-bold color-orange float-right d-lg-none" href="{{ route('locale.region.authenticated.dashboard', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <i class="fa fa-user color-white fs-30" style="-webkit-text-stroke: 1px #ff5001;"></i>
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent_left">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto center-md" style="text-transform: uppercase;">
                    <li class="nav-item uppercase-sm" style="font-size:15px;margin:auto;">
                        <a class="nav-link" href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>$urllang, 'countryCode'=>$currentCountryIso]) }}">@lang('website_contents.global.header.get_started')</a>

                        <!--
                        <a class="nav-link" href="{{ route('locale.region.shave-plans.trial-plan', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">Get Started</a>
                        -->
                    </li>
                    <li class="nav-item uppercase-sm" style="font-size:15px;margin:auto;">
                        <a class="nav-link" href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.products')</a>
                    </li>
                    <li class="nav-item uppercase-sm" style="font-size:15px;margin:auto;">
                        <a class="nav-link" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.help')</a>
                    </li>
                    @if((view()->shared('currentCountryIso') === 'my' || view()->shared('currentCountryIso') === 'sg' || view()->shared('currentCountryIso') === 'hk') && view()->shared('url') === 'en')
                    <li class="nav-item uppercase-sm" style="font-size:15px;margin:auto;">
                        <a class="nav-link" href="{{ route('locale.magazines.list', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.magazine')</a>
                    </li>
                    @endif
                    <li class="nav-item uppercase-sm" style="font-size:15px;margin:auto;">
                        <a class="nav-link" href="{{ route('locale.region.referral', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.referral')</a>
                    </li>
                    <li class="nav-item uppercase-sm dropdown d-lg-none" style="font-size:15px">
                        <a class="nav-link" style="font-size:15px" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @foreach (view()->shared('allcountry') as $u)
                            @if(strtolower($u['urlLang']) == strtolower(view()->shared('url')) && strtolower($u['codeIso']) == strtolower(view()->shared('currentCountryIso')) )
                            <span class="d-none d-lg-block">
                                <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.$u["codeIso"].'.png')}}" alt="">
                                {{$u["name"]}}
                            </span>
                            <span class="d-lg-none ml-3">
                                    @lang("website_contents.language") <i class="fa fa-caret-down float-right mt-1 fs-25"></i>
                            </span>
                            @endif
                            @endforeach
                        </a>
                        <div class="dropdown-menu center-md" aria-labelledby="navbarDropdownMenuLink" style="font-size:15px">
                            @foreach (view()->shared('allcountry') as $u)
                            <a class="dropdown-item" href="{{ route('locale.region', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=>strtolower($u['codeIso'])]) }}">
                                <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.$u["codeIso"].'.png')}}" alt="">
                                {{$u["name"]}}({{$u["urlLang"]}})
                            </a>
                            @endforeach
                        </div>
                    </li>
                </ul>
            </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent_right" style="font-size:16px; text-transform: uppercase;">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto center-md">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item border-lg-right">
                        <a class="nav-link color-orange" href="{{ route('login', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.login')</a>
                    </li>
                    @if (Route::has('register'))
                    @endif
                    @else
                    <li class="nav-item dropdown border-lg-right d-none d-lg-block">
                        <a class="nav-link font-weight-bold color-orange" href="{{ route('locale.region.authenticated.dashboard', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                            @lang('website_contents.authenticated.content.logged_in_name', ['name' => Auth::user()->firstName])
                        </a>
                    </li>
                    @endguest

                    <li class="nav-item dropdown d-none d-lg-block">
                        <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @foreach (view()->shared('allcountry') as $u)
                            @if(strtolower($u['urlLang']) == strtolower(view()->shared('url')) && strtolower($u['codeIso']) == strtolower(view()->shared('currentCountryIso')) )
                            <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.strtoupper($u["codeIso"]).'.png')}}" alt="">
                            {{$u["name"]}}
                            @endif
                            @endforeach
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            @if(view()->shared('allcountry'))
                            <?php

                            function unique_key($array, $keyname)
                            {
                                $new_array = array();
                                foreach ($array as $key => $value) {

                                    if (!isset($new_array[$value[$keyname]])) {
                                        $new_array[$value[$keyname]] = $value;
                                    }
                                }
                                $new_array = array_values($new_array);
                                return $new_array;
                            }

                            ?>
                            @php($allcountry_unique = unique_key(view()->shared('allcountry'),'name'))
                            @foreach ($allcountry_unique as $u)
                            <a class="dropdown-item" href="{{ route('locale.region', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=>strtolower($u['codeIso'])]) }}">
                                <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.strtoupper($u["codeIso"]).'.png')}}" alt="">
                                {{$u["name"]}}
                            </a>
                            @endforeach
                            @endif
                        </div>
                    </li>
                    <li class="nav-item d-none d-lg-block" style="margin:auto;">
                        <div class="row mr-0 ml-0 pt-2 pb-2">
                            @if(view()->shared('allcountry'))
                            @foreach (view()->shared('allcountry') as $u)
                            @if(strtolower($u['codeIso']) == strtolower(view()->shared('currentCountryIso')) )
                            @if(strtolower($u['urlLang']) == strtolower(view()->shared('url')) )
                            <u><a class="nav-link pt-0 pb-0" href="{{ route('locale.region', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=>strtolower($u['codeIso'])]) }}">{{$u["urlLang"]}}</a></u>
                            @else
                            <a class="nav-link pt-0 pb-0" href="{{ route('locale.region', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=>strtolower($u['codeIso'])]) }}">{{$u["urlLang"]}}</a>
                            @endif
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- Desktop Header End -->

<!-- Mobile Header Start -->
<div class="col-xs-12 d-block d-lg-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm bold-lg padd0">
        <div class="container-fluid">
            <button class="navbar-toggler border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_left" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon" style="background-image: none;">
                <i class="fa fa-bars color-orange"></i>
            </span>
            </button>
            <?php
            $urllang = view()->shared('url');
            $iso = view()->shared('currentCountryIso');
            $url = url("/" . $urllang . "-" . $iso);
            ?>
            <a class="navbar-brand text-center" href="{{ $url }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img class="topLogo-mobile" src="{{asset('/images/common/logo-mobile.svg')}}"/>
            </a>
            <a class="nav-link font-weight-bold color-orange float-right d-lg-none" href="{{ route('locale.region.authenticated.dashboard', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <i class="fa fa-user color-white fs-30" style="-webkit-text-stroke: 1px #ff5001;"></i>
            </a>
        </div>
    </nav>

    <div class="collapse navbar-collapse" id="navbarSupportedContent_left" style="background-color: #4d4d4d;">
    <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto center-md" style="text-transform: uppercase;">
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2" style="font-size:16px, ">
                <a class="nav-link" style="color:white;" href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>$urllang, 'countryCode'=>$currentCountryIso]) }}">@lang('website_contents.global.header.get_started')</a>

                <!--
                <a class="nav-link" href="{{ route('locale.region.shave-plans.trial-plan', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">Get Started</a>
                -->
            </li>
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2" style="font-size:16px">
                <a class="nav-link" style="color:white;" href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.products')</a>
            </li>
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2" style="font-size:16px">
                <a class="nav-link" style="color:white;" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.help')</a>
            </li>
            @if(view()->shared('currentCountryIso') === 'my')
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2" style="font-size:16px">
                    <a class="nav-link" style="color:white;" href="{{ route('locale.magazines.list', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.magazine')</a>
                </li>
            @endif
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2" style="font-size:16px">
                <a class="nav-link" style="color:white;" href="{{ route('locale.region.referral', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.header.referral')</a>
            </li>
            <li class="nav-item uppercase-sm py-2 px-5 py-lg-2 dropdown d-lg-none" style="font-size:16px">
                <a class="nav-link" style="color:white;" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @foreach (view()->shared('allcountry') as $u)
                    @if(strtolower($u['urlLang']) == strtolower(view()->shared('url')) && strtolower($u['codeIso']) == strtolower(view()->shared('currentCountryIso')) )
                    <span class="d-none d-lg-block">
                        <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.$u["codeIso"].'.png')}}" alt="">
                        {{$u["name"]}}
                    </span>
                    <span class="d-lg-none ml-3">
                            @lang("website_contents.language") <i class="fa fa-caret-down float-right mt-1 fs-25"></i>
                    </span>
                    @endif
                    @endforeach
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="">
                    @foreach (view()->shared('allcountry') as $u)
                    <a class="dropdown-item" style="
                    color: white;" href="{{ route('locale.region', ['langCode'=>strtolower($u['urlLang']), 'countryCode'=>strtolower($u['codeIso'])]) }}">
                        <img class="ico icon-country mr-2 click-able" src="{{asset('/images/common/icon-'.$u["codeIso"].'.png')}}" alt="">
                        {{$u["name"]}}({{$u["urlLang"]}})
                    </a>
                    @endforeach
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- Mobile Header End -->
