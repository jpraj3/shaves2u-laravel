@php
// Get Data from session
$sessionCountryData = session()->get('currentCountry');

// If Data from session exist
if (!empty($sessionCountryData)) {
// Set Current country info based on session Data
$currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
}
// If Data from Session Storage does not exist
else {
// Redirect back to / where session is set
redirect()->route('locale.index');
}
$currentLocale = strtolower(app()->getLocale());
@endphp
<style>
    .topLogo-desktop {
        width: 40px;
        height: 40px;
        margin-left: 70px !important;
        margin-right: 20px !important;
    }


    .topLogo-mobile {
        width: 40px;
        height: 40px;
    }

    .fs-16 {
        font-size: 16px;
    }

    .custom-logo-header {
        margin-left: 38%;
    }

    @media (max-width: 726px) {
        .custom-logo-header {
            margin-left: 36%;
        }
    }

    @media (max-width: 660px) {
        .custom-logo-header {
            margin-left: 32%;
        }
    }

    @media (max-width: 430px) {
        .custom-logo-header {
            margin-left: 24%;
        }
    }

</style>

<!-- Desktop Header Start-->
<div class="col-lg-12 d-none d-lg-block p-0">
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm bold-lg padd0 pt-3 pb-3">
        <div class="container-fluid">
            <!-- <button class="navbar-toggler border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_left" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon" style="background-image: none;">
                    <i class="fa fa-bars color-orange"></i>
                </span>
            </button> -->
            <a class="navbar-brand  d-none d-lg-block topLogo-desktop" href="{{ route('locale.region', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img src="{{asset('/images/common/logo-mobile.svg')}}" />
            </a>

            <a class="navbar-brand  d-block d-lg-none custom-logo-header topLogo-desktop" href="{{ route('locale.region', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img src="{{asset('/images/common/logo-mobile.svg')}}" />
            </a>
            <!-- <a class="nav-link font-weight-bold color-orange float-right d-lg-none" href="{{ route('locale.region.authenticated.dashboard', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                <i class="fa fa-user color-white fs-30" style="-webkit-text-stroke: 1px #ff5001;"></i>
            </a> -->

            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent_right">
                    <div class="col-12 d-none d-sm-block inlineB">
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <div class="row d-table">
                                    <div id="step1-heading" class="topnav-1-heading col-10 text-center d-table-cell bord-thick fs-16 orange">
                                            @lang('website_contents.custom_plan.header.design_shave_plan')
                                    </div>
                                    <div id="step1-title" class="topnav-1-arrow col-2 d-table-cell fs-16">
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div id="step2-heading" class="topnav-2-heading col-10 text-center d-table-cell fs-16">
                                            @lang('website_contents.custom_plan.header.shipping_frequency')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<!-- Desktop Header End -->

<!-- Mobile Header Start -->
<div class="col-xs-12 d-block d-lg-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm bold-lg ">
        <div class="container-fluid">
        <?php
            $urllang = view()->shared('url');
            $iso = view()->shared('currentCountryIso');
            $url = url("/" . $urllang . "-" . $iso);
            ?>
            <a class="navbar-brand mx-auto" href="{{ $url }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img class="topLogo-mobile" src="{{asset('/images/common/logo-mobile.svg')}}"/>
            </a>
        </div>
    </nav>
</div>
<!-- Mobile Header End -->
