<!DOCTYPE html>
<html>
    <head>
        <style>
            .table-responsive {
                display: block;
                width: 100%;
                overflow-x: auto;
                box-sizing: border-box;
            }

            table {
                border: 0;
                width: 100%;
                margin-bottom: 1rem;
                color: #575757;
                background-color: transparent;
                border-collapse: collapse;
            }
            
            .table-bordered th, .table-bordered td {
                border: 1px solid #ebebeb;
            }

            .table th, .table td {
                padding: 0.75rem;
                vertical-align: top;
            }

            .text-right {
                text-align: right !important;
            }
        </style>
    </head>

    <body>
        <div>
            <header style="background-color: transparent; padding: 50px; text-align: center">
                <a href="{{ url('/') }}">
                    <img height="45" width="45" src="{{ public_path('/images/common/logo-mobile.svg') }}"/>
                </a>
            </header>
            <main>
                <div class="content">
                    <div class="row mx-0">
                        <div class="col-12">
                            <p>Hey {{ $data['deliverydetails']->firstName }},</p>
                            <p>Thanks for trusting us with your shaving needs. Your order is being processed, and we'll let you know when it's on its way to you.</p>
                        </div>
                        <div class="col-12">
                            <div class="table-responsive push">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th class="text-right">Quantity</th>
                                            <th class="text-right">Unit Price</th>
                                            <th class="text-right">Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody id="invoice">
                                        @foreach ($data['orderdetails'] as $orderdetails)
                                            <tr>
                                                <td>{{ $orderdetails->name }}</td>
                                                <td class="text-right">{{ $orderdetails->qty }}</td>
                                                <td class="text-right">{{ $data['invoice']->currency.' '.$orderdetails->price }}</td>
                                                <td class="text-right">{{ $data['invoice']->currency.' '.$orderdetails->total_price }}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3" class="font-w600 text-right">Sub Total</td>
                                            <td class="text-right" id="sub_total">{{ $data['invoice']->currency.' '.$data['invoice']->originPrice }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="font-w600 text-right">Discount</td>
                                        <td class="text-right" id="discount">{{ $data['invoice']->currency.' '.$data['invoice']->discountAmount }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="font-w600 text-right">Processing Fee</td>
                                            <td class="text-right" id="processing_fee">{{ $data['invoice']->currency.' '.$data['invoice']->chargeFee }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="font-w600 text-right">Grand Total</td>
                                            <td class="text-right" id="grand_total">{{ $data['invoice']->currency.' '.$data['invoice']->totalPrice }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="font-w600 text-right">Cash Rebate</td>
                                            <td class="text-right" id="cash_rebate"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="font-w700 text-uppercase text-right bg-body-light">Total Payable (Incl. Tax)</td>
                                            <td class="font-w700 text-right bg-body-light" id="total_payable"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                @if ($data['deliverydetails'] != null)
                                    <div class="col-6 pl-0 border-dark rounded">
                                        <p>SHIPPING</p>
                                        <p>{{ $data['deliverydetails']->address }}</p>
                                        <p>{{ $data['deliverydetails']->contactNumber }}</p>
                                    </div>
                                @endif
                                @if ($data['billingdetails'] != null)
                                    <div class="col-6 pr-0 border-dark rounded">
                                        <p>BILLING</p>
                                        <p>{{ $data['billingdetails']->address }}</p>
                                        <p>{{ $data['billingdetails']->contactNumber }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                
            </footer>
        </div>
    </body>

</html>