<?php
    // Get Data from session
    $sessionCountryData = session()->get('currentCountry');

    // If Data from session exist
    if (!empty($sessionCountryData)) {
        // Set Current country info based on session Data
        $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
    }
    // If Data from Session Storage does not exist
    else {
        // Redirect back to / where session is set
        redirect()->route('locale.index');
    }
    $currentLocale = strtolower(app()->getLocale());
?>

@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/shave-plan/shave-plan.css') }}">
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
<div class="container">
    <!--
    <button onclick="GoToTrialPlanPage()">Get your trial plan now!</button>
    -->
    <div class="row justify-content-center py-5 mb-5 mx-0">
        <div class="col-12 col-lg-7 py-5 mb-5">
            <div class="row mx-0">
                <div class="col-12 pt-5 pb-4 text-center">
                    <h1 class="MuliExtraBold fs-30-sm">@lang('website_contents.global.shave_plans.what_like_do')</h1>
                </div>
                <div class="col-12 col-lg-6 d-flex">
                    <div id="trial_kit" class="item-unselected w-100" style="padding:5px;margin-bottom:14px;">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-3 col-lg-12 p-0 py-lg-4 pt-lg-5 text-center d-flex align-items-center">
                                @if($m_h_sku == 'H3')
                                <img class="img-fluid mx-auto" src="{{ asset('/images/common/img-getstarted.png') }}" alt="" style="max-height: 150px;" />
                                @elseif($m_h_sku == 'H1')
                                <img class="img-fluid mx-auto" src="{{ asset('/images/common/premium/product/img-getstarted.png') }}" alt="" style="max-height: 150px;" />
                                @else
                                <img class="img-fluid mx-auto" src="{{ asset('/images/common/img-getstarted.png') }}" alt="" style="max-height: 150px;" />
                                @endif
                                </div>
                                <div class="col-9 col-lg-12 px-4 py-4 py-lg-0 d-flex align-items-center h-64-lg">
                                    <div class="col-12 padd0 center-lg">
                                        <p class="MuliBold fs-sm-16 fs-20 mb-0">@lang('website_contents.global.shave_plans.start_trial_kit')</p>
                                    </div>
                                </div>
                                <div class="col-12 py-3 text-center d-none d-lg-block">
                                    <div class="checkbox d-flex align-items-center">
                                        <label class="custom-checkbox mx-auto">
                                            <input id="trial_kit_check" type="checkbox" value="" disabled>
                                            <span style="height: 25px; width: 25px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-flex">
                    <div id="blade_refill" class="item-unselected w-100" style="padding:5px;margin-bottom:14px;">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-3 col-lg-12 p-0 py-lg-4 pt-lg-5 text-center d-flex align-items-center">
                                    <img class="img-fluid mx-auto" src="{{ asset('/images/common/Blades-s3.png') }}" alt="Chania" style="max-height: 150px;" />
                                </div>
                                <div class="col-9 col-lg-12 px-4 py-4 py-lg-0 d-flex align-items-center h-64-lg">
                                    <div class="col-12 padd0 center-lg">
                                        <p class="MuliBold fs-sm-16 fs-20 mb-0">@lang('website_contents.global.shave_plans.get_blade_refills')</p>
                                    </div>
                                </div>
                                <div class="col-12 py-3 text-center d-none d-lg-block">
                                    <div class="checkbox d-flex align-items-center">
                                        <label class="custom-checkbox mx-auto">
                                            <input id="blade_refill_check" type="checkbox" value="" disabled>
                                            <span style="height: 25px; width: 25px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-0 px-md-3 pt-4 btn-fixed">
                    <div class="row mx-lg-0 w-100">
                        <div class="row mx-lg-0 w-100">
                            <button class="btn btn-start MuliExtraBold fs-20" onclick="GoToTrialPlanPage()" style="min-width: 100%; padding: 20px !important;">@lang('website_contents.global.shave_plans.continue')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/functions/shave-plans/shave-plans.function.js') }}"></script>
<script>
    function GoToTrialPlanPage() {
        if ($("#trial_kit").hasClass('item-selected')) {
            location.href = "{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso]) }}";
        }
        else if ($("#blade_refill").hasClass('item-selected')) {
            location.href = "{{ route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso, '#refill']) }}";
        }
    }
</script>
@endsection
