@extends('layouts.app')
@section('content')
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
<link rel="stylesheet" href="{{ asset('css/custom-plan/custom-plan-selection.css') }}">
<style>
        #footer {
             /* position: fixed !important; */
         }
</style>
<div class="d-none d-md-block">
    <div class="container">
        <div class="row">
            <!-- <div class="col-12 pr-0 pl-0">
                <button id="button-back" class="button-back">
                    < BACK
                </button>
            </div> -->
        </div>
        <div id="collapseTitle1" class="row panel-collapse collapse show">
            <div class="row row-center">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h1 class="mt-5 MuliExtraBold trial-kit-title">
                        @lang('website_contents.custom_plan.selection.design_own_plan')
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <style>
            .img-scale {
                margin: auto;
                width: 100%;
            }
        </style>

        <div id="collapse1" class="row panel-collapse collapse show">
            <div class="col-6 custom-img-div">
                <div class="custom-bag-img-div">  <!-- invisible visible -->
                    <img src="{{URL::asset('/images/custom/customcheck_bag-off.png')}}" class="img img-responsive img-scale custom-product-image custom-bag-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.ToiletryBag'))}}-img"/>
                </div>
                <div class="custom-shavesleeve-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_shavesleeve-off.png')}}" class="img img-responsive img-scale custom-product-image custom-shavesleeve-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.RubberSleeve'))}}-img"/>
                </div>
                <div class="custom-aftershave-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_aftershave-off.png')}}" class="img img-responsive img-scale custom-product-image custom-aftershave-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.AfterShaveCream'))}}-img"/>
                </div>
                <div class="custom-shavecream-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_shavecream-off.png')}}" class="img img-responsive img-scale custom-product-image custom-shavecream-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.ShaveCream'))}}-img"/>
                </div>
                <div class="custom-swivelhandle-img-div">
                    @if(isset($m_h_sku) && $m_h_sku !== null && $m_h_sku == 'H3')
                    <img src="{{URL::asset('/images/common/premium/product/customcheck_premiumhandle-off.png')}}" class="img img-responsive img-scale custom-product-image custom-swivelhandle-img custom-{{config('global.' . config('app.appType') . '.MenSwivelHandle')}}-img"/>
                    @elseif(isset($m_h_sku) && $m_h_sku !== null && $m_h_sku == 'H1')
                    <img src="{{URL::asset('/images/custom/customcheck_swivelhandle-off.png')}}" class="img img-responsive img-scale custom-product-image custom-swivelhandle-img custom-{{config('global.' . config('app.appType') . '.MenSwivelHandle')}}-img"/>
                    @else
                    <img src="{{URL::asset('/images/custom/customcheck_swivelhandle-off.png')}}" class="img img-responsive img-scale custom-product-image custom-swivelhandle-img custom-{{config('global.' . config('app.appType') . '.MenSwivelHandle')}}-img"/>
                    @endif
                </div>
                <div class="custom-blade-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_6blades.png')}}" class="img img-responsive img-scale custom-product-image custom-blade-img"/>
                </div>
            </div>
            <div class="col-md-6 pr-0 pl-0">
                <div class="col-md-12 pt-5">
                    <div class="row">
                        <div class="col-md-12 MuliExtraBold color-orange">
                            <h2>@lang('website_contents.custom_plan.selection.customised_plan')</h2>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12 pt-2 pr-0 pl-0 pb-3">
                        <h4><b>@lang('website_contents.selection.select_blade')</b></h4>
                        <div class="col-md-12 pr-0 pl-0 pt-2">
                            @foreach($blade_products as $product)
                            <?php
                            $skunumber = substr($product['sku'], 1, 1);
                            ?>
                            <div class="p-2">
                                <div class="row pr-0 pl-0 ml-0 p-3 item-unselected <?php echo "blade-" . $product['productcountriesid']  ?>">
                                    <div class="col-md-3 pr-0 pl-0 ml-0">
                                        <img src="{{URL::asset('/images/common/checkoutAssets/'.$skunumber.'blades.png')}}" alt="{{$product['producttranslatesname']}}" class="img-fluid  div-text-center" />
                                    </div>
                                    <div class="col-md-5 pr-0 pl-3 pt-3">
                                        <h5><b>{{$product['producttranslatesname']}}</b></h5>
                                    </div>
                                    <div class="col-md-4 pr-2 pl-0 pt-3" align="right">
                                         <!-- Currency and price for kr -->
                                    @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                    <h5><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                    @else
                                    <h5><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- <div class="row pr-0 pl-0 ml-0 pt-5">
                            <div class="col-md-3 pr-0 pl-0 ml-0">
                                <img src="{{URL::asset('/images/common/checkoutAssets/5blades.png')}}" class="img-fluid" />
                            </div>
                            <div class="col-md-3 pr-0 pl-0 pt-3">
                                <h5><b>5 Blades</b></h5>
                            </div>
                            <div class="col-md-6 pr-2 pl-0 pt-3" align="right">
                                <h5><b>RM XX.XX</b></h5>
                            </div>
                        </div>
                        <div class="row pr-0 pl-0 ml-0 pt-5">
                            <div class="col-md-3 pr-0 pl-0 ml-0">
                                <img src="{{URL::asset('/images/common/checkoutAssets/6blades.png')}}" class="img-fluid" />
                            </div>
                            <div class="col-md-3 pr-0 pl-0 pt-3">
                                <h5><b>6 Blades</b></h5>
                            </div>
                            <div class="col-md-6 pr-2 pl-0 pt-3" align="right">
                                <h5><b>RM XX.XX</b></h5>
                            </div>
                        </div> -->
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12 pt-2 pr-0 pl-0 pb-5">
                        <h4><b>@lang('website_contents.custom_plan.selection.select_addons')</b></h4>
                        <h5 class="color-grey"><b>@lang('website_contents.custom_plan.selection.add_more')</b></h5>
                        <div class="col-md-12 pr-0 pl-0 pt-2">
                            @foreach($addon_products as $product)
                            <div class="pb-3">
                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "addon-" . $product['productcountriesid']  ?>">
                                    <div class="col-md-3 pr-0 pl-3 ml-0">
                                        <img src="{{$product['productDefaultImage']}}" alt="{{$product['producttranslatesname']}}" class="img-fluid" style="width:70px;" />
                                    </div>
                                    <p class="d-none productsku" >{{$product['sku']}}</p>
                                    <div class="col-md-5 pr-0 pl-3">
                                        <h5 class="div-text-center"><b>{{$product['producttranslatesname']}}</b></h5>
                                    </div>
                                    <div class="col-md-4 pr-2 pl-0" align="right">
                                         <!-- Currency and price for kr -->
                                    @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                    <h5 class="div-text-center"><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                    @else
                                    <h5 class="div-text-center"><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach($bag_products as $product)
                            <div class="pb-3">
                                <div class="row pr-0 pl-0 ml-0 p-2 addon-cycle item-unselected <?php echo "bag-" . $product['productcountriesid']  ?>" onclick='addonCycle("{{$product["productcountriesid"]}}","{{$product["producttranslatesname"]}}","{{$product["productDefaultImage"]}}","bag-")'>
                                    <div class="col-md-3 pr-0 pl-0 ml-0">
                                        <img src="{{$product['productDefaultImage']}}" alt="{{$product['producttranslatesname']}}" class="img-fluid" style="width:70px;" />
                                    </div>
                                    <p class="d-none productsku" >{{$product['sku']}}</p>
                                    <div class="col-md-5 pr-0 pl-0">
                                        <h5 class="div-text-center"><b>{{$product['producttranslatesname']}}</b></h5>
                                    </div>
                                    <div class="col-md-4 pr-2 pl-0" align="right">
                                         <!-- Currency and price for kr -->
                                    @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                    <h5 class="div-text-center"><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                    @else
                                    <h5 class="div-text-center"><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="col-12 text-center pt-2 pr-0 pl-0 ml-0">
                            <button class="button-next button-proceed-checkout MuliExtraBold w-100" type="submit">@lang('website_contents.selection.next_step')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="collapse2" class="row panel-collapse collapse pb-3">
            <div class="row row-center">
                <div class="row pb-5">
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h1 class="mt-5 MuliExtraBold trial-kit-title">
                            @lang('website_contents.selection.shave_frequency')
                        </h1>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h4 class="mt-3 MuliPlain">
                            @lang('website_contents.selection.change_frequency')
                        </h4>
                    </div>
                </div>
                @foreach($frequency_list as $frequency)
                <div class="col-md-3 mb-3 d-none d-sm-block" style="padding-bottom: 4.5vw;">
                    <div class="frequency-{{$frequency['duration']}} text-center pt-4 item-unselected" style="height: 365px;">
                        <img src="{{URL::asset($frequency['image'])}}" class="img-fluid" />
                        <h4 class="MuliExtraBold pt-4">
                            {!! $frequency['durationText'] !!}
                        </h4>
                        <h6 class="frequency-{{$frequency['duration']}}-details MuliPlain pt-4 pb-3 frequency-p">
                            {!! $frequency['detailsText'] !!}<br><br>
                        </h6>
                    </div>
                </div>
                @endforeach


                <div class="col px-0 text-center pt-5">
                    <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                    <button class="button-annual-pop-out2 button-proceed-checkout MuliExtraBold" type="button" style="width: 30%">@lang('website_contents.custom_plan.step3.review_order')</button>
                    <form id="form-checkout" method="GET" action="{{ route('locale.region.shave-plans.custom-plan.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                        <button id="proceed-checkout" class="d-none button-proceed-checkout MuliExtraBold" type="submit" style="width: 30%">@lang('website_contents.custom_plan.step3.review_order')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-block d-md-none">
    <div class="container">
        <div id="collapseTitle-mobile-1" class="row panel-collapse collapse show">
            <div class="row row-center">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h3 class="mt-3 MuliExtraBold trial-kit-title">
                    @lang('website_contents.custom_plan.selection.design_own_plan')
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div id="collapse-mobile-1" class="row panel-collapse collapse show m-0">
        <div class="container">
            <div class="col-md-6 pr-0 pl-0 ">
            <div class="custom-img-div">
                <div class="custom-bag-img-div">  <!-- invisible visible -->
                    <img src="{{URL::asset('/images/custom/customcheck_bag-off.png')}}" class="img img-responsive img-scale custom-product-image custom-bag-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.ToiletryBag'))}}-img"/>
                </div>
                <div class="custom-shavesleeve-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_shavesleeve-off.png')}}" class="img img-responsive img-scale custom-product-image custom-shavesleeve-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.RubberSleeve'))}}-img"/>
                </div>
                <div class="custom-aftershave-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_aftershave-off.png')}}" class="img img-responsive img-scale custom-product-image custom-aftershave-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.AfterShaveCream'))}}-img"/>
                </div>
                <div class="custom-shavecream-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_shavecream-off.png')}}" class="img img-responsive img-scale custom-product-image custom-shavecream-img custom-{{str_replace('/','',config('global.' . config('app.appType') . '.ShaveCream'))}}-img"/>
                </div>
                <div class="custom-swivelhandle-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_swivelhandle-off.png')}}" class="img img-responsive img-scale custom-product-image custom-swivelhandle-img custom-{{config('global.' . config('app.appType') . '.MenSwivelHandle')}}-img"/>
                </div>
                <div class="custom-blade-img-div">
                    <img src="{{URL::asset('/images/custom/customcheck_6blades.png')}}" class="img img-responsive img-scale custom-product-image custom-blade-img"/>
                </div>
            </div>
            </div>
            <div class="col-md-12 pt-2">
                <div class="row">
                    <div class="col-md-7 MuliExtraBold color-orange text-center">
                        <h2>@lang('website_contents.custom_plan.selection.customised_plan')</h2>
                    </div>
                </div>
                <hr>
                <div class="col-md-12 pt-3 pr-0 pl-0 pb-5">
                    <h4><b>@lang('website_contents.selection.select_blade')</b></h4>
                    <div class="col-xs-12 pr-0 pl-0 pt-4">
                        @foreach($blade_products as $product)
                        <?php
                        $skunumber = substr($product['sku'], 1, 1);
                        ?>
                        <div class="p-2">
                            <div
                                class="row pr-0 pl-0 ml-0 p-3 item-unselected <?php echo "blade-" . $product['productcountriesid']  ?>">
                                <div class="col-xs-6 pr-0 pl-0 ml-0">
                                    <img src="{{URL::asset('/images/common/checkoutAssets/'.$skunumber.'blades.png')}}"
                                        alt="{{$product['producttranslatesname']}}" class="img-fluid div-text-center" />
                                </div>
                                <div class="col-xs-6 pr-0 pl-0 pl-3">
                                    <h5><b>{{$product['producttranslatesname']}}</b></h5>
                                     <!-- Currency and price for kr -->
                                    @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                    <h5><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                    @else
                                    <h5><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                    @endif

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-12 pt-3 pr-0 pl-0 pt-5">
                        <h4><b>@lang('website_contents.custom_plan.selection.select_addons')</b></h4>
                        <h5 class="color-grey"><b>@lang('website_contents.custom_plan.selection.add_more')</b></h5>
                        <div class="col-md-12 pr-0 pl-0 pt-5">
                            @foreach($addon_products as $product)
                            <div class="pb-3">
                                <div
                                    class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "addon-" . $product['productcountriesid']  ?>">
                                    <div class="col-xs-6 pr-0 pl-0 ml-0">
                                        <img src="{{$product['productDefaultImage']}}"
                                            alt="{{$product['producttranslatesname']}}" class="img-fluid"
                                            style="width:70px;" />
                                    </div>
                                    <div class="col-xs-6 pr-0 pl-0 pl-5">
                                        <div class="div-text-center">
                                            <p class="d-none productsku">{{$product['sku']}}</p>
                                            <h5><b>{{$product['producttranslatesname']}}</b></h5>
                                            <!-- Currency and price for kr -->
                                            @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                            <h5><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                            @else
                                            <h5><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            {{-- @foreach($handle_products as $product)
                            <div class="pb-3">
                                <div class="row pr-0 pl-0 ml-0 p-2 addon-cycle item-unselected {{ "handle-" . $product['productcountriesid']  }}"
                                    onclick='addonCycle("{{$product["productcountriesid"]}}","{{$product["producttranslatesname"]}}","{{$product["productDefaultImage"]}}","handle-")'>
                                    <div class="col-xs-6 pr-0 pl-0 ml-0">
                                        <img src="{{$product['productDefaultImage']}}"
                                            alt="{{$product['producttranslatesname']}}" class="img-fluid"
                                            style="width:70px;" />
                                    </div>
                                    <div class="col-xs-6 pr-0 pl-0 pl-5">
                                        <div class="div-text-center">
                                            <p class="d-none productsku">{{$product['sku']}}</p>
                                            <h5><b>{{$product['producttranslatesname']}}</b></h5>
                                            <!-- Currency and price for kr -->
                                            @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                            <h5><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                            @else
                                            <h5><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach --}}
                            @foreach($bag_products as $product)
                            <div class="pb-3">
                                <div class="row pr-0 pl-0 ml-0 p-2 addon-cycle item-unselected <?php echo "bag-" . $product['productcountriesid']  ?>"
                                    onclick='addonCycle("{{$product["productcountriesid"]}}","{{$product["producttranslatesname"]}}","{{$product["productDefaultImage"]}}","bag-")'>
                                    <div class="col-xs-6 pr-0 pl-0 ml-0">
                                        <img src="{{$product['productDefaultImage']}}"
                                            alt="{{$product['producttranslatesname']}}" class="img-fluid"
                                            style="width:70px;" />
                                    </div>
                                    <div class="col-xs-6 pr-0 pl-0 pl-5">
                                        <div class="div-text-center">
                                            <p class="d-none productsku">{{$product['sku']}}</p>
                                            <h5><b>{{$product['producttranslatesname']}}</b></h5>
                                             <!-- Currency and price for kr -->
                                            @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                            <h5><b>{{number_format($product['sellPrice'])}}{{$currency}}</b></h5>
                                            @else
                                            <h5><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div id="mobile-button" class="col-12 pl-0 pr-0">
            <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">@lang('website_contents.selection.next_step')</button>
        </div>
    </div>

    <div id="collapse-mobile-2" class="panel-collapse collapse">
        <div class="row row-center">
            <div class="row pb-5 pl-3 pr-3">
                <div class="col-md-12 text-center pl-0 pr-0">
                    <h1 class="mt-5 MuliExtraBold trial-kit-title">
                        @lang('website_contents.selection.shave_frequency')
                    </h1>
                </div>
                <div class="col-md-12 text-center pl-1 pr-1">
                    <div class="col-md-12">
                        <h5 class="mt-3 MuliPlain">
                            @lang('website_contents.selection.change_frequency')
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-md-12 d-block d-lg-none pt-3" style="padding-bottom: 20vw;">
                @foreach($frequency_list as $frequency)
                <div class="col-12 mb-3">
                    <div class="row frequency-{{$frequency['duration']}} frequency-box-m text-center pt-4 item-unselected">
                        <div class="col-8" style="top:-10px">
                            <h4 class="MuliExtraBold pt-4">
                                {!! $frequency['durationText'] !!}
                            </h4>
                            <h6 class="frequency-{{$frequency['duration']}}-details Muliplain pt-4 frequency-p">
                                {!! $frequency['detailsText'] !!}<br><br>
                            </h6>
                        </div>
                        <div class="col-4">
                            <img src="{{URL::asset($frequency['image'])}}" class="img-fluid pb-3" />
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="row d-block d-sm-none m-auto pl-0 pr-0" id="mobile-button">
            <div class="col-12 text-center pl-0 pr-0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                <button class="button-annual-pop-out button-proceed-checkout MuliExtraBold" type="button">@lang('website_contents.selection.review_order')</button>
                <form id="form-checkout" method="GET" action="{{ route('locale.region.shave-plans.custom-plan.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                    <button id="proceed-checkout" class="d-none button-proceed-checkout-mobile MuliExtraBold" type="submit">@lang('website_contents.selection.review_order')</button>
                </form>
            </div>
        </div>
    </div>
</div>

<input class="d-none" id="change-annual-value" name="change-annual-value" type="hidden"  value="0">
<div class="modal fade" id="change-annual-modal" tabindex="-1" role="dialog" aria-labelledby="change-annual-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-changeannual-cycle" role="document">
            <div class="modal-content">
            <div class="modal-header" style="border: none ;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                <div class="modal-body pl-5 pr-5" style="padding-top: 0 !important;">
                    <div class="col-12 text-center">
                        <h4 class="MuliExtraBold pt-4">@lang('website_contents.selection.change-to-annual-model.title')</h4>
                        <h6 class="MuliPlain pt-2 pb-4">@lang('website_contents.selection.change-to-annual-model.sub-title')</h6>
         
                    <!-- <label class="form-check-label">
                    <input class="form-check-input" id="change-annual-checkbox" name="change-annual-checkbox" type="checkbox" onclick="changeAnnualCheckbox()">
                    Change
                    </label> -->
                    </div>
                </div>
                <div class="modal-footer-btn col-12" style="padding-top:2%; padding-bottom:2%;">
                <button onclick="submitNoAnnualModal();" class="button-proceed-checkout-cancel MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 30%; margin-right:2%;">@lang('website_contents.selection.change-to-annual-model.cancel')</button>
                <button id="button-proceed-checkout" onclick="submitAnnualModal();" class="button-proceed-checkout MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 30%; margin-left:2%;">@lang('website_contents.selection.change-to-annual-model.submit')</button>
                </div>
            </div>
        </div>
</div>
<!-- Modal: Notification Select Cycle -->
<div class="modal fade" id="addon-cycle-modal" tabindex="-1" role="dialog" aria-labelledby="addon-cycle-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-dialog-addon-cycle" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-addon-cycle">
                <button type="button" onclick="addonCycleClose();" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12 text-center pb-3">
                    <h3><b>@lang('website_contents.custom_plan.selection.how_deliver')</b></h3>
                </div>
                <div class="col-12 margB5rem">
                    <div class="row justify-content-md-center">
                        <div class="col-md-6 addon-cycle-product">
                            <div class="col-12">
                                <input type="hidden" id="addonid-hidden" name="addonid" value="">
                                <input type="hidden" id="addontype-hidden" name="addontype" value="">
                                <img id="img-addon-cycle-modal" src="" class="img-fluid product-sleeve-bag-img" />
                                <h5><b id="name-addon-cycle-modal">{{$product['producttranslatesname']}}</b></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer-btn col-12 pb-5">
                    <button type="button" onclick="addonCycleEvery();" data-dismiss="modal" aria-label="Close" class="btn btn-every-shipping"><b>@lang('website_contents.custom_plan.selection.add_all_shipments')</b></button>
                    <button type="button" onclick="addonCycleOne();" data-dismiss="modal" aria-label="Close" class="btn btn-one-shipping"><b>@lang('website_contents.custom_plan.selection.add_this_shipment')</b></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // Get all product
    let handle_products = {!! json_encode($handle_products) !!};
    let bag_products = {!! json_encode($bag_products) !!};
    let blade_products = {!! json_encode($blade_products) !!};
    let frequency_list = {!! json_encode($frequency_list) !!};
    let addon_products = {!! json_encode($addon_products) !!};
    let allplan = {!! json_encode($allplan) !!};
    let country_id = {!! json_encode($country_id) !!};
    let langCode = {!! json_encode($langCode) !!};
    let shippingfeeget = {!! json_encode($shippingfee) !!}
    let shippingfee= parseFloat(shippingfeeget);
    let plantypeconfig = "<?php echo config('global.' . config("app.appType") . '.CustomPlan.CustomPlanType') ?>";
    let plangroupconfig = "<?php echo config('global.' . config("app.appType") . '.CustomPlan.CustomPlanGroup') ?>";
    let imageassetspath = "<?php echo URL::asset('/images/custom/') ?>";
    let country_handles = {!!json_encode($m_h_sku) !!};
    // let handle_products = {!! json_encode($handle_products) !!};
    // let bag_products = {!! json_encode($bag_products) !!};
    // let blade_products = {!! json_encode($blade_products) !!};
    // let frequency_list = {!! json_encode($frequency_list) !!};
    // let addon_products = {!! json_encode($addon_products) !!};
    // let allplan = {!! json_encode($allplan) !!};
    // let country_id = {!! json_encode($country_id) !!};
    // let langCode = {!! json_encode($langCode) !!};
    // let shippingfeeget = {!! json_encode($shippingfee) !!}
    // let shippingfee= parseFloat(shippingfeeget);
</script>
<script src="{{ asset('js/functions/custom-plan/custom-plan-selection.function.js') }}"></script>
@endsection
