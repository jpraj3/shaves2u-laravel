<?php
    // Get Data from session
    $sessionCountryData = session()->get('currentCountry');

    // If Data from session exist
    if (!empty($sessionCountryData)) {
        // Set Current country info based on session Data
        $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
    }
    // If Data from Session Storage does not exist
    else {
        // Redirect back to / where session is set
        redirect()->route('locale.index');
    }
    $currentLocale = strtolower(app()->getLocale());
?>

@extends('payment_layout.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/custom-plan/custom-plan-selection.css') }}">

<div class="container">
    <div class="row px-3">
        <div class="col-3 pr-0 pl-0 d-flex align-items-center">
            <button id="button-back" class="button-back">
            < BACK</button>
        </div>
    </div>
        <div class="checkout-content-desktop col-12 d-none d-lg-block pr-5 pb-100">
            <div class="row">
                <div class="col-7 pt-2 pb-4">
                    <div class="col-12 rounded-10 bg-white shadow-sm padd15 mb-4">
                        <div class="row ml-0 mr-0">
                            <h4 class="MuliExtraBold">Create Account</h4>
                        </div>
                        <div class="col-lg-12 padd20 pl-0 pr-0">
                            <div class="form-group">
                                <label>Name</label>
                                <div class="input-group">
                                    <input id="email" type="text" class="fs-20 form-control @error('name') is-invalid @enderror" placeholder="John Doe"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <div class="input-group">
                                    <input id="email" type="text" class="fs-20 form-control @error('name') is-invalid @enderror" placeholder="johndoe@gmail.com"/>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="">{{ __('Password') }}</label>

                                <div class="input-group">
                                    <input id="password" type="password" class="fs-20 form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label for="date-of-birth" class="col-12 col-form-label text-left">{{ __('Date of Birth') }}</label>
                                    <div class="col-4">
                                        <select id="day-of-birth"  class="form-control custom-select @error('name') is-invalid @enderror" name="day_of_birth" required style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                            <option>25</option>
                                            <option>26</option>
                                            <option>27</option>
                                            <option>28</option>
                                            <option>29</option>
                                            <option>30</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <select id="month-of-birth" class="form-control custom-select" name="month_of_birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                            <option selected disabled hidden>MM</option>
                                            <option>Jan</option>
                                            <option>Feb</option>
                                            <option>Mar</option>
                                            <option>Apr</option>
                                            <option>May</option>
                                            <option>Jun</option>
                                            <option>Jul</option>
                                            <option>Aug</option>
                                            <option>Sep</option>
                                            <option>Oct</option>
                                            <option>Nov</option>
                                            <option>Dec</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <select id="year-of-birth" class="form-control custom-select" name="year_of_birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                            <option selected disabled hidden>YY</option>
                                            <option>1985</option>
                                            <option>1986</option>
                                            <option>1987</option>
                                            <option>1988</option>
                                            <option>1989</option>
                                            <option>1990</option>
                                            <option>1991</option>
                                            <option>1992</option>
                                            <option>1993</option>
                                            <option>1994</option>
                                            <option>1995</option>
                                            <option>1996</option>
                                            <option>1997</option>
                                            <option>1998</option>
                                            <option>1999</option>
                                            <option>2000</option>
                                            <option>2001</option>
                                            <option>2002</option>
                                            <option>2003</option>
                                            <option>2004</option>
                                            <option>2005</option>
                                            <option>2006</option>
                                            <option>2007</option>
                                            <option>2008</option>
                                            <option>2009</option>
                                            <option>2010</option>
                                            <option>2011</option>
                                            <option>2012</option>
                                            <option>2013</option>
                                            <option>2014</option>
                                            <option>2015</option>
                                            <option>2016</option>
                                            <option>2017</option>
                                            <option>2018</option>
                                            <option>2019</option>
                                        </select>
                                    </div>
                                    @error('date-of-birth')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12 pb-2 pl-0 pr-0">
                                <button class="btn btn-load-more-orange w-100"><b>Create Account</b></button>
                            </div>
                            <div class="col-lg-12 pb-2 pl-0 pr-0 pt-2">
                                <button class="btn btn-load-more-fb w-100"><img src="{{asset('/images/common/checkoutAssets/fb_icon.png')}}"/> <b>Sign in with Facebook</b></button>
                            </div>
                            <div class="col-lg-12 pl-0 pr-0 pt-2">
                                <h6>Already have an account? <u>Sign in</u></h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 rounded-10 bg-white shadow-sm padd15 mb-4">
                        <div class="row ml-0 mr-0">
                            <h4 class="MuliExtraBold">Shipping Address</h4>
                        </div>
                        <p class="fs-20 MuliExtraBold text-orng">Address</p>
                        <p>110C, Jalan SS21/39, Damansara Uptown, 47400 Petaling Jaya, Selangor</p>
                        <div class="col-lg-10 pb-2 pl-0 pr-0">
                            <button class="btn btn-load-more"><b>EDIT ADDRESS</b></button>
                        </div>
                    </div>
                    <div class="col-12 rounded-10 bg-white shadow-sm padd15">
                        <h4 class="MuliExtraBold">CARD DETAILS</h4>
                        <div class="row mr-0 ml-0">
                            <div class="col-6 pl-0">
                                <div class="col-lg-12 pl-0 pr-0 bg-card" style="background-image: url({{asset('/images/cards/visa_card.png'')}});">""
                                    <div class="card-index ml-auto mr-4 text-center fs-12 mb-5">
                                        1
                                    </div>
                                    <div class="card-number text-center text-white mt-2 mb-5">
                                        <span class="cc_last4digits">1234</span>
                                    </div>
                                    <div class="card-expiry mr-4 text-right text-white fs-12">
                                        <p class="mb-2">09 / 20</p>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center ">
                                    <button class="btn btn-load-more w-100"><b>Edit Card</b></button>
                                </div>
                            </div>
                            <div class="col-6 pl-0 pr-0">
                                <div class="row mr-0 ml-0">
                                    <div class="col-12 padd20 pl-0 pr-0">
                                        <div class="form-group">
                                            <label>Card Number</label>
                                            <input type="text" class="fs-20 form-control" placeholder=""/>
                                        </div>
                                    </div>
                                    <div class="col-6 padd20 pl-0">
                                        <div class="form-group">
                                            <label>Expiry Date (MM/YY)</label>
                                            <input type="text" class="fs-20 form-control" placeholder="12 / 24"/>
                                        </div>
                                    </div>
                                    <div class="col-6 padd20 pr-0">
                                        <div class="form-group">
                                            <label>CVC</label>
                                            <input type="text" class="fs-20 form-control" placeholder="xxx"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pl-0 pr-0">
                                <p>By making a purchase, you accept the <u>Terms of Service</u> (update effective as of 11/17/17). Your subscription will automatically renew and your credit card will be charged the subscription fee. You can cancel or modify your plan at any time from your profile page.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 pt-4 pl-0 pr-0 text-left">
                        <form id="form-payment" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.thankyou', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                            <!-- {{ csrf_field() }} -->
                            <button id="pay_now" class="btn btn-start MuliExtraBold d-inline-block" type="submit">@lang('website_contents.checkout.content.pay_now')</button>
                        </form>
                    </div>
                </div>
                <div class="col-5 pt-2 pb-4 pl-5">
                    <div class="col-12 rounded-t-10 bg-white shadow-sm padd15">
                        <div class="row mr-0 ml-0">
                            <h4 class="MuliExtraBold">Summary</h4>
                            <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                                <div class="row">
                                    <div class="col-5 padd0 d-flex align-items-center">
                                        <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}"/>
                                    </div>
                                    <div class="col-7 padd0 d-flex align-items-center">
                                        <div class="col-12 padd0">
                                            <h3 class="MuliBold">Trial Kit</h3>
                                            <p class="fs-18 Muli">5 Blade Cartridge</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                <div class="row mr-0 ml-0">
                                    <div class="col-12 padd0">
                                        <label class="col-12 padd0">Subtotal <span class="pull-right" id="c-subtotal"></span></label>
                                        <label class="col-12 padd0">Shipping <span class="pull-right" id="c-shipping"></span></label>
                                        <label class="col-12 padd0">Discount <span class="pull-right" id="c-discount"></span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 padd20 pl-0 pr-0">
                                <div class="row mr-0 ml-0">
                                    <div class="col-7 padd0">
                                        <p class="fs-18">Promo Code</p>
                                        <div class="form-group">
                                            <label>Enter promo code</label>
                                            <input id="promo_code" type="text" class="fs-20 form-control" placeholder="123456"/>
                                        </div>
                                    </div>
                                    <div class="col-5 pl-3">
                                        <button class="btn btn-load-more w-100 pAbsolute b-8px"><b>APPLY</b></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 bg-dark shadow-sm padd15 text-white">
                        <div class="row mr-0 ml-0">
                            <div class="col-6 padd0">
                                <p class="MuliExtraBold fs-20 mb-0">TODAY TOTAL</p>
                            </div>
                            <div class="col-6 padd0 text-right">
                                <p class="MuliExtraBold fs-20 mb-0">RM <span>6.50</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 padd0 d-lg-none">
            <div class="col text-center pt-lg-5 pl-0 pr-0">
                <button id="button-next" class="button-proceed-checkout MuliExtraBold" type="submit">NEXT</button>
            </div>
            <form id="form-checkout" class="d-none" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.thankyou', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                <!-- {{ csrf_field() }} -->
                <div class="col text-center padd0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                    <button id="proceed-pay" class="button-proceed-checkout" type="submit">PROCEED TO CHECKOUT</button>
                </div>
            </form>
            <br>
            <button class="button-proceed-checkout d-none" id="check">CHECK</button>
            <button class="button-proceed-checkout d-none" id="clear">CLEAR SESSION</button>
        </div>
    </div>
</div>


<!-- Referrals -->
<script src="{{ asset('js/functions/rebates/referral.js') }}"></script>
<script>
function StartTrialPlanSelection() {
    location.href =
        "{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>$currentLocale, 'countryCode'=>$currentCountryIso]) }}";
}
</script>
@endsection
