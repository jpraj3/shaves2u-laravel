@extends('layouts.app')

@section('content')
<div class="container">
    <button onclick="StartFreeTrialSelection()">Start</button>
</div>
<script>
    function StartFreeTrialSelection() {
      
        location.href = "{{ route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso]) }}";
    }
</script>
@endsection