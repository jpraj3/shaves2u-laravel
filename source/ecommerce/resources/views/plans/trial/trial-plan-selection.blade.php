@extends('layouts.app')
@section('content')
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">
<style>
   #footer {
        position: fixed !important;
    }
</style>

<div style="background: linear-gradient(to bottom, white 40%, #ECECEC 60%);height: 100% !important;">
    <div class="container">

        <!-- Section: BACK BUTTON -->
        <div class="row">
            <div class="col-md-12 d-none d-lg-block pr-0 pl-0">
                <button id="button-back" class="button-back hidden">
                    < @lang('website_contents.global.content.back') </button>
            </div>
        </div>

        <!-- Section: GET STARTED -->
        <div id="collapse1" class="row panel-collapse collapse show">

            <!-- Desktop Step 1 View Start -->
            <div class="d-none d-lg-block">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 text-center pb-2">
                        <h2 class="mt-5 pl-1 pr-1 MuliExtraBold w-100">
                            @lang('website_contents.trial_plan.selection.step1.best_deal')
                        </h2>
                    </div>

                    <div class="col-md-6 pr-0 pl-0 text-center">
                        <img src="" class="package-image w-60" />
                    </div>

                    <div class="col-md-6 pr-0 pl-0">
                        <div class="col-md-12 pt-2">
                            <div class="row ">
                                <div class="col-md-12 color-orange">
                                     <!-- Currency and price for kr -->
                                <h2 class="MuliBold"><b><span class="MuliBold" style="color: black">@lang('website_contents.trial_plan.selection.step1.starter_kit') | </span><span class="color-grey MuliBold"><strike>@if(strtolower($currentCountryIso) == "kr"){{ $trial_saving_price }}{{$currency}}@else{{$currency}}{{ $trial_saving_price }}@endif</strike></span> @if(strtolower($currentCountryIso) == "kr"){{$trial_price}}{{$currency}}@else{{$currency}}{{$trial_price}}@endif</b></h2>
                                </div>
                            </div>
                            <div class="col-md-12 pt-3 pr-0 pl-0">
                                <h4 class="px-1"><b>@lang('website_contents.trial_plan.selection.step1.included_kit')</b></h4>
                                @if(strtolower($currentCountryIso) === "kr")
                                    <span>
                                        @if($m_h_sku == 'H3')
                                        <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                        @elseif($m_h_sku == 'H1')
                                        <h5>Premium Handle</h5>
                                        @else
                                        <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                        @endif
                                    </span>

                                    <span>
                                        <h5>@lang('website_contents.trial_plan.selection.step1.blade_cartridge')</h5>
                                    </span>
                                    <span>
                                        <h5>@lang('website_contents.trial_plan.selection.step1.shave_cream')</h5>
                                    </span>
                                @else
                                <ul class="pr-0 pl-3">
                                    <li>
                                        @if($m_h_sku == 'H3')
                                        <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                        @elseif($m_h_sku == 'H1')
                                        <h5>Premium Handle</h5>
                                        @else
                                        <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                        @endif
                                    </li>

                                    <li>
                                        <h5>@lang('website_contents.trial_plan.selection.step1.blade_cartridge')</h5>
                                    </li>
                                    <li>
                                        <h5>@lang('website_contents.trial_plan.selection.step1.shave_cream')</h5>
                                    </li>
                                    <li>
                                        <h5>@lang('website_contents.trial_plan.selection.step1.free_shipping')</h5>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <hr>
                        </div>

                        <div class="col-md-12 pt5 pr-0">
                            <p class="fs-25 MuliBold mb-0"><b>@lang('website_contents.trial_plan.selection.step1.select_blade')</b></p>
                            <div class="row pt-2">
                                @foreach($blade_products as $blade)
                                <div class="blade-{{$blade['productcountriesid']}} col-xs-4 pr-2 pl-2 ml-3 item-unselected" style="cursor: pointer;">
                                    <img src="{{URL::asset($blade['smallBladeImageUrl'])}}" class="w-100" />
                                    <h6 class="text-center"><b>{{$blade['producttranslatesname']}}</b></h6>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-12 text-center pt-3">
                        <button id="get_started-next" class="button-next button-proceed-checkout MuliExtraBold text-uppercase" style="width: 30%">@lang('website_contents.global.content.start_now')</button>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <div class="row">
                            <span class="w-100">
                        <h5><img src="{{URL::asset('/images/common/checkoutAssets/thumbsup.png')}}" class="img-fluid pr-2" /> <b> @lang('website_contents.trial_plan.selection.step1.quality_guaranteed') </b></h5>
                        </span>
                        </div>
                    </div>

                    <div class="col-12 row">
                        <div class="col-md-12 text-center pr-0 pl-0 pt-5">
                            <h1 class="mt-5 MuliExtraBold trial-kit-title">
                                @lang('website_contents.trial_plan.selection.step1.how_works')
                            </h1>
                        </div>
                        <div class="col-md-12 text-center pr-0 pl-0 pb-5" style="color: #828282;">
                            <h4 class="mt-4 MuliPlain">
                                @lang('website_contents.trial_plan.selection.step1.unique_experience')@lang('website_contents.trial_plan.selection.step1.get_started')
                            </h4>
                        </div>
                    </div>

                    <div class="row pr-0 pl-0 row-center">
                        <div class="col-md-4 text-center pr-0 pl-0">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3261.png')}}" class="img-fluid" />
                            <h5 class="MuliPlain color-orange pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step1')
                            </h5>
                            <h2 class="MuliSemiBold pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step1_title')
                            </h2>
                            <h5 class="MuliPlain" style="color: #828282;">
                                @lang('website_contents.trial_plan.selection.step1.step1_desc')
                            </h5>
                        </div>
                        <div class="col-md-4 text-center pr-0 pl-0">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3263.png')}}" class="img-fluid" />
                            <h5 class="MuliPlain color-orange pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step2')
                            </h5>
                            <h2 class="MuliSemiBold pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step2_title')
                            </h2>
                            <h5 class="MuliPlain" style="color: #828282;">
                                @lang('website_contents.trial_plan.selection.step1.step2_desc')
                            </h5>
                        </div>
                        <div class="col-md-4 text-center pr-0 pl-0">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3262.png')}}" class="img-fluid" />
                            <h5 class="MuliPlain color-orange pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step3')
                            </h5>
                            <h2 class="MuliSemiBold pb-3">
                                @lang('website_contents.trial_plan.selection.step1.step3_title')
                            </h2>
                            <h5 class="MuliPlain" style="color: #828282;">
                                @lang('website_contents.trial_plan.selection.step1.step3_desc')
                            </h5>
                        </div>
                    </div>
                    @if($m_h_sku == 'H3')
                    <div class="row pr-0 pl-0 pt-150">

                        <div class="col-lg-4 text-center pr-0 pl-0">
                            <h2 class="MuliExtraBold pt-100 w-500px">
                                @lang('website_contents.trial_plan.selection.step1.handle_title')
                            </h2>
                            <p class="w-500px" style="font-family: Muli;font-size:18px;color:#828282;">
                                @lang('website_contents.trial_plan.selection.step1.handle_desc')
                            </p>
                        </div>

                        <div id="swivel-img2" class="col-lg-8 pr-0 pl-5">

                        </div>
                    </div>
                    @elseif($m_h_sku == 'H1')
                        @endif
                    <div class="row pr-0 pl-0 pt-150">
                        <div class="col-lg-6 pr-0 pl-0">
                            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-blades.png')}}" class="w-100" />
                        </div>
                        <div class="col-lg-6 text-center pr-0 pl-0">
                            <h2 class="MuliExtraBold pt-100">
                                @lang('website_contents.trial_plan.selection.step1.blades_title')
                            </h2>
                            <p style="font-family: Muli;font-size:18px;color:#828282;">
                                @lang('website_contents.trial_plan.selection.step1.blades_desc')
                            </p>
                        </div>
                    </div>
                    <div class="row pr-0 pl-0 pt-5 pb-5">
                        <div class="col-lg-6 text-center pr-0 pl-0">
                            <h2 class="MuliExtraBold pt-100">
                                @lang('website_contents.trial_plan.selection.step1.cream_title')
                            </h2>
                            <p style="font-family: Muli;font-size:18px;color:#828282;">
                                    @lang('website_contents.trial_plan.selection.step1.cream_desc')
                            </p>
                        </div>
                        <div class="col-lg-6 pt-5 pr-0 pl-0 d-none d-lg-block pb-5">
                            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-shavecream2.png')}}" class="w-100" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- Desktop Step 1 View End -->

            <!-- Mobile Step 1 View Start -->
            <div class="d-block d-lg-none">
                <div class="col-12 col-sm-12 col-md-12 text-center pb-2">
                    <h2 class="mt-5 pl-1 pr-1 MuliExtraBold w-100">
                        @lang('website_contents.trial_plan.selection.step1.best_deal')
                    </h2>
                </div>
                <div class="text-center m-auto pt-1">
                    <div class="col-12 color-orange">
                         <!-- Currency and price for kr -->
                        <p class="fs-22 MuliBold"><b>@lang('website_contents.trial_plan.selection.step1.starter_kit') | <span class="color-grey fs-22 MuliBold"><strike>@if(strtolower($currentCountryIso) == "kr"){{ $trial_saving_price }}{{$currency}}@else{{$currency}}{{ $trial_saving_price }}@endif</strike></span>&nbsp;@if(strtolower($currentCountryIso) == "kr"){{$trial_price}}{{$currency}}@else{{$currency}}{{$trial_price}}@endif</b></p>
                    </div>
                </div>
                <div class="col-md-6 pr-0 pl-0 text-center">
                    <img src="" class="package-image w-60" />
                </div>
                <div class="col-12 pt5 m-auto">
                    <hr>
                    <h4 class="text-center"><b>@lang('website_contents.trial_plan.selection.step1.select_blade')</b></h4>
                    <div class="row pt-2 justify-content-center">
                        @foreach($blade_products as $blade)
                        <div class="blade-{{$blade['productcountriesid']}} col-xs-4 mr-1 item-unselected">
                            <img src="{{URL::asset($blade['smallBladeImageUrl'])}}" class="img-fluid" />
                            <h6 class="text-center"><b>{{$blade['bladeName']}}</b></h6>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="m-auto <?php if(strtolower($currentCountryIso) === "kr"){ echo "text-center";} ?> pt-3">
                    <div class="col-12 col-sm-12 col-md-12 p-4">
                        <h4 class="px-1"><b>@lang('website_contents.trial_plan.selection.step1.included_kit')</b></h4>
                        @if(strtolower($currentCountryIso) === "kr")
                            <span>
                                @if($m_h_sku == 'H3')
                                <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                @elseif($m_h_sku == 'H1')
                                <h5>Premium Handle</h5>
                                @else
                                <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                @endif
                            </span>
                            <span>
                                <h5>@lang('website_contents.trial_plan.selection.step1.blade_cartridge')</h5>
                            </span>
                            <span>
                                <h5>@lang('website_contents.trial_plan.selection.step1.shave_cream')</h5>
                            </span>
                        @else
                        <ul class="pr-0 pl-3">
                            <li>
                                @if($m_h_sku == 'H3')
                                <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                @elseif($m_h_sku == 'H1')
                                <h5>Premium Handle</h5>
                                @else
                                <h5>@lang('website_contents.trial_plan.selection.step1.swivel_handle')</h5>
                                @endif
                            </li>
                            <li>
                                <h5>@lang('website_contents.trial_plan.selection.step1.blade_cartridge')</h5>
                            </li>
                            <li>
                                <h5>@lang('website_contents.trial_plan.selection.step1.shave_cream')</h5>
                            </li>
                            <li>
                                <h5>@lang('website_contents.trial_plan.selection.step1.free_shipping')</h5>
                            </li>
                        </ul>
                        @endif
                    </div>
                    <div class="row pl-4">
                        <span class="w-100">
                        <h5><img src="{{URL::asset('/images/common/checkoutAssets/thumbsup.png')}}" class="img-fluid" /><b> @lang('website_contents.trial_plan.selection.step1.quality_guaranteed') </b></h5>
                    </span>
                    </div>
                </div>

                <div class="col-12 pt-5 pb-5 mb-3">
                    <div class="row">
                        <div class="col-xs-12 text-center m-auto pr-0 pl-0">
                            <h1 class="MuliExtraBold trial-kit-title">
                                @lang('website_contents.trial_plan.selection.step1.how_works')
                            </h1>
                        </div>
                        <div class="col-xs-6 text-center m-auto pr-3 pl-3" style="color: #828282;">
                            <h5 class="MuliPlain d-none d-sm-block">
                                @lang('website_contents.trial_plan.selection.step1.unique_experience') @lang('website_contents.trial_plan.selection.step1.get_started')
                            </h5>
                            <h5 class="MuliPlain d-block d-sm-none">
                                @lang('website_contents.trial_plan.selection.step1.unique_experience_mobile') @lang('website_contents.trial_plan.selection.step1.get_started')
                            </h5>
                        </div>
                    </div>
                    <div class="row pr-0 pl-0 row-center">
                        <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3261.png')}}" class="w-50" />
                            <h5 class="MuliPlain color-orange">
                                @lang('website_contents.trial_plan.selection.step1.step1')
                            </h5>
                            <h2 class="MuliSemiBold">
                                @lang('website_contents.trial_plan.selection.step1.step1_title')
                            </h2>
                            <h5 class="MuliPlain">
                                @lang('website_contents.trial_plan.selection.step1.step1_desc')
                            </h5>
                        </div>
                        <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3263.png')}}" class="w-50" />
                            <h5 class="MuliPlain color-orange">
                                @lang('website_contents.trial_plan.selection.step1.step2')
                            </h5>
                            <h2 class="MuliSemiBold">
                                @lang('website_contents.trial_plan.selection.step1.step2_title')
                            </h2>
                            <h5 class="MuliPlain">
                                @lang('website_contents.trial_plan.selection.step1.step2_desc')
                            </h5>
                        </div>
                        <div class="col-xs-12 text-center pr-0 pl-0 pt-5">
                            <img src="{{URL::asset('/images/common/checkoutAssets/Group3262.png')}}" class="w-50" />
                            <h5 class="MuliPlain color-orange">
                                @lang('website_contents.trial_plan.selection.step1.step3')
                            </h5>
                            <h2 class="MuliSemiBold">
                                @lang('website_contents.trial_plan.selection.step1.step3_title')
                            </h2>
                            <h5 class="MuliPlain">
                                @lang('website_contents.trial_plan.selection.step1.step3_desc')
                            </h5>
                        </div>
                    </div>
                    <div class="row pr-0 pl-0 pt-5">
                            @if($m_h_sku == 'H3')
                            <div id="swivel-img" class="col-xs-12 pr-0 pl-0">

                            </div>


                        <div class="col-xs-12 text-center pr-3 pl-3">
                            <h2 class="MuliExtraBold">
                                @lang('website_contents.trial_plan.selection.step1.handle_title')
                            </h2>
                            <h5 class="MultiPlain">
                                @lang('website_contents.trial_plan.selection.step1.handle_desc')
                            </h5>
                        </div>
                        @elseif($m_h_sku == 'H1')
                            @endif
                        <div class="col-xs-12 pr-0 pl-0 pt-5 mx-auto">
                            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-blades.png')}}" class="w-100" />
                        </div>
                        <div class="col-xs-12 text-center pr-4 pl-4">
                            <h2 class="MuliExtraBold">
                                @lang('website_contents.trial_plan.selection.step1.blades_title')
                            </h2>
                            <h5 class="MultiPlain" style="color: #828282;">
                                @lang('website_contents.trial_plan.selection.step1.blades_desc')
                            </h5>
                        </div>
                        <div class="col-xs-12 text-center pr-3 pl-3 pt-5">
                            <div class="col-xs-12 pr-0 pl-0">
                                <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-shavecream2.png')}}" class="w-100" />
                            </div>
                            <h2 class="MuliExtraBold">
                                @lang('website_contents.trial_plan.selection.step1.cream_title')
                            </h2>
                            <h5 class="MultiPlain" style="color: #828282;">
                                @lang('website_contents.trial_plan.selection.step1.cream_desc')
                            </h5>
                        </div>
                    </div>

                <div class="row d-block d-lg-none" id="mobile-button">
                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold text-uppercase" type="submit">@lang('website_contents.global.content.start_now')</button>
                </div>
            </div>
            <!-- Mobile Step 1 View End -->
        </div>
    </div>

    <!-- Section: ONGOING PRODUCTS -->
    <div id="collapse2" class="row panel-collapse collapse">
        <!-- Desktop Step 2 View Start -->
        <div class="d-none d-lg-block">
            <div style="padding-bottom: 6.45rem">
                <div class="row pb-2">
                    <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                        <h1 class="mt-2 MuliExtraBold">
                            @lang('website_contents.trial_plan.selection.step2.choose_refill')
                    </h1>
                    </div>
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <p class="fs-18 MuliPlain mt-3">
                            @lang('website_contents.trial_plan.selection.step2.refill_ships_date',['date' => $next_refill_date])
                        </p>
                    </div>
                </div>

                <!-- No addons -->
                <div class="row">
                    <div class='col-md-4 d-flex'>
                        <div class="refill-0 text-center pt-4 pb-4 item-unselected pointer">
                            <h5 class="refill-0-title Muliplain">
                                @lang('website_contents.trial_plan.selection.step2.solo_pack_products',['blade_count' => ':blade_count:'])
                            </h5>
                            <h4 class="refill-0-price MuliExtraBold">
                                :price:
                            </h4>
                            <img src="" class="refill-0-image w-30 pt-4" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.trial_plan.selection.step2.solo_pack')
                            </h6>
                        </div>
                    </div>
                    <!-- Plus shave cream -->
                    <div class='col-md-4 d-flex'>
                        <div class="refill-1 text-center pt-4 item-unselected pointer" style="padding-bottom: 40px">
                            <h5 class="refill-1-title Muliplain">
                                @lang('website_contents.trial_plan.selection.step2.duo_pack_products',['blade_count' => ':blade_count:'])
                            </h5>
                            <h4 class="refill-1-price MuliExtraBold ">
                                :price:
                            </h4>
                            <img src="" class="refill-1-image w-34 pt-4" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.trial_plan.selection.step2.duo_pack')
                            </h6>
                        </div>
                    </div>
                    <div class='col-md-4 d-flex'>
                        <!-- Plus after shave cream -->
                        <div class="refill-2 text-center pt-4 item-unselected pointer" style="padding-bottom: 5px">
                            <h5 class="refill-2-title Muliplain">
                                @lang('website_contents.trial_plan.selection.step2.combo_pack_products',['blade_count' => ':blade_count:'])
                            </h5>
                            <h4 class="refill-2-price MuliExtraBold">
                                :price:
                            </h4>
                            <img src="" class="refill-2-image" style="width: 48%" />
                            <h6 class="MuliPlain pt-3">
                                @lang('website_contents.trial_plan.selection.step2.combo_pack')
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center pt-4 mb-5">
                    <button class="button-next button-proceed-checkout MuliExtraBold" style="width: 30%" type="submit">@lang('website_contents.trial_plan.selection.step2.next_step')</button>
                </div>
            </div>
        </div>
        <!-- Desktop Step 2 View End -->

        <!-- Mobile Step 2 View Start -->
        <div class="d-block d-lg-none">
            <div class="row pb-5 pl-3 pr-3">
                <div class="col-md-12 text-center pl-0 pr-0 step1-heading">
                    <h1 class="mt-5 MuliExtraBold trial-kit-title step1-title">
                        @lang('website_contents.trial_plan.selection.step2.choose_refill')
                    </h1>
                </div>
                <div class="col-md-12 text-center pl-0 pr-0">
                    <h4 class="mt-3 MuliPlain">
                        @lang('website_contents.trial_plan.selection.step2.refill_ships_date', ['date' => $next_refill_date])
                    </h4>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class='col-11'>
                    <div class="refill-0 text-left m-2 pl-2 pt-2 item-unselected">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="refill-0-title Muliplain">
                                    @lang('website_contents.trial_plan.selection.step2.solo_pack_products',['blade_count' => ':blade_count:'])
                                </h5>
                                <h4 class="refill-0-price MuliExtraBold">
                                    :price:
                                </h4>
                                <h5 class="Muliplain pt-3">
                                    @lang('website_contents.trial_plan.selection.step2.solo_pack')
                                </h5>
                            </div>
                            <div class="col-6">
                                <img src="" class="refill-0-image w-60 pb-10 pl-30" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-11'>
                    <div class="refill-1 m-2 pl-2 pt-2 item-unselected">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="refill-1-title Muliplain">
                                    @lang('website_contents.trial_plan.selection.step2.duo_pack_products',['blade_count' => ':blade_count:'])
                                </h5>
                                <h4 class="refill-1-price MuliExtraBold">
                                    :price:
                                </h4>
                                <h4 class="mt-3 MuliPlain">
                                    @lang('website_contents.trial_plan.selection.step2.duo_pack')
                                </h4>
                            </div>
                            <div class="col-6">
                                <img src="" class="refill-1-image w-60 ml-4" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-11' style="padding-bottom:20vw">
                    <div class="refill-2 m-2 pl-2 pt-2 item-unselected">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="refill-2-title Muliplain">
                                    @lang('website_contents.trial_plan.selection.step2.combo_pack_products',['blade_count' => ':blade_count:'])
                                </h5>
                                <h4 class="refill-2-price MuliExtraBold">
                                    :price:
                                </h4>
                                <h5 class="Muliplain pt-3">
                                    @lang('website_contents.trial_plan.selection.step2.combo_pack')
                                </h5>
                            </div>
                            <div class="col-6">
                                <img src="" class="refill-2-image w-100 pr-3 pb-2" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-block d-lg-none m-auto">
                <div id="mobile-button" class="col-12 text-center pt-5 pl-0 pr-0">
                    <button class="button-next button-proceed-checkout-mobile MuliExtraBold" type="submit">@lang('website_contents.trial_plan.selection.step2.next_step')</button>
                </div>
            </div>
        </div>
        <!-- Mobile Step 2 View End -->
    </div>

    <!-- Section: SHIPPING FREQUENCY -->
    <div id="collapse3" class="row panel-collapse collapse">
        <!-- Desktop Step 3 View Start -->
        <div class="d-none d-lg-block w-100">
            <div class="row row-center">
                <div class="row pb-5">
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h1 class="mt-2 MuliExtraBold">
                            @lang('website_contents.trial_plan.selection.step3.shave_frequency')
                        </h1>
                    </div>
                    <div class="col-md-12 text-center pl-1 pr-1">
                        <h5 class="mt-3 MuliPlain pl-3 pr-3">
                            @lang('website_contents.trial_plan.selection.step3.change_frequency')
                        </h5>
                    </div>
                </div>

                <div class="row justify-content-center">
                    @foreach($frequency_list as $frequency)
                    <div class="col-md-3 mb-3">
                        <div class="frequency-{{$frequency['duration']}} text-center pt-4 item-unselected pointer" style="height: 350px">
                            <img src="{{URL::asset($frequency['image'])}}" class="img-fluid" />
                            <h4 class="MuliExtraBold pt-4">
                                {!! $frequency['durationText'] !!}
                            </h4>
                            <h6 class="frequency-{{$frequency['duration']}}-details MuliPlain pt-4 pb-3 frequency-p">
                                {!! $frequency['detailsText'] !!}<br><br>
                            </h6>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="col px-0 text-center pt-4 pb-5 mb-4">
                <!-- <button class=" button-annual-pop-out button-proceed-checkout MuliExtraBold text-uppercase" type="button" style="width: 30%">@lang('website_contents.trial_plan.selection.step3.review_order')</button> -->
                    <form id="form-proceed-checkout" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                        <button id="button-proceed-checkout" class="button-proceed-checkout MuliExtraBold text-uppercase" type="button" style="width: 30%">@lang('website_contents.trial_plan.selection.step3.review_order')</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Desktop Step 3 View End -->

        <!-- Mobile Step 3 View Start -->
        <div class="d-block d-lg-none">
            <div class="row row-center">
                <div class="row pb-3 pl-3 pr-3">
                    <div class="col-md-12 text-center pl-0 pr-0">
                        <h1 class="mt-2 MuliExtraBold">
                            @lang('website_contents.trial_plan.selection.step3.shave_frequency')
                        </h1>
                    </div>
                    <div class="col-md-12 text-center pl-1 pr-1">
                        <h5 class="mt-3 MuliPlain pl-3 pr-3">
                            @lang('website_contents.trial_plan.selection.step3.change_frequency')
                        </h5>
                    </div>
                </div>

                <div class="col-md-12 d-block d-lg-none pt-3" style="padding-bottom: 20vw;">
                    @foreach($frequency_list as $frequency)
                    <div class="col-12 mb-3">
                        <div class="row frequency-{{$frequency['duration']}} frequency-box-m text-center pt-4 item-unselected" >
                            <div class="col-8" style="top:-10px">
                                <h4 class="MuliExtraBold pt-4">
                                    {!! $frequency['durationText'] !!}
                                </h4>
                                <h6 class="frequency-{{$frequency['duration']}}-details MuliPlain pt-4 frequency-p">
                                    {!! $frequency['detailsText'] !!}<br><br>
                                </h6>
                            </div>
                            <div class="col-4">
                                <img src="{{URL::asset($frequency['image'])}}" class="img-fluid pb-3" />
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div id="mobile-button" class="col-12 pl-0 pr-0">
                        <!-- <button class="button-annual-pop-out2 button-next button-proceed-checkout-mobile MuliExtraBold text-center text-uppercase" type="submit">@lang('website_contents.trial_plan.selection.step3.review_order')</button> -->
                            <form id="form-proceed-checkout" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                                <button id="button-proceed-checkout2" class="button-next button-proceed-checkout-mobile MuliExtraBold text-center text-uppercase" type="submit">@lang('website_contents.trial_plan.selection.step3.review_order')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile Step 3 View End -->
    </div>
    <input class="d-none" id="change-annual-value" name="change-annual-value" type="hidden"  value="0">
    <div class="modal fade" id="change-annual-modal" tabindex="-1" role="dialog" aria-labelledby="change-annual-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="col-12 text-center">
                    <label class="form-check-label">
                    <input class="form-check-input" id="change-annual-checkbox" name="change-annual-checkbox" type="checkbox" onclick="changeAnnualCheckbox()">
                    Change
                    </label>
                    </div>
                </div>
                <div class="modal-footer-btn col-12 pb-5">
                <button id="button-proceed-checkout" onclick="submitAnnualModal();" class="button-proceed-checkout MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 30%">@lang('website_contents.trial_plan.selection.step3.review_order')</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal: Notification trial plan exist -->
    <div class="modal fade" id="notification_trial_plan_exist" tabindex="-1" role="dialog" aria-labelledby="notification_trial_plan_exist" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-trial-plan-exist">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5" style="padding-top: 0 !important">
                    <div class="col-12 text-center">
                    <h5 class="modal-trial-plan-text">@lang('website_contents.checkout.content.already_signed_up', ['url' => route('locale.region.authenticated.savedplans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])])</h5>
                    <br>
                    <h5 class="modal-trial-plan-text">@lang('website_contents.checkout.content.restart_plan', ['url' => route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso])])</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let handle_products = {!!json_encode($handle_products) !!};
    let plan_type_based_on_handle = {!!json_encode($plan_type_based_on_handle) !!};
    let handle_based_on_country = {!!json_encode($handle_based_on_country) !!};
    let blade_products = {!!json_encode($blade_products) !!};
    let frequency_list = {!!json_encode($frequency_list) !!};
    let addon_products = {!!json_encode($addon_products) !!};
    let free_shave_cream_id = {!!json_encode($free_shave_cream_id) !!};
    let allplan = {!!json_encode($allplan) !!};
    let country_id = {!!json_encode($country_id) !!};
    let currency = {!!json_encode($currency) !!};
    let trial_price = {!!json_encode($trial_price) !!};
    let trial_saving_price = {!!json_encode($trial_saving_price) !!};
    let next_refill_date = {!!json_encode($next_refill_date) !!};
    let user_id = {!!json_encode($user_id) !!};
    let country_handles = {!!json_encode($m_h_sku) !!};
</script>

<script>
    var currentCountry = window.localStorage.getItem('currentCountry');
    if (country_handles == "H3") {
        if (JSON.parse(currentCountry).codeIso === "MY" && JSON.parse(currentCountry).defaultLang === "EN" ||
            JSON.parse(currentCountry).codeIso === "SG" && JSON.parse(currentCountry).defaultLang === "EN" ){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
        } else if( JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "EN"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        } else if(JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "ZH-HK" || JSON.parse(currentCountry).codeIso === "TW" && JSON.parse(currentCountry).defaultLang === "ZH-TW"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        } else if(JSON.parse(currentCountry).codeIso === "KR" && JSON.parse(currentCountry).defaultLang === "KO"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
        }
    } else if (country_handles == "H1") {
        // if (JSON.parse(currentCountry).codeIso === "MY" && JSON.parse(currentCountry).defaultLang === "EN" ||
        // JSON.parse(currentCountry).codeIso === "SG" && JSON.parse(currentCountry).defaultLang === "EN" ){
        //     $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
        //     $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
        // } else if( JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "EN"){
        //     $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        //     $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        // } else if(JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "ZH-HK" || JSON.parse(currentCountry).codeIso === "TW" && JSON.parse(currentCountry).defaultLang === "ZH-TW"){
        //     $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        //     $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        // } else if(JSON.parse(currentCountry).codeIso === "KR" && JSON.parse(currentCountry).defaultLang === "KO"){
        //     $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
        //     $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
        // }
    } else {
        if (JSON.parse(currentCountry).codeIso === "MY" && JSON.parse(currentCountry).defaultLang === "EN" ||
        JSON.parse(currentCountry).codeIso === "SG" && JSON.parse(currentCountry).defaultLang === "EN" ){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}' class='w-100' />");
        } else if( JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "EN"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        } else if(JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "ZH-HK" || JSON.parse(currentCountry).codeIso === "TW" && JSON.parse(currentCountry).defaultLang === "ZH-TW"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_CHI.png')}}' class='w-100' />");
        } else if(JSON.parse(currentCountry).codeIso === "KR" && JSON.parse(currentCountry).defaultLang === "KO"){
            $('#swivel-img').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
            $('#swivel-img2').html("<img src='{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel_KR.png')}}' class='w-100' />");
        }
    }
</script>
<script src="{{ asset('js/functions/trial-plan/trial-plan-selection.function.js') }}"></script>
<script src="{{ asset('js/functions/rebates/referral.js') }}"></script>
@endsection
