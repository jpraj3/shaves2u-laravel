<?php
    // Get Data from session
    $sessionCountryData = session()->get('currentCountry');

    // If Data from session exist
    if (!empty($sessionCountryData)) {
        // Set Current country info based on session Data
        $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
    }
    // If Data from Session Storage does not exist
    else {
        // Redirect back to / where session is set
        redirect()->route('locale.index');
    }
    $currentLocale = strtolower(app()->getLocale());
?>

@extends('starter_layout.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">


<div class="container">
    <div class="row">
        <div class="col-md-12 text-center pr-0 pl-0">
            <h1 class="mt-5 MuliExtraBold trial-kit-title">
                Get started with the Shaves2U Started Kit
            </h1>
            <h5 class="mt-3 mb-4 MuliPlain">
                A better shave, delivered. Start with a shaver and a shave cream for just RM 6.50
            </h5>
        </div>
        <div class="col-md-6 pr-0 pl-0 text-center">
            <img src="{{URL::asset('/images/common/checkoutAssets/Trialkit.jpg')}}" class="w-70"/>
        </div>
        <div class="col-md-6 pr-0 pl-0">
            <div class="col-md-12 pt-5">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Starter Kit | RM 6.50</h2>
                    </div>
                    <div class="col-md-5 pt-1">
                        <h5>Save RM18.00</h5>
                    </div>
                </div>
                <div class="col-md-12 pt-3 pr-0 pl-0">
                    <h4><b>What's included inside the box</b></h4>
                    <ul class="pr-0 pl-3">
                        <li><h5>Handle</h5></li>
                        <li><h5>Blade Cartridge</h5></li>
                        <li><h5>Shave Cream</h5></li>
                        <li><h5>Free Shipping</h5></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="col-md-12 pt5 pr-0">
                <h4><b>Select your blade type</b></h4>
                <div class="row pt-2">
                    <div class="col-xs-4 pr-0 pl-2">
                        <img src="{{URL::asset('/images/common/checkoutAssets/3blades.png')}}" class="img-fluid" />
                        <h6 class="text-center"><b>3 Blades</b></h6>
                    </div>
                    <div class="col-xs-4 pr-0 pl-5">
                        <img src="{{URL::asset('/images/common/checkoutAssets/5blades.png')}}" class="img-fluid" />
                        <h6 class="text-center"><b>5 Blades</b></h6>
                    </div>
                    <div class="col-xs-4 pr-0 pl-5">
                        <img src="{{URL::asset('/images/common/checkoutAssets/6blades.png')}}" class="img-fluid" />
                        <h6 class="text-center"><b>6 Blades</b></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center pl-0 pr-0 pt-5">
        <button class="trial-start-btn MuliExtraBold" onclick="StartTrialPlanSelection()">Next Step</button>
    </div>
    <div class="col-md-12 text-center pl-0 pr-0">
        <div class="row">
            <span class="w-100">
            <h5><img src="{{URL::asset('/images/common/checkoutAssets/thumbsup.png')}}" class="img-fluid" /><b> Quality Guaranteed </b></h5>
            </span>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-md-12 text-center pr-0 pl-0 pt-5">
            <h1 class="mt-5 MuliExtraBold trial-kit-title">
                How it works
            </h1>
        </div>
        <div class="col-md-12 text-center pr-0 pl-0">
            <h4 class="mt-4 MuliPlain">
                Shaves2U brings you a unique shaving experience tailored for your grooming needs.
            </h4>
            <h4 class="MuliPlain">
               Get started in just three simple steps!
            </h4>
        </div>
    </div>
    <div class="row pr-0 pl-0">
        <div class="col-md-4 text-center pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/Group3261.png')}}" class="img-fluid" />
            <h5 class="MuliPlain">
                Step 1
            </h5>
            <h2 class="MuliSemiBold">
                Build your plan
            </h2>
            <h5 class="MuliPlain">
                Choose your frequency of delivery<br>you desire.
            </h5>
        </div>
        <div class="col-md-4 text-center pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/Group3262.png')}}" class="img-fluid" />
            <h5 class="MuliPlain">
                Step 2
            </h5>
            <h2 class="MuliSemiBold">
                Try for 2 weeks
            </h2>
            <h5 class="MuliPlain">
                Start by selecting the ideal blade<br>type of your shaving needs.
            </h5>
        </div>
        <div class="col-md-4 text-center pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/Group3263.png')}}" class="img-fluid" />
            <h5 class="MuliPlain">
                Step 3
            </h5>
            <h2 class="MuliSemiBold">
                Be in Control
            </h2>
            <h5 class="MuliPlain">
                Change or stop your plan at any<br>time from your profile page.
            </h5>
        </div>
    </div>
    <div class="row pr-0 pl-0 pt-150">
        <div class="col-lg-4 text-center pr-0 pl-0">
            <h1 class="MuliExtraBold pt-100 w-500px">
                Crafted for control.
            </h1>
            <h4 class="MultiPlain lh-40px w-500px">
                Our Swivel Handle is designed with a comfortable<br>rubberised grip and weighted metal body for<br>optimal control,topped with a striking chrome<br>finish.For a shave with substance and style.
            </h4>
        </div>
        <div class="col-lg-8 pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-swivel.png')}}" class="w-150"/>
        </div>
    </div>
    <div class="row pr-0 pl-0 pt-150">
        <div class="col-lg-6 pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-blades.png')}}" class="w-100"/>
        </div>
        <div class="col-lg-6 text-center pr-0 pl-0">
            <h1 class="MuliExtraBold pt-100">
                Precision-cut blades.<br>Moisturising lubrication strip.<br>Open-blade architecture.
            </h1>
            <h4 class="MultiPlain lh-40px">
                Our Blade Cartridges have everything you<br>need for a superior shave: presicion-honed<br>carbon steel blades, a moisturising<br>lubrication strip, and an anti-clog design.
            </h4>
        </div>
    </div>
    <div class="row pr-0 pl-0 pt-150">
        <div class="col-lg-6 text-center pr-0 pl-0">
            <h1 class="MuliExtraBold pt-100">
                Good for your skin.
            </h1>
            <h4 class="MultiPlain lh-40px w-500px">
                Our shave cream cushions and conditions your<br>facial hair for a gentler, smoother shave<br>Enriched with anti-inflammatory ingredients to<br>help reduce post-shave redness.
            </h4>
        </div>
        <div class="col-lg-6  pr-0 pl-0">
            <img src="{{URL::asset('/images/common/checkoutAssets/TK-checkout-shavecream2.png')}}" class="w-130" />
        </div>
    </div>
</div>

<!-- Referrals -->
<script src="{{ asset('js/functions/rebates/referral.js') }}"></script>
<script>
// function StartTrialPlanSelection() {
//     location.href =
//         "{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>$currentLocale, 'countryCode'=>$currentCountryIso]) }}";
// }
</script>
@endsection
