<?php
// Get Data from session
$sessionCountryData = session()->get('currentCountry');

// If Data from session exist
if (!empty($sessionCountryData)) {
    // Set Current country info based on session Data
    $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
}
// If Data from Session Storage does not exist
else {
    // Redirect back to / where session is set
    redirect()->route('locale.index');
}
$currentLocale = strtolower(app()->getLocale());
?>

@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-selection.css') }}">

<div class="container" style="margin-top: 100px; margin-bottom: 200px;">
    <div class="row">
        <div class="col-12 text-center pr-0 pl-0">
            <h1 class="mt-5 MuliExtraBold trial-kit-title">
                Thank You
            </h1>
        </div>
        <div class="col-12 text-center mt-2 mb-2">
            <p class="fs-20 Muli">Your order number is</p>
            <p class="color-orange fs-25 MuliBold">#MY000000226</p>
        </div>

        <div class="col-12 text-center mt-2 mb-2">
            <p class="fs-20 Muli">The estimated delivery date is <b>July 18 - July 25, 2019</b>.</p>
            <p class="fs-20 Muli">Email us at help.my@shaves2u.com with any questions or suggestions.</p>
        </div>

        <div class="col-12 text-center mt-2 mb-2">
            <?php
            $urllang = view()->shared('url');
            $iso = view()->shared('currentCountryIso');
            $url = url("/" . $urllang . "-" . $iso);
            ?>
            <a class="btn btn-start" href="{{ $url }}">BACK TO HOME</a>
        </div>
    </div>
</div>
@endsection