@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/aboutus/aboutus.css') }}">

<section class="career-banner bg-banner" style="background-image: url({{asset('/images/common/Main_banner_career.png')}});">
    <div class="container">
        <div class="row">
            <div class="center-md col-md-12" style="padding: 15% 15px 25% 15px;">
                <h1 class="fs-36-md bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold; color: white;"><strong>Join Our Team<br></strong></h1>
                <p class="fs-24-md" style="margin-bottom: 5%; font-family: Muli; color: white;">Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit.</p>
            </div>
        </div>
    </div>
</section>

<section class="paddTB40 career-lookout" style="background-color: #f5f5f5;">
    <div class="container">
        <div class="row" style="padding: 0 30px;">
            <div class="col-md-12 text-center padd15">
                <h1 class="bold-max d-none d-md-block" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>We're on the lookout for<br></strong></h1>
                <h4 class="d-sm-none" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-SemiBold">We're on the lookout for<br></h4>
            </div>
            <div class="col-md-5 border border-dark rounded padd30 margTB15 bg-white">
                <h4 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Corporate Executive<br></strong></h4>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                <a href="#" style="color: #FE5000; font-family: Muli;">view more</a>
            </div>
            <div class="col-md-5 offset-md-2 ml-auto border border-dark rounded padd30 margTB15 bg-white">
                <h4 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold"><strong>Finance Manager<br></strong></h4>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                <a href="#" style="color: #FE5000; font-family: Muli;">view more</a>
            </div>
            <div class="col-md-5 border border-dark rounded padd30 margTB15 bg-white">
                <h4 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold"><strong>Senior Marketing Exec<br></strong></h4>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                <a href="#" style="color: #FE5000; font-family: Muli;">view more</a>
            </div>
            <div class="col-md-5 offset-md-2 border border-dark rounded padd30 margTB15 bg-white">
                <h4 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold"><strong>Senior Marketing Exec<br></strong></h4>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                <a href="#" style="color: #FE5000; font-family: Muli;">view more</a>
            </div>
        </div>
    </div>
</section>

<section class="career-nurture">
    <div class="col-md-12 text-center padd15 d-sm-none">
        <h3 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold;"><strong>We seek to nurture<br>talents for greater<br>growth and development<br></strong></h3>
    </div>
    <img alt="We seek to nurture talents for greater growth and development" src="{{asset('/images/common/banner_career2.png')}}" style="width: 100%;">
    <div class="col-md-12 text-center padd15 d-none d-md-block">
        <h1 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold;"><strong>We seek to nurture talents<br>for greater growth and development<br></strong></h1>
    </div>
</section>

<section class="paddTB40 career-culture">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 text-center padd15">
                <h1 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>Company Culture<br></strong></h1>
            </div>
            <div class="d-none d-lg-block">
                <div class="row justify-content-around">
                    <div class="col-md-6 col-lg-5 pt-5 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Dynamic Team<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 p-5 margTB10">
                        <img alt="Dynamic Team" class="img-fluid" src="{{asset('/images/common/dynamic_team1.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                    <div class="col-md-6 col-lg-5 p-5 margTB10">
                        <img alt="Trait 2" class="img-fluid" src="{{asset('/images/common/dynamic_team2.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                    <div class="col-md-6 col-lg-5 pt-5 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Trait 2<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 pt-5 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Trait 3<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 p-5 margTB10">
                        <img alt="Trait 3" class="img-fluid" src="{{asset('/images/common/dynamic_team3.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                </div>
            </div>
            <div class="d-lg-none">
                <div id="culture_carousel" class="carousel slide" data-ride="carousel" style="min-height: 550px">
                    <ol class="carousel-indicators">
                        <li data-target="#culture_carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#culture_carousel" data-slide-to="1"></li>
                        <li data-target="#culture_carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="padd30 margTB10">
                                <img alt="Dynamic Team" class="img-fluid d-block mx-auto" src="{{asset('/images/common/dynamic_team1.png')}}" style="width: 60%;">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Dynamic Team<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="padd30 margTB10">
                                <img alt="Trait 2" class="img-fluid d-block mx-auto" src="{{asset('/images/common/dynamic_team2.png')}}" style="width: 60%;">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Trait 2<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="padd30 margTB10">
                                <img alt="Trait 3" class="img-fluid d-block mx-auto" src="{{asset('/images/common/dynamic_team3.png')}}" style="width: 60%;">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Trait 3<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="paddTB40 career-perks">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center padd15">
                <h1 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>Company Perks<br></strong></h1>
            </div>
            <div class="col-lg-4 padd30 text-center margTB10">
                <h3 class="bold-max" style="margin-bottom: 5%; font-family: Muli-ExtraBold; color: grey;"><strong>Perk 1<br></strong></h3>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
            </div>
            <div class="col-lg-4 padd30 text-center margTB10">
                <h3 class="bold-max" style="margin-bottom: 5%; font-family: Muli-ExtraBold; color: grey;"><strong>Perk 2<br></strong></h3>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
            </div>
            <div class="col-lg-4 padd30 text-center margTB10">
                <h3 class="bold-max" style="margin-bottom: 5%; font-family: Muli-ExtraBold; color: grey;"><strong>Perk 3<br></strong></h3>
                <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
            </div>
        </div>
    </div>
</section>

<section class="paddTB40 career-welcome" style="background-color: #46545d">
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h3 class="bold-max d-none d-md-block" style="margin-bottom: 4%; margin-top: 2%; color:white; font-family: Muli-ExtraBold"><strong>Let's welcome you to the family<br></strong></h3>
                    <h3 class="bold-max d-md-none" style="margin-bottom: 10%; margin-top: 2%; font-family: Muli-Bold; color: white;"><strong>Eager to join us?<br>Submit your resume<br></strong></h3>
                </div>
                <div class="col-md-12 text-center pr-0 pl-0">
                    <p class=" d-none d-md-block" style="margin-bottom: 4%; margin-top: 2%; font-size: 15px; color:white; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation<br> ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p class="d-md-none" style="margin-bottom: 10%; margin-top: 2%; font-size: 15px; color:white; font-family: Muli;">Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit.</p>
                    <button class="btn btn-start">GET IN TOUCH</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection