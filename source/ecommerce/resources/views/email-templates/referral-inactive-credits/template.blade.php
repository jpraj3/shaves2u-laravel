@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <img src="@lang('email.content.body.referral-inactive-credits.banner-f')" alt="shaves2u-referral-3" class="img-responsive">
                            @else
                                <img
                                    src="@lang('email.content.body.referral-inactive-credits.introImg-m')"
                                    alt="shaves2u" class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                        <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                            <p>
                                                            @lang('email.content.body.referral-inactive-credits.hello-f',['name' => $moduleData->fullname])<br>
                                                                <br>
                                                                @lang('email.content.body.referral-inactive-credits.block-1-f.note-1-f',['friendname' => $moduleData->reffullname])
                                                                <br>
                                                                <br>@lang('email.content.body.referral-inactive-credits.block-1-f.note-2-f',['friendname' => $moduleData->reffullname,'currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])<br>
                                                                <br>@lang('email.content.body.referral-inactive-credits.block-1-f.note-3-f')</p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        @else
                            <td align="center" valign="top" class="center-column-padding"
                                style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0"
                                       style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-inactive-credits.hello',['name' => $moduleData->fullname])
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-inactive-credits.block-1.note-1',['friendname' => $moduleData->reffullname])
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-inactive-credits.block-1.note-2',['friendname' => $moduleData->reffullname,'currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-inactive-credits.block-1.note-3')
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection

