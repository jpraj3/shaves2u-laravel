@extends('email-templates.base')

@section('content')
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                        <td>
                        @if (strtolower($countrycode) == "sg")
                        <img src="@lang('email.content.body.referral-email-invite.banner-sg-f')" alt="shaves2u-referral-2" class="img-responsive">
                        @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                        <img src="@lang('email.content.body.referral-email-invite.banner-hk-f')" alt="shaves2u-referral-2" class="img-responsive">
                        @else
                        <img src="@lang('email.content.body.referral-email-invite.banner-f')" alt="shaves2u-referral-2" class="img-responsive">
                        @endif   
                        </td>
                        @else
                        <td style="padding:80px 0;margin:0;text-align:center;" class="row-padding">
                            <img src="@lang('email.content.body.referral-email-invite.introImg-m')" alt="shaves2u-referral-2" class="img-responsive">
                        </td>
                        @endif
                    </tr>
                    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                    <tr>
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                        <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:center;">
                                                            <p>@lang('email.content.body.referral-email-invite.intro-f',['firstName' => $moduleData->referrerFName])</p>
                                                            <table width="100%" align="center" style="margin:16px 0 24px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/3fdaaedb-f11d-4076-9cd1-044500376100.jpg" alt="">
                                                                            <br>
                                                                            <h2>@lang('email.content.body.referral-email-invite.pay-less.heading-f')
                                                                            </h2>
                                                                            <p>@lang('email.content.body.referral-email-invite.pay-less.description-f')
                                                                            </p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/0ae7e25a-bf7e-48b0-af3f-1ae85c35110c.jpg" alt="">
                                                                            <br>
                                                                            <h2>@lang('email.content.body.referral-email-invite.high-quality.heading-f')
                                                                            </h2>
                                                                            <p>@lang('email.content.body.referral-email-invite.high-quality.description-f')
                                                                            </p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/60954bc4-efc0-4ef2-bc45-f97dc60efd65.jpg" alt="">
                                                                            <br>
                                                                            <h2>@lang('email.content.body.referral-email-invite.free-shipping.heading-f')
                                                                            </h2>
                                                                            <p>@lang('email.content.body.referral-email-invite.free-shipping.description-f')
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <a href="{{$moduleData->inviteLink}}"><img src="@lang('email.content.body.referral-email-invite.accept-invite-f')" style="padding-top:20px;padding-bottom:20px;"></a>
                                                            <p style="padding-bottom:24px;">@lang('email.content.body.referral-email-invite.referral-email-invite-footer-f',['trialPrice' => $moduleData->tprice])</p>
                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                                                            @if (strtolower($countrycode) == "sg")
                                                                <img src="@lang('email.content.common.referral.referralBox-b-f-sg')" class="img-responsive" alt="">
                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                <img src="@lang('email.content.common.referral.referralBox-b-f-hk')" class="img-responsive" alt="">
                                                                @else
                                                                <img src="@lang('email.content.common.referral.referralBox-b-f')" class="img-responsive" alt="">
                                                                @endif
</a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td style="font-size:16px;text-align:center;color:#000000;padding:0px 20px 0;">
                            @lang('email.content.body.referral-email-invite.intro',['firstName' => $moduleData->referrerFName])
                            <table width="525" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="steps-table body" align="center">
                                <tr>
                                    <td style="padding:20px 0;" width="33%" valign="top" class="break-align">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                            <tr>
                                                <td width="100%" style="padding:20px;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/7258bbe1-e9bd-4292-bfdc-f22a9dd7803a.png" alt="7258bbe1-e9bd-4292-bfdc-f22a9dd7803a.png">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;font-weight:bold;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.pay-less.heading')</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.pay-less.description')
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:20px 0;" width="33%" valign="top" class="break-align">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                            <tr>
                                                <td width="100%" style="padding:20px;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/7887341d-48c3-437b-9948-9a6e177078f8.png" alt="7887341d-48c3-437b-9948-9a6e177078f8.png">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;font-weight:bold;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.high-quality.heading')</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.high-quality.description')</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:20px 0;" width="33%" valign="top" class="break-align">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                            <tr>
                                                <td width="100%" style="padding:20px;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/b188e530-fecd-47fb-8b45-73afe550b57c.png" alt="b188e530-fecd-47fb-8b45-73afe550b57c.png">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;font-weight:bold;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.free-shipping.heading')</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:16px;text-align:center;padding-left:10px;">@lang('email.content.body.referral-email-invite.free-shipping.description')</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight:700;color:#41352F;font-size:18px;text-align:center;padding:40px 0;">
                            <a href="{{$moduleData->inviteLink}}" style="border:0 none;" target="_blank">
                                <img src="@lang('email.content.body.referral-email-invite.btn-accept-invite')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#000000;font-size:16px;text-align:center;font-weight:normal;line-height:23px;padding:20px 50px !important;">
                            @lang('email.content.body.referral-email-invite.get-tk',['trialPrice' => $moduleData->tprice])
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#000000;font-size:16px;text-align:center;font-weight:normal;line-height:23px;padding:0px 20px 20px !important;">
                            @lang('email.content.body.referral-email-invite.mens-tk.handle') <span style="color: #FE6119">|</span> @lang('email.content.body.referral-email-invite.mens-tk.blades')
                            <span style="color: #FE6119">|</span> @lang('email.content.body.referral-email-invite.mens-tk.cream')
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:20px 0 40px;margin:0;text-align:center;" class="row-padding">
                            <img src="@lang('email.content.body.referral-email-invite.referral-footer-img')" width="520" class="responsive-image" alt="Shipping Now" border="0">
                        </td>
                    </tr>

                    @endif
                </tbody>
            </table>
        </td>
    </tr>
</table>
@endsection