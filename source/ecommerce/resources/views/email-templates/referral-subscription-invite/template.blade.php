@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <img src="@lang('email.content.body.referral-subscription-invite.banner-f')" alt="shaves2u-referral-1" class="img-responsive">
                            @else
                            <img src="@lang('email.content.body.referral-subscription-invite.introImg-m')" alt="shaves2u" class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:center;">
                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1-f.note-1-f')</p>
                                                            <table width="100%" align="left" style="margin:16px 0 36px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="middle" width="50%">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/398b0727-3a77-442e-84a4-9a6cee8b229c.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1-f.note-2-f')</p>
                                                                        </td>
                                                                        <td align="center" valign="middle" width="50%">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/22fb98d6-98d7-4213-87ac-dad7bb37e4a5.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1-f.note-3-f')</p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2-f.note-1-f')</p>
                                                            <table width="100%" align="center" style="margin:16px 0 24px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/3fdaaedb-f11d-4076-9cd1-044500376100.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2-f.note-2-f')</p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/0ae7e25a-bf7e-48b0-af3f-1ae85c35110c.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2-f.note-3-f')</p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/60954bc4-efc0-4ef2-bc45-f97dc60efd65.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2-f.note-4-f',['currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])</p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" align="left" class="highlight-box">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="middle">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                                                                                @if (strtolower($countrycode) == "sg")
                                                                                <img src="@lang('email.content.common.referral.referralBox-f-sg')" class="img-responsive" alt="">
                                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                                <img src="@lang('email.content.common.referral.referralBox-f-hk')" class="img-responsive" alt="">
                                                                                @else
                                                                                <img src="@lang('email.content.common.referral.referralBox-f')" class="img-responsive" alt="">
                                                                                @endif
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        @else
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:center;">
                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1.note-1')</p>
                                                            <table width="100%" align="left" style="margin:16px 0 36px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="middle" width="50%">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/78102517-a8a5-4ca3-9a15-29dc436a13c7.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1.note-2')</p>
                                                                        </td>
                                                                        <td align="center" valign="middle" width="50%">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/78102517-a8a5-4ca3-9a15-29dc436a13c7.jpg" alt="">
                                                                            <br>
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-1.note-3')</p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2.note-1')</p>
                                                            <table width="100%" align="center" style="margin:16px 0 24px;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/bf599f35-78ad-43cc-be71-c6907ba34c4d.jpg" alt="">
                                                                            <br />
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2.note-2')</p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/2de88193-20c0-433f-88c8-da549b7d9938.jpg" alt="">
                                                                            <br />
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2.note-3')</p>
                                                                        </td>
                                                                        <td width="40" class="column"></td>
                                                                        <td align="center" valign="top" class="column mobile-box-size" style="padding:16px 0 0;vertical-align:top;">
                                                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/37ad2c1a-d0f4-4dde-b371-82288a233cda.jpg" alt="">
                                                                            <br />
                                                                            <p>@lang('email.content.body.referral-subscription-invite.block-2.note-4',['currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])</p>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table width="100%" align="left" class="highlight-box">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="color:#000000;font-size:16px;text-align:center;font-weight:bold;line-height:23px;padding:0 50px 40px !important;">
                                                                            @lang('email.content.body.referral-subscription-invite.referral-invite')
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="color:#000000;font-size:18px;text-align:center;font-weight:bold;padding:0;">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals" style="border:0 none;" target="_blank">
                                                                                @if (strtolower($countrycode) == "sg")
                                                                                <img src="@lang('email.content.body.referral-subscription-invite.btn-earn-now-sg')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" />
                                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                                <img src="@lang('email.content.body.referral-subscription-invite.btn-earn-now-hk')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" />
                                                                                @else
                                                                                <img src="@lang('email.content.body.referral-subscription-invite.btn-earn-now')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" />
                                                                                @endif

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection