@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" class="center-column-padding"
                                            style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff"
                                                   align="center">
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="margin: 50px auto;" class="body"
                                                               align="center">
                                                            <tr>
                                                                <td style="font-family:Montserrat, sans-serif;color:black;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    @if($moduleData && isset($moduleData->data) && $moduleData->data)
                                                                    @php($lists = $moduleData->data)
                                                                    </br></br>
                                                                    <table  cellpadding="0" cellspacing="0" width="800">
                                                                        <tr style="font-family:Arial, Helvetica, sans-serif;color:black;font-size:14px;text-align:left;">
                                                                            <td>Job Type</td>
                                                                            <td>Email</td>
                                                                            <td>Subscription Id</td>
                                                                            <td>Date Charged</td>
                                                                            <td>Status</td>
                                                                            <td>Function</td>
                                                                        </tr>
                                                                        @foreach($lists as $list)
                                                                            @foreach($list as $row)
                                                                            <tr style="font-family:Arial, Helvetica, sans-serif;color:black;font-size:12px;text-align:left;">
                                                                                <td>{{ $row["JobType"] }}</td>
                                                                                <td>{{ $row["email"] }}</td>
                                                                                <td>{{ $row["subscriptionId"] }}</td>
                                                                                <td>{{ $row["charge_date"] }}</td>
                                                                                <td>{{ $row["status"] }}</td>
                                                                                <td>stop after ({{ $row["function"] }}) function</td>
                                                                            </tr>
                                                                            @endforeach
                                                                        @endforeach
                                                                    </table>
                                                                    </br></br>
                                                                    @endif
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection
