@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <img src="@lang('email.content.body.subscription-modified.banner-f')" alt="shaves2u-subscription-modify" class="img-responsive">
                            @else
                            <img src="@lang('email.content.body.subscription-modified.introImg-m')" alt="shaves2u" class="img-responsive">
                            @endif
                        </td>
                    </tr>

                    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                    <tr>
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                            <p>
                                                                @lang('email.content.body.subscription-modified.hello-f', ['displayName' => $moduleData->fullname])<br> <br>
                                                                <br>
                                                                @lang('email.content.body.subscription-modified.block-1-f.note-1-f', ['requestOn' => $moduleData->currentdate])
                                                                <br>
                                                                <br>@lang('email.content.body.subscription-modified.block-1-f.note-2-f', ['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate,'Product' => $moduleData->productnamet,'periodTime' => $moduleData->duration,'currency' => $moduleData->currencyDisplay,'price' => $moduleData->totalprice])
                                                                <br>
                                                                <br>@lang('email.content.body.subscription-modified.block-1-f.note-3-f')
                                                                <br>
                                                                <br>@lang('email.content.body.subscription-modified.block-1-f.note-4-f')
                                                                <table width="100%" align="left" class="highlight-box">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center" valign="middle">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_modify">
                                                                                    @if (strtolower($countrycode) == "sg")
                                                                <img src="@lang('email.content.common.referral.referralBox-f-sg')" class="img-responsive" alt="female-referral" style="padding-bottom: 48px;">
                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                <img src="@lang('email.content.common.referral.referralBox-f-hk')" class="img-responsive" alt="female-referral" style="padding-bottom: 48px;">
                                                                @else
                                                                <img src="@lang('email.content.common.referral.referralBox-f')" class="img-responsive" alt="female-referral" style="padding-bottom: 48px;">
                                                                @endif
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                            <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                <tr>
                                    <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                    @lang('email.content.body.subscription-modified.hello', ['displayName' => $moduleData->fullname])
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                    @lang('email.content.body.subscription-modified.block-1.note-1', ['requestOn' => $moduleData->currentdate])<br>
                                        <br>@lang('email.content.body.subscription-modified.block-1.note-2', ['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate,'Product' => $moduleData->productnamet,'periodTime' => $moduleData->duration,'currency' => $moduleData->currencyDisplay,'price' => $moduleData->totalprice])<br>
                                        <br>@lang('email.content.body.subscription-modified.block-1.note-3')<br>
                                        <br>@lang('email.content.body.subscription-modified.block-1.note-4')
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:40px 0;" class="row-padding">
                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                            @if (strtolower($countrycode) == "sg")
                                            <img src="@lang('email.content.common.referral.referralBox-sg')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                            <img src="@lang('email.content.common.referral.referralBox-hk')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @else
                                            <img src="@lang('email.content.common.referral.referralBox')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @endif
                        </a>
                        </td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection