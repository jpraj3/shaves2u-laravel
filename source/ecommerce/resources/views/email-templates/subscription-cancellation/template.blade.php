@extends('email-templates.base')

@section('content')
    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
    <!-- FEMALE: Cancellation Journey 1 Custom Plan content: START -->
        <table class="section" cellpadding="0" cellspacing="0" width="600">
            <tr>
                <td class="column">
                    <table width="100%">
                        <tr>
                            <td>
                                <img src="@lang('email.content.body.subscription-cancellation.banner-f')"
                                     alt="shaves2u-unsubscribe" class="img-responsive">
                            </td>
                        </tr>
                        <tbody>
                        <tr>
                            <td class="column">
                                <table width="100%">
                                    <tbody>
                                    <tr>
                                        <td align="center">
                                        <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center" valign="top" class="content-box"
                                                        style="text-align:left;">
                                                        <p>
                                                            @lang('email.content.body.subscription-cancellation.hello-f',['name' => $moduleData->fullname])
                                                            <br>
                                                            <br>
                                                            @lang('email.content.body.subscription-cancellation.content-f')
                                                        </p>
                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_unsubscribe"><img src="@lang('email.content.body.subscription-cancellation.btn-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    <!-- FEMALE: Cancellation Journey 1 Custom Plan content: END -->
    @else
    <!-- MALE: Cancellation Journey 1 Custom Plan content: START -->
        <table width="600" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body"
               bgcolor="#ffffff" align="center">
            <tr>
                <td style="padding:40px 0;" class="row-padding">
                    <img
                        src="@lang('email.content.body.subscription-cancellation.journey-one.custom.introImg-m')"
                        class="responsive-image" alt="9e4f90b0-aa7d-497a-95fb-4cecb7aa3d19.png"/>
                </td>
            </tr>
            <tr>
                <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;"
                           class="body" align="center">
                        <tr>
                            <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                @lang('email.content.body.subscription-cancellation.journey-one.custom.hello-m', ['name' => $moduleData->fullname])
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                @lang('email.content.body.subscription-cancellation.journey-one.custom.block-1.note-1')
                                <br/>
                                <br/>
                                @lang('email.content.body.subscription-cancellation.journey-one.custom.block-1.note-2')
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 40px;margin:0;text-align:center;" class="row-padding adjust-row-btm-padding">
                    <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;"
                           class="body" align="center">
                        <tr>
                            <td style="padding:20px 0;" align="center" valign="middle" class="break-align">
                                <img
                                    src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/1bd539be-0b1b-4f48-9e78-19c72b8c8b2d.jpg"
                                    border="0" width="250" alt="1bd539be-0b1b-4f48-9e78-19c72b8c8b2d.jpg">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 0;" valign="middle" class="break-align">
                                <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_male_unsubscribe" style="border:0 none;" target="_blank">
                                    <img
                                        src="@lang('email.content.body.subscription-cancellation.journey-one.custom.btn-backtosite-m')"
                                        border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <!-- MALE: Cancellation Journey 1 Custom Plan content: END -->
    @endif
@endsection
