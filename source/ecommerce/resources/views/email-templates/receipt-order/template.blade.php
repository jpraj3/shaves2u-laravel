@extends('email-templates.base')
@section('content')
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="content-box">
                                            <p>@lang('email.content.body.receipt-order.hello', ['name' => $moduleData->user->firstName])<br>
                                                <br>
                                                @lang('email.content.body.receipt-order.intro-order-confirmed')
                                                <br>
                                            </p>
                                            <table width="100%">
                                                <tbody>
                                                    @if ($moduleData->identifiers->isFemale && $moduleData->identifiers->isFemale == true)
                                                    <tr>
                                                        <td valign="top" style="color:#606060;font-size:14px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="body" align="center" style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;margin-top:20px !important;border-left: 1px solid #D3D3D3;border-right: 1px solid #D3D3D3;">
                                                                <tr style="border-top: 1px solid #D3D3D3;border-bottom: 1px solid #D3D3D3;">
                                                                    <td style="padding:10px;font-size:16px;">
                                                                        Item
                                                                    </td>
                                                                    <td></td>
                                                                    <td style="text-align:right;padding:10px;font-size:16px;">
                                                                        Total</td>
                                                                </tr>

                                                                @foreach ($moduleData->orderDetails as $orderDetail)
                                                                @php($totalPPrice =  $orderDetail->price * $orderDetail->qty)
                                                                <tr>
                                                                    <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                        <img src="{{$orderDetail->details->imageUrl}}" style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;" alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                    </td>
                                                                    <td style="padding:20px;">
                                                                        <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                        <p>SKU: {{$orderDetail->details->sku}}</p>
                                                                        <p>Quantity: {{$orderDetail->qty}}</p>
                                                                        <p>Tax code: {{$orderDetail->taxCode}}</p>
                                                                        <p>Unit Price (incl. tax): {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                    </td>
                                                                    <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$totalPPrice}}</td>
                                                                </tr>
                                                                @endforeach

                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="color:#606060;font-size:14px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;border-left: 1px solid #D3D3D3;border-right: 1px solid #D3D3D3;" class="body" align="center">
                                                                <tr>
                                                                    <td style="text-align:right;padding:20px 0;border-bottom:1px solid #D3D3D3;">
                                                                        <p>Sub Total</p>
                                                                        <p style="font-weight:bold;">Discount</p>
                                                                        <p>Processing Fee</p>
                                                                        <p style="font-weight:bold;">Grand Total</p>
                                                                        <p>Cash Rebate</p>
                                                                    </td>
                                                                    <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border-bottom:1px solid #D3D3D3;">
                                                                        <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                        <p>{{$moduleData->receipt->currency}} 0.00</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                        <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:right;padding:20px 0;">
                                                                        <p style="font-weight:bold;">Total Payable (Incl.
                                                                            Tax)</p>
                                                                    </td>
                                                                    <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                        <p style="font-weight:bold;">
                                                                            {{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="font-size:14px;padding-bottom:20px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="body" align="center" style="padding:0;margin:0 auto;border-left: 1px solid #D3D3D3;border-right: 1px solid #D3D3D3;border-bottom: 1px solid #D3D3D3;">
                                                                <tr>
                                                                    <td style="padding:20px;width:40%;">
                                                                        <p>Summary</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->country->taxRate}}%</p>

                                                                    </td>
                                                                    <td style="padding:20px;width:40%;">
                                                                        <p>Amount</p>
                                                                        @if($moduleData->country->taxRate <= 0)
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                    @else
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>
@endif
                                                                    </td>
                                                                    <td style="padding:20px;width:20%;">
                                                                        <p>{{$moduleData->country->taxName}}</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @else
                                                    <tr>
                                                        <td valign="top" style="color:#606060;font-size:14px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="body" align="center" style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;margin-top:20px !important;">
                                                                <tr class="" style="background-color:#403d3b;">
                                                                    <td style="padding:10px;font-size:16px;
                                                                    
                                                                    color:#ffffff;">
                                                                        Item</td>
                                                                    <td></td>
                                                                    <td style="text-align:right;padding:10px;font-size:16px;
                                                                    
                                                                    color:#ffffff;">
                                                                        Total</td>
                                                                </tr>

                                                                @foreach ($moduleData->orderDetails as $orderDetail)
                                                                @php($totalPPrice =  $orderDetail->price * $orderDetail->qty)
                                                                <tr style="background-color:#fafafa;">
                                                                    <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                        <img src="{{$orderDetail->details->imageUrl}}" style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;" alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                    </td>
                                                                    <td style="padding:20px;">
                                                                        <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                        <p>SKU: {{$orderDetail->details->sku}}</p>
                                                                        <p>Quantity: {{$orderDetail->qty}}</p>
                                                                        <p>Tax code: {{$orderDetail->taxCode}}</p>
                                                                        <p>Unit Price (incl. tax): {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                    </td>
                                                                    <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$totalPPrice}}</td>
                                                                </tr>
                                                                @endforeach

                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="color:#606060;font-size:14px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;" class="body" align="center">
                                                                <tr style="background-color:#fafafa;">
                                                                    <td style="text-align:right;padding:20px 0;border-bottom:1px solid #D3D3D3;">
                                                                        <p>Sub Total</p>
                                                                        <p style="font-weight:bold;">Discount</p>
                                                                        <p>Processing Fee</p>
                                                                        <p style="font-weight:bold;">Grand Total</p>
                                                                        <p>Cash Rebate</p>
                                                                    </td>
                                                                    <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border-bottom:1px solid #D3D3D3;">
                                                                        <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                        <p>{{$moduleData->receipt->currency}} 0.00</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                        <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                    </td>
                                                                </tr>
                                                                <tr style="background-color:#fafafa;">
                                                                    <td style="text-align:right;padding:20px 0;">
                                                                        <p style="font-weight:bold;">Total Payable (Incl.
                                                                            Tax)</p>
                                                                    </td>
                                                                    <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                        <p style="font-weight:bold;">
                                                                            {{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="color:#ffffff;font-size:14px;padding-bottom:20px;">
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="body" align="center" style="padding:0;margin:0 auto;">
                                                                <tr style="background-color:#403d3b;">
                                                                    <td style="padding:20px;width:40%;">
                                                                        <p>Summary</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->country->taxRate}}%</p>

                                                                    </td>
                                                                    <td style="padding:20px;width:40%;">
                                                                        <p>Amount</p>
                                                                        @if($moduleData->country->taxRate <= 0)
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                    @else
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>
@endif
                                                                    </td>
                                                                    <td style="padding:20px;width:20%;">
                                                                        <p>{{$moduleData->country->taxName}}</p>
                                                                        <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                            <h2 style="text-align:center;">Looks Good?</h2>
                                            <p style="text-align:center;">Make sure we’ve got your details right.</p>
                                            <table width="100%" style="border-collapse:collapse;margin-top:20px !important;" align="center" class="steps-table body">
                                                <tr>
                                                    <td style="width:44%;" class="break-align">
                                                        <table style="width:100%;border:1px solid #000000;padding:10px;height:200px;border-radius:5px;border-collapse:separate !important;">
                                                            <tr>
                                                                <td style="vertical-align:top;">
                                                                    Shipping
                                                                </td>
                                                                <td style="text-align:right;color:#FFA500;vertical-align:top;">
                                                                    <a href="{{$moduleData->links->viewProfile}}" target="_blank">Edit</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align:top;">
                                                                    <p>{{$moduleData->deliveryAddress->firstName}}
                                                                        {{$moduleData->deliveryAddress->lastName}}</p>
                                                                    <p>
                                                                        {{$moduleData->deliveryAddress->address}},
                                                                        {{$moduleData->deliveryAddress->city}},
                                                                        {{$moduleData->deliveryAddress->portalCode}},&nbsp;{{$moduleData->deliveryAddress->state}},
                                                                        Tel:
                                                                        {{$moduleData->deliveryAddress->contactNumber}}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width:2%;" class="mobile-hidden"></td>
                                                    <td style="width:44%;" class="break-align">
                                                        <table style="width:100%;border:1px solid #000000;padding:10px;height:200px;border-radius:5px;border-collapse:separate !important;">
                                                            <tr>
                                                                <td style="vertical-align:top;">
                                                                    Billing
                                                                </td>
                                                                <td style="text-align:right;color:#FFA500;vertical-align:top;">
                                                                    <a href="{{$moduleData->links->viewProfile}}" target="_blank">Edit</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align:top;">
                                                                    <p>{{$moduleData->billingAddress->firstName}}
                                                                        {{$moduleData->billingAddress->lastName}}</p>
                                                                    <p>
                                                                        {{$moduleData->billingAddress->address}},
                                                                        {{$moduleData->billingAddress->city}},
                                                                        {{$moduleData->billingAddress->portalCode}},&nbsp;{{$moduleData->billingAddress->state}},
                                                                        Tel:
                                                                        {{$moduleData->billingAddress->contactNumber}}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            @if ($moduleData->identifiers->isFemale && $moduleData->identifiers->isFemale == true)
                                            <table width="100%" align="left" class="highlight-box">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="middle">
                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_orderconfirm">
                                                                <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/e6e6299e-f457-4a1e-8928-1a827a60a8d5.jpg" class="img-responsive" alt=""></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @else
                                            <table width="100%" align="left" class="highlight-box">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="600" style="height:224px;background-image:url('https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/a57cceba-0c2a-4fbb-8cb1-935762d74511.jpg');background-repeat:no-repeat;background-size:cover;background-position:center;text-align:center;" class="body">
                                                                <tr>
                                                                    <td valign="top" style="padding-top:40px !important;text-align:center;">
                                                                        <span style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:700;">@lang('email.content.common.referral.referralTitle')</span>
                                                                        <p style="font-family:Montserrat, sans-serif;color:#000000;font-size:14px;font-weight:400;padding:8px 21%;margin-bottom:50px !important;">
                                                                            @lang('email.content.common.referral.referralBody')
                                                                        </p>
                                                                        <span style="margin-top: 10% !important;font-family:Montserrat, sans-serif;color:#000000;font-size:18px;text-align:center;font-weight:bold;padding: 10px 20px; background-color: #FE6119;text-align: center; margin: auto; text-transform: uppercase;">
                                                                            <a href="{{$moduleData->links->referralEarnXX}}" style="color:#ffffff;">@lang('email.content.common.referral.referralBtn')</a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

@endsection