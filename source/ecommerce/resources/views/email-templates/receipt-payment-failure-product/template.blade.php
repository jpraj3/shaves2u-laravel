@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                <img src="@lang('email.content.body.receipt-payment-failure.introImg-alacarte-f')"
                                     alt="shaves2u-payment-failure" class="img-responsive">
                            @else
                                <img
                                    src="@lang('email.content.body.receipt-payment-failure.introImg-alacarte-m')"
                                    class="responsive-image" border="0"
                                    alt="b27192f5-c13b-42a1-91bd-001ca7143c79.jpg">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <table class="section" cellpadding="0" cellspacing="0" width="600">
                                <tr>
                                    <td class="column">
                                        <table width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <table width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top" class="content-box"
                                                                style="text-align:left;">
                                                                <p style="padding-bottom:24px;">
                                                                    @lang('email.content.body.receipt-payment-failure.hello',['firstName' => $moduleData->fullname])
                                                                    <br>
                                                                    <br>
                                                                    @lang('email.content.body.receipt-payment-failure.on-hold',['translatedPlanName' => $moduleData->translatedProductName])
                                                                </p>
                                                                <span class="btn">
                                                                    <a href="config('environment.emailUrl') }}{{$lang}}-{{$countrycode}}/user/shave-plan?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=tw_female_paymentfailure">
                                                                        @lang('email.content.body.receipt-payment-failure.update-payment')
                                                                    </a>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        @else
                            <td align="center" valign="top" class="center-column-padding"
                                style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0"
                                       style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                    <tr>
                                        <td style="padding:0;margin:0;text-align:left;">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.receipt-payment-failure.hello',['firstName' => $moduleData->fullname])
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.receipt-payment-failure.on-hold',['translatedPlanName' => $moduleData->translatedProductName])
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"
                                            style="color:#606060;font-size:14px;padding-bottom:20px !important;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;font-weight:700;color:#41352F;font-size:18px;text-align:center;padding:20px 0 40px;">
                                            <span
                                                style="padding: 10px 30px; background-color: #FE6119; text-align: center; margin: auto; text-transform: uppercase;"
                                                class="btn-custom">
                                                <a href="{{ config('environment.emailUrl') }}{{$lang}}-{{$countrycode}}/user/shave-plan"
                                                   style="color:#ffffff;">@lang('email.content.body.receipt-payment-failure.update-payment')
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection
