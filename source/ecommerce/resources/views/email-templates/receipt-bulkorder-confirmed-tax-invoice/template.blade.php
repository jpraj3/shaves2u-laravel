<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Shaves2U - Tax Invoice</title>
    <style type="text/css">
 @font-face {
            font-family: "droidsansfallback";
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-weight: bold;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-style: italic;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-style: italic;
            font-weight: bold;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "OpenSans";
            src: url({{asset("/fonts/pdf-fonts/OpenSans.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-Bold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-Bold.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-ExtraBold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-ExtraBold.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-Light";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-Light.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-SemiBold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-SemiBold.ttf")}});
        }

        p {
            margin-bottom: 0;
            margin: 0;
        }

        table {
            border-collapse: separate;
            border-spacing: 0;
        }

        td {
            padding: 0;
            border-collapse: collapse;
        }

        span.preheader {
            display: none !important;
        }

        span a {
            color: #ffffff !important;
            text-decoration: none !important;
        }

        .date a {
            color: #f6f3f2 !important;
            text-decoration: none !important;
        }

        .date-color a {
            color: #f26b33 !important;
            text-decoration: none !important;
        }

        .address a {
            /*@font-face {*/
            color: #6a6867 !important;
            text-decoration: none !important;
        }

        .border-right {
            border-right: 1px solid #41352f !important;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

        .footer-bar,
        #footer-bar {
            background: #000000 !important;
        }

        @media only screen and (max-width: 600px) {
            .center-column-padding {
                padding: 0 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body-responsive {
                padding-left: 5% !important;
                padding-right: 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .row-padding {
                padding-left: 8% !important;
                padding-right: 8% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .top-padding {
                padding-top: 8.5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .responsive-image {
                max-width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .no-padding {
                padding: 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .hide {
                display: none !important;
                font-size: 0;
                max-height: 0;
                min-height: 0;
                line-height: 0;
                padding: 0;
                margin: 0;
                mso-hide: all;
            }
        }

        @media only screen and (max-width: 540px) {
            .show {
                display: block !important;
                line-height: normal !important;
                height: auto !important;
                min-height: auto !important;
                max-height: none !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .expand-row {
                width: 100% !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-row {
                width: 121px !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-product-mobile {
                width: 121px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-padding {
                padding: 10px 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-image {
                width: 275px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-headline {
                width: 179px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .adjust-row-btm-padding {
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .steps-table {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .break-align {
                width: 100% !important;
                display: block !important;
                padding: 20px 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .mobile-hidden {
                display: none;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-bottom {
                border-bottom: 1px solid #B8B7B8 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .left-padding-adjust {
                padding-left: 25px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-right {
                border-right: 0 solid #41352f !important;
            }
        }

        @media only screen and (max-width: 414px) {
            .show-product-mobile {
                width: 100px !important;
            }
        }

        @media only screen and (max-width: 320px) {
            .show-product-mobile {
                width: 121px !important;
            }
        }

        #outlook a {
            padding: 0;
        }
        *{ font-family:"OpenSans" , sans-serif !important;}
        .DroidSans{ font-family:"droidsansfallback" , sans-serif !important;}
        .OpenSans{ font-family:"OpenSans" , sans-serif !important; }
    </style>

    <style>
      </style>
</head>

<body>
    <div style="page-break-inside: avoid;">
        <table width="100%" align="center">
            <tr>
                <td class="column">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td align="center" valign="top" class="logo-box" style="padding-bottom:20px !important;">
                                    <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/">
                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/378ef5ee-c8a1-4630-8440-e589efcb104b.png" alt="picsum">
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        <!-- Content -->
        <table width="100%" align="center">
                <tr>
                    <td align="center" valign="top" class="center-column-padding"
                        style="color:#ffffff;font-size:14px;text-align:left;">
                        <table cellpadding="0" cellspacing="0" border="0"
                                style="padding:0;margin:0 auto;" bgcolor="#ffffff" align="center">
                            <tr>
                                <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="padding:0;margin:0 auto;" class="body" align="center">
                                        <tr>
                                            <td width="640" style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:10px !important;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:10px !important;">
                                            
                                                {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->deliveryAddress->firstName.' '.$moduleData->deliveryAddress->lastName ) !!}<br>
                                        
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->deliveryAddress->address.', '.$moduleData->deliveryAddress->portalCode.', '.$moduleData->deliveryAddress->city.', '.$moduleData->deliveryAddress->state) !!}</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->billingAddress->address.', '.$moduleData->billingAddress->portalCode.', '.$moduleData->billingAddress->city.', '.$moduleData->billingAddress->state) !!}</p>
                                                <p>{{$moduleData->order->taxInvoiceNo}}</p>
                                                <p>{{$moduleData->order->orderNo}}</p><br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;margin-top:40px !important;"
                                            align="center">
                                        <tr style="background-color:#fe5000;">
                                            <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_confirmed.table.header-1-f')</td>
                                            <td></td>
                                            <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_confirmed.table.header-2-f')</td>
                                        </tr>
                                        @php($newprice = 0)
                                        @foreach ($moduleData->orderDetails as $orderDetail)
                                        @php($newcurrency = $orderDetail->currency)
                                        @if($orderDetail->currency === "KRW")
                                        @php($orderDetail->currency='원')
                                        @php($newcurrency ='원')
                                        @endif
                                        @php($newprice = $orderDetail->price + $newprice)
                                            <tr style="background-color:#fafafa;">
                                                <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                    <img src="{{$orderDetail->details->imageUrl}}" width="100px" height="100px">
                                                </td>
                                                <td style="padding:20px;">
                                                    <p style="font-weight:bold;color:#000;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->details->translatedName ) !!}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-2-f') {{$orderDetail->qty}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-3-f') {{$orderDetail->taxCode}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-4-f') {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->price}}</p>
                                                </td>
                                                <td style="text-align:center;padding:20px 10px 20px 0;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->details->totalproductprice}}</td>
                                            </tr>
                                        @endforeach
                                        @php($newprice = number_format($newprice, 2, '.', ''))
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="border-bottom:1px solid #D3D3D3;" align="center">
                                        <tr style="background-color:#fafafa;">
                                            <td style="text-align:right;padding:20px 0;border-bottom:1px solid #D3D3D3;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-5-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-6-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-7-f')</p>
                                                <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-8-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-9-f')</p>
                                            </td>
                                            <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border-bottom:1px solid #D3D3D3;">
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} {{$newprice}}</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} 0.00</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} 0.00</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} {{$newprice}}</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} 0.00</p>
                                            </td>
                                        </tr>
                                        <tr style="background-color:#fafafa;">
                                            <td style="text-align:right;padding:20px 0;">
                                                <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-10-f')</p>
                                            </td>
                                            <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} {{$newprice}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                @if($moduleData->country->taxRate > 0)
                                @php($taxa = $moduleData->country->taxRate/100)

                                @if($moduleData->country->code === "KOR")
                                @php($taxamount = 1.10 )
                                @endif
                                @if($moduleData->country->code === "SGP")
                                @php($taxamount = 1.07 )
                                @endif
                               
                                @php($taxamount =  number_format($taxamount, 2, '.', ''))
                                @php($originalprice = $newprice/$taxamount )
                                @php($originalprice = number_format($originalprice, 2, '.', ''))
                                @php($taxamount = number_format($newprice - $originalprice, 2, '.', ''))
                                @else
                                @php($taxamount = "0.00" )
                                @php($originalprice = "0.00" )
                                @endif
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0" align="center">
                                        <tr style="background-color:#fafafa;">
                                            <td style="padding:20px;width:40%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-11-f')</p>
                                                <p style="font-weight:bold;">{{$moduleData->country->taxRate}} %</p>
                                            </td>
                                            <td style="padding:20px;width:40%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-12-f')</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} {{$originalprice}}</p>

                                            </td>
                                            <td style="padding:20px;width:20%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-13-f')</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $newcurrency) !!} {{$taxamount}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <!-- Footer -->
        <table width="640" id="footer-bar" class="footer-bar" align="center">
            <tbody>
                <tr>
                    <td class="wrapper" width="640" align="center">
                        <!-- footer -->
                        <table class="footer" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="column" width="640" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="margin:0 !important;-webkit-text-size-adjust:none;">
                                        <tr>
                                            <td style="color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                <a href="{{ config('environment.emailUrl') }}" style="border:0 none;" target="_blank">
                                                    <img class="logo" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/cdda04dd-d36e-4c06-818c-4c0894573ed1.png" width="70" height="69" alt="Shaves2u Logo" border="0" style="padding: 20px 0;"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px 0 15px;text-align:center;" valign="middle">
                                                <a href="http://instagram.com/shaves2u" target="_blank" style="text-decoration:none;"> <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/855b433d-07ba-4d45-b4b6-fcc6e7e65146.png" alt="Social Icon - Instagram" style="border: 0;"></a>???
                                                <a href="http://facebook.com/shaves2u" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/ed0713cd-e824-427e-93d9-274e6d7962a9.png" alt="Social Icon - Facebook" style="border: 0"></a>???
                                                <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/8b01525a-2213-48c6-8f8f-59c99397c6dc.png" alt="Social Icon - Youtube" style="border: 0"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', config('global.all.email-template.'.strtolower($countrycode).'.footer.company-name')) !!}
                                                <br />
                                                ©All Rights Reserved <?php echo date("Y"); ?>.
                                                <br />
                                                <br />
                                                {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', config('global.all.email-template.'.strtolower($countrycode).'.footer.company-address')) !!}
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>
