@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.reactivate.banner-f')" alt="shaves2u-reactivate" class="img-responsive">
                                            @else
                                                <img src="@lang('email.content.body.reactivate.introImg-m')" alt="8973f01e-f1cc-4b20-a76c-6503b16458e6.jpg" class="responsive-image">
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <!-- FEMALE: Cancellation Journey 2 content: START -->
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <table width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                                            <p>
                                                                            @lang('email.content.body.reactivate.hello-f',['name' => $moduleData->fullname])<br>
                                                                                <br>
                                                                            @lang('email.content.body.reactivate.note-1-f')
                                                                            </p>
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/custom-plans?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_reactivate"><img src="@lang('email.content.body.reactivate.btn-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <!-- FEMALE: Cancellation Journey 2 content: END -->
                                        @else
                                        <!-- MALE: Cancellation Journey 2 content: START -->
                                            <td align="center" valign="top" class="center-column-padding" style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                                <table width="600" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                                    <tr>
                                                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                            <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                                                <tr>
                                                                    <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                                        @lang('email.content.body.reactivate.hello-m',['name' => $moduleData->fullname])
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:40px !important;">
                                                                        @lang('email.content.body.reactivate.note-1-m')
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.reactivate.note-2-m')
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:#000000;font-size:18px;text-align:left;font-weight:bold;">
                                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/" style="border:0 none;" target="_blank">
                                                                            <img src="@lang('email.content.body.reactivate.btn-comeback-m')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding:40px 0 !important;" class="row-padding">
                                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/4a17a2cd-cdf3-4418-bf2f-c665817dcfa8.jpg" alt="4a17a2cd-cdf3-4418-bf2f-c665817dcfa8.jpg" class="responsive-image">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        <!-- MALE: Cancellation Journey 2 content: END -->
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection
