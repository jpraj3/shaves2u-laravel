@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.password-updated.banner-f')" alt="shaves2u-new-password" class="img-responsive">
                                            @else
                                            <img src="@lang('email.content.body.password-updated.banner')" alt="shaves2u-new-password" class="img-responsive">
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td align="center">
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                                            <p class="email-a">
                                                                                @lang('email.content.body.password-updated.hello-f',['name' => $moduleData->fullname])<br>
                                                                                <br>
                                                                                @lang('email.content.body.password-updated.block-1-f.note-1-f',['email' => $moduleData->email])<br>
                                                                                <br>@lang('email.content.body.password-updated.block-1-f.note-2-f')
                                                                            </p>
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/profile-info/?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_newpassword" target="_blank"><img src="@lang('email.content.body.password-updated.btn-img-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        @else
                                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                    @lang('email.content.body.password-updated.hello',['name' => $moduleData->fullname])<br>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:40px !important;" class="email-a">
                                                    <p class="email-a">@lang('email.content.body.password-updated.block-1.note-1',['email' => $moduleData->email])</p><br>
                                                        <br>@lang('email.content.body.password-updated.block-1.note-2')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:18px;text-align:left;font-weight:bold;padding-bottom:40px !important;">
                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/profile-info" style="border:0 none;" target="_blank">
                                                            <img src="@lang('email.content.body.password-updated.myaccountimg')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png">
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection
