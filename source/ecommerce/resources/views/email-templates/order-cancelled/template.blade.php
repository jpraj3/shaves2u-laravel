@extends('email-templates.base')
@section('content')
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img
                                                src="@lang('email.content.body.order_cancelled.banner-f')"
                                                alt="shaves2u-order-cancelled" class="img-responsive">
                                        @else
                                            <img
                                                src="@lang('email.content.body.subscription-cancellation.journey-one.trial.introImg-m')"
                                                alt="shaves2u-order-cancelled" class="img-responsive">
                                            {{--                                                <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/0dddd53d-1bdb-4f16-b4ca-67d8c0d75589.jpg" alt="shaves2u-order-cancelled" class="img-responsive">--}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                    <!-- FEMALE: Cancellation Journey 1 Trial Plan content: START -->
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               align="center">
                                                            <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="content-box"
                                                                    style="text-align:left;">
                                                                    <p style="padding-bottom:24px;">
                                                                        @lang('email.content.body.order_cancelled.hello-f',['name' => $moduleData->fullname])
                                                                        <br>
                                                                        <br>

                                                                        @lang('email.content.body.order_cancelled.note-1-f',['shaveplan' => $moduleData->plantypeget,'product' => $moduleData->productnamet,'duration' => $moduleData->duration])
                                                                        <br>

                                                                        <br>
                                                                        @lang('email.content.body.order_cancelled.note-2-f')
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_cancelled.note-3-f')
                                                                    </p>

                                                                    <table width="600" cellpadding="0" cellspacing="0"
                                                                           border="0"
                                                                           style="padding:24px 0;margin:0 auto;"
                                                                           align="center">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;border:1px solid #dedede;">
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_cancelled.table.header-1-f')</td>
                                                                                        <td></td>
                                                                                        <td style="text-align:right;padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_cancelled.table.header-1-f')</td>
                                                                                    </tr>
                                                                                    <tr style="background-color:#ffffff;border-bottom:1px solid #dedede;">
                                                                                        <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                                            <img
                                                                                                src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                                                style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                                                alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                                        </td>
                                                                                        <td style="padding:20px;">
                                                                                            <p style="font-weight:bold;color:#000;">@lang('email.content.body.order_cancelled.table.item-0-f',['shaveplan' => $moduleData->plantypeget,'product' => $moduleData->productnamet,'duration' => $moduleData->duration])</p>
                                                                                            <p>@lang('email.content.body.order_cancelled.table.item-1-f') {{$moduleData->planskuget}}</p>
                                                                                            <p>@lang('email.content.body.order_cancelled.table.item-2-f')
                                                                                                1</p>
                                                                                            @if (strtolower($countrycode) != "kr")
                                                                                                <p>@lang('email.content.body.order_cancelled.table.item-3-f') {{$moduleData->staxCode}}</p> @endif
                                                                                            <p>@lang('email.content.body.order_cancelled.table.item-4-f') {{$moduleData->currencyDisplay}} {{$moduleData->subscription->pricePerCharge}}</p>
                                                                                        </td>
                                                                                        <td style="text-align:center;padding:20px 10px 20px 0;">{{$moduleData->currencyDisplay}} {{$moduleData->subscription->pricePerCharge}}</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <p style="text-align:left;padding-top:48px;" class="email-a" >
                                                                        @lang('email.content.body.order_cancelled.note-4-f')
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_cancelled.note-5-f')
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <!-- FEMALE: Cancellation Journey 1 Trial Plan content: END -->
                                @else
                                    <!-- MALE: Cancellation Journey 1 Trial Plan content: START -->
                                        <td align="center" valign="top" class="center-column-padding"
                                            style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff"
                                                   align="center">
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;" align="center">
                                                            <tr>
                                                                <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.subscription-cancellation.journey-one.trial.hello',['name' => $moduleData->fullname])
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-1',['shaveplan' => $moduleData->plantypeget,'product' => $moduleData->productnamet,'duration' => $moduleData->duration])
                                                                    <br>
                                                                    <br>
                                                                    @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-2')
                                                                    <br>
                                                                    <br>
                                                                    @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-3')
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="color:#606060;font-size:14px;">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;" class="steps-table"
                                                               align="center">
                                                            <tr style="background-color:#fe5000;">
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.item')</td>
                                                                <td></td>
                                                                <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.total')</td>
                                                            </tr>
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                    <img
                                                                        src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                        style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                        alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                </td>
                                                                <td style="padding:20px;">
                                                                    <p style="font-weight:bold;color:#000;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.name',['shaveplan' => $moduleData->plantypeget,'product' => $moduleData->productnamet,'duration' => $moduleData->duration])</p>
                                                                    <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.sku') {{$moduleData->planskuget}}</p>
                                                                    <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.quantity')
                                                                        1</p>
                                                                    @if (strtolower($countrycode) != "kr")
                                                                        <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.tax-code') {{$moduleData->staxCode}}</p> @endif
                                                                    <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.unit-price') {{$moduleData->currencyDisplay}} {{$moduleData->subscription->pricePerCharge}}</p>
                                                                </td>
                                                                <td style="text-align:center;padding:20px 10px 20px 0;">{{$moduleData->currencyDisplay}} {{$moduleData->subscription->pricePerCharge}}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;padding-top:20px !important;"
                                                        class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;" class="body"
                                                               align="center">
                                                            <tr>
                                                                <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.subscription-cancellation.journey-one.trial.enquiry')
                                                                    <a
                                                                        href="{{$moduleData->email->link_mailto}}"
                                                                        style="color:#fe5000;"
                                                                        target="_top">{{$moduleData->email->link_mailto_title}}
                                                                    </a>
                                                                    @if (strtolower($countrycode) == "hk")
                                                                        @lang('email.content.body.subscription-cancellation.journey-one.trial.enquiry-end')
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <!-- MALE: Cancellation Journey 1 Trial Plan content: END -->
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
@endsection
