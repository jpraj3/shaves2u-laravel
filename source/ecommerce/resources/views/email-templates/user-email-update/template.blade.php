@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" class="center-column-padding"
                                            style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff"
                                                   align="center">
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;" class="body"
                                                               align="center">
                                                            <tr>
                                                                <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    <br/>
                                                                    <br/>
                                                                    User updated email from {{ $moduleData->oldEmail }} to {{ $moduleData->newEmail }}
                                                                    <br/>
                                                                    <br/>
                                                                    <br/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection
