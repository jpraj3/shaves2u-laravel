<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>*|MC:SUBJECT|*</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600%7CMuli:300,400&amp;display=swap" rel="stylesheet">

    <style type="text/css">
        <?php if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale)) { ?>body,
        table,
        thead,
        tbody,
        tr,
        td,
        img {
            border: none;
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
        }

        .wrapper {
            padding-left: 10px;
            padding-right: 10px;
        }

        body {
            background-color: #edeff3 !important;
            color: #000000;
            font-family: 'Muli', sans-serif !important;
            margin: 0 auto;
            padding: 0;
        }

        p {
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            margin: 0;
            word-break: keep-all;
        }

        h1 {
            color: #403d3b;
            font-family: 'Montserrat', sans-serif !important;
            font-weight: 600;
            font-size: 48px;
            margin: 0;
            text-transform: uppercase;
            word-break: keep-all;
        }

        h2 {
            color: #403d3b;
            font-family: 'Muli', sans-serif !important;
            font-weight: 700;
            font-size: 18px;
            margin: 8px 0;
            word-break: keep-all;
        }

        h3 {
            font-weight: 300;
            font-size: 12px;
            font-style: italic;
            line-height: 18px;
            word-break: keep-all;
        }

        a {
            color: #ffffff;
            font-family: 'Montserrat', sans-serif !important;
            text-decoration: none;
        }

        a:hover {
            color: #363636;
        }

        .footer a {
            color: #363636;
            font-size: 12px;
            font-weight: 400;
        }

        .img-responsive {
            box-sizing: border-box;
            display: block;
            height: auto;
            width: 100%;
        }

        .btn {
            background-color: #ff40b4;
            border-radius: 6px;
            color: #ffffff;
            display: inline-block;
            font-family: 'Montserrat', sans-serif;
            font-weight: 600;
            font-size: 15px;
            margin: 20px auto;
            padding: 10px 30px;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
        }

        .btn:hover {
            color: #000000;
        }

        .logo-box {
            background-color: #ffffff;
            padding: 24px 0;
        }

        .section {
            background-color: #edeff3;
        }

        .content-box {
            background-color: #ffffff;
            padding: 48px 36px;
        }

        .highlight-box {
            margin: 36px 0 0;
        }

        .highlight-box-content {
            background-color: #403d3b;
            padding: 36px;
            text-align: center;
        }

        .highlight-box h2,
        .highlight-box p {
            color: #ffffff;
        }

        .mobile-box-size {
            width: 33.333333333%;
        }

        .footer-bar {
            background-color: #edeff3 !important;
        }

        @media only screen and (max-width: 620px) {
            .wrapper .section {
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {
            .wrapper .column {
                display: block;
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {

            .footer-mobile-space p,
            .footer-mobile-space ul {
                text-align: center !important;
            }

        }

        @media only screen and (max-width: 620px) {
            .img-mobile-responsive {
                display: block;
                height: auto;
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {
            .banner-padding-top {
                padding-top: 40px;
            }

        }

        @media only screen and (max-width: 620px) {
            .mobile-box-size {
                width: 100% !important;
                text-align: center !important;
            }
        }
        <?php } else { ?>
        p {
            margin-bottom: 0;
            margin: 0;
            font-family: 'Muli', sans-serif !important;
        }

        table {
            border-collapse: separate;
            border-spacing: 0;
            font-family: 'Muli', sans-serif !important;
        }

        td {
            padding: 0;
            border-collapse: collapse;
            font-family: 'Muli', sans-serif !important;
        }

        span.preheader {
            display: none !important;
        }

        span a {
            color: #ffffff !important;
            text-decoration: none !important;
        }

        .date a {
            color: #f6f3f2 !important;
            text-decoration: none !important;
        }

        .date-color a {
            color: #f26b33 !important;
            text-decoration: none !important;
        }

        .address a {
            /*@font-face {*/
            color: #6a6867 !important;
            text-decoration: none !important;
        }

        .border-right {
            border-right: 1px solid #41352f !important;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

        .footer-bar,
        #footer-bar {
            background: #000000 !important;
        }

        @media only screen and (max-width: 600px) {
            .center-column-padding {
                padding: 0 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body-responsive {
                padding-left: 5% !important;
                padding-right: 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .row-padding {
                padding-left: 8% !important;
                padding-right: 8% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .top-padding {
                padding-top: 8.5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .responsive-image {
                max-width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .no-padding {
                padding: 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .hide {
                display: none !important;
                font-size: 0;
                max-height: 0;
                min-height: 0;
                line-height: 0;
                padding: 0;
                margin: 0;
                mso-hide: all;
            }
        }

        @media only screen and (max-width: 540px) {
            .show {
                display: block !important;
                line-height: normal !important;
                height: auto !important;
                min-height: auto !important;
                max-height: none !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .expand-row {
                width: 100% !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-row {
                width: 121px !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-product-mobile {
                width: 121px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-padding {
                padding: 10px 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-image {
                width: 275px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-headline {
                width: 179px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .adjust-row-btm-padding {
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .steps-table {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .break-align {
                width: 100% !important;
                display: block !important;
                padding: 20px 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .mobile-hidden {
                display: none;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-bottom {
                border-bottom: 1px solid #B8B7B8 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .left-padding-adjust {
                padding-left: 25px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-right {
                border-right: 0 solid #41352f !important;
            }
        }

        @media only screen and (max-width: 414px) {
            .show-product-mobile {
                width: 100px !important;
            }
        }

        @media only screen and (max-width: 320px) {
            .show-product-mobile {
                width: 121px !important;
            }
        }

        <?php } ?>
        #outlook a {
            padding: 0;
        }
         /* custom css */
         .email-a a {
            color: #363636;
            font-family: 'Muli', sans-serif !important;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            margin: 0;
            word-break: keep-all;
        }

        .email-a a:hover {
            color: #363636;
        }
    </style>
</head>

<body>
    <?php if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale)) { ?>
        <table width="100%" class="section" style="background-color:#ffffff !important;">
        <?php } else { ?>
            <table width="100%" class="section">
            <?php } ?>
            <tbody>
                <tr>
                    <td class="wrapper" width="600" align="center">
                        <!-- Header -->
                        <table class="section" cellpadding="0" cellspacing="0" width="600">
                            <tr>
                                <td class="column">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <?php if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale)) { ?>
                                                    <td align="center" valign="top" class="logo-box">
                                                    <?php } else { ?>
                                                    <td align="center" valign="top" class="logo-box" style="padding-bottom:20px !important;">
                                                    <?php } ?>
                                                    <a href="https://shaves2u.com">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/378ef5ee-c8a1-4630-8440-e589efcb104b.png" alt="picsum">
                                                    </a>
                                                    </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- end of Header -->
                        @yield('content')
                    </td>
                </tr>
            </tbody>
            </table>
            <!-- footer start -->
            <table width="100%" id="footer-bar" class="footer-bar">
                <tbody>
                    <tr>
                        <td class="wrapper" width="600" align="center">
                            <!-- footer -->
                            <table class="footer" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="column" width="600" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0 !important;-webkit-text-size-adjust:none;">
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            {{-- Begin Female Footer Logo & Icons --}}
                                            <tr>
                                                <td style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                    <a href="{{ config('environment.emailUrl') }}" style="border:0 none;" target="_blank">
                                                        <img class="logo" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/378ef5ee-c8a1-4630-8440-e589efcb104b.png" width="64" height="64" alt="Shaves2u Logo" border="0" style="padding: 20px 0;"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:10px 0 15px;text-align:center;" valign="middle">
                                                    <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.instagram')}}" target="_blank" style="text-decoration:none;">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/eee1d439-2e6c-44df-ae0f-26174dab931d.png" alt="Social Icon - Instagram" style="border: 0;">
                                                    </a>   
                                                    <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.facebook')}}" target="_blank" style="text-decoration:none;">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/267cd6a8-71e3-4dc3-b10f-40504bb19691.png" alt="Social Icon - Facebook" style="border: 0">
                                                    </a>   
                                                    <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.youtube')}}" target="_blank" style="text-decoration:none;">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/76e77848-2aff-481d-8352-45177cecf40f.png" alt="Social Icon - Youtube" style="border: 0">
                                                    </a>
                                                </td>
                                            </tr>
                                            {{-- End Female Footer Logo & Icons --}}
                                            @else
                                            <tr>
                                                <td style="font-family:Montserrat, sans-serif;color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                    <a href="{{ config('environment.emailUrl') }}" style="border:0 none;" target="_blank">
                                                        <img class="logo" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/cdda04dd-d36e-4c06-818c-4c0894573ed1.png" width="70" height="69" alt="Shaves2u Logo" border="0" style="padding: 20px 0;"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:10px 0 15px;text-align:center;" valign="middle">
                                                    <a href="http://instagram.com/shaves2u" target="_blank" style="text-decoration:none;"> <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/855b433d-07ba-4d45-b4b6-fcc6e7e65146.png" alt="Social Icon - Instagram" style="border: 0;"></a>   
                                                    <a href="http://facebook.com/shaves2u" target="_blank" style="text-decoration:none;">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/ed0713cd-e824-427e-93d9-274e6d7962a9.png" alt="Social Icon - Facebook" style="border: 0"></a>   
                                                    <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ" target="_blank" style="text-decoration:none;">
                                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/8b01525a-2213-48c6-8f8f-59c99397c6dc.png" alt="Social Icon - Youtube" style="border: 0"></a>
                                                </td>
                                            </tr>
                                            @endif
                                            <tr>
                                                @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                                <td style="color:#363636;font-size:11px;text-align:center;padding:36px;line-height:18px;letter-spacing:.05em;">
                                                    @else
                                                <td style="font-family:Montserrat, sans-serif;color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                    @endif
                                                    {!! config('global.all.email-template.'.strtolower($countrycode).'.footer.company-name') !!}
                                                    <br />
                                                    ©All Rights Reserved <?php echo date("Y"); ?>.
                                                    <br />
                                                    <br />
                                                    {{config('global.all.email-template.'.strtolower($countrycode).'.footer.company-address')}}
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
</body>

</html>