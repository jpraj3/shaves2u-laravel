Email Address: {{ $moduleData->user_email }}<br>
User country: {{ $moduleData->country }}<br>
Cash-out amount: {{ $moduleData->currency }} {{ $moduleData->amount }}<br>
Bank: {{ $moduleData->bank_name }}<br>
Full Name as per Bank Account: {{ $moduleData->full_name }}<br>
Bank Account Number: {{ $moduleData->bank_acc_no }}<br>
Request date/time: {{ $moduleData->datetime }}