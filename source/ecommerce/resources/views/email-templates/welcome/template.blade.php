@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            {{-- @if ($moduleData->identifiers->isFemale == true) --}}
                                            <img src="@lang('email.content.body.welcome.introImgFemale')" alt="shaves2u-welcome-email" class="img-responsive">
                                            @else
                                            <img src="@lang('email.content.body.welcome.introImg')" alt="shaves2u" class="img-responsive">
                                            @endif
                                        </td>
                                    </tr>
                                    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                    <tr>
                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                            <p>
                                                @lang('email.content.body.welcome.hello-f',['name' => $moduleData->fullname])<br>
                                                <br>
                                                @lang('email.content.body.welcome.intro-f')
                                            </p>
                                            @if ($moduleData->apptype == "offline") 
                                                <table width="100%" align="left">
                                                    <tr>
                                                        <td style="padding:24px 0;">
                                                            <hr style="height:2px;background-color:#61366e;">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <h2>@lang('email.content.body.welcome.verifyTitle-f')</h2>
                                                <p> @lang('email.content.body.welcome.verifyBody-f',['currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])</p>
                                                <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/activate/{{Crypt::encrypt($moduleData->email)}}/?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_welcome"><img src="@lang('email.content.body.welcome.verifyBtn-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                          @endif
                                            <table width="100%" align="left" class="highlight-box">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="middle">
                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_welcome">
                                                                @if (strtolower($countrycode) == "sg")
                                                                <img src="@lang('email.content.common.referral.referralBox-f-sg')" class="img-responsive" alt="" style="padding-bottom: 48px;">
                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                <img src="@lang('email.content.common.referral.referralBox-f-hk')" class="img-responsive" alt="" style="padding-bottom: 48px;">
                                                                @else
                                                                <img src="@lang('email.content.common.referral.referralBox-f')" class="img-responsive" alt="" style="padding-bottom: 48px;">
                                                                @endif
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p style="padding-bottom:24px;">@lang('email.content.body.welcome.ps-f')</p>
                                        </td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.welcome.hello',['name' => $moduleData->fullname])
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.welcome.intro')
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php if ($moduleData->apptype == "offline") { ?>
                                        <tr style="font-size:14px;color:#606060;">
                                            <td style="padding-top:20px;">
                                                <table width="500" style="border-collapse:collapse;" align="center" class="steps-table body">
                                                    <tr>
                                                        <td class="break-align">
                                                            <table style="width:100%;border:1px solid #000000;padding:10px;height:200px;border-radius:5px;">
                                                                <tr style="height:25px;">
                                                                    <td style="color:#41352F;font-size:16px;text-align:center;font-weight:bold;border-bottom:1px solid #000000;">
                                                                        <strong>@lang('email.content.body.welcome.verifyTitle')</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="color:#41352F;font-size:16px;text-align:center;font-weight:normal;line-height:17px;padding:0 10px;">
                                                                        @lang('email.content.body.welcome.verifyBody')<br>
                                                                        <br>
                                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/activate/{{Crypt::encrypt($moduleData->email)}}/" style="border:0 none;" target="_blank">
                                                                            <img src="@lang('email.content.body.welcome.verifyBtn')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png">
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td style="padding:40px 0;" class="row-padding">
                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                                            @if (strtolower($countrycode) == "sg")
                                            <img src="@lang('email.content.common.referral.referralBox-sg')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                            <img src="@lang('email.content.common.referral.referralBox-hk')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @else
                                            <img src="@lang('email.content.common.referral.referralBox')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @endif
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection