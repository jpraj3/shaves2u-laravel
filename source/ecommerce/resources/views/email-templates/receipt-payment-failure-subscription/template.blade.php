@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                <img src="@lang('email.content.body.receipt-payment-failure.banner-f')"
                                     alt="shaves2u-payment-failure" class="img-responsive">
                            @else
                                <img
                                    src="@lang('email.content.body.receipt-payment-failure.introImg-m')"
                                    alt="shaves2u" class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <td align="center">
                                <table width="100%">
                                    <tbody>
                                    <tr>
                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                            <p style="padding-bottom:24px;" class="email-a">
                                                @lang('email.content.body.receipt-payment-failure.hello-f', ['name' => $moduleData->fullname])
                                                <br>
                                                <br>
                                                @lang('email.content.body.receipt-payment-failure.on-hold-f', ['translatedPlanName' => $moduleData->subscription->translatedPlanName])
                                                <br>
                                                <br>
                                                @lang('email.content.body.receipt-payment-failure.on-hold-1-f')<br>
                                            </p>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:24px 0 48px;margin:0 auto;" align="center">
                                                <tr style="background-color:#f0eeee;color:#000000;font-size:16px;">
                                                    <td style="background-color:#913493;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.current-product-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.current-product-f.data-f', ['translatedPlanName' => $moduleData->subscription->translatedPlanName])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#f0eeee;color:#000000;font-size:16px;">
                                                    <td style="background-color:#913493;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.transaction-id-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.transaction-id-f.data-f', ['transactionId' => $moduleData->subscription->transactionId])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#f0eeee;color:#000000;font-size:16px;">
                                                    <td style="background-color:#913493;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.next-payment-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.next-payment-f.data-f', ['nextPaymentDate' => $moduleData->subscription->nextChargeDate])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#f0eeee;color:#000000;font-size:16px;">
                                                    <td style="background-color:#913493;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.billing-cycle-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.billing-cycle-f.data-f', ['billingCycle' => $moduleData->subscription->billingCycle])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#f0eeee;color:#000000;font-size:16px;">
                                                    <td style="background-color:#913493;width:40%;text-align:center;color:#ffffff;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.payment-method-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;padding:10px 0 20px;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.payment-method-f.data-f', ['cardNumber' => $moduleData->subscription->cardNumber])
                                                    </td>
                                                </tr>
                                            </table>
                                            <a href="{{ config('environment.emailUrl') }}{{$lang}}-{{$countrycode}}/user/shave-plan?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_paymentfailure"><img src="@lang('email.content.body.receipt-payment-failure.update-payment-img-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        @else
                            <td align="center" valign="top" class="center-column-padding"
                                style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0"
                                       style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                    <tr>
                                        <td style="padding:0;margin:0;text-align:left;">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.receipt-payment-failure.hello', ['firstName' => $moduleData->user->firstName])
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.receipt-payment-failure.on-hold', ['translatedPlanName' => $moduleData->subscription->translatedPlanName])
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"
                                            style="color:#606060;font-size:14px;padding-bottom:20px !important;">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr style="background-color:#fafafa;color:#000000;font-size:16px;">
                                                    <td style="background-color:#363636;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.current-product.title')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.current-product.data', ['translatedPlanName' => $moduleData->subscription->translatedPlanName])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#fafafa;color:#000000;font-size:16px;">
                                                    <td style="background-color:#363636;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.transaction-id-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.transaction-id.data', ['transactionId' => $moduleData->subscription->transactionId])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#fafafa;color:#000000;font-size:16px;">
                                                    <td style="background-color:#363636;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription-f.next-payment-f.title-f')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.next-payment.data', ['nextPaymentDate' => $moduleData->subscription->nextChargeDate])
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#fafafa;color:#000000;font-size:16px;">
                                                    <td style="background-color:#363636;width:40%;text-align:center;color:#ffffff;padding:10px;border-bottom:1px solid #fafafa;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.billing-cycle.title')
                                                    </td>
                                                    <td style="text-align:center;">
                                                        @lang('email.content.body.receipt-payment-failure.subscription.billing-cycle.data', ['billingCycle' => $moduleData->subscription->billingCycle])
                                                    </td>
                                                </tr>
                                                @if ($moduleData->country->code !== "KOR")
                                                    <tr style="background-color:#fafafa;color:#000000;font-size:16px;">
                                                        <td style="background-color:#363636;width:40%;text-align:center;color:#ffffff;">
                                                            @lang('email.content.body.receipt-payment-failure.subscription.payment-method.title')
                                                        </td>
                                                        <td style="text-align:center;padding:10px 0 20px;">
                                                            @lang('email.content.body.receipt-payment-failure.subscription.payment-method.data', ['cardNumber' => $moduleData->subscription->cardNumber])
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:700;color:#41352F;font-size:18px;text-align:center;padding:20px 0 40px;">
                                            <a href="{{ config('environment.emailUrl') }}{{$lang}}-{{$countrycode}}/user/profile-info"
                                               style="border:0 none;" target="_blank">
                                                <img src="@lang('email.content.body.receipt-payment-failure.btn-update-payment')"
                                                    border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png">
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection

