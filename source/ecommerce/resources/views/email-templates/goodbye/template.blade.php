@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.goodbye.banner-f')" alt="shaves2u-goodbye" class="img-responsive">
                                            @else
                                            <img class="responsive-image"
                                                     src="@lang('email.content.body.goodbye.introImg-m')"
                                                     alt="28a521ca-33e7-46ee-80d1-60840561e813.jpg">
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <!-- FEMALE: Cancellation Journey 5 content: START -->
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="content-box"
                                                                    style="text-align:left;">
                                                                    <p>
                                                                        @lang('email.content.body.goodbye.hello-f',['name' => $moduleData->fullname])
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.goodbye.note-1-f')
                                                                        <br>
                                                                        <br> @lang('email.content.body.goodbye.note-2-f')
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <!-- FEMALE: Cancellation Journey 5 content: END -->
                                    @else
                                        <!-- MALE: Cancellation Journey 5 content: START -->
                                        <td align="center" valign="top" class="center-column-padding"
                                            style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff"
                                                   align="center">
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;" class="body"
                                                               align="center">
                                                            <tr>
                                                                <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.goodbye.hello-m',['name' => $moduleData->fullname])
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.goodbye.note-1-m')
                                                                    <br/>
                                                                    <br/>
                                                                    @lang('email.content.body.goodbye.note-2-m')
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <!-- MALE: Cancellation Journey 5 content: END -->
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection
