<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Shaves2U - Tax Invoice</title>
    <style type="text/css">
        @font-face {
            font-family: "droidsansfallback";
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-weight: bold;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-style: italic;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallback";
            font-style: italic;
            font-weight: bold;
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallback.ttf")}});
        }
        @font-face {
            font-family: "droidsansfallbackfull";
            src: url({{asset("/fonts/pdf-fonts/DroidSansFallbackFull.ttf")}});
        }
        @font-face {
            font-family: "OpenSans";
            src: url({{asset("/fonts/pdf-fonts/OpenSans.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-Bold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-Bold.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-ExtraBold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-ExtraBold.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-Light";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-Light.ttf")}});
        }

        @font-face {
            font-family: "OpenSans-SemiBold";
            src: url({{asset("/fonts/pdf-fonts/OpenSans-SemiBold.ttf")}});
        }
        <?php if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale)) { ?>body,
        table,
        thead,
        tbody,
        tr,
        td,
        img {
            border: none;
            border-collapse: collapse;
            border-spacing: 0;
            margin: 0;
            padding: 0;
        }

        .wrapper {
            /* padding-left: 10px;
            padding-right: 10px; */
        }

        body {
            background-color: #edeff3 !important;
            color: #000000;
            margin: 0 auto;
            padding: 0;
        }

        p {
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            margin: 0;
            word-break: keep-all;
        }

        h1 {
            color: #403d3b;
            font-weight: 600;
            font-size: 48px;
            margin: 0;
            text-transform: uppercase;
            word-break: keep-all;
        }

        h2 {
            color: #403d3b;
            font-weight: 700;
            font-size: 18px;
            margin: 8px 0;
            word-break: keep-all;
        }

        h3 {
            font-weight: 300;
            font-size: 12px;
            font-style: italic;
            line-height: 18px;
            word-break: keep-all;
        }

        a {
            color: #ffffff;
            text-decoration: none;
        }

        a:hover {
            color: #363636;
        }

        .footer a {
            color: #363636;
            font-size: 12px;
            font-weight: 400;
        }

        .img-responsive {
            box-sizing: border-box;
            display: block;
            height: auto;
            width: 100%;
        }

        .btn {
            background-color: #ff40b4;
            border-radius: 6px;
            color: #ffffff;
            display: inline-block;
            font-weight: 600;
            font-size: 15px;
            margin: 20px auto;
            padding: 10px 30px;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
        }

        .btn:hover {
            color: #000000;
        }

        .logo-box {
            background-color: #ffffff;
            padding: 24px 0;
        }

        .section {
            background-color: #edeff3;
        }

        .content-box {
            background-color: #ffffff;
            padding: 48px 36px;
        }

        .highlight-box {
            margin: 36px 0 0;
        }

        .highlight-box-content {
            background-color: #403d3b;
            padding: 36px;
            text-align: center;
        }

        .highlight-box h2,
        .highlight-box p {
            color: #ffffff;
        }

        .mobile-box-size {
            width: 33.333333333%;
        }

        .footer-bar {
            background-color: #edeff3 !important;
        }

        @media only screen and (max-width: 620px) {
            .wrapper .section {
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {
            .wrapper .column {
                display: block;
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {

            .footer-mobile-space p,
            .footer-mobile-space ul {
                text-align: center !important;
            }

        }

        @media only screen and (max-width: 620px) {
            .img-mobile-responsive {
                display: block;
                height: auto;
                width: 100%;
            }

        }

        @media only screen and (max-width: 620px) {
            .banner-padding-top {
                padding-top: 40px;
            }

        }

        @media only screen and (max-width: 620px) {
            .mobile-box-size {
                width: 100% !important;
                text-align: center !important;
            }
        }

        /* custom css */
        .email-a a {
            color: #363636;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            margin: 0;
            word-break: keep-all;
        }

        .email-a a:hover {
            color: #363636;
        }

        <?php } else { ?>p {
            margin-bottom: 0;
            margin: 0;
        }

        table {
            border-collapse: separate;
            border-spacing: 0;
        }

        td {
            padding: 0;
            border-collapse: collapse;
        }

        span.preheader {
            display: none !important;
        }

        span a {
            color: #ffffff !important;
            text-decoration: none !important;
        }

        .date a {
            color: #f6f3f2 !important;
            text-decoration: none !important;
        }

        .date-color a {
            color: #f26b33 !important;
            text-decoration: none !important;
        }

        .address a {
            /*@font-face {*/
            color: #6a6867 !important;
            text-decoration: none !important;
        }

        .border-right {
            border-right: 1px solid #41352f !important;
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

        .footer-bar,
        #footer-bar {
            background: #000000 !important;
        }

        @media only screen and (max-width: 600px) {
            .center-column-padding {
                padding: 0 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body-responsive {
                padding-left: 5% !important;
                padding-right: 5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .row-padding {
                padding-left: 8% !important;
                padding-right: 8% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .top-padding {
                padding-top: 8.5% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .responsive-image {
                max-width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .body {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .no-padding {
                padding: 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .hide {
                display: none !important;
                font-size: 0;
                max-height: 0;
                min-height: 0;
                line-height: 0;
                padding: 0;
                margin: 0;
                mso-hide: all;
            }
        }

        @media only screen and (max-width: 540px) {
            .show {
                display: block !important;
                line-height: normal !important;
                height: auto !important;
                min-height: auto !important;
                max-height: none !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .expand-row {
                width: 100% !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-row {
                width: 121px !important;
                padding-top: 5px !important;
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-product-mobile {
                width: 121px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-padding {
                padding: 10px 0 !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .gif-image {
                width: 275px !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .show-headline {
                width: 179px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 540px) {
            .adjust-row-btm-padding {
                padding-bottom: 20px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .steps-table {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .break-align {
                width: 100% !important;
                display: block !important;
                padding: 20px 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .mobile-hidden {
                display: none;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-bottom {
                border-bottom: 1px solid #B8B7B8 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .left-padding-adjust {
                padding-left: 25px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .border-right {
                border-right: 0 solid #41352f !important;
            }
        }

        @media only screen and (max-width: 414px) {
            .show-product-mobile {
                width: 100px !important;
            }
        }

        @media only screen and (max-width: 320px) {
            .show-product-mobile {
                width: 121px !important;
            }
        }

        <?php } ?>#outlook a {
            padding: 0;
        }
        *{ font-family:"OpenSans" , sans-serif !important;}
        .DroidSans{ font-family:"droidsansfallback" , sans-serif !important;}
        .DroidSansF{ font-family:"droidsansfallbackfull" , sans-serif !important;}
        .OpenSans{ font-family:"OpenSans" , sans-serif !important; }
    </style>

    <style>
      </style>
</head>

<body>
    <div style="page-break-inside: avoid;">
        @if(isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale))
        <table width="100%" style="background-color:#ffffff !important;">
        @else
        <table width="100%" align="center">
        @endif
        <table width="100%" align="center">
            <tr>
                <td class="column">
                    <table width="100%">
                        <tbody>
                            <tr>
                                @if(isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale))
                                    <td align="center" valign="top" class="logo-box">
                                    @else
                                    <td align="center" valign="top" class="logo-box" style="padding-bottom:20px !important;">
                                    @endif
                                    <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/">
                                        <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/378ef5ee-c8a1-4630-8440-e589efcb104b.png" alt="picsum">
                                    </a>
                                    </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        </table>
        <!-- Content -->
        <table width="100%" align="center">
            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                <tr>
                    <td align="center">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" class="content-box" style="text-align:left;">
                                    <p style="padding-bottom:24px;">
                                        @lang('email.content.body.order_confirmed.hello-f',['name' => $moduleData->order->fullName])
                                        <br>
                                        <br>
                                        @lang('email.content.body.order_confirmed.note-1-f')
                                        {{-- {{ Lang::get('email.content.body.order_confirmed.note-1-f', 'ko') }} --}}
                                        <br>
                                        {{-- {{  __('email.content.body.order_confirmed.note-1-f', 'zh-tw') }} --}}

                                    </p>
                                    <table cellpadding="0" cellspacing="0" border="0"
                                            style="padding:24px 0;margin:0 auto;border-bottom:0px solid #D3D3D3;"
                                            align="center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                        style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0px solid #000000;"
                                                        class="body" align="center">
                                                    <tr style="background-color:#ffffff;border:1px solid #dedede;">
                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_confirmed.table.header-1-f')</td>
                                                        <td></td>
                                                        <td style="text-align:right;padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_confirmed.table.header-2-f')</td>
                                                    </tr>
                                                    @foreach ($moduleData->orderDetails as $orderDetail)
                                                        <tr style="background-color:#ffffff;">
                                                            <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                <img
                                                                    src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                    style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                    alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                            </td>
                                                            <td style="padding:20px;">
                                                                <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                <p>@lang('email.content.body.order_confirmed.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                <p>@lang('email.content.body.order_confirmed.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                @if (strtolower($countrycode) != "kr")
                                                                    <p>@lang('email.content.body.order_confirmed.table.item-3-f') {{$orderDetail->taxCode}}</p>
                                                                @endif
                                                                <p>@lang('email.content.body.order_confirmed.table.item-4-f') {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->price}}</p>
                                                            </td>
                                                            <td style="text-align:center;padding:20px 10px 20px 0;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->details->totalproductprice}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="color:#606060;font-size:14px;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                        style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0 solid #D3D3D3;"
                                                        class="body" align="center">
                                                    <tr style="background-color:#ffffff;">
                                                        <td style="text-align:right;padding:20px 0;border:1px solid #D3D3D3;border-right:0 solid #000000;">
                                                            <p>@lang('email.content.body.order_confirmed.table.item-5-f')</p>
                                                            <p>@lang('email.content.body.order_confirmed.table.item-6-f')</p>
                                                            <p>@lang('email.content.body.order_confirmed.table.item-7-f')</p>
                                                            <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-8-f')</p>
                                                            <p>@lang('email.content.body.order_confirmed.table.item-9-f')</p>
                                                        </td>
                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border:1px solid #D3D3D3;border-left:0 solid #000000;">
                                                            <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->originPrice}}</p>
                                                            <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->discountAmount}}</p>
                                                            <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} 0.00</p>
                                                            <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                            <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->cashRebate}}</p>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color:#ffffff;">
                                                        <td style="text-align:right;padding:20px 0;">
                                                            <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-10-f')</p>
                                                        </td>
                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                            <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>

                                        @if($moduleData->country->code === "KOR")
                                        @php($taxamount =  number_format($moduleData->receipt->taxPrice, 2, '.', ''))
                                @php($originalpricewithouttax = $moduleData->receipt->pricewithouttax)
                                @else
                            @if($moduleData->country->taxRate > 0)
                                @php($taxamount =  number_format($moduleData->receipt->taxPrice, 2, '.', ''))
                                @php($originalpricewithouttax = $moduleData->receipt->priceAfterDiscountAndRebate/$taxamount )
                                @php($originalpricewithouttax = number_format($originalpricewithouttax, 2, '.', ''))
                                @php($taxamount = $moduleData->receipt->priceAfterDiscountAndRebate - $originalpricewithouttax)
                                @php($taxamount =  number_format($taxamount, 2, '.', ''))
                            @else
                                @php($originalpricewithouttax = "0.00" )
                                @php($taxamount = "0.00" )
                            @endif
                            @endif
                                            <td valign="top" style="color:#000000;font-size:14px;padding-bottom:20px;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                        class="body" align="center"
                                                        style="padding:0;margin:0 auto;border:1px solid #D3D3D3;">
                                                    <tr style="background-color:#ffffff;">
                                                        <td style="padding:20px;width:40%;">
                                                            <p>@lang('email.content.body.order_confirmed.table.item-11-f')</p>
                                                            <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                                %</p>
                                                        </td>
                                                        <td style="padding:20px;width:40%;color:#000000;">
                                                            <p>@lang('email.content.body.order_confirmed.table.item-12-f')</p>
                                                            <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$originalpricewithouttax}}</p>
                                                        </td>
                                                        <td style="padding:20px;width:20%;color:#000000;">
                                                            <p>@lang('email.content.body.order_confirmed.table.item-13-f')</p>
                                                            <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->taxPrice}}</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            @else
                <tr>
                    <td align="center" valign="top" class="center-column-padding"
                        style="color:#ffffff;font-size:14px;text-align:left;">
                        <table cellpadding="0" cellspacing="0" border="0"
                                style="padding:0;margin:0 auto;" bgcolor="#ffffff" align="center">
                            <tr>
                                <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="padding:0;margin:0 auto;" class="body" align="center">
                                        <tr>
                                            <td width="640" style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:10px !important;">
                                                {{-- @lang('email.content.body.tax-invoice.hello', ['name' => $moduleData->user->firstName]) --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:10px !important;">
                                                {{-- @lang('email.content.body.tax-invoice.intro-order-confirmed')
                                                <br>
                                                <p class="DroidSans">결제 오류가 발생했습니다  很抱歉扣款失敗 驗證您的電子郵件</p>
                                                <br>
                                                <p> {!! Lang::get('email.content.body.tax-invoice.intro-order-confirmed', [], 'ko') !!}</p>
                                                <br> --}}
                                                <div class="">
                                                    {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->user->firstName.' '.$moduleData->user->lastName ) !!}<br>
                                                    @if (isset($moduleData->deliveryAddress))
                                                    <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->deliveryAddress->address.', '.$moduleData->deliveryAddress->portalCode.', '.$moduleData->deliveryAddress->city.', '.$moduleData->deliveryAddress->state) !!}</p>
                                                   @else
                                                   @endif
                                                   @if (isset($moduleData->billingAddress))
                                                    <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->billingAddress->address.', '.$moduleData->billingAddress->portalCode.', '.$moduleData->billingAddress->city.', '.$moduleData->billingAddress->state) !!}</p>
                                                    @else
                                                   @endif
                                                    <p>{{$moduleData->order->taxInvoiceNo}}</p>
                                                    <p>{{$moduleData->order->orderNo}}</p><br>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;margin-top:40px !important;"
                                            align="center">
                                        <tr style="background-color:#fe5000;">
                                            <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_confirmed.table.header-1-f')</td>
                                            <td></td>
                                            <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_confirmed.table.header-2-f')</td>
                                        </tr>
                                        @foreach ($moduleData->orderDetails as $orderDetail)
                                            <tr style="background-color:#fafafa;">
                                                <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                    <img src="{{$orderDetail->details->imageUrl}}" width="100px" height="100px">
                                                </td>
                                                <td style="padding:20px;">
                                                    <p style="font-weight:bold;color:#000;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->details->translatedName ) !!}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-2-f') {{$orderDetail->qty}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-3-f') {{$orderDetail->taxCode}}</p>
                                                    <p>@lang('email.content.body.order_confirmed.table.item-4-f') {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->price}}</p>
                                                </td>
                                                <td style="text-align:center;padding:20px 10px 20px 0;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $orderDetail->currency) !!} {{$orderDetail->details->totalproductprice}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            style="border-bottom:1px solid #D3D3D3;" align="center">
                                        <tr style="background-color:#fafafa;">
                                            <td style="text-align:right;padding:20px 0;border-bottom:1px solid #D3D3D3;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-5-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-6-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-7-f')</p>
                                                <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-8-f')</p>
                                                <p>@lang('email.content.body.order_confirmed.table.item-9-f')</p>
                                            </td>
                                            <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border-bottom:1px solid #D3D3D3;">
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->originPrice}}</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->discountAmount}}</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} 0.00</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                <p>{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->cashRebate}}</p>
                                            </td>
                                        </tr>
                                        <tr style="background-color:#fafafa;">
                                            <td style="text-align:right;padding:20px 0;">
                                                <p style="font-weight:bold;">@lang('email.content.body.order_confirmed.table.item-10-f')</p>
                                            </td>
                                            <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                @if($moduleData->country->code === "KOR")
                                @php($taxamount =  number_format($moduleData->receipt->taxPrice, 2, '.', ''))
                                @php($originalpricewithouttax = $moduleData->receipt->pricewithouttax)
                                @else
                            @if($moduleData->country->taxRate > 0)
                                @php($taxamount =  number_format($moduleData->receipt->taxPrice, 2, '.', ''))
                                @php($originalpricewithouttax = $moduleData->receipt->priceAfterDiscountAndRebate/$taxamount )
                                @php($originalpricewithouttax = number_format($originalpricewithouttax, 2, '.', ''))
                                @php($taxamount = $moduleData->receipt->priceAfterDiscountAndRebate - $originalpricewithouttax)
                                @php($taxamount =  number_format($taxamount, 2, '.', ''))
                            @else
                                @php($originalpricewithouttax = "0.00" )
                                @php($taxamount = "0.00" )
                            @endif
                            @endif
                                <td valign="top" style="color:#606060;font-size:14px;">
                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                            align="center">
                                        <tr style="background-color:#fafafa;">
                                            <td style="padding:20px;width:40%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-11-f')</p>
                                                <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                    %</p>
                                            </td>
                                            <td style="padding:20px;width:40%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-12-f')</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$originalpricewithouttax}}</p>

                                            </td>
                                            <td style="padding:20px;width:20%;">
                                                <p>@lang('email.content.body.order_confirmed.table.item-13-f')</p>
                                                <p style="font-weight:bold;">{!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', $moduleData->receipt->currency) !!} {{$taxamount}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            @endif
        </table>
        <!-- Footer -->
        <table width="640" id="footer-bar" class="footer-bar" align="center">
            <tbody>
                <tr>
                    <td class="wrapper" width="640" align="center">
                        <!-- footer -->
                        <table class="footer" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="column" width="640" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="margin:0 !important;-webkit-text-size-adjust:none;">
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        {{-- Begin Female Footer Logo & Icons --}}
                                        <tr>
                                            <td style="color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                <a href="{{ config('environment.emailUrl') }}" style="border:0 none;" target="_blank">
                                                    <img class="logo" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/378ef5ee-c8a1-4630-8440-e589efcb104b.png" width="64" height="64" alt="Shaves2u Logo" border="0" style="padding: 20px 0;"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px 0 15px;text-align:center;" valign="middle">
                                                <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.instagram')}}" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/eee1d439-2e6c-44df-ae0f-26174dab931d.png" alt="Social Icon - Instagram" style="border: 0;">
                                                </a>???
                                                <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.facebook')}}" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/267cd6a8-71e3-4dc3-b10f-40504bb19691.png" alt="Social Icon - Facebook" style="border: 0">
                                                </a>???
                                                <a href="{{config('global.all.email-template.'.strtolower($countrycode).'.footer.links.youtube')}}" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/76e77848-2aff-481d-8352-45177cecf40f.png" alt="Social Icon - Youtube" style="border: 0">
                                                </a>
                                            </td>
                                        </tr>
                                        {{-- End Female Footer Logo & Icons --}}
                                        @else
                                        <tr>
                                            <td style="color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                <a href="{{ config('environment.emailUrl') }}" style="border:0 none;" target="_blank">
                                                    <img class="logo" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/cdda04dd-d36e-4c06-818c-4c0894573ed1.png" width="70" height="69" alt="Shaves2u Logo" border="0" style="padding: 20px 0;"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px 0 15px;text-align:center;" valign="middle">
                                                <a href="http://instagram.com/shaves2u" target="_blank" style="text-decoration:none;"> <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/855b433d-07ba-4d45-b4b6-fcc6e7e65146.png" alt="Social Icon - Instagram" style="border: 0;"></a>???
                                                <a href="http://facebook.com/shaves2u" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/ed0713cd-e824-427e-93d9-274e6d7962a9.png" alt="Social Icon - Facebook" style="border: 0"></a>???
                                                <a href="https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ" target="_blank" style="text-decoration:none;">
                                                    <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/8b01525a-2213-48c6-8f8f-59c99397c6dc.png" alt="Social Icon - Youtube" style="border: 0"></a>
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <td style="color:#363636;font-size:11px;text-align:center;padding:36px;line-height:18px;letter-spacing:.05em;">
                                                @else
                                            <td style="color:#ffffff;font-size:11px;text-align:center;padding:0px;line-height:18px;letter-spacing:.05em;">
                                                @endif
                                                {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', config('global.all.email-template.'.strtolower($countrycode).'.footer.company-name')) !!}
                                                <br />
                                                ©All Rights Reserved <?php echo date("Y"); ?>.
                                                <br />
                                                <br />
                                                {!! preg_replace("/(\p{Han}|\p{Hangul})+/u", '<span class="DroidSans">$0</span>', config('global.all.email-template.'.strtolower($countrycode).'.footer.company-address')) !!}
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>
