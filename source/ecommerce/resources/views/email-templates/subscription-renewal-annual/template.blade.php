@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <img src="@lang('email.content.body.subscription-renewal-annual.banner-f')" alt="shaves2u-annual-billing" class="img-responsive">
                            @else
                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/ef72de4c-cd50-43c4-9713-6f01c9663e43.jpg" alt="shaves2u-annual-billing" class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                            <p>
                                                                @lang('email.content.body.subscription-renewal-annual.hello-f', ['displayName' => $moduleData->fullname]) <br>
                                                                <br>
                                                                @lang('email.content.body.subscription-renewal-annual.block-1-f.note-1-f')<br>
                                                                <br>@lang('email.content.body.subscription-renewal-annual.block-1-f.plan-renew-f', ['Product' => $moduleData->productnamet,'periodTime' => $moduleData->duration,'currency' => $moduleData->currencyDisplay,'price' => $moduleData->totalprice])<br>
                                                                <br>@lang('email.content.body.subscription-renewal-annual.block-1-f.note-2-f')<br>
                                                                <br> @lang('email.content.body.subscription-renewal-annual.block-1-f.note-3-f', ['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate])</p>
                                                                <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}user/shave-plan?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_annualbilling"><img src="@lang('email.content.body.subscription-renewal-annual.btn-img-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        @else
                            <td align="center" valign="top" class="center-column-padding" style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">

                                    <tr>
                                        <td style="padding:40px 0;" class="row-padding">
                                            <img class="responsive-image" src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/1b597243-e108-451b-bc96-26ea1ccb22c6.jpg" alt="1b597243-e108-451b-bc96-26ea1ccb22c6.jpg">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0" style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                        Hey *|NAME|*,
                                                        @lang('email.content.body.subscription-renewal-annual.hello')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:40px !important;">
                                                        @lang('email.content.body.subscription-renewal-annual.block-1.note-1')
                                                        <br/>
                                                        <br/>
                                                        @lang('email.content.body.subscription-renewal-annual.block-1.plan-info')
                                                        <br/>
                                                        <br/>
                                                        @lang('email.content.body.subscription-renewal-annual.block-1.note-2')
                                                        <br/>
                                                        <br/>
                                                        @lang('email.content.body.subscription-renewal-annual.block-1.note-3')
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:18px;text-align:left;font-weight:bold;padding-bottom:40px !important;">
                                                        <a href="{{ config('environment.emailUrl') }}user/shave-plan" style="border:0 none;" target="_blank">
                                                            <img src="@lang('email.content.body.subscription-renewal-annual.btn-editplan-m')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection
