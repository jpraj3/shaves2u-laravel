@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table border="0" cellpadding="3px" cellspacing="0" width="80%" style="background-color: #FAFAFA;color: #606060;text-align:left;padding:20px;">
        <caption style="text-align:left;font-weight:bold">Customer Info</caption>
        <tbody>
        <tr>
        <td style="width:200px">Customer name</td>
        <th>{{$moduleData['customerName']}}</th>
        </tr>
    </tbody>
    <tbody>
        <tr>
        <td style="width:200px">Email address</td>
        <th>{{$moduleData['email']}}</th>
        </tr>
    </tbody>
    <tbody>
        <tr>
        <td style="width:200px">Mobile number</td>
        <th>{{$moduleData['mobileNumber']}}</th>
        </tr>
    </tbody>
    <tbody>
        <tr>
        <td style="width:200px">Subscription plan</td>
        <th>{{$moduleData['subscriptionPlan']}}</th>
        </tr>
    </tbody>
    </table>
    @foreach($moduleData['cancellationJourneys'] as $journey)
    <br>
    <div>
    <table border="0" cellpadding="3px" cellspacing="0" width="80%" style="background-color: #FAFAFA;color: #606060;text-align:left;padding:20px;">
        <caption style="text-align:left;font-weight:bold">{{$journey['attemptNumber']}} Attempt</caption>
        <tbody>
        <tr>
            <td style="width:200px">Datetime</td>
            <th>{{$journey['dateTime']}}</th>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td style="width:200px">User action</td>
            <th>{{$journey['userAction']}}</th>
        </tr>
        </tbody>
        @if($journey['cancellationReason'])
        <tbody>
            <tr>
                <td style="width:200px">Reason for cancellation</td>
                <th>{{$journey['cancellationReason']}}</th>
            </tr>
        </tbody>
        @endif
    </table>
    </div>
    @endforeach
    <!-- end of content -->
@endsection
