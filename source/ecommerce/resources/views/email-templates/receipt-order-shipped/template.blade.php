@extends('email-templates.base')
@section('content')
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.order_delivering.banner-f')"
                                                 alt="shaves2u-order-shipped" class="img-responsive">
                                        @else
                                            <img src="@lang('email.content.body.order_delivering.introImg-m')"
                                                 alt="shaves2u-order-shipped" class="img-responsive">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="content-box"
                                                                    style="text-align:left;">
                                                                    <p style="padding-bottom:24px;">
                                                                        @lang('email.content.body.order_delivering.hello-f',['name' => $moduleData->user->firstName])
                                                                        <br><br>
                                                                        <br>
                                                                        @lang('email.content.body.order_delivering.note-1-f',['orderdate' => $moduleData->orderdate])
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_delivering.note-2-f')
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_delivering.note-3-f',['localcarrier' => $moduleData->email->local_carrier])
                                                                        )
                                                                    </p>
                                                                    <h2>{{$moduleData->order->taxInvoiceNo}}</h2>
                                                                    <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/order-plan"><img src="@lang('email.content.body.order_completed.btn-img-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:10px;"></a>
                                                                    <h3>@lang('email.content.body.order_delivering.note-4-f')</h3>
                                                                    <table width="100%" align="left">
                                                                        <tr>
                                                                            <td style="padding:24px 0;">
                                                                                <hr style="height:2px;background-color:#61366e;">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <p style="padding-bottom:24px;">@lang('email.content.body.order_delivering.note-5-f')</p>

                                                                    <table width="600" cellpadding="0" cellspacing="0"
                                                                           border="0"
                                                                           style="padding:24px 0;margin:0 auto;border-bottom:0px solid #D3D3D3;"
                                                                           align="center">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0px solid #000000;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;border:1px solid #dedede;">
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_delivering.table.header-1-f')</td>
                                                                                        <td></td>
                                                                                        <td style="text-align:right;padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_delivering.table.header-2-f')</td>
                                                                                    </tr>

                                                                                    @foreach ($moduleData->orderDetails as $orderDetail)
                                                                                    @php($totalPPrice =  $orderDetail->price * $orderDetail->qty)
                                                                                        <tr style="background-color:#ffffff;">
                                                                                            <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                                                <img
                                                                                                    src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                                                    style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                                                    alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                                            </td>
                                                                                            <td style="padding:20px;">
                                                                                                <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                                                <p>@lang('email.content.body.order_delivering.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                                                <p>@lang('email.content.body.order_delivering.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                                                @if (strtolower($countrycode) != "kr")
                                                                                                    <p>@lang('email.content.body.order_delivering.table.item-3-f') {{$orderDetail->taxCode}}</p> @endif
                                                                                                <p>@lang('email.content.body.order_delivering.table.item-4-f') {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                                            </td>
                                                                                            <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$totalPPrice}}</td>
                                                                                        </tr>
                                                                                    @endforeach

                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top"
                                                                                style="color:#606060;font-size:14px;">
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0 solid #D3D3D3;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="text-align:right;padding:20px 0;border:1px solid #D3D3D3;border-right:0 solid #000000;">
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-5-f')</p>
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-6-f')</p>
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-7-f')</p>
                                                                                            <p style="font-weight:bold;">@lang('email.content.body.order_delivering.table.item-8-f')</p>
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-9-f')</p>
                                                                                        </td>
                                                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border:1px solid #D3D3D3;border-left:0 solid #000000;">
                                                                                            <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                                            <p>{{$moduleData->receipt->currency}}
                                                                                                0.00</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                                            <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="text-align:right;padding:20px 0;">
                                                                                            <p style="font-weight:bold;">@lang('email.content.body.order_delivering.table.item-10-f')</p>
                                                                                        </td>
                                                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top"
                                                                                style="color:#000000;font-size:14px;padding-bottom:20px;">
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       class="body" align="center"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;">
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="padding:20px;width:40%;">
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-11-f')</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                                                                %</p>
                                                                                        </td>
                                                                                        <td style="padding:20px;width:40%;color:#000000;">
                                                                                            <p>@lang('email.content.body.order_delivering.table.item-12-f')</p>
                                                                                            @if($moduleData->country->taxRate <= 0)
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                    @else
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>
                                                                                       @endif
                                                                                        </td>
                                                                                        <td style="padding:20px;width:20%;color:#000000;">
                                                                                            <p>{{$moduleData->country->taxName}}</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" align="left"
                                                                           class="highlight-box">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td align="center" valign="middle">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_ordershipped">
                                                                                @if (strtolower($countrycode) == "sg")
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f-sg')" class="img-responsive" alt="">
                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f-hk')" class="img-responsive" alt="">
                                                                @else
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f')" class="img-responsive" alt="">
                                                                @endif
                                                                            </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    @else
                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                            <p style="padding-bottom:24px;">
                                                @lang('email.content.body.order_delivering.hello',['name' => $moduleData->user->firstName])
                                                <br>
                                                <br>
                                                @lang('email.content.body.order_delivering.note-1-m',['orderdate' => $moduleData->orderdate])
                                                <br>
                                                <br>
                                                @lang('email.content.body.order_delivering.note-3-f',['localcarrier' => $moduleData->email->local_carrier]) <br>
                                            </p>
                                            <h2>{{$moduleData->order->deliveryId}}</h2>
                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/" style="border:0 none;" target="_blank">
                                                <img src="@lang('email.content.body.order_delivering.btn-trackorder-m')"
                                                     border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                                            <h3>@lang('email.content.body.order_delivering.note-4-f')</h3>
                                            <table width="100%" align="left">
                                                <tr>
                                                    <td style="padding:24px 0;">
                                                        <hr style="height:2px;background-color:#000000;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <p style="padding-bottom:24px;">@lang('email.content.body.order_delivering.note-5-f')</p>

                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:24px 0;margin:0 auto;border-bottom:0px solid #D3D3D3;"
                                                   align="center">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0px solid #000000;"
                                                               class="body" align="center">
                                                            <tr style="background-color:#FE6119;border:1px solid #dedede;">
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_delivering.table.header-1-f')</td>
                                                                <td></td>
                                                                <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_delivering.table.header-2-f')</td>
                                                            </tr>
                                                            @foreach ($moduleData->orderDetails as $orderDetail)
                                                            @php($totalPPrice =  $orderDetail->price * $orderDetail->qty)
                                                                <tr style="background-color:#fafafa;">
                                                                    <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                        <img
                                                                            src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                            style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                            alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                    </td>
                                                                    <td style="padding:20px;">
                                                                        <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                        <p>@lang('email.content.body.order_delivering.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                        <p>@lang('email.content.body.order_delivering.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                        <p>@lang('email.content.body.order_delivering.table.item-3-f') {{$orderDetail->taxCode}}</p>
                                                                        <p>@lang('email.content.body.order_delivering.table.item-4-f') {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                    </td>
                                                                    <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$totalPPrice}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="color:#606060;font-size:14px;">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0 solid #D3D3D3;"
                                                               class="body" align="center">
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="text-align:right;padding:20px 0;border:1px solid #D3D3D3;border-right:0 solid #000000;">
                                                                    <p>@lang('email.content.body.order_delivering.table.item-5-f')</p>
                                                                    <p>@lang('email.content.body.order_delivering.table.item-6-f')</p>
                                                                    <p>@lang('email.content.body.order_delivering.table.item-7-f')</p>
                                                                    <p style="font-weight:bold;">@lang('email.content.body.order_delivering.table.item-8-f')</p>
                                                                    <p>@lang('email.content.body.order_delivering.table.item-9-f')</p>
                                                                </td>
                                                                <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border:1px solid #D3D3D3;border-left:0 solid #000000;">
                                                                    <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                    <p>{{$moduleData->receipt->currency}} 0.00</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                    <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="text-align:right;padding:20px 0;">
                                                                    <p style="font-weight:bold;">@lang('email.content.body.order_delivering.table.item-10-f')</p>
                                                                </td>
                                                                <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"
                                                        style="color:#000000;font-size:14px;padding-bottom:20px;">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               class="body" align="center"
                                                               style="padding:0;margin:0 auto;border:1px solid #D3D3D3;">
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="padding:20px;width:40%;">
                                                                    <p>@lang('email.content.body.order_delivering.table.item-11-f')</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                                        %</p>
                                                                </td>
                                                                <td style="padding:20px;width:40%;color:#000000;">
                                                                    <p>@lang('email.content.body.order_delivering.table.item-12-f')</p>
                                                                    @if($moduleData->country->taxRate <= 0)
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                    @else
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>
                                                                    @endif
                                                                </td>
                                                                <td style="padding:20px;width:20%;color:#000000;">
                                                                    <p>{{$moduleData->country->taxName}}</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table width="100%" align="left" class="highlight-box">
                                                <tbody>
                                                <tr>
                                                    <td align="center" valign="middle">
                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_ordershipped">
                                                            <img
                                                                src="@lang('email.content.common.referral.referralBox')"
                                                                class="img-responsive" alt=""></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
@endsection
