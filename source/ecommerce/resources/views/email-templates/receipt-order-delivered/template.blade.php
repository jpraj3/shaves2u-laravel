@extends('email-templates.base')
@section('content')
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.order_completed.banner-f')"
                                                 alt="shaves2u-order-delivered" class="img-responsive">
                                        @else
                                            <img src="@lang('email.content.body.order_completed.introImg-m')"
                                                 alt="shaves2u-order-delivered" class="img-responsive">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <td class="column">
                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               align="center">
                                                            <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" class="content-box"
                                                                    style="text-align:left;">
                                                                    <p style="padding-bottom:24px;" class="email-a">
                                                                        @lang('email.content.body.order_completed.hello-f',['name' => $moduleData->user->firstName])
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_completed.note-1-f')
                                                                        <br>
                                                                        @lang('email.content.body.order_completed.note-2-f')
                                                                        <br>
                                                                        <br>
                                                                        @lang('email.content.body.order_completed.note-3-f')
                                                                    </p>
                                                                    <table width="600" cellpadding="0" cellspacing="0"
                                                                           border="0"
                                                                           style="padding:0 0 24px 0;margin:0 auto;border-bottom:0px solid #D3D3D3;"
                                                                           align="center">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td valign="top"
                                                                                style="color:#606060;font-size:14px;">
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;margin-top:20px !important;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;border:1px solid #D3D3D3;border-bottom:0 solid #000000;">
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.note-4-f')</td>
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.note-5-f')</td>
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.note-6-f')</td>
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.note-7-f')</td>
                                                                                    </tr>
                                                                                    <tr style="background-color:#ffffff;border:1px solid #D3D3D3;border-bottom:0 solid #000000;">
                                                                                        <td style="width:25%;height:100px;">{{$moduleData->orderNo}}</td>
                                                                                        <td style="width:25%;">{{$moduleData->order->taxInvoiceNo}}</td>
                                                                                        <td style="width:25%;">{{$moduleData->orderdate}}</td>
                                                                                        <td style="width:25%;">{{$moduleData->shippingdate}}</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="background-color:#ffffff;border:1px solid #D3D3D3;border-top:0 solid #000000;">
                                                                            <td style="text-align:center !important;">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/order-plan" target="_blank"><img src="@lang('email.content.body.order_completed.btn-img-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px; padding-bottom:20px;"></a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height:48px;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0px solid #000000;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;border:1px solid #dedede;">
                                                                                        <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.table.header-1-f')</td>
                                                                                        <td></td>
                                                                                        <td style="text-align:right;padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_completed.table.header-2-f')</td>
                                                                                    </tr>
                                                                                    @foreach ($moduleData->orderDetails as $orderDetail)
                                                                                        <tr style="background-color:#ffffff;">
                                                                                            <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                                                <img
                                                                                                    src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                                                    style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                                                    alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                                            </td>
                                                                                            <td style="padding:20px;">
                                                                                                <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                                                <p>@lang('email.content.body.order_completed.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                                                <p>@lang('email.content.body.order_completed.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                                                @if (strtolower($countrycode) != "kr")
                                                                                                    <p>@lang('email.content.body.order_completed.table.item-3-f') {{$orderDetail->taxCode}}</p> @endif
                                                                                                <p>@lang('email.content.body.order_completed.table.item-4-f') {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                                            </td>
                                                                                            <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$orderDetail->totalitemprice}}</td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top"
                                                                                style="color:#606060;font-size:14px;">
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;border-bottom:0 solid #D3D3D3;"
                                                                                       class="body" align="center">
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="text-align:right;padding:20px 0;border:1px solid #D3D3D3;border-right:0 solid #000000;">
                                                                                            <p>@lang('email.content.body.order_completed.table.item-5-f')</p>
                                                                                            <p>@lang('email.content.body.order_completed.table.item-6-f')</p>
                                                                                            <p>@lang('email.content.body.order_completed.table.item-7-f')</p>
                                                                                            <p style="font-weight:bold;">@lang('email.content.body.order_completed.table.item-8-f')</p>
                                                                                            <p>@lang('email.content.body.order_completed.table.item-9-f')</p>
                                                                                        </td>
                                                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border:1px solid #D3D3D3;border-left:0 solid #000000;">
                                                                                            <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                                            <p>{{$moduleData->receipt->currency}}
                                                                                                0.00</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                                            <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="text-align:right;padding:20px 0;">
                                                                                            <p style="font-weight:bold;">@lang('email.content.body.order_completed.table.item-10-f')</p>
                                                                                        </td>
                                                                                        <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top"
                                                                                style="color:#000000;font-size:14px;padding-bottom:20px;">
                                                                                <table width="100%" cellpadding="0"
                                                                                       cellspacing="0" border="0"
                                                                                       class="body" align="center"
                                                                                       style="padding:0;margin:0 auto;border:1px solid #D3D3D3;">
                                                                                    <tr style="background-color:#ffffff;">
                                                                                        <td style="padding:20px;width:40%;">
                                                                                            <p>@lang('email.content.body.order_completed.table.item-11-f')</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                                                                %</p>
                                                                                        </td>
                                                                                        <td style="padding:20px;width:40%;color:#000000;">
                                                                                            <p>@lang('email.content.body.order_completed.table.item-12-f')</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>
                                                                                        </td>
                                                                                        <td style="padding:20px;width:20%;color:#000000;">
                                                                                            <p>@lang('email.content.body.order_completed.table.item-13-f')</p>
                                                                                            <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" align="left"
                                                                           class="highlight-box">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td align="center" valign="middle">
                                                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_orderdelivered">
                                                                                @if (strtolower($countrycode) == "sg")
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f-sg')" class="img-responsive" alt="">
                                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f-hk')" class="img-responsive" alt="">
                                                                @else
                                                                <img src="@lang('email.content.common.referral.referralBox-i-f')" class="img-responsive" alt="">
                                                                @endif
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    @else
                                        <td align="center" valign="top" class="center-column-padding"
                                            style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" bgcolor="#ffffff" align="center">
                                                <tr>
                                                    <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;border-bottom:1px solid #000000;margin-bottom:20px !important;"
                                                               class="body" align="center">
                                                            <tr>
                                                                <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.order_completed.hello-f',['name' => $moduleData->user->firstName])
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="email-a" style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                    @lang('email.content.body.order_completed.note-1-f')@lang('email.content.body.order_completed.note-2-m')
                                                                    <br/>
                                                                    <br/>
                                                                    @lang('email.content.body.order_completed.note-3-f')
                                                                    <a href="{{$moduleData->email->link_mailto}}"
                                                                       target="_top">{{$moduleData->email->link_mailto_title}}</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="color:#606060;font-size:14px;">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;margin-bottom:40px !important;"
                                                               align="center">
                                                            <tr style="background-color:#fe5000;text-align:center;">
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.note-4-f')</td>
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.note-5-f')</td>
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.note-6-f')</td>
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.note-7-f')</td>
                                                            </tr>
                                                            <tr style="background-color:#fafafa;text-align:center;">
                                                                <td style="padding:10px;font-size:16px;">{{$moduleData->orderNo}}</td>
                                                                <td style="padding:10px;font-size:16px;">{{$moduleData->order->taxInvoiceNo}}</td>
                                                                <td style="padding:10px;font-size:16px;">{{$moduleData->orderdate}}</td>
                                                                <td style="padding:10px;font-size:16px;">{{$moduleData->shippingdate}}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:18px;text-align:center;font-weight:bold;">
                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/" style="border:0 none;"
                                                           target="_blank">
                                                            <img
                                                                src="@lang('email.content.body.order_completed.btn-vieworder-m')"
                                                                border="0"
                                                                alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="color:#606060;font-size:14px;">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;border-bottom:1px solid #D3D3D3;margin-top:40px !important;"
                                                               align="center">
                                                            <tr style="background-color:#fe5000;">
                                                                <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.table.header-1-f')</td>
                                                                <td></td>
                                                                <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.order_completed.table.header-2-f')</td>
                                                            </tr>
                                                            @foreach ($moduleData->orderDetails as $orderDetail)
                                                                <tr style="background-color:#fafafa;">
                                                                    <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                        <img
                                                                            src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                            style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                            alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                    </td>
                                                                    <td style="padding:20px;">
                                                                        <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                        <p>@lang('email.content.body.order_completed.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                        <p>@lang('email.content.body.order_completed.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                        <p>@lang('email.content.body.order_completed.table.item-3-f') {{$orderDetail->taxCode}}</p>
                                                                        <p>@lang('email.content.body.order_completed.table.item-4-f') {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                    </td>
                                                                    <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$orderDetail->totalitemprice}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="color:#606060;font-size:14px;">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               style="border-bottom:1px solid #D3D3D3;" align="center">
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="text-align:right;padding:20px 0;border-bottom:1px solid #D3D3D3;">
                                                                    <p>@lang('email.content.body.order_completed.table.item-5-f')</p>
                                                                    <p>@lang('email.content.body.order_completed.table.item-6-f')</p>
                                                                    <p>@lang('email.content.body.order_completed.table.item-7-f')</p>
                                                                    <p style="font-weight:bold;">@lang('email.content.body.order_completed.table.item-8-f')</p>
                                                                    <p>@lang('email.content.body.order_completed.table.item-9-f')</p>
                                                                </td>
                                                                <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;border-bottom:1px solid #D3D3D3;">
                                                                    <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->originPrice}}</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->discountAmount}}</p>
                                                                    <p>{{$moduleData->receipt->currency}} 0.00</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscount}}</p>
                                                                    <p>{{$moduleData->receipt->currency}} {{$moduleData->receipt->cashRebate}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="text-align:right;padding:20px 0;">
                                                                    <p style="font-weight:bold;">@lang('email.content.body.order_completed.table.item-10-f')</p>
                                                                </td>
                                                                <td style="text-align:right;padding-right:10px;width:100px;padding:20px 10px;">
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceAfterDiscountAndRebate}}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"
                                                        style="color:#606060;font-size:14px;padding-bottom:20px;">
                                                        <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                               align="center">
                                                            <tr style="background-color:#fafafa;">
                                                                <td style="padding:20px;width:40%;">
                                                                    <p>@lang('email.content.body.order_completed.table.item-11-f')</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->country->taxRate}}
                                                                        %</p>

                                                                </td>
                                                                <td style="padding:20px;width:40%;">
                                                                    <p>@lang('email.content.body.order_completed.table.item-12-f')</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->priceExcludeTax}}</p>

                                                                </td>
                                                                <td style="padding:20px;width:20%;">
                                                                    <p>@lang('email.content.body.order_completed.table.item-13-f')</p>
                                                                    <p style="font-weight:bold;">{{$moduleData->receipt->currency}} {{$moduleData->receipt->taxPrice}}</p>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:40px 0;" class="row-padding">
                                                        <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                                               @if (strtolower($countrycode) == "sg")
                                            <img src="@lang('email.content.common.referral.referralBox-sg')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                            <img src="@lang('email.content.common.referral.referralBox-hk')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @else
                                            <img src="@lang('email.content.common.referral.referralBox')" alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg" class="responsive-image">
                                            @endif
                                                            </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
@endsection
