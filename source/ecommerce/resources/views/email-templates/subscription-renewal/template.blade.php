@extends('email-templates.base')

@section('content')
    <!-- content -->
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                <img src="@lang('email.content.body.subscription-renewal.banner-f')"
                                     alt="shaves2u-subscription-renew" class="img-responsive">
                            @else
                                <img
                                    src="@lang('email.content.body.subscription-renewal.banner-m')"
                                    alt="shaves2u-subscription-renew" class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            <td class="column">
                                <table width="100%">
                                    <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td align="center" valign="top" class="content-box"
                                                        style="text-align:left;">
                                                        <p>
                                                            @lang('email.content.body.subscription-renewal.hello-f', ['displayName' => $moduleData->fullname])
                                                            <br> <br>
                                                            <br>
                                                            @lang('email.content.body.subscription-renewal.block-1-f.note-1-f')
                                                        </p>
                                                        <br>
                                                        <?php 
                                                        $productname = "";
                                                        $countp = 0 ; 
                                                        foreach($moduleData->planDetails as $planDetail){
                                                            if($countp == 0){
                                                                $productname = $planDetail->pqty." x ".$planDetail->pname."(".$planDetail->psku.")";
                                                            }else{
                                                                $productname = ",".$planDetail->pqty." x ".$planDetail->pname."(".$planDetail->psku.")";
                                                            }
                                                        }
                                                        ?>
                                                        <p>@lang('email.content.body.subscription-renewal.block-1-f.plan-renew-f', ['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate,'currency' =>$country['currencyDisplay'],'price' => $moduleData->subscription->price, 'periodTime' => $moduleData->plans->duration,'Product' => $productname])</p>
                                                        <table width="100%" align="left" class="highlight-box">
                                                            <tbody>
                                                            <tr>
                                                                <td align="center" valign="middle">
                                                                <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_refer_renewal">
                                                                        @if (strtolower($countrycode) == "sg")
                                                                            <img
                                                                                src="@lang('email.content.common.referral.referralBox-f-sg')"
                                                                                class="img-responsive" alt=""
                                                                                style="padding-bottom: 48px;">
                                                                        @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                                            <img
                                                                                src="@lang('email.content.common.referral.referralBox-f-hk')"
                                                                                class="img-responsive" alt=""
                                                                                style="padding-bottom: 48px;">
                                                                        @else
                                                                            <img
                                                                                src="@lang('email.content.common.referral.referralBox-f')"
                                                                                class="img-responsive" alt=""
                                                                                style="padding-bottom: 48px;">
                                                                        @endif
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        @else
                            <td align="center" valign="top" class="center-column-padding"
                                style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0"
                                       style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                    <tr>
                                        <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                            <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                   style="padding:0;margin:0 auto;" class="body" align="center">
                                                <tr>
                                                    <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.subscription-renewal.hello', ['displayName' => $moduleData->fullname])
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                        @lang('email.content.body.subscription-renewal.block-1.note-1')
                                                        <br/>
                                                        <br/>
                                                        @if(strtolower($countrycode) !== "kr")
                                                            @lang('email.content.body.subscription-renewal.block-1.next-deliver',['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate])
                                                            @foreach($moduleData->planDetails as $planDetail)
                                                                {{$planDetail->pqty}} x {{$planDetail->pname}}
                                                                ({{$planDetail->psku}})
                                                            @endforeach
                                                            @lang('email.content.body.subscription-renewal.block-1.cycle-info',['currency' =>$country['currencyDisplay'],'pricePerCharge' => $moduleData->subscription->price, 'duration' => $moduleData->plans->duration])
                                                        @else
                                                            @lang('email.content.body.subscription-renewal.block-1.plan-renew-1',['nextDeliverDate' =>$moduleData->subscription->nextDeliverDate,'duration' => $moduleData->plans->duration, 'currency' =>$country['currencyDisplay'],'pricePerCharge' => $moduleData->subscription->price,])
                                                            @foreach($moduleData->planDetails as $planDetail)
                                                                {{$planDetail->pqty}} x {{$planDetail->pname}}
                                                                ({{$planDetail->psku}})
                                                            @endforeach
                                                            @lang('email.content.body.subscription-renewal.block-1.plan-renew-2')
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:20px 0;" class="row-padding">
                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals">
                                                @if (strtolower($countrycode) == "sg")
                                                    <img src="@lang('email.content.common.referral.referralBox-sg')"
                                                         alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg"
                                                         class="responsive-image">
                                                @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                                                    <img src="@lang('email.content.common.referral.referralBox-hk')"
                                                         alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg"
                                                         class="responsive-image">
                                                @else
                                                    <img src="@lang('email.content.common.referral.referralBox')"
                                                         alt="aabf58f9-28d6-408f-9033-3bab98c7cd7a.jpg"
                                                         class="responsive-image">
                                                @endif
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection
