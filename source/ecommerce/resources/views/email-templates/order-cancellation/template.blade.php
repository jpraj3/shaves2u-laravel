@extends('email-templates.base')
@section('content')
    <table class="section" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td class="column">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                            <img src="@lang('email.content.body.order_cancellation.banner-f')" alt="shaves2u-order-cancellation" class="img-responsive">
                                            @else
                                            <img
                                                    src="@lang('email.content.body.order_cancellation.note-2-f')"
                                                    alt="shaves2u-order-cancellation" class="img-responsive">
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                                        <td align="center">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                   align="center">
                                                <tbody>
                                                <tr>
                                                    <td align="center" valign="top" class="content-box"
                                                        style="text-align:left;">
                                                        <p style="padding-bottom:24px;">
                                                            @lang('email.content.body.order_cancellation.hello-f',['name' => $moduleData->fullname])
                                                            <br>
                                                            <br>
                                                            @lang('email.content.body.order_cancellation.note-1-f',['orderNo' => $moduleData->orderNo])
                                                            <br>
                                                        </p>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               align="center">
                                                            <tr>
                                                                <td>
                                                                    <strong>@lang('email.content.body.order_cancellation.note-2-f')</strong>
                                                                </td>
                                                                <td>
                                                                    <strong>@lang('email.content.body.order_cancellation.note-3-f')</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>{{$moduleData->orderNo}}</td>
                                                                <td>{{$moduleData->orderdate}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="height:48px !important;"> </td>
                                                            </tr>
                                                        </table>
                                                        <table width="600" cellpadding="0" cellspacing="0" border="0"
                                                               style="padding:0;margin:0 auto;border:1px solid #D3D3D3;"
                                                               class="body" align="center">
                                                            <tr style="background-color:#ffffff;border-bottom:1px solid #ededed;">
                                                                <td style="padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_cancellation.table.header-1-f')</td>
                                                                <td></td>
                                                                <td style="text-align:right;padding:10px;font-size:16px;color:#000000;">@lang('email.content.body.order_cancellation.table.header-2-f')</td>
                                                            </tr>
                                                            @foreach ($moduleData->orderDetails as $orderDetail)
                                                                <tr style="background-color:#ffffff;">
                                                                    <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                        <img
                                                                            src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                            style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                            alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png">
                                                                    </td>
                                                                    <td style="padding:20px;">
                                                                        <p style="font-weight:bold;color:#000;">{{$orderDetail->details->translatedName}}</p>
                                                                        <p>@lang('email.content.body.order_cancellation.table.item-1-f') {{$orderDetail->details->sku}}</p>
                                                                        <p>@lang('email.content.body.order_cancellation.table.item-2-f') {{$orderDetail->qty}}</p>
                                                                        @if (strtolower($countrycode) != "kr") <p>@lang('email.content.body.order_cancellation.table.item-3-f') {{$orderDetail->taxCode}}</p> @endif
                                                                        <p>@lang('email.content.body.order_cancellation.table.item-4-f') {{$orderDetail->currency}} {{$orderDetail->price}}</p>
                                                                    </td>
                                                                    <td style="text-align:center;padding:20px 10px 20px 0;">{{$orderDetail->currency}} {{$orderDetail->price}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    @else
                                        <table width="600" cellpadding="0" cellspacing="0" border="0"
                                               style="padding:0;margin:0 auto;" class="body"
                                               bgcolor="#ffffff" align="center">
                                            <tr>
                                                <td style="padding:40px 0;" class="row-padding">
                                                    <img class="responsive-image"
                                                         src="@lang('email.content.body.subscription-cancellation.journey-one.trial.introImg-m')"
                                                         width="600" height="135" border="0"
                                                         alt="10cf95f8-b0a4-408c-8333-b8650d3b38f9.jpg">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0;margin:0;text-align:left;" class="row-padding">
                                                    <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                           style="padding:0;margin:0 auto;"
                                                           align="center">
                                                        <tr>
                                                            <td style="color:#000000;font-size:16px;font-weight:bold;text-align:left;line-height:23px;padding-bottom:20px !important;">
                                                                @lang('email.content.body.subscription-cancellation.journey-one.trial.hello')
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-1')
                                                                <br/>
                                                                <br/>
                                                                @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-2')
                                                                <br/>
                                                                <br/>
                                                                @lang('email.content.body.subscription-cancellation.journey-one.trial.block-1.note-3')
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="color:#606060;font-size:14px;">
                                                    <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                           style="padding:0;margin:0 auto;"
                                                           class="steps-table" align="center">
                                                        <tr style="background-color:#fe5000;">
                                                            <td style="padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.item')</td>
                                                            <td></td>
                                                            <td style="text-align:right;padding:10px;font-size:16px;color:#ffffff;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.total')</td>
                                                        </tr>
                                                        <tr style="background-color:#fafafa;">
                                                            <td style="width:20%;padding:20px 0 20px 10px;min-width:70px;">
                                                                <img
                                                                    src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/674918f0-1cba-422f-8976-5d322441e993.jpg"
                                                                    style="border: 1px solid #41352F;padding: 12% 24%;width: 30%;max-width:104px;"
                                                                    alt="f5ac28ed-2627-4ddf-a4e9-819b2b1ce009.png"/>
                                                            </td>
                                                            <td style="padding:20px;">
                                                                <p style="font-weight:bold;color:#000;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.item.product.name')</p>
                                                                <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.sku')</p>
                                                                <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.quantity')</p>
                                                                <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.tax-code')</p>
                                                                <p>@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.product.unit-price')</p>
                                                            </td>
                                                            <td style="text-align:center;padding:20px 10px 20px 0;">@lang('email.content.body.subscription-cancellation.journey-one.trial.order-summary.item.product.total')</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0;margin:0;text-align:left;padding-top:20px !important;"
                                                    class="row-padding">
                                                    <table width="500" cellpadding="0" cellspacing="0" border="0"
                                                           style="padding:0;margin:0 auto;"
                                                           class="body" align="center">
                                                        <tr>
                                                            <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding-bottom:20px !important;">
                                                                @lang('email.content.body.subscription-cancellation.journey-one.trial.enquiry')
                                                                <a
                                                                    href="@lang('email.content.body.subscription-cancellation.journey-one.trial.mailto-link')"
                                                                    style="color:#fe5000;"
                                                                    target="_top">@lang('email.content.body.subscription-cancellation.journey-one.trial.mailto-name')
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
@endsection
