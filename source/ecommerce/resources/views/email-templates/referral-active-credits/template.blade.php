@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                            @if (strtolower($countrycode) == "sg")
                            <img src="@lang('email.content.body.referral-active-credits.banner-f-sg')" alt="shaves2u-referral-4" class="img-responsive">
                            @elseif(strtolower($countrycode) == "hk" && strtolower($lang) == "en")
                            <img src="@lang('email.content.body.referral-active-credits.banner-f-hk')" alt="shaves2u-referral-4" class="img-responsive">
                            @else
                            <img src="@lang('email.content.body.referral-active-credits.banner-f')" alt="shaves2u-referral-4" class="img-responsive">
                            @endif
                            @else
                                <img src="@lang('email.content.body.referral-active-credits.introImg-m')" alt="shaves2u"
                                     class="img-responsive">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if (isset($moduleData->identifiers) && ($moduleData->identifiers->isFemale == true))
                        <td class="column">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="content-box" style="text-align:left;">
                                                            <p>
                                                                @lang('email.content.body.referral-active-credits.hello-f',['name' => $moduleData->fullname])<br>
                                                                <br>@lang('email.content.body.referral-active-credits.block-1-f.note-1-f',['friendname' => $moduleData->reffullname])<br>
                                                                <br>@lang('email.content.body.referral-active-credits.block-1-f.note-2-f',['currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])<br>
                                                                <br>@lang('email.content.body.referral-active-credits.block-1-f.note-3-f')</p>

                                                           <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals?utm_source=mandrill&amp;utm_medium=email&amp;utm_campaign=my_female_referral_4"><img src="@lang('email.content.body.referral-active-credits.refer-friends-f')" border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png" style="padding-top:20px;"></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        @else
                            <td align="center" valign="top" class="center-column-padding"
                                style="font-family:Arial, Helvetica, sans-serif;color:#ffffff;font-size:14px;text-align:left;">
                                <table width="600" cellpadding="0" cellspacing="0" border="0"
                                       style="padding:0;margin:0 auto;" class="body" bgcolor="#ffffff" align="center">
                                    <tr>
                                        <td style="font-family:Montserrat, sans-serif;color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-active-credits.hello',['name' => $moduleData->fullname])
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#000000;font-size:16px;text-align:left;font-weight:normal;line-height:23px;padding:20px !important;">
                                            @lang('email.content.body.referral-active-credits.block-1.note-1',['friendname' => $moduleData->reffullname])
                                            <br/>
                                            <br/>
                                            @lang('email.content.body.referral-active-credits.block-1.note-2',['currencyDisplay' => $moduleData->referral->currencyDisplay , 'value' => $moduleData->referral->value])
                                            <br/>
                                            <br/>
                                            @lang('email.content.body.referral-active-credits.block-1.note-3')
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:700;color:#41352F;font-size:18px;text-align:center;padding:20px 0 40px;">
                                            <a href="{{ config('environment.emailUrl') }}{{strtolower($lang)}}-{{strtolower($countrycode)}}/user/referrals" style="border:0 none;" target="_blank">
                                                <img
                                                    src="@lang('email.content.body.referral-active-credits.btn-refer-more')"
                                                    border="0" alt="11eab5ca-3b57-4465-b190-0b359274ed40.png"></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!-- end of content -->
@endsection

