@extends('email-templates.base')

@section('content')
<!-- content -->
<table class="section" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td class="column">
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/21eda7da-98c0-426d-b02d-ea5a9b7d4114.jpg" alt="shaves2u" class="img-responsive">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="content-box">
                                            <p>
                                                @lang('email.content.body.welcome.hello')
                                                <br />
                                                <br />
                                                @lang('email.content.body.welcome.intro')
                                            </p>
                                            <img src="https://gallery.mailchimp.com/a1f5e35b929b6d70bbdff908e/images/a5891a43-2677-481e-bc74-60bb21026530.jpg" alt="shaves2u" style="padding: 24px 0;" class="img-responsive">
                                            <!-- <p>We hope you'll enjoy our products as much as we did crafting them for you.</p> -->
                                            <h2>@lang('email.content.body.welcome.verifyTitle')</h2>
                                            <p>@lang('email.content.body.welcome.verifyBody')</p>
                                            <span class="btn"><a href="https://www.google.com/" target="_blank">@lang('email.content.body.welcome.verifyBtn')</a></span>
                                            <table width="100%" align="left" class="highlight-box">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" valign="middle" class="highlight-box-content">
                                                            <h2>>@lang('email.content.body.welcome.referralTitle')</h2>
                                                            <p>@lang('email.content.body.welcome.referralBody')</p>
                                                            <span class="btn"><a href="https://www.google.com/" target="_blank">@lang('email.content.body.welcome.referralBtn')</a></span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<!-- end of content -->
@endsection