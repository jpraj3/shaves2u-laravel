@extends('layouts.app') @section('content')

    <link rel="stylesheet" href="{{ asset('css/product/product.css') }}">
    @php($m_h = null)
    @php($m_h_sku = null)
    @if(Session::has('country_handles'))
    @php($m_h = Session::get('country_handles'))
    @php($m_h_sku = $m_h->sku)
    @endif
    <!-- Desktop START -->
    <div style="background-color:#ECECEC">
        <div class="container">
            <section id="product_banner" class="d-none d-lg-block">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 text-left mt-5 pt-5" style="padding-left: 13%">
                            <h1 class="MuliExtraBold product-h-title">@lang('website_contents.products.product.starter_kit')</h2>
                            <h3 class="MuliSemiBold product-h-sub color-orange pb-0">@lang('website_contents.products.product.better_shaves')</h3>
                            <p class="MuliSemiBold p-18 product-h-sub">@lang('website_contents.products.product.start_plan', ['currency' => $currentCountry->currencyDisplay,'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso))])</p>

                            <a href="{{ route('locale.region.shave-plans.trial-plan', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                            class="btn-start btn-custom MuliBold m-0 text-uppercase" style="font-size:2rem;">@lang('website_contents.global.header.get_started')</a>
                        </div>
                        <div class="col-lg-6">
                            <img src="{{URL::asset('/images/common/product_banner.png')}}" class="img-fluid"/>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div style="background: linear-gradient(to bottom, white 2%, #ECECEC 95%, white 3%);">
        <div class="container">
            {{-- <section id="product_handle" class="d-none d-lg-block">
                <div class="col-md-12 d-none d-md-block">
                    <div class="row mt-5 pt-5">
                        <div class="col-md-6">
                            <img src="{{URL::asset('/images/common/product-swivel@2x.png')}}" class="img-fluid"/>
                        </div>
                        <div class="col-md-6 word-padd" style="text-align:center;padding-left:5%;">
                            <h2 class="MuliExtraBold product-h-title">
                                @lang('website_contents.products.product.swivel_handle')
                            </h2>
                            <p class="MuliPlain fs-18 product-h-sub mb-5p">
                                @lang('website_contents.products.product.swivel_handle_desc')
                            </p>
                            <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                               class="btn-start btn-product-custom MuliBold" style="font-size:2rem;">@lang('website_contents.products.content.buy_now')</a>
                            <p class="pointer" style="color:#000000; text-decoration: underline;"
                               onclick="handleProductPage();">@lang('website_contents.products.content.learn_more')</p>
                        </div>
                    </div>
                </div>
            </section> --}}
            <section id="product_blade" class="d-none d-lg-block">
                <div id="product_carousel" class="carousel slide" data-ride="carousel">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="product-blade-info">
                                    <h2 class="MuliExtraBold product-h-title">
                                        @lang('website_contents.products.product.blade_packs')
                                    </h2>
                                    <p class="MuliPlain fs-18 product-h-sub mb-5p">
                                        @lang('website_contents.products.product.blade_packs_desc')
                                    </p>
                                    <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                                       class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                                    <p class="pointer" style="text-decoration: underline;"
                                       onclick="bladeProductPage();">
                                        @lang('website_contents.products.content.learn_more')</p>
                                    <ol class="carousel-indicators" style="top:225px;">
                                        <li data-target="#product_carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#product_carousel" data-slide-to="1"></li>
                                        <li data-target="#product_carousel" data-slide-to="2"></li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="col-md-12 product-other-banner product-other-banner2"
                                             style="background-image: url('{{asset('/images/common/product-3blade@2x.png')}}');">
                                        </div>
                                    <!--img alt="Blade 1" class="img-fluid d-block mx-auto" src="{{asset('/images/common/s2u-products-desk-3blades.jpg')}}"-->
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col-md-12 product-other-banner product-other-banner3"
                                             style="background-image: url('{{asset('/images/common/product-5blade@2x.png')}}');">
                                        </div>
                                    <!--img alt="Blade 2" class="img-fluid d-block mx-auto" src="{{asset('/images/common/product-5blade@2x.png')}}"-->
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col-md-12 product-other-banner product-other-banner4"
                                             style="background-image: url('{{asset('/images/common/product-6blade@2x.png')}}');">
                                        </div>
                                    <!--img alt="Blade 3" class="img-fluid d-block mx-auto" src="{{asset('/images/common/product-5blade@2x.png')}}"-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="product_cream" class="d-none d-lg-block">
                <div class="col-md-12 d-none d-md-block">
                    <div class="col-md-12 d-none d-md-block">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{URL::asset('/images/common/ShaveCream@2x.png')}}" class="img-fluid"/>
                            </div>
                            <div class="col-md-6 text-center">
                                <div style="margin-top: 50%">
                                    <h2 class="MuliExtraBold product-h-title">
                                        @lang('website_contents.products.product.shave_cream')
                                    </h2>
                                    <p class="MuliPlain fs-18 product-h-sub mb-5p">
                                        @lang('website_contents.products.product.shave_cream_desc')
                                    </p>
                                    <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                                       class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                                    <p class="pointer" style="text-decoration: underline;"
                                       onclick="screamProductPage();">
                                        @lang('website_contents.products.content.learn_more')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="product_after_cream" class="d-none d-lg-block pb-5">
                <div class="col-md-12 d-none d-md-block">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <div style="margin-top: 50%">
                                <h2 class="MuliExtraBold product-h-title">
                                    @lang('website_contents.products.product.after_shave_cream')
                                </h2>
                                <p class="MuliPlain fs-18 product-h-sub mb-5p">
                                    @lang('website_contents.products.product.after_shave_cream_desc')
                                </p>
                                <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                                   class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                                <p class="pointer" style="color:#000000; text-decoration: underline;"
                                   onclick="ascreamProductPage();">@lang('website_contents.products.content.learn_more')</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="{{URL::asset('/images/common/Aftershave@2x.png')}}" class="img-fluid"/>
                        </div>
                    </div>
                </div>
            </section>
            <section id="product_awesome_kit" class="d-none d-lg-block pb-5">
                <div class="col-md-12 d-none d-md-block">
                    <div class="row">
                        <div class="col-md-6">
                            @if($m_h_sku == 'H3')
                            <img src="{{URL::asset('/images/common/product-ask.png')}}" class="img-fluid"/>
                            @elseif($m_h_sku == 'H1')
                            <img src="{{URL::asset('/images/common/premium/product/product-ask.png')}}" class="img-fluid"/>
                            @else
                            <img src="{{URL::asset('/images/common/product-ask.png')}}" class="img-fluid"/>
                            @endif
                        </div>
                        <div class="col-md-6 text-center">
                            <div style="margin-top: 30%">
                                <h2 class="MuliExtraBold product-h-title">
                                    @lang('website_contents.products.product.ask')
                                </h2>
                                <p class="MuliPlain fs-18 product-h-sub mb-5p">
                                    @lang('website_contents.products.product.ask_desc')
                                </p>
                                <button class="btn-start btn-product-custom MuliBold" onclick="askProductPage();">
                                    @lang('website_contents.products.content.buy_now')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <section id="product_sleeve_bag" class="d-none d-lg-block">
        <div class="col-12 margB5rem d-none d-md-block">
            <div class="row justify-content-md-center">
                <div class="col-md-4 product-sleeve-bag"  >
                    <div class="product-sleeve-bag-border" style="flex: 1; padding: 16px; height: 100%">
                        <div class="col-12">
                            <img src="{{ asset('/images/common/product-rubbersleeve.png') }}"
                                 class="img-fluid product-sleeve-bag-img"/>
                        </div>
                        <h2 class="MuliExtraBold product-h-title product-sleeve-bag-title">
                            @lang('website_contents.products.product.razor_protector')
                        </h2>
                        <p class="MuliPlain fs-18 pl-5 pr-5 product-sleeve-bag-p">
                            @lang('website_contents.products.product.razor_protector_desc')
                        </p>
                        <button class="btn-start btn-product-custom MuliBold" onclick="pAddonsProductPage();">@lang('website_contents.products.content.buy_now')</button>
                        <p class="MuliPlain fs-18 product-sleeve-bag-p2">
                            @lang('website_contents.products.content.available_existing_mobile')
                        </p>
                    </div>
                </div>
                <div class="col-md-4 product-sleeve-bag">
                    <div class="product-sleeve-bag-border" style="flex: 1; padding: 16px; height: 100%">
                        <div class="col-12">
                            <img src="{{ asset('/images/common/product-toiletrybag.png') }}"
                                 class="img-fluid product-sleeve-bag-img"/>
                        </div>
                        <h2 class="MuliExtraBold product-h-title product-sleeve-bag-title">
                            @lang('website_contents.products.product.toiletry_bag')
                        </h2>
                        <p class="MuliPlain fs-18 pl-5 pr-5 pb-4 product-sleeve-bag-p">
                            @lang('website_contents.products.product.toiletry_bag_desc')
                        </p>
                        <button class="btn-start btn-product-custom MuliBold" onclick="pAddonsProductPage();">@lang('website_contents.products.content.buy_now')</button>
                        <p class="MuliPlain fs-18 product-sleeve-bag-p2">
                            @lang('website_contents.products.content.available_existing_mobile')
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Desktop END -->

    <!-- Mobile START -->

    <div class="pb-5">
        <section id="mobile_product_banner-text" class="d-block d-lg-none pt-5">
            <div class="col-12" style="text-align:center;">
                <h3 class="MuliExtraBold product-h">
                    @lang('website_contents.products.product.starter_kit')
                </h3>
                <h3 class="MuliExtraBold product-h color-orange">
                    @lang('website_contents.products.product.better_shaves')
                </h3>
                <p class="MuliPlain mb-3" style="font-size:1.2rem;">@lang('website_contents.products.product.start_plan', ['currency' => 'RM','price' => '6.50'])</p>
                <a href="{{ route('locale.region.shave-plans.trial-plan', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                class="btn-start btn-custom MuliBold" style="font-size:2rem;">@lang('website_contents.global.header.get_started')</a>
            </div>
        </section>

        <section id="mobile_product_banner" class="d-block d-lg-none">
            <div class="col-12 p-0 d-block d-sm-none">
                <img src="{{URL::asset('/images/common/product_banner.png')}}" class="img-fluid"/>
            </div>
        </section>
    </div>

    <section id="mobile_product_handle" class="d-block d-lg-none pt-4">
        <div class="col-12 product-other-banner1 d-sm-block">
            <img src="{{URL::asset('/images/common/product-swivel@2x.png')}}" class="img-fluid"/>
        </div>
    </section>

    <section id="mobile_product_handle_text" class="d-block d-lg-none pb-5 white-bg">
        <div class="col-12" style="text-align:center;">
            <h3 class="MuliExtraBold product-h">
                @lang('website_contents.products.product.swivel_handle')
            </h3>
            <h3 class="MuliPlain product-p mt-3 mb-4">
                @lang('website_contents.products.product.swivel_handle_desc')
            </h3>
            <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
               class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
            <p class="pointer" style="color:#000000; text-decoration: underline;" onclick="handleProductPage();">
                @lang('website_contents.products.content.learn_more')
            </p>
        </div>
    </section>

    <section id="mobile_product_blade" class="d-block d-lg-none pb-2"
             style="background: linear-gradient(to bottom, white 0%, #ECECEC 20%, #ECECEC 80% );">
        <div id="mobile_product_carousel" class="carousel slide" data-ride="carousel" style="min-height: 550px">
            <ol class="carousel-indicators">
                <li data-target="#mobile_product_carousel" data-slide-to="0" class="active"></li>
                <li data-target="#mobile_product_carousel" data-slide-to="1"></li>
                <li data-target="#mobile_product_carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="col-12 product-other-banner2 d-sm-block"
                         style="background-image: url('{{asset('images/common/product-3blade@2x.png ')}}');">
                    </div>

                    <div class="col-12 pb-5 mb-5" style="text-align:center;">
                        <h3 class="MuliExtraBold product-h">
                            @lang('website_contents.products.product.blade_packs')
                        </h3>
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-block d-sm-none">
                            @lang('website_contents.products.product.blade_packs_desc_mobile')
                        </h3>
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-none d-sm-block">
                            @lang('website_contents.products.product.blade_packs_desc')
                        </h3>
                        <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                           class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                        <p class="pointer" style="color:#000000; text-decoration: underline;"
                           onclick="bladeProductPage();">@lang('website_contents.products.content.learn_more')
                        </p>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-12 product-other-banner3 d-sm-block"
                         style="background-image: url('{{asset('images/common/product-5blade@2x.png')}}');">
                    </div>
                    <div class="col-12 pb-5 mb-5" style="text-align:center;">
                        <h3 class="MuliExtraBold product-h">
                            @lang('website_contents.products.product.blade_packs')
                        </h3>
                        {{-- <h3 class="MuliPlain product-p mt-3 mb-4">
                            Comes with all you need to experience our <br class="d-none d-sm-block"/>
                            quality for yourself. Trial period lasts for 14 days.
                        </h3> --}}
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-block d-sm-none">
                            @lang('website_contents.products.product.blade_packs_desc_mobile')
                        </h3>
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-none d-sm-block">
                            @lang('website_contents.products.product.blade_packs_desc')
                        </h3>
                        <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                           class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                        <p class="pointer" style="color:#000000; text-decoration: underline;"
                           onclick="bladeProductPage();">@lang('website_contents.products.content.learn_more')
                        </p>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-12 product-other-banner4 d-sm-block"
                         style="background-image: url('{{asset('images/common/product-6blade@2x.png')}}');">
                    </div>

                    <div class="col-12 pb-5 mb-5" style="text-align:center;">
                        <h3 class="MuliExtraBold product-h">
                            @lang('website_contents.products.product.blade_packs')
                        </h3>
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-block d-sm-none">
                            @lang('website_contents.products.product.blade_packs_desc_mobile')
                        </h3>
                        <h3 class="MuliPlain product-p mt-3 mb-4 d-none d-sm-block">
                            @lang('website_contents.products.product.blade_packs_desc')
                        </h3>
                        {{-- <h3 class="MuliPlain product-p mt-3 mb-4">
                            Comes with all you need to experience our <br class="d-none d-sm-block"/>
                            quality for yourself. Trial period lasts for 14 days.
                        </h3> --}}
                        <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                           class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
                        <p class="pointer" style="color:#000000; text-decoration: underline;"
                           onclick="bladeProductPage();">@lang('website_contents.products.content.learn_more')
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="mobile_product_cream" class="d-block d-lg-none"
             style="background: linear-gradient(to bottom, #ECECEC 0%, #ECECEC 20%, white 80%);">
        <div class="col-12 product-other-banner5 d-sm-block"
             style="background-image: url('{{asset('images/common/ShaveCream@2x.png')}}');">
        </div>

    </section>

    <section id="mobile_product_cream_text" class="d-block d-lg-none pb-5">
        <div class="col-12" style="text-align:center;">
            <h3 class="MuliExtraBold product-h">
                @lang('website_contents.products.product.shave_cream')
            </h3>
            <h3 class="MuliPlain product-p mt-3 mb-4">
                @lang('website_contents.products.product.shave_cream_desc')
            </h3>
            <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
               class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
            <p class="pointer" style="color:#000000; text-decoration: underline;" onclick="screamProductPage();">
                @lang('website_contents.products.content.learn_more')
            </p>
        </div>
    </section>

    <section id="mobile_product_aftercream" class="d-block d-lg-none">
        <div class="col-12 product-other-banner6 d-sm-block"
             style="background-image: url('{{asset('images/common/Aftershave@2x.png')}}');">
        </div>
    </section>

    <section id="mobile_product_aftercream_text" class="d-block d-lg-none pb-5"
             style="background: linear-gradient(to bottom, white 0%, #ECECEC 100% );">
        <div class="col-12" style="text-align:center;">
            <h3 class="MuliExtraBold product-h">
                @lang('website_contents.products.product.after_shave_cream')
            </h3>
            <h3 class="MuliPlain product-p mt-3 mb-4">
                @lang('website_contents.products.product.after_shave_cream_desc')
            </h3>
            <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
               class="btn-start btn-product-custom MuliBold">@lang('website_contents.products.content.buy_now')</a>
            <p class="pointer" style="color:#000000; text-decoration: underline;" onclick="ascreamProductPage();">
                @lang('website_contents.products.content.learn_more')
            </p>
        </div>
    </section>

    <section id="mobile_product_trialkit" class="d-block d-lg-none pb-3"
             style="background: linear-gradient(to bottom, #ececec 100%, white 10%);">
        <div class="col-12 product-other-banner7 d-sm-block">
            <img src="{{URL::asset('/images/common/product-ask@2x.png')}}" class="img-fluid"/>
        </div>
        <div class="col-12" style="text-align:center;">
            <h3 class="MuliExtraBold product-h">
                @lang('website_contents.products.product.ask')
            </h3>
            <h3 class="MuliPlain product-p mt-3 mb-4">
                @lang('website_contents.products.product.ask_desc')
            </h3>
            <button class="btn-start btn-product-custom MuliBold" onclick="askProductPage();">@lang('website_contents.products.content.buy_now')</button>
        </div>
    </section>

    <section id="mobile_product_sleeve_bag" class="d-block d-lg-none">
        <div class="col-12 margB5rem">
            <div class="row justify-content-md-center">
                <div class="col-md-5 product-sleeve-bag">
                    <div class="product-sleeve-bag-border">
                        <div class="col-12" style="padding-top: 2%;">
                            <img src="{{ asset('/images/common/product-rubbersleeve.png') }}"
                                 class="img-fluid product-sleeve-bag-img"/>
                        </div>
                        <h3 class="MuliExtraBold product-h-title product-sleeve-bag-title">
                            @lang('website_contents.products.product.razor_protector')
                        </h3>
                        <p class="MuliPlain fs-18 pl-5 pr-5 product-sleeve-bag-p">
                            @lang('website_contents.products.product.razor_protector_desc')
                        </p>
                        <button class="btn-start btn-product-custom MuliBold" onclick="pAddonsProductPage();">@lang('website_contents.products.content.buy_now')</button>
                        <p class="MuliPlain fs-18 mb-5p product-sleeve-bag-p2">
                            @lang('website_contents.products.content.available_existing_mobile')
                        </p>
                    </div>
                </div>
                <div class="col-md-5 product-sleeve-bag">
                    <div class="product-sleeve-bag-border">
                        <div class="col-12" style="padding-top: 2%;">
                            <img src="{{ asset('/images/common/product-toiletrybag.png') }}" class="img-fluid product-sleeve-bag-img"/>
                        </div>
                        <h3 class="MuliExtraBold product-h-title product-sleeve-bag-title">
                            @lang('website_contents.products.product.toiletry_bag')
                        </h3>
                        <p class="MuliPlain fs-18 pl-5 pr-5 product-sleeve-bag-p">
                            @lang('website_contents.products.product.toiletry_bag_desc')
                        </p>
                        <button class="btn-start btn-product-custom MuliBold" onclick="pAddonsProductPage();">@lang('website_contents.products.content.buy_now')</button>
                        <p class="MuliPlain fs-18 product-sleeve-bag-p2">
                            @lang('website_contents.products.content.available_existing_mobile')
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Mobile END -->

    <section class="landing-get-started product-orange-bg pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pr-0 pl-0">
                    @if($m_h_sku == 'H3')
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                    @elseif($m_h_sku == 'H1')
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/premium/product/img-getstarted.png')}}">
                    @else
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                    @endif
                </div>
                <div class="col-md-6 text-center pr-md-0 pl-md-0">
                    <h2 class="bold-max get-started-font" style=""><strong>
                            @lang('website_contents.products.content.shave_on')
                        <br></strong>
                    </h2>
                    <p class="get-started-sub-font pb-xl-4">@lang('website_contents.products.content.new_shaving_experience', ['currency' => $currentCountry->currencyDisplay,'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso))])</p>
                    <a href="{{ route('locale.region.shave-plans.trial-plan', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                       class="btn btn-start text-uppercase" style="font-size:2rem;">@lang('website_contents.global.header.get_started')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="product-free-text">
        <div class="container" id="free-text">
            {{--        <div class="col-md-12 text-center pr-0 pl-0">--}}
            {{--            <h2 class="bold-max d-block white MuliExtraBold seo-meta"><strong>SEO META--}}
            {{--                    TEXT<br></strong></h2>--}}
            {{--        </div>--}}
            <div class="col-md-12 pr-0 pl-0">
                <p class="ftcolor-swap fs-12 text-left" style="margin-top: 2%; color:white;">
                    @lang('website_contents.products.content.free_text')
                </p>
            </div>
        </div>

        <style>
            .allplan_lists{}
            .each_plan{
                border: 1px solid lightgrey;
                margin: 2% auto;
                text-align: center;
                cursor: pointer;
            }
            .each_plan:hover{
                background: #fe5000;
            }
            /* .each_plan_name:hover{
                color:white;
            } */
            .each_plan_name{
                font-weight: bolder;
                text-align: center;
                margin: auto;
                padding: 2%;
            };
        </style>
        <div class="modal fade" id="notification_select_plan_edit" tabindex="-1" role="dialog" aria-labelledby="notification_select_plan_edit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 590px; !important;">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="col-12 text-center pt-5 pl-2 pr-2" id="plan_title_content">

                        </div>
                        <div class="col-12 text-center pb-5 allplan_lists" id="plan_list_content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function bladeProductPage() {
            window.open("{{ route('locale.region.blade', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}",'_blank');
        }

        function handleProductPage() {
            window.open("{{ route('locale.region.handle', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}",'_blank');
        }

        function screamProductPage() {
            window.open("{{ route('locale.region.shave-cream', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}",'_blank');
        }

        function ascreamProductPage() {
            window.open("{{ route('locale.region.aftershave', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}",'_blank');
        }

        function askProductPage() {
            location.href = "{{ route('locale.region.ask', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}";
        }

        function pAddonsProductPage() {
            let isLoggedIn = "<?php echo $isLoggedIn; ?>" ;
            if(isLoggedIn === "logged_in"){
                getSubsListsForAddon();
            }else{
                location.href = "{{ route('locale.region.product-addons', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}";
            }

        }

        function proceedToSelectedEditShavePlan(id){
            return new Promise(function(resolve, reject) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    url: GLOBAL_URL + "/product/product-addons/" + id,
                    success: function(data) {
                        loading.style.display = "none";
                        if(data !== false){
                            // console.log(data);
                            window.location.replace(data);
                        }
                    },
                    error: function(data) {
                        if (loading) {
                            loading.style.display = "none";
                        }
                        reject(data);
                    },
                    failure: function(data) {
                        if (loading) {
                            loading.style.display = "none";
                        }
                        reject(data);
                    }
                });
            });
        }

        function getSubsListsForAddon(){
            loading.style.display = "block";
            return new Promise(function(resolve, reject) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    url: GLOBAL_URL + "/product/subs-list",
                    success: function(data) {
                        // console.log(data);
                        loading.style.display = "none";
                        $("#plan_list_content").empty();
                        let dataArray = data.data;
                        if(JSON.parse(dataArray.length) !== 0){
                            let plan_title = '<div class="col-12 text-center"><h4 class="col-12"><strong>@lang("website_contents.products.product.select_plan")</strong></h4></div>';

                            $("#plan_title_content").html(plan_title);

                            data.data.forEach(function (sub){
                                // console.log("sub foreach", sub);
                                let blade_name = sub.blade_type.name + ' ' + trans('website_contents.every') +' ' + sub.subscription.PlanDuration + ' '+ trans('website_contents.Months');
                                let status = sub.subscription.status;
                                let plan_data = '<div class="col-12 each_plan" onClick="proceedToSelectedEditShavePlan('+sub.subscription.id+')">' +
                                '<p class="each_plan_name">'+blade_name+'</p>'+
                                '</div>';
                                $("#plan_list_content").append(plan_data);
                            })
                            // build popup data:
                            $("#notification_select_plan_edit").modal('show');
                        } else {
                            // console.log("else");
                            $("#plan_list_content").empty();
                            $("#plan_list_content").append('<p class="each_plan_name fs-20">@lang("website_contents.products.product.not_shave_plan")</p><a href="{{ route("locale.region.shave-plans.trial-plan.selection", ["langCode"=>view()->shared("url"), "countryCode"=>view()->shared("currentCountryIso")]) }}" class="btn-start btn-custom-banner text-uppercase pt-2" style="width: 295px; border-radius: 5px;" onClick=""><b>@lang("website_contents.products.product.start_shave")</b></a>');
                            $("#notification_select_plan_edit").modal('show');
                        }
                    },
                    error: function(data) {
                        if (loading) {
                            loading.style.display = "none";
                        }
                        reject(data);
                    },
                    failure: function(data) {
                        if (loading) {
                            loading.style.display = "none";
                        }
                        reject(data);
                    }
                });
            });
        }


        // function
    </script>
@endsection
