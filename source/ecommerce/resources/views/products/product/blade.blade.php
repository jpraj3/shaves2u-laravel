@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{ asset('css/product/blade.css') }}">

    <section id="handle_banner" style="background: #eaeaea;">
        <div class="col-md-12 d-none d-lg-block">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                            <button id="button-back" class="button-back float-left fs-30-sm d-flex">
                                <i class="fa fa-angle-left fs-30"></i> <span
                                    class="d-none d-lg-block my-1 ml-3">@lang('website_contents.global.content.back')</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 pb-5 pt-5 text-center">
                            <h1 class="MuliExtraBold fs-96" style="white-space: nowrap;">
                                @lang('website_contents.products.blades.blade_pack')
                            </h1>
                            <p class="MuliPlain fs-20 pb-3">@lang('website_contents.products.blades.blade_pack_desc')</p>
                            <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                               class="btn-product-custom btn-start MuliBold px-4"
                               style="font-size: 1.25rem !important;">@lang('website_contents.products.content.buy_now')</a>
                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('/images/common/product-5blade@2x.png')}}" style="max-width:120%;"
                                 class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 d-block d-lg-none padd0">
            <div class="col-12 padd0" style="position:relative;">
                <div class="col-12 text-center MuliExtraBold">
                    <h1 class="MuliExtraBold product-h pt-4 mb-0">
                        @lang('website_contents.products.blades.blade_pack')
                    </h1>
                    <p class="MuliPlain pb-4">
                        @lang('website_contents.products.blades.blade_pack_desc')
                    </p>
                </div>
                <div class="col-12 text-center">
                    <img src="{{asset('/images/common/product-5blade@2x.png')}}" class="img-fluid"/>
                </div>
                <div class="col-12 text-center pb-5">
                    {{-- <h3 class="MuliExtraBold product-h mb-3">
                        @foreach($data as $d)
                        @if($d->sku === 'S3/2018')
                        @lang('website_contents.products.blades.3blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $d->sellPrice])
                        @endif
                        @endforeach
                    </h3> --}}
                    <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                        class="btn-start btn-custom MuliBold pb-3" style="font-size:2rem;margin-bottom:0px;">@lang('website_contents.products.content.buy_now')</a>
                </div>
            </div>
        </div>
    </section>
    <section id="handle_view" class="p-3 darkgrey-bg MuliBold white text-center" style="font-size:1rem;">
        <?php
        session_start();
        if (isset($_SESSION['randviewnumber']) && !empty($_SESSION['randviewnumber'])) {
            $randnumber = $_SESSION["randviewnumber"];
            $randnumber = rand($randnumber - 2, $randnumber);
            $_SESSION["randviewnumber"] = "";
        } else {
            $randnumber = rand(3, 20);
            $_SESSION["randviewnumber"] = $randnumber;
        }
        echo $randnumber;
        ?> @lang('website_contents.products.content.looking_at_this')
    </section>

    <!-- Desktop Start -->
    <div class="blade-grey-bg d-none d-lg-block pt-3">
        <div class="container">
            <div class="row">
                <div class="col-12 p-5">
                    <div class="row" style="cursor: pointer;">
                        <div class="col-md-4 text-center pt-5 pb-5 pl-0 pr-0 3blade">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.3blades.name')</p>
                                <h4 class="MuliExtraBold height108md 3bladeFont" style="font-size:1.6rem;">
                                    @lang('website_contents.products.blades.3blades.desc')
                                </h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/3blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                                <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S3/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.3blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' =>  $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                        <div class="col-md-4 text-center pt-5 pb-5 pl-0 pr-0 5blade">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.5blades.name')</p>
                                <h4 class="MuliExtraBold 5bladeFont" style="font-size:1.6rem;">@lang('website_contents.products.blades.5blades.desc')</h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/5blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                                 <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S5/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.5blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                        <div class="col-md-4 pt-5 pb-5 pl-0 pr-0 text-center orange-item-selected 6blade">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.6blades.name')</p>
                                <h4 class="6bladeFont MuliExtraBold orange-font-selected" style="font-size:1.6rem;">@lang('website_contents.products.blades.6blades.desc')</h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/6blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                                <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S6/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.6blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Desktop End -->

    <!-- Mobile Start -->
    <div class="blade-grey-bg-full d-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12 p-3 pt-3">
                    <div class="row">
                        <div class="col-md-12 text-center 3blade m-4">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.3blades.name')</p>
                                <h4 class="MuliExtraBold height108md 3bladeFont" style="font-size:1.6rem;">@lang('website_contents.products.blades.3blades.desc')</h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/3blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                                <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S3/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.3blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12 p-3 pt-3">
                    <div class="row">
                        <div class="col-md-12 text-center 5blade m-4">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.5blades.name')</p>
                                <h4 class="MuliExtraBold 5bladeFont" style="font-size:1.6rem;">@lang('website_contents.products.blades.5blades.desc')</h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/5blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                                 <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S5/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.5blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blade-grey-bg-full d-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12 p-3 pt-3">
                    <div class="row">
                        <div class="col-md-12 text-center orange-item-selected 6blade m-4">
                            <div class="col-12">
                                <p class="MuliPlain" style="font-size:1.5rem;">@lang('website_contents.products.blades.6blades.name')</p>
                                <h4 class="6bladeFont MuliExtraBold orange-font-selected" style="font-size:1.6rem;">@lang('website_contents.products.blades.6blades.desc')</h4>
                            </div>
                            <img alt="Handle" class="img-fluid d-block mx-auto blade-width"
                                 src="{{asset('/images/common/6blades.png')}}"/>
                            <h3 class="MuliExtraBold color-orange mt-3" style="font-size:1.9rem;">
                               <!-- Currency and price for kr -->
                                @foreach($data as $d)
                                @if($d->sku === 'S6/2018')
                                <?php
                               $s_price = $d->sellPrice;

                               if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){
                                   $s_price = number_format($s_price);
                               }

                               ?>
                                @lang('website_contents.products.blades.6blades.price', ['currency' => $currentCountry->currencyDisplay, 'price' => $s_price])
                                @endif
                                @endforeach
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile End -->

    <div class="container white-bg" id="blade-features">
        <div class="row">
            <section id="handle_info" class="pb-5 pl-2 pr-2 white-bg">
                <h3 class="MuliExtraBold text-center pt-5 pb-5" style="font-size:1.9rem;">@lang('website_contents.products.blades.6blades.blade_pack_makes_good')</h3>
                <div class="col-12 pl-lg-0 pr-lg-0 pl-5 pr-5">
                    <div class="row">
                        <div class="col-12 col-md-4 pb-4">
                            <div class="row">
                                <div class="col-12 pb-2 height100md">
                                    <img alt="Handle" class="img-fluid d-block mx-auto"
                                         src="{{asset('/images/common/6blade-feature1.png')}}"/>
                                </div>
                                <div class="col-12 d-none d-lg-block padd0 text-center pt-md-5 pt-lg-0">
                                    <p>@lang('website_contents.products.blades.6blades.subdesc1')</p>
                                </div>
                                <div class="col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0">
                                    <p>@lang('website_contents.products.blades.6blades.subdesc1')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 pb-4">
                            <div class="row">
                                <div class="col-12 pb-2 height100md">
                                    <img alt="Handle" class="img-fluid d-block mx-auto"
                                         src="{{asset('/images/common/6blade-feature2.png')}}"/>
                                </div>
                                <div class="col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0">
                                    <p>@lang('website_contents.products.blades.6blades.subdesc2')</p>
                                </div>
                                <div class="col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0">
                                    <p>@lang('website_contents.products.blades.6blades.subdesc2')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 pb-4">
                            <div class="row">
                                <div class="col-12 pb-2 height100md">
                                    <img alt="Handle" class="img-fluid d-block mx-auto blademargT"
                                         src="{{asset('/images/common/6blade-feature3.png')}}"/>
                                </div>
                                <div class="col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0">

                                    <p>@lang('website_contents.products.blades.6blades.subdesc3')</p>
                                </div>
                                <div class="col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0">

                                    <p>@lang('website_contents.products.blades.6blades.subdesc3')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <section id="testimonials" class="d-flex align-items-center"
             style="background-image: url('{{asset('/images/common/testimonial-section.jpg')}}'); height: 720px;">
        <div class="col-md-12 justify-content-center">
            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators" style="top:9rem !important;">
                    <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#blogCarousel" data-slide-to="1"></li>
                    <li data-target="#blogCarousel" data-slide-to="2"></li>
                    <li data-target="#blogCarousel" data-slide-to="3"></li>
                    <li data-target="#blogCarousel" data-slide-to="4"></li>
                </ol>

                <div class="carousel-inner" style="height:150px;">

                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h5 class="mb-3" id="testimonial-one">
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h5 class="mb-3" id="testimonial-two">
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h5 class="mb-3" id="testimonial-three">
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h5 class="mb-3" id="testimonial-four">
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h5 class="mb-3" id="testimonial-five">
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="product-free-text">
        <div class="container" id="free-text">
            {{--        <div class="col-md-12 text-center pr-0 pl-0">--}}
            {{--            <h1 class="bold-max d-block white MuliExtraBold mb-4"><strong>SEO META--}}
            {{--                    TEXT<br></strong></h1>--}}
            {{--        </div>--}}
            <div class="col-md-12 text-left pr-0 pl-0">
                <p class="ftcolor-swap fs-12 text-left" style="margin-top: 2%; color:white;">@lang('website_contents.products.blades.blades_meta_text') </p>
{{--                <div class="col-12 mt-5 padd0">--}}
{{--                    <button class="btn-start handle-btn MuliBold" style="font-size:2rem;">@lang('website_contents.products.content.buy_now')</button>--}}
{{--                </div>--}}
            </div>
            <div class="col-md-12 text-center">
                <a href="{{ route('locale.region.shave-plans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                        class="btn-product-custom btn-start MuliBold px-4"
                        style="font-size: 1.25rem !important;">@lang('website_contents.products.content.buy_now')</a>
            </div>
        </div>
    </section>

    <!-- live sale notification (hide)-->
    <div class="blurb_fix bg-grey d-none">
        <div class="row mx-0">
            <div class="col-4 d-flex align-items-center">
                <div class="col-12 padd5 blurb_img white-bg">
                    <img class="center" src="{{URL::asset('/images/common/3blade.jpg')}}"
                         style="max-width: 100%; height: auto;"/>
                </div>
            </div>
            <div class="col-8 mb-0">
                <p class="MuliExtraBold color-orange mb-1 blurb_font">@lang('website_contents.products.content.bought_this')</p>
                <p class="MuliPlain">@lang('website_contents.products.blades.3blades.name')</p>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.6blade').click(function () {
            $('#blade-features').html(
            "<div class='row'> <section id='handle_info' class='pb-1 pl-2 pr-2 white-bg'>"+
            "<h3 class='MuliExtraBold text-center pt-5 pb-5' style='font-size:1.9rem;'>@lang('website_contents.products.blades.6blades.blade_pack_makes_good')</h3> "+
            "<div class='col-12 pl-lg-0 pr-lg-0 pl-5 pr-5'> <div class='row'> "+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'> "+
            "<img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/6blade-feature1.png')}}' /> "+
            "</div> "+
            "<div class='col-12 d-none d-lg-block padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p> @lang('website_contents.products.blades.6blades.subdesc1') </p> "+
            "</div> "+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p> @lang('website_contents.products.blades.6blades.subdesc1') </p> "+
            "</div> </div> </div> "+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'> "+
            "<img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/6blade-feature2.png')}}' /> </div> "+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'>  "+
            "<p> @lang('website_contents.products.blades.6blades.subdesc2') </p> </div> "+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>  "+
            "<p> @lang('website_contents.products.blades.6blades.subdesc2') </p> </div> </div> </div> "+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'> "+
            "<img alt='Handle' class='img-fluid d-block mx-auto blademargT' src='{{asset('/images/common/6blade-feature3.png')}}' /> </div> "+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'> "+
            "<p> @lang('website_contents.products.blades.6blades.subdesc3') </p> "+
            "</div> <div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p> @lang('website_contents.products.blades.6blades.subdesc3') </p> </div> </div> </div> </div> </div> </section> </div>");
            $(".6blade").addClass('orange-item-selected');
            $(".5blade").removeClass('orange-item-selected');
            $(".3blade").removeClass('orange-item-selected');
            $(".6bladeFont").addClass('orange-font-selected');
            $(".5bladeFont").removeClass('orange-font-selected');
            $(".3bladeFont").removeClass('orange-font-selected');
        });

        $('.5blade').click(function () {
            $('#blade-features').html(
            "<div class='row'> <section id='handle_info' class='pb-1 pl-2 pr-2 white-bg'>"+
            "<h3 class='MuliExtraBold text-center pt-5 pb-5' style='font-size:1.9rem;'>@lang('website_contents.products.blades.5blades.blade_pack_makes_good')</h3>"+
            "<div class='col-12 pl-lg-0 pr-lg-0 pl-5 pr-5'> <div class='row'> <div class='col-12 col-md-4 pb-4'> <div class='row'>"+
            "<div class='col-12 pb-2 height100md'> <img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/f1_blade@2x.png')}}' />"+
            "</div> <div class='col-12 d-none d-lg-block padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.5blades.subdesc1')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.5blades.subdesc1')</p> </div> </div>"+
            "</div> <div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'>"+
            "<img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/f2_blade.png')}}' /> </div>"+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'>  <p>@lang('website_contents.products.blades.5blades.subdesc2')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>  <p> @lang('website_contents.products.blades.5blades.subdesc2')</p> </div> </div> </div>"+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'>"+
            "<img alt='Handle' class='img-fluid d-block mx-auto blademargT' src='{{asset('/images/common/f3_blade.png')}}' /> </div>"+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.5blades.subdesc3')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.5blades.subdesc3')</p> </div> </div> </div> </div> </div> </section> </div>");
            $(".6blade").removeClass('orange-item-selected');
            $(".5blade").addClass('orange-item-selected');
            $(".3blade").removeClass('orange-item-selected');
            $(".6bladeFont").removeClass('orange-font-selected');
            $(".5bladeFont").addClass('orange-font-selected');
            $(".3bladeFont").removeClass('orange-font-selected');
        });

        $('.3blade').click(function () {
            $('#blade-features').html(
            "<div class='row'> <section id='handle_info' class='pb-1 pl-2 pr-2 white-bg'>"+
            "<h3 class='MuliExtraBold text-center pt-5 pb-5' style='font-size:1.9rem;'>@lang('website_contents.products.blades.3blades.blade_pack_makes_good')</h3>"+
            "<div class='col-12 pl-lg-0 pr-lg-0 pl-5 pr-5'> <div class='row'> <div class='col-12 col-md-4 pb-4'> <div class='row'>"+
            "<div class='col-12 pb-2 height100md'> <img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/3blade-feature1.png')}}' /> </div>"+
            "<div class='col-12 d-none d-lg-block padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.3blades.subdesc1')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p> @lang('website_contents.products.blades.3blades.subdesc1')</p> </div> </div> </div>"+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'>"+
            "<img alt='Handle' class='img-fluid d-block mx-auto' src='{{asset('/images/common/3blade-feature2.png')}}' /> </div>"+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'>"+
            "<p> @lang('website_contents.products.blades.3blades.subdesc2')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.3blades.subdesc2')</p> </div> </div> </div>"+
            "<div class='col-12 col-md-4 pb-4'> <div class='row'> <div class='col-12 pb-2 height100md'>"+
            "<img alt='Handle' class='img-fluid d-block mx-auto blademargT' src='{{asset('/images/common/3blade-feature3.png')}}' /> </div>"+
            "<div class='col-12 padd0 d-none d-lg-block text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.3blades.subdesc3')</p> </div>"+
            "<div class='col-12 d-block d-lg-none padd0 text-center pt-md-5 pt-lg-0'>"+
            "<p>@lang('website_contents.products.blades.3blades.subdesc3')</p> </div> </div> </div> </div> </div> </section> </div>");
            $(".6blade").removeClass('orange-item-selected');
            $(".5blade").removeClass('orange-item-selected');
            $(".3blade").addClass('orange-item-selected');
            $(".6bladeFont").removeClass('orange-font-selected');
            $(".5bladeFont").removeClass('orange-font-selected');
            $(".3bladeFont").addClass('orange-font-selected');
        });
    </script>

    <script>
        var testimonials = new Array();
        testimonials[0] = 'Weekly routine. #shaves2u @shaves2u<br><br><span style="font-size:16px;">F O O 王振富</span>';
        testimonials[1] = 'Do you shave daily? Well I do! If I don’t do it daily, the hair grows back really fast. Just recently I’ve come across @shaves2u, giving me the flexibility to shave more frequently with the same blades due to their high quality materials<br>Best part is, they’re delivered right to my doorstep!<br><br><span style="font-size:16px;">isaactanjs</span>';
        testimonials[2] = 'Sama tapi tak serupa ehh! Selepas menggunakan @shaves2u Shaving Trial Kit... Bercukur kini semudah 1-2-3 je ok! Bulu2 janggut & misai yang tertinggal melekat di celah2 blade dah senang dibilas dengan air, tak seperti jenaman pencukur yang lain... Shaving cream dia pun wangi & sangat membantu.... Thank you Shaves2U! Boleh dapatkan shaving kit anda dengan harga semurah RM6.50 je ok! Siyesly berbaloi2 untuk yang nak trim janggut, misai & bulu2 yang sewaktu dengannya gitchuu... Lepas ni tiada alasan lagi untuk bercukur dengan mudah pantas & sangat convenient di mana jua anda berada eh! #shaves2u #shaver #shaving #movember #cleanshave #easypeasy #freshlook<br><br><span style="font-size:16px;">aizatyanan</span>';
        testimonials[3] = 'Benda boleh dapat sama kualiti tapi murah. Pengganti gillette<br>#shaves2u<br><br><span style="font-size:16px;">hazwan01</span>';
        testimonials[4] = 'Ramai yang tanya, apa kelebihan guna servis shaver plan daripada Shaves2U? Selain daripada dapat Trial Kit untuk tempoh percubaan 14 hari dan penghantaran ke depan pintu, Shaves2U juga membolehkan kita jimat sehingga 40% berbanding penggunaan shaver dari brand-brand lain di pasaran. Jimat masa, jimat duit!<br><br><span style="font-size:16px;">penaberkala</span>';
        testimonials[5] = 'We work, we have fun, we enjoy!! #25BAbyMarch2019 #100BAbyDecember2019<br><br><span style="font-size:16px;">Sutra Adv Group</span>';
        testimonials[6] = '#MrWorkHardPlayHarder #Shaves2U<br><br><span style="font-size:16px;">Roen Cian</span>';
        testimonials[7] = 'Counting down to 2019,<br>Let’s shave for a shaper looking you!<br>Thanks @shaves2u for this premium great quality shaver!<br>Help me to minimise all the razor burns while I shave myself to a sharp looking year of 2019..<br>Get yourself one today !<br><br><span style="font-size:16px;">Aaryonstar</span>';
        testimonials[8] = '夠我用一年的剃鬚套裝來了，看來沒理由再留“頹廢Look”了啦~🧔🏽🙅🏽‍♂️🤣<br>各位男士朋友們，如果有常用手剃刀的話，不妨試試看 @shaves2u ，跟我之前用過的牌子來比，這個的剃刀用得比較順暢，剃完後也比較乾淨光滑多了~👍<br> 無論要每1到3~4個月的配套，或者像我一樣拿整盒Gift Box，或者要免費拿一套Trial Set(Trial不包括郵費RM6.50)，都可以任你們選擇，整體的價錢對比也會比較划算多了~👌<br> 除此之外，也可以用我的refer link獲得RM20的Credit折扣：<br> https://shaves2u.com/shave-plans/free-trial?ref=s2u-42UjNwgdcB&src=link<br><br><span style="font-size:16px;">Raymond Kong</span>';
        testimonials[9] = '깔끔하게 면도하고 기분좋은 하루 스타또<br>나는 깔끔한 남좌니까 ✌️✌️<br>(몸에 힘준거 안..비밀😂)<br>오늘도 좋은하루 보내잣💛<br><br><span style="font-size:16px;">park Jungbeom</span>';
        testimonials[10] = 'LOOKING ALL GOOD 😎<br>•   Nothing beats the feeling of smooth, stubble-less skin after a good shave. (Not to mention your wife will thank you for that too<br>•   The three blades in the @shaves2u shaver ensures that you get a close shave every single time.<br>•   The shaving cream has a great consistency that does not drip upon application. Together, they are the best shaving combo every man can ask for!<br>•   Get your starter kits from @shaves2u today!<br>•   Apologies for showing a kinda topless pic 😝😝><br><br><span style="font-size:16px;">shukorzailan</span>';

        $(document).ready(function() {
            var randomTestimonial1 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial2 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial3 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial4 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial5 = Math.floor(Math.random() * testimonials.length);
            document.getElementById('testimonial-one').innerHTML = testimonials[randomTestimonial1];
            document.getElementById('testimonial-two').innerHTML = testimonials[randomTestimonial2];
            document.getElementById('testimonial-three').innerHTML = testimonials[randomTestimonial3];
            document.getElementById('testimonial-four').innerHTML = testimonials[randomTestimonial4];
            document.getElementById('testimonial-five').innerHTML = testimonials[randomTestimonial5];
        });
    </script>

@endsection
