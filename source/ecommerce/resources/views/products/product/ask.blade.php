@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{ asset('css/product/ask.css') }}">

    <div style="background: #d4d4d4;">
        <section id="handle_banner">
            <div class="col-md-12 d-none d-lg-block">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                                <button id="button-back" class="button-back float-left fs-30-sm d-flex">
                                    <i class="fa fa-angle-left fs-30"></i> <span class="d-none d-lg-block my-1 ml-3">BACK</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{asset('/images/common/product-ask.png')}}" class="img-fluid pull-right">
                            </div>
                            <div class="col-md-6 mt-5 pt-5 text-center">
                                <h1 class="MuliExtraBold" style="font-size: 73px;">
                                    @lang('website_contents.products.ask.title')
                                </h1>
                                <p class="MuliPlain fs-20 pb-3">@lang('website_contents.products.ask.subtitle')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-12 d-block d-lg-none padd0">
                    <div class="col-12 padd0" style="position:relative;">
                        <div class="col-12 text-center MuliExtraBold">
                            <h1 class="MuliExtraBold product-h pt-4 mb-0">
                                @lang('website_contents.products.ask.title')
                            </h1>
                            <p class="MuliPlain pb-4">
                                @lang('website_contents.products.ask.subtitle')
                            </p>
                        </div>
                        <div class="col-12 text-center">
                            <img src="{{asset('/images/common/product-ask.png')}}" class="img-fluid"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section id="handle_view" class="p-3 darkgrey-bg MuliBold white text-center" style="font-size:1rem;">
        <?php
        session_start();
        if (isset($_SESSION['randviewnumber']) && !empty($_SESSION['randviewnumber'])) {
            $randnumber = $_SESSION["randviewnumber"];
            $randnumber = rand($randnumber - 2, $randnumber);
            $_SESSION["randviewnumber"] = "";
        } else {
            $randnumber = rand(3, 20);
            $_SESSION["randviewnumber"] = $randnumber;
        }
        echo $randnumber;
        ?> @lang('website_contents.products.content.looking_at_this')
    </section>

    <section>
        <div class="container white-bg">
            <div class="row">
                <section id="blade_info" class="pl-2 pr-2 white-bg">
                    <div class="col-12 pl-0 pr-0">
                        <h3 class="MuliExtraBold text-center pt-5 pb-3" style="font-size:1.9rem;">
                            @lang('website_contents.products.ask.start_customizing')
                        </h3>
                        <p class="MuliBold text-center pb-4" style="color:#828282;font-size:1.3rem;">
                            @lang('website_contents.products.ask.select_blade_type')
                        </p>
                        <div class="row">
                            @php($count = 0)
                            @foreach($featured as $ask)
                                <?php
                                $name = "";
                                $detail = "";
                                $img = "";
                                if ($ask["sku"]) {
                                    if (strpos($ask["sku"], 'ASK3') !== false) {
                                        $name = $ask["producttranslatesname"];
                                        $detail = $ask["producttranslatesshortDescription"];
                                        $img = asset('/images/common/3blade.png');
                                    } else if (strpos($ask["sku"], 'ASK5') !== false) {
                                        $name = $ask["producttranslatesname"];
                                        $detail = $ask["producttranslatesshortDescription"];
                                        $img = asset('/images/common/5blade.png');
                                    } else if (strpos($ask["sku"], 'ASK6') !== false) {
                                        $name = $ask["producttranslatesname"];
                                        $detail = $ask["producttranslatesshortDescription"];
                                        $img = asset('/images/common/6blade.png');
                                    }
                                }
                                ?>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div id="blade-{{$count}}" class="item-selected"
                                         style="padding:5px;margin-bottom:14px;width:100%">
                                        <div class="col-12 pt-3 d-none d-lg-block">
                                            <div class="row">
                                                <div class="col-6 pl-0 pr-0 text-center">
                                                    <img class="img-fluid" src="{{$img}}" alt="{{$name}}"
                                                         height="200px"/>
                                                </div>
                                                <div class="col-6 d-flex align-items-center">
                                                    <div class="row">
                                                        <div class="col-12 text-center">
                                                            <p class="fs-25 MuliBold mx-0">{{$name}}</p>
                                                        </div>
                                                        <div class="col-12 text-center">
                                                            <p style="margin: 0; font-size:14px !important; line-height:16px;">{{$detail}}</p>
                                                        </div>
                                                        <div class="col-12 text-center">
                                                            <p class="MuliBold mx-0" style="font-size:20px !important;">
                                                             <!-- Currency and price for kr -->
                                                            @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){{number_format($ask["sellPrice"])}}{{$currency}}@else{{$currency}} {{$ask["sellPrice"]}}@endif</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pt-3 d-block d-lg-none">
                                            <div class="row">
                                                <div class="col-6 pl-0 pr-0 text-center">
                                                    <img class="img-fluid" src="{{$img}}" alt="{{$name}}" style="width : 150px"/>
                                                </div>
                                                <div class="col-6 d-flex align-items-center">
                                                    <div class="row">
                                                        <div class="col-12 text-center">
                                                            <p class="MuliBold mx-0"
                                                               style="font-size:20px !important">{{$name}}</p>
                                                        </div>
                                                        <div class="col-12 text-center">
                                                            <p style="margin: 0;">{{$detail}}</p>
                                                        </div>
                                                        <div class="col-12 text-center">
                                                            <p class="MuliBold mx-0" style="font-size:16px !important">
                                                             <!-- Currency and price for kr -->
                                                            @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr"){{number_format($ask["sellPrice"])}}{{$currency}}@else{{$currency}} {{$ask["sellPrice"]}}@endif</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="product_country_id_{{$count}}"
                                               id="product_country_id_{{$count}}"
                                               value="{{$ask["productcountriesid"]}}">
                                        <input type="hidden" name="product_name_{{$count}}" id="product_name_{{$count}}"
                                               value="{{$name}}">
                                                <!-- Currency and price for kr -->
                                               @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                               <input type="hidden" name="product_price_{{$count}}"
                                               id="product_price_{{$count}}"
                                               value="{{$ask["sellPrice"]}}{{$currency}}">
                                               @else
                                               <input type="hidden" name="product_price_{{$count}}"
                                               id="product_price_{{$count}}"
                                               value="{{$currency}} {{$ask["sellPrice"]}}">
                                               @endif
                                        <img id="blade-tick-{{$count}}" class="tick-mark"
                                             src="{{asset('/images/common/tick.png')}}" style="max-width: 1rem;"/>
                                    </div>
                                </div>
                                @php($count++)
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 py-md-5 padd0">
                        <form method="GET"
                              action="{{ route('locale.region.alacarte.ask.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                            @csrf
                            <input type="hidden" name="product_country_id" id="product_country_id" value="">
                            <!-- Currency and price for kr -->
                            <button type="submit" class="btn-start handle-btn MuliBold" style="font-size:2rem;">@lang('website_contents.products.content.buy_now')
                                |
                                @if(strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) == "kr")
                                <span id="selection_price">70.00{{$currency}}</span></button>
                                @else
                                <span id="selection_price">{{$currency}} 70.00</span></button>
                                @endif
                        </form>
                    </div>
                </section>
            </div>
        </div>

    </section>

    <section class="d-none d-lg-block">
        <div class="container">
            <div class="col-md-12 text-center pr-0 pl-0">
                <h2 class="MuliExtraBold fs-30" style="margin-bottom: 2%; margin-top: 2%"><strong>@lang('website_contents.products.ask.what_comes_in_box')<br></strong></h2>
                <p class="fs-20 Muli" style="margin-bottom: 5%; margin-top: 2%;">@lang('website_contents.products.ask.what_comes_in_box_detail')<br></p>
            </div>
            <!--Accordion wrapper-->
            <div class="accordion md-accordion bordB-grey" id="accordionAsk" role="tablist" aria-multiselectable="true">

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                    <div class="row" style="min-height: 250px;">
                        <div class="col-3 pr-5 d-flex align-items-center">
                            <div class="box-image-change"></div>
                        </div>
                        <div class="col-9 pl-5 d-flex align-items-center">
                            <div class="row w-100">

                                <!-- Card header -->

                                <div id="box-change" class="w-100">
                                    <div class="card-header center-sm bg-none bordB-grey w-100" role="tab"
                                         id="headingOne1" style="background-color:initial !important;">
                                        <a data-toggle="collapse" data-parent="#accordionAsk" href="#collapseOne1"
                                           aria-expanded="true" aria-controls="collapseOne1">
                                            <h4 class="mb-0 MuliExtraBold color-black fs-25">
                                                @lang('website_contents.products.ask.blade.6blades.name')
                                                <i
                                                    class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                            </h4>
                                            <h5 class="mb-0 MuliBold color-darkgrey fs-24">
                                                @lang('website_contents.products.ask.blade.6blades.desc')
                                            </h5>
                                        </a>
                                    </div>

                                    <!-- Card body -->
                                    <div id="collapseOne1" class="collapse show" role="tabpanel"
                                         aria-labelledby="headingOne1" data-parent="#accordionAsk">
                                        <div class="card-body">
                                            <p class="mb-0 MuliBold fs-16 mb-4">
                                                @lang('website_contents.products.ask.blade.6blades.info_1')<br/>
                                            </p>
                                            <p class="mb-0 Muli fs-16">
                                                @lang('website_contents.products.ask.blade.6blades.info_1_desc')
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Card footer -->
                                <div class="card-footer center-sm d-sm-none" role="tab" id="footerOne1"
                                     style="background-color:initial !important;">
                                    <a data-toggle="collapse" data-parent="#accordionAsk" href="#collapseOne1"
                                       aria-expanded="true" aria-controls="collapseOne1">
                                        <h5 class="mb-0 text-center MuliBold color-black">
                                            <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                    <div class="row" style="min-height: 250px;">
                        <div class="col-3 pr-5 d-flex align-items-center">
                            <img class="img-fluid" src="{{asset('/images/common/swivel-handle_front@2x.png')}}">
                        </div>
                        <div class="col-9 pl-5 d-flex align-items-center">
                            <div class="row w-100">

                                <!-- Card header -->
                                <div class="card-header center-sm bg-none bordB-grey w-100" role="tab" id="headingTwo2"
                                     style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                        <h4 class="mb-0 MuliExtraBold color-black fs-25">
                                            @lang('website_contents.products.ask.handle.name')
                                            <i
                                                class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                        </h4>
                                        <h5 class="mb-0 MuliBold color-darkgrey fs-24">
                                            @lang('website_contents.products.ask.handle.desc')
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                                     data-parent="#accordionAsk">
                                    <div class="card-body">
                                        <p class="mb-0 MuliBold fs-16 mb-4">
                                            @lang('website_contents.products.ask.blade.6blades.info_1')<br/>
                                        </p>
                                        <p class="mb-0 Muli fs-16">
                                        <li>@lang('website_contents.products.ask.handle.info_1_desc')</li>
                                        <li>@lang('website_contents.products.ask.handle.info_2_desc')</li>
                                        <li>@lang('website_contents.products.ask.handle.info_3_desc')</li>
                                        </p>
                                    </div>
                                </div>

                                <!-- Card footer -->
                                <div class="card-footer center-sm d-sm-none" role="tab" id="footerTwo2"
                                     style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                        <h5 class="mb-0 text-center MuliBold color-black">
                                            <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                    <div class="row" style="min-height: 250px;">
                        <div class="col-3 pr-5 d-flex align-items-center">
                            <img class="img-fluid" src="{{asset('/images/common/ShaveCream_shadowless@2x.png')}}">
                        </div>
                        <div class="col-9 pl-5 d-flex align-items-center">
                            <div class="row w-100">

                                <!-- Card header -->
                                <div class="card-header center-sm bg-none bordB-grey w-100" role="tab"
                                     id="headingThree3" style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                        <h4 class="mb-0 MuliExtraBold color-black fs-25">
                                            @lang('website_contents.products.ask.shave_cream.name')
                                            <i
                                                class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                        </h4>
                                        <h5 class="mb-0 MuliBold color-darkgrey fs-24">
                                            @lang('website_contents.products.ask.shave_cream.desc')
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapseThree3" class="collapse" role="tabpanel"
                                     aria-labelledby="headingThree3" data-parent="#accordionAsk">
                                    <div class="card-body">
                                        <p class="mb-0 MuliBold fs-16 mb-4">
                                            @lang('website_contents.products.ask.blade.6blades.info_1')<br/>
                                        </p>
                                        <p class="mb-0 Muli fs-16">
                                        <li>@lang('website_contents.products.ask.shave_cream.info_1_desc')</li>
                                        <li>@lang('website_contents.products.ask.shave_cream.info_2_desc')</li>
                                        <li>@lang('website_contents.products.ask.shave_cream.info_3_desc')</li>
                                        </p>
                                    </div>
                                </div>

                                <!-- Card footer -->
                                <div class="card-footer center-sm d-sm-none" role="tab" id="footerThree3"
                                     style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                        <h5 class="mb-0 text-center MuliBold color-black">
                                            <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                    <div class="row" style="min-height: 250px;">
                        <div class="col-3 pr-5 d-flex align-items-center">
                            <img class="img-fluid" src="{{asset('/images/common/Aftershave_shadowless@2x.png')}}">
                        </div>
                        <div class="col-9 pl-5 d-flex align-items-center">
                            <div class="row w-100">

                                <!-- Card header -->
                                <div class="card-header center-sm bg-none bordB-grey w-100" role="tab" id="headingFour4"
                                     style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4">
                                        <h4 class="mb-0 MuliExtraBold color-black fs-25">
                                            @lang('website_contents.products.ask.after_shave_cream.name')
                                            <i
                                                class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                        </h4>
                                        <h5 class="mb-0 MuliBold color-darkgrey fs-24">
                                            @lang('website_contents.products.ask.after_shave_cream.desc')
                                        </h5>
                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
                                     data-parent="#accordionAsk">
                                    <div class="card-body">
                                        <p class="mb-0 MuliBold fs-16 mb-4">
                                            @lang('website_contents.products.ask.blade.6blades.info_1')<br/>
                                        </p>
                                        <p class="mb-0 Muli fs-16">
                                        <li>@lang('website_contents.products.ask.after_shave_cream.info_1_desc')</li>
                                        <li>@lang('website_contents.products.ask.after_shave_cream.info_2_desc')</li>
                                        <li>@lang('website_contents.products.ask.after_shave_cream.info_3_desc')</li>
                                        </p>
                                    </div>
                                </div>

                                <!-- Card footer -->
                                <div class="card-footer center-sm d-sm-none" role="tab" id="footerFour4"
                                     style="background-color:initial !important;">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                       href="#collapseFour4" aria-expanded="false" aria-controls="collapseFour4">
                                        <h5 class="mb-0 text-center MuliBold color-black">
                                            <i class="fa fa-angle-down rotate-icon"></i>
                                        </h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Accordion card -->

            </div>
            <!-- Accordion wrapper -->
            <div class="col-12 my-5">
                <div class="row justify-content-center">
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-2 padd0">
                            <img class="img-fluid"
                                 src="{{asset('/images/common/iconfinder_equipmentrental_2318443@2x.png')}}">
                        </div>
                        <div class="col-10 padd0">
                            <span class="fs-16 Muli font-weight-bold">@lang('website_contents.products.ask.free_delivery_door')</span>
                        </div>
                    </div>
                    <div class="col-4 d-flex align-items-center">
                        <div class="col-2 padd0">
                            <img class="img-fluid"
                                 src="{{asset('/images/common/iconfinder_26_razor_barbershop_blade_shaving_shave_equipment_998303@2x.png')}}">
                        </div>
                        <div class="col-10 padd0">
                            <span class="fs-16 Muli font-weight-bold">@lang('website_contents.products.ask.cartridges_fit_handles')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="d-block d-lg-none">
        <div class="container">
            <div class="col-md-12 text-center pr-0 pl-0 pt-5">
                <h2 class="MuliExtraBold fs-30"><strong>@lang('website_contents.products.ask.what_comes_in_box')<br></strong></h2>
                <p class="fs-20 Muli" style="margin-bottom: 5%; margin-top: 2%;">@lang('website_contents.products.ask.what_comes_in_box_detail')<br></p>
            </div>
        </div>

        <!--Accordion wrapper-->
        <div class="container">
            <hr>
        </div>
        <div class="accordion md-accordion bordB-grey" id="accordionAsk" role="tablist" aria-multiselectable="true">
            <!-- Accordion card -->
            <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                <div class="row">
                    <div class="col-5 align-items-center">
                        <div class="box-image-change"></div>
                    </div>
                    <div class="col-6 d-flex align-items-center">
                        <div class="row w-100">

                            <!-- Card header -->
                            <div id="box-change"  class="w-100">
                                <div class="card-header pl-0 pr-0 center-sm bg-none bordB-grey w-100" role="tab"
                                     id="headingOne1">
                                    <a data-toggle="collapse" data-parent="#accordionAsk" href="#collapseOne1"
                                       aria-expanded="true" aria-controls="collapseOne1">
                                        <h5 class="mb-0 MuliExtraBold color-black text-left">
                                            @lang('website_contents.products.ask.blade.6blades.name')
                                            <i
                                                class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                        </h5>
                                        <h6 class="mb-0 MuliBold color-darkgrey text-left pt-3">
                                            @lang('website_contents.products.ask.blade.6blades.desc')
                                        </h6>
                                    </a>
                                </div>
                                <!-- Card body -->
                                <div id="collapseOne1" class="collapse show" role="tabpanel"
                                     aria-labelledby="headingOne1"
                                     data-parent="#accordionAsk">
                                    <div class="card-body pl-0 pr-0">
                                        <p class="mb-0 MuliBold fs-16 mb-4 text-left">
                                            @lang('website_contents.products.ask.blade.6blades.info_1')<br/>
                                        </p>
                                        <p class="mb-0 Muli fs-16 text-left">
                                            @lang('website_contents.products.ask.blade.6blades.info_1_desc')
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1 align-items-top">
                            <!-- Card footer -->
                            <div class="center-sm d-sm-none" role="tab" id="footerOne1">
                                <a data-toggle="collapse" data-parent="#accordionAsk" href="#collapseOne1"
                                   aria-expanded="true" aria-controls="collapseOne1">
                                    <h6><i class="fa fa-angle-down rotate-icon"></i></h6>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="container">
                <hr>
            </div>
            <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                <div class="row">
                    <div class="col-5 align-items-center pr-0">
                        <img class="img-fluid" src="{{asset('/images/common/swivel-handle_front@2x.png')}}">
                    </div>
                    <div class="col-6 pl-0 pr-0 d-flex align-items-center">
                        <div class="row w-100">

                            <!-- Card header -->
                            <div class="card-header center-sm bg-none bordB-grey w-100 pr-0" role="tab" id="headingTwo2"
                                 style="background-color:initial !important;">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                   href="#collapseTwo2"
                                   aria-expanded="false" aria-controls="collapseTwo2">
                                    <h5 class="mb-0 MuliExtraBold color-black text-left">
                                        @lang('website_contents.products.ask.handle.name')
                                        <i
                                            class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                    </h5>
                                    <h6 class="mb-0 MuliBold color-darkgrey text-left">
                                        @lang('website_contents.products.ask.handle.desc')
                                    </h6>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                                 data-parent="#accordionAsk">
                                <div class="card-body">
                                    <p class="mb-0 MuliBold fs-16 mb-4 text-left">
                                        @lang('website_contents.products.ask.handle.info_1')<br/>
                                    </p>
                                    <p class="mb-0 Muli fs-16 text-left pl-0">
                                    <li class="text-left">@lang('website_contents.products.ask.handle.info_1_desc')
                                    </li>
                                    <li class="text-left">@lang('website_contents.products.ask.handle.info_2_desc')</li>
                                    <li class="text-left">@lang('website_contents.products.ask.handle.info_3_desc')</li>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="container">
                <hr>
            </div>
            <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                <div class="row">
                    <div class="col-5 align-items-center pr-0">
                        <img class="img-fluid" src="{{asset('/images/common/ShaveCream_shadowless@2x.png')}}">
                    </div>
                    <div class="col-6 pl-0 pr-0 d-flex align-items-center">
                        <div class="row w-100">

                            <!-- Card header -->
                            <div class="card-header center-sm bg-none bordB-grey w-100 pr-0" role="tab" id="headingTwo3"
                                 style="background-color:initial !important;">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                   href="#collapseTwo3"
                                   aria-expanded="false" aria-controls="collapseTwo3">
                                    <h5 class="mb-0 MuliExtraBold color-black text-left">
                                        @lang('website_contents.products.ask.shave_cream.name')
                                        <i
                                            class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                    </h5>
                                    <h6 class="mb-0 MuliBold color-darkgrey text-left">
                                        @lang('website_contents.products.ask.shave_cream.desc')
                                    </h6>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseTwo3" class="collapse" role="tabpanel" aria-labelledby="headingTwo3"
                                 data-parent="#accordionAsk">
                                <div class="card-body">
                                    <p class="mb-0 MuliBold fs-16 mb-4 text-left">
                                        @lang('website_contents.products.ask.shave_cream.info_1')<br/>
                                    </p>
                                    <p class="mb-0 Muli fs-16 text-left pl-0">
                                    <li class="text-left">@lang('website_contents.products.ask.shave_cream.info_1_desc')</li>
                                    <li class="text-left">@lang('website_contents.products.ask.shave_cream.info_2_desc')</li>
                                    <li class="text-left">@lang('website_contents.products.ask.shave_cream.info_3_desc')</li>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="container">
                <hr>
            </div>
            <div class="card bord-none bordT-grey" style="background-color:initial !important;">
                <div class="row">
                    <div class="col-5 align-items-center pr-0">
                        <img class="img-fluid" src="{{asset('/images/common/Aftershave_shadowless@2x.png')}}">
                    </div>
                    <div class="col-6 pl-0 pr-0 d-flex align-items-center">
                        <div class="row w-100">

                            <!-- Card header -->
                            <div class="card-header center-sm bg-none bordB-grey w-100 pr-0" role="tab" id="headingTwo4"
                                 style="background-color:initial !important;">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionAsk"
                                   href="#collapseTwo4"
                                   aria-expanded="false" aria-controls="collapseTwo4">
                                    <h5 class="mb-0 MuliExtraBold color-black text-left">
                                        @lang('website_contents.products.ask.after_shave_cream.name')
                                        <i
                                            class="fa fa-angle-down rotate-icon d-none d-md-block float-right color-black"></i>
                                    </h5>
                                    <h6 class="mb-0 MuliBold color-darkgrey text-left">
                                        @lang('website_contents.products.ask.after_shave_cream.desc')
                                    </h6>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseTwo4" class="collapse" role="tabpanel" aria-labelledby="headingTwo4"
                                 data-parent="#accordionAsk">
                                <div class="card-body">
                                    <p class="mb-0 MuliBold fs-16 mb-4 text-left">
                                        @lang('website_contents.products.ask.after_shave_cream.info_1')<br/>
                                    </p>
                                    <p class="mb-0 Muli fs-16 text-left pl-0">
                                    <li class="text-left">@lang('website_contents.products.ask.after_shave_cream.info_1_desc')</li>
                                    <li class="text-left">@lang('website_contents.products.ask.after_shave_cream.info_2_desc')</li>
                                    <li class="text-left">@lang('website_contents.products.ask.after_shave_cream.info_3_desc')</li>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->

        </div>
        <!-- Accordion wrapper -->
        <div class="container">
            <hr>
        </div>
        <div class="col-12 my-5">
            <div class="row justify-content-center">
                <div class="col-12 d-flex align-items-center">
                    <div class="col-2 padd0">
                        <img class="img-fluid"
                             src="{{asset('/images/common/iconfinder_equipmentrental_2318443@2x.png')}}">
                    </div>
                    <div class="col-10 padd0">
                        <span class="fs-16 Muli font-weight-bold">@lang('website_contents.products.ask.free_delivery_door')</span>
                    </div>
                </div>
                <div class="col-12 d-flex align-items-center">
                    <div class="col-2 padd0">
                        <img class="img-fluid"
                             src="{{asset('/images/common/iconfinder_26_razor_barbershop_blade_shaving_shave_equipment_998303@2x.png')}}">
                    </div>
                    <div class="col-10 padd0">
                        <span class="fs-16 Muli font-weight-bold">@lang('website_contents.products.ask.cartridges_fit_handles')</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section id="testimonials" class="d-flex align-items-center"
             style="background-image: url('{{asset('/images/common/testimonial-section.jpg')}}'); height: 720px;">
        <div class="col-md-12 justify-content-center">
            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                <ol class="carousel-indicators">
                    <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#blogCarousel" data-slide-to="1"></li>
                    <li data-target="#blogCarousel" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h4 class="mb-3">"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    official.<br/>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco"</h4>
                                <p style="font-size:16px;">Person Name
                                <p>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h4 class="mb-3">"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    official.<br/>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco"</h4>
                                <p style="font-size:16px;">Person Name</p>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-12 text-center color-white">
                                <h4 class="mb-3">"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    official.<br/>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco"</h4>
                                <p style="font-size:16px;">Person Name</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="landing-get-started product-orange-bg pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pr-0 pl-0">
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                </div>
                <div class="col-md-6 text-center mb-3">
                    <h2 class="bold-max get-started-font" style=""><strong>@lang('website_contents.products.ask.prefer_shave_plan')<br></strong>
                    </h2>
                    <p class="get-started-sub-font pb-2">
                        @lang('website_contents.products.ask.enjoy_low_prices')
                    </p>
                    <button class="btn btn-tp-start btn-start font-weight-bold"><a href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"class="btn-start btn-custom-banner text-uppercase" >@lang('website_contents.global.header.get_started')</a></button>
                </div>
            </div>
        </div>
    </section>

    <section class="product-free-text">
        <div class="container" id="free-text">
            {{--            <div class="col-md-12 text-center pr-0 pl-0">--}}
            {{--                <h2 class="bold-max d-block white MuliExtraBold seo-meta"><strong>SEO META--}}
            {{--                        TEXT<br></strong></h2>--}}
            {{--            </div>--}}
            <div class="col-md-12 text-left pr-0 pl-0">
                <p class="ftcolor-swap white fs-12 seo-meta-sub">@lang('website_contents.products.ask.meta_text')</p>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $("#blade-0").click(function () {
            $('.box-image-change').html(
                "<img class='img-fluid' style='width: 80%' src='{{asset('/images/common/3blade.png')}}'>"
            )

            $("#box-change").html(
                "<div class='card-header center-sm bg-none bordB-grey w-100' role='tab' id='headingOne1' style='background-color:initial !important;'>"+
                "<a data-toggle='collapse' data-parent='#accordionAsk' href='#collapseOne1' aria-expanded='true' aria-controls='collapseOne1'>"+
                "<h4 class='mb-0 MuliExtraBold color-black fs-25'>@lang('website_contents.products.ask.blade.3blades.name') <i class='fa fa-angle-down rotate-icon d-none d-md-block float-right color-black'></i> </h4> "+
                "<h5 class='mb-0 MuliBold color-darkgrey fs-24'> @lang('website_contents.products.ask.blade.3blades.desc') </h5> </a> </div> "+
                "<!-- Card body --> <div id='collapseOne1' class='collapse show' role='tabpanel' aria-labelledby='headingOne1' data-parent='#accordionAsk'> "+
                "<div class='card-body'> "+
                "<p class='mb-0 MuliBold fs-16 mb-4'> @lang('website_contents.products.ask.blade.3blades.info_1')<br /> </p> "+
                "<p class='mb-0 Muli fs-16'> @lang('website_contents.products.ask.blade.3blades.info_1_desc') </p> "+
                "</div> </div>"
            )
        });

        $("#blade-1").click(function () {
            $('.box-image-change').html(
                "<img class='img-fluid' src='{{asset('/images/common/5blade.png')}}'>"
            )

            $("#box-change").html(
                "<div class='card-header center-sm bg-none bordB-grey w-100' role='tab' id='headingOne1' style='background-color:initial !important;'>"+
                    "<a data-toggle='collapse' data-parent='#accordionAsk' href='#collapseOne1' aria-expanded='true' aria-controls='collapseOne1'>"+
                        "<h4 class='mb-0 MuliExtraBold color-black fs-25'>@lang('website_contents.products.ask.blade.5blades.name') <i class='fa fa-angle-down rotate-icon d-none d-md-block float-right color-black'></i> </h4>"+
                        "<h5 class='mb-0 MuliBold color-darkgrey fs-24'> @lang('website_contents.products.ask.blade.5blades.desc') </h5>"+
                        "</a> </div>"+
                        "<!-- Card body --> <div id='collapseOne1' class='collapse show' role='tabpanel' aria-labelledby='headingOne1' data-parent='#accordionAsk'>"+
                            "<div class='card-body'>"+
                                "<p class='mb-0 MuliBold fs-16 mb-4'> @lang('website_contents.products.ask.blade.5blades.info_1')<br /> </p>"+
                                "<p class='mb-0 Muli fs-16'> @lang('website_contents.products.ask.blade.5blades.info_1_desc') </p> </div> </div>"
            )
        });

        $("#blade-2").click(function () {
            $('.box-image-change').html(
                "<img class='img-fluid' src='{{asset('/images/common/6blade.png')}}'>"
            )

            $("#box-change").html(
                "<div class='card-header center-sm bg-none bordB-grey w-100' role='tab' id='headingOne1' style='background-color:initial !important;'>"+
                    "<a data-toggle='collapse' data-parent='#accordionAsk' href='#collapseOne1' aria-expanded='true' aria-controls='collapseOne1'>"+
                        "<h4 class='mb-0 MuliExtraBold color-black fs-25'> @lang('website_contents.products.ask.blade.6blades.name') <i class='fa fa-angle-down rotate-icon d-none d-md-block float-right color-black'></i> </h4>"+
                        "<h5 class='mb-0 MuliBold color-darkgrey fs-24'> @lang('website_contents.products.ask.blade.6blades.desc') </h5> </a> </div>"+
                        "<!-- Card body --> <div id='collapseOne1' class='collapse show' role='tabpanel' aria-labelledby='headingOne1' data-parent='#accordionAsk'>"+
                            "<div class='card-body'> <p class='mb-0 MuliBold fs-16 mb-4'> @lang('website_contents.products.ask.blade.6blades.info_1')<br /> </p>"+
                                "<p class='mb-0 Muli fs-16'>@lang('website_contents.products.ask.blade.6blades.info_1_desc') </p> </div> </div>"
            )
        });
        let codeiso = "<?php echo strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']); ?>";
    </script>

    <script src="{{ asset('js/functions/alacarte/ask/ask-checkout.function.js') }}"></script>
@endsection
