@extends('layouts.app')

@section('content')
<style>
    .product-box-container {
        display: inline-flex;
        cursor: pointer;
    }

    .product-box {
        padding: 0px;
        text-decoration: none;
        background: lightgray;
        padding: 2%;
        border: 1px solid orange;
        color: black;
    }

    .product-box:hover,
    .product-box:active,
    .product-box:focus {
        background: lightgreen;
        color: black;
    }
</style>
<div class="container">
    <div class="container-fluid col-12" style="margin:5%;">
        @foreach($featured as $ask)
        <div id="selection-container" onClick="onSelect({{$ask['productcountriesid']}},'{{$ask['producttranslatesname']}}')" class="row col-4 product-box-container">
            <div class="col-12 product-box">
                <p class="col-12" style="text-align:center;">
                    {{$ask["producttranslatesname"]}}
                </p>
                <img src="{{$ask["default_image_url"]}}" style="width:100%">
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="container-fluid" style="margin:5% 0;text-align:center;">
        <form method="GET" action="{{ route('locale.region.alacarte.ask.checkout', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
            <input type="hidden" name="product_country_id" id="product_country_id" value="">
            <button type="submit">Proceed to Checkout</button>
        </form>
    </div>
</div>
<script src="{{ asset('js/functions/alacarte/ask/ask-checkout.function.js') }}"></script>

@endsection
