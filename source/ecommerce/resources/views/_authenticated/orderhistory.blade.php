@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')

<link rel="stylesheet" href="{{ asset('css/_authenticated/orderhistory/orderhistory.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
@if($orders_info)
<div class="bg-grey">
@else
<div class="bg-grey" style="height: 670px;">
@endif
<section class="orderhistory-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 padd30 text-center">
                <h1 class="MuliExtraBold">@lang('website_contents.authenticated.orderhistory.order_history')</h1>
            </div>
            <div class="col-lg-12 px-5">
                @if($orders_info)
                   @foreach($orders_info as $order_info)
                  @php($orderNo = \App\Services\OrderService::formatOrderNumberV2($order_info->order,null,false))
                     <div class="row border-top border-bottom padd30 pl-0 pr-0 pb-3">
                        <div class="col-7 col-lg-8 pl-0 pr-0">
                            <p class="MuliBold fs-25 d-none d-lg-block">@lang('website_contents.authenticated.orderhistory.order_no',['orderNo' => $orderNo])</strong></p>
                            <p class="MuliBold d-block d-lg-none">@lang('website_contents.authenticated.orderhistory.order_no',['orderNo' => $orderNo])</strong></p>

                            <p class="MuliPlain fs-18 fs-14-sm d-none d-lg-block">{{ $order_info->order->created_at }}</p>
                            <p class="MuliPlain d-block d-lg-none">{{ $order_info->order->created_at }}</p>
                                  @if(strtolower($currentCountryIso) == 'kr')
                                  <!-- Currency and price for kr -->
                                  <?php
                               $s_tprice =  number_format($order_info->receipt->totalPrice) ;
                               ?>
                                 <p class="MuliPlain fs-18 fs-14-sm d-none d-lg-block">{{  $s_tprice }}{{ $order_info->country->currencyDisplay }}</p>
                            <p class="MuliPlain d-block d-lg-none">{{  $s_tprice }}{{ $order_info->country->currencyDisplay }}</p>
                                  @else
                                  <p class="MuliPlain fs-18 fs-14-sm d-none d-lg-block">{{ $order_info->country->currencyDisplay }} {{ $order_info->receipt->totalPrice }}</p>
                            <p class="MuliPlain d-block d-lg-none">{{ $order_info->country->currencyDisplay }} {{ $order_info->receipt->totalPrice }}</p>
                                  @endif
                        </div>
                        <div class="col-5 col-lg-4 pl-0 pr-0 pt-2">
                            @if($order_info->order->status === 'Pending' || $order_info->order->status === 'On Hold' || $order_info->order->status === 'Unrealized' || $order_info->order->status === 'Payment Failure' || $order_info->order->status === 'Cancelled')
                            <div class="status-onhold col-12 col-md-6 fs-14 text-center bg-green ml-auto color-white mb-3">
                                <span class="mx-auto">@lang('website_contents.order_status.'.$order_info->order->status)</span>
                            </div>
                            @elseif($order_info->order->status === 'Processing' || $order_info->order->status === 'Delivering' || $order_info->order->status === 'Completed' || $order_info->order->status === 'Payment Received')
                            <div class="status-active col-12 col-md-6 fs-14 text-center bg-green ml-auto color-white mb-3">
                                <span class="mx-auto">@lang('website_contents.order_status.'.$order_info->order->status)</span>
                            </div>
                            @endif
                            <p class="MuliPlain d-none d-lg-block"><a class="color-orange float-right" href="{{ route('locale.region.authenticated.orderhistory.details', ['id'=>$order_info->order->id,'langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"><strong>@lang('website_contents.authenticated.orderhistory.view_details') ></strong></a></h4>
                            <p class="MuliPlain d-block d-lg-none" style="bottom:0; right:0;"><a class="color-orange float-right" href="{{ route('locale.region.authenticated.orderhistory.details', ['id'=>$order_info->order->id,'langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"><strong>@lang('website_contents.authenticated.orderhistory.view_details') ></strong></a></h4>
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="col-lg-12 padd30 text-center">
                    @lang('website_contents.authenticated.orderhistory.no_history')
                 </div>
                @endif
            </div>
        </div>
    </div>
</section>


<section class="orderhistory-load-more paddTB40">
@if($orders_info)
    <div class="container px-5">
        <div class="row">
           <div class="col-lg-6 pl-0 pr-0 d-flex align-items-center">
                <!-- <h3><a class="padd30 color-orange MuliBold pl-0 pr-0" href=""><strong>LOAD MORE</strong></a></h3> -->
            </div>
            <div id="shop-more" class="col-lg-6 pl-0 pr-0">
                <a href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" >
                    <img class="img img-responsive img-fluid float-right" alt="Shop More" src="{{asset('/images/common/shop-more/order-shop-more-'.strtolower(view()->shared('url')).'.png')}}"></a>
            </div>
        </div>
    </div>
    @else
    <div class="container">
        <div class="row">
            <div id="shop-more2" class="col-lg-12 text-center" >
                <a href="{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" >
                    <img class="img img-responsive img-fluid float-right"  alt="Shop More" src="{{asset('/images/common/shop-more/order-shop-more-'.strtolower(view()->shared('url')).'.png')}}"></a>
            </div>
        </div>
    </div>
    @endif
</section>

<!-- <section class="orderhistory-load-more-mobile d-lg-none bg-orange">
        <button id="orderhistory-load-more-btn" class="btn-load-section paddTB20">
            Load More
        </button>
</section> -->
</div>

<script src="{{ asset('js/functions/_authenticated/orderhistory/orderhistory.function.js') }}"></script>
<script>
    var currentCountry = window.localStorage.getItem('currentCountry');

    if(JSON.parse(currentCountry).codeIso === "MY" && JSON.parse(currentCountry).defaultLang === "EN" || JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "EN" || JSON.parse(currentCountry).codeIso === "SG" && JSON.parse(currentCountry).defaultLang === "EN" ){
        $('#shop-more').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img class='float-right' alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-en.png')}}' class='img img-responsive img-fluid' style='width:100%;'/></a>");
        $('#shop-more2').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-en.png')}}' class='img img-responsive img-fluid'  style='width:100%;'/></a>");
    } else if(JSON.parse(currentCountry).codeIso === "HK" && JSON.parse(currentCountry).defaultLang === "ZH-HK" || JSON.parse(currentCountry).codeIso === "TW" && JSON.parse(currentCountry).defaultLang === "ZH-TW"){
        $('#shop-more').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img class='float-right' alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-zh.png')}}' class='img img-responsive img-fluid' style='width:100%;' /></a>");
        $('#shop-more2').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-zh.png')}}' class='img img-responsive img-fluid' style='width:100%;' /></a>");
    } else if(JSON.parse(currentCountry).codeIso === "KR" && JSON.parse(currentCountry).defaultLang === "KO"){
        $('#shop-more').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img class='float-right' alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-ko.png')}}' class='img img-responsive img-fluid' style='width:100%;' /></a>");
        $('#shop-more2').html("<a href='{{ route('locale.region.product', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}' ><img alt='Shop More' src='{{URL::asset('/images/common/shop-more/order-shop-more-ko.png')}}' class='img img-responsive img-fluid' style='width:100%;' /></a>");
    }
</script>
@endsection
