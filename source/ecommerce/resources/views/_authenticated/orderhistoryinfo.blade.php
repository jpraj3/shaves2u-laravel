@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')
@php($m_h = null)
@php($m_h_sku = null)
@if(Session::has('country_handles'))
@php($m_h = Session::get('country_handles'))
@php($m_h_sku = $m_h->sku)
@endif
<link rel="stylesheet" href="{{ asset('css/_authenticated/orderhistory/orderhistory.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
<section class="orderhistory-info">
    <div class="container">
        <div class="row mx-0">
            @if($order_details)
                <div class="col-lg-12 padd0 pt-4 pb-4 text-center border-bottom">
                    <a href="{{ route('locale.region.authenticated.orderhistory', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                        <button id="button-back" class="button-back float-left fs-30-sm d-flex">
                            <i class="fa fa-angle-left fs-30"></i> <span class="d-none d-lg-block my-1 ml-3">@lang('website_contents.global.content.back')</span>
                        </button>
                    </a>
                    @php($orderNo = \App\Services\OrderService::formatOrderNumberV2($order_details->order,null,false))
                    <h1 class="MuliExtraBold">@lang('website_contents.authenticated.orderhistory.order_no',['orderNo' => $orderNo])</h1>
                    <p>@lang('website_contents.authenticated.orderhistoryinfo.placed_on', ['date' => \Carbon\Carbon::parse($order_details->order->created_at)])</p>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 padd0">
                            <div class="col-lg-12 border-bottom padd30">
                                <h4>@lang('website_contents.authenticated.orderhistoryinfo.account_details')</h4>
                                <label class="color-grey">@lang('website_contents.authenticated.orderhistoryinfo.email')</label>
                                <p class="fs-20">{{ $order_details->user->email }}</p>
                            </div>
                            <div class="col-lg-12 border-bottom padd30">
                                <h4>@lang('website_contents.authenticated.orderhistoryinfo.address')</h4>
                                <label class="color-grey">@lang('website_contents.checkout.address.shipping_address')</label>
                                <p class="fs-20">{{ $order_details->address->shipping->address }}<br>{{ $order_details->address->shipping->portalCode }} {{ $order_details->address->shipping->city }} {{ $order_details->address->shipping->state }}</p>
                            </div>
                            <div class="col-lg-12 padd30">
                                <h4>@lang('website_contents.checkout.payment_method.payment_method')</h4>
                                <div class="col-md-12 col-lg-12 pl-0 pr-0">
                                    <div class="col-10 offset-1" style="margin-left: 10px;">
                                        <div class="row justify-content-center">
                                            <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                                <div class="text-left">
                                                    <p class="fs-16 MuliSemiBold pt-3">XXXX XXXX XXXX {{ $order_details->receipt->last4 }}</p>
                                                </div>
                                                <div class="row justify-content-between pt-5 pb-4">
                                                    <div class="col-7">
                                                        <div class="text-left">
                                                            <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                            <p class="fs-18 MuliPlain">{{ $order_details->receipt->expiredMonth }} / {{ $order_details->receipt->expiredYear }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                                        @switch($order_details->receipt->branchName)
                                                        @case("Visa")
                                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid" style="position:absolute; bottom:0; max-width:65% !important;" />
                                                        @break
                                                        @case("MasterCard")
                                                        <img src="{{asset('/images/cards/logo-mastercard.png')}}" class="pr-2" style="position:absolute; bottom:0; width: 75%;" />
                                                        @break
                                                        @case("American Express")
                                                        <img src="{{asset('/images/cards/logo-amex.png')}}" class="pl-2" style="position:absolute; bottom:0; " />
                                                        @break
                                                        @default
                                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0;" />
                                                        @endswitch
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 padd0">
                            <div class="col-lg-12 padd30 pl-lg-5 pl-0 pr-0">
                                <div class="col-12 rounded-10 bg-white shadow-md padd30">
                                    <div class="row mx-lg-0">
                                        <h4 class="MuliExtraBold">@lang('website_contents.authenticated.orderhistoryinfo.order_summary')</h4>
                                        <div class="col-12 padd20 border-bottom">
                                            <div class="row">
                                                <div class="col-5 padd0 d-flex align-items-center">
                                                    @if($m_h_sku == 'H3')
                                                        {{-- start --}}
                                                        @if($order_details->subscription->subscription !== null && $order_details->subscription->plan !== null)
                                                            @if($order_details->subscription->subscription->isTrial == 1 && $order_details->subscription->subscription->isCustom == 0)
                                                                @if(strtoupper(substr($order_details->subscription->plan["plansku"], -2)) === '-W')
                                                                    <img class="img-fluid" src="{{asset('images/common/checkoutAssetsWomen/TrialKit-Women.jpg')}}"/>
                                                                @else
                                                                    <img class="img-fluid" src="{{asset('images/common/checkoutAssets/TrialKit-6blade.png')}}"/>
                                                                @endif
                                                            @elseif($order_details->subscription->subscription->isCustom == 1 && $order_details->subscription->subscription->isTrial == 0)
                                                                @if($m_h_sku == 'H3')
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                @elseif($m_h_sku == 'H1')
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                @else
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                @endif
                                                            @else
                                                                <img class="img-fluid" src="{{asset('/images/common/customplan.png')}}"/>
                                                            @endif
                                                        @else
                                                            @if(count(get_object_vars($order_details->orderdetailsinfo)) > 0)
                                                                {{-- Awesome Shave Kits --}}
                                                                @if(count(get_object_vars($order_details->orderdetailsinfo)) === 1)
                                                                    @if($order_details->orderdetailsinfo->{0}->ProductId === 1013 || $order_details->orderdetailsinfo->{0}->ProductId === 1014 || $order_details->orderdetailsinfo->{0}->ProductId === 1015)
                                                                        <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}"/>
                                                                    @else
                                                                        @if($m_h_sku == 'H3')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @elseif($m_h_sku == 'H1')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                        @else
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @endif
                                                                    @endif
                                                                @else
                                                                    @if($m_h_sku == 'H3')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @elseif($m_h_sku == 'H1')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                    @else
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                        {{-- end --}}
                                                    @elseif($m_h_sku == 'H1')
                                                        {{-- start --}}
                                                        @if($order_details->subscription->subscription !== null && $order_details->subscription->plan !== null)
                                                            @if($order_details->subscription->subscription->isTrial == 1 && $order_details->subscription->subscription->isCustom == 0)
                                                                @if(strtoupper(substr($order_details->subscription->plan["plansku"], -2)) === '-W')
                                                                    <img class="img-fluid" src="{{asset('images/common/checkoutAssetsWomen/TrialKit-Women.jpg')}}"/>
                                                                @else
                                                                    <img class="img-fluid" src="{{asset('images/common/premium/product/TrialKit-6blade.png')}}"/>
                                                                @endif
                                                            @elseif($order_details->subscription->subscription->isCustom == 1 && $order_details->subscription->subscription->isTrial == 0)
                                                                <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                            @else
                                                                <img class="img-fluid" src="{{asset('/images/common/customplan.png')}}"/>
                                                            @endif
                                                        @else
                                                            @if(count(get_object_vars($order_details->orderdetailsinfo)) > 0)
                                                                {{-- Awesome Shave Kits --}}
                                                                @if(count(get_object_vars($order_details->orderdetailsinfo)) === 1)
                                                                    @if($order_details->orderdetailsinfo->{0}->ProductId === 1013 || $order_details->orderdetailsinfo->{0}->ProductId === 1014 || $order_details->orderdetailsinfo->{0}->ProductId === 1015)
                                                                        <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}"/>
                                                                    @else
                                                                        @if($m_h_sku == 'H3')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @elseif($m_h_sku == 'H1')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                        @else
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @endif
                                                                    @endif
                                                                @else
                                                                    @if($m_h_sku == 'H3')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @elseif($m_h_sku == 'H1')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                    @else
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                        {{-- end --}}
                                                    @else
                                                        {{-- start --}}
                                                        @if($order_details->subscription->subscription !== null && $order_details->subscription->plan !== null)
                                                            @if($order_details->subscription->subscription->isTrial == 1 && $order_details->subscription->subscription->isCustom == 0)
                                                                @if(strtoupper(substr($order_details->subscription->plan["plansku"], -2)) === '-W')
                                                                    <img class="img-fluid" src="{{asset('images/common/checkoutAssetsWomen/TrialKit-Women.jpg')}}"/>
                                                                @else
                                                                    @if($m_h_sku == 'H3')
                                                                        <img class="img-fluid" src="{{asset('images/common/checkoutAssets/TrialKit-6blade.png')}}"/>
                                                                    @elseif($m_h_sku == 'H1')
                                                                        <img class="img-fluid" src="{{asset('images/common/premium/product/TrialKit-6blade.png')}}"/>
                                                                    @else
                                                                        <img class="img-fluid" src="{{asset('images/common/checkoutAssets/TrialKit-6blade.png')}}"/>
                                                                    @endif
                                                                @endif
                                                            @elseif($order_details->subscription->subscription->isCustom == 1 && $order_details->subscription->subscription->isTrial == 0)
                                                                @if($m_h_sku == 'H3')
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                @elseif($m_h_sku == 'H1')
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                @else
                                                                    <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                @endif
                                                            @else
                                                                <img class="img-fluid" src="{{asset('/images/common/customplan.png')}}"/>
                                                            @endif
                                                        @else
                                                            @if(count(get_object_vars($order_details->orderdetailsinfo)) > 0)
                                                                {{-- Awesome Shave Kits --}}
                                                                @if(count(get_object_vars($order_details->orderdetailsinfo)) === 1)
                                                                    @if($order_details->orderdetailsinfo->{0}->ProductId === 1013 || $order_details->orderdetailsinfo->{0}->ProductId === 1014 || $order_details->orderdetailsinfo->{0}->ProductId === 1015)
                                                                        <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}"/>
                                                                    @else
                                                                        @if($m_h_sku == 'H3')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @elseif($m_h_sku == 'H1')
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                        @else
                                                                            <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                        @endif
                                                                    @endif
                                                                @else
                                                                    @if($m_h_sku == 'H3')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @elseif($m_h_sku == 'H1')
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix-premium.png"/>
                                                                    @else
                                                                        <img style="width: 90%;" class="img-fluid" src="https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/plan-images-v2/ProductsBAMix.png"/>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                        {{-- end --}}
                                                    @endif
                                                </div>
                                                <div class="col-7 padd0 d-flex align-items-center">
                                                    <div class="col-12 padd0">
                                                        {{-- start --}}
                                                        @if($order_details->subscription->subscription !== null && $order_details->subscription->plan !== null)
                                                            {{-- Plans --}}
                                                            @if($order_details->subscription->subscription->isTrial === 1 && $order_details->subscription->subscription->isCustom === 0)
                                                                <h3 class="MuliBold">@lang('website_contents.authenticated.orderhistoryinfo.starter_kit')</h3>
                                                            @elseif($order_details->subscription->subscription->isCustom === 1 && $order_details->subscription->subscription->isTrial === 0)
                                                                <h3 class="MuliBold">@lang('website_contents.authenticated.orderhistoryinfo.shave_plan')</h3>
                                                            @else
                                                                <h3 class="MuliBold">@lang('website_contents.authenticated.orderhistoryinfo.plan')</h3>
                                                            @endif

                                                            @foreach($order_details->subscription->plan["product_info"] as $type)
                                                                @if($type["sku"] === 'S6/2018')
                                                                    <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.6_blade_cartridge')</p>
                                                                @elseif($type["sku"] === 'S5/2018')
                                                                    <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.5_blade_cartridge')</p>
                                                                @elseif($type["sku"] === 'S3/2018')
                                                                    <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.3_blade_cartridge')</p>
                                                                @endif
                                                            @endforeach

                                                        @else

                                                            {{-- Alacarte --}}
                                                            @if(count(get_object_vars($order_details->orderdetailsinfo)) > 0)

                                                                {{-- Awesome Shave Kits --}}
                                                                @if(count(get_object_vars($order_details->orderdetailsinfo)) === 1)
                                                                    @foreach($order_details->orderdetailsinfo as $item)
                                                                        @if($item->ProductId === 1013 || $item->ProductId === 1014 || $item->ProductId === 1015)
                                                                            <h3 class="MuliBold">@lang('website_contents.products.product.ask')</h3>
                                                                            @if($item->sku === 'ASK6/2018')
                                                                                <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.6_blade_cartridge')</p>
                                                                            @elseif($item->sku === 'ASK5/2018')
                                                                                <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.5_blade_cartridge')</p>
                                                                            @elseif($item->sku === 'ASK3/2018')
                                                                                <p class="fs-18 Muli">@lang('website_contents.authenticated.orderhistoryinfo.3_blade_cartridge')</p>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @else

                                                                {{-- Alacarte Product Listings --}}
                                                                @if(count(get_object_vars($order_details->orderdetailsinfo)) > 1)
                                                                    <h3 class="MuliBold">@lang('website_contents.authenticated.orderhistoryinfo.alacarte_orders')</h3>
                                                                    {{-- Purchased Products --}}
                                                                    <ul>
                                                                        <p>Purchased Products</p>
                                                                        @foreach($order_details->orderdetailsinfo as $item)
                                                                            @foreach($order_details->orderdetails as $_item)
                                                                                @if((int) $_item["ProductCountryId"] === (int) $item->ProductCountryId && $_item["isFreeProduct"] === 0 && $_item["isAddon"] === 0)
                                                                                    <li class="MuliBold">{{ $item->producttranslatesname }} x {{ $_item["qty"] }} </li>
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    </ul>
                                                                    {{-- Free Products --}}
                                                                    @if(isset($freeproductsexists) && $freeproductsexists)
                                                                        <ul>
                                                                            <p>Free Products</p>
                                                                            @foreach($order_details->orderdetailsinfo as $item)
                                                                                @foreach($order_details->orderdetails as $_item)
                                                                                    @if((int) $_item["ProductCountryId"] === (int) $item->ProductCountryId && $_item["isFreeProduct"] === 1 && $_item["isAddon"] === 0)
                                                                                        <li class="MuliBold">{{ $item->producttranslatesname }} x {{ $_item["qty"] }} </li>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endif
                                                    {{-- end --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mx-lg-0">
                                        <div class="col-12 py-3 px-lg-0 fs-18" style="border-bottom: 10px solid black;">
                                            <div class="row mx-0">
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.price')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    @if(strtolower($currentCountryIso) == 'kr')
                                                        <!-- Currency and price for kr -->
                                                        <?php
                                                            $s_oprice =  number_format($order_details->receipt->originPrice) ;
                                                        ?>
                                                        <p id="c-price">{{ $s_oprice }}{{ $order_details->country->currencyDisplay }}</p>
                                                    @else
                                                        <p id="c-price">{{ $order_details->country->currencyDisplay }} {{ $order_details->receipt->originPrice }}</p>
                                                    @endif
                                                </div>
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.quantity')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    <p id="c-quantity">1</p>
                                                </div>
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.subtotal')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    @if(strtolower($currentCountryIso) == 'kr')
                                                        <?php
                                                            $s_stprice =  number_format($order_details->receipt->subTotalPrice) ;
                                                        ?>
                                                        <p id="c-subtotal">{{ $s_stprice }}{{ $order_details->country->currencyDisplay }}</p>
                                                    @else
                                                        <p id="c-subtotal">{{ $order_details->country->currencyDisplay }} {{ $order_details->receipt->subTotalPrice }}</p>
                                                    @endif
                                                </div>
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.total')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    @if(strtolower($currentCountryIso) == 'kr')
                                                        <?php
                                                            $s_tprice =  number_format($order_details->receipt->totalPrice) ;
                                                        ?>
                                                        <p id="c-total">{{ $s_tprice }}{{ $order_details->country->currencyDisplay }}</p>
                                                    @else
                                                        <p id="c-total">{{ $order_details->country->currencyDisplay }} {{ $order_details->receipt->totalPrice }}</p>
                                                    @endif
                                                </div>
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.shipping_cost')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    @if(strtolower($currentCountryIso) == 'kr')
                                                        <?php
                                                            $s_sfprice =  number_format($order_details->country->shippingFee) ;
                                                        ?>
                                                        <p id="c-shipping">{{ $s_sfprice }}{{ $order_details->country->currencyDisplay }}</p>
                                                    @else
                                                        <p id="c-shipping">{{ $order_details->country->currencyDisplay }} {{ $order_details->country->shippingFee }}</p>
                                                    @endif
                                                </div>
                                                <div class="col-6 padd0">
                                                    <label class="col-12 padd0">@lang('website_contents.authenticated.orderhistoryinfo.promo_code')</label>
                                                </div>
                                                <div class="col-6 padd0 color-orange">
                                                    <p id="c-discount">@lang('website_contents.authenticated.orderhistoryinfo.none')</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 padd20 pl-0 pr-0">
                                        <div class="row mr-0 ml-0">
                                            <div class="col-6 padd0">
                                                <p class="MuliExtraBold fs-16 fs-20-lg mb-0">@lang('website_contents.authenticated.orderhistoryinfo.grand_total')</p>
                                            </div>
                                            <div class="col-6 padd0">
                                                @if(strtolower($currentCountryIso) == 'kr')
                                                    <?php
                                                        $o_tprice =  number_format($order_details->receipt->totalPrice) ;
                                                    ?>
                                                    <p class="MuliExtraBold fs-20 fs-24-lg mb-0"><span>{{ $o_tprice }}</span>{{ $order_details->country->currencyDisplay }}</p>
                                                @else
                                                    <p class="MuliExtraBold fs-20 fs-24-lg mb-0">{{ $order_details->country->currencyDisplay }} <span>{{ $order_details->receipt->totalPrice }}</span></p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

<script src="{{ asset('js/functions/_authenticated/orderhistory/orderhistory.function.js') }}"></script>


@endsection
