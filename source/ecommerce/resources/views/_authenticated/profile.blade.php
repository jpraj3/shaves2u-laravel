@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')

<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/_authenticated/profile/profile.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
<style>

    .hasError {
        color: red !important;
        padding-left: 0;
        padding-right: 0;
    }
    .swal2-confirm{
        background-color: transparent !important;
        color:black !important;
    }
    input.mask_content {
    -webkit-text-security: disc;
    }

    .add-card-container{
        width: 310px;
        height: auto;
        min-height: 174px;
        max-height: 25vh;
        font-size: 15px !important;
        margin-left: auto;
        margin-right: auto;
    }
    .card-container>.fs-18{
        font-size: 15px !important;
    }
    .save_btn_div{
        margin-top: 4% !important;
        max-width: 100%;
        margin: auto;
        height: 53px !important;
    }
    #pc_a_save{
        width: 100%;
        max-width: 95%;
        margin: auto;
    }

    #pu_existing_image{
        width: 100%;
        border-radius: 50%;
        max-width: 200px;
        height: auto;
    }

  .swal2-styled.swal2-confirm{
    border: solid 2px #FE5000 !important;
    border-radius: 5px;
    background: rgba (255, 255, 255, 0);
    padding: 14px 45px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: #FE5000 !important;
    margin: auto;
  }
  .btn-black:hover{
    color:white !important;
  }

</style>
<section class="personal-details">
{{-- <form
id="test_img2"
method="POST"
action="{{route('image.upload',['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']),'countryCode'=>strtolower(json_decode(session()->get('currentCountry'),true)['codeIso'])])}}"
enctype="multipart/form-data"
>
@csrf
<input type="file" name="file2" accept="image/*" id="file2">
<button type="submit">save</button>
</form> --}}

<div class="color-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 padd30">
                <div class="rounded-10 bg-white shadow padd-lg-50 padd-md-15">
                    <form id="form_pe_user" method="POST" action="" enctype="multipart/form-data">
                    @csrf
                        <div id="form_pe_user_update_fail"></div>
                        <div class="row marg-lg-0">
                        <div class="col-12 col-lg-5 pl-lg-0 pr-lg-3 text-center">
                            <div class="col-12 pl-lg-0 pr-lg-0">
                                <h3 class="color-orange MuliExtraBold">@lang('website_contents.checkout.user.personal_details')</h3>
                            </div>
                            <div id="pu_img_d" class="exp col-12
                             pt-3 pl-lg-0 pr-lg-0" style="padding: 5.5% 0; position: relative; padding-bottom: 1.7%">
                            @if(isset($user["user_profile_img"]))
                            <img id="pu_existing_image" src="{{ $user["user_profile_img"] }}" style="width: 200px; height: 200px; border-radius: 50%; object-fit:cover;">
                            @else
                            <img id="pu_existing_image" src="{{ asset('images/default/default_user_image.jpg') }}" style="width: 200px; height: 200px;border-radius: 50%;">
                            @endif
                            <i class="fa fa-plus-circle plus-icon-change-position" id="uploadTrigger" style="cursor:pointer; font-size:100px; position: absolute; transform: translate( -167%, 50% );" hidden></i>
                            </div>
                            <div>
                            <input hidden type="file" name="file" accept="image/*" id="file" value="">
                            </div>
                            <div class="form-group pt-3 pb-0 mb-0">
                                <label class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.checkout.user.name')</label>
                                <input disabled id="pu_e_fullname" name="pu_e_fullname"  type="text"
                                    class="fs-20 form-control border-lg-bottom text-center"
                                    value="{{ $user["firstName"] }}" style="background-color: white;" />
                                    <div id="pu_e_fullname_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="col-12 pl-lg-0 pr-lg-0 d-none d-lg-block">
                            <button id="pu_a_edit" type="button" onClick="p_user_actions('edit')" class="btn btn-load-more" style="width: 100%;"><b>@lang('website_contents.authenticated.profile.edit_profile')</b></button>
                            <button id="pu_a_save" type="button" onClick="p_user_actions('save')" hidden class="btn btn-black" style="width: 100%;"><b>@lang('website_contents.global.content.save')</b></button>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 pl-lg-0 pr-lg-0">
                            <div class="form-group pt-3 pb-0 mb-0">
                                <label class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.checkout.user.email')</label>
                                {{-- <p class="fs-18 form-control mb-0 pl-0">{{ $user["email"] }}</p> --}}
                                <input disabled id="pu_e_email" name="pu_e_email"  type="email" class="fs-20 form-control" style="background-color: white;word-wrap:break-word;" value="{{ $user["email"] }}">
                                <div id="pu_e_email_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group pt-3 pb-0 mb-0">
                                <label class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.checkout.user.password')</label>
                                <div class="input-group" id="show_hide_password">
                                    <input disabled id="pu_e_password" name="pu_e_password"  type="password" class="fs-20 form-control"
                                        placeholder="●●●●●●●●●●" style="background-color: white;">
                                    <div class="input-group-addon">
                                        <a id="password-eye" href="" class="fs-20" style="position:absolute; right: 10px; top: 5px; display:none;"><i
                                                class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            <div id="pu_e_password_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group pt-0 pb-0 mb-0">
                                <label class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.checkout.address.phone')</label>
                                <div class="input-group">
                                    <input disabled style="margin-left:0 !important; background-color: white;" id="pu_e_phoneext" name="pu_e_phoneext"  type="text" class="fs-18 col-2 ml-3 mb-0 form-control"
                                        value="+{{ view()->shared('callcode')->callingCode }}" readonly />
                                    <input disabled style="background-color: white;" id="pu_e_phone" name="pu_e_phone"  type="text" class="fs-18 col-10 ml-3 mb-0 form-control"
                                        value="{{ str_replace(("+".view()->shared('callcode')->callingCode), "", $user["phone"]) }}" onkeydown="return !(event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57));" />
                                </div>
                                <div id="pu_e_phone_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group pt-0 pb-0">
                                <label class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.checkout.user.date_of_birth')</label>
                                <div class="input-group">
                                    <select disabled id="pu_e_birthday_day" name="pu_e_birthday_day"  class="col-3 form-control custom-select mr-2 fs-18"
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}}); background-color: white;">
                                        @if ($user["birthday"] === null)
                                        <option value="" selected disabled hidden>DD</option>
                                        @for($i=1;$i<32;$i++)
                                        @switch($i) @case($i) @if($i < 10)
                                        <option value="{{sprintf("%02d", $i)}}">{{sprintf("%02d", $i)}}</option>
                                        @else
                                        <option value="{{ $i }}">{{$i}}</option>
                                        @endif
                                        @break
                                        @default
                                        @endswitch
                                        @endfor
                                        @else
                                        @for($i=1;$i<32;$i++)
                                        @switch($i) @case($i) @if($i < 10)
                                        <option value="{{sprintf("%02d", $i)}}" @if(explode('-', $user["birthday"])[2] == sprintf("%02d", $i)) selected="selected"@endif>{{sprintf("%02d", $i)}}</option>
                                        @else
                                        <option value="{{ $i }}" @if (explode('-', $user["birthday"])[2] == $i) selected="selected" @endif>{{$i}}</option>
                                        @endif
                                        @break
                                        @default
                                        @endswitch
                                        @endfor
                                        @endif
                                    </select>
                                    <select disabled id="pu_e_birthday_month" name="pu_e_birthday_month"  class="col-3 form-control custom-select ml-21 mr-2 fs-18"
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}}); background-color: white;>
                                        @if ($user["birthday"] === null)
                                        @for($i=1;$i<13;$i++)
                                        @switch($i) @case(1) <option value="{{ $i }}">Jan</option>
                                            @break
                                            @case(2)
                                            <option value="{{ $i }}">Feb</option>
                                            @break
                                            @case(3)
                                            <option value="{{ $i }}">Mar</option>
                                            @break
                                            @case(4)
                                            <option value="{{ $i }}">Apr</option>
                                            @break
                                            @case(5)
                                            <option value="{{ $i }}">May</option>
                                            @break
                                            @case(6)
                                            <option value="{{ $i }}">Jun</option>
                                            @break
                                            @case(7)
                                            <option value="{{ $i }}">Jul</option>
                                            @break
                                            @case(8)
                                            <option value="{{ $i }}">Aug</option>
                                            @break
                                            @case(9)
                                            <option value="{{ $i }}">Sep</option>
                                            @break
                                            @case(10)
                                            <option value="{{ $i }}">Oct</option>
                                            @break
                                            @case(11)
                                            <option value="{{ $i }}">Nov</option>
                                            @break
                                            @case(12)
                                            <option value="{{ $i }}">Dec</option>
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                        @else
                                        @for($i=1;$i<13;$i++)
                                        @switch($i)
                                            @case(1)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected"@endif>Jan</option>
                                            @break
                                            @case(2)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Feb</option>
                                            @break
                                            @case(3)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Mar</option>
                                            @break
                                            @case(4)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Apr</option>
                                            @break
                                            @case(5)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>May</option>
                                            @break
                                            @case(6)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Jun</option>
                                            @break
                                            @case(7)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Jul</option>
                                            @break
                                            @case(8)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Aug</option>
                                            @break
                                            @case(9)
                                            <option value="{{sprintf("%02d", $i)}}" @if(explode('-',$user["birthday"])[1]==sprintf("%02d", $i)) selected="selected" @endif>Sep</option>
                                            @break
                                            @case(10)
                                            <option value="{{ $i }}" @if (explode('-',$user["birthday"])[1]==$i) selected="selected" @endif>Oct</option>
                                            @break
                                            @case(11)
                                            <option value="{{ $i }}" @if (explode('-',$user["birthday"])[1]==$i) selected="selected" @endif>Nov</option>
                                            @break
                                            @case(12)
                                            <option value="{{ $i }}" @if (explode('-',$user["birthday"])[1]==$i) selected="selected" @endif>Dec</option>
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                            @endif
                                    </select>
                                    <select disabled id="pu_e_birthday_year" name="pu_e_birthday_year"  class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 form-control custom-select ml-2 fs-18"
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}}); background-color: white;">
                                        @if ($user["birthday"] === null)
                                        @for($i=0;$i<100;$i++)
                                        @switch($i)
                                        @case($i) @if($i < 10)
                                        <option value="19{{sprintf("%02d", $i)}}">19{{sprintf("%02d", $i)}}</option>
                                            @else
                                            <option value="19{{ $i }}">19{{$i}}</option>
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                            @for($i=0;$i<100;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf("%02d", $i)}}" >20{{sprintf("%02d", $i)}}</option>
                                            @else
                                            <option value="20{{ $i }}">20{{$i}}</option>
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                        @else
                                        @for($i=0;$i<100;$i++)
                                        @switch($i)
                                        @case($i) @if($i < 10)
                                        <option value="19{{sprintf("%02d", $i)}}" @if (explode('-',$user["birthday"])[0]=='19' .sprintf("%02d", $i)) selected="selected" @endif>19{{sprintf("%02d", $i)}}</option>
                                            @else
                                            <option value="19{{ $i }}" @if (explode('-',$user["birthday"])[0]=='19' .$i) selected="selected" @endif>19{{$i}}</option>
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                            @for($i=0;$i<100;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf("%02d", $i)}}" @if (explode('-',$user["birthday"])[0]=='20' .sprintf("%02d", $i)) selected="selected" @endif>20{{sprintf("%02d", $i)}}</option>
                                            @else
                                            <option value="20{{ $i }}" @if (explode('-',$user["birthday"])[0]=='20' .$i) selected="selected" @endif>20{{$i}}</option>
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                            @endif
                                    </select>
                                </div>
                                <div id="pu_e_birthday_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>
                        <div class="col-12 pl-lg-0 pr-lg-0 d-lg-none">
                            <button id="pu_a_edit_mob" type="button" onClick="p_user_actions('edit')" class="btn btn-black" style="width: 100%;"><b>@lang('website_contents.authenticated.profile.edit_profile')</b></button>
                            <button id="pu_a_save_mob" type="button" onClick="p_user_actions('save')" hidden class="btn btn-black" style="width: 100%;"><b>@lang('website_contents.global.content.save')</b></button>
                        </div>
                    </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="image_upload_zone"></div>
        <div id="stripe_otp_container" class="modal">
            <div class="modal-content" style="width: max-content;margin: auto;top: 20%;border-radious: none !important;background: white;border: none;border-radius: unset;">
              <div class="modal-container" id="stripe_otp_iframe"></div>
            </div>
        </div>
<script>
$("body").delegate("#uploadTrigger", "click", function (e) {
    e.preventDefault();
    $("#file").click();
});

$("body").delegate("#file", "change", function () {
    var getoldimg = $('#pu_existing_image').val();
    var uploadedfile = document.getElementById('file').value.replace(/C:\\fakepath\\/i, '');
    var file_ext = uploadedfile.substring(uploadedfile.lastIndexOf('.') + 1).toLowerCase();
    if ((file_ext == "png" || file_ext == "jpeg" || file_ext == "jpg" || file_ext == "ico")) {
        var noexeuploadedfile = uploadedfile.substring(0, uploadedfile.lastIndexOf('.'));
        if (uploadedfile.length > 12) {
            var newfilename = noexeuploadedfile.substr(0, 10) + '...' + noexeuploadedfile.substr(-2);
        } else {
            var newfilename = noexeuploadedfile;
        }
        readURL(this);
    } else {
        if (uploadedfile != "") {
            document.getElementById("file").value = "";
            $('#pu_existing_image').attr('src', '{{asset("images/default/default_user_image.png")}}/');
        } else {
            document.getElementById("file").value = "";
            $('#pu_existing_image').attr('src', '{{asset("images/default/default_user_image.png")}}/');
        }
    }
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#pu_existing_image').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
</div>
</section>



<section class="address-details pt-5">
    <div class="container" @if(strtolower(view()->shared('currentCountryIso')) == 'kr') style="padding: 0px;" @endif>
        <div class="row justify-content-between">
            <div class="col-9 col-sm-6 col-md-8 col-lg-9 col-xl-9" style="padding-left: 30px;">
                <h3 class="MuliExtraBold color-orange pt-2" onClick="myFunction('test','test2')">@lang('website_contents.checkout.address.address_details')</h3>
            </div>
            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
            <div class="col-3 d-block d-sm-none">
                <button onClick="addNewAddresskoMobile(1)" class="btn btn-add-new-logo text-center pl-0 pr-0" style="width: 50px;"><b><span class="fa fa-plus"></span></b></button>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 d-none d-sm-block" style="padding-right: 30px;">
                <button onClick="addNewAddressko(1)" class="btn btn-add-new w-100"><b>@lang('website_contents.authenticated.profile.add_new_address')</b></button>
            </div>
            @else
            <div class="col-3 d-block d-sm-none">
                <button onClick="addNewAddressMobile(1)" class="btn btn-add-new-logo text-center pl-0 pr-0" style="width: 50px;"><b><span class="fa fa-plus"></span></b></button>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 d-none d-sm-block" style="padding-right: 30px;">
                <button onClick="addNewAddress(1)" class="btn btn-add-new w-100"><b>@lang('website_contents.authenticated.profile.add_new_address')</b></button>
            </div>
            @endif
            @if($user_delivery)
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padd30 pb-0 d-flex" style="height:100%">
                <div class="col-lg-12 bg-white rounded-10 shadow padd-lg-50 padd-md-15 pb-4">
                    <h4 class="MuliExtraBold pb-4">@lang('website_contents.authenticated.profile.shipping_address_primary')</h4>
                    <p id="hide_address_data_delivery" class="MuliPlain fs-18 fs-14-sm">
                        @if(strtolower($currentCountryIso) == 'tw')
                            {{$user_delivery !== '' ? $user_delivery["SSN"] : ''}}
                            <?php if($user_delivery){ echo ", ";} ?>
                        @endif
                        {{$user_delivery !== '' ? $user_delivery["address"] : ''}}
                        <?php if($user_delivery){ echo ", ";} ?>
                        {{$user_delivery !== '' ? $user_delivery["portalCode"] : ' '}}
                        {{$user_delivery !== '' ? $user_delivery["city"] : ' '}}
                        <?php if($user_delivery){ echo ", ";} ?>
                        {{$user_delivery !== '' ? $user_delivery["state"] : ' '}}
                    </p>
                    <form id="form_pe_delivery">
                        <input type="hidden" name="update_address_type" value="delivery" />
                        @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                        <div id="show_edit_form_for_delivery" hidden class="col-lg-12 padd0 pl-0 pr-0">
                        <a href="javascript:;" onclick="koreanProfileDeliveryAddressPrefill('edit','no')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-delivery" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
                            @if(strtolower($currentCountryIso) == 'tw')
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.ssn')</label>
                                <input disabled name="pd_e_ssn" id="pd_e_ssn" type="text"
                                    class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["SSN"] : ' '}}" readonly="readonly" />
                                <div id="pd_e_ssn_error" class="hasError col-12 col-form-label text-left px-0"></div>
                            </div>
                            @endif
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.unit_details')</label>
                                <?php
                $delivery_address1=$user_delivery !== '' ? $user_delivery["address"] : '';
                $flat = '';
                $showaddress='' ;
                if( strpos($delivery_address1, ',') !== false){
                    $seperate = explode(",", $delivery_address1);
                    $showaddress= $seperate[0];
                    $flat= $seperate[1];
                }else{
                    $showaddress= $delivery_address1;
                }
                ?>
                                <input  name="pd_e_address1" id="pd_e_address1" type="text"
                                    class="fs-18 form-control"
                                    value="{{$showaddress}}" readonly="readonly" />
                                    <input name="pd_e_address" id="pd_e_address" type="hidden"
                                    class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["address"] : ''}}" readonly="readonly"/>
                                <div id="pd_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.flat')</label>
                                <input name="pd_e_flat" id="pd_e_flat" type="text" class="fs-18 form-control"
                                    value="{{ $flat }}" onchange="onChangeProfileDeliveryAddressUnitNumber(this.value, 'no')"/>
                                <div id="pd_e_flat_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.town_city')</label>
                                <input disabled name="pd_e_city" id="pd_e_city" type="text" class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["city"] : ' '}}" readonly="readonly" />
                                <div id="pd_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.state')</label>
                                <input disabled name="pd_e_state" id="pd_e_state" type="text" class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["state"] : ' '}}" readonly="readonly" />
                                <div id="pd_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                            <?php
                              $postcodeda = "";
                            if($user_delivery){
                             if($user_delivery["portalCode"] != "00000"){
                                $postcodeda = $user_delivery["portalCode"];
                              }
                            }

                                ?>
                                <label>@lang('website_contents.checkout.address.postal_code')</label>
                                <input disabled name="pd_e_portalCode" id="pd_e_portalCode" type="number"
                                    class="fs-18 form-control"
                                    value="{{$postcodeda}}" readonly="readonly" />
                                <div id="pd_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>
                        @else
                        <div id="show_edit_form_for_delivery" hidden class="col-lg-12 padd0 pl-0 pr-0">
                            @if(strtolower($currentCountryIso) == 'tw')
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.ssn')</label>
                                <input disabled name="pd_e_ssn" id="pd_e_ssn" type="text"
                                    class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["SSN"] : ''}}" />
                                <div id="pd_e_ssn_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            @endif
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.unit_details')</label>
                                <input disabled name="pd_e_address" id="pd_e_address" type="text"
                                    class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["address"] : ''}}" />
                                <div id="pd_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.town_city')</label>
                                <input disabled name="pd_e_city" id="pd_e_city" type="text" class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["city"] : ' '}}" />
                                <div id="pd_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.postal_code')</label>
                                <input disabled name="pd_e_portalCode" id="pd_e_portalCode" type="number"
                                    class="fs-18 form-control"
                                    value="{{$user_delivery !== '' ? $user_delivery["portalCode"] : ' '}}" />
                                <div id="pd_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.state')</label>
                                <select class="fs-18 padd0 form-control" id="pd_e_state" name="pd_e_state" disabled>
                                @if($states)
                               @foreach ($states as $s)
                               @if($user_delivery)
                              @if($user_delivery["state"] === $s["name"])
                                <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                               @else
                             <option value="{{$s['name']}}">{{$s["name"]}}</option>
                             @endif
                               @else
                             <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                @endif
                             @endforeach
                               @endif
                               </select>
                                <div id="pd_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>
                       @endif
                        <div class="col-lg-12 text-center padd20 pl-0  pr-md-30" style="bottom: 0px;">
                            <button id="pd_a_edit" type="button" onClick="p_address_actions('edit2','delivery')"
                                class="btn btn-load-more" style="width: 100%;"><b>@lang('website_contents.checkout.address.edit_address')</b></button>
                            <button id="pd_a_save" type="button" hidden onClick="p_address_actions('save','delivery')"
                                class="btn btn-load-more" style="width: 100%;"><b>@lang('website_contents.global.content.save')</b></button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @if($user_billing)
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padd30 pb-0 d-flex" style="height:100%">
                <div class="col-lg-12 bg-white rounded-10 shadow padd-lg-50 padd-md-15 pb-4">
                    <h4 class="MuliExtraBold pb-4">@lang('website_contents.authenticated.profile.billing_address_primary')</h4>
                    <p id="hide_address_data_billing" class="MuliPlain fs-18 fs-14-sm">{{$user_billing !== '' ? $user_billing["address"] : ''}}<?php if($user_billing){ echo ",";} ?>
                        {{$user_billing !== '' ? $user_billing["portalCode"] : ' '}} {{$user_billing !== '' ? $user_billing["city"] : ' '}}<?php if($user_billing){ echo ",";} ?> {{$user_billing !== '' ? $user_billing["state"] : ' '}}</p>
                    <form id="form_pe_billing">
                        <input type="hidden" name="update_address_type" value="billing" />
                        @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                        <div id="show_edit_form_for_billing" hidden class="col-lg-12 padd0 pl-0 pr-0">
                        <a href="javascript:;" onclick="koreanProfileBillingAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
               <div id="daum-postcode-wrap-edit-billing" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
                </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.unit_details')</label>
                                <?php
                $delivery_address1=$user_billing !== '' ? $user_billing["address"] : ' ';
                $flatb = '';
                $showaddressb='' ;
                if( strpos($delivery_address1, ',') !== false){
                    $seperate = explode(",", $delivery_address1);
                    $showaddressb= $seperate[0];
                    $flatb= $seperate[1];
                }else{
                    $showaddressb= $delivery_address1;
                }
                ?>
                                <input name="pb_e_address1" id="pb_e_address1" type="text"
                                    class="fs-18 form-control"
                                    value="{{$showaddressb}}"  readonly="readonly"/>
                                    <input disabled name="pb_e_address" id="pb_e_address" type="hidden"
                                    class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["address"] : ' '}}"  readonly="readonly"/>
                                <div id="pb_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.flat')</label>
                                <input name="pb_e_flat" id="pb_e_flat" type="text" class="fs-18 form-control"
                                    value="{{$flatb}}" onchange="onChangeProfileBillingAddressUnitNumber(this.value)"/>
                                <div id="pb_e_flat_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.town_city')</label>
                                <input disabled name="pb_e_city" id="pb_e_city" type="text" class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["city"] : ' '}}" readonly="readonly"/>
                                <div id="pb_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.state')</label>
                                <input disabled name="pb_e_state" id="pb_e_state" type="text"
                                    class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["state"] : ' '}}" readonly="readonly"/>
                                <div id="pb_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.postal_code')</label>
                                <input disabled name="pb_e_portalCode" id="pb_e_portalCode" type="number"
                                    class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["portalCode"] : ' '}}" readonly="readonly"/>
                                <div id="pb_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>
                        @else
                        <div id="show_edit_form_for_billing" hidden class="col-lg-12 padd0 pl-0 pr-0">
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.unit_details')</label>
                                <input disabled name="pb_e_address" id="pb_e_address" type="text"
                                    class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["address"] : ' '}}" />
                                <div id="pb_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.town_city')</label>
                                <input disabled name="pb_e_city" id="pb_e_city" type="text" class="fs-18 form-control"
                                    value="{{$user_billing !== '' ? $user_billing["city"] : ' '}}" />
                                <div id="pb_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.postal_code')</label>
                                <?php
                              $postcodeda = "";
                            if($user_billing){
                             if($user_billing["portalCode"] != "00000"){
                                $postcodeda = $user_billing["portalCode"];
                              }
                            }

                                ?>
                                <input disabled name="pb_e_portalCode" id="pb_e_portalCode" type="number"
                                    class="fs-18 form-control"
                                    value="{{$postcodeda}}" />
                                <div id="pb_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                            <div class="form-group mb-0">
                                <label>@lang('website_contents.checkout.address.state')</label>
                                <select class="fs-18 padd0 form-control" id="pb_e_state" name="pb_e_state" disabled>
                                @if($states)
                               @foreach ($states as $s)
                               @if($user_billing)
                              @if($user_billing["state"] === $s["name"])
                                <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                               @else
                             <option value="{{$s['name']}}">{{$s["name"]}}</option>
                             @endif
                               @else
                             <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                @endif
                             @endforeach
                               @endif
                               </select>
                                <div id="pb_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-12 text-center padd20 pl-0  pr-md-30" style="bottom: 0px;">
                            <button id="pb_a_edit" type="button" onClick="p_address_actions('edit2','billing')"
                                class="btn btn-load-more" style="width: 100%;"><b>@lang('website_contents.checkout.address.edit_address')</b></button>
                            <button id="pb_a_save" type="button" hidden onClick="p_address_actions('save','billing')"
                                class="btn btn-load-more" style="width: 100%;"><b>@lang('website_contents.global.content.save')</b></button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @if(!$user_delivery && !$user_billing)
            <div class="container pt-3"  style="padding-left:30px;">
                <div class="row p-0 text-left">
                    <div class="col-12 plan-column-show">
                        <p class="fs-20 mb-0">@lang('website_contents.authenticated.profile.not_given_address')</p>
                        <p class="fs-20">@lang('website_contents.authenticated.profile.update_address')</p>
                    </div>
                </div>
            </div>
            @endif

            <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 padd30"></div> -->
            <!-- @if(isset($nondefault_addresses) && count($nondefault_addresses) > 0)
            <div hidden id="btn_cancel_show_more_address" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 padd30" style="font-size:20px;text-align: end;padding: 30px 40px;vertical-align: middle !important;">
                <a onClick="showOtherAddresses('close')" class="" style="width: 100%;" style="text-decoration: underline !important;"><b style="cursor: pointer;text-decoration: underline !important;">Close<i class="fa fa-chevron-up"></i></b></a>
            </div>
            <div id="btn_show_more_address" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 padd30" style="font-size:20px;text-align: end;padding: 30px 40px;vertical-align: middle !important;">
                <a onClick="showOtherAddresses('show')" class="" style="width: 100%;" style="text-decoration: underline !important;font-size:20px;"><b style="cursor: pointer;text-decoration: underline !important;">Show More Addresses <i class="fa fa-chevron-down"></i></b></a>
            </div>
            @endif -->
        </div>


        <div class="col-12 pt-3" id="show_more_addresses" style="display:block;">
            <div class="row">
                <!-- <div class="col-12" style="padding: 10px 30px 0px 30px;">
                    <h1 class="MuliBold fs-20-sm fs-24-md" onClick="myFunction('test','test2')">Other Addresses
                    </h1>
                </div> -->
                @if(isset($nondefault_addresses))
                @php($oa_c = 0)
                @foreach($nondefault_addresses as $oa)
                @php($oa_c++)
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 padd30 d-flex">
                    <div class="col-lg-12 bg-white rounded-10 shadow padd-lg-50 padd-md-15 w-100 pb-2">
                        <p id="hide_address_data_{{ $oa_c }}" class="MuliPlain fs-18 fs-14-sm">{{$oa !== '' ? $oa["address"] : ''}},
                        {{$oa !== '' ? $oa["portalCode"] : ' '}} {{$oa !== '' ? $oa["city"] : ' '}}, {{$oa !== '' ? $oa["state"] : ' '}}</p>
                        <form id="form_pe_delivery_{{ $oa_c }}">
                            <input type="hidden" name="update_address_type_{{ $oa_c }}" value="delivery" />
                            <input type="hidden" name="update_address_id_{{ $oa_c }}" value="{{ $oa["id"] }}" />
                            <input type="hidden" name="count" value="{{ $oa_c }}" />
                            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                            <div id="show_edit_form_other_addresses_{{ $oa_c }}" class="col-lg-12 padd20 pl-0 pr-0" hidden>
                            <a href="javascript:;" onclick="koreanProfileDeliveryAddressPrefill('edit',{{ $oa_c }})" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
        우편번호검색</a>
    <div id="daum-postcode-wrap-edit-delivery-{{ $oa_c }}" style="display:none;border:1px solid;margin:5px 0;position:relative">
        <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
    </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                    <?php
        $delivery_address1= $oa !== '' ? $oa["address"] : '';
        ${"flat" . $oa_c} = '';
        ${"showaddress" . $oa_c}='' ;
        if( strpos($delivery_address1, ',') !== false){
            $seperate = explode(",", $delivery_address1);
            ${"showaddress" . $oa_c}= $seperate[0];
            ${"flat" . $oa_c}= $seperate[1];
        }else{
            ${"showaddress" . $oa_c}= $delivery_address1;
        }
        ?>
                                    <input name="pd_e_address1_{{ $oa_c }}"
                                        id="pd_e_address1_{{ $oa_c }}" type="text" class="fs-20 form-control"
                                        value="{{(${'showaddress' . $oa_c})}}" readonly="readonly" />
                                        <input disabled name="pd_e_address_{{ $oa_c }}"
                                        id="pd_e_address_{{ $oa_c }}" type="hidden" class="fs-20 form-control"
                                        value="{{$oa !== '' ? $oa["address"] : ''}}" readonly="readonly" />
                                    <div id="pd_e_address_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.flat')</label>
                                    <input name="pd_e_flat_{{ $oa_c }}" id="pd_e_flat_{{ $oa_c }}"
                                        type="text" class="fs-20 form-control"
                                        value="{{(${'flat' . $oa_c})}}" onchange="onChangeProfileDeliveryAddressUnitNumber(this.value, {{ $oa_c }})"/>
                                    <div id="pd_e_flat_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                    <input disabled name="pd_e_city_{{ $oa_c }}" id="pd_e_city_{{ $oa_c }}"
                                        type="text" class="fs-20 form-control"
                                        value="{{$oa !== '' ? $oa["city"] : ' '}}" readonly="readonly" />
                                    <div id="pd_e_city_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.state')</label>
                                    <input disabled name="pd_e_state_{{ $oa_c }}"
                                        id="pd_e_state_{{ $oa_c }}" type="text"
                                        class="fs-20 form-control"
                                        value="{{$oa !== '' ? $oa["state"] : ' '}}" readonly="readonly" />
                                    <div id="pd_e_state_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.postal_code')</label>
                                    <?php
                              $postcodeda = "";
                            if($oa){
                             if($oa["portalCode"] != "00000"){
                                $postcodeda = $oa["portalCode"];
                              }
                            }

                                ?>
                                    <input disabled name="pd_e_portalCode_{{ $oa_c }}"
                                        id="pd_e_portalCode_{{ $oa_c }}" type="number"
                                        class="fs-20 form-control"
                                        value="{{$postcodeda}}" readonly="readonly" />
                                    <div id="pd_e_portalCode_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                            </div>
                            @else
                            <div id="show_edit_form_other_addresses_{{ $oa_c }}" class="col-lg-12 padd20 pl-0 pr-0" hidden>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                    <input disabled name="pd_e_address_{{ $oa_c }}"
                                        id="pd_e_address_{{ $oa_c }}" type="text" class="fs-20 form-control"
                                        value="{{$oa !== '' ? $oa["address"] : ''}}" />
                                    <div id="pd_e_address_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                    <input disabled name="pd_e_city_{{ $oa_c }}" id="pd_e_city_{{ $oa_c }}"
                                        type="text" class="fs-20 form-control"
                                        value="{{$oa !== '' ? $oa["city"] : ' '}}" />
                                    <div id="pd_e_city_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.postal_code')</label>
                                    <?php
                              $postcodeda = "";
                            if($oa){
                             if($oa["portalCode"] != "00000"){
                                $postcodeda = $oa["portalCode"];
                              }
                            }

                                ?>
                                    <input disabled name="pd_e_portalCode_{{ $oa_c }}"
                                        id="pd_e_portalCode_{{ $oa_c }}" type="number"
                                        class="fs-20 form-control"
                                        value="{{$postcodeda}}" />
                                    <div id="pd_e_portalCode_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.address.state')</label>
                                    <select class="fs-20 padd0 form-control" id="pd_e_state_{{ $oa_c }}" name="pd_e_state_{{ $oa_c }}" disabled>
                                     @if($states)
                                      @foreach ($states as $s)
                                      @if($oa)
                                      @if($oa["state"] === $s["name"])
                                     <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                                      @else
                                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                        @endif
                                       @else
                                   <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                     @endif
                                  @endforeach
                                 @endif
                                   </select>

                                    <div id="pd_e_state_error_{{ $oa_c }}"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                            </div>
                            @endif
                            <div class="col-lg-12 text-center padd20 pl-0  pr-md-30" style=bottom: 0px;">
                                <button id="pd_a_edit_{{ $oa_c }}" type="button"
                                    onClick="p_address_actions('edit','delivery',{{ $oa_c }})" class="btn btn-load-more"
                                    style="width: 100%;"><b>@lang('website_contents.checkout.address.edit_address')</b></button>
                                <button id="pd_a_save_{{ $oa_c }}" type="button" hidden
                                    onClick="p_address_actions('save2','delivery_multiple', {{ $oa_c }})" class="btn btn-load-more"
                                    style="width: 100%;"><b>@lang('website_contents.global.content.save')</b></button>
                            </div>
                            <div class="col-12" style="padding: 0px;font-size: 15px;text-align: center;">
                                    <p class="col-12" onClick="setDefaultDelivery({{ $oa["id"] }},'delivery')" style="margin-bottom: 0px;font-weight:700;"><b style="cursor: pointer;text-decoration: underline !important;">Set as Primary Shipment</b></a>
                                    <p class="col-12" onClick="setDefaultBilling({{ $oa["id"] }},'billing')" style="font-weight:700;"><b style="cursor: pointer;text-decoration: underline !important;">Set as Primary Billing</b></a>
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
                @else

                @endif
            </div>
    </div>
    </div>
</section>

@if(strtolower($currentCountryIso) != 'kr')
<section class="payment-method pt-5 d-none d-sm-block d-md-block" id="add-card-section">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-9 col-sm-6 col-md-8 col-lg-9 col-xl-9" style="padding-left: 30px;">
                <h3 class="MuliExtraBold color-orange pt-2" onClick="myFunction('test','test2')">@lang('website_contents.checkout.payment_method.payment_method')</h3>
            </div>
            <div class="col-3 d-block d-sm-none">
                <button id="add_new_card" onClick="p_card_actions('show_add_form')" class="btn btn-add-new-logo" style="width: 80%"><img src="{{URL::asset('/images/common/icon_add.png')}}" class="img-fluid"/></button>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 d-none d-sm-block" style="padding-right: 30px;">
                <button id="add_new_card" onClick="p_card_actions('show_add_form')" class="btn btn-add-new" style="width: 100%;"><b>@lang('website_contents.checkout.payment_method.add_new_card')</b></button>
            </div>
            <div class="col-12" style="padding: 30px 30px 0px 35px;">
                @if(isset($cards) && count($cards) > 0)
                <div class="col-12 bg-white rounded-10 shadow padd-lg-20 padd-md-15 pt-2 pb-3">
                    <div class="row">
                        @php($cc_c = 0)
                        @foreach($cards as $pu_c)
                        <div class="col-xs-12 col-lg-6 p-0">
                            <div class="row justify-content-center m-0 pl-5 pr-5 pb-3 pt-5" >
                                <div class="col-12 border border-dark rounded pb-0"  style="display: table-cell; vertical-align: middle;">
                                    <input type="hidden" name="pu_u_default" id="pu_u_default" value="{{ $pu_c->id }}" />
                                    @if($pu_c->isDefault != 1)
                                    <div id="deleteCard" onClick="deleteCard({{ $pu_c->id }})" class="card-index ml-auto text-center fs-12">
                                        <i class="fa fa-trash-o pt-2" style="background: transparent; color: black; position: relative; font-size: 22px;"></i>
                                    </div>
                                    @else
                                    <div class="card-index ml-auto text-center fs-12">
                                 
                                    </div>
                                    @endif
                                    <div class="text-left">
                                        @if($pu_c->isDefault != 1)
                                        <p class="color-orange mb-0" id="" onClick="p_card_actions('change_default_card',{{ $pu_c->id }})">
                                            <b style="text-decoration: underline !important;">
                                                    @lang('website_contents.authenticated.profile.set_primary_card')
                                            </b>
                                        </p>
                                        @endif
                                        @if($pu_c->isDefault != 1)
                                            <p class="fs-20 MuliSemiBold pt-0">XXXX XXXX XXXX {{ $pu_c->cardNumber }}</p>
                                        @else
                                        @if( (strtolower($currentCountryIso) == 'tw') || (strtolower($currentCountryIso) == 'hk') && (strtolower(json_decode(session()->get('currentCountry'), true)['urlLang'])) == 'zh')
                                        <img src="{{asset('/images/common/tag-primary-zh.png')}}" class="img-fluid pt-2">
                                        @else
                                        <img src="{{asset('/images/common/tag-primary.png')}}" class="img-fluid pt-2">
                                        @endif
                                            <p class="fs-20 MuliSemiBold pt-2">XXXX XXXX XXXX {{ $pu_c->cardNumber }}</p>
                                        @endif
                                    </div>
                                    <div class="row justify-content-between">
                                    <div class="col-4">
                                        <div class="text-left pt-5">
                                            <p class="fs-16 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                            <p class="fs-18 MuliPlain">{{ $pu_c->expiredMonth }} / {{ $pu_c->expiredYear }}</p>
                                        </div>
                                    </div>
                                    <div class="col-4 pr-0 ml-1" style="padding-left: 7%">
                                        @switch($pu_c->branchName)
                                        @case("Visa")
                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid" style="position:absolute; bottom:0; padding-bottom: 2rem!important; max-width:65% !important;" />
                                        @break
                                        @case("MasterCard")
                                        <img src="{{asset('/images/cards/logo-mastercard.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0; padding-bottom: 1rem!important;" />
                                        @break
                                        @case("American Express")
                                        <img src="{{asset('/images/cards/logo-amex.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0; padding-bottom: 2rem!important; padding-left:25%;" />
                                        @break
                                        @default
                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0; padding-bottom: 2rem!important;" />
                                        @endswitch
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <!-- @if($pu_c->isDefault != 1)
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="" style="text-align:center;">
                                <p style="color:orange;font-weight:700;" class="" id=""
                                    onClick="p_card_actions('change_default_card',{{ $pu_c->id }})"><b
                                        style="text-decoration: underline !important;">Set as primary
                                        card</b></p>
                            </div>
                            @endif -->
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>


<section class="payment-method pt-5 d-sm-block d-md-none">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-9 col-sm-6 col-md-8 col-lg-9 col-xl-9" style="padding-left: 30px;">
                <h3 class="MuliExtraBold color-orange pt-2" onClick="myFunction('test','test2')">@lang('website_contents.checkout.payment_method.payment_method')</h3>
            </div>
            <div class="col-3 d-block d-sm-none">
                <button id="add_new_card" onClick="p_card_actions('show_add_form')" class="btn btn-add-new-logo" style="width: 50px;"><b><span class="fa fa-plus"></span></b></button>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 d-none d-sm-block" style="padding-right: 30px;">
                <button id="add_new_card" onClick="p_card_actions('show_add_form')" class="btn btn-add-new" style="width: 100%;"><b>@lang('website_contents.checkout.payment_method.add_new_card')</b></button>
            </div>
            <div class="col-12" style="padding: 30px 30px 0px 35px;">
                @if(isset($cards) && count($cards) > 0)
                <div class="col-12 bg-white rounded-10 shadow padd-lg-20 padd-md-15 pt-2 pb-3">
                    <div class="row">
                        @php($cc_c = 0)
                        @foreach($cards as $pu_c)
                        <div class="col-sm-12 col-lg-6 p-0">
                            <div class="row justify-content-center m-0 pl-3 pr-3 pb-3 pt-5" >
                                <div class="col-12 border border-dark rounded pb-0"  style="display: table-cell; vertical-align: middle;">
                                    <input type="hidden" name="pu_u_default" id="pu_u_default" value="{{ $pu_c->id }}" />
                                    @if($pu_c->isDefault != 1)
                                    <div id="deleteCard" onClick="deleteCard({{ $pu_c->id }})" class="card-index ml-auto text-center fs-12">
                                        <i class="fa fa-trash-o pt-2" style="background: transparent; color: black; position: relative; font-size: 22px;"></i>
                                    </div>
                                    @else
                                    <div class="card-index ml-auto text-center fs-12">
                                      
                                    </div>
                                    @endif
                                    <div class="text-left">
                                        @if($pu_c->isDefault != 1)
                                        <p class="color-orange mb-0" id="" onClick="p_card_actions('change_default_card',{{ $pu_c->id }})">
                                            <b style="text-decoration: underline !important;">
                                                    @lang('website_contents.authenticated.profile.set_primary_card')
                                            </b>
                                        </p>
                                        @endif
                                        @if($pu_c->isDefault != 1)
                                            <p class="fs-20 MuliSemiBold pt-0">XXXX XXXX XXXX {{ $pu_c->cardNumber }}</p>
                                        @else
                                        @if( (strtolower($currentCountryIso) == 'tw') || (strtolower($currentCountryIso) == 'hk') && (strtolower(json_decode(session()->get('currentCountry'), true)['urlLang'])) == 'zh')
                                        <img src="{{asset('/images/common/tag-primary-zh.png')}}" class="img-fluid pt-2">
                                        @else
                                            <img src="{{asset('/images/common/tag-primary.png')}}" class="img-fluid pt-2">
                                            @endif
                                            <p class="fs-20 MuliSemiBold pt-2">XXXX XXXX XXXX {{ $pu_c->cardNumber }}</p>
                                        @endif
                                    </div>
                                    <div class="row justify-content-between">
                                    <div class="col-7">
                                        <div class="text-left pt-3">
                                            <p class="fs-16 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                            <p class="fs-18 MuliPlain">{{ $pu_c->expiredMonth }} / {{ $pu_c->expiredYear }}</p>
                                        </div>
                                    </div>
                                    <div class="col-4 pr-0 ml-1" style="padding-left: 7%">
                                        @switch($pu_c->branchName)
                                        @case("Visa")
                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid" style="position:absolute; bottom:0; padding-bottom: 2rem!important; max-width:65% !important;" />
                                        @break
                                        @case("MasterCard")
                                        <img src="{{asset('/images/cards/logo-mastercard.png')}}" class="pr-2" style="position:absolute; bottom:0; padding-bottom: 1rem!important; width: 75%;" />
                                        @break
                                        @case("American Express")
                                        <img src="{{asset('/images/cards/logo-amex.png')}}" class="pl-2" style="position:absolute; bottom:0; padding-bottom: 1rem!important;" />
                                        @break
                                        @default
                                        <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0; padding-bottom: 2rem!important;" />
                                        @endswitch
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>

<section class="payment-method pt-5">
    <div class="container mb-5 p-0">
        <div id="pc_a_show_form" style="display: none; padding-top:45px !important;" class="ml-4 mr-4 pl-0 pr-0">
            <div class="col-12 bg-white rounded-10 shadow padd-lg-20 padd-md-15 pt-3 pb-3 d-lg-inline-flex">
                <div class="col-xs-12 col-lg-6 pl-0 pr-0">
                    <div class="col-12 d-sm-block d-md-none">
                        <div class="row justify-content-center">
                            <div class="col-12 border border-dark rounded pb-0"  style="display: table-cell; vertical-align: middle;">
                                <div class="text-left">
                                    <p class="fs-20 MuliSemiBold pt-4">XXXX XXXX XXXX 1234</p>
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-7">
                                        <div class="text-left">
                                            <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                            <p class="fs-18 MuliPlain">xx/xx</p>
                                        </div>
                                    </div>
                                    <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                        <div class="add-logo-change">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-none d-sm-block d-md-block" style="height:70%;">
                        <div class="row justify-content-center" style="width: 80%; height:210px; display:table;">
                            <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                <div class="text-left">
                                    <p class="fs-20 MuliSemiBold pt-3">XXXX XXXX XXXX 1234</p>
                                </div>
                                <div class="row justify-content-between pt-5">
                                    <div class="col-7">
                                        <div class="text-left">
                                            <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                            <p class="fs-18 MuliPlain">xx/xx</p>
                                        </div>
                                    </div>
                                    <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                        <div class="add-logo-change">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 save_btn_div mb-5">
                        <p>@lang('website_contents.authenticated.shaveplansedit.card_information')</p>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 pl-0 pr-0">
                    <form id="form_pe_card">
                        <div class="col-md-10 col-sm-12 pt-3 pb-0">
                            <div class="col-sm-12 pl-0 pr-0">
                                <div class="form-group">
                                    <label>@lang('website_contents.checkout.payment_method.card_number')</label>
                                    <input id="pc_e_cardnumber_masked" name="pc_e_cardnumber_masked" type="text"
                                        class="pu_a_cc_cardnumber number-only fs-20 form-control" />
                                    <input id="pc_e_cardnumber" name="pc_e_cardnumber" type="hidden"
                                        class="credit-card-logo pu_a_cc_cardnumber number-only"/>
                                    <div id="pc_e_cardnumber_error"
                                        class="hasError col-12 col-form-label text-left"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 pl-0 pr-0">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.expiry_date')</label>
                                            <input id="pc_e_expiry" name="pc_e_expiry" type="text"
                                                class="pu_a_cc_card_expiry_date number-only fs-20 form-control" />
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.cvv')</label>
                                            <input id="pc_e_cvv" maxlength="4" name="pc_e_cvv"
                                                type="text"
                                                class="mask_content number-only fs-20 form-control" />
                                        </div>
                                    </div>
                                    <div id="pc_e_expiry_error"
                                        class="hasError col-12 col-form-label text-left" style="padding-right: 15px; padding-left: 15px;"></div>
                                    <div id="pc_e_cvv_error"
                                        class="hasError col-12 col-form-label text-left" style="padding-right: 15px; padding-left: 15px;"></div>
                                        <div id="pc_e_card_error"
                                        class="hasError col-12 col-form-label text-left" style="padding-right: 15px; padding-left: 15px;"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-10 text-center pt-0 pl-0 pr-0">
                        <button disabled id="pc_a_save" type="button"
                            onClick="p_card_actions('save')" class="btn btn-load-more"
                            style="width: 310px;"><b>@lang('website_contents.global.content.save')</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
</div>

<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script src="{{ asset('js/helpers/laravel-helper.js') }}"></script>
<script src="{{ asset('js/helpers/sweetalert2.popups.js') }}"></script>
<script src="{{ asset('js/functions/_authenticated/profile/profile.function.js') }}"></script>
<script>
    let visa =  "<?php echo asset('/images/cards/logo-visa.png'); ?>";
    let mastercard =  "<?php echo asset('/images/cards/logo-mastercard.png'); ?>";
    let amex =  "<?php echo asset('/images/cards/logo-amex.png'); ?>";
</script>

<script>

let user_id = {!!json_encode(Auth::user()->id)!!};
$("#form_pe_delivery_popup").change(function (event) {
        let changes = $("#form_pe_delivery_popup").trackChanges();
        let changes2 = $("#form_pe_delivery_popup").isChanged()
        v_p_delivery_popup("form_pe_delivery_popup");
        let _register_btn = document.getElementById('pd_a_save_popup');
        if ($('#form_pe_delivery_popup').valid() === true) {
            console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_delivery_valid_popup = true;
        } else {
            console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_delivery_valid_popup = false;
        }
    })

//HOW TO USE
$('#pc_e_cardnumber_masked').keydown(function (event) {
    $(this).val(CreditCardFormat($('#pc_e_cardnumber').val()));
});

$('#pc_e_cardnumber_masked').on('input',function (event) {
    $('#pc_e_cardnumber').val(CreditCardFormat($(this).val()));
    $(this).val(CreditCardFormatMasked($('#pc_e_cardnumber').val()));
});

// $('#pc_e_cardnumber_masked').keyup(function (event) {
//     // simulate key event on another input
//     $('#pc_e_cardnumber').val(CreditCardFormat($('#pc_e_cardnumber').val(),event.key));

//     $(this).val(CreditCardFormatMasked($('#pc_e_cardnumber').val()));
// });

$('#pc_e_cardnumber_masked').blur(function (event) {
    // deselectd control
    $(this).val(CreditCardMaskAll($(this).val()));
});

$('.pu_a_cc_card_expiry_date').keyup(function () {
    $(this).val(ExpiryDateFormat($(this).val()));
});

//ENABLE NUMBER ONLY

$(".number-only").keydown(function (value) {
    if ((value.which < 48 || value.which > 57) && (value.which !== 8) && (value.which !== 0)) {
        return false;
    }
    return true;
});

//CREDIT CARD INPUT MASK
function CreditCardFormatMasked(value) {
    var v_masked = value.replace(/\s/g, '').replace(/.(?!$)/gi, '•');
    var matches_masked = v_masked.match(/.{4,16}/g);
    var match_masked = matches_masked && matches_masked[0] || '';
    var parts_masked = [];

    for (i=0, len=match_masked.length; i<len; i+=4) {
        parts_masked.push(match_masked.substring(i, i+4));
    }

    if (parts_masked.length) {
        return parts_masked.join(' ');
    } else {
        return v_masked;
    }
}

function CreditCardMaskAll(value) {
    var v_masked = value.replace(/\s/g, '').replace(/./gi, '•');
    var matches_masked = v_masked.match(/.{4,16}/g);
    var match_masked = matches_masked && matches_masked[0] || '';
    var parts_masked = [];

    for (i=0, len=match_masked.length; i<len; i+=4) {
        parts_masked.push(match_masked.substring(i, i+4));
    }

    if (parts_masked.length) {
        return parts_masked.join(' ');
    } else {
        return v_masked;
    }
}

function CreditCardFormat(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

    var matches = v.match(/.{4,16}/g);
    var match = matches && matches[0] || '';
    var parts = [];

    for (i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4));
    }

    if (parts.length) {
        return parts.join('');
    } else {
        return v;
    }
}

//EXPIRY DATE INPUT MASK
function ExpiryDateFormat(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{2,4}/g);
    var match = matches && matches[0] || ''
    var parts = []

    for (i=0, len=match.length; i<len; i+=2) {
        parts.push(match.substring(i, i+2))
    }

    if (parts.length) {
        return parts.join(' / ')
    } else {
        return value
    }
}


</script>


@endsection
