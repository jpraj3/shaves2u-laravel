@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')
<link rel="stylesheet" href="{{ asset('css/_authenticated/shave-plans/shave-plans.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<style>
    .hasError {
        color: red !important;
       margin:0px;
       padding:0px;
    }

    .hasError>label{
        margin-bottom:0px;
    }
    .spe_close_btn {background-image: url({{ asset('/images/common/spe_close.png')}});}
    .fa-pencil {
        color: white;
        font-size: 1em;
        border: 1px solid darkgrey;
        background: darkgrey;
        border-radius: 50px;
        padding: 2px 3px;
    }
    input.mask_content {-webkit-text-security: disc;}
    .btn-circle:focus {box-shadow:none !important;}
    .btn-circle {color:white !important;}
    .btn-circle:hover { color:white !important;}
    #spc_e_ccv-error{
        color: red !important;
        margin:0px;
       padding:0px;
    }
    .swal2-content{
        font-size: 1.3em !important;
    }

    .swal2-popup{
        /* width: 50vw !important; */
        max-height: 35vh !important;
    }

    .btn-black2 {
        display: block;
        border: none;
        background-color: #fe5000 !important;
        color: white !important;
        cursor: pointer;
        text-align: center;
        font-size: 16px;
        color: white;
        margin: auto;
        width: 100%;
    }

    @media screen and (min-width: 320px) and (max-width: 374px) {
        .btn-black2 {
            width: 80%;
        }
    }
    @media screen and (min-width: 375px) and (max-width: 767px){
        .btn-black2 {
            width: 40%;
        }
    }

    @media screen and (min-width: 768px) and (max-width: 1024px) {
        .btn-black2 {
            width: 50%;
        }
    }

    @media screen and (min-width: 1024px) {
        .btn-black2 {
            width: 35%;
        }
    }


</style>
<section class="shaveplans-edit">
    <input type="hidden" id="ori_spe_presaved_products" name="ori_spe_presaved_products" value=""/>
    <input type="hidden" id="spe_presaved_products" name="spe_presaved_products" value=""/>

    <div class="container">
        <div class="row">
            <div class="col-3 pt-4 px-lg-0">
                <a href="{{ route('locale.region.authenticated.savedplans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                    <button id="button-back" class="button-back float-left fs-30-sm d-flex">
                        <i class="fa fa-angle-left fs-30 text_uppercase"></i> <span class="d-none d-lg-block my-1 ml-3">@lang('website_contents.global.content.back')</span>
                    </button>
                </a>
            </div>
            <div class="col-6 pt-4 px-0 text-center">
                <h1 class="MuliExtraBold fs-24-sm mt-3">@lang('website_contents.authenticated.shaveplansedit.edit_shave_plan')</h1>
            </div>
            <div class="col-12 padd30">
                <div class="rounded-10 bg-white shadow padd30">
                    <div class="row mx-lg-0">
                        <div class="col-12 px-0">
                            <div class="row mx-0 align-items-center">
                                <div class="col-9 col-md-10 padd5 mb-2 pl-0">
                                    <h3 class="MuliExtraBold fs-16-sm fs-24-md mb-0">@lang('website_contents.authenticated.shaveplansedit.plan_details')</h3>
                                </div>
                                @if($data->subscription->status === 'Pending' || $data->subscription->status === 'Payment Failure' || $data->subscription->status === 'Cancelled')
                                <div class="status-inactive status-tag-edit col-3 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.inactive')</span>
                                </div>
                                @elseif($data->subscription->status === 'On Hold' || $data->subscription->status === 'Payment Failure' || $data->subscription->status === 'Cancelled')
                                <div class="status-onhold status-tag-edit col-3 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.on_hold')</span>
                                </div>
                                @elseif($data->subscription->status === 'Processing')
                                <div class="status-active status-tag-edit col-3 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.active')</span>
                                </div>
                                @endif
                            </div>
                            @if($data->plan_details !== null && $data->plan_details !== null)
                            @foreach($data->plan_details["product_info"] as $type)
                            @if(in_array($type["sku"],config('global.all.blade_types.skus')))
                            <h2 class="color-orange col-9 col-md-10 pl-0 fs-20-sm"><strong>{{ config('global.all.blade_no.'.$type["sku"]) }} @lang('website_contents.blade_packV2') @lang('website_contents.every') {{ $data->plan_details["isAnnual"] == 1 ? 12 : $data->subscription->duration }} @lang('website_contents.Months')</strong></h2>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>

                    <hr class="d-lg-none" style="margin-left: -30px; margin-right: -30px; width: auto;" />
                    <div class="row mx-lg-0 pt-3 bord-top-lg bord-bot-lg">
                        <div class="col-12 col-md-5 px-0 pr-lg-4 pb-4 fs-16 bord-right-lg">
                                <h4 class="MuliExtraBold fs-24-md">@lang('website_contents.authenticated.shaveplans.plan_products')</h4>
                                @if($data->defaultplanproducts !== null)
                                <input type="hidden" id="spe_defaultplanproducts" name="spe_defaultplanproducts" value="{{ $data->defaultplanproducts }}" />
                                @endif
                                @php($existing_plan_products = 0)
                                @if($data->plan_details["plantype"] === "trial")
                                @php($plantype = "trial")
                                @elseif($data->plan_details["plantype"] === "custom")
                                @php($plantype = "custom")
                                @endif

                                {{-- Default plan products --}}

                                @foreach ($data->plan_details["product_info"] as $product)
                                @if(!in_array($product["sku"], config('global.all.handle_types.trial.skus')))
                                @php($existing_plan_products++)
                                <input type="hidden"
                                id="existing_plan_products_{{ $existing_plan_products }}"
                                name="existing_plan_products_{{ $existing_plan_products }}" value="{{ $product["productcountryid"] }}" />
                                <input type="hidden"
                                id="existing_plan_products_addon_{{ $existing_plan_products }}"
                                name="existing_plan_products_addon_{{ $existing_plan_products }}" value="false" />
                                <div class="col-12 padd0" id="spe_updateSubsProductList_{{ $existing_plan_products }}">
                                    <div class="row pt-4 mx-0 border-bottom align-items-center" >
                                        <div class="col-4 text-center">
                                            <img class="img-fluid image_selected_blade" style="max-height:100px;" src="{{ $product["product_default_image"] }}" />
                                        </div>
                                        <div class="col-6">
                                            <b><span class="text_selected_blade">{{ $product["productname"]}}{{ $data->plan_details["isAnnual"] == 1 ? ' x ' .$product["product_qty"] . '' : '' }}</span></b>
                                        </div>
                                        <div class="col-2 text-right padd0">
                                            @if(in_array($product["sku"],config('global.all.blade_types.skus')))
                                                @if($data->available_plans->country_of_origin_matches == true)
                                                <button type="button" onClick="pre_changeBlade(null,1)" id=""  class="remove_selected_blade btn btn-circle" ><i class="fa fa-pencil"></i></button>
                                                @endif
                                            @else
                                                @if($plantype === "trial" && in_array($product["sku"], config('global.all.handle_types.trial.skus')))
                                                @else
                                                    @if($data->plan_details["isAnnual"] == 0 && $data->available_plans->country_of_origin_matches == true)
                                                    <button type="button" onClick="pre_removeSubsProducts({{ $product['productcountryid'] }})" id=""  class="remove_selected_blade btn btn-circle" style="background-image: url({{asset('/images/common/spe_close.png')}});"></button>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if(in_array($product["sku"],config('global.all.blade_types.skus')))
                                <div style="display:none;padding:0px;" class="col-12" id="show_blade_options">
                                    @if($data->available_plans)
                                    @php($total_types = 0)
                                    @foreach($data->available_plans->blade_types as $blade_types)
                                    @php($total_types++)
                                    @php($blade_name = config('global.all.blade_no.'.$blade_types->sku))
                                    @if($product["sku"] !== $blade_types->sku)
                                    <div class="row pt-4 align-items-center mx-0 border-bottom pl-0" id="blade_type_{{ $total_types }}" onClick="pre_changeBlade({{ $blade_types->ProductCountryId }},2)" style="cursor:pointer;">
                                        <div class="col-4 text-center pl-0">
                                            <img class="img-fluid image_selected_blade" style="max-height:100px;" src="{{ $blade_types->product_default_image }}">
                                        </div>
                                        <div class="col-6 pl-2">
                                            <b><span class="text_selected_blade">@lang('website_contents.global.blade', ["blade" => (int) $blade_name])</span></b>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                                @endif
                                @endif
                                @endforeach
                                
                                {{-- Default addon products --}}

                                @if($data->defaultaddons !== null)
                                @foreach ($data->defaultaddons as $productaddon)
                                @php($existing_plan_products++)
                                <input type="hidden"
                                id="existing_plan_products_{{ $existing_plan_products }}"
                                name="existing_plan_products_{{ $existing_plan_products }}" value="{{ $productaddon["ProductCountryId"] }}" />
                                <input type="hidden"
                                id="existing_plan_products_addon_{{ $existing_plan_products }}"
                                name="existing_plan_products_addon_{{ $existing_plan_products }}" value="false" />
                                <div class="col-12 padd0" id="spe_updateSubsProductList_{{ $existing_plan_products }}">
                                    <div class="row pt-4 mx-0 border-bottom align-items-center pb-3">
                                        <div class="col-4 text-center">
                                            <img class="img-fluid image_selected_blade" style="max-height:70px;" src="{{ $productaddon["url"] }}" />
                                        </div>
                                        <div class="col-6">
                                            <b><span class="text_selected_blade">{{ $productaddon["name"]}}{{ $data->plan_details["isAnnual"] == 1 ? ' x ' .$productaddon["AddonsQty"] . '' : '' }}</span></b>
                                        </div>
                                        <div class="col-2 text-right padd0">
                                            @if($data->plan_details["isAnnual"] == 0)
                                            <button type="button" onClick="pre_removeSubsProducts({{ $productaddon["ProductCountryId"] }})" id=""  class="remove_selected_blade btn btn-circle" style="background-image: url({{asset('/images/common/spe_close.png')}});"></button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                <div class="col-12 padd0" id="spe_updateSubsProductList"></div>
                                @if($data->plan_details["isAnnual"] == 0 && $data->gender == 'male' && $data->available_plans->country_of_origin_matches == true)
                                <div class="row pt-4 mx-0">
                                    <button  id="addon_products_to_subscription" type="button" class="btn btn-load-more w-100" data-toggle="modal" data-target="#addProduct">@lang('website_contents.authenticated.shaveplansedit.add_new_product')</button>
                                </div>
                                @else
                                <div class="row pt-4 mx-0">
                                    <button  id="addon_products_to_subscription" type="button" class="btn btn-load-more w-100"  hidden>@lang('website_contents.authenticated.shaveplansedit.add_new_product')</button>
                                </div>
                                @endif
                                <form id="form-spe-products" action="">
                                        <input type="hidden" id="spe_to_update_products" name="spe_to_update_products" value="" />
                                </form>
                        </div>
                        <div class="col-12 col-md-7 pl-lg-4 px-0 fs-16">
                            <hr class="d-lg-none" style="margin-left: -15px; margin-right: -15px; width: auto;" />
                            <form id="form-spe-frequency" action="">
                                <div class="row mx-0 pb-3 pt-3 pt-lg-0">
                                    <div class="col-12 col-md-6 px-0">
                                        <h4 class="MuliExtraBold fs-20">@lang('website_contents.authenticated.shaveplansedit.frequency')</h4>
                                    </div>
                                    <div class="col-12 col-md-6 px-0">
                                        <form id="spe-select-frequency">
                                            <input type="hidden" name="subs_id" id="subs_id" value="{{ $data->subscription->id }}" />
                                            <select @if($data->plan_details["isAnnual"] == 1 || count($data->available_plans->active_plans) < 1 || $data->available_plans->country_of_origin_matches == false) disabled @endif name="spf_e_frequency" id="spf_e_frequency" class="form-control custom-select mr-2" style="background-image: url({{asset('/images/common/Icon_Arrow-Left-1@2x.png')}}); " >
                                                @if(count($data->available_plans->active_plans) > 0 && $data->available_plans->country_of_origin_matches == true)
                                                @foreach ($data->available_plans->active_plans as $types)
                                                @if($data->plan_details["isAnnual"] == 1)
                                                <option value="{{ $types['duration'] }}" @if($data->subscription->duration == $types['duration']) selected="selected"@endif>12 @lang('website_contents.Months')</option>
                                                @else
                                                <option value="{{ $types['duration'] }}" @if($data->subscription->duration == $types['duration']) selected="selected"@endif>{{ $types['duration'] }} @lang('website_contents.Months')</option>
                                                @endif
                                                @endforeach
                                                @else
                                                <option value="" disabled selected="selected">-</option>
                                                @endif
                                            </select>
                                            <div style="display:none;" id="spf_e_frequency_error" class="hasError col-12 col-form-label text-left">
                                                @lang('validation.custom.validation.plan.frequency.required')
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </form>

                            <div class="row mx-0 pb-3 pt-3 pt-lg-0">
                                <div class="col-12 col-md-6 px-0">
                                    <h4 class="MuliExtraBold fs-20">@lang('website_contents.authenticated.shaveplans.next_billing')</h4>
                                </div>
                                <div class="col-12 col-md-6 px-0">
                                @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                                <!-- Currency and price for kr -->
                                   <h4 class="MuliExtraBold fs-24-md"><span id="total-subprice">{{ number_format($data->subscription->pricePerCharge) }}</span>{{$data->country->currencyDisplay}}</h4>
                                @else
                                <h4 class="MuliExtraBold fs-24-md">{{$data->country->currencyDisplay}}<span id="total-subprice">{{ $data->subscription->pricePerCharge }}</span></h4>
                                @endif
                                    @if($data->subscription->nextChargeDate !== null)
                                    <h4 class="Muli fs-24-md">{{ \Carbon\Carbon::parse($data->subscription->nextChargeDate)->format('d M Y') }}</h4>
                                    @else
                                    <p class="Muli">@lang('website_contents.under_trial')</p>
                                    @endif
                                    <h4 class="Muli fs-16"><a class="fs-16 color-orange" data-toggle="modal" data-target="#shipRefills"><u>@lang('website_contents.authenticated.shaveplans.refill_now')</u></a></h4>
                                </div>
                            </div>

                            <form id="form-spe-promotions" action="">
                                <div id="promotion_selection" class="{{ $data->subscription->promoCode == null || $data->subscription->promotionId == null ? 'd-none' : ''}} promotion-selection row mx-0 pb-3 pt-3 pt-lg-0">
                                    <div class="col-12 col-md-6 px-0">
                                        <h4 class="MuliExtraBold fs-20">@lang('website_contents.authenticated.shaveplansedit.promotion')</h4>
                                    </div>
                                    @php($promo_name = $data->subscription->promoCode ?  $data->subscription->promoCode : '-')
                                    @php($existing_promo_name = $data->subscription->promoCode ?  $data->subscription->promoCode : '')
                                    @php($existing_promo_id = $data->subscription->promotionId ?  $data->subscription->promotionId : '')
                                    @php($existing_cancellation = $data->subscription->temp_discountPercent > 0 ? true : false)
                                    @php($existing_cancellation_discount = $data->subscription->temp_discountPercent)
                                    <input type="hidden" name="spe_existing_promo_name" id="spe_existing_promo_name" value="{{ $existing_promo_name }}" />
                                    <input type="hidden" name="spe_existing_promo_id" id="spe_existing_promo_id" value="{{ $existing_promo_id }}" />
                                    <input type="hidden" name="spe_applied_promo_name" id="spe_applied_promo_name" value="" />
                                    <input type="hidden" name="spe_applied_promo_id" id="spe_applied_promo_id" value="" />
                                    <input type="hidden" name="spe_applied_cancellation" id="spe_applied_cancellation" value="{{ $existing_cancellation }}" />
                                    <input type="hidden" name="spe_applied_temp_discount" id="spe_applied_temp_discount" value="{{ $existing_cancellation_discount }}" />
                                    <div class="col-12 col-md-6 px-0">
                                        <select  id="applied_promo" class="form-control custom-select mr-2" style="background-image: url({{asset('/images/common/Icon_Arrow-Left-1@2x.png')}});">
                                            @foreach($data->existing_promotions as $promotions)
                                            @if($promotions->type === 'normal')
                                            <option value="{{ $promotions->info->pid }}">{{ $promotions->code }}</option>
                                            @elseif($promotions->type === 'cancellation')
                                            <option selected value="cancellation">@lang('website_contents.authenticated.shaveplansedit.voucher',['promo' => $promotions->discount ])</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mx-0 pb-3 pt-3 pt-lg-0">
                                    <div class="col-12 col-md-6 px-0">
                                        <h4 class="MuliExtraBold fs-20">@lang('website_contents.authenticated.shaveplansedit.enter_promo_code')</h4>
                                    </div>
                                    <div class="col-12 col-md-6 px-0">
                                        <div class="input-group">
                                            <input id="spp_e_promoCode" type="text" class="col-9 form-control" placeholder="@lang('website_contents.authenticated.shaveplansedit.promo_code')"  />
                                            <button  id="apply_promo_use_btn" type="button" class="col-3 btn btn-use" onClick="pre_applyPromo()">@lang('website_contents.authenticated.shaveplansedit.use')</button>
                                        </div>
                                        <p class="error-promotion" id="error-promotion"></p>
                                        <p class="success-promotion" id="success-promotion"></p>
                                    </div>
                                </div>
                            </form>
                            <div class="col-12 px-0 pt-3 pt-lg-0">
                                <h4 class="MuliExtraBold fs-20 mb-0 mb-lg-2">@lang('website_contents.authenticated.shaveplans.payment_method')</h4>
                            </div>

                            <div class="row mx-0 pb-3 pt-3 pt-lg-0">
                            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                            <div class="col-12 col-md-12 px-0 d-flex pt-3">
                                    <form id="form_spe_card" method="POST" action="">
                                            <div class="col-12 padd0">
                                                    <form id="form_pe_card">
                                                        <div class="col-lg-10 pl-0 pr-0">
                                                        <div class="row marg-lg-0">
                                                         <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-6 pl-lg-0">
                                                            <div class="form-group">
                                                                <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.card_number')</label>
                                                                <input  id="spc_e_cardNumber_masked" maxlength="19" name="spc_e_cardNumber_masked" type="text"
                                                                    class="spc_e_cardNumber_masked number-only fs-18 form-control"
                                                                    placeholder="xxxx xxxx xxxx xxxx" />
                                                                <input  id="spc_e_cardNumber" maxlength="19" name="spc_e_cardNumber" type="hidden"
                                                                    class="spc_e_cardNumber number-only fs-18"
                                                                    placeholder="xxxx xxxx xxxx xxxx" />
                                                                <div id="spc_e_cardNumber_error" class="hasError col-12 col-form-label text-left"></div>
                                                            </div>
                                                         </div>
                                                         <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-6 pr-lg-0">
                                                                    <div class="form-group">
                                                                        <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.expiry_date')</label>
                                                                        <input  maxlength="10" id="spc_e_expirydate" name="spc_e_expirydate" type="text"
                                                                            class="spc_e_expirydate number-only fs-18 form-control"
                                                                            placeholder="xx / xxxx" />
                                                                    </div>
                                                                </div>
                                                        </div>
                                                            <div class="row marg-lg-0">
                                                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 pl-lg-0">
                                                                    <div class="form-group">
                                                                        <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.card_birth')</label>
                                                                        <input  maxlength="6" id="spc_e_card_birth" name="spc_e_card_birth" type="text"
                                                                            class="spc_e_card_birth number-only fs-18 form-control"
                                                                            placeholder="YYMMDD" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pr-lg-0">
                                                                    <div class="form-group">
                                                                        <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.card_password')</label>
                                                                        <input placeholder="비밀번호 첫 두자리" id="spc_e_card_password" maxlength="2" name="spc_e_card_password" type="text"
                                                                            class="mask_content number-only fs-18 form-control"
                                                                            placeholder="xx" />
                                                                    </div>
                                                                </div>
                                                                <div id="spc_e_expirydate_error" class="hasError col-12 col-form-label text-left"></div>
                                                                <div id="spc_e_ccv_error2" class="hasError col-12 col-form-label text-left"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="col-lg-12 padd20 pl-0 pr-0">
                                                        <p>@lang('website_contents.authenticated.shaveplansedit.card_information')</p>
                                                    </div>
                                                </div>
                                    </form>
                                </div>
                            @else
                                    <div class="col-md-12 col-lg-6 pl-0 pr-0">
                                            <div class="col-10 offset-1" style="margin-left: 0;">
                                                <div class="row justify-content-center pt-4" style="height:90%; display:table;">
                                                    <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                                        <div class="text-left">
                                                            <p class="fs-16 MuliSemiBold pt-3">XXXX XXXX XXXX {{ $data->subscription_card->cardNumber }}</p>
                                                        </div>
                                                        <div class="row justify-content-between">
                                                        <div class="col-7">
                                                            <div class="text-left">
                                                                <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                                <p class="fs-16 MuliPlain">{{ $data->subscription_card->expiredMonth }} / {{ $data->subscription_card->expiredYear }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 pr-0 ml-1" style="padding-left: 7%">
                                                            @switch($data->subscription_card->branchName)
                                                            @case("Visa")
                                                            <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid" style="position:absolute; bottom:0; padding-bottom: 2rem!important; max-width:65% !important;" />
                                                            @break
                                                            @case("MasterCard")
                                                            <img src="{{asset('/images/cards/logo-mastercard.png')}}" class="img-fluid pb-3 pr-4" style="position:absolute; bottom:0; padding-bottom: 1rem!important;" />
                                                            @break
                                                            @case("American Express")
                                                            <img src="{{asset('/images/cards/logo-amex.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0;" />
                                                            @break
                                                            @default
                                                            <img src="{{asset('/images/cards/logo-visa.png')}}" class="img-fluid pb-3" style="position:absolute; bottom:0; padding-bottom: 2rem!important;" />
                                                            @endswitch
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <div style="" class="col-12 col-md-6 px-0 d-flex pt-3">
                                    <form id="form_spe_card" method="POST" action="">
                                            <div class="col-12 padd0">
                                                    <form id="form_pe_card">
                                                        <div class="col-lg-10 pl-0 pr-0">
                                                            <div class="form-group">
                                                                <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.card_number')</label>
                                                                <input  id="spc_e_cardNumber_masked" maxlength="19" name="spc_e_cardNumber_masked" type="text"
                                                                    class="spc_e_cardNumber_masked number-only fs-18 form-control"
                                                                    placeholder="xxxx xxxx xxxx xxxx" />
                                                                <input  id="spc_e_cardNumber" maxlength="19" name="spc_e_cardNumber" type="hidden"
                                                                    class="spc_e_cardNumber number-only fs-18"
                                                                    placeholder="xxxx xxxx xxxx xxxx" />
                                                                <div id="spc_e_cardNumber_error" class="hasError col-12 col-form-label text-left"></div>
                                                            </div>
                                                            <div class="row marg-lg-0">
                                                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 pl-lg-0">
                                                                    <div class="form-group">
                                                                        <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.expiry_date')</label>
                                                                        <input  maxlength="10" id="spc_e_expirydate" name="spc_e_expirydate" type="text"
                                                                            class="spc_e_expirydate number-only fs-18 form-control"
                                                                            placeholder="xx / xx" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pr-lg-0">
                                                                    <div class="form-group">
                                                                        <label class="fs-12">@lang('website_contents.authenticated.shaveplansedit.cvc')</label>
                                                                        <input  id="spc_e_ccv" maxlength="4" name="spc_e_ccv" type="text"
                                                                            class="mask_content number-only fs-18 form-control"
                                                                            placeholder="xxx" />
                                                                    </div>
                                                                </div>
                                                                <div id="spc_e_expirydate_error" class="hasError col-12 col-form-label text-left"></div>
                                                                <div id="spc_e_ccv_error2" class="hasError col-12 col-form-label text-left"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="col-lg-12 padd20 pl-0 pr-0">
                                                        <p>@lang('website_contents.authenticated.shaveplansedit.card_information')</p>
                                                    </div>
                                                </div>
                                    </form>
                                </div>
                                {{-- <div class="col-12 text-center">
                                <button type="button" onClick="otpTest({{$data->subscription->id}},'new')">OTP TEST</button>
                                <div id="stripe_otp_container" class="modal">
                                    <div class="modal-content" style="width: max-content;margin: auto;top: 20%;border-radious: none !important;background: white;border: none;border-radius: unset;">
                                      <div class="modal-container" id="stripe_otp_iframe"></div>
                                    </div>
                                  </div>
                                </div> --}}
                             @endif
                                 <div class="col-12 px-0 pt-3">
                                    <h4 class="MuliExtraBold ">@lang('website_contents.authenticated.shaveplansedit.address')</h4>
                                </div>
                                <div class="col-12 px-0 pt-3">
                                </div>
                                <ul id="combined-address" class="nav nav-tabs border-0 Muli fs-16 font-weight-bold">
                                    <li class="active padd15 px-0">
                                        <span id="address-placeholder" class="color-orange">@lang('website_contents.authenticated.shaveplansedit.shipping_billing')</span>
                                    </li>
                                </ul>
                                <ul id="separate-address" class="nav nav-tabs border-0 Muli fs-16 font-weight-bold">
                                    <li class="active padd15 px-0">
                                        <a id="shipping-address-toggle" class="color-orange active" data-toggle="tab" href="#shipping">@lang('website_contents.authenticated.shaveplansedit.shipping')</a>
                                    </li>
                                    <span class="padd15">|</span>
                                    <li class="padd15 px-0">
                                        <!-- If different billing address is checked -->
                                        <a id="billing-address-toggle" class="color-orange d-none" data-toggle="tab" href="#billing">@lang('website_contents.authenticated.shaveplansedit.billing')</a>
                                        <!-- If different billing address is not checked -->
                                        <span id="billing-address-placeholder">@lang('website_contents.authenticated.shaveplansedit.billing')</span>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                        @if (isset($data->address))
                                    <div id="shipping" class="tab-pane in active">
                                        <form id="form_spe_shipping">
                                            <div class="col-md-12 px-0">
                                            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                                            <a href="javascript:;" onclick="koreanEditDeliveryAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-delivery" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                                                    <?php
                $delivery_address1=$data->address->shipping ? $data->address->shipping->address : '';
                $flat = '';
                $showaddress='' ;
                if( strpos($delivery_address1, ',') !== false){
                    $seperate = explode(",", $delivery_address1);
                    $showaddress= $seperate[0];
                    $flat= $seperate[1];
                }else{
                    $showaddress= $delivery_address1;
                }
                ?>
                                                    <input id="spd_e_address1" name="spd_e_address1" type="text" class="fs-16 form-control"  value="{{ $showaddress }}" readonly="readonly" />
                                                    <input id="spd_e_address" name="spd_e_address" type="hidden" class="fs-16 form-control" @if(isset($data->address->shipping)) value="{{ $data->address->shipping->address }}" @else value="" @endif  />
                                                    <div id="spd_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.flat')</label>
                                                    <input id="spd_e_flat" name="spd_e_flat" type="text" class="fs-16 form-control" value="{{ $flat }}" onchange="onChangeEditDeliveryAddressUnitNumber(this.value)" />
                                                    <div id="spd_e_flat_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                                    <input id="spd_e_city" name="spd_e_city" type="text" class="fs-16 form-control" value="{{ $data->address->shipping ? $data->address->shipping->city : '' }}" readonly="readonly" />
                                                    <div id="spd_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="row mx-0">
                                                    <div class="col-6 pl-0 form-group">
                                                        <label>@lang('website_contents.checkout.address.state')</label>
                                                        <input id="spd_e_state" name="spd_e_state" type="text" class="fs-16 form-control" value="{{ $data->address->shipping ? $data->address->shipping->state : '' }}" readonly="readonly" />
                                                       <div id="spd_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="col-6 padd0 form-group">
                                                        <label>@lang('website_contents.checkout.address.postal_code')</label>
                                                        <input id="spd_e_portalCode" name="spd_e_portalCode" type="number" class="fs-16 form-control" value="{{ $data->address->shipping ? $data->address->shipping->portalCode : '' }}" readonly="readonly" />
                                                        <div id="spd_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                </div>
                                            @else
                                                @if(strtolower($data->country->codeIso) === 'tw')
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                                    <input id="spd_e_ssn" name="spd_e_ssn" type="text" class="fs-16 form-control"  value="{{  $data->address->shipping ? $data->address->shipping->SSN : '' }}"  />
                                                    <div id="spd_e_ssn_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                @endif
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                                    <input id="spd_e_address" name="spd_e_address" type="text" class="fs-16 form-control"  @if(isset($data->address->shipping)) value="{{ $data->address->shipping->address }}" @else value="" @endif />
                                                    <div id="spd_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                                    <input id="spd_e_city" name="spd_e_city" type="text" class="fs-16 form-control" value="{{  $data->address->shipping ? $data->address->shipping->city : '' }}"  />
                                                    <div id="spd_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="row mx-0">
                                                    <div class="col-6 pl-0 form-group">
                                                        <label>@lang('website_contents.checkout.address.postal_code')</label>
                                                        <?php
                                                         $postcodeda = "";
                                                    if($data->address){
                                                      if($data->address->shipping){
                                                         if($data->address->shipping->portalCode != "00000"){
                                                         $postcodeda = $data->address->shipping->portalCode;
                                                         }
                                                       }
                                                    }
                                                        ?>
                                                        <input id="spd_e_portalCode" name="spd_e_portalCode" type="number" class="fs-16 form-control" value="{{ $postcodeda }}"  />
                                                        <div id="spd_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="col-6 padd0 form-group">
                                                        <label>@lang('website_contents.checkout.address.state')</label>
                                                        <select class="fs-16 padd0 form-control MuliBold"  id="spd_e_state" name="spd_e_state" >
                                                        @if($data->countrystates)
                                                       @foreach ($data->countrystates as $s)
                                                       @if($data->address->shipping)
                                                      @if($data->address->shipping->state === $s["name"])
                                                     <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                                                     @else
                                                     <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                                       @endif
                                                       @else
                                                      <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                                        @endif
                                                      @endforeach
                                                      @endif
                                                       </select>
                                                       <div id="spd_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                </div>
                                            @endif
                                            </div>
                                        </form>
                                    </div>

                                    <div id="billing" class="tab-pane fade">
                                        <form id="form_spe_billing">
                                            <div class="col-md-12 px-0">
                                            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                                            <a href="javascript:;" onclick="koreanEditBillingAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-billing" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
                                            <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                                    <?php
                $delivery_address1=$data->address->billing ? $data->address->billing->address : '';
                $flatb = '';
                $showbaddress='' ;
                if( strpos($delivery_address1, ',') !== false){
                    $seperate = explode(",", $delivery_address1);
                    $showbaddress= $seperate[0];
                    $flatb= $seperate[1];
                }else{
                    $showbaddress= $delivery_address1;
                }
                ?>
                                                    <input id="spb_e_address1" name="spb_e_address1" type="text" class="fs-16 form-control" value="{{ $showbaddress }}"  readonly="readonly"/>
                                                    <input id="spb_e_address" name="spb_e_address" type="hidden" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->address : '' }}" readonly="readonly" />
                                                    <div id="spb_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.flat')</label>
                                                    <input id="spb_e_flat" name="spb_e_flat" type="text" class="fs-16 form-control" value="{{ $flatb }}" onchange="onChangeEditBillingAddressUnitNumber(this.value)" />
                                                    <div id="spb_e_flat_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                                    <input id="spb_e_city" name="spb_e_city" type="text" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->city : '' }}"  readonly="readonly"/>
                                                    <div id="spb_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="row mx-0">
                                                <div class="col-6 pl-0 form-group">
                                                        <label>@lang('website_contents.checkout.address.state')</label>
                                                        <input id="spb_e_state" name="spb_e_state" type="text" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->state : '' }}" readonly="readonly" />
                                                       <div id="spb_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="col-6 padd0 form-group">
                                                        <label>@lang('website_contents.checkout.address.postal_code')</label>
                                                        <input id="spb_e_portalCode" name="spb_e_portalCode" type="number" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->portalCode : '' }}"  readonly="readonly"/>
                                                        <div id="spb_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>

                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.unit_details')</label>
                                                    <input id="spb_e_address" name="spb_e_address" type="text" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->address : '' }}"  />
                                                    <div id="spb_e_address_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.address.town_city')</label>
                                                    <input id="spb_e_city" name="spb_e_city" type="text" class="fs-16 form-control" value="{{ $data->address->billing ? $data->address->billing->city : '' }}"  />
                                                    <div id="spb_e_city_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="row mx-0">
                                                    <div class="col-6 pl-0 form-group">
                                                        <label>@lang('website_contents.checkout.address.postal_code')</label>
                                                        <?php
                                                         $postcodeba = "";
                                                    if($data->address){
                                                       if($data->address->billing){
                                                         if($data->address->billing->portalCode != "00000"){
                                                         $postcodeba = $data->address->billing->portalCode;
                                                         }
                                                        }
                                                    }

                                                        ?>
                                                        <input id="spb_e_portalCode" name="spb_e_portalCode" type="number" class="fs-16 form-control" value="{{ $postcodeba }}"  />
                                                        <div id="spb_e_portalCode_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                    <div class="col-6 padd0 form-group">
                                                        <label>@lang('website_contents.checkout.address.state')</label>
                                                        <select class="fs-16 padd0 form-control MuliBold"  id="spb_e_state" name="spb_e_state" >
                                                        @if($data->countrystates)
                                                       @foreach ($data->countrystates as $s)
                                                       @if($data->address->billing)
                                                      @if($data->address->billing->state === $s["name"])
                                                     <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                                                     @else
                                                     <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                                       @endif
                                                       @else
                                                      <option value="{{$s['name']}}">{{$s["name"]}}</option>
                                                        @endif
                                                      @endforeach
                                                      @endif
                                                       </select>
                                                       <div id="spb_e_state_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                </div>
                                            @endif
                                            </div>
                                        </form>
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="checkbox d-flex align-items-center">
                                        <label class="custom-checkbox pr-2">
                                            <input  id="diff_billingaddress" type="checkbox" value="" @if(isset($data->address->shipping) && isset($data->address->billing) && ($data->address->shipping->address !== $data->address->billing->address)) checked="checked" @endif />
                                            <span style="height: 20px; width: 20px;"></span>
                                        </label>
                                        <p class="mb-0 font-weight-bold"><u>@lang('website_contents.checkout.address.different_address')</u></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="error-edit-shave" class="hasError MuliPlain hidden padd20 pr-0 pt-0 text-right"></div>
                    <div class="row mr-0 ml-0 mt-4 align-items-center d-none d-lg-flex">
                        <div class="col-9 text-right">
                            @if ($data->subscription->nextChargeDate !== null)
                            <a class="color-orange fs-20 font-weight-bold" data-toggle="modal" data-target="#pausePlan"><u>@lang('website_contents.authenticated.shaveplansedit.pause_plan')</u></a>
                            @else
                            <a class="start-cancellation-journey color-orange fs-20 font-weight-bold" data-dismiss="modal"><u>@lang('website_contents.authenticated.shaveplansedit.cancel_subscription')</u></a>
                            @endif
                        </div>
                        <div class="col-3 padd0">
                            {{-- <button id="sp_a_edit" type="button" onClick="sp_user_actions('edit')" class="btn btn-black" style="width: 100%;"><b>EDIT PLAN</b></button> --}}
                            <button id="sp_a_save" type="button" onClick="sp_user_actions('save')" class="btn btn-black" style="width: 100%;background:#ff5001 !important;color:white !important;"><b>@lang('website_contents.authenticated.shaveplansedit.save_plan')</b></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-lg-none btn-fixed" style="background-color: #f5f5f5;">
            <div class="col-12 padd0 text-center">
                <button type="button" class="btn btn-start rounded-0 MuliBold ml-0 mr-0 py-2" onClick="sp_user_actions('save')" style="min-width: 100%">@lang('website_contents.authenticated.shaveplansedit.save_plan')</button>
            </div>
            <div class="col-12 text-center py-2">
                    @if ($data->subscription->nextChargeDate !== null)
                    <a class="color-orange fs-18 font-weight-bold" data-toggle="modal" data-target="#pausePlan"><u>@lang('website_contents.authenticated.shaveplansedit.pause_plan')</u></a>
                    @else
                    <a class="start-cancellation-journey color-grey fs-18 font-weight-bold" data-dismiss="modal"><u>@lang('website_contents.authenticated.shaveplansedit.cancel_subscription')</u></a>
                    @endif
            </div>
        </div>
    </div>

</section>

<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="addProductLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body py-5 px-0 px-lg-5">
                <div class="col-12 text-center">
                    <h3>@lang('website_contents.authenticated.shaveplansedit.select_product_add')</h3>
                </div>
                <div class="col-12">
                    @if($data->optional_products)
                    @if(count($data->optional_products) > 4 )
                    <div class="row" style="overflow-y: scroll;max-height: 75vh;">
                    @else
                    <div class="row">
                    @endif
                        @php($total_optional_products = count($data->optional_products))
                        <input type="hidden" id="spe_total_optional_products" name="spe_total_optional_products" value="{{ $total_optional_products }}" />
                        @foreach($data->optional_products as $cp_addon)
                        <input type="hidden" id="addon_products_{{ $cp_addon->ProductCountryId }}" name="addon_products_{{ $cp_addon->ProductCountryId }}" value="<?php if(in_array($cp_addon->ProductId, $data->defaultaddonsArray)){ echo "true"; } else{ echo "false"; } ?>"/>
                        @endforeach
                        @foreach($data->optional_products as $cp_addon)
                        <div class="col-12 col-md-6 padd15 d-flex">
                        <div id="product-{{ $cp_addon->ProductCountryId }}" onClick="SelectHandle({{ $cp_addon->ProductCountryId }})" class="<?php if(in_array($cp_addon->ProductId, $data->defaultaddonsArray)){ echo "item-selected"; } else{ echo "item-unselected"; } ?> col-12 addonselection" style="padding:5px;margin-bottom:14px;">
                                <div class="col-12" style="z-index: 10">
                                    <div class="row">
                                        <div class="col-5 py-4 px-0 text-center d-flex align-items-center">
                                            <img style="margin:auto;max-height:100px;" class="img-fluid" src="{{ $cp_addon->url }}" alt="{{ $cp_addon->name }}" height="200px" />
                                        </div>
                                        <div class="col-7 pt-5 pb-4 text-center">
                                            <p class="MuliExtraBold product-name fs-16-sm fs-20">{{ $cp_addon->name }}</p>
                                            {{-- <p class="MuliPlain product-desc fs-10-sm fs-16">{{ $cp_addon->shortDescription }}</p> --}}
                                            @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                                            <!-- Currency and price for kr -->
                                           <?php
                               $s_price =  number_format($cp_addon->sellPrice) ;
                               ?>
                                            <p class="MuliExtraBold product-price fs-16-sm fs-20">{{ $s_price }}{{ $data->country->currencyDisplay }}</p>
                                            @else
                                            <p class="MuliExtraBold product-price fs-16-sm fs-20">{{ $data->country->currencyDisplay }}{{ $cp_addon->sellPrice }}</p>
                                            @endif
                                            <div class="col-12 text-center pl-0 pr-0">
                                                <input type="checkbox" class="mr-1" 
                                                name="spe_optional_products_recurring_{{ $cp_addon->ProductCountryId }}" 
                                                id="spe_optional_products_recurring_{{ $cp_addon->ProductCountryId }}" 
                                                @if($cp_addon->isRecurring == true) checked @endif>
                                                <span> @lang('website_contents.global.include_all_deliveries')</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <img id="product-tick-{{ $cp_addon->ProductCountryId }}" class="tick-mark d-none" src="{{asset('/images/common/tick.png')}}" /> -->
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                <div class="col-12 col-md-8 offset-md-2 text-center">
                    <p class="MuliBold">@lang('website_contents.authenticated.shaveplans.added_items')</p>
                </div>
                <div class="row px-3 px-md-0">
                    <div class="col-12 col-md-4 offset-md-2 px-3 mb-2">
                        <button class="btn btn-load-more" onClick="pre_updateSubsProducts()" style="min-width:100%">@lang('website_contents.authenticated.shaveplansedit.add_product')</button>
                    </div>
                    <div class="col-12 col-md-4 px-3 mb-2">
                        <button id="add_product_close" class="btn btn-cancel text-uppercase" style="min-width:100%" data-dismiss="modal">@lang('website_contents.global.content.cancel')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shipRefills" tabindex="-1" role="dialog" aria-labelledby="addProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h3>@lang('website_contents.authenticated.shaveplans.sure_ship_now')</h3>
                     <!-- Currency and price for kr -->
                    <?php
                               $s_ppc = $data->subscription->pricePerCharge - $data->subscription->appliedDiscount;
                            //    if(strtolower(view()->shared('currentCountryIso')) == 'kr'){
                                   $s_ppc = number_format((float) $s_ppc, 2, '.', '');
                            //    }

                     ?>
                    <p class="fs-20 Muli">@lang('website_contents.authenticated.shaveplans.charged_instantly', ['currency' => $data->country->currencyDisplay,'price' => $s_ppc] )</p>
                </div>
                <!-- <div class="col-8 offset-2 text-center">
                    <p class="MuliBold">@lang('website_contents.authenticated.shaveplans.added_items')</p>
                </div> -->
                <div class="row">
                    <div class="col-12 col-md-4 offset-md-2 px-0 px-md-3 mb-2">
                        <button id="button-pay-now" class="btn btn-start" style="min-width:100%">@lang('website_contents.authenticated.shaveplans.ship_now')</button>
                    </div>
                    <div class="col-12 col-md-4 px-0 px-md-3 mb-2">
                        <button class="btn btn-cancel" style="min-width:100%" data-dismiss="modal">@lang('website_contents.authenticated.shaveplans.cancel')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="selectPromoToBeApplied" tabindex="-1" role="dialog" aria-labelledby="addProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h2 class="MuliExtraBold text-success mb-4 mb-lg-2">@lang('website_contents.authenticated.shaveplansedit.accepted_promo')</h3>
                        <h3 class="MuliBold">@lang('website_contents.authenticated.shaveplansedit.change_promo')</h3>
                </div>
                <div class="col-12 my-4">
                    <div class="row">
                        <div style="display:none !important;" class="col-12 col-md-5 d-flex align-items-center">
                            <div class="col-12 padd0 text-center">
                                <h4 class="MuliExtraBold mb-4">@lang('website_contents.authenticated.shaveplansedit.current_promotion')</h4>
                                @foreach($data->existing_promotions as $actv_promo)

                                @if($actv_promo->type === "normal")

                                @if($actv_promo->subtype === "free_products")
                                @if($actv_promo->product_info)
                                <div class="col-12" style="display:inline-flex;">
                                @foreach($actv_promo->product_info as $related_products)
                                <div class="col-4" style="margin:auto;">
                                <img src="{{ $related_products->productDefaultImage }}" class="img img-responsive" style="width:100%;margin:auto;">
                                {{-- <label class="col-12">{{ $related_products->name }}</label> --}}
                                </div>
                                @endforeach
                                </div>
                                @endif
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Free Products)</p>
                                @endif

                                @if($actv_promo->subtype === "free_existing_products")
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Free Existing Products)</p>
                                @endif

                                @if($actv_promo->subtype === "discount")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Discount)</p>
                                @endif

                                @if($actv_promo->subtype === "free_existing_products_and_discount")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Free Existing Products & Discount)</p>
                                @endif

                                @if($actv_promo->subtype === "free_products_and_discount")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Free Products & Discounts)</p>
                                @endif

                                @if($actv_promo->subtype === "product_bundle")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Product Bundle Discount)</p>
                                @endif

                                @if($actv_promo->subtype === "undefined")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->info->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16"></p>
                                @endif

                                @elseif($actv_promo->type === "cancellation")
                                <h3 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.promotion_discount', ['promo' => $actv_promo->discount])</h3>
                                <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.promotion_cycle', ['promo' => $actv_promo->cycle])</p>
                                <p class="Muli fs-16">(Cancellation Discount)</p>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div style="display:none !important;" class="col-12 col-md-2 my-0 text-center">
                            <div class="row h-64-sm h-200-lg">
                                <div class="col-4 col-lg-12 bg-line">
                                </div>
                                <div class="col-4 col-lg-12">
                                    <h2 class="MuliSemiBold mb-0 padd15 mx-auto">@lang('website_contents.authenticated.shaveplansedit.to')</h3>
                                </div>
                                <div class="col-4 col-lg-12 bg-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 d-flex align-items-center">
                            <div class="col-12 padd0 text-center color-orange" id="spe_new_promo_details">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-4 offset-md-2 px-0 px-md-3 mb-2">
                        <input type="hidden" name="spe_selected_promo_id" id="spe_selected_promo_id" value="" />
                        <button type="button" id="pre_selectNewPromo_btn" onClick="pre_selectNewPromo()" class="btn btn-start" style="min-width:100%">@lang('website_contents.authenticated.shaveplansedit.change_now')</button>
                    </div>
                    <div class="col-12 col-md-4 px-0 px-md-3 mb-2"">
                        <button class=" btn btn-cancel" style="min-width:100%" data-dismiss="modal">@lang('website_contents.authenticated.shaveplans.cancel')</button>
                    </div>
                    <div class="col-12 mt-4 text-center">
                        <p class="Muli">@lang('website_contents.authenticated.shaveplansedit.promo_next_cycle')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pausePlan" tabindex="-1" role="dialog" aria-labelledby="pausePlanLabel" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h3>@lang('website_contents.authenticated.shaveplansedit.pause_subscription')</h3>
                    <p class="Muli fs-16">@lang('website_contents.authenticated.shaveplansedit.select_months')</p>
                </div>
                <input type="hidden" id="spe_pause_months" name="spe_pause_months" value="" />
                <div class="col-12 col-lg-10 offset-lg-1">
                    <div class="row">
                        <div class="col-12 col-md-4 padd15">
                            <div id="months_1" onClick="selectPause(1)" class="item-unselected" style="padding:5px;margin-bottom:14px;">
                                <div class="col-12 text-center py-1 py-lg-5">
                                    <h1 class="fs-30-sm fs-48 MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.1month')</h1>
                                </div>
                                <img id="months-tick-1" class="tick-mark d-none" src="{{asset('/images/common/tick.png')}}" />
                            </div>
                        </div>
                        <div class="col-12 col-md-4 padd15">
                            <div id="months_2" onClick="selectPause(2)" class="item-unselected" style="padding:5px;margin-bottom:14px;">
                                <div class="col-12 text-center py-1 py-lg-5">
                                    <h1 class="fs-30-sm fs-48 MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.2months')</h1>
                                </div>
                                <img id="months-tick-2" class="tick-mark d-none" src="{{asset('/images/common/tick.png')}}" />
                            </div>
                        </div>
                        <div class="col-12 col-md-4 padd15">
                            <div id="months_3" onClick="selectPause(3)" class="item-unselected" style="padding:5px;margin-bottom:14px;">
                                <div class="col-12 text-center py-1 py-lg-5">
                                    <h1 class="fs-30-sm fs-48 MuliExtraBold">@lang('website_contents.authenticated.shaveplansedit.3months')</h1>
                                </div>
                                <img id="months-tick-3" class="tick-mark d-none" src="{{asset('/images/common/tick.png')}}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8 offset-2 text-center">
                @if(strtolower(view()->shared('currentCountryIso')) == 'kr')
                <p class="MuliBold">@lang('website_contents.authenticated.shaveplansedit.resume_date')<span id="pause_plan_date"><b>{{\Carbon\Carbon::parse($data->subscription->nextChargeDate)->format('Y')}}년 {{\Carbon\Carbon::parse($data->subscription->nextChargeDate)->format('m')}}월 {{\Carbon\Carbon::parse($data->subscription->nextChargeDate)->format('d')}}일</b></span>@lang('website_contents.authenticated.shaveplansedit.resume_date2') </p>
                @else
                <p class="MuliBold">@lang('website_contents.authenticated.shaveplansedit.resume_date') <span id="pause_plan_date"><b>{{\Carbon\Carbon::parse($data->subscription->nextChargeDate)->format('d M Y')}}</b></span>@lang('website_contents.authenticated.shaveplansedit.resume_date2') </p>
                @endif
                </div>
                <div class="row mx-0">
                    <div class="col-12 col-md-4 offset-md-2 px-3 mb-2">
                        <button type="button" class="btn btn-load-more" onClick="pauseSubscription()" style="min-width:100%">@lang('website_contents.authenticated.shaveplansedit.pause_my_plan')</button>
                    </div>
                    <div class="col-12 col-md-4 px-3 mb-2">
                        <button class="btn btn-cancel" style="min-width:100%" data-dismiss="modal">@lang('website_contents.authenticated.shaveplans.cancel')</button>
                    </div>
                    <div class="col-12 text-center fs-sm-24">
                        <b>@lang('website_contents.authenticated.shaveplansedit.cancel_plan')</u></a></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Payment Failed Popup -->
<div class="modal fade" id="notification_payment_failed" tabindex="-1" role="dialog" aria-labelledby="notification_payment_failed" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h5 id="notification_payment_failed_text"></h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cancellationJourneyModal" tabindex="-1" role="dialog" aria-labelledby="cancellationJourneyLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div id="cancellationJourneyModalContent" class="modal-content">
        </div>
    </div>
</div>

<div id="stripe_otp_container" class="modal">
    <div class="modal-content" style="width: max-content;margin: auto;top: 20%;border-radious: none !important;background: white;border: none;border-radius: unset;">
      <div class="modal-container" id="stripe_otp_iframe"></div>
    </div>
</div>
<script>
// plan type
let isAnnual = <?php echo json_encode($data->plan_details["isAnnual"]) ?>;
let user_id = {!!json_encode(Auth::user()->id)!!};
// plan gender
let gender =  <?php echo json_encode($data->gender) ?>;
let total_existing_plan_products = <?php echo count($data->plan_details["product_info"]) ?>;
let spe_data = <?php echo json_encode($data) ?>;
let default_products = <?php echo json_encode($data->plan_details["product_info"]) ?>;
let default_productaddons = <?php echo json_encode($data->defaultaddons) ?>;
let country_iso = "<?php echo strtolower(view()->shared('currentCountryIso')) ?>";
<?php
if(strtolower(view()->shared('currentCountryIso')) == 'kr') {
?>
let pauseplanone = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(1)->format('Y') ?>년 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(1)->format('m') ?>월 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(1)->format('d') ?>일";
let pauseplantwo = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(2)->format('Y') ?>년 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(2)->format('m') ?>월 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(2)->format('d') ?>일";
let pauseplanthree = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(3)->format('Y') ?>년 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(3)->format('m') ?>월 "+"<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(3)->format('d') ?>일";
<?php
}else{
?>
let pauseplanone = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(1)->format('d M Y') ?>";
let pauseplantwo = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(2)->format('d M Y') ?>";
let pauseplanthree = "<?php echo \Carbon\Carbon::parse($data->subscription->nextChargeDate)->addMonths(3)->format('d M Y') ?>";
<?php
}
?>

</script>

<!-- Function: Pay Now -->
<script>
    $(function(){

        // Check if got any payment error message after paynow OTP is completed,
        // Then, clear the session completely
        DisplayPaymentError();

        // OnClick pay now button
        $("#button-pay-now").click(function() {

            $("#loading").css("display", "block");
            if(country_iso == "kr"){
                let PAY_NOW_URL =
                GLOBAL_URL +
                "/user/shave-plan/paynow/nicepay";


            AJAX(PAY_NOW_URL, 'POST', {
                    "subscription_id": spe_data.subscription.id,
                })
                .done(function(data) {

                    // Close ship refills modal
                    $("#shipRefills").modal("hide");

                    if (data.status === "success") {
                        window.location = window.location.origin + GLOBAL_URL + '/thankyou/' + data.orderId;
                    } else if (data.status === "failure") {
                        $("#notification_payment_failed_text").html(data.message);
                        $("#notification_payment_failed").modal("show");
                    } else if (data.status === "requires_source_action") {
                        session("subscription_pay_now", "set", JSON.stringify(data.subs)).done(function() {
                            window.location = data.subs.charge.next_action.redirect_to_url.url;
                        });
                    } else {
                        console.log("Failed to pay now.");
                    }
                    $("#loading").css("display", "none");
                })
                .fail(function(jqXHR, textStatus, error) {
                    $("#loading").css("display", "none");
                    console.log("Failed to pay now.");
                });
            }
            else{
            let PAY_NOW_URL =
                GLOBAL_URL +
                "/user/shave-plan/paynow";
                
            let RETURN_URL_WITH_OTP =
                window.location.origin +
                GLOBAL_URL +
                `/stripe/redirect?is_pay_now=1`;

            AJAX(PAY_NOW_URL, 'POST', {
                    "subscription_id": spe_data.subscription.id,
                    "return_url": RETURN_URL_WITH_OTP
                })
                .done(function(data) {

                    // Close ship refills modal
                    $("#shipRefills").modal("hide");

                    if (data.status === "success") {
                        window.location = window.location.origin + GLOBAL_URL + '/thankyou/' + data.orderId;
                    } else if (data.status === "failure") {
                        $("#notification_payment_failed_text").html(data.message);
                        $("#notification_payment_failed").modal("show");
                    } else if (data.status === "requires_source_action") {
                        session("subscription_pay_now", "set", JSON.stringify(data.subs)).done(function() {
                            window.location = data.subs.charge.next_action.redirect_to_url.url;
                        });
                    } else {
                        console.log("Failed to pay now.");
                    }
                    $("#loading").css("display", "none");
                })
                .fail(function(jqXHR, textStatus, error) {
                    $("#loading").css("display", "none");
                    console.log("Failed to pay now.");
                });
            }
        });
        
        })

        async function DisplayPaymentError() {
        await session("subscription_pay_now", "get", null).then(data => {
            if (data) {
                let dataParsed = JSON.parse(data);
                if (dataParsed.status === "success") {
                    $("#notification_payment_success").modal("show");
                } else {
                    $("#notification_payment_failed_text").html(dataParsed.message);
                    $("#notification_payment_failed").modal("show");
                }
                session("subscription_pay_now", "clear", null);
            }
        });
    }
</script>

<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>
<script src="{{ asset('js/functions/saved-plans/saved-plans-edit.function.js') }}"></script>
@endsection
