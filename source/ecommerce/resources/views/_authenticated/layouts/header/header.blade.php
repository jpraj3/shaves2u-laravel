<nav class="authenticated-navbar navbar navbar-expand-lg padd0">
    <div class="container">
        <div class="row mx-0 w-100 padd20 d-lg-none">
            <div class="col-8 offset-2 padd0 text-center">
                <span class="current-page Muli mx-auto font-weight-bold color-orange">
                    {{ request()->routeIs('locale.region.authenticated.dashboard') ? Lang::get('website_contents.authenticated.content.dashboard') : "" }}
                    {{ request()->routeIs('locale.region.authenticated.savedplans*') ? Lang::get('website_contents.authenticated.content.shave_plans') : "" }}
                    {{ request()->routeIs('locale.region.authenticated.profile') ? Lang::get('website_contents.authenticated.content.profile') : "" }}
                    {{ request()->routeIs('locale.region.authenticated.orderhistory*') ? Lang::get('website_contents.authenticated.content.order_history') : "" }}
                    {{ request()->routeIs('locale.region.authenticated.referrals') ? Lang::get('website_contents.authenticated.content.referrals') : "" }}
                </span>
            </div>
            <button id="authenticated_nav_collapse" class="navbar-toggler border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_authenticated" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <a id="authnavbarToggle" class="dropdown-item nav-link font-weight-bold color-white" href="#">
                    <i class="fa fa-caret-down"></i>
                </a>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent_authenticated">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mx-auto center-md pl-5 pr-5">
                <li class="nav-item"><a class="nav-link text-center {{ request()->routeIs('locale.region.authenticated.dashboard') ? 'active' : '' }}" href="{{ route('locale.region.authenticated.dashboard', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.authenticated.content.dashboard')</a></li>
                <li class="nav-item border-md-top"><a class="nav-link text-center {{ request()->routeIs('locale.region.authenticated.savedplans*') ? 'active' : '' }}" href="{{ route('locale.region.authenticated.savedplans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.authenticated.content.shave_plans')</a></li>
                <li class="nav-item border-md-top"><a class="nav-link text-center {{ request()->routeIs('locale.region.authenticated.profile') ? 'active' : '' }}" href="{{ route('locale.region.authenticated.profile', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.authenticated.content.profile')</a></li>
                <li class="nav-item border-md-top"><a class="nav-link text-center {{ request()->routeIs('locale.region.authenticated.orderhistory*') ? 'active' : '' }}" href="{{ route('locale.region.authenticated.orderhistory', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.authenticated.content.order_history')</a></li>
                <li class="nav-item border-md-top"><a class="nav-link text-center {{ request()->routeIs('locale.region.authenticated.referrals') ? 'active' : '' }}" href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.authenticated.content.referrals')</a></li>
                <li class="nav-item border-md-top"><a class="nav-link text-center" href="" data-toggle="modal" data-target="#signOut">@lang('website_contents.authenticated.content.sign_out_copy')</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="signOut" tabindex="-1" role="dialog" aria-labelledby="signOutLabel" aria-hidden="true" style="padding: 0 !important;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content sign-out-modal">
            <div class="modal-body py-5 px-2 col-12">
                <div class="col-12 text-center py-2">
                    <h3>@lang('website_contents.authenticated.content.leaving_soon')</h3>
                </div>
                <div class="col-12 col-md-12 mb-2 pt-2 pb-2">
                    <a class="btn btn-start" href="" style="width:80%" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">@lang('website_contents.authenticated.content.sign_out')</a>

                    <form id="logout-form" action="{{ route('logout', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
                <div class="col-12 col-md-12 mb-2 text-center">
                    <button class="btn btn-cancel" style="width:80%; margin: 0 auto !important" data-dismiss="modal">@lang('website_contents.authenticated.content.stay')</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/functions/_authenticated/_authenticated.function.js') }}"></script>
