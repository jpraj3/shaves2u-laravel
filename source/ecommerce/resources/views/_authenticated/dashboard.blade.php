@extends('layouts.app')
@section('content')
    @include('_authenticated.layouts.header.header')

    <link rel="stylesheet" href="{{ asset('css/dashboard/dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">

    <section class="dashboard-content">
        <div class="container">
            <div class="row">
                <div class="col-12 padd30 d-lg-none">
                    <div class="row mx-0">
                        <div class="col-6 pl-0">
                            <h2 class="fs-25 fs-20-sm MuliExtraBold"><strong>@lang('website_contents.global.content.welcome')</strong>
                            </h2>
                            <p class="MuliBold fs-25 fs-20-sm">{{Auth::user()->firstName}}</p>
                        </div>
                        <div class="col-4 col-md-2 offset-2 offset-md-4 pr-0">
                            @if(isset(Auth::user()->user_profile_img) && Auth::user()->user_profile_img !== null)
                                <img class="img-fluid" alt="Profile Picture"
                                     src="{{ Auth::user()->user_profile_img }}" style="width: 80px; height: 80px; border-radius: 50%; object-fit:cover;"/>
                            @else
                                <img class="img-fluid rounded-circle" alt="Profile Picture"
                                     src="{{asset('/images/default/default_user_image.jpg')}}" style="width: 80px; height: 80px; border-radius: 50%; object-fit:cover;"/>
                        @endif
                        <!-- <img class="img-fluid" alt="Profile Picture" src="{{asset('/images/common/Avatar.png')}}" /> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 padd30 pt-0">
                    <div class="rounded-10 bg-white shadow padd-lg-50 padd-md-15">
                        <div class="row marg-lg-0">
                            <div class="col-3 pl-0 d-none d-lg-block">
                                <h2 class="MuliExtraBold text-uppercase">
                                    <strong>@lang('website_contents.global.content.welcome')</strong></h2>
                                <p class="MuliPlain fs-25 fs-20-sm">{{Auth::user()->firstName}}</p>
                            </div>
                            @if(!empty($retrieveLatestSubscription->latest_sub))
                                <div class="col-md-12 col-lg-9 pl-lg-0 pr-lg-0">
                                    <h2 class="MuliExtraBold" style="font-size: 22px;">
                                        <strong>@lang('website_contents.authenticated.dashboard.current_plan')</strong>
                                    </h2>
                                    @if(!empty($retrieveLatestSubscription->latest_sub) && !empty($retrieveLatestSubscription->blade_type))
                                        <p class="color-orange MuliBold fs-25 fs-20-sm">
                                            <strong>{{ $retrieveLatestSubscription->blade_type->name }} @lang('website_contents.blade_pack') @lang('website_contents.every') {{ $retrieveLatestSubscription->latest_sub->PlanDuration }} @lang('website_contents.Months')</strong>
                                        </p>
                                    @endif
                                </div>
                            @endif
                        </div>
                        <div class="row padd15">
                            <div class="col-3 pl-0 pr-md-0 pr-lg-50 d-none d-lg-block" style="padding-right: 50px;">
                                @if(isset(Auth::user()->user_profile_img) && Auth::user()->user_profile_img !== null)
                                    <img class="img-fluid" alt="Profile Picture"
                                         src="{{ Auth::user()->user_profile_img }}" style="width: 200px; height: 170px; border-radius: 50%; object-fit:cover;"//>
                                @else
                                    <img class="img-fluid rounded-circle" alt="Profile Picture"
                                         src="{{asset('/images/default/default_user_image.jpg')}}" style="width: 200px; height: 170px; border-radius: 50%; object-fit:cover;"//>
                                @endif
                            </div>
                            @if(!empty($retrieveLatestSubscription->latest_sub))
                                <div class="col-md-9 col-lg-9 pl-0 pr-0 fs-16 bord-top-lg">
                                    <div class="row padd15 fs-20 fs-16-sm">
                                        <div class="col-6 col-md-4 pl-0 pr-0">
                                            <p class="MuliBold fs-20 fs-16-sm mb-0">@lang('website_contents.authenticated.dashboard.next_billing')</p>
                                            @if(!empty($retrieveLatestSubscription->latest_sub))
                                                @if ($retrieveLatestSubscription->latest_sub->nextChargeDate !== null)
                                                    <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ \Carbon\Carbon::parse($retrieveLatestSubscription->latest_sub->nextChargeDate)->format('d M Y') }}</p>
                                                @else
                                                    <p class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.trial')</p>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-6 col-md-4 pl-0 pr-0">
                                            <p class="MuliBold fs-20 fs-16-sm mb-0">@lang('website_contents.authenticated.dashboard.plan_products')</p>
                                            @if(!empty($retrieveLatestSubscription->latest_sub))
                                                @if(!empty($retrieveLatestSubscription->product_details))
                                                    @foreach ($retrieveLatestSubscription->product_details as $item)
                                                        @if ($item->name === '3 Blade')
                                                            <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                                        @elseif($item->name === '5 Blade')
                                                            <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                                        @elseif($item->name === '6 Blade')
                                                            <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                                        @else
                                                            <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ $item->name }}</p>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if(!empty($retrieveLatestSubscription->active_addons))
                                                    @foreach ($retrieveLatestSubscription->active_addons as $addon)
                                                        <p class="MuliPlain fs-18 mb-0 fs-14-sm">{{ $addon->name }}</p>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 padd0">
                                            <p class="MuliBold fs-20 fs-16-sm mb-0">@lang('website_contents.authenticated.dashboard.payment_method')</p>
                                            @if(!empty($retrieveLatestSubscription->latest_sub))
                                                <p class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.authenticated.dashboard.ending_in') {{ $retrieveLatestSubscription->card_info ? $retrieveLatestSubscription->card_info->cardNumber : '-'}}</p>
                                                <p class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.authenticated.dashboard.expiring_in') {{ $retrieveLatestSubscription->card_info ? $retrieveLatestSubscription->card_info->expiredMonth : '-'}}/{{ $retrieveLatestSubscription->card_info ? $retrieveLatestSubscription->card_info->expiredYear : '-'}}</p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-7 text-center pl-0 pr-0 pt-3">
                                        <a href="{{route('locale.region.authenticated.savedplans', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])}}"
                                           class="btn btn-black"
                                           style="width: 100%;">@lang('website_contents.authenticated.shaveplans.view_plan')</a>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-12 col-lg-8 pl-0 pr-0 fs-16">
                                    <div class="row text-center">
                                        <div class="col-12">
                                            <p>@lang('website_contents.authenticated.shaveplans.not_on_plan')</p>
                                            <p class="fs-20 fs-16-sm">@lang('website_contents.authenticated.shaveplans.custom_plan')</p>
                                        </div>
                                        <div class="col-12">
                                            <div class="col-lg-12 text-center padd20 pl-0 pr-lg-100 pr-md-30" style="padding-right: 0px;bottom: 0px;">
                                                    <a class="btn btn-load-more-v2"
                                                    href="{{route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])}}" >
                                                    <b>@lang('website_contents.authenticated.dashboard.start_shave')</b></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="dashboard-details">
        <div class="container">
            <div class="row fs-20 fs-16-sm">
                <div class="col-lg-6 padd30 d-flex">
                    <div class="col-lg-12 bg-white rounded-10 shadow padd-lg-50 padd-md-15">
                        <p class="MuliBold fs-25 fs-20-sm mb-0">@lang('website_contents.authenticated.dashboard.referrals')</p>
                        <div class="col-lg-12 padd20 pt-2 pl-0 pr-0">
                            <div class="col-lg-12 padd5 pl-0 pr-0">
                            <span
                                    class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.authenticated.dashboard.total_credits')</span>
                                @if($user_info->country->id ==8)
                                <strong
                                    class="float-right text-orng MuliPlain fs-18 mb-0 fs-14-sm">{{$referral_cash}}{{$user_info->country->currencyDisplay}}</strong>
                                @else
                                <strong
                                    class="float-right text-orng MuliPlain fs-18 mb-0 fs-14-sm">{{$user_info->country->currencyDisplay}}{{$referral_cash}}</strong>
                                @endif
                            </div>
                            <div class="col-lg-12 padd5 pl-0 pr-0">
                                <span
                                    class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.authenticated.dashboard.people_referred')</span>
                                <strong
                                    class="float-right text-orng MuliPlain fs-18 mb-0 fs-14-sm">{{$retrieveTotalSignUps}}</strong>
                            </div>
                            <div class="col-lg-12 padd5 pl-0 pr-0" style="margin-bottom: 75px;">
                                <span
                                    class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.authenticated.dashboard.successful_referees')</span>
                                <strong
                                    class="float-right text-orng MuliPlain fs-18 mb-0 fs-14-sm">{{$retrieveTotalSucessful}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center padd20 pl-0 pr-lg-100 pr-md-30"
                             style="position: absolute; bottom: 0px;">
                            <a class="btn btn-load-more"
                               href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                               style="width: 100%;"><b>@lang('website_contents.authenticated.dashboard.earn_more_referrals')</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 padd30 d-flex">
                    <div class="col-lg-12 bg-white rounded-10 shadow padd-lg-50 padd-md-15">
                        <p class="MuliBold fs-25 fs-20-sm mb-0 text-uppercase">@lang('website_contents.checkout.user.personal_details')</p>
                        <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                            <label class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.checkout.user.email_address')</label>
                        </div>
                        <div class="col-lg-12 pl-0 pr-0 bord-bot pb-3 fs-14-sm">
                            <span class="MuliPlain fs-18 mb-0 fs-14-sm" style="word-wrap:break-word;">{{ $user_info->user->email }}</span>
                        </div>
                        <div class="col-lg-12 pl-0 pr-0 pt-3 fs-14-sm">
                            <label class="MuliPlain fs-18 mb-0 fs-14-sm">@lang('website_contents.checkout.user.password')</label>
                        </div>
                        <div class="col-lg-12 pl-0 pr-0 pb-3 bord-bot fs-14-sm" style="margin-bottom: 75px;">
                            <span class="MuliPlain fs-18 mb-0 fs-14-sm">●●●●●●●●●●</span>
                        </div>
                        <div class="col-lg-12 text-center padd20 pl-0 pr-lg-100 pr-md-30"
                             style="position: absolute; bottom: 0px;">
                            <a class="btn btn-load-more"
                               href="{{ route('locale.region.authenticated.profile', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                               style="width: 100%"><b>@lang('website_contents.authenticated.dashboard.view_profile')</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
