@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')

<link rel="stylesheet" href="{{ asset('css/_authenticated/landing/landing.css') }}">

<section class="dashboard-plan-slideshow">
    <div id="banner_carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#banner_carousel" data-slide-to="0" class="active"></li>
            <li data-target="#banner_carousel" data-slide-to="1"></li>
            <li data-target="#banner_carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="banner-carousel-item carousel-item active">
                <div class="container">
                    <div class="row" style="margin: 0;">
                        <div class="col-lg-5 text-center pr-0 pl-0" style="padding: 120px 0 120px 0;">
                            <h1 class="MuliExtraBold color-lg-white mt-2p mb-2p"><strong>Awesome Shave Kit<br></strong></h1>
                            <p class="fs-24 color-lg-white Muli mt-5p mb-10p">Need the perfect shave gift?<br>We got it covered<br></p>
                            <button class="btn-start padd10" style="width: 66.666%;">LEARN MORE</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-carousel-item carousel-item">
                <div class="container">
                    <div class="row" style="margin: 0;">
                        <div class="col-lg-5 text-center pr-0 pl-0" style="padding: 120px 0 120px 0;">
                                <h1 class="MuliExtraBold color-lg-white mt-2p mb-2p"><strong>Awesome Shave Kit<br></strong></h1>
                                <p class="fs-24 color-lg-white Muli mt-5p mb-10p">Need the perfect shave gift?<br>We got it covered<br></p>
                            <button class="btn-start padd10" style="width: 66.666%;">LEARN MORE</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-carousel-item carousel-item">
                <div class="container">
                    <div class="row" style="margin: 0;">
                        <div class="col-lg-5 text-center pr-0 pl-0" style="padding: 120px 0 120px 0;">
                                <h1 class="MuliExtraBold color-lg-white mt-2p mb-2p"><strong>Awesome Shave Kit<br></strong></h1>
                                <p class="fs-24 color-lg-white Muli mt-5p mb-10p">Need the perfect shave gift?<br>We got it covered<br></p>
                            <button class="btn-start padd10" style="width: 66.666%;">LEARN MORE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-shave-plan bg-black" style="padding: 30px 0 30px 0;">
    <div class="container">
        <div class="row">
            <div class="col-9 offset-1 pr-0 pl-0 Muli">
                <h6 class="color-orange mt-1p mb-1p">YOUR CURRENT PLAN<br></h4>
                <h5 class="color-white mt-1p mb-1p">Custom 3 Blade Pack Every 2 Months</h5>
            </div>
            <div class="col-1 pr-0 pl-0 text-right">
                <i class="fa fa-angle-right current-plan color-white"></i>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-news paddTB40">
    <div class="container">
        <div class="col-md-12 text-center pr-0 pl-0">
            <h1 class="MuliExtraBold mt-2p mb-2p"><strong>Latest News & Articles<br></strong></h1>
        </div>
        <div class="scroller Muli">
            <div class="row" style="padding: 40px 0px 30px 0px;">
                <div class="col-4 d-flex">
                    <div class="col-md-12 news-article">
                        <div style="padding: 15px 30px;">
                            <span><i class="fa fa-heart color-orange"></i></span>
                            <span class="pull-right">Grooming</span>
                        </div>
                        <img class="img-fluid" alt="Is Shave Cream An Absolute Necessity?" src="{{asset('/images/common/news-thumb1.jpg')}}">
                        <div style="padding: 15px 30px;">
                            <h4 class="mt-2p mb-10p"><strong>Is Shave Cream An Absolute Necessity?<br></strong></h4>
                            <a class="color-black mt-5p mb-2p" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex">
                    <div class="col-md-12 news-article">
                        <div style="padding: 15px 30px;">
                            <span><i class="fa fa-heart-o" style="color: #FE5000"></i></span>
                            <span class="pull-right">Health</span>
                        </div>
                        <img class="img-fluid" alt="Tips Amalkan Gaya Hidup Sihat Yang Efektif dan Mudah" src="{{asset('/images/common/news-thumb2.jpg')}}">
                        <div style="padding: 15px 30px;">
                            <h4 class="mt-2p mb-10p"><strong>Tips Amalkan Gaya Hidup Sihat Yang Efektif dan Mudah<br></strong></h4>
                            <a class="color-black mt-5p mb-2p" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-4 d-flex">
                    <div class="col-md-12 news-article">
                        <div style="padding: 15px 30px;">
                            <span><i class="fa fa-heart-o" style="color: #FE5000"></i></span>
                            <span class="pull-right">Health</span>
                        </div>
                        <img class="img-fluid" alt="Fastidious Grooming Habits Make You Better At Work" src="{{asset('/images/common/news-thumb3.jpg')}}">
                        <div style="padding: 15px 30px;">
                            <h4 class="mt-2p mb-10p"><strong>Fastidious Grooming Habits Make You Better At Work<br></strong></h4>
                            <a class="color-black mt-5p mb-2p" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dashboard-start-earning d-none d-lg-block paddTB40" style="background-color: #E0E0E0;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-1 text-center pr-0 pl-0 paddTB40c">
                <h3 class="MuliExtraBold mt-10p mb-5p"><strong>Earn with our Referral Program<br></strong></h3>
                <p class="Muli fs-20 color-black mt-5p mb-10p">Share us with your friends and start earning cash<br></p>
                <a class="btn-start padd10" href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" style="min-width: 70%;">START EARNING NOW</a>
            </div>
            <div class="col-md-4 offset-md-2 text-center pr-0 pl-0">
                <img alt="Referral" src="{{asset('/images/common/img-referral.png')}}">
            </div>
        </div>
    </div>
</section>
<section class="dashboard-start-earning-mobile d-lg-none paddTB40" style="background-color: #f8fafc;">
    <div id="earning_carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#earning_carousel" data-slide-to="0" class="active"></li>
            <li data-target="#earning_carousel" data-slide-to="1"></li>
            <li data-target="#earning_carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row">
                        <div class="col-3 pr-0 pl-0">
                        </div>
                        <div class="col-6 text-center pr-0 pl-0">
                            <img alt="Referral" class="img-fluid" src="{{asset('/images/common/img-referral.png')}}">
                        </div>
                        <div class="col-12 text-center pr-0 pl-0 paddTB40">
                            <h3 class="MuliExtraBold mt-2p mb-2p"><strong>Earn with our Referral Program<br></strong></h3>
                            <p class="Muli fs-20 color-black mt-5p mb-10p">Share us with your friends and start earning cash<br></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row">
                        <div class="col-3 pr-0 pl-0">
                        </div>
                        <div class="col-6 text-center pr-0 pl-0">
                            <img alt="Referral" class="img-fluid" src="{{asset('/images/common/img-referral.png')}}">
                        </div>
                        <div class="col-12 text-center pr-0 pl-0 paddTB40">
                            <h3 class="MuliExtraBold mt-2p mb-2p"><strong>Earn with our Referral Program<br></strong></h3>
                            <p class="Muli fs-20 color-black mt-5p mb-10p">Share us with your friends and start earning cash<br></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row">
                        <div class="col-3 pr-0 pl-0">
                        </div>
                        <div class="col-6 text-center pr-0 pl-0">
                            <img alt="Referral" class="img-fluid" src="{{asset('/images/common/img-referral.png')}}">
                        </div>
                        <div class="col-12 text-center pr-0 pl-0 paddTB40">
                            <h3 class="MuliExtraBold mt-3p mb-3p"><strong>Earn with our Referral Program<br></strong></h3>
                            <p class="Muli fs-20 color-black mt-5p mb-10p">Share us with your friends and start earning cash<br></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center pr-0 pl-0">
            <a class="btn-start padd10" href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" style="min-width: 30%;">START EARNING NOW</a>
        </div>
    </div>
</section>

<section class="dashboard-testimonies d-md-none">
    <div class="container">
        <div class="col-md-12 text-center pr-0 pl-0">
            <h2 class="MuliBold"><strong>Testimonies</strong></h2>
        </div>
    </div>
    <div id="testimony_carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#testimony_carousel" data-slide-to="0" class="active"></li>
            <li data-target="#testimony_carousel" data-slide-to="1"></li>
            <li data-target="#testimony_carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <img class="center mt-2p" alt="Avatar Thumbnail" id="avatar-image" src="{{asset('/images/common/avatar.png')}}" style="width:30%;">
                    <div class="col-md-12 text-center pr-0 pl-0">
                        <h4 class="MuliBold mt-2p"><strong>Person Name</strong></h4>
                    </div>
                    <div class="col-md-12 text-center pr-0 pl-0" style="padding-bottom: 60px;">
                        <h1 class="MuliExtraBold mt-2p"><strong>Excepteur sint<br>occaecat<br>cupidatat non<br>proident, sunt in<br>culpa qui officia.</strong></h1>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <img class="center mt-2p" alt="Avatar Thumbnail" id="avatar-image" src="{{asset('/images/common/avatar.png')}}" style="width:30%;">
                    <div class="col-md-12 text-center pr-0 pl-0">
                        <h4 class="MuliBold mt-2p""><strong>Person Name</strong></h4>
                    </div>
                    <div class="col-md-12 text-center pr-0 pl-0" style="padding-bottom: 60px;">
                        <h1 class="MuliExtraBold mt-2p""><strong>Excepteur sint<br>occaecat<br>cupidatat non<br>proident, sunt in<br>culpa qui officia.</strong></h1>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <img class="center mt-2p" alt="Avatar Thumbnail" id="avatar-image" src="{{asset('/images/common/avatar.png')}}" style="width:30%;">
                    <div class="col-md-12 text-center pr-0 pl-0">
                        <h4 class="MuliBold mt-2p"><strong>Person Name</strong></h4>
                    </div>
                    <div class="col-md-12 text-center pr-0 pl-0" style="padding-bottom: 60px;">
                        <h1 class="MuliExtraBold mt-2p"><strong>Excepteur sint<br>occaecat<br>cupidatat non<br>proident, sunt in<br>culpa qui officia.</strong></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-start-learning orng-bg-sm paddTB40">
    <div class="container">
        <div class="row">
            <div class="col-md-5 pr-0 pl-0">
                <img alt="Learn More" class="dsl-image" src="{{asset('/images/common/aftershavecream.png')}}">
            </div>
            <div class="col-md-4 offset-md-2 text-center pr-0 pl-0" style="padding-top: 50px;">
                <h2 class="color-sm-orange color-md-orange color-lg-white MuliExtraBold mt-2p mb-5p"><strong>Want a new creamy shave experience?<br></strong></h2>
                <p class="color-sm-orange color-md-orange color-lg-white fs-20 Muli mt-5p" style="margin-bottom: 15%;">Lorem ac efficitur. Suspen disse<br class="d-md-none"> nullatellus, aliquam ut Ipsum ac,<br>venenatis finibus lectus.<br></p>
                <button class="btn-start padd10" style="width: 70%;">LEARN MORE</button>
            </div>
        </div>
    </div>
</section>

<section id="need-to-know" class="landing-section paddTB40">
    <div class="container" id="faq">
        <div class="col-md-12 text-center pr-0 pl-0">
            <h2 class="MuliExtraBold mt-2p mb-2p"><strong>Need to know more?<br></strong></h2>
            <h6 class="Muli mt-2p mb-1p">Shaves2U brings you a unique shaving experience tailored for your grooming needs.<br></h6>
            <h6 class="Muli mt-1p mb-5p">Get started in just three simple steps!<br>
            </h6>
        </div>
        <!--Accordion wrapper-->
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

            <!-- Accordion card -->
            <div class="card bord-none bordT-grey">

                <!-- Card header -->
                <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingOne1">
                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                        aria-controls="collapseOne1">
                        <h5 class="mb-0 MuliBold color-black">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            <i class="fa fa-angle-right rotate-icon d-none d-md-block" style="float: right;"></i>
                        </h5>
                    </a>
                </div>

                <!-- Card body -->
                <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                    data-parent="#accordionEx">
                    <div class="card-body">
                        It's a way better deal. Here, you get our best razor AND extra stuff for less than the cost of
                        razor by itself. You don't need a graphing calculator to see that's a better deal.
                    </div>
                </div>

                <!-- Card footer -->
                <div class="card-footer center-sm d-md-none" role="tab" id="footerOne1">
                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                        aria-controls="collapseOne1">
                        <h5 class="mb-0 text-center MuliBold color-black">
                            <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="card bord-none bordT-grey">

                <!-- Card header -->
                <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingTwo2">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                        aria-expanded="false" aria-controls="collapseTwo2">
                        <h5 class="mb-0 MuliBold color-black">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            <i class="fa fa-angle-right rotate-icon d-none d-md-block" style="float: right;"></i>
                        </h5>
                    </a>
                </div>

                <!-- Card body -->
                <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                    data-parent="#accordionEx">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    </div>
                </div>

                <!-- Card footer -->
                <div class="card-footer center-sm d-md-none" role="tab" id="footerTwo2">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                        aria-expanded="false" aria-controls="collapseTwo2">
                        <h5 class="mb-0 text-center MuliBold color-black">
                            <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="card bord-none bordT-grey">

                <!-- Card header -->
                <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingThree3">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                        aria-expanded="false" aria-controls="collapseThree3">
                        <h5 class="mb-0 MuliBold color-black">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            <i class="fa fa-angle-right rotate-icon d-none d-md-block" style="float: right;"></i>
                        </h5>
                    </a>
                </div>

                <!-- Card body -->
                <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                    data-parent="#accordionEx">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    </div>
                </div>

                <!-- Card footer -->
                <div class="card-footer center-sm d-md-none" role="tab" id="footerThree3">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                        aria-expanded="false" aria-controls="collapseThree3">
                        <h5 class="mb-0 text-center MuliBold color-black">
                            <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="card bord-none bordB-grey">

                <!-- Card header -->
                <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFour4">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                        aria-expanded="false" aria-controls="collapseFour4">
                        <h5 class="mb-0 MuliBold color-black">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            <i class="fa fa-angle-right rotate-icon d-none d-md-block" style="float: right;"></i>
                        </h5>
                    </a>
                </div>

                <!-- Card body -->
                <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
                    data-parent="#accordionEx">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    </div>
                </div>

                <!-- Card footer -->
                <div class="card-footer center-sm d-md-none" role="tab" id="footerFour4">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                        aria-expanded="false" aria-controls="collapseFour4">
                        <h5 class="mb-0 text-center MuliBold color-black">
                            <i class="fa fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>

            </div>
            <!-- Accordion card -->

        </div>
        <!-- Accordion wrapper -->
        <div class="col-md-12 text-center pr-0 pl-0 d-md-none">
            <button class="btn btn-load-more mt-2p">Load More</button>
        </div>
    </div>
</section>

<section class="dashboard-free-text">
    <div class="container" id="free-text">
        <div class="col-md-12 text-center pr-0 pl-0">
            <h1 class="MuliExtraBold color-white d-none d-md-block mt-2p mb-2p"><strong>SEO META TEXT<br></strong></h1>
            <h2 class="MuliBold d-md-none mt-2p mb-10p"><strong>SEO META TEXT<br></strong></h2>
        </div>
        <div class="col-md-12 text-center pr-0 pl-0">
            <h5 class="MuliBold color-lg-white mt-2p">Shaves2U shaving handles are designed to be durable, flexible and sleek. The swivel handle comes with a dual-axis Swivel mechanism that allows smooth gliding
across your face so that you can shave with little to no effort. With the Swivel handle’s grip, you can rest assured there will be no slips while shaving. The metal
swivel handle also fits all types of blade, regardless if they’re three-blade, five-blade or six-blade.
Are you interested in owning a Shaves2U swivel handle? You can do so by ordering the Starter Kit or the Awesome Shave Kit! For as low as RM6.50, you can start
your shave plan with us.</h5>
        </div>
    </div>
</section>
@endsection