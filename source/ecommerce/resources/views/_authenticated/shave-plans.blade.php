@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')

<link rel="stylesheet" href="{{ asset('css/_authenticated/shave-plans/shave-plans.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
<style>
  .swal2-styled,.swal2-confirm{
    border: solid 2px #FE5000 !important;
    border-radius: 5px;
    background: transparent !important;
    padding: 14px 45px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: #FE5000 !important;
    margin: auto;
  }
</style>
<!-- -->
@if (empty($latest_sub->latest_sub))
<div class="container">
    <div class="row p-0 text-center">
        <div class="col-12 plan-column-show">
            <p class="fs-20">@lang('website_contents.authenticated.shaveplans.not_on_plan')</p>
            <p class="fs-22">@lang('website_contents.authenticated.shaveplans.custom_plan')</p>
            <div class="col-12 text-center pl-0 pr-0 pt-3">
                <a href="{{route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])}}" class="btn btn-black">@lang('website_contents.authenticated.dashboard.start_shave')</a>
            </div>
        </div>
    </div>
</div>
@endif
@if (!empty($latest_sub->latest_sub))
<section class="shaveplans-current">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pt-4 pb-4 text-center">
                <h1 class="MuliExtraBold fs-20-sm">@lang('website_contents.authenticated.shaveplans.shaving_plans')</h1>
            </div>
            <div class="col-lg-12 pb-1">
                <div class="rounded-10 bg-white shadow padd30 padd-lg-50">
                    <div class="row marg-lg-0">
                        <div class="col-12 pl-lg-0 pr-lg-0">
                            <div class="row">
                                <div class="col-7 padd0">
                                    <h2 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplans.current_plan')</h2>
                                </div>
                                @if($latest_sub->latest_sub->status === 'Pending' || $latest_sub->latest_sub->status === 'Payment Failure' || $latest_sub->latest_sub->status === 'Cancelled')
                                <div class="status-inactive status-tag col-5 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                    <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.inactive')</span>
                                </div>
                                @elseif($latest_sub->latest_sub->status === 'On Hold' || $latest_sub->latest_sub->status === 'Unrealized')
                                <div class="status-onhold status-tag col-5 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                    <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.on_hold')</span>
                                </div>
                                @elseif($latest_sub->latest_sub->status === 'Processing')
                                <div class="status-active status-tag col-5 col-md-2 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                                    <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.active')</span>
                                </div>
                                @endif
                                @if(!empty($latest_sub->latest_sub) && !empty($latest_sub->blade_type))
                                @if($latest_sub->latest_sub->isAnnualDiscountIncludeAddon == 1)
                                <p class="MuliBold fs-25 color-orange"><strong>{{ $latest_sub->blade_type->name }} @lang('website_contents.blade_pack') @lang('website_contents.every') 12 @lang('website_contents.Months')</strong></p>
                                @else
                                <p class="MuliBold fs-25 color-orange"><strong>{{ $latest_sub->blade_type->name }} @lang('website_contents.blade_pack') @lang('website_contents.every') {{ $latest_sub->latest_sub->PlanDuration }} @lang('website_contents.Months')</strong></p>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pl-0 pr-0 fs-16 bord-top-lg bord-bot-lg bord-top-md bord-bot-md">
                            <div class="row padd15 justify-content-between">
                                <div class="col-6 pl-0 pr-0">
                                    <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.next_billing')</strong></p>
                                    @if ($latest_sub->latest_sub->nextChargeDate !== null)
                                    <p class="MuliPlain fs-18 fs-14-sm">{{ \Carbon\Carbon::parse( $latest_sub->latest_sub->nextChargeDate)->format('d M Y') }}</p>
                                    @else
                                    <p class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.under_trial')</p>
                                    @endif
                                    <div class="col-12 padd0 pt-3 d-md-none">
                                        <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.payment_method')</strong></p>
                                        <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.ending_in') {{ $latest_sub->card_info ? $latest_sub->card_info->cardNumber : '-'}}</p>
                                        <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.expiring_in') {{ $latest_sub->card_info ? $latest_sub->card_info->expiredMonth : '-'}} / {{ $latest_sub->card_info ? $latest_sub->card_info->expiredYear : '-'}}</p>
                                    </div>
                                </div>
                                <div class="col-5 col-md-4 pl-0 pr-0">
                                    <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.plan_products')</strong></p>
                                    @if(!empty($latest_sub->product_details))
                                    @foreach ($latest_sub->product_details as $item)
                                    @if($latest_sub->latest_sub->isAnnualDiscountIncludeAddon == 1)
                                    @php($getannualqty = 12/$latest_sub->latest_sub->PlanDuration )
                                    @if ($item->name === '3 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{$getannualqty}} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @elseif($item->name === '5 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{$getannualqty}} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @elseif($item->name === '6 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{$getannualqty}} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @else
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{$getannualqty}} x {{ $item->name }}</p>
                                    @endif
                                    @else
                                    @if ($item->name === '3 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @elseif($item->name === '5 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @elseif($item->name === '6 Blade')
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                                    @else
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }}</p>
                                    @endif
                                    @endif
                                    @endforeach
                                    @endif
                                    @if(!empty($latest_sub->active_addons))
                                    @foreach ($latest_sub->active_addons as $addon)
                                    @if($latest_sub->latest_sub->isAnnualDiscountIncludeAddon == 1)
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $addon->qty }} x {{ $addon->name }}</p>
                                    @else
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $addon->name }}</p>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>

                                <div class="col-6 col-md-4 padd0 d-none d-md-block">
                                    <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.payment_method')</strong></p>
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.ending_in') {{ $latest_sub->card_info ? $latest_sub->card_info->cardNumber : '-'}}</p>
                                    <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.expiring_in') {{ $latest_sub->card_info ? $latest_sub->card_info->expiredMonth : '-'}} / {{ $latest_sub->card_info ? $latest_sub->card_info->expiredYear : '-'}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 pl-0 pr-0 pt-3">
                            <div class="row mx-0">
                                <div class="col-12 col-md-6 mb-1 text-center padd0">
                                    @if(in_array($latest_sub->latest_sub->status,['Pending','Payment Failure','Cancelled']))
                                    <!-- <a style="width: 100%;">
                                        <button class="btn btn-black desktop-view-plan btn-load-more" disabled> @lang('website_contents.authenticated.shaveplans.view_plan')</button>
                                    </a> -->
                                    @else
                                        @if($latest_sub->latest_active_plan_origin_country_matches == true)
                                            <a href="{{ route('locale.region.authenticated.savedplans.edit', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso'), 'subscriptionId'=>$latest_sub->latest_sub->id ]) }}" style="width: 100%;">
                                                <button class="btn btn-black desktop-view-plan btn-load-more"> @lang('website_contents.authenticated.shaveplans.view_plan')</button>
                                            </a>
                                        @else 
                                            <button onclick="PlanCountryOfOriginError()" class="btn btn-black desktop-view-plan btn-load-more"> @lang('website_contents.authenticated.shaveplans.view_plan')</button>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-12 col-md-6 text-right pl-0 pr-0 d-flex align-items-end">
                                    @if(!in_array($latest_sub->latest_sub->status,['Pending','Payment Failure','Cancelled']))
                                    @if($latest_sub->latest_active_plan_origin_country_matches == true)
                                    <a class="ship-my-refill-btn color-orange ml-auto mr-auto mr-lg-0 fs-14-sm fs-16-md fs-16" data-toggle="modal" data-target="#shipRefills"><u>@lang('website_contents.authenticated.shaveplans.refill_now')</u></a>
                                    @else
                                    <a class="ship-my-refill-btn color-orange ml-auto mr-auto mr-lg-0 fs-14-sm fs-16-md fs-16" onclick="PlanCountryOfOriginError()"><u>@lang('website_contents.authenticated.shaveplans.refill_now')</u></a>
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="shipRefills" tabindex="-1" role="dialog" aria-labelledby="addProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h3>@lang('website_contents.authenticated.shaveplans.sure_ship_now')</h3>
                    <!-- Currency and price for kr -->
                    <?php
                               $s_ppc = $latest_sub->latest_sub->price - $latest_sub->latest_sub->appliedDiscount;
                            //    if(strtolower(view()->shared('currentCountryIso')) == 'kr'){
                                   $s_ppc = number_format((float) $s_ppc, 2, '.', '');
                            //    }

                     ?>
                    <p class="fs-20 Muli">@lang('website_contents.authenticated.shaveplans.charged_instantly', ['currency' => isset($latest_sub->latest_sub->currencyDisplay) ? $latest_sub->latest_sub->currencyDisplay : "", 'price' => $s_ppc])</p>
                </div>
                <!-- <div class="col-8 offset-2 text-center">
                    <p class="MuliBold">@lang('website_contents.authenticated.shaveplans.added_items')</p>
                </div> -->
                <div class="row">
                    <div class="col-12 col-md-4 offset-md-2 px-0 px-md-3 mb-2">
                        <button id="button-pay-now" class="btn btn-start" style="min-width:100%">@lang('website_contents.authenticated.shaveplans.ship_now')</button>
                    </div>
                    <div class="col-12 col-md-4 px-0 px-md-3 mb-2">
                        <button class="btn btn-cancel" style="min-width:100%" data-dismiss="modal">@lang('website_contents.authenticated.shaveplans.cancel')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Payment Failed Popup -->
<div class="modal fade" id="notification_payment_failed" tabindex="-1" role="dialog" aria-labelledby="notification_payment_failed" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="col-12 text-center">
                    <h5 id="notification_payment_failed_text"></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(!empty($subs))
<section class="shaveplans pt-5">
    <div class="container">
        <div class="row">
            @foreach($subs as $index => $sub)

            <div class="col-lg-6 d-flex pb-5">
                <div class="col-lg-12 bg-white rounded-10 shadow padd30 padd-lg-50">
                    <div class="row">
                        <div class="col-7 col-md-8 padd0">
                            <h2 class="MuliExtraBold">@lang('website_contents.authenticated.shaveplans.shave_plan_indexed', ['index' => ($index + 2) ])</h2>
                        </div>
                        @if($sub->subscription->status === 'Pending' || $sub->subscription->status === 'Payment Failure' || $sub->subscription->status === 'Cancelled')
                        <div class="status-inactive status-tag col-5 col-md-4 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                            <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.inactive')</span>
                        </div>
                        @elseif($sub->subscription->status === 'On Hold' || $sub->subscription->status === 'Unrealized')
                        <div class="status-onhold status-tag col-5 col-md-4 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                            <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.on_hold')</span>
                        </div>
                        @elseif($sub->subscription->status === 'Processing')
                        <div class="status-active status-tag col-5 col-md-4 padd5 mb-2 text-center bg-green ml-auto color-white float-right d-flex align-items-center">
                            <span class="mx-auto">@lang('website_contents.authenticated.shaveplans.active')</span>
                        </div>
                        @endif
                        @if(!empty($sub->subscription) && !empty($sub->blade_type))
                        @if($sub->subscription->isAnnualDiscountIncludeAddon == 1)
                        <p class="MuliBold fs-25 color-orange"><strong>{{ $sub->blade_type->name }} @lang('website_contents.blade_pack') @lang('website_contents.every') 12 @lang('website_contents.Months')</strong></p>
                        @else
                        <p class="MuliBold fs-25 color-orange"><strong>{{ $sub->blade_type->name }} @lang('website_contents.blade_pack') @lang('website_contents.every') {{ $sub->subscription->PlanDuration }} @lang('website_contents.Months')</strong></p>
                        @endif
                        @endif
                    </div>

                    <div class="row padd15 pl-0 pr-0 bord-top-lg bord-bot-lg justify-content-between">
                        <div class="col-6 pl-0 pr-0">
                            <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.next_billing')</strong></p>
                            @if ($sub->subscription->nextChargeDate !== null)
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ \Carbon\Carbon::parse($sub->subscription->nextChargeDate)->format('d M Y') }}</p>
                            @else
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.under_trial')</p>
                            @endif
                            <div class="col-12 padd0 pt-3 d-md-none">
                                <p class="MuliPlain fs-18 fs-14-sm mb-0"><strong>@lang('website_contents.authenticated.shaveplans.payment_method')</strong></p>
                                <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.ending_in') {{ $sub->card_info ? $sub->card_info->cardNumber : '-'}}</p>
                                <p class="MuliPlain fs-18 fs-14-sm mb-0">@lang('website_contents.authenticated.dashboard.expiring_in') {{ $sub->card_info ? $sub->card_info->expiredMonth : '-'}} / {{ $sub->card_info ? $sub->card_info->expiredYear : '-'}}</p>
                            </div>
                        </div>
                        <div class="col-5 pl-0 pr-0">
                            <p class="MuliBold fs-25 mb-0"><strong>@lang('website_contents.authenticated.shaveplans.plan_products')</strong></p>
                            @if(!empty($sub->product_details))
                            @foreach ($sub->product_details as $item)
                          
                            @if($sub->subscription->isAnnualDiscountIncludeAddon == 1)
                            @php($getannualqty = 12/$sub->subscription->PlanDuration )
                            @if ($item->name === '3 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $getannualqty }} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @elseif($item->name === '5 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $getannualqty }} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @elseif($item->name === '6 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $getannualqty }} x {{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @else
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $getannualqty }} x {{ $item->name }}</p>
                            @endif
                            @else
                            @if ($item->name === '3 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @elseif($item->name === '5 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @elseif($item->name === '6 Blade')
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }} @lang('website_contents.blade_pack')</p>
                            @else
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $item->name }}</p>
                            @endif
                            @endif
                  
                            @endforeach
                            @endif
                            @if(!empty($sub->active_addons))
                            @foreach ($sub->active_addons as $addon)
                            @if($sub->subscription->isAnnualDiscountIncludeAddon == 1)
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $addon->qty }} x {{ $addon->name }}</p>
                            @else
                            <p class="MuliPlain fs-18 fs-14-sm mb-0">{{ $addon->name }}</p>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center pt-4 pl-0 pr-0 d-flex align-items-end">
                            @if(in_array($sub->subscription->status,['Pending','Payment Failure','Cancelled']))
                            <!-- <a style="width: 100%;"><button class="btn btn-load-more w-100" disabled> @lang('website_contents.authenticated.shaveplans.view_plan')</button></a> -->
                            @else
                            @if($sub->plans_origin_country_matches == true)
                            <a class="btn btn-load-more" href="{{ route('locale.region.authenticated.savedplans.edit', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso'), 'subscriptionId'=>$sub->subscription->id ]) }}" style="width: 100%;">@lang('website_contents.authenticated.shaveplans.view_plan')</a>
                            @else 
                            <button onclick="PlanCountryOfOriginError()" class="btn btn-load-more" style="width: 100%;"> @lang('website_contents.authenticated.shaveplans.view_plan')</button>
                            @endif
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </div>
</section>
@endif


<script>
    // Get varibles from PHP
    let subs = {!!json_encode($latest_sub->latest_sub) !!};
    let currentCountryIso = "<?php echo strtolower(view()->shared('currentCountryIso')) ?>";
    $(function() {

        // Check if got any payment error message after paynow OTP is completed,
        // Then, clear the session completely
        DisplayPaymentError();

        // OnClick pay now button
        $("#button-pay-now").click(function() {

            $("#loading").css("display", "block");
            if(currentCountryIso == "kr"){
                let PAY_NOW_URL =
                GLOBAL_URL +
                "/user/shave-plan/paynow/nicepay";


            AJAX(PAY_NOW_URL, 'POST', {
                    "subscription_id": subs.id,
                })
                .done(function(data) {

                    // Close ship refills modal
                    $("#shipRefills").modal("hide");

                    if (data.status === "success") {
                        window.location = window.location.origin + GLOBAL_URL + '/thankyou/' + data.orderId;
                    } else if (data.status === "failure") {
                        $("#notification_payment_failed_text").html(data.message);
                        $("#notification_payment_failed").modal("show");
                    } else if (data.status === "requires_source_action") {
                        session("subscription_pay_now", "set", JSON.stringify(data.subs)).done(function() {
                            window.location = data.subs.charge.next_action.redirect_to_url.url;
                        });
                    } else {
                        console.log("Failed to pay now.");
                    }
                    $("#loading").css("display", "none");
                })
                .fail(function(jqXHR, textStatus, error) {
                    $("#loading").css("display", "none");
                    console.log("Failed to pay now.");
                });
            }
            else{
            let PAY_NOW_URL =
                GLOBAL_URL +
                "/user/shave-plan/paynow";

            let RETURN_URL_WITH_OTP =
                window.location.origin +
                GLOBAL_URL +
                `/stripe/redirect?is_pay_now=1`;

            AJAX(PAY_NOW_URL, 'POST', {
                    "subscription_id": subs.id,
                    "return_url": RETURN_URL_WITH_OTP
                })
                .done(function(data) {

                    // Close ship refills modal
                    $("#shipRefills").modal("hide");

                    if (data.status === "success") {
                        window.location = window.location.origin + GLOBAL_URL + '/thankyou/' + data.orderId;
                    } else if (data.status === "failure") {
                        $("#notification_payment_failed_text").html(data.message);
                        $("#notification_payment_failed").modal("show");
                    } else if (data.status === "requires_source_action") {
                        session("subscription_pay_now", "set", JSON.stringify(data.subs)).done(function() {
                            window.location = data.subs.charge.next_action.redirect_to_url.url;
                        });
                    } else {
                        console.log("Failed to pay now.");
                    }
                    $("#loading").css("display", "none");
                })
                .fail(function(jqXHR, textStatus, error) {
                    $("#loading").css("display", "none");
                    console.log("Failed to pay now.");
                });
            }
        });

    })

    function PlanCountryOfOriginError(){
        Swal.fire(
                '',
                trans('website_contents.global.not_country_origin_plan', {}),
                'error'
            ).then((result) => {
        })
    }

    async function DisplayPaymentError() {
        await session("subscription_pay_now", "get", null).then(data => {
            if (data) {
                let dataParsed = JSON.parse(data);
                if (dataParsed.status === "success") {
                    $("#notification_payment_success").modal("show");
                } else {
                    $("#notification_payment_failed_text").html(dataParsed.message);
                    $("#notification_payment_failed").modal("show");
                }
                session("subscription_pay_now", "clear", null);
            }
        });
    }
</script>
@endsection
