@extends('layouts.app')

@section('content')
@include('_authenticated.layouts.header.header')
<meta property="og:site_name" content="My Company Name" />
<meta property="fb:app_id" content="723177734789504" />
<link rel="stylesheet" href="{{ asset('css/_authenticated/referrals/referrals.css') }}">
<link rel="stylesheet" href="{{ asset('css/_authenticated/_authenticated.css') }}">
<style>
   #kakaostory-share-button a img {
    display: none;
    }
    .swal2-styled,.swal2-confirm{
    border: solid 2px #FE5000 !important;
    border-radius: 5px;
    background: transparent !important;
    padding: 14px 45px;
    cursor: pointer;
    text-align: center;
    font-size: 16px !important;
    color: #FE5000 !important;
    margin: auto;
  }
</style>

<section class="referrals-content" style="padding-bottom: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 padd30 pb-2 text-center">
                <h1 class="MuliExtraBold">@lang('website_contents.authenticated.referrals.referrals')</h1>
            </div>
            <div class="referrals-desc col-lg-12">
                <div class="col-12 text-center pl-0 pr-0">
                    <p class="MuliPlain fs-18 fs-14-sm">
                        @lang('website_contents.authenticated.referrals.get_rewarded',['currency' => $user_info->country->currencyDisplay, 'value' => $user_info->referral_info->ref_currency->value])
                    </p>
                </div>
            </div>
            <div class="col-lg-12 referrals-container">
                <div class="referrals-counter row shadow-md color-white p-4">
                    <div class="col-12 padd15">
                        <div class="row mx-0 text-center">
                            <div class="col-12 col-lg-6 offset-0 offset-lg-3 padd0">
                                <h2 class="MuliExtraBold">@lang('website_contents.authenticated.referrals.your_referrals')</h2>
                            </div>
                            <div class="col-3 padd0 d-none d-lg-block">
                                @if($referral_cash && !$retrieveLastCashOut)
                                    @if($referral_cash > 0)
                                        <button class="btn bg-white color-orange m border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                        </button>
                                    @else
                                        <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                        </button>
                                    @endif
                                @elseif($referral_cash && $retrieveLastCashOut)
                                    @if($referral_cash + $retrieveLastCashOut["amount"] > $retrieveLastCashOut["status"] === "in_process")
                                        <button class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                        </button>
                                    @elseif($referral_cash + $retrieveLastCashOut["amount"] < $retrieveLastCashOut["status"]==="in_process" )
                                        <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.processing')
                                        </button>
                                    @elseif(($referral_cash + $retrieveLastCashOut["amount"]) == ($retrieveLastCashOut["status"] === "in_process"))
                                        <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.processing')
                                        </button>
                                    @elseif(($referral_cash + $retrieveLastCashOut["amount"]) == ($retrieveLastCashOut["status"] === "withdrawn"))
                                        <button class="btn bg-white color-orange m border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                        </button>
                                    @else
                                        <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                        </button>
                                    @endif
                                @else
                                    <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                            @lang('website_contents.authenticated.referrals.cash_out')
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="d-none d-lg-inline-flex m-auto">
                        <div class="col-12 col-lg-4 border-right">
                            <div class="row align-items-center mx-0">
                                <div class="col-8 text-left fs-20 Muli pl-4 pr-0" style="line-height: 25px">
                                    <p class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.authenticated.referrals.sign_ups')</p>
                                </div>
                                <div class="col-4 text-right">
                                    <p class="MuliPlain fs-25 fs-14-sm">{{$retrieveTotalSignUps}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 border-right">
                            <div class="row align-items-center mx-0">
                                <div class="col-6 text-left fs-20 Muli pl-4 pr-0" style="line-height: 25px">
                                    <p class="MuliPlain fs-18 fs-14-sm"> @lang('website_contents.authenticated.referrals.completed_billing')</p>
                                </div>
                                <div class="col-6 text-right">
                                    <p class="MuliPlain fs-25 fs-14-sm">{{$retrieveTotalSucessful}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="row align-items-center mx-0">
                                <div class="col-6 text-left fs-20 Muli pl-4 pr-0" style="line-height: 25px">
                                    <p class="MuliPlain fs-18 fs-14-sm">@lang('website_contents.authenticated.referrals.referral_cash')<p>
                                </div>
                                <div class="col-6 text-right">
                                @if(strtolower($currentCountryIso) == 'kr')
                                <!-- Currency and price for kr -->
                                  <?php
                               $r_cash =  number_format($referral_cash) ;
                               ?>
                                 <p class="MuliPlain fs-25 fs-14-sm">{{$r_cash}}{{$user_info->country->currencyDisplay}}
                               @else
                               <p class="MuliPlain fs-25 fs-14-sm">{{$user_info->country->currencyDisplay}}{{$referral_cash}}
                               @endif
                                  
                                        <span class="d-none">{{$user_info->referral_info->ref_currency->value}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-block d-lg-none w-100">
                        <div class="col-12 pr-0">
                            <div class="row align-items-center mx-0 border-bottom">
                                <div class="col-10 text-left fs-14 Muli pr-0">
                                    @lang('website_contents.authenticated.referrals.sign_ups')
                                </div>
                                <div class="col-2 text-right MuliSemiBold fs-30">
                                    {{$retrieveTotalSignUps}}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pr-0">
                            <div class="row align-items-center mx-0 border-bottom">
                                <div class="col-10 text-left fs-14 Muli pr-0">
                                    @lang('website_contents.authenticated.referrals.completed_billing')
                                </div>
                                <div class="col-2 text-right MuliSemiBold fs-30">
                                    {{$retrieveTotalSucessful}}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pr-0">
                            <div class="row align-items-center mx-0 border-bottom">
                                <div class="col-8 text-left fs-14 Muli pr-0">
                                    @lang('website_contents.authenticated.referrals.referral_cash')
                                </div>
                                <div class="col-4 text-right MuliSemiBold fs-30">
                                @if(strtolower($currentCountryIso) == 'kr')
                                <!-- Currency and price for kr -->
                                  <?php
                               $r_cash =  number_format($referral_cash) ;
                               ?>
                                 {{$referral_cash}}{{$user_info->country->currencyDisplay}}
                               @else
                               {{$user_info->country->currencyDisplay}}{{$referral_cash}}
                               @endif
                                    <span class="d-none">{{$user_info->referral_info->ref_currency->value}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 padd0 d-block d-lg-none pt-3">
                        <div class="row align-items-center mx-0 pl-4 pr-4">
                            @if($referral_cash && !$retrieveLastCashOut)
                            @if($referral_cash > 0)
                            <button class="btn bg-white color-orange m border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                @lang('website_contents.authenticated.referrals.cash_out')
                            </button>
                            @else
                            <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                @lang('website_contents.authenticated.referrals.cash_out')
                            </button>
                            @endif
                            @elseif($referral_cash && $retrieveLastCashOut)
                            @if($referral_cash + $retrieveLastCashOut["amount"] > $retrieveLastCashOut["status"] === "in_process")
                            <button class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                @lang('website_contents.authenticated.referrals.cash_out')
                            </button>
                            @elseif($referral_cash + $retrieveLastCashOut["amount"] < $retrieveLastCashOut["status"]==="in_process" ) <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">PROCESSING</button>
                                @elseif(($referral_cash + $retrieveLastCashOut["amount"]) == ($retrieveLastCashOut["status"] === "in_process"))
                                <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                    @lang('website_contents.authenticated.referrals.processing')
                                </button>
                                @else
                                <button disabled class="btn bg-white color-orange border-orange padd5 float-right MuliBold pl-4 pr-4" data-toggle="modal" data-target="#cashOut">
                                    @lang('website_contents.authenticated.referrals.cash_out')
                                </button>
                                @endif
                            @else
                            <button disabled class="btn bg-white color-orange border-orange padd5 MuliBold  w-100" data-toggle="modal" data-target="#cashOut">
                                @lang('website_contents.authenticated.referrals.cash_out')
                            </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div id="cashOut" class="modal fade" role="dialog" aria-labelledby="cashOutLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form id="submit_referral_cash_withdrawals" method="POST" action="">
                            <div class="modal-body p-5">
                                <div class="col-12 text-center">
                                    <h3 class="MuliExtraBold fs-25">@lang('website_contents.authenticated.referrals.fill_in_details')</h3>
                                </div>
                                <div class="col-8 offset-2">
                                    <div class="form-group">
                                        <label for="bank_name"><b>@lang('website_contents.authenticated.referrals.bank')</b></label>
                                        <input type="text" name="bank_name" class="bank_name form-control-2" id="bank_name" placeholder="" maxlength="50" />
                                        <div id="bank_name_error" class="hasError col-form-label text-left"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="withdrawal_name"><b>@lang('website_contents.authenticated.referrals.full_name')</b></label>
                                        <input type="text" name="withdrawal_name" class="withdrawal_name form-control-2" id="withdrawal_name" placeholder="" maxlength="50" />
                                        <div id="withdrawal_name_error" class="hasError col-form-label text-left"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_account_no"><b>@lang('website_contents.authenticated.referrals.account_number')</b></label>
                                        <input type="text" name="bank_account_no" class="bank_account_no form-control-2" id="bank_account_no" placeholder="" maxlength="30" />
                                        <div id="bank_account_no_error" class="hasError col-form-label text-left"></div>
                                    </div>
                                    <p type="text" class="withdrawal_amount" id="withdrawal_amount"></p>
                                </div>
                                <div class="row mx-0">
                                    <div class="col-12 col-md-4 offset-md-2 px-3 mb-2">
                                        <button type="submit" class="btn btn-start MuliBold text-uppercase" style="min-width:100%">@lang('website_contents.global.content.submit')</button>
                                    </div>
                                    <div class="col-12 col-md-4 px-3 mb-2">
                                        <button class="btn btn-cancel MuliBold text-uppercase" style="min-width:100%" data-dismiss="modal">@lang('website_contents.global.content.cancel')</button>
                                    </div>
                                    <div class="col-12 text-center fs-sm-24">
                                        <b>@lang('website_contents.authenticated.referrals.transaction_credited')</b>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal -->

            <div class="modal fade pr-0" id="emailShare"  role="dialog" aria-labelledby="emailShare" aria-hidden="true" >
                <div class="modal-dialog modal-dialog-centered modal-dialog-emailShare" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-email-send">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body pl-5 pr-5" style="padding-top: 0 !important">
                            <div class="col-12 p-0">
                                <div class="row padd10">
                                    <form id="send_referral_invitation_email" method="POST" action="" style="" class="w-100">
                                    <p class="pl-4 pr-4 successmessage" id="email-send-msg"></p>
                                        <div class="pb-2">
                                            <input class="col-12 fs-16 padd20" type="text" id="referral_email_lists" required name="referral_email_lists" placeholder="@lang('website_contents.authenticated.referrals.email_friend')" />
                                            <div id="email_error" class="hasError col-form-label text-left"></div>
                                        </div>
                                        <p class="text-center fs-18">@lang('website_contents.authenticated.referrals.share_invitation')</p>
                                        <div class="col-12 text-center pt-2">
                                            <button type="submit" id="get_started-next" class="button-next button-proceed-checkout MuliExtraBold" style="width: 50%" href="{{$referral_link}}">
                                                @lang('website_contents.authenticated.referrals.send')
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="cashOutConfirmation" class="modal fade" role="dialog" aria-labelledby="cashOutConfirmationLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-5">
                            <div class="col-12 text-center text-success mb-3">
                                <h3 class="MuliExtraBold fs-30">@lang('website_contents.authenticated.referrals.thanks_bank')</h3>
                            </div>
                            <div class="col-12 text-center fs-sm-24 mb-5">
                                <b>@lang('website_contents.authenticated.referrals.withdrawal_credited')</b>
                            </div>
                            <div class="row mx-0 justify-content-center">
                                <div class="col-12 col-md-4 px-3 mb-2">
                                    <button class="btn btn-start MuliBold" style="min-width:100%" data-dismiss="modal">
                                        @lang('website_contents.authenticated.referrals.close')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 padd-md30-lg50" style="z-index: 0;">
                <div class="row mx-0" style="margin-top: 40px; margin-bottom: -30px;">
                    <div class="col-12 d-lg-none text-center padd0">
                        <p class="MuliExtraBold fs-16">@lang('website_contents.authenticated.referrals.start_sharing_mobile')</p>
                    </div>
                    <div class="col-6 col-lg-3 mt-3" style="z-index: -1;">
                        <img class="img-fluid float-right" src="{{asset('/images/common/referral-woman.png')}}">
                    </div>
                    <div class="col-6 d-none d-lg-block text-center">
                        <p class="MuliBold fs-25 pt-3" style="line-height: 35px">@lang('website_contents.authenticated.referrals.start_sharing_desktop')</p>
                    </div>
                    <div class="col-6 col-lg-3" style="z-index: -1;">
                        <img class="img-fluid float-left" src="{{asset('/images/common/referral-man.png')}}">
                    </div>
                </div>
                <div class="row py-4 border-lg border-dark bg-white rounded-10 shadow-md" style="z-index: 1">
                    <div class="col-10 pr-0 d-lg-none referrals-share">

                        <p class="MuliBold fs-25 color-orange"><b>@lang('website_contents.authenticated.referrals.share_now')</b></p>

                    </div>
                    <div class="col-2 col-lg-3 pl-0 center-lg right-md referrals-share share-now-column">

                        <img class="img-fluid" src="{{asset('/images/common/referral-share.png')}}" />
                        <p class="MuliBold fs-25 color-orange d-none d-lg-block d-xl-block"><b>@lang('website_contents.authenticated.referrals.share_now')</b></p>

                    </div>
                    <div class="col-md-12 col-lg-9 share-via-column">
                        <div class="col-md-12 col-lg-11 referral-link-box">
                            <div class="row padd10 border border-dark rounded mb-2">
                                <div class="col-lg-12">
                                    <p class="MuliBold fs-18 mb-1">@lang('website_contents.authenticated.referrals.unique_link')</p>
                                </div>
                                <div class="col-lg-12" style="display:inline-flex;">
                                    <div class="col-11 px-0">
                                        <p id="referrals_unique_link" class="color-orange fs-17 truncate">{!! $referral_link !!}</p>
                                    </div>
                                    <div class="col-1 px-0 text-right" style='bottom: 10%;'>
                                        <span><i class="fa fa-copy pulse-single" style='font-size: 2em;padding: 3%;color: #ff5001;' onclick="copyToClipboard('id','referrals_unique_link')"></i></span>
                                        {{-- <span><i class="fa fa-link"
                                            onclick="socialSharing('{{$referral_link}}')"></i></span> --}}
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="row padd10 border border-orange rounded">
                                <a class="MuliExtraBold fs-20 color-orange" href="">SHARE LINK</a>
                            </div>
                              --}}

                        </div>

                        <div class="col-md-12 col-lg-9 d-inline-flex p-0">
                            <p class="MuliPlain fs-18 fs-14-sm mb-0"><b>@lang('website_contents.authenticated.referrals.share_via')</b></p>
                        </div>
                        <div class="col-md-12 col-lg-9 d-inline-flex p-0 pl-0 pt-2 pr-3">
                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-6 pl-0 pr-0 social-share-btn">
                                {!! $referral_share_template !!}
                            </div>
                            <div class="col-5 pl-0 email-share-btn">
                                <div class="row justify-content-center">
                                    <div class="col-12 mr-0 pr-0 pl-0 ml-0" id="email-links">
                                        <ul style="width: 91%">
                                            <li style="margin-right:10%;"><a href="" class="social-button" id="email-share-button">
                                                    <div class="col-12">
                                                        <div class="row justify-content-center">
                                                            <div>
                                                                <img class="img-fluid d-none d-lg-inline-block ml-0" src="{{ asset('images/common/btn-share-email.png') }}"/>
                                                                <img class="img-fluid d-block d-lg-none ml-3" src="{{ asset('images/common/mobile-email-share.png') }}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <div id="kakaostory-share-button" data-url="https://dev.kakao.com"><div id="kakao-referral-share"></div></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</section>

<div>
</div>

<!-- Referrals -->
<script src="{{ asset('js/functions/rebates/referral.js') }}"></script>
<script src="{{ asset('js/functions/_authenticated/orderhistory/orderhistory.function.js') }}"></script>
<script>
    let kakao_referral = {!!json_encode($referral_kakao_link)!!}
</script>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script>
    window.kakaoAsyncInit = function () {
        Kakao.Story.createShareButton({
            container: '#kakaostory-share-button',
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//developers.kakao.com/sdk/js/kakao.story.min.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'kakao-js-sdk'));
</script>

<script type="text/javascript">
    let countryData = window.localStorage.getItem('currentCountry');

    if(JSON.parse(countryData).codeIso == "KR"){
        $('#kakao-referral-share').html("<div data-url='" + kakao_referral + "'><img id='kakaoImg' class='img-fluid d-none d-lg-inline-block' src='{{asset('/images/common/btn-share-kakao.png')}}'/><img id='kakaoImg2' class='img-fluid d-block d-lg-none ml-2' src='{{asset('/images/common/mobile-kakao-share.png')}}' /></div></div>")

        $("#social-link-whatsapp").html("<div class='col-12'><div class='row justify-content-center'><img class='img-fluid d-none d-lg-inline-block' src='{{asset('/images/common/btn-share-whatsapp.png')}}'/><img class='img-fluid d-block d-lg-none' src='{{asset('/images/common/mobile-whatsapp-share.png')}}' /></div></div>")
    } else {
        $("#social-link-whatsapp").html("<div class='col-12'><div class='row justify-content-center'><img class='img-fluid d-none d-lg-inline-block' src='{{asset('/images/common/btn-share-whatsapp.png')}}'/><img class='img-fluid d-block d-lg-none' src='{{asset('/images/common/mobile-whatsapp-share.png')}}' /></div></div>")
    }

    $("#social-link-facebook").html("<div class='col-12'><div class='row justify-content-center'><img class='img-fluid d-none d-lg-inline-block' src='{{asset('/images/common/btn-share-fb.png')}}' /><img class='img-fluid d-block d-lg-none' src='{{asset('/images/common/mobile-fb-share.png')}}' /></div></div>")

    $("#social-link-twitter").html("<div class='col-12'><div class='row justify-content-center'><img class='img-fluid d-none d-lg-inline-block' src='{{asset('/images/common/btn-share-twit.png')}}' /><img class='img-fluid d-block d-lg-none' src='{{asset('/images/common/mobile-twit-share.png')}}' /></div></div>")
</script>

<script type="text/javascript">
    $('#kakaoImg').click(function() {
        $("#kakaostory-share-button a img").click();
    });

    $('#kakaoImg2').click(function() {
        $("#kakaostory-share-button a img").click();
    });
</script>
@endsection
