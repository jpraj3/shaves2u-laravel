<style>
.close{
        font-size: 40px;
    }

.header{
    margin-top: -20px;
    margin-bottom: 20px;
}

.blade-image{
    margin-top: 30px;
    width: 40%;
    margin-bottom: 30px;
}

.button-keep-shaving{
    background-color: #ff5001 !important;
    color: white !important;
}
</style>

<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body" style="padding: 1em 3em 3em 3em;">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.free-product-applied-page.header')</b></h3>
        <h5>@lang('cancellation.modal.free-product-applied-page.subheader-1')</h5>
        <h5>@lang('cancellation.modal.free-product-applied-page.subheader-2')</h5>
        <img class="blade-image" src="{{$product_default_image_free_blade}}" />
        <br>
        <button id="close" type="button" class="btn btn-load-more button-keep-shaving back" style="min-width:33%">@lang('cancellation.modal.global.ok-great')</button>
    </div>
</div>

<script>
    $(function(){
        $(".back").click(function() {
            window.location = window.location.origin + GLOBAL_URL + "/user/shave-plan";
        });
    })
</script>