<style>
.close{
        font-size: 40px;
    }

.header{
    margin-top: -20px;
    margin-bottom: 20px;
}

.cry-image{
    width:20%;
    margin-top:30px;
    margin-bottom:30px;
}
</style>

<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body" style="padding: 1em 3em 3em 3em;">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.cancelled-page.header')</b></h3>
        <h5>@lang('cancellation.modal.cancelled-page.subheader-1')</h5>
        <h5>@lang('cancellation.modal.cancelled-page.subheader-2')</h5>
        <img class="cry-image" src="{{asset('/images/common/cancellation-journey/cancellation_crying_face.png')}}" />
        <br>
        <button id="close" type="button" class="btn btn-cancel back" style="min-width:33%">@lang('cancellation.modal.global.close')</button>
    </div>
</div>

<script>
    $(function(){
        $(".back").click(function() {
            window.location = window.location.origin + GLOBAL_URL + "/user/shave-plan";
        });
    })
</script>