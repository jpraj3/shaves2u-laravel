<style>
    .close{
        font-size: 40px;
    }
    .header{
        margin-top: -20px;
        margin-bottom: 20px;
    }
    .reason-selected{
        background-color:#ff5001;
        color:white;
        padding:10px !important;
        border:1px solid #ececec;
        border-radius: 8px;
        margin-bottom:10px;
        vertical-align: middle;
        text-align:center;
        font-size: 18px;
    }
    .reason-unselected{
        background-color:#ececec;
        color:#000000;
        padding:10px !important;
        border:1px solid #cccccc;
        border-radius: 8px;
        margin-bottom:10px;
        vertical-align: middle;
        text-align:center;
        font-size: 18px;
    }

    .other-reason-box{
        background-color: white;
        border:1px solid #cccccc;
        border-radius: 8px;
        width: 100%;
    }

    .dont-cancel{
        background-color: black !important;
        color: white !important;
    }
</style>
<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body">
    <div class="col-12 text-center">
        <h4 class="header"><b>@lang('cancellation.modal.main.header')</b></h4>
        <h6>@lang('cancellation.modal.main.subheader-1')</h6>
        <h6>@lang('cancellation.modal.main.subheader-2')</h6>
        <div style="col-12 display:flex; justify-content:center; margin-top:20px;">
            <div class="w-100">
                @foreach ($data->cancellationReasons as $reason)
                    <div id="reason-{{$reason->id}}" class="reason-unselected">
                        <div style="text-align: center">
                            {{$reason->content}}
                        </div>        
                    </div>
                @endforeach
                    <div id="reason-other-0" class="reason-unselected">
                        <div style="text-align: center">
                            @lang('cancellation.modal.global.other-reason')
                        </div>
                        <div>
                        <textarea id="modal_level_0_other_reason_text" class="other-reason-box" rows="2" style="resize:none" disabled></textarea>   
                        </div>     
                    </div>
            </div>
        </div>  
        
        <div class="col-12 mx-0 d-inline-block">
            <div class="row justify-content-center">
                <div class="col-md-6 col-xs-12 text-center pt-2">
                    <button id="proceed" type="button" class="btn btn-load-more" style="width:100%; white-space: normal; ">@lang('cancellation.modal.global.proceed-cancellation')</button>
                </div>
                <div class="col-md-6 col-sm-12 text-center pt-2">
                    <button class="btn btn-cancel back dont-cancel" style="min-width:100%">@lang('cancellation.modal.global.dont-cancel')</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function(){

        let data = {!! json_encode($data) !!};
        let selected_option = "";
        let selected_reason = "";
        let other_reason = "";

        // OnClick Predefined Reason
        for (let i = 0; i < data.cancellationReasons.length; i++) {
            let cancellationReasonId = data.cancellationReasons[i].id;
            $("#reason-" + cancellationReasonId).click(SelectCancellationReason_MainModal(data.cancellationReasons[i]));
        }

        // OnClick Other Reason
        $("#reason-other-0").click(SelectCancellationReason_MainModal(0));

        // OnClick Proceed
        $("#proceed").click(function() {
            if(selected_option === "") 
            {
                CancelSubscription(data.subscription.id,"","",$(this));
            }
            else if(selected_option === "modal_level_0_others")
            {
                other_reason = $("#modal_level_0_other_reason_text").val();
                
                let cancellationData = {
                    other_reason
                }
                
                // Immediately cancel if annual plan
                if(data.cancellationBenefitStatus.isAnnualPlan || data.cancellationBenefitStatus.haveAppliedCancellationDiscount || data.cancellationBenefitStatus.isWithinTrialPeriod) {
                    CancelSubscription(data.subscription.id,selected_reason.id,other_reason,$(this));
                }
                else {
                    session('cancellation_journey', 'set', JSON.stringify(cancellationData)).done(function() {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/navigate/${data.subscription.id}/${data.nextPageCancellation.id}/${data.nextPageCancellation.viewName}`);    
                    })
                }
            }
            else 
            {   
                // Immediately cancel if annual plan
                if(data.cancellationBenefitStatus.isAnnualPlan && ["option1","option3","option4","option5","option6","option7"].includes(selected_reason.viewName)) {
                    CancelSubscription(data.subscription.id,selected_reason.id,"",$(this));
                }
                // Immediately cancel if already get cancellation discount before this or still in trial period
                else if((data.cancellationBenefitStatus.haveAppliedCancellationDiscount || data.cancellationBenefitStatus.isWithinTrialPeriod) && ["option1","option5","option6","option7"].includes(selected_reason.viewName)) {
                    CancelSubscription(data.subscription.id,selected_reason.id,"",$(this));
                }
                else {
                    $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/navigate/${data.subscription.id}/${selected_reason.id}/${selected_reason.viewName}`);
                }
            }
        });

        $(".back").click(function() {
            ClosePopup();
        });

        // Function: Select handle 
        function SelectCancellationReason_MainModal(cancellationReason) {
            return function () {
                if(cancellationReason.id > 0) {
                    
                    selected_option = "reason-" + cancellationReason.id;
                    selected_reason = cancellationReason;
                    $("#reason-" + cancellationReason.id).removeClass('reason-unselected').addClass('reason-selected');
                    $("#reason-other-0").removeClass('reason-selected').addClass('reason-unselected');
                    $("#modal_level_0_other_reason_text").prop('disabled', true);
                    data.cancellationReasons.forEach(reason => {
                        if (reason.id != cancellationReason.id) {
                            $("#reason-" + reason.id).removeClass('reason-selected').addClass('reason-unselected');
                        }
                    });
                } else {
                    selected_option = "modal_level_0_others";
                    $("#reason-other-0").removeClass('reason-unselected').addClass('reason-selected');
                    $("#modal_level_0_other_reason_text").prop('disabled', false);
                    data.cancellationReasons.forEach(reason => {
                        if (reason.id != cancellationReason.id) {
                            $("#reason-" + reason.id).removeClass('reason-selected').addClass('reason-unselected');
                        }
                    });
                }
            };
        };

    });
</script>