<style>
    .hidden{
        display: none;
    }
</style>

<div id="cancellation-journey-router" />

<script>
    $(document).ready(function(){
        // console.log("Cancellation journey loaded.");
        let data = {!! json_encode($data) !!};
        session('cancellation_journey', 'clear', null);
        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/navigate/${data.subscription.id}/0/main`);
    });
    
    function CancelSubscription(subscriptionId,cancellationTranslatesId,otherReason,element) {
        element.prop('disabled', true);
        let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";
        let cancellationData =  
        {
            "SubscriptionId" : subscriptionId,
            "CancellationTranslatesId" : cancellationTranslatesId,
            "OtherReason" : otherReason
        }
        AJAX(url, "POST", cancellationData)
            .done(function(resp) {
                if(resp === 'success') {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                } else {
                    element.prop('disabled', false);
                    // console.log(resp);
                }
            });
    }

    function ClosePopup(){
        $("#cancellationJourneyModal").modal("hide");
    }
</script>