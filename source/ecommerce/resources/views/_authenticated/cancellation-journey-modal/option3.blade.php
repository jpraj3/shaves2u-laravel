<style>

.close{
        font-size: 40px;
    }

.header{
    margin-top: -20px;
    margin-bottom: 20px;
}

.discount{
    color: #ff5001;
    font-size: 23px;
    font-weight: bolder;
}

.button-keep-shaving{
    background-color: #ff5001 !important;
    color: white !important;
}

.dropdown-list{
        padding:10px;
        border: 1px #cccccc black;
        border-radius: 2px;
    }
</style>

<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.option3.header')</b></h3>
        <h5>@lang('cancellation.modal.option3.subheader-1')</h5>
    <br>
    <div><b>@lang('cancellation.modal.option3.text-1')</b></div>
    <br>
    <div class="row" style="justify-content: center;">
        <select id="blade-list" name="blade-list" class="dropdown-list col-lg-3 col-xs-12 mt-2">
            @foreach ($data->bladeList as $blade)
                @if($blade['ProductCountryId'] === $data->currentBlade['ProductCountryId'])
                {
                    <option value="{{json_encode($blade)}}" selected>{{$blade['productTranslatesName']}} - {{$data->country->currencyDisplay}} {{$blade['sellPrice']}}</option>
                }
                @else
                {
                    <option value="{{json_encode($blade)}}">{{$blade['productTranslatesName']}} - {{$data->country->currencyDisplay}} {{$blade['sellPrice']}}</option>
                }
                @endif
            @endforeach
        </select>
        <div class="col-lg-2 col-xs-12 mt-2"><b>@lang('cancellation.modal.option3.text-2')</b></div>
        <select id="plan-frequency-list" name="plan-frequency-list" class="dropdown-list col-lg-3 col-xs-12 mt-2">
            @foreach ($data->planFrequencyList as $planFrequency)
                @if($planFrequency['duration'] === $data->currentPlanFrequency)
                {
                    @if($planFrequency['duration'] !== 12)
                    <option value="{{json_encode($planFrequency)}}" selected>{{$planFrequency['durationTranslateName']}}</option>
                    @endif
                }
                @else
                {
                    @if($planFrequency['duration'] !== 12)
                    <option value="{{json_encode($planFrequency)}}">{{$planFrequency['durationTranslateName']}}</option>
                    @endif
                }
                @endif
            @endforeach
        </select>
    </div>
    <img id="current-blade-image" class="img-responsive" height="200px" src="{{$data->currentBlade['product_default_image']}}" alt="image">
    <br>
    @if(!$data->cancellationBenefitStatus['haveAppliedCancellationDiscount'])
        <div>@lang('cancellation.modal.option3.text-3', ['discount' => $data->cancellationDiscount])</div>
    @endif
    </div>

    <div class="col-12 mx-0 d-inline-block">
        <div class="row justify-content-center">
            <div class="col-lg-5  col-xs-12 text-center mt-2 pl-0 pr-0">
                <button id="continue" type="button" class="btn btn-load-more button-keep-shaving w-100">@lang('cancellation.modal.global.keep-shaving')</button>
            </div>
            <div class="col-lg-5 offset-lg-1 col-xs-12 text-center mt-2 pl-0 pr-0">
                <button id="cancel" class="btn btn-cancel w-100">@lang('cancellation.modal.global.cancel-shave-plan')</button>
            </div>
        </div>
    </div>

</div>

<script>
    $(function(){
        let data = {!! json_encode($data) !!};
        // console.log(data);
        let gender = "male";
        if(data){
            if(data.currentPlanData.plan_details.plan_gender_type == "female"){
                gender = "female";
            }
        }
        $("#blade-list").change(function(){
            var selectedBlade = JSON.parse(($(this).children("option:selected").val()));
            $('#current-blade-image').attr('src',selectedBlade.product_default_image);
        });


        // OnClick Try a Different Blade
        $("#continue").click(function() {

            let modifiedBladeName = JSON.parse(($("#blade-list").children("option:selected").val())).productDefaultName;
            let modifiedBladeSku = JSON.parse(($("#blade-list").children("option:selected").val())).sku;
            let modifiedBladeProductCountryId = JSON.parse(($("#blade-list").children("option:selected").val())).ProductCountryId;
            let modifiedFrequencyName = JSON.parse(($("#plan-frequency-list").children("option:selected").val())).durationDefaultName;
            let modifiedFrequencyDuration = JSON.parse(($("#plan-frequency-list").children("option:selected").val())).duration;

            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/continue_subscription";

            let action_parameters = {
                modifiedBladeName,
                modifiedBladeSku,
                modifiedBladeProductCountryId,
                modifiedFrequencyName,
                modifiedFrequencyDuration
            };

            let actionsArray = gender == "female" ? ["change_frequency"] : ["change_blade","change_frequency"];

            if(!data.cancellationBenefitStatus.haveAppliedCancellationDiscount) {
                actionsArray.push("apply_cancellation_discount");
            }

            let cancellationData =
            {
                "Actions" : actionsArray,
                "ActionParameters" : action_parameters,
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        window.location = window.location.origin + GLOBAL_URL + "/user/shave-plan";
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });
        });

        // OnClick Cancel My Subscription
        $("#cancel").click(function(){

            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

            let cancellationData =
            {
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });

        });


        $(".back").click(function() {
            ClosePopup();
        });

    });
</script>
