<style>

.close{
        font-size: 40px;
    }

.header{
    margin-top: -20px;
    margin-bottom: 20px;
}

.discount{
    color: #ff5001;
    font-size: 23px;
    font-weight: bolder;
}

.button-keep-shaving{
    background-color: #ff5001 !important;
    color: white !important;
}

.parent-box{
    margin-top: 10px;
    width:100%;
}

.child-box{
    min-height:300px;
    background-color:white;
    border:1px solid #cccccc !important;
    border-radius:8px;
    padding:0px 0px;
}

.child-box-right{
    margin-right:10px;
    min-height:300px;
    background-color:white;
    border:1px solid #cccccc !important;
    border-radius:8px;
    padding:0px 0px;
}

.child-box-right-header{
    background-color:black !important;
    color:white;
    padding:10px;
    border-top-left-radius:8px;
    border-top-right-radius:8px;
    font-size:18px;
}

.child-box-right-footer{
    border:none;
    width:100%;
    margin: 0px 0px !important;
}

.blades-good{
    font-size:18px;
    color:#ff5001;
    font-weight: bolder;
}

.child-box-left{
    margin-left:10px;
    min-height:300px;
    background-color:#ececec;
    border:1px solid #cccccc !important;
    border-radius:8px;
}

.child-box-left-header{
    background-color:transparent !important;
    color:transparent;
    padding:10px;
    border-top-left-radius:8px;
    border-top-right-radius:8px;
    font-size:18px;
}

.child-box-left-footer{
    border:none;
    width:100%;
    margin: 0px 0px !important;
    background-color:transparent !important;
}

.this-is-goodbye{
    font-size:18px;
    color:black;
    font-weight: bolder;
}

.cry-image{
    width:35%;
    margin-top:30px;
}

</style>
<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.option1.header')<b></h3>
        <h5>@lang('cancellation.modal.option1.subheader-1')</h5>
        <h5>@lang('cancellation.modal.option1.subheader-2')</h5>
    </div>
    <div class="col-12 mx-0 d-inline-block parent-box pb-3">
        <div class="row justify-content-center text-center">
            <div class="card col-lg-5 col-xs-12 text-center child-box mt-2">
                <div class="card-header child-box-right-header">
                    @lang('cancellation.modal.option1.text-1')
                </div>
                <div class="card-body">
                    <div class="blades-good">@lang('cancellation.modal.option1.text-2')</div>
                    <hr>
                    <div>@lang('cancellation.modal.option1.text-3', ['discount' => $data->cancellationDiscount])</div>
                </div>
                <div class="card-footer child-box-right-footer">
                    <button id="continue" type="button" class="btn btn-load-more button-keep-shaving" style="min-width:100%">@lang('cancellation.modal.global.keep-shaving')</button>
                </div>
            </div>
            <div class="card col-lg-5 offset-lg-1 col-xs-12 text-center child-box mt-2" style="background-color: #ECECEC !important;">
                <div class="card-header child-box-left-header">
                    @lang('cancellation.modal.option1.text-1')
                </div>
                <div class="card-body">
                    <div class="this-is-goodbye">@lang('cancellation.modal.option1.text-4')</div>
                    <img class="cry-image" src="{{asset('/images/common/cancellation-journey/cancellation_crying_face.png')}}" />
                </div>
                <div class="card-footer child-box-left-footer col-12">
                    <button id="cancel" type="button" class="btn btn-cancel" style="white-space: normal;">@lang('cancellation.modal.global.cancel-subscription')</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        let data = {!! json_encode($data) !!};

        // OnClick Keep Shaving for Less
        $("#continue").click(function(){

            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/continue_subscription";

            let action_parameters = {};

            let cancellationData =  
            {
                "Actions" : ["apply_cancellation_discount"],
                "ActionParameters" : action_parameters,
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        window.location = window.location.origin + GLOBAL_URL + "/user/shave-plan";
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });
        });

        // OnClick Cancel My Subscription
        $("#cancel").click(function(){

            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

            let cancellationData =  
            {
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });

        });

        
        $(".back").click(function() {
            ClosePopup();
        });

    });
</script>