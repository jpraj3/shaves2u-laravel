<style>
    .close{
        font-size: 40px;
    }
    .header{
        margin-top: -20px;
        margin-bottom: 20px;
    }
    .reason-selected{
        background-color:#ff5001;
        color:white;
        padding:10px !important;
        border:1px solid #ececec;
        border-radius: 8px;
        margin-bottom:10px;
        vertical-align: middle;
        text-align:center;
        font-size: 18px;
    }
    .reason-unselected{
        background-color:#ececec;
        color:#000000;
        padding:10px !important;
        border:1px solid #cccccc;
        border-radius: 8px;
        margin-bottom:10px;
        vertical-align: middle;
        text-align:center;
        font-size: 18px;
    }

    .other-reason-box{
        background-color: white;
        border:1px solid #cccccc;
        border-radius: 8px;
        width: 100%;
    }

    .button-keep-shaving{
        background-color: #ff5001 !important;
        color: white !important;
    }

</style>

<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.option2.header')</b></h3>
        <h5>@lang('cancellation.modal.option2.subheader-1')</h5>
        <h5>@lang('cancellation.modal.option2.subheader-2')</h5>
        <div style="display:flex;justify-content: center;margin-top:20px;">
            <div class="w-100">
                @foreach ($data->cancellationReasons as $reason)
                    <div id="reason-{{$reason->id}}" class="reason-unselected">
                        <div style="text-align: center">
                            {{$reason->content}}
                        </div>        
                    </div>
                @endforeach
                    <div id="reason-other-1" class="reason-unselected">
                        <div style="text-align: center">
                            @lang('cancellation.modal.global.any-other-reason')
                        </div>
                        <textarea id="modal_level_1_other_reason_text" class="other-reason-box" rows="2" style="resize:none" disabled></textarea>        
                    </div>
            </div>
        </div>  
        
        <div class="col-12 mx-0 d-inline-block">
            <div class="row">
                <div class="col-xs-12 col-lg-6 text-center mt-2 pl-0 pr-0">
                    <button id="proceed" type="button" class="btn btn-load-more button-keep-shaving w-100">@lang('cancellation.modal.global.proceed-next')</button>
                </div>
                <div class="col-xs-12 col-lg-6 text-center mt-2 pl-0 pr-0">
                    <button id="cancel" class="btn btn-cancel w-100" style="white-space: normal;">@lang('cancellation.modal.global.cancel-shave-plan')</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function(){

        let data = {!! json_encode($data) !!};
        let gender = "male";
        if(data){
            if(data.currentPlanData.plan_details.plan_gender_type == "female"){
                gender = "female";
            }
        }
        var selected_option = "";
        var selected_reason = "";

        // OnClick Predefined Reason
        for (let i = 0; i < data.cancellationReasons.length; i++) {
            let cancellationReasonId = data.cancellationReasons[i].id;
            $("#reason-" + cancellationReasonId).click(SelectCancellationReason_Option2Modal(data.cancellationReasons[i]));
        }

        // OnClick Other Reason
        $("#reason-other-1").click(SelectCancellationReason_Option2Modal(0));

        // Onselect cancellation reason
        $("input[name='cancellation_reason_group']").change(function(){
            selected_option = $("input[name='cancellation_reason_group']:checked").val();
            // If other reason is selected, unhide the other reason
            if(selected_option === "modal_level_1_others") {
                $("#modal_level_1_other_reason_text").removeClass("hidden");
            } else {
                $("#modal_level_1_other_reason_text").addClass("hidden");
                selected_reason = JSON.parse(selected_option);
            }
        });

        $("#proceed").click(function(){
            if(selected_option === "") {
                // Do nothing
            }
            else if(selected_option === "modal_level_1_others") {
                let other_reason = $("#modal_level_1_other_reason_text").val();
                
                let cancellationData = {
                    other_reason
                }           

                // Immediately cancel if annual plan
                if(gender == "female" || data.cancellationBenefitStatus.isAnnualPlan || data.cancellationBenefitStatus.haveAppliedCancellationDiscount || data.cancellationBenefitStatus.isWithinTrialPeriod) {
                    CancelSubscription(data.subscription.id,selected_reason.id,other_reason,$(this));
                }
                else {
                    session('cancellation_journey', 'set', JSON.stringify(cancellationData)).done(function() {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/navigate/${data.subscription.id}/${data.nextPageCancellation.id}/${data.nextPageCancellation.viewName}`);
                    });
                }

            } 
            else {
                // If already apply free product, immediately cancel the subscription
                if(selected_reason.viewName === "option2_2" && data.cancellationBenefitStatus.haveAppliedFreeProduct) {
                    CancelSubscription(data.subscription.id,selected_reason.id,"",$(this));
                } else if(selected_reason.viewName === "option2_1" && gender == "female") {
                    CancelSubscription(data.subscription.id,selected_reason.id,"",$(this));
                } else {
                    $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/navigate/${data.subscription.id}/${selected_reason.id}/${selected_reason.viewName}`);
                }
            }
        });

        $("#cancel").click(function(){

            $(this).prop('disabled', true);

            if(selected_option === "") 
            {   
                let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

                let cancellationData =  
                {
                    "SubscriptionId" : data.subscription.id,
                    "CancellationTranslatesId" : data.rootCancellationReason.id,
                    "OtherReason" : ""
                }
                
                AJAX(url, "POST", cancellationData)
                    .done(function(resp) {
                        if(resp === 'success') {
                            $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                        } else {
                            $(this).prop('disabled', false);
                            // console.log(resp);
                        }
                    });
            }
            else if(selected_option === "modal_level_1_others")
            {
                let otherReason = $("#modal_level_1_other_reason_text").val();

                let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

                let cancellationData =  
                {
                    "SubscriptionId" : data.subscription.id,
                    "CancellationTranslatesId" : data.rootCancellationReason.id,
                    "OtherReason" : otherReason
                }
                
                AJAX(url, "POST", cancellationData)
                    .done(function(resp) {
                        if(resp === 'success') {
                            $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                        } else {
                            $(this).prop('disabled', false);
                            // console.log(resp);
                        }
                    });
            }
            else 
            {
                let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

                let cancellationData =  
                {
                    "SubscriptionId" : data.subscription.id,
                    "CancellationTranslatesId" : selected_reason.id,
                    "OtherReason" : ""
                }
                
                AJAX(url, "POST", cancellationData)
                    .done(function(resp) {
                        if(resp === 'success') {
                            $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                        } else {
                            $(this).prop('disabled', false);
                            // console.log(resp);
                        }
                    });
            }
        });

        // Function: Select handle 
        function SelectCancellationReason_Option2Modal(cancellationReason) {
            return function () {
                if(cancellationReason.id > 0) {
                    
                    selected_option = "reason-" + cancellationReason.id;
                    selected_reason = cancellationReason;
                    $("#reason-" + cancellationReason.id).removeClass('reason-unselected').addClass('reason-selected');
                    $("#reason-other-1").removeClass('reason-selected').addClass('reason-unselected');
                    $("#modal_level_1_other_reason_text").prop('disabled', true);
                    data.cancellationReasons.forEach(reason => {
                        if (reason.id != cancellationReason.id) {
                            $("#reason-" + reason.id).removeClass('reason-selected').addClass('reason-unselected');
                        }
                    });
                } else {
                    selected_option = "modal_level_1_others";
                    $("#reason-other-1").removeClass('reason-unselected').addClass('reason-selected');
                    $("#modal_level_1_other_reason_text").prop('disabled', false);
                    data.cancellationReasons.forEach(reason => {
                        if (reason.id != cancellationReason.id) {
                            $("#reason-" + reason.id).removeClass('reason-selected').addClass('reason-unselected');
                        }
                    });
                }
            };
        };

        $(".back").click(function() {
            ClosePopup();
        });

    });
</script>