<style>

.close{
        font-size: 40px;
    }

.header{
    margin-top: -20px;
    margin-bottom: 20px;
}

.discount{
    color: #ff5001;
    font-size: 23px;
    font-weight: bolder;
}

.button-keep-shaving{
    background-color: #ff5001 !important;
    color: white !important;
}

.parent-box{
    margin-top: 10px;
    width:100%;
}

.child-box-right{
    margin-right:10px;
    min-height:300px;
    background-color:white;
    border:1px none !important;
    padding:0px 0px;
}

.child-box-right-header{
    background-color:black !important;
    color:white;
    padding:10px;
    border-top-left-radius:8px;
    border-top-right-radius:8px;
    font-size:18px;
}

.child-box-right-footer{
    border:none;
    width:100%;
    margin: 0px 0px !important;
}

.blades-good{
    font-size:18px;
    color:#ff5001;
    font-weight: bolder;
}

.child-box-left{
    margin-left:10px;
    min-height:300px;
    background-color:#ececec;
    border:none !important;
}

.child-box-left-header{
    background-color:transparent !important;
    color:transparent;
    padding:10px;
    border-top-left-radius:8px;
    border-top-right-radius:8px;
    font-size:18px;
}

.child-box-left-footer{
    border:none;
    width:100%;
    margin: 0px 0px !important;
    background-color:transparent !important;
}

.this-is-goodbye{
    font-size:18px;
    color:black;
    font-weight: bolder;
}

.blade-image{
    width:80%;
}

.blade-list{
    width:100%;
    padding:10px;
    border: 1px #cccccc black;
    border-radius: 2px;
}

</style>

<div class="col-12" style="height:4em;">
    <button style="margin-top: 1%;margin-right: 1%;" type="button" class="back close" >&times;</button>
</div>
<div class="modal-body">
    <div class="col-12 text-center">
        <h3 class="header"><b>@lang('cancellation.modal.option2_1.header')</b></h3>  
        <h5>@lang('cancellation.modal.option2_1.subheader-1')</h5>
    </div>
    <br>
    
    <div class="col-12 mx-0 d-inline-block parent-box">
        <div class="row justify-content-center">
            <div class="card col-lg-6 col-xs-12 text-center">
                <div class="card-body">
                    @lang('cancellation.modal.option2_1.text-1')
                    <br><br>
                    <select id="blade-list" class="blade-list" name="blade-list" class="form-control">
                        @foreach ($data->bladeList as $blade)
                            @if($blade['ProductCountryId'] === $data->currentBlade['ProductCountryId']) 
                            {
                                <option value="{{json_encode($blade)}}" selected>{{$blade['productTranslatesName']}} - {{$data->country->currencyDisplay}} {{$blade['sellPrice']}}</option>
                            }
                            @else
                            {
                                <option value="{{json_encode($blade)}}">{{$blade['productTranslatesName']}} - {{$data->country->currencyDisplay}} {{$blade['sellPrice']}}</option>
                            }
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card col-lg-6 col-xs-12 text-center">
                <div class="card-body">
                    <img id="current-blade-image" class="img-responsive blade-image" src="{{$data->currentBlade['product_default_image']}}" alt="image">
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 mx-0 d-inline-block">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-xs-12 text-center mt-2 pl-0 pr-0">
                <button id="continue" type="button" class="btn btn-load-more button-keep-shaving" style="min-width:100%">@lang('cancellation.modal.global.try-different-blade')</button>
            </div>
            <div class="col-lg-5 offset-lg-1 col-xs-12 text-center mt-2 pl-0 pr-0">
                <button id="cancel" class="btn btn-cancel" style="min-width:100%">@lang('cancellation.modal.global.cancel-shave-plan')</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){

        let data = {!! json_encode($data) !!};

        // OnChange blade list
        $("#blade-list").change(function(){
            var selectedBlade = JSON.parse(($(this).children("option:selected").val()));
            $('#current-blade-image').attr('src',selectedBlade.product_default_image);
        });

        // OnClick Try a Different Blade
        $("#continue").click(function() {
            let modifiedBladeName = JSON.parse(($("#blade-list").children("option:selected").val())).productDefaultName;
            let modifiedBladeSku = JSON.parse(($("#blade-list").children("option:selected").val())).sku;
            let modifiedBladeProductCountryId = JSON.parse(($("#blade-list").children("option:selected").val())).ProductCountryId;
            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/continue_subscription";

            let action_parameters = {
                modifiedBladeName,
                modifiedBladeSku,
                modifiedBladeProductCountryId
            };

            let cancellationData =  
            {
                "Actions" : ["change_blade"],
                "ActionParameters" : action_parameters,
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/changed_blade/${modifiedBladeSku.replace("/","_")}`);
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });
        });

        // OnClick Cancel My Subscription
        $("#cancel").click(function(){

            $(this).prop('disabled', true);

            let url = GLOBAL_URL + "/cancellation-journey/action/cancel_subscription";

            let cancellationData =  
            {
                "SubscriptionId" : data.subscription.id,
                "CancellationTranslatesId" : data.cancellationReason.id,
                "OtherReason" : ""
            }

            AJAX(url, "POST", cancellationData)
                .done(function(resp) {
                    if(resp === 'success') {
                        $('#cancellation-journey-router').load(`${window.location.origin}${GLOBAL_URL}/cancellation-journey/notify/cancelled`);
                    } else {
                        $(this).prop('disabled', false);
                        // console.log(resp);
                    }
                });

        });

        $(".back").click(function() {
            ClosePopup();
        });

    });
</script>