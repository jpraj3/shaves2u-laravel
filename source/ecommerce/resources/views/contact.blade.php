@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/contact/contact.css') }}">

<style>
    hr {
        color: #fe5000;
        background-color: #fe5000
    }
    .swal2-confirm{
        background-color: transparent !important;
        color:black !important;
    }
    .swal2-styled.swal2-confirm {
        border: solid 2px #FE5000 !important;
        border-radius: 5px;
        background: rgba (255, 255, 255, 0);
        padding: 14px 28px;
        cursor: pointer;
        text-align: center;
        font-size: 16px !important;
        color: #FE5000 !important;
        margin: auto;
    }

    @media (max-width: 990px) {
        .main { flex: 2 0px; }
        .order1 { order: 1; }
        .order2 { order: 2; }
        }
</style>

<section class="contact_form bg-white paddTB20">
    <div class="container">
        <div class="row main">
            <div class="col-12 col-lg-6 order2">
                <form id="form_ce_contact" name="form_ce_contact" method="POST">
                    <div class="col-12 padd15">
                        <h1 class="MuliExtraBold">@lang('website_contents.landing.contact.contact_us')</h1>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="c_e_topic"><b>@lang('website_contents.landing.contact.what_need_help')</b></label>
                            <select id="c_e_topic" name="c_e_topic" class="form-control custom-select" style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                <option>@lang('website_contents.landing.contact.enquiry_type.billing_payment')</option>
                                <option>@lang('website_contents.landing.contact.enquiry_type.plan')</option>
                                <option>@lang('website_contents.landing.contact.enquiry_type.account')</option>
                                <option>@lang('website_contents.landing.contact.enquiry_type.returns')</option>
                                <option>@lang('website_contents.landing.contact.enquiry_type.products')</option>
                                <option>@lang('website_contents.landing.contact.enquiry_type.delivery')</option>
                            </select>
                            <p class="c_e_topic_error" id="c_e_topic_error"></p>
                        </div>
                        <div class="form-group">
                            <label for="c_e_name"><b>@lang('website_contents.landing.contact.name')</b></label>
                            <input id="c_e_name" name="c_e_name" type="text" class="form-control" />
                            <p class="c_e_name_error" id="c_e_name_error"></p>
                        </div>
                        <div class="form-group">
                            <label for="c_e_email"><b>@lang('website_contents.landing.contact.email')</b></label>
                            <input id="c_e_email" name="c_e_email" type="text" class="form-control" />
                            <p class="c_e_email_error" id="c_e_email_error"></p>
                        </div>
                        <div class="form-group">
                            <label for="c_e_phone"><b>@lang('website_contents.landing.contact.phone')</b></label>
                            <input id="c_e_phone" name="c_e_phone" type="text" class="form-control" />
                            <p class="c_e_phone_error" id="c_e_phone_error"></p>
                        </div>
                        <div class="form-group">
                            <label for="c_e_enquiry"><b>@lang('website_contents.landing.contact.enquiry')</b></label>
                            <textarea id="c_e_enquiry" name="c_e_enquiry" class="form-control" style="resize: none; height:8rem"></textarea>
                            <p class="c_e_enquiry_error" id="c_e_enquiry_error"></p>
                        </div>
                        {{ csrf_field() }}
                        <div class="form-group col-12 col-lg-7 px-0">
                            <button id="c_a_submit" type="submit" class="btn btn-submit MuliExtraBold w-100">@lang('website_contents.landing.contact.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-lg-5 offset-lg-1 help-topics order1">
                <div class="row padd30 hrline d-none d-lg-block" style="background-color: #f5f5f5;">
                    <div class="col-12">
                        <h5 class="MuliExtraBold">@lang('website_contents.landing.contact.popular_topics')</h5>
                    </div>
                    <div class="col-12 pt-3">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#shaveplan">@lang('website_contents.landing.contact.shave_plans')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#technicalissues">@lang('website_contents.landing.contact.technical_issues')</a></h5>
                        <hr>
                    </div>

                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#deliveryshipping">@lang('website_contents.landing.contact.delivery_shipping')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#account">@lang('website_contents.landing.contact.account')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#returnsrefunds">@lang('website_contents.landing.contact.returns_refunds')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#products">@lang('website_contents.landing.contact.products')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#payment">@lang('website_contents.landing.contact.payments')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#companyquestions">@lang('website_contents.landing.contact.company')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        @if(strtolower(view()->shared('currentCountryIso')) == 'my')
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2u Sdn Bhd</h5>
                        @elseif(strtolower(view()->shared('currentCountryIso')) == 'sg')
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2U Pte Ltd</h5>
                        @elseif(strtolower(view()->shared('currentCountryIso')) == 'hk')
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2U HK Limited</h5>
                        @elseif(strtolower(view()->shared('currentCountryIso')) == 'kr')
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2U</h5>
                        @elseif(strtolower(view()->shared('currentCountryIso')) == 'tw')
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2U HK Limited</h5>
                        @else
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2u Sdn Bhd</h5>
                        @endif
                    </div>
                    <div class="row" style="margin: 0px;">
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-envelope"></i>
                        </div>
                        <div class="col-10 company-info">
                            @if(strtolower(view()->shared('currentCountryIso')) == 'my')
                            <p>help.my@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'sg')
                            <p>help.sg@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'hk')
                            <p>help.hk@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'kr')
                            <p>help.kr@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'tw')
                            <p>help.tw@shaves2u.com</p>
                            @else
                            <p>help.my@shaves2u.com</p>
                            @endif
                        </div>
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-phone"></i>
                        </div>
                        <div class="col-10 company-info">
                            @if(strtolower(view()->shared('currentCountryIso')) == 'my')
                            <p>+603 2773 2882</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'sg')
                            <p>+65 6329 1488</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'hk')
                            <p>+852 3500 8839</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'kr')
                            <p>전화번호 : +02 6006 9777</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'tw')
                            <p>+886 2 7729 9144</p>
                            @else
                            <p>+603 2773 2882</p>
                            @endif
                        </div>
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-clock-o"></i>
                        </div>
                        <div class="col-10 company-info">
                            <p>@lang('website_contents.landing.contact.shaves2u_time')</p>
                        </div>
                    </div>
                </div>
                <div class="row padd30 hrline d-block d-lg-none" style="background-color: none;">
                    <div class="col-12">
                        <h5 class="MuliExtraBold">@lang('website_contents.landing.contact.popular_topics')</h5>
                    </div>
                    <div class="col-12 pt-3">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#shaveplan">@lang('website_contents.landing.contact.shave_plans')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#technicalissues">@lang('website_contents.landing.contact.technical_issues')</a></h5>
                        <hr>
                    </div>

                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#deliveryshipping">@lang('website_contents.landing.contact.delivery_shipping')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#account">@lang('website_contents.landing.contact.account')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#returnsrefunds">@lang('website_contents.landing.contact.returns_refunds')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#products">@lang('website_contents.landing.contact.products')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#payment">@lang('website_contents.landing.contact.payments')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5><a class="color-orange" href="{{ route('locale.region.faq', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}#companyquestions">@lang('website_contents.landing.contact.company')</a></h5>
                        <hr>
                    </div>
                    <div class="col-12">
                        <h5 class="MuliExtraBold" style="padding-top: 10px;">Shaves2u Sdn Bhd</h5>
                    </div>
                    <div class="row" style="margin: 0px;">
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-envelope"></i>
                        </div>
                        <div class="col-10 company-info">
                            @if(strtolower(view()->shared('currentCountryIso')) == 'my')
                            <p>help.my@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'sg')
                            <p>help.sg@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'hk')
                            <p>help.hk@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'kr')
                            <p>help.kr@shaves2u.com</p>
                            @elseif(strtolower(view()->shared('currentCountryIso')) == 'tw')
                            <p>help.tw@shaves2u.com</p>
                            @else
                            <p>help.my@shaves2u.com</p>
                            @endif
                        </div>
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-phone"></i>
                        </div>
                        <div class="col-10 company-info">
                            <p>@lang('website_contents.landing.contact.shaves2u_phone')</p>
                        </div>
                        <div class="col-2 company-info">
                            <i class="fs-30 fa fa-clock-o"></i>
                        </div>
                        <div class="col-10 company-info">
                            <p>@lang('website_contents.landing.contact.shaves2u_time')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('js/functions/contact/contact.function.js') }}"></script>
@endsection
