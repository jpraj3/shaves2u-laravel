@extends('_campaign.layouts.app')

@section('content')
<!-- VS -->
<link rel="stylesheet" href="{{ asset('css/landing/landing.css') }}">
<link rel="stylesheet" href="{{ asset('css/campaign/welcomebackpromo.css') }}">
<div class="container">

    <div class="text-center" style="padding-top: 10%;">
        <h2 class="bold-max" style="font-family: Muli-ExtraBold; padding-bottom: 2%;"><strong>@lang('campaign.welcomebackpromo.thankyou_title')</strong></h2>
        <div class="col-12 text-center pt-2 pr-0 pl-0 ml-0">
        <form action="{{ route('locale.index') }}">
            <button  class="button-next button-proceed-checkout MuliExtraBold" style="width: 50%;" type="submit">@lang('campaign.welcomebackpromo.thankyou_button')</button>
        </form>
        </div>
    </div>

    @endsection