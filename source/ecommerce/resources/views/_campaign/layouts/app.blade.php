<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="theme-color" content="#fdb918">
    <div id="manifest"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    {{--
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script> --}}
    <script src="{{ asset('validator/jquery-validation-1.19.1/dist/jquery.validate.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.3/lottie.min.js"></script>
    <link rel="shortcut icon" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/common/logo/logo-mobile.svg') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- sweetalert2 popup -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2.v8/sweetalert2.min.css') }}">
    <!-- wow animation -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <!-- Toastr Notification Plugin -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr-v2.1.4/toastr.min.css')}}"/>
    <script src="{{ asset('plugins/toastr-v2.1.4/toastr.min.js') }}"></script>   <!-- End Toastr Notification Plugin -->
    <script src="{{ asset('plugins/sweetalert2.v8/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('/js/wow.min.js') }}"></script>
    <script src="{{ asset('/js/cleave.min.js') }}"></script>

    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHXJ3CB');</script>
    <!-- End Google Tag Manager -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-M7SX4VH');</script>
    <!-- End Google Tag Manager -->
    @endif

    <!-- Global change disabled input background color -->
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
                -moz-appearance:textfield;
        }
        input[type="text"]:disabled {
            background-color: white !important;
        }

        input[type="number"]:disabled {
            background-color: white !important;
        }

        .status-inactive {
            background-color: #ffc107 !important;
        }

        .status-active {
            background-color: #06cb7c !important;
        }

        .status-onhold {
            background-color: #6c757d !important;
        }

        select:disabled {
            background-color: white !important;
        }
        .success-promotion{
            color: green !important;
        }
        .error-promotion{
            color: red !important;
        }
    </style>
    <meta property="fb:app_id" content="723177734789504"/>
    <!-- Laravel Social Sharing  -->
    <script src="{{ asset('js/share.js') }}"></script>
    @include('_campaign.layouts.styles.styles')
    @include('_campaign.layouts.scripts.mainscripts')
    @include('_campaign.layouts.scripts.social-login')
    {!! SEO::generate() !!}
    {!! OpenGraph::generate() !!}
</head>

<body id="hideBody">

    @if(config('app.env') == 'staging')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHXJ3CB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    @if(config('app.env') == 'production')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7SX4VH"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    
    <div id="app">
        <header id="header">
                @include('_campaign.layouts.header.welcomebackpromoheader')
        </header>
        <main id="content">
            @include('loading.loading')
            @yield('content')
          
        </main>
        <footer>
            <!-- @if(Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection' || Route::currentRouteName() == 'locale.region.shave-plans.trial-plan')
                @include('_campaign.layouts.footer.starter_footer')
            @elseif(Route::currentRouteName() == 'locale.region.shave-plans.trial-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.checkout' || Route::currentRouteName() == 'locale.region.shave-plans.custom-plan.selection' || Route::currentRouteName() == 'locale.region.alacarte.ask.checkout' || Route::currentRouteName() == 'locale.region.alacarte.ask' )

            @else
                @include('_campaign.layouts.footer.footer')
            @endif -->
        </footer>
    </div>
</body>
{{-- Render Zendesk widget on All but pages with "checkout" in URL --}}
@if (strpos(request()->path(), 'checkout') == false)
    <!-- Start of shaves-2u Zendesk Widget script -->
    <script id="ze-snippet"
            src="https://static.zdassets.com/ekr/snippet.js?key=b8bf9b95-b617-4f7d-89ab-24e2e6a581a5"></script>
    <!-- End of shaves-2u Zendesk Widget script -->
@endif

@php
    // Get Sales Notification List From Session
    $sessionProductList = session()->get('SalesNotificationProductList');
@endphp


<script>
        /* Sample function that returns boolean in case the browser is Internet Explorer*/
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
            return is_ie;
        }

        /* Create an alert to show if the browser is IE or not */
        if (isIE()){
            $("#hideBody").html("<style>*{font-family: 'Muli', sans-serif; }p{font: 18px;}</style><div style='text-align: center'><img style='width: 50px;' src='{{asset('/images/common/logo-mobile.svg')}}'/></div><div style='text-align: center; color:#fe5000;'><h1>You are using an unsupported browser.</h1></div><div style='text-align:center'><p>For your information, the official Shaves2U website<br>cannot be accessed through Internet Explorer (IE).</p></div><div style='text-align:center'><p>You may try to install one of our recommended browser:</p></div><div style='text-align:center;'><p><b>Google Chrome</b></p></div><div style='text-align:center'><a href='https://www.google.com/chrome/browser/desktop/index.html'>Google Chrome Browser</a></div><div style='text-align:center'><a href='https://www.google.com/chrome/browser/mobile/index.html'>Google Chrome Mobile App</a></div><hr style='width: 70%'/><div style='text-align:center;'><b>Mozilla Firefox</b></div><div style='text-align:center'><a href='https://www.mozilla.org/en-US/firefox/new/'>Mozilla Firefox Browser</a></div>");
        }
    </script>


</html>
