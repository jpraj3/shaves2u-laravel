@include('layouts.styles.global.styles')

<style>

    .sign-out-modal{
        width:60% !important; 
        margin: 0 auto !important;
    }

    @media (max-width: 767px) {

        html,
        body {
            overflow-x: hidden;
            max-width: 100%;
        }

        body {
            position: relative;
        }

        .color-sm-black {
            color: black;
        }

        .color-sm-white {
            color: white;
        }

        .color-sm-orange {
            color: #FE5000;
        }

        i.current-plan {
            top: 20%;
            position: relative;
        }

        .card,
        .card-header {
            text-align: center;
        }

        .orng-bg-sm {
            background: linear-gradient(to bottom, #FE5000 39%, white 33%);
        }

        .fa-angle-up,
        .fa-angle-down,
        .fa-angle-right {
            font-size: 3rem;
        }

        .card-footer {
            background-color: white;
            margin-left: 33%;
            margin-right: 33%;
            border-bottom: 1px solid black;
        }

        .btn-start {
            width: 60%;
        }

        .btn-load-more {
            width: 60%;
        }

        .btn-load-more-white {
            width: 60%;
            border: solid 2px black;
            color: black;
            margin-top: 10%;
        }

        .fs-36-sm {
            font-size: 2.25rem !important;
        }

        .fs-30-sm {
            font-size: 1.875rem !important;
        }

        .fs-24-sm {
            font-size: 1.5rem !important;
        }

        .fs-20-sm {
            font-size: 1.25rem;
        }

        .fs-16-sm {
            font-size: 1rem;
        }

        .fs-14-sm {
            font-size: 0.875rem;
        }

        .fs-13-sm {
            font-size: 0.8125rem;
        }

        .fs-10-sm {
            font-size: 0.625rem;
        }

        .footer .container {
            padding-top: 10%;
        }

        .footer h4 {
            margin-top: 5%;
        }

        .footer h6 {
            padding-bottom: 15%;
        }

        .social-media-icons {
            margin-top: 3%;
            margin-bottom: 3%;
            margin-left: 50px;
            margin-right: 50px;
        }

        .kr-footer-sec-2
        {
            margin-left: 10%;
            margin-right: 10%;
        }

        .center-md {
            text-align: center;
        }

        .uppercase-sm {
            text-transform: uppercase;
        }
    }

    @media (max-width: 991px) and (min-width: 768px) {
        html,
        body {
            overflow-x: hidden;
            max-width: 100%;
        }

        body {
            position: relative;
        }

        .fa-angle-up, .fa-angle-down, .fa-angle-right {
            font-size: 2.5rem;
        }

        .fs-36-md {
            font-size: 2.25rem;
        }

        .fs-24-md {
            font-size: 1.5rem;
        }

        .fs-20-md {
            font-size: 1.25rem;
        }

        .fs-16-md {
            font-size: 1rem;
        }

        .color-md-black {
            color: black;
        }

        .color-md-white {
            color: white;
        }

        .color-md-orange {
            color: #FE5000;
        }

        .btn-start {
            width: 60%;
        }

        .btn-load-more {
            width: 60%;
        }

        i.current-plan {
            top: 30%;
            position: relative;
        }

        .orng-bg-sm {
            background: linear-gradient(to bottom, #FE5000 72%, white 28%);
        }

        .social-media-icons {
            margin-top: 5%;
            margin-bottom: 5%;
        }

        .center-md {
            text-align: center;
        }

        .kr-footer-sec-2
        {
            margin-left: 15%;
            margin-right: 15%;
        }
    }

    @media (min-width: 992px) {
        .color-lg-black {
            color: black;
        }

        .color-lg-white {
            color: white;
        }

        .color-lg-orange {
            color: #FE5000;
        }

        .orng-bg-sm {
            background: linear-gradient(to bottom, #FE5000 66%, white 33%);
        }

        .fa-angle-up, .fa-angle-down, .fa-angle-right {
            font-size: 2rem;
        }

        .btn-load-more-white {
            margin-top: 2%
        }

        .fs-36-lg {
            font-size: 2.25rem;
        }

        .fs-24-lg {
            font-size: 1.5rem;
        }

        .fs-20-lg {
            font-size: 1.25rem;
        }

        .fs-18-lg {
            font-size: 1.125rem;
        }

        .fs-16-lg {
            font-size: 1rem;
        }

        .fs-12-lg {
            font-size: 0.75rem;
        }

        .footer h4 {
            margin-top: 2%;
        }

        .footer h6 {
            padding-bottom: 3%;
        }

        .bold-lg {
            font-weight: 700 !important;
        }
    }

    .nav-item {
        padding: 0 15px;
    }

    .nav-link {
        color: black;
        font-family: Muli;
    }

    .margTB15 {
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .padd0 {
        padding: 0;
    }

    .padd10 {
        padding: 10px;
    }

    .padd15 {
        padding: 15px;
    }

    .padd30 {
        padding: 30px;
    }

    .footer h4 {
        margin-top: 2%;
    }

    .footer h6 {
        padding-bottom: 3%;
    }

    .nav-item.active a h5 {
        color: #FE5000;
    }

    .custom-select {
        background-size: 25px 25px;
    }

    #toast-container > div.toast-sales {
        padding: 15px;
    }

    #toast-container > div.toast-sales > div.toast-message {
        display: flex;
    }

    .toast-bottom-right {
        bottom: 75px;
    }

    .toast-sales {
        background-color: #e2e2e2;
        border: 3px solid black;
    }

    .w-30 {
        width: 30%;
    }

    .w-70 {
        width: 70%;
    }

    .sales-image {
        border-radius: 10px;
        -webkit-box-shadow: -5px 5px 0px 0px rgba(116, 116, 116, 1);
        -moz-box-shadow: -5px 5px 0px 0px rgba(116, 116, 116, 1);
        box-shadow: -3px 3px 0px 0px rgba(116, 116, 116, 1);
    }

    @media (max-width: 451px) {
        
    .sign-out-modal{
        width:90% !important; 
        margin: 0 auto !important;
    }

    }
    @media (max-width: 300px) {
        
        .sign-out-modal{
            width:100% !important; 
            margin: 0 auto !important;
        }
    
        }
</style>
