@php
// Get Data from session
$sessionCountryData = session()->get('currentCountry');

// If Data from session exist
if (!empty($sessionCountryData)) {
    // Set Current country info based on session Data
    $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
}
// If Data from Session Storage does not exist
else {
    // Redirect back to / where session is set
    redirect()->route('locale.index');
}
$currentLocale = strtolower(app()->getLocale());
@endphp

<!-- Desktop Footer Start -->
<div class="col-lg-12 d-none d-lg-block p-0" style="cursor: default !important;">
    <footer id="footer" class="footer-padding">
        <div class="footer-container">
            <div class="footer bg-faded">
                <div class="container">
                    <div class="col-lg-12 text-center inlineB">
                        <h6 class="fs-16-md fs-12-lg mb-0 color-white">@lang('website_contents.global.footer.all_rights_reserved')</h6>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Desktop Footer End -->
