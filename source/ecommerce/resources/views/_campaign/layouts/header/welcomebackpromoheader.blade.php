
<style>
    .topLogo-desktop{
        width: 45px;
        height: 45px;
        margin-left: 70px !important;
        margin-right: 20px !important;
    }

    .topLogo-mobile{
        width: 40px;
        height: 40px;
    }

    .fs-16 {
        font-size: 16px;
    }
</style>
<!-- shadow-sm -->
<!-- Desktop Header Start-->
<div class="col-lg-12 d-none d-lg-block p-0">
    <nav class="navbar navbar-expand-lg navbar-light bg-white bold-lg p-0 pt-2 pb-2">
        <div class="container-fluid">
            <button class="navbar-toggler border-0 collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent_left" aria-controls="navbarSupportedContent_left" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon" style="background-image: none;">
                    <i class="fa fa-bars color-orange"></i>
                </span>
            </button>


            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent_right">
                    <div class="col-12 d-none d-md-block inlineB">
                        <div class="row align-middle">
                            <div class="col text-center" style="padding-right : 7%;">
                            <a class="navbar-brand m-0 topLogo-desktop" href="{{ route('locale.index') }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img src="{{asset('/images/common/logo-mobile.svg')}}"/>
            </a>
            <a class="nav-link font-weight-bold color-orange float-right d-lg-none" href="{{ route('locale.index') }}">
                <i class="fa fa-user color-white fs-30" style="-webkit-text-stroke: 1px #ff5001;"></i>
            </a>
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<!-- Desktop Header End -->

<!-- Mobile Header Start -->
<div class="col-xs-12 d-block d-lg-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-white bold-lg ">
        <div class="container-fluid">
        <?php
            $urllang = view()->shared('url');
            $iso = view()->shared('currentCountryIso');
            $url = url("/" . $urllang . "-" . $iso);
            ?>
            <a class="navbar-brand mx-auto" href="{{ $url }}">
                <!--{{ config('app.name', 'Laravel') }}-->
                <img class="topLogo-mobile" src="{{asset('/images/common/logo-mobile.svg')}}"/>
            </a>
        </div>
    </nav>
</div>
<!-- Mobile Header End -->
