@extends('_campaign.layouts.app')

@section('content')
<!-- VS -->
<link rel="stylesheet" href="{{ asset('css/landing/landing.css') }}">
<link rel="stylesheet" href="{{ asset('css/campaign/welcomebackpromo.css') }}">
<div class="container">

    <div class="text-center" style="padding-bottom: 5%; padding-top: 3%;">
        <h2 class="bold-max" style="font-family: Muli-ExtraBold"><strong>@lang('campaign.welcomebackpromo.header')</strong></h2>
        <div class="col-12 d-none d-lg-block" style="text-align:center;color:#828282;font-size:18px;">
            @lang('campaign.welcomebackpromo.header1')
        </div>
    </div>
    <div id="collapse1" class="row panel-collapse collapse show">
        <div class="col-md-6 pr-0 pl-0">
            <div class="text-center">
            <img src="{{asset('/images/campaign/s2u_promo_image.png')}}" class="package-image w-80 text-center">
            </div>
            <div class="col-md-12 MuliExtraBold color-orange">
                <h4><b>@lang('campaign.welcomebackpromo.image_subtile')</b></h4>
            </div>
            <div class="col-md-12">
                <ul class="pr-0 pl-3">
                    <li>
                        <h5>@lang('campaign.welcomebackpromo.image_li1')</h5>
                    </li>
                    <li>
                        <h5>@lang('campaign.welcomebackpromo.image_li2')</h5>
                    </li>
                    <li>
                        <h5>@lang('campaign.welcomebackpromo.image_li3')</h5>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 pr-0 pl-0">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 MuliExtraBold color-orange">
                        <h2>@lang('campaign.welcomebackpromo.title')</h2>
                    </div>
                </div>
                <hr>
                <div class="col-md-12 pt-2 pr-0 pl-0 pb-3">
                    <h4><b>@lang('campaign.welcomebackpromo.select_blade')</b></h4>
                    <div class="col-md-12 pr-0 pl-0 pt-2">
                        <!-- blade-->
                        <div class="accordion md-accordion pb-4" id="accordion_blade" role="tablist" aria-multiselectable="true">
                            <div class="card bord-none">
                                <div class="card-header bg-none" role="tab" id="heading_blade" style="padding-top: 0;padding-left: 0; width:101%">

                                    <a data-toggle="collapse" data-parent="#accordion_blade" href="#blade_collapse" aria-expanded="false" aria-controls="blade_collapse">
                                        @foreach($blades as $blade)
                                        <?php if (strpos($blade['sku'], 'S6') !== false) { ?>
                                            <div class="color-black">
                                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "blade-h-" . $blade['productcountriesid']  ?>" id="blade-selection">
                                                    <div class="col-md-3 pr-0 pl-3 ml-0">
                                                        <img src="{{$blade['productDefaultImage']}}" alt="{{$blade['producttranslatesname']}}" class="img-fluid" style="width:70px;" id="blade-selection-img" />
                                                    </div>
                                                    <p class="d-none productsku" id="blade-selection-sku">{{$blade['sku']}}</p>
                                                    <div class="col-md-5 pr-0 pl-3">
                                                        <h5 class="div-text-center" id="blade-selection-name"><b>{{$blade['producttranslatesname']}}</b></h5>
                                                    </div>
                                                    <div class="col-md-4 pr-2 pl-0 d-sm-none d-none d-md-block" align="right">
                                                        <h5 class="div-text-center"><b>&nbsp</b></h5>
                                                        <i class="fa fa-angle-right rotate-icon" style="float: right;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        @endforeach
                                    </a>
                                </div>
                                <div id="blade_collapse" class="collapse" role="tabpanel" aria-labelledby="heading_blade" data-parent="#blade_collapse">

                                    <div class="col-md-12 pr-0 pl-0 pt-2" style="max-width : 97%;">
                                        @foreach($blades as $blade)
                                        <div class="pb-3 color-black pointer" id="<?php echo "blade-" . $blade['productcountriesid']  ?>" onclick="selectBlade({{$blade['productcountriesid']}},'{{$blade['producttranslatesname']}}','{{$blade['productDefaultImage']}}','{{$blade['sku']}}')">
                                            <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "blade-" . $blade['productcountriesid']  ?>">
                                                <div class="col-md-3 pr-0 pl-3 ml-0">
                                                    <img src="{{$blade['productDefaultImage']}}" alt="{{$blade['producttranslatesname']}}" class="img-fluid" style="width:70px;" />
                                                </div>
                                                <p class="d-none productsku">{{$blade['sku']}}</p>
                                                <div class="col-md-5 pr-0 pl-3">
                                                    <h5 class="div-text-center"><b>{{$blade['producttranslatesname']}}</b></h5>
                                                </div>
                                                <div class="col-md-4 pr-2 pl-0" align="right">
                                                    <h5 class="d-none div-text-center"><b>{{$currency}}{{$blade['sellPrice']}}</b></h5>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end blade-->

                        <!-- quantity-->
                        <h4><b>@lang('campaign.welcomebackpromo.select_quantity')</b></h4>
                        <div class="accordion md-accordion pb-4" id="accordion_quantity" role="tablist" aria-multiselectable="true">
                            <div class="card bord-none">
                                <div class="card-header bg-none" role="tab" id="heading_quantity" style="padding-top: 0;padding-left: 0; width:101%">

                                    <a data-toggle="collapse" data-parent="#accordion_quantity" href="#quantity_collapse" aria-expanded="false" aria-controls="quantity_collapse">
                                        @foreach($bladeno as $blade)
                                        <?php if (strpos($blade, 'TK6') !== false) { ?>
                                            @foreach($monthno as $duration)
                                            
                                            <?php if ($duration != '6') { if (strpos($duration, '2') !== false) {
                                                $monthM = "M" . $duration; ?>
                                                <div class="color-black">
                                                    <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "quantity-h-"  . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month  ?>" id="quantity-selection">
                                                        <div class="col-md-10 pr-0 pl-6 ml-0">
                                                            <h5 class="d-none div-text-center" id="quantity-selection-skuname"><b>{{$datac->$blade->$monthM->planSku}}</b></h5>
                                                            <h5 class="d-none div-text-center" id="quantity-selection-cashback"><b>{{$datac->$blade->$monthM->cashback}}</b></h5>

                                                            <p class="d-none productsku" id="quantity-selection-sku">{{$datac->$blade->$monthM->planSku}}</p>
                                                            <p class="d-none planid" id="quantity-selection-planid">{{$datac->$blade->$monthM->planId}}</p>
                                                            <h5 class="div-text-center" id="quantity-selection-qty-full"><b><span id="quantity-selection-qty">{{$datac->$blade->$monthM->catridgeQTY}}</span> @lang('campaign.welcomebackpromo.cassette')</b></h5>
                                                            <h5 class="d-none div-text-center"><b>{{$currency}}<span id="quantity-selection-sellPrice">{{$datac->$blade->$monthM->sellPrice}}</span></b></h5>
                                                        </div>
                                                        <div class="col-md-2 pr-2 pl-0 d-none d-lg-block" align="right">
                                                            <i class="fa fa-angle-right rotate-icon" style="float: right;"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } } ?>
                                            @endforeach
                                        <?php } ?>
                                        @endforeach
                                    </a>
                                </div>
                                <div id="quantity_collapse" class="collapse" role="tabpanel" aria-labelledby="heading_quantity" data-parent="#quantity_collapse">

                                    <div class="col-md-12 pr-0 pl-0 pt-2" style="max-width : 97%;">
                                        @foreach($bladeno as $blade)
                                        @foreach($monthno as $duration)
                                        <?php if ($duration != '6') { ?>
                                        @php($monthM = "M".$duration)
                                        <div class="pb-3 color-black pointer quantity-option" id="<?php echo "quantity-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month  ?>" onclick="selectQuantity('{{$datac->$blade->$monthM->planSku}}','{{$datac->$blade->$monthM->cashback}}','{{$datac->$blade->$monthM->catridgeQTY}}','{{$datac->$blade->$monthM->sellPrice}}','{{$datac->$blade->$monthM->blade}}','{{$datac->$blade->$monthM->month}}','{{$datac->$blade->$monthM->planId}}')">
                                            <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "quantity-"  . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month  ?>">
                                                <div class="col-md-10 pr-0 pl-6 ml-0">
                                                    <h5 class="d-none div-text-center" id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-skuname"  ?>"><b>{{$datac->$blade->$monthM->planSku}}</b></h5>
                                                    <h5 class="d-none div-text-center" id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-cashback"  ?>"><b>{{$datac->$blade->$monthM->cashback}}</b></h5>

                                                    <p class="d-none productsku" id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-sku"  ?>">{{$datac->$blade->$monthM->planSku}}</p>
                                                    <p class="d-none planid" id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-planid"  ?>">{{$datac->$blade->$monthM->planId}}</p>

                                                    <h5 class="div-text-center"><b><span id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-qty"  ?>">{{$datac->$blade->$monthM->catridgeQTY}}</span> @lang('campaign.welcomebackpromo.cassette')</b></h5>
                                                </div>
                                                <div class="col-md-2 pr-2 pl-0" align="right">
                                                    <h5 class="d-none div-text-center"><b>{{$currency}}<span id="<?php echo "quantity-option-" . $datac->$blade->$monthM->blade . "M" . $datac->$blade->$monthM->month . "-sellPrice"  ?>">{{$datac->$blade->$monthM->sellPrice}}</span></b></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end quantity-->
                        <hr>
                        <div class="col-md-12 pt-2 pr-0 pl-0">

                            <!-- add on-->
                            <div class="accordion md-accordion" id="accordion_addon" role="tablist" aria-multiselectable="true">
                                <div class="card bord-none">
                                    <div class="card-header bg-none" role="tab" id="heading_addon" style="padding-top: 0;padding-left: 0;">

                                        <a data-toggle="collapse" data-parent="#accordion_addon" href="#addon_collapse" aria-expanded="true" aria-controls="addon_collapse">
                                            <h4 class="mb-0 MuliBold color-black"><b>@lang('campaign.welcomebackpromo.select_add-on')</b> <i class="fa fa-angle-right rotate-icon" style="float: right;"></i></h4>
                                            <h5 class="color-grey"><b>@lang('campaign.welcomebackpromo.select_add-on_sub')</b></h5>
                                        </a>
                                    </div>
                                    <div id="addon_collapse" class="collapse show" role="tabpanel" aria-labelledby="heading_addon" data-parent="#addon_collapse">

                                        <div class="col-md-12 pr-0 pl-0 pt-2" style="max-width : 97%;">
                                            @foreach($products as $product)
                                            <div class="pb-3 pointer" onclick='selectAddOn("{{$product['productcountriesid']}}","{{$product['producttranslatesname']}}","{{$product['productDefaultImage']}}","{{$product['sellPrice']}}")'>
                                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "addon-" . $product['productcountriesid']  ?>" id="addon-{{$product['productcountriesid']}}">
                                                    <div class="col-md-3 pr-0 pl-3 ml-0">
                                                        <img src="{{$product['productDefaultImage']}}" alt="{{$product['producttranslatesname']}}" class="img-fluid" style="width:70px;" />
                                                    </div>
                                                    <p class="d-none productsku">{{$product['sku']}}</p>
                                                    <div class="col-md-5 pr-0 pl-3">
                                                        <h5 class="div-text-center"><b>{{$product['producttranslatesname']}}</b></h5>
                                                    </div>
                                                    <div class="col-md-4 pr-2 pl-0 add-on-price">
                                                        <h5 class="div-text-center"><b>{{$currency}}{{$product['sellPrice']}}</b></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <!-- Accordion card -->

                            </div>
                            <!-- end add on-->
                        </div>

                        <div class="col-12 text-center pt-2 pr-0 pl-0 ml-0">
                            <input type="hidden" id="countrypost" name="countrypost" value="{{$country_id}}">
                            <input type="hidden" id="langpost" name="langpost" value="{{$langCode}}">
                            <input type="hidden" id="planidpost" name="planidpost" value="">
                            <input type="hidden" id="productaddonpost" name="productaddonpost" value="">
                            <input type="hidden" id="urlpost" name="urlpost" value="{{$urllang}}-{{$codeIso}}">
                            <input type="hidden" id="campaignidpost" name="campaignidpost" value="{{$CampaignReactiveHistoriesid}}">

                            <input type="hidden" id="utm_source" name="utm_source" value="{{$utm_source}}">
                            <input type="hidden" id="utm_medium" name="utm_medium" value="{{$utm_medium}}">
                            <input type="hidden" id="utm_campaign" name="utm_campaign" value="{{$utm_campaign}}">
                            <input type="hidden" id="utm_content" name="utm_content" value="{{$utm_content}}">
                            <input type="hidden" id="utm_term" name="utm_term" value="{{$utm_term}}">
                            <button id="proceed-update-subscription" class="button-next button-proceed-checkout MuliExtraBold w-100" type="submit">@lang('campaign.welcomebackpromo.btn')<span class="d-none" id="campaign-totalprice"><span></button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Notification Select Cycle -->
    <div class="modal fade" id="addon-cycle-modal" tabindex="-1" role="dialog" aria-labelledby="addon-cycle-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-dialog-addon-cycle" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-addon-cycle">
                    <button type="button" onclick="addonCycleClose();" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12 text-center pb-3">
                        <h3><b>@lang('website_contents.custom_plan.selection.how_deliver')</b></h3>
                    </div>
                    <div class="col-12">
                        <div class="row justify-content-md-center">
                            <div class="col-md-6 addon-cycle-product">
                                <div class="col-12">
                                    <input type="hidden" id="addonid-hidden" name="addonid" value="">
                                    <input type="hidden" id="addonprice-hidden" name="addonprice" value="">
                                    <!-- <input type="hidden" id="addontype-hidden" name="addontype" value=""> -->
                                    <img id="img-addon-cycle-modal" src="" class="img-fluid product-sleeve-bag-img add-on-cycle-img" />
                                    <h5><b id="name-addon-cycle-modal">{{$product['producttranslatesname']}}</b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer-btn col-12 pb-5">
                        <button type="button" onclick="addonCycleEvery();" data-dismiss="modal" aria-label="Close" class="btn btn-every-shipping"><b>@lang('website_contents.custom_plan.selection.add_all_shipments')</b></button>
                        <button type="button" onclick="addonCycleOne();" data-dismiss="modal" aria-label="Close" class="btn btn-one-shipping"><b>@lang('website_contents.custom_plan.selection.add_this_shipment')</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>


     <!-- Modal: Redirect Card Popout -->
     <div class="modal fade" id="redirect-card-modal" tabindex="-1" role="dialog" aria-labelledby="redirect-card-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-dialog-addon-cycle" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-addon-cycle">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <div class="col-12 text-center pb-3">
                        <h5>@lang('campaign.welcomebackpromo.redirect_card_title')</h5>
                        </br>
                        <h5>@lang('campaign.welcomebackpromo.redirect_card_plan_detail')</h5>
                        <h5 class="color-orange"><b><span id="redirect-card-blade"></span></b></h5>
                        <h5 class="color-orange"><b><span id="redirect-card-quantity"></span></b></h5>
                        <h5 class="color-orange"><b><span id="redirect-card-addon"></span></b></h5>
                        <h5 class="color-orange"><b>@lang('campaign.welcomebackpromo.redirect_card_free_after_shavecream')</b></h5>
                        </br>
                        <h5>@lang('campaign.welcomebackpromo.redirect_card_price')</h5>
                        <h4 class="color-orange"><b>{{$currency}}<span id="redirect-card-price"></span></b></h4>
                        </br>
                        <h5>@lang('campaign.welcomebackpromo.redirect_card_date')</h5>
                        <h5><b>{{Carbon\Carbon::now()->addDays(1)->format('d F Y')}}</b></h5>
                        </br>
                        <h5>@lang('campaign.welcomebackpromo.redirect_card_footer')</h5>
                    </div>
                    <div class="modal-footer-btn col-12 pb-5">
                        <button type="button" onclick="redirectToEditCard();" data-dismiss="modal" aria-label="Close" class="btn btn-every-shipping"><b>@lang('campaign.welcomebackpromo.btn_yes')</b></button>
                        <button type="button" onclick="redirectToThankyou();" data-dismiss="modal" aria-label="Close" class="btn btn-one-shipping"><b>@lang('campaign.welcomebackpromo.btn_no')</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let order_first_duration = {!! json_encode($firstmonthno) !!};
        let total_price = 0;
        let addon_total_price = 0;
        let addon_name = [];
    </script>
    <script src="{{ asset('js/functions/campaign/welcomebackpromo.function.js') }}"></script>
    @endsection