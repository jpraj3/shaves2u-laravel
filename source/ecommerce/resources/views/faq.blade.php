@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/faq/faq.css') }}">
<section class="faq-tabs faq-desktop d-none d-lg-block">
    <div class="row m-0">
        <ul class="nav nav-tabs justify-content-center bg-white shadow-sm align-items-center" role="tablist" style="width: 100%">
            <?php
            $count = 0;
            foreach ($faq as $f) {
                $title = $f["title"];
                $type = $f["type"];
                if ($count == 0) {
                    ?>
                    <li class="nav-item"><a class="nav-link text-center text-uppercase active" data-toggle="tab" href="#{{$type}}" aria-controls="{{$type}}" aria-selected="true">{!!$title!!}</a></li>
                <?php
                    } else {
                        ?>
                    <li class="nav-item"><a class="nav-link text-center text-uppercase" data-toggle="tab" href="#{{$type}}" aria-controls="{{$type}}" aria-selected="true">{!!$title!!}</a></li>
            <?php
                }
                $count =  $count + 1;
            }
            ?>
        </ul>
    </div>
</section>

<section class="faq-tabs faq-mobile d-lg-none">
    <div class="row">
        <div class="col-12 mt-2">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="@lang('website_contents.landing.faq.content.looking')" style="border-top: 0; border-left: 0; border-right: 0; border-radius: 0" oninput="searchfaq(this.value)">
                <div class="input-group-append" style="width: 1px;">
                    <i class="fa fa-search color-orange" style="top: 12px; right: 20px; position: relative;"></i>
                </div>
            </div>
        </div>
        <div class="col-12 text-center border-bottom" style="margin-top: 10px;">
            <p style="font-weight: 700;">@lang('website_contents.landing.faq.content.select_topic')</p>
            <p>
            </p>
        </div>
        <ul class="nav nav-tabs justify-content-center bg-white shadow-sm d-lg-none w-100" role="tablist">
            <li class="nav-item dropdown text-center w-100">
                <a id="faq-dropdown" class="nav-link dropdown-toggle w-100" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="">@lang('website_contents.landing.faq.content.shave_plan') <i class="fa fa-angle-down" style="font-size: 1.5rem; position: absolute; right: 30px;"></i></a>
                <div class="dropdown-menu w-100" style="background-color: #dddddd; transform: translate3d(0, 38px, 0) !important;">

                    <?php
                    $countm = 0;
                    foreach ($faq as $f) {
                        $title = $f["title"];
                        $type = $f["type"];
                        if ($countm == 0) {
                            ?>
                            <a class="dropdown-item nav-link text-center text-uppercase active" data-toggle="tab" href="#{{$type}}" aria-controls="{{$type}}" aria-selected="true">{!!$title!!}</a>
                        <?php
                            } else {
                                ?>
                            <a class="dropdown-item nav-link text-center text-uppercase" data-toggle="tab" href="#{{$type}}" aria-controls="{{$type}}" aria-selected="true">{!!$title!!}</a>
                    <?php
                        }
                        $countm =  $countm + 1;
                    }
                    ?>
                </div>
            </li>
        </ul>
    </div>
</section>

<section class="faq-search" style="padding-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-7 center-md">
                <h5 style="color:#8e979e;"><b>@lang('website_contents.landing.faq.content.faq_title')</b></h5>
            </div>
            <div class="col-md-12 col-lg-5 d-none d-lg-block">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="@lang('website_contents.landing.faq.content.looking')" style="border-top: 0; border-left: 0; border-right: 0; border-radius: 0" oninput="searchfaq(this.value)">
                    <div class="input-group-append">
                        <i class="fa fa-search color-orange" style="top: 12px; right: 20px; position: relative;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="faq-content" style="min-height: 500px;">
    <div class="container">
        <div class="row">
            <div class="tab-content">
                <?php
                $counts = 0;
                foreach ($faq as $f) {
                    $title = $f["title"];
                    $subtitle = $f["subtitle"];
                    $type = $f["type"];
                    if ($counts == 0) {
                        ?>
                        <div class="tab-pane fade show active" id="{{$type}}" role="tabpanel" aria-labelledby="{{$type}}-tab">
                        <?php
                            } else {
                                ?>
                            <div class="tab-pane fade" id="{{$type}}" role="tabpanel" aria-labelledby="{{$type}}-tab">
                            <?php
                                }
                                $counts =  $counts + 1;

                                ?>
                            <div class="col-12 center-md">
                                {!! $subtitle !!}
                            </div>
                            <!--Accordion wrapper-->
                            <div class="accordion md-accordion paddTB40" id="accordion{{$type}}" role="tablist" aria-multiselectable="true">
                                <?php
                                    $countss = 0;

                                    for ($x = 0; $x < count($f["faqtranslates"]["question"]); $x++) {
                                        $question = $f["faqtranslates"]["question"][$x];
                                        $answer = $f["faqtranslates"]["answer"][$x];
                                        ?>
                                    <!-- Accordion card -->
                                    <div class="card bord-none">

                                        <!-- Card header -->
                                        <div class="card-header bg-none" role="tab" id="heading{{$x}}">
                                            <?php
                                                    if ($countss == 0) {
                                                        ?>
                                                <a data-toggle="collapse" data-parent="#accordion{{$type}}" href="#{{$type}}collapse{{$x}}" aria-expanded="true" aria-controls="{{$type}}collapse{{$x}}">
                                                <?php
                                                        } else {
                                                            ?>
                                                    <a data-toggle="collapse" data-parent="#accordion{{$type}}" href="#{{$type}}collapse{{$x}}" aria-expanded="false" aria-controls="{{$type}}collapse{{$x}}">
                                                    <?php
                                                            }
                                                            ?>
                                                    <h5 class="mb-0 MuliBold color-black">
                                                        {!! $question !!} <i class="fa fa-angle-right rotate-icon" style="float: right;"></i>
                                                    </h5>
                                                    </a>
                                        </div>

                                        <!-- Card body -->
                                        <?php
                                                if ($countss == 0) {
                                                    ?>
                                            <div id="{{$type}}collapse{{$x}}" class="collapse show" role="tabpanel" aria-labelledby="heading{{$x}}" data-parent="#accordion{{$type}}">
                                            <?php
                                                    } else {
                                                        ?>
                                                <div id="{{$type}}collapse{{$x}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$x}}" data-parent="#accordion{{$type}}">
                                                <?php
                                                        }
                                                        ?>
                                                <div class="card-body pt-0">
                                                    {!! $answer !!}
                                                </div>
                                                </div>

                                            </div>
                                            <hr>
                                            <!-- Accordion card -->
                                        <?php
                                           $countss =  $countss + 1;
                                            }


                                            ?>
                                    </div>
                            </div>
                        <?php
                        }
                        ?>
                        <div>
                        </div>
                        <div>
</section>

<section class="faq-content-search" style=" display:none;">
    <div class="container">
        <div class="row">
            <div class="tab-content" id="tab-content-search">

             <div>
         </div>
     <div>
</section>

<section class="faq-load-more bg-orange d-none d-lg-block mt-5">
    <button id="faq-load-more-btn" class="btn-load-section fs-20 text-center pt-3" style="display : table-row; vertical-align : bottom;">
        <a class="color-white" href="{{ route('locale.region.contact', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
            <p style="letter-spacing: 2px"><b>@lang('website_contents.landing.faq.content.need_help')</b></p>
        </a>
    </button>
</section>
<script>
      let currentCountryIso = "<?php echo strtolower(view()->shared('currentCountryIso')) ?>";
    </script>
<script src="{{ asset('js/functions/faq/faq.function.js') }}"></script>
@endsection
