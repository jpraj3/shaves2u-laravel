@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/aboutus/aboutus.css') }}">
<style>
h1,h2,h3{
    font-weight: bolder;
    font-family: 'Muli-ExtraBold';
}
h1{
    font-size:32px;
    margin: 5% 0;
    font-family: 'Muli-ExtraBold';
    color: #1f1f1f;
    text-align: center;
}
a{
    color:#ff6c00;
}
div{
    font-family: 'Muli';
    font-size:16px;
}
</style>
<section class="career-welcome" style="">
        <div class="container">
            <div class="row">
                <div class="container">
                    <h1 class="Muli text-center col-12">{!! $content->title !!}</h1>
                    <img class="img img-responsive img-fluid" src="{{ $content->bannerUrl }}" />
                </div>
            </div>
        </div>
</section>

<section class="paddTB40 career-welcome" style="">
        <div class="container">
            @php($URL = config('environment.webUrl') . view()->shared('url') .'-'. view()->shared('currentCountryIso') . '/')
            @php($content = str_replace("https://shaves2u.com/",$URL,$content->content))
            {!! $content !!}
    </div>
</section>
@endsection
