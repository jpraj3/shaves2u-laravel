@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/aboutus/aboutus.css') }}">
<style>
.container {
    max-width: 1500px !important;
}

.flex-row-container {
    background: white;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
}

.flex-row-container > .flex-row-item {
    flex-grow: 1;
    flex: 1 1 30%;
    height: 280px;
}

.flex-row-item {
  background-color: white;
  /* border: 1px solid #f76707; */
}

.flex-row-item-content {
  padding: 2%;
}

.article-title{
    font-style:bold;
    font-size: 18px;
    height:55px;
}

.lineHeader{
    text-align: center;
    margin-top: 10px;
    margin-bottom: 36px;
}

p{
    font-size:16px;
    font-family: 'Muli-ExtraBold';
    text-align: center;
}

@media (min-width: 768px) and (max-width: 950px) {
    .flex-row-container > .flex-row-item {
        height: 240px;
    }
}

@media (min-width: 412px) and (max-width: 767px) {
    .article-title{
        font-style:bold;
        font-size: 18px;
        height: 55px;
    }
    .flex-row-container { }
    .flex-row-container > .flex-row-item {
        flex-grow: 1;
        flex: 1 1 50%;
        height: 25vh;
    }

    .flex-row-item {}

    .flex-row-item-content { }
}

@media (max-width: 411px) {
    .article-title{
        font-style:bold;
        font-size: 20px;
        height: inherit;
    }
    .flex-row-container { }
    .flex-row-container > .flex-row-item {
        flex-grow: 1;
        flex: 1 1 100%;
        /* height: 30vh; */
    }

    .flex-row-item {}

    .flex-row-item-content { }
}

</style>

<section class="paddTB40 career-welcome" style="">
        <div class="container">
            <div class="row">
                <div class="container">
                    {{-- <div class="col-md-12 text-center pr-0 pl-0">
                        <h3 class="bold-max" style="margin-bottom: 4%; margin-top: 2%; color:black; font-family: Muli-ExtraBold"><strong>Let's welcome you<br class="d-md-none"> to the family<br></strong></h3>
                    </div>
                    <div class="col-md-12 text-center pr-0 pl-0">
                        <p class=" d-none d-md-block" style="margin-bottom: 4%; margin-top: 2%; font-size: 15px; color:black; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation<br> ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <p class="d-md-none" style="margin-bottom: 10%; margin-top: 2%; font-size: 15px; color:black; font-family: Muli;">Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit.</p>
                    </div> --}}
                    <div class="img-center">
                        <div class="image-container">
                            <div class="articles-img d-none d-md-block">
                                <img class="img img-responsive img-fluid" alt="The MANual logo | Shaves2U" src="https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/7059f770-d2e8-11e8-9b28-19e53b218776.png">
                            </div>
                            <div class="articles-img d-md-none">
                                <img class="img img-responsive img-fluid" alt="Shaving Magazing - The MANual Logo | Shaves2U" src="https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/product-images/f6cf77f0-d2dc-11e8-844f-5def1bf77da2.png">
                            </div>
                        </div>
                        <hr class="lineHeader">
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="magazine-list">
    <div class="container">
        <div class="row">
                <div class="col-12 flex-row-container" style="margin-bottom: 10vh;">
                @if(!empty($lists))
                @foreach($lists as $list)
                
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        @php($article_url = explode('/', $list->URL)[2])
                        <a href="{{ route('locale.magazines.content', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso'), 'magazine_name' => $article_url]) }}">
                            <img class="img img-responsive img-fluid" src="{{ $list->bannerUrl }}" />
                        </a>
                        <p class="article-title text-center"><b>{{ $list->title}}</b></p>
                    </div>
              
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>

@endsection
