
@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/privacy-policy/privacy-policy.css') }}">

{!!$privacycontent!!}

<script src="{{ asset('js/functions/privacy-policy/privacy-policy.function.js') }}"></script>
@endsection