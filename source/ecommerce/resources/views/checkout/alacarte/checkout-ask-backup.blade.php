@extends('layouts.app')
@section('content')




@php($user = isset($User) ? $User["user"] : null)
@php($user_id = isset($User) ? $User["user"]["id"] : null)
@php($delivery_address = isset($User) ? $User["delivery_address"] : null)
@php($billing_address = isset($User) ? $User["billing_address"] : null)
@php($cards = isset($User) ? $User["cards"] : null)
@php($default_card = isset($User) ? $User["default_card"] : null )
@php($session = $session_data)
@php($checkout_details = isset($checkout_details) ? $checkout_details : null )
<style>
    .hasError_msg {
        vertical-align: middle !important;
        text-align: center;
        margin: auto;
        color: black;
        font-weight: 700;
    }

    .hasError2 {
        background: #e8bdbd;
        margin: 5% 0;
        padding: 1% 0;
    }

    .hasError {
        color: red;
    }
</style>
<link rel="stylesheet" href="{{ asset('css/alacarte/ask/ask-checkout.css') }}">
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<script src="{{ asset('js/iamport.js') }}"></script>
<div class="container">
    <!-- Hidden Inputs for Checkout -->
    <input type="hidden" name="payment_intent_next_update_type" id="payment_intent_next_update_type"
        value="first-update" />
    <input type="hidden" name="payment_intent_id" id="payment_intent_id" value="" />
    <!-- END -->

    <div class="row px-3">
        <div class="col-3 pr-0 pl-0 d-flex align-items-center">
            <button id="button-back" class="button-back">
                < BACK</button> </div> <div class="col-6 pr-0 pl-0 d-flex align-items-center">
                    <div class="checkout-title-1 col-12 text-center pr-0 pl-0 d-lg-none">
                        <h4 class="mb-0">Payment</h4>
                    </div>
                    <div class="checkout-title-2 col-12 text-center pr-0 pl-0 d-none d-lg-none">
                        <h4 class="mb-0">Total Summary</h4>
                    </div>
                    <div class="col-12 text-center pr-0 pl-0 d-none d-lg-block">
                        <h4 class="mb-0">Payment</h4>
                    </div>
        </div>
    </div>

    <div class="container" style="display:inline-flex;">
        <!-- CHECKOUT  -->
        <div id="checkout-block" class="col-8 panel-group">
            <!--Account-->
            <div class="col-12 panel panel-default" id="account-section">
                <div id="step1-heading" class="panel-heading panel-heading-selected mobile_heading1 d-lg-none"
                    data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step1-title" class="panel-title panel-title-selected mobile_title1"
                        style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
                        Account
                    </h5>
                </div>
                <div class="collapse1 col-12 panel-collapse collapse show" style="margin: 0px !important; padding: 20px"
                    id="account-body"></div>
                <div id="collapse1" class="panel-collapse collapse show">
                    <div class="panel-body">
                        <div class="col-12 pt-2 pb-5">
                            <div class="col-12 rounded-10 bg-white shadow-sm padd15">
                                <h4 class="MuliExtraBold">Personal Details</h4>
                                <div class="col-lg-12 padd20 pl-0 pr-0">
                                    @if(!Auth::check())
                                    <div class="col-12 rounded-10" style="display: inline-flex;text-align:center;">
                                        <div class="col-6">
                                            <h4 class="MuliExtraBold" onClick="checkout_login()">Login</h4>
                                        </div>
                                        <div class="col-6">
                                            <h4 class="MuliExtraBold" onClick="checkout_register()">Register</h4>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!Auth::check())
                                    <form id="new_user_login" method="POST" action="">
                                        @csrf

                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-group">
                                                <input type="email" class="fs-20 form-control"
                                                    id="sign_in_sign_up_email" placeholder="johndoe@gmail.com" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-angle-right float-right"
                                                        style="position:absolute; right: 10px; top: 5px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <div class="input-group">
                                                <input type="password" class="fs-20 form-control"
                                                    id="sign_in_sign_up_password" placeholder="" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-angle-right float-right"
                                                        style="position:absolute; right: 10px; top: 5px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="error_login" class="hasError2" hidden></div>
                                        <div class="col-lg-12 pl-0 pr-0 text-center">
                                            <button type="submit" class="btn btn-load-more"
                                                style="width: 100%;"><b>Login</b></button>
                                        </div>
                                    </form>
                                    <form id="new_user_register" hidden method="POST" action="">
                                        @csrf
                                        <div class="form-group">
                                            <label>@lang('website_contents.authentication.register.name')</label>
                                            <div class="input-group">
                                                <input id="register_name" type="text" name="register_name"
                                                    class="fs-20 form-control" placeholder="John Doe" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-angle-right float-right"
                                                        style="position:absolute; right: 10px; top: 5px;"></i>
                                                </div>
                                            </div>
                                            <div id="name_error" class="hasError col-12 col-form-label text-left"></div>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-group">
                                                <input type="email" class="fs-20 form-control"
                                                    id="sign_in_sign_up_email_2" placeholder="johndoe@gmail.com" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-angle-right float-right"
                                                        style="position:absolute; right: 10px; top: 5px;"></i>
                                                </div>
                                            </div>
                                            <div id="email_error" class="hasError col-12 col-form-label text-left">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <div class="input-group">
                                                <input type="password" class="fs-20 form-control"
                                                    id="sign_in_sign_up_password_2" placeholder="" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-angle-right float-right"
                                                        style="position:absolute; right: 10px; top: 5px;"></i>
                                                </div>
                                            </div>
                                            <div id="password_error" class="hasError col-12 col-form-label text-left">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="date-of-birth" style="padding: 0 0px;"
                                                class="col-12 col-form-label text-left">@lang('website_contents.authentication.register.date_of_birth')</label>
                                            <div class="row mx-0">
                                                <div class="col-4" style="padding: 0 0px;">
                                                    <select id="day_of_birth" class="form-control custom-select"
                                                        name="day_of_birth" required
                                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                                        <option value="" selected disabled hidden>DD</option>
                                                        @for($i=1;$i<32;$i++) <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                                <div class="col-4">
                                                    <select id="month_of_birth" class="form-control custom-select"
                                                        name="month_of_birth" required
                                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                                        <option value="" selected disabled hidden>MM</option>
                                                        @for($i=1;$i<13;$i++) <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                                <div class="col-4" style="padding: 0 0px;">
                                                    <select id="year_of_birth" class="form-control custom-select"
                                                        name="year_of_birth" required
                                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                                        <option value="" selected disabled hidden>YY</option>
                                                        @for($i=0;$i<100;$i++) <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="birthday_error" class="hasError col-12 col-form-label text-left">
                                            </div>
                                        </div>
                                        <div id="error_register" class="hasError2" hidden></div>
                                        <div class="col-lg-12 pl-0 pr-0 text-center">
                                            <button id="_register_btn" type="submit" class="btn btn-load-more"
                                                style="width: 100%;"><b>Sign
                                                    Up</b></button>
                                        </div>
                                    </form>
                                    @else
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <input disabled type="text" class="fs-20 form-control"
                                                placeholder="johndoe@gmail.com" value="{{Auth::user()->email}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <div class="input-group">
                                            <input disabled type="text" class="fs-20 form-control"
                                                placeholder="●●●●●●●●●●" />
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Shipping Address-->
            <div class="col-12 panel panel-default" id="shipping-address-section">
                <div class="col-12 step2-heading panel-heading panel-heading-unselected" data-toggle="collapse"
                    data-parent="#accordion" href="#collapse-" id="shipping-address-header" onClick="shippingaddress()"
                    style="margin: 0px;padding: 0px;height: 100%;">
                    <h5 class="step2-title panel-title panel-title-unselected product-group-title"
                        style="padding-top: 15px;padding-bottom: 15px;text-align: center;">Shipping Address <i
                            class="fa fa-chevron-down"></i></h5>
                </div>
                <div id="collapse2" class="panel-collapse collapse" style="margin: 0px !important; padding: 20px">
                </div>

                <div id="shipping-address-body" class="collapse2 panel-collapse"
                    style="margin: 0px !important; padding: 20px">
                    @if($delivery_address !== null && count($delivery_address) > 0)
                    @if(strtolower($currentCountryIso) == 'kr')
                    @include('checkout.alacarte.checkout-modules.delivery-address.delivery-address-kr-edit')
                    @else
                    @include('checkout.alacarte.checkout-modules.delivery-address.delivery-address-edit')
                    @endif
                    @else
                    @if(strtolower($currentCountryIso) == 'kr')
                    @include('checkout.alacarte.checkout-modules.delivery-address.delivery-address-kr-add')
                    @else
                    @include('checkout.alacarte.checkout-modules.delivery-address.delivery-address-add')
                    @endif
                    @endif
                    <div>
                        @if($delivery_address !== null && count($delivery_address) > 0)
                        <button id="enable-address-edit" class="" onclick="editAddress()">EDIT ADDRESS</button>
                        <button id="enable-address-add" class="" onclick="addAddress()">ADD ADDRESS</button>
                        @else
                        <button id="enable-address-add" class="" onclick="addAddress()">ADD ADDRESS</button>
                        <button id="enable-address-edit" class="" onclick="editAddress()" style="display:none;">EDIT
                            ADDRESS</button>
                        @endif
                        <button id="cancel-address-edit" onclick="cancelEditAddress()"
                            style="display:none;">Cancel</button>
                        <label id="estimated-delivery-date" style="font-weight: bold; font-size: 13px">{{Carbon\Carbon::now()->addDays(7)->format('M d')}} - {{Carbon\Carbon::now()->addDays(14)->format('M d')}}</label><br>

                    </div>
                </div>

            </div>
            <!--for kr-->
            @if($currentCountryIso != 'kr')
            <!--Payment-->
            <div class="col-12 panel panel-default" id="payment-section" onClick="payment()">
                <div id="payment-header" class="col-12 step3-heading panel-heading panel-heading-unselected"
                    data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step3-title" class="product-group-title panel-title panel-title-unselected"
                        style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
                        Payment <i class="fa fa-chevron-down"></i>
                    </h5>
                </div>
                <div id="payment-body" class="collapse3 panel-collapse collapse"
                    style="margin: 0px !important; padding: 20px">
                    asdasd123 @include('checkout.alacarte.checkout-modules.payment')
                </div>
                <br>
            </div>
            @endif
            <span style="color: red;" id="error_msg_alacarte"></span>
            <button id="button-next" class="button-proceed-checkout"></button>
            <div id="form-pay" class="display-none">
                {{ csrf_field() }}
                <button class="button-proceed-checkout">PROCEED TO PAY</button>
            </div>

        </div>
        <!-- SUMMARY -->
        <!-- // Promotion -->
        <div id="summary-block" class="col-4 panel-group">
            <div style="margin: 0px !important; padding: 20px">
                <div class="panel-body">
                    <label style="font-weight: bold">TOTAL SUMMARY</label><br>
                    <div class="box-grey">
                        <br><label>ASK</label>
                        <br><label>5 Blade Cartridge</label>
                        <p id="c-free-product"></p>
                        <p id="c-free-exist-product"></p>
                        <hr />
                        <br><label>Subtotal: <span id="c-subtotal">{{$current_price}}</span></label>
                        <!-- <br><label>Next Month Subtotal: <span id="c-nsubtotal">{{$next_price}}</span></label> -->
                        <br><label>Shipping: <span id="c-shipping">{{$shipping_fee}}</span></label>
                        <br><label>Discount: <span id="c-discount"></span></label>
                        <hr />
                        <br><label>Promo Code</label>
                        <br><label>Enter promo code</label>
                        <br><input id="promo_code" type="text" />
                        <br><button onclick="applyPromotion()">APPLY</button>
                        <br>
                        <p class="error-promotion" id="error-promotion"></p>
                        <p class="success-promotion" id="success-promotion"></p>
                        <hr />
                        <!-- <br><label>Next Month Total: <span id="c-ntotal">{{$next_price}}</span></label> -->
                        <br><label>Tax: <span id="c-tax">{{$taxAmount}}</span></label>
                        <br><label>Today Total</label>
                        <br><label><span id="c-total">{{number_format($current_totalprice, 2, '.', '') }}</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<script>
    let country_id = "<?php echo $currentCountryIso; ?>";
    let countryid = {!! json_encode($currentCountryid)!!};
    let langCode = {!!json_encode($langCode)!!};
    let urllangCode = {!! json_encode($urllangCode)!!};
    let user_id = {!!json_encode($user_id)!!} !== null ? "<?php echo $user_id; ?>" : null;
    let user = {!!json_encode($user)!!} !== null ? {!!json_encode($user)!!} : null;
    let delivery_address = {!!json_encode($delivery_address)!!} !== null ? {!!json_encode($delivery_address)!!} : null;
    let billing_address = {!!json_encode($billing_address)!!} !== null ? {!!json_encode($billing_address)!!} : null;
    let default_card = {!!json_encode($default_card)!!} !== null ? {!!json_encode($default_card)!!} : null;
    let session_data = {!!json_encode($session)!!};
    let payment_intent = <?php echo $payment_intent; ?> !== "" ? <?php echo $payment_intent; ?> : "";
    let checkout_details = <?php echo $checkout_details; ?> !== "" ? <?php echo $checkout_details; ?> : "";
    let current_price = {!!json_encode($current_totalprice)!!};
    let next_price = {!!json_encode($next_totalprice)!!};
    let shipping_fee = {!!json_encode($shipping_fee)!!};
    let taxRate = <?php echo $taxRate; ?>;
    let ctaxAmount = <?php echo $taxAmount; ?>;
</script>

<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script src="{{ asset('js/functions/alacarte/ask/ask-checkout.function.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>
@endsection
