@extends('layouts.app')
@section('content')
<!-- PHP START -->
@php($user = isset($User) ? $User["user"] : null)
@php($user_id = isset($User) ? $User["user"]["id"] : null)
@php($delivery_address = isset($User) ? $User["delivery_address"] : null)
@php($billing_address = isset($User) ? $User["billing_address"] : null)
@php($cards = isset($User) ? $User["cards"] : null)
@php($default_card = isset($User) ? $User["default_card"] : null )
@php($session = $session_data)
@php($checkout_details = isset($checkout_details) ? $checkout_details : null )
@php($currentCountry = session()->has('currentCountry') ? session()->get('currentCountry') : null )
@php($sessionLoginError = session()->get('loginerror'))
<?php session()->forget('loginerror'); ?>
<link rel="stylesheet" href="{{ asset('css/alacarte/ask/ask-checkout.css') }}">
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<script src="{{ asset('js/iamport.js') }}"></script>

<style>
    input.mask_content {
        -webkit-text-security: disc;
    }

    .error {
        color: red;
    }

    .error-promotion {
        color: red;
    }
    .hasError{
        padding:0px;color:red;margin-top:10px;
    }
    .notification {
        height: 40px;
        position: sticky;
        top: 0;
        width: 100%;
        display: table;
        background: #f4f8fc;
        opacity: 1;
        z-index: 10000;
    }

    .notification > span {
        text-align: center;
        margin: auto;
        display: table-cell;
        vertical-align: middle;
    }

    .click-able {
        color: #ed594a;
        cursor: pointer;
    }
</style>

<div class="grey-bg">

    <!-- Hidden Inputs for Checkout -->
    <input type="hidden" name="payment_intent_next_update_type" id="payment_intent_next_update_type" value="first-update" />
    <input type="hidden" name="payment_intent_id" id="payment_intent_id" value="" />
    <!-- END -->

    <!-- Begin: Mobile Layout -->
    <div class="row px-3">
        <div class="checkout-content-mobile col-12 padd0 d-lg-none">
            <div class="panel-group" id="accordion">
            <div class="panel panel-default" style="margin: 0px !important ;background: white; border-top: 2px solid #dee2e6!important;">
                    <div class="mobile_heading1 col-12">
                                <button id="button-back" class="button-back" style="position:absolute; bottom: -3px;">
                                    @lang('website_contents.global.content.back')
                                </button>
                        <div class="row mx-auto text-center">
                            <div class="fs-22 MuliBold panel-title mobile_title1 m-0 pt-0 mx-auto pb-2"><b>
                                @lang('website_contents.checkout.content.payment')
                            </b></div>
                        </div>
                    </div>
             </div>

                <div class="panel panel-default border-top" style="margin: 0px !important">
                    <div id="step1-heading" class="panel-heading panel-heading-selected mobile_heading1 d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                        <h5 id="step1-title" class="panel-title panel-title-selected mobile_title1" style="padding-top: 15px; padding-bottom: 15px; text-align: center;">
                        @lang('website_contents.checkout.content.account')
                        </h5>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse show">
                        <div class="panel-body">
                            <div class="col-12 pt-2 pb-3">
                                <div class="col-12 rounded-10 bg-white shadow-sm padd15">
                                    @if($loginStatus === "pre-login")
                                    <div class="container-create-account">

                               

                                        <form id="form_register" method="POST" action="" onsubmit="return false">
                                            @csrf
                                            <div class="col-lg-12 padd20 pt-0 pl-0 pr-0">
                                                <div class="col-lg-12 padd20 pl-0 pr-0">
                                                    <div class="register-group form-group">
                                                        <label class="color-grey pb-0">@lang('website_contents.checkout.user.name')</label>
                                                        <div class="input-group">
                                                            <input id="d-name-tk" name="name" type="text" class="name padd0 form-control MuliBold @error('name') is-invalid @enderror" required /> @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                            <div id="name_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="color-grey">@lang('website_contents.checkout.user.email_address')</label>
                                                        <div class="input-group">
                                                            <input id="d-email-tk" name="email" type="email" class="email padd0 form-control MuliBold @error('email') is-invalid @enderror" required /> @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                            <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password" class="color-grey">@lang('website_contents.checkout.user.password')</label>
                                                        <div class="input-group">
                                                            <input id="d-password-tk" placeholder="<?php if(strtolower($currentCountryIso) == 'kr'){ echo trans('validation.custom.validation.password.validpassword'); }?>" name="password" type="password" class="password padd0 form-control MuliBold @error('password') is-invalid @enderror" required autocomplete="new-password"><span style="position:absolute; right: 0;" toggle="#d-password-tk" class="fa fa-eye field-icon toggle-password"></span>
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span> @enderror
                                                        </div>
                                                    </div>
                                                    <div id="password_error" class="hasError col-12 col-form-label text-left"></div>
                                                </div>
                                                <div class="error error_password"></div>
                                                <div class="register-group form-group">
                                                    <div class="row">
                                                        <label for="date-of-birth" class="col-12 col-form-label text-left color-grey">@lang('website_contents.checkout.user.date_of_birth')</label>
                                                        <div class="col-4 pl-3 MuliBold">
                                                            <select id="d-day-of-birth-tk" class="day-of-birth form-control custom-select @error('name') is-invalid @enderror" name="day-of-birth" required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.dd')</option>
                                                                @for($i=1;$i
                                                                <32;$i++) @switch($i) @case($i) @if($i < 10) <option value="{{sprintf(" %02d ", $i)}}">
                                                                    {{sprintf("%02d", $i)}}</option>
                                                                    @else
                                                                    <option value="{{ $i }}">{{$i}}</option>
                                                                    @endif @break @default @endswitch @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-4 pl-1 MuliBold">
                                                            <select id="d-month-of-birth-tk" class="month-of-birth form-control custom-select" name="month-of-birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.mm')</option>
                                                                @for($i=1;$i<13;$i++) @switch($i) @case(1) <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jan')</option>
                                                                @break @case(2)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.feb')</option>
                                                                @break @case(3)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.mar')</option>
                                                                @break @case(4)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.apr')</option>
                                                                @break @case(5)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.may')</option>
                                                                @break @case(6)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jun')</option>
                                                                @break @case(7)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jul')</option>
                                                                @break @case(8)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.aug')</option>
                                                                @break @case(9)
                                                                <option value="0{{ $i }}">@lang('website_contents.global.dates.months.sep')</option>
                                                                @break @case(10)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.oct')</option>
                                                                @break @case(11)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.nov')</option>
                                                                @break @case(12)
                                                                <option value="{{ $i }}">@lang('website_contents.global.dates.months.dec')</option>
                                                                @break
                                                                @default
                                                                @endswitch
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-4 pr-3 MuliBold">
                                                            <select id="d-year-of-birth-tk" class="year-of-birth form-control custom-select" name="year-of-birth" required required style="background-image: url({{asset('/images/common/arrow-down.svg')}});" required>
                                                                <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.yyyy')</option>
                                                                @for($i=30;$i<100;$i++) @switch($i) @case($i) @if($i < 10) <option value="19{{sprintf(" %02d ", $i)}}">
                                                                    19{{sprintf("%02d", $i)}}</option>
                                                                    @else
                                                                    @if($i==80)
                                                                    <option value="19{{ $i }}" selected>19{{$i}}</option>
                                                                    @else
                                                                    <option value="19{{ $i }}">19{{$i}}</option>
                                                                    @endif
                                                                    @endif @break @default @endswitch @endfor
                                                                    @for($i=0;$i<(int)substr(date("Y"), -2) + 1;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf(" %02d ", $i)}}">
                                                                        20{{sprintf("%02d", $i)}}</option>
                                                                        @else
                                                                        <option value="20{{ $i }}">20{{$i}}</option>
                                                                        @endif @break @default @endswitch @endfor
                                                            </select>
                                                        </div>
                                                        @error('date-of-birth')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span> @enderror
                                                        <div id="birthday_error" class="hasError col-12 col-form-label text-left pl-4"></div>
                                                    </div>
                                                </div>

                                                @if(strtolower($currentCountryIso) == 'kr')
                                                    <div class="form-group register-group">
                                                        <div class="row">
                                                            <div class="input-group">
                                                                <input required id="tos_agree" name="tos_agree" type="checkbox" class="col-1 @error('tos_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.tos_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="privacy_policy_agree" name="privacy_policy_agree" type="checkbox" class="col-1 @error('privacy_policy_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.privacy_policy_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="personal_info_agree" name="personal_info_agree" type="checkbox" class="col-1 @error('personal_info_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.personal_info_transfer_agreement')</u></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input required id="min_age_agree" name="min_age_agree" type="checkbox" class="col-1 @error('min_age_agree') is-invalid @enderror"/>
                                                                <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.minimum_age_agreement')</p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input id="email_sub_agree" name="email_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.email_sub_agreement')</p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input id="sms_sub_agree" name="sms_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.sms_sub_agreement')</p>
                                                            </div>
                                                            <ul class="col-11 offset-1">
                                                                <li>@lang('website_contents.authentication.register.sms_sub_agreement_point1')</li>
                                                                <li>@lang('website_contents.authentication.register.sms_sub_agreement_point2')</li>
                                                            </ul>
                                                        </div>
                                                        @error('required_checked')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                        <div id="required_checked_error" class="hasError col-12 col-form-label text-left"></div>
                                                    </div>
                                                @else
                                                    <div class="form-group register-group">
                                                        <div class="row text-left">
                                                            <div class="input-group">
                                                                <input id="marketing_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                                <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.marketing_sub_agreement')</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-lg-12 pb-2 pl-0 pr-0">
                                                    <button id="btn-create-account-desktop" type="submit" class="btn-create-account btn btn-load-more-orange w-100">
                                                    @lang('website_contents.checkout.user.create_account')
                                                    </button>
                                                </div>
                                                <div class="col-lg-12 pb-2 pl-0 pr-0 pt-2">
                                                    <a href="{{ route('auth.facebook', ['type' =>'purchase_ask','langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" class="btn btn-load-more-fb w-100"><img src="{{asset('/images/common/checkoutAssets/fb_icon.png')}}" />
                                                    <b>@lang('website_contents.checkout.user.sign_in_facebook')</b></a>
                                                </div>
                                                <div class="col-lg-12 pl-0 pr-0 pt-2">
                                                    <h6 class="text-sign-in">@lang('website_contents.checkout.user.have_account')</h6>
                                                    <h6 class="text-create-one hidden">@lang('website_contents.checkout.user.no_account')</h6>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    @else
                                    <div class="container-profile-account">

                                        <!-- <div class="row ml-0 mr-0">
                                            <h4 class="MuliExtraBold pl-2 pt-2 pb-2">@lang('website_contents.checkout.user.personal_details')</h4>
                                            <i class="orange-tick-personal fa fa-check-circle ml-auto fs-30 text-orng hidden"></i>
                                        </div> -->

                                        <div class="col-lg-12 padd20 pt-0 pb-0 pl-0 pr-0">
                                            <div class="col-lg-12 padd20 pt-0 pb-0 pl-2 pr-0">
                                                <div class="form-group">
                                                    <label class="color-grey">@lang('website_contents.checkout.user.name')</label>
                                                    <div class="input-group">
                                                        <label class="MuliBold form-control">{{$user['firstName']}}
                                                           </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="color-grey">@lang('website_contents.checkout.user.email')</label>
                                                    <div class="input-group">
                                                        <label class="MuliBold form-control">{{$user['email']}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pl-2 pr-0 pt-1 pb-0">
                                                <h6 class="text-sign-in" onClick="switchAccounts();">
                                                    @lang('website_contents.authenticated.content.switch_account')
                                                </h6>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div id="step2-heading" class="panel-heading panel-heading-unselected mobile_heading2 d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                        <h5 id="step2-title" class="panel-title panel-title-unselected mobile_title2 pl-2" style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
                        @lang('website_contents.checkout.address.shipping_address')
                        </h5>
                    </div>
                    @if($loginStatus === "post-login")
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body pb-3">
                            <div class="col-12 pt-2">
                                <div class="rounded-10 bg-white padd20 pt-2 pb-3">
                                    @if(count($delivery_address) > 0) @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-edit') @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-edit') @endif
                                    @else
                                    @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-add') @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-add') @endif
                                    @endif
                                    <div class="col-12 pt-4 pl-0 pr-0" hidden>
                                        @if(count($delivery_address) > 0)
                                        <div class="row mx-0">
                                            <div class="col-6 pl-0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">@lang('website_contents.checkout.address.edit_address')</button>
                                            </div>
                                            <div class="col-6 pr-0">
                                                <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">@lang('website_contents.checkout.address.add_address')</button>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row mx-0">
                                            <div class="col-6 padd0">
                                                <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">@lang('website_contents.checkout.address.add_address')</button>
                                            </div>
                                            <div class="col-6 padd0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()" style="display:none;">@lang('website_contents.checkout.address.edit_address')</button>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-6 padd0">
                                            <button id="cancel-address-edit" class="btn btn-load-more col-12" onclick="cancelEditAddress()" style="display:none;">Cancel</button>
                                            <label class="pt-4" id="estimated-delivery-date" style="font-weight: bold; font-size: 13px">@lang('website_contents.checkout.address.estimated_delivery', ['date1' => Carbon\Carbon::now()->addDays(7)->format('M d'),'date2' => Carbon\Carbon::now()->addDays(14)->format('M d') ])</label>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End: Mobile Shipping -->
                </div>
                @endif
            </div>
            @if(strtolower($currentCountryIso) != 'kr')
            <div class="panel panel-default">
                <div id="step3-heading" class="panel-heading panel-heading-unselected mobile_heading3 d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step3-title" class="panel-title panel-title-unselected mobile_title3" style="padding-top: 20px; padding-bottom: 15px; text-align: center;">
                    @lang('website_contents.checkout.payment_method.payment_method')
                    </h5>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-12 pt-2 pb-3">
                            <div class="col-12 rounded-10 bg-white shadow-sm padd15">
                                <div class="row mr-0 ml-0">
                                    <h4 class="MuliExtraBold d-none d-sm-block d-md-none">@lang('website_contents.checkout.payment_method.payment_method')</h4>
                                    @if($loginStatus === "post-login")
                                    <!-- Add card -->
                                    <div id="add-card-container" class="hidden">
                                    <form id="form_card">
                                        <div class="row mr-0 ml-0">
                                                <div class="col-10 offset-2" style="margin: auto;">
                                                        <div class="row justify-content-center pt-4">
                                                            <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                                                <div class="text-left">
                                                                    <p class="fs-16 MuliSemiBold pt-3"><span class="cc_last4digits">xxxx xxxx xxxx 1234</span></p>
                                                                </div>
                                                                <div class="row justify-content-between">
                                                                    <div class="col-7">
                                                                        <div class="text-left">
                                                                            <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                                            <p class="fs-18 MuliPlain"><span class="cc_expmy">xx/xx</span></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                                                        <div class="add-logo-change">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <div class="col-12 pl-0 pr-0 pb-0">
                                                <div class="row mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <div class="form-group pl-0 pr-0">
                                                            <label>@lang('website_contents.checkout.payment_method.card_number')</label>
                                                            <input id="card-number-masked" name="card_number_masked" type="text" class="card_number_masked number-only fs-20 form-control pl-0" maxlength="19" />
                                                            <input id="card-number" name="card_number" type="hidden" class="credit-card-logo card_number number-only fs-20 pl-0" maxlength="19" />
                                                            <div id="c_cardNumber_error" class="hasError col-12 col-form-label text-left"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 padd20 pl-0 pt-0 pb-0">
                                                        <div class="form-group pl-0 pr-0 pb-0">
                                                            <label>@lang('website_contents.checkout.payment_method.expiry_date')</label>
                                                            <input id="card-expiry" name="card_expiry_date" type="text" class="card_expiry_date number-only fs-20 form-control" placeholder="xx / xx" maxlength="7" />
                                                        </div>
                                                    </div>
                                                    <div class="col-6 padd20 pr-0 pt-0 pb-0">
                                                        <div class="form-group pl-0 pr-0 pb-0">
                                                            <label>@lang('website_contents.checkout.payment_method.cvv')</label>
                                                            <input id="card-cvv" name="card_cvv" type="text" class="card_cvv number-only fs-20 form-control mask_content" placeholder="xxx" maxlength="4" />
                                                        </div>
                                                    </div>
                                                    <div id="c_expirydate_error" class="hasError col-12 col-form-label text-left"></div>
                                                    <div id="c_ccv_error" class="hasError col-12 col-form-label text-left"></div>
                                                    <div id="error-payment-method" class="error MuliPlain hidden padd20 pr-0 pt-0"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center pb-2">
                                                <div class="row">
                                                    <div class="col-12 pb-2">
                                                        <button id="button-add-card" class="btn btn-load-more w-100" type="button"><b>@lang('website_contents.checkout.payment_method.save_card')</b></button>
                                                    </div>
                                                    <div class="col-12">
                                                        <button id="button-cancel-add-new-card" class="btn btn-load-more hidden w-100" type="button"><b>@lang('website_contents.global.content.cancel')</b></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 pl-0 pr-0 pl-2">
                                            <p>@lang('website_contents.checkout.payment_method.terms_of_service_agree', ['url' => route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])])</p>
                                            </div>
                                        </div>
                                    </form>
                                    </div>

                                    <!-- Edit card -->
                                    <div id="edit-card-container" class="hidden">
                                        <div class="row mr-0 ml-0">
                                            <div class="col-12 pl-0 pb-0 pr-0">
                                                <div class="col-10 offset-2" style="margin: auto;">
                                                    <div class="row justify-content-center pt-4">
                                                        <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                                            <div class="text-left">
                                                                <p class="fs-16 MuliSemiBold pt-3">xxxx xxxx xxxx <span class="cc_last4digits">{{ isset($default_card) && isset($default_card[0]) ? $default_card[0]['cardNumber'] : '1234' }}</span></p>
                                                            </div>
                                                            <div class="row justify-content-between">
                                                                <div class="col-7">
                                                                    <div class="text-left">
                                                                        <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                                        <p class="fs-18 MuliPlain"><span class="cc_expmy">{{ isset($default_card) && isset($default_card[0]) ? $default_card[0]['expiredMonth'] : 'xx' }}/{{ isset($default_card) && isset($default_card[0]) ? substr($default_card[0]['expiredYear'], -2) : 'xx' }}</span></p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                                                    <div class="logo-change">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 pl-0 pr-0">
                                                <div class="row mr-0 ml-0">
                                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.checkout.payment_method.selected_card')</label>
                                                            <select id="card-dropdown" class="MuliBold form-control" placeholder="">
                                                                <!-- @foreach($cards as $card)
                                                                        <option value="{{json_encode($card)}}">xxxx xxxx xxxx {{$card["cardNumber"]}}</option>
                                                                    @endforeach -->
                                                            </select>
                                                            <!-- <input type="text" class="fs-20 form-control" placeholder=""/> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-6 padd20 pl-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.checkout.payment_method.expiry_date')</label>
                                                            <input id="view-expiry-date" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="12 / 24" />
                                                        </div>
                                                    </div>
                                                    <div class="col-6 padd20 pr-0 pt-0 pb-0">
                                                        <div class="form-group">
                                                            <label class="color-grey">@lang('website_contents.checkout.payment_method.cvv')</label>
                                                            <input id="view-cvv" type="text" class="MuliBold form-control" readonly="readonly" style="background-color: white" value="xxx" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center justify-content-start">
                                                <div class="col-12 p-0 m-0 pl-0 pb-2">
                                                    <button id="button-add-new-card" class="btn btn-load-more w-100 uppercase"><b>@lang('website_contents.checkout.payment_method.add_new_card')</b></button>
                                                </div>
                                            </div>
                                            <div class="col-12 pr-0 pl-2">
                                            <p>@lang('website_contents.checkout.payment_method.terms_of_service_agree', ['url' => route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])])</p>
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: Mobile Payment Method -->

            </div>
            @endif
            <div class="panel panel-default">
                <div id="step4-heading" class="panel-heading panel-heading-unselected mobile_heading4 d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 id="step4-title" class="panel-title panel-title-unselected mobile_title4" style="padding-top: 20px; padding-bottom: 15px; text-align: center;">
                    @lang('website_contents.checkout.payment_summary.ships_today')
                    </h5>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-12 pt-2 pb-3">
                            <div class="col-12 rounded-10 bg-white shadow-sm ">
                                <div class="row mr-0 ml-0">

                                    <div class="col-12 pt-2 pb-1 pl-0 pr-0">

                                        <div class="col-12 rounded-t-10 bg-white shadow-sm pt-3 pb-0 pl-2 pr-2">
                                            <div class="row mr-0 ml-0">
                                                <h4 class="MuliExtraBold d-none d-sm-block d-md-none">@lang('website_contents.checkout.payment_summary.ships_today')</h4>
                                                <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                                                    <div class="row">
                                                        <div class="col-5 padd0 d-flex align-items-center">
                                                            <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}" />
                                                        </div>
                                                        <div class="col-7 padd0 d-flex align-items-center">
                                                            <div class="col-12 padd0">
                                                                <h3 class="MuliBold">@lang('website_contents.products.product.ask')</h3>
                                                                <p class="fs-18 Muli">{{$productname}}</p>
                                                                <p class="fs-18 Muli summary-number"><span id="c-free-product"></span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                                    <div class="row mr-0 ml-0">
                                                        <div class="col-12 padd0">
                                    <!-- Currency and price for kr -->

                                                        <?php
                               
                               $discount = '0.00';
                               $c_price = $current_price;
                               $s_fee = $shipping_fee;
                               if(strtolower($currentCountryIso) == 'kr'){
                                   $c_price = number_format($current_price);
                                   $s_fee= number_format($shipping_fee);
                                   $discount = '0';
                               }

                               ?>
                                                            <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.subtotal1', ['currency' => $currency, 'current_price' => $c_price])</label>
                                                            <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.shipping1', ['currency' => $currency, 'shipping_fee' => $s_fee])</label>
                                                            <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.discount1', ['currency' => $currency, 'discount' => $discount])</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                                    <div class="row mr-0 ml-0">
                                                        <div class="col-7 padd0">
                                                            <p id="c-tax" class="d-none">{{$taxAmount}}</span>
                                                                <!-- <p class="d-none" id="c-free-product"></p> -->
                                                                <p class="d-none" id="c-free-exist-product"></p>
                                                                <p class="d-none" id="c-ntotal"></p>
                                                                <p class="fs-18">@lang('website_contents.checkout.payment_summary.promo_code')</p>
                                                                <div class="form-group">
                                                                    <input id="promo_code" type="text" class="form-control" style="font-size: 14px" placeholder="@lang('website_contents.checkout.payment_summary.enter_promo_code')" />
                                                                </div>
                                                        </div>
                                                        <div class="col-5 padd0">
                                                            <button class="btn btn-load-more pt-1 pb-1" style="width: 93%; position: absolute; bottom: 20px;" onclick="applyPromotion()"><b>@lang('website_contents.checkout.payment_summary.apply')</b></button>
                                                        </div>
                                                        <p class="error-promotion" id="error-promotion"></p>
                                                        <p class="success-promotion" id="success-promotion"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 bg-dark shadow-sm padd15 text-white">
                                            <div class="row mr-0 ml-0">
                                                <div class="col-6 padd0">
                                                    <p class="MuliExtraBold fs-20 mb-0">@lang('website_contents.checkout.payment_summary.today_total')</p>
                                                </div>
                                                <div class="col-6 padd0 text-right">
                                                    <p class="MuliExtraBold fs-20 mb-0">
                                    <!-- Currency and price for kr -->
                                                @if(strtolower($currentCountryIso) == 'kr')
                                                <span id="c-total">{{number_format($current_price) }}</span>{{$currency}}
                                                @else
                                                {{$currency}}
                                                <span id="c-total">{{number_format($current_price, 2, '.', '') }}</span>
                                                @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <br>

                                        <!-- <div class="col-12 rounded-t-10 bg-white shadow-sm pt-3">
                                            <div class="row mr-0 ml-0">
                                                <h4 class="MuliExtraBold">SHIPS & CHARGES</h4>
                                                <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                                                    <div class="row">
                                                        <div class="col-5 padd0 d-flex align-items-center">
                                                            <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}" />
                                                        </div>
                                                        <div class="col-7 padd0 d-flex align-items-center">
                                                            <div class="col-12 padd0">
                                                                <h3 class="MuliBold">Shave Plan</h3>
                                                                <p class="fs-18 Muli"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                                    <div class="row mr-0 ml-0">
                                                        <div class="col-12 padd0">
                                                            <label class="col-12 padd0">TOTAL <span class="pull-right" id="c-nsubtotal">
                                                                    </span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                                    <div class="row mr-0 ml-0">
                                                        <div class="col-12 padd0">
                                                            <label class="col-12 padd0"><b>Deliver every
                                                                    months<b></label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div> -->

                                        @if($loginStatus === "post-login")
                                        <div class="col-12 text-center pt-4 pl-0 pr-0">

                                        </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(strtolower($currentCountryIso) == 'kr')
            <div id="form-pay" class="hidden">
                {{ csrf_field() }}
                <div id="error_msg_alacarte" class="error MuliPlain padd20 pr-0 pt-0"></div>
                <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
            </div>
            @else
            <div id="form-pay" class="hidden">
                {{ csrf_field() }}
                <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
            </div>
            @endif

        </div>
        <div class="panel panel-default d-lg-none">
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-12 pt-2 pb-3">
                        <div class="col-12 rounded-t-10 bg-white shadow-sm padd15">
                            <div class="row mr-0 ml-0">
                                <h4 class="MuliExtraBold">@lang('website_contents.checkout.payment_summary.summary')</h4>
                                <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                                    <div class="row">
                                        <div class="col-5 padd0 d-flex align-items-center">
                                            <img class="img-fluid" src="{{asset('/images/common/img-getstarted.png')}}" />
                                        </div>
                                        <div class="col-7 padd0 d-flex align-items-center">
                                            <div class="col-12 padd0">
                                                <h3 class="MuliBold">@lang('website_contents.products.product.ask')</h3>
                                                <p class="fs-18 Muli">{{$productname}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                                    <div class="row mr-0 ml-0">
                                        <div class="col-6 padd0">
                                            <p>@lang('website_contents.checkout.payment_summary.subtotal')</p>
                                            <p>@lang('website_contents.checkout.payment_summary.shipping')</p>
                                            <p>@lang('website_contents.checkout.payment_summary.discount')</p>
                                        </div>
                                        <div class="col-6 padd0 text-right text-orng">
                                    <!-- Currency and price for kr -->
                                        <?php
                               
                               $discount = '0.00';
                               $c_price = $current_price;
                               $s_fee = $shipping_fee;
                               if(strtolower($currentCountryIso) == 'kr'){
                                   $c_price = number_format($current_price);
                                   $s_fee= number_format($shipping_fee);
                                   $discount = '0';
                               }

                               ?>
                                  @if(strtolower($currentCountryIso) == 'kr')
                                  <p><span> {{$c_price}}</span>{{$currency}}</p>
                                  <p>{{$s_fee}}{{$currency}}</p>
                                  <p><span>{{$discount}}</span>{{$currency}}</p>
                                  @else
                                  <p>{{$currency}} <span> {{$current_price}}</span></p>
                                  <p>{{$currency}} {{$shipping_fee}}</p>
                                  <p>{{$currency}} <span>0.00</span></p>
                                  @endif
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 padd20 pl-0 pr-0">
                                    <div class="row mr-0 ml-0">
                                        <div class="col-7 padd0">
                                            <p id="c-tax" class="d-none">{{$taxAmount}}</span>
                                                <p class="fs-18">@lang('website_contents.checkout.payment_summary.promo_code')</p>
                                                <div class="form-group">
                                                    <label>@lang('website_contents.checkout.payment_summary.enter-promo_code')</label>
                                                    <input type="text" class="fs-20 form-control" placeholder="123456" />
                                                </div>
                                        </div>
                                        <div class="col-5 padd0">
                                            <button class="btn btn-load-more" style="width: 100%; position: absolute; bottom: 20px;"><b>@lang('website_contents.checkout.payment_summary.apply')</b></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 bg-dark shadow-sm padd15 text-white">
                            <div class="row mr-0 ml-0">
                                <div class="col-6 padd0">
                                    <p class="MuliExtraBold fs-18 mb-0">@lang('website_contents.checkout.payment_summary.today_total')</p>
                                </div>
                                <div class="col-6 padd0 text-right">
                                     <!-- Currency and price for kr -->
                                @if(strtolower($currentCountryIso) == 'kr')
                                <p class="MuliExtraBold fs-18 mb-0"><span>{{number_format($current_totalprice) }}</span>{{$currency}}</p>
                                @else
                                <p class="MuliExtraBold fs-18 mb-0">{{$currency}} <span>{{number_format($current_totalprice, 2, '.', '') }}</span></p>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End: Mobile Payment Summary -->

        </div>
    </div>
    <!-- End: Mobile Layout -->

    <!--Proceed!-->
    <div class="col-12 padd0 d-lg-none">
        <div class="col text-center pt-lg-5 pl-0 pr-0">
            <button id="button-next" class="button-proceed-checkout MuliExtraBold" type="submit">@lang('website_contents.checkout.user.m_next_btn')</button>
        </div>
        <form id="form-checkout" class="d-none" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.thankyou', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
            {{ csrf_field() }}
            <div class="col text-center padd0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                <button id="proceed-pay" class="button-proceed-checkout" type="submit">@lang('website_contents.checkout.payment_summary.proceed_checkout')</button>
            </div>
        </form>
        <br>
        <button class="button-proceed-checkout" id="check" style="display:none">CHECK</button>
        <button class="button-proceed-checkout" id="clear" style="display:none">CLEAR SESSION</button>
    </div>

    <!-- Payment Failed Popup -->
    <div class="modal fade" id="notification_payment_failed" tabindex="-1" role="dialog" aria-labelledby="notification_payment_failed" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="col-12 text-center">
                        <h5 id="notification_payment_failed_text"></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- mask selection -->
        <div class="modal fade" id="model-mask-selection" tabindex="-1" role="dialog" aria-labelledby="model-mask-selection" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered model-mask-selection" role="document">
            <div class="modal-content">
            <div class="modal-header" style="border: none ;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                <div class="modal-body pl-5 pr-5">
                    <div class="col-12 text-center">
                        <!-- <h5 id="notification_payment_failed_text">{{$countmaskqty}}</h5> -->
                        <h4 class="MuliExtraBold">Stay Safe</h4>
                        <h6 class="MuliPlain pt-2">In the midst of this Covid 19 season, your health and wellness is always one of our main priorities. As a token of thanks, we are offering you a <b>FREE</b> complimentary coloured face mask with your purchase.</h6>
                        <h6 class="MuliPlain">Kindly select a face mask colour below:</h6>
                    <div class="row">
                        <div class="col-md-6">
                          <img id="selection-mask-img" src="" class="img img-responsive img-scale img-mask-selection" style="width:50%;"/>
                          <ul>
                          <li class="MuliPlain" style="text-align:left;">Reuse by simple cleaning</li>
                          <li class="MuliPlain" style="text-align:left;">Anti-bacterial raw materials</li>
                          <li class="MuliPlain" style="text-align:left;">Deodorization effect</li>
                          <li class="MuliPlain" style="text-align:left;">3D design for comfortable fit</li>
                          <li class="MuliPlain" style="text-align:left;">Dust-proof function</li>
                        </ul>
                        </div>
                        <div class="col-md-6">
                        <div class="col-md-12 pt-2 pr-0 pl-0 pb-3">
                        <!-- <h4><b>@lang('website_contents.selection.select_blade')</b></h4> -->
                        <div class="col-md-12 pr-0 pl-0 pt-2">
                        @if($maskselection)
                            @foreach($maskselection as $ms)
                            <div class="p-2">
                            @php($pname = str_replace(array( '(', ')' ), '', $ms['name']))
                            @php($pname = str_replace("Face Mask","",$pname))
          
                                @if($ms['qty'] <= 0)
                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected-no-stock">
                                    <div class="col-md-12 pr-0 pl-0">
                                        <h6 style="margin:0;" ><b>{{$pname}} (out of stock)</b></h6>
                                    </div>
                                </div>
                                @else
                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "maskselection-" . $ms['pcid']  ?>">
                                    <div class="col-md-12 pr-0 pl-0">
                                        <h5 style="margin:0;" ><b>{{$pname}}</b></h5>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @endforeach
                        @endif
                        </div>
                      </div>
                      </div>
                <div class="modal-footer-btn col-12" style="padding-top:5%;">
                <button onclick="submitWithoutMask();" class="button-proceed-checkout-cancel MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 99%; margin-left:4%;">@lang('website_contents.selection.change-to-annual-model.cancel')</button>
                <button id="button-proceed-checkout" onclick="submitWithMask();" class="button-proceed-checkout MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 99%; margin-left:4%;">Confirm</button>
                </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- mask selection -->

</div>

<script>
       let country_id = "<?php echo strtolower($currentCountryIso); ?>";
    let countryid = {!! json_encode($currentCountryid)!!};
    let langCode = {!!json_encode($langCode)!!};
    let urllangCode = {!! json_encode($urllangCode)!!};
    let user_id = {!!json_encode($user_id)!!} !== null ? "<?php echo $user_id; ?>" : null;
    let user = {!!json_encode($user)!!} !== null ? {!!json_encode($user)!!} : null;
    let delivery_address = {!!json_encode($delivery_address)!!} !== null ? {!!json_encode($delivery_address)!!} : null;
    let billing_address = {!!json_encode($billing_address)!!} !== null ? {!!json_encode($billing_address)!!} : null;
    let default_card = {!!json_encode($default_card)!!} !== null ? {!!json_encode($default_card)!!} : null;
    let session_data = {!!json_encode($session)!!};
    let payment_intent = {!!json_encode($payment_intent)!!} !== null ? {!!json_encode($payment_intent)!!} : null;
    let checkout_details = {!!json_encode($checkout_details)!!} !== null ? {!!json_encode($checkout_details)!!} : null;
    let current_price = {!!json_encode($current_totalprice)!!};
    let next_price = {!!json_encode($next_totalprice)!!};
    let shipping_fee = {!!json_encode($shipping_fee)!!};
    let taxRate  = <?php echo $taxRate; ?>;
    let ctaxAmount  = <?php echo $taxAmount; ?>;
    let cards = {!!json_encode($cards) !!};
    let loginStatus = {!! json_encode($loginStatus) !!};
    let current_country_from_session = {!!json_encode($currentCountry)!!};
    // mask selection
    let maskselection = {!! json_encode($maskselection) !!};
    let countmaskqty = <?php echo $countmaskqty; ?>;
    // mask selection
    let loginvalidation = "@lang('website_contents.checkout.content.login_validation')";
    let promotionFreeproduct = "@lang('website_contents.checkout.free-product')";
    let sessionLoginError  = <?php echo "'".$sessionLoginError."'"; ?>;
</script>

<script>
    let formMode = "register"

    $(function() {
        // Display error message if any
        DisplayPaymentError();
        if(sessionLoginError){
        $("#password_error").html(sessionLoginError);
        onChangeLoginOrRegisterForm();
        }
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        if (loginStatus === "post-login") {
            // Show orange tick
            // $(".orange-tick-personal").removeClass("hidden");

            // $(".orange-tick-address").removeClass("hidden");

            if (cards.length > 0) {
                $("#edit-card-container").removeClass("hidden");
                let cardOptionList = [];
                cards.forEach(card => {

                    var selected = '';
                    if (card.isDefault == 1) {
                        selected = "selected";
                        $("#view-expiry-date").val(card.expiredMonth + " / " + card.expiredYear);
                        $("#view-cvv").val(". . .");
                        ChangeCardBackground(card.branchName);
                    }
                    cardOptionList.push("<option value='" + JSON.stringify(card) + "' " + selected + ">xxxx xxxx xxxx " + card.cardNumber + "</option>");
                });
                $("#card-dropdown").html(cardOptionList.join());
            } else {
                $("#add-card-container").removeClass("hidden");
            }
        }


        $("#card-dropdown").change(function() {
            var selectedCard = JSON.parse(this.value);
            $("#view-expiry-date").val(selectedCard.expiredMonth + " / " + selectedCard.expiredYear);
            onUpdateCardSelection(selectedCard.id);
        });

        $('#btn-create-account-desktop').click(function() {
            validateRegistration("form_register");
            onLoginOrRegister();
        });

        $('.btn-sign-in').click(function() {
            onChangeLoginOrRegisterForm();
        })

        $('.btn-create-one').click(function() {
            onChangeLoginOrRegisterForm();
        })

        $('#card-number-masked').keydown(function (event) {
            $(this).val(CreditCardFormat($('#card-number').val()));
        });

        $('#card-number-masked').on('input',function (event) {
            $('#card-number').val(CreditCardFormat($(this).val()));
            $(this).val(CreditCardFormatMasked($('#card-number').val()));
        });

        $('#card-number-masked').blur(function (event) {
            // deselectd control
            $(this).val(CreditCardMaskAll($(this).val()));
        });

        $('.card_expiry_date').keyup(function() {
            $(this).val(ExpiryDateFormat($(this).val()));
        })

        $(".number-only").on('input', function(value) {
            if ((value.which < 48 || value.which > 57) && (value.which !== 8) && (value.which !== 0)) {
                return false;
            }
            return true;
        });

        $("#button-add-new-card").click(function() {
            $("#edit-card-container").addClass("hidden");
            $("#add-card-container").removeClass("hidden");
            $("#button-cancel-add-new-card").removeClass("hidden");
        });

        $("#button-cancel-add-new-card").click(function() {
            $("#edit-card-container").removeClass("hidden");
            $("#add-card-container").addClass("hidden");
            $("#button-cancel-add-new-card").addClass("hidden");
        });
    })

    // async function DisplayPaymentError() {
    //     await session(SESSION_CHECKOUT_TRIAL_PLAN, SESSION_GET, null).then(data => {
    //         if (data) {
    //             let dataParsed = JSON.parse(data);
    //             if (dataParsed.payment_error) {
    //                 $("#notification_payment_failed_text").html(trans('website_contents.global.stripe_payment_fail'));
    //                 $("#notification_payment_failed").modal("show");
    //             }
    //         }
    //     });
    // }

    function onChangeLoginOrRegisterForm() {
        if (formMode === "register") {
            formMode = "login";
            $(".title-create-account").html(trans('website_contents.checkout.user.sign_in'));
            $(".btn-create-account").html(trans('website_contents.checkout.user.sign_in'));
            $('.text-sign-in').addClass("hidden");
            $('.text-create-one').removeClass("hidden");
            $(".register-group").addClass("hidden");
            $(".name").removeAttr('required');
            $(".day-of-birth").removeAttr('required');
            $(".month-of-birth").removeAttr('required');
            $(".year-of-birth").removeAttr('required');
            $(".error_password").html("");
        } else {
            formMode = "register"
            $(".title-create-account").html(trans('website_contents.checkout.user.create_account'));
            $(".btn-create-account").html(trans('website_contents.checkout.user.create_account'));
            $('.text-sign-in').removeClass("hidden");
            $('.text-create-one').addClass("hidden");
            $(".register-group").removeClass("hidden");
            $(".name").attr("required", true);
            $(".day-of-birth").attr("required", true);
            $(".month-of-birth").attr("required", true);
            $(".year-of-birth").attr("required", true);
            $(".error_password").html("");
        }
    }

    function onLoginOrRegister() {
        let email = $("#d-email-tk").val();
        let password = $("#d-password-tk").val();
        let name = $("#d-name-tk").val();
        let dob = $("#d-day-of-birth-tk").val();
        let mob = $("#d-month-of-birth-tk").val();
        let yob = $("#d-year-of-birth-tk").val();
        let birthday = yob + "-" + mob + "-" + dob;
        birthday = birthday.replace(/\s/g, '');
        if ($('#form_register').valid() === true) {
            if (formMode === "register") {
                if (email && password && email.includes("@") && name && dob && mob && yob && birthday) {
                    RegisterCustomer(email, password, name, birthday, formMode);
                }
            } else {
                if (email && password && email.includes("@")) {
                    // console.log(email);
                    // console.log(password);
                    LoginCustomer(email, password, formMode);
                }
            }
        }
    }

    function RegisterCustomer(email, password, name, birthday, type) {

        let url = window.location.origin + GLOBAL_URL_V2 + '/api/check/email';
        let method = "POST";
        let data = {
            password: password,
            email: email,
            type: type
        };

        $("#loading").css("display", "block");
        AJAX(url, method, data).done(response => {
            if (type === "register") {
                if (response["user_data"] === null) {
                    let url = window.location.origin + GLOBAL_URL_V2 + '/api/user/register';
                    let method = "POST";
                    let data = {
                        password: password,
                        email: email,
                        name: name,
                        birthday: birthday,
                        countryid: countryid,
                        langCode: langCode.toUpperCase()
                    };

                    AJAX(url, method, data).done(function(response) {
                        location.reload();
                    })
                } else {
                    $("#loading").css("display", "none");
                    let exists = response.user_data !== null && response.isActive !== null ? true : false;
                    let isActive = response.user_data !== null && response.isActive === 1 ? true : false;
                    if (exists && isActive) {
                        location.reload();
                    } else if (exists && !isActive) {
                        // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                        $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                    } else if (exists && isActive) {
                        $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                    }
                    // $(".error_password").html("Email already exist");
                }
            }
        })
    }

    function LoginCustomer(email, password, type) {

        let url = window.location.origin + GLOBAL_URL_V2 + '/api/check/email';
        let method = "POST";
        let data = {
            password: password,
            email: email,
            type: type
        };

        $("#loading").css("display", "block");
        AJAX(url, method, data).done(response => {
            if (type === "login") {
                let exists = response.user_data !== null && response.isActive !== null ? true : false;
                let isActive = response.user_data !== null && response.isActive === 1 ? true : false;
                if (exists && isActive) {
                    location.reload();
                } else if (exists && !isActive) {
                    // // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                    $("#email_error").html('You need to be active to login.')
                } else if (!exists && !isActive) {
                    $("#email_error").html('The email does not exist.')
                }
                $("#loading").css("display", "none");
            }
        })
    }

    //CREDIT CARD INPUT MASK
    function CreditCardFormatMasked(value) {
        var v_masked = value.replace(/\s/g, '').replace(/.(?!$)/gi, '•');
        var matches_masked = v_masked.match(/.{4,16}/g);
        var match_masked = matches_masked && matches_masked[0] || '';
        var parts_masked = [];

        for (i=0, len=match_masked.length; i<len; i+=4) {
            parts_masked.push(match_masked.substring(i, i+4));
        }

        if (parts_masked.length) {
            return parts_masked.join(' ');
        } else {
            return v_masked;
        }
    }

    function CreditCardMaskAll(value) {
        var v_masked = value.replace(/\s/g, '').replace(/./gi, '•');
        var matches_masked = v_masked.match(/.{4,16}/g);
        var match_masked = matches_masked && matches_masked[0] || '';
        var parts_masked = [];

        for (i=0, len=match_masked.length; i<len; i+=4) {
            parts_masked.push(match_masked.substring(i, i+4));
        }

        if (parts_masked.length) {
            return parts_masked.join(' ');
        } else {
            return v_masked;
        }
    }

    function CreditCardFormat(value) {
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

        var matches = v.match(/.{4,16}/g);
        var match = matches && matches[0] || '';
        var parts = [];

        for (i=0, len=match.length; i<len; i+=4) {
            parts.push(match.substring(i, i+4));
        }

        if (parts.length) {
            return parts.join('');
        } else {
            return v;
        }
    }

    function ExpiryDateFormat(value) {
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
        var matches = v.match(/\d{2,4}/g);
        var match = matches && matches[0] || ''
        var parts = []

        for (i = 0, len = match.length; i < len; i += 2) {
            parts.push(match.substring(i, i + 2))
        }

        if (parts.length) {
            return parts.join(' / ')
        } else {
            return value
        }
    }

    function UpdateCardSelectionList(newCard) {
        let cardOptionList = [];
        cards.push(newCard);
        cards.forEach(card => {
            var selected = '';
            if (card.id === newCard.id) {
                selected = "selected";
                $("#view-expiry-date").val(card.expiredMonth + " / " + card.expiredYear);
                $("#view-cvv").val(". . .");
            }
            cardOptionList.push("<option value='" + JSON.stringify(card) + "' " + selected + ">xxxx xxxx xxxx " + card.cardNumber + "</option>");
        });

        $("#card-dropdown").html(cardOptionList.join());
    }

    function ChangeCardBackground(branchName) {
        if (branchName === "Visa") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/visa_card.png')");
        } else if (branchName === "MasterCard") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/mastercard_card.png')");
        } else if (branchName === "American Express") {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/amex_card.png')");
        } else {
            $(".credit-card-bg").css("background-image", "url('" + GLOBAL_URL_V2 + "/images/cards/visa_card.png')");
        }
    }
</script>

<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script src="{{ asset('js/functions/alacarte/ask/ask-checkout.function.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>

@endsection
