<div id="shipping-address-show" hidden>
    <input type="hidden" id="delivery_address_id" name="delivery_address_id" value="" />
    <input type="hidden" id="billing_address_id" name="billing_address_id" value="" />
</div>

<div id="shipping-address-add" class="pl-0 pt-0 MuliPlain">
    <h4 class="color-orange MuliBold d-none d-sm-block d-md-none">주소</h4>
    <form id="add-addresses-block" method="POST" action="">
        @csrf
        <input type="hidden" name="product_country_id" value="">
        <div id="shipping-address-block">
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Full Name">받으시는 분 성함</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_name" name="delivery_name">
                <div id="pd_name_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">연락처</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="add_delivery_phoneext" name="delivery_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold" id="add_delivery_phone" name="delivery_phone" class="">
                </div>
                <div id="pd_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanDeliveryAddressPrefill('add')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-add-delivery" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">상세주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_address1" name="delivery_address1" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="add_delivery_address" name="delivery_address" readonly="readonly">
                <div id="pd_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Flat">단지, 동, 호</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_flat" name="delivery_flat" onchange="onChangeDeliveryAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">도시</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_city" name="delivery_city" readonly="readonly">
                <div id="pd_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">군/구</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_state" name="delivery_state" readonly="readonly">
                <div id="pd_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">우편주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_postcode" name="delivery_postcode" readonly="readonly">
                <div id="pd_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_add col-12">
            <div class="row">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox form-control" type="checkbox" id="enableBillingEditCheckBox_for_add" name="isDifferentBilling" onclick="enableBilling()">
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">주문자 주소 별도 지정하기</h5>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_add" style="display:none;">
            <div class="col-12 padd0 form-group pt-3">
                <b>주문자 주소</b>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">연락처</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="edit_billing_phoneext" name="billing_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold number-only" id="edit_billing_phone" name="billing_phone">
                </div>
                <div id="pb_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanBillingAddressPrefill('add')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-add-billing" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">상세주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_address1" name="billing_address1" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="add_billing_address" name="billing_address" readonly="readonly">
                <div id="pb_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Flat">단지, 동, 호</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_flat" name="billing_flat"
                    onchange="onChangeBillingAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">도시</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_city" name="billing_city" readonly="readonly">
                <div id="pb_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">군/구</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_state" name="billing_state" readonly="readonly">
                <div id="pb_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>

            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">우편주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_postcode" name="billing_postcode" readonly="readonly">
                <div id="pb_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <button id="btn-address-add" class="btn btn-load-more col-md-12 col-lg-6 uppercase mt-3" style="font-size: 12px" type="submit">주소 저장하기</button>
    </form>
</div>

<div id="shipping-address-edit" class="pl-0 pt-0 MuliPlain" style="display:none;">
    <h4 class="color-orange MuliBold d-none d-sm-block d-md-none">주소</h4>
    <form id="edit-addresses-block" method="POST" action="">
        <input type="hidden" name="product_country_id" value="">
        <div id="shipping-address-block">
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Full Name">받으시는 분 성함</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_name" name="delivery_name" value="{{  $delivery_address ? $delivery_address[0]["fullName"] : '' }}">
                <div id="pd_name_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0" for="phone">연락처</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_phone" name="delivery_phone" value="{{  $delivery_address ? $delivery_address[0]["contactNumber"] : ''}}">
                <div id="pd_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanDeliveryAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-delivery" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0" for="address">상세주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_address1" name="delivery_address1" value="{{ $delivery_address ? $delivery_address[0]["address"] : ''}}" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="edit_delivery_address" name="delivery_address" value="{{ $delivery_address ? $delivery_address[0]["address"] : ''}}" readonly="readonly">
                <div id="pd_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Flat">단지, 동, 호</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_flat" name="delivery_flat" onchange="onChangeDeliveryAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">도시</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_city" name="delivery_city" value="{{  $delivery_address ? $delivery_address[0]["city"] : ''}}" readonly="readonly">
                <div id="pd_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group">
                <label class="col-12 padd0 color-grey" for="state">군/구</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_state" name="delivery_state" value="{{  $delivery_address ? $delivery_address[0]["city"] : ''}}" readonly="readonly">
                <div id="pd_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>


            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">우편주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_postcode" name="delivery_postcode" value="{{  $delivery_address ? $delivery_address[0]["portalCode"]: '' }}">
                <div id="pd_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>

        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_edit col-12">
            <div class="row">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox" type="checkbox" id="enableBillingEditCheckBox_for_edit" name="isDifferentBilling" onclick="enableBilling()">
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">주문자 주소 별도 지정하기</h5>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_edit" style="display:none;">
            <div class="col-12 padd0 form-group pt-3">
                <b>주문자 주소</b>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">연락처</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_phone" name="billing_phone">
                <div id="pb_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <a href="javascript:;" onclick="koreanBillingAddressPrefill('edit')" class="text-center text-danger" style="color: #0275d8; text-decoration: underline; cursor: pointer;">
                우편번호검색</a>
            <div id="daum-postcode-wrap-edit-billing" style="display:none;border:1px solid;margin:5px 0;position:relative">
                <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">상세주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_address1" name="billing_address" readonly="readonly">
                <input type="hidden" class="col-12 hide-input-template" id="edit_billing_address" name="billing_address" value="{{  $delivery_address ? $billing_address[0]["address"] : ''}}" readonly="readonly">
                <div id="pb_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Flat">단지, 동, 호</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_flat" name="billing_flat" onchange="onChangeBillingAddressUnitNumber(this.value)">
            </div>
            <div class="col-12 pl-0 form-group">
                <label class="col-12 padd0 color-grey" for="city">도시</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_city" name="billing_city" readonly="readonly">
                <div id="pb_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">군/구</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_state" name="billing_state" readonly="readonly">
                <div id="pb_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group">
                <label class="col-12 padd0 color-grey" for="postcode">우편주소</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_postcode" name="billing_postcode" readonly="readonly">
                <div id="pb_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>


        </div>
        <button class="btn btn-load-more col-6 uppercase mt-3" style="font-size: 12px" type="submit"><b>주소 저장하기</b></button>
    </form>
</div>
