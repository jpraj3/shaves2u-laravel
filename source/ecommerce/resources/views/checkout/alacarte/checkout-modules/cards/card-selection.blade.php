<style>
    .individual-card-selection:hover {
        background: green;
        color: white;
    }
</style>
@if(count($cards) > 0)
<div id="card_selection" class="panel-body">
    <div class="rounded-10 bg-white padd20" id="appendCardList">
        @if(count($cards) > 0)
        @foreach($cards as $card)
        <div class="container col-12 individual-card-selection"
            style="border: 2px solid black; margin:1%;padding:10px;cursor: pointer;"
            onClick="onUpdateCardSelection({{$card->id}})">
            <div class="form-group">
                <label for="">Card Name</label>
                <p>{{$card->cardName}}</p>
            </div>
            <div class="form-group">
                <label for="">Card Number</label>
                <p>XXXX XXXX XXXX {{$card->cardNumber}}</p>
            </div>
            <div class="form-group">
                <label for="">Card Expiry</label>
                <p>{{$card->expiredMonth}}/{{$card->expiredYear}}</p>
            </div>
        </div>
        <br> <br>
        @endforeach
        @else
        @include('checkout.alacarte.checkout-modules.cards.card-add')
        @endif
        <div class="form-group" id="add-card" style="text-align:center;">
            <button class="btn btn-primary" onClick="CardBack()">Back</button>
            <button type="submit" class="btn btn-primary" onClick="selectionAddCard()">Add Another Card</button>
        </div>
    </div>
</div>
@else
@include('checkout.alacarte.checkout-modules.cards.card-add')
@endif