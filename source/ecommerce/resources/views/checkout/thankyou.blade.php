@extends('layouts.app')
@section('content')
<style>
    #thankYouPage .page-inner {
  max-width: 975px;
  margin: auto;
}

#thankYouPage .page-inner .text-heading {
  margin: 90px 0;
}

#thankYouPage .page-inner .text-heading .ico-check {
  background-image: url("https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/assets/images/tick-128.png");
  background-repeat: no-repeat;
  background-position: center left;
  width: 30px;
  height: 30px;
  display: inline-block;
  margin-right: 20px;
}

#thankYouPage .page-inner .cart-container {
  border: 1px solid #d7d7d7;
  padding: 35px 40px;
}

#thankYouPage .page-inner .cart-list {
  position: relative;
}

#thankYouPage .page-inner .cart-list .heading {
  margin-bottom: 15px;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .row-custom {
  display: flex;
  width: 100%;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .product-name {
  font-size: 18px;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .product-description {
  font-size: 16px;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .item-info {
  width: 47%;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .item-price {
  width: 19%;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .item-quantity {
  width: 24%;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .item-total {
  width: 10%;
  text-align: right;
}

#thankYouPage .page-inner .cart-list .cart-list-contain .item-total .text-contain {
  float: right;
}

#thankYouPage .page-inner .cart-list .image-contain {
  width: 175px;
  max-width: 44%;
}

#thankYouPage .page-inner .cart-list .image-contain .image-inner .image {
  height: 175px;
}

#thankYouPage .page-inner .cart-list .text-contain {
  max-width: 55%;
  padding-left: 25px;
  text-align: left;
}

#thankYouPage .page-inner .cart-list .text-contain .text-inner {
  height: 175px;
}

#thankYouPage .page-inner .cart-list .item {
  border-bottom: 1px solid #CCC;
  padding: 20px 0;
}

#thankYouPage .page-inner .cart-list .box-body .item {
  padding: 40px 0;
}

#thankYouPage .page-inner .cart-list .note {
  font-family: 'Muli Bold';
  padding: 0 70px;
}

#thankYouPage .page-inner .cart-list .note .note-1 {
  font-size: 16px;
}

#thankYouPage .page-inner .cart-list .note .note-2 {
  font-size: 14px;
  color: #aaacaf;
  background: url("https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/assets/images/icon-shield.png") no-repeat top left;
  min-height: 35px;
  padding-left: 40px;
}

#thankYouPage .page-inner .order-detail .card-custom {
  max-height: 256px;
}

#thankYouPage .page-inner .order-detail .card-custom .text-content {
  padding: 35px 45px;
}

#thankYouPage .page-inner .order-detail .card-custom .text-contain {
  display: block;
}

#thankYouPage .page-inner .order-detail .card-custom .text-contain .text-inner {
  height: 160px;
}

#thankYouPage .page-inner .order-detail .card-custom img {
  max-height: 256px;
}

    #thankYouPage {
        max-width: 800px;
        text-align: center;
        min-height: calc(100vh - 81px - 207px);
        display: flex;
        justify-content: center;

        .page-inner {
            margin: auto;

            .box-header {
                display: block;
                margin-bottom: 40px;
            }

            .cart-container {
                padding: 35px 0;

                h2 {
                    color: #000;
                    letter-spacing: 2px;
                    font-size: 40px;
                }
            }

            .box-body {
                .order-number {
                    font-weight: bold;
                    font-size: 13px;
                    letter-spacing: 1px;

                    .code {
                        color: $primary-color;
                    }
                }

                .estimated-delivery {
                    white-space: pre-line;
                    font-size: 14px;
                }
            }
        }
    }
</style>
<!--
<div id="thankYouPage" class="container-fluid" *ngIf="orderInfo">
    <div class="page-inner">
        <div class="box cart-container">
            <div class="box-header box-flex">
                <h2 class="text-center text-uppercase">Thank you!</h2>
            </div>
            <div class="box-body">
                <p class="order-number text-uppercase" >your order number is {{$orderid}}</p>

                <p class="estimated-delivery" >The estimated delivery date is <strong>{{$deliveryDateFrom}}–{{$deliveryDateTo}}</strong>.</br>Email us at <a href="mailto:help.my@shaves2u.com">help.my@shaves2u.com</a> with any questions or suggestions.</p>
            </div>
        </div>
    </div>
</div>-->


<div id="thankYouPage" class="container-fluid pb-5" *ngIf="orderInfo">
  <div class="page-inner">
    <div class="row">
        <div class="col-12 text-center pr-0 pl-0">
            <h1 class="mt-5 MuliExtraBold trial-kit-title" style="font-size: 2rem;">
                @lang('website_contents.thankyou.thank_you')
            </h1>
        </div>
        <div class="col-12 text-center mt-4">
            <p class="fs-20 Muli mb-0">@lang('website_contents.thankyou.order_number', ['order_number' => $orderid]) </p>
            <p class="color-orange fs-25 MuliBold">#{{$orderid}}</p>
        </div>
        <?php 
          $langCode = view()->shared('langCode');
          $urllang = view()->shared('url');
          $iso = view()->shared('currentCountryIso');
          $url = url("/".$urllang."-".$iso) ;
          ?>
        <div class="col-12 text-center mt-4">
            {{-- <p class="fs-20 Muli estimated-delivery mb-0">@lang('website_contents.thankyou.order_number', ['date1' => date('F d', strtotime($deliveryDateFrom)), 'date2' => date('F d, Y', strtotime($deliveryDateTo)),'order_number' => $orderid])</p> --}}
            @if(strtolower(view()->shared('currentCountryIso')) == 'sg')
            <p class="fs-20 Muli">@lang('website_contents.thankyou.email_us_sg')</p>
            @elseif(strtolower($iso) == 'hk' && strtolower($langCode) == 'en')
            <p class="fs-20 Muli">@lang('website_contents.thankyou.email_us_hk')</p>
            @else
            <p class="fs-20 Muli">@lang('website_contents.thankyou.email_us')</p>
            @endif
        </div>

        <div class="col-12 text-center mt-4">
            <a class="btn btn-start text-uppercase" href="{{ $url }}">@lang('website_contents.thankyou.back_to_home')</a>
        </div>
    </div>
  </div>
</div>
@endsection
