@extends('layouts.app')
<!--Temporary Style Begin-->
<style>
    .panel-heading-selected {
        background-color: black;
    }

    .panel-heading-unselected {
        background-color: white;
    }

    .panel-title-selected {
        color: white;
    }

    .panel-title-unselected {
        color: black;
    }

    .button-back {
        color: tomato;
        background-color: transparent;
        border: 0px;
        font-size: 15px;
        margin-top: 14px;
        margin-bottom: 14px;
    }

    .panel {
        background-color: #ececec !important;
    }

    .panel-collapse {
        background-color: #ececec !important;
    }

    button:focus {
        outline: 0 !important;
    }

    .display-none {
        display: none;
    }

    .box-grey {
        background-color: #ececec;
        padding: 20px;
    }

    input {
        background-color: white;
        padding: 5px;
        width: 100%;
    }

    .button-edit-account {
        background-color: white;
        color: black;
        border-color: tomato;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 10px;
        width: 100%;
    }

    .button-proceed-checkout {
        background-color: tomato;
        color: white;
        border-color: tomato;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 30px;
        width: 100%;
    }
</style>
<!--Temporary Style End-->
@section('content')
<div class="container">
    <button id="button-back" class="button-back">
        < BACK</button> <br>
            <div style="margin: 0px !important; padding: 20px">
                <div class="panel-body">
                    <label style="font-weight: bold">TOTAL SUMMARY</label><br>
                    <div class="box-grey">
                        <br><img alt="image" />
                        <br><label>Trial Kit</label>
                        <br><label>5 Blade Cartridge</label>
                        <hr />
                        <br><label>Subtotal: <span></span></label>
                        <br><label>Shipping: <span></span></label>
                        <br><label>Discount: <span></span></label>
                        <hr />
                        <br><label>Promo Code</label>
                        <br><label>Enter promo code</label>
                        <br><input type="text" />
                        <br><button>APPLY</button>
                        <hr />
                        <br><label>Today Total</label>
                        <br><label>RM 6.50</label>
                    </div>
                    <div id="card-container" style="display:block;">

                        <div id="card-element" style="display:block;">


                        </div>
                    </div>
                    <button id="button-next" class="button-proceed-checkout" type="submit">@lang('website_contents.checkout.content.pay_now')</button>
                </div>
            </div>
</div>
<script>
    let payment_intent_id = "<?php echo $payment_intent_id  ?>";
    var urlParams = new URLSearchParams(window.location.search);
    var type = "trial-plan"
    let return_url = `http://192.168.2.109:8000/en-my/stripe/redirect?type=${type}`;

    // On load
    $(function() {
        $("#button-next").click(function(){
            $.ajax({
                url: "http://192.168.2.109:8000/en-my/stripe/test-otp-payment",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                cache: false,
                data: {
                    "payment_intent_id" : payment_intent_id,
                    "return_url" : return_url
                },
            }).done(function(data){
                // console.log(JSON.stringify(data));

                if(data.status == "requires_source_action") {
                    // If requires otp
                    window.location = data.next_action.redirect_to_url.url;
                } else if (data.status == "succeeded") {
                    // Redirect to post payment redirect with success code
                } else {
                    // Redirect to post payment redirect with failure code
                }
            })
        });
    })
</script>
@endsection
