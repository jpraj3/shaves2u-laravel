@extends('layouts.app')
@section('content')
@php($user = isset($User) ? $User["user"] : null)
@php($user_id = isset($User) ? $User["user"]["id"] : null)
@php($delivery_address = isset($User) ? $User["delivery_address"] : null)
@php($billing_address = isset($User) ? $User["billing_address"] : null)
@php($cards = isset($User) ? $User["cards"] : null)
@php($default_card = isset($User) ? $User["default_card"] : null )
@php($session = $session_data)
@php($checkout_details = isset($checkout_details) ? $checkout_details : null )
<link rel="stylesheet" href="{{ asset('css/trial-plan/trial-plan-checkout.css') }}">
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<div class="container">
    <!-- Hidden Inputs for Checkout -->
    <input type="hidden" name="payment_intent_next_update_type" id="payment_intent_next_update_type" value="first-update" />
    <input type="hidden" name="payment_intent_id" id="payment_intent_id" value="" />
    <!-- END -->

    <div class="col-12">
        <button id="button-back" class="button-back">
            <i class="fa fa-chevron-left"></i> BACK</button> <br>
    </div>
    <div class="row mr-0 ml-0" style="background-color: #eaeaea;"">
        <!-- CHECKOUT  -->
        <div id="checkout-block" class="col-12 col-lg-8 panel-group padd0">
            <!--Account-->
            <div class="col-12 panel panel-default padd0" id="account-section">
                <div class="col-12 step1-heading panel-heading panel-heading-selected d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-" id="account-header" onClick="account()" style="margin: 0px;padding: 0px;height: 100%;">
                    <h5 class="step1-title panel-title panel-title-selected product-group-title" style="padding-top: 15px;padding-bottom: 15px;text-align: center;">Account <i class="fa fa-chevron-down"></i></h5>
                </div>
                <div class="collapse1 col-12 panel-collapse collapse show d-lg-block" style="margin: 0px !important; padding: 20px" id="account-body">
                    <div class="panel-body">
                        <div class="rounded-10 bg-white padd20">
                            <label class="muliBold">Personal Details</label><br>
                            <label>Email</label><br>
                            <p class="col-12 border-bottom"> {{Auth::user()->email}}</p>
                            <label>Password</label><br>
                            <p class="col-12 border-bottom">******</p>
                        </div>
                    </div>
                </div>
            </div>

            <!--Shipping Address-->
            <div class="col-12 panel panel-default padd0" id="shipping-address-section">
                <div class="col-12 step2-heading panel-heading panel-heading-unselected d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-" id="shipping-address-header" onClick="shippingaddress()" style="margin: 0px;padding: 0px;height: 100%;">
                    <h5 class="step2-title panel-title panel-title-unselected product-group-title" style="padding-top: 15px;padding-bottom: 15px;text-align: center;">Shipping Address <i class="fa fa-chevron-down"></i></h5>
                </div>

                <div id="shipping-address-body" class="collapse2 col-12 panel-collapse d-lg-block" style="margin: 0px !important; padding: 20px">
                    <div class="panel-body">
                        <div class="rounded-10 bg-white padd20">
                            @if(count($delivery_address) > 0)
                                @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-edit')
                                @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-edit')
                                @endif
                            @else
                                @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-add')
                                @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-add')
                                @endif
                            @endif
                            <div class="col-12 pt-4 pl-0 pr-0">
                                @if(count($delivery_address) > 0)
                                <div class="row mx-0">
                                    <div class="col-6 pl-0">
                                        <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">EDIT ADDRESS</button>
                                    </div>
                                    <div class="col-6 pr-0">
                                        <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">ADD ADDRESS</button>
                                    </div>
                                </div>
                                @else
                                <div class="row mx-0">
                                    <div class="col-6 padd0">
                                        <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">ADD ADDRESS</button>
                                    </div>
                                    <div class="col-6 padd0">
                                        <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()" style="display:none;">EDIT ADDRESS</button>
                                    </div>
                                </div>
                                @endif
                                <div class="col-6 padd0">
                                    <button id="cancel-address-edit" class="btn btn-load-more col-12" onclick="cancelEditAddress()" style="display:none;">Cancel</button>
                                    <label class="pt-4" id="estimated-delivery-date" style="font-weight: bold; font-size: 13px">Estimated delivery {{Carbon\Carbon::now()->addDays(7)->format('M d')}} - {{Carbon\Carbon::now()->addDays(14)->format('M d')}}</label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Payment-->
            <div class="col-12 panel panel-default padd0" id="payment-section" onClick="payment()">
                <div id="payment-header" class="col-12 step3-heading panel-heading panel-heading-unselected d-lg-none" data-toggle="collapse" data-parent="#accordion" href="#collapse-">
                    <h5 class="step3-title product-group-title panel-title panel-title-unselected" style="padding-top: 15px;padding-bottom: 15px;text-align: center;">
                        Payment <i class="fa fa-chevron-down"></i>
                    </h5>
                </div>
                <div id="payment-body" class="collapse3 col-12 panel-collapse collapse d-lg-block" style="margin: 0px !important; padding: 20px">
                    @if(strtolower($currentCountryIso) == 'kr')
                    @include('checkout.subscriptions.checkout-modules.payment-kr')
                    @else
                    @include('checkout.subscriptions.checkout-modules.payment')
                    @endif
                </div>
                    <br>
                    @if(strtolower($currentCountryIso) == 'kr')
                    <div id="form-pay-kr" class="display-none">
                        {{ csrf_field() }}
                        <button id="proceed-pay-kr" class="button-proceed-checkout">PROCEED TO PAY</button>
                    </div>
                    @else
                    <div id="form-pay">
                        {{ csrf_field() }}
                        <button class="button-proceed-checkout">PROCEED TO PAY</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- SUMMARY -->
        <!-- // Promotion -->
        <div id="summary-block" class="col-12 col-lg-4 panel-group padd0">

            <div id="summary-body" class="collapse4 col-12 panel-collapse collapse d-lg-block" style="margin: 0px !important; padding: 20px">
                <div class="panel-body">
                    <div class="rounded-t-10 bg-white padd20">
                        <h4 class="MuliExtraBold">Summary</h4>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                            <div class="row mr-0 ml-0">
                                <div class="col-5 padd0 d-flex align-items-center">
                                    <img class="img-fluid d-none d-lg-block" src="{{asset('/images/common/customplan.png')}}"/>
                                    <img class="img-fluid d-lg-none" src="{{asset('/images/common/img-get-started.png')}}"/>
                                </div>
                                <div class="col-6 offset-1 padd0 d-flex align-items-center">
                                    <div class="col-12 padd0">
                                        <h3>Trial Plan</h3>
                                        <p>{{$plandescription}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p id="c-free-product"></p>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                            <div class="row mr-0 ml-0">
                                <div class="col-12 padd0">
                                    <label class="col-12 padd0">Subtotal <span class="pull-right" id="c-subtotal">{{$current_price}}</span></label>
                                    <label class="col-12 padd0">Next Month Subtotal <span class="pull-right" id="c-nsubtotal">{{$next_price}}</span></label>
                                    <label class="col-12 padd0">Shipping <span class="pull-right" id="c-shipping">{{$shipping_fee}}</span></label>
                                    <label class="col-12 padd0">Discount <span class="pull-right" id="c-discount"></span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 padd20 pl-0 pr-0">
                            <div class="row mr-0 ml-0">
                                <div class="col-7 padd0">
                                    <p class="fs-18">Promo Code</p>
                                    <div class="form-group">
                                        <label>Enter promo code</label>
                                        <input id="promo_code" class="fs-20 form-control" type="text" />
                                    </div>
                                </div>
                                <div class="col-5 padd0">
                                    <button class="btn btn-load-more" style="position: absolute; bottom: 20px;" onclick="applyPromotion()">APPLY</button>
                                </div>
                                <br><p class="error-promotion" id="error-promotion"></p>
                                <p class="success-promotion" id="success-promotion"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 bg-dark shadow-sm padd15 text-white">
                    <div class="row mr-0 ml-0">
                        <div class="col-12 padd0">
                            <label class="col-12 padd0">Today Total <span class="pull-right" id="c-total">{{$current_price}}</span></label>
                        <label class="col-12 padd0">Next Month Total <span class="pull-right" id="c-ntotal">{{$next_price}}</span></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="pay-section-2 d-none d-lg-block">
            @if(strtolower($currentCountryIso) == 'kr')
            <div id="form-pay-kr"  class="padd20 pt-0">
                {{ csrf_field() }}
                <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
            </div>
            @else
            <div id="form-pay"  class="padd20 pt-0">
                {{ csrf_field() }}
                <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
            </div>
            @endif
        </div>
        <!-- SUMMARY -->
        <!-- // Promotion -->
        <div id="summary-block" class="col-4 panel-group">
            <div style="margin: 0px !important; padding: 20px">
                <div class="panel-body">
                    <label style="font-weight: bold">TOTAL SUMMARY</label><br>
                    <div class="box-grey">
                        <br><label>Trial Plan</label>
                        <br><label>{{$plandescription}}</label>
                        <p id="c-free-product"></p>
                        <p id="c-free-exist-product"></p>
                        <hr />
                        <br><label>Subtotal: <span id="c-subtotal">{{$current_price}}</span></label>
                        <br><label>Next Month Subtotal: <span id="c-nsubtotal">{{$next_price}}</span></label>
                        <br><label>Shipping: <span id="c-shipping">{{$shipping_fee}}</span></label>
                        <br><label>Discount: <span id="c-discount"></span></label>
                        <hr />
                        <br><label>Promo Code</label>
                        <br><label>Enter promo code</label>
                        <br><input id="promo_code" type="text" />
                        <br><button onclick="applyPromotion()">APPLY</button>
                        <br><p class="error-promotion" id="error-promotion"></p>
                        <p class="success-promotion" id="success-promotion"></p>
                        <hr />
                        <br><label>Next Tax: <span id="c-tax"></span>{{$taxAmount}}</label>
                        <br><label>Next Month Total: <span id="c-ntotal">{{$next_price}}</span></label>
                        <br><label>Today Total</label>
                        <br><label><span id="c-total">{{$current_price}}</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button id="button-next" class="button-proceed-checkout d-lg-none"></button>
</div>
<script>
    let country_id = "<?php echo $currentCountryIso; ?>";
    let countryid = {!! json_encode($currentCountryid)!!};
    let langCode = {!!json_encode($langCode)!!};
    let urllangCode = {!! json_encode($urllangCode)!!};
    let user_id = {!!json_encode($user_id)!!} !== null ? "<?php echo $user_id; ?>" : null;
    let user = {!!json_encode($user)!!} !== null ? {!!json_encode($user)!!} : null;
    let delivery_address = {!!json_encode($delivery_address)!!} !== null ? {!!json_encode($delivery_address)!!} : null;
    let billing_address = {!!json_encode($billing_address)!!} !== null ? {!!json_encode($billing_address)!!} : null;
    let default_card = {!!json_encode($default_card)!!} !== null ? {!!json_encode($default_card)!!} : null;
    let session_data = {!!json_encode($session)!!};
    let payment_intent = {!!json_encode($payment_intent)!!} !== null ? {!!json_encode($payment_intent)!!} : null;
    let checkout_details = {!!json_encode($checkout_details)!!} !== null ? {!!json_encode($checkout_details)!!} : null;
    let current_price = {!!json_encode($current_totalprice)!!};
    let next_price = {!!json_encode($next_totalprice)!!};
    let shipping_fee = {!!json_encode($shipping_fee)!!};
    let taxRate  = <?php echo $taxRate; ?>;
    let ctaxAmount  = <?php echo $taxAmount; ?>;
</script>
<script src="{{ asset('js/functions/trial-plan/trial-plan-checkout.function.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>
@endsection
