<div id="shipping-address-show">
    <div class="panel-body">
        <div class="" id="appendAddress">
            <br>
            <label style="font-weight: bold">@lang('website_contents.checkout.address.delivery_address')</label><br>
            <p style="margin-bottom:0;">{{ $delivery_address ?  $delivery_address[0]["fullName"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $delivery_address ?  $delivery_address[0]["SSN"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $delivery_address ?  $delivery_address[0]["address"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $delivery_address ? $delivery_address[0]["portalCode"] : ''}}, {{ $delivery_address ? $delivery_address[0]["city"]: ''}}, {{ $delivery_address ? $delivery_address[0]["state"] : ''}}</p>
            <p style="margin-bottom:0;">{{ $delivery_address ? $delivery_address[0]["contactNumber"] : '' }}</p><br>
            <br>
            <label style="font-weight: bold">@lang('website_contents.checkout.address.billing_address')</label><br>
            <p style="margin-bottom:0;">{{ $billing_address ?  $billing_address[0]["address"] : '' }}</p>
            <p style="margin-bottom:0;">{{ $billing_address ? $billing_address[0]["portalCode"] : ''}}, {{ $billing_address ? $billing_address[0]["city"]: ''}}, {{ $billing_address ? $billing_address[0]["state"] : ''}}</p>
            <p style="margin-bottom:0;">{{ $billing_address ? $billing_address[0]["contactNumber"] : '' }}</p><br>
            <br>
        </div>
        <input type="hidden" id="delivery_address_id" name="delivery_address_id" value="{{ $delivery_address ?  $delivery_address[0]["id"] : '' }}" />
        <input type="hidden" id="billing_address_id" name="billing_address_id" value="{{ $billing_address ?  $billing_address[0]["id"] : '' }}" />

        <div><button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">@lang('website_contents.checkout.address.edit_address')</button></div>
    </div>
</div>

<div id="shipping-address-add" class="pl-0 pt-0 MuliPlain" style="display:none;">
    <h4 class="color-orange MuliBold d-none d-sm-block d-md-none">@lang('website_contents.checkout.address.address_details')</h4>
    <form id="add-addresses-block" method="POST" action="">
        @csrf
        <input type="hidden" name="product_country_id" value="">
        <div class="" id="shipping-address-block">
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Full Name" class="">@lang('website_contents.checkout.address.recipient_name')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_name" name="delivery_name" class="">
                <div id="pd_name_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @if(strtolower($currentCountryIso) == 'tw')
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="SSN" class="">@lang('website_contents.checkout.address.ssn')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_ssn" name="SSN" class="">
                <div id="pd_ssn_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @endif
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone" class="">@lang('website_contents.checkout.address.phone')</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="add_delivery_phoneext" name="delivery_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold number-only" id="add_delivery_phone" name="delivery_phone" class="">
                </div>
                <div id="pd_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address" class="">@lang('website_contents.checkout.address.unit_details')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_address" name="delivery_address" class="">
                <div id="pd_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state" class="">@lang('website_contents.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="add_delivery_state" name="delivery_state">
                @if($states)
                 @foreach ($states as $s)
                   @if($delivery_address)
                    @if($delivery_address[0]["state"] === $s["name"])
                    <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                    @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                    @endif
                   @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                   @endif
                  @endforeach
                @endif
                </select>
                <div id="pd_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city" class="">@lang('website_contents.checkout.address.town')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_delivery_city" name="delivery_city" class="">
                <div id="pd_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode" class="">@lang('website_contents.checkout.address.postcode')</label>
                <input class="col-12 padd0 form-control MuliBold number-only" id="add_delivery_postcode" name="delivery_postcode" class="">
                <div id="pd_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_add col-12">
            <div class="row">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox  form-control" type="checkbox" id="enableBillingEditCheckBox_for_add" name="isDifferentBilling" onclick="enableBilling()">
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">@lang('website_contents.checkout.address.different_address')</h5>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_add" style="display:none;">
            <div class="col-12 padd0 form-group pt-3">
                <b>@lang('website_contents.checkout.address.billing_address')</b>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">@lang('website_contents.checkout.address.phone')</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="add_billing_phoneext" name="billing_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold number-only" id="add_billing_phone" name="billing_phone">
                </div>
                <div id="pb_phone_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">@lang('website_contents.checkout.address.unit_details')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_address" name="billing_address">
                <div id="pb_address_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>

            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">@lang('website_contents.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="add_billing_state" name="billing_state">
                @if($states)
                 @foreach ($states as $s)
                   @if($billing_address)
                    @if($billing_address[0]["state"] === $s["name"])
                    <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                    @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                    @endif
                   @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                   @endif
                  @endforeach
                @endif
                </select>
                <div id="pb_state_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">@lang('website_contents.checkout.address.town')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_billing_city" name="billing_city">
                <div id="pb_city_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">@lang('website_contents.checkout.address.postcode')</label>
                <input class="col-12 padd0 form-control MuliBold number-only" id="add_billing_postcode" name="billing_postcode">
                <div id="pb_portalCode_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>
        <button class="btn btn-load-more col-6 uppercase" type="submit">@lang('website_contents.checkout.address.save_address')</button>
    </form>
</div>

<div id="shipping-address-edit" class="pl-0 pt-0 MuliPlain" style="display:none;">
    <h4 class="color-orange MuliBold d-none d-sm-block d-md-none">@lang('website_contents.checkout.address.address_details')</h4>
    <form id="edit-addresses-block" method="POST" action="">
        @csrf
        <input type="hidden" name="product_country_id" value="">
        <div class="" id="shipping-address-block">
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="Full Name" class="">@lang('website_contents.checkout.address.recipient_name')</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_name" name="delivery_name" class="" value="{{  $delivery_address ? $delivery_address[0]["fullName"] : '' }}">
                <div id="pd_name_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @if(strtolower($currentCountryIso) == 'tw')
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="SSN" class="">@lang('website_contents.checkout.address.ssn')</label>
                <input class="col-12 padd0 form-control MuliBold" id="add_ssn" name="SSN" class="" value="{{  $delivery_address ? $delivery_address[0]["SSN"] : '' }}">
                <div id="pd_ssn_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            @endif
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone" class="">@lang('website_contents.checkout.address.phone')</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="edit_delivery_phoneext" name="delivery_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold number-only" id="edit_delivery_phone" name="delivery_phone" class="" value="{{  $delivery_address ? str_replace(("+".view()->shared('callcode')->callingCode), "", $delivery_address[0]["contactNumber"]) : ''}}">
                </div>
                <div id="pd_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address" class="">@lang('website_contents.checkout.address.unit_details')</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_address" name="delivery_address" class="" value="{{ $delivery_address ? $delivery_address[0]["address"] : ''}}">
                <div id="pd_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state" class="">@lang('website_contents.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="edit_delivery_state" name="delivery_state">
                @if($states)
                 @foreach ($states as $s)
                   @if($delivery_address)
                    @if($delivery_address[0]["state"] === $s["name"])
                    <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                    @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                    @endif
                   @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                   @endif
                  @endforeach
                @endif
                </select>
                <div id="pd_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city" class="">@lang('website_contents.checkout.address.town')</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_delivery_city" name="delivery_city" class="" value="{{  $delivery_address ? $delivery_address[0]["city"] : ''}}">
                <div id="pd_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode" class="">@lang('website_contents.checkout.address.postcode')</label>
                <?php
                $postcodeda = "";
                if($delivery_address){
                    if($delivery_address[0]["portalCode"] != "00000"){
                        $postcodeda = $delivery_address[0]["portalCode"];
                    }
                }
                ?>
                <input class="col-12 padd0 form-control MuliBold number-only" id="edit_delivery_postcode" name="delivery_postcode" class="" value="{{  $postcodeda }}">
                <div id="pd_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>

        <!-- Billing Address -->
        <div id="enable-billing-address_for_edit col-12">
            <div class="row">
                <div class="col-1 pl-0 pr-0">
                    <input class="address-billing-checkbox" type="checkbox" id="enableBillingEditCheckBox_for_edit" name="isDifferentBilling" onclick="enableBilling()" <?php echo ($billing_address ? 'checked' : ''); ?>>
                </div>
                <div class="col-11 pl-0">
                    <h5 class="MuliPlain">@lang('website_contents.checkout.address.different_address')</h5>
                </div>
            </div>
        </div>
        <div id="billing-address-block_for_edit" style="display:none;">
            <div class="col-12 padd0 form-group pt-3">
                <b>@lang('website_contents.checkout.address.billing_address')</b>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="phone">@lang('website_contents.checkout.address.phone')</label>
                <div class="input-group">
                    <input class="col-2 padd0 pr-2 form-control MuliBold" id="edit_billing_phoneext" name="billing_phoneext" class="" value="+{{ view()->shared('callcode')->callingCode }}" style="background-color: white" readonly>
                    <input class="col-10 padd0 form-control MuliBold number-only" id="edit_billing_phone" name="billing_phone" value="{{  $billing_address ? str_replace(("+".view()->shared('callcode')->callingCode), "", $billing_address[0]["contactNumber"]) : ''}}">
                </div>
                <div id="pb_phone_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 padd0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="address">@lang('website_contents.checkout.address.unit_details')</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_address" name="billing_address" value="{{  $billing_address ? $billing_address[0]["address"] : ''}}">
                <div id="pb_address_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="state">@lang('website_contents.checkout.address.state')</label>
                <select class="col-12 padd0 form-control MuliBold" id="edit_billing_state" name="billing_state">
                @if($states)
                 @foreach ($states as $s)
                   @if($billing_address)
                    @if($billing_address[0]["state"] === $s["name"])
                    <option value="{{$s['name']}}" selected>{{$s["name"]}}</option>
                    @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                    @endif
                   @else
                    <option value="{{$s['name']}}">{{$s["name"]}}</option>
                   @endif
                  @endforeach
                @endif
                </select>
                <div id="pb_state_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="city">@lang('website_contents.checkout.address.town')</label>
                <input class="col-12 padd0 form-control MuliBold" id="edit_billing_city" name="billing_city" value="{{  $billing_address ? $billing_address[0]["city"] : ''}}">
                <div id="pb_city_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>

            <div class="col-12 pl-0 form-group" id="">
                <label class="col-12 padd0 color-grey" for="postcode">@lang('website_contents.checkout.address.postcode')</label>
                <?php
                $postcodeba = "";
                if($billing_address){
                    if($billing_address[0]["portalCode"] != "00000"){
                        $postcodeba = $billing_address[0]["portalCode"];
                    }
                }
                ?>
                <input class="col-12 padd0 form-control MuliBold number-only" id="edit_billing_postcode" name="billing_postcode" value="{{  $postcodeba }}">
                <div id="pb_portalCode_e_error" class="hasError col-12 col-form-label text-left px-0"></div>
            </div>
        </div>
        <button class="btn btn-load-more col-6 uppercase mt-3" style="font-size: 12px" type="submit"><b>@lang('website_contents.checkout.address.save_address')</b></button>
    </form>
</div>
