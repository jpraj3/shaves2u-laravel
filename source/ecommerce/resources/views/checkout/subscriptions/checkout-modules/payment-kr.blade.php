
            <div id="show_card_details_container" class="panel-body">
                <div class="rounded-10 bg-white padd20">
                    <div class="form-group owner">
kr
                    </div>
                </div>
            </div>
            <br>
            <div id="add_card_details_container" class="panel-body">
                <div class="rounded-10 bg-white padd20">
                    <div class="payment">
                        <form id="add-card-details-kr" method="POST" action="">
                            <div class="form-group owner">
                                <label for="owner">Card Name</label>
                                <input type="text" name="card_name" class="form-control" id="card_name">
                            </div>
                            <div class="form-group" id="card-number-field">
                                <label for="cardNumber">Card Number</label>
                                <input type="text" name="card_number" class="form-control" id="card_number">
                            </div>
                            <div class="form-group Birth">
                                <label for="birth">Birth(YYMMDD)</label>
                                <input type="text" name="card_birth" class="form-control" id="card_birth">
                            </div>
                            <div class="form-group" id="expiration-date">
                                <label>Expiration Date</label>
                                <select name="card_expiry_month" id="card_expiry_month">
                                    <option value="01">January</option>
                                    <option value="02">February </option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="card_expiry_year" id="card_expiry_year">
                                    <option value="2019"> 2019</option>
                                    <option value="2020"> 2020</option>
                                    <option value="2021"> 2021</option>
                                    <option value="2022"> 2022</option>
                                    <option value="2023"> 2023</option>
                                    <option value="2024"> 2024</option>
                                    <option value="2025"> 2025</option>
                                    <option value="2026"> 2026</option>
                                    <option value="2027"> 2027</option>
                                    <option value="2028"> 2028</option>
                                    <option value="2029"> 2029</option>
                                    <option value="2030"> 2030</option>
                                    <option value="2031"> 2031</option>
                                    <option value="2032"> 2031</option>
                                </select>
                            </div>
                            <div class="form-group Password">
                                <label for="password">Password</label>
                                <input type="text" name="card_password" class="form-control" id="card_password">
                            </div>
                            <div class="form-group" id="credit_cards">
                                <img src="{{ asset('images/cards/visa.jpg') }}" id="visa">
                                <img src="{{ asset('images/cards/mastercard.jpg') }}" id="mastercard">
                                <img src="{{ asset('images/cards/amex.jpg') }}" id="amex">
                            </div>
                            <p class="error-card"  id="error-card"></p>
                        </form>
                    </div>
                </div>
            </div>
 