@extends('layouts.app')
<!--Temporary Style Begin-->
<style>
    .panel-heading-selected {
        background-color: black;
    }

    .panel-heading-unselected {
        background-color: white;
    }

    .panel-title-selected {
        color: white;
    }

    .panel-title-unselected {
        color: black;
    }

    .button-back {
        color: tomato;
        background-color: transparent;
        border: 0px;
        font-size: 15px;
        margin-top: 14px;
        margin-bottom: 14px;
    }

    .panel {
        background-color: #ececec !important;
    }

    .panel-collapse {
        background-color: #ececec !important;
    }

    button:focus {
        outline: 0 !important;
    }

    .display-none {
        display: none;
    }

    .box-grey {
        background-color: #ececec;
        padding: 20px;
    }

    input {
        background-color: white;
        padding: 5px;
        width: 100%;
    }

    .button-edit-account {
        background-color: white;
        color: black;
        border-color: tomato;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 10px;
        width: 100%;
    }

    .button-proceed-checkout {
        background-color: tomato;
        color: white;
        border-color: tomato;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 30px;
        width: 100%;
    }
</style>
<!--Temporary Style End-->
@section('content')
<div class="container">
    <button id="button-back" class="button-back">
        < BACK</button> <br>
            <div style="margin: 0px !important; padding: 20px">
                <div class="panel-body">
                    <label style="font-weight: bold">TOTAL SUMMARY</label><br>
                    <div class="box-grey">
                        <br><img alt="image" />
                        <br><label>Trial Kit</label>
                        <br><label>5 Blade Cartridge</label>
                        <hr />
                        <br><label>Subtotal: <span></span></label>
                        <br><label>Shipping: <span></span></label>
                        <br><label>Discount: <span></span></label>
                        <hr />
                        <br><label>Promo Code</label>
                        <br><label>Enter promo code</label>
                        <br><input type="text" />
                        <br><button>APPLY</button>
                        <hr />
                        <br><label>Today Total</label>
                        <br><label>RM 6.50</label>
                    </div>
                    <div id="card-container" style="display:block;">

                        <div id="card-element" style="display:block;">


                        </div>
                    </div>
             <form id="form-pay" method="POST" action="{{ route('locale.region.shave-plans.custom-plan.checkout.payment', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
             {{ csrf_field() }}   
             <button id="proceed-pay" class="button-proceed-checkout" type="submit">PROCEED TO PAY</button>
            </form>

                </div>
            </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script>
    let session_data = {
        !!$Session!!
    };

    let plan_id = session_data["selection"]["step5"]["planId"];

    // On load
    $(function() {

        //Card element styling
        var style = {
            base: {
                // Add your base input styles here. For example:
                fontSize: '16px',
                color: "#32325d",
            }
        };

        //Test stripe payment intent completion
        var stripe = Stripe("pk_test_dtbtR43Joi49oJUwwGfItzUM");
        var elements = stripe.elements();
        var cardElement = elements.create('card', {
            style: style
        });
        cardElement.mount('#card-element');

        setTimeout(prefill(), 5000);

        var cardholderName = "Syevesther Liew";
        var cardButton = document.getElementById('button-next');
        var clientSecret = stripe_client_secret;

        cardButton.addEventListener('click', function(ev) {
            stripe.handleCardPayment(
                clientSecret, cardElement, {
                    payment_method_data: {
                        billing_details: {
                            name: cardholderName
                        }
                    }
                }
            ).then(function(result) {
                if (result.error) {
                    console.log("Failed.");
                } else {
                    console.log("Success.");
                }
            });
        });
    })


    function prefill() {
        let abc = $("input[name$='cardnumber']").is(":visible");
    }
</script>
@endsection