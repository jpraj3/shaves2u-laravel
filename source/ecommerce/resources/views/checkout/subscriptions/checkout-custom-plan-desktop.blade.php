@extends('layouts.app')

@section('content')
<!-- PHP START -->
@php($user = isset($User) ? $User["user"] : null)
@php($user_id = isset($User) ? $User["user"]["id"] : null)
@php($delivery_address = isset($User) ? $User["delivery_address"] : null)
@php($billing_address = isset($User) ? $User["billing_address"] : null)
@php($cards = isset($User) ? $User["cards"] : null)
@php($default_card = isset($User) ? $User["default_card"] : null )
@php($session = $session_data)
@php($checkout_details = isset($checkout_details) ? $checkout_details : null )
@php($currentCountry = session()->has('currentCountry') ? session()->get('currentCountry') : null )
@php($sessionLoginError = session()->get('loginerror'))
<?php session()->forget('loginerror'); ?>
<script src="{{asset('js/addressAPI/daum.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/custom-plan/custom-plan-checkout.css') }}">

<style>
    input.mask_content {
        -webkit-text-security: disc;
    }

    .error {
        color: red;
    }

    .error-promotion {
        color: red;
    }
    .hasError{
        padding:0px;color:red;margin-top:10px;
    }
    .notification {
        height: 40px;
        position: sticky;
        top: 0;
        width: 100%;
        display: table;
        background: #f4f8fc;
        opacity: 1;
        z-index: 10000;
    }

    .notification > span {
        text-align: center;
        margin: auto;
        display: table-cell;
        vertical-align: middle;
    }

    .click-able {
        color: #ed594a;
        cursor: pointer;
    }
</style>
<div id="inactiveUserNotification" class="notification" hidden>
        <span>
            You account is not activated. Please check your mailbox. Did not receive activation email?  <br class="d-sm-none d-block"> <strong class="click-able" onClick="sendWelcomeEdm()">Resend Activation Email</strong>?
        </span>
    </div>
<div class="grey-bg">
    <div class="container">

        <!-- Hidden Inputs for Checkout -->
        <input type="hidden" name="payment_intent_next_update_type" id="payment_intent_next_update_type" value="first-update" />
        <input type="hidden" name="payment_intent_id" id="payment_intent_id" value="" />
        <!-- END -->

        <div class="row px-3">
            <div class="col-3 pr-0 pl-0 d-flex align-items-center">
                <button id="button-back" class="button-back">
                    < @lang('website_contents.global.content.back')</button> </div> <div class="col-6 pr-0 pl-0 d-flex align-items-center">
                        <div class="checkout-title-1 col-12 text-center pr-0 pl-0 d-lg-none">
                            <h5 class="mb-0">@lang('website_contents.checkout.content.payment')</h5>
                        </div>
                        <div class="checkout-title-2 col-12 text-center pr-0 pl-0 d-none d-lg-none">
                            <h4 class="mb-0">Total Summary</h4>
                        </div>
            </div>
        </div>

        <!-- BEGIN: DESKTOP LAYOUT -->
        <div class="checkout-content-desktop col-12 d-lg-block" style="padding-bottom: 100px;">
            <div class="row">
                <div class="col-7 pt-2 pb-4">
                    <!-- Begin: Desktop Account -->
                    <div class="col-12 rounded-10 bg-white shadow-sm padd15 pl-4 pr-4 mb-4 pb-0">

                        @if($loginStatus === "pre-login")
                        <div class="container-create-account">

                            <div class="row ml-0 mr-0 pt-2">
                                <h4 class="title-create-account MuliExtraBold">@lang('website_contents.checkout.user.create_account')</h4>
                                <!-- <i class="fa fa-check-circle ml-auto fs-30 text-orng"></i> -->
                            </div>

                            <form id="form_register" method="POST" action="" onsubmit="return false">
                                @csrf
                                <div class="col-lg-12 padd20 pt-0 pl-0 pr-0">
                                    <div class="col-lg-12 padd20 pl-0 pr-0">
                                        <div class="register-group form-group">
                                            <label class="color-grey pb-0">@lang('website_contents.checkout.user.name')</label>
                                            <div class="input-group">
                                                <input id="d-name-tk" name="name" type="text" class="name padd0 form-control MuliBold @error('name') is-invalid @enderror"  />
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <div id="name_error" class="hasError col-12 col-form-label text-left"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="color-grey">@lang('website_contents.checkout.user.email_address')</label>
                                            <div class="input-group">
                                                <input id="d-email-tk" name="email" type="email" class="email padd0 form-control MuliBold @error('email') is-invalid @enderror"  />
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="color-grey">@lang('website_contents.checkout.user.password')</label>
                                            <div class="input-group">
                                                <input id="d-password-tk" placeholder="<?php if(strtolower($currentCountryIso) == 'kr'){ echo trans('validation.custom.validation.password.validpassword'); }?>" name="password" type="password" class="password padd0 form-control MuliBold @error('password') is-invalid @enderror"  autocomplete="new-password"><span style="position:absolute; right: 0;" toggle="#d-password-tk" class="fa fa-eye field-icon toggle-password"></span>
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <div id="password_error" class="hasError col-12 col-form-label text-left"></div>
                                            </div>
                                        </div>
                                        <div class="error error_password"></div>
                                    </div>
                                    <div class="register-group form-group">
                                        <div class="row">
                                            <label for="date-of-birth" class="col-12 col-form-label text-left color-grey">@lang('website_contents.checkout.user.date_of_birth')</label>
                                            <div class="col-4 pl-3 MuliBold">
                                                <select id="d-day-of-birth-tk" class="day-of-birth form-control custom-select @error('name') is-invalid @enderror" name="day-of-birth"  style="background-image: url({{asset('/images/common/arrow-down.svg')}});" >
                                                    <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.dd')</option>
                                                    @for($i=1;$i<32;$i++) @switch($i) @case($i) @if($i < 10) <option value="{{sprintf(" %02d ", $i)}}">{{sprintf("%02d", $i)}}</option>
                                                        @else
                                                        <option value="{{ $i }}">{{$i}}</option>
                                                        @endif
                                                        @break
                                                        @default
                                                        @endswitch
                                                        @endfor
                                                </select>
                                            </div>
                                            <div class="col-4 pl-1 MuliBold">
                                                <select id="d-month-of-birth-tk" class="month-of-birth form-control custom-select" name="month-of-birth"   style="background-image: url({{asset('/images/common/arrow-down.svg')}});" >
                                                    <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.mm')</option>
                                                    @for($i=1;$i<13;$i++) @switch($i) @case(1) <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jan')</option>
                                                        @break @case(2)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.feb')</option>
                                                        @break @case(3)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.mar')</option>
                                                        @break @case(4)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.apr')</option>
                                                        @break @case(5)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.may')</option>
                                                        @break @case(6)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jun')</option>
                                                        @break @case(7)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jul')</option>
                                                        @break @case(8)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.aug')</option>
                                                        @break @case(9)
                                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.sep')</option>
                                                        @break @case(10)
                                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.oct')</option>
                                                        @break @case(11)
                                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.nov')</option>
                                                        @break @case(12)
                                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.dec')</option>
                                                        @break
                                                        @default
                                                        @endswitch
                                                        @endfor
                                                </select>
                                            </div>
                                            <div class="col-4 pr-3 MuliBold">
                                                <select id="d-year-of-birth-tk" class="year-of-birth form-control custom-select" name="year-of-birth"   style="background-image: url({{asset('/images/common/arrow-down.svg')}});" >
                                                    <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.yyyy')</option>
                                                    @for($i=30;$i<100;$i++) @switch($i) @case($i) @if($i < 10) <option value="19{{sprintf(" %02d ", $i)}}">19{{sprintf("%02d", $i)}}</option>
                                                        @else
                                                        @if($i==80)
                                                        <option value="19{{ $i }}" selected>19{{$i}}</option>
                                                        @else
                                                        <option value="19{{ $i }}">19{{$i}}</option>
                                                        @endif
                                                        @endif
                                                        @break
                                                        @default
                                                        @endswitch
                                                        @endfor
                                                        @for($i=0;$i<(int)substr(date("Y"), -2) + 1;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf(" %02d ", $i)}}">20{{sprintf("%02d", $i)}}</option>
                                                            @else
                                                            <option value="20{{ $i }}">20{{$i}}</option>
                                                            @endif
                                                            @break
                                                            @default
                                                            @endswitch
                                                            @endfor
                                                </select>
                                            </div>
                                            @error('date-of-birth')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div id="birthday_error" class="hasError col-12 col-form-label text-left pl-4"></div>
                                        </div>
                                    </div>

                                    @if(strtolower($currentCountryIso) == 'kr')
                                        <div class="form-group register-group">
                                            <div class="row">
                                                <div class="input-group">
                                                    <input required id="tos_agree" name="tos_agree" type="checkbox" class="col-1 @error('tos_agree') is-invalid @enderror"/>
                                                    <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.tos_agreement')</u></p>
                                                </div>
                                                <div class="input-group">
                                                    <input required id="privacy_policy_agree" name="privacy_policy_agree" type="checkbox" class="col-1 @error('privacy_policy_agree') is-invalid @enderror"/>
                                                    <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.privacy_policy_agreement')</u></p>
                                                </div>
                                                <div class="input-group">
                                                    <input required id="personal_info_agree" name="personal_info_agree" type="checkbox" class="col-1 @error('personal_info_agree') is-invalid @enderror"/>
                                                    <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.personal_info_transfer_agreement')</u></p>
                                                </div>
                                                <div class="input-group">
                                                    <input required id="min_age_agree" name="min_age_agree" type="checkbox" class="col-1 @error('min_age_agree') is-invalid @enderror"/>
                                                    <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.minimum_age_agreement')</p>
                                                </div>
                                                <div class="input-group">
                                                    <input id="email_sub_agree" name="email_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                    <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.email_sub_agreement')</p>
                                                </div>
                                                <div class="input-group">
                                                    <input id="sms_sub_agree" name="sms_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                    <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.sms_sub_agreement')</p>
                                                </div>
                                                <ul class="col-11 offset-1">
                                                    <li>@lang('website_contents.authentication.register.sms_sub_agreement_point1')</li>
                                                    <li>@lang('website_contents.authentication.register.sms_sub_agreement_point2')</li>
                                                </ul>
                                            </div>
                                            @error('required_checked')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div id="required_checked_error" class="hasError col-12 col-form-label text-left"></div>
                                        </div>
                                    @else
                                        <div class="form-group register-group">
                                            <div class="row text-left">
                                                <div class="input-group">
                                                    <input id="marketing_sub_agree" type="checkbox" class="col-1" value="1"/>
                                                    <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.marketing_sub_agreement')</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-lg-12 pb-2 pl-0 pr-0">
                                        <button id="btn-create-account-desktop" type="submit" class="btn-create-account btn btn-load-more-orange w-100">
                                                @lang('website_contents.checkout.user.create_account')
                                        </button>
                                    </div>
                                    <div class="col-lg-12 pb-2 pl-0 pr-0 pt-2">
                                        <a href="{{ route('auth.facebook', ['type' =>'purchase_custom_plan','langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" class="btn btn-load-more-fb w-100"><img src="{{asset('/images/common/checkoutAssets/fb_icon.png')}}" /> <b>@lang('website_contents.checkout.user.sign_in_facebook')</b></a>
                                    </div>
                                    <div class="col-lg-12 pl-0 pr-0 pt-2">
                                        <h6 class="text-sign-in">@lang('website_contents.checkout.user.have_account')</h6>
                                        <h6 class="text-create-one hidden">@lang('website_contents.checkout.user.no_account')</h6>
                                    </div>
                                </div>
                            </form>

                        </div>
                        @else
                        <div class="container-profile-account">

                            <div class="row ml-0 mr-0">
                                <h4 class="MuliExtraBold pl-2 pt-2 pb-2">@lang('website_contents.checkout.user.personal_details')</h4>
                                <i class="orange-tick-personal fa fa-check-circle ml-auto fs-30 text-orng hidden"></i>
                            </div>

                            <div class="col-lg-12 padd20 pl-0 pr-0">
                                <div class="col-lg-12 padd20 pl-2 pr-0">
                                    <div class="form-group">
                                        <label class="color-grey">@lang('website_contents.checkout.user.name')</label>
                                        <div class="input-group">
                                            <label class="MuliBold form-control">{{$user['firstName']}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="color-grey">@lang('website_contents.checkout.user.email')</label>
                                        <div class="input-group">
                                            <label class="MuliBold form-control">{{$user['email']}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 pl-2 pr-0 pt-1 pb-3">
                                    <h6 class="text-sign-in" onClick="switchAccounts();">
                                        @lang('website_contents.authenticated.content.switch_account')
                                    </h6>
                            </div>
                        </div>
                        @endif

                    </div>
                    <!-- End: Desktop Account -->


                    <!-- Begin: Desktop Shipping -->
                    <div class="col-12 rounded-10 bg-white shadow-sm padd15 mb-4">
                        <div class="row ml-0 mr-0">
                            <h4 class="MuliExtraBold pl-3 pt-2 pb-2">@lang('website_contents.checkout.address.shipping_address')</h4>
                            @if($loginStatus === "post-login")
                            <i class="orange-tick-address fa fa-check-circle ml-auto fs-30 text-orng hidden"></i>
                        </div>
                        <div id="shipping-address-body" class="collapse2 col-12 panel-collapse-white d-lg-block pt-0" style="margin: 0px !important; padding: 20px; padding-left: 0;">
                            <div class="panel-body">
                                <div class="rounded-10 bg-white padd20 pt-0 pb-0">
                                    @if(count($delivery_address) > 0)
                                    @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-edit')
                                    @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-edit')
                                    @endif @else @if(strtolower($currentCountryIso) == 'kr')
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-kr-add')
                                    @else
                                    @include('checkout.subscriptions.checkout-modules.delivery-address-add')
                                    @endif
                                    @endif
                                    <div class="col-12 pt-4 pl-0 pr-0" hidden>
                                        @if(count($delivery_address) > 0)
                                        <div class="row mx-0">
                                            <div class="col-6 pl-0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">@lang('website_contents.checkout.address.edit_address')</button>
                                            </div>
                                            <div class="col-6 pr-0">
                                                <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">@lang('website_contents.checkout.address.save_address')</button>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row mx-0">
                                            <div class="col-6 padd0">
                                                <button id="enable-address-add" class="btn btn-load-more col-12" onclick="addAddress()">@lang('website_contents.checkout.address.save_address')</button>
                                            </div>
                                            <div class="col-6 padd0">
                                                <button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()" style="display:none;">@lang('website_contents.checkout.address.edit_address')</button>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-6 padd0">
                                            <button id="cancel-address-edit" class="btn btn-load-more col-12" onclick="cancelEditAddress()" style="display:none;">Cancel</button>
                                            <label class="pt-4" id="estimated-delivery-date" style="font-weight: bold; font-size: 13px">Estimated delivery {{Carbon\Carbon::now()->addDays(7)->format('M d')}} - {{Carbon\Carbon::now()->addDays(14)->format('M d')}}</label>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <i class="orange-tick-address fa fa-check-circle ml-auto fs-30 text-orng hidden"></i>
                    </div>
                    @endif
                </div>
                <!-- End: Desktop Shipping -->


                <!-- Begin: Desktop Payment Method -->
                <div class="col-12 rounded-10 bg-white shadow-sm padd15 pl-4 pr-4">
                    <h4 class="MuliExtraBold pl-2 pt-2 pb-2">@lang('website_contents.checkout.payment_method.payment_method')</h4>

                    @if($loginStatus === "post-login")
                    @if(strtolower($currentCountryIso) == 'kr')
                    <form id="form_card-kr">
                        <div class="row mr-0 ml-0">
                            <div class="col-12 pl-0 pr-0">
                                <div class="row mr-0 ml-0">
                                    <div class="col-6 padd20 pl-0 pr-0 pb-0">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.card_number')</label>
                                            <input id="card-number-masked" name="card_number_masked" type="text" class="card_number_masked number-only fs-20 form-control pl-0" maxlength="19" />
                                            <input id="card-number" name="card_number" type="hidden" class="credit-card-logo card_number number-only fs-20 pl-0" maxlength="19" />
                                            <div id="c_cardNumber_error" class="hasError col-12 col-form-label text-left p-0 "></div>
                                        </div>
                                    </div>
                                    <div class="col-6 padd20 pr-0 pb-0">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.expiry_date')</label>
                                            <input id="card-expiry" name="card_expiry_date" type="text" class="card_expiry_date number-only fs-20 form-control pl-0" maxlength="9" />
                                            <div id="c_expirydate_error" class="hasError col-12 col-form-label text-left  p-0 "></div>
                                        </div>
                                    </div>
                                    <div class="col-7 padd20 pl-0 pr-0 pt-0 pb-0">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.card_birth')</label>
                                            <input id="card_birth" name="card_birth" type="text" class="card_birth number-only fs-20 form-control pl-0" maxlength="6" />
                                            <div id="c_birth_error" class="hasError col-12 col-form-label text-left  p-0 "></div>
                                        </div>
                                    </div>
                                    <div class="col-5 padd20 pr-0 pt-0 pb-0">
                                        <div class="form-group">
                                            <label>@lang('website_contents.checkout.payment_method.card_password')</label>
                                            <input id="card_password" placeholder="비밀번호 첫 두자리" name="card_password" type="text" class="card_password number-only fs-20 form-control mask_content pl-0" maxlength="2" />
                                            <div id="c_password_error" class="hasError col-12 col-form-label text-left  p-0  "></div>
                                        </div>
                                    </div>
                                    <div id="error-payment-method" class="error MuliPlain padd20 pl-0 pr-0 pt-0"></div>
                                </div>
                            </div>
                            <div class="col-12 pr-0 pl-2">
                                <p>@lang('website_contents.checkout.payment_method.terms_of_service_agree', ['url' => route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')])])</p>
                            </div>
                        </div>
                    </form>

                    @else
                    <!-- Add card -->
                    <div id="add-card-container" class="hidden">
                    <form id="form_card">
                        <div class="row mr-0 ml-0">
                            <div class="col-md-12 col-lg-6 pl-0 pr-0">
                                <div class="col-10 offset-1 p-0" style="margin-left: 10px;">
                                    <div class="row justify-content-center pt-4" style="height:90%; margin: 0;">
                                        <div class="col-12 border border-dark rounded pb-0"  style="height:100%; display: table-cell; vertical-align: middle;">
                                            <div class="text-left">
                                                <p class="fs-16 MuliSemiBold pt-3"><span class="cc_last4digits">1234</span></p>
                                            </div>
                                            <div class="row justify-content-between">
                                                <div class="col-7">
                                                    <div class="text-left">
                                                        <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                        <p class="fs-18 MuliPlain"><span class="cc_expmy">xx/xx</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                                    <div class="add-logo-change">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 pl-0 pr-0">
                                <div class="row mr-0 ml-0">
                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                        <div class="form-group">
                                            <label>Card Number</label>
                                            <input id="card-number-masked" name="card_number_masked" type="text" class="card_number_masked number-only fs-20 form-control pl-0" maxlength="19" />
                                            <input id="card-number" name="card_number" type="hidden" class="credit-card-logo card_number number-only fs-20 pl-0" maxlength="19" />
                                            <div id="c_cardNumber_error" class="hasError col-12 col-form-label text-left"></div>
                                        </div>
                                    </div>
                                    <div class="col-8 padd20 pl-0 pt-0 pb-0">
                                        <div class="form-group">
                                            <label>Expiry Date (MM/YY)</label>
                                            <input id="card-expiry" name="card_expiry_date" type="text" class="card_expiry_date number-only fs-20 form-control pl-0" maxlength="7" />
                                        </div>
                                    </div>
                                    <div class="col-4 padd20 pr-0 pt-0 pb-0">
                                        <div class="form-group">
                                            <label>CVV</label>
                                            <input id="card-cvv" name="card_cvv" type="text" class="card_cvv number-only fs-20 form-control mask_content pl-0" maxlength="4" />
                                        </div>
                                    </div>
                                    <div id="c_expirydate_error" class="hasError col-12 col-form-label text-left"></div>
                                    <div id="c_ccv_error" class="hasError col-12 col-form-label text-left"></div>
                                    <div id="error-payment-method" class="error MuliPlain hidden padd20 pr-0 pt-0"></div>
                                </div>
                            </div>
                            <div class="col-12 pr-0 pl-2">
                                <p>@lang('website_contents.checkout.payment_method.terms_of_service_agree')</p>
                            </div>
                        </div>

                        <div class="col-lg-12 text-center ">
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <button id="button-add-card" class="btn btn-load-more w-100" type="button"><b>Save Card</b></button>
                                </div>
                                <div class="col-6">
                                    <button id="button-cancel-add-new-card" class="btn btn-load-more hidden w-100" type="button"><b>Cancel</b></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>

                    <!-- Edit card -->
                    <div id="edit-card-container" class="hidden">
                        <div class="row mr-0 ml-0 justify-content-center">
                            <div class="col-md-12 col-lg-6 pl-0 pr-0">
                                <div class="col-10 offset-1" style="margin-left: 10px;">
                                    <div class="row justify-content-center pt-4" style="height:90%;">
                                        <div class="col-12 border border-dark rounded pb-0"  style="height:90%; display: table-cell; vertical-align: middle;">
                                            <div class="text-left">
                                                <p class="fs-16 MuliSemiBold pt-3"><span class="cc_last4digits">{{ isset($default_card) && isset($default_card[0]) ? $default_card[0]['cardNumber'] : '1234' }}</span>
</p>
                                            </div>
                                            <div class="row justify-content-between">
                                                <div class="col-7">
                                                    <div class="text-left">
                                                        <p class="fs-14 MuliPlain mb-0 color-grey">@lang('website_contents.authenticated.profile.valid_thru')</p>
                                                        <p class="fs-18 MuliPlain"><span class="cc_expmy">{{ isset($default_card) && isset($default_card[0]) ? $default_card[0]['expiredMonth'] : 'xx' }}/{{ isset($default_card) && isset($default_card[0]) ? substr($default_card[0]['expiredYear'], -2) : 'xx' }}</span>
</p>
                                                    </div>
                                                </div>
                                                <div class="col-4 pr-0 ml-1 credit-card-bg" style="padding-left: 7%">
                                                    <div class="logo-change">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 pl-0 pr-0">
                                <div class="row mr-0 ml-0">
                                    <div class="col-12 padd20 pl-0 pr-0 pb-0">
                                        <div class="form-group">
                                            <label class="color-grey">Selected Card Number</label>
                                            <select id="card-dropdown" class="MuliBold form-control pl-0" placeholder="">
                                                <!-- @foreach($cards as $card)
                                                        <option value="{{json_encode($card)}}">xxxx xxxx xxxx {{$card["cardNumber"]}}</option>
                                                    @endforeach -->
                                            </select>
                                            <!-- <input type="text" class="fs-20 form-control" placeholder=""/> -->
                                        </div>
                                    </div>
                                    <div class="col-8 padd20 pl-0 pt-0">
                                        <div class="form-group">
                                            <label class="color-grey">Expiry Date (MM/YY)</label>
                                            <input id="view-expiry-date" type="text" class="MuliBold form-control pl-0" readonly="readonly" style="background-color: white" value="12 / 24" />
                                        </div>
                                    </div>
                                    <div class="col-4 padd20 pr-0 pt-0">
                                        <div class="form-group">
                                            <label class="color-grey">CVV</label>
                                            <input id="view-cvv" type="text" class="MuliBold form-control pl-0" readonly="readonly" style="background-color: white" value="xxx" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pr-0 pl-2">
                                <p>@lang('website_contents.checkout.payment_method.terms_of_service_agree')</p>
                            </div>
                        </div>

                        <div class="col-lg-12 text-center justify-content-start pl-1">
                            <div class="col-6 p-0 m-0 pl-0">
                                <button id="button-add-new-card" class="btn btn-load-more w-100 uppercase"><b>ADD NEW CARD</b></button>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                </div>
                <!-- End: Desktop Payment Method -->
                <style>
                    .subs_agreement_tos_text_error {
                        color: red !important;
                        border-color: red !important;
                    }
                    .subs_agreement_tos_checkbox_error {
                        width: 18px !important;
                        height: 18px !important;
                        -webkit-appearance: none;
                        -moz-appearance: none;
                        -o-appearance: none;
                        outline: 1px solid red;
                        box-shadow: none;
                        margin-top: 1.2em !important;
                    }
                    .tos-subscription-agree{
                     width: 15px;   
                    }
                </style>
                <form id="tos_subscription_agree_form" class="col-12 d-inline-flex rounded-10 border-direct" style="margin: 1% 0 5% 0;">
                    <div class="col-12 d-inline-flex">
                        <div class="col-2 col-sm-1 col-md-1 col-xl-1 col-lg-1">
                            <input required id="tos_subscription_agree" name="tos_subscription_agree" type="checkbox" class="MuliBold form-control dierect-btn mt-2 tos-subscription-agree"/>
                        </div>
                        <div class="col-10 col-sm-11 col-md-11 col-xl-11 col-lg-11">
                            @php($country_data = json_decode(session()->get('currentCountry'),true))
                            @php($terms_url = route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]))
                            <p class="mt-3"><b>@lang('website_contents.checkout.agreement.subscription_agreement', [ 'link' => $terms_url])</b></p>
                        </div>
                    </div>
                </form>

                <!-- Start: Desktop Pay Now Button -->
                @if($loginStatus === "post-login")
                <div class="col-6 text-center pt-4 pl-0 pr-0">
                    @if(strtolower($currentCountryIso) == 'kr')
                    <div id="form-pay-kr">
                        {{ csrf_field() }}
                        <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
                    </div>
                    @else
                    <div id="form-pay">
                        {{ csrf_field() }}
                        <button class="btn btn-start MuliExtraBold" type="submit" style="width: 100%">@lang('website_contents.checkout.content.pay_now')</button>
                    </div>
                    @endif
                </div>
                @endif
                <!-- End: Desktop Pay Now Button -->

            </div>
            <div class="col-5 pt-2 pb-1">

                <!-- Start: Desktop Payment Summary -->
                <div class="col-12 rounded-t-10 bg-white shadow-sm pr-4 pl-4 pt-3 pb-0">
                    <div class="row mr-0 ml-0">
                        <h4 class="MuliExtraBold">@lang('website_contents.checkout.payment_summary.summary')</h4>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                            <div class="row">
                                <div class="col-5 padd0 d-flex align-items-center">
                                    <img class="img-fluid custom-image" src="{{asset('/images/common/shaveplan.png')}}" />
                                </div>
                                <div class="col-7 padd0 d-flex align-items-center">
                                    <div class="col-12 padd0">
                                        <h3 class="MuliBold">@lang('website_contents.authenticated.content.shave_plans')</h3>
                                        <?php
                                        $plandescriptionexplode  = $plandescription;
                                        if ($plandescriptionexplode) {
                                            $pdescription = explode(",", $plandescriptionexplode);
                                            foreach ($pdescription as $value) {
                                                if (strpos($value, 'Month') !== false) {
                                                    $value = str_replace('Months', "", $value);
                                                    $value = str_replace('Month', "", $value);
                                                    $value = str_replace(' ', "", $value);
                                                    echo "<i class='fa fa-refresh' aria-hidden='true'><span class='fs-18 Muli MuliPlain m-0'> ".trans('website_contents.every') . $value .trans('website_contents.Months') . "</span></i>";
                                                } else {
                                                    echo "<p class='fs-18 Muli' style='margin: 0;'>" . $value . "</p>";
                                                }
                                            }
                                        }

                                        ?>
                                       <p class='fs-18 Muli summary-number' style='margin: 0;'><span id='c-free-product'></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                            <div class="row mr-0 ml-0">
                                <div class="col-12 padd0">
                                     <!-- Currency and price for kr -->
                                <?php
                              if((float)$discount_amount > 0){
                                $discount =  number_format($discount_amount, 2, '.', '');
                              }else{
                               $discount = '0.00';
                              }
                              if((float)$ndiscount_amount > 0){
                                $ndiscount = $ndiscount_amount;
                              }else{
                               $ndiscount = '0.00';
                              }
                               $c_price = $current_price;
                               $s_fee = $shipping_fee;
                               if(strtolower($currentCountryIso) == 'kr'){
                                   $c_price = number_format($current_price);
                                   $s_fee= number_format($shipping_fee);
                                   if((float)$discount_amount > 0){
                                    $discount = $discount_amount;
                                  }else{
                                   $discount = '0';
                                  }
                                  if((float)$ndiscount_amount > 0){
                                    $ndiscount = $ndiscount_amount;
                                  }else{
                                   $ndiscount = '0';
                                  }
                               }

                               ?>
                                @if(strtolower($currentCountryIso) == 'kr')
                                <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.subtotal') <span class="pull-right summary-number"><span id="c-subtotal">{{$c_price}}</span>{{$currency}}</span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.shipping') <span class="pull-right summary-number"><span id="c-shipping">{{$s_fee}}</span>{{$currency}}</span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.discount') <span class="pull-right summary-number"><span id="c-discount">{{$discount}}</span>{{$currency}}</span></label>
                                @else
                                <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.subtotal') <span class="pull-right summary-number">{{$currency}} <span id="c-subtotal">{{$current_price}}</span></span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.shipping') <span class="pull-right summary-number">{{$currency}} <span id="c-shipping">{{$shipping_fee}}</span></span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.discount') <span class="pull-right summary-number">{{$currency}} <span id="c-discount">{{$discount}}</span></span></label>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 padd20 pl-0 pr-0 pb-0">
                            <div class="row mr-0 ml-0">
                                <div class="col-7 padd0">
                                    <p id="c-tax" class="d-none">{{$taxAmount}}</span>
                                        <!-- <p class="d-none" id="c-free-product"></p> -->
                                        <p class="d-none" id="c-free-exist-product"></p>
                                        <!--for next price-->
                                        <p class="fs-18">@lang('website_contents.checkout.payment_summary.promo_code')</p>
                                        <div class="form-group">
                                            <input id="promo_code" type="text" class="form-control" style="font-size: 14px" placeholder="@lang('website_contents.checkout.payment_summary.enter_promo_code')" />
                                        </div>
                                </div>
                                <div class="col-5 padd0">
                                    <button class="btn btn-load-more pt-1 pb-1" style="width: 93%; position: absolute; bottom: 20px;" onclick="applyPromotion()"><b>@lang('website_contents.checkout.payment_summary.apply')</b></button>
                                </div>
                                <p class="error-promotion" id="error-promotion"></p>
                                <p class="success-promotion" id="success-promotion"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 bg-dark shadow-sm padd15 text-white">
                    <div class="row mr-0 ml-0">
                        <div class="col-6 padd0">
                            <p class="MuliExtraBold fs-20 mb-0">@lang('website_contents.checkout.payment_summary.total')</p>
                        </div>
                        <div class="col-6 padd0 text-right">
                             <!-- Currency and price for kr -->
                        @if(strtolower($currentCountryIso) == 'kr')
                        <p class="MuliExtraBold fs-20 mb-0"><span id="c-total">{{number_format($current_tprice) }}</span>{{$currency}}</p>
                        @else
                        <p class="MuliExtraBold fs-20 mb-0">{{$currency}} <span id="c-total">{{number_format($current_tprice, 2, '.', '') }}</span></p>
                        @endif
                        </div>
                    </div>
                </div>
                <!-- End: Desktop Payment Summary -->

                <br>

                <!-- Start: Desktop Next Billing Summary -->
                <div class="col-12 rounded-t-10 bg-white shadow-sm pr-4 pl-4 pt-3">
                    <div class="row mr-0 ml-0">
                        <h6 class="MuliExtraBold text-uppercase">@lang('website_contents.checkout.payment_summary.next_delivery')</h6>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom">
                            <div class="row">
                                <div class="col-5 padd0 d-flex align-items-center">
                                    <img class="img-fluid custom-image" src="{{asset('/images/common/shaveplan.png')}}" />
                                </div>
                                <div class="col-7 padd0 d-flex align-items-center">
                                    <div class="col-12 padd0">
                                        <h3 class="MuliBold">@lang('website_contents.checkout.payment_summary.shave_plan')</h3>
                                        <?php

$nplandescriptionexplode  = $nplandescription;
if ($nplandescriptionexplode) {
    $npdescription = explode(",", $nplandescriptionexplode);
    foreach ($npdescription as $value) {
        if (strpos($value, 'Month') !== false) {
            $value = str_replace('Months', "", $value);
            $value = str_replace('Month', "", $value);
            $value = str_replace(' ', "", $value);
            echo "<i class='fa fa-refresh' aria-hidden='true'><span class='fs-18 Muli MuliPlain m-0'> ".trans('website_contents.every') . $value .trans('website_contents.Months') . "</span></i>";
        } else {
            echo "<p class='fs-18 Muli' style='margin: 0;'>" . $value . "</p>";
        }
    }
}
                                        // $plandescriptionexplode  = $nplandescription;
                                        // if ($plandescriptionexplode) {
                                        //     $pdescription = explode(",", $plandescriptionexplode);
                                        //     $size = count($pdescription);
                                        //     echo "<span class='fs-18 Muli' style='margin: 0;'>";
                                        //     foreach ($pdescription as $key => $value) {
                                        //         if (strpos($value, 'Month') !== false) {
                                        //             //do nothing
                                        //         } else {
                                        //             echo $value;
                                        //             if ($key < ($size - 1)) {
                                        //                 echo ", ";
                                        //             }
                                        //         }
                                        //     }
                                        //     echo "</span>";
                                        // }

                                        ?>
                                        <p class='fs-18 Muli summary-number' style='margin: 0;'><span id='c-nfree-product'></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 padd0 pt-3">
                                     <!-- Currency and price for kr -->
                                @if(strtolower($currentCountryIso) == 'kr')
                                <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.subtotal') <span class="pull-right summary-number"><span id="c-nsubtotal">{{number_format($next_price, 2, '.', '')}}</span>{{$currency}}</span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.shipping') <span class="pull-right summary-number"><span id="c-shipping">{{$s_fee}}</span>{{$currency}}</span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.discount') <span class="pull-right summary-number"><span id="c-ndiscount">{{number_format($ndiscount, 2, '.', '')}}</span>{{$currency}}</span></label>
                                @else
                                <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.subtotal') <span class="pull-right">{{$currency}} <span id="c-nsubtotal">{{$next_price, 2, '.', ''}}</span></span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.shipping') <span class="pull-right">{{$currency}} <span id="c-shipping">{{$s_fee}}</span></span></label>
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.discount') <span class="pull-right">{{$currency}} <span id="c-ndiscount">{{number_format($ndiscount, 2, '.', '')}}</span></span></label>
                                @endif
                                    <!-- <p class="d-none" id="c-nfree-product"></p> -->
                                    <p class="d-none" id="c-nfree-exist-product"></p>
                            </div>
                        </div>
                        <hr>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                            <div class="row mr-0 ml-0">
                                <div class="col-12 padd0">
                                     <!-- Currency and price for kr -->
                                @if(strtolower($currentCountryIso) == 'kr')
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.total') <span class="pull-right summary-number"><span id="c-ntotal">{{number_format($next_tprice, 2, '.', '')}}</span>{{$currency}}</span></label>
                                @else
                                    <label class="col-12 padd0">@lang('website_contents.checkout.payment_summary.total') <span class="pull-right">{{$currency}} <span id="c-ntotal">{{number_format($next_tprice, 2, '.', '')}}</span></span></label>
                                @endif
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-12 padd20 pl-0 pr-0 border-bottom fs-18">
                            <div class="row mr-0 ml-0">
                                <div class="col-12 padd0">
                                    @if($isAnnual == '1')
                                    <i class="fa fa-refresh" aria-hidden="true"><span class="col-12 padd0 MuliPlain"><b> @lang('website_contents.checkout.payment_summary.deliver_frequency', ['planFrequency' => 12])<b></span></i>
                                    @else
                                <i class="fa fa-refresh" aria-hidden="true"><span class="col-12 padd0 MuliPlain"><b> @lang('website_contents.checkout.payment_summary.deliver_frequency', ['planFrequency' => $planFrequency])<b></span></i>
                               @endif
                            </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End: Desktop Next Billing Summary -->

                @if($loginStatus === "post-login")
                <div class="col-12 text-center pt-4 pl-0 pr-0">

                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- END: DESKTOP LAYOUT -->

    <!--Proceed!-->
    <div class="col-12 padd0 d-lg-none">
        <!-- <div class="col text-center pt-lg-5 pl-0 pr-0">
            <button id="button-next" class="button-proceed-checkout MuliExtraBold" type="submit">NEXT</button>
        </div> -->
        <form id="form-checkout" class="d-none" method="GET" action="{{ route('locale.region.shave-plans.trial-plan.thankyou', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
            {{ csrf_field() }}
            <div class="col text-center padd0">
                <!-- <input type="hidden" name="handle" id="handle" value="test"> -->
                <button id="proceed-pay" class="button-proceed-checkout" type="submit">PROCEED TO CHECKOUT</button>
            </div>
        </form>
        <br>
        <button class="button-proceed-checkout" id="check" style="display:none">CHECK</button>
        <button class="button-proceed-checkout" id="clear" style="display:none">CLEAR SESSION</button>
    </div>
    <!-- Payment Failed Popup -->
    <div class="modal fade" id="notification_payment_failed" tabindex="-1" role="dialog" aria-labelledby="notification_payment_failed" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="col-12 text-center">
                        <h5 id="notification_payment_failed_text"></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

         <!-- mask selection -->
         <div class="modal fade" id="model-mask-selection" tabindex="-1" role="dialog" aria-labelledby="model-mask-selection" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered model-mask-selection" role="document">
            <div class="modal-content">
            <div class="modal-header" style="border: none ;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                <div class="modal-body pl-5 pr-5">
                    <div class="col-12 text-center">
                        <!-- <h5 id="notification_payment_failed_text">{{$countmaskqty}}</h5> -->
                        <h4 class="MuliExtraBold">Stay Safe</h4>
                        <h6 class="MuliPlain pt-2">In the midst of this Covid 19 season, your health and wellness is always one of our main priorities. As a token of thanks, we are offering you a <b>FREE</b> complimentary coloured face mask with your purchase.</h6>
                        <h6 class="MuliPlain pb-4">Kindly select a face mask colour below:</h6>
                      <div class="row">
                        <div class="col-6">
                          <img id="selection-mask-img" src="" class="img img-responsive img-scale" style="width:60%;"/>
                          <ul>
                          <li class="MuliPlain" style="text-align:left;">Reuse by simple cleaning</li>
                          <li class="MuliPlain" style="text-align:left;">Anti-bacterial raw materials</li>
                          <li class="MuliPlain" style="text-align:left;">Deodorization effect</li>
                          <li class="MuliPlain" style="text-align:left;">3D design for comfortable fit</li>
                          <li class="MuliPlain" style="text-align:left;">Dust-proof function</li>
                        </ul>
                        </div>
                        <div class="col-6">
                        <div class="col-md-12 pt-0 pr-0 pl-0 pb-3">
             
                        <!-- <h4><b>@lang('website_contents.selection.select_blade')</b></h4> -->
                        <div class="col-md-12 pr-0 pl-0 pt-0">
                        @if($maskselection)
                            @foreach($maskselection as $ms)
                            <div class="pl-3 pr-3 pb-4 pt-0">
                            @php($pname = str_replace(array( '(', ')' ), '', $ms['name']))
                            @php($pname = str_replace("Face Mask","",$pname))
          
                                @if($ms['qty'] <= 0)
                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected-no-stock">
                                    <div class="col-md-12 pr-0 pl-0">
                                        <h6 style="margin:0;" ><b>{{$pname}} (out of stock)</b></h6>
                                    </div>
                                </div>
                                @else
                                <div class="row pr-0 pl-0 ml-0 p-2 item-unselected <?php echo "maskselection-" . $ms['pcid']  ?>">
                                    <div class="col-md-12 pr-0 pl-0">
                                        <h5 style="margin:0;" ><b>{{$pname}}</b></h5>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @endforeach
                            @endif
                        </div>
                      </div>
                      </div>
                <div class="modal-footer-btn col-12" style="padding-top:5%;">
                <button onclick="submitWithoutMask();" class="button-proceed-checkout-cancel MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 35%; margin-right:2%;">@lang('website_contents.selection.change-to-annual-model.cancel')</button>
                <button id="button-proceed-checkout" onclick="submitWithMask();" class="button-proceed-checkout MuliExtraBold text-uppercase" data-dismiss="modal" aria-label="Close" type="button" style="width: 35%; margin-left:2%;">Confirm</button>
                </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- mask selection -->
</div>

<script>
// var cleave = new Cleave('.credit-card-logo', {
//     creditCard: true,
//     onCreditCardTypeChanged: function (type) {
//         if (type === "visa") {
//             $(".add-logo-change").html("<img src='"+ visa +"' class='img-fluid' style='position:absolute; bottom:0; padding-bottom: 1rem!important; max-width:65% !important;' />");
//         } else if (type === "mastercard") {
//             $(".add-logo-change").html("<img src='"+ mastercard +"' class='img-fluid' style='position:absolute; bottom:0; padding-bottom: 1rem!important; max-width:65% !important;' />");
//         } else if (type === "amex") {
//             $(".add-logo-change").html("<img src='"+ amex +"' class='img-fluid' style='position:absolute; bottom:0; padding-bottom: 1rem!important; max-width:65% !important;'/>");
//         } else if (type == "unknown") {
//             $(".add-logo-change").html("");
//         }
//     }
// });
</script>


<script>
      let visa =  "<?php echo asset('/images/cards/logo-visa.png'); ?>";
      let mastercard =  "<?php echo asset('/images/cards/logo-mastercard.png'); ?>";
      let amex =  "<?php echo asset('/images/cards/logo-amex.png'); ?>";
      let country_id = "<?php echo $currentCountryIso; ?>";
      let countryid = {!! json_encode($currentCountryid) !!};
      let langCode = {!! json_encode($langCode) !!};
      let urllangCode = {!! json_encode($urllangCode) !!};
      let user_id = {!!json_encode($user_id)!!} !== null ? "<?php echo $user_id; ?>" : null;
      let user = {!!json_encode($user)!!} !== null ? {!!json_encode($user)!!} : null;
      let delivery_address = {!!json_encode($delivery_address)!!} !== null ? {!!json_encode($delivery_address)!!} : null;
      let billing_address = {!!json_encode($billing_address)!!} !== null ? {!!json_encode($billing_address)!!} : null;
      let session_data = {!! json_encode($session) !!};
      let default_card = {!!json_encode($default_card)!!} !== null ? {!!json_encode($default_card)!!} : null;
      let payment_intent = {!!json_encode($payment_intent)!!} !== null ? {!!json_encode($payment_intent)!!} : null;
      let checkout_details = {!!json_encode($checkout_details)!!} !== null ? {!!json_encode($checkout_details)!!} : null;
      let cardget  = {!!json_encode($cards)!!} !== null ? {!!json_encode($cards)!!} : null;
      let current_price  = <?php echo $current_totalprice; ?>;
      let next_price  = <?php echo $next_totalprice; ?>;
      let shipping_fee  = <?php echo $shipping_fee; ?>;
      let taxRate  = <?php echo $taxRate; ?>;
      let ctaxAmount  = <?php echo $taxAmount; ?>;
      let cards = {!!json_encode($cards) !!};
      let loginStatus = {!! json_encode($loginStatus) !!};
      let formMode = "register";
      let current_country_from_session = {!!json_encode($currentCountry)!!};
    // mask selection
    let maskselection = {!! json_encode($maskselection) !!};
    let countmaskqty = <?php echo $countmaskqty; ?>;
    // mask selection
      let loginvalidation = "@lang('website_contents.checkout.content.login_validation')";
      let promotionFreeproduct = "@lang('website_contents.checkout.free-product')";
      let sessionLoginError  = <?php echo "'".$sessionLoginError."'"; ?>;
      let oridiscountamount  = <?php echo $discount; ?>;
      let noridiscountamount  = <?php echo $ndiscount; ?>;
      let current_tprice  = <?php echo $current_tprice; ?>;
      let next_tprice  = <?php echo $next_tprice; ?>;
</script>



<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script src="{{ asset('js/functions/custom-plan/custom-plan-checkout.function.js') }}"></script>
<script src="{{ asset('js/functions/promotion/promotions.function.js') }}"></script>
@endsection
