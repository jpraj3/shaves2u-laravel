@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{ asset('css/landing/landing.css') }}">
    <style>
        .landing-banner-desktop-swivel-img {
            background-image: url('{{asset("/images/common/banner-menlanding.jpg")}}')
        }
        .landing-banner-tablet-swivel-img {
            background-image: url('{{asset("/images/common/banner-menlanding-tablet.jpg")}}');
            width: 100vw;
            height: 40vh;
        }
        .landing-banner-mobile-swivel-img {
            background-image: url('{{asset("/images/common/banner-menlanding-tablet.jpg")}}');
            width: 100vw;
            height: 35vh;
        }

        .landing-banner-desktop-premium-img {
            background-image: url('{{asset("/images/common/premium/banner/banner-menlanding-premium.jpg")}}')
        }
        .landing-banner-tablet-premium-img {
            background-image: url('{{asset("/images/common/premium/banner/banner-menlanding-tablet-premium.jpg")}}');
            width: 100vw;
            height: 40vh;
        }
        .landing-banner-mobile-premium-img {
            background-image: url('{{asset("/images/common/premium/banner/banner-menlanding-tablet-premium.jpg")}}');
            width: 100vw;
            height: 35vh;
        }
    </style>
    @php($m_h = null)
    @php($m_h_sku = null)
    @if(Session::has('country_handles'))
    @php($m_h = Session::get('country_handles'))
    @php($m_h_sku = $m_h->sku)
    @endif
    <section id="landing_banner">
    <div id="landing-banner-child" class="col-md-12 landing-banner {{$m_h_sku !== null && $m_h_sku == 'H1' ? 'landing-banner-desktop-premium-img' : 'landing-banner-desktop-swivel-img'}}">
            <div class="col-md-12 d-none d-lg-block">
                @if($currentCountry->id === 8)
                <div class="col-sm-8 offset-lg-1 col-lg-5 text-center" style="padding-top:12%;padding-left:5%;">
                    <h2 class="MuliExtraBold product-h-black pb-1 mb-0 text-overflow-center"> @lang('website_contents.landing.hero_section.better_shave')</h2>
                    <h1 class="MuliExtraBold product-sub-h-black pb-2">@lang('website_contents.landing.hero_section.landing_trial_price',['currency' => $currentCountry->currencyDisplay, 'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso)) ])</h1>
                    <h2 class="MuliExtraBold product-h-black pb-1 mb-0 text-overflow-center">@lang('website_contents.landing.hero_section.start_shave')</h2>
                    <br/>
                    <a href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                       class="btn-start btn-custom-banner text-uppercase" style="" onClick="">@lang('website_contents.global.header.get_started')</a>
                    <a href="{{route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso])}}"
                       style="color:#000000; text-decoration: underline;font-size:18px;">@lang('website_contents.landing.hero_section.looking_blade_refills')</a>
                </div>
                @else
                <div class="col-sm-8 offset-lg-1 col-lg-5 text-center" style="padding-top:12%;padding-left:5%;">
                        <h2 class="MuliExtraBold product-h-black pb-1 mb-0 text-overflow-center">
                            @lang('website_contents.landing.hero_section.better_shave')
                            <br/>
                            @lang('website_contents.landing.hero_section.start_shave')
                        </h2>
                        <h1 class="MuliExtraBold product-sub-h-black pb-2">@lang('website_contents.landing.hero_section.landing_trial_price',['currency' => $currentCountry->currencyDisplay, 'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso)) ])</h1>
                        <a href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                           class="btn-start btn-custom-banner text-uppercase" style="" onClick="">@lang('website_contents.global.header.get_started')</a>
                        <a href="{{route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso])}}"
                           style="color:#000000; text-decoration: underline;font-size:18px;">@lang('website_contents.landing.hero_section.looking_blade_refills')</a>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section id="landing-wording-for-mobile" class="d-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12" style="text-align:center;padding-top:2%;">
                    <h3 class="bold-max" style="margin-top: 2%; font-family: Muli-Bold;font-size:1.2rem;">
                        @lang('website_contents.landing.hero_section.better_shave')
                    </h3>
                    <h3 class="bold-max"
                        style="margin-bottom: 2%; margin-top: 1%; font-family: Muli-Bold;font-size:1.2rem;">
                        @lang('website_contents.landing.hero_section.start_shave')
                    </h3>
                    <h2 style="font-family: Muli-ExtraBold;padding-bottom:10px;font-size:4rem;">@lang('website_contents.global.trial_price',['currency' => $currentCountry->currencyDisplay, 'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso)) ])</h2>
                    <a href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                       class="btn-start btn-custom text-uppercase" style="">@lang('website_contents.global.header.get_started')</a>
                    <a href="{{route('locale.region.shave-plans.custom-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>$currentCountryIso])}}"
                       style="color:#000000; text-decoration: underline;font-size:20px;">@lang('website_contents.landing.hero_section.looking_blade_refills')</a>
                </div>
            </div>
        </div>
    </section>
    <section id="how-it-works" class="landing-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-12 text-center pr-0 pl-0">
                                <h2 class="bold-max"
                                    style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold">
                                    <strong>@lang('website_contents.landing.start_trial.how_it_works')</strong></h2>
                            </div>
                            <div class="col-12 d-none d-lg-block" style="text-align:center;color:#828282;font-size:18px;">
                                @lang('website_contents.landing.start_trial.shaving_experience')
                                <br/>
                                @lang('website_contents.landing.start_trial.get_started_steps')
                            </div>
                        </div>
                        <div class="col-12 d-block d-lg-none pb-5"
                             style="text-align:center;color:#828282;font-size:18px;">
                             @lang('website_contents.landing.start_trial.shaving_experience') @lang('website_contents.landing.start_trial.get_started_steps')
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row text-sm-left">
                            <div class="col-sm-5 pt-md-5 pl-md-5 pr-md-5 pb-md-0 order-md-2 order-lg-2 order-xl-2">
                                <div id="buildPlan" class="img-fluid wow-buildplan zoomIn"></div>
                            </div>
                            <div class="col-sm-6 margT4rem paddLR5rem-md order-md-1 order-lg-1 order-xl-1">
                                <p style="font-family: Muli-SemiBold;color:#bc654a;font-size:18px;margin-bottom:0px;">
                                    @lang('website_contents.landing.start_trial.step_1')
                                </p>
                                <p style="font-family: Muli-Bold;font-size:25px;">@lang('website_contents.landing.start_trial.step_1_build_plan')</p>
                                <p style="font-family: Muli;font-size:18px;color:#828282;">@lang('website_contents.landing.start_trial.step_1_desc')</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row text-sm-left">
                            <div class="col-sm-5 col-sm-5 pt-md-0 pl-md-5 pr-md-5 pb-md-0">
                                <div id="twoWeeks" class="img-fluid wow-twoweeks zoomIn"></div>
                            </div>
                            <div class="col-sm-6 margT4rem paddLR5rem-md">
                                <p style="font-family: Muli-SemiBold;color:#bc654a;font-size:18px;margin-bottom:0px;">
                                    @lang('website_contents.landing.start_trial.step_2')
                                </p>
                                <p style="font-family: Muli-Bold;font-size:25px;">@lang('website_contents.landing.start_trial.step_2_try')</p>
                                <p style="font-family: Muli;font-size:18px;color:#828282;">@lang('website_contents.landing.start_trial.step_2_desc')</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row text-sm-left">
                            <div class="col-sm-5 pt-md-0 pl-md-5 pr-md-5 pb-md-5 order-md-2 order-lg-2 order-xl-2">
                                <div id="control" class="img-fluid wow-control zoomIn"></div>
                            </div>
                            <div class="col-sm-6 margT4rem paddLR5rem-md order-md-1 order-lg-1 order-xl-1">
                                <p style="font-family: Muli-SemiBold;color:#bc654a;font-size:18px;margin-bottom:0px;">
                                    @lang('website_contents.landing.start_trial.step_3')
                                </p>
                                <p style="font-family: Muli-Bold;font-size:25px;">@lang('website_contents.landing.start_trial.step_3_control')</p>
                                <p style="font-family: Muli;font-size:18px;color:#828282;">@lang('website_contents.landing.start_trial.step_3_desc')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="not-typical" class="light-grey-bg landing-section">
        <div class="container">
            <div class="row-no-gutters">
                <div class="col-12 mb-5">
                    <div class="row">
                        <div class="col-md-12 text-center pr-0 pl-0">
                            <h2 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold">
                                <strong>@lang('website_contents.landing.start_trial.not_typical')</strong></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-md-3 margB2rem ml-0 mr-0">
                        <div class="row">
                            <div class="col-4 col-md-12 text-center p-md-4 mb-3">
                                <img src="{{URL::asset('/images/common/icon-m-quality.png')}}" class="img-fluid wow-nottypical zoomIn"/>
                            </div>
                            <div class="col-8 col-md-12 text-md-center">
                                <p class="color-orange fs-25"><strong>@lang('website_contents.landing.start_trial.quality')</strong></p>
                                <p class="fs-16" style="color:#828282;">@lang('website_contents.landing.start_trial.razor_blades')</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 margB2rem  ml-0 mr-0">
                        <div class="row">
                            <div class="col-4 col-md-12 text-center p-md-4 mb-3">
                                <img src="{{URL::asset('/images/common/icon-m-affordable.png')}}" class="img-fluid wow-nottypical zoomIn"/>
                            </div>
                            <div class="col-8 col-md-12 text-md-center">
                                <p class="color-orange fs-25"><strong>@lang('website_contents.landing.start_trial.affordable')</strong></p>
                                <p class="fs-16" style="color:#828282;">@lang('website_contents.landing.start_trial.savings_40_leading')</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 margB2rem ml-0 mr-0">
                        <div class="row">
                            <div class="col-4 col-md-12 text-center p-md-4 mb-1">
                                <img src="{{URL::asset('/images/common/icon-m-convenience.png')}}" class="img-fluid wow-nottypical zoomIn"/>
                            </div>
                            <div class="col-8 col-md-12 text-md-center">
                                <p class="color-orange fs-25"><strong>@lang('website_contents.landing.start_trial.convenience')</strong></p>
                                <p class="fs-16" style="color:#828282;">@lang('website_contents.landing.start_trial.control_products')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        @media (max-width: 1024px) {

        }
    </style>
    <div id="testimonials-container" class="d-none d-lg-block">
        <section id="testimonials" class="align-items-center d-flex"
                style="background-image: url('{{asset('/images/common/testimonial-section.jpg')}}'); height: 720px;">
            <div class="col-md-12 justify-content-center">
                <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators" style="top:9rem !important;">
                        <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#blogCarousel" data-slide-to="1"></li>
                        <li data-target="#blogCarousel" data-slide-to="2"></li>
                        <li data-target="#blogCarousel" data-slide-to="3"></li>
                        <li data-target="#blogCarousel" data-slide-to="4"></li>
                    </ol>

                    <div class="carousel-inner" style="height:150px;">

                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <h5 class="mb-3" id="testimonial-one">
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <h5 class="mb-3" id="testimonial-two">
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <h5 class="mb-3" id="testimonial-three">
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <h5 class="mb-3" id="testimonial-four">
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <h5 class="mb-3" id="testimonial-five">
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="testimonials-t-container" class="d-none d-md-block d-lg-none">
    <section id="testimonials-tablet" class="align-items-center">
            <div class="col-12 justify-content-center" style="height: 135px;padding:0px;">
                <img class="img img-responsive img-fluid" src="{{asset('/images/common/banners/top_tablet_testimony.png')}}" />
            </div>
            <div class="col-12 justify-content-center" style="background-color:grey;height: 240px;padding: 5% 5% 5% 5%;">
                <div id="blogCarousel-t" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators" style="top:9rem !important;">
                        <li data-target="#blogCarousel-t" data-slide-to="0" class="active"></li>
                        <li data-target="#blogCarousel-t" data-slide-to="1"></li>
                        <li data-target="#blogCarousel-t" data-slide-to="2"></li>
                        <li data-target="#blogCarousel-t" data-slide-to="3"></li>
                        <li data-target="#blogCarousel-t" data-slide-to="4"></li>
                    </ol>

                    <div class="carousel-inner" style="height:150px;">

                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-one-t">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-two-t">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-three-t">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-four-t">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-five-t">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 justify-content-center" style="height: 135px;padding:0px;">
                <img class="img img-responsive img-fluid" src="{{asset('/images/common/banners/bottom_tablet_testimony.png')}}" />
            </div>
        </section>
    </div>

    <div id="testimonials-m-container" class="d-none d-block d-sm-none">
    <section id="testimonials-mobile" class="align-items-center">
            <div class="col-12 justify-content-center" style="height: 120px;padding:0px;">
                <img class="img img-responsive img-fluid" src="{{asset('/images/common/banners/top_mobile_testimony.png')}}" />
            </div>
            <div class="col-12 justify-content-center" style="background-color:grey;height: 240px;padding: 12% 5% 20% 5%;">
                <div id="blogCarousel-m" class="carousel slide" data-ride="carousel">

                    <ol class="carousel-indicators" style="top:9rem !important;">
                        <li data-target="#blogCarousel-m" data-slide-to="0" class="active"></li>
                        <li data-target="#blogCarousel-m" data-slide-to="1"></li>
                        <li data-target="#blogCarousel-m" data-slide-to="2"></li>
                        <li data-target="#blogCarousel-m" data-slide-to="3"></li>
                        <li data-target="#blogCarousel-m" data-slide-to="4"></li>
                    </ol>

                    <div class="carousel-inner" style="height:150px;">

                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-one-m">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-two-m">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-three-m">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-four-m">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-12 text-center color-white">
                                    <p style="font-size: 16px;" class="mb-3" id="testimonial-five-m">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 justify-content-center" style="height: 120px;padding:0px;">
                <img class="img img-responsive img-fluid" src="{{asset('/images/common/banners/bottom_mobile_testimony.png')}}" />
            </div>
        </section>
    </div>




    <section id="stop-the-razor" class="landing-section light-grey-bg">
        <div class="container">
            <div class="col-12">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="col-md-12 text-center pr-0 pl-0 description">
                            <h2 class="bold-max"
                                style="margin-bottom: 5%; font-family: Muli-ExtraBold">
                                <strong>@lang('website_contents.landing.start_trial.stop_ripoff')</strong>
                            </h2>
                            <p class="fs-18" style="margin-bottom: 1%; margin-top: 2%; font-family: Muli">@lang('website_contents.landing.start_trial.sell_direct')</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 text-center">
                        <div id="priceBar" class="img-fluid wow-pricebar zoomIn"></div>
                        <h4><b>@lang('website_contents.landing.start_trial.5blade_pack')</b></h4>
                        <p class="fine-print">@lang('website_contents.landing.start_trial.average_price')</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="landing-get-started landing-orange-bg landing-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 pr-0 pl-0">
                    @if($m_h_sku == 'H3')
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                    @elseif($m_h_sku = 'H1')
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                    @else
                    <img alt="Learn More" class="img-fluid" src="{{URL::asset('/images/common/img-getstarted.png')}}">
                    @endif
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-center pr-3 pl-3">
                    <h2 class="bold-max get-started-font" style=""><strong>@lang('website_contents.landing.start_trial.get_underway')<br></strong>
                    </h2>
                    <p class="get-started-sub-font pb-xl-4">@lang('website_contents.landing.start_trial.new_shaving_experience',['currency' => $currentCountry->currencyDisplay, 'price' => config('global.all.general_trial_price.'.strtoupper($currentCountry->codeIso)) ])<br></p>
                    <a href="{{ route('locale.region.shave-plans.trial-plan.selection', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}"
                       class="btn-start btn-custom MuliBold text-uppercase" style="font-size:2rem;">@lang('website_contents.global.header.get_started')</a>
                </div>
            </div>
        </div>
    </section>
    <section id="need-to-know" class="landing-section">
        <div class="container" id="faq">
            <div class="col-md-12 text-center pr-0 pl-0">
                <h2 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>@lang('website_contents.global.faq.know_more')<br></strong></h2>
                <p class="fs-18" style="margin-bottom: 8%; margin-top: 2%; font-family: Muli; color: #828282;">@lang('website_contents.global.faq.curious')</p>
            </div>
            <!--Accordion wrapper-->
            <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                    <!-- Card header -->
                    <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingOne1"
                         style="background-color:initial !important;">
                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                           aria-controls="collapseOne1">
                           <div class="row">
                                <div class="col-10">
                                    <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                        @lang('website_contents.global.faq.how_sp_work')
                                    </h5>
                                </div>
                                <div class="col-2">
                                    <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                </div>
                            </div>
                        </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                         data-parent="#accordionEx">
                        <div class="card-body Muli-card-body text-left" style="padding-top: 0;">
                            @lang('website_contents.global.faq.how_sp_work_ans')
                        </div>
                    </div>

                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                    <!-- Card header -->
                    <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingTwo2"
                         style="background-color:initial !important;">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                           aria-expanded="false" aria-controls="collapseTwo2">
                            <div class="row">
                                <div class="col-10">
                                    <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                    @lang('website_contents.global.faq.how_long_arrive')
                                    </h5>
                                </div>
                                <div class="col-2">
                                    <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                </div>
                            </div>
                        </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                         data-parent="#accordionEx">
                        <div class="card-body Muli-card-body text-left" style="padding-top: 0;">
                            @lang('website_contents.global.faq.how_long_arrive_ans')
                        </div>
                    </div>

                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                    <!-- Card header -->
                    <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingThree3"
                         style="background-color:initial !important;">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                           aria-expanded="false" aria-controls="collapseThree3">
                            <div class="row">
                                <div class="col-10">
                                    <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">

                                @lang('website_contents.global.faq.which_locations')
                                    </h5>
                                </div>
                                <div class="col-2">
                                    <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                </div>
                            </div>
                        </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                         data-parent="#accordionEx">
                        <div class="card-body Muli-card-body text-left" style="padding-top: 0;">
                            @lang('website_contents.global.faq.which_locations_ans')
                        </div>
                    </div>

                </div>
                <!-- Accordion card -->

                <!-- Accordion card -->
                <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                    <!-- Card header -->
                    <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFour4"
                         style="background-color:initial !important;">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                           aria-expanded="false" aria-controls="collapseFour4">
                            <div class="row">
                                <div class="col-10">
                                    <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                        @lang('website_contents.global.faq.offer_free_delivery')
                                    </h5>
                                </div>
                                <div class="col-2">
                                    <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                </div>
                            </div>
                        </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
                         data-parent="#accordionEx">
                        <div class="card-body Muli-card-body text-left" style="padding-top: 0;">
                            @lang('website_contents.global.faq.offer_free_delivery_ans')
                        </div>
                    </div>

                </div>
                <!-- Accordion card -->

            </div>
            <!-- Accordion wrapper -->
        </div>
    </section>
    <section class="landing-free-text">
        <div class="container" id="free-text">
            {{--        <div class="col-md-12 text-center pr-0 pl-0">--}}
            {{--            <h2 class="bold-max d-none d-md-block" style="margin-bottom: 2%; margin-top: 2%; color:white; font-family: Muli-ExtraBold"><strong>SEO META--}}
            {{--                    TEXT<br></strong></h2>--}}
            {{--            <h2 class="bold-max d-md-none" style="margin-bottom: 10%; margin-top: 2%; font-family: Muli-Bold">--}}
            {{--                <strong>SEO META TEXT<br></strong></h2>--}}
            {{--        </div>--}}
            <div class="col-md-12 pr-0 pl-0">
                <p class="ftcolor-swap fs-12 text-left" style="margin-top: 2%; color:white;">
                    @lang('website_contents.landing.start_trial.free_text')
                </p>
            </div>
        </div>
    </section>


    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar-hkd-chi.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar-hkd-eng.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar-kor.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar-sgd.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar-tw.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-buildPlan.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-2weeks.json')}}"></script>
    <script type="text/javascript" src="{{asset('/json/animation/data-control.json')}}"></script>

    <script>
        let buildplans_wowEffect = 0;
        let twoweeks_wowEffect = 0;
        let control_wowEffect = 0;
        let notTypical_wowEffect = 0;
        let priceBar_wowEffect = 0;

        let countryCode = JSON.parse(window.localStorage.getItem('currentCountry'));

        var testimonials = new Array();
        testimonials[0] = 'Weekly routine. #shaves2u @shaves2u<br><br><span style="font-size:16px;">F O O 王振富</span>';
        testimonials[1] = 'Do you shave daily? Well I do! If I don’t do it daily, the hair grows back really fast. Just recently I’ve come across @shaves2u, giving me the flexibility to shave more frequently with the same blades due to their high quality materials<br>Best part is, they’re delivered right to my doorstep!<br><br><span style="font-size:16px;">isaactanjs</span>';
        testimonials[2] = 'Sama tapi tak serupa ehh! Selepas menggunakan @shaves2u Shaving Trial Kit... Bercukur kini semudah 1-2-3 je ok! Bulu2 janggut & misai yang tertinggal melekat di celah2 blade dah senang dibilas dengan air, tak seperti jenaman pencukur yang lain... Shaving cream dia pun wangi & sangat membantu.... Thank you Shaves2U! Boleh dapatkan shaving kit anda dengan harga semurah RM6.50 je ok! Siyesly berbaloi2 untuk yang nak trim janggut, misai & bulu2 yang sewaktu dengannya gitchuu... Lepas ni tiada alasan lagi untuk bercukur dengan mudah pantas & sangat convenient di mana jua anda berada eh! #shaves2u #shaver #shaving #movember #cleanshave #easypeasy #freshlook<br><br><span style="font-size:16px;">aizatyanan</span>';
        testimonials[3] = 'Benda boleh dapat sama kualiti tapi murah. Pengganti gillette<br>#shaves2u<br><br><span style="font-size:16px;">hazwan01</span>';
        testimonials[4] = 'Ramai yang tanya, apa kelebihan guna servis shaver plan daripada Shaves2U? Selain daripada dapat Trial Kit untuk tempoh percubaan 14 hari dan penghantaran ke depan pintu, Shaves2U juga membolehkan kita jimat sehingga 40% berbanding penggunaan shaver dari brand-brand lain di pasaran. Jimat masa, jimat duit!<br><br><span style="font-size:16px;">penaberkala</span>';
        testimonials[5] = 'S H A V E S <br> To all the guys out there that are looking for an alternative choice for shavers. Always love it when details are being paid attention to, be it the products functionality or the overall marketing strategy. Check their social media and site out for more details! Thank you @shaves2u 🤓🙆🏻‍♂';
        testimonials[6] = '#MrWorkHardPlayHarder #Shaves2U<br><br><span style="font-size:16px;">Roen Cian</span>';
        testimonials[7] = 'Counting down to 2019,<br>Let’s shave for a shaper looking you!<br>Thanks @shaves2u for this premium great quality shaver!<br>Help me to minimise all the razor burns while I shave myself to a sharp looking year of 2019..<br>Get yourself one today !<br><br><span style="font-size:16px;">Aaryonstar</span>';
        testimonials[8] = '夠我用一年的剃鬚套裝來了，看來沒理由再留“頹廢Look”了啦~🧔🏽🙅🏽‍♂️🤣<br>各位男士朋友們，如果有常用手剃刀的話，不妨試試看 @shaves2u ，跟我之前用過的牌子來比，這個的剃刀用得比較順暢，剃完後也比較乾淨光滑多了~👍<br> 無論要每1到3~4個月的配套，或者像我一樣拿整盒Gift Box，或者要免費拿一套Trial Set(Trial不包括郵費RM6.50)，都可以任你們選擇，整體的價錢對比也會比較划算多了~👌<br> 除此之外，也可以用我的refer link獲得RM20的Credit折扣：<br> https://shaves2u.com/shave-plans/free-trial?ref=s2u-42UjNwgdcB&src=link<br><br><span style="font-size:16px;">Raymond Kong</span>';
        testimonials[9] = '깔끔하게 면도하고 기분좋은 하루 스타또<br>나는 깔끔한 남좌니까 ✌️✌️<br>(몸에 힘준거 안..비밀😂)<br>오늘도 좋은하루 보내잣💛<br><br><span style="font-size:16px;">park Jungbeom</span>';
        testimonials[10] = 'LOOKING ALL GOOD 😎<br>•   Nothing beats the feeling of smooth, stubble-less skin after a good shave. (Not to mention your wife will thank you for that too<br>•   The three blades in the @shaves2u shaver ensures that you get a close shave every single time.<br>•   The shaving cream has a great consistency that does not drip upon application. Together, they are the best shaving combo every man can ask for!<br>•   Get your starter kits from @shaves2u today!<br>•   Apologies for showing a kinda topless pic 😝😝><br><br><span style="font-size:16px;">shukorzailan</span>';

        var testimonials_mobile = new Array();
        testimonials_mobile[0] = 'Weekly routine. #shaves2u @shaves2u<br><br><span style="font-size:16px;">F O O 王振富</span>';
        testimonials_mobile[1] = 'Do you shave daily? Well I do! If I don’t do it daily, the hair grows back really fast. Just recently I’ve come across @shaves2u, giving me the flexibility to shave more frequently with the same blades due to their high quality materials<br>Best part is, they’re delivered right to my doorstep!<br><br><span style="font-size:16px;">isaactanjs</span>';
        testimonials_mobile[2] = 'Benda boleh dapat sama kualiti tapi murah. Pengganti gillette<br>#shaves2u<br><br><span style="font-size:16px;">hazwan01</span>';
        testimonials_mobile[3] = 'Ramai yang tanya, apa kelebihan guna servis shaver plan daripada Shaves2U? Selain daripada dapat Trial Kit untuk tempoh percubaan 14 hari dan penghantaran ke depan pintu, Shaves2U juga membolehkan kita jimat sehingga 40% berbanding penggunaan shaver dari brand-brand lain di pasaran. Jimat masa, jimat duit!<br><br><span style="font-size:16px;">penaberkala</span>';
        testimonials_mobile[4] = 'S H A V E S <br> To all the guys out there that are looking for an alternative choice for shavers. Always love it when details are being paid attention to, be it the products functionality or the overall marketing strategy. Check their social media and site out for more details! Thank you @shaves2u 🤓🙆🏻‍♂';
        testimonials_mobile[5] = '#MrWorkHardPlayHarder #Shaves2U<br><br><span style="font-size:16px;">Roen Cian</span>';
        testimonials_mobile[6] = 'Counting down to 2019,<br>Let’s shave for a shaper looking you!<br>Thanks @shaves2u for this premium great quality shaver!<br>Help me to minimise all the razor burns while I shave myself to a sharp looking year of 2019..<br>Get yourself one today !<br><br><span style="font-size:16px;">Aaryonstar</span>';
        testimonials_mobile[7] = '깔끔하게 면도하고 기분좋은 하루 스타또<br>나는 깔끔한 남좌니까 ✌️✌️<br>(몸에 힘준거 안..비밀😂)<br>오늘도 좋은하루 보내잣💛<br><br><span style="font-size:16px;">park Jungbeom</span>';

        $(document).ready(function() {
            var randomTestimonial1 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial2 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial3 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial4 = Math.floor(Math.random() * testimonials.length);
            var randomTestimonial5 = Math.floor(Math.random() * testimonials.length);

            var randomTestimonial1_mobile = Math.floor(Math.random() * testimonials_mobile.length);
            var randomTestimonial2_mobile = Math.floor(Math.random() * testimonials_mobile.length);
            var randomTestimonial3_mobile = Math.floor(Math.random() * testimonials_mobile.length);
            var randomTestimonial4_mobile = Math.floor(Math.random() * testimonials_mobile.length);
            var randomTestimonial5_mobile = Math.floor(Math.random() * testimonials_mobile.length);

            document.getElementById('testimonial-one').innerHTML = testimonials[randomTestimonial1];
            document.getElementById('testimonial-two').innerHTML = testimonials[randomTestimonial2];
            document.getElementById('testimonial-three').innerHTML = testimonials[randomTestimonial3];
            document.getElementById('testimonial-four').innerHTML = testimonials[randomTestimonial4];
            document.getElementById('testimonial-five').innerHTML = testimonials[randomTestimonial5];
            document.getElementById('testimonial-one-m').innerHTML = testimonials[randomTestimonial1_mobile];
            document.getElementById('testimonial-two-m').innerHTML = testimonials[randomTestimonial2_mobile];
            document.getElementById('testimonial-three-m').innerHTML = testimonials[randomTestimonial3_mobile];
            document.getElementById('testimonial-four-m').innerHTML = testimonials[randomTestimonial4_mobile];
            document.getElementById('testimonial-five-m').innerHTML = testimonials[randomTestimonial5_mobile];
            document.getElementById('testimonial-one-t').innerHTML = testimonials[randomTestimonial1];
            document.getElementById('testimonial-two-t').innerHTML = testimonials[randomTestimonial2];
            document.getElementById('testimonial-three-t').innerHTML = testimonials[randomTestimonial3];
            document.getElementById('testimonial-four-t').innerHTML = testimonials[randomTestimonial4];
            document.getElementById('testimonial-five-t').innerHTML = testimonials[randomTestimonial5];
        });

        $(function(){
            window.addEventListener("scroll", function (event) {
                let buildPlan = elementInViewport(document.getElementById("buildPlan"));
                let twoWeeks = elementInViewport(document.getElementById("twoWeeks"));
                let control = elementInViewport(document.getElementById("control"));
                let notTypical = elementInViewport(document.getElementById("not-typical"));
                let priceBar = elementInViewport(document.getElementById("priceBar"));

                var priceBarAnimation;
                var buildPlanAnimation;
                var twoWeeksAnimation;
                var controlAnimation;

                if(countryCode.codeIso == "MY") {
                    if(priceBar === true){
                        if(priceBar_wowEffect === 0){
                            var priceBarParams = {
                        container: document.getElementById('priceBar'),
                        renderer: 'svg',
                        autoplay: true,
                        animationData: priceBarData
                        };
                        priceBarAnimation = lottie.loadAnimation(priceBarParams);
                        priceBarAnimation.setSpeed(0.5);
                            priceBar_wowEffect++;
                            wow = new WOW(
                                {
                                    boxClass:     'wow-pricebar',
                                    animateClass: 'animated',
                                    offset:       250,
                                }
                            )
                            wow.init();
                        }
                    }
                } else if(countryCode.codeIso == "HK"){
                    if(countryCode.defaultLang == "EN" ){
                        if(priceBar_wowEffect === 0){
                            var priceBarParams = {
                            container: document.getElementById('priceBar'),
                            renderer: 'svg',
                            autoplay: true,
                            animationData: priceBarDataHKEng
                            };
                            priceBarAnimation = lottie.loadAnimation(priceBarParams);
                            priceBarAnimation.setSpeed(0.5);
                                priceBar_wowEffect++;
                                wow = new WOW(
                                    {
                                        boxClass:     'wow-pricebar',
                                        animateClass: 'animated',
                                        offset:       250,
                                    }
                                )
                            wow.init();
                        }

                    } else if(countryCode.defaultLang == "ZH-HK" ){
                        if(priceBar === true){
                            if(priceBar_wowEffect === 0){
                                var priceBarParams = {
                            container: document.getElementById('priceBar'),
                            renderer: 'svg',
                            autoplay: true,
                            animationData: priceBarDataHKCh
                            };
                            priceBarAnimation = lottie.loadAnimation(priceBarParams);
                            priceBarAnimation.setSpeed(0.5);
                                priceBar_wowEffect++;
                                wow = new WOW(
                                    {
                                        boxClass:     'wow-pricebar',
                                        animateClass: 'animated',
                                        offset:       250,
                                    }
                                )
                                wow.init();
                            }
                        }
                    }
                } else if(countryCode.codeIso == "SG"){
                    if(priceBar === true){
                        if(priceBar_wowEffect === 0){
                            var priceBarParams = {
                        container: document.getElementById('priceBar'),
                        renderer: 'svg',
                        autoplay: true,
                        animationData: priceBarDataSG
                        };
                        priceBarAnimation = lottie.loadAnimation(priceBarParams);
                        priceBarAnimation.setSpeed(0.5);
                            priceBar_wowEffect++;
                            wow = new WOW(
                                {
                                    boxClass:     'wow-pricebar',
                                    animateClass: 'animated',
                                    offset:       250,
                                }
                            )
                            wow.init();
                        }
                    }
                } else if(countryCode.codeIso == "KR"){
                    if(priceBar === true){
                        if(priceBar_wowEffect === 0){
                            var priceBarParams = {
                        container: document.getElementById('priceBar'),
                        renderer: 'svg',
                        autoplay: true,
                        animationData: priceBarDataKR
                        };
                        priceBarAnimation = lottie.loadAnimation(priceBarParams);
                        priceBarAnimation.setSpeed(0.5);
                            priceBar_wowEffect++;
                            wow = new WOW(
                                {
                                    boxClass:     'wow-pricebar',
                                    animateClass: 'animated',
                                    offset:       250,
                                }
                            )
                            wow.init();
                        }
                    }
                } else if(countryCode.codeIso == "TW"){
                    if(priceBar === true){
                        if(priceBar_wowEffect === 0){
                            var priceBarParams = {
                        container: document.getElementById('priceBar'),
                        renderer: 'svg',
                        autoplay: true,
                        animationData: priceBarDataTW
                        };
                        priceBarAnimation = lottie.loadAnimation(priceBarParams);
                        priceBarAnimation.setSpeed(0.5);
                            priceBar_wowEffect++;
                            wow = new WOW(
                                {
                                    boxClass:     'wow-pricebar',
                                    animateClass: 'animated',
                                    offset:       250,
                                }
                            )
                            wow.init();
                        }
                    }
                }

                if(buildPlan === true){
                    if(buildplans_wowEffect === 0){
                        var buildPlanParams = {
                    container: document.getElementById('buildPlan'),
                    renderer: 'svg',
                    loop: false,
                    autoplay: true,
                    animationData: buildPlanData
                    };
                    buildPlanAnimation = lottie.loadAnimation(buildPlanParams);
                        buildplans_wowEffect++;
                        wow = new WOW(
                            {
                                boxClass:     'wow-buildplan',
                                animateClass: 'animated',
                                offset:       250,
                            }
                        )
                        wow.init();
                    }
                }

                if(twoWeeks === true){

                    if(twoweeks_wowEffect === 0){
                        var twoWeeksParams = {
                    container: document.getElementById('twoWeeks'),
                    renderer: 'svg',
                    loop: false,
                    autoplay: true,
                    animationData: twoWeeksData
                    };
                    twoWeeksAnimation = lottie.loadAnimation(twoWeeksParams);
                        twoweeks_wowEffect++;
                        wow = new WOW(
                            {
                                boxClass:     'wow-buildplan',
                                animateClass: 'animated',
                                offset:       250,
                            }
                        )
                        wow.init();
                    }
                }

                if(control === true){

                    if(control_wowEffect === 0){
                        var controlParams = {
                    container: document.getElementById('control'),
                    renderer: 'svg',
                    loop: false,
                    autoplay: true,
                    animationData: controlData
                    };
                    controlAnimation = lottie.loadAnimation(controlParams);
                        control_wowEffect++;
                        wow = new WOW(
                            {
                                boxClass:     'wow-buildplan',
                                animateClass: 'animated',
                                offset:       250,
                            }
                        )
                        wow.init();
                    }
                }

                if(notTypical === true){
                    if(notTypical_wowEffect === 0){
                        notTypical_wowEffect++;
                        wow = new WOW(
                            {
                                boxClass:     'wow-buildplan',
                                animateClass: 'animated',
                                offset:       250,
                            }
                        )
                        wow.init();
                    }
                }
            });
        });

        function elementInViewport(el) {
            var top = el.offsetTop;
            var left = el.offsetLeft;
            var width = el.offsetWidth;
            var height = el.offsetHeight;

            while(el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
            }

            return (
                top >= window.pageYOffset &&
                left >= window.pageXOffset &&
                (top + height) <= (window.pageYOffset + window.innerHeight) &&
                (left + width) <= (window.pageXOffset + window.innerWidth)
            );
        }

    </script>

    <script type="text/javascript" src="{{asset('/json/animation/data-priceBar.json')}}"></script>
    <script type="text/javascript" src="{{asset('/js/wow.min.js')}}"></script>
    <script>
        let country_handles = {!!json_encode($m_h_sku) !!};
        let size = { width: window.innerWidth || document.body.clientWidth, height: window.innerHeight || document.body.clientHeight };
        if (country_handles == "H1"){
            if(size.width >= 767 && size.width <= 992){
            if(!$("#landing-banner-child").hasClass("landing-banner-desktop-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-premium-img').addClass('landing-banner-tablet-premium-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-premium-img').addClass('landing-banner-tablet-premium-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-premium-img').addClass('landing-banner-tablet-premium-img');
            }
        } else if(size.width > 767){
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-premium-img').addClass('landing-banner-desktop-premium-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-premium-img').addClass('landing-banner-desktop-premium-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-premium-img').addClass('landing-banner-desktop-premium-img');
            }
        } else if(size.width < 767){
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-premium-img').addClass('landing-banner-mobile-premium-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-desktop-premium-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-premium-img').addClass('landing-banner-mobile-premium-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-mobile-premium-img').addClass('landing-banner-mobile-premium-img');
            }
        }
        } else if (country_handles == "H3"){
            if(size.width >= 767 && size.width <= 992){
            // console.log("2");
            if(!$("#landing-banner-child").hasClass("landing-banner-desktop-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-tablet-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-tablet-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-tablet-swivel-img');
            }
        } else if(size.width > 767){
            // console.log("1");
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-swivel-img').addClass('landing-banner-desktop-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-desktop-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-desktop-swivel-img');
            }
        } else if(size.width < 767){
            // console.log("3");
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-swivel-img').addClass('landing-banner-mobile-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-desktop-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-mobile-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-mobile-swivel-img');
            }
        }
        } else{
            if(size.width >= 767 && size.width <= 992){
            // console.log("2");
            if(!$("#landing-banner-child").hasClass("landing-banner-desktop-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-tablet-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-tablet-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-tablet-swivel-img');
            }
        } else if(size.width > 767){
            // console.log("1");
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-swivel-img').addClass('landing-banner-desktop-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-mobile-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-desktop-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-desktop-swivel-img');
            }
        } else if(size.width < 767){
            // console.log("3");
            if(!$("#landing-banner-child").hasClass("landing-banner-tablet-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-tablet-swivel-img').addClass('landing-banner-mobile-swivel-img');
            } else if(!$("#landing-banner-child").hasClass("landing-banner-desktop-swivel-img")){
                $('#landing-banner-child').removeClass('landing-banner-desktop-swivel-img').addClass('landing-banner-mobile-swivel-img');
            } else {
                $('#landing-banner-child').removeClass('landing-banner-mobile-swivel-img').addClass('landing-banner-mobile-swivel-img');
            }
        }
        }

        // console.log("size",size);
    </script>
@endsection
