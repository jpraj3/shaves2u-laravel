@extends('layouts.app')
<style>
    .hasError {
        color: red;
    }
</style>
@section('content')

<link rel="stylesheet" href="{{ asset('css/login/login.css') }}">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6 pt-5 text-center">
            <h1 class="MuliExtraBold">Forgot your password?<br> Don't worry!</h1>
            <p class="d-lg-none">Enter your email to receive instructions<br>on how to reset your password</p>
            <p class="fs-20 d-none d-lg-block">Enter your email to receive instructions<br>on how to reset your password</p>
            <div class="card border-0">
                <div class="card-body padd0">
                    <form id="normal_ecommerce_login" method="POST" action="{{route('login',['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']),'countryCode'=>strtolower(json_decode(session()->get('currentCountry'),true)['codeIso'])])}}">
                        @csrf

                        <div class="form-group">
                            <label for="email"
                                class="col-12 col-form-label text-left"><strong>@lang('website_contents.authentication.content.email')</strong></label>

                            <div class="col-12">
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror border-dark" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <button disabled id="_register_btn" type="submit" class="btn btn-load-section paddBtn">
                                    SUBMIT
                                </button>

                            </div>
                        </div>

                
                        <div class="form-group">
                            <div class="col-12 text-center">
                                <a class="btn btn-link fs-18-lg MuliSemiBold"
                                    href="{{ route('register', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}"
                                    style="color:black;">
                                    <strong>Return to Login Page</strong>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
@endsection