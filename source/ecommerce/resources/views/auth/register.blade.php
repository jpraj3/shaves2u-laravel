@extends('layouts.app')
<style>
    .hasError {
        color: red;
    }

    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -28px;
        position: relative;
        z-index: 2;
        width: 12%;
    }

    .fa{
        font: normal normal normal 20px/1 FontAwesome !important;
        font-size: 20px !important;
    }

    .notification {
        height: 40px;
        position: sticky;
        top: 0;
        width: 100%;
        display: table;
        background: #f4f8fc;
        opacity: 1;
        z-index: 10000;
    }

    .notification > span {
        text-align: center;
        margin: auto;
        display: table-cell;
        vertical-align: middle;
    }

    .click-able {
        color: #ed594a;
        cursor: pointer;
    }
</style>
@section('content')
<link rel="stylesheet" href="{{ asset('css/register/register.css') }}">
@php($currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']))
<div id="inactiveUserNotification" class="notification" hidden>
        <span>
            You account is not activated. Please check your mailbox. Did not receive activation email?  <br class="d-sm-none d-block"> <strong class="click-able" onClick="sendWelcomeEdm()">Resend Activation Email</strong>?
        </span>
    </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6 pt-5 text-center">
            <h1 class="MuliExtraBold">@lang('website_contents.authentication.register.register')</h1>
            <p class="fs-20 d-none d-lg-block">@lang('website_contents.authentication.content.create_account')</p>

            <div class="card border-0">
                <div class="card-body">
                    <form id="normal_ecommerce_registration" method="POST" action="{{ route('ecommerce.registration', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}">
                        @csrf

                        <div class="form-group">
                            <label for="name"
                                class="col-12 col-form-label text-left">@lang('website_contents.authentication.register.name')</label>

                            <div class="col-12">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="name_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>
                        <div class="form-group">
                            <label for="email"
                                class="col-12 col-form-label text-left">@lang('website_contents.authentication.content.email_address')</label>

                            <div class="col-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>
                        <div class="form-group">
                            <label for="password"
                                class="col-12 col-form-label text-left">@lang('website_contents.authentication.login.password')</label>

                            <div class="col-12">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"><span toggle="#password" class="fa fa-eye field-icon toggle-password"></span>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="password_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>
                        <div class="form-group">
                            <label for="date-of-birth"
                                class="col-12 col-form-label text-left">@lang('website_contents.authentication.register.date_of_birth')</label>

                            <div class="row mx-0">
                                <div class="col-4 day_of_birth_div">
                                    <select id="day_of_birth"
                                        class="form-control custom-select @error('day_of_birth') is-invalid @enderror"
                                        name="day_of_birth" required
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                        <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.dd')</option>
                                        @for($i=1;$i<32;$i++)
                                        @switch($i) @case($i) @if($i < 10)
                                        <option value="{{sprintf("%02d", $i)}}">{{sprintf("%02d", $i)}}</option>
                                        @else
                                        <option value="{{ $i }}">{{$i}}</option>
                                        @endif
                                        @break
                                        @default
                                        @endswitch
                                        @endfor
                                    </select>

                                    @error('day_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-4 month_of_birth_div">
                                    <select id="month_of_birth"
                                        class="form-control custom-select @error('day_of_birth') is-invalid @enderror"
                                        name="month_of_birth" required
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                        <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.mm')</option>
                                        @for($i=1;$i<13;$i++) @switch($i) @case(1) <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jan')</option>
                                        @break @case(2)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.feb')</option>
                                        @break @case(3)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.mar')</option>
                                        @break @case(4)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.apr')</option>
                                        @break @case(5)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.may')</option>
                                        @break @case(6)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jun')</option>
                                        @break @case(7)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.jul')</option>
                                        @break @case(8)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.aug')</option>
                                        @break @case(9)
                                        <option value="0{{ $i }}">@lang('website_contents.global.dates.months.sep')</option>
                                        @break @case(10)
                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.oct')</option>
                                        @break @case(11)
                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.nov')</option>
                                        @break @case(12)
                                        <option value="{{ $i }}">@lang('website_contents.global.dates.months.dec')</option>
                                        @break
                                        @default
                                        @endswitch
                                        @endfor
                                    </select>

                                    @error('month_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-4 year_of_birth_div">
                                    <select id="year_of_birth"
                                        class="form-control custom-select @error('day_of_birth') is-invalid @enderror"
                                        name="year_of_birth" required
                                        style="background-image: url({{asset('/images/common/arrow-down.svg')}});">
                                        <option value="" selected disabled hidden>@lang('website_contents.global.dates.settings.yyyy')</option>
                                        @for($i=30;$i<100;$i++)
                                        @switch($i)
                                        @case($i) @if($i < 10)
                                        <option value="19{{sprintf("%02d", $i)}}">19{{sprintf("%02d", $i)}}</option>
                                            @else
                                            @if($i==80)
                                            <option value="19{{ $i }}" selected>19{{$i}}</option>
                                            @else
                                            <option value="19{{ $i }}">19{{$i}}</option>
                                            @endif
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                            @for($i=0;$i<(int)substr(date("Y"), -2) + 1;$i++) @switch($i) @case($i) @if($i < 10) <option value="20{{sprintf("%02d", $i)}}" >20{{sprintf("%02d", $i)}}</option>
                                            @else
                                            <option value="20{{ $i }}">20{{$i}}</option>
                                            @endif
                                            @break
                                            @default
                                            @endswitch
                                            @endfor
                                    </select>

                                    @error('year_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div id="birthday_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        </div>

                        @if(strtolower($currentCountryIso) == 'kr')
                            <div class="form-group">
                                <div class="row text-left">
                                    <div class="input-group">
                                        <input required id="tos_agree" name="tos_agree" type="checkbox" class="col-1 @error('tos_agree') is-invalid @enderror"/>
                                        <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.tos_agreement')</u></p>
                                    </div>
                                    <div class="input-group">
                                        <input required id="privacy_policy_agree" name="privacy_policy_agree" type="checkbox" class="col-1 @error('privacy_policy_agree') is-invalid @enderror"/>
                                        <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.privacy_policy_agreement')</u></p>
                                    </div>
                                    <div class="input-group">
                                        <input required id="personal_info_agree" name="personal_info_agree" type="checkbox" class="col-1 @error('personal_info_agree') is-invalid @enderror"/>
                                        <p class="col-11 mb-0 font-weight-bold"><u>@lang('website_contents.authentication.register.personal_info_transfer_agreement')</u></p>
                                    </div>
                                    <div class="input-group">
                                        <input required id="min_age_agree" name="min_age_agree" type="checkbox" class="col-1 @error('min_age_agree') is-invalid @enderror"/>
                                        <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.minimum_age_agreement')</p>
                                    </div>
                                    <div class="input-group">
                                        <input id="email_sub_agree" name="email_sub_agree" type="checkbox" class="col-1" value="1"/>
                                        <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.email_sub_agreement')</p>
                                    </div>
                                    <div class="input-group">
                                        <input id="sms_sub_agree" name="sms_sub_agree" type="checkbox" class="col-1" value="1"/>
                                        <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.sms_sub_agreement')</p>
                                    </div>
                                    <ul class="col-11 offset-1">
                                        <li>@lang('website_contents.authentication.register.sms_sub_agreement_point1')</li>
                                        <li>@lang('website_contents.authentication.register.sms_sub_agreement_point2')</li>
                                    </ul>
                                </div>
                                @error('required_checked')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <div id="required_checked_error" class="hasError col-12 col-form-label text-left"></div>
                            </div>
                        @else
                            <div class="form-group">
                                <div class="row text-left">
                                    <div class="input-group">
                                        <input id="marketing_sub_agree" type="checkbox" class="col-1" value="1"/>
                                        <p class="col-11 mb-0 font-weight-bold">@lang('website_contents.authentication.register.marketing_sub_agreement')</p>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <button id="_register_btn" type="button" class="btn btn-load-section pb-3 pt-3" onclick="register_btn()">
                                    @lang('website_contents.authentication.register.create_account')
                                </button>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center padd0">
                                @if (Route::has('password.request'))
                                <p style="color:black;">
                                    <strong>@lang('website_contents.authentication.register.accept_disclaimer')<br>
                                        <a class="color-black"
                                            href="{{ route('locale.region.tnc', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" target="_blank">@lang('website_contents.authentication.register.accept_disclaimer_terms')</a>
                                        @lang('website_contents.authentication.register.accept_disclaimer_and')
                                        <a class="color-black"
                                            href="{{ route('locale.region.privpolicy', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}" target="_blank">@lang('website_contents.authentication.register.accept_disclaimer_privacy')</a>
                                    </strong>
                                </p>
                                @endif
                            </div>
                        </div>

                        <div class="row ml-0 mr-0">
                            <div class="col-4 offset-1 border-top border-dark padd0">
                            </div>
                            <div class="col-2 text-center fs-16 MuliExtraBold padd0" style="margin-top: -5px;">
                                @lang('website_contents.authentication.content.or')
                            </div>
                            <div class="col-4 border-top border-dark padd0">
                            </div>
                        </div>

                        <div class="form-group mt-3 mb-3">
                            <div class="col-12 text-center">
                                <a href="{{ route('auth.facebook', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" class="col-12 btn btn-load-more pb-3 pt-3 MuliBold">
                                        <strong>@lang('website_contents.authentication.register.signup_facebook')</strong>
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <a class="btn btn-link fs-18 MuliSemiBold"
                                    href="{{ route('login', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}"
                                    style="color:black;">
                                    <strong>@lang('website_contents.authentication.register.have_login')</strong>
                                </a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>

<script>

 function validateOnLoadRegister(){
    validateRegistration("normal_ecommerce_registration");
        let form = document.getElementById('normal_ecommerce_registration');
        let _register_btn = document.getElementById('_register_btn');
        if ($('#normal_ecommerce_registration').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _register_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _register_valid = false;
        }
 }
    function checkIfEmailExistsOrActive (){
        return new Promise(function (resolve, reject) {
        loading.style.display = "block";
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL_V2 + '/api/check/existsoractive',
                method: "POST",
                cache: false,
                data: { email: $("#email").val()},
                success: function (data) {
                    loading.style.display = "none";
                    resolve(data);
                },
                error: function (data) {
                    loading.style.display = "none";
                    reject(data);
                },
                failure: function (data) {
                    loading.style.display = "none";
                    reject(data);
                }
            });
        });
    }

    function sendWelcomeEdm(){
        
    }

    function register_btn() {
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
        validateRegistration("normal_ecommerce_registration");
        
        checkIfEmailExistsOrActive().then(function (check) {
            if(check){
                let exists = check.user_data !== null && check.isActive !== null ? true : false;
                let isActive = check.user_data !== null && check.isActive === 1 ? true : false;
                if(!exists && !isActive){
                    if ($('#normal_ecommerce_registration').valid() === true) { // Calling validation function
                        _register_valid = true;
                        document.getElementById("normal_ecommerce_registration").submit(); //form submission
                    }else{
                        _register_valid = false;
                    }
                    if (loading) {
                        loading.style.display = "none";
                    }
                } else if (exists && !isActive){
                    // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                    $("#email_error").html('You need to be active to login.')
                } else if (exists && isActive){
                    $("#email_error").html(trans('validation.custom.validation.email.email_exists', {}))
                }
            }
        }).catch(function (err) {
            // Run this when promise was rejected via reject()
        })
    }


   $(document).ready(function(){

    // validateOnLoadRegister();

    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
        input.attr("type", "password");
        }
    });

    //     $('#email').keyup(function() {
    //         validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });

    // $('#password').keyup(function() {
    //     validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });
    // $('#name').keyup(function() {
    //         validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });
    // $('#day_of_birth').on('change',function() {
    //         validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });
    // $('#month_of_birth').on('change',function() {
    //         validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });
    // $('#year_of_birth').on('change',function() {
    //         validateRegistration("normal_ecommerce_registration");
    //     let form = document.getElementById('normal_ecommerce_registration');
    //     let _register_btn = document.getElementById('_register_btn');
    //     if ($('#normal_ecommerce_registration').valid() === true) {
    //         console.log("pass validation");
    //         _register_btn.removeAttribute("disabled");
    //         _register_valid = true;
    //     } else {
    //         console.log("fail validation");
    //         _register_btn.setAttribute("disabled", null);
    //         _register_valid = false;
    //     }
    // });
// validate normal registration
// $("#normal_ecommerce_registration").change(function (event) {
//         validateRegistration("normal_ecommerce_registration");
//         let form = document.getElementById('normal_ecommerce_registration');
//         let _register_btn = document.getElementById('_register_btn');
//         if ($('#normal_ecommerce_registration').valid() === true) {
//             console.log("pass validation");
//             _register_btn.removeAttribute("disabled");
//             _register_valid = true;
//         } else {
//             console.log("fail validation");
//             _register_btn.setAttribute("disabled", null);
//             _register_valid = false;
//         }
//     })
});
</script>
@endsection
