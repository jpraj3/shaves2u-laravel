@extends('layouts.app')

@section('content')
    <div class="container pt-5">
        <div class="row justify-content-center ">
            <div class="col-md-8 bg-grey" style="padding: 80px 80px 50px;">
                <div class="card bg-grey" style="border:none !important;">
                    <div class="card-header bg-grey mb-0 pb-0">
                        <h3 class="MuliExtraBold">@lang('website_contents.global.reset_password.got_mail')</h3>
                    </div>
                    <p class="card-header bg-grey">
                            @lang('website_contents.global.reset_password.got_mail_desc')
                    </p>
                    <a class="card-header bg-grey text-uppercase" style="color:#000000;"
                       href="{{ route('login', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                        <u>@lang('website_contents.global.reset_password.got_mail_return')</u>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
