@extends('layouts.app')

@section('content')
    <style>
        .hasError {
            color: red;
        }
    </style>
    <div class="container pt-5">
        <div class="row justify-content-center ">
            <div class="col-md-8 bg-grey" style="padding: 80px 80px 50px;">
                <div class="card bg-grey" style="border:none !important;">
                    <div class="card-header bg-grey mb-0 pb-0"><h3
                            class="MuliExtraBold">@lang('website_contents.authentication.forgot_password.title')</h3></div>
                    <div class="card-header bg-grey mt-0 pt-0"><h3 class="MuliExtraBold">@lang('website_contents.authentication.forgot_password.sub_title')</h3>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form id="form_rp_email" method="POST"
                              action="{{ route('password.email.new', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-12 col-form-label text-md-left">@lang('website_contents.authentication.forgot_password.instructions')</label>
                                <div class="col-md-12">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ old('email') }}" autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div id="email_error" class="hasError col-form-label text-left"></div>
                                </div>
                            </div>

                            <div class="form-group row mb-0 pt-4">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn bg-orange MuliSemiBold w-50"
                                            style="color: white; border:none; font-size: 13px;">
                                            @lang('website_contents.authentication.forgot_password.reset_link')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
@endsection
