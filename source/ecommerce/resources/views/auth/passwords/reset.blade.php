@extends('layouts.app')

@section('content')
    <style>
        .error {
          color: red;
       }
    </style>
    <div class="container pt-5">
        <div class="row justify-content-center ">
            <div class="col-md-8 bg-grey" style="padding: 80px 80px 50px;">
                <div class="card bg-grey" style="border:none !important;">
                    <div class="row justify-content-center">
                        <div class="card-header col-md-9 bg-grey mb-0 pb-0">
                            <h3 class="MuliExtraBold text-center bg-grey">@lang('website_contents.global.reset_password.confirm_pw')</h3>
                            <h6 class="MuliPlain text-center bg-grey">@lang('website_contents.global.reset_password.confirm_pw_desc')</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        {{-- Default Post Action to send Reset email. Commented out until fixed --}}
                        <form method="POST"
                              action="{{ route('password.update',['langCode'=> $langCode,'countryCode'=>$countryCode]) }}">
{{--                        <form action="document.location.reload()">--}}
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus hidden>

                            <div class="form-group row justify-content-center">
                                <label for="password" class="MuliPlain col-md-9 col-form-label text-md-left">@lang('website_contents.global.reset_password.confirm_pw_new_pw')</label>

                                <div class="col-md-9">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <label for="password-confirm"
                                       class="col-md-9 col-form-label text-md-left MuliPlain">@lang('website_contents.global.reset_password.confirm_pw_password')</label>

                                <div class="col-md-9">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn bg-orange MuliSemiBold" style="color: white; border:none; font-size: 13px;">
                                    @lang('website_contents.global.reset_password.reset-password')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/functions/reset-password/reset-password.function.js') }}"></script>
@endsection
