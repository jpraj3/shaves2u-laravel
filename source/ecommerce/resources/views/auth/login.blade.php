@extends('layouts.app')
<style>
    .hasError {
        color: red;
    }

    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -28px;
        position: relative;
        z-index: 2;
        width: 12%;
    }
    .fa {
        font: normal normal normal 20px/1 FontAwesome !important;
        font-size: 20px !important;
    }

    .notification {
        height: 40px;
        position: sticky;
        top: 0;
        width: 100%;
        display: table;
        background: #f4f8fc;
        opacity: 1;
        z-index: 10000;
    }

    .notification > span {
        text-align: center;
        margin: auto;
        display: table-cell;
        vertical-align: middle;
    }

    .click-able {
        color: #ed594a;
        cursor: pointer;
    }
</style>
@section('content')
@php($sessionLoginError = session()->get('loginerror'))
<?php session()->forget('loginerror'); ?>

<link rel="stylesheet" href="{{ asset('css/login/login.css') }}">
<div id="inactiveUserNotification" class="notification" hidden>
    <span>
        You account is not activated. Please check your mailbox. Did not receive activation email?  <br class="d-sm-none d-block"> <strong class="click-able" onClick="sendWelcomeEdm()">Resend Activation Email</strong>?
    </span>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6 pt-5 text-center">
            <h1 class="MuliExtraBold">@lang('website_contents.global.content.welcome')</h1>
            <p class="d-lg-none">@lang('website_contents.authentication.login.enter_details')</p>
            <p class="fs-20 d-none d-lg-block">@lang('website_contents.authentication.login.enter_details')</p>
            <div class="card border-0">
                <div class="card-body padd0">
                    <form id="normal_ecommerce_login" method="POST" action="{{route('login',['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']),'countryCode'=>strtolower(json_decode(session()->get('currentCountry'),true)['codeIso'])])}}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-12 col-form-label text-left"><strong>@lang('website_contents.authentication.content.email_address')</strong></label>

                            <div class="col-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror border-dark" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="email_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-12 col-form-label text-left"><strong>@lang('website_contents.authentication.login.password')</strong></label>

                            <div class="col-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror border-dark" name="password" required autocomplete="current-password"><span toggle="#password" class="fa fa-eye field-icon toggle-password"></span>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div id="password_error" class="hasError col-12 col-form-label text-left"></div>
                        </div>

                        <div class="form-group">
                        <div id="login_error" class="hasError col-12 col-form-label text-left">{{$sessionLoginError}}</div>
                            <div class="col-12 text-center">
                                <button id="_login_btn" type="button" class="btn btn-load-section pb-3 pt-3" onclick="login_btn()">
                                    @lang('website_contents.authentication.login.login')
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center padd0">
                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" style="color:black;">
                                    <strong>@lang('website_contents.authentication.login.forgot_password')</strong>
                                </a>
                                @endif
                            </div>
                        </div>

                        <div class="row ml-0 mr-0">
                            <div class="col-4 offset-1 border-top border-dark padd0">
                            </div>
                            <div class="col-2 text-center fs-16 MuliExtraBold padd0" style="margin-top: -13px;">
                                @lang('website_contents.authentication.content.or')
                            </div>
                            <div class="col-4 border-top border-dark padd0">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <a href="{{ route('auth.facebook', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" class="col-12 btn btn-fb-login pb-3 pt-3 MuliBold">
                                    <strong>@lang('website_contents.authentication.login.login_facebook')</strong>
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <a class="btn btn-link fs-18-lg MuliExtraBold" href="{{ route('register', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}" style="color:black;">
                                    <strong>@lang('website_contents.authentication.login.new_sign_up')</strong>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/helpers/ecommerce_form_validations.js') }}"></script>
<script>
    function validateOnLoadLogin() {
        validateLogin("normal_ecommerce_login");
        let _login_btn = document.getElementById('_login_btn');
        if ($('#normal_ecommerce_login').valid() === true) {
            login_btn.removeAttribute("disabled");
                _login_valid = true;
        } else {
            console.log("fail validation");
            _login_btn.setAttribute("disabled", null);
            _login_valid = false;
        }
    }

    function checkIfEmailExistsOrActive (){
        return new Promise(function (resolve, reject) {
        loading.style.display = "block";
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL_V2 + '/api/check/existsoractive',
                method: "POST",
                cache: false,
                data: { email: $("#email").val()},
                success: function (data) {
                    loading.style.display = "none";
                    resolve(data);
                },
                error: function (data) {
                    loading.style.display = "none";
                    reject(data);
                },
                failure: function (data) {
                    loading.style.display = "none";
                    reject(data);
                }
            });
        });
    }

    function sendWelcomeEdm(){
        
    }

    function login_btn() {
        validateLogin("normal_ecommerce_login");
        checkIfEmailExistsOrActive().then(function (check) {
            if(check){
                let exists = check.user_data !== null && check.isActive !== null ? true : false;
                let isActive = check.user_data !== null && check.isActive === 1 ? true : false;
                // console.log(exists,isActive);
                if(exists && isActive){
                    _login_btn.removeAttribute("disabled");
                    _login_valid = true;
                    document.getElementById("normal_ecommerce_login").submit(); //form submission
                } else if (exists && !isActive){
                    // document.getElementById("inactiveUserNotification").removeAttribute("hidden", null);
                    $("#email_error").html('You need to be active to login.')
                } else if (!exists && !isActive){
                    $("#email_error").html('The email does not exist.')
                }
            }
        }).catch(function (err) {
            // Run this when promise was rejected via reject()
        })
        
        
        // if ($('#normal_ecommerce_login').valid() === true) { // Calling validation function
        //     document.getElementById("normal_ecommerce_login").submit(); //form submission
        //     _login_valid = true;
        // }else{
        //     _login_valid = false;
        // }
    }
    $(document).ready(function() {

        // validateOnLoadLogin();
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        // $('#email').keyup(function() {
        //     validateLogin("normal_ecommerce_login");
        //     let _login_btn = document.getElementById('_login_btn');
        //     if ($('#normal_ecommerce_login').valid() === true) {
        //         console.log("pass validation");
        //         _login_btn.removeAttribute("disabled");
        //         _login_valid = true;
        //     } else {
        //         console.log("fail validation");
        //         _login_btn.setAttribute("disabled", null);
        //         _login_valid = false;
        //     }
        // });

        // $('#password').keyup(function() {
        //     validateLogin("normal_ecommerce_login");
        //     let _login_btn = document.getElementById('_login_btn');
        //     if ($('#normal_ecommerce_login').valid() === true) {
        //         console.log("pass validation");
        //         _login_btn.removeAttribute("disabled");
        //         _login_valid = true;
        //     } else {
        //         console.log("fail validation");
        //         _login_btn.setAttribute("disabled", null);
        //         _login_valid = false;
        //     }
        // });

        // validate normal login
        // $("#normal_ecommerce_login").change(function (event) {
        //     validateLogin("normal_ecommerce_login");
        //     let _login_btn = document.getElementById('_login_btn');
        //     if ($('#normal_ecommerce_login').valid() === true) {
        //         console.log("pass validation");
        //         _login_btn.removeAttribute("disabled");
        //         _login_valid = true;
        //     } else {
        //         console.log("fail validation");
        //         _login_btn.setAttribute("disabled", null);
        //         _login_valid = false;
        //     }
        // })
    });
</script>
@endsection
