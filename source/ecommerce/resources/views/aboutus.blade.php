@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('css/aboutus/aboutus.css') }}">

<section class="about-banner bg-banner" style="background-image: url({{asset('/images/common/About_Us_main_banner.png')}})">
    <div class="container">
        <div class="row">
            <div class="center-md col-md-12">
                <h1 class="fs-36-md" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-Bold; color: white;"><strong>Shaving created with<br> you in mind<br></strong></h1>
                <p class="fs-20" style="margin-bottom: 5%; font-family: Muli; color: white;">Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit.</p>
            </div>
        </div>
    </div>
</section>

<section class="about-banner2">
    <div class="container">
        <div class="row">
            <div class="center-md col-md-12 col-lg-6" style="padding: 5% 15px 0 15px;">
                <h1 class="fs-36-md" style="margin-bottom: 5%; margin-top: 2%; font-family: Muli-Bold; color: black;"><strong>Our story<br></strong></h1>
                <p class="fs-20-md" style="margin-bottom: 5%; font-family: Muli; color: black;">Lorem ipsum dolor sit amet, consectetur<br>adipiscing elit, sed do eiusmod tempor<br>incididunt ut labore et dolore magna<br>aliqua. Ut enim ad minim veniam, quis<br>nostrud exercitation ullamco laboris nis</p>
            </div>
            <div class="center-md col-md-12 col-lg-6" style="padding: 5% 15px 0 15px;">
                <img alt="Our Story" class="img-fluid" src="{{asset('/images/common/hero_banner_1105_cus.png')}}">
            </div>
        </div>
    </div>
</section>

<section class="paddTB40 about-traits">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 text-center padd15">
                <h1 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>What makes us different?<br></strong></h1>
            </div>
            <div class="d-none d-md-block">
                <div class="row justify-content-around">
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Trait 1<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <img alt="Dynamic Team" class="img-fluid" src="{{asset('/images/common/trait_1.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <img alt="Trait 2" class="img-fluid" src="{{asset('/images/common/trait_2.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Trait 2<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <h2 class="bold-max" style="margin-bottom: 5%; margin-top: 10%; font-family: Muli-SemiBold;"><strong>Trait 3<br></strong></h2>
                        <p style="margin-bottom: 5%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-md-6 col-lg-5 padd30 margTB10">
                        <img alt="Trait 3" class="img-fluid" src="{{asset('/images/common/trait_3.png')}}" style="width: 100%; padding-right: 30px;">
                    </div>
                </div>
            </div>
            <div class="d-sm-none">
                <div id="culture_carousel" class="carousel slide" data-ride="carousel" style="min-height: 550px">
                    <ol class="carousel-indicators">
                        <li data-target="#culture_carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#culture_carousel" data-slide-to="1"></li>
                        <li data-target="#culture_carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="padd30 margTB10">
                                <img alt="Dynamic Team" class="img-fluid d-block mx-auto" src="{{asset('/images/common/trait_1.png')}}" style="width: 60%;">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Trait 1<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="padd30 margTB10">
                                <img alt="Trait 2" class="img-fluid d-block mx-auto" src="{{asset('/images/common/trait_2.png')}}" style="width: 60%; ">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Trait 2<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="padd30 margTB10">
                                <img alt="Trait 3" class="img-fluid d-block mx-auto" src="{{asset('/images/common/trait_3.png')}}" style="width: 60%; ">
                            </div>
                            <div class="padd30 text-center margTB10">
                                <h2 class="bold-max" style="margin-bottom: 5%; font-family: Muli-SemiBold;"><strong>Trait 3<br></strong></h2>
                                <p style="margin-bottom: 15%; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-video bg-banner d-none d-lg-block" style="background-image: url({{asset('/images/common/video_wrapper.jpg')}})">
</section>

<section class="about-video2 bg-banner d-lg-none" style="background-image: url({{asset('/images/common/banner-video.png')}})">
</section>

<section class="paddTB40 career-welcome" style="background-color: #46545d">
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h3 class="bold-max" style="margin-bottom: 4%; margin-top: 2%; color:white; font-family: Muli-ExtraBold"><strong>Let's welcome you<br class="d-md-none"> to the family<br></strong></h3>
                </div>
                <div class="col-md-12 text-center pr-0 pl-0">
                    <p class=" d-none d-md-block" style="margin-bottom: 4%; margin-top: 2%; font-size: 15px; color:white; font-family: Muli;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation<br> ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p class="d-md-none" style="margin-bottom: 10%; margin-top: 2%; font-size: 15px; color:white; font-family: Muli;">Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit.</p>
                    <button class="btn btn-start">GET IN TOUCH</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection