@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="{{ asset('css/referral/referral.css') }}">
    @php($m_h = null)
    @php($m_h_sku = null)
    @if(Session::has('country_handles'))
    @php($m_h = Session::get('country_handles'))
    @php($m_h_sku = $m_h->sku)
    @endif
    <!-- Desktop View Start -->
    <div class="d-none d-lg-block">
        <section class="referral-banner bg-banner"
                 style="background-image: url({{asset('/images/common/s2u_referral_banner.png')}}); height: 40vw;">
            <div class="bg-shade">
                <div class="container">
                    <div class="row">
                        <div class="text-center col-12 col-lg-6 color-md-white color-sm-white"
                             style="padding: 15% 25px 25% 15px;">
                            <h1 class="MuliExtraBold mb-4" style="margin-top: 5%; color:black">
                                @lang('website_contents.landing.referral.referral_program')<br></h1>
                            <h4 class="MuliExtraBold mb-4"
                                style="color:black">@lang('website_contents.landing.referral.good_things')</h4>
                            <a class="btn btn-start" style="width: 40%;"
                               href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.content.start_now')</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="referral-perks paddTB40 text-white text-center" style="background-color: #46545d;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-4 pb-4 pl-1 pr-1">
                        <img class="pb-4" src="{{asset('/images/common/icon_easycomission.png')}}"/>
                        <h4 class="pb-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.easy_commissions')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.easy_commissions_desc')</p>
                    </div>
                    <div class="col-12 col-lg-4 pb-4 pl-1 pr-1">
                        <img class="pb-4" src="{{asset('/images/common/icon_leverage.png')}}"/>
                        <h4 class="pb-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.leverage_network')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.leverage_network_desc')</p>
                    </div>
                    <div class="col-12 col-lg-4 pb-4 pl-1 pr-1">
                        <img class="pb-4" src="{{asset('/images/common/icon_makemoney.png')}}"/>
                        <h4 class="pb-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.make_money')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.make_money_desc')</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="paddTB40 referral-how">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-12 text-center padd15">
                        <h2 class="MuliExtraBold mt-2p mb-2p">
                            <strong>@lang('website_contents.landing.referral.how_it_works')<br></strong></h2>
                        <p class="fs-24 Muli mt-2p mb-2p" style="text-align:center;color:#828282;font-size:18px;">
                            @lang('website_contents.landing.referral.get_started_steps')<br></p>
                    </div>
                    <div class="row justify-content-around center-md">
                        <div class="col-md-6 col-lg-6 margTB10 pr-0 d-none d-md-block pt-5" style="padding-left:7%">
                            <p class="MuliSemiBold mt-10p mb-2p text-uppercase"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_1')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px;">
                                @lang('website_contents.landing.referral.step_1_signup')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_1_desc')</p>
                        </div>
                        <div class="col-md-6 col-lg-5 pt-0 pb-0 pt-lg-5 pb-lg-5 margTB10">
                            <img alt="Dynamic Team" class="img-fluid"
                                 src="{{asset('/images/common/referral_step1.png')}}">
                        </div>
                        <div class="col-md-6 col-lg-6 pt-0 pb-0 pt-lg-5 margTB10 text-center">
                            <img alt="Trait 2" class="img-fluid" src="{{asset('/images/common/referral_step2.png')}}">
                        </div>
                        <div class="col-md-6 col-lg-6 margTB10 pr-0 d-none d-md-block">
                            <p class="MuliSemiBold mt-10p mb-2p text-uppercase"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_2')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px;">
                                @lang('website_contents.landing.referral.step_2_share')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_2_desc')</p>
                        </div>
                        <div class="col-md-6 col-lg-6 margTB10 pr-0 d-none d-md-block" style="padding-left:7%">
                            <p class="MuliSemiBold mt-10p mb-2p text-uppercase"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_3')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px; line-height:30px;">
                                @lang('website_contents.landing.referral.step_3_earn')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_3_desc', ["currency" => $country->currencyDisplay, "amount" => $referralValues->value])</p>
                        </div>
                        <div class="col-md-6 col-lg-5 pt-0 pb-0 pt-lg-5 pb-lg-5 margTB10">
                            <img alt="Trait 3" class="img-fluid" src="{{asset('/images/common/referral_step3.png')}}">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="referral-start">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 pl-0 pr-0 d-none d-lg-inline-flex">
                        <div class="d-flex align-self-center">
                            <div class="text-center" style="padding-left: 100px;">
                                <h2 class="MuliExtraBold mt-2p mb-10p">
                                    <strong>@lang('website_contents.landing.referral.start_journey')<br></strong></h2>
                                <a class="btn btn-start" style="min-width: 65%;"
                                   href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.content.start_now')</a>
                            </div>
                        </div>
                    </div>
                    <div class="center-md col-md-12 col-lg-6" style="padding: 5% 15px 0 15px;">
                        @if($m_h_sku == 'H3')
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/hero_banner_1105_cus.png')}}">
                        @elseif($m_h_sku == 'H1')
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/premium/product/hero_banner_1105_cus.png')}}">
                        @else
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/hero_banner_1105_cus.png')}}">
                        @endif
                    </div>
                </div>
            </div>
        </section>

        <section class="referral-start-mobile d-lg-none">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3">
                        <div class="d-flex align-items-center">
                            <div class="col-12 text-center">
                                <h4 class="fs-24 MuliExtraBold mt-2p mb-5p"><strong>@lang('website_contents.global.content.start_journey_mobile')<br></strong></h4>
                                <a class="btn btn-start mt-5 mb-5 text-uppercase" style="min-width: 100%;"
                                   href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.faq.get_started')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="referal-need-to-know" class="landing-section py-5">
            <div class="container" id="faq">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h2 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>@lang('website_contents.global.faq.know_more')<br></strong></h2>
                    <p class="fs-18" style="margin-bottom: 8%; margin-top: 2%; font-family: Muli; color: #828282;">@lang('website_contents.global.faq.curious')</p>
                </div>
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingOne1"
                             style="background-color:initial !important;">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                               aria-expanded="true"
                               aria-controls="collapseOne1">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_sp_work')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_sp_work_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerOne1"
                             style="background-color:initial !important;">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                               aria-expanded="true"
                               aria-controls="collapseOne1">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingTwo2"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                               aria-expanded="false" aria-controls="collapseTwo2">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_long_arrive')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_long_arrive_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerTwo2"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                               aria-expanded="false" aria-controls="collapseTwo2">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingThree3"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx"
                               href="#collapseThree3"
                               aria-expanded="false" aria-controls="collapseThree3">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.which_locations')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.which_locations_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerThree3"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx"
                               href="#collapseThree3"
                               aria-expanded="false" aria-controls="collapseThree3">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFour4"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                               aria-expanded="false" aria-controls="collapseFour4">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.offer_free_delivery')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.offer_free_delivery_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerFour4"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                               aria-expanded="false" aria-controls="collapseFour4">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFive5"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5"
                               aria-expanded="false" aria-controls="collapseFive5">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_refer_friend')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseFive5" class="collapse" role="tabpanel" aria-labelledby="headingFive5"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_refer_friend_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerFive5"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5"
                               aria-expanded="false" aria-controls="collapseFive5">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingSix6"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6"
                               aria-expanded="false" aria-controls="collapseSix6">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_much_earn')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseSix6" class="collapse" role="tabpanel" aria-labelledby="headingSix6"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_much_earn_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerSix6"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6"
                               aria-expanded="false" aria-controls="collapseSix6">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingSeven7"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7"
                               aria-expanded="false" aria-controls="collapseSeven7">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_keep_track')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseSeven7" class="collapse" role="tabpanel" aria-labelledby="headingSeven7"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_keep_track_ans', ["currency" => $country->currencyDisplay, "amount" => $referralValues->value])
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerSeven7"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7"
                               aria-expanded="false" aria-controls="collapseSeven7">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingEight8"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseEight8"
                               aria-expanded="false" aria-controls="collapseEight8">
                                <h5 class="mb-0" style="font-family: Muli-Bold; color:black">
                                    @lang('website_contents.global.faq.how_cash_out')
                                    <i class="fa fa-angle-right rotate-icon d-none d-md-block"
                                       style="float: right;"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseEight8" class="collapse" role="tabpanel" aria-labelledby="headingEight8"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0">
                                @lang('website_contents.global.faq.how_cash_out_ans')
                            </div>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer center-sm d-sm-none" role="tab" id="footerEight8"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseEight8"
                               aria-expanded="false" aria-controls="collapseEight8">
                                <h5 class="mb-0 text-center" style="font-family: Muli-Bold; color:black">
                                    <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                    </div>
                    <!-- Accordion card -->

                </div>
                <!-- Accordion wrapper -->
            </div>
        </section>
    </div>
    <!-- Desktop View End -->

    <!-- Mobile View Start -->
    <div class="d-block d-lg-none">
        <section class="referral-banner bg-banner"
                 style="background-image: url({{asset('/images/common/s2u_referral_banner.png')}});">
            <div class="bg-shade">
                <div class="container">
                    <div class="row">
                        <div class="text-center col-12 col-lg-6 color-md-white color-sm-white"
                             style="padding: 15% 25px 25% 15px;">
                            <h1 class="MuliExtraBold mb-4" style="margin-top: 15%; ">
                                @lang('website_contents.landing.referral.referral_program')<br></h1>
                            <h4 class="MuliExtraBold mb-4">@lang('website_contents.landing.referral.good_things')</h4>
                            <a class="btn btn-start" style="width: 70%;"
                               href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">@lang('website_contents.global.content.start_now')</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="referral-perks paddTB40 text-white text-center" style="background-color: #46545d;">
            <div class="container">
                <div class="row" style="padding: 0 30px;">
                    <div class="col-12 col-lg-4 pb-4">
                        <img class="pb-4" src="{{asset('/images/common/icon_easycomission.png')}}"/>
                        <h4 class="pb-xl-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.easy_commissions')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.easy_commissions_desc')</p>
                    </div>
                    <div class="col-12 col-lg-4 pb-4">
                        <img class="pb-4" src="{{asset('/images/common/icon_leverage.png')}}"/>
                        <h4 class="pb-xl-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.leverage_network')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.leverage_network_desc')</p>
                    </div>
                    <div class="col-12 col-lg-4 pb-4">
                        <img class="pb-4" src="{{asset('/images/common/icon_makemoney.png')}}"/>
                        <h4 class="pb-xl-4 MuliSemiBold mb-5p">
                            <strong>@lang('website_contents.landing.referral.make_money')<br></strong></h4>
                        <p class="Muli mb-5p">@lang('website_contents.landing.referral.make_money_desc')</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="paddTB40 referral-how">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-12 text-center padd15">
                        <h2 class="MuliExtraBold mt-2p mb-2p">
                            <strong>@lang('website_contents.landing.referral.how_it_works')<br></strong></h2>
                        <p class="fs-24 Muli mt-2p mb-2p" style="text-align:center;color:#828282;font-size:18px;">
                            @lang('website_contents.landing.referral.get_started_steps')<br></p>
                    </div>
                    <div class="row justify-content-around center-md">
                        <div class="col-md-6 col-lg-5 pt-0 pb-0 pt-lg-5 pb-lg-5 padd30 margTB10">
                            <img alt="Dynamic Team" class="img-fluid"
                                 src="{{asset('/images/common/referral_step1.png')}}">
                        </div>
                        <div class="col-md-6 col-lg-6 padd30 pt-0 pt-lg-5 margTB10">
                            <p class="MuliSemiBold mt-10p mb-2p"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_1')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px;">
                                @lang('website_contents.landing.referral.step_1_signup')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_1_desc')</p>
                        </div>
                        <div class="col-md-6 col-lg-6 pt-5 pb-0 pt-lg-5 padd30 margTB10 text-center">
                            <img alt="Trait 2" class="img-fluid" src="{{asset('/images/common/referral_step2.png')}}">
                        </div>
                        <div class="col-md-6 col-lg-6 margTB10">
                            <p class="MuliSemiBold mt-10p mb-2p"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_2')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px;">
                                @lang('website_contents.landing.referral.step_2_share')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_2_desc')</p>
                        </div>
                        <div class="col-md-6 col-lg-5 pt-3 pb-0 pt-lg-5 pb-lg-5 padd30 margTB10">
                            <img alt="Trait 3" class="img-fluid" src="{{asset('/images/common/referral_step3.png')}}">
                        </div>
                        <div class="col-md-6 col-lg-5 padd30 pt-0 pt-lg-5 margTB10">
                            <p class="MuliSemiBold mt-10p mb-2p"
                               style="color: #d45c39;font-size:18px;margin-bottom:0px;">
                                @lang('website_contents.landing.referral.step_3')<br></p>
                            <p class="MuliBold mt-2p" style="font-size:25px; margin-bottom:16px; line-height:30px;">
                                @lang('website_contents.landing.referral.step_3_earn')<br></p>
                            <p class="Muli mb-5p" style="color: #d45c39;font-size:18px;color:#828282;">
                                @lang('website_contents.landing.referral.step_3_desc_mobile', ["currency" => $country->currencyDisplay, "amount" => $referralValues->value])</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="referral-start">
            <div class="container">
                <div class="row">
                    <div class="center-md col-md-12 col-lg-6" style="padding: 5% 15px 0 15px;">
                        @if($m_h_sku == 'H3')
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/hero_banner_1105_cus.png')}}">
                        @elseif($m_h_sku == 'H1')
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/premium/product/hero_banner_1105_cus.png')}}">
                        @else
                        <img alt="Start Now" class="img-fluid" src="{{asset('/images/common/hero_banner_1105_cus.png')}}">
                        @endif
                    </div>
                </div>

                <div class="col-md-12 col-lg-6 pl-0 pr-0 pb-5">
                    <div class="align-items-center">
                        <div class="text-center">
                            <h2 class="MuliExtraBold pb-2">
                                <strong>@lang('website_contents.landing.referral.start_journey_mobile')<br></strong>
                            </h2>
                            <a class="btn btn-start" style="min-width: 80%;"
                               href="{{ route('locale.region.authenticated.referrals', ['langCode'=>view()->shared('url'), 'countryCode'=>view()->shared('currentCountryIso')]) }}">
                               @lang('website_contents.global.content.start_now')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="referal-need-to-know" class="landing-section py-5">
            <div class="container" id="faq">
                <div class="col-md-12 text-center pr-0 pl-0">
                    <h2 class="bold-max" style="margin-bottom: 2%; margin-top: 2%; font-family: Muli-ExtraBold"><strong>
                        @lang('website_contents.global.faq.know_more')<br></strong></h2>
                    <p class="fs-18" style="margin-bottom: 8%; margin-top: 2%; font-family: Muli; color: #828282;">
                        @lang('website_contents.global.faq.curious')
                    </p>
                </div>
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingOne1"
                             style="background-color:initial !important;">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                               aria-expanded="true"
                               aria-controls="collapseOne1">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                            @lang('website_contents.global.faq.how_sp_work')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_sp_work_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingTwo2"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                               aria-expanded="false" aria-controls="collapseTwo2">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">

                                    @lang('website_contents.global.faq.how_long_arrive')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_long_arrive_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingThree3"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx"
                               href="#collapseThree3"
                               aria-expanded="false" aria-controls="collapseThree3">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                            @lang('website_contents.global.faq.which_locations')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.which_locations_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFour4"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFour4"
                               aria-expanded="false" aria-controls="collapseFour4">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                            @lang('website_contents.global.faq.offer_free_delivery')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingFour4"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.offer_free_delivery_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingFive5"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseFive5"
                               aria-expanded="false" aria-controls="collapseFive5">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                            @lang('website_contents.global.faq.how_refer_friend')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseFive5" class="collapse" role="tabpanel" aria-labelledby="headingFive5"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_refer_friend_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordB-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordT-grey bordB-grey" role="tab" id="headingSix6"
                             style="background-color:initial !important;">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseSix6"
                               aria-expanded="false" aria-controls="collapseSix6">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                            @lang('website_contents.global.faq.how_much_earn')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseSix6" class="collapse" role="tabpanel" aria-labelledby="headingSix6"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_much_earn_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingSeven7"
                             style="background-color:initial !important;">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseSeven7"
                               aria-expanded="false"
                               aria-controls="collapseSeven7">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                        @lang('website_contents.global.faq.how_keep_track')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseSeven7" class="collapse" role="tabpanel" aria-labelledby="headingSeven7"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_keep_track_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card bord-none bordT-grey" style="background-color:initial !important;">

                        <!-- Card header -->
                        <div class="card-header center-sm bg-none bordB-grey" role="tab" id="headingEight8"
                             style="background-color:initial !important;">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseEight8"
                               aria-expanded="false"
                               aria-controls="collapseEight8">
                                <div class="row">
                                    <div class="col-10">
                                        <h5 class="mb-0 text-left" style="font-family: Muli-Bold; color:black;">
                                        @lang('website_contents.global.faq.how_cash_out')
                                        </h5>
                                    </div>
                                    <div class="col-2">
                                        <i class="fa fa-angle-right rotate-icon" style="float: right; font-size: 25px"></i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseEight8" class="collapse" role="tabpanel" aria-labelledby="headingEight8"
                             data-parent="#accordionEx">
                            <div class="card-body pt-0 text-left">
                                @lang('website_contents.global.faq.how_cash_out_ans')
                            </div>
                        </div>

                    </div>
                    <!-- Accordion card -->

                </div>
                <!-- Accordion wrapper -->
            </div>
        </section>
    </div>
    <!-- Mobile View End -->
@endsection
