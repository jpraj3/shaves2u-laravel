<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Check if User email exists

Route::group(['prefix' => 'check'], function () {
    Route::post('/email', 'Globals\Users\UserController@checkEmailExists')->name('api.check.email');
    Route::post('/existsoractive', 'Globals\Users\UserController@checkEmailExistsOrActive')->name('api.check.existsoractive');
    Route::get('/country-list', 'Globals\API\CountriesController@list')->name('api.all.countries');
    Route::post('/unique', 'Globals\Users\UserController@checkUniqueEmail')->name('api.unique.email');
});

// User Registration
Route::group(['prefix' => 'user'], function () {
    Route::post('/register', 'Globals\Users\UserController@APIUserRegistration')->name('api.user.register');
    Route::post('/list', 'Globals\APIControllers\Admin\Users\UsersAPIController@APIUsers')->name('api.user.list');
});

Route::group(['prefix' => 'admin'], function () {
    Route::post('/', 'Globals\APIControllers\Admin\Admin\AdminAPIController@getAdmins');
    Route::post('/create', 'Globals\APIControllers\Admin\Admin\AdminAPIController@createAdmin');
    Route::post('/update/{id}', 'Globals\APIControllers\Admin\Admin\AdminAPIController@updateAdmin');
    // Route::post('/authenticate', 'Globals\Admin\AdminController@authenticate');// User Registration

    Route::group(['prefix' => 'user'], function () {
        Route::post('/list', 'Globals\APIControllers\Admin\Users\UsersAPIController@APIUsers')->name('api.user.list');
        Route::group(['prefix' => 'customer'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomersCount')->name('api.user.customers.count');
            Route::post('/list', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomers')->name('api.user.customers.list');
            Route::post('/details', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerDetails')->name('api.user.customers.list.details');
            Route::post('/status/update/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerStatusUpdate')->name('api.user.customers.status.update');
            Route::post('/profile/update/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerProfileUpdate')->name('api.user.customers.profile.update');
            Route::group(['prefix' => 'address'], function () {
                Route::post('/select/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerSelectAddress')->name('api.user.customers.address.select');
                Route::post('/create/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerCreateAddress')->name('api.user.customers.address.create');
                Route::post('/update/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerUpdateAddress')->name('api.user.customers.address.update');
                Route::post('/remove/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerRemoveAddress')->name('api.user.customers.address.remove');
            });
            Route::group(['prefix' => 'card'], function () {
                Route::post('/select/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerSelectCard')->name('api.user.customers.card.select');
                Route::post('/update/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerUpdateCard')->name('api.user.customers.card.update');
                Route::post('/remove/{id}', 'Globals\APIControllers\Admin\Users\UsersAPIController@APICustomerRemoveCard')->name('api.user.customers.card.remove');
            });
        });
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::post('/count', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrdersCount')->name('api.orders.count');
        Route::post('/list', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrders')->name('api.orders.list');
        Route::post('/details', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrderDetails')->name('api.orders.orderdetails');
        Route::post('/status/update', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrderStatusUpdate')->name('api.orders.status.update');
        Route::post('/status/paymenthistoriesupdate', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrderPaymentStatusUpdate')->name('api.orders.status.paymenthistoriesupdate');
        Route::post('/tracking/update', 'Globals\APIControllers\Admin\Orders\OrderAPIController@APIOrderTrackingUpdate')->name('api.orders.tracking.update');
    });

    Route::group(['prefix' => 'bulkorders'], function () {
        Route::post('/count', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIBulkOrdersCount')->name('api.bulkorders.count');
        Route::post('/list', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIBulkOrders')->name('api.bulkorders.list');
        Route::post('/details', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIBulkOrderDetails')->name('api.bulkorders.bulkorderdetails');
        Route::post('/upload/bulkorders', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIUploadBulkOrder')->name('api.bulkorders.upload');
        Route::post('/status/update', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIBulkOrderStatusUpdate')->name('api.bulkorders.status.update');
        Route::post('/tracking/update', 'Globals\APIControllers\Admin\BulkOrders\BulkOrderAPIController@APIBulkOrderTrackingUpdate')->name('api.bulkorders.tracking.update');
    });

    Route::group(['prefix' => 'download'], function () {
        Route::post('/orders/list', 'Globals\APIControllers\Admin\DownloadController@APIOrders')->name('api.download.orders.list');
        Route::post('/bulkorders/list', 'Globals\APIControllers\Admin\DownloadController@APIBulkOrders')->name('api.download.bulkorders.list');
        Route::post('/subscribers/list', 'Globals\APIControllers\Admin\DownloadController@APISubscribers')->name('api.download.subscribers.list');
        Route::post('/subscriberssummary/list', 'Globals\APIControllers\Admin\DownloadController@APISubscribersSummary')->name('api.download.subscribers.summary.list');
        Route::post('/cancellations/list', 'Globals\APIControllers\Admin\DownloadController@APICancellations')->name('api.download.cancellations.list');
        Route::post('/referrals/list', 'Globals\APIControllers\Admin\DownloadController@APIReferrals')->name('api.download.referrals.list');
        Route::post('/referralsCashout/list', 'Globals\APIControllers\Admin\DownloadController@APIReferralsCashOut')->name('api.download.referrals.cashout.list');
        Route::post('/recharges/list', 'Globals\APIControllers\Admin\DownloadController@APIRecharges')->name('api.download.recharges.list');
        Route::post('/pauseplans/list', 'Globals\APIControllers\Admin\DownloadController@APIPausePlans')->name('api.download.pauseplans.list');
        Route::post('/customers/list', 'Globals\APIControllers\Admin\DownloadController@APICustomers')->name('api.download.customers.list');

        Route::group(['prefix' => 'reports'], function () {
            Route::post('/master/list', 'Globals\APIControllers\Admin\DownloadController@APIReportsMaster')->name('api.download.report.master.list');
            Route::post('/mastersummary/list', 'Globals\APIControllers\Admin\DownloadController@APIReportsMasterSummary')->name('api.download.report.master.summary.list');
            Route::post('/appco/list', 'Globals\APIControllers\Admin\DownloadController@APIReportsAppco')->name('api.download.report.appco.list');
            Route::post('/mo/list', 'Globals\APIControllers\Admin\DownloadController@APIReportsMO')->name('api.download.report.mo.list');
        });
        // Route::post('/bulk-order-list', 'Globals\APIControllers\Admin\DownloadController@APIBulkOrders')->name('api.download.bulkorders.list');
        // Route::post('/sales-list', 'Globals\APIControllers\Admin\DownloadController@APISales')->name('api.download.sales.list');
        // Route::post('/subscription-list', 'Globals\APIControllers\Admin\DownloadController@APISubscribers')->name('api.download.subscriptions.subscribers');
        Route::group(['prefix' => 'tax-invoice'], function () {
            Route::post('/order', 'Globals\APIControllers\Admin\DownloadController@APIOrderTaxInvoice')->name('api.download.orders.tax-invoice');
            Route::post('/bulkorder', 'Globals\APIControllers\Admin\DownloadController@APIBulkOrderTaxInvoice')->name('api.download.orders.tax-invoice');
        });
    });

    Route::group(['prefix' => 'sales'], function () {
        Route::post('/list', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APISales')->name('api.sales.list');
    });

    Route::group(['prefix' => 'countries'], function () {
        Route::post('/list', 'Globals\GeoLocation\LocaleController@APICountries')->name('api.countries.list');
    });

    Route::group(['prefix' => 'skus'], function () {
        Route::post('/list', 'Globals\APIControllers\Admin\Products\ProductAPIController@APISKUs')->name('api.skus.list');
    });

    Route::group(['prefix' => 'planskus'], function () {
        Route::post('/list', 'Globals\APIControllers\Admin\Plans\PlanAPIController@APIPlanSKUs')->name('api.planskus.list');
    });

    Route::group(['prefix' => 'subscriptions'], function () {
        Route::group(['prefix' => 'subscribers'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Subscriptions\SubscriptionAPIController@APISubscribersCount')->name('api.subscribers.count');
            Route::post('/', 'Globals\APIControllers\Admin\Subscriptions\SubscriptionAPIController@APISubscribers')->name('api.subscriptions.subscribers');
            Route::post('/details', 'Globals\APIControllers\Admin\Subscriptions\SubscriptionAPIController@APISubscriberDetails')->name('api.subscriptions.subscriberDetails');
            Route::post('/status/update', 'Globals\APIControllers\Admin\Subscriptions\SubscriptionAPIController@APISubscriberStatusUpdate')->name('api.subscriptions.status.update');
        });

        Route::group(['prefix' => 'recharges'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Recharges\RechargeAPIController@APIRechargesCount')->name('api.recharges.count');
            Route::post('/', 'Globals\APIControllers\Admin\Recharges\RechargeAPIController@APIRecharges')->name('api.subscriptions.recharges');
        });

        Route::group(['prefix' => 'cancellations'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Cancellations\CancellationAPIController@APICancellationsCount')->name('api.cancellations.count');
            Route::post('/', 'Globals\APIControllers\Admin\Cancellations\CancellationAPIController@APICancellations')->name('api.subscriptions.cancellations');
        });

        Route::group(['prefix' => 'pauseplans'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\PausePlans\PausePlansAPIController@APIPausePlansCount')->name('api.pauseplans.count');
            Route::post('/', 'Globals\APIControllers\Admin\PausePlans\PausePlansAPIController@APIPausePlans')->name('api.subscriptions.pauseplans');
        });
    });

    Route::group(['prefix' => 'referrals'], function () {
        Route::post('/count', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralsCount')->name('api.referrals.count');
        Route::post('/', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferrals')->name('api.referrals.referrals');
        Route::post('/details', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralDetails')->name('api.referrals.referralDetails');
        Route::post('/details/count', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralDetailsCount')->name('api.referrals.referralDetails.count');

        Route::group(['prefix' => 'cashout'], function () {
            Route::post('/', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralsCashOut')->name('api.referrals.referralscashout');
            Route::post('/count', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralsCashOutCount')->name('api.referralscashout.count');
            Route::post('/details', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralCashOutDetails')->name('api.referrals.referralcashoutDetails');
            Route::post('/details/count', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralCashOutDetailsCount')->name('api.referrals.referralcashoutDetails.count');
            Route::post('/status/update', 'Globals\APIControllers\Admin\Referrals\ReferralAPIController@APIReferralCashOutStatusUpdate')->name('api.referralscashout.status.update');
        });
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::group(['prefix' => 'master'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIMastersCount')->name('api.reports.master.count');
            Route::post('/', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIMasters')->name('api.reports.master');
        });
        Route::group(['prefix' => 'appco'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIAppcoCount')->name('api.reports.appco.count');
            Route::post('/', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIAppco')->name('api.reports.appco');
        });
        Route::group(['prefix' => 'mo'], function () {
            Route::post('/count', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIMOCount')->name('api.reports.mo.count');
            Route::post('/', 'Globals\APIControllers\Admin\Reports\ReportsAPIController@APIMO')->name('api.reports.mo');
        });
    });

    Route::group(['prefix' => 'ba'], function () {
        Route::post('/create', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@APICreateSellerUser')->name('api.ba.create');
        Route::post('/count', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@APISellerUsersCount')->name('api.ba.count');
        Route::post('/list', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@APISellerUsers')->name('api.ba.list');
        Route::post('/status/update/{id}', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@APIChangeSellerUserStatus')->name('api.ba.status.update');
        Route::post('/upload', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@APIUploadSellerUsers')->name('api.ba.upload');
    });

    Route::group(['prefix' => 'mo'], function () {
        Route::post('/list', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@getMOs');
        Route::post('/create', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@createMO');
        Route::post('/update/{id}', 'Globals\APIControllers\Admin\BaWebsite\BaWebsiteAPIController@updateMO');
    });

    Route::group(['prefix' => 'export'], function () {
        Route::post('/count', 'Globals\APIControllers\Admin\Export\ExportAPIController@APIExportCount')->name('api.export.count');
        Route::post('/list', 'Globals\APIControllers\Admin\Export\ExportAPIController@APIExportList')->name('api.export.list');
    });
});

// ---------------------------------------------- BAWebsite API
Route::group(['middleware' => 'cors'], function () {
    Route::group(['prefix' => 'ba'], function () {

        // ------------- ecommerce customers action
        Route::group(['prefix' => 'user'], function () {
            // user data checking
            Route::group(['prefix' => 'check'], function () {
                Route::post('/email', 'Globals\APIControllers\BaWebsite\BACustomerController@checkEmailExists')->name('api.ba.user.check.email');
                Route::post('/phone', 'Globals\APIControllers\BaWebsite\BACustomerController@checkPhoneExists')->name('api.ba.user.check.phone');
                Route::post('/trial', 'Globals\APIControllers\BaWebsite\BACustomerController@checkTrialPlanExist')->name('api.ba.user.check.applied.trial');
                Route::post('/promotion', 'Globals\Promotions\PromotionController@checkPromotionAPI')->name('API.locale.region.promotion');
            });
            Route::post('/register', 'Globals\APIControllers\BaWebsite\BACustomerController@registerBAUsers')->name('api.ba.user.register');

            // user data checking
            Route::group(['prefix' => 'address'], function () {
                Route::post('/edit/{user_id}', 'Globals\APIControllers\BaWebsite\BACustomerController@editDelivery')->name('api.ba.user.address.edit');
                Route::post('/add', 'Globals\APIControllers\BaWebsite\BACustomerController@addDelivery')->name('api.ba.user.address.add');
            });
        });

        // plan purchase
        Route::group(['prefix' => 'plan'], function () {
            Route::post('/confirm-purchase', 'Globals\Plans\PlansController@confirmPurchase')->name('api.locale.region.checkout.confirm-purchase');
            Route::post('/nicepay/confirm-purchase', 'Globals\Plans\PlansNicePayController@confirmPurchase')->name('api.locale.region.checkout.nicepay.confirm-purchase');
        });

        // ------------- products & plans list
        Route::group(['prefix' => 'products'], function () {
            Route::post('/list', 'Globals\APIControllers\BaWebsite\PGController@getListsBasedOnGender')->name('api.ba.gender.product.list');
            Route::post('/get-all-types-list', 'Globals\APIControllers\BaWebsite\PGController@getMainProductTypeListsInfo')->name('api.ba.gender.main.product.list');

            Route::group(['prefix' => 'trial'], function () {
                Route::post('/list', 'Globals\APIControllers\BaWebsite\Plans\BATrialPlanController@apiEntryPoint')->name('api.ba.products.main.trial.list');
                Route::post('/related-annual-plan', 'Globals\APIControllers\BaWebsite\Plans\BATrialPlanController@getRelatedAnnualPlan')->name('api.ba.products.main.trial.related_annual_plan');
                Route::post('/start-checkout', 'Globals\APIControllers\BaWebsite\Plans\BATrialPlanController@proceedCheckout')->name('api.ba.products.main.trial.proceed.checkout');
            });

            Route::group(['prefix' => 'alacarte'], function () {
                Route::post('/list', 'Globals\APIControllers\BaWebsite\ProdController@getItemListsBasedOnGender')->name('api.ba.products.main.alacarte.list');
                Route::post('/start-checkout', 'Globals\APIControllers\BaWebsite\ProdController@proceedAlacarteCheckout')->name('api.ba.products.main.alacarte.proceed.checkout');
            });

            Route::post('/confirm-purchase', 'Globals\Products\ProductController@confirmPurchase')->name('api.locale.region.products.checkout.confirm-purchase');
            Route::post('/complete-purchase', 'Globals\Products\ProductCashController@confirmPurchase')->name('api.locale.region.products.checkout.complete-purchase');
        });

        // ------------- stripe payment gateway
        Route::group(['prefix' => 'stripe'], function () {
            // Add/Edit/Delete Card
            Route::group(['prefix' => 'card'], function () {
                Route::post('/', 'Globals\Payment\APIStripeController@listCard')->name('api.locale.region.user.stripe.list-card');
                Route::post('/each', 'Globals\Payment\APIStripeController@getEachCardBA')->name('api.locale.region.user.stripe.each-card');
                Route::post('/add', 'Globals\Payment\APIStripeController@addCardBA')->name('api.locale.region.user.stripe.add-card');
                Route::post('/select', 'Globals\Payment\APIStripeController@selectCardBA')->name('api.locale.region.user.stripe.select-card');
            });

            // Add/Edit Customer
            Route::group(['prefix' => 'customer'], function () {
                Route::post('/add', 'Globals\Payment\APIStripeController@addCustomer')->name('api.locale.region.user.stripe.add-customer');
            });

            // Add/Edit Charges
            Route::group(['prefix' => 'charge'], function () {
                Route::post('/add', 'Globals\Payment\APIStripeController@addCharges')->name('api.locale.region.user.stripe.add-charge');
            });

            // Update PaymentIntent
            Route::group(['prefix' => 'payment-intent'], function () {
                Route::post('/update', 'Globals\Payment\APIStripeController@updatePaymentIntent')->name(('api.locale.region.user.stripe.payment-intent.update'));
            });

            // Regenerate PaymentIntents
            Route::post('/regenerate-payment-intents', 'Globals\APIControllers\BaWebsite\Plans\BATrialPlanController@generatePaymentIntents')->name(('api.locale.region.user.stripe.payment-intent.generate'));
            // Confirm Payment Intent
            Route::post('/confirm', 'Globals\Payment\APIStripeController@confirmPaymentIntent')->name(('api.locale.region.user.stripe.confirm'));
            // Post Payment Redirect
            Route::post('/redirect', 'Globals\Payment\APIStripeController@postPaymentRedirect')->name(('api.locale.region.user.stripe.redirect'));

            Route::post('/regenerate-alacarte-payment-intents', 'Globals\APIControllers\BaWebsite\ProdController@generatePaymentIntents')->name(('api.locale.region.user.stripe.alacarte-payment-intent.generate'));
        });
    });
});
