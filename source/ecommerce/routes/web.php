<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*
| -----------------------------------
| Jason Paulraj - Routes Implemented
| -----------------------------------
 */

 //campaign
 Route::get('/welcomebackpromo', 'Globals\Campaigns\WelcomeBackPromoController@annualPlanPromoPage')->name('campaign.annualplanpromo');
 Route::post('/welcomebackpromo/updatesubscription', 'Globals\Campaigns\WelcomeBackPromoController@annualPlanPromoUpdateSubscription')->name('campaign.annualplanpromoupdatesubscription');
 Route::get('/welcomebackpromo/thankyou', 'Globals\Campaigns\WelcomeBackPromoController@annualPlanPromoThankyouPage')->name('campaign.annualplanpromothankyouPage');

// Global HTML Templates
Route::group(['prefix' => 'templates'], function () {
    Route::post('/popup', 'Globals\Misc\PopupController@retrieveTemplate')->name('template.popup');
    Route::get('/popupko', 'Globals\Misc\PopupController@retrieveTemplateKO')->name('template.popupko');
    Route::get('/referral-share', 'Ecommerce\LandingController@returnReferralShare')->name('template.share.referral');
});

// Webhooks
Route::group(['prefix' => 'webhooks'], function () {
    // aftership return API
    Route::post('/aftership', 'Globals\Warehouse\LFController@updateOrderTrackings')->name('webhooks.aftership');
});
Route::group(['prefix' => 'social'], function () {
    // Facebook
    Route::get('auth/facebook', 'Auth\LoginController@authenticate_facebook')->name('auth.facebook');
    Route::get('auth/facebook/callback', 'Auth\LoginController@authenticate_facebook_callback')->name('auth.facebook.callback');
    // Google
    Route::get('auth/google', 'Auth\LoginController@authenticate_google')->name('auth.google');
    Route::get('auth/google/callback', 'Auth\LoginController@authenticate_google_callback')->name('auth.google.callback');
});

// This part Links to Locale Middleware
Route::get('/', 'Globals\GeoLocation\LocaleController@index')->name('locale.index');
Route::post('/ajax/autoCreatePlan', 'Globals\Plans\CustomPlanController@autoCreatePlan')->name('locale.autoCreatePlan');
// Sessions
Route::post('/ajax/session', 'Globals\Utilities\SessionController@session')->name('locale.session');

// Locale
Route::get('lang-select/{langCodeList}', 'Globals\GeoLocation\LocaleController@changeActiveLanguage')->name('locale.changeActiveLanguage');




Route::group(['prefix' => '{langCode}-{countryCode}', 'middleware' => 'locale'], function () {
    Auth::routes();
    Route::post('/logoutV2', 'Auth\LoginController@logoutV2')->name('locale.logoutV2');
    Route::post('/password/email/new', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email.new');
    Route::post('/register_ecommerce', 'Auth\RegisterController@register')->name('ecommerce.registration');

    Route::group(['prefix' => 'magazine'], function () {
        Route::get('/', 'Ecommerce\Magazines\MagazinesController@index')->name('locale.magazines.list');
        Route::get('/{magazine_name}', 'Ecommerce\Magazines\MagazinesController@content')->name('locale.magazines.content');
    });

    // Global HTML Templates
    Route::group(['prefix' => 'share'], function () {
        Route::get('/referral', 'Ecommerce\LandingController@returnReferralShare')->name('template.share.referral');
    });

    // Testing
    Route::group(['prefix' => 'test'], function () {
        Route::get('/stripe', 'Globals\Payment\PaymentController@stripe')->name('locale.region.test.stripe');
        Route::get('/urbanfox', 'Globals\Warehouse\UrbanFoxController@index')->name('locale.region.test.urbanfox');
        Route::get('/lf', 'Globals\Warehouse\LFController@index')->name('locale.region.test.urbanfox');
    });

    // General Pages
    Route::get('/', 'Globals\GeoLocation\LocaleController@region')->name('locale.region');

    //nicepay
    Route::post('/nicepay/card/add', 'Globals\Payment\NicePayController@nicepayAddCard')->name('locale.region.user.nicepay.addCard');

    // Shave Plans
    Route::get('/shave-plans', 'Ecommerce\Landing\PagesController@shavePlansView')->name('locale.region.shave-plans');
    Route::post('/shave-plans/checkout/confirm-purchase', 'Globals\Plans\PlansController@confirmPurchase')->name('locale.region.shave-plans.checkout.confirm-purchase');
    Route::post('/shave-plans/checkout/nicepay/confirm-purchase', 'Globals\Plans\PlansNicePayController@confirmPurchase')->name('locale.region.shave-plans.checkout.nicepay.confirm-purchase');

    // Trial Plans
    Route::get('/shave-plans/trial-plan', 'Ecommerce\Landing\PagesController@trialPlanSelectionView')->name('locale.region.shave-plans.trial-plan');
    Route::get('/shave-plans/trial-plan/selection', 'Ecommerce\Landing\PagesController@trialPlanSelectionView')->name('locale.region.shave-plans.trial-plan.selection');
    Route::get('/shave-plans/trial-plan/checkout', 'Globals\Plans\TrialPlanController@proceedCheckout')->name('locale.region.shave-plans.trial-plan.checkout');
    Route::get('/shave-plans/trial-plan/thankyou', 'Globals\Plans\TrialPlanController@proceedThankYou')->name('locale.region.shave-plans.trial-plan.thankyou');
    Route::get('/shave-plans/trial-plan/check-trial-plan-exist/{userid}', 'Globals\Plans\TrialPlanController@checkTrialPlanExist')->name('locale.region.shave-plans.trial-plan.check-trial-plan-exist');
    Route::post('/shave-plans/trial-plan/converttrialtoannual', 'Globals\Plans\TrialPlanController@converttrialtoannual')->name('locale.region.shave-plans.trial-plan.convert-trial-to-annual');

    // Custom plan
    Route::get('/shave-plans/custom-plan', 'Ecommerce\Landing\PagesController@customPlanView')->name('locale.region.shave-plans.custom-plan');
    Route::get('/shave-plans/custom-plan/selection', 'Ecommerce\Landing\PagesController@customPlanSelectionView')->name('locale.region.shave-plans.custom-plan.selection');
    Route::get('/shave-plans/custom-plan/checkout', 'Globals\Plans\CustomPlanController@proceedCheckout')->name('locale.region.shave-plans.custom-plan.checkout');
    Route::get('/shave-plans/custom-plan/checkout/summary', 'Globals\Plans\CustomPlanController@proceedSummary')->name('locale.region.shave-plans.custom-plan.checkout.summary');
    Route::post('/shave-plans/custom-plan/convertcustomtoannual', 'Globals\Plans\CustomPlanController@convertcustomtoannual')->name('locale.region.shave-plans.custom-plan.convert-custom-to-annual');
    // Route::post('/shave-plans/custom-plan/checkout/payment', 'Globals\Plans\CustomPlanController@proceedPayment')->name('locale.region.shave-plans.custom-plan.checkout.payment');

    // Awesome Shave Kits
    Route::get('/ask', 'Ecommerce\Landing\PagesController@ASK')->name('locale.region.alacarte.ask');
    Route::get('/ask/checkout', 'Globals\Products\ASKController@checkout')->name('locale.region.alacarte.ask.checkout');
    Route::post('/ask/checkout/confirm-purchase', 'Globals\Products\ProductController@confirmPurchase')->name('locale.region.alacarte.ask.checkout.confirm-purchase');
    Route::post('/ask/checkout/nicepay/confirm-purchase', 'Globals\Products\ProductNicePayController@confirmPurchase')->name('locale.region.alacarte.ask.nicepay.checkout.confirm-purchase');

    //Thank You
    Route::get('/thankyou/{id}', 'Globals\ThankYou\ThankYouController@index')->name('locale.thankyou');

    // mask selection
    Route::post('/check/mask/qty', 'Ecommerce\LandingController@checkmaskqty')->name('locale.region.check.mask.qty');
    // mask selection

    // Career
    Route::get('/career', 'Ecommerce\HomeController@career')->name('locale.region.career');

    // About Us
    Route::get('/about-us', 'Ecommerce\HomeController@about')->name('locale.region.about');

    // Product
    Route::get('/product', 'Ecommerce\HomeController@product')->name('locale.region.product');
    // Product - Handle
    Route::get('/product/handle', 'Ecommerce\HomeController@handle')->name('locale.region.handle');
    // Product - Shave cream
    Route::get('/product/shave-cream', 'Ecommerce\HomeController@shave_cream')->name('locale.region.shave-cream');
    // Product - Aftershave cream
    Route::get('/product/aftershave', 'Ecommerce\HomeController@aftershave')->name('locale.region.aftershave');
    // Product - Aftershave cream
    Route::get('/product/blade', 'Ecommerce\HomeController@blade')->name('locale.region.blade');
    // Product - Awesome Shave Kit
    Route::get('/product/ask', 'Ecommerce\Landing\PagesController@ASK')->name('locale.region.ask');
    // Product - Product Addons
    Route::get('/product/product-addons', 'Ecommerce\Landing\PagesController@productAddons')->name('locale.region.product-addons');
    Route::post('/product/product-addons/{id}', 'Ecommerce\Landing\PagesController@productAddonsSelectedSubs')->name('locale.region.product-addons-selected-sub');
    Route::post('/product/subs-list', 'Ecommerce\Landing\PagesController@productAddonsGetSubsList')->name('locale.region.product-addons-subs-list');

    // Terms and Conditions
    Route::get('/terms-of-use', 'Ecommerce\HomeController@tnc')->name('locale.region.tnc');

    // FAQ
    Route::get('/faq', 'Ecommerce\HomeController@faq')->name('locale.region.faq');

    // Privacy Policy
    Route::get('/privacy', 'Ecommerce\HomeController@privpolicy')->name('locale.region.privpolicy');

    // Activate Account
    Route::get('/activate/{email}', 'Globals\Users\UserController@activate')->name('locale.region.activate');

    // Contact Us
    Route::get('/contact-us', 'Ecommerce\HomeController@contact')->name('locale.region.contact');
    Route::post('/contact-us', 'Ecommerce\HomeController@handleContactFormSubmit')->name('locale.region.contact.submit');

    // Referral
    Route::get('/referral', 'Ecommerce\HomeController@referral')->name('locale.region.referral');

    // Promotion
    Route::post('/promotion/check', 'Globals\Promotions\PromotionController@checkPromotion')->name('locale.region.promotion');

    // Referrals
    Route::group(['prefix' => 'referral'], function () {
        Route::post('/get-referrer-info', 'Ecommerce\Rebates\Referral\ReferralController@getReferrerInfo')->name('locale.region.authenticated.referral.get_referrer_info');
    });

    // Authenticated Users here ---
    Route::group(['middleware' => 'auth'], function () {
        // User
        Route::group(['prefix' => 'user'], function () {
            Route::get('/home', 'Ecommerce\LandingController@index')->name('locale.region.authenticated');
            Route::get('/dashboard', 'Ecommerce\LandingController@dashboard')->name('locale.region.authenticated.dashboard');
            Route::get('/shave-plan', 'Ecommerce\LandingController@savedPlans')->name('locale.region.authenticated.savedplans');
            Route::get('/shave-plan/edit/{subscriptionId}', 'Ecommerce\LandingController@EditShavePlans')->name('locale.region.authenticated.savedplans.edit');
            Route::post('/shave-plan/paynow', 'Globals\Plans\PayNowController@MainProcess')->name('locale.region.authenticated.shaveplans.paynow');
            Route::post('/shave-plan/paynow/nicepay', 'Globals\Plans\PayNowNicepayController@MainProcess')->name('locale.region.authenticated.shaveplans.paynow.nicepay');

            Route::post('/shave-plan/edit/cards', 'Ecommerce\LandingController@shavePlanUpdateCard')->name('locale.region.authenticated.savedplans.edit.cards');
            Route::post('/shave-plan/edit/address', 'Ecommerce\LandingController@EditShavePlans')->name('locale.region.authenticated.savedplans.edit.address');
            Route::post('/ssss', 'Ecommerce\LandingController@updateSubsProducts')->name('locale.region.user.update_subs_products');
            Route::post('/checkfrequency', 'Ecommerce\LandingController@updateSubsFrequency')->name('locale.region.user.update_subs_frequency');
            Route::post('/checkbladetype', 'Ecommerce\LandingController@getPlansBasedOnBlade')->name('locale.region.user.update_subs_blade_pack');
            Route::get('/order-plan', 'Ecommerce\LandingController@orderHistory')->name('locale.region.authenticated.orderhistory');
            Route::get('/order-plan/details/{id}', 'Ecommerce\LandingController@orderHistoryInfo')->name('locale.region.authenticated.orderhistory.details');
            Route::get('/referrals', 'Ecommerce\LandingController@referrals')->name('locale.region.authenticated.referrals');
            Route::get('/profile-info', 'Ecommerce\LandingController@profile')->name('locale.region.authenticated.profile');

            // Referrals
            Route::group(['prefix' => 'referrals'], function () {
                Route::post('/send-invitation-email', 'Ecommerce\Rebates\Referral\ReferralController@sendInvitationEmail')->name('locale.region.authenticated.referral.send_email');
                Route::post('/withdraw-cash', 'Ecommerce\Rebates\Referral\ReferralController@cashOut')->name('locale.region.authenticated.referral.cash_out');
                Route::post('/cash', 'Ecommerce\Rebates\Referral\ReferralController@calculateReferralCash')->name('locale.region.authenticated.referral.cash');
            });

            // Profile Updates
            Route::group(['prefix' => 'updates'], function () {
                Route::post('/info', 'Ecommerce\User\UserController@updateUserProfile')->name('locale.region.authenticated.user.profile.update_info');
                Route::post('/delivery', 'Ecommerce\User\UserController@updateUserAddress')->name('locale.region.authenticated.user.profile.update_default_address');
                Route::post('/cards/add', 'Ecommerce\User\UserController@addNewCard')->name('locale.region.authenticated.user.profile.add_card');
                Route::post('/cards/add-api', 'Ecommerce\User\UserController@addNewCardAPI')->name('locale.region.authenticated.user.profile.add_card.api');
                Route::post('/cards/default', 'Ecommerce\User\UserController@updateDefaultCard')->name('locale.region.authenticated.user.profile.update_default_card');
                Route::post('/cards/delete', 'Ecommerce\User\UserController@deleteCard')->name('locale.region.authenticated.user.profile.delete_card');
                Route::post('/profileimage', 'Ecommerce\User\UserController@uploadImage')->name('image.upload');
                Route::post('/delivery/default', 'Ecommerce\User\UserController@updateDefaultAddress')->name('locale.region.authenticated.user.profile.delivery_default_address');
                Route::post('/confirm_shave_plan', 'Ecommerce\LandingController@updateShavePlan')->name('locale.region.authenticated.user.profile.confirm_shave_plan');
                Route::post('/confirm_shave_plan_annuals', 'Ecommerce\LandingController@updateShavePlanAnnuals')->name('locale.region.authenticated.user.profile.confirm_shave_plan_annuals');
                Route::post('/pause_shave_plan', 'Ecommerce\LandingController@pauseSubscription')->name('locale.region.authenticated.user.profile.pause_shave_plan');
            });
        });
        // Cancellation Journey
        Route::group(['prefix' => 'cancellation-journey'], function () {
            Route::get('/navigate/{subscriptionid}/{cancellationreasonid}/{view}', 'Globals\CancellationJourney\CancellationJourneyController@Navigate')->name('locale.region.authenticated.cancellationjourney');
            Route::post('/action/cancel_subscription', 'Globals\CancellationJourney\CancellationJourneyController@CancelSubscription')->name('locale.region.authenticated.cancellationjourney.action.cancel');
            Route::post('/action/continue_subscription', 'Globals\CancellationJourney\CancellationJourneyController@ContinueSubscription')->name('locale.region.authenticated.cancellationjourney.action.continue');
            Route::post('/action/check_cancellation_benefit_status', 'Globals\CancellationJourney\CancellationJourneyController@GetCancellationBenefitStatusAPI')->name('locale.region.authenticated.cancellationjourney.action.check_benefit');
            Route::get('/notify/cancelled', 'Globals\CancellationJourney\CancellationJourneyController@NotifyCancelled')->name('locale.region.authenticated.cancellationjourney.notify.cancelled');
            Route::get('/notify/changed_blade/{blade_sku}', 'Globals\CancellationJourney\CancellationJourneyController@NotifyChangedBlade')->name('locale.region.authenticated.cancellationjourney.notify.changed_blade');
            Route::get('/notify/applied_free_blade/{blade_sku}', 'Globals\CancellationJourney\CancellationJourneyController@NotifyAppliedFreeBlade')->name('locale.region.authenticated.cancellationjourney.notify.applied_free_blade');
        });

        // Stripe
        Route::group(['prefix' => 'stripe'], function () {
            // Start/End OTP Stripe
            Route::group(['prefix' => 'otp'], function () {
                Route::post('/start', 'Globals\Payment\StripeController@otpstart')->name('locale.region.user.stripe.otpstart');
                Route::post('/stop', 'Globals\Payment\StripeController@otpstop')->name('locale.region.user.stripe.otpstop');
            });
            // Add/Edit/Delete Card
            Route::group(['prefix' => 'card'], function () {
                Route::post('/', 'Globals\Payment\StripeController@listCard')->name('locale.region.user.stripe.list-card');
                Route::post('/each', 'Globals\Payment\StripeController@getEachCard')->name('locale.region.user.stripe.each-card');
                Route::post('/add', 'Globals\Payment\StripeController@addCard')->name('locale.region.user.stripe.add-card');
                Route::post('/add-api', 'Globals\Payment\StripeController@addCardAPI')->name('locale.region.user.stripe.add-card-api');
                Route::post('/select', 'Globals\Payment\StripeController@selectCard')->name('locale.region.user.stripe.select-card');
                Route::post('/edit/{id}', 'Globals\Payment\StripeController@editCard')->name('locale.region.user.stripe.edit-card');
                Route::post('/destroy/{id}', 'Globals\Payment\StripeController@destroyCard')->name('locale.region.user.stripe.destroy-card');
            });
            // Add/Edit Customer
            Route::group(['prefix' => 'customer'], function () {
                Route::post('/', 'Globals\Payment\StripeController@listCustomer')->name('locale.region.user.stripe.list-customer');
                Route::post('/add', 'Globals\Payment\StripeController@addCustomer')->name('locale.region.user.stripe.add-customer');
                Route::post('/edit/{id}', 'Globals\Payment\StripeController@editCustomer')->name('locale.region.user.stripe.edit-customer');
            });
            // Add/Edit Charges
            Route::group(['prefix' => 'charge'], function () {
                Route::post('/add', 'Globals\Payment\StripeController@addCharges')->name('locale.region.user.stripe.add-charge');
                Route::post('/edit/{id}', 'Globals\Payment\StripeController@editCharges')->name('locale.region.user.stripe.edit-charge');
            });
            // Add/Edit Refunds
            Route::group(['prefix' => 'refund'], function () {
                Route::post('/create', 'Globals\Payment\StripeController@addRefund')->name('locale.region.user.stripe.create-refund');
            });

            // Update PaymentIntent
            Route::group(['prefix' => 'payment-intent'], function () {
                Route::post('/update', 'Globals\Payment\StripeController@updatePaymentIntent')->name(('locale.region.user.stripe.payment-intent.update'));
                Route::post('/otp-charge', 'Globals\Payment\StripeController@createOneTimeCharge')->name(('locale.region.user.stripe.payment-intent.one-time-charge'));
                Route::post('/otp-charge-api', 'Globals\Payment\StripeController@createOneTimeChargeAPI')->name(('locale.region.user.stripe.payment-intent.one-time-charge.api'));
                Route::get('/otp-return', 'Globals\Payment\StripeController@OneTimeChargeReturn')->name(('locale.region.user.stripe.payment-intent.one-time-charge-return'));
            });

            // Confirm Payment Intent
            Route::post('/confirm', 'Globals\Payment\StripeController@confirmPaymentIntent')->name(('locale.region.user.stripe.confirm'));
            Route::post('/confirm-api', 'Globals\Payment\StripeController@confirmPaymentIntentAPI')->name(('locale.region.user.stripe.confirm.api'));

            // Post Payment Redirect
            Route::get('/redirect', 'Globals\Payment\StripeController@postPaymentRedirect')->name(('locale.region.user.stripe.redirect'));
        });

        // User Actions

        // Delivery & Billing - Add/Edit/Delete
        Route::group(['prefix' => 'delivery-address'], function () {
            Route::post('/add', 'Globals\Users\UserController@add')->name('locale.region.user.delivery-address.add');
            Route::post('/edit/{id}', 'Globals\Users\UserController@edit')->name('locale.region.user.delivery-address.edit');
        });
    });
    Route::get('/{route_name}', 'Errors\PageExistsController@pageExists');
});
Route::get('/{route_name}', 'Errors\PageExistsController@pageExists');

Route::get('/home', 'HomeController@index')->name('home');
