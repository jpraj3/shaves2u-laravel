<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    // Initialize cronjobs
    protected $commands = [];

    // Setup cronjobs
    protected function schedule(Schedule $schedule)
    {
        $env = config('app.env');
        if ($env === 'jobserver') {
            $this->commands = [
                \App\Jobs\CreateOrderInboundJob::class,
                \App\Jobs\SubscriptionChargeJob::class,
                \App\Jobs\SubscriptionRechargeJob::class,
                \App\Jobs\SubscriptionChargeNicepayJob::class,
                \App\Jobs\SubscriptionRechargeNicepayJob::class,
                \App\Jobs\SendReferralInvite7Days::class,
                \App\Jobs\CancellationJourneyEdmJob::class,
                \App\Jobs\SendFollowUpTrialEmailJob::class,
                \App\Jobs\UpdateSalesReportJob::class,
                \App\Jobs\TaxInvoiceJob::class,
                \App\Jobs\UpdateUrbanFoxOrderJob::class,
                \App\Jobs\SalesReportSummaryTblJob::class,
                \App\Jobs\SubscriberSummaryTblJob::class,
                \App\Jobs\UpdateAftershipTracking::class,
                \App\Jobs\ReportQueueGenerateJob::class,
                // \App\Jobs\RetriggerCreateOrderInboundJob::class,
                // \App\Jobs\RetriggerPostAfterShipJob::class,
            ];

            $schedule->command('subscription-charge:job')->cron('0 6 * * *');
            $schedule->command('subscription-recharge:job')->cron('0 9 * * *');
            $schedule->command('subscription-charge-nicepay:job')->cron('0 15 * * *');
            $schedule->command('subscription-recharge-nicepay:job')->cron('0 16 * * *');
            $schedule->command('create-order-inbound:cron')->withoutOverlapping()->cron('*/3 * * * *');
            $schedule->command('update-urbanfox-order:cron')->cron('0 3 * * *');
            $schedule->command('send-referral-invite-after-7-days:cron')->cron('0 17 * * *');
            $schedule->command('cancellation-journey-edm:job')->cron('0 18 * * *');
            $schedule->command('send-follow-up-trial-email:job')->cron('0 19 * * *');
            $schedule->command('tax-invoice-job:cron')->withoutOverlapping()->cron('*/35 * * * *');
            $schedule->command('update-sales-report:job')->withoutOverlapping()->cron('*/30 * * * *');
            $schedule->command('salesreport-summary:job')->withoutOverlapping()->cron('0 21 * * *');
            $schedule->command('subscriber-summary:job')->cron('0 23 * * *');
            $schedule->command('update-aftership-trackings:cron')->withoutOverlapping()->cron('*/40 * * * *');
            $schedule->command('report-queue-generate:job')->withoutOverlapping()->cron('* * * * *');
            // $schedule->command('retrigger-create-order-inbound:job')->cron('0 23 * * *');
            // $schedule->command('retrigger-post-aftership:job')->cron('30 23 * * *');
        }

        if ($env === 'production') {
            $this->commands = [
                // \App\Jobs\ReportQueueGenerate2Job::class,
                \App\Jobs\SendEmail::class,
                \App\Jobs\CreateSalesReportJob::class,
                // \App\Jobs\ReportQueueGenerateJob::class,
                \App\Jobs\SendOpsTeamNotification::class,
                \App\Jobs\CreateAftershipWebhookList::class,
               // \App\Jobs\RetriggerReferralCashoutEmailJob::class,
            ];
            // $schedule->command('report-queue-generate2:job')->withoutOverlapping()->cron('* * * * *');
            $schedule->command('send-email:job')->withoutOverlapping()->cron('* * * * *');
            $schedule->command('create-sales-report:job')->withoutOverlapping()->cron('*/15 * * * *');
        //    $schedule->command('report-queue-generate:job')->withoutOverlapping()->cron('* * * * *');
            $schedule->command('send-ops-team-notification:job')->cron('0 5 * * *');
            $schedule->command('create-aftership-webhooks-list:cron')->withoutOverlapping()->cron('*/5 * * * *');
           // $schedule->command('retrigger-referral-cashout-email:job')->cron('* * * * *');
        }

        if ($env === 'staging') {
            $this->commands = [
                // \App\Jobs\SendEmail::class,
                // \App\Jobs\CreateOrderInboundJob::class,
                // \App\Jobs\UpdateUrbanFoxOrderJob::class,
                // \App\Jobs\SubscriptionChargeJob::class,
                // \App\Jobs\SubscriptionRechargeJob::class,
                // \App\Jobs\SubscriptionChargeNicepayJob::class,
                // \App\Jobs\SubscriptionRechargeNicepayJob::class,
                // \App\Jobs\SendReferralInvite7Days::class,
                // \App\Jobs\CancellationJourneyEdmJob::class,
                // \App\Jobs\SendFollowUpTrialEmailJob::class,
                // \App\Jobs\CreateSalesReportJob::class,
                // \App\Jobs\UpdateSalesReportJob::class,
                // \App\Jobs\TaxInvoiceJob::class,
                // \App\Jobs\SalesReportSummaryTblJob::class,
                // \App\Jobs\SubscriberSummaryTblJob::class,
                // \App\Jobs\ReportQueueGenerateJob::class,
                // \App\Jobs\RetriggerCreateOrderInboundJob::class,
                // \App\Jobs\RetriggerPostAfterShipJob::class,
                // \App\Jobs\UpdateAftershipTracking::class,
                // \App\Jobs\CreateAftershipWebhookList::class,
                // \App\Jobs\SendOpsTeamNotification::class
                // \App\Jobs\RetriggerReferralCashoutEmailJob::class,
            ];

            // $schedule->command('send-email:job')->cron('* * * * *');
            // $schedule->command('create-sales-report:job')->cron('5 * * * *');
            // $schedule->command('subscription-charge:job')->cron('45 2 * * *');
            // $schedule->command('subscription-recharge:job')->cron('45 4 * * *');
            // $schedule->command('subscription-charge-nicepay:job')->cron('45 6 * * *');
            // $schedule->command('subscription-recharge-nicepay:job')->cron('45 6 * * *');
            // $schedule->command('create-order-inbound:cron')->withoutOverlapping()->cron('*/3 * * * *');
            // $schedule->command('update-urbanfox-order:cron')->cron('0 3 * * *');
            // $schedule->command('send-referral-invite-after-7-days:cron')->cron('0 5 * * *');
            // $schedule->command('cancellation-journey-edm:job')->cron('0 7 * * *');
            // $schedule->command('send-follow-up-trial-email:job')->cron('30 7 * * *');
            // $schedule->command('tax-invoice-job:cron')->cron('3 * * * *');
            // $schedule->command('update-sales-report:job')->cron('30 * * * *');
            // $schedule->command('salesreport-summary:job')->cron('* * * * *');
            // $schedule->command('subscriber-summary:job')->cron('* * * * *');
            // $schedule->command('report-queue-generate:job')->cron('* * * * *');
            // $schedule->command('retrigger-create-order-inbound:job')->cron('* * * * *');
            // $schedule->command('retrigger-post-aftership:job')->cron('* * * * *');
            // $schedule->command('update-aftership-trackings:cron')->cron('0 */2 * * *');
            // $schedule->command('create-aftership-webhooks-list:cron')->cron('0 */2 * * *');
            // $schedule->command('send-ops-team-notification:job')->cron('0 5 * * *');
            // $schedule->command('retrigger-referral-cashout-email:job')->cron('* * * * *');
        }

        if ($env === 'local') {
            $this->commands = [
                // \App\Jobs\SendEmail::class,
                // \App\Jobs\CreateOrderInboundJob::class,
                // \App\Jobs\UpdateUrbanFoxOrderJob::class,
                // \App\Jobs\SubscriptionChargeJob::class,
                // \App\Jobs\SubscriptionRechargeJob::class,
                // \App\Jobs\SubscriptionChargeNicepayJob::class,
                // \App\Jobs\SubscriptionRechargeNicepayJob::class,
                // \App\Jobs\SendReferralInvite7Days::class,
                // \App\Jobs\CancellationJourneyEdmJob::class,
                // \App\Jobs\SendFollowUpTrialEmailJob::class,
                // \App\Jobs\CreateSalesReportJob::class,
                // \App\Jobs\UpdateSalesReportJob::class,
                // \App\Jobs\TaxInvoiceJob::class,
                //\App\Jobs\SalesReportSummaryTblJob::class,
                //\App\Jobs\SubscriberSummaryTblJob::class,
                // \App\Jobs\ReportQueueGenerateJob::class,
                // \App\Jobs\RetriggerCreateOrderInboundJob::class,
                // \App\Jobs\RetriggerPostAfterShipJob::class,
                // \App\Jobs\UpdateAftershipTracking::class,
                // \App\Jobs\CreateAftershipWebhookList::class,
                // \App\Jobs\SendOpsTeamNotification::class
                // \App\Jobs\ReportQueueGenerateJob::class,
                // \App\Jobs\RetriggerReferralCashoutEmailJob::class,
            ];
            // $schedule->command('report-queue-generate:job')->withoutOverlapping()->cron('* 21 * * *');
            // $schedule->command('send-email:job')->cron('* * * * *');
            // $schedule->command('create-sales-report:job')->cron('5 * * * *');
            // $schedule->command('subscription-charge:job')->cron('45 2 * * *');
            // $schedule->command('subscription-recharge:job')->cron('45 4 * * *');
            // $schedule->command('subscription-charge-nicepay:job')->cron('45 6 * * *');
            // $schedule->command('subscription-recharge-nicepay:job')->cron('45 6 * * *');
            // $schedule->command('create-order-inbound:cron')->cron('3 * * * *');
            // $schedule->command('update-urbanfox-order:cron')->cron('0 3 * * *');
            // $schedule->command('send-referral-invite-after-7-days:cron')->cron('0 5 * * *');
            // $schedule->command('cancellation-journey-edm:job')->cron('0 7 * * *');
            // $schedule->command('send-follow-up-trial-email:job')->cron('30 7 * * *');
            // $schedule->command('tax-invoice-job:cron')->cron('3 * * * *');
            // $schedule->command('update-sales-report:job')->cron('30 * * * *');
            // $schedule->command('salesreport-summary:job')->cron('* * * * *');
            // $schedule->command('subscriber-summary:job')->cron('* * * * *');
            // $schedule->command('report-queue-generate:job')->cron('* * * * *');
            // $schedule->command('retrigger-create-order-inbound:job')->cron('* * * * *');
            // $schedule->command('retrigger-post-aftership:job')->cron('* * * * *');
            // $schedule->command('update-aftership-trackings:cron')->cron('0 */2 * * *');
            // $schedule->command('create-aftership-webhooks-list:cron')->cron('0 */2 * * *');
            // $schedule->command('send-ops-team-notification:job')->cron('0 5 * * *');
            // $schedule->command('retrigger-referral-cashout-email:job')->cron('* * * * *');
        }
    }

    // Load cronjobs
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
