<?php

namespace App\Listeners;

use App\Events\SendReceiptsEvent;
use App\Helpers\Email\EmailHelper;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderReceiptListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->email = new EmailHelper();
        $this->emailjob = new SendEmail();
        $this->sessionCountry = json_decode(session()->get('currentCountry'), true);
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendReceiptsEvent $event)
    {
        sleep(5);
        $this->generateFakeData($event->order_data);
    }

    public function generateFakeData($data)
    {
        $test = [];
        $test["email"] = 'jason@dci-agency.com';
        $test["name"] = 'Jason Paulraj';
        $test["order"] = $data->order_data;
        return $this->email->buildEmailTemplate('receipt-order', $test);

    }
}
