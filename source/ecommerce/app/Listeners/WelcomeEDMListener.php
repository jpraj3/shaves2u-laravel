<?php

namespace App\Listeners;

use App\Events\CustomerRegisteredEvent;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeEDMListener implements ShouldQueue
{
    private $emailQueueHelper;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CustomerRegisteredEvent $event)
    {
        sleep(5);

        $this->emailQueueHelper->buildEmailTemplate('welcome', $event->data, $event->CountryAndLocaleData);
    }
}
