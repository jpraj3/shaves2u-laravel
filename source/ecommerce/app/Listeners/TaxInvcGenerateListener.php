<?php

namespace App\Listeners;

use App\Events\GenerateTaxInvoice;
use App\Helpers\TaxInvoiceHelper;

class TaxInvcGenerateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->taxInvoiceHelper = new TaxInvoiceHelper();
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GenerateTaxInvoice $event)
    {
    

        $generateHTML = $this->taxInvoiceHelper->__generateHTML($event->order_data);

        if ($generateHTML === 'create-pdf-success') {
            return $this->taxInvoiceHelper->__upload();
        } else {
            return 0;
        }
    }
}
