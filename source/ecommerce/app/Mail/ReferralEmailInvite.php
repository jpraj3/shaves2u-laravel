<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;

class ReferralEmailInvite extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userGender = User::select('gender')->where('email', $this->moduleData['ownemail'])->first();
        $userData = User::where('email', $this->moduleData['ownemail'])->first();
        if (is_array($this->moduleData)) {
            $this->moduleData = json_decode(json_encode($this->moduleData));
        }else{
            $this->moduleData = $this->moduleData;
        }


        $lang= "";
        $this->moduleData->identifiers = (object) array();
        $this->moduleData->identifiers->isMale = true;
        $this->moduleData->identifiers->isFemale = false;
        if($userData->defaultLanguage) {
            $lang = $userData->defaultLanguage;
        }
        else {
            if($this->country['defaultLang']){
                $lang = $this->country['defaultLang'];
            }else if($this->country){
                $lang = $this->country;
            }
        }
        \App::setLocale(strtolower($lang));
         if($userGender){
             switch ($userGender->gender) {
                 case 'female':
                 $this->moduleData->identifiers->isMale = true;
                     break;

                 default:
                 $this->moduleData->identifiers->isMale = true;
                     break;
             }
         }

        $codeIso= "";
        if($this->country['codeIso']){
         $codeIso = $this->country['codeIso'];
        }else if($this->country){
         $codeIso = $this->country;
        }
        $countryg = Countries::select('currencyDisplay')->where('codeIso', $codeIso)->first();
        if($countryg){
            if(strtolower($codeIso) == "kr"){
                $this->moduleData->refprice = $this->moduleData->refCredits.$countryg->currencyDisplay;
                $this->moduleData->tprice = (config('global.all.general_trial_price.' . $codeIso)).$countryg->currencyDisplay;
            }else{
                $this->moduleData->refprice = $countryg->currencyDisplay.$this->moduleData->refCredits;
                $this->moduleData->tprice = $countryg->currencyDisplay.(config('global.all.general_trial_price.' . $codeIso));
            }
        }else{
            if(strtolower($codeIso) == "kr"){
                $this->moduleData->refprice = $this->moduleData->refCredits.$this->moduleData->currencySymbol;
                $this->moduleData->tprice = (config('global.all.general_trial_price.' . $codeIso)).$this->moduleData->currencySymbol;
            }
            else{
                $this->moduleData->refprice = $this->moduleData->currencySymbol.$this->moduleData->refCredits;
                $this->moduleData->tprice = $this->moduleData->currencySymbol.(config('global.all.general_trial_price.' . $codeIso));
            }

        }
        
        if ($lang == 'EN' || $lang == 'en') {
            $subject = str_replace("<name>", $this->moduleData->referrerFName,$this->emailHeaders['subject']);
        }
        else {
            $subject = $this->emailHeaders['subject'];
        }

        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.referral-email-invite.template') // ->view = Template Name
            ->locale($lang) // ->locale = Email Locale
            ->with([
                'moduleData' => $this->moduleData,
                'country' => $this->country,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template
    }


}
