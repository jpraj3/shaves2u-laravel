<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var moduleData
     */
    protected $moduleData;
    /**
     * @var country
     */
    protected $country;
    /**
     * @var emailHeaders
     */
    protected $emailHeaders;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Initiate Email Data Object
        $emailData = (object) $this->moduleData;
      
        $countryid = "";
        if ($this->country['id']) {
            $countryid = $this->country['id'];
        } else if ($this->country) {
            $countryid = $this->country;
        }

        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }
        \App::setLocale(strtolower($lang));
        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
        $subject = $this->emailHeaders['subject'];
        $useremail = $emailData->emailuser;
        $subject = str_replace("<email>", $useremail, $subject);
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.contact-us.template') //->view = Template Name
            ->locale($lang) // ->locale = Email Locale
            ->with([
                'moduleData' => $emailData,
                'country' => $countryid,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template
    }
}
