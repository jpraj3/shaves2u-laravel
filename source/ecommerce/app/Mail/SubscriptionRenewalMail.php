<?php

namespace App\Mail;

// Models

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ecommerce\User;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\Plans;

use Lang;
use Carbon\Carbon;
use App\Helpers\LaravelHelper;

class SubscriptionRenewalMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;

    // Create a new message instance
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = json_decode(json_encode($moduleData));
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    // Build the message.
    public function build()
    {
        // Initialize email contents
        $content = (object)array();
        $content->identifiers = (object)array();
        $content->identifiers->isMale = false;
        $content->identifiers->isFemale = false;
        $content->subscription = Subscriptions::where('id', $this->moduleData->subscriptionId)->first();
        $userData = User::where('id', $this->moduleData->userId)->first();

        $content->planDetails = PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->where('plan_details.PlanId', $content->subscription->PlanId)
            ->where('producttranslates.langCode', $userData->defaultLanguage)
            ->select('plan_details.qty as pqty','producttranslates.name as pname','products.sku as psku')
            ->get();

        $content->plans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
            ->where('plans.id', $content->subscription->PlanId)->first();
        if ($userData) {
            switch ($userData->gender) {
                case 'female':
                    $content->identifiers->isFemale = true;
                    break;
                default:
                    $content->identifiers->isMale = true;
                    break;
            }
            if ($userData->firstName && $userData->lastName) {
                $content->fullname = $userData->firstName . " " . $userData->lastName;
            } else if ($userData->firstName) {
                $content->fullname = $userData->firstName;
            } else if ($userData->lastName) {
                $content->fullname = $userData->lastName;
            }
        }
        \App::setLocale($userData->defaultLanguage);
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
//        ->bcc($content->email->ccList)
        ->subject(Lang::get('email.subject.subscription-renewal')) // ->subject = Email Subject Title
        ->view("email-templates.subscription-renewal.template") // ->view = Template Name
        ->locale($userData->defaultLanguage) // ->locale = Email Locale
        ->with([
            'moduleData' => $content,
            'country' => $this->country,
            'countrycode' => $this->country['codeIso'],
            'lang' => $userData->defaultLanguage
        ]); // ->with = Variables to be passed to email template
    }
}
