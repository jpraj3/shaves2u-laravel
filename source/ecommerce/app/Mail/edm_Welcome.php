<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class edm_Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email-templates.welcome.template2')->subject('[TEST][Shaves2U] Activate Your Account.');
    }


    // public function __construct($user, $transaction, $package, $getdiscount, $invoiceNumber) {
    //     $this->user = $user;
    //     $this->transaction = $transaction;
    //     $this->package = $package;
    //     $this->getdiscount = $getdiscount;
    //     $this->invoiceNumber = $invoiceNumber;
    // }

   
    // public function build() {
    //     if (!empty($this->getdiscount)) {
    //         return $this->markdown('emails.admin.tax_invoice')
    //                         ->subject('Tax Invoice')
    //                         ->with('user', $this->user->toArray())
    //                         ->with('transaction', $this->transaction->toArray())
    //                         ->with('invoiceNumber', $this->invoiceNumber)
    //                         ->with('discount', $this->getdiscount->toArray())
    //                         ->with('package', $this->package->toArray());
    //     } else {
    //         return $this->markdown('emails.admin.tax_invoice')
    //                         ->subject('Tax Invoice')
    //                         ->with('user', $this->user->toArray())
    //                         ->with('transaction', $this->transaction->toArray())
    //                         ->with('invoiceNumber', $this->invoiceNumber)
    //                         ->with('package', $this->package->toArray());
    //     }
    // }
}
