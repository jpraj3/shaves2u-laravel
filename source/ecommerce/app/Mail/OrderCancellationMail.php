<?php

namespace App\Mail;

// Models
use App\Models\Orders\Orders;
use App\Models\Orders\OrderDetails;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Promotions\Promotions;
use App\Models\Products\ProductCountry;
use App\Models\Products\Product;
use App\Models\Products\ProductTranslate;
use App\Models\Products\ProductImage;
use App\Models\User\DeliveryAddresses;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanTranslates;
use App\Models\Plans\PlanImages;
use App\Models\Plans\PlanSKU;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Lang;
use Carbon\Carbon;
use App\Helpers\LaravelHelper;
use App\Services\OrderService;

class OrderCancellationMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;

    // Create a new message instance
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    // Build the message.
    public function build()
    {
        if (is_array($this->moduleData)) {
            $this->moduleData = json_decode(json_encode($this->moduleData));
        } else {
            $this->moduleData = $this->moduleData;
        }

        // Initialize helper
        $this->orderService = new OrderService;

        // Initialize email contents
        $content = (Object) array();
        $content->order = json_decode(Orders::where('id', $this->moduleData->orderId)->first()->toJson());
        $content->orderDetails = json_decode(OrderDetails::where('OrderId', $this->moduleData->orderId)->get()->toJson());

        if($this->moduleData->isPlan == 1) {
            $content->subscription = json_decode(Subscriptions::where('id', $this->moduleData->subscriptionId)->first()->toJson());
        }
        $content->receipt = json_decode(Receipts::where('id', $this->moduleData->receiptId)->first()->toJson());
        $content->user = json_decode(User::where('id', $this->moduleData->userId)->first()->toJson());

        if ($content->user) {
            if ($content->user->firstName && $content->user->lastName) {
                $fullname = $content->user->firstName . " " . $content->user->lastName;
            } else if ($content->user->firstName) {
                $fullname = $content->user->firstName;
            } else if ($content->user->lastName) {
                $fullname = $content->user->lastName;
            }
        }
        $content->fullname = $fullname;

        $content->country = json_decode(Countries::where('id', $this->moduleData->countryId)->first()->toJson());

        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }
        \App::setLocale(strtolower($lang));

        $content->orderNo = \App\Services\OrderService::formatOrderNumberV2($content->order,$content->country,false);
        if($this->moduleData->isPlan == 1) {
            $content->subscription->isOnline = $content->subscription->isOffline === 1 ? 0 : 1;
            $content->subscription->appType = $content->subscription->isOffline === 1 ? 'baWebsite' : 'ecommerce';
        }

        $content->promotion = (Object) array();
        $content->promotion->id = null;
        $content->promotion->promoCode = "";

        $content->referral = (Object) array();
        $content->referral_rewards = 0.00;

        $content->links = (Object) array();   
        $content->links->orderDetailsLink = "";
        $content->links->trackOrderLink = "";

        $content->identifiers = (Object) array();
        $content->identifiers->isOldUser = false;
        $content->identifiers->isActiveUser = false;
        $content->identifiers->isNewUser = false;
        $content->identifiers->isEcommerce = false;
        $content->identifiers->checkTaxInvoice = false;
        $content->identifiers->isBaSale = false;
        $content->identifiers->isBaSubSaleMys = false;
        $content->identifiers->isTaxInvoice = false;
        $content->identifiers->isTrial = false;
        $content->identifiers->NoGstCheck = false;
        $content->identifiers->isOffline = false;

        $content->email = (Object) array();
        $content->email->title = "";
        $content->email->subject =  Lang::get('email.subject.order_cancellation');
        $content->email->ccList = [];
        $content->email->local_carrier = "";
        $content->email->link_mailto_title = "";
        $content->email->link_mailto = "";
        $content->email->template = "";

        // Initialize config data
        $userCountryCodeISO = strtolower(json_decode(Countries::where('id', $content->user->CountryId)->first()->toJson())->codeIso);
        $config_webUrl = config('environment.webUrl').$content->user->defaultLanguage."-".$userCountryCodeISO."/";
        $config_apiToken = '1234567890'; // Old site use like this: jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)


        // Set promotion if any
        if($content->order->PromotionId)
        {
            $content->promotion = json_decode(Promotions::where('id', $content->order->PromotionId)->first()->toJson());
            $content->promotion->promoCode = $content->order->promoCode;
        }

        // Set link to order detail page    
        if($content->user->isActive) 
        {
            $content->links->orderDetailsLink = $config_webUrl.'user/orders/'.$content->order->id.'?utm_source=mandrill&utm_medium=email&utm_campaign=_'.strtolower($content->country->code).'_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web';
            $content->identifiers->isOldUser = true;
        }
        else if(!$content->user->isGuest)
        {
            $content->links->orderDetailsLink = $config_webUrl.'login/'.$config_apiToken.'?nextPage=/user/orders/'.$content->order->id.'&utm_source=mandrill&utm_medium=email&utm_campaign=_'.strtolower($content->country->code).'_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web';
            $content->identifiers->isActiveUser = true;
        } 
        else
        {
            $content->links->orderDetailsLink = $config_webUrl.'users/reset-password?email='.$content->user->email.'&token='.$config_apiToken.'&isActive=true&nextPage=/user/orders/'.$content->order->id.'&utm_source=mandrill&utm_medium=email&utm_campaign=_'.strtolower($content->country->code).'_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web';
            $content->identifiers->isNewUser = true;
        }
        
        // Check if is ecommerce order
        if($content->country->isWebEcommerce)
        {
            $content->identifiers->isEcommerce = true;
        }
        else 
        {
            $content->links->orderDetailsLink = $config_webUrl;
        }

        // Enable text invoice checking for some countries
        $content->identifiers->checkTaxInvoice = in_array($content->country->code,['MYS','SGP']);
        

        // Get order details
        foreach ($content->orderDetails as $detail) {
            $detail->trialPrice = null;
            $detail->NoGstCheck = false;

            if($detail->PlanId)
            {
                $content->identifiers->isTrial = true;
                $detail->trialPrice = $content->receipt->originPrice;
            }

            $detail->taxCode = $content->country->taxCode;
            $detail->taxRate = $content->country->taxRate;
            $detail->taxName = $content->country->taxName;

            if (in_array($content->order->CountryId,[1,2]))
            {
                $detail->NoGstCheck = true;
            }
            $content->identifiers->NoGstCheck = $detail->NoGstCheck;

            $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))),2);
            $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00,2);

            if ($content->country->includedTaxToProduct) {
                $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($detail->price - $detail->priceExcludeTax),2);
            }
        }

        $content->receipt->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
        $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);

        // Cash rebate calculations
        $content->receipt->cashRebate = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
        if($content->promotion->promoCode === "referral")
        {
            $content->receipt->cashRebate = $content->receipt->discountAmount;
            $content->receipt->discountAmount = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
        }

        $content->receipt->priceAfterDiscount = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount), 2);
        $content->receipt->priceAfterDiscountAndRebate = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount - $content->receipt->cashRebate), 2);

        $content->identifiers->isOffline = !!$content->order->SellerUserId;

        if($this->moduleData->isPlan == 1) {
            $content->order->trialShippingDate = $content->subscription->startDeliverDate;
        }

        $content->links->referralEarnXX = $config_webUrl.'user/referrals';
        $content->links->viewOrder = $config_webUrl.'user/orders/'.$content->order->id;
        $content->links->viewProfile = $config_webUrl.'user/settings';

        // Set carrier and mailto settings
        switch ($content->country->code) {
            case 'MYS':
            {
                $content->email->local_carrier = "GDEX";
                $content->email->link_mailto_title = "help.my@shaves2u.com";
                $content->email->link_mailto = "mailto:help.my@shaves2u.com";
            }
            break;
            case 'SGP':
            {
                $content->email->local_carrier = "GDEX";
                $content->email->link_mailto_title = "help.sg@shaves2u.com";
                $content->email->link_mailto = "mailto:help.sg@shaves2u.com";
            }
            break;
            case 'KOR':
            {
                $content->email->local_carrier = "LOTTE";
                $content->email->link_mailto_title = "help.kr@shaves2u.com";
                $content->email->link_mailto = "mailto:help.kr@shaves2u.com";
            }
            break;
            case 'HKG':
            {
                $content->email->local_carrier = "TAQBIN-HK";
                $content->email->link_mailto_title = "help.hk@shaves2u.com";
                $content->email->link_mailto = "mailto:help.hk@shaves2u.com";
            }
            break;
            case 'TWN':
            {
                $content->email->local_carrier = "S.F Express";
                $content->email->link_mailto_title = "help.tw@shaves2u.com";
                $content->email->link_mailto = "mailto:help.tw@shaves2u.com";
            }
            break;
            
            default:
            {
                $content->email->local_carrier = "GDEX";
                $content->email->link_mailto_title = "help.my@shaves2u.com";
                $content->email->link_mailto = "mailto:help.my@shaves2u.com";
            }    
            break;
        }

        // Remove decimal number and add comma separation for Korea prices
        if($content->country->code === "KOR")
        {
            $content->receipt->discountAmount = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->discountAmount),0));
            $content->receipt->subTotalPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->subTotalPrice),0));
            $content->receipt->priceAfterDiscount = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->priceAfterDiscount),0));
            $content->receipt->cashRebate = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->cashRebate),0));
            $content->receipt->originPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice),0));
            $content->receipt->priceAfterDiscountAndRebate = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->priceAfterDiscountAndRebate),0));
            $content->receipt->priceExcludeTax = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->priceExcludeTax),0));
            $content->receipt->taxPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->receipt->taxPrice),0));
            foreach ($content->orderDetails as $detail) 
            {
                $detail->price = number_format(LaravelHelper::ConvertToNDecimalPoints(($detail->price),0));
                $detail->totalPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($detail->totalPrice),0));
            }
            $content->referral->referral_rewards = number_format(LaravelHelper::ConvertToNDecimalPoints(($content->referral->referral_rewards),0));
        }

        // Free product
        if($content->promotion->id) {
            if($content->promotion->freeProductCountryIds) {
                $freeProductList = [];
                foreach (json_decode($content->promotion->freeProductCountryIds) as $freeProductIdStr) {
                    array_push($freeProductList, (int) $freeProductIdStr);
                }

                // Change every order detail price to 0 for matching product country ids
                foreach ($content->orderDetails as $orderDetail) {
                    if(in_array($orderDetail->ProductCountryId,$freeProductList)) {
                        $orderDetail->price = LaravelHelper::ConvertToNDecimalPoints(0,2);
                    }
                }
            }
        }

        // Build additional order details
        foreach ($content->orderDetails as $orderDetail) {
            // If is a product
            if($orderDetail->ProductCountryId) {
                $orderDetail->details = (Object) array();
                $productId = json_decode(ProductCountry::where('id',$orderDetail->ProductCountryId)->first()->toJson())->ProductId;
                $orderDetail->details->translatedName = json_decode(ProductTranslate::where('ProductId', $productId)->where('langCode', $content->country->defaultLang)->first()->toJson())->name;
                $orderDetail->details->sku = json_decode(Product::where('id', $productId)->first()->toJson())->sku;
                $orderDetail->details->imageUrl = json_decode(ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson())->url;
            }

            if($orderDetail->PlanId) {
                $orderDetail->details = (Object) array();
                $planSkuId = json_decode(Plans::where('id', $orderDetail->PlanId)->first()->toJson())->PlanSkuId;
                $orderDetail->details->translatedName = json_decode(PlanTranslates::where('PlanId', $orderDetail->PlanId)->where('langCode', $content->country->defaultLang)->first()->toJson())->name;
                $orderDetail->details->sku = json_decode(PlanSKU::where('id', $planSkuId)->first()->toJson())->sku;
                $orderDetail->details->imageUrl = json_decode(PlanImages::where('PlanId', $orderDetail->PlanId)->where('isDefault', 1)->first()->toJson())->url;
            }
        }      

        // Run Function to filter gender for Current User
        $this->getWomanProductFilter($content);
        // Define email template (OLD FUNCTION)
            $content->email->template = 'email-templates.order-cancellation.template';

        $content->orderdate = Carbon::parse($content->order->created_at)->format('d M Y');
        $content->shippingdate = Carbon::parse($content->order->created_at)->addDay(1)->format('d M Y');

        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
 
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->bcc($content->email->ccList)
            ->subject($content->email->subject) // ->subject = Email Subject Title
            ->view($content->email->template) // ->view = Template Name
            ->locale($lang)
            ->with([
                'moduleData' => $content,
                'country' => $this->country,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template
    }
    public function getWomanProductFilter($content)
    {
        // Default Female Indentifier
        $content->identifiers->isFemale = false;
        $content->identifiers->isMale = true;
    
        if ($content->user) {
            if ($content->user->gender) {
                switch ($content->user->gender) {
                    case 'female':
                        $content->identifiers->isFemale = true;
                        $content->identifiers->isMale = false;
                        break;
    
                    default:
                        $content->identifiers->isFemale = false;
                        $content->identifiers->isMale = true;
                        break;
                }
            }
        }
    
        /** 
         * Filter Using SKUs to test feature. This method is pretty hacky cause the identifiers is based on hardcoded SKU
         * Ideally Product Filtering should be done based on product categories, 
         * but needs to scructured based on how The women products is displayed
         * */
        // $womansProductSkuArr = (object) ["F5"];
        // foreach ($content->orderDetails as $orderDetails) {
        //     foreach ($womansProductSkuArr as $womansProductSku) {
        //         if( $womansProductSku == $orderDetails->details->sku){
        //             $content->identifiers->isFemale = true;
        //         }
        //     }
    
        // }
        return $content;
    }
}