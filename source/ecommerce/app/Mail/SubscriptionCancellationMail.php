<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ecommerce\User;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionCancellationMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var moduleData
     */
    protected $moduleData;
    /**
     * @var country
     */
    protected $country;
    /**
     * @var emailHeaders
     */
    protected $emailHeaders;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {

            // Initiate Email Data Object
            $emailData = (object)$this->moduleData;
            $emailData->identifiers = (object)array();
            $emailData->identifiers->isMale = false;
            $emailData->identifiers->isFemale = false;
            $emailData->referral = (object)array();
            $fullname = "";
            // Get User Gender From User Tables (Should execute after user is created)
            $userGender = User::select('gender', 'firstName', 'lastName')->where('email', $this->moduleData['email'])->first();
            $userData = User::where('email', $this->moduleData['email'])->first();
            // \Log::channel('cronjob')->info('=== $this->country ===');
            // \Log::channel('cronjob')->info(json_encode($this->country['id']));
            // dd($this->country['id']);

            // Setting Template Identifiers based on User Gender
            if ($userGender) {
                switch ($userGender->gender) {

                    case 'female':
                        $emailData->identifiers->isFemale = true;
                        break;

                    default:
                        $emailData->identifiers->isMale = true;
                        break;
                }

                if ($userGender->firstName && $userGender->lastName) {
                    $fullname = $userGender->firstName . " " . $userGender->lastName;
                } else if ($userGender->firstName) {
                    $fullname = $userGender->firstName;
                } else if ($userGender->lastName) {
                    $fullname = $userGender->lastName;
                }
            }
            $emailData->fullname = $fullname;

            $countryid = "";
            if ($this->country['id']) {
                $countryid = $this->country['id'];
            } else if ($this->country) {
                $countryid = $this->country;
            }

            $lang = "";
            if ($userData->defaultLanguage) {
                $lang = $userData->defaultLanguage;
            } else {
                if ($this->country['defaultLang']) {
                    $lang = $this->country['defaultLang'];
                } else if ($this->country) {
                    $lang = $this->country;
                }
            }

            $codeIso = "";
            if ($this->country['codeIso']) {
                $codeIso = $this->country['codeIso'];
            } else if ($this->country) {
                $codeIso = $this->country;
            }
            \App::setLocale(strtolower($lang));
            // Options set up here will be passed to queue to be processed
            return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($this->emailHeaders['subject']) // ->subject = Email Subject Title
            ->view('email-templates.subscription-cancellation.template') //->view = Template Name
            ->locale($lang) 
            ->with([
                'moduleData' => $emailData,
                'country' => $countryid,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template

        } catch (\Throwable $th) {
            // dd("Exception has occured: ". $th);
        }
    }
}
