<?php

namespace App\Mail;

// Models
use App\Models\Orders\Orders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Plans\PlanTranslates;
use App\Models\Plans\PlanSKU;
use App\Models\Cards\Cards;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Lang;
use Carbon\Carbon;
use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Services\OrderService;
use App\Services\CountryService as CountryService;

class SubscriptionModifiedMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;

    // Create a new message instance
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;

    }

    // Build the message.
    public function build()
    {
        // Initialize helpers
        $this->orderService = new OrderService;
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->country_service = new CountryService();
        if (is_array($this->moduleData)) {
            $this->moduleData = json_decode(json_encode($this->moduleData));
        } else {
            $this->moduleData = $this->moduleData;
        }

        // Initialize email contents
        $content = (object) array();
        if( $this->moduleData->currentdate){
            $content->currentdate = $this->moduleData->currentdate;
        }else{
            $content->currentdate = '';
        }

        $subscriptiong = Subscriptions::where('id', $this->moduleData->subscriptionId)->first();
        if ($subscriptiong) {
            $content->subscription = json_decode($subscriptiong->toJson());
        } else {
            $content->subscription = $subscriptiong;
        }
        $userg = User::where('id', $this->moduleData->userId)->first();
        if ($userg) {
            $content->user = json_decode($userg->toJson());
        } else {
            $content->user = $userg;
        }
        $countryg = Countries::where('id', $this->moduleData->countryId)->first();
        if ($countryg) {
            $content->country = json_decode($countryg->toJson());
        } else {
            $content->country = $countryg;
        }
        $content->currencyDisplay = $countryg->currencyDisplay;
        $content->identifiers = (object) array();
        $content->identifiers->isMale = false;
        $content->identifiers->isFemale = false;

        $content->oriprice = $subscriptiong->pricePerCharge;
        $content->tax = (object) array($this->laravelHelper->ConvertArraytoObject($this->country_service->tax($this->moduleData->countryId)))["0"];
        $shippingfee =  $this->country_service->shippingFee($this->moduleData->countryId, $content->user->defaultLanguage);
        if ($shippingfee) {
            $content->shipping = (float) $shippingfee["shippingfee"];
        }
        $totalprice = $content->oriprice +  $content->shipping;
        $taxAmount = $content->tax->taxRate / 100 * ($totalprice);
        $totalprice = $totalprice + $taxAmount;
        $content->totalprice = $totalprice;

        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }
        \App::setLocale(strtolower($lang));
        
        if ($userg) {
            switch ($userg->gender) {

                case 'female':
                    $content->identifiers->isFemale = true;
                    break;

                default:
                    $content->identifiers->isMale = true;
                    break;
            }
            if ($userg->firstName && $userg->lastName) {
                $fullname = $userg->firstName . " " . $userg->lastName;
            } else if ($userg->firstName) {
                $fullname = $userg->firstName;
            } else if ($userg->lastName) {
                $fullname = $userg->lastName;
            }
        }
        $content->fullname = $fullname;


        $plandata = (object) array();
        $plandata->id =  $subscriptiong->PlanId;
        $plandata->lang = $content->user->defaultLanguage;

        $subdata = (object) array();
        $subdata->id =  $subscriptiong->id;
        $subdata->lang = $content->user->defaultLanguage;

        $planget = $this->planHelper->getPlanDuration($plandata);

        if ($planget) {
            $content->duration = $planget->duration;
        } else {
            $content->duration = '';
        }
        $planproduct = $this->productHelper->getProductWithoutImageByPlanID($plandata);
        $planaddon = $this->productHelper->getProductWithoutImageByAddOn($subdata);
        $countpp = 0;
        $productnamet = "";
        foreach ($planproduct as $pd) {
            if ($pd->productname) {
                if ($countpp == 0) {
                    $productnamet = $productnamet . $pd->productname;
                } else {
                    $productnamet = $productnamet . "," . $pd->productname;
                }
            }
            $countpp++;
        }
        foreach ($planaddon as $pa) {
            if ($pa->productname) {
                if ($countpp == 0) {
                    $productnamet = $productnamet . $pa->productname;
                } else {
                    $productnamet = $productnamet . "," . $pa->productname;
                }
            }
            $countpp++;
        }
        $content->productnamet =  $productnamet;
        $content->links = (object) array();
        $content->links->userSettingsUrl = "";

        $content->email = (object) array();
        $content->email->title = "";
        $content->email->subject = Lang::get('email.subject.subscription-modified');
        $content->email->ccList = [];
        $content->email->local_carrier = "";
        $content->email->link_mailto_title = "";
        $content->email->link_mailto = "";
        $content->email->template = "email-templates.subscription-modified.template";

        // Initialize config data
        $userCountryCodeISO = strtolower($content->country->codeIso);
        $config_webUrl = config('environment.webUrl') . $content->user->defaultLanguage . "-" . $userCountryCodeISO . "/";


        // Get next payment date (format might be different)
        $content->subscription->nextPaymentDate = $content->subscription->nextChargeDate;
        $content->subscription->nextDeliverDate = $content->subscription->nextDeliverDate;
        // Get billing cycle
        $content->subscription->billingCycle = "";


        // Set user settings Url
        $content->links->userSettingsUrl = $config_webUrl . "user/settings?utm_source=mandrill&utm_medium=email&utm_campaign=_" . strtolower($content->country->code) . "_paymentfail&utm_content=S2U_html_paymentfail&utm_term=_paymentfail_web";

        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->bcc($content->email->ccList)
            ->subject($content->email->subject) // ->subject = Email Subject Title
            ->view($content->email->template) // ->view = Template Name
            ->locale($lang) // ->locale = Email Locale
            ->with([
                'moduleData' => $content,
                'country' => $this->country,
                'countrycode' => $userCountryCodeISO,
                'lang' => $content->user->defaultLanguage
            ]); // ->with = Variables to be passed to email template
    }
}
