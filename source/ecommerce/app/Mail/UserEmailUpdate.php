<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserEmailUpdate extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var moduleData
     */
    protected $moduleData;
    /**
     * @var country
     */
    protected $country;
    /**
     * @var emailHeaders
     */
    protected $emailHeaders;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Initiate Email Data Object
        $emailData = (object) $this->moduleData;
      
        $countryid = "";
        if ($this->country['id']) {
            $countryid = $this->country['id'];
        } else if ($this->country) {
            $countryid = $this->country;
        }

        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }

        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
        $subject = $this->emailHeaders['subject'];
        $oldEmail = $emailData->oldEmail;
        $newEmail = $emailData->newEmail;
        $subject = str_replace("<email_old>", $oldEmail, $subject);
        $subject = str_replace("<email_new>", $newEmail, $subject);
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.user-email-update.template') //->view = Template Name
            ->locale('EN') // ->locale = Email Locale
            ->with([
                'moduleData' => $emailData,
                'country' => 1,
                'countrycode' => 'MY',
                'lang' => 'EN'
            ]); // ->with = Variables to be passed to email template
    }
}
