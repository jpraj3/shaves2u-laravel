<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ecommerce\User;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fullname = "";
        $userGender = User::select('gender', 'firstName', 'lastName')->where('email', $this->moduleData['email'])->first();
        if (is_array($this->moduleData)) {
            $this->moduleData = json_decode(json_encode($this->moduleData));
        } else {
            $this->moduleData = $this->moduleData;
        }

        $this->moduleData->identifiers = (object) array();
        $this->moduleData->identifiers->isMale = false;
        $this->moduleData->identifiers->isFemale = false;

        if ($userGender) {
            switch ($userGender->gender) {
                case 'male':
                    $this->moduleData->identifiers->isMale = true;
                    break;

                case 'female':
                    $this->moduleData->identifiers->isFemale = true;
                    break;

                default:
                    $this->moduleData->identifiers->isMale = true;
                    break;
            }
            if ($userGender->firstName && $userGender->lastName) {
                $fullname = $userGender->firstName . " " . $userGender->lastName;
            } else if ($userGender->firstName) {
                $fullname = $userGender->firstName;
            } else if ($userGender->lastName) {
                $fullname = $userGender->lastName;
            }
        }
        $this->moduleData->fullname = $fullname;
        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }
        \App::setLocale(strtolower($lang));
        $subject = $this->emailHeaders['subject'];
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.password-reset.template') // ->view = Template Name
            ->locale($lang) // ->locale = Email Locale
            ->with([
                'moduleData' => $this->moduleData,
                'country' => $this->country,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template
    }
}
