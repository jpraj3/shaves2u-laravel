<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ecommerce\User;
use App\Models\Rebates\RewardsCurrency;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReferralInactiveCreditsMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var moduleData
     */
    protected $moduleData;
    /**
     * @var country
     */
    protected $country;
    /**
     * @var emailHeaders
     */
    protected $emailHeaders;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Initiate Email Data Object
        $emailData = (object) $this->moduleData;
        $emailData->identifiers = (object) array();
        $emailData->identifiers->isMale = false;
        $emailData->identifiers->isFemale = false;
        $emailData->referral = (object) array();
        $fullname="";
        $reffullname="";
        // Get User Gender From User Tables (Should execute after user is created)
        $userGender = User::select('gender','firstName','lastName')->where('email', $this->moduleData['email'])->first();
        $refuser = User::select('firstName','lastName')->where('id', $this->moduleData['refuserId'])->first();
        $refUserData = User::where('id', $this->moduleData['refuserId'])->first();
        // \Log::channel('cronjob')->info('=== $this->country ===');
        // \Log::channel('cronjob')->info(json_encode($this->country['id']));
        // dd($this->country['id']);

        // Setting Template Identifiers based on User Gender
        if($userGender){
        switch ($userGender->gender) {
            case 'male':
                $emailData->identifiers->isMale = true;
                break;

            case 'female':
                $emailData->identifiers->isFemale = true;
                break;

            default:
                $emailData->identifiers->isMale = true;
                break;
        }

            if( $userGender->firstName && $userGender->lastName){
                $fullname = $userGender->firstName . " " . $userGender->lastName;
            }
            else if($userGender->firstName){
                $fullname = $userGender->firstName;
            }
            else if($userGender->lastName){
                $fullname = $userGender->lastName;
            }
       }
       $emailData->fullname = $fullname;

       if($refuser){
            if( $refuser->firstName && $refuser->lastName){
                $reffullname = $refuser->firstName . " " . $refuser->lastName;
            }
            else if($refuser->firstName){
                $reffullname = $refuser->firstName;
            }
            else if($refuser->lastName){
                $reffullname = $refuser->lastName;
            }
       }
       $emailData->reffullname = $reffullname;

       $countryid= "";
       if($this->country['id']){
        $countryid = $this->country['id'];
       }else if($this->country){
        $countryid = $this->country;
       }

       $lang= "";
       if($refUserData->defaultLanguage) {
           $lang = $refUserData->defaultLanguage;
       }
       else {
           if($this->country['defaultLang']){
               $lang = $this->country['defaultLang'];
           }else if($this->country){
               $lang = $this->country;
           }
       }
       \App::setLocale(strtolower($lang));
       $codeIso= "";
       if($this->country['codeIso']){
        $codeIso = $this->country['codeIso'];
       }else if($this->country){
        $codeIso = $this->country;
       }

        // Getting Referral Rewards Based on Country id
        $referralInfo = RewardsCurrency::join('countries', 'countries.id', 'rewardscurrencies.CountryId')->select('countries.currencyDisplay', 'rewardscurrencies.value')->where('countries.id', $countryid)->first(); //json_decode()->gender;
        if($referralInfo){
            json_decode($referralInfo->toJson());
        }
        $emailData->referral = $referralInfo;
        $subject = $this->emailHeaders['subject'];
 
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.referral-inactive-credits.template') //->view = Template Name
            ->locale($lang) // ->locale = Email Locale
            ->with([
                'moduleData' => $emailData,
                'country' => $countryid,
                'countrycode' => $codeIso,
                'lang' => $lang
            ]); // ->with = Variables to be passed to email template
    }
}
