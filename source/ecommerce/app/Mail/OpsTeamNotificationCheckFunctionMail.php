<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class OpsTeamNotificationCheckFunctionMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var moduleData
     */
    protected $moduleData;
    /**
     * @var country
     */
    protected $country;
    /**
     * @var emailHeaders
     */
    protected $emailHeaders;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Initiate Email Data Object
        $emailData = (object) $this->moduleData;
        $subject = $this->emailHeaders['subject'];
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject($subject) // ->subject = Email Subject Title
            ->view('email-templates.ops-team-notification-check-function.template') //->view = Template Name
            ->locale('EN') // ->locale = Email Locale
            ->with([
                'moduleData' => $emailData,
                'country' => 1,
                'countrycode' => 'MY',
                'lang' => 'EN'
            ]); // ->with = Variables to be passed to email template
    }
}
