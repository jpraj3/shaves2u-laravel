<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReferralSendInviteAfter7Days extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = json_decode(json_encode($moduleData));
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->subject('TEST REFERRAL INVITE AFTER 7 Days') // ->subject = Email Subject Title
            ->view('email-templates.referral-subscription-invite.template') // ->view = Template Name
            ->with([
                'moduleData' => $this->moduleData,
                'country' => $this->country,
            ]); // ->with = Variables to be passed to email template
    }
}
