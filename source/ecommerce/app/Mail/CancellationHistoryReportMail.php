<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\Ecommerce\User;
use App\Helpers\SubscriptionHelper;
use App\Models\CancellationJourney\CancellationJourneys;
use Carbon\Carbon;

class CancellationHistoryReportMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = $moduleData;
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subscriptionHelper = new SubscriptionHelper();

        try {
            
            $cancellationJourneys = CancellationJourneys::where('SubscriptionId', $this->moduleData["subscriptionId"])
                ->orderByRaw("id asc")
                ->get();
            $cancelledJourney = CancellationJourneys::where('SubscriptionId', $this->moduleData["subscriptionId"])->where('hasCancelled', 1)->first();

            $userData = User::where('id',$this->moduleData["userId"])->first();
            $this->moduleData["customerName"] = $userData["firstName"];
            $this->moduleData["mobileNumber"] = $userData["phone"];
            $this->moduleData["subscriptionPlan"] = $cancelledJourney["subscriptionName"];

            $this->moduleData["cancellationJourneys"] = array();

            $i = 1;
            foreach ($cancellationJourneys as $journey) {

                $attemptNumber = $i;
                if($attemptNumber == 1) {
                    $attemptNumber .= "st";
                } else if($attemptNumber == 2) {
                    $attemptNumber .= "nd";
                } else if($attemptNumber == 3) {
                    $attemptNumber .= "rd";
                } else {
                    $attemptNumber .= "th";
                }

                $dateTime = Carbon::parse($journey["created_at"])->toDateTimeString();
                
                $cancellationReason = "";
                $userAction = "";
                if ($journey["hasCancelled"] === 1) {
                    $userAction = "Cancelled subscription.";
                    $cancellationReason = $journey["OtherReason"];
                }
                else {
                    if ($journey["hasChangedBlade"] === 1) {
                        $userAction .= ("Changed cartridge type to " . $journey["modifiedBlade"] . ". ");
                    }
                    if ($journey["hasChangedFrequency"] === 1) {
                        $userAction .= ("Changed usage plan to " . $journey["modifiedFrequency"] . ". ");
                    }
                    if ($journey["hasGetFreeProduct"] === 1) {
                        $userAction .= ("Applied for blade replacement.");
                    }
                    if ($journey["hasGetDiscount"] === 1) {
                        $userAction .= ("Applied cancellation discount for " . $journey["discountPercent"] . "%. ");
                    }
                    if ($journey["hasPausedSubscription"] === 1) {
                        $userAction .= ("Paused subscription. ");
                    }
                }

                $info = array();
                $info["attemptNumber"] = $attemptNumber;
                $info["dateTime"] = $dateTime;
                $info["userAction"] = $userAction;
                $info["cancellationReason"] = $cancellationReason;
                array_push($this->moduleData["cancellationJourneys"], $info);
                $i++;
            }

            $codeIso = "";
            if ($this->country['codeIso']) {
                $codeIso = $this->country['codeIso'];
            } else if ($this->country) {
                $codeIso = $this->country;
            }
            $lang = "";
            if ($this->country['defaultLang']) {
                $lang = $this->country['defaultLang'];
            } else if ($this->country) {
                $lang = $this->country;
            }
            \App::setLocale(strtolower($lang));
            // Options set up here will be passed to queue to be processed
            return $this->to(config("global.all.cancellation.cancellation-report-recipient")) // ->to = Email Recipient
                ->subject($this->emailHeaders['subject']) // ->subject = Email Subject Title
                ->view('email-templates.cancellation-history-report.template') // ->view = Template Name
                ->locale($lang) 
                ->with([
                    'moduleData' => $this->moduleData,
                    'country' => $this->country,
                    'countrycode' => $codeIso,
                    'lang' => $lang
                ]); // ->with = Variables to be passed to email template

        } catch (\Throwable $th) {
            // dd($th);
        }
    }
}
