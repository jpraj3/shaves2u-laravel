<?php

namespace App\Mail;

// Models
use App\Helpers\ProductHelper;
use App\Models\Orders\Orders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Plans\PlanTranslates;
use App\Models\Plans\PlanSKU;
use App\Models\Plans\Plans;
use App\Models\Cards\Cards;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Lang;
use Carbon\Carbon;
use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Services\OrderService;

class SubcriptionChargeFailureMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $moduleData;
    protected $country;
    protected $emailHeaders;

    // Create a new message instance
    public function __construct($moduleData, $country, $emailHeaders)
    {
        $this->moduleData = json_decode(json_encode($moduleData));
        $this->country = $country;
        $this->emailHeaders = $emailHeaders;
    }

    // Build the message.
    public function build()
    {
        // Initialize helpers
        $this->orderService = new OrderService;
        $this->planHelper = new PlanHelper();

        // Initialize email contents
        $content = (object)array();
        if (isset($this->moduleData->subscriptionId)) {
            $subscriptiong = Subscriptions::where('id', $this->moduleData->subscriptionId)->first();
            if ($subscriptiong) {
                $content->subscription = json_decode($subscriptiong->toJson());
            } else {
                $content->subscription = $subscriptiong;
            }
        }
        $userg = User::where('id', $this->moduleData->userId)->first();
        if ($userg) {
            $content->user = json_decode($userg->toJson());
        } else {
            $content->user = $userg;
        }
        $countryg = Countries::where('id', $this->moduleData->countryId)->first();
        if ($countryg) {
            $content->country = json_decode($countryg->toJson());
        } else {
            $content->country = $countryg;
        }
        $content->identifiers = (object)array();
        $content->identifiers->isMale = false;
        $content->identifiers->isFemale = false;

        if ($userg) {
            switch ($userg->gender) {

                case 'female':
                    $content->identifiers->isFemale = true;
                    break;

                default:
                    $content->identifiers->isMale = true;
                    break;
            }
            if ($userg->firstName && $userg->lastName) {
                $fullname = $userg->firstName . " " . $userg->lastName;
            } else if ($userg->firstName) {
                $fullname = $userg->firstName;
            } else if ($userg->lastName) {
                $fullname = $userg->lastName;
            }
        }
        $content->fullname = $fullname;

        $content->links = (object)array();
        $content->links->userSettingsUrl = "";
        $lang = "";
        if ($this->country['defaultLang']) {
            $lang = $this->country['defaultLang'];
        } else if ($this->country) {
            $lang = $this->country;
        }
        \App::setLocale($content->user->defaultLanguage);
        $content->email = (Object)array();
        $content->email->title = "";
        $content->email->subject = Lang::get('email.subject.receipt-payment-failure');
        $content->email->ccList = [];
        $content->email->local_carrier = "";
        $content->email->link_mailto_title = "";
        $content->email->link_mailto = "";

        // Initialize config data
        $userCountryCodeISO = strtolower($content->country->codeIso);
        $config_webUrl = config('environment.webUrl') . $content->user->defaultLanguage . "-" . $userCountryCodeISO . "/";

        // Get translated plan name
        if (isset($content->subscription)) {
            $plant = PlanTranslates::select('translates')->where('PlanId', $content->subscription->PlanId)->where('langCode', $userg->defaultLanguage)->first();
            if ($plant) {
                $content->subscription->translatedPlanName = json_decode($plant->toJson())->translates;
            } else {
                $this->productHelper = new ProductHelper();
                $plandata = (object)array();
                $plandata->id = $content->subscription->PlanId;
                $plandata->lang = $userg->defaultLanguage;

                $plantypeget = Lang::get('email.content.common.custom-plan');
                if ($content->subscription->isTrial == true) {
                    $plantypeget = Lang::get('email.content.common.trial-plan');
                }

                $planproduct = $this->productHelper->getProductWithoutImageByPlanID($plandata);
                $countpp = 0;
                $productnamet = "";
                foreach ($planproduct as $pd) {
                    if ($pd->productname) {
                        if ($countpp == 0) {
                            $productnamet = $productnamet . $pd->productname;
                        } else {
                            $productnamet = $productnamet . ", " . $pd->productname;
                        }
                    }
                    $countpp++;
                }

                $content->subscription->translatedPlanName = $plantypeget . ' - ' . $productnamet;
            }
            $content->translatedProductName = $content->subscription->translatedPlanName;
        } // If no Subscription
        else {
            // If orderId is passed from previous function
            if (isset($this->moduleData->orderId)) {
                // Get Translated Product Name
                $productTranslate = Orders::join('orderdetails', 'orderdetails.OrderId', 'orders.id')
                    ->join('productcountries', 'productcountries.id', 'orderdetails.ProductCountryId')
                    ->join('products', 'products.id', 'productcountries.ProductId')
                    ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
                    ->select('producttranslates.name')
                    ->where(['orders.id' => $this->moduleData->orderId, 'producttranslates.langCode' => $userg->defaultLanguage])
                    ->first();

                $content->translatedProductName = $productTranslate->name;
            }

        }

        // Get order prefix
        // Get transaction id
        if (isset($content->subscription)) {
            if ($content->subscription->currentOrderId) {
                $order = Orders::where('id', $content->subscription->currentOrderId)->first();
                $orderPrefix = $this->orderService->formatOrderNumber($order, $content->country, false);
                $content->subscription->transactionId = $orderPrefix . $content->subscription->currentOrderId;
            } else {
                $content->subscription->transactionId = "";
            }

            // Get next payment date (format might be different)
            $content->subscription->nextPaymentDate = $content->subscription->nextChargeDate;

            // Get billing cycle
            $content->subscription->billingCycle = "";

            // Get plan sku
            $sku = Plans::leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->where('plans.id', $content->subscription->PlanId)
            ->select('plansku.duration')
            ->first();

            if ($content->subscription->PlanId) {
                // Get duration from sku
                $content->subscription->billingCycle = Lang::get('email.content.body.receipt-payment-failure.planType.normal', ['duration' => $sku->duration]);
            } else {
                $content->subscription->billingCycle = Lang::get('email.content.body.receipt-payment-failure.planType.annual');
            }

            // Get card last 4 digit
            $content->subscription->cardNumber = json_decode(Cards::where('id', $content->subscription->CardId)->first()->toJson())->cardNumber;
        }

        // Set user settings Url
        $content->links->userSettingsUrl = $config_webUrl . "user/settings?utm_source=mandrill&utm_medium=email&utm_campaign=_" . strtolower($content->country->code) . "_paymentfail&utm_content=S2U_html_paymentfail&utm_term=_paymentfail_web";

        $codeIso = "";
        if ($this->country['codeIso']) {
            $codeIso = $this->country['codeIso'];
        } else if ($this->country) {
            $codeIso = $this->country;
        }
   
        if (($this->moduleData->isPlan == 0) || is_null($content->subscription->nextChargeDate)) {
            $content->email->template = "email-templates.receipt-payment-failure-product.template";
        } else {
            $content->email->template = "email-templates.receipt-payment-failure-subscription.template";
        }

        // Options set up here will be passed to queue to be processed
        return $this->to(($this->emailHeaders)['mailto']) // ->to = Email Recipient
            ->bcc($content->email->ccList)
            ->subject($content->email->subject) // ->subject = Email Subject Title
            ->view($content->email->template) // ->view = Template Name
            ->locale(strtolower($content->user->defaultLanguage))
            ->with([
                'moduleData' => $content,
                'country' => $this->country,
                'countrycode' => $userCountryCodeISO,
                'lang' => strtolower($content->user->defaultLanguage)
            ]); // ->with = Variables to be passed to email template
    }
}
