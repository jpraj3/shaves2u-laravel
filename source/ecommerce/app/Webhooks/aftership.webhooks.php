<?php

namespace App\Webhooks;

use App\Helpers\PlanHelper;
use App\Helpers\UserHelper;
use App\Helpers\Warehouse\AftershipHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\DeliveryTracking;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Queue\AftershipWebhookQueueService;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;

class AftershipWebhooks
{

    public function __construct()
    {
        $this->Log = \Log::channel('cronjob');
        $this->userHelper = new UserHelper();
        $this->orderService = new OrderService();
        $this->_aftership = new AftershipHelper;
        $this->planHelper = new PlanHelper();
        if ($this->_aftership->init) {
            $this->_aftershipConfig = $this->_aftership->init;
        }
    }

    public function handle(Request $request)
    {
        $this->Log = \Log::channel('cronjob');
        // $this->Log->info('Webhook | Aftership | response => ' . $request);
        // $this->Log->info('Webhook | Aftership | event => ' . $request->event);
        // $this->Log->info('Webhook | Aftership | msg => ' . json_encode($request->msg));
        $this->postUpdateOrder($request);
    }

    public function postAPI()
    {
        $this->aftership->post('/aftership', $this->postUpdateOrder);
    }

    public function test()
    {
        $order = Orders::where('id', 1)->first();
        if ($order) {
            $country = Countries::where('id', $order->CountryId)->first();
            $user = User::where('id', $order->UserId)->first();

            $shipment = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
            $options = [
                'order' => $order,
                // 'country' => $country,
                'shipment' => $shipment,
                'user' => $user,
            ];
            $ccode = "";
            if(isset($country)){
                if($country->codeIso){
                $ccode = $country->codeIso;
                }
            }
            $this->_aftership->postTracking($options,$ccode, false);
        }
    }

    public function test2()
    {
        $type = 'Delivered';
        $limit = 100;
        $pagination = 1;
        $test = $this->_aftership->getTrackings($type, $limit, $pagination);

        // dd($test);
    }

    public function postUpdateOrder($request)
    {
        $this->Log = \Log::channel('cronjob');
        // $this->Log->info('Aftership | postUpdateOrder | response => ' . json_encode($request));
        $params = $request;
        $key = $request->get('secretKey');
        try {
            // check event name
            // $this->Log->info('Aftership | postUpdateOrder | webhooks key => ' . json_encode($key) . ', aftership key =>' . json_encode($this->_aftershipConfig->secretKey));
            // $this->Log->info('Aftership | postUpdateOrder | webhooks event => ' . json_encode($params->event) . ', webhooks tracking number => ' . json_encode($params->msg["tracking_number"]));

            // $key === $this->_aftershipConfig->secretKey ? $this->Log->info('Aftership | postUpdateOrder | key match true') : null;
            // $params->event === 'tracking_update' ? $this->Log->info('Aftership | postUpdateOrder | update type === tracking_update') : null;
            // isset($params->msg) && isset($params->msg["tag"]) && $params->msg["tag"] === 'Delivered' ? $this->Log->info('Aftership | postUpdateOrder | status is Delivered') : null;
            // isset($params->msg["tracking_number"]) && $params->msg["tracking_number"] !== "" ? $this->Log->info('Aftership | postUpdateOrder | tracking_number exists') : null;

            if (
                $key === $this->_aftershipConfig->secretKey &&
                $params->event === 'tracking_update' &&
                isset($params->msg) &&
                isset($params->msg["tag"]) &&
                $params->msg["tag"] === 'Delivered'
                && isset($params->msg["tracking_number"]) &&
                $params->msg["tracking_number"] !== ""
            ) {

                $options = [
                    'type' => $this->orderService->checkOrderNumberType($params->msg["order_id"]),
                    'deliveryId' => $params->msg["tracking_number"],
                    'api_data' => $params->msg,
                ];
                AftershipWebhookQueueService::addHash($params->msg["tracking_number"], $options);

                // find order fit with this hook
                // check order type
                // $this->Log->info('Aftership | postUpdateOrder | update type => ' . $this->orderService->checkOrderNumberType($params->msg["order_id"]));
                // if ($this->orderService->checkOrderNumberType($params->msg["order_id"]) === 'order') {

                //     $this->Log->info('Aftership | postUpdateOrder | start update orders');
                //     try {
                //         $orderFilter = Orders::where('deliveryId', $params->msg["tracking_number"])->where('status', 'Delivering')->first();
                //         // $matchThese = ['orders.deliveryId' => $orderFilter->deliveryId, 'users.id' => $orderFilter->UserId, 'receipts.OrderId' => $orderFilter->id, 'countries.id' => $orderFilter->CountryId];
                //         if ($orderFilter) {

                //             // save api response to database
                //             DeliveryTracking::firstOrCreate([
                //                 'OrderId' => $orderFilter->id,
                //             ], [
                //                 'warehouse_id' => $params->msg["order_id"],
                //                 'status' => 'Pending',
                //                 'deliveryId' => $params->msg["tracking_number"],
                //                 'api_data' => json_encode($params->msg),
                //             ]);
                //         }
                //     } catch (Exception $ex) {
                //         $this->Log->error('Aftership | postUpdateOrder | error => ' . $ex->getMessage());
                //     }
                // } else {
                //     $this->Log->info('Aftership | postUpdateOrder | check for bulkorder');
                //     //bulk order
                //     try {
                //         $_order = BulkOrders::where('deliveryId', $params->msg["tracking_number"])->where('status', 'Delivering')->first();
                //         if ($_order) {

                //             // save api response to database
                //             DeliveryTracking::firstOrCreate([
                //                 'BulkOrderId' => $_order->id,
                //             ], [
                //                 'warehouse_id' => $params->msg["order_id"],
                //                 'status' => 'Pending',
                //                 'deliveryId' => $params->msg["tracking_number"],
                //                 'api_data' => json_encode($params->msg),
                //             ]);
                //         }
                //     } catch (Exception $ex) {
                //         $this->Log->error('Aftership | postUpdateBulkOrder | error => ' . $ex->getMessage());
                //     }
                // }
            }
        } catch (Exception $ex) {
            $this->Log->error('Aftership | postUpdateOrder | error => ' . $ex->getMessage());
        }
    }
}

// sample query for request
// [2020-01-03 14:38:48] production.INFO: Webhook | Aftership | response => POST /webhooks/aftership?secretKey=8j9ZbkWrKQaJWA5n HTTP/1.0
// Accept:                application/json
// Aftership-Hmac-Sha256: V4Fz16dEgpAIiKNUpujn4dkHa8azkxC11vRyo7V2hFQ=
// Connection:            close
// Content-Length:        4194
// Content-Type:          application/json
// Host:                  shaves2u.com
// X-Forwarded-For:       52.5.72.61
// X-Forwarded-Proto:     https
// X-Real-Ip:             52.5.72.61

// {
//      "event_id":"68cad621-ea5a-4c86-806f-b6820f28687e",
//      "event":"tracking_update",
//      "is_tracking_first_tag":true,
//      "msg":{"id":"5e0ca7cf4ef4083010ca9e25","tracking_number":"ML3146863","title":"ML3146863","note":null,"origin_country_iso3":"SGP","destination_country_iso3":"SGP","courier_destination_country_iso3":"SGP","shipment_package_count":1,"active":false,"order_id":"SGB00181348","order_id_path":"https://shaves2u.aftership.com/SGB00181348","customer_name":"K","source":"api","emails":["antujoseph@hotmail.com"],"smses":["+6592300394"],"subscribed_smses":[],"subscribed_emails":[],"android":[],"ios":[],"return_to_sender":false,"custom_fields":{},"tag":"Delivered","subtag":"Delivered_001","subtag_message":"Delivered","tracked_count":17,"expected_delivery":"2020-01-03T22:01:00+08:00","signed_by":null,"shipment_type":null,"created_at":"2020-01-01T14:08:15+00:00","updated_at":"2020-01-03T06:38:42+00:00","slug":"courex","unique_token":"deprecated","path":"deprecated","shipment_weight":null,"shipment_weight_unit":null,"delivery_time":2,"last_mile_tracking_supported":null,"language":null,"shipment_pickup_date":"2020-01-01T14:07:59","shipment_delivery_date":"2020-01-03T06:26:19","last_updated_at":"2020-01-03T06:38:42+00:00","checkpoints":[{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"New Order","coordinates":[],"tag":"InTransit","subtag":"InTransit_001","subtag_message":"In Transit","created_at":"2020-01-01T14:08:17+00:00","checkpoint_time":"2020-01-01T22:07:59+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Picked Up","coordinates":[],"tag":"InfoReceived","subtag":"InfoReceived_001","subtag_message":"Info Received","created_at":"2020-01-02T14:08:29+00:00","checkpoint_time":"2020-01-02T21:02:50+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Order Processing","coordinates":[],"tag":"InTransit","subtag":"InTransit_001","subtag_message":"In Transit","created_at":"2020-01-02T17:08:31+00:00","checkpoint_time":"2020-01-02T23:16:18+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Scan Checked","coordinates":[],"tag":"InTransit","subtag":"InTransit_001","subtag_message":"In Transit","created_at":"2020-01-02T20:08:33+00:00","checkpoint_time":"2020-01-03T02:25:07+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Departed Hub","coordinates":[],"tag":"InTransit","subtag":"InTransit_001","subtag_message":"In Transit","created_at":"2020-01-03T02:08:36+00:00","checkpoint_time":"2020-01-03T08:50:33+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Departed CP","coordinates":[],"tag":"InTransit","subtag":"InTransit_001","subtag_message":"In Transit","created_at":"2020-01-03T02:08:36+00:00","checkpoint_time":"2020-01-03T08:54:49+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"On the way to deliver","coordinates":[],"tag":"OutForDelivery","subtag":"OutForDelivery_001","subtag_message":"Out for Delivery","created_at":"2020-01-03T05:08:37+00:00","checkpoint_time":"2020-01-03T13:01:26+08:00","slug":"courex"},{"location":"Singapore","country_name":"Singapore","country_iso3":"SGP","state":null,"city":null,"zip":null,"message":"Delivered","coordinates":[],"tag":"Delivered","subtag":"Delivered_001","subtag_message":"Delivered","created_at":"2020-01-03T06:38:42+00:00","checkpoint_time":"2020-01-03T14:26:19+08:00","slug":"courex"}],"order_promised_delivery_date":null,"delivery_type":null,"pickup_location":null,"pickup_note":null,"tracking_account_number":null,"tracking_origin_country":null,"tracking_destination_country":null,"tracking_key":null,"tracking_postal_code":null,"tracking_ship_date":null,"tracking_state":null},
//      "ts":1578033527
//  }

// sample query for $params->msg
// {
//     "id":"5e0b39601641a54511f30448",
//     "tracking_number":"ML3146266",
//     "title":"ML3146266",
//     "note":null,
//     "origin_country_iso3":"SGP",
//     "destination_country_iso3":"SGP",
//     "courier_destination_country_iso3":"SGP",
//     "shipment_package_count":1,
//     "active":false,
//     "order_id":"SGB00180868",
//     "order_id_path":"https:\/\/shaves2u.aftership.com\/SGB00180868",
//     "customer_name":"Chew",
//     "source":"api",
//     "emails":[  ],
//     "smses":[  ],
//     "subscribed_smses":[  ],
//     "subscribed_emails":[  ],
//     "android":[  ],
//     "ios":[  ],
//     "return_to_sender":false,
//     "custom_fields":[  ],
//     "tag":"Delivered",
//     "subtag":"Delivered_001",
//     "subtag_message":"Delivered",
//     "tracked_count":22,
//     "expected_delivery":"2020-01-03T22:01:00+08:00",
//     "signed_by":null,
//     "shipment_type":null,
//     "created_at":"2019-12-31T12:04:48+00:00",
//     "updated_at":"2020-01-03T03:08:20+00:00",
//     "slug":"courex",
//     "unique_token":"deprecated",
//     "path":"deprecated",
//     "shipment_weight":null,
//     "shipment_weight_unit":null,
//     "delivery_time":3,
//     "last_mile_tracking_supported":null,
//     "language":null,
//     "shipment_pickup_date":"2019-12-31T12:04:42",
//     "shipment_delivery_date":"2020-01-03T03:00:36",
//     "last_updated_at":"2020-01-03T03:08:20+00:00",
//     "checkpoints":[  ],
//     "order_promised_delivery_date":null,
//     "delivery_type":null,
//     "pickup_location":null,
//     "pickup_note":null,
//     "tracking_account_number":null,
//     "tracking_origin_country":null,
//     "tracking_destination_country":null,
//     "tracking_key":null,
//     "tracking_postal_code":null,
//     "tracking_ship_date":null,
//     "tracking_state":null
//  }
