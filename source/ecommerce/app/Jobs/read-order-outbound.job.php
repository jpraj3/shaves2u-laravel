<?php

namespace App\Jobs;

use App\Helpers\LaravelHelper;
use App\Helpers\Warehouse\FtpHelperHK;
use App\Helpers\Warehouse\FtpHelperKR;
use App\Helpers\Warehouse\FtpHelperMY;
use App\Helpers\Warehouse\FtpHelperTW;
use App\Helpers\Warehouse\WarehouseHelperHK;
use App\Helpers\Warehouse\WarehouseHelperKR;
use App\Helpers\Warehouse\WarehouseHelperMY;
use App\Helpers\Warehouse\WarehouseHelperTW;
use Illuminate\Console\Command;

class ReadOrderOutboundJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read-order-outbound:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LF - Read Return Order file from Warehouse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('ReadOrderOutboundJob start');

        $this->_WareHouseHelperMY = new WarehouseHelperMY();
        $this->ftpHelperMY = new FtpHelperMY();
        $this->_WareHouseHelperHK = new WarehouseHelperHK();
        $this->ftpHelperHK = new FtpHelperHK();
        $this->_WareHouseHelperKR = new WarehouseHelperKR();
        $this->ftpHelperKR = new FtpHelperKR();
        $this->_WareHouseHelperTW = new WarehouseHelperTW();
        $this->ftpHelperTW = new FtpHelperTW();
        $this->laravelHelper = new LaravelHelper();
        $this->environment_variables = $this->laravelHelper->ConvertArraytoObject(config('environment'));
        $this->config = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'));

        $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->readOrderMalaysia();
        $this->readOrderHongKong();
        $this->readOrderKorea();
        $this->readOrderTaiwan();
    }

    public function readOrderMalaysia()
    {
        $this->Log->info('MY | readOrderMalaysia process start');
        try {
            $fileNames = [];
            $remotePath = config('environment.warehouse_paths.MYS.ftp_inbox_folder');
            $files = $this->ftpHelperMY->listFiles($remotePath);
            $localPath = config('environment.warehouse_paths.MYS.local_download_folder');
            $source = '';
            if ($files !== null) {
                foreach ($files as $file) {
                    if (strrpos($file, 'WMSSHP') !== false) {
                        array_push($fileNames, $file);
                    }
                }
                if (count($fileNames) > 0) {
                    $this->Log->info("MY | readOrderMalaysia | Warehouse output files: " . json_encode($fileNames));
                }
                foreach ($fileNames as $filename) {
                    $this->ftpHelperMY->getFile($remotePath, $localPath, $filename); // download order outbound file
                    // read data in order outbound file and update order status
                    /**
                     * URL prefixing is already done in  readOrderConfirmation function.
                     * Function param supplied should be relative path of file.
                     * */
                    $this->_WareHouseHelperMY->readOrderConfirmation($localPath . '/' . $filename);
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('MY | readOrderMalaysia ERROR => ' . $ex->getMessage());
        }

    }

    public function readOrderHongKong()
    {
        $this->Log->info('HK | readOrderHongKong process start');
        try {
            $fileNames = [];
            $remotePath = config('environment.warehouse_paths.HKG.ftp_inbox_folder');
            $files = $this->ftpHelperHK->listFiles($remotePath);
            $localPath = config('environment.warehouse_paths.HKG.local_download_folder');
            $source = '';
            if ($files !== null) {
                foreach ($files as $file) {
                    if (strrpos($file, 'WMSSHP') !== false) {
                        array_push($fileNames, $file);
                    }
                }
                if (count($fileNames) > 0) {
                    $this->Log->info("HK | readOrderHongKong | Warehouse output files: " . json_encode($fileNames));
                }
                foreach ($fileNames as $filename) {
                    $this->ftpHelperHK->getFile($remotePath, $localPath, $filename); // download order outbound file
                    $this->_WareHouseHelperHK->readOrderConfirmation($localPath . '/' . $filename); // read data in order outbound file and update order status
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('HK | readOrderHongKong ERROR => ' . $ex->getMessage());
        }
    }

    public function readOrderKorea()
    {
        $this->Log->info('KR | readOrderKorea process start');
        try {
            $fileNames = [];
            $remotePath = config('environment.warehouse_paths.KOR.ftp_inbox_folder');
            $files = $this->ftpHelperKR->listFiles($remotePath);
            $localPath = config('environment.warehouse_paths.KOR.local_download_folder');
            $source = '';
            if ($files !== null) {
                foreach ($files as $file) {
                    if (strrpos($file, 'WMSSHP') !== false) {
                        array_push($fileNames, $file);
                    }
                }
                if (count($fileNames) > 0) {
                    $this->Log->info("KR | readOrderKorea | Warehouse output files: " . json_encode($fileNames));
                }
                foreach ($fileNames as $filename) {
                    $this->ftpHelperKR->getFile($remotePath, $localPath, $filename); // download order outbound file
                    $this->_WareHouseHelperKR->readOrderConfirmation($localPath . '/' . $filename); // read data in order outbound file and update order status
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('KR | readOrderKorea ERROR => ' . $ex->getMessage());
        }
    }

    public function readOrderTaiwan()
    {
        $this->Log->info('TW | readOrderKorea process start');
        try {
            $fileNames = [];
            $remotePath = config('environment.warehouse_paths.TWN.ftp_inbox_folder');
            $files = $this->ftpHelperTW->listFiles($remotePath);
            $localPath = config('environment.warehouse_paths.TWN.local_download_folder');
            $source = '';
            if ($files !== null) {
                foreach ($files as $file) {
                    if (strrpos($file, 'WMSSHP') !== false) {
                        array_push($fileNames, $file);
                    }
                }
                if (count($fileNames) > 0) {
                    $this->Log->info("TW | readOrderKorea | Warehouse output files: " . json_encode($fileNames));
                }
                foreach ($fileNames as $filename) {
                    $this->ftpHelperTW->getFile($remotePath, $localPath, $filename); // download order outbound file
                    $this->_WareHouseHelperTW->readOrderConfirmation($localPath . '/' . $filename); // read data in order outbound file and update order status
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('TW | readOrderKorea ERROR => ' . $ex->getMessage());
        }
    }

}
