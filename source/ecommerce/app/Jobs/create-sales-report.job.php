<?php

namespace App\Jobs;

use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanSKU;
use App\Models\Products\Product;
use App\Models\Promotions\Promotions;
use App\Models\Promotions\PromotionCodes;
use App\Models\Receipts\Receipts;
use App\Models\Reports\SalesReport;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Queue\SaleReportQueueService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class CreateSalesReportJob extends Command
{

    protected $signature = 'create-sales-report:job';
    protected $description = 'Create sales report.';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'Create-Sales-Report: ';
        $this->Log = \Log::channel('cronjob');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->Log->info($this->Job . 'Process start.');
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->CreateOrderSalesReport();
        // $this->addKeyToRedis();
        // $this->CreateBulkOrderSalesReport(); // Only enable once Admin Panel is ready [Needs Code Modifications]
    }

    public function addKeyToRedis()
    {

        // order
        $orderId_array = [];
        if (count($orderId_array) > 0) {
            foreach ($orderId_array as $sE) {
                $_redis_sales_report_data = ['id' => $sE, 'bulkOrder' => false];
                SaleReportQueueService::addHash($sE, $_redis_sales_report_data);
            }
        }

        //bulkorder
        $bulkorderId_array = [];
        if (count($bulkorderId_array) > 0) {
            foreach ($bulkorderId_array as $BsE) {
                $_redis_sales_report_data = ['id' => $BsE, 'bulkOrder' => true];
                SaleReportQueueService::addHash($BsE, $_redis_sales_report_data);
            }
        }
    }

    public function CreateOrderSalesReport()
    {
        $orders = SaleReportQueueService::getAllHash();
        $_orders = '';
        if (count($orders) > 30) {
            $_orders = array_slice($orders, 0, 30);
        } else {
            $_orders = $orders;
        }
        $reportLists = $this->buildReportInfo($_orders);
        $this->Log->info('$reportInfo ===> ' . json_encode($reportLists));
        SalesReport::insert($reportLists);
    }

    // build detail info
    public function buildReportInfo($orders)
    {
        $this->Log->info('buildReportInfo process start');
        $reportList = [];
        foreach ($orders as $_order) {
            var_dump('This $_order is ' . $_order);
            $this->Log->info('This $_order is ' . $_order);
            try {
                $_order = json_decode($_order);
                // get order info from order table
                if ($_order->bulkOrder === false) {
                    $order = Orders::where('id', $_order->id)->first();
                    if ($order) {
                        $Seller = null;
                        $Country = null;
                        $User = null;
                        $DeliveryAddress = null;
                        $BillingAddress = null;
                        $Receipt = null;
                        $OrderDetails = null;
                        $Subscription = null;
                        $PlanInfo = null;
                        $marketingOffice = null;
                        $productCategory = null;
                        $email = '';
                        $productName = '';
                        $pcountryqty = '';
                        $sku = '';
                        $qty = '';
                        $deliveryAddress = '';
                        $billingAddress = '';
                        $deliveryContact = '';
                        $billingContact = '';
                        $discountAmount = 0;
                        $fullPrice = 0;
                        $User = User::where('id', $order->UserId)->first();
                        $Country = Countries::where('id', $order->CountryId)->first();
                        if ($order->BillingAddressId === $order->DeliveryAddressId) {
                            $DeliveryAddress = $BillingAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                        } else {
                            $BillingAddress = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
                            $DeliveryAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                        }
                        $Receipt = Receipts::where('OrderId', $order->id)->first();
                        $OrderDetails = OrderDetails::where('OrderId', $order->id)->get();
                        if ($order["SellerUserId"]) {
                            $Seller = SellerUser::where('id', $order["SellerUserId"])->first();
                        }
                        $marketingOffice = $Seller !== null ? MarketingOffice::where('id', $order->MarketingOfficeId)->first() : null;
                        $agentName = $Seller !== null ? $Seller->agentName : '';
                        $badgeId = $Seller !== null ? $Seller->badgeId : '';
                        $region = $Country->code;
                        $category = $Seller !== null ? 'sales app' : 'client';
                        $customerName = $Seller !== null ? $order->fullName : $User->firstName . '' . $User->lastName;
                        $deliveryContact = $order->phone;
                        if (isset($DeliveryAddress)) {
                            $deliveryContact = $DeliveryAddress->contactNumber !== null ? $DeliveryAddress->contactNumber : $order->phone;
                        }
                        $billingContact = $order->phone;
                        if (isset($BillingAddress)) {
                            $billingContact = $BillingAddress->contactNumber !== null ? $DeliveryAddress->contactNumber : $order->phone;
                        }
                        // check if order is ala carte or subscription order
                        if ($order->subscriptionIds) {
                            $Subscription = Subscriptions::where('id', (int) $order->subscriptionIds)->first();
                            if ($Subscription !== null) {
                                $PlanInfo = $this->planHelper->getPlanDetailsByPlanId($Subscription->PlanId, $Country->id, $User->defaultLanguage);
                                // $PlanSKUInfo = PlanSKU::where('sku', $PlanInfo["plansku"])->first();
                                $isPlanAnnual = "Normal";
                                if (isset($PlanInfo) && $PlanInfo["PlanSkuIsAnnual"]) {
                                    $isPlanAnnual = $PlanInfo["PlanSkuIsAnnual"] === 1 ? 'Annual' : 'Normal';
                                }
                            }
                        } else {
                        }
                        if ($Receipt !== null) {

                            $Receipt->subTotalPrice =  $Receipt->originPrice - $Receipt->discountAmount;

                            $discountAmount = $Receipt->discountAmount;
                            if ($order["SellerUserId"]) {
                                // $deliveryContact = $order->phone;
                                // $billingContact = $order->phone;
                                // Setting Discount Values for BA Annual Plan
                                if ($Subscription !== null) {
                                    // First BA (Trial) Order
                                    // check if first order
                                    if ($Subscription->isTrial == 1 && $order->isSubsequentOrder == 1) {
                                        // check if plan is normal or annual
                                        if ($isPlanAnnual === 'Annual' && (int) $PlanInfo["discount_percent"] > 0) {
                                            $discountAmount = (float) $PlanInfo["base_price"] * ((int) $PlanInfo["discount_percent"] / 100);
                                        }
                                    }
                                    // check for subsequent orders for subscription
                                    else {
                                        foreach ($OrderDetails as $od) {
                                            $fullPrice = $fullPrice + ($od["qty"] * $od["price"]);
                                        }
                                        $discountAmount = $fullPrice - $Receipt->originPrice;
                                        if ($fullPrice > $Receipt->originPrice) {
                                            $discountAmount = number_format((float) $fullPrice - $Receipt->originPrice, 2, '.', '');
                                        }
                                    }
                                    // End Setting Discount Values for BA Annual Plan
                                }
                            }

                            // Setting Trial kits Total Price, Discount and Sub-total to 0
                            if ($Subscription) {
                                $Receipt->subTotalPrice = $Receipt->originPrice - $discountAmount;
                            } else {
                                $Receipt->subTotalPrice = $Receipt->subTotalPrice;
                            }
                        }

                        // Else Just use back the original order var values
                        // End Setting Trial kits Total Price, Discount and Sub-total to 0
                        if ($User) {
                            $email = $User->email;
                        } else if ($Seller) {
                            $email = $Seller->email;
                        } else {
                            $email = $order->email;
                        }
                        $this->Log->info('$order->subscriptionIds: ' . $order->subscriptionIds);
                        $this->Log->info('$PlanInfo["plansku"]: ' . $PlanInfo["plansku"]);
                        $this->Log->info('$PlanInfo["plantype"]: ' . $PlanInfo["plantype"]);
                        if ($order->subscriptionIds && $order->isSubsequentOrder === 0 && (isset($PlanInfo) ? $PlanInfo["plantype"] : "") === "trial") {
                            $productCategory = 'Subscription';
                            $plantrialproductinfo = isset($PlanInfo["plantrialproductid"]) && $PlanInfo["plantrialproductid"] !== ""  ?  Product::where('id', (int) $PlanInfo["plantrialproductid"])->first() : "";
                            $productName = isset($PlanInfo) && $PlanInfo !== null && $plantrialproductinfo !== null && $plantrialproductinfo != "" ? $plantrialproductinfo->sku : null;
                            $sku = isset($PlanInfo) && $PlanInfo !== null && $plantrialproductinfo !== null && $plantrialproductinfo != "" ? $plantrialproductinfo->sku : null;
                            $qty = 1;
                            $this->Log->info("subs");
                            $this->Log->info(json_encode($productName));
                            $this->Log->info(json_encode($sku));
                            $this->Log->info(json_encode($qty));
                        } else if ($order->subscriptionIds && $order->isSubsequentOrder === 1 || (isset($PlanInfo) ? $PlanInfo["plantype"] : "") === "custom") {
                            $productCategory = 'Subscription';
                            $productcountryids = [];
                            foreach ($OrderDetails as $od) {
                                $qty = ltrim($qty . ',' . $od["qty"], ',');
                                if ($od["ProductCountryId"]) {
                                    array_push($productcountryids, (int) $od["ProductCountryId"]);
                                }
                            }

                            $productcountries_details = array(
                                "appType" => 'job',
                                "product_country_ids" => $productcountryids,
                            );
                            $purchased_products = $this->productHelper->getProductByProductCountriesV2($Country->id, $productcountries_details);

                            foreach ($purchased_products as $pp) {
                                $productName = ltrim($productName . ',' . $pp["sku"], ',');
                                $sku = ltrim($sku . ',' . $pp["sku"], ',');
                            }
                        } else {
                            $productCategory = 'Ala Carte';
                            // Set Value for Orderdetails
                            $productcountryids = [];
                            foreach ($OrderDetails as $od) {
                                $this->Log->info(json_encode($od["qty"]));
                                $pcountryqty = ltrim($pcountryqty . ',' . (int) $od["ProductCountryId"] . "|" . $od["qty"], ',');
                                if ($od["ProductCountryId"]) {
                                    array_push($productcountryids, (int) $od["ProductCountryId"]);
                                }
                            }

                            $productcountries_details = array(
                                "appType" => 'job',
                                "product_country_ids" => $productcountryids,
                            );
                            $purchased_products = $this->productHelper->getProductByProductCountriesV2($Country->id, $productcountries_details);

                            if (strpos($pcountryqty, ',') !== false) {
                                $seperate = explode(",", $pcountryqty);
                                foreach ($seperate as $seperateq) {
                                    $seperate2 = explode("|", $seperateq);
                                    foreach ($purchased_products as $pp) {
                                        if ($seperate2[0] == $pp["ProductCountryId"]) {
                                            $productName = ltrim($productName . ',' . $pp["sku"], ',');
                                            $sku = ltrim($sku . ',' . $pp["sku"], ',');
                                            $qty = ltrim($qty . ',' . $seperate2[1], ',');
                                        }
                                    }
                                }
                            } else {
                                $seperate2 = explode("|", $pcountryqty);
                                foreach ($purchased_products as $pp) {
                                    $productName = ltrim($productName . ',' . $pp["sku"], ',');
                                    $sku = ltrim($sku . ',' . $pp["sku"], ',');
                                    $qty = ltrim($qty . ',' . $seperate2[1], ',');
                                }
                            }

                            $this->Log->info(json_encode(ltrim($productName, ',')));
                            $this->Log->info(json_encode(ltrim($sku, ',')));
                            $this->Log->info(json_encode(ltrim($qty, ',')));
                        }
                        $orderV2 = $order->toArray();
                        $saleReport = array_search($_order->id, $orderV2);
                        $source = isset($order->source) ? $order->source : null;
                        if ($Receipt) {
                            $daaddress = "";
                            $dacity = "";
                            $daportalCode = "";
                            $dastate = "";

                            $baaddress = "";
                            $bacity = "";
                            $baportalCode = "";
                            $bastate = "";

                            if (isset($DeliveryAddress)) {
                                if (isset($DeliveryAddress->address)) {
                                    $daaddress = $DeliveryAddress->address;
                                }
                                if (isset($DeliveryAddress->city)) {
                                    $dacity =  $DeliveryAddress->city;
                                }
                                if (isset($DeliveryAddress->portalCode)) {
                                    $daportalCode =  (string) $DeliveryAddress->portalCode;
                                }
                                if (isset($DeliveryAddress->state)) {
                                    $dastate = $DeliveryAddress->state;
                                }
                            }

                            if (isset($BillingAddress)) {
                                if (isset($BillingAddress->address)) {
                                    $baaddress = $BillingAddress->address;
                                }
                                if (isset($BillingAddress->city)) {
                                    $bacity =  $BillingAddress->city;
                                }
                                if (isset($BillingAddress->portalCode)) {
                                    $baportalCode =  (string) $BillingAddress->portalCode;
                                }
                                if (isset($BillingAddress->state)) {
                                    $bastate = $BillingAddress->state;
                                }
                            }


                            $reportInfo = [
                                "badgeId" => $Seller !== null ? $Seller->badgeId : null,
                                "agentName" => $Seller !== null ? $Seller->agentName : null,
                                "channelType" => $order->channelType,
                                "eventLocationCode" => $order->eventLocationCode,
                                "email" => $email,
                                "customerName" => $customerName,
                                "deliveryAddress" => $daaddress . ', ' . $dacity . ', ' . $daportalCode . ', ' . $dastate,
                                "deliveryContact" => (string) $deliveryContact,
                                "billingAddress" =>  $baaddress . ', ' . $bacity . ', ' . $baportalCode . ', ' .  $bastate,
                                "billingContact" => (string) $billingContact,
                                "productCategory" => $productCategory,
                                "productName" => $productName,
                                "sku" => $sku,
                                "qty" => $qty,
                                "region" => $region,
                                "category" => $category,
                                "deliveryId" => $order->deliveryId,
                                "carrierAgent" => $order->carrierAgent,
                                "status" => $order->status,
                                "promoCode" => $order->promoCode,
                                "paymentType" => $order->paymentType,
                                "orderId" => $order->id,
                                // "receiptId" => $Receipt->id,
                                "currency" => $Country->currencyCode,
                                "created_at" => $order->created_at,
                                "updated_at" => $order->updated_at,
                                "saleDate" => $order->created_at,
                                "totalPrice" => $Receipt->originPrice,
                                "subTotalPrice" => $Receipt->subTotalPrice,
                                "discountAmount" => $Receipt->discountAmount,
                                "shippingFee" => $Receipt->shippingFee,
                                "taxAmount" => $Receipt->taxAmount,
                                "grandTotalPrice" => $Receipt->originPrice - $Receipt->discountAmount,
                                "source" => $source,
                                "medium" => isset($order->medium) ? $order->medium : null,
                                "campaign" => isset($order->campaign) ? $order->campaign : null,
                                "term" => isset($order->term) ? $order->term : null,
                                "content" => isset($order->content) ? $order->content : null,
                                "CountryId" => $Country->id,
                                "taxInvoiceNo" => $order->taxInvoiceNo,
                                "moCode" => $marketingOffice ? $marketingOffice->moCode : null,
                                "MarketingOfficeId" => $order->MarketingOfficeId,
                                "taxRate" => $order->taxRate,
                                // "includedTaxToProduct" => $order->includedTaxToProduct,
                                "isDirectTrial" => $order->isDirectTrial,
                            ];
                            array_push($reportList, $reportInfo);
                            SaleReportQueueService::deleteHash($_order->id);
                        } else {
                            // SaleReportQueueService::deleteHash($_order->id);
                        }
                    }
                }

                if ($_order->bulkOrder === true) {
                    $order = BulkOrders::where('id', $_order->id)->first();
                    var_dump($order->id, $order->status);
                    if ($order) {
                        $Seller = null;
                        $Country = null;
                        $User = null;
                        $DeliveryAddress = null;
                        $BillingAddress = null;
                        $Receipt = null;
                        $OrderDetails = null;
                        $Subscription = null;
                        $PlanInfo = null;
                        $marketingOffice = null;
                        $productCategory = null;
                        $email = '';
                        $productName = '';
                        $sku = '';
                        $qty = '';
                        $pcountryqty = '';
                        $deliveryAddress = '';
                        $billingAddress = '';
                        $deliveryContact = '';
                        $billingContact = '';
                        $discountAmount = 0;
                        $fullPrice = 0;

                        $Country = Countries::where('id', $order->CountryId)->first();
                        if ($order->BillingAddressId === $order->DeliveryAddressId) {
                            $DeliveryAddress = $BillingAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                        } else {
                            $BillingAddress = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
                            $DeliveryAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                        }
                        $OrderDetails = BulkOrderDetails::where('BulkOrderId', $order->id)->get();

                        $marketingOffice = null;
                        $agentName = '';
                        $badgeId = '';
                        $region = $Country->code;
                        $category = 'Bulk Order';
                        $customerName = $DeliveryAddress->firstName . ' ' . $DeliveryAddress->lastName;

                        $deliveryContact = $order->phone;
                        if (isset($DeliveryAddress)) {
                            $deliveryContact = $DeliveryAddress->contactNumber !== null ? $DeliveryAddress->contactNumber : $order->phone;
                        }
                        $billingContact = $order->phone;
                        if (isset($BillingAddress)) {
                            $billingContact = $BillingAddress->contactNumber !== null ? $BillingAddress->contactNumber : $order->phone;
                        }
                        // check if order is ala carte or subscription order
                        if ($order->subscriptionIds) {
                            $Subscription = Subscriptions::where('id', $order->subscriptionIds)->first();
                            $PlanInfo = $this->planHelper->getPlanDetailsByPlanId($Subscription->PlanId, $Country->id, $User->defaultLanguage);
                            // $PlanSKUInfo = PlanSKU::where('sku', $PlanInfo["plansku"])->first();
                            // $isPlanAnnual = $PlanSKUInfo->isAnnual === 1 ? 'Annual' : 'Normal';
                        } else {
                        }

                        $totalprice = 0.00;

                        foreach ($OrderDetails as $details) {
                            $totalprice += $details->price;
                            $details->unit_price = number_format($details->price / $details->qty, 2);
                            $details->price = number_format($details->price, 2);
                        }

                        $totalprice = number_format($totalprice, 2);

                        $discount = PromotionCodes::where('code', $order->promoCode)
                            ->leftJoin('promotions', 'promotioncodes.PromotionId', '=', 'promotions.id')
                            ->pluck('promotions.discount')->first();

                        if ($discount) {
                            $discountPercent = ($discount) / 100;
                            $reversePercent = 1 + $discountPercent;
                            $discountAmount = number_format($discountPercent * $totalprice, 2);
                            $subTotal = number_format($reversePercent * $totalprice, 2);
                        } else {
                            $discountAmount = number_format(0.00, 2);
                            $subTotal = $totalprice;
                        }

                        // Else Just use back the original order var values
                        // End Setting Trial kits Total Price, Discount and Sub-total to 0
                        $email = $order->email;

                        if ($order->subscriptionIds) {
                            $productCategory = 'Subscription';
                            $productName = $PlanInfo["plansku"];
                            $sku = $PlanInfo["plansku"];
                            $qty = 1;
                            $this->Log->info("subs");
                            $this->Log->info(json_encode($productName));
                            $this->Log->info(json_encode($sku));
                            $this->Log->info(json_encode($qty));
                        } else {
                            $productCategory = 'Ala Carte';
                            // Set Value for Orderdetails
                            $productcountryids = [];
                            foreach ($OrderDetails as $od) {
                                $this->Log->info(json_encode($od["qty"]));
                                $pcountryqty = ltrim($pcountryqty . ',' . (int) $od["ProductCountryId"] . "|" . $od["qty"], ',');
                                if ($od["ProductCountryId"]) {
                                    array_push($productcountryids, (int) $od["ProductCountryId"]);
                                }
                            }

                            $productcountries_details = array(
                                "appType" => 'job',
                                "product_country_ids" => $productcountryids,
                            );
                            $purchased_products = $this->productHelper->getProductByProductCountriesV2($Country->id, $productcountries_details);
                            if (strpos($pcountryqty, ',') !== false) {
                                $seperate = explode(",", $pcountryqty);
                                foreach ($seperate as $seperateq) {
                                    $seperate2 = explode("|", $seperateq);
                                    foreach ($purchased_products as $pp) {
                                        if ($seperate2[0] == $pp["ProductCountryId"]) {
                                            $productName = ltrim($productName . ',' . $pp["sku"], ',');
                                            $sku = ltrim($sku . ',' . $pp["sku"], ',');
                                            $qty = ltrim($qty . ',' . $seperate2[1], ',');
                                        }
                                    }
                                }
                            } else {
                                $seperate2 = explode("|", $pcountryqty);
                                foreach ($purchased_products as $pp) {
                                    $productName = ltrim($productName . ',' . $pp["sku"], ',');
                                    $sku = ltrim($sku . ',' . $pp["sku"], ',');
                                    $qty = ltrim($qty . ',' . $seperate2[1], ',');
                                }
                            }

                            $this->Log->info(json_encode(ltrim($productName, ',')));
                            $this->Log->info(json_encode(ltrim($sku, ',')));
                            $this->Log->info(json_encode(ltrim($qty, ',')));
                        }

                        $orderV2 = $order->toArray();
                        $saleReport = array_search($_order->id, $orderV2);
                        $source = isset($order->source) ? $order->source : null;
                        $daaddress = "";
                        $dacity = "";
                        $daportalCode = "";
                        $dastate = "";

                        $baaddress = "";
                        $bacity = "";
                        $baportalCode = "";
                        $bastate = "";

                        if (isset($DeliveryAddress)) {
                            if (isset($DeliveryAddress->address)) {
                                $daaddress = $DeliveryAddress->address;
                            }
                            if (isset($DeliveryAddress->city)) {
                                $dacity =  $DeliveryAddress->city;
                            }
                            if (isset($DeliveryAddress->portalCode)) {
                                $daportalCode =  (string) $DeliveryAddress->portalCode;
                            }
                            if (isset($DeliveryAddress->state)) {
                                $dastate = $DeliveryAddress->state;
                            }
                        }

                        if (isset($BillingAddress)) {
                            if (isset($BillingAddress->address)) {
                                $baaddress = $BillingAddress->address;
                            }
                            if (isset($BillingAddress->city)) {
                                $bacity =  $BillingAddress->city;
                            }
                            if (isset($BillingAddress->portalCode)) {
                                $baportalCode =  (string) $BillingAddress->portalCode;
                            }
                            if (isset($BillingAddress->state)) {
                                $bastate = $BillingAddress->state;
                            }
                        }

                        $reportInfo = [
                            "badgeId" => null,
                            "agentName" => null,
                            "channelType" => $order->channelType,
                            "eventLocationCode" => $order->eventLocationCode,
                            "email" => $email,
                            "customerName" => $customerName,
                            "deliveryAddress" =>  $daaddress . ', ' .  $dacity . ', ' . $daportalCode . ', ' .  $dastate,
                            "deliveryContact" => (string) $deliveryContact,
                            "billingAddress" => $baaddress . ', ' . $bacity . ', ' .  $baportalCode . ', ' . $bastate,
                            "billingContact" => (string) $billingContact,
                            "productCategory" => $productCategory,
                            "productName" => $productName,
                            "sku" => $sku,
                            "qty" => $qty,
                            "region" => $region,
                            "category" => $category,
                            "deliveryId" => $order->deliveryId,
                            "carrierAgent" => $order->carrierAgent,
                            "status" => $order->status,
                            "promoCode" => $order->promoCode,
                            "paymentType" => $order->paymentType,
                            "bulkOrderId" => $order->id,
                            // "receiptId" => $Receipt->id,
                            "currency" => $Country->currencyCode,
                            "created_at" => $order->created_at,
                            "updated_at" => $order->updated_at,
                            "saleDate" => $order->created_at,
                            "totalPrice" => $totalprice,
                            "subTotalPrice" => $subTotal,
                            "discountAmount" => $discountAmount,
                            "shippingFee" => '0.00',
                            "taxAmount" => '0.00',
                            "grandTotalPrice" => $totalprice,
                            "source" => $source,
                            "medium" => isset($order->medium) ? $order->medium : null,
                            "campaign" => isset($order->campaign) ? $order->campaign : null,
                            "term" => isset($order->term) ? $order->term : null,
                            "content" => isset($order->content) ? $order->content : null,
                            "CountryId" => $Country->id,
                            "taxInvoiceNo" => $order->taxInvoiceNo,
                            "moCode" => null,
                            "MarketingOfficeId" => null,
                            "taxRate" => $order->taxRate,
                            // "includedTaxToProduct" => $order->includedTaxToProduct,
                            "isDirectTrial" => $order->isDirectTrial,
                        ];
                        array_push($reportList, $reportInfo);
                        SaleReportQueueService::deleteHash($_order->id);
                    }
                }
            } catch (Exception $e) {
                $this->Log->error('sales report create for orders  error=> ' . json_encode($e->getMessage()));
                SaleReportQueueService::addHash($_order->id, $_order);
            }
        }

        return $reportList;
    }

    // public function CreateBulkOrderSalesReport()
    // {
    //     $bulkorders = SaleReportQueueService::getAllHash();
    //     $_bulkorders = '';
    //     if (count($bulkorders) > 1) {
    //         $_bulkorders = array_slice($bulkorders, 0, 1);
    //     } else {
    //         $_bulkorders = $bulkorders;
    //     }
    //     $reportLists = $this->buildBulkOrderReportInfo($_bulkorders);
    //     SalesReport::insert($reportLists);
    // }

    // build detail info
    // public function buildBulkOrderReportInfo($bulkorders)
    // {
    //     $this->Log->info('buildBulkOrderReportInfo process start');
    //     $reportList = [];
    //     foreach ($bulkorders as $_order) {
    //         try {
    //             $_order = json_decode($_order);
    //             // get order info from order table
    //             if ($_order->bulkOrder === true) {
    //                 $order = BulkOrders::where('id', $_order->id)->first();
    //                 if ($order) {
    //                     $Seller = null;
    //                     $Country = null;
    //                     $User = null;
    //                     $DeliveryAddress = null;
    //                     $BillingAddress = null;
    //                     $Receipt = null;
    //                     $OrderDetails = null;
    //                     $Subscription = null;
    //                     $PlanInfo = null;
    //                     $marketingOffice = null;
    //                     $productCategory = null;
    //                     $email = '';
    //                     $productName = '';
    //                     $sku = '';
    //                     $qty = '';
    //                     $deliveryAddress = '';
    //                     $billingAddress = '';
    //                     $deliveryContact = '';
    //                     $billingContact = '';
    //                     $discountAmount = 0;
    //                     $fullPrice = 0;

    //                     $Country = Countries::where('id', $order->CountryId)->first();
    //                     if ($order->BillingAddressId === $order->DeliveryAddressId) {
    //                         $DeliveryAddress = $BillingAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
    //                     } else {
    //                         $BillingAddress = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
    //                         $DeliveryAddress = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
    //                     }
    //                     $OrderDetails = BulkOrderDetails::where('BulkOrderId', $order->id)->get();

    //                     $marketingOffice = null;
    //                     $agentName = '';
    //                     $badgeId = '';
    //                     $region = $Country->code;
    //                     $category = 'Bulk Order';
    //                     $customerName = $DeliveryAddress->firstName . ' ' . $DeliveryAddress->lastName;

    //                     $deliveryContact = $order->phone;
    //                     if (isset($DeliveryAddress)) {
    //                         $deliveryContact = $DeliveryAddress->contactNumber !== null ? $DeliveryAddress->contactNumber : $order->phone;
    //                     }
    //                     $billingContact = $order->phone;
    //                     if (isset($BillingAddress)) {
    //                         $billingContact = $BillingAddress->contactNumber !== null ? $BillingAddress->contactNumber : $order->phone;
    //                     }
    //                     // check if order is ala carte or subscription order
    //                     if ($order->subscriptionIds) {
    //                         $Subscription = Subscriptions::where('id', $order->subscriptionIds)->first();
    //                         $PlanInfo = $this->planHelper->getPlanDetailsByPlanId($Subscription->PlanId, $Country->id, $User->defaultLanguage);
    //                         // $PlanSKUInfo = PlanSKU::where('sku', $PlanInfo["plansku"])->first();
    //                         // $isPlanAnnual = $PlanSKUInfo->isAnnual === 1 ? 'Annual' : 'Normal';
    //                     } else {
    //                     }

    //                     $totalprice = 0.00;

    //                     foreach ($OrderDetails as $details) {
    //                         $totalprice += $details->price;
    //                         $details->unit_price = number_format($details->price / $details->qty, 2);
    //                         $details->price = number_format($details->price, 2);
    //                     }

    //                     $totalprice = number_format($totalprice, 2);

    //                     $discount = PromotionCodes::where('code', $order->promoCode)
    //                         ->leftJoin('promotions', 'promotioncodes.PromotionId', '=', 'promotions.id')
    //                         ->pluck('promotions.discount')->first();

    //                     if ($discount) {
    //                         $discountPercent = ($discount) / 100;
    //                         $reversePercent = 1 + $discountPercent;
    //                         $discountAmount = number_format($discountPercent * $totalprice, 2);
    //                         $subTotal = number_format($reversePercent * $totalprice, 2);
    //                     } else {
    //                         $discountAmount = number_format(0.00, 2);
    //                         $subTotal = $totalprice;
    //                     }

    //                     // Else Just use back the original order var values
    //                     // End Setting Trial kits Total Price, Discount and Sub-total to 0
    //                     $email = $order->email;

    //                     if ($order->subscriptionIds) {
    //                         $productCategory = 'Subscription';
    //                         $productName = $PlanInfo["plansku"];
    //                         $sku = $PlanInfo["plansku"];
    //                         $qty = 1;
    //                         $this->Log->info("subs");
    //                         $this->Log->info(json_encode($productName));
    //                         $this->Log->info(json_encode($sku));
    //                         $this->Log->info(json_encode($qty));
    //                     } else {
    //                         $productCategory = 'Ala Carte';
    //                         // Set Value for Orderdetails
    //                         $productcountryids = [];
    //                         foreach ($OrderDetails as $od) {
    //                             $this->Log->info(json_encode($od["qty"]));
    //                             $qty = ltrim($qty . ',' . $od["qty"], ',');
    //                             if ($od["ProductCountryId"]) {
    //                                 array_push($productcountryids, (int) $od["ProductCountryId"]);
    //                             }
    //                         }

    //                         $productcountries_details = array(
    //                             "appType" => 'job',
    //                             "product_country_ids" => $productcountryids,
    //                         );
    //                         $purchased_products = $this->productHelper->getProductByProductCountriesV2($Country->id, $productcountries_details);

    //                         foreach ($purchased_products as $pp) {
    //                             $productName = ltrim($productName . ',' . $pp["sku"], ',');
    //                             $sku = ltrim($sku . ',' . $pp["sku"], ',');
    //                         }
    //                         $this->Log->info(json_encode(ltrim($productName, ',')));
    //                         $this->Log->info(json_encode(ltrim($sku, ',')));
    //                         $this->Log->info(json_encode(ltrim($qty, ',')));
    //                     }
    //                 }

    //                 $saleReport = array_search($_order->id, $bulkorders);
    //                 $source = isset($saleReport->source) ? $saleReport->source : null;
    //                 $daaddress = "";
    //                 $dacity = "";
    //                 $daportalCode = "";
    //                 $dastate = "";

    //                 $baaddress = "";
    //                 $bacity = "";
    //                 $baportalCode = "";
    //                 $bastate = "";

    //                 if (isset($DeliveryAddress)) {
    //                     if (isset($DeliveryAddress->address)) {
    //                         $daaddress = $DeliveryAddress->address;
    //                     }
    //                     if (isset($DeliveryAddress->city)) {
    //                         $dacity =  $DeliveryAddress->city;
    //                     }
    //                     if (isset($DeliveryAddress->portalCode)) {
    //                         $daportalCode =  (string) $DeliveryAddress->portalCode;
    //                     }
    //                     if (isset($DeliveryAddress->state)) {
    //                         $dastate = $DeliveryAddress->state;
    //                     }
    //                 }

    //                 if (isset($BillingAddress)) {
    //                     if (isset($BillingAddress->address)) {
    //                         $baaddress = $BillingAddress->address;
    //                     }
    //                     if (isset($BillingAddress->city)) {
    //                         $bacity =  $BillingAddress->city;
    //                     }
    //                     if (isset($BillingAddress->portalCode)) {
    //                         $baportalCode =  (string) $BillingAddress->portalCode;
    //                     }
    //                     if (isset($BillingAddress->state)) {
    //                         $bastate = $BillingAddress->state;
    //                     }
    //                 }

    //                 $reportInfo = [
    //                     "badgeId" => null,
    //                     "agentName" => null,
    //                     "channelType" => $order->channelType,
    //                     "eventLocationCode" => $order->eventLocationCode,
    //                     "email" => $email,
    //                     "customerName" => $customerName,
    //                     "deliveryAddress" =>  $daaddress . ', ' .  $dacity . ', ' . $daportalCode . ', ' .  $dastate,
    //                     "deliveryContact" => (string) $deliveryContact,
    //                     "billingAddress" => $baaddress . ', ' . $bacity . ', ' .  $baportalCode . ', ' . $bastate,
    //                     "billingContact" => (string) $billingContact,
    //                     "productCategory" => $productCategory,
    //                     "productName" => $productName,
    //                     "sku" => $sku,
    //                     "qty" => $qty,
    //                     "region" => $region,
    //                     "category" => $category,
    //                     "deliveryId" => $order->deliveryId,
    //                     "carrierAgent" => $order->carrierAgent,
    //                     "status" => $order->status,
    //                     "promoCode" => $order->promoCode,
    //                     "paymentType" => $order->paymentType,
    //                     "bulkOrderId" => $order->id,
    //                     // "receiptId" => $Receipt->id,
    //                     "currency" => $Country->currencyCode,
    //                     "created_at" => $order->created_at,
    //                     "updated_at" => $order->updated_at,
    //                     "saleDate" => $order->created_at,
    //                     "totalPrice" => $totalprice,
    //                     "subTotalPrice" => $subTotal,
    //                     "discountAmount" => $discountAmount,
    //                     "shippingFee" => '0.00',
    //                     "taxAmount" => '0.00',
    //                     "grandTotalPrice" => $totalprice,
    //                     "source" => $source,
    //                     "medium" => isset($saleReport->medium) ? $saleReport->medium : null,
    //                     "campaign" => isset($saleReport->campaign) ? $saleReport->campaign : null,
    //                     "term" => isset($saleReport->term) ? $saleReport->term : null,
    //                     "content" => isset($saleReport->content) ? $saleReport->content : null,
    //                     "CountryId" => $Country->id,
    //                     "taxInvoiceNo" => $order->taxInvoiceNo,
    //                     "moCode" => null,
    //                     "MarketingOfficeId" => null,
    //                     "taxRate" => $order->taxRate,
    //                     // "includedTaxToProduct" => $order->includedTaxToProduct,
    //                     "isDirectTrial" => $order->isDirectTrial,
    //                 ];
    //                 array_push($reportList, $reportInfo);
    //                 SaleReportQueueService::deleteHash($_order->id);
    //             }
    //         } catch (Exception $e) {
    //             $this->Log->info('sales report create bulkorder error=> ' . json_encode($e->getMessage()));
    //         }
    //     }

    //     return $reportList;
    // }
}
