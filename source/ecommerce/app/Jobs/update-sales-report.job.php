<?php

namespace App\Jobs;

use App\Models\Orders\Orders;
use App\Queue\SaleReportQueueService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateSalesReportJob extends Command
{
    protected $signature = 'update-sales-report:job';
    protected $description = 'update sales report.';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'update-sales-report: ';
        $this->Log = \Log::channel('cronjob');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->Log->info($this->Job . 'Process start.');
        $this->UpdateOrderSalesReport();
        $this->UpdateBulkOrderSalesReport();
        $this->UpdateSalesReportUTMSources();
    }

    public function UpdateOrderSalesReport()
    {
        $this->Log->info('UpdateOrderSalesReport Process start.');
        DB::statement('UPDATE salereports AS SR
        INNER JOIN orders AS ORD
          ON SR.OrderId = ORD.id
        SET SR.status = ORD.status,
          SR.deliveryId = ORD.deliveryId,
          SR.carrierAgent = ORD.carrierAgent,
          SR.taxInvoiceNo = ORD.taxInvoiceNo,
          SR.updated_at = ORD.updated_at,
          SR.source = ORD.source,
          SR.medium = ORD.medium,
          SR.campaign = ORD.campaign,
          SR.term = ORD.term,
          SR.content = ORD.content
        WHERE SR.status <> ORD.status OR
          SR.deliveryId <> ORD.deliveryId OR
          SR.carrierAgent <> ORD.carrierAgent OR
          SR.taxInvoiceNo <> ORD.taxInvoiceNo OR
          SR.updated_at <> ORD.updated_at OR
          SR.source <> ORD.source OR
          SR.medium <> ORD.medium OR
          SR.campaign <> ORD.campaign OR
          SR.term <> ORD.term OR
          SR.content <> ORD.content OR
          (SR.deliveryId IS NULL AND ORD.deliveryId IS NOT NULL) OR
          (SR.carrierAgent IS NULL AND ORD.carrierAgent IS NOT NULL) OR
          (SR.taxInvoiceNo IS NULL AND ORD.taxInvoiceNo IS NOT NULL);');

        // $this->UpdateSalesReportUTMSources();
    }
    public function UpdateBulkOrderSalesReport()
    {
        $this->Log->info('UpdateOrderSalesReport Process start.');
        DB::statement('UPDATE salereports AS SR
        INNER JOIN bulkorders AS BORD
          ON SR.bulkOrderId = BORD.id
        SET SR.status = BORD.status,
          SR.deliveryId = BORD.deliveryId,
          SR.carrierAgent = BORD.carrierAgent,
          SR.taxInvoiceNo = BORD.taxInvoiceNo,
          SR.updated_at = BORD.updated_at
        WHERE SR.status <> BORD.status OR
          SR.deliveryId <> BORD.deliveryId OR
          SR.carrierAgent <> BORD.carrierAgent OR
          SR.taxInvoiceNo <> BORD.taxInvoiceNo OR
          SR.updated_at <> BORD.updated_at OR
          (SR.deliveryId IS NULL AND BORD.deliveryId IS NOT NULL) OR
          (SR.carrierAgent IS NULL AND BORD.carrierAgent IS NOT NULL) OR
          (SR.taxInvoiceNo IS NULL AND BORD.taxInvoiceNo IS NOT NULL);');
    }
    public function UpdateSalesReportUTMSources()
    {
        $this->Log->info('UpdateSalesReportUTMSources Process start.');
        $orders = Orders::select('orders.*')
            ->whereNotIn('id', [DB::raw('SELECT OrderId FROM salereports WHERE OrderId IS NOT NULL')])
            ->get();
        $this->Log->info(json_encode($orders));
        if (!empty($orders)) {
            foreach ($orders as $order) {
                if ($order->source !== null || $order->medium !== null || $order->campaign !== null || $order->term !== null || $order->content !== null) {
                    $_redis_sales_report_data = [
                        'id' => $order->id,
                        'bulkOrder' => false,
                        'source' => $order->source,
                        'medium' => $order->medium,
                        'campaign' => $order->campaign,
                        'term' => $order->term,
                        'content' => $order->content,
                    ];
                    SaleReportQueueService::addHash($order->id, $_redis_sales_report_data);
                } else {
                    $_redis_sales_report_data = [
                        'id' => $order->id,
                        'bulkOrder' => false,
                    ];
                    SaleReportQueueService::addHash($order->id, $_redis_sales_report_data);
                }
            }
        }
    }
}
