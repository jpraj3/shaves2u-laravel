<?php

namespace App\Jobs;

use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\Payment\NicePayHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\TaxInvoiceHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\Promotions\Promotions;
use App\Models\Promotions\PromotionCodes;
use App\Models\Reports\SalesReport;
use App\Models\Subscriptions\ChargeJobChecks;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use App\Models\Rebates\Referral;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Rebates\RewardsIn;
use App\Models\Campaigns\CampaignReactiveHistories;
use App\Queue\SendEmailQueueService as EmailQueueService;
use App\Services\CountryService as CountryService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use App\Http\Controllers\Ecommerce\Rebates\Referral\ReferralController;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\Subscriptions\ChargeJobLists;
use App\Queue\SaleReportQueueService;
use App\Queue\TaxInvoiceQueueService;

class SubscriptionRechargeNicepayJob extends Command
{
    protected $signature = 'subscription-recharge-nicepay:job';
    protected $description = 'Subscription recharge nicepay job.';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->Job = 'Subscription-Recharge-Nicepay-Job: ';
        $this->Log = \Log::channel('cronjob');

        $this->nicepayController = new NicePayController();
        $this->nicepay = new NicePayHelper();
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    // Function: Main
    public function handle()
    {
        $this->userHelper = new UserHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->planHelper = new PlanHelper();
        $this->country_service = new CountryService();
        $this->productHelper = new ProductHelper();
        $this->taxInvoiceHelper = new TaxInvoiceHelper();
        $this->orderHelper = new OrderHelper();
        $this->referralController = new ReferralController();
        $this->Log->info($this->Job . 'Process start.');
        $this->total_to_charge = 0;
        $this->is_from_campaign = 0;
        $this->utm_source = null;
        $this->utm_medium = null;
        $this->utm_campaign = null;
        $this->utm_content = null;
        $this->utm_term = null;
        $this->GetSubscriptions();
    }

    // Function: Get subscriptions
    public function GetSubscriptions()
    {
        $this->Log->info($this->Job . 'Get subscription start.');
        try {
            // Get subscription based on several filters
            $subscriptions = Subscriptions::leftJoin('promotions', 'promotions.id', 'subscriptions.promotionId')
                ->select('subscriptions.*', 'promotions.*', 'subscriptions.id as subscriptionsid', 'promotions.id as promotionsid')
                ->where('subscriptions.status', 'On Hold')
                ->where('subscriptions.nextChargeDate', '<=', $this->GetCurrentDate())
                ->where(function ($nextDeliverDate) {
                    $nextDeliverDate->where('subscriptions.nextDeliverDate', '<=', $this->GetCurrentDate())->orWhere('subscriptions.nextDeliverDate', null);
                })
                ->where('subscriptions.CardId', '!=', null)
                ->where('subscriptions.recharge_date', '!=', null)
                ->where('subscriptions.recharge_date', '<=', $this->GetCurrentDate())
                ->where('subscriptions.unrealizedCust', '!=', 1)
                ->where('subscriptions.total_recharge', '!=', 0)
                ->where('subscriptions.total_recharge', '<=', 5)
                ->get();
            $this->total_to_charge = count($subscriptions);
            // Loop through each subscription
            foreach ($subscriptions as $subs) {
                // Build all required information
                $subs->user = (object) array(User::where('id', $subs->UserId)->first())["0"];
                $subs->user->fullname = $subs->user->firstName . $subs->user->lastName;
                $subs->card = (object) array(Cards::where('id', $subs->CardId)->first())["0"];
                $subs->deliveryAddress = (object) array(DeliveryAddresses::where('id', $subs->DeliveryAddressId)->first())["0"];
                $subs->billingAddress = (object) array(DeliveryAddresses::where('id', $subs->BillingAddressId)->first())["0"];
                $subs->plan = $this->planHelper->getOnePlanData($subs->PlanId);
                $subs->plan_sku = (object) array(PlanSKU::where('id', $subs->plan->PlanSkuId)->first())["0"];
                $subs->country = (object) array(Countries::where('id', $subs->plan->CountryId)->first())["0"];
                $subs->post_payment = (object) array();
                $subs->isOnline = $subs->isOffline === 1 ? 0 : 1;
                $subs->appType = $subs->isOffline === 1 ? 'baWebsite' : 'ecommerce';
                $subs->tax = (object) array($this->laravelHelper->ConvertArraytoObject($this->country_service->tax($subs->country->id)))["0"];
                $this->utm_source = null;
                $this->utm_medium = null;
                $this->utm_campaign = null;
                $this->utm_content = null;
                $this->utm_term = null;
                // Promotion
                $subs->promotionallow = 0;
                $subs->promotionusebefore = 0;
                if ($subs->promotionId) {
                    if ($subs->promotionType == 'Instant' || $subs->promotionType == 'Upcoming') {
                        $promotionuse = Orders::where('subscriptionIds', $subs->subscriptionsid)
                            ->where('PromotionId', $subs->promotionsid)
                            ->where('PromotionId', '!=', null)
                            ->count();
                        if ($promotionuse == 0) {
                            $subs->promotionallow = 1;
                        }else{
                            $subs->promotionusebefore = 1;
                        }
                    } else if ($subs->promotionType == 'Recurring') {
                        $subs->promotionallow = 1;
                    }
                }

                // Continue the subscription charge job for these countries
                if (in_array($subs->country->id, [8])) {
                    $subs->checkid = $this->createChecking($subs);
                    $this->CheckPlanPrice($subs);
                }
            }

            // $emailData = [];
            // // Passing $subs Data into $moduleData for Email
            // $emailData['title'] = 'subscription-recharge-nicepay-email-report';
            // $emailData['moduleData'] = (object) array(
            //     'email' => '',
            //     'emaillist' => $subscriptions,
            //     'countryId' => 8,
            //     'isPlan' => 1,
            //     'isMale' => 1,
            // );
            // $emailData['CountryAndLocaleData'] = array(8, 'ko');

            // EmailQueueService::addHash('admin', 'subscription_recharge_nicepay_email_report', $emailData);
        } catch (Exception $e) {
            $this->onSubscriptionRechargeError('Get subscription error: ' . $e->getMessage());
        }
    }

    // Function: Check plan price
    public function CheckPlanPrice($subs)
    {
        $this->Log->info($this->Job . 'Check plan price for subscription id ' . $subs->subscriptionsid . '.');

        try {
            //Initialize parameters to get product country details
            $checkproduct = array(
                "appType" => 'job',
                "isOnline" => $subs->isOnline,
                "isOffline" => $subs->isOffline,
                "product_country_ids" => $this->planHelper->getAllAvailableProductsByCycle($subs->subscriptionsid, $subs->currentCycle + 1, $subs->country->id, $subs->user->defaultLanguage),
            );
            // Get all order details
            $checkorderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $checkproduct));

            //for annual
            $checkorderDetailListAnnual = '';
            $originalannualprice = 0;
            $originalannualtotalprice = 0;
            $current_annual_addons_total_price = 0;
            $originalannualdiscountper =0;
            $originalannualtotaldiscount = 0;
            $originalannualtotaldiscountprice = 0;
          
            if($subs->isAnnualDiscountIncludeAddon == "1"){
                $originalannualprice = $subs->plan->price;
                $originalannualdiscountper = $subs->plan->discountPercent;
             //Initialize parameters to get product country details
            $checkproductAnnual = array(
                "appType" => 'job',
                "isOnline" => $subs->isOnline,
                "isOffline" => $subs->isOffline,
                "product_country_ids" => $this->planHelper->getAllAvailableAddOnProductsByCycle($subs->subscriptionsid, $subs->currentCycle + 1, $subs->country->id, $subs->user->defaultLanguage),
            );
            // Get all order details
            $current_annual_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
            ->where('subscriptionId', $subs->subscriptionsid)
            ->whereIn('productcountries.id', $checkproductAnnual["product_country_ids"]) // product type id
            ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
            ->sum(DB::raw('productcountries.sellPrice * subscriptions_productaddon.qty'));
            $originalannualtotalprice = $originalannualprice + $current_annual_addons_total_price;
            $originalannualtotaldiscount =  (($originalannualtotalprice * $originalannualdiscountper) / 100);
            $originalannualtotaldiscountprice =  $originalannualtotalprice - $originalannualtotaldiscount;
            }

            $discountforfreeexistproduct = 0;
            $promofreeexistresultproduct = [];

            // Promotion free existing product
            if ($subs->temp_discountPercent != null && $subs->temp_discountPercent > 0) {
                $subs->promotionallow = 1;
            } else if ($subs->promotionallow == 1) {
                $productseperate = [];
                if ($subs->freeExistProductCountryIds) {
                    $subs->freeExistProductCountryIds = str_replace("[", "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace("]", "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace('"', "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace('"', "", $subs->freeExistProductCountryIds);

                    $splitproduct = $subs->freeExistProductCountryIds;
                    if (strpos($splitproduct, ',') !== false) {
                        $splitproduct = explode(",", $subs->freeExistProductCountryIds);
                    }
                    $productseperate = [];
                    if (is_array($splitproduct)) {
                        foreach ($splitproduct as $product) {
                            array_push($productseperate, $product);
                        }
                    } else {
                        array_push($productseperate, $splitproduct);
                    }
                }
                // Create order details
                foreach ($checkorderDetailList as $product) {

                    if (in_array($product->ProductCountryId, $productseperate)) {
                        array_push($promofreeexistresultproduct, $product->ProductCountryId);
                        $discountforfreeexistproduct = $discountforfreeexistproduct + $product->sellPrice;
                    }
                }
            }
            $subs->promo_freeexistresultproduct = [];
            if ($promofreeexistresultproduct) {
                $subs->promo_freeexistresultproduct = $promofreeexistresultproduct;
            }

            // Get price from subscription
            $subscriptionPrice = $subs->pricePerCharge;

            // Initialize variables
            $totalprice = 0;
            $totaldiscount = 0;
            $totaldiscountprice = 0;

            // Get shipping fee
            $getshippingfee = $this->country_service->shippingFee($subs->country->id, $subs->country->lang_code);
            $shippingfee = "";
            if ($getshippingfee) {
                $shippingfee = (float) $getshippingfee["shippingfee"];
            }

            $totalprice = $totalprice + $subscriptionPrice;
            // Promotion free existing product
            if ($subs->promotionallow == 1) {
                if ($discountforfreeexistproduct > 0) {
                    $totalprice = $totalprice - $discountforfreeexistproduct;
                }
            }

            // Promotion free shippingfee
            if ($subs->promotionallow == 1) {
                if ($subs->isFreeShipping) {
                    if ($subs->isFreeShipping === 1) {
                        $shippingfee = 0;
                    }
                }
                // Promotion
                if ($subs->temp_discountPercent != null && $subs->temp_discountPercent > 0) {
                    $totaldiscount = (($totalprice * $subs->temp_discountPercent) / 100);
                } else {
                    $totaldiscount = (($totalprice * $subs->discount) / 100);
                }
                $totaldiscountprice = $totalprice - $totaldiscount;

                $totaldiscountprice = $totaldiscountprice + $shippingfee;
                $taxAmount = $subs->tax->taxRate / 100 * $totaldiscountprice;
                $totaldiscountprice = $totaldiscountprice + 0.00;

                $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');

                // Promotion maxdiscount
                if ($subs->maxDiscount) {
                    if ($subs->maxDiscount < $totaldiscount) {
                        $totaldiscount = $subs->maxDiscount;
                        $totaldiscountprice = $totalprice - $subs->maxDiscount;
                        $totaldiscountprice = $totaldiscountprice + $shippingfee;
                        $taxAmount = $subs->tax->taxRate / 100 * ($totaldiscountprice);
                        $totaldiscountprice = $totaldiscountprice + 0.00;
                        $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                    }
                }
                // Promotion free exist product
                if ($discountforfreeexistproduct > 0) {
                    $totaldiscount = $totaldiscount + $discountforfreeexistproduct;
                }
                if($subs->isAnnualDiscountIncludeAddon == "1"){
                    $totaldiscount =  $totaldiscount + $originalannualtotaldiscount;
                }
                // Promotion no negative number
                if ($totaldiscountprice <= 0) {
                    $totaldiscountprice = 0;
                    $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                }
            }

            // Calculate tax amount and total price
            $totalprice = $totalprice + $shippingfee;
            $taxAmount = $subs->tax->taxRate / 100 * ($totalprice);
            $totalprice = $totalprice + 0.00;
            $totalprice = number_format($totalprice, 2, '.', '');

            // Insert into priceData variable for later access
            $subs->priceData = (object) array();
            $subs->priceData->totalprice = $totalprice;
            $subs->priceData->totaldiscount = $totaldiscount;
            $subs->priceData->subtotal = $subscriptionPrice;
            $subs->priceData->totaldiscountprice = $totaldiscountprice;
            $subs->priceData->shippingfee = $shippingfee;
            $subs->priceData->taxAmount = $taxAmount;

            $subs->priceData->originalannualprice = $originalannualprice;
            $subs->priceData->originalannualtotalprice = $originalannualtotalprice;
            $subs->priceData->current_annual_addons_total_price = $current_annual_addons_total_price;
            $subs->priceData->originalannualdiscountper =$originalannualdiscountper;
            $subs->priceData->originalannualtotaldiscount = $originalannualtotaldiscount;
            $subs->priceData->originalannualtotaldiscountprice = $originalannualtotaldiscountprice;
    
            Subscriptions::where('id', $subs->subscriptionsid)->update(['daily_charge' => 1]);
            $trackCharges = ChargeJobLists::create([
                'charge_date' => Carbon::now(),
                'total_to_charge' => $this->total_to_charge,
                'JobType' => 'Subscription Recharge Nicepay',
                'subscriptionId' => $subs->subscriptionsid,
                'email' => $subs->email,
                'CountryId' => $subs->country->id,
                'status' => 'Sent for Charge',
                'function' => 'start, check price'
            ]);
            $subs->chargejoblistId = $trackCharges->id;
            return $this->PerformCharge($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Check plan price error: ' . $e->getMessage());
        }
    }

    // Function: Perform charge
    public function PerformCharge($subs)
    {
        $this->Log->info($this->Job . 'Perform charge for subscription id ' . $subs->subscriptionsid . '.');
        try {
            // Initialize data for nicepay
            $customer_id = $subs->card->customerId;
            $currency = $subs->country->currencyCode;
            if ($subs->promotionallow == 1) {
                // Promotion
                $amount = $subs->priceData->totaldiscountprice;
            } else {
                $amount = $subs->priceData->totalprice;
            }
            $this->updateChecking($subs->checkid, 1, 'n');
            if ($amount != 0) {
                // Perform normal charge if amount is more than 0
                try {
                    $charge = $this->nicepayController->nicepayPayment($subs->user->fullname, $subs->user->email, $customer_id, $amount, $subs->subscriptionsid);
                    $subs->getnicepay_imp = $charge["data"]["response"]["imp_uid"];
                    $subs->getnicepay_mid = $charge["data"]["response"]["merchant_uid"];
                    $subs->branchNameNicepay = $charge["data"]["response"]["card_name"];
                    $subs->chargecurrenyNicepay = $charge["data"]["response"]["currency"];
                    $subs->status1 = "nofree";
                } catch (Exception $ex) {
                    $charge = (object) array();
                    $charge->status = "payment_failed";
                    $charge->message = $ex->getMessage();
                    $charge->id = null;
                    $charge->currency = null;
                }
            } else {
                // If amount is 0, skip the nicepay and continue with status 'free'
                $charge = (object) array();
                $subs->status1 = "free";
                $charge->id = null;
                $charge->currency = null;
                $subs->getnicepay_imp = "zero price";
                $subs->getnicepay_mid = "zero price";
                $subs->branchNameNicepay = "-";
                $subs->chargecurrenyNicepay = "-";
            }

            // Insert charge object into a variable
            $subs->charge = $charge;

            return $this->UpdateSubscription($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Perform charge error: ' . $e->getMessage());
        }
    }

    // Function: Update subscription
    public function UpdateSubscription($subs)
    {
        $this->Log->info($this->Job . 'Update subscription for subscription id ' . $subs->subscriptionsid . '.');
        try {
            // Get payment status and charge duration
            $this->updateChecking($subs->checkid, 2, 'n');
            if ($subs->status1 == 'free') {
                $payment_status = $subs->status1;
            } else {
                if (is_array($subs->charge)) {
                    $payment_status = $subs->charge['status'];
                } else {
                    $payment_status = $subs->charge->status;
                }
            }

            $charge_duration = $subs->plan_sku->isAnnual == 1 ? 12 : $subs->plan_sku->duration;

            // If payment successful or no need to pay anything, do the following
            if ($payment_status == 'success' || $payment_status == 'free') {
                // Initialize data
                $CampaignReactiveHistories = CampaignReactiveHistories::select('*')
                    ->where('subscriptionId', $subs->subscriptionsid)
                    ->where('email', $subs->user->email)
                    ->where('isChargeSuccess', 0)
                    ->first();

                if (isset($CampaignReactiveHistories)) {

                    $this->utm_source = $CampaignReactiveHistories->source;
                    $this->utm_medium = $CampaignReactiveHistories->medium;
                    $this->utm_campaign = $CampaignReactiveHistories->campaign;
                    $this->utm_content = $CampaignReactiveHistories->content;
                    $this->utm_term = $CampaignReactiveHistories->term;
                    $this->is_from_campaign = 1;
                    CampaignReactiveHistories::where('id', $CampaignReactiveHistories->id)->update(
                        [
                            'isChargeSuccess' => 1
                        ]
                    );
                }

                $next_charge_date = Carbon::now()->addMonths($charge_duration);
                $current_charge_number = $subs->currentChargeNumber + 1;
                $current_cycle = $subs->currentCycle + 1;
                $recharge_date = null;
                $total_recharge = 0;
                $unrealized_customer = 0;
                $has_recharge_flag = null;
                $temp_discount_percent = 0;


                $update_subs_discountPercent = $subs->discountPercent;
                $update_subs_promoCode = $subs->promoCode;
                $update_subs_promotionId = $subs->promotionId;

                if ($subs->promotionType == 'Instant' || $subs->promotionType == 'Upcoming') {
                    if ($subs->promotionallow == 1 ||  $subs->promotionusebefore == 1) {
                    $update_subs_discountPercent = 0;
                    $update_subs_promoCode = null;
                    $update_subs_promotionId = null;
                    }
                }

                // Update pricePerCharge for Subscriptions for next billing
                $current_charge = $subs->pricePerCharge;
                $next_billing_charge = 0;
                $next_billing_addons_total_price = 0;
                $current_addons_total_price = 0;

                if ($subs->plan_sku->isAnnual == 1) {
                    if ($this->is_from_campaign == 1) {
                        try {
                            $aftershavecrem = config('global.' . config('app.appType') . '.AfterShaveCream');
                            $current_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                                ->join('products', 'products.id', 'productcountries.ProductId')
                                ->where('subscriptionId', $subs->subscriptionsid)
                                ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                    $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle);
                                })
                                ->where('products.sku', '!=', $aftershavecrem)
                                ->select('subscriptions_productaddon.*', 'productcountries.sellPrice', 'products.sku')
                                ->sum('productcountries.sellPrice');

                            $next_billing_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                                ->join('products', 'products.id', 'productcountries.ProductId')
                                ->where('subscriptionId', $subs->subscriptionsid)
                                ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                    $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle + 1);
                                })
                                ->where('products.sku', '!=', $aftershavecrem)
                                ->select('subscriptions_productaddon.*', 'productcountries.sellPrice', 'products.sku')
                                ->sum('productcountries.sellPrice');


                            if ($next_billing_addons_total_price == 0 && $current_addons_total_price != 0) {
                                $next_billing_charge = (float) $current_charge - (float) $current_addons_total_price;
                            } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price == 0) {
                                $next_billing_charge = (float) $current_charge + (float) $next_billing_addons_total_price;
                            } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price != 0) {
                                $next_billing_charge = ((float) $current_charge - (float) $current_addons_total_price) + ((float) $next_billing_addons_total_price);
                            } else if ($next_billing_addons_total_price == 0 && $current_addons_total_price == 0) {
                                $next_billing_charge = (float) $subs->pricePerCharge;
                            } else {
                                $next_billing_charge = (float) $subs->pricePerCharge;
                            }
                        } catch (Exception $ex) {
                            $this->Log->info($this->Job . 'Update subscription pricePerCharge failed: ' . $ex->getMessage());
                        }
                    } else {
                        if($subs->isAnnualDiscountIncludeAddon == "1"){
                                      
                            $noriginalannualprice = $subs->plan->price;
                            $noriginalannualdiscountper = $subs->plan->discountPercent;
                         //Initialize parameters to get product country details
                        $ncheckproductAnnual = array(
                            "appType" => 'job',
                            "isOnline" => $subs->isOnline,
                            "isOffline" => $subs->isOffline,
                            "product_country_ids" => $this->planHelper->getAllAvailableAddOnProductsByCycle($subs->subscriptionsid, $subs->currentCycle + 2, $subs->country->id, $subs->user->defaultLanguage),
                        );
                        // Get all order details
                        $ncurrent_annual_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                        ->where('subscriptionId', $subs->subscriptionsid)
                        ->whereIn('productcountries.id', $ncheckproductAnnual["product_country_ids"]) // product type id
                        ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
                        ->sum(DB::raw('productcountries.sellPrice * subscriptions_productaddon.qty'));
                        $noriginalannualtotalprice = $noriginalannualprice + $ncurrent_annual_addons_total_price;
                        $noriginalannualtotaldiscount =  (($noriginalannualtotalprice * $noriginalannualdiscountper) / 100);
                        $noriginalannualtotaldiscountprice =  $noriginalannualtotalprice - $noriginalannualtotaldiscount;
                        
            
                                        
                                        $next_billing_charge = $noriginalannualtotaldiscountprice;
                                    }else{
                                    $next_billing_charge = $current_charge;
                                    }
                    }
                } else {
                    // Calculate Addons Total for next billing
                    try {
                        $current_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                            ->where('subscriptionId', $subs->subscriptionsid)
                            ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle);
                            })
                            ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
                            ->sum('productcountries.sellPrice');

                        $next_billing_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                            ->where('subscriptionId', $subs->subscriptionsid)
                            ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle + 1);
                            })
                            ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
                            ->sum('productcountries.sellPrice');


                        if ($next_billing_addons_total_price == 0 && $current_addons_total_price != 0) {
                            $next_billing_charge = (int) $current_charge - (int) $current_addons_total_price;
                        } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price == 0) {
                            $next_billing_charge = (int) $current_charge + (int) $next_billing_addons_total_price;
                        } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price != 0) {
                            $next_billing_charge = ((int) $current_charge - (int) $current_addons_total_price) + ((int) $next_billing_addons_total_price);
                        } else if ($next_billing_addons_total_price == 0 && $current_addons_total_price == 0) {
                            $next_billing_charge = (int) $subs->pricePerCharge;
                        } else {
                            $next_billing_charge = (int) $subs->pricePerCharge;
                        }
                    } catch (Exception $ex) {
                        $this->Log->info($this->Job . 'Update subscription pricePerCharge failed: ' . $ex->getMessage());
                    }
                }

                // Update subscription status to processing
                Subscriptions::where('id', $subs->subscriptionsid)
                    ->update([
                        'pricePerCharge' => (float) $next_billing_charge,
                        'price' => (float) $next_billing_charge,
                        'nextChargeDate' => $next_charge_date,
                        'currentCycle' => $current_cycle,
                        'currentChargeNumber' => $current_charge_number,
                        'recharge_date' => $recharge_date,
                        'total_recharge' => $total_recharge,
                        'unrealizedCust' => $unrealized_customer,
                        'hasRechargeFlag' => $has_recharge_flag,
                        'temp_discountPercent' => $temp_discount_percent,
                        'discountPercent' => $update_subs_discountPercent,
                        'promoCode' => $update_subs_promoCode,
                        'promotionId' => $update_subs_promotionId,
                        'status' => "Processing",
                        'daily_charge' => 3,
                        'convertTrial2Subs' => $subs->isTrial === 1 && $subs->currentCycle === 1 ? $this->GetCurrentDate() : $subs->convertTrial2Subs,
                        'UnrealizedDate' => null,
                        'OnHoldDate' => null
                    ]);

                if ((int) $current_charge != (int) $next_billing_charge) {
                    $subshistory = new SubscriptionHistories();
                    $subshistory->message = 'Next Billing Update';
                    $subshistory->detail = $subs->country->currencyDisplay . number_format($current_charge, 2, '.', '') . ' to ' . $subs->country->currencyDisplay . number_format($next_billing_charge, 2, '.', '');
                    $subshistory->subscriptionId = $subs->subscriptionsid;
                    $subshistory->save();
                }

                ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                    'status' => 'Successfully Charged',
                    'function'=> 'update subscription (success charge)'
                ]);

                // Send Subscription Renewal EDM only for Trial Plan first billing
                if ($subs->isTrial === 1 && $subs->currentCycle === 1) {
                    $this->SendSubscriptionRenewalEDM($subs);
                }
            } else {
                $max_total_recharge = 5;

                if ($subs->total_recharge == $max_total_recharge) {
                    Subscriptions::where('id', $subs->subscriptionsid)
                        ->update([
                            'status' => "Unrealized",
                            'total_recharge' => $subs->total_recharge + 1,
                            'hasRechargeFlag' => 1,
                            'hasFlag' => 1,
                            'unrealizedCust' => 1,
                            'daily_charge' => 2,
                            'OnHoldDate' => $subs->total_recharge == 5 && $subs->OnHoldDate == null ? Carbon::now()->subDays(35) : $subs->OnHoldDate,
                            'UnrealizedDate' => Carbon::now()
                        ]);
                    ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                        'status' => 'Fail to Charge',
                        'function'=> 'update subscription (fail charge)'
                    ]);
                } else {
                    Subscriptions::where('id', $subs->subscriptionsid)
                        ->update([
                            'status' => "On Hold",
                            'recharge_date' => Carbon::now()->addDays(7)->toDateString(),
                            'total_recharge' => $subs->total_recharge + 1,
                            'hasRechargeFlag' => 1,
                            'hasFlag' => 1,
                            'daily_charge' => 2,
                            'OnHoldDate' => $subs->total_recharge == 1 && $subs->OnHoldDate == null ? Carbon::now()->subDays(7) : ($subs->total_recharge == 2 && $subs->OnHoldDate == null ? Carbon::now()->subDays(14) : ($subs->total_recharge == 3 && $subs->OnHoldDate == null ? Carbon::now()->subDays(21) : ($subs->total_recharge == 4 && $subs->OnHoldDate == null ? Carbon::now()->subDays(28) : $subs->OnHoldDate)))
                        ]);
                    ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                        'status' => 'Fail to Charge'
                    ]);
                }
            }

            // Save latest subscription date into post payment object
            $subs->post_payment->subscription = (object) array(Subscriptions::where('id', $subs->subscriptionsid)->first())["0"];

            return $this->CreateSubscriptionHistory($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Update subscription error: ' . $e->getMessage());
        }
    }

    // Function: Create subscription history
    public function CreateSubscriptionHistory($subs)
    {
        $this->Log->info($this->Job . 'Create subscription history for subscription id ' . $subs->subscriptionsid . '.');
        try {
            // Create subscription history
            $subshistory = new SubscriptionHistories();
            $subshistory->message = $subs->post_payment->subscription->status;
            $subshistory->subscriptionId = $subs->subscriptionsid;
            if ($subs->post_payment->subscription->status == "On Hold") {
                $subshistory->message = 'Recharge Attempt #' . $subs->total_recharge;
            } else {
                $subshistory->message = 'On Hold (Unrealized Customer)';
            }
            $subshistory->save();
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'update subscription history'
            ]);
            // If subscription status is processing, continue to create order
            // Else, continue to send payment failure edm
            if ($subs->post_payment->subscription->status == "Processing") {
                return $this->CreateOrder($subs);
            } else {
                return $this->SendPaymentFailureEDM($subs);
            }
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create subscription history error: ' . $e->getMessage());
        }
    }

    // Function: Create order
    public function CreateOrder($subs)
    {
        $this->Log->info($this->Job . 'Create order for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Create order
            $orders = new Orders();
            if ($subs->promotionallow == 1) {
                $orders->promoCode = $subs->promoCode;
                $orders->PromotionId = $subs->promotionId;

                if ($subs->promotionId) {
                    $PromotionCodes = Promotions::where('id', $subs->promotionId)
                        ->first();
     
                    $PromotionCodes->appliedTo = str_replace("{", "", $PromotionCodes->appliedTo);
                    $PromotionCodes->appliedTo = str_replace("}", "", $PromotionCodes->appliedTo);
                    $splitapplyto = $PromotionCodes->appliedTo;
                    // $peruse = $PromotionCodes->timePerUser;
                    if (strpos($splitapplyto, ',') !== false) {
                        $splitapplyto = explode(",", $PromotionCodes->appliedTo);
                    }
                    // $promoapplyupdate = ",".$userid.":1";
                    $promoapplyupdate = "";
                    $combineapply = "{";
                    $count = 1;
                    if (is_array($splitapplyto)) {
                        foreach ($splitapplyto as $at) {
                            if (strpos($at, ':') !== false) {
                                $splitqty = explode(":", $at);
                                if ($subs->user->id == $splitqty[0]) {
                                    if($subs->promotionType == 'Recurring'){
                                        $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                                    }else{
                                        $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1]);
                                    }
                                  
                                    if ($count == 1) {
                                        $combineapply = $combineapply . $promoapplyupdate;
                                        $count++;
                                    } else {
                                        $combineapply = $combineapply . "," . $promoapplyupdate;
                                        $count++;
                                    }
                                } else {
                                    if ($count == 1) {
                                        $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                                        $count++;
                                    } else {
                                        $combineapply = $combineapply . "," . $splitqty[0] . ":" . $splitqty[1];
                                        $count++;
                                    }
                                }
                            }
                        }
                    } else {
                        if (strpos($splitapplyto, ':') !== false) {
                            $splitqty = explode(":", $splitapplyto);
                            if ($subs->user->id  == $splitqty[0]) {
                                if($subs->promotionType == 'Recurring'){
                                $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                                }else{
                                    $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1]);
                                }
                                $combineapply = $combineapply . $promoapplyupdate;
                            } else {
                                $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                            }
                        }
                    }
                    if ($promoapplyupdate) {
                        $combineapply = $combineapply . "}";
                    } else {
                        if ($splitapplyto) {
                            $combineapply = $combineapply . "," . $subs->user->id  . ":1" . "}";
                        } else {
                            $combineapply = $combineapply . $subs->user->id  . ":1" . "}";
                        }
                    }

                    Promotions::where('id', $subs->promotionId)
                        ->update([
                            'appliedTo' => $combineapply,
                        ]);
                }
                
            } else {
                $orders->promoCode = null;
                $orders->PromotionId = null;
            };
            $orders->SSN = null;
            $orders->email = $subs->user->email;
            $orders->phone = $subs->user->phone;
            $orders->fullName = $subs->user->fullname;
            $orders->DeliveryAddressId = isset($subs->deliveryAddress->id) ? $subs->deliveryAddress->id : (isset($subs->DeliveryAddressId)? $subs->DeliveryAddressId : 0);
            $orders->BillingAddressId = isset($subs->billingAddress->id) ? $subs->billingAddress->id : (isset($subs->BillingAddressId)? $subs->BillingAddressId : 0 );
            $orders->taxRate = $subs->tax->taxRate;
            $orders->taxName = $subs->tax->taxName;
            $orders->CountryId = $subs->country->id;
            $orders->subscriptionIds = $subs->subscriptionsid;
            $orders->source = $this->utm_source;
            $orders->medium = $this->utm_medium;
            $orders->campaign = $this->utm_campaign;
            $orders->term = $this->utm_term;
            $orders->content = $this->utm_content;
            $orders->status = "Payment Received";
            $orders->payment_status = "Charged";
            $orders->paymentType = "nicepay";
            $orders->isSubsequentOrder = 1;

            if ($subs->country->id == "7") {
                $orders->carrierAgent = "courex";
            } else {
                $orders->carrierAgent = null;
            }

            $orders->UserId = $subs->user->id;

            if (isset($subs->SellerUserId) && $subs->SellerUserId !== null) {
                $orders->SellerUserId = $subs->SellerUserId;
                $orders->channelType = $subs->channelType;
                $orders->eventLocationCode = $subs->eventLocationCode;
                $orders->MarketingOfficeId = $subs->MarketingOfficeId;
                // $seller_data = SellerUser::where($subs->SellerUserId)->first();
                // if ($seller_data) {
                //     $mo_data = MarketingOffice::where($seller_data->MarketingOfficeId)->first();
                //     if ($mo_data) {
                //         $orders->MarketingOfficeId = $mo_data->id;
                //     }
                // }
            } else {
                $orders->SellerUserId = null;
                $orders->channelType = null;
                $orders->eventLocationCode = null;
                $orders->MarketingOfficeId = null;
            }

            // $orders->pymt_intent = $subs->charge->id;
            $orders->imp_uid = $subs->getnicepay_imp;
            $orders->merchant_uid = $subs->getnicepay_mid;
            $orders->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->order = $orders;
            // Add order to queue service
            $_redis_order_data = (object) array(
                'OrderId' => $subs->post_payment->order->id,
                'CountryId' => $subs->country->id,
                'CountryCode' => strtolower($subs->country->codeIso),
            );
            $this->orderHelper->addTaskToOrderQueueService($_redis_order_data);

            $charge_duration = $subs->plan_sku->isAnnual == 1 ? 12 : $subs->plan_sku->duration;

            $nextDeliverDate = Carbon::now()->addMonths($charge_duration);
            $current_deliver_number = $subs->currentDeliverNumber + 1;
            // Update current order time and id in subscription
            Subscriptions::where('id', $subs->subscriptionsid)
                ->update([
                    'currentDeliverNumber' => $current_deliver_number,
                    'nextDeliverDate' => $nextDeliverDate,
                    'startDeliverDate' => Carbon::now(),
                    'currentOrderTime' => Carbon::now(),
                    'currentOrderId' => $subs->post_payment->order->id,
                ]);
                ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                    'function'=> 'create order'
                ]);
            return $this->CreateOrderHistory($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order error: ' . $e->getMessage());
        }
    }

    // Function: Create order history
    public function CreateOrderHistory($subs)
    {
        $this->Log->info($this->Job . 'Create order history for subscription id ' . $subs->subscriptionsid . '.');
        try {
            // Create order history
            $ordershistories = new OrderHistories();
            $ordershistories->message = 'Payment Received';
            $ordershistories->OrderId = $subs->post_payment->order->id;
            $ordershistories->save();
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'create order history'
            ]);
            return $this->CreateOrderDetails($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order history error: ' . $e->getMessage());
        }
    }

    // Function: Create order details
    public function CreateOrderDetails($subs)
    {
        $this->Log->info($this->Job . 'Create order details for subscription id ' . $subs->subscriptionsid . '.');
        try {
            //Initialize parameters to get product country details
            $subs->productCountryDetailsParams = array(
                "appType" => 'job',
                "isOnline" => $subs->isOnline,
                "isOffline" => $subs->isOffline,
                "product_country_ids" => $this->planHelper->getAllAvailableProductsByCycle($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage),
            );

            // Get all order details
            $orderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $subs->productCountryDetailsParams));

            // Get product country ids for addons
            $addonsProductCountryIds = $this->planHelper->getAvailableAddonProductsByCycle($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage);
            $productQuantities = $this->planHelper->getAllAvailableProductsByCycleV2($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage);

            // Create order details
            foreach ($orderDetailList as $product) {
                $orderdetails = new OrderDetails();
                $orderdetails->qty = $productQuantities[$product->ProductCountryId];
                $orderdetails->price = $product->sellPrice;
                $orderdetails->currency = $subs->country->currencyDisplay;
                $orderdetails->startDeliverDate = null;
                $orderdetails->created_at = Carbon::now();
                $orderdetails->updated_at = Carbon::now();
                $orderdetails->OrderId = $subs->post_payment->order->id;
                $orderdetails->ProductCountryId = $product->ProductCountryId;
                $orderdetails->isAddon = in_array($product->ProductCountryId, $addonsProductCountryIds) ? 1 : 0;
                // Promotion free exist product
                if ($subs->promotionallow == 1) {
                    if ($subs->promo_freeexistresultproduct) {
                        if (in_array($product->ProductCountryId, $subs->promo_freeexistresultproduct)) {
                            $orderdetails->isFreeProduct = 1;
                        }
                    }
                }
                $orderdetails->save();
            }

            // Promotion Free Product
            if ($subs->promotionallow == 1) {
                $productseperate = [];
                if ($subs->freeProductCountryIds) {
                    $subs->freeProductCountryIds = str_replace("[", "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace("]", "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace('"', "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace('"', "", $subs->freeProductCountryIds);

                    $splitproduct = $subs->freeProductCountryIds;
                    if (strpos($splitproduct, ',') !== false) {
                        $splitproduct = explode(",", $subs->freeProductCountryIds);
                    }
                    if (is_array($splitproduct)) {
                        foreach ($splitproduct as $product) {
                            array_push($productseperate, $product);
                        }
                    } else {
                        array_push($productseperate, $splitproduct);
                    }

                    // Prepare parameters for trial plan order details
                    $productcountyidarray = array(
                        "appType" => "job",
                        "isOnline" => 1,
                        "isOffline" => 1,
                        "product_country_ids" => $productseperate,
                    );
                    $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $productcountyidarray));

                    foreach ($product as $pci) {
                        $orderdetails = new OrderDetails();
                        $orderdetails->qty = 1;
                        $orderdetails->price = $pci->sellPrice;
                        $orderdetails->currency = $subs->country->currencyDisplay;
                        $orderdetails->startDeliverDate = null;
                        $orderdetails->created_at = Carbon::now();
                        $orderdetails->updated_at = Carbon::now();
                        $orderdetails->OrderId = $subs->post_payment->order->id;
                        $orderdetails->ProductCountryId = $pci->ProductCountryId;
                        $orderdetails->isFreeProduct = 1;
                        $orderdetails->isAddon = 0;
                        $orderdetails->save();
                    }
                }
            }
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'create order detail'
            ]);
            return $this->UpdateReferral($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order history error: ' . $e->getMessage());
        }
    }

    // Function: Update Referral (optional)
    public function UpdateReferral($subs)
    {
        $this->Log->info($this->Job . 'UpdateReferral start');
        try {
            $this->updateChecking($subs->checkid, 4, 'n');
            if ($subs && (is_array($subs->charge) ? $subs->charge['status'] === 'succeeded' : $subs->charge->status === 'succeeded')) {
                $this->Log->info('succeeded');
                $referee_info = User::where('id', $subs->user->id)->first();
                if ($referee_info) {
                    $referrer_info = User::where('id', $referee_info->referralId)->first();
                    $referrer_country = Countries::where('id', $referrer_info->CountryId)->first();
                    // start checking if referrer_id && referee_id exists in referral table
                    $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $referee_info->id)->whereNull('rewardId')->get();
                    if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                        $rewardscurrency = RewardsCurrency::where('CountryId', $referrer_info->CountryId)->first();
                        if ($rewardscurrency) {
                            $currentOrder = Orders::where('id', $subs->post_payment->order->id)->first();
                            if ($currentOrder) {
                                $_insert_into_rewardsin = RewardsIn::create([
                                    "CountryId" => $referrer_info->CountryId,
                                    "UserId" => $referrer_info->id,
                                    "value" => $rewardscurrency->value,
                                    "rewardscurrencyId" => $rewardscurrency->id,
                                    "OrderId" => $currentOrder->id,
                                ]);
                                Referral::where('referralId', $referrer_info->id)->update(["firstPurchase" => 1, "rewardId" => $_insert_into_rewardsin->id, "status" => "Active"]);
                                //send email to referrer for active credits
                                // $data = (Object) array(
                                //     'referrer' => (Object) array(
                                //         'id' => $referrer_info->id,
                                //         'first_name' => $referrer_info->firstName.$referrer_info->lastName,
                                //         'last_name' => null,
                                //         'country_id' => $referrer_info->CountryId,
                                //         'amount_credited' => $rewardscurrency->value,
                                //     ),
                                //     'referee' => (Object) array(
                                //         'id' => $referee_info->id,
                                //         'first_name' => $referee_info->firstName.$referee_info->lastName,
                                //         'last_name' => null,
                                //         'country_id' => $referee_info->CountryId,
                                //     ),
                                // );

                                // $this->referralController->sendReferralActiveCredits($data);
                                $emailData['title'] = 'referral-active-credits';

                                $emailData['moduleData'] = array(
                                    'email' => $referrer_info->email,
                                    'refuserId' =>  $referee_info->id,
                                    'countryId' => $referrer_info->CountryId,
                                    'isPlan' => 1,
                                );

                                $currentCountry = json_encode($referrer_country);
                                $currentLocale = $referrer_info->defaultLanguage;
                                $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                                // hide email redis
                                $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                            }
                        }
                    }
                }
            }
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'check referral'
            ]);
            return $this->CreateReceipt($subs);
        } catch (Exception $ex) {
            $this->updateChecking($subs->checkid, '0',  $ex->getMessage());
            $this->Log->error($this->Job . 'UpdateReferral for referrer id: ' . $referrer_info->id . ' => ' . $ex);
        }
    }

    // Function: Create receipt
    public function CreateReceipt($subs)
    {
        $this->Log->info($this->Job . 'Create receipt for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Create receipt
            $this->updateChecking($subs->checkid, 5, 'n');
            $receipts = new Receipts();
            $receipts->totalPrice = $subs->priceData->totalprice;
            $receipts->subTotalPrice = $subs->priceData->subtotal;
            $receipts->originPrice = $subs->priceData->subtotal;
            $receipts->discountAmount = 0;
            if($subs->isAnnualDiscountIncludeAddon == "1"){
                $receipts->totalPrice = $subs->priceData->originalannualtotaldiscountprice;
                $receipts->subTotalPrice = $subs->priceData->originalannualtotaldiscountprice;
                $receipts->originPrice = $subs->priceData->originalannualtotalprice;
                $receipts->discountAmount =  $subs->priceData->originalannualtotaldiscount;
                
                if ($subs->promotionallow == 1) {
                    $receipts->totalPrice = $subs->priceData->totaldiscountprice;
                    $receipts->subTotalPrice = $subs->priceData->totaldiscountprice;
                    $receipts->discountAmount = $subs->priceData->totaldiscount;
                }

            }else{
            // Apply promotion
            if ($subs->promotionallow == 1) {
                $receipts->totalPrice = $subs->priceData->totaldiscountprice;
                $receipts->subTotalPrice = $subs->priceData->totaldiscountprice;
                $receipts->discountAmount = $subs->priceData->totaldiscount;
            }
            if (isset($subs->plan) && $subs->plan !== null && $subs->plan->discountPercent > 0) {
                $receipts->totalPrice = $subs->priceData->totalprice;
                $receipts->originPrice = ($subs->priceData->totalprice) +  $subs->plan->discountAmount;
                if ($subs->promotionallow == 0) {
                    $receipts->discountAmount = $receipts->discountAmount + $subs->plan->discountAmount;
                } else {
                    $receipts->discountAmount = $receipts->discountAmount + $subs->plan->discountAmount;
                }
            }
        }

            $receipts->currency = $subs->country->currencyDisplay;
            $receipts->taxAmount = $subs->priceData->taxAmount;
            $receipts->shippingFee = $subs->priceData->shippingfee;
            $receipts->OrderId = $subs->post_payment->order->id;
            $receipts->chargeFee = 0;
            $receipts->chargeCurrency = 'krw';
            $receipts->branchName = $subs->card->branchName;
            $receipts->chargeId = $subs->card->customerId;
            $receipts->last4 = $subs->card->cardNumber;
            $receipts->expiredYear = $subs->card->expiredYear;
            $receipts->expiredMonth = $subs->card->expiredMonth;
            $receipts->SubscriptionId = $subs->subscriptionsid;
            $receipts->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->receipt = $receipts;
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'create receipts'
            ]);
            return $this->SendReceiptEDM($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create receipt error: ' . $e->getMessage());
        }
    }

    // Function: Send receipt edm
    public function SendReceiptEDM($subs)
    {
        $this->updateChecking($subs->checkid, 3, 'n');
        $this->Log->info($this->Job . 'Send receipt EDM for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'receipt-order';
            $emailData['moduleData'] = (object) array(
                'email' => $subs->email,
                'subscriptionId' => $subs->post_payment->subscription->id,
                'orderId' => $subs->post_payment->order->id,
                'receiptId' => $subs->post_payment->receipt->id,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);

            EmailQueueService::addHash($subs->user->id, 'receipt_order_confirmation', $emailData);
            $this->Log->info($this->Job . 'Dispatched receipt EDM for subscription id ' . $subs->subscriptionsid . '.');
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'send payment success edm'
            ]);
            return $this->CreateTaxInvoice($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Send receipt edm error: ' . $e->getMessage());
        }
    }

    // Function: Create tax invoice
    public function CreateTaxInvoice($subs)
    {
        $this->Log->info($this->Job . 'Create tax invoice for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // $this->taxInvoiceHelper->__generateHTML($subs->post_payment->order, $subs->post_payment->receipt, $subs->country);
            return $this->CreateSalesReport($subs);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create tax invoice error: ' . $e->getMessage());
        }
    }

    // Function: Create sales report
    public function CreateSalesReport($subs)
    {
        $this->Log->info($this->Job . 'Create sales report for subscription id ' . $subs->subscriptionsid . '.');
        try {
            $_redis_sales_report_data = ['id' =>  $subs->post_payment->order->id, 'bulkOrder' => false];
            SaleReportQueueService::addHash($subs->post_payment->order->id, $_redis_sales_report_data);
            $_redis_tax_invoice_data = ['id' => $subs->post_payment->order->id, 'CountryId' => $subs->country->id];
            TaxInvoiceQueueService::addHash($subs->post_payment->order->id, $_redis_tax_invoice_data);
            $this->deleteChecking($subs->checkid);
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'end'
            ]);
            // Create sales report
            // $salesReport = new SalesReport();
            //$salesReport->badgeId = "";
            //$salesReport->agentName = "";
            //$salesReport->channelType = "";
            //$salesReport->eventLocationCode = "";
            //$salesReport->states = "";
            //$salesReport->deliveryId = "";
            //$salesReport->carrierAgent = "";
            //$salesReport->customerName = "";
            // $salesReport->deliveryAddress = $subs->deliveryAddress->state;
            // $salesReport->deliveryContact = $subs->deliveryAddress->contactNumber;
            // $salesReport->billingAddress = $subs->billingAddress->state;
            // $salesReport->billingContact = $subs->billingAddress->contactNumber;
            // $salesReport->email = $subs->user->email;
            //$salesReport->productCategory = "";
            // $salesReport->sku = $subs->plan_sku->sku;
            // $salesReport->qty = 1;
            // $salesReport->productName = "";
            // $salesReport->currency = $subs->country->currencyCode;
            // $salesReport->paymentType = "card";
            // $salesReport->region = "";
            //$salesReport->category = "";
            // $salesReport->saleDate = $this->GetCurrentDate();
            // $salesReport->orderId = $subs->post_payment->order->id;
            //$salesReport->bulkOrderId = "";
            //$salesReport->promoCode = "";
            // Apply promotion
            // if ($subs->promotionallow == 1) {
            //     $salesReport->totalPrice = number_format($subs->priceData->totaldiscountprice, 2, '.', '');
            //     $salesReport->subTotalPrice = number_format($subs->priceData->subtotal, 2, '.', '');
            //     $salesReport->discountAmount = number_format($subs->priceData->totaldiscount, 2, '.', '');
            // } else {
            //     $salesReport->totalPrice = number_format($subs->priceData->totalprice, 2, '.', '');
            //     $salesReport->subTotalPrice = number_format($subs->priceData->subtotal, 2, '.', '');
            //     $salesReport->discountAmount = 0;
            // }
            // $salesReport->shippingFee = $subs->priceData->shippingfee;
            // $salesReport->taxAmount = $subs->priceData->taxAmount;
            //$salesReport->grandTotalPrice = "";
            //$salesReport->source = "";
            // $salesReport->medium = "";
            // $salesReport->campaign = "";
            // $salesReport->term = "";
            // $salesReport->content = "";
            // $salesReport->CountryId = $subs->country->id;
            //$salesReport->taxInvoiceNo = "";
            //$salesReport->MarketingOfficeId = "";
            //$salesReport->moCode = "";
            //$salesReport->taxRate = "";
            //$salesReport->isDirectTrial = "";
            // $salesReport->created_at = $this->GetCurrentDateTime();
            // $salesReport->updated_at = $this->GetCurrentDateTime();
            // $salesReport->save();
            // $this->Log->info($salesReport);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Create report error: ' . $e->getMessage());
        }
    }

    // Function: Send payment failure edm
    public function SendPaymentFailureEDM($subs)
    {
        $this->Log->info($this->Job . 'Send payment failure EDM for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'receipt-payment-failure-subscription';
            $emailData['moduleData'] = (object) array(
                'email' => $subs->email,
                'subscriptionId' => $subs->subscriptionsid,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'isPlan' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            EmailQueueService::addHash($subs->user->id, 'receipt_subscription_payment_failure', $emailData);
            ChargeJobLists::where('id', $subs->chargejoblistId)->update([
                'function'=> 'send payment fail edm'
            ]);
        } catch (Exception $e) {
            $this->updateChecking($subs->checkid, '0',  $e->getMessage());
            $this->onSubscriptionRechargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Send payment failure edm error: ' . $e->getMessage());
        }
    }

    // Function: Send subscription renewal EDM
    public function SendSubscriptionRenewalEDM($subs)
    {

        $this->Log->info($this->Job . 'Send subscription renewal EDM for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'subscription-renewal';
            $emailData['moduleData'] = (object) array(
                'email' => $subs->email,
                'subscriptionId' => $subs->subscriptionsid,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'type' => 1,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            EmailQueueService::addHash($subs->user->id, 'subscription_renewal', $emailData);
        } catch (Exception $e) {
            $this->onSubscriptionChargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Send subscription renewal edm error: ' . $e->getMessage());
        }
    }

    /*** Additional helper functions ***/
    public function GetCurrentDateTime()
    {
        return Carbon::now();
    }

    public function GetCurrentDate()
    {
        return Carbon::now()->toDateString();
    }

    public function onSubscriptionRechargeError($message)
    {
        $this->Log->error($this->Job . $message);
    }

    public function createChecking($subscription)
    {
        // $this->Log->info($this->Job . 'createChecking for subscription start.'.$subscription->subscriptionsid);
        //Get user info
        if ($subscription) {
            if ($subscription->user) {

                $chargejobcheck = new ChargeJobChecks();
                $chargejobcheck->subscriptionIds = $subscription->subscriptionsid;
                $chargejobcheck->UserId = $subscription->user->id;
                $chargejobcheck->email = $subscription->email;
                $chargejobcheck->CountryId = $subscription->country->id;
                $chargejobcheck->JobType = "Subscription Recharge Nicepay";
                $chargejobcheck->save();


                return 0;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function updateChecking($checkid, $type, $error)
    {
        // $this->Log->info($this->Job . 'update ('.$type.')-'.$checkid.' start');
        //Get user info
        if ($checkid && $type) {
            if ($type == 1) {
                return ChargeJobChecks::where('id', $checkid)->update(['ReferralDiscount' => 1]);
            } else if ($type == 2) {
                return ChargeJobChecks::where('id', $checkid)->update(['ChargeFunction' => 1]);
            } else if ($type == 3) {
                return ChargeJobChecks::where('id', $checkid)->update(['CreateReceiptSub' => 1]);
            } else if ($type == 4) {
                return ChargeJobChecks::where('id', $checkid)->update(['CreateOrder' => 1]);
            } else if ($type == 5) {
                return ChargeJobChecks::where('id', $checkid)->update(['UpdateReceiptSub' => 1]);
            } else if ($type == '0') {
                if ($error) {
                    return ChargeJobChecks::where('id', $checkid)->update(['ErrorOccur' => $error]);
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function deleteChecking($checkid)
    {
        return  ChargeJobChecks::where('id', $checkid)->where('ReferralDiscount', 1)->where('ChargeFunction', 1)->where('CreateReceiptSub', 1)->where('CreateOrder', 1)->where('UpdateReceiptSub', 1)->take(1)->delete();
    }
    /*** Additional helper functions ***/
}
