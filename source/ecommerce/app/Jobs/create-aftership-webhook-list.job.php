<?php

namespace App\Jobs;

use App\Helpers\LaravelHelper;
use App\Models\BulkOrders\BulkOrders;
use Illuminate\Console\Command;
use Exception;
use App\Models\Orders\DeliveryTracking;
use App\Models\Orders\Orders;
use App\Queue\AftershipWebhookQueueService;
use App\Services\OrderService;
use DB;

class CreateAftershipWebhookList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-aftership-webhooks-list:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aftership - Save Webhook API DATA from Redis to Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->orderService = new OrderService();
    }

    public function handle()
    {
        $this->addWebhookToDB();
    }

    public function addWebhookToDB()
    {
        $redis_data = AftershipWebhookQueueService::getAllHash();
        $keys = [];
        if (count($redis_data) > 100) {
            $keys = array_slice($redis_data, 0, 100);
        } else {
            $keys = $redis_data;
        }

        foreach ($keys as $key => $value) {
            $data = json_decode($value);
            $params = (object) array();
            $params->msg = $data->api_data;
            // find order fit with this hook
            // check order type
            $this->Log->info('Aftership | addWebhookToDB | add type => ' . $this->orderService->checkOrderNumberType($params->msg->order_id));
            try {
                if ($this->orderService->checkOrderNumberType($params->msg->order_id) === 'order') {
                    $this->Log->info('Aftership | addWebhookToDB ID => ' . $params->msg->order_id);
                    try {
                        $orderFilter = Orders::where('deliveryId', $params->msg->tracking_number)->whereIn('status', ['Delivering', 'Completed', 'Cancelled', 'Returned'])->select('id', 'status')->first();
                        if ($orderFilter && $orderFilter->status == 'Delivering') {
                            // save api response to database
                            DeliveryTracking::firstOrCreate([
                                'OrderId' => $orderFilter->id,
                            ], [
                                'warehouse_id' => $params->msg->order_id,
                                'status' => 'Pending',
                                'deliveryId' => $params->msg->tracking_number,
                                'api_data' => json_encode($params->msg),
                                'delivery_edm' => 0
                            ]);

                            AftershipWebhookQueueService::deleteHash($data->deliveryId);
                        } else if ($orderFilter && ($orderFilter->status == 'Completed' || $orderFilter->status == 'Returned' || $orderFilter->status == 'Cancelled')) {
                            DeliveryTracking::firstOrCreate([
                                'OrderId' => $orderFilter->id,
                            ], [
                                'warehouse_id' => $params->msg->order_id,
                                'status' => 'Captured',
                                'deliveryId' => $params->msg->tracking_number,
                                'api_data' => json_encode($params->msg),
                                'delivery_edm' => 0
                            ]);
                            AftershipWebhookQueueService::deleteHash($data->deliveryId);
                        } else {
                            // do nothing
                            DeliveryTracking::firstOrCreate([
                                'OrderId' => $orderFilter->id,
                            ], [
                                'warehouse_id' => $params->msg->order_id,
                                'status' => 'Captured',
                                'deliveryId' => $params->msg->tracking_number,
                                'api_data' => json_encode($params->msg),
                                'delivery_edm' => 0
                            ]);
                            $this->Log->info('Aftership | aftership tracking no processed : tracking ID => ' . $params->msg->tracking_number);
                        }
                    } catch (Exception $ex) {
                        AftershipWebhookQueueService::addHash($data->deliveryId, $data);
                        $this->Log->info('Aftership | addWebhookToDB | error => ' . $ex->getMessage());
                    }
                } else {
                    $this->Log->info('Aftership | addWebhookToDB | check for bulkorder ID => ' . $params->msg->order_id);
                    //bulk order
                    try {
                        $_order = BulkOrders::where('deliveryId', $params->msg->tracking_number)->whereIn('status', ['Delivering', 'Completed', 'Cancelled', 'Returned'])->select('id', 'status')->first();
                        if ($_order) {
                            if ($_order && $_order->status == 'Delivering') {
                                // save api response to database
                                DeliveryTracking::firstOrCreate([
                                    'BulkOrderId' => $_order->id,
                                ], [
                                    'warehouse_id' => $params->msg->order_id,
                                    'status' => 'Pending',
                                    'deliveryId' => $params->msg->tracking_number,
                                    'api_data' => json_encode($params->msg),
                                    'delivery_edm' => 0
                                ]);

                                AftershipWebhookQueueService::deleteHash($data->deliveryId);
                            } else if ($_order && ($_order->status == 'Completed' || $_order->status == 'Returned' || $_order->status == 'Cancelled')) {
                                DeliveryTracking::firstOrCreate([
                                    'BulkOrderId' => $_order->id,
                                ], [
                                    'warehouse_id' => $params->msg->order_id,
                                    'status' => 'Captured',
                                    'deliveryId' => $params->msg->tracking_number,
                                    'api_data' => json_encode($params->msg),
                                    'delivery_edm' => 0
                                ]);

                                AftershipWebhookQueueService::deleteHash($data->deliveryId);
                            } else {
                                // do nothing
                                DeliveryTracking::firstOrCreate([
                                    'BulkOrderId' => $_order->id,
                                ], [
                                    'warehouse_id' => $params->msg->order_id,
                                    'status' => 'Captured',
                                    'deliveryId' => $params->msg->tracking_number,
                                    'api_data' => json_encode($params->msg),
                                    'delivery_edm' => 0
                                ]);
                                $this->Log->info('Aftership | aftership tracking no processed <bulk> : tracking ID => ' . $params->msg->tracking_number);
                            }
                        }
                    } catch (Exception $ex) {
                        AftershipWebhookQueueService::addHash($data->deliveryId, $data);
                        $this->Log->info('Aftership | addWebhookToDB | error => ' . $ex->getMessage());
                    }
                }
            } catch (Exception $ex) {
            }
        }
    }
}
