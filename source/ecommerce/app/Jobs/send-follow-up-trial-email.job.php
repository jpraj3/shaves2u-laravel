<?php

namespace App\Jobs;

use App\Helpers\UserHelper;
use Illuminate\Console\Command;
use App\Models\GeoLocation\Countries;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Ecommerce\User;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;

class SendFollowUpTrialEmailJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-follow-up-trial-email:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Follow Up email to Trial Plan Subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->userHelper = new UserHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->Log->info('SendFollowUpTrialEmailJob start');

        //
        //        $subs = User::join('subscriptions', 'subscriptions.UserId', '=', 'users.id')
        //            ->where('subscriptions.hasSendFollowUp1', false)
        //            ->where('subscriptions.isTrial', true)
        //            ->where('subscriptions.status', 'Processing')
        //            ->where('subscriptions.currentChargeNumber', '1')
        //            ->get();
        $subs = Subscriptions::where('subscriptions.hasSendFollowUp1', false)
            ->where('subscriptions.isTrial', true)
            ->where('subscriptions.status', 'Processing')
            ->where('subscriptions.currentChargeNumber', '1')
            ->get();
        foreach ($subs as $sub) {
            $userData = User::where('id', $sub->UserId)->first();
            $sub->country = Countries::where('id', $userData->CountryId)->first();
            try {
                // Getting Country & Locale Data From $subs
                $currentCountry = $sub->country;
                $currentLocale = $this->userHelper->getEmailDefaultLanguage($sub->UserId, $sub->PlanId);

                $emailData = [];
                // Passing $subs Data into $moduleData for Email
                $emailData['title'] = 'subscription-renewal';
                $emailData['moduleData'] = (object) array(
                    'email' => $sub->email,
                    'subscriptionId' => $sub->id,
                    'userId' => $sub->UserId,
                    'countryId' => $userData->CountryId,
                    'type' => 1,
                    'isPlan' => 1,
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                EmailQueueService::addHash($sub->UserId, 'subscription_renewal', $emailData);

                // Update hasSendFollowUp1 column
                Subscriptions::where('id', $sub->id)->update(['hasSendFollowUp1' => true]);
            } catch (Exception $e) {
                // Need to handle failure
                $this->onSubscriptionChargeError('SubscriptionId: ' . $subs->subscriptionsid . '. Send subscription renewal edm error: ' . $e->getMessage());
            }
        }
    }
}
