<?php

namespace App\Jobs;

use App\Helpers\LaravelHelper;
use App\Helpers\Warehouse\AftershipHelper;
use App\Helpers\Warehouse\UrbanFoxHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;
use AfterShip\Trackings;
use App\Helpers\PlanHelper;
use App\Models\Orders\DeliveryTracking;

class UpdateUrbanFoxOrderJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-urbanfox-order:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Urbanfox Warehouse - Get Order Tracking Updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->urbanfoxHelper = new UrbanFoxHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->orderService = new OrderService();
        $this->_aftership = new AftershipHelper();
        $this->planHelper = new PlanHelper();
    }

    public function handle()
    {
        $this->updateUrbanFoxOrders();
        //$this->updateAftershipTrackingsOrders();
        // $this->updateAftershipTrackingsBulkOrders();
    }

    public function updateUrbanFoxOrders()
    {
        $this->Log->info('SG | updateUrbanFoxOrders process start');
        try {
            // get all singapore orders/bulkorders
            $orders = Orders::where('status', 'Processing')->where('carrierAgent', 'courex')->get();
            if ($orders) {
                // $this->Log->info('SG | $orders => ' . json_encode($orders));
                foreach ($orders as $od) {
                    $_data = (object) array();
                    $_data->status = $od->status;
                    $_data->deliveryId = $od->deliveryId !== null ? $od->deliveryId : null;
                    $user = User::where('id', $od->UserId)->first();
                    $receipt = Receipts::where('OrderId', $od->id)->first();
                    $country = Countries::where('id', $od->CountryId)->first();
                    $shipment = DeliveryAddresses::where('id', $od->DeliveryAddressId)->first();
                    sleep(65);
                    $result_ = $this->laravelHelper->ConvertArraytoObject($this->urbanfoxHelper->getTransactionInfo($od, $country, false));
                    // $this->Log->info('SG | update result => ' . json_encode($result_));

                    // save query into a text-file
                    // $formatted_order_number = $this->orderService->formatOrderNumber($od, $country, true);
                    // $this->Log->info('getTransactionInfo from urbanfox: 232323 => ' . $formatted_order_number);
                    // $path = config('environment.warehouse_paths.SGP.local_download_folder');
                    // file_put_contents($path . '/' . 'UrbanFox_' . $formatted_order_number . '.txt', json_encode($result_), FILE_APPEND | LOCK_EX);

                    if (!$result_) {
                        return;
                    } else {
                        foreach ($result_ as $result) {
                            if (isset($result->getOrder)) {
                                if ($result->getOrder->status === 'DELIVERED') {
                                    $_data->status = 'Completed';
                                } else if ($result->getOrder->status === 'CANCELLED') {
                                    $_data->status = 'Cancelled';
                                }
                                if (count(get_object_vars($result->getOrder->fulfillments)) > 0) {
                                    foreach ($result->getOrder->fulfillments as $fulfillment) {
                                        if ($fulfillment->trackingNumber && $fulfillment->trackingNumber !== '') {
                                            $_data->status = 'Delivering';
                                            $_data->deliveryId = $fulfillment->trackingNumber;
                                        }
                                    }
                                }

                                if ($_data->status && $_data->status === 'Delivering' && $od->status !== $_data->status) {
                                    if (isset($_data->deliveryId)) {
                                        $od->deliveryId = $_data->deliveryId;
                                    }
                                    Orders::where('id', $od->id)->update(["status" => $_data->status, "deliveryId" => $_data->deliveryId]);
                                    OrderHistories::create(["message" => $_data->status, "isRemark" => 0, "OrderId" => $od->id]);

                                    $defLang = '';
                                    if ($od->subscriptionIds !== null) {
                                        $subscription = Subscriptions::where('id', $od->subscriptionIds)->first();
                                        // Passing subs Data into $emailData
                                        $emailData['title'] = 'receipt-order-shipped';
                                        if ($subscription->isTrial == 1 && $subscription->isCustom == 0) {
                                            $emailData['moduleData'] = (object) array(
                                                'email' => $od->email,
                                                'subscriptionId' => $od->subscriptionIds,
                                                'orderId' => $od->id,
                                                'receiptId' => $receipt->id,
                                                'userId' => $od->UserId,
                                                'countryId' => $od->CountryId,
                                                'isPlan' => 1,
                                            );
                                        } elseif ($subscription->isTrial == 0 && $subscription->isCustom == 1) {
                                            $emailData['moduleData'] = (object) array(
                                                'email' => $od->email,
                                                'subscriptionId' => $od->subscriptionIds,
                                                'orderId' => $od->id,
                                                'receiptId' => $receipt->id,
                                                'userId' => $od->UserId,
                                                'countryId' => $od->CountryId,
                                                'isPlan' => 1,
                                            );
                                        } else {
                                            $emailData['moduleData'] = (object) array(
                                                'email' => $od->email,
                                                'subscriptionId' => $od->subscriptionIds,
                                                'orderId' => $od->id,
                                                'receiptId' => $receipt->id,
                                                'userId' => $od->UserId,
                                                'countryId' => $od->CountryId,
                                                'isPlan' => 0,
                                            );
                                        }
                                        $emailData['CountryAndLocaleData'] = array($od->Country, $user->defaultLanguage);
                                        EmailQueueService::addHash($receipt->id, 'receipt_order_delivery', $emailData);
                                    } else {
                                        // Passing ala carte Data into $emailData
                                        $emailData['title'] = 'receipt-order-shipped';
                                        $emailData['moduleData'] = (object) array(
                                            'email' => $od->email,
                                            'orderId' => $od->id,
                                            'receiptId' => $receipt->id,
                                            'userId' => $od->UserId,
                                            'countryId' => $od->CountryId,
                                            'isPlan' => 0,
                                        );
                                        $emailData['CountryAndLocaleData'] = array($od->Country, $user->defaultLanguage);
                                        EmailQueueService::addHash($receipt->id, 'receipt_order_delivery', $emailData);
                                    }

                                    // post order to aftership
                                    try {
                                        $options = [
                                            'order' => $od,
                                            // 'country' => $country,
                                            'shipment' => $shipment,
                                            'user' => $user,
                                        ];
                                        $ccode = "";
                                        if(isset($country)){
                                            if($country->codeIso){
                                            $ccode = $country->codeIso;
                                            }
                                        }
                                        $this->_aftership->postTracking($options,$ccode, false);
                                    } catch (Exception $ex) {
                                        $this->Log->error('SG | updateUrbanFoxOrders | post Aftership ERROR: OrderId = [' . $od->id . '] => ' . $ex->getMessage());
                                    }

                                    if ($od && $od->subscriptionIds) {
                                        Subscriptions::where('id', $od->subscriptionIds)->update(["lastDeliverydate" => Carbon::now()]);
                                    }
                                } else if ($_data->status && $_data->status === 'Cancelled' || $_data->status && $_data->status === 'Cancelled') {
                                    // $cancelOrderQueueService = CancelOrderQueueService.getInstance();
                                    // return cancelOrderQueueService.addTask(order.id);
                                } else {
                                    // $this->Log->info('getTransactionInfo from urbanfox = order = : empty');
                                }
                            }
                        }
                    }
                }
            }

            $bulkorders = BulkOrders::where('status', 'Processing')->where('carrierAgent', 'courex')->get();
            if ($bulkorders) {
                foreach ($bulkorders as $_uBulkOrder) {
                    $_data = (object) array();
                    $_data->status = $_uBulkOrder->status;
                    $_data->deliveryId = $_uBulkOrder->deliveryId !== null ? $_uBulkOrder->deliveryId : null;
                    $country = Countries::where('id', $_uBulkOrder->CountryId)->first();
                    $shipment = DeliveryAddresses::where('id', $_uBulkOrder->DeliveryAddressId)->first();
                    $result_ = $this->laravelHelper->ConvertArraytoObject($this->urbanfoxHelper->getTransactionInfo($_uBulkOrder, $country, true));
                    // save query into a text-file
                    $formatted_order_number = $this->orderService->formatBulkOrderNumber($_uBulkOrder, $country, true);
                    // $this->Log->info('getTransactionInfo from urbanfox: ' . $formatted_order_number);
                    $path = config('environment.warehouse_paths.SGP.local_download_folder');
                    // file_put_contents($path . '/' . 'UrbanFox_' . $formatted_order_number . '.txt', json_encode($result_), FILE_APPEND | LOCK_EX);

                    if (!$result_) {
                        $this->Log->info('getTransactionInfo from urbanfox = bulkorder = : empty');
                        return;
                    } else {
                        foreach ($result_ as $result) {
                            if (isset($result->getOrder)) {
                                if ($result->getOrder->status === 'DELIVERED') {
                                    $_data->status = 'Completed';
                                } else if ($result->getOrder->status === 'CANCELLED') {
                                    $_data->status = 'Cancelled';
                                }
                                if (count(get_object_vars($result->getOrder->fulfillments)) > 0) {
                                    foreach ($result->getOrder->fulfillments as $fulfillment) {
                                        if ($fulfillment->trackingNumber && $fulfillment->trackingNumber !== '') {
                                            $_data->status = 'Delivering';
                                            $_data->deliveryId = $fulfillment->trackingNumber;
                                        }
                                    }
                                }

                                if ($_data->status && $_data->status === 'Delivering' && $_uBulkOrder->status !== $_data->status) {
                                    if (isset($_data->deliveryId)) {
                                        $_uBulkOrder->deliveryId = $_data->deliveryId;
                                    }
                                    BulkOrders::where('id', $_uBulkOrder->id)->update(["status" => $_data->status, "deliveryId" => $_data->deliveryId]);
                                    BulkOrderHistories::create(["message" => $_data->status, "isRemark" => 0, "BulkOrderId" => $_uBulkOrder->id]);
                                    $defLang = '';
                                    //   sendEmailQueueService.addTask({method: 'sendReceiptsEmail', data: {orderId: order.id}, langCode: defLang})
                                    // post order to aftership
                                    try {
                                        $user =  null;
                                        $options = [
                                            'order' => $_uBulkOrder,
                                            // 'country' => $country,
                                            'shipment' => $shipment,
                                            'user' => $user
                                        ];
                                        $ccode = "";
                                        if(isset($country)){
                                            if($country->codeIso){
                                            $ccode = $country->codeIso;
                                            }
                                        }
                                        $this->_aftership->postTracking($options,$ccode, true);
                                    } catch (Exception $ex) {
                                        $this->Log->error('SG | updateUrbanFoxOrders | post Aftership ERROR: OrderId = [' . $_uBulkOrder->id . '] => ' . $ex->getMessage());
                                    }
                                } else if ($_data->status && $_data->status === 'Cancelled' || $_data->status && $_data->status === 'Cancelled') {
                                    BulkOrders::where('id', $_uBulkOrder->id)->update(["status" => $_data->status, "deliveryId" => $_data->deliveryId]);
                                    BulkOrderHistories::create(["message" => $_data->status, "isRemark" => 0, "BulkOrderId" => $_uBulkOrder->id]);
                                    // sendEmailQueueService.addTask({method: 'sendCancelBulkOrderEmail', data: order, langCode: order.Country.defaultLang.toLowerCase()}));
                                } else {
                                    // $this->Log->info('getTransactionInfo from urbanfox = bulkorder = : empty');
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('SG | updateUrbanFoxOrders | ERROR = [] => ' . $ex->getMessage());
        }
    }

    public function updateAftershipTrackingsOrders()
    {
        $init = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA.Aftership'));
        $trackings = new \AfterShip\Trackings($init->apiKey);
        $orders = Orders::whereNotNull('deliveryId')->whereNotNull('carrierAgent')->where('status', 'Delivering')->orderBy('created_at', 'DESC')->take(10)->get();
        if ($orders) {
            foreach ($orders as $od) {
                try {
                    $response = $trackings->get($od->carrierAgent, $od->deliveryId);
                    $params = (object) array();
                    $defLang = "";
                    if (isset($response["data"]["tracking"]["tag"]) && $response["data"]["tracking"]["tag"] === "Delivered") {
                        $params->msg = $response["data"]["tracking"];
                        var_dump('update order : ' . $params->msg["order_id"] . ', tracking: ' . $params->msg["tracking_number"] . ', status: ' . $params->msg["tag"]);
                        try {
                            $orderFilter = Orders::where('deliveryId', $params->msg["tracking_number"])->first();
                            $matchThese = ['orders.deliveryId' => $orderFilter->deliveryId, 'users.id' => $orderFilter->UserId, 'receipts.OrderId' => $orderFilter->id, 'countries.id' => $orderFilter->CountryId];
                            if ($orderFilter) {

                                // save api response to database
                                $abc =  DeliveryTracking::firstOrCreate([
                                    'OrderId' => $orderFilter->id,
                                ], [
                                    'warehouse_id' => $params->msg["order_id"],
                                    'status' => 'Processed',
                                    'deliveryId' => $params->msg["tracking_number"],
                                    'api_data' => json_encode($params->msg),
                                ]);


                                Orders::where('id', $orderFilter->id)->where('deliveryId', $params->msg["tracking_number"])->update(["status" => "Completed"]);
                                OrderHistories::create(["message" => "Completed", "isRemark" => 0, "OrderId" => $orderFilter->id]);
                                $_order = Orders::leftJoin('countries', 'countries.id', 'orders.CountryId')
                                    ->leftJoin('users', 'users.id', 'orders.UserId')
                                    ->leftJoin('receipts', 'receipts.OrderId', 'orders.id')
                                    ->leftJoin('subscriptions', 'subscriptions.id', 'orders.subscriptionIds')
                                    ->leftJoin('plans', 'plans.id', 'subscriptions.PlanId')
                                    ->leftJoin('plansku', 'plansku.id', 'plans.PlanSkuId')
                                    ->select(
                                        'orders.id',
                                        'orders.CountryId',
                                        'orders.subscriptionIds',
                                        'orders.UserId',
                                        'orders.email',
                                        'countries.defaultLang as cDefaultLang',
                                        'users.defaultLanguage as uDefaultLang',
                                        'receipts.id as ReceiptsId',
                                        'plansku.duration as PlanDuration',
                                        'subscriptions.isTrial',
                                        'subscriptions.startDeliverDate'
                                    )
                                    ->where($matchThese)
                                    ->first();

                                if ($_order->UserId !== null) {
                                    if ($_order->uDefaultLang) {
                                        $defLang = strtolower($_order->uDefaultLang);
                                    } else {
                                        $defLang = strtolower($_order->cDefaultLang);
                                    }
                                } else {
                                    $defLang = strtolower($_order->cDefaultLang);
                                }

                                // send email
                                try {
                                    if ($_order && $_order->subscriptionIds !== null) {
                                        if ($_order->isTrial === 1) {
                                            // send email update order status
                                            $emailData['title'] = 'receipt-order-delivered';
                                            $emailData['moduleData'] = (object) array(
                                                'email' => $_order->email,
                                                'subscriptionId' => $_order->subscriptionIds,
                                                'orderId' => $_order->id,
                                                'receiptId' => $_order->ReceiptsId,
                                                'userId' => $_order->UserId,
                                                'countryId' => $_order->CountryId,
                                                'isPlan' => 1,
                                            );
                                            $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                            // EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                        } else {
                                            // send email update order status
                                            $emailData['title'] = 'receipt-order-delivered';
                                            $emailData['moduleData'] = (object) array(
                                                'email' => $_order->email,
                                                'subscriptionId' => $_order->subscriptionIds,
                                                'orderId' => $_order->id,
                                                'receiptId' => $_order->ReceiptsId,
                                                'userId' => $_order->UserId,
                                                'countryId' => $_order->CountryId,
                                                'isPlan' => 1,
                                            );
                                            $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                            // EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                        }

                                        if ($_order->subscriptionIds !== null) {
                                            if ($_order->startDeliverDate == null && $_order->isTrial === 1) {
                                                $trial_duration = '';
                                                if ($_order->CountryId === 2) {
                                                    $trial_duration = config('environment.trialPlan.trial_first_delivery_hk');
                                                } else {
                                                    $trial_duration = config('environment.trialPlan.trial_first_delivery');
                                                }
                                                $future_delivery_date = \App\Helpers\DateTimeHelper::getFutureDates('add', $trial_duration);
                                                // update subscription for Trial (Start Subscription Journey)
                                                Subscriptions::where('id', $_order->subscriptionIds)
                                                    ->where('startDeliverDate', null)
                                                    ->update([
                                                        "lastDeliverydate" => Carbon::now()->toDateTime(),
                                                        "nextDeliverDate" => $future_delivery_date,
                                                        "nextChargeDate" => $future_delivery_date,
                                                        "startDeliverDate" => $future_delivery_date,
                                                    ]);
                                            } else {
                                                $non_trial_duration = '';
                                                if (isset($_order->PlanDuration) && $_order->PlanDuration !== null) {
                                                    $non_trial_duration = (int) $_order->PlanDuration . ' months';

                                                    $future_delivery_date = \App\Helpers\DateTimeHelper::getFutureDates('add', $non_trial_duration);
                                                    // update subscription for normal delivery (normal)
                                                    Subscriptions::where('id', $_order->subscriptionIds)
                                                        ->update([
                                                            "lastDeliverydate" => Carbon::now()->toDateTime(),
                                                            "nextDeliverDate" => $future_delivery_date,
                                                            "nextChargeDate" => $future_delivery_date,
                                                            "startDeliverDate" => $future_delivery_date,
                                                        ]);
                                                } else {
                                                    Subscriptions::where('id', $_order->subscriptionIds)
                                                        ->update([
                                                            "lastDeliverydate" => Carbon::now()->toDateTime(),
                                                        ]);
                                                    $this->Log->info('updateAftershipTrackingsOrders #1 | unable to update nextcharge : planinfo not found for sub id: ' . json_encode($_order->subscriptionIds));
                                                }
                                            }
                                        }
                                    } else {
                                        // send email update order status
                                        $emailData['title'] = 'receipt-order-delivered';
                                        $emailData['moduleData'] = (object) array(
                                            'email' => $_order->email,
                                            'subscriptionId' => $_order->subscriptionIds,
                                            'orderId' => $_order->id,
                                            'receiptId' => $_order->ReceiptsId,
                                            'userId' => $_order->UserId,
                                            'countryId' => $_order->CountryId,
                                            'isPlan' => 0,
                                        );
                                        $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                        // EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                    }
                                } catch (Exception $ex) {
                                    // var_dump('1 order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ',' . $ex->getMessage());
                                    $this->Log->error('updateAftershipTrackingsOrders #2 | error => order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                                }
                            }
                        } catch (Exception $ex) {
                            // var_dump('2 order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ',' . $ex->getMessage());
                            $this->Log->error('updateAftershipTrackingsOrders #3 | error => order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                        }
                    }
                } catch (Exception $ex) {
                    if ($ex->getCode() == 4004 && ($od->carrierAgent !== "not_found" || $od->carrierAgent !== null)) {
                        try {
                            $country = Countries::where('id', $od->CountryId)->first();
                            $user = User::where('id', $od->UserId)->first();
                            $shipment = DeliveryAddresses::where('id', $od->DeliveryAddressId)->first();
                            $options = [
                                'order' => $od,
                                'country' => $country,
                                'shipment' => $shipment,
                                'user' => $user,
                            ];
                            // $this->_aftership->postTracking($options, false);
                            $this->Log->info('updateAftershipTrackingsOrders #4 | create tracking if not found => order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId);
                        } catch (Exception $ex) {
                            // var_dump('3 order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ',' . $ex->getMessage());
                            $this->Log->error('updateAftershipTrackingsOrders #5 | create tracking if not found => order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                        }
                    } else {
                        DeliveryTracking::where('OrderId', $od->id)->update(['status' => 'Failed']);
                        // var_dump('4 order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ',' . $ex->getMessage());
                        $this->Log->error('updateAftershipTrackingsOrders #6 | error => order: ' . $od->id . ', slug: ' . $od->carrierAgent . ', tracking_number: ' . $od->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                    }
                }
            }
        }
    }

    public function updateAftershipTrackingsBulkOrders()
    {
        $init = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA.Aftership'));
        $trackings = new \AfterShip\Trackings($init->apiKey);
        $bulkorders = BulkOrders::whereNotNull('deliveryId')->whereNotNull('carrierAgent')->where('status', 'Delivering')->orderBy('created_at', 'DESC')->take(500)->get();
        if ($bulkorders) {
            foreach ($bulkorders as $bod) {
                try {
                    $response = $trackings->get($bod->carrierAgent, $bod->deliveryId);
                    $params = (object) array();
                    $defLang = "";
                    if ($response) {
                        if (isset($response["data"]["tracking"]["tag"]) && $response["data"]["tracking"]["tag"] === "Delivered") {
                            $params->msg = $response["data"]["tracking"];
                            if ($this->orderService->checkOrderNumberType($params->msg["order_id"]) === 'bulkOrder') {
                                // var_dump('update bulk order : ' . $params->msg["order_id"] . ', tracking: ' . $params->msg["tracking_number"] . ', status: ' . $params->msg["tag"]);
                                // $this->Log->info('Aftership | postUpdateOrder | check for bulkorder');
                                //bulk order
                                try {
                                    $_order = BulkOrders::where('deliveryId', $params->msg["tracking_number"])->first();
                                    if ($_order) {

                                        // save api response to database
                                        DeliveryTracking::firstOrCreate([
                                            'BulkOrderId' => $_order->id,
                                        ], [
                                            'warehouse_id' => $params->msg["order_id"],
                                            'status' => 'Processed',
                                            'deliveryId' => $params->msg["tracking_number"],
                                            'api_data' => json_encode($params->msg),
                                        ]);
                                        BulkOrders::where('id', $_order->id)->where('deliveryId', $params->msg["tracking_number"])->update(["status" => "Completed"]);
                                        BulkOrderHistories::create(["message" => "Completed", "isRemark" => 0, "BulkOrderId" => $_order->id]);
                                    }
                                } catch (Exception $ex) {
                                    DeliveryTracking::where('BulkOrderId', $_order->id)->update(['status' => 'Failed']);
                                    // var_dump('updateAftershipTrackingsBulkOrders | error => bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                                    $this->Log->error('updateAftershipTrackingsBulkOrders #1 | error => ' . $ex->getMessage());
                                }
                            }
                        }
                    }
                } catch (Exception $ex) {
                    if ($ex->getCode() == 4004 && ($bod->carrierAgent !== "not_found" || $bod->carrierAgent !== null)) {
                        try {
                            $country = Countries::where('id', $bod->CountryId)->first();
                            $user = $bod->UserId !== null ? User::where('id', $bod->UserId)->first() : null;
                            $shipment = DeliveryAddresses::where('id', $bod->DeliveryAddressId)->first();
                            $options = [
                                'order' => $bod,
                                // 'country' => $country,
                                'shipment' => $shipment,
                                'user' => $user,
                            ];
                            $ccode = "";
                            if(isset($country)){
                                if($country->codeIso){
                                $ccode = $country->codeIso;
                                }
                            }
                            $this->_aftership->postTracking($options, $ccode, true);
                            $this->Log->info('updateAftershipTrackingsOrders #2 | create tracking if not found => bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId);
                        } catch (Exception $ex) {
                            // var_dump('bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId . ',' . $ex->getMessage());
                            $this->Log->error('updateAftershipTrackingsBulkOrders #3 | create tracking if not found => bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                        }
                    } else {
                        DeliveryTracking::where('BulkOrderId', $bod->id)->update(['status' => 'Failed']);
                        // var_dump('bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId . ',' . $ex->getMessage());
                        $this->Log->error('updateAftershipTrackingsBulkOrders #4 | error => bulkorder: ' . $bod->id . ', slug: ' . $bod->carrierAgent . ', tracking_number: ' . $bod->deliveryId . ', MESSAGE: ' . $ex->getMessage());
                    }
                }
            }
        }
    }
}
