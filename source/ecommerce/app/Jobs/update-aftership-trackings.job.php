<?php

namespace App\Jobs;

use App\Helpers\LaravelHelper;
use App\Helpers\Warehouse\AftershipHelper;
use App\Helpers\Warehouse\UrbanFoxHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;
use AfterShip\Trackings;
use App\Helpers\PlanHelper;
use App\Models\Orders\DeliveryTracking;
use App\Queue\AftershipWebhookQueueService;
use CreateDeliveryTrackingsTable;
use DB;

class UpdateAftershipTracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-aftership-trackings:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aftership - Get Order Tracking Updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        // $this->urbanfoxHelper = new UrbanFoxHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->orderService = new OrderService();
        $this->_aftership = new AftershipHelper();
        $this->planHelper = new PlanHelper();
    }

    public function handle()
    {
        $this->processAftershipTrackings();
        $this->checkings();
        // $this->reTrackNUpdate();
    }

    public function processAftershipTrackings()
    {
        $this->Log->info('Aftership | update-aftership-trackings.job.php | processAftershipTrackings start');
        $lists = DeliveryTracking::where('status', 'Pending')->limit(30)->orderBy('created_at', 'DESC')->get();
        foreach ($lists as $tracking) {
            DeliveryTracking::where('id', $tracking->id)->update(['status' => 'Processing']);
            $params = $tracking->api_data !== null ? json_decode($tracking->api_data) : null;
            $defLang = "";
            if ($tracking->OrderId !== null && $tracking->BulkOrderId === null) {
                $orderFilter = Orders::where('deliveryId', $tracking->deliveryId)->where('id', $tracking->OrderId)->where('status', 'Delivering')->first();
                if ($params !== null && $orderFilter !== null) {
                    $matchThese = [
                        'deliverytrackings.status' => 'Pending',
                        'orders.status' => 'Delivering',
                        'orders.deliveryId' => $orderFilter->deliveryId,
                        'users.id' => $orderFilter->UserId,
                        'receipts.OrderId' => $orderFilter->id,
                        'countries.id' => $orderFilter->CountryId
                    ];
                    Orders::where('id', $orderFilter->id)->where('status', 'Delivering')->where('deliveryId', $params->tracking_number)->update(["status" => "Completed"]);
                    OrderHistories::create(["message" => "Completed", "isRemark" => 0, "OrderId" => $orderFilter->id]);
                    // var_dump('ID: ' . $tracking->OrderId . ', tracking_number: ' . $params->tracking_number);
                    $this->Log->info('Aftership | processAftershipTrackings | update data : ID : ' . $tracking->OrderId . ', tracking_number: ' . $params->tracking_number);
                    $_order = Orders::leftJoin('countries', 'countries.id', 'orders.CountryId')
                        ->leftJoin('users', 'users.id', 'orders.UserId')
                        ->leftJoin('deliverytrackings', 'deliverytrackings.OrderId', 'orders.id')
                        ->leftJoin('receipts', 'receipts.OrderId', 'orders.id')
                        ->leftJoin('subscriptions', 'subscriptions.id', 'orders.subscriptionIds')
                        ->leftJoin('plans', 'plans.id', 'subscriptions.PlanId')
                        ->leftJoin('plansku', 'plansku.id', 'plans.PlanSkuId')
                        ->select(
                            'orders.id',
                            'orders.deliveryId',
                            'orders.CountryId',
                            'orders.subscriptionIds',
                            'orders.UserId',
                            'orders.email',
                            'countries.defaultLang as cDefaultLang',
                            'users.defaultLanguage as uDefaultLang',
                            'receipts.id as ReceiptsId',
                            'plansku.duration as PlanDuration',
                            'subscriptions.isTrial',
                            'subscriptions.startDeliverDate'
                        )
                        ->where($matchThese)
                        ->first();

                    if ($_order !== null) {
                        if ($_order->UserId !== null) {
                            if ($_order->uDefaultLang) {
                                $defLang = strtolower($_order->uDefaultLang);
                            } else {
                                $defLang = strtolower($_order->cDefaultLang);
                            }
                        } else {
                            $defLang = strtolower($_order->cDefaultLang);
                        }

                        // update subs
                        if ($_order->subscriptionIds !== null) {
                            // dd($_order->subscriptionIds,$_order->startDeliverDate,$_order->isTrial);
                            try {
                                if ($_order->startDeliverDate == null && $_order->isTrial === 1) {
                                    $trial_duration = '';
                                    if ($_order->CountryId === 2) {
                                        $trial_duration = config('environment.trialPlan.trial_first_delivery_hk');
                                    } else {
                                        $trial_duration = config('environment.trialPlan.trial_first_delivery');
                                    }
                                    $future_delivery_date = \App\Helpers\DateTimeHelper::getFutureDates('add', $trial_duration);
                                    $this->Log->info('Aftership | processAftershipTrackings | update subs : ID : ' . $_order->subscriptionIds . ', future_delivery_date: ' . $future_delivery_date);
                                    // update subscription for Trial (Start Subscription Journey)
                                    Subscriptions::where('id', $_order->subscriptionIds)
                                        ->where('startDeliverDate', null)
                                        ->update([
                                            "lastDeliverydate" => Carbon::now(),
                                            "nextDeliverDate" => $future_delivery_date,
                                            "nextChargeDate" => $future_delivery_date,
                                            "startDeliverDate" => $future_delivery_date,
                                        ]);
                                } else {
                                    $this->Log->info('Aftership | processAftershipTrackings | update subs : ID : ' . $_order->subscriptionIds . ', lastDeliverydate: ' . Carbon::now());
                                    Subscriptions::where('id', $_order->subscriptionIds)->update(["lastDeliverydate" => Carbon::now()]);
                                }
                            } catch (Exception $ex) {
                                DeliveryTracking::where('OrderId', $tracking->OrderId)->update(['status' => 'Failed at Subscriptions', 'issue' => 'Failed at Subscriptions'. $ex->getMessage()]);
                                $this->Log->error('Aftership | processAftershipTrackings -> send Order Delivered Email | error => ' . $ex->getMessage());
                            }
                        }

                        // send email
                        try {
                            if ($tracking->delivery_edm == 0) {
                                $this->Log->info('Aftership | processAftershipTrackings | start process edm');

                                if ($_order && $_order->subscriptionIds !== null) {
                                    if ($_order->isTrial === 1) {
                                        // 'email' => $_order->email,
                                        // send email update order status
                                        $emailData['title'] = 'receipt-order-delivered';
                                        $emailData['moduleData'] = (object) array(
                                            'email' => $_order->email,
                                            'subscriptionId' => $_order->subscriptionIds,
                                            'orderId' => $_order->id,
                                            'receiptId' => $_order->ReceiptsId,
                                            'userId' => $_order->UserId,
                                            'countryId' => $_order->CountryId,
                                            'isPlan' => 1,
                                        );
                                        $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                        EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                    } else {
                                        // send email update order status
                                        $emailData['title'] = 'receipt-order-delivered';
                                        $emailData['moduleData'] = (object) array(
                                            'email' => $_order->email,
                                            'subscriptionId' => $_order->subscriptionIds,
                                            'orderId' => $_order->id,
                                            'receiptId' => $_order->ReceiptsId,
                                            'userId' => $_order->UserId,
                                            'countryId' => $_order->CountryId,
                                            'isPlan' => 1,
                                        );
                                        $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                        EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                    }
                                } else {
                                    // send email update order status
                                    $emailData['title'] = 'receipt-order-delivered';
                                    $emailData['moduleData'] = (object) array(
                                        'email' => $_order->email,
                                        'subscriptionId' => $_order->subscriptionIds,
                                        'orderId' => $_order->id,
                                        'receiptId' => $_order->ReceiptsId,
                                        'userId' => $_order->UserId,
                                        'countryId' => $_order->CountryId,
                                        'isPlan' => 0,
                                    );
                                    $emailData['CountryAndLocaleData'] = array($_order->CountryId, $defLang);
                                    EmailQueueService::addHash($_order->ReceiptsId, 'receipt_order_completed', $emailData);
                                }
                                DeliveryTracking::where('OrderId', $tracking->OrderId)->update(['status' => 'Processed', 'delivery_edm' => 1]);
                                $this->Log->info('Aftership | processAftershipTrackings | delivery edm sent successful to :' . $_order->email);
                            } else {
                                $this->Log->error('Aftership | processAftershipTrackings -> send Order Delivered Email  | error => EDM has been sent previously, cannot retrigger edm');
                            }
                        } catch (Exception $ex) {
                            DeliveryTracking::where('OrderId', $tracking->OrderId)->update(['status' => 'Failed at EDM', 'delivery_edm' => 0, 'issue' => 'Failed at EDM'. $ex->getMessage()]);
                            $this->Log->error('Aftership | processAftershipTrackings -> send Order Delivered Email | error => ' . $ex->getMessage());
                        }
                    } else {
                        DeliveryTracking::where('OrderId', $tracking->OrderId)->update(['status' => 'Failed', 'delivery_edm' => 0, 'issue' => 'order not found 1']);
                    }
                } else {
                    DeliveryTracking::where('OrderId', $tracking->OrderId)->update(['status' => 'Failed', 'delivery_edm' => 0, 'issue' => 'order not found 2']);
                }
            } else if ($tracking->BulkOrderId !== null && $tracking->OrderId === null) {
                //bulk order
                try {
                    $_order = BulkOrders::where('deliveryId', $params->tracking_number)->where('id', $tracking->BulkOrderId)->where('status', 'Delivering')->first();
                    if ($_order) {
                        // var_dump('BulkID: ' . $tracking->BulkOrderId . ', tracking_number: ' . $params->tracking_number);
                        $this->Log->info('Aftership | update-aftership-trackings.job.php | processAftershipTrackings | update data : BulkID : ' . $tracking->BulkOrderId . ', tracking_number: ' . $params->tracking_number);
                        BulkOrders::where('id', $_order->id)->where('status', 'Delivering')->where('deliveryId', $params->tracking_number)->update(["status" => "Completed"]);
                        BulkOrderHistories::create(["message" => "Completed", "isRemark" => 0, "BulkOrderId" => $_order->id]);
                        DeliveryTracking::where('BulkOrderId', $tracking->BulkOrderId)->update(['status' => 'Processed']);
                    }else{
                        DeliveryTracking::where('BulkOrderId', $tracking->BulkOrderId)->update(['status' => 'Failed', 'issue' => 'bulkorder not found']);
                    }
                } catch (Exception $ex) {
                    DeliveryTracking::where('BulkOrderId', $tracking->BulkOrderId)->update(['status' => 'Failed', 'issue' => $ex->getMessage()]);
                    $this->Log->error('Aftership | update-aftership-trackings.job.php | processAftershipTrackings | error => ' . $ex->getMessage());
                }
            } else {
                $this->Log->error('Aftership | update-aftership-trackings.job.php | processAftershipTrackings | error => cannot process api_data for  Order/BulkOrder ID : ' . json_encode($tracking->id));
            }
        }
    }

    public function checkings()
    {
        $this->Log->info('Aftership | update-aftership-trackings.job.php | checkings start');
        DB::table('deliverytrackings')
            ->leftJoin('orders', 'orders.id', '=', 'deliverytrackings.OrderId')
            ->where('orders.status', 'Completed')
            ->where('deliverytrackings.status', 'Failed')
            ->where('deliverytrackings.created_at', '>=', Carbon::now()->subMonths(1))
            ->whereNull('deliverytrackings.issue')
            ->select(
                'orders.*',
                'deliverytrackings.id as DeliveryTrackingsId',
                'deliverytrackings.status as DeliveryTrackingsStatus',
                'deliverytrackings.deliveryId as DeliveryTrackingsNumber'
            )
            ->orderBy('deliverytrackings.created_at')
            ->chunk(1000, function ($orders) use (&$orders_final) {
                foreach ($orders as $od) {
                    if ($od->status === 'Completed' && $od->DeliveryTrackingsStatus === 'Failed' && $od->deliveryId == $od->DeliveryTrackingsNumber) {
                        DeliveryTracking::where([
                            'id' => $od->DeliveryTrackingsId,
                            'OrderId' => $od->id,
                            'deliveryId' => $od->DeliveryTrackingsNumber,
                            'status' => 'Failed'
                        ])->update([
                            'status' => 'Processed'
                        ]);
                    }
                }
            });

        DB::table('deliverytrackings')
            ->leftJoin('bulkorders', 'bulkorders.id', '=', 'deliverytrackings.BulkOrderId')
            ->where('bulkorders.status', 'Completed')
            ->where('deliverytrackings.status', 'Failed')
            ->where('deliverytrackings.created_at', '>=', Carbon::now()->subMonths(1))
            ->whereNull('deliverytrackings.issue')
            ->select(
                'bulkorders.*',
                'deliverytrackings.id as DeliveryTrackingsId',
                'deliverytrackings.status as DeliveryTrackingsStatus',
                'deliverytrackings.deliveryId as DeliveryTrackingsNumber'
            )
            ->orderBy('deliverytrackings.created_at')
            ->chunk(1000, function ($orders) use (&$orders_final) {
                foreach ($orders as $od) {
                    if ($od->status === 'Completed' && $od->DeliveryTrackingsStatus === 'Failed' && $od->deliveryId == $od->DeliveryTrackingsNumber) {
                        DeliveryTracking::where([
                            'id' => $od->DeliveryTrackingsId,
                            'BulkOrderId' => $od->id,
                            'deliveryId' => $od->DeliveryTrackingsNumber,
                            'status' => 'Failed'
                        ])->update([
                            'status' => 'Processed'
                        ]);
                    }
                }
            });
    }

    public function reTrackNUpdate()
    {

        $retrack = Orders::where('status', 'Delivering')->whereNotNull('deliveryId')->whereNotNull('carrierAgent')->orderBy('created_at', 'DESC')->limit(10)->get();

        foreach ($retrack as $t) {
            try {
                $track = $this->_aftership->getTrackingOneByOne($t->carrierAgent, $t->deliveryId);
                sleep(75);
                $sp = \App\Helpers\LaravelHelper::ConvertArraytoObject($track["data"]["tracking"]);
                $params = (object) array();
                $params->msg = $sp;
                if ($this->orderService->checkOrderNumberType($params->msg->order_id) === 'order') {
                    $this->Log->info('Aftership | addWebhookToDB ID => ' . $params->msg->order_id);
                    try {
                        $orderFilter = Orders::where('deliveryId', $params->msg->tracking_number)->where('id', $t->id)->where('carrierAgent', $params->msg->slug)->whereIn('status', ['Delivering', 'Completed', 'Cancelled', 'Returned'])->select('id', 'status')->first();
                        if ($orderFilter && $orderFilter->status == 'Delivering') {

                            // save api response to database
                            DeliveryTracking::firstOrCreate([
                                'OrderId' => $orderFilter->id,
                            ], [
                                'warehouse_id' => $params->msg->order_id,
                                'status' => 'Pending',
                                'deliveryId' => $params->msg->tracking_number,
                                'api_data' => json_encode($params->msg),
                                'delivery_edm' => 0
                            ]);

                            var_dump($params->msg->order_id . " added to DeliveryTrackings");
                        } else {
                            // do nothing
                            $this->Log->info('Aftership | aftership tracking no processed : tracking ID => ' . $params->msg->tracking_number);
                        }
                    } catch (Exception $ex) {
                        $this->Log->error('Aftership | retrigger-addWebhookToDB | error => ' . $params->msg->tracking_number . ' >>>> ' . $ex->getMessage());
                    }
                }
            } catch (Exception $ex) {
                $this->Log->error('Aftership | get tracking updates from aftership | error Order ID => [' . $t->id . '] >>>> ' . $ex->getMessage());
            }
        }


        $retrackBO = BulkOrders::where('status', 'Delivering')->whereNotNull('deliveryId')->whereNotNull('carrierAgent')->orderBy('created_at', 'DESC')->limit(10)->get();
        foreach ($retrackBO as $tBO) {
            try {
                $trackBO = $this->_aftership->getTrackingOneByOne($tBO->carrierAgent, $tBO->deliveryId);
                sleep(75);
                $spBO = \App\Helpers\LaravelHelper::ConvertArraytoObject($trackBO["data"]["tracking"]);
                $params = (object) array();
                $params->msg = $spBO;
                if ($this->orderService->checkOrderNumberType($params->msg->order_id) === 'bulkOrder') {
                    try {
                        $_order = BulkOrders::where('deliveryId', $params->msg->tracking_number)->whereIn('status', ['Delivering', 'Completed', 'Cancelled', 'Returned'])->select('id', 'status')->first();
                        // save api response to database
                        DeliveryTracking::firstOrCreate([
                            'BulkOrderId' => $_order->id,
                        ], [
                            'warehouse_id' => $params->msg->order_id,
                            'status' => 'Pending',
                            'deliveryId' => $params->msg->tracking_number,
                            'api_data' => json_encode($params->msg),
                            'delivery_edm' => 0
                        ]);
                    } catch (Exception $ex) {
                        $this->Log->error('Aftership | retrigger-addWebhookToDB | <bulk> error => ' . $params->msg->tracking_number . ' >>>> ' . $ex->getMessage());
                    }
                }
            } catch (Exception $ex) {
                $this->Log->error('Aftership | get tracking updates from aftership | error BulkOrder ID => [' . $tBO->id . '] >>>> ' . $ex->getMessage());
            }
        }
        //     else {
        //         $this->Log->info('Aftership | addWebhookToDB | check for bulkorder ID => ' . $sp["order_id"]);
        //         //bulk order
        //         try {
        //             $_order = BulkOrders::where('deliveryId', $sp["tracking_number"])->whereIn('status', ['Delivering', 'Completed', 'Cancelled', 'Returned'])->select('id', 'status')->first();
        //             if ($_order) {
        //                 if ($_order && $_order->status == 'Delivering') {
        //                     // save api response to database
        //                     DeliveryTracking::firstOrCreate([
        //                         'BulkOrderId' => $_order->id,
        //                     ], [
        //                         'warehouse_id' => $sp["order_id"],
        //                         'status' => 'Pending',
        //                         'deliveryId' => $sp["tracking_number"],
        //                         'api_data' => json_encode($sp),
        //                         'delivery_edm' => 0
        //                     ]);
        //                 } else {
        //                     // do nothing
        //                     $this->Log->info('Aftership | aftership tracking no processed <bulk> : tracking ID => ' . $sp["tracking_number"]);
        //                 }
        //             }
        //         } catch (Exception $ex) {
        //             $this->Log->error('Aftership | retrigger-addWebhookToDB | <bulk> error => ' . $sp["tracking_number"] . ' >>>> ' . $ex->getMessage());
        //         }
        //     }
        // }

    }
}
