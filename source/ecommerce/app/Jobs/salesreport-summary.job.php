<?php

namespace App\Jobs;

use App\Helpers\PlanHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Reports\SummarySalesReport;
   //remove master report
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanTrialProducts;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
   //remove master report
use App\Models\User\DeliveryAddresses;
use App\Services\OrderService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SalesReportSummaryTblJob extends Command
{
    protected $signature = 'salesreport-summary:job';
    protected $description = 'Summary table of Sales Report';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'salesreport-summary: ';
        $this->Log = \Log::channel('cronjob');
        $this->orderService = new OrderService;
        $this->planHelper = new PlanHelper;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->CreateInitialSalesReportSummary();
        $this->UpdateSalesReportOrderSummary();
        $this->UpdateSalesReportBulkOrderSummary();
        $this->UpdateCompletionDateSummary();
    }

    public function CreateInitialSalesReportSummary()
    {
        $this->Log->info('CreateSalesReportSummary Process start.');

        $channel = "all";
        $country = "all";
        $status = "all";
        $fromdate = \Carbon\Carbon::now()->subDays(2)->format('m/d/Y');
        $todate = \Carbon\Carbon::now()->format('m/d/Y');
        // $fromdate = '01/01/2017';
        // $todate = '12/31/2019';

        $reports = collect();
        $cSummaryIds = [];
        DB::table('salereports')
            ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
            ->leftJoin('bulkorders', 'salereports.bulkOrderId', '=', 'bulkorders.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->leftJoin('subscriptions', 'subscriptions.id', '=', 'orders.subscriptionIds')
            ->leftJoin('cards', 'cards.id', '=', 'subscriptions.CardId')
            ->leftJoin('countries', 'countries.id', '=', 'salereports.CountryId')
            // ->leftJoin('cards', function ($join) {
            //     $join->on('orders.UserId', '=', 'cards.UserId');
            //     $join->on('receipts.last4', '=', 'cards.cardNumber');
            //     $join->on('receipts.branchName', '=', 'cards.branchName');
            //     $join->on('receipts.expiredYear', '=', 'cards.expiredYear');
            //     $join->on('receipts.expiredMonth', '=', 'cards.expiredMonth');
            // })
            ->select(
                'countries.codeIso as OrderCountryCodeIso',
                'countries.includedTaxToProduct as isIncludedTaxToProduct',
                'subscriptions.isTrial as SubscriptionIsTrial',
                 //remove master report
                'subscriptions.id as Subscriptionid',
                'subscriptions.PlanId as SubscriptionPlanId',
                 //remove master report
                'salereports.*',
                'salereports.created_at as screated_at',
                'orders.subscriptionIds as subscriptionIds',
                'orders.imp_uid as imp_uid',
                'orders.payment_status as paymentStatus',
                'orders.carrierAgent as carrierAgent',
                'orders.UserId as UserId',
                'orders.isSubsequentOrder as isSubsequentOrder',
                'orders.DeliveryAddressId as odaid',
                'orders.BillingAddressId as obaid',
                'orders.created_at as order_created_at',
                'orders.updated_at as order_updated_at',
                'orders.phone as order_phone',
                // 'orders.email as orderEmail',
                'bulkorders.carrierAgent as bulkCarrierAgent',
                'bulkorders.created_at as bulkorder_created_at',
                'bulkorders.updated_at as bulkorder_updated_at',
                'bulkorders.phone as bulkorder_phone',
                // 'bulkorders.email as bulkOrderEmail',
                'receipts.chargeId as rchargeId',
                'receipts.branchName as rbranchName',
                'cards.cardType as cCardType'
            )
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                } else if ($channel === "seller") {
                    return $q->where('salereports.category', "sales app");
                } else if ($channel === "customer") {
                    return $q->where('salereports.category', "client");
                } else if ($channel === "bulk") {
                    return $q->where('salereports.category', "Bulk Order");
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.CountryId', $country);
                }
            })

            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.status', $status);
                }
            })
        // ->count();
            ->orderBy('salereports.created_at', 'ASC')
            ->chunk(100, function ($reportsmaster) use (&$reports) {
                foreach ($reportsmaster as $key => $rm) {
                    $rm->includedTaxToProduct = $rm->isIncludedTaxToProduct;
                    $rm->order_id = isset($rm->orderId) && $rm->orderId !== null ? $rm->orderId : null;
                    $rm->bulkorder_id = isset($rm->bulkOrderId) && $rm->bulkOrderId !== null ? $rm->bulkOrderId : null;
                    $rm->order_contact_no = '-';
                    $first_order_of_account = '';
                    if ($rm->category == "Bulk Order") {
                        // $first_order_of_account = BulkOrders::where('email', $rm->bulkOrderEmail)->first();
                        // $countryIso = '';
                        // if ($first_order_of_account) {
                        //     if ($first_order_of_account->CountryId === 1) {$countryIso = 'MY';} elseif ($first_order_of_account->CountryId === 2) {$countryIso = 'HK';} elseif ($first_order_of_account->CountryId === 7) {$countryIso = 'SG';} elseif ($first_order_of_account->CountryId === 8) {$countryIso = 'KR';} elseif ($first_order_of_account->CountryId === 9) {$countryIso = 'TW';} else {}
                        //     // $first_order_of_account_country = Countries::where('id', $first_order_of_account->CountryId)->first();
                        //     $rm->order_no_user_id = $this->orderService->formatBulkOrderNumberV5($first_order_of_account->id, strtoupper($countryIso), $first_order_of_account->created_at, true);
                        //     // $rm->order_no_user_id = $this->orderService->formatBulkOrderNumberV5($first_order_of_account->FirstOrderId, strtoupper($first_order_of_account->codeIso), $first_order_of_account->FirstOrderCreatedAt, true);
                        // } else {
                        //     $rm->order_no_user_id = '-';
                        // }
                        $rm->order_no = $this->orderService->formatBulkOrderNumberV5($rm->bulkOrderId, strtoupper($rm->OrderCountryCodeIso), $rm->bulkorder_created_at, false);
                        $rm->isSubsequentOrder = 0;
                        $rm->OrderCreatedAt = $rm->bulkorder_created_at;
                        $rm->OrderUpdatedAt = $rm->bulkorder_updated_at;
                        $rm->order_contact_no = (string) $rm->bulkorder_phone;
                    } else {
                        // $first_order_of_account = Orders::where('email', $rm->orderEmail)->first();
                        // $countryIso = '';
                        // if ($first_order_of_account) {
                        //     if ($first_order_of_account->CountryId === 1) {$countryIso = 'MY';} elseif ($first_order_of_account->CountryId === 2) {$countryIso = 'HK';} elseif ($first_order_of_account->CountryId === 7) {$countryIso = 'SG';} elseif ($first_order_of_account->CountryId === 8) {$countryIso = 'KR';} elseif ($first_order_of_account->CountryId === 9) {$countryIso = 'TW';}
                        //     // $first_order_of_account_country = Countries::where('id', $first_order_of_account->CountryId)->first();
                        //     $rm->order_no_user_id = $this->orderService->formatOrderNumberV5($first_order_of_account->id, strtoupper($countryIso), $first_order_of_account->created_at, true);
                        //     // $rm->order_no_user_id = $this->orderService->formatOrderNumberV5($first_order_of_account->FirstOrderId, strtoupper($first_order_of_account->codeIso), $first_order_of_account->FirstOrderCreatedAt, true);
                        // } else {
                        //     $rm->order_no_user_id = '-';
                        // }
                                     //remove master report
                                     if (strpos($rm->sku, ',') !== false) {
                                    }else{
                                        $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
            
                                        if(!in_array($rm->sku,$srplans)){
                                            $combineqty = "";
                                            $combinesku = "";
                                            $countcombinesku = 0;
                                            if($rm->Subscriptionid){
                                            if($rm->SubscriptionPlanId){
                                                if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {

                                                    $getPlanProduct =  PlanTrialProducts::join('products', 'products.id', 'plantrialproducts.ProductId')
                                                        ->where('plantrialproducts.PlanId', $rm->SubscriptionPlanId)
                                                        ->select('plantrialproducts.qty as pqty', 'products.sku as psku')
                                                        ->orderBy('products.sku')
                                                        ->get();
                                                    if ($getPlanProduct) {
                                                        foreach ($getPlanProduct as $_getPlanProduct) {
                                                            if ($countcombinesku == 0) {
                                                                $combinesku = $_getPlanProduct->psku;
                                                                $combineqty = $_getPlanProduct->pqty;
                                                            } else {
                                                                $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                                $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                            }
                                                            $countcombinesku++;
                                                        }
                                                    }
                                                } else {
                                               $getPlanProduct =  PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                                                ->join('products', 'products.id', 'productcountries.ProductId')
                                                ->where('plan_details.PlanId', $rm->SubscriptionPlanId)
                                                ->select('plan_details.qty as pqty','products.sku as psku')
                                                ->orderBy('products.sku')
                                                ->get();
                                                if($getPlanProduct){
                                                    if($rm->SubscriptionIsTrial == 1 ){
                                                    foreach ($getPlanProduct as $_getPlanProduct) {
                                                        if (!in_array($_getPlanProduct->psku, config('global.all.handle_types.skusV2')) ) {
                                                        if($countcombinesku == 0){
                                                            $combinesku = $_getPlanProduct->psku;
                                                            $combineqty = $_getPlanProduct->pqty;
                                                      
                                                        }else{
                                                            $combinesku =  $combinesku.",".$_getPlanProduct->psku;
                                                            $combineqty = $combineqty.",".$_getPlanProduct->pqty;
                                                        }
                                                        }
                                                        $countcombinesku++;
                                                   }
                                                } else{
            
                                                   foreach ($getPlanProduct as $_getPlanProduct) {       
                                                    if($countcombinesku == 0){
                                                        $combinesku = $_getPlanProduct->psku;
                                                        $combineqty = $_getPlanProduct->pqty;
                                                  
                                                    }else{
                                                        $combinesku =  $combinesku.",".$_getPlanProduct->psku;
                                                        $combineqty = $combineqty.",".$_getPlanProduct->pqty;
                                                    }
                                                        $countcombinesku++;
                                                    }
                                                }
                                                }
                                           
                                             } 
                                            }
                                            if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {
                                            } else {
                                            $subaddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                                                ->join('products', 'products.id', 'productcountries.ProductId')
                                                ->where("subscriptions_productaddon.subscriptionId",$rm->Subscriptionid)
                                                ->select('subscriptions_productaddon.qty as pqty','products.sku as psku')
                                                ->orderBy('products.sku')
                                                ->get();
                                            if($subaddons){
                                                foreach ($subaddons as $_subaddons) {
                                                    if($countcombinesku == 0){
                                                        $combinesku = $_subaddons->psku;
                                                        $combineqty = $_subaddons->pqty;
                                                  
                                                    }else{
                                                        $combinesku = $combinesku.",".$_subaddons->psku;
                                                        $combineqty = $combineqty.",".$_subaddons->pqty;
                                                    }
                                                    $countcombinesku++;
                                               }
                                            }
                                        }
                                            if($combineqty != "" && $combinesku != "")
                                             {
                                                $rm->sku = $combinesku;
                                                $rm->qty = $combineqty;
                                             }
            
                                          }
                                        }
                                    }
                                    //remove master report
                        $rm->order_no = $this->orderService->formatOrderNumberV5($rm->orderId, strtoupper($rm->OrderCountryCodeIso), $rm->order_created_at, false);
                        $rm->OrderCreatedAt = $rm->order_created_at;
                        $rm->OrderUpdatedAt = $rm->order_updated_at;
                        $rm->order_contact_no = (string) $rm->order_phone;
                        if(isset($rm->deliveryContact)){

                        }else{
                          $getDeliveryAddresses = DeliveryAddresses::where('id',$rm->odaid)
                          ->select('contactNumber')
                          ->first();
                          $rm->deliveryContact = $getDeliveryAddresses->contactNumber;
                        }
                        if(isset($rm->billingContact)){

                        }else{
                          $getBillingAddresses = DeliveryAddresses::where('id',$rm->obaid)
                          ->select('contactNumber')
                          ->first();
                          $rm->deliveryContact = $getBillingAddresses->contactNumber;
                        }
                    }

                    // $rm->created_at = date('d-m-y H:i', strtotime($rm->created_at));
                    $rm->completed_date = null;
                    if ($rm->status == "Completed") {
                        if ($rm->category != "Bulk Order") {
                            $rm->completed_at = OrderHistories::where('OrderId', $rm->orderId)->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                            if ($rm->completed_at != null) {
                                $rm->completed_date = $rm->completed_at;
                            }
                        } else {
                            $rm->completed_at = BulkOrderHistories::where('BulkOrderId', $rm->bulkOrderId)->where('message', 'Completed')->where('isRemark', '!=', '1')->pluck('created_at')->first();
                            if ($rm->completed_at != null) {
                                $rm->completed_date = $rm->completed_at;
                            }
                        }
                    }
                    $rm->isActiveTrial = 0;
                    if (isset($rm->SubscriptionIsTrial) && $rm->SubscriptionIsTrial != null) {
                        $rm->isTrial = $rm->SubscriptionIsTrial;
                        $rm->isActiveTrial = $rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0 ? 1 : 0;
                    }
                }

                $reports = $reports->merge($reportsmaster);
            });

        if ($reports) {
            $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
            $data = [];
            foreach ($reports as $report_) {
                // Initialize variables
                $getChargeid = '';
                if ($report_ != null && $report_ != '') {
                    if ($report_->CountryId == 8) {
                        if ($report_->imp_uid != null && $report_->imp_uid != '') {
                            $getChargeid = $report_->imp_uid;
                        } else {
                            $getChargeid = '';
                        }
                    } else {
                        $getChargeid = $report_->rchargeId;
                    }
                }

                // Tax amount calculation (Based on edm)
                $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                // Let this be referral Discount Section
                $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                if ($report_->promoCode && $report_->promoCode == "referral") {
                    // For referral Discounts, Set cash rebate to equal to Discount Amount
                    $cashRebate = $report_->discountAmount;
                    // Then set Discount ammount to 0
                    $discountAmount = number_format((float) 0, 2, '.', '');
                } else {
                    // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                    $cashRebate = number_format((float) 0, 2, '.', '');
                }

                $portalCode = $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? explode(',', $report_->deliveryAddress) : '-') : '-';
                if ($portalCode !== '-') {
                    array_pop($portalCode);
                    $portalCode = end($portalCode);
                }
                $bportalCode = $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? explode(',', $report_->billingAddress) : '-') : '-';
                if ($bportalCode !== '-') {
                    array_pop($bportalCode);
                    $bportalCode = end($bportalCode);
                }
                $category = (isset($report_->category) && $report_->category === 'sales app') ? 'BA' : ((isset($report_->category) && $report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');

                $e = [
                    "OrderId" => $report_ !== null ? (isset($report_->order_id) && $report_->order_id != "" ? $report_->order_id : null) : null,
                    "BulkOrderId" => $report_ !== null ? (isset($report_->bulkorder_id) && $report_->bulkorder_id != "" ? $report_->bulkorder_id : null) : null,
                    "OrderNumber" => $report_ !== null ? (isset($report_->order_no) && $report_->order_no != "" ? $report_->order_no : '-') : '-',
                    "OrderStatus" => $report_ !== null ? (isset($report_->status) && $report_->status != "" ? $report_->status : '-') : '-',
                    "SaleDate" => $report_ !== null ? (isset($report_->OrderCreatedAt) && $report_->OrderCreatedAt != "" ? $report_->OrderCreatedAt : '-') : '-',
                    "CompletionDate" => $report_ !== null ? (isset($report_->completed_date) && $report_->completed_date != null ? $report_->completed_date : '-') : '-',
                    "Region" => $report_ !== null ? (isset($report_->region) && $report_->region != "" ? $report_->region : '-') : '-',
                    "Category" => isset($category) ? $category : '-',
                    "MOCode" => $report_ !== null ? (isset($report_->moCode) && $report_->moCode != "" ? $report_->moCode : '-') : '-',
                    "BadgeId" => $report_ !== null ? (isset($report_->badgeId) && $report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                    "AgentName" => $report_ !== null ? (isset($report_->agentName) && $report_->agentName != "" ? $report_->agentName : '-') : '-',
                    "ChannelType" => $report_ !== null ? (isset($report_->channelType) && $report_->channelType != "" ? $report_->channelType : '-') : '-',
                    "Event/LocationCode" => $report_ !== null ? (isset($report_->eventLocationCode) && $report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                    "DeliveryMethod" => $report_ !== null ? (isset($report_->isDirectTrial) && $report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                    "TrackingNumber" => $report_ !== null ? (isset($report_->deliveryId) && $report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                    "UserID" => $report_ !== null ? (isset($report_->UserId) && $report_->UserId != "" ? $report_->UserId : '-') : '-',
                    "CustomerName" => $report_ !== null ? (isset($report_->customerName) && $report_->customerName != "" ? $report_->customerName : '-') : '-',
                    "Email" => $report_ !== null ? (isset($report_->email) && $report_->email != "" ? $report_->email : '-') : '-',
                    "DeliveryAddress" => $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                    "DeliveryPostcode" => (string) $portalCode,
                    "DeliveryContactNumber" => $report_ !== null ? (isset($report_->deliveryContact) && $report_->deliveryContact != "" ? (string) $report_->deliveryContact : $report_->order_contact_no) : '-',
                    "BillingAddress" => $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                    "BillingPostcode" => (string) $bportalCode,
                    "BillingContactNumber" => $report_ !== null ? (isset($report_->billingContact) && $report_->billingContact != "" ? (string) $report_->billingContact : $report_->order_contact_no) : '-',
                    "ProductCategory" => $report_ !== null ? (isset($report_->productCategory) && $report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                ];

                $unitcount = 0;
                $checkskuget = 0;
                $unitcount_array = [];

                if (strpos($report_->sku, ',') !== false) {
                    $report_->sku = str_replace(" ", "", $report_->sku);
                    $report_->qty = str_replace(" ", "", $report_->qty);
                    $splitproduct = explode(",", $report_->sku);
                    $splitqty = explode(",", $report_->qty);
                    foreach ($srplans as $sp) {
                        $checkskuget = 0;
                        if($report_->SubscriptionIsTrial == 1){
                        foreach ($splitproduct as $key => $p) {
                            if (($sp == $p) && !in_array($p, config('global.all.handle_types.skusV2'))) {
                                $unitcount = $unitcount + (int) $splitqty[$key];
                                $checkskuget = 1;

                                $e["" . $sp] = $splitqty[$key];
                            }
                        }
                       }else{
                        foreach ($splitproduct as $key => $p) {
                            if ($sp == $p) {
                                $unitcount = $unitcount + (int) $splitqty[$key];
                                $checkskuget = 1;

                                $e["" . $sp] = $splitqty[$key];
                            }
                        }
                       }
                        if ($checkskuget == 1) {
                        } else {
                            $e["" . $sp] =  "0";
                        }
                    }
                } else {
                    if($report_->SubscriptionIsTrial == 1){
                    foreach ($srplans as $sp) {
                        if (($sp == $report_->sku) && !in_array($report_->sku, config('global.all.handle_types.skusV2')) ) {
                            $unitcount = $unitcount + $report_->qty;

                            $e["" . $sp] = $report_->qty;
                        } else {

                            $e["" . $sp] =  "0";
                        }
                    }
                }else{
                    foreach ($srplans as $sp) {
                        if ($sp == $report_->sku) {
                            $unitcount = $unitcount + $report_->qty;

                            $e["" . $sp] = $report_->qty;
                        } else {

                            $e["" . $sp] =  "0";
                        }
                    }
                }
                }
                $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');

                $taxinvoicenumber = '';
                if (isset($report_->region) && $report_->region !== null) {
                    $taxinvoicenumber = isset($report_->taxInvoiceNo) && $report_->taxInvoiceNo !== null ? $report_->taxInvoiceNo : '-';
                } else {
                    $taxinvoicenumber = '-';
                }
                $processingfee = ['5','6.5','46','4000','175','3000','40','150'];
                $e["UnitTotal"] = (string) $unitcount;
                $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" ? $report_->cCardType : '-') : '-';
                $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                $e["DiscountAmount"] = $discountAmount;
                $e["CashRebate"] = $cashRebate;
                $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';
                $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                if(isset($report_->screated_at)){
                    if($report_->screated_at <= "2019-12-08 19:00:00"){
                           $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->shippingFee != "" ? $report_->shippingFee : '-') : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                    }
                }
                if(!in_array( $e["ProcessingFee"], $processingfee)){
                    $e["ProcessingFee"] = "0";
                }
                $e["GrandTotalWithTax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                $e["GrandTotalWithoutTax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                $e["TaxAmount"] = $taxAmount;
                $e["PaymentStatus"] = $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                $e["ChargeID"] = $getChargeid ? $getChargeid : '-';
                $e["TaxInvoicenumber"] = $taxinvoicenumber;
                $e["Source"] = $report_->source ? $report_->source : "none";
                $e["Medium"] = $report_->medium ? $report_->medium : "none";
                $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                $e["Term"] = $report_->term ? $report_->term : "none";
                $e["Content"] = $report_->content ? $report_->content : "none";
                $e["created_at"] = $report_->OrderCreatedAt;
                $e["updated_at"] = $report_->OrderUpdatedAt;

                echo $e["OrderNumber"] . ' => ' . $e["TaxInvoicenumber"] . ' => ' . $report_->region . "\n";
                try {
                    SummarySalesReport::insert($e);
                } catch (Exception $ex) {
                    // echo $ex;
                }
            }

            // SummarySalesReport::insert($data);
        }
    }

    public function UpdateSalesReportOrderSummary()
    {
        $this->Log->info('UpdateSalesReportOrderSummary Process start.');
        SummarySalesReport::join('orders', 'orders.id', 'summary_salesreport.OrderId')
            ->whereDate('orders.updated_at', '>', DB::raw('summary_salesreport.updated_at'))
        // ->orWhere('orders.status', '!=', DB::raw('summary_salesreport.OrderStatus'))
        // ->orWhere('orders.deliveryId', '!=', DB::raw('summary_salesreport.TrackingNumber'))
        // ->orWhere('orders.taxInvoiceNo', '!=', DB::raw('summary_salesreport.TaxInvoicenumber'))
            ->select(
                'summary_salesreport.OrderId as SummarySalesReportOrderId',
                'summary_salesreport.OrderStatus as SummarySalesReportOrderStatus',
                'summary_salesreport.TaxInvoicenumber as SummarySalesReportTaxInvoicenumber',
                'summary_salesreport.TrackingNumber as SummarySalesReportTrackingNumber',
                'summary_salesreport.created_at as SummarySalesReportCreatedAt',
                'summary_salesreport.updated_at as SummarySalesReportUpdatedAt',
                'orders.id as id',
                'orders.status',
                'orders.deliveryId',
                'orders.taxInvoiceNo',
                'orders.created_at',
                'orders.updated_at'
            )
            ->chunk(1000, function ($reportsmaster) use (&$reports) {
                foreach ($reportsmaster as $key => $rm) {
                    $update = SummarySalesReport::where('OrderId', $rm["SummarySalesReportOrderId"])->first();
                    if ($update) {
                        if ($rm["status"] !== null) {
                            $update->OrderStatus = $rm["status"];
                        }
                        if ($rm["deliveryId"] !== null) {
                            $update->TrackingNumber = $rm["deliveryId"];
                        }
                        if ($rm["taxInvoiceNo"] !== null) {
                            $update->TaxInvoicenumber = $rm["taxInvoiceNo"];
                        }
                        echo "Update OrderId : Order(" . $rm["id"] . ") to SummarySalesReport(" . $rm["SummarySalesReportOrderId"] . ") " . "\n";
                        $update->created_at = $rm["created_at"];
                        $update->updated_at = $rm["updated_at"];
                        $update->save();
                    }
                }
            });
    }

    public function UpdateSalesReportBulkOrderSummary()
    {
        $this->Log->info('UpdateSalesReportBulkOrderSummary Process start.');
        SummarySalesReport::join('bulkorders', 'bulkorders.id', 'summary_salesreport.BulkOrderId')
            ->whereDate('bulkorders.updated_at', '>', DB::raw('summary_salesreport.updated_at'))
        // ->orWhere('bulkorders.status', '!=', DB::raw('summary_salesreport.OrderStatus'))
        // ->orWhere('bulkorders.deliveryId', '!=', DB::raw('summary_salesreport.TrackingNumber'))
        // ->orWhere('bulkorders.taxInvoiceNo', '!=', DB::raw('summary_salesreport.TaxInvoicenumber'))
            ->select(
                'summary_salesreport.BulkOrderId as SummarySalesReportBulkOrderId',
                'summary_salesreport.OrderStatus as SummarySalesReportBulkOrderStatus',
                'summary_salesreport.TaxInvoicenumber as SummarySalesReportTaxInvoicenumber',
                'summary_salesreport.TrackingNumber as SummarySalesReportTrackingNumber',
                'summary_salesreport.created_at as SummarySalesReportCreatedAt',
                'summary_salesreport.updated_at as SummarySalesReportUpdatedAt',
                'bulkorders.id as id',
                'bulkorders.status',
                'bulkorders.deliveryId',
                'bulkorders.taxInvoiceNo',
                'bulkorders.created_at',
                'bulkorders.updated_at'
            )
            ->chunk(1000, function ($reportsmaster) use (&$reports) {
                foreach ($reportsmaster as $key => $rm) {
                    $update = SummarySalesReport::where('BulkOrderId', $rm["SummarySalesReportBulkOrderId"])->first();
                    if ($update) {
                        if ($rm["status"] !== null) {
                            $update->OrderStatus = $rm["status"];
                        }
                        if ($rm["deliveryId"] !== null) {
                            $update->TrackingNumber = $rm["deliveryId"];
                        }
                        if ($rm["taxInvoiceNo"] !== null) {
                            $update->TaxInvoicenumber = $rm["taxInvoiceNo"];
                        }
                        echo "Update BulkOrderId : Order(" . $rm["id"] . ") to SummarySalesReport(" . $rm["SummarySalesReportBulkOrderId"] . ") " . "\n";
                        $update->created_at = $rm["created_at"];
                        $update->updated_at = $rm["updated_at"];
                        $update->save();
                    }
                }
            });
    }

    public function UpdateCompletionDateSummary(){
        $this->Log->info('UpdateCompletionDateSummary Process start.');
        SummarySalesReport::where('OrderStatus', 'Completed')
            ->where('CompletionDate', '-')
            ->select(
                'OrderStatus',
                'Category',
                'CompletionDate',
                'OrderId',
                'BulkOrderId'
            )
            ->chunk(1000, function ($reportsmaster) use (&$reports) {
                foreach ($reportsmaster as $key => $rm) {
                    $rm["completed_date"] = '-';
                    if ($rm["OrderStatus"] == "Completed") {
                        if ($rm["Category"] != "Bulk Order") {
                            $rm["completed_at"] = OrderHistories::where('OrderId', $rm["OrderId"])->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                            if ($rm["completed_at"] != null) {
                                $rm["completed_date"] = $rm["completed_at"];
                            }
                            $update = SummarySalesReport::where('OrderId', $rm["OrderId"])->first();
                            if ($update && $rm["completed_date"] !="-") {
                                if ($rm["completed_date"] !== null) {
                                    $update->CompletionDate = $rm["completed_date"];
                                }
                                echo "Update OrderId : Order(" . $rm["OrderId"] . ") to ".$update->CompletionDate. "\n";
                                $update->save();
                            }
                        } else {
                            $rm["completed_at"] = BulkOrderHistories::where('BulkOrderId', $rm["BulkOrderId"])->where('message', 'Completed')->where('isRemark', '!=', '1')->pluck('created_at')->first();
                            if ($rm["completed_at"] != null) {
                                $rm["completed_date"] = $rm["completed_at"];
                            }
                            $update = SummarySalesReport::where('BulkOrderId', $rm["BulkOrderId"])->first();
                            if ($update && $rm["completed_date"] !="-") {
                                if ($rm["completed_date"] !== null) {
                                    $update->CompletionDate = $rm["completed_date"];
                                }
                                echo "Update BulkOrderId : BulkOrderId(" . $rm["BulkOrderId"] . ") to ".$update->CompletionDate. "\n";
                                $update->save();
                            }
                        }
                    }

                }
            });



  }
}
