<?php

namespace App\Jobs;

use App\Helpers\Warehouse\UrbanFoxHelper;
use App\Helpers\Warehouse\WarehouseHelperHK;
use App\Helpers\Warehouse\WarehouseHelperKR;
use App\Helpers\Warehouse\WarehouseHelperMY;
use App\Helpers\Warehouse\WarehouseHelperTW;
use App\Jobs\CreateBulkOrderInboundJob;
use App\Jobs\ReadOrderOutboundJob;
use App\Queue\OrderInboundQueueService;
use Exception;
use Illuminate\Console\Command;

class CreateOrderInboundJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-order-inbound:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LF Warehouse - Create Order file for Warehouse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->className = 'createOrderInbound';
        $this->functionName = 'createOrderInbound';
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('CreateOrderInboundJob start');

        $this->_WareHouseHelperMY = new WarehouseHelperMY();
        $this->_WareHouseHelperHK = new WarehouseHelperHK();
        $this->_WareHouseHelperKR = new WarehouseHelperKR();
        $this->_WareHouseHelperTW = new WarehouseHelperTW();
        $this->_urbanFoxHelper = new UrbanFoxHelper();

        // get order updates for all countries
        // $this->readOrderOutboundJob->handle();

        // create bulkorders
        // $this->createBulkOrderInboundJob->handle();

        // create orders
        // $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createOrderInbound_MYS();
        $this->createOrderInbound_HKG();
        $this->createOrderInbound_TWN();
        $this->createOrderInbound_SGP();
        $this->createOrderInbound_KOR();
        $this->readOrderOutboundJob = new ReadOrderOutboundJob();
        $this->createBulkOrderInboundJob = new CreateBulkOrderInboundJob();
    }

    public function createOrderInbound_MYS()
    {
        $this->Log->info('MY | createOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = OrderInboundQueueService::getAllHashMY();
        if (count($_orderIds) === 0) {
            try {
                throw new Exception("queue_empty");
            } catch (Exception $ex) {
                $this->Log->error('MY | createOrderInbound_MYS | ' . $ex->getMessage());
            }
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.MYS.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('MY | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('MY | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Malaysia - [Courier = GDEX]
                    if (isset($data->countryId) && $data->countryId === 1) {
                        if ($data->retry <= config('environment.warehouse_paths.MYS.retry')) {
                            $_send = $this->_WareHouseHelperMY->createOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('MY | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashMY($data->orderId);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashMY($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('MY | remove key from queue');
                                OrderInboundQueueService::deleteHashMY($data->orderId);
                            }
                        }
                    }
                    if (isset($data->CountryId) && $data->CountryId === 1) {
                            $_send = $this->_WareHouseHelperMY->createOrderInbound($data->id);
                            if ($_send !== 1) {
                                $this->Log->error('MY | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashMY($data->id);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashMY($data->id, $data->CountryId);
                            } else {
                                $this->Log->info('MY | remove key from queue');
                                OrderInboundQueueService::deleteHashMY($data->id);
                            }
                    }

                } catch (Exception $ex) {
                    var_dump($ex->getMessage());
                    $this->Log->error('MY | createOrderInbound_MYS | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createOrderInbound_HKG()
    {
        $this->Log->info('HK | createOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = OrderInboundQueueService::getAllHashHK();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('HK | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.HKG.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('HK | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('HK | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Hong Kong - [Courier = TAQBIN-HK]
                    if ($data->countryId === 2) {
                        if ($data->retry <= config('environment.warehouse_paths.HKG.retry')) {
                            $_send = $this->_WareHouseHelperHK->createOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('HK | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashHK($data->orderId);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashHK($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('HK | remove key from queue');
                                OrderInboundQueueService::deleteHashHK($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('HK | createOrderInbound_HKG | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createOrderInbound_SGP()
    {
        $this->Log->info('SG | createOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = OrderInboundQueueService::getAllHashSG();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('SG | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.SGP.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('SG | $_orderId => ' . json_encode($data));
                    //Warehouse - UrbanFox - Singapore - [Courier = COURIER]
                    if ($data->countryId === 7) {
                        if ($data->retry <= config('environment.warehouse_paths.SGP.retry')) {
                            $_send = $this->_urbanFoxHelper->createOrderInbound($data->orderId, $data->countryId, false);
                            $this->Log->info('RESULT: ==> ' . json_encode($_send));
                            if (is_array($_send)) {
                            if ($_send["errors"][0]["message"] == '"referenceNumber1" has already been taken') {
                                $this->Log->info('SG | remove key from queue');
                                OrderInboundQueueService::deleteHashSG($data->orderId);
                            } else {
                                $this->Log->error('SG | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashSG($data->orderId);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashSG($data->orderId, $data->countryId);
                            }
                         }else{
                            if ($_send =="statuschange") {
                                $this->Log->info('SG | remove key from queue(status change)');
                                OrderInboundQueueService::deleteHashSG($data->orderId);
                            }
                         }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('SG | createOrderInbound_SGP | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createOrderInbound_KOR()
    {
        $this->Log->info('KR | createOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = OrderInboundQueueService::getAllHashKR();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('KR | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.KOR.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('KR | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('KR | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - South Korea - [Courier = LOTTE]
                    if ($data->countryId === 8) {
                        if ($data->retry <= config('environment.warehouse_paths.KOR.retry')) {
                            $_send = $this->_WareHouseHelperKR->createOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('KR | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashKR($data->orderId);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashKR($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('KR | remove key from queue');
                                OrderInboundQueueService::deleteHashKR($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('KR | createOrderInbound_KOR | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createOrderInbound_TWN()
    {
        $this->Log->info('TW | createOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = OrderInboundQueueService::getAllHashTW();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('TW | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.TWN.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('TW | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('TW | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Malaysia - [Courier = TAQBIN-HK]
                    if ($data->countryId === 9) {
                        if ($data->retry <= config('environment.warehouse_paths.TWN.retry')) {
                            $_send = $this->_WareHouseHelperTW->createOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('TW | retry & add to queue');
                                // retry
                                OrderInboundQueueService::deleteHashTW($data->orderId);
                                // data.retry += 1;
                                OrderInboundQueueService::addHashTW($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('TW | remove key from queue');
                                OrderInboundQueueService::deleteHashTW($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('TW | createOrderInbound_TWN | error => ' . $ex->getMessage());
                }
            }
        }
    }
}
