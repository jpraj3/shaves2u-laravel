<?php

namespace App\Jobs;

// Models
use App\Models\CancellationJourney\CancellationFollowUp;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Plans\Plans;
use App\Models\GeoLocation\Countries;

// Helpers
use App\Helpers\UserHelper;
use App\Helpers\SubscriptionHelper;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Illuminate\Console\Command;
use Carbon\Carbon;

class CancellationJourneyEdmJob extends Command
{
    protected $signature = 'cancellation-journey-edm:job';
    protected $description = 'Cancellation journey edm job.';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->Job = 'Cancellation-Journey-Edm: ';
        $this->Log = \Log::channel('cronjob');
    }

    public function handle()
    {
        $this->userHelper = new UserHelper();
        $this->GetAllCancellationFollowUpStatus();
    }

    private function GetAllCancellationFollowUpStatus()
    {
        $allRecords = CancellationFollowUp::where('ReminderDate', Carbon::now()->toDateString())->get();

        foreach ($allRecords as $record) {

            $cancellationFollowUpId = $record->id;
            $subsId = $record->SubscriptionId;
            $subs = Subscriptions::where('id',$subsId)->first();
            $planId = $subs->PlanId;
            $userId = $record->UserId;
            $cancelEdm2_Status = $record->CancelEdm2;
            $cancelEdm3_Status = $record->CancelEdm3;
            $cancelEdm4_Status = $record->CancelEdm4;
            $cancelEdm5_Status = $record->CancelEdm5;

            // Get country id
            $plan = Plans::where('id', $subs->PlanId)->first();
            $country = Countries::where('id', $plan->CountryId)->first();

            // Initialize data for email
            $currentCountry = $country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($userId, $planId);
            $emailData = [];

            // Send corresponding cancellation journey edm
            if($cancelEdm2_Status === 0)
            {
                // Update cancellation follow up table
                CancellationFollowUp::where('id', $cancellationFollowUpId)->update(
                    [
                        "CancelEdm2" => 1,
                        "NextJourneyStatus" => 3,
                        "JourneyDesc" => 'Journey EDM 2',
                        "ReminderDate" => Carbon::parse($record->ReminderDate)->addDays(7)->toDateString()
                    ]
                );

                // Add Cancellation Journey Two email to queue
                $emailData['title'] = 'reactivate';
                $emailData['moduleData'] = (Object) array(
                    'email' => $subs->email,
                    'subscriptionId' => $subsId,
                    'userId' => $userId,
                    'countryId' => $country->id
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
                EmailQueueService::addHash($userId,'reactivate',$emailData);
            }
            else if($cancelEdm3_Status === 0)
            {
                // Update cancellation follow up table
                CancellationFollowUp::where('id', $cancellationFollowUpId)->update(
                    [
                        "CancelEdm3" => 1,
                        "NextJourneyStatus" => 4,
                        "JourneyDesc" => 'Journey EDM 3',
                        "ReminderDate" => Carbon::parse($record->ReminderDate)->addDays(4)->toDateString()
                    ]
                );

                // Add Cancellation Journey Three email to queue
                $emailData['title'] = 'reminder-promo-7day';
                $emailData['moduleData'] = (Object) array(
                    'email' => $subs->email,
                    'subscriptionId' => $subsId,
                    'userId' => $userId,
                    'countryId' => $country->id
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
                EmailQueueService::addHash($userId,'reminder_promo_7day',$emailData);
            }
            else if($cancelEdm4_Status === 0)
            {
                // Update cancellation follow up table
                CancellationFollowUp::where('id', $cancellationFollowUpId)->update(
                    [
                        "CancelEdm4" => 1,
                        "NextJourneyStatus" => 5,
                        "JourneyDesc" => 'Journey EDM 4',
                        "ReminderDate" => Carbon::parse($record->ReminderDate)->addDays(3)->toDateString()
                    ]
                );

                // Add Cancellation Journey Four email to queue
                $emailData['title'] = 'reminder-promo-3day';
                $emailData['moduleData'] = (Object) array(
                    'email' => $subs->email,
                    'subscriptionId' => $subsId,
                    'userId' => $userId,
                    'countryId' => $country->id
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
                EmailQueueService::addHash($userId,'reminder_promo_3day',$emailData);
            }
            else if($cancelEdm5_Status === 0)
            {
                // Update cancellation follow up table
                CancellationFollowUp::where('id', $cancellationFollowUpId)->update(
                    [
                        "CancelEdm5" => 1,
                        "NextJourneyStatus" => 0,
                        "JourneyDesc" => 'Journey EDM 5'
                    ]
                );

                // Add Cancellation Journey Five email to queue
                $emailData['title'] = 'goodbye';
                $emailData['moduleData'] = (Object) array(
                    'email' => $subs->email,
                    'subscriptionId' => $subsId,
                    'userId' => $userId,
                    'countryId' => $country->id
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
                EmailQueueService::addHash($userId,'goodbye',$emailData);
            }
        }
    }
}
