<?php

namespace App\Jobs;

use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Subscriptions\Subscriptions;
use App\Queue\SendEmailQueueService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Exception;
class SendReferralInvite7Days extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-referral-invite-after-7-days:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Referral Invite after 7 Days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('SendReferralInvite7Days start');
    }

    public function handle()
    {
        $this->followUp_7days();
    }

    public function referralInviteAfter7DaysRenewal()
    {
        return Subscriptions::where('hasSendFollowUp1', true)
            ->where('hasSendReferralInvite', false)
            ->where('isTrial', true)
            ->where('status', 'Processing')
            ->where('convertTrial2Subs', Carbon::now()->subDays(7)->toDateString())->get();
    }

    public function getRewardByCountry($id)
    {
        return RewardsCurrency::where('CountryId', $id)->first();
    }

    public function followUp_7days()
    {
        $this->Log->info('SendReferralInvite7Days => followUp_7days start');
        $subscriptions = $this->referralInviteAfter7DaysRenewal();
        $defLang = '';
        $currencyCode = '';
        $referralCreditValue = '';
        foreach ($subscriptions as $subscription) {
            $user = User::where('id', $subscription->UserId)->first();
            if ($user) {
                $defLang = $user->defaultLanguage;
                $country = Countries::where('id', $user->CountryId)->first();
                if ($country) {
                    $currencyCode = $country->currencyCode;
                    $currencyDisplay = $country->currencyDisplay;
                    $reward_country = $this->getRewardByCountry($country->id);
                    if ($reward_country) {
                        $referralCreditValue = $reward_country->value;
                        try {
                            // Getting Country & Locale Data From $subs
                            $currentCountry = json_encode($country);
                            $currentLocale = $defLang;
                            $emailData = [];
                            // Passing $subs Data into $moduleData for Email
                            $emailData['title'] = 'referral-subscription-invite';
                            $emailData['moduleData'] = (Object) array(
                                'email' => $subscription->email,
                                'countryId' => $country->id,
                                'currentCountry' => $currentCountry,
                                'currentLocale' => $currentLocale,
                                'referralCreditValue' => $referralCreditValue,
                                'currencyDisplay' => $currencyDisplay,
                                'currencyCode' => $currencyCode,
                                'value' => $currencyDisplay . $referralCreditValue,
                            );
                            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                             // hide email redis
                            SendEmailQueueService::addHash('referral_subscription_invite_' . \App\Helpers\LaravelHelper::GenerateUUID(), 'referral_subscription_invite', $emailData);
                            $this->Log->info('Dispatched referral email invite after 7 days EDM for email: ' . $subscription->email . '.');

                            Subscriptions::where('id', $subscription->id)->update(["hasSendReferralInvite" => true]);

                        } catch (Exception $e) {
                            $this->Log->error('Dispatched referral email invite after 7 days EDM for email: ' . $subscription->email . '.' . $e->getMessage());
                        }
                    }
                }
            }
        }
    }
}
