<?php

namespace App\Jobs;

use App\Helpers\UserHelper;
use Illuminate\Console\Command;
use App\Models\GeoLocation\Countries;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Ecommerce\User;
use App\Helpers\Email\EmailQueueHelper;
use Exception;

class RetriggerReferralCashoutEmailJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retrigger-referral-cashout-email:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'retrigger Referral Cashout email to Shaves2u';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->Log = \Log::channel('cronjob');
        $this->userHelper = new UserHelper();
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->Log->info('RetriggerReferralCashoutEmailJob start');

        $cashouts = ReferralCashout::where('referralcashouts.status', "active")
            ->whereNotNull('referralcashouts.bank_name')
            ->whereNotNull('referralcashouts.bank_account_no')
            ->where('referralcashouts.bank_name','<>','')
            ->where('referralcashouts.bank_account_no','<>','')
            ->get();

        foreach ($cashouts as $cashout) {
            $userData = User::where('id', $cashout->UserId)->first();
            $currentCountry = Countries::where('id', $cashout->CountryId)->first();
            $currentLocale = 'en';
            try {
                // Getting Country & Locale Data From $subs
                $emailData = [];
                // Passing $subs Data into $moduleData for Email
                $emailData['title'] = 'referral-cashout-request';
                $emailData['moduleData'] = array(
                    'email' => config('environment.email-blasting.referral-cashouts'),
                    'user_email' => $userData->email,
                    'country' => $currentCountry->name,
                    'currency' => $currentCountry->currencyCode,
                    'amount' => $cashout->amount,
                    'bank_name' => $cashout->bank_name,
                    'full_name' => $cashout->name,
                    'bank_acc_no' => $cashout->bank_account_no,
                    'datetime' => $cashout->created_at,
                    'userId' => $cashout->UserId,
                    'countryId' => $cashout->CountryId,
                );
                $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                
                $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);

                // Update hasSendFollowUp1 column
                ReferralCashOut::where('id', $cashout->id)->update(['status' => 'in_process']);
            } catch (Exception $e) {
                $this->Log->info('RetriggerReferralCashoutEmailJob error'.$e);
            }
        }
    }
}
