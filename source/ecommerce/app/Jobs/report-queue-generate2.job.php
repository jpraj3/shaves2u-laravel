<?php

namespace App\Jobs;

use Illuminate\Console\Command;

use App\Services\OrderService;
use App\Helpers\Payment\StripeBA as StripeBA;
use App\Helpers\PlanHelper;
use App\Models\Ecommerce\User;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductTranslate;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanTrialProducts;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use App\Models\Geolocation\Countries;
use App\Models\Geolocation\LanguageDetails;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\FileUploads\FileUploads;
use App\Models\CancellationJourney\CancellationReasonTranslates;
use Illuminate\Support\Facades\Redis;
use App\Models\Rebates\Referral;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Rebates\RewardsIn;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SummarySubscriber;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\Plans\PausePlanHistories;
use App\Models\Reports\QueueReport;
use App\Models\Reports\SummarySalesReport;
use App\Queue\SendEmailQueueService;
use App\Queue\CSVReportToDownloadQueueService;
use App\Exports\SubscribersExport;
use App\Exports\ReportMasterExport;
use App\Exports\ReportAppcoExport;
use App\Exports\ReportMOExport;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;
use Exception;
use Excel;
use App\Helpers\AWSHelper;
use Illuminate\Support\Facades\Storage;

class ReportQueueGenerate2Job extends Command
{

    protected $signature = 'report-queue-generate2:job';
    protected $description = 'Generate Report by Queue';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->awsHelper = new AWSHelper();
        $this->orderService = new OrderService;
        $this->stripeBA = new StripeBA();
        $this->planHelper = new PlanHelper();
        $this->Log = \Log::channel('cronjob');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $reportQueueDatas = CSVReportToDownloadQueueService::getAllHash();
            $countloop = 0;

            if (count($reportQueueDatas) > 0) {
                $countQueueReport = QueueReport::where('status', 'Processing')
                    ->count();
                if ($countQueueReport < 1) {
                    ini_set('memory_limit', '5000M');
                    foreach ($reportQueueDatas as $reportqueue) {
                        if ($countloop == 0) {
                            $reportqueue = json_decode($reportqueue);

                            QueueReport::where('id', $reportqueue->queueid)->update([
                                'status' => 'Processing',
                            ]);

                            if ($reportqueue->reportType == "subscribers") {
                                $this->GenerateSubscriber($reportqueue);
                            }
                            if ($reportqueue->reportType == "subscriberssummary") {
                                $this->GenerateSubscriberSummary($reportqueue);
                            }
                            if ($reportqueue->reportType == "reportsmaster") {
                                $this->GenerateReportsMaster($reportqueue);
                            }
                            if ($reportqueue->reportType == "reportsmastersummary") {
                                $this->GenerateReportsMasterSummary($reportqueue);
                            }
                            if ($reportqueue->reportType == "reportsappco") {
                                $this->GenerateReportsAppco($reportqueue);
                            }
                            if ($reportqueue->reportType == "reportsmo") {
                                $this->GeneraterReportsMO($reportqueue);
                            }
                        }
                        $countloop++;
                    }
                    ini_set('memory_limit', '2000M');
                }
            }
        } catch (Exception $e) {
            \Log::channel('cronjob')->info('Generate report cronjob error = ' . $e->getMessage());
        }
    }

    public function GenerateSubscriber($data)
    {
        try {
            $this->Log->info('GenerateSubscriber Process start.');
            $maxresults = null;
            $search = null;
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }

            $subscriptions = DB::table('subscriptions')
                ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
                ->leftJoin('rechargehistories', 'rechargehistories.subscriptionId', 'subscriptions.id')
                ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
                ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
                ->leftJoin('deliveryaddresses as ShippingAddress', 'subscriptions.DeliveryAddressId', '=', 'ShippingAddress.id')
                ->leftJoin('deliveryaddresses as BillingAddress', 'subscriptions.BillingAddressId', '=', 'BillingAddress.id')
                ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
                ->leftJoin('sellerusers', 'subscriptions.SellerUserId', '=', 'sellerusers.id')
                ->leftJoin('marketingoffices', 'subscriptions.MarketingOfficeId', '=', 'marketingoffices.id')
                ->leftJoin('marketingoffices as smarketingoffices', 'sellerusers.MarketingOfficeId', '=', 'smarketingoffices.id')
                ->leftJoin('pause_plan_histories', function ($query) {
                    $query->on('pause_plan_histories.subscriptionIds', '=', 'subscriptions.id');
                    $query->take(1);
                })
                ->select(
                    'subscriptions.id as id',
                    'subscriptions.*',
                    'subscriptions.status as substatus',
                    'subscriptions.cancellationReason as subcancellationReason',
                    'subscriptions.created_at as subcreatedat',
                    'subscriptions.updated_at as subupdatedAt',
                    'ShippingAddress.id as shippingaddressid',
                    'ShippingAddress.address as saddress',
                    'ShippingAddress.portalCode  as sportalCode',
                    'ShippingAddress.city as scity',
                    'ShippingAddress.state as sstate',
                    'BillingAddress.id as billingaddressid',
                    'BillingAddress.address as baddress',
                    'BillingAddress.portalCode  as bportalCode',
                    'BillingAddress.city as bcity',
                    'BillingAddress.state as bstate',
                    'countries.codeIso as countrycodeIso',
                    'countries.name as countryName',
                    'plansku.duration',
                    'plansku.sku',
                    'plansku.isAnnual',
                    'sellerusers.badgeId',
                    'marketingoffices.moCode',
                    'smarketingoffices.moCode as smoCode',
                    'pause_plan_histories.id as pauseid',
                    'users.id as uid',
                    'users.firstName as ufirstName',
                    'users.lastName as ulastName'
                )
                ->when($search != null, function ($q) use ($search) {
                    return $q->where('subscriptions.email', 'LIKE', '%' . $search . '%');
                })
                ->when($country != null, function ($q) use ($country) {
                    if ($country === "all") {
                        return $q;
                    } else {
                        return $q->where('plans.CountryId', $country);
                    }
                })
                ->when($sku != null, function ($q) use ($sku) {
                    if ($sku === "all") {
                        return $q;
                    } else {
                        return $q->where('plans.PlanSkuId', $sku);
                    }
                })
                ->when($plantype != null, function ($q) use ($plantype) {
                    if ($plantype === "trial") {
                        return $q->where('subscriptions.isTrial', 1);
                    } else if ($plantype === "custom") {
                        return $q->where('subscriptions.isCustom', 1);
                    } else {
                        return $q;
                    }
                })
                ->when($fromdate != null, function ($q) use ($fromdate) {
                    return $q->whereDate('subscriptions.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                })
                ->when($todate != null, function ($q) use ($todate) {
                    return $q->whereDate('subscriptions.created_at', '<=', date("Y-m-d", strtotime($todate)));
                })
                ->when($status != null, function ($q) use ($status) {
                    if ($status === "all") {
                        return $q;
                    } else {
                        return $q->where('subscriptions.status', $status);
                    }
                })
                ->orderBy('subscriptions.created_at', 'DESC')
                // ->distinct()
                ->get();
            $data1 = [];
            $countdata = 1;
            foreach ($subscriptions as $sub) {
                $_order = Orders::select('id', 'created_at')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                // $current_order = Orders::select('status')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                if (isset($sub->ufirstName) && isset($sub->ulastName)) {
                    $sub->ufullname = $sub->ufirstName . " " . $sub->ulastName;
                } else if (!isset($sub->ufirstName) && isset($sub->ulastName)) {
                    $sub->ufullname = $sub->ulastName;
                } else {
                    $sub->ufullname = $sub->ufirstName;
                }

                if ($_order !== null) {
                    $sub->formattedId = $this->orderService->formatOrderNumberV3($_order->id, $_order->created_at, $sub->countrycodeIso, true);
                    if ($sub->isTrial === 1 && $sub->convertTrial2Subs) {
                        //take second order
                        $sub->conversion_date = isset($sub->convertTrial2Subs) ? $sub->convertTrial2Subs : '-';
                    } else {
                        $sub->conversion_date = null;
                    }
                    // $sub->currentOrderStatus = null;
                    // foreach ($_order as $order) {
                    //     if($order->id ==  $sub->currentOrderId){
                    //         $sub->currentOrderStatus = $order->status;
                    //     }
                    // }
                } else {
                    $sub->formattedId = "";
                }

                // if ($current_order !== null) {
                //     $sub->currentOrderStatus = $current_order->status;
                // }

                $sub->created_date = $sub->subcreatedat;

                if ($sub->shippingaddressid) {
                    $sub->deliveryaddress = $sub->saddress . " " . $sub->sportalCode . " " . $sub->scity . " " . $sub->sstate;
                } else {
                    $sub->deliveryaddress = "";
                }

                if ($sub->billingaddressid) {
                    $sub->billingaddress = $sub->baddress . " " . $sub->bportalCode . " " . $sub->bcity . " " . $sub->bstate;
                } else {
                    $sub->billingaddress = "";
                }

                if ($sub->MarketingOfficeId) {
                    $sub->moCode = $sub->moCode;
                } else {
                    if ($sub->smoCode) {
                        $sub->moCode = $sub->smoCode;
                    } else {
                        $sub->moCode = "";
                    }
                }
                // $sub->cancellationReason = "";
                // $sub->canceledDate = "";
                // if ($sub->substatus == 'Cancelled') {
                //     $sub->cancellationReason = $sub->subcancellationReason;
                //     if ($sub->canceldate) {
                //         $sub->canceledDate = date('d/m/y', strtotime($sub->canceldate));
                //     }
                //     else {
                //         $sub->canceledDate = date('d/m/y', strtotime($sub->subupdatedAt));
                //     }
                //   }

                if ($sub->substatus == "Cancelled") {
                    $cancellation = DB::table('cancellation_journeys')->where('SubscriptionId', $sub->id)->orderBy('created_at', 'DESC')->first();

                    if (isset($cancellation)) {
                        $sub->cancellationcreated_date = $cancellation->created_at;
                        $sub->cancellationotherreason = $cancellation->OtherReason;
                        $cancellationTranslates = CancellationReasonTranslates::where("id", $cancellation->CancellationTranslatesId)->first();
                        if ($cancellationTranslates) {
                            $sub->cancellationdefaultContent = $cancellationTranslates->defaultContent;
                        } else {
                            $sub->cancellationdefaultContent = "";
                        }
                    } else {
                        $cancellation1 = DB::table('subscriptionhistories')
                            ->where('subscriptionId', $sub->id)
                            ->where(function ($query) {
                                $query->orWhere('message',  'Canceled')
                                    ->orWhere('message', 'Cancelled');
                            })
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if (isset($cancellation1)) {
                            $sub->cancellationcreated_date = $cancellation1->created_at;
                        }
                    }

                    if (isset($sub->cancellationdefaultContent)) {
                        if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                            $sub->cancellationdefaultContent = $sub->subcancellationReason . "," . $sub->cancellationdefaultContent;
                        } else {
                            $sub->cancellationdefaultContent = "";
                        }
                    } else {
                        if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                            $sub->cancellationdefaultContent = $sub->subcancellationReason;
                        } else {
                            $sub->cancellationdefaultContent = "";
                        }
                    }
                } else if ($sub->substatus === "Payment Failure") {
                    $sub->cancellation = DB::table('subscriptionhistories')
                        ->where('subscriptionId', $sub->id)
                        ->where('message', 'Payment Failure')
                        ->orderBy('created_at', 'DESC')
                        ->first();

                    if (isset($sub->cancellation)) {
                        $sub->cancellationdefaultContent = $sub->cancellation->detail;
                        // $sub->cancellationcreated_date = $sub->cancellation->created_at;
                    }

                    if ($sub->subcancellationReason === "Cancelled due to payment failed" || $sub->subcancellationReason === "Canceled due to payment failed" || $sub->subcancellationReason === "Cancelled due to user card has insufficient funds.") {
                        $sub->cancellationdefaultContent = $sub->subcancellationReason;
                    } else {
                        $sub->cancellationdefaultContent = "-";
                    }
                }

                $sub->blade_type = null;
                $sub->shave_cream = null;
                $sub->after_shave_cream = null;
                $sub->pouch = null;
                $sub->rubber_handle = null;

                $products = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                    ->leftJoin('plan_details', 'productcountries.id', '=', 'plan_details.ProductCountryId')
                    ->where('plan_details.PlanId', $sub->PlanId)
                    ->select('products.*', 'plan_details.qty as pd_qty')
                    ->get();

                foreach ($products as $product) {
                    if (in_array($product->sku, config('global.all.blade_types.skus'))) {
                        $sub->blade_type = config('global.all.blade_no_v2.' . $product->sku) . ' blade';
                        $sub->blade_quantity = $product->pd_qty;
                    }
                }

                // if ($sub->isTrial === 1 && ($sub->currentCycle === 1 || $sub->currentCycle === 0)) {
                //     $sub->blade_quantity = 1;
                // }

                $addons = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                    ->leftJoin('subscriptions_productaddon', 'productcountries.id', '=', 'subscriptions_productaddon.ProductCountryId')
                    ->where('subscriptions_productaddon.subscriptionId', $sub->id)
                    ->select('products.*')
                    ->get();

                foreach ($addons as $addon) {
                    if (in_array($addon->sku, config('global.all.aftershavecream_types.skus'))) {
                        $sub->after_shave_cream = $addon->id;
                    }
                    if (in_array($addon->sku, config('global.all.shavecream_types.skus'))) {
                        $sub->shave_cream = $product->id;
                    }
                    if (in_array($addon->sku, config('global.all.pouch_types.skus'))) {
                        $sub->pouch = $addon->id;
                    }
                    if (in_array($addon->sku, config('global.all.razor_potector.skus'))) {
                        $sub->rubber_handle = $addon->id;
                    }
                }


                if ($sub->pauseid) {
                    $sub->pauseplan = "Yes";
                } else {
                    $sub->pauseplan = "No";
                }
                $sub->reactivatedplan = null;

                if ($sub->currentDeliverNumber == 0 && $sub->isCustom == 1 && $sub->isTrial != 1 && ($sub->substatus !== "Payment Failure" && $sub->substatus !== "On Hold" && $sub->substatus !== "Pending")) {
                    $sub->currentDeliverNumber = "1";
                }

                $e = [
                    "UserID" => $sub !== null ? (isset($sub->uid) && $sub->uid  != "" ? $sub->uid  : '-') : '-',
                    "CustomerName" => $sub !== null ? (isset($sub->ufullname) && $sub->ufullname  != "" ? $sub->ufullname  : '-') : '-',
                    "Email" =>  $sub !== null ? (isset($sub->email) && $sub->email  != "" ? $sub->email  : '-') : '-',
                    "Country" => $sub !== null ? (isset($sub->countryName) && $sub->countryName  != "" ? $sub->countryName  : '-') : '-',
                    "Category" => $sub !== null ? (isset($sub->isOffline) && $sub->isOffline == 1 ? "BA" : "eCommerce") : '-',
                    "MOCode" => $sub !== null ? (isset($sub->moCode) && $sub->moCode  != "" ? $sub->moCode  : '-') : '-',
                    "BadgeID" => $sub !== null ? (isset($sub->badgeId) && $sub->badgeId  != "" ? $sub->badgeId  : '-') : '-',
                    "DeliveryAddress" => $sub !== null ? (isset($sub->deliveryaddress) && $sub->deliveryaddress  != "" ? $sub->deliveryaddress  : '-') : '-',
                    "PlanType" => $sub !== null ? (isset($sub->isTrial) && $sub->isTrial == 1 ? "TK" : (isset($sub->isCustom) && $sub->isCustom == 1 ? "Custom" : "Custom")) : 'Custom',
                    "BladeType" => $sub !== null ? (isset($sub->blade_type) && $sub->blade_type !== null ? $sub->blade_type : "0") : '-',
                    "DeliveryInterval" => $sub !== null ? (isset($sub->isAnnual) && $sub->isAnnual != 0 ? '12' : (isset($sub->duration) && $sub->duration != "" ? $sub->duration  : '-')) : '-',
                    "CassetteCycle" => $sub !== null ? (isset($sub->blade_quantity) && $sub->blade_quantity  != "" ? $sub->blade_quantity  : '-') : '-',
                    "CycleCount" => $sub !== null ?  (isset($sub->currentDeliverNumber) && $sub->currentDeliverNumber  != "" ? $sub->currentDeliverNumber  : '0') : '0',
                    "AfterShaveCream" => $sub !== null ? (isset($sub->after_shave_cream) && $sub->after_shave_cream !== null ? "1" : "0") : "0",
                    "ShaveCream" => $sub !== null ? (isset($sub->shave_cream) && $sub->shave_cream !== null ? "1" : "0") : "0",
                    "Pouch" => $sub !== null ? (isset($sub->pouch) && $sub->pouch !== null ? "1" : "0") : "0",
                    "RubberHandle" => $sub !== null ? (isset($sub->rubber_handle) && $sub->rubber_handle !== null ? "1" : "0") : "0",
                    "SignUp" => $sub !== null ? (isset($sub->created_date) && $sub->created_date  != "" ? Carbon::parse($sub->created_date)->format('Y-m-d') : '-') : '-',
                    "Conversion" => $sub !== null ? (isset($sub->conversion_date) && $sub->conversion_date  != "" ? $sub->conversion_date  : '-') : '-',
                    "LastDelivery" => $sub !== null ? (isset($sub->lastDeliverydate) && $sub->lastDeliverydate  != "" ? Carbon::parse($sub->lastDeliverydate)->format('Y-m-d') : '-') : '-',
                    "LastCharge" => $sub !== null ? (isset($sub->currentOrderTime) && $sub->currentOrderTime  != "" ? Carbon::parse($sub->currentOrderTime)->format('Y-m-d') : '-') : '-',
                    "NextCharge" => $sub !== null ? (isset($sub->nextChargeDate) && $sub->nextChargeDate  != "" ? $sub->nextChargeDate  : '-') : '-',
                    "RechargeAttempt" => $sub !== null ? (isset($sub->recharge_date) && $sub->recharge_date !== null ? "Yes" : "No") : "No",
                    "RechargeCount" => $sub !== null ? (isset($sub->total_recharge) && $sub->total_recharge !== null ? $sub->total_recharge : "0") : '0',
                    "CancellationDate" => $sub !== null ? (isset($sub->cancellationcreated_date) && $sub->cancellationcreated_date  != "" ?  Carbon::parse($sub->cancellationcreated_date)->format('Y-m-d') : '-') : '-',
                    "Status" => $sub !== null ? (isset($sub->substatus) && $sub->substatus  != "" ? $sub->substatus  : '-') : '-',
                    "CancellationReason" => $sub !== null ? (isset($sub->cancellationdefaultContent) && $sub->cancellationdefaultContent  != "" ? $sub->cancellationdefaultContent  : '-') : '-',
                    "OtherReason" => $sub !== null ? (isset($sub->cancellationotherreason) && $sub->cancellationotherreason  != "" ? $sub->cancellationotherreason  : '-') : '-',
                    "PauseSubscription" => $sub !== null ? (isset($sub->pauseplan) && $sub->pauseplan  != "" ? $sub->pauseplan  : '-') : '-',
                    "OnHoldDate" => $sub !== null ? $sub->OnHoldDate : '-',
                    "UnrealizedDate" => $sub !== null ? $sub->UnrealizedDate : '-',
                    // "created_at" => \Carbon\Carbon::now(),
                    // "updated_at" => \Carbon\Carbon::now(),

                ];
                array_push($data1, $e);
                $countdata++;
            }
            $url = $data->url;
            Excel::store(new SubscribersExport($data1), config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

            FileUploads::where('file_id', $data->file_id)->update([
                'url' => $url,
                'status' => 'Completed',
                'isDownloaded' => 1,
            ]);

            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Finished',
            ]);

            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            $logvalue = "GenerateSubscriber Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Subscriber report cronjob error = ' . $e->getMessage());
        }
    }

    public function GenerateSubscriberSummary($data)
    {
        try {
            $this->Log->info('GenerateSubscriberSummary Process start.');
            $maxresults = null;
            $search = null;
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
                if ($country != 'all') {
                    $countget = DB::table('countries')
                        ->select('name')
                        ->where('id', $country)
                        ->first();
                    $country = $countget->name;
                }
            } else {
                $country = 'all';
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }
            // $query = DB::raw('CASE 
            // WHEN BulkOrderId IS NULL THEN OrderId
            // ELSE BulkOrderId
            // END AS "Order"');
            $subscriptionss = DB::table('summary_subscribers')
            ->select(
                "UserID",
                "CustomerName",
                "Email",
                "Country",
                "Category",
                "MOCode",
                "BadgeID",
                "DeliveryAddress",
                "PlanType",
                "BladeType",
                "DeliveryInterval",
                "CassetteCycle",
                "CycleCount",
                "AfterShaveCream",
                "ShaveCream",
                "Pouch",
                "RubberHandle",
                "SignUp",
                "Conversion",
                "LastDelivery",
                "LastCharge",
                "NextCharge",
                "RechargeAttempt",
                "RechargeCount",
                "CancellationDate",
                "Status",
                "CancellationReason",
                "OtherReason",
                "PauseSubscription",
                "OnHoldDate",
                "UnrealizedDate"
            )
            ->when($search != null, function ($q) use ($search) {
                return $q->where('Email', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('Country', $country);
                }
            })

            ->when($plantype != null, function ($q) use ($plantype) {
                if ($plantype === "trial") {
                    return $q->where('PlanType', "TK");
                } else if ($plantype === "custom") {
                    return $q->where('PlanType', "Custom");
                } else {
                    return $q;
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('Status', $status);
                }
            })
            ->orderBy('created_at', 'DESC')
            // ->distinct()
            ->get();

        $totalget = $subscriptionss->count();

        QueueReport::where('id', $data->queueid)->update([
            'total' => $totalget
        ]);
        // Excel::store(new SubscribersExport([]),  'report/' . $data->filename);
        $localpath  = storage_path('app/report/' . $data->filename);
        $fp = fopen($localpath, 'w');
        fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        fputcsv($fp, [
            "User ID",
            "Customer Name",
            "Email",
            "Country",
            "Category",
            "MO Code",
            "Badge ID",
            "Delivery Address",
            "Plan Type",
            "Blade Type",
            "Delivery Interval",
            "Cassette / Cycle",
            "No. of Cycle",
            "After Shave Cream",
            "Shave Cream",
            "Toiletry Bag",
            "Razor Protector",
            "Sign Up",
            "Conversion",
            "Last Delivery",
            "Last Charge",
            "Next Charge",
            "Recharge Attempt",
            "No. of Recharge",
            "Cancellation Date",
            "Status",
            "Cancellation Reason",
            "Other Reason",
            "Opted in For pausing subscription",
            "OnHold Date",
            "Unrealized Date"
            // "Opted in For Reactivating subscription plan",
        ]);
            foreach ($subscriptionss as $sub) {
                $data1 = [];
                $e = [];

                $e = [
                    "UserID" => $sub->UserID,
                    "CustomerName" => $sub->CustomerName,
                    "Email" =>  $sub->Email,
                    "Country" => $sub->Country,
                    "Category" => $sub->Category,
                    "MOCode" => $sub->MOCode,
                    "BadgeID" => $sub->BadgeID,
                    "DeliveryAddress" => $sub->DeliveryAddress,
                    "PlanType" => $sub->PlanType,
                    "BladeType" => $sub->BladeType,
                    "DeliveryInterval" => $sub->DeliveryInterval,
                    "CassetteCycle" => $sub->CassetteCycle,
                    "CycleCount" => $sub->CycleCount,
                    "AfterShaveCream" => $sub->AfterShaveCream,
                    "ShaveCream" => $sub->ShaveCream,
                    "Pouch" => $sub->Pouch,
                    "RubberHandle" => $sub->RubberHandle,
                    "SignUp" => $sub->SignUp == "-" ? "-" : Carbon::parse($sub->SignUp)->format('Y-m-d'),
                    "Conversion" =>  $sub->Conversion == "-" ? "-" : Carbon::parse($sub->Conversion)->format('Y-m-d'),
                    "LastDelivery" =>  $sub->LastDelivery == "-" ? "-" : Carbon::parse($sub->LastDelivery)->format('Y-m-d'),
                    "LastCharge" =>  $sub->LastCharge == "-" ? "-" : Carbon::parse($sub->LastCharge)->format('Y-m-d'),
                    "NextCharge" =>  $sub->NextCharge == "-" ? "-" : Carbon::parse($sub->NextCharge)->format('Y-m-d'),
                    "RechargeAttempt" => $sub->RechargeAttempt,
                    "RechargeCount" => $sub->RechargeCount,
                     "CancellationDate" => ($sub->CancellationDate == "0000-00-00" || $sub->CancellationDate == "-") ? "-" : Carbon::parse($sub->CancellationDate)->format('Y-m-d'),
                    "Status" => $sub->Status,
                    "CancellationReason" => $sub->CancellationReason,
                    "OtherReason" => $sub->OtherReason,
                    "PauseSubscription" => $sub->PauseSubscription,
                    "OnHoldDate" => $sub->OnHoldDate,
                    "UnrealizedDate" => $sub->UnrealizedDate,
                    // "created_at" => \Carbon\Carbon::now(),
                    // "updated_at" => \Carbon\Carbon::now(),

                ];
                array_push($data1, $e);
                            
                foreach ($data1 as $fields) {
                    fputcsv($fp, $fields);
                }

            }
            $url = $data->url;
            fclose($fp);
            $localFilepath = file_get_contents($localpath);
            Storage::disk('s3')->put(config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, $localFilepath);
            $this->deleteFile($localpath);
            // Excel::store(new SubscribersExport($data1), config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

            FileUploads::where('file_id', $data->file_id)->update([
                'url' => $url,
                'status' => 'Completed',
                'isDownloaded' => 1,
            ]);

            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Finished',
            ]);

            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            $logvalue = "GenerateSubscriberSummary Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Subscriber Summary report cronjob error = ' . $e->getMessage());
        }
    }

    public function GenerateReportsMaster($data)
    {
        try {
            $this->Log->info('GenerateReportsMaster Process start.');
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
            }

            if (isset($data->channel)) {
                $channel = $data->channel;
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }

            $reports = collect();
            DB::table('salereports')
                ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
                ->leftJoin('bulkorders', 'salereports.bulkOrderId', '=', 'bulkorders.id')
                // ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
                // ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
                ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
                ->leftJoin('subscriptions', 'subscriptions.id', '=', 'orders.subscriptionIds')
                ->leftJoin('cards', 'cards.id', '=', 'subscriptions.CardId')
                ->leftJoin('countries', 'countries.id', '=', 'salereports.CountryId')
                // ->leftJoin('cards', function ($join) {
                //     $join->on('orders.UserId', '=', 'cards.UserId');
                //     $join->on('receipts.last4', '=', 'cards.cardNumber');
                //     $join->on('receipts.branchName', '=', 'cards.branchName');
                //     $join->on('receipts.expiredYear', '=', 'cards.expiredYear');
                //     $join->on('receipts.expiredMonth', '=', 'cards.expiredMonth');
                // })
                ->select(
                    'countries.codeIso as OrderCountryCodeIso',
                    'countries.includedTaxToProduct as isIncludedTaxToProduct',
                    'subscriptions.isTrial as SubscriptionIsTrial',
                    //remove master report
                    'subscriptions.id as Subscriptionid',
                    'subscriptions.PlanId as SubscriptionPlanId',
                    //remove master report
                    // 'deliveryaddresses.portalCode as portalCode',
                    // 'billing.portalCode as bportalCode',
                    'salereports.*',
                    'salereports.created_at as screated_at',
                    'orders.subscriptionIds as subscriptionIds',
                    'orders.imp_uid as imp_uid',
                    'orders.payment_status as paymentStatus',
                    'orders.carrierAgent as carrierAgent',
                    'orders.UserId as UserId',
                    'orders.isSubsequentOrder as isSubsequentOrder',
                    'orders.DeliveryAddressId as odaid',
                    'orders.BillingAddressId as obaid',
                    'orders.created_at as order_created_at',
                    'bulkorders.carrierAgent as bulkCarrierAgent',
                    'bulkorders.created_at as bulkorder_created_at',
                    'receipts.chargeId as rchargeId',
                    'receipts.branchName as rbranchName',
                    'cards.cardType as cCardType'
                )
                ->when($channel != null, function ($q) use ($channel) {
                    if ($channel === "all") {
                        return $q;
                    } else if ($channel === "seller") {
                        return $q->where('salereports.category', "sales app");
                    } else if ($channel === "customer") {
                        return $q->where('salereports.category', "client");
                    } else if ($channel === "bulk") {
                        return $q->where('salereports.category', "Bulk Order");
                    }
                })
                ->when($country != null, function ($q) use ($country) {
                    if ($country === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.CountryId', $country);
                    }
                })

                ->when($fromdate != null, function ($q) use ($fromdate) {
                    return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                })
                ->when($todate != null, function ($q) use ($todate) {
                    return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
                })
                ->when($status != null, function ($q) use ($status) {
                    if ($status === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.status', $status);
                    }
                })
                ->orderBy('salereports.created_at', 'DESC')
                // ->distinct()
                ->chunk(1000, function ($reportsmaster) use (&$reports, $search) {
                    foreach ($reportsmaster as $key => $rm) {
                        // $_country = Countries::findorfail($rm->CountryId);
                        $rm->includedTaxToProduct =  $rm->isIncludedTaxToProduct;
                        if ($rm->category == "Bulk Order") {
                            // $_rm = BulkOrders::find($rm->bulkOrderId);
                            // if ($_rm !== null) {
                            $rm->order_no =  $this->orderService->formatBulkOrderNumberV5($rm->bulkOrderId, strtoupper($rm->OrderCountryCodeIso), $rm->bulkorder_created_at, false);
                            $rm->isSubsequentOrder = 0;
                            // } else {
                            //     $rm->order_no =  '';
                            //     $rm->isSubsequentOrder = 0;
                            // }
                            $rm->carrierAgent = $rm->bulkCarrierAgent;
                        } else {

                            //remove master report
                            if (strpos($rm->sku, ',') !== false) {
                            } else {
                                $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];

                                if (!in_array($rm->sku, $srplans)) {
                                    $combineqty = "";
                                    $combinesku = "";
                                    $countcombinesku = 0;
                                    if ($rm->Subscriptionid) {
                                        if ($rm->SubscriptionPlanId) {
                                            if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {

                                                $getPlanProduct =  PlanTrialProducts::join('products', 'products.id', 'plantrialproducts.ProductId')
                                                    ->where('plantrialproducts.PlanId', $rm->SubscriptionPlanId)
                                                    ->select('plantrialproducts.qty as pqty', 'products.sku as psku')
                                                    ->orderBy('products.sku')
                                                    ->get();
                                                if ($getPlanProduct) {
                                                    foreach ($getPlanProduct as $_getPlanProduct) {
                                                        if ($countcombinesku == 0) {
                                                            $combinesku = $_getPlanProduct->psku;
                                                            $combineqty = $_getPlanProduct->pqty;
                                                        } else {
                                                            $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                            $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                        }
                                                        $countcombinesku++;
                                                    }
                                                }
                                            } else {
                                                $getPlanProduct =  PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                                                    ->join('products', 'products.id', 'productcountries.ProductId')
                                                    ->where('plan_details.PlanId', $rm->SubscriptionPlanId)
                                                    ->select('plan_details.qty as pqty', 'products.sku as psku')
                                                    ->orderBy('products.sku')
                                                    ->get();
                                                if ($getPlanProduct) {
                                                    if ($rm->SubscriptionIsTrial == 1) {
                                                        foreach ($getPlanProduct as $_getPlanProduct) {
                                                            if (!in_array($_getPlanProduct->psku, config('global.all.handle_types.skusV2'))) {
                                                                if ($countcombinesku == 0) {
                                                                    $combinesku = $_getPlanProduct->psku;
                                                                    $combineqty = $_getPlanProduct->pqty;
                                                                } else {
                                                                    $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                                    $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                                }
                                                            }
                                                            $countcombinesku++;
                                                        }
                                                    } else {

                                                        foreach ($getPlanProduct as $_getPlanProduct) {
                                                            if ($countcombinesku == 0) {
                                                                $combinesku = $_getPlanProduct->psku;
                                                                $combineqty = $_getPlanProduct->pqty;
                                                            } else {
                                                                $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                                $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                            }
                                                            $countcombinesku++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {
                                        } else {
                                            $subaddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                                                ->join('products', 'products.id', 'productcountries.ProductId')
                                                ->where("subscriptions_productaddon.subscriptionId", $rm->Subscriptionid)
                                                ->select('subscriptions_productaddon.qty as pqty', 'products.sku as psku')
                                                ->orderBy('products.sku')
                                                ->get();

                                            if ($subaddons) {
                                                foreach ($subaddons as $_subaddons) {
                                                    if ($countcombinesku == 0) {
                                                        $combinesku = $_subaddons->psku;
                                                        $combineqty = $_subaddons->pqty;
                                                    } else {
                                                        $combinesku = $combinesku . "," . $_subaddons->psku;
                                                        $combineqty = $combineqty . "," . $_subaddons->pqty;
                                                    }
                                                    $countcombinesku++;
                                                }
                                            }
                                        }
                                        if ($combineqty != "" && $combinesku != "") {
                                            $rm->sku = $combinesku;
                                            $rm->qty = $combineqty;
                                        }
                                    }
                                }
                            }
                            //remove master report

                            // $_rm = Orders::find($rm->orderId);
                            // $rm->order_no = $this->orderService->formatOrderNumberV4($_rm, strtoupper($rm->OrderCountryCodeIso), false);
                            // $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                            // if ($_rm !== null) {
                            $rm->order_no = $this->orderService->formatOrderNumberV5($rm->orderId, strtoupper($rm->OrderCountryCodeIso), $rm->order_created_at, false);
                            if (isset($rm->deliveryContact)) {
                            } else {
                                $getDeliveryAddresses = DeliveryAddresses::where('id', $rm->odaid)
                                    ->select('contactNumber')
                                    ->first();
                                if (isset($getDeliveryAddresses)) {
                                    $rm->deliveryContact = $getDeliveryAddresses->contactNumber;
                                }
                            }
                            if (isset($rm->billingContact)) {
                            } else {
                                $getBillingAddresses = DeliveryAddresses::where('id', $rm->obaid)
                                    ->select('contactNumber')
                                    ->first();
                                if (isset($getBillingAddresses)) {
                                    $rm->deliveryContact = $getBillingAddresses->contactNumber;
                                }
                            }
                            // $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                            // } else {
                            //     $rm->order_no =  '';
                            //     $rm->isSubsequentOrder = 0;
                            // }
                        }

                        $canproceed = null;
                        if ($search !== null && $search !== "") {
                            if (strpos($rm->order_no . $rm->taxInvoiceNo . $rm->sku, $search) !== false) { //if found
                                $canproceed = true;
                            } else {
                                $reportsmaster->forget($key);
                                $canproceed = false;
                            }
                        } else {
                            $canproceed = true;
                        }

                        if ($canproceed === true) {
                            // $rm->created_at = date('d-m-y H:i', strtotime($rm->created_at));
                            $rm->completed_date = null;
                            if ($rm->status == "Completed") {
                                if ($rm->category != "Bulk Order") {
                                    $rm->completed_at = OrderHistories::where('OrderId', $rm->orderId)->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                                    if ($rm->completed_at != null) {
                                        $rm->completed_date = date('Y-m-d', strtotime($rm->completed_at));
                                    }
                                } else {
                                    $rm->completed_at = BulkOrderHistories::where('BulkOrderId', $rm->bulkOrderId)->where('message', 'Completed')->where('isRemark', '!=', '1')->pluck('created_at')->first();
                                    if ($rm->completed_at != null) {
                                        $rm->completed_date = date('Y-m-d', strtotime($rm->completed_at));
                                    }
                                }
                            }

                            // $_subscription = Subscriptions::find($rm->subscriptionIds);

                            $rm->isActiveTrial = 0;
                            if (isset($rm->SubscriptionIsTrial) && $rm->SubscriptionIsTrial != null) {
                                $rm->isTrial = $rm->SubscriptionIsTrial;
                                $rm->isActiveTrial = $rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0 ? 1 : 0;
                            }

                            // $rm->cCardType = null;
                            // get card details

                            // if ($rm->paymentType === "stripe" && $rm->rchargeId != null && $rm->rchargeId != "") {
                            //     $apptype = ($rm->category === 'sales app') ? 'baWebsite' : (($rm->category === 'client') ? 'ecommerce' : 'ecommerce');
                            //     $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $rm->CountryId, $rm->rchargeId);
                            //     $rm->cCardType = $chargeDetails ? $chargeDetails->payment_method_details->card->funding : null;
                            // }
                            // else if ($rm->paymentType === "nicepay") {
                            //     $rm->cCardType = null;
                            // }
                            // else {
                            //     $rm->cCardType = null;
                            // }
                        }
                        // if ($_subscription != null) {
                        //     $rm->isTrial = $_subscription->isTrial;
                        //     $rm->gender = $this->planHelper->getPlanDetailsByPlanIdV2($_subscription->PlanId, $_country->id)['plan_gender_type'];
                        // }
                        // else {
                        //     $rm->gender = null;
                        //     $skulist = explode(",", $rm->sku);

                        //     $productcategories = [];
                        //     foreach ($skulist as $sku) {
                        //         $producttypes = Product::where("sku", $sku)
                        //         ->leftJoin('producttypedetails','products.id', '=', 'producttypedetails.ProductId')
                        //         ->pluck('producttypedetails.ProductTypeId');

                        //         foreach ($producttypes as $pt) {
                        //             array_push($productcategories, $pt);
                        //         }
                        //     }

                        //     if (count(array_intersect($productcategories, config('global.all.product_category_types.men'))) >= 1) {
                        //         $rm->gender = "male";
                        //     }
                        //     else if (count(array_intersect($productcategories, config('global.all.product_category_types.women'))) >= 1) {
                        //         $rm->gender = "female";
                        //     }
                        // }
                    }

                    $reports = $reports->merge($reportsmaster);
                });
            $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
            $data1 = [];
            $countdata = 1;
            foreach ($reports as $report_) {

                // Initialize variables
                $getChargeid = '';
                if ($report_ != null && $report_ != '') {
                    if ($report_->CountryId == 8) {
                        if ($report_->imp_uid != null && $report_->imp_uid !== '') {
                            $getChargeid = $report_->imp_uid;
                        } else {
                            $getChargeid = '';
                        }
                    } else {
                        $getChargeid =  $report_->rchargeId;
                    }
                }

                // Tax amount calculation (Based on edm)
                $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                // Let this be referral Discount Section
                $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                if ($report_->promoCode && $report_->promoCode == "referral") {
                    // For referral Discounts, Set cash rebate to equal to Discount Amount
                    $cashRebate = $report_->discountAmount;
                    // Then set Discount ammount to 0
                    $discountAmount = number_format((float) 0, 2, '.', '');
                } else {
                    // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                    $cashRebate = number_format((float) 0, 2, '.', '');
                }

                $portalCode = $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? explode(',', $report_->deliveryAddress) : '-') : '-';
                if ($portalCode !== '-') {
                    array_pop($portalCode);
                    $portalCode = end($portalCode);
                }
                $bportalCode = $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? explode(',', $report_->billingAddress) : '-') : '-';
                if ($bportalCode !== '-') {
                    array_pop($bportalCode);
                    $bportalCode = end($bportalCode);
                }
                $category = (isset($report_->category) && $report_->category === 'sales app') ? 'BA' : ((isset($report_->category) && $report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                $e = [
                    "Order" => $report_ !== null ? (isset($report_->order_no) && $report_->order_no != "" ? $report_->order_no : '-') : '-',
                    "OrderStatus" =>  $report_ !== null ? (isset($report_->status) && $report_->status != "" ? $report_->status : '-') : '-',
                    "SaleDate" => $report_ !== null ? (isset($report_->saleDate) && $report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                    "CompletionDate" => $report_ !== null ? (isset($report_->completed_date) && $report_->completed_date != null ? $report_->completed_date : '-') : '-',
                    "Region" => $report_ !== null ? (isset($report_->region) && $report_->region != "" ? $report_->region : '-') : '-',
                    "Category" => isset($category) ? $category : '-',
                    "MOCode" => $report_ !== null ? (isset($report_->moCode) && $report_->moCode != "" ? $report_->moCode : '-') : '-',
                    "BadgeId" =>  $report_ !== null ? (isset($report_->badgeId) && $report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                    "AgentName" =>  $report_ !== null ? (isset($report_->agentName) && $report_->agentName != "" ? $report_->agentName : '-') : '-',
                    "ChannelType" =>  $report_ !== null ? (isset($report_->channelType) && $report_->channelType != "" ? $report_->channelType : '-') : '-',
                    "EventLocationCode" => $report_ !== null ? (isset($report_->eventLocationCode) && $report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                    "DeliveryMethod" =>  $report_ !== null ? (isset($report_->isDirectTrial) && $report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                    "TrackingNumber" => $report_ !== null ? (isset($report_->deliveryId) && $report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                    "UserID" => $report_ !== null ? (isset($report_->UserId) && $report_->UserId != "" ? $report_->UserId : '-') : '-',
                    "CustomerName" => $report_ !== null ? (isset($report_->customerName) && $report_->customerName != "" ? $report_->customerName : '-') : '-',
                    "Email" => $report_ !== null ? (isset($report_->email) && $report_->email != "" ? $report_->email : '-') : '-',
                    "DeliveryAddress" => $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                    "DeliveryPostcode" => $portalCode,
                    "DeliveryContactNumber" => $report_ !== null ? (isset($report_->deliveryContact) && $report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                    "BillingAddress" => $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                    "BillingPostcode" => $bportalCode,
                    "BillingContactNumber" => $report_ !== null ? (isset($report_->billingContact) && $report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                    "ProductCategory" => $report_ !== null ? (isset($report_->productCategory) && $report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                ];

                $unitcount = 0;
                $checkskuget = 0;
                $unitcount_array = [];

                if (strpos($report_->sku, ',') !== false) {
                    $report_->sku = str_replace(" ", "", $report_->sku);
                    $report_->qty = str_replace(" ", "", $report_->qty);
                    $splitproduct = explode(",", $report_->sku);
                    $splitqty = explode(",", $report_->qty);
                    foreach ($srplans as $sp) {
                        $checkskuget = 0;
                        if ($report_->SubscriptionIsTrial == 1) {
                            foreach ($splitproduct as $key => $p) {
                                if (($sp == $p) && !in_array($p, config('global.all.handle_types.skusV2'))) {
                                    $unitcount = $unitcount + (int) $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] = $splitqty[$key];
                                }
                            }
                        } else {
                            foreach ($splitproduct as $key => $p) {
                                if ($sp == $p) {
                                    $unitcount = $unitcount + (int) $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] = $splitqty[$key];
                                }
                            }
                        }
                        if ($checkskuget == 1) {
                        } else {
                            $e["" . $sp] =  "0";
                        }
                    }
                } else {
                    if ($report_->SubscriptionIsTrial == 1) {
                        foreach ($srplans as $sp) {
                            if (($sp == $report_->sku) && !in_array($report_->sku, config('global.all.handle_types.skusV2'))) {
                                $unitcount = $unitcount + $report_->qty;

                                $e["" . $sp] = $report_->qty;
                            } else {

                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {
                        foreach ($srplans as $sp) {
                            if ($sp == $report_->sku) {
                                $unitcount = $unitcount + $report_->qty;

                                $e["" . $sp] = $report_->qty;
                            } else {

                                $e["" . $sp] =  "0";
                            }
                        }
                    }
                }

                $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');
                $processingfee = ['5', '6.5', '46', '4000', '175', '3000', '40', '150'];

                $e["UnitTotal"] = (string) $unitcount;
                $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" ? $report_->cCardType : '-') : '-';
                $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                $e["DiscountAmount"] = $discountAmount;
                $e["CashRebate"] = $cashRebate;
                $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';

                $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                if (isset($report_->screated_at)) {
                    if ($report_->screated_at <= "2019-12-08 19:00:00") {
                        $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->shippingFee != "" ? $report_->shippingFee : '-') : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                    }
                }
                if (!in_array($e["ProcessingFee"], $processingfee)) {
                    $e["ProcessingFee"] = "0";
                }

                $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                $e["TaxAmount"] = $taxAmount;
                $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                $e["Source"] = $report_->source ? $report_->source : "none";
                $e["Medium"] = $report_->medium ? $report_->medium : "none";
                $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                $e["Term"] = $report_->term ? $report_->term : "none";
                $e["Content"] = $report_->content ? $report_->content : "none";

                array_push($data1, $e);
                $countdata++;
            }
            $url = $data->url;

            Excel::store(new ReportMasterExport($data1), config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

            FileUploads::where('file_id', $data->file_id)->update([
                'url' => $url,
                'status' => 'Completed',
                'isDownloaded' => 1,
            ]);

            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Finished',
            ]);

            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            $logvalue = "GenerateReportsMaster Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Reports Master cronjob error = ' . $e->getMessage());
        }
    }

    public function GenerateReportsMasterSummary($data)
    {
        try {
            $this->Log->info('GenerateReportsMasterSummary Process start.');
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
                if ($country != 'all') {
                    $countget = DB::table('countries')
                        ->select('code')
                        ->where('id', $country)
                        ->first();
                    $country = $countget->code;
                }
            } else {
                $country = 'all';
            }

            if (isset($data->channel)) {
                $channel = $data->channel;
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }

            // $query = DB::raw('CASE 
            // WHEN BulkOrderId IS NULL THEN OrderId
            // ELSE BulkOrderId
            // END AS "Order"');
            $total = 0;
            $rms = DB::table('summary_salesreport')
                ->select(
                    // $query,
                    "OrderNumber",
                    "OrderStatus",
                    "SaleDate",
                    "CompletionDate",
                    "Region",
                    "Category",
                    "MOCode",
                    "BadgeId",
                    "AgentName",
                    "ChannelType",
                    "Event/LocationCode as EventLocationCode",
                    "DeliveryMethod",
                    "TrackingNumber",
                    "UserID",
                    "CustomerName",
                    "Email",
                    "DeliveryAddress",
                    "DeliveryPostcode",
                    "DeliveryContactNumber",
                    "BillingAddress",
                    "BillingPostcode",
                    "BillingContactNumber",
                    "ProductCategory",
                    "A1",
                    "A2",
                    "A2/2018",
                    "A3",
                    "A4-2017",
                    "A5",
                    "A5/2018",
                    "ASK-2017",
                    "ASK3/2018",
                    "ASK5/2018",
                    "ASK6/2018",
                    "F5",
                    "FK5/2019",
                    "H1",
                    "H1S3/2018",
                    "H1S5/2018",
                    "H1S6/2018",
                    "H2",
                    "H3",
                    "H3S3/2018",
                    "H3S5/2018",
                    "H3S6/2018",
                    "H3TK3/2018",
                    "H3TK5/2018",
                    "H3TK6/2018",
                    "PF",
                    "PS",
                    "S3",
                    "S3/2018",
                    "S5",
                    "S5/2018",
                    "S6",
                    "S6/2018",
                    "TK3/2018",
                    "TK5/2018",
                    "TK6/2018",
                    "POUCH/2019",
                    "PTC-HDL",
                    "MASK/2019-BLACK",
                    "MASK/2019-BLUE",
                    "MASK/2019-GREEN",
                    "MASK/2019-GREY",
                    "UnitTotal",
                    "PaymentType",
                    "CardBrand",
                    "CardType",
                    "Currency",
                    "PromoCode",
                    "TotalPrice",
                    "DiscountAmount",
                    "CashRebate",
                    "SubTotalPrice",
                    "ProcessingFee",
                    "GrandTotalWithTax as GrandTotalIncltax",
                    "GrandTotalWithoutTax as GrandTotalBeforetax",
                    "TaxAmount",
                    "PaymentStatus as Payment Status",
                    "ChargeID",
                    "TaxInvoicenumber",
                    "Source",
                    "Medium",
                    "Campaign",
                    "Term",
                    "Content"
                )
                ->when($channel != null, function ($q) use ($channel) {
                    if ($channel === "all") {
                        return $q;
                    } else if ($channel === "seller") {
                        return $q->where('Category', "BA");
                    } else if ($channel === "customer") {
                        return $q->where('Category', "Online / E-Commerce");
                    } else if ($channel === "bulk") {
                        return $q->where('Category', "Bulk Order");
                    }
                })
                ->when($country != null, function ($q) use ($country) {
                    if ($country === "all") {
                        return $q;
                    } else {
                        return $q->where('Region', $country);
                    }
                })

                ->when($fromdate != null, function ($q) use ($fromdate) {
                    return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                })
                ->when($todate != null, function ($q) use ($todate) {
                    return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
                })
                ->when($status != null, function ($q) use ($status) {
                    if ($status === "all") {
                        return $q;
                    } else {
                        return $q->where('OrderStatus', $status);
                    }
                })
                ->orderBy('created_at', 'DESC')
                ->Count();
    
            // Excel::store(new ReportMasterExport([]),  'report/' . $data->filename);
            $localpath  = storage_path('app/report/' . $data->filename);
            $fp = fopen($localpath, 'a');
            fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

            fputcsv($fp, [
                "Order Number","Order Status","Sale Date","Completion Date","Region",
                "Category","MO Code","Badge Id","Agent Name","Channel Type","Event/Location Code","Delivery Method","Tracking Number",
                "User ID","Customer Name","Email","Delivery Address","Delivery Postcode","Delivery Contact Number","Billing Address",
                "Billing Postcode","Billing Contact Number","Product Category",
                "A1", "A2", "A2/2018", "A3", "A4-2017", "A5", "A5/2018", "ASK-2017", "ASK3/2018", "ASK5/2018", "ASK6/2018", "F5", "FK5/2019", "H1", "H1S3/2018", "H1S5/2018", "H1S6/2018", "H2", "H3", "H3S3/2018", "H3S5/2018",  "H3S6/2018", "H3TK3/2018", "H3TK5/2018", "H3TK6/2018", "PF", "PS", "S3", "S3/2018", "S5", "S5/2018", "S6", "S6/2018", "TK3/2018", "TK5/2018", "TK6/2018","POUCH/2019","PTC-HDL",'MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY',
                "Unit Total","Payment Type","Card Brand","Card Type","Currency","Promo Code","Total Price","Discount Amount",
                "Cash Rebate","Sub Total Price","Processing Fee","Grand Total(Incl. tax)","Grand Total(Before tax)","Tax Amount",
                "Payment Status","Charge ID","Tax Invoice number","Source","Medium","Campaign","Term","Content",
            ]);
            fclose($fp);
            // $rmsarray= $rms->toArray();

            // Storage::put('attempt1.txt', '');
            // Storage::append('attempt1.txt', "test");

            // ->distinct()
            // $data1=[];
            // // Excel::store(new ReportMasterExport($data1),  'report/' . $data->filename); 
            if($rms){
            $total = $rms;
            }
            $count=0;
            $limit=50000;
            $offset=0;
            $this->_GenerateReportsMasterSummary($data,$total,$count,$limit,$offset,$channel,$country,$status,$fromdate,$todate,$fp,$localpath);
            $logvalue = "GenerateReportsSummaryMaster Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Reports Summary Master cronjob error = ' . $e->getMessage());
        }
    }

    public function _GenerateReportsMasterSummary($data,$total,$count,$limit,$offset,$channel,$country,$status,$fromdate,$todate,$fp,$localpath)
    {
        if($total > 0){
        $this->Log->info('_GenerateReportsMasterSummary Process start. Total :' .$total. ' Count :'. $count);
        $rms = DB::table('summary_salesreport')
        ->select(
            // $query,
            "OrderNumber",
            "OrderStatus",
            "SaleDate",
            "CompletionDate",
            "Region",
            "Category",
            "MOCode",
            "BadgeId",
            "AgentName",
            "ChannelType",
            "Event/LocationCode as EventLocationCode",
            "DeliveryMethod",
            "TrackingNumber",
            "UserID",
            "CustomerName",
            "Email",
            "DeliveryAddress",
            "DeliveryPostcode",
            "DeliveryContactNumber",
            "BillingAddress",
            "BillingPostcode",
            "BillingContactNumber",
            "ProductCategory",
            "A1",
            "A2",
            "A2/2018",
            "A3",
            "A4-2017",
            "A5",
            "A5/2018",
            "ASK-2017",
            "ASK3/2018",
            "ASK5/2018",
            "ASK6/2018",
            "F5",
            "FK5/2019",
            "H1",
            "H1S3/2018",
            "H1S5/2018",
            "H1S6/2018",
            "H2",
            "H3",
            "H3S3/2018",
            "H3S5/2018",
            "H3S6/2018",
            "H3TK3/2018",
            "H3TK5/2018",
            "H3TK6/2018",
            "PF",
            "PS",
            "S3",
            "S3/2018",
            "S5",
            "S5/2018",
            "S6",
            "S6/2018",
            "TK3/2018",
            "TK5/2018",
            "TK6/2018",
            "POUCH/2019",
            "PTC-HDL",
            "MASK/2019-BLACK",
            "MASK/2019-BLUE",
            "MASK/2019-GREEN",
            "MASK/2019-GREY",
            "UnitTotal",
            "PaymentType",
            "CardBrand",
            "CardType",
            "Currency",
            "PromoCode",
            "TotalPrice",
            "DiscountAmount",
            "CashRebate",
            "SubTotalPrice",
            "ProcessingFee",
            "GrandTotalWithTax as GrandTotalIncltax",
            "GrandTotalWithoutTax as GrandTotalBeforetax",
            "TaxAmount",
            "PaymentStatus as Payment Status",
            "ChargeID",
            "TaxInvoicenumber",
            "Source",
            "Medium",
            "Campaign",
            "Term",
            "Content"
        )
        ->when($channel != null, function ($q) use ($channel) {
            if ($channel === "all") {
                return $q;
            } else if ($channel === "seller") {
                return $q->where('Category', "BA");
            } else if ($channel === "customer") {
                return $q->where('Category', "Online / E-Commerce");
            } else if ($channel === "bulk") {
                return $q->where('Category', "Bulk Order");
            }
        })
        ->when($country != null, function ($q) use ($country) {
            if ($country === "all") {
                return $q;
            } else {
                return $q->where('Region', $country);
            }
        })

        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            } else {
                return $q->where('OrderStatus', $status);
            }
        })
        ->orderBy('created_at', 'DESC')
        ->offset($offset)
        ->limit($limit)
        ->get();

    $totalget = $rms->count();
    $count = $count + $totalget;
    $offset = $offset + $limit;
    QueueReport::where('id', $data->queueid)->update([
        'total' => $totalget
    ]);
    $localpath  = storage_path('app/report/' . $data->filename);
    $fp = fopen($localpath, 'a');

    foreach ($rms as $rm) {
        $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
        // $eventlocation = "Event/LocationCode";
        $paymentstatus = "Payment Status";
        $data1 = [];
        $e = [];
        $e = [
            "Order" => $rm->OrderNumber,
            "OrderStatus" => $rm->OrderStatus,
            "SaleDate" => $rm->SaleDate == "-" ? "-" : Carbon::parse($rm->SaleDate)->format('Y-m-d'),
            "CompletionDate" => $rm->CompletionDate,
            "Region" => $rm->Region,
            "Category" => $rm->Category,
            "MOCode" => $rm->MOCode,
            "BadgeId" =>  $rm->BadgeId,
            "AgentName" =>  $rm->AgentName,
            "ChannelType" =>  $rm->ChannelType,
            "EventLocationCode" => $rm->EventLocationCode,
            "DeliveryMethod" => $rm->DeliveryMethod,
            "TrackingNumber" => $rm->TrackingNumber,
            "UserID" => $rm->UserID,
            "CustomerName" => $rm->CustomerName,
            "Email" => $rm->Email,
            "DeliveryAddress" => $rm->DeliveryAddress,
            "DeliveryPostcode" => $rm->DeliveryPostcode,
            "DeliveryContactNumber" => $rm->DeliveryContactNumber,
            "BillingAddress" => $rm->BillingAddress,
            "BillingPostcode" => $rm->BillingPostcode,
            "BillingContactNumber" => $rm->BillingContactNumber,
            "ProductCategory" => $rm->ProductCategory,
        ];
        foreach ($srplans as $sp) {
            $e["" . $sp] = $rm->$sp;
        }
        $e["UnitTotal"] = $rm->UnitTotal;
        $e["PaymentType"] = $rm->PaymentType;
        $e["CardBrand"] = $rm->CardBrand;
        $e["CardType"] = $rm->CardType;
        $e["Currency"] = $rm->Currency;
        $e["PromoCode"] = $rm->PromoCode;
        $e["TotalPrice"] = $rm->TotalPrice;
        $e["DiscountAmount"] = $rm->DiscountAmount;
        $e["CashRebate"] = $rm->CashRebate;
        $e["SubTotalPrice"] = $rm->SubTotalPrice;
        $e["ProcessingFee"] = $rm->ProcessingFee;
        $e["GrandTotalIncltax"] = $rm->GrandTotalIncltax;
        $e["GrandTotalBeforetax"] = $rm->GrandTotalBeforetax;
        $e["TaxAmount"] = $rm->TaxAmount;
        $e["Payment Status"] = $rm->$paymentstatus;
        $e["ChargeID"] = $rm->ChargeID;
        $e["TaxInvoicenumber"] = $rm->TaxInvoicenumber;
        $e["Source"] = $rm->Source;
        $e["Medium"] = $rm->Medium;
        $e["Campaign"] = $rm->Campaign;
        $e["Term"] = $rm->Term;
        $e["Content"] = $rm->Content;
        array_push($data1, $e);
        
        
        foreach ($data1 as $fields) {
            fputcsv($fp, $fields);
        }

 
    }
    fclose($fp);

    if($count >= $total){
    $localFilepath = file_get_contents($localpath);
    Storage::disk('s3')->put(config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, $localFilepath);
    $this->deleteFile($localpath);
    // Excel::store(new ReportMasterExport($rmsarray), config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);


    $url = $data->url;

    FileUploads::where('file_id', $data->file_id)->update([
        'url' => $url,
        'status' => 'Completed',
        'isDownloaded' => 1,
    ]);

    QueueReport::where('id', $data->queueid)->update([
        'status' => 'Finished',
    ]);

    CSVReportToDownloadQueueService::deleteHash($data->queueid);
    $logvalue = "_GenerateReportsSummaryMaster Process (" . $data->filename . ") done.";
    $this->Log->info($logvalue);
    return 0;
    }else{
        $this->_GenerateReportsMasterSummary($data,$total,$count,$limit,$offset,$channel,$country,$status,$fromdate,$todate,$fp,$localpath);
    }

    }else{
        return 0;
    }

    }

    public function GenerateReportsAppco($data)
    {
        try {
            $this->Log->info('GenerateReportsAppco Process start.');
            $maxresults = 50;
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
            }

            if (isset($data->channel)) {
                $channel = $data->channel;
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }

            $reportsmaster = DB::table('salereports')
                ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
                ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
                ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
                ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
                ->select('deliveryaddresses.portalCode as portalCode', 'billing.portalCode as bportalCode', 'salereports.*', 'orders.subscriptionIds as subscriptionIds', 'orders.imp_uid as imp_uid', 'orders.payment_status as paymentStatus', 'orders.carrierAgent as carrierAgent', 'orders.UserId as UserId', 'receipts.chargeId as rchargeId', 'receipts.branchName as rbranchName')
                ->when($country != null, function ($q) use ($country) {
                    if ($country === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.CountryId', $country);
                    }
                })

                ->when($fromdate != null, function ($q) use ($fromdate) {
                    return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                })
                ->when($todate != null, function ($q) use ($todate) {
                    return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
                })
                ->when($status != null, function ($q) use ($status) {
                    if ($status === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.status', $status);
                    }
                })
                ->where('salereports.category', "sales app")
                ->orderBy('salereports.created_at', 'DESC')
                ->distinct()
                ->get();

            foreach ($reportsmaster as $rm) {

                $_country = Countries::findorfail($rm->CountryId);
                $rm->includedTaxToProduct =  $_country->includedTaxToProduct;
                if ($rm->category == "Bulk Order") {
                    $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                    $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false);
                    $rm->isSubsequentOrder = 0;
                } else {
                    $_rm = Orders::findorfail($rm->orderId);
                    $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false);
                    $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                }
            }

            $reportsmaster_final = $reportsmaster->filter(function ($report) use ($search) {
                if ($search !== null && $search !== "") {
                    return strpos($report->order_no . $report->taxInvoiceNo . $report->sku, $search) >= 0;
                } else {
                    return true;
                }
            });

            foreach ($reportsmaster_final as $rmf) {
                // $rmf->created_at = date('d-m-y H:i', strtotime($rmf->created_at));

                $_subscription = Subscriptions::find($rmf->subscriptionIds);

                $rmf->isActiveTrial = 0;
                if ($_subscription != null) {
                    $rmf->isTrial = $_subscription->isTrial;
                    $rmf->isActiveTrial = $rmf->isTrial == 1 && $rmf->isSubsequentOrder == 0 ? 1 : 0;
                }

                $rmf->cCardType = null;

                if ($rmf->paymentType === "stripe" && $rmf->rchargeId != null && $rmf->rchargeId != "") {
                    $apptype = ($rmf->category === 'sales app') ? 'baWebsite' : (($rm->category === 'client') ? 'ecommerce' : 'ecommerce');
                    $chargeDetails = "s";
                    $rmf->cCardType = "s";
                } else if ($rm->paymentType === "nicepay") {
                    $rmf->cCardType = null;
                } else {
                    $rmf->cCardType = null;
                }
            }
            $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
            $data1 = [];
            $countdata = 1;
            foreach ($reportsmaster as $report_) {

                // Initialize variables
                $getChargeid = '';
                if ($report_ != null && $report_ != '') {
                    if ($report_->CountryId == 8) {
                        if ($report_->imp_uid != null && $report_->imp_uid != '') {
                            $getChargeid = $report_->imp_uid;
                        } else {
                            $getChargeid = '';
                        }
                    } else {
                        $getChargeid =  $report_->rchargeId;
                    }
                }

                // Tax amount calculation (Based on edm)
                $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                // Let this be referral Discount Section
                $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                if ($report_->promoCode && $report_->promoCode == "referral") {
                    // For referral Discounts, Set cash rebate to equal to Discount Amount
                    $cashRebate = $report_->discountAmount;
                    // Then set Discount ammount to 0
                    $discountAmount = number_format((float) 0, 2, '.', '');
                } else {
                    // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                    $cashRebate = number_format((float) 0, 2, '.', '');
                }

                $category = ($report_->category === 'sales app') ? 'BA' : (($report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                $e = [
                    "Order" => $report_ !== null ? ($report_->order_no != "" ? $report_->order_no : '-') : '-',
                    "OrderStatus" =>  $report_ !== null ? ($report_->status != "" ? $report_->status : '-') : '-',
                    "SaleDate" => $report_ !== null ? ($report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                    "Region" => $report_ !== null ? ($report_->region != "" ? $report_->region : '-') : '-',
                    "Category" => $category,
                    "MOCode" => $report_ !== null ? ($report_->moCode != "" ? $report_->moCode : '-') : '-',
                    "BadgeId" =>  $report_ !== null ? ($report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                    "AgentName" =>  $report_ !== null ? ($report_->agentName != "" ? $report_->agentName : '-') : '-',
                    "ChannelType" =>  $report_ !== null ? ($report_->channelType != "" ? $report_->channelType : '-') : '-',
                    "EventLocationCode" => $report_ !== null ? ($report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                    "DeliveryMethod" =>  $report_ !== null ? ($report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                    "TrackingNumber" => $report_ !== null ? ($report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                    "UserID" => $report_ !== null ? ($report_->UserId != "" ? $report_->UserId : '-') : '-',
                    "CustomerName" => $report_ !== null ? ($report_->customerName != "" ? $report_->customerName : '-') : '-',
                    "Email" => $report_ !== null ? ($report_->email != "" ? $report_->email : '-') : '-',
                    "DeliveryAddress" => $report_ !== null ? ($report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                    "DeliveryPostcode" => $report_ !== null ? ($report_->portalCode != "" ? $report_->portalCode : '-') : '-',
                    "DeliveryContactNumber" => $report_ !== null ? ($report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                    "BillingAddress" => $report_ !== null ? ($report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                    "BillingPostcode" => $report_ !== null ? ($report_->bportalCode != "" ? $report_->bportalCode : '-') : '-',
                    "BillingContactNumber" => $report_ !== null ? ($report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                    "ProductCategory" => $report_ !== null ? ($report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                ];

                $unitcount = 0;
                $checkskuget = 0;
                if ($report_->productCategory === 'Subscription') {
                    if (strpos($report_->sku, ',') !== false) {
                        $splitproduct = explode(",", $report_->sku);
                        $splitqty = explode(",", $report_->qty);
                        foreach ($srplans as $sp) {
                            $checkskuget = 0;
                            foreach ($splitproduct as $key => $p) {
                                if ($sp == $p) {
                                    $unitcount = $unitcount +  $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] =  $splitqty[$key];
                                }
                            }
                            if ($checkskuget == 1) {
                            } else {
                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {

                        $planproduct = $this->planHelper->getPlanProductBySKU($report_->sku, $report_->CountryId);

                        if (!empty($planproduct)) {
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($planproduct as $p) {

                                    if ($sp == $p->productssku) {
                                        $unitcount = $unitcount + $p->qty;
                                        $checkskuget = 1;
                                        $e["" . $sp] =  $p->qty;
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {
                            foreach ($srplans as $sp) {
                                if ($sp == $report_->sku) {
                                    $unitcount = $unitcount + $report_->qty;
                                    $e["" . $sp] = $report_->qty;
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        }
                    }
                } else {
                    if (strpos($report_->sku, ',') !== false) {
                        $splitproduct = explode(",", $report_->sku);
                        $splitqty = explode(",", $report_->qty);
                        foreach ($srplans as $sp) {
                            $checkskuget = 0;
                            foreach ($splitproduct as $key => $p) {
                                if ($sp == $p) {
                                    $unitcount = $unitcount +  $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] = $splitqty[$key];
                                }
                            }
                            if ($checkskuget == 1) {
                            } else {
                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {
                        foreach ($srplans as $sp) {
                            if ($sp == $report_->sku) {
                                $unitcount = $unitcount + $report_->qty;

                                $e["" . $sp] = $report_->qty;
                            } else {

                                $e["" . $sp] =  "0";
                            }
                        }
                    }
                }
                $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');

                $e["UnitTotal"] = (string) $unitcount;
                $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" && $report_->cCardType != null ? $report_->cCardType : '-') : '-';
                $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                $e["DiscountAmount"] = $discountAmount;
                $e["CashRebate"] = $cashRebate;
                $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';
                $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                $e["TaxAmount"] = $taxAmount;
                $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                $e["Source"] = $report_->source ? $report_->source : "none";
                $e["Medium"] = $report_->medium ? $report_->medium : "none";
                $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                $e["Term"] = $report_->term ? $report_->term : "none";
                $e["Content"] = $report_->content ? $report_->content : "none";

                array_push($data1, $e);
                $countdata++;
            }

            $url = $data->url;

            Excel::store(new ReportAppcoExport($data1), config('environment.aws.settings.csvReportsUpload') . '/' . $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

            FileUploads::where('file_id', $data->file_id)->update([
                'url' => $url,
                'status' => 'Completed',
                'isDownloaded' => 1,
            ]);

            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Finished',
            ]);

            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            $logvalue = "GenerateReportsAppco Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Reports Appco cronjob error = ' . $e->getMessage());
        }
    }

    public function GeneraterReportsMO($data)
    {
        try {
            $this->Log->info('GenerateReportsMaster Process start.');
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;

            if (isset($data->maxresults)) {
                $maxresults = $data->maxresults;
            }

            if (isset($data->search) && ($data->search !== null || $data->search !== "")) {
                $search = $data->search;
            } else {
                if (isset($data->fromdate)) {
                    $fromdate = $data->fromdate;
                }

                if (isset($data->todate)) {
                    $todate = $data->todate;
                }
            }

            if (isset($data->country)) {
                $country = $data->country;
            }

            if (isset($data->channel)) {
                $channel = $data->channel;
            }

            if (isset($data->plantype)) {
                $plantype = $data->plantype;
            }

            if (isset($data->sku)) {
                $sku = $data->sku;
            }

            if (isset($data->status)) {
                $status = $data->status;
            }

            if (isset($data->pagenum)) {
                $pagenum = $data->pagenum;
            }

            $reportsmaster = DB::table('salereports')
                ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
                ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
                ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
                ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
                ->select('deliveryaddresses.portalCode as portalCode', 'billing.portalCode as bportalCode', 'salereports.*', 'orders.subscriptionIds as subscriptionIds', 'orders.imp_uid as imp_uid', 'orders.payment_status as paymentStatus', 'orders.carrierAgent as carrierAgent', 'orders.UserId as UserId', 'receipts.chargeId as rchargeId', 'receipts.branchName as rbranchName')
                ->when($country != null, function ($q) use ($country) {
                    if ($country === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.CountryId', $country);
                    }
                })

                ->when($fromdate != null, function ($q) use ($fromdate) {
                    return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                })
                ->when($todate != null, function ($q) use ($todate) {
                    return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
                })
                ->when($status != null, function ($q) use ($status) {
                    if ($status === "all") {
                        return $q;
                    } else {
                        return $q->where('salereports.status', $status);
                    }
                })
                ->where('salereports.category', "sales app")
                ->orderBy('salereports.created_at', 'DESC')
                ->distinct()
                ->get();

            foreach ($reportsmaster as $rm) {

                $_country = Countries::findorfail($rm->CountryId);
                $rm->includedTaxToProduct =  $_country->includedTaxToProduct;
                if ($rm->category == "Bulk Order") {
                    $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                    $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false);
                    $rm->isSubsequentOrder = 0;
                } else {
                    $_rm = Orders::findorfail($rm->orderId);
                    $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false);
                    $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                }
            }

            $reportsmaster_final = $reportsmaster->filter(function ($report) use ($search) {
                if ($search !== null && $search !== "") {
                    return strpos($report->order_no . $report->taxInvoiceNo . $report->sku, $search) >= 0;
                } else {
                    return true;
                }
            });

            foreach ($reportsmaster_final as $rmf) {
                // $rmf->created_at = date('d-m-y H:i', strtotime($rmf->created_at));

                $_subscription = Subscriptions::find($rmf->subscriptionIds);

                $rmf->isActiveTrial = 0;
                if ($_subscription != null) {
                    $rmf->isTrial = $_subscription->isTrial;
                    $rmf->isActiveTrial = $rmf->isTrial == 1 && $rmf->isSubsequentOrder == 0 ? 1 : 0;
                }

                $rmf->cCardType = null;

                // if ($_subscription != null) {
                //     $rm->isTrial = $_subscription->isTrial;
                //     $rm->gender = $this->planHelper->getPlanDetailsByPlanIdV2($_subscription->PlanId, $_country->id)['plan_gender_type'];
                // }
                // else {
                //     $rm->gender = null;
                //     $skulist = explode(",", $rm->sku);

                //     $productcategories = [];
                //     foreach ($skulist as $sku) {
                //         $producttypes = Product::where("sku", $sku)
                //         ->leftJoin('producttypedetails','products.id', '=', 'producttypedetails.ProductId')
                //         ->pluck('producttypedetails.ProductTypeId');

                //         foreach ($producttypes as $pt) {
                //             array_push($productcategories, $pt);
                //         }
                //     }

                //     if (count(array_intersect($productcategories, config('global.all.product_category_types.men'))) >= 1) {
                //         $rm->gender = "male";
                //     }
                //     else if (count(array_intersect($productcategories, config('global.all.product_category_types.women'))) >= 1) {
                //         $rm->gender = "female";
                //     }
                // }

                if ($rmf->paymentType === "stripe" && $rmf->rchargeId != null && $rmf->rchargeId != "") {
                    $apptype = ($rmf->category === 'sales app') ? 'baWebsite' : (($rmf->category === 'client') ? 'ecommerce' : 'ecommerce');
                    $chargeDetails = "s";
                    $rmf->cCardType = "s";
                } else if ($rmf->paymentType === "nicepay") {
                    $rmf->cCardType = null;
                } else {
                    $rmf->cCardType = null;
                }
            }

            $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];
            $data1 = [];
            $countdata = 1;
            foreach ($reportsmaster_final as $report_) {

                // Initialize variables
                $getChargeid = '';
                if ($report_ != null && $report_ != '') {
                    if ($report_->CountryId == 8) {
                        if ($report_->imp_uid != null && $report_->imp_uid != '') {
                            $getChargeid = $report_->imp_uid;
                        } else {
                            $getChargeid = '';
                        }
                    } else {
                        $getChargeid =  $report_->rchargeId;
                    }
                }

                // Tax amount calculation (Based on edm)
                $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                // Let this be referral Discount Section
                $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                if ($report_->promoCode && $report_->promoCode == "referral") {
                    // For referral Discounts, Set cash rebate to equal to Discount Amount
                    $cashRebate = $report_->discountAmount;
                    // Then set Discount ammount to 0
                    $discountAmount = number_format((float) 0, 2, '.', '');
                } else {
                    // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                    $cashRebate = number_format((float) 0, 2, '.', '');
                }

                $category = ($report_->category === 'sales app') ? 'BA' : (($report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                $e = [
                    "Order" => $report_ !== null ? ($report_->order_no != "" ? $report_->order_no : '-') : '-',
                    "OrderStatus" =>  $report_ !== null ? ($report_->status != "" ? $report_->status : '-') : '-',
                    "SaleDate" => $report_ !== null ? ($report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                    "Region" => $report_ !== null ? ($report_->region != "" ? $report_->region : '-') : '-',
                    "Category" => $category,
                    "MOCode" => $report_ !== null ? ($report_->moCode != "" ? $report_->moCode : '-') : '-',
                    "BadgeId" =>  $report_ !== null ? ($report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                    "AgentName" =>  $report_ !== null ? ($report_->agentName != "" ? $report_->agentName : '-') : '-',
                    "ChannelType" =>  $report_ !== null ? ($report_->channelType != "" ? $report_->channelType : '-') : '-',
                    "EventLocationCode" => $report_ !== null ? ($report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                    "DeliveryMethod" =>  $report_ !== null ? ($report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                    "TrackingNumber" => $report_ !== null ? ($report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                    "UserID" => $report_ !== null ? ($report_->UserId != "" ? $report_->UserId : '-') : '-',
                    "CustomerName" => $report_ !== null ? ($report_->customerName != "" ? $report_->customerName : '-') : '-',
                    "Email" => $report_ !== null ? ($report_->email != "" ? $report_->email : '-') : '-',
                    "DeliveryAddress" => $report_ !== null ? ($report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                    "DeliveryPostcode" => $report_ !== null ? ($report_->portalCode != "" ? $report_->portalCode : '-') : '-',
                    "DeliveryContactNumber" => $report_ !== null ? ($report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                    "BillingAddress" => $report_ !== null ? ($report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                    "BillingPostcode" => $report_ !== null ? ($report_->bportalCode != "" ? $report_->bportalCode : '-') : '-',
                    "BillingContactNumber" => $report_ !== null ? ($report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                    "ProductCategory" => $report_ !== null ? ($report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                ];

                $unitcount = 0;
                $checkskuget = 0;
                if ($report_->productCategory === 'Subscription') {
                    if (strpos($report_->sku, ',') !== false) {
                        $splitproduct = explode(",", $report_->sku);
                        $splitqty = explode(",", $report_->qty);
                        foreach ($srplans as $sp) {
                            $checkskuget = 0;
                            foreach ($splitproduct as $key => $p) {
                                if ($sp == $p) {
                                    $unitcount = $unitcount +  $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] =  $splitqty[$key];
                                }
                            }
                            if ($checkskuget == 1) {
                            } else {
                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {

                        $planproduct = $this->planHelper->getPlanProductBySKU($report_->sku, $report_->CountryId);

                        if (!empty($planproduct)) {
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($planproduct as $p) {

                                    if ($sp == $p->productssku) {
                                        $unitcount = $unitcount + $p->qty;
                                        $checkskuget = 1;
                                        $e["" . $sp] =  $p->qty;
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {
                            foreach ($srplans as $sp) {
                                if ($sp == $report_->sku) {
                                    $unitcount = $unitcount + $report_->qty;
                                    $e["" . $sp] = $report_->qty;
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        }
                    }
                } else {
                    if (strpos($report_->sku, ',') !== false) {
                        $splitproduct = explode(",", $report_->sku);
                        $splitqty = explode(",", $report_->qty);
                        foreach ($srplans as $sp) {
                            $checkskuget = 0;
                            foreach ($splitproduct as $key => $p) {
                                if ($sp == $p) {
                                    $unitcount = $unitcount +  $splitqty[$key];
                                    $checkskuget = 1;

                                    $e["" . $sp] = $splitqty[$key];
                                }
                            }
                            if ($checkskuget == 1) {
                            } else {
                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {
                        foreach ($srplans as $sp) {
                            if ($sp == $report_->sku) {
                                $unitcount = $unitcount + $report_->qty;

                                $e["" . $sp] = $report_->qty;
                            } else {

                                $e["" . $sp] =  "0";
                            }
                        }
                    }
                }
                $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');

                $e["UnitTotal"] = (string) $unitcount;
                $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" ? $report_->cCardType : '-') : '-';
                $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                $e["DiscountAmount"] = $discountAmount;
                $e["CashRebate"] = $cashRebate;
                $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';
                $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                $e["TaxAmount"] = $taxAmount;
                $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                $e["Source"] = $report_->source ? $report_->source : "none";
                $e["Medium"] = $report_->medium ? $report_->medium : "none";
                $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                $e["Term"] = $report_->term ? $report_->term : "none";
                $e["Content"] = $report_->content ? $report_->content : "none";

                array_push($data1, $e);
                $countdata++;
            }

            $url = $data->url;
            Excel::store(new ReportMOExport($data1), config('environment.aws.settings.csvReportsUpload') . '/' .  $data->filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

            FileUploads::where('file_id', $data->file_id)->update([
                'url' => $url,
                'status' => 'Completed',
                'isDownloaded' => 1,
            ]);

            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Finished',
            ]);

            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            $logvalue = "GenerateReportsMaster Process (" . $data->filename . ") done.";
            $this->Log->info($logvalue);
            return 0;
        } catch (Exception $e) {
            CSVReportToDownloadQueueService::deleteHash($data->queueid);
            QueueReport::where('id', $data->queueid)->update([
                'status' => 'Failure',
            ]);

            FileUploads::where('file_id', $data->file_id)->update([
                'status' => 'Canceled',
            ]);
            \Log::channel('cronjob')->info('Generate Reports Master cronjob error = ' . $e->getMessage());
        }
    }

    public function deleteFile($path)
    {
        return is_file($path) ?
            @unlink($path) :
            array_map('rrmdir', glob($path . '/*')) == @rmdir($path);
    }
}
