<?php

namespace App\Jobs;

use App\Helpers\TaxInvoiceHelper;
use App\Models\BulkOrders\BulkOrders;
use App\Models\FileUploads\FileUploads;
use App\Models\Orders\Orders;
use App\Queue\CompressedFileToDownloadQueueService;
use App\Queue\TaxInvoiceBulkOrderQueueService;
use App\Queue\TaxInvoiceQueueService;
use App\Services\TaxInvoiceService;
use Exception;
use Illuminate\Console\Command;

class TaxInvoiceJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tax-invoice-job:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate & Update Tax Invoice Number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->className = 'taxInvoice';
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('TaxInvoice start');
        $this->TaxInvoiceService = new TaxInvoiceService();
        $this->TaxInvoiceHelper = new TaxInvoiceHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateOrderTaxInvoice(); // update tax invoice & generate invoice pdf for orders
        $this->updateBulkOrderTaxInvoice(); // update tax invoice & generate invoice pdf for bulkorders
        $this->downloadCompressedfile(); // find order & bulkorder taxinvoices based on selected date range, download, compress & re-upload to aws
        // $this->retriggerTaxInvoice(); // retrigger list
        // $this->retriggerTaxInvoiceDateTime();
    }

    public function retriggerTaxInvoice()
    {
        // order
        $triggerlistarrays = [];

        if (count($triggerlistarrays) > 0) {
            $triggerlist = Orders::whereIn('id', $triggerlistarrays)->get();
            foreach ($triggerlist as $tl) {
                $_redis_tax_invoice_data = ['id' => $tl->id, 'CountryId' => $tl->CountryId];
                TaxInvoiceQueueService::addHash($tl->id, $_redis_tax_invoice_data);
            }
        }

        // bulkorder
        $triggerlistarrays_bulkorder = [];

        if (count($triggerlistarrays_bulkorder) > 0) {
            $triggerlist_bo = BulkOrders::whereIn('id', $triggerlistarrays_bulkorder)->get();
            foreach ($triggerlist_bo as $tlB) {
                $_redis_tax_invoice_data = ['id' => $tlB->id, 'CountryId' => $tlB->CountryId];
                TaxInvoiceBulkOrderQueueService::addHash($tlB->id, $_redis_tax_invoice_data);
            }
        }
    }

    public function retriggerTaxInvoiceDateTime()
    {
        // order
        $def = [];
        $countryarray = [7, 8];
        $triggerlistarrays = Orders::whereNotIn('status', ['Payment Failure', 'Pending'])
        ->whereDate('created_at', '>=', '2019-08-01')
        ->whereIn('CountryId', $countryarray)
        ->whereNotNull('taxInvoiceNo')
        // ->whereIn('id',[194684])
        ->get();
  
            foreach ($triggerlistarrays as $d) {
                    $_redis_tax_invoice_data = ['id' => $d->id, 'CountryId' => $d->CountryId];
                    TaxInvoiceQueueService::addHash($d->id, $_redis_tax_invoice_data);
            }
  
  
        // bulkorder
        $triggerlistarrays_bulkorder = BulkOrders::whereNotIn('status', ['Payment Failure', 'Pending'])
        ->whereDate('created_at', '>=', '2019-08-01')
        ->whereIn('CountryId', $countryarray)
        ->get();
  
        foreach ($triggerlistarrays_bulkorder as $d) {
            $_redis_tax_invoice_data = ['id' => $d->id, 'CountryId' => $d->CountryId];
            TaxInvoiceBulkOrderQueueService::addHash($d->id, $_redis_tax_invoice_data);
        }
    }

    public function retriggerTaxInvoiceTESTING()
    {


        $a = FileUploads::whereNotNull('orderId')->select('orderId')->get();

        $abc = [];
        foreach ($a as $d) {
            array_push($abc, $d->orderId);
        }

        var_dump('Total => ' . count($abc));
        $def = [];
        $b = Orders::whereNotIn('status', ['Payment Failure', 'Pending'])
            ->where('created_at', '>', '2019-12-08 16:00:00')
            ->whereNotNull('taxInvoiceNo')
            ->chunk(1000, function ($data) use ($def) {
                foreach ($data as $d) {
                    $c = FileUploads::where('orderId', $d->id)->whereNotNull('orderId')->first();
                    if (!$c) {
                        var_dump('Not Found: ' . $d->id);
                        $_redis_tax_invoice_data = ['id' => $d->id, 'CountryId' => $d->CountryId];
                        TaxInvoiceQueueService::addHash($d->id, $_redis_tax_invoice_data);
                    }
                }
            });

        die();

        $triggerlistarrays = [];
        $triggerlist = Orders::whereIn('id', $triggerlistarrays)->get();

        foreach ($triggerlist as $tl) {
            $_redis_tax_invoice_data = ['id' => $tl->id, 'CountryId' => $tl->CountryId];
            TaxInvoiceQueueService::addHash($tl->id, $_redis_tax_invoice_data);
        }
    }


    public function updateOrderTaxInvoice()
    {

        $this->Log->info('taxInvoice | updateTaxInvoice process start');
        //check order table for all orders without tax invoice
        $missingTXs = Orders::whereNull('taxInvoiceNo')->whereDate('created_at', '>=', '2019-12-01')->limit(50)->orderBy('created_at', 'DESC')->get();
        if ($missingTXs) {
            // format & generate new tax invoice numbers
            foreach ($missingTXs as $order) {
                $this->TaxInvoiceService->updateTaxInvoiceNoOrder($order->id);
            }
        }

        $this->Log->info('taxInvoice | generate tax invoice process start');
        $orderIds = TaxInvoiceQueueService::getAllHash();
        $_orderIds = '';
        if (empty($orderIds)) {
            try {
                throw new Exception("queue_empty");
            } catch (Exception $ex) {
                $this->Log->error('taxInvoice | updateTaxInvoice  | ' . $ex->getMessage());
            }
        } else {

            // only process x record per times
            if (count($orderIds) > 50) {
                $_orderIds = array_slice($orderIds, 0, 50);
            } else {
                $_orderIds = $orderIds;
            }
            
            //check order table for all orders without tax invoice
            foreach ($_orderIds as $order) {
                try {
                    $order_info = json_decode($order);
                    $this->Log->info('create pdf for order no :  => ' . json_encode($order_info->id));
                    $_send = $this->TaxInvoiceHelper->__generateHTML($order, false);
                    $this->Log->info('pdf response _send => ' . json_encode($_send));
                    if ($_send !== 1) {
                        $this->Log->error('retry & add to queue');
                        // retry
                        TaxInvoiceQueueService::deleteHash($order_info->id);
                        // data.retry += 1;
                        TaxInvoiceQueueService::addHash($order_info->id, $order_info);
                    } else {
                        $this->Log->info('remove key from queue');
                        TaxInvoiceQueueService::deleteHash($order_info->id);
                    }
                } catch (Exception $ex) {
                    $this->Log->error('taxInvoice | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function updateBulkOrderTaxInvoice()
    {

        $this->Log->info('BulkOrderTaxInvoice | updateBulkOrderTaxInvoice process start');
        // check order table for all orders without tax invoice
        // $missingTXs = BulkOrders::whereNull('taxInvoiceNo')->limit(50)->orderBy('created_at')->get();
        // if ($missingTXs) {
        //     // format & generate new tax invoice numbers
        //     foreach ($missingTXs as $order) {
        //         $this->TaxInvoiceService->updateTaxInvoiceNoBulkOrder($order->id);
        //     }
        // }

        $orderIds = TaxInvoiceBulkOrderQueueService::getAllHash();
        $_orderIds = '';
        if (empty($orderIds)) {
            try {
                throw new Exception("queue_empty");
            } catch (Exception $ex) {
                $this->Log->error('BulkOrderTaxInvoice | updateTaxInvoice  | ' . $ex->getMessage());
            }
        } else {

            // only process x record per times
            if (count($orderIds) > 10) {
                $_orderIds = array_slice($orderIds, 0, 10);
            } else {
                $_orderIds = $orderIds;
            }

            //check order table for all orders without tax invoice
            foreach ($_orderIds as $order) {
                try {
                    $order_info = json_decode($order);
                    $this->Log->info('create pdf for bulkorder no :  => ' . json_encode($order_info->id));
                    $_send = $this->TaxInvoiceHelper->__generateHTML($order, true);
                    $this->Log->info('pdf response _send => ' . json_encode($_send));
                    if ($_send !== 1) {
                        $this->Log->error('retry & add to queue');
                        // retry
                        TaxInvoiceBulkOrderQueueService::deleteHash($order_info->id);
                        // data.retry += 1;
                        TaxInvoiceBulkOrderQueueService::addHash($order_info->id, $order_info);
                    } else {
                        $this->Log->info('remove key from queue');
                        TaxInvoiceBulkOrderQueueService::deleteHash($order_info->id);
                    }
                } catch (Exception $ex) {
                    $this->Log->error('BulkOrderTaxInvoice | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function downloadCompressedfile()
    {
        $this->Log->info('taxInvoice | downloadCompressedfile process start');
        $options = CompressedFileToDownloadQueueService::getAllHash();

        if (empty($options)) {
            try {
                throw new Exception("queue_empty");
            } catch (Exception $ex) {
                $this->Log->error('taxInvoice | downloadCompressedfile  | ' . $ex->getMessage());
            }
        } else {
            //check order table for all orders without tax invoice
            $this->downloadProcessing = false;
            foreach ($options as $key => $option) {
                if (!$this->downloadProcessing) {
                    $this->downloadProcessing = true;
                    try {
                        CompressedFileToDownloadQueueService::deleteHash($key);
                        $this->downloadProcessing = false;
                        return $this->TaxInvoiceHelper->compressedFile($option);
                    } catch (Exception $ex) {
                        CompressedFileToDownloadQueueService::addHash($key, $option);
                        FileUploads::where('file_id', $key)->update(['status' => 'Completed', 'isDownloaded' => 0]);
                        $this->Log->error('taxInvoice | error => ' . $ex->getMessage());
                        $this->downloadProcessing = false;
                    }
                }
            }
        }
    }
}
