<?php

namespace App\Jobs;

use App\Helpers\Warehouse\UrbanFoxHelper;
use App\Helpers\Warehouse\WarehouseHelperHK;
use App\Helpers\Warehouse\WarehouseHelperKR;
use App\Helpers\Warehouse\WarehouseHelperMY;
use App\Helpers\Warehouse\WarehouseHelperTW;
use App\Queue\BulkOrderInboundQueueService;
use Exception;
use Illuminate\Console\Command;

class CreateBulkOrderInboundJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-bulkorder-inbound:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LF Warehouse - Create BulkOrder file for Warehouse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->className = 'createBulkOrderInbound';
        $this->functionName = 'createBulkOrderInbound';
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('CreateBulkOrderInboundJob start');

        $this->_WareHouseHelperMY = new WarehouseHelperMY();
        $this->_WareHouseHelperHK = new WarehouseHelperHK();
        $this->_WareHouseHelperKR = new WarehouseHelperKR();
        $this->_WareHouseHelperTW = new WarehouseHelperTW();
        $this->_urbanFoxHelper = new UrbanFoxHelper();
        $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createBulkOrderInbound_MYS();
        $this->createBulkOrderInbound_HKG();
        $this->createBulkOrderInbound_SGP();
        $this->createBulkOrderInbound_KOR();
        $this->createBulkOrderInbound_TWN();
    }

    public function createBulkOrderInbound_MYS()
    {
        $this->Log->info('MY | createBulkOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = BulkOrderInboundQueueService::getAllHashMY();
        if (count($_orderIds) === 0) {
            try {
                throw new Exception("queue_empty");
            } catch (Exception $ex) {
                $this->Log->error('MY | createBulkOrderInbound_MYS | ' . $ex->getMessage());
            }
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.MYS.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('MY | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('MY | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Malaysia - [Courier = GDEX]
                    if ($data->countryId == 1) {
                        if ($data->retry <= config('environment.warehouse_paths.MYS.retry')) {
                            $_send = $this->_WareHouseHelperMY->createBulkOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('MY | retry & add to queue');
                                // retry
                                BulkOrderInboundQueueService::deleteHashMY($data->orderId);
                                // data.retry += 1;
                                BulkOrderInboundQueueService::addHashMY($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('MY | remove key from queue');
                                BulkOrderInboundQueueService::deleteHashMY($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('MY | createBulkOrderInbound_MYS | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createBulkOrderInbound_HKG()
    {
        $this->Log->info('HK | createBulkOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = BulkOrderInboundQueueService::getAllHashHK();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('HK | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.HKG.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('HK | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('HK | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Hong Kong - [Courier = TAQBIN-HK]
                    if ($data->countryId == 2) {
                        if ($data->retry <= config('environment.warehouse_paths.HKG.retry')) {
                            $_send = $this->_WareHouseHelperHK->createBulkOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('HK | retry & add to queue');
                                // retry
                                BulkOrderInboundQueueService::deleteHashHK($data->orderId);
                                // data.retry += 1;
                                BulkOrderInboundQueueService::addHashHK($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('HK | remove key from queue');
                                BulkOrderInboundQueueService::deleteHashHK($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('HK | createBulkOrderInbound_HKG | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createBulkOrderInbound_SGP()
    {
        $this->Log->info('SG | createBulkOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = BulkOrderInboundQueueService::getAllHashSG();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('SG | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.SGP.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('SG | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('SG | $_orderId => ' . json_encode($data));
                    //Warehouse - UrbanFox - Singapore - [Courier = COURIER]
                    if ($data->countryId == 7) {
                        if ($data->retry <= config('environment.warehouse_paths.SGP.retry')) {
                            if ($data->retry <= config('environment.warehouse_paths.SGP.retry')) {
                                $_send = $this->_urbanFoxHelper->createOrderInbound($data->orderId, $data->countryId, true);
                                $this->Log->info('RESULT: ==> ' . json_encode($_send));
                            if (is_array($_send)) {  
                                if ($_send["errors"][0]["message"] == '"referenceNumber1" has already been taken') {
                                    $this->Log->info('SG | remove key from queue');
                                    BulkOrderInboundQueueService::deleteHashSG($data->orderId);
                                } else {
                                    $this->Log->error('SG | retry & add to queue');
                                    // retry
                                    BulkOrderInboundQueueService::deleteHashSG($data->orderId);
                                    // data.retry += 1;
                                    BulkOrderInboundQueueService::addHashSG($data->orderId, $data->countryId);
                                }
                            }else{
                                if ($_send =="statuschange") {
                                    $this->Log->info('SG | remove key from queue(status change)');
                                    BulkOrderInboundQueueService::deleteHashSG($data->orderId);
                                }
                             }
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('SG | createBulkOrderInbound_SGP | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createBulkOrderInbound_KOR()
    {
        $this->Log->info('KR | createBulkOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = BulkOrderInboundQueueService::getAllHashKR();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('KR | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.KOR.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('KR | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('KR | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - South Korea - [Courier = LOTTE]
                    if ($data->countryId == 8) {
                        if ($data->retry <= config('environment.warehouse_paths.KOR.retry')) {
                            $_send = $this->_WareHouseHelperKR->createBulkOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('KR | retry & add to queue');
                                // retry
                                BulkOrderInboundQueueService::deleteHashKR($data->orderId);
                                // data.retry += 1;
                                BulkOrderInboundQueueService::addHashKR($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('KR | remove key from queue');
                                BulkOrderInboundQueueService::deleteHashKR($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('KR | createBulkOrderInbound_KOR | error => ' . $ex->getMessage());
                }
            }
        }
    }

    public function createBulkOrderInbound_TWN()
    {
        $this->Log->info('TW | createBulkOrderInbound process start');
        $orderIds = '';
        // get keys in update product queue
        $_orderIds = BulkOrderInboundQueueService::getAllHashTW();
        if ($_orderIds && count($_orderIds) === 0) {
            $this->Log->warning('TW | queue_empty');
        } else {
            // only process x record per times
            if (count($_orderIds) > config('environment.warehouse_paths.TWN.maxFiles')) {
                $orderIds = array_slice($_orderIds,0,5);
            } else {
                $orderIds = $_orderIds;
            }
            $this->Log->info('TW | $_orderIds => ' . json_encode($orderIds));
            foreach ($orderIds as $data) {
                try {
                    $data = json_decode($data);
                    $this->Log->info('TW | $_orderId => ' . json_encode($data));
                    //Warehouse - LF - Malaysia - [Courier = TAQBIN-HK]
                    if ($data->countryId == 9) {
                        if ($data->retry <= config('environment.warehouse_paths.TWN.retry')) {
                            $_send = $this->_WareHouseHelperTW->createBulkOrderInbound($data->orderId);
                            if ($_send !== 1) {
                                $this->Log->error('TW | retry & add to queue');
                                // retry
                                BulkOrderInboundQueueService::deleteHashTW($data->orderId);
                                // data.retry += 1;
                                BulkOrderInboundQueueService::addHashTW($data->orderId, $data->countryId);
                            } else {
                                $this->Log->info('TW | remove key from queue');
                                BulkOrderInboundQueueService::deleteHashTW($data->orderId);
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $this->Log->error('TW | createBulkOrderInbound_TWN | error => ' . $ex->getMessage());
                }
            }
        }
    }
}
