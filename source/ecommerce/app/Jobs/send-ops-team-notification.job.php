<?php

namespace App\Jobs;

use App\Helpers\Email\EmailQueueHelper;
use App\Models\GeoLocation\Countries;
use App\Models\Subscriptions\ChargeJobLists;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Exception;
use Carbon\Carbon;

class SendOpsTeamNotification extends Command
{
    protected $signature = 'send-ops-team-notification:job';
    protected $description = 'Ops Team Notification';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'send-ops-team-notification: ';
        $this->Log = \Log::channel('cronjob');
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->doubleChargeNotification();
        $this->functionFailNotification();
    }

    public function doubleChargeNotification()
    {
        $this->Log->info('Ops Team Notification | doubleChargeNotification process start');

        $data = [
            "charge" => [],
            "recharge" => [],
            "charge_nicepay" => [],
            "recharge_nicepay" => []
        ];

        try {
            $lists = ChargeJobLists::where('status', 'Sent for Charge')->whereDate('created_at', '=', Carbon::now()->subDays(1)->format('Y-m-d'))
                ->select('charge_date', 'JobType', 'subscriptionId', 'email', 'status', 'CountryId')
                ->get()->toArray();
            if (count($lists) > 0) {
                foreach ($lists as $list) {
                    if ($list["JobType"] == 'Subscription Charge') {
                        array_push($data["charge"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Recharge') {
                        array_push($data["recharge"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Charge Nicepay') {
                        array_push($data["charge_nicepay"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Recharge Nicepay') {
                        array_push($data["recharge_nicepay"], $list);
                    }
                }
            }
            // if any suspicious charge in any of the categories, send edm to ops team
            if (
                (count($data["charge"]) > 0 || count($data["recharge"]) > 0) ||
                (count($data["charge_nicepay"]) > 0 || count($data["recharge_nicepay"]) > 0)
            ) {
                $currentCountry = Countries::where('id', 1)->first();
                $currentLocale = 'en';
                $emailData['title'] = 'ops-team-notification';
                $emailData['moduleData'] = $data;
                $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                $emailList = config('environment.email-blasting.ops-team-notications.double-charge');
                if (isset($emailList)) {
                    foreach ($emailList as $eL) {
                        $emailData['moduleData'] = array(
                            'email' => $eL,
                            'data' =>  $data
                        );
                        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                        // hide email redis
                        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('Ops Team Notification |  ERROR: ' . $ex->getMessage());
        }
    }

    public function functionFailNotification()
    {
        $this->Log->info('Ops Team Notification | functionFailNotification process start');

        $data = [
            "charge" => [],
            "recharge" => [],
            "charge_nicepay" => [],
            "recharge_nicepay" => []
        ];

        try {
            $lists = ChargeJobLists::where('status', 'Successfully Charged')->whereNotNull('function')->where('function', '!=' , 'end')->whereDate('created_at', '=', Carbon::now()->subDays(1)->format('Y-m-d'))
                ->select('charge_date', 'JobType', 'subscriptionId', 'email', 'status','function', 'CountryId')
                ->get()->toArray();
            if (count($lists) > 0) {
                foreach ($lists as $list) {
                    if ($list["JobType"] == 'Subscription Charge') {
                        array_push($data["charge"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Recharge') {
                        array_push($data["recharge"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Charge Nicepay') {
                        array_push($data["charge_nicepay"], $list);
                    }
                    if ($list["JobType"] == 'Subscription Recharge Nicepay') {
                        array_push($data["recharge_nicepay"], $list);
                    }
                }
            }
            // if any suspicious charge in any of the categories, send edm to ops team
            if (
                (count($data["charge"]) > 0 || count($data["recharge"]) > 0) ||
                (count($data["charge_nicepay"]) > 0 || count($data["recharge_nicepay"]) > 0)
            ) {
                $currentCountry = Countries::where('id', 1)->first();
                $currentLocale = 'en';
                $emailData['title'] = 'ops-team-notification-function';
                $emailData['moduleData'] = $data;
                $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                $emailList = config('environment.email-blasting.ops-team-notications.double-charge');
                
                if (isset($emailList)) {
                    foreach ($emailList as $eL) {
                        $emailData['moduleData'] = array(
                            'email' => $eL,
                            'data' =>  $data
                        );
                        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                        // hide email redis
                        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('Ops Team Notification functionFailNotification |  ERROR: ' . $ex->getMessage());
        }
    }
}
