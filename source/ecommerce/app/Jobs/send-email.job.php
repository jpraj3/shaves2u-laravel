<?php

namespace App\Jobs;

use Illuminate\Console\Command;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;

/**
 * Relevant Models
 */
use App\Models\Audits\EmailAudits;

class SendEmail extends Command implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $signature = 'send-email:job';
    protected $description = 'Send email job.';

    public $template;

    private $moduleData;

    private $emailQueueHelper;

    public $CountryAndLocaleData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Get all send-email-key-list from queue and pass into $emailQueueDatas
        $emailQueueDatas = EmailQueueService::getAllHash();

        // Get $emailQueueDatas parameter type
        $moduleDataType = gettype($emailQueueDatas);
        // $emailQueueDatas conversion
        switch ($moduleDataType) {
            case 'string':
                parse_str($emailQueueDatas, $emailQueueDatas);
                break;
            case 'object':
                $moduleDataDecoded = json_decode($emailQueueDatas, true);
                if (json_last_error() === JSON_ERROR_NONE) {

                    $emailQueueDatas = $moduleDataDecoded;
                } else { }
            default:
                break;
        }

        /**
         * Try Block 1, Handles $emailQueueDatas when there is 1 or more data
         * While Throwing exception when $emailQueueDatas length is less than 1
         * */
        try {
            // Handles $emailQueueDatas when there is 1 or more data
            if (count($emailQueueDatas) > 0) {
                // only process x record per times
                if (count($emailQueueDatas) > 3) {
                    $_emailQueueDatas = array_slice($emailQueueDatas,0,3);
                } else {
                    $_emailQueueDatas = $emailQueueDatas;
                }
                // Evaluate Each $emailQueueDatas as key => value pair
                foreach ($_emailQueueDatas as $key => $value) {
                    /**
                     * Convert value pair from json string to array
                     * */
                    $emailOption = json_decode($value, true);
                    if (json_last_error() === JSON_ERROR_NONE) {
                    }else{
                        $emailOption = json_decode(json_decode($value, true),true);
                    }
                    //\Log::channel('cronjob')->info($emailOption);
                    // Try Block 2, Attempts to Calls Build Email Template function in EmailQueueHelper
                    try {
                        $this->emailQueueHelper->buildEmailTemplate($emailOption['title'], $emailOption['moduleData'], $emailOption['CountryAndLocaleData']);
                        \Log::channel('cronjob')->info('Building Email Template for receipts id ' . $key);
                    } catch (Exception $e) {
                        $errorMessage = 'send-email error = ' . $e->getMessage();
                        $this->onSendEmailError($key, $value, $errorMessage);
                    }

                    // Try Block 3, Attempts to Removes Redis key from queue after task has been processed.
                    try {
                        EmailQueueService::deleteHash($key);
                        \Log::channel('cronjob')->info('Removing Queue Hash for receipts id ' . $key);
                    } catch (Exception $e) {
                        $errorMessage = 'Remove send-email key error = ' . $e->getMessage();
                        $this->onSendEmailError($key, $value, $errorMessage);
                    }
                }
                // When $emailQueueDatas length is less than 1, Throw new QueueEmptyException
            } else {
                throw new Exception('QUEUE EMPTY');
            }
            // When $emailQueueDatas length is less than 1, Catch QueueEmptyException
        } catch (Exception $e) {
            \Log::channel('cronjob')->info('Send-email error = ' . $e->getMessage());
        }
    }

    // Function: Execute this if got error while sending email
    function onSendEmailError($key, $value, $errorMessage)
    {
        \Log::channel('cronjob')->info($errorMessage);

        // Get email type and user id from key
        $keyDetails = explode ("-", $key);

        // Insert into email_audits table
        $emailAudits = new EmailAudits();
        $emailAudits->email_key = $key;
        $emailAudits->email_type = $keyDetails[1];
        $emailAudits->email_data = json_encode($value);
        $emailAudits->UserId = $keyDetails[0];
        $emailAudits->SubscriptionId = property_exists($value, 'subscriptionId') ? $value->subscriptionId : null;
        $emailAudits->OrderId = property_exists($value, 'orderId') ? $value->orderId : null;
        $emailAudits->error = $errorMessage;
        $emailAudits->save();
    }
}
