<?php

namespace App\Jobs;

use Illuminate\Console\Command;
use App\Queue\SendEmailQueueService;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use Carbon\Carbon;

class DemoCron extends Command
{
    protected $signature = 'demo:cron';
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    public function handle()
    {
        $userid = 52;
        $emailtype = "receipt-order-delivery";
        $emailtypevalue = config('global.all.email-type.'.$emailtype);
        $datetime = Carbon::now()->format('Ymdhis');

        // dd($userid."-".$emailtypevalue."-".$datetime);
        // $emailQueueDatas = SendEmailQueueService::getAllHash();
        // foreach ($emailQueueDatas as $identifier => $emailData) {
        //     $emailOption = json_decode($emailData,true);
        //     $this->emailQueueHelper->buildEmailTemplate($emailOption['title'], $emailOption['moduleData'], $emailOption['CountryAndLocaleData']);
        // }
    }
}
