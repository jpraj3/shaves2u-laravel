<?php

namespace App\Jobs;


use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Helpers\OrderHelper;
use App\Models\Orders\Orders;
use App\Models\BulkOrders\BulkOrders;
use Exception;

class RetriggerCreateOrderInboundJob extends Command
{
    protected $signature = 'retrigger-create-order-inbound:job';
    protected $description = 'Retrigger order inbound';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'retrigger-create-order-inbound: ';
        $this->Log = \Log::channel('cronjob');
        $this->orderHelper = new OrderHelper();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $this->triggerOrder();
        // $this->triggerBulkOrder();
    }

    public function triggerOrder()
    {
        $this->Log->info('Retrigger order inbound process start');
        try {
            // $countryarray=[1,2,7,8,9];
            $countryarray = [2, 7];
            $idarray = [0];
            $limitamount = 1;

            $orders = Orders::where('status', 'Payment Received')
                ->whereIn('CountryId', $countryarray)
                ->whereIn('id', $idarray)
                ->orderBy('created_at', 'ASC')
                ->limit($limitamount)
                ->get();

            if ($orders) {
                foreach ($orders as $od) {
                    // add order to queue service
                    $_redis_order_data = (object) array(
                        'OrderId' => $od->id,
                        'CountryId' => $od->CountryId
                    );
                    $this->orderHelper->addTaskToOrderQueueServiceJob($_redis_order_data);
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('Retrigger order inbound ERROR: ' . $ex->getMessage());
        }
    }

    public function triggerBulkOrder()
    {
        $this->Log->info('Retrigger bulk order inbound process start');
        try {
            // $countryarray=[1,2,7,8,9];
            $countryarray = [7];
            $idarray = [1, 2];
            $limitamount = 1;

            $orders = BulkOrders::where('status', 'Payment Received')
                ->whereIn('CountryId', $countryarray)
                ->whereIn('id', $idarray)
                ->orderBy('created_at', 'ASC')
                ->limit($limitamount)
                ->get();
            if ($orders) {
                foreach ($orders as $od) {
                    // add order to queue service
                    $_redis_order_data = (object) array(
                        'OrderId' => $od->id,
                        'CountryId' => $od->CountryId
                    );
                    $this->orderHelper->addTaskToBulkOrderQueueServiceJob($_redis_order_data);
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('Retrigger order inbound ERROR: ' . $ex->getMessage());
        }
    }
}
