<?php

namespace App\Jobs;


use Illuminate\Support\Facades\DB;
use App\Models\Subscriptions\SummarySubscriber;
use App\Models\Orders\Orders;
use App\Models\CancellationJourney\CancellationReasonTranslates;
use Illuminate\Console\Command;
use App\Services\OrderService;
use Carbon\Carbon;
use Exception;

class SubscriberSummaryTblJob extends Command
{
    protected $signature = 'subscriber-summary:job';
    protected $description = 'Summary table of Subscriber';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'subscriber-summary: ';
        $this->Log = \Log::channel('cronjob');
        $this->orderService = new OrderService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->CreateInitialSubscriberSummary();
        $this->UpdateSubscriberSummary();
    }

    public function CreateInitialSubscriberSummary()
    {
        $this->Log->info('CreateSubscriberSummary Process start.');

        $channel = "all";
        $country = "all";
        $status = "all";
        $fromdate = \Carbon\Carbon::now()->subDays(2)->format('m/d/Y');
        $todate = \Carbon\Carbon::now()->format('m/d/Y');
        // $fromdate = '12/31/2017';
        // $todate = '12/31/2018';

        $reports = collect();
        $cSummaryIds = [];
        // $cSummary = SummarySubscriber::select('SubID')->get()->toArray();
        // if ($cSummary !== null && count($cSummary) > 0) {
        //     foreach ($cSummary as $key => $content) {
        //         foreach ($content as $key => $value) {
        //             array_push($cSummaryIds, $value);
        //         }
        //     }
        // }
        $data = [];
        $countdata = 1;

        DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('rechargehistories', 'rechargehistories.subscriptionId', 'subscriptions.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->leftJoin('deliveryaddresses as ShippingAddress', 'subscriptions.DeliveryAddressId', '=', 'ShippingAddress.id')
            ->leftJoin('deliveryaddresses as BillingAddress', 'subscriptions.BillingAddressId', '=', 'BillingAddress.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->leftJoin('sellerusers', 'subscriptions.SellerUserId', '=', 'sellerusers.id')
            ->leftJoin('marketingoffices', 'subscriptions.MarketingOfficeId', '=', 'marketingoffices.id')
            ->leftJoin('marketingoffices as smarketingoffices', 'sellerusers.MarketingOfficeId', '=', 'smarketingoffices.id')
            ->leftJoin('pause_plan_histories', function ($query) {
                $query->on('pause_plan_histories.subscriptionIds', '=', 'subscriptions.id');
                $query->take(1);
            })
            ->select(
                'subscriptions.id as id',
                'subscriptions.*',
                'subscriptions.status as substatus',
                'subscriptions.cancellationReason as subcancellationReason',
                'subscriptions.created_at as subcreatedat',
                'subscriptions.updated_at as subupdatedAt',
                'ShippingAddress.id as shippingaddressid',
                'ShippingAddress.address as saddress',
                'ShippingAddress.portalCode  as sportalCode',
                'ShippingAddress.city as scity',
                'ShippingAddress.state as sstate',
                'BillingAddress.id as billingaddressid',
                'BillingAddress.address as baddress',
                'BillingAddress.portalCode  as bportalCode',
                'BillingAddress.city as bcity',
                'BillingAddress.state as bstate',
                'countries.codeIso as countrycodeIso',
                'countries.name as countryName',
                'plansku.duration',
                'plansku.sku',
                'plansku.isAnnual',
                'sellerusers.badgeId',
                'marketingoffices.moCode',
                'smarketingoffices.moCode as smoCode',
                'pause_plan_histories.id as pauseid',
                'users.id as uid',
                'users.firstName as ufirstName',
                'users.lastName as ulastName'
            )
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('plans.CountryId', $country);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptions.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptions.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('subscriptions.status', $status);
                }
            })
            ->orderBy('subscriptions.created_at', 'DESC')
            // ->distinct()
            // ->get();
            ->chunk(1000, function ($subscriptions) use ($countdata) {
                foreach ($subscriptions as $sub) {
                    $_order = Orders::select('id', 'created_at')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    // $current_order = Orders::select('status')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    if (isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ufirstName . " " . $sub->ulastName;
                    } else if (!isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ulastName;
                    } else {
                        $sub->ufullname = $sub->ufirstName;
                    }

                    if ($_order !== null) {
                        $sub->formattedId = $this->orderService->formatOrderNumberV3($_order->id, $_order->created_at, $sub->countrycodeIso, true);
                        if ($sub->isTrial === 1 && $sub->convertTrial2Subs) {
                            //take second order
                            $sub->conversion_date = isset($sub->convertTrial2Subs) ? $sub->convertTrial2Subs : '-';
                        } else {
                            $sub->conversion_date = null;
                        }
                        // $sub->currentOrderStatus = null;
                        // foreach ($_order as $order) {
                        //     if($order->id ==  $sub->currentOrderId){
                        //         $sub->currentOrderStatus = $order->status;
                        //     }
                        // }
                    } else {
                        $sub->formattedId = "";
                    }

                    // if ($current_order !== null) {
                    //     $sub->currentOrderStatus = $current_order->status;
                    // }

                    $sub->created_date = date('Y-m-d', strtotime($sub->subcreatedat));

                    if ($sub->shippingaddressid) {
                        $sub->deliveryaddress = $sub->saddress . " " . $sub->sportalCode . " " . $sub->scity . " " . $sub->sstate;
                    } else {
                        $sub->deliveryaddress = "";
                    }

                    if ($sub->billingaddressid) {
                        $sub->billingaddress = $sub->baddress . " " . $sub->bportalCode . " " . $sub->bcity . " " . $sub->bstate;
                    } else {
                        $sub->billingaddress = "";
                    }

                    if ($sub->MarketingOfficeId) {
                        $sub->moCode = $sub->moCode;
                    } else {
                        if ($sub->smoCode) {
                            $sub->moCode = $sub->smoCode;
                        } else {
                            $sub->moCode = "";
                        }
                    }
                    // $sub->cancellationReason = "";
                    // $sub->canceledDate = "";
                    // if ($sub->substatus == 'Cancelled') {
                    //     $sub->cancellationReason = $sub->subcancellationReason;
                    //     if ($sub->canceldate) {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->canceldate));
                    //     }
                    //     else {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->subupdatedAt));
                    //     }
                    //   }

                    if ($sub->substatus == "Cancelled") {
                        $cancellation = DB::table('cancellation_journeys')->where('SubscriptionId', $sub->id)->orderBy('created_at', 'DESC')->first();

                        if (isset($cancellation)) {
                            $sub->cancellationcreated_date = $cancellation->created_at;
                            $sub->cancellationotherreason = $cancellation->OtherReason;
                            $cancellationTranslates = CancellationReasonTranslates::where("id", $cancellation->CancellationTranslatesId)->first();
                            if ($cancellationTranslates) {
                                $sub->cancellationdefaultContent = $cancellationTranslates->defaultContent;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        } else {
                            $cancellation1 = DB::table('subscriptionhistories')
                                ->where('subscriptionId', $sub->id)
                                ->where(function ($query) {
                                    $query->orWhere('message',  'Canceled')
                                        ->orWhere('message', 'Cancelled');
                                })
                                ->orderBy('created_at', 'DESC')
                                ->first();

                            if (isset($cancellation1)) {
                                $sub->cancellationcreated_date = $cancellation1->created_at;
                            }
                        }

                        if (isset($sub->cancellationdefaultContent)) {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason . "," . $sub->cancellationdefaultContent;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        } else {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        }
                    } else if ($sub->substatus === "Payment Failure") {
                        $sub->cancellation = DB::table('subscriptionhistories')
                            ->where('subscriptionId', $sub->id)
                            ->where('message', 'Payment Failure')
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if (isset($sub->cancellation)) {
                            $sub->cancellationdefaultContent = $sub->cancellation->detail;
                            // $sub->cancellationcreated_date = date('Y-m-d', strtotime($sub->cancellation->created_at));
                        }

                        if ($sub->subcancellationReason === "Cancelled due to payment failed" || $sub->subcancellationReason === "Canceled due to payment failed" || $sub->subcancellationReason === "Cancelled due to user card has insufficient funds.") {
                            $sub->cancellationdefaultContent = $sub->subcancellationReason;
                        } else {
                            $sub->cancellationdefaultContent = "-";
                        }
                    }

                    $sub->blade_type = null;
                    $sub->shave_cream = null;
                    $sub->after_shave_cream = null;
                    $sub->pouch = null;
                    $sub->rubber_handle = null;

                    $products = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('plan_details', 'productcountries.id', '=', 'plan_details.ProductCountryId')
                        ->where('plan_details.PlanId', $sub->PlanId)
                        ->select('products.*', 'plan_details.qty as pd_qty')
                        ->get();

                    foreach ($products as $product) {
                        if (in_array($product->sku, config('global.all.blade_types.skus'))) {
                            $sub->blade_type = config('global.all.blade_no_v2.' . $product->sku) . ' blade';
                            $sub->blade_quantity = $product->pd_qty;
                        }
                    }

                    // if ($sub->isTrial === 1 && ($sub->currentCycle === 1 || $sub->currentCycle === 0)) {
                    //     $sub->blade_quantity = 1;
                    // }

                    $addons = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('subscriptions_productaddon', 'productcountries.id', '=', 'subscriptions_productaddon.ProductCountryId')
                        ->where('subscriptions_productaddon.subscriptionId', $sub->id)
                        ->select('products.*')
                        ->get();

                    foreach ($addons as $addon) {
                        if (in_array($addon->sku, config('global.all.aftershavecream_types.skus'))) {
                            $sub->after_shave_cream = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.shavecream_types.skus'))) {
                            $sub->shave_cream = $product->id;
                        }
                        if (in_array($addon->sku, config('global.all.pouch_types.skus'))) {
                            $sub->pouch = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.razor_potector.skus'))) {
                            $sub->rubber_handle = $addon->id;
                        }
                    }


                    if ($sub->pauseid) {
                        $sub->pauseplan = "Yes";
                    } else {
                        $sub->pauseplan = "No";
                    }
                    $sub->reactivatedplan = null;

                    if ($sub->currentDeliverNumber == 0 && $sub->isCustom == 1 && $sub->isTrial != 1 && ($sub->substatus !== "Payment Failure" && $sub->substatus !== "On Hold" && $sub->substatus !== "Pending")) {
                        $sub->currentDeliverNumber = "1";
                    }

                    $e = [
                        "SubID" => $sub !== null ? (isset($sub->id) && $sub->id  != "" ? $sub->id  : '-') : '-',
                        "UserID" => $sub !== null ? (isset($sub->uid) && $sub->uid  != "" ? $sub->uid  : '-') : '-',
                        "CustomerName" => $sub !== null ? (isset($sub->ufullname) && $sub->ufullname  != "" ? $sub->ufullname  : '-') : '-',
                        "Email" =>  $sub !== null ? (isset($sub->email) && $sub->email  != "" ? $sub->email  : '-') : '-',
                        "Country" => $sub !== null ? (isset($sub->countryName) && $sub->countryName  != "" ? $sub->countryName  : '-') : '-',
                        "Category" => $sub !== null ? (isset($sub->isOffline) && $sub->isOffline == 1 ? "BA" : "eCommerce") : '-',
                        "MOCode" => $sub !== null ? (isset($sub->moCode) && $sub->moCode  != "" ? $sub->moCode  : '-') : '-',
                        "BadgeID" => $sub !== null ? (isset($sub->badgeId) && $sub->badgeId  != "" ? $sub->badgeId  : '-') : '-',
                        "DeliveryAddress" => $sub !== null ? (isset($sub->deliveryaddress) && $sub->deliveryaddress  != "" ? $sub->deliveryaddress  : '-') : '-',
                        "PlanType" => $sub !== null ? (isset($sub->isTrial) && $sub->isTrial == 1 ? "TK" : (isset($sub->isCustom) && $sub->isCustom == 1 ? "Custom" : "Custom")) : 'Custom',
                        "BladeType" => $sub !== null ? (isset($sub->blade_type) && $sub->blade_type !== null ? $sub->blade_type : "0") : '-',
                        "DeliveryInterval" => $sub !== null ? (isset($sub->isAnnual) && $sub->isAnnual != 0 ? '12' : (isset($sub->duration) && $sub->duration != "" ? $sub->duration  : '-')) : '-',
                        "CassetteCycle" => $sub !== null ? (isset($sub->blade_quantity) && $sub->blade_quantity  != "" ? $sub->blade_quantity  : '-') : '-',
                        "CycleCount" => $sub !== null ?  (isset($sub->currentDeliverNumber) && $sub->currentDeliverNumber  != "" ? $sub->currentDeliverNumber  : '0') : '0',
                        "AfterShaveCream" => $sub !== null ? (isset($sub->after_shave_cream) && $sub->after_shave_cream !== null ? "1" : "0") : "0",
                        "ShaveCream" => $sub !== null ? (isset($sub->shave_cream) && $sub->shave_cream !== null ? "1" : "0") : "0",
                        "Pouch" => $sub !== null ? (isset($sub->pouch) && $sub->pouch !== null ? "1" : "0") : "0",
                        "RubberHandle" => $sub !== null ? (isset($sub->rubber_handle) && $sub->rubber_handle !== null ? "1" : "0") : "0",
                        "SignUp" => $sub !== null ? (isset($sub->created_date) && $sub->created_date  != "" ?  Carbon::parse($sub->created_date)->format('Y-m-d')  : '-') : '-',
                        "Conversion" => $sub !== null ? (isset($sub->conversion_date) && $sub->conversion_date  != "" ? $sub->conversion_date  : '-') : '-',
                        "LastDelivery" => $sub !== null ? (isset($sub->lastDeliverydate) && $sub->lastDeliverydate  != "" ?  Carbon::parse($sub->lastDeliverydate)->format('Y-m-d')  : '-') : '-',
                        "LastCharge" => $sub !== null ? (isset($sub->currentOrderTime) && $sub->currentOrderTime  != "" ?  Carbon::parse($sub->currentOrderTime)->format('Y-m-d')  : '-') : '-',
                        "NextCharge" => $sub !== null ? (isset($sub->nextChargeDate) && $sub->nextChargeDate  != "" ? $sub->nextChargeDate  : '-') : '-',
                        "RechargeAttempt" => $sub !== null ? (isset($sub->recharge_date) && $sub->recharge_date !== null ? "Yes" : "No") : "No",
                        "RechargeCount" => $sub !== null ? (isset($sub->total_recharge) && $sub->total_recharge !== null ? $sub->total_recharge : "0") : '0',
                        "CancellationDate" => $sub !== null ? (isset($sub->cancellationcreated_date) && $sub->cancellationcreated_date != "" ?  Carbon::parse($sub->cancellationcreated_date)->format('Y-m-d')  : '-') : '-',
                        "Status" => $sub !== null ? (isset($sub->substatus) && $sub->substatus  != "" ? $sub->substatus  : '-') : '-',
                        "CancellationReason" => $sub !== null ? (isset($sub->cancellationdefaultContent) && $sub->cancellationdefaultContent  != "" ? $sub->cancellationdefaultContent  : '-') : '-',
                        "OtherReason" => $sub !== null ? (isset($sub->cancellationotherreason) && $sub->cancellationotherreason  != "" ? $sub->cancellationotherreason  : '-') : '-',
                        "PauseSubscription" => $sub !== null ? (isset($sub->pauseplan) && $sub->pauseplan  != "" ? $sub->pauseplan  : '-') : '-',
                        "OnHoldDate" => $sub !== null ? ($sub->OnHoldDate == null ? '-' : ($sub->OnHoldDate == '' ? '-' : $sub->OnHoldDate)) : '-',
                        "UnrealizedDate" => $sub !== null ? ($sub->UnrealizedDate == null ? '-' : ($sub->UnrealizedDate == '' ? '-' : $sub->UnrealizedDate)) : '-',
                        "created_at" => $sub !== null ? (isset($sub->subcreatedat) && $sub->subcreatedat  != "" ? $sub->subcreatedat  : \Carbon\Carbon::now()) : \Carbon\Carbon::now(),
                        "updated_at" => $sub !== null ? (isset($sub->subupdatedAt) && $sub->subupdatedAt  != "" ? $sub->subupdatedAt  : \Carbon\Carbon::now()) : \Carbon\Carbon::now(),
                        // "created_at" => \Carbon\Carbon::now(),
                        // "updated_at" => \Carbon\Carbon::now(),

                    ];
                    $countdata++;
                    // if (!in_array($e["SubID"], $cSummaryIds)) {
                    try {
                        // array_push($data, $e);
                        echo " || ";
                        echo $countdata . $e["SubID"] . ' => ' . $e["UserID"];
                        echo " || ";
                        $checksummarysub = SummarySubscriber::where('SubID', $e["SubID"])
                        ->count();
                        if($checksummarysub <= 0){
                        SummarySubscriber::insert($e);
                        }
                    } catch (Exception $err) {
                        echo $err->getMessage();
                    }
                    // }
                }
            });
    }

    public function UpdateSubscriberSummary()
    {
        $this->Log->info('UpdateSubscriberSummary Process start.');
echo "UpdateSubscriberSummary Process start.";
        // $reports = collect();
        DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('rechargehistories', 'rechargehistories.subscriptionId', 'subscriptions.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->leftJoin('deliveryaddresses as ShippingAddress', 'subscriptions.DeliveryAddressId', '=', 'ShippingAddress.id')
            ->leftJoin('deliveryaddresses as BillingAddress', 'subscriptions.BillingAddressId', '=', 'BillingAddress.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->leftJoin('sellerusers', 'subscriptions.SellerUserId', '=', 'sellerusers.id')
            ->leftJoin('marketingoffices', 'subscriptions.MarketingOfficeId', '=', 'marketingoffices.id')
            ->leftJoin('marketingoffices as smarketingoffices', 'sellerusers.MarketingOfficeId', '=', 'smarketingoffices.id')
            ->leftJoin('pause_plan_histories', function ($query) {
                $query->on('pause_plan_histories.subscriptionIds', '=', 'subscriptions.id');
                $query->take(1);
            })
            // ->leftJoin('summary_subscribers', 'subscriptions.id', '=', 'summary_subscribers.SubID')
            ->select(
                'subscriptions.id as id',
                'subscriptions.*',
                'subscriptions.status as substatus',
                'subscriptions.cancellationReason as subcancellationReason',
                'subscriptions.created_at as subcreatedat',
                'subscriptions.updated_at as subupdatedAt',
                'ShippingAddress.id as shippingaddressid',
                'ShippingAddress.address as saddress',
                'ShippingAddress.portalCode  as sportalCode',
                'ShippingAddress.city as scity',
                'ShippingAddress.state as sstate',
                'BillingAddress.id as billingaddressid',
                'BillingAddress.address as baddress',
                'BillingAddress.portalCode  as bportalCode',
                'BillingAddress.city as bcity',
                'BillingAddress.state as bstate',
                'countries.codeIso as countrycodeIso',
                'countries.name as countryName',
                'plansku.duration',
                'plansku.sku',
                'plansku.isAnnual',
                'sellerusers.badgeId',
                'marketingoffices.moCode',
                'smarketingoffices.moCode as smoCode',
                'pause_plan_histories.id as pauseid',
                'users.id as uid',
                'users.firstName as ufirstName',
                'users.lastName as ulastName'
            )
            // ->whereRaw('subscriptions.updated_at <> summary_subscribers.updated_at')
            ->whereDate('subscriptions.updated_at', '>=', \Carbon\Carbon::now()->subDays(30))
            ->where(function ($q) {
                return $q->orWhereDate('subscriptions.updated_at', '>=', \Carbon\Carbon::now()->subDays(30))
                    ->orWhereDate('users.updated_at', '>=', \Carbon\Carbon::now()->subDays(30))
                    ->orWhereDate('ShippingAddress.updated_at', '>=', \Carbon\Carbon::now()->subDays(30))
                    ->orWhereDate('BillingAddress.updated_at', '>=', \Carbon\Carbon::now()->subDays(30));
            })
            ->orderBy('subscriptions.created_at', 'DESC')
            // ->chunk(1000, function ($subscriptions) use (&$reports) {
            //     foreach ($subscriptions as $sub) {

            //     }
            //     $reports = $reports->merge($subscriptions);
            //     // $this->Log->info(count($subscriptions));
            // });
            ->chunk(1000, function ($subscriptions) {
                echo count($subscriptions);
                foreach ($subscriptions as $sub) {
                    echo $sub->id."//";
                    // echo "SUBID : " . $sub->id . ", Last Update: " . $sub->updated_at . "\n";
                    // $this->Log->info($sub->id);
                    // $_order = Orders::select('id','created_at')->where("email", $sub->email)->orderBy('created_at', 'desc')->first();
                    $_order = Orders::select('id', 'created_at')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    // $current_order = Orders::select('status')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    if (isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ufirstName . " " . $sub->ulastName;
                    } else if (!isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ulastName;
                    } else {
                        $sub->ufullname = $sub->ufirstName;
                    }

                    if ($_order !== null) {
                        $sub->formattedId = $this->orderService->formatOrderNumberV3($_order->id, $_order->created_at, $sub->countrycodeIso, true);
                        if ($sub->isTrial === 1 && $sub->convertTrial2Subs) {
                            //take second order
                            $sub->conversion_date = isset($sub->convertTrial2Subs) ? $sub->convertTrial2Subs : '-';
                        } else {
                            $sub->conversion_date = null;
                        }
                        // $sub->currentOrderStatus = null;
                        // foreach ($_order as $order) {
                        //     if($order->id ==  $sub->currentOrderId){
                        //         $sub->currentOrderStatus = $order->status;
                        //     }
                        // }
                    } else {
                        $sub->formattedId = "";
                    }

                    // if ($current_order !== null) {
                    //     $sub->currentOrderStatus = $current_order->status;
                    // }

                    $sub->created_date = date('Y-m-d', strtotime($sub->subcreatedat));

                    if ($sub->shippingaddressid) {
                        $sub->deliveryaddress = $sub->saddress . " " . $sub->sportalCode . " " . $sub->scity . " " . $sub->sstate;
                    } else {
                        $sub->deliveryaddress = "";
                    }

                    if ($sub->billingaddressid) {
                        $sub->billingaddress = $sub->baddress . " " . $sub->bportalCode . " " . $sub->bcity . " " . $sub->bstate;
                    } else {
                        $sub->billingaddress = "";
                    }

                    if ($sub->MarketingOfficeId) {
                        $sub->moCode = $sub->moCode;
                    } else {
                        if ($sub->smoCode) {
                            $sub->moCode = $sub->smoCode;
                        } else {
                            $sub->moCode = "";
                        }
                    }
                    // $sub->cancellationReason = "";
                    // $sub->canceledDate = "";
                    // if ($sub->substatus == 'Cancelled') {
                    //     $sub->cancellationReason = $sub->subcancellationReason;
                    //     if ($sub->canceldate) {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->canceldate));
                    //     }
                    //     else {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->subupdatedAt));
                    //     }
                    //   }

                    if ($sub->substatus == "Cancelled") {
                        $sub->cancellation = DB::table('cancellation_journeys')->where('SubscriptionId', $sub->id)->orderBy('created_at', 'DESC')->first();

                        if ($sub->cancellation != null) {
                            $sub->cancellationcreated_date = date('Y-m-d', strtotime($sub->cancellation->created_at));
                            $sub->cancellationotherreason = $sub->cancellation->OtherReason;
                            $cancellationTranslates = CancellationReasonTranslates::where("id", $sub->cancellation->CancellationTranslatesId)->first();
                            if ($cancellationTranslates) {
                                $sub->cancellationdefaultContent = $cancellationTranslates->defaultContent;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        } else {
                            $sub->cancellation = DB::table('subscriptionhistories')
                                ->where('subscriptionId', $sub->id)
                                ->where(function ($query) {
                                    $query->orWhere('message',  'Canceled')
                                        ->orWhere('message', 'Cancelled');
                                })
                                // ->whereNotNull('detail')
                                ->orderBy('created_at', 'DESC')
                                ->first();

                            if ($sub->cancellation != null) {
                                $sub->cancellationdefaultContent = $sub->cancellation->detail;
                                $sub->cancellationcreated_date = date('Y-m-d', strtotime($sub->cancellation->created_at));
                            }
                        }

                        if (isset($sub->cancellationdefaultContent)) {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason . "," . $sub->cancellationdefaultContent;
                            }
                        } else {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        }
                    } else if ($sub->substatus === "Payment Failure") {
                        $sub->cancellation = DB::table('subscriptionhistories')
                            ->where('subscriptionId', $sub->id)
                            ->where('message', 'Payment Failure')
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if ($sub->cancellation != null) {
                            $sub->cancellationdefaultContent = $sub->cancellation->detail;
                            // $sub->cancellationcreated_date = date('Y-m-d', strtotime($sub->cancellation->created_at));
                        }

                        if ($sub->subcancellationReason === "Cancelled due to payment failed" || $sub->subcancellationReason === "Canceled due to payment failed" || $sub->subcancellationReason === "Cancelled due to user card has insufficient funds.") {
                            $sub->cancellationdefaultContent = $sub->subcancellationReason;
                        } else {
                            $sub->cancellationdefaultContent = "-";
                        }
                    }

                    $sub->blade_type = null;
                    $sub->shave_cream = null;
                    $sub->after_shave_cream = null;
                    $sub->pouch = null;
                    $sub->rubber_handle = null;

                    $products = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('plan_details', 'productcountries.id', '=', 'plan_details.ProductCountryId')
                        ->where('plan_details.PlanId', $sub->PlanId)
                        ->select('products.*', 'plan_details.qty as pd_qty')
                        ->get();

                    foreach ($products as $product) {
                        if (in_array($product->sku, config('global.all.blade_types.skus'))) {
                            $sub->blade_type = config('global.all.blade_no_v2.' . $product->sku) . ' blade';
                            $sub->blade_quantity = $product->pd_qty;
                        }
                    }

                    // if ($sub->isTrial === 1 && ($sub->currentCycle === 1 || $sub->currentCycle === 0)) {
                    //     $sub->blade_quantity = 1;
                    // }

                    $addons = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('subscriptions_productaddon', 'productcountries.id', '=', 'subscriptions_productaddon.ProductCountryId')
                        ->where('subscriptions_productaddon.subscriptionId', $sub->id)
                        ->select('products.*')
                        ->get();

                    foreach ($addons as $addon) {
                        if (in_array($addon->sku, config('global.all.aftershavecream_types.skus'))) {
                            $sub->after_shave_cream = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.shavecream_types.skus'))) {
                            $sub->shave_cream = $product->id;
                        }
                        if (in_array($addon->sku, config('global.all.pouch_types.skus'))) {
                            $sub->pouch = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.razor_potector.skus'))) {
                            $sub->rubber_handle = $addon->id;
                        }
                    }


                    if ($sub->pauseid) {
                        $sub->pauseplan = "Yes";
                    } else {
                        $sub->pauseplan = "No";
                    }
                    $sub->reactivatedplan = null;

                    if ($sub->currentDeliverNumber == 0 && $sub->isCustom == 1 && $sub->isTrial != 1 && ($sub->substatus !== "Payment Failure" && $sub->substatus !== "On Hold" && $sub->substatus !== "Pending")) {
                        $sub->currentDeliverNumber = "1";
                    }

                    $e = [
                        "SubID" => $sub !== null ? (isset($sub->id) && $sub->id  != "" ? $sub->id  : '-') : '-',
                        "UserID" => $sub !== null ? (isset($sub->uid) && $sub->uid  != "" ? $sub->uid  : '-') : '-',
                        "CustomerName" => $sub !== null ? (isset($sub->ufullname) && $sub->ufullname  != "" ? $sub->ufullname  : '-') : '-',
                        "Email" =>  $sub !== null ? (isset($sub->email) && $sub->email  != "" ? $sub->email  : '-') : '-',
                        "Country" => $sub !== null ? (isset($sub->countryName) && $sub->countryName  != "" ? $sub->countryName  : '-') : '-',
                        "Category" => $sub !== null ? (isset($sub->isOffline) && $sub->isOffline == 1 ? "BA" : "eCommerce") : '-',
                        "MOCode" => $sub !== null ? (isset($sub->moCode) && $sub->moCode  != "" ? $sub->moCode  : '-') : '-',
                        "BadgeID" => $sub !== null ? (isset($sub->badgeId) && $sub->badgeId  != "" ? $sub->badgeId  : '-') : '-',
                        "DeliveryAddress" => $sub !== null ? (isset($sub->deliveryaddress) && $sub->deliveryaddress  != "" ? $sub->deliveryaddress  : '-') : '-',
                        "PlanType" => $sub !== null ? (isset($sub->isTrial) && $sub->isTrial == 1 ? "TK" : (isset($sub->isCustom) && $sub->isCustom == 1 ? "Custom" : "Custom")) : 'Custom',
                        "BladeType" => $sub !== null ? (isset($sub->blade_type) && $sub->blade_type !== null ? $sub->blade_type : "0") : '-',
                        "DeliveryInterval" => $sub !== null ? (isset($sub->isAnnual) && $sub->isAnnual != 0 ? '12' : (isset($sub->duration) && $sub->duration != "" ? $sub->duration  : '-')) : '-',
                        "CassetteCycle" => $sub !== null ? (isset($sub->blade_quantity) && $sub->blade_quantity  != "" ? $sub->blade_quantity  : '-') : '-',
                        "CycleCount" => $sub !== null ?  (isset($sub->currentDeliverNumber) && $sub->currentDeliverNumber  != "" ? $sub->currentDeliverNumber  : '0') : '0',
                        "AfterShaveCream" => $sub !== null ? (isset($sub->after_shave_cream) && $sub->after_shave_cream !== null ? "1" : "0") : "0",
                        "ShaveCream" => $sub !== null ? (isset($sub->shave_cream) && $sub->shave_cream !== null ? "1" : "0") : "0",
                        "Pouch" => $sub !== null ? (isset($sub->pouch) && $sub->pouch !== null ? "1" : "0") : "0",
                        "RubberHandle" => $sub !== null ? (isset($sub->rubber_handle) && $sub->rubber_handle !== null ? "1" : "0") : "0",
                        "SignUp" => $sub !== null ? (isset($sub->created_date) && $sub->created_date  != "" ? $sub->created_date  : '-') : '-',
                        "Conversion" => $sub !== null ? (isset($sub->conversion_date) && $sub->conversion_date  != "" ? $sub->conversion_date  : '-') : '-',
                        "LastDelivery" => $sub !== null ? (isset($sub->lastDeliverydate) && $sub->lastDeliverydate  != "" ? $sub->lastDeliverydate  : '-') : '-',
                        "LastCharge" => $sub !== null ? (isset($sub->currentOrderTime) && $sub->currentOrderTime  != "" ? $sub->currentOrderTime  : '-') : '-',
                        "NextCharge" => $sub !== null ? (isset($sub->nextChargeDate) && $sub->nextChargeDate  != "" ? $sub->nextChargeDate  : '-') : '-',
                        "RechargeAttempt" => $sub !== null ? (isset($sub->recharge_date) && $sub->recharge_date !== null ? "Yes" : "No") : "No",
                        "RechargeCount" => $sub !== null ? (isset($sub->total_recharge) && $sub->total_recharge !== null ? $sub->total_recharge : "0") : '0',
                        "CancellationDate" => $sub !== null ? (isset($sub->cancellationcreated_date) && $sub->cancellationcreated_date  != "" ? $sub->cancellationcreated_date  : '') : '',
                        "Status" => $sub !== null ? (isset($sub->substatus) && $sub->substatus  != "" ? $sub->substatus  : '-') : '-',
                        "CancellationReason" => $sub !== null ? (isset($sub->cancellationdefaultContent) && $sub->cancellationdefaultContent  != "" ? $sub->cancellationdefaultContent  : '-') : '-',
                        "OtherReason" => $sub !== null ? (isset($sub->cancellationotherreason) && $sub->cancellationotherreason  != "" ? $sub->cancellationotherreason  : '-') : '-',
                        "PauseSubscription" => $sub !== null ? (isset($sub->pauseplan) && $sub->pauseplan  != "" ? $sub->pauseplan  : '-') : '-',
                        "OnHoldDate" => $sub !== null ? $sub->OnHoldDate : '-',
                        "UnrealizedDate" => $sub !== null ? $sub->UnrealizedDate : '-',
                        "created_at" => $sub !== null ? (isset($sub->subcreatedat) && $sub->subcreatedat  != "" ? $sub->subcreatedat  : \Carbon\Carbon::now()) : \Carbon\Carbon::now(),
                        "updated_at" => $sub !== null ? (isset($sub->subupdatedAt) && $sub->subupdatedAt  != "" ? $sub->subupdatedAt  : \Carbon\Carbon::now()) : \Carbon\Carbon::now(),
                        // "created_at" => \Carbon\Carbon::now(),
                        // "updated_at" => \Carbon\Carbon::now(),
                    ];
                    DB::table('summary_subscribers')
                        ->where('SubID', $sub->id)
                        ->update($e);
                }
            });

        // DB::table('summary_subscribers')
        // ->leftJoin('subscriptions', 'summary_subscribers.SubID', '=', 'subscriptions.id')
        // ->select('summary_subscribers.*')
        // ->whereRaw('subscriptions.updated_at = summary_subscribers.updated_at')
        // ->select('summary_subscribers.*')
        // ->orderBy('summary_subscribers.created_at', 'DESC')
        // ->chunk(1000, function ($summaries) use (&$reports) {
        //     foreach ($summaries as $summary) {
        //         $sub = $reports->filter(function($item) use ($summary) {
        //             return $item->id == $summary->SubID;
        //         })->first();

        //         if ($sub !== null) {
        //             // $this->Log->info($sub->id);
        //             $_order = Orders::select('id','created_at','status')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
        //             if( isset($sub->ufirstName) && isset($sub->ulastName)){
        //                 $sub->ufullname = $sub->ufirstName . " " . $sub->ulastName;
        //             }
        //             else if(  !isset($sub->ufirstName) && isset($sub->ulastName)){
        //                 $sub->ufullname = $sub->ulastName;
        //             }
        //             else{
        //                 $sub->ufullname = $sub->ufirstName;
        //             }

        //             if ($_order !== null) {
        //                 $sub->formattedId = $this->orderService->formatOrderNumberV3($_order->id,$_order->created_at , $sub->countrycodeIso, true);
        //                 if ($sub->isTrial === 1 && $sub->convertTrial2Subs) {
        //                     //take second order
        //                     $sub->conversion_date = isset($sub->convertTrial2Subs) ? date('d/m/y', strtotime($sub->convertTrial2Subs)) : '-';
        //                 } else {
        //                     $sub->conversion_date = null;
        //                 }
        //                 $sub->currentOrderStatus = $_order->status;
        //                 // $sub->currentOrderStatus = null;
        //                 // foreach ($_order as $order) {
        //                 //     if($order->id ==  $sub->currentOrderId){
        //                 //         $sub->currentOrderStatus = $order->status;
        //                 //     }
        //                 // }
        //             }
        //             else {
        //                 $sub->formattedId = "";
        //             }

        //             $sub->created_date = date('d/m/y H:i', strtotime($sub->created_at));

        //             if ($sub->shippingaddressid) {
        //                 $sub->deliveryaddress = $sub->saddress . " " . $sub->sportalCode . " " . $sub->scity . " " . $sub->sstate;
        //             } else {
        //                 $sub->deliveryaddress = "";
        //             }

        //             if ($sub->billingaddressid) {
        //                 $sub->billingaddress = $sub->baddress . " " . $sub->bportalCode . " " . $sub->bcity . " " . $sub->bstate;
        //             } else {
        //                 $sub->billingaddress = "";
        //             }

        //             if ($sub->MarketingOfficeId) {
        //                 $sub->moCode = $sub->moCode;
        //             } else {
        //                 $sub->moCode = "";
        //             }
        //             // $sub->cancellationReason = "";
        //             // $sub->canceledDate = "";
        //             // if ($sub->substatus == 'Cancelled') {
        //             //     $sub->cancellationReason = $sub->subcancellationReason;
        //             //     if ($sub->canceldate) {
        //             //         $sub->canceledDate = date('d/m/y', strtotime($sub->canceldate));
        //             //     }
        //             //     else {
        //             //         $sub->canceledDate = date('d/m/y', strtotime($sub->subupdatedAt));
        //             //     }
        //             //   }

        //             if($sub->substatus == "Cancelled"){
        //                 $sub->cancellation = DB::table('cancellation_journeys')->where('SubscriptionId', $sub->id)->orderBy('created_at', 'DESC')->first();

        //                 if ($sub->cancellation) {
        //                     $sub->cancellationcreated_date = date('d/m/y H:i', strtotime($sub->cancellation->created_at));
        //                     $sub->cancellationotherreason = $sub->cancellation->OtherReason;
        //                     $cancellationTranslates = CancellationReasonTranslates::where("id", $sub->cancellation->CancellationTranslatesId)->first();
        //                     if ($cancellationTranslates) {
        //                         $sub->cancellationdefaultContent = $cancellationTranslates->defaultContent;
        //                     } else {
        //                         $sub->cancellationdefaultContent = "";
        //                     }
        //                 }
        //                 if(isset($sub->cancellationdefaultContent)){
        //                     if(isset($sub->subcancellationReason)){
        //                         $sub->cancellationdefaultContent = $sub->subcancellationReason.",". $sub->cancellationdefaultContent;
        //                     }
        //                 }else{
        //                     if(isset($sub->subcancellationReason)){
        //                         $sub->cancellationdefaultContent = $sub->subcancellationReason;
        //                     }
        //                 }
        //             }

        //             $sub->blade_type = null;
        //             $sub->shave_cream = null;
        //             $sub->after_shave_cream = null;
        //             $sub->pouch = null;
        //             $sub->rubber_handle = null;

        //             $products = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
        //                 ->leftJoin('plan_details', 'productcountries.id', '=', 'plan_details.ProductCountryId')
        //                 ->where('plan_details.PlanId', $sub->PlanId)
        //                 ->select('products.*', 'plan_details.qty as pd_qty')
        //                 ->get();

        //             foreach ($products as $product) {
        //                 if (in_array($product->sku, config('global.all.blade_types.skus'))) {
        //                     $sub->blade_type = config('global.all.blade_no_v2.' . $product->sku) . ' blade';
        //                     $sub->blade_quantity = $product->pd_qty;
        //                 }
        //             }

        //             if ($sub->isTrial === 1 && ($sub->currentCycle === 1 || $sub->currentCycle === 0)) {
        //                 $sub->blade_quantity = 1;
        //             }

        //             $addons = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
        //                 ->leftJoin('subscriptions_productaddon', 'productcountries.id', '=', 'subscriptions_productaddon.ProductCountryId')
        //                 ->where('subscriptions_productaddon.subscriptionId', $sub->id)
        //                 ->select('products.*')
        //                 ->get();

        //             foreach ($addons as $addon) {
        //                 if (in_array($addon->sku, config('global.all.aftershavecream_types.skus'))) {
        //                     $sub->after_shave_cream = $addon->id;
        //                 }
        //                 if (in_array($addon->sku, config('global.all.shavecream_types.skus'))) {
        //                     $sub->shave_cream = $product->id;
        //                 }
        //                 if (in_array($addon->sku, config('global.all.pouch_types.skus'))) {
        //                     $sub->pouch = $addon->id;
        //                 }
        //                 if (in_array($addon->sku, config('global.all.handle_types.rubber.skus'))) {
        //                     $sub->rubber_handle = $addon->id;
        //                 }
        //             }


        //             if ($sub->pauseid) {
        //                 $sub->pauseplan = "Yes";
        //             } else {
        //                 $sub->pauseplan = "No";
        //             }
        //             $sub->reactivatedplan = null;

        //             $e = [
        //                 // "SubID" => $sub !== null ? (isset($sub->id) && $sub->id  != "" ? $sub->id  : '-') : '-',
        //                 "UserID" => $sub !== null ? (isset($sub->uid) && $sub->uid  != "" ? $sub->uid  : '-') : '-',
        //                 "CustomerName" => $sub !== null ? (isset($sub->ufullname) && $sub->ufullname  != "" ? $sub->ufullname  : '-') : '-',
        //                 "Email" =>  $sub !== null ? (isset($sub->email) && $sub->email  != "" ? $sub->email  : '-') : '-',
        //                 "Country" => $sub !== null ? (isset($sub->countryName) && $sub->countryName  != "" ? $sub->countryName  : '-') : '-',
        //                 "Category" => $sub !== null ? (isset($sub->isOffline) && $sub->isOffline == 1 ? "BA" : "eCommerce") : '-',
        //                 "MOCode" => $sub !== null ? (isset($sub->moCode) && $sub->moCode  != "" ? $sub->moCode  : '-') : '-',
        //                 "BadgeID" => $sub !== null ? (isset($sub->badgeId) && $sub->badgeId  != "" ? $sub->badgeId  : '-') : '-',
        //                 "DeliveryAddress" => $sub !== null ? (isset($sub->deliveryaddress) && $sub->deliveryaddress  != "" ? $sub->deliveryaddress  : '-') : '-',
        //                 "PlanType" => $sub !== null ? (isset($sub->isTrial) && $sub->isTrial == 1 ? "TK" : ( isset($sub->isCustom) && $sub->isCustom == 1 ? "Custom" : "")) : '-',
        //                 "BladeType" => $sub !== null ? (isset($sub->blade_type) && $sub->blade_type !== null ? $sub->blade_type : "0") : '-',
        //                 "DeliveryInterval" => $sub !== null ? (isset($sub->isAnnual) && $sub->isAnnual != 0 ? '12' : (isset($sub->duration) && $sub->duration != "" ? $sub->duration  : '-')) : '-',
        //                 "CassetteCycle" => $sub !== null ? (isset($sub->blade_quantity) && $sub->blade_quantity  != "" ? $sub->blade_quantity  : '-') : '-',
        //                 "CycleCount" => $sub !== null ?  (isset($sub->currentDeliverNumber) && $sub->currentDeliverNumber  != "" ? $sub->currentDeliverNumber  : '0') : '0',
        //                 "AfterShaveCream" => $sub !== null ? (isset($sub->after_shave_cream) && $sub->after_shave_cream !== null ? "1" : "0") : "0",
        //                 "ShaveCream" => $sub !== null ? (isset($sub->shave_cream) && $sub->shave_cream !== null ? "1" : "0") : "0",
        //                 "Pouch" => $sub !== null ? (isset($sub->pouch) && $sub->pouch !== null ? "1" : "0") : "0",
        //                 "RubberHandle" => $sub !== null ? (isset($sub->rubber_handle) && $sub->rubber_handle !== null ? "1" : "0") : "0",
        //                 "SignUp" => $sub !== null ? (isset($sub->created_date) && $sub->created_date  != "" ? $sub->created_date  : '-') : '-',
        //                 "Conversion" => $sub !== null ? (isset($sub->conversion_date) && $sub->conversion_date  != "" ? $sub->conversion_date  : '-') : '-',
        //                 "LastDelivery" => $sub !== null ? (isset($sub->lastDeliverydate) && $sub->lastDeliverydate  != "" ? $sub->lastDeliverydate  : '-') : '-',
        //                 "LastCharge" => $sub !== null ? (isset($sub->currentOrderTime) && $sub->currentOrderTime  != "" ? $sub->currentOrderTime  : '-') : '-',
        //                 "NextCharge" => $sub !== null ? (isset($sub->nextChargeDate) && $sub->nextChargeDate  != "" ? $sub->nextChargeDate  : '-') : '-',
        //                 "RechargeAttempt" => $sub !== null ? (isset($sub->recharge_date) && $sub->recharge_date !== null ? "Yes" : "No") : "No",
        //                 "RechargeCount" => $sub !== null ? (isset($sub->total_recharge) && $sub->total_recharge !== null ? $sub->total_recharge : "0") : '0',
        //                 "CancellationDate" => $sub !== null ? (isset($sub->cancellationcreated_date) && $sub->cancellationcreated_date  != "" ? $sub->cancellationcreated_date  : '-') : '-',
        //                 "Status" => $sub !== null ? (isset($sub->substatus) && $sub->substatus  != "" ? $sub->substatus  : '-') : '-',
        //                 "CancellationReason" => $sub !== null ? (isset($sub->cancellationdefaultContent) && $sub->cancellationdefaultContent  != "" ? $sub->cancellationdefaultContent  : '-') : '-',
        //                 "OtherReason" => $sub !== null ? (isset($sub->cancellationotherreason) && $sub->cancellationotherreason  != "" ? $sub->cancellationotherreason  : '-') : '-',
        //                 "PauseSubscription" => $sub !== null ? (isset($sub->pauseplan) && $sub->pauseplan  != "" ? $sub->pauseplan  : '-') : '-',
        //                 // "created_at" => \Carbon\Carbon::now(),
        //                 "updated_at" => \Carbon\Carbon::now(),
        //             ];
        //             DB::table('summary_subscribers')
        //                 ->where('id', $summary->id)
        //                 ->update($e);
        //         }
        //     }
        // });
        $this->Log->info('UpdateSubscriberSummary Process end.');
    }
}
