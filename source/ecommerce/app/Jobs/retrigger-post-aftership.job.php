<?php

namespace App\Jobs;


use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Helpers\Warehouse\AftershipHelper;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;

use App\Models\User\DeliveryAddresses;
use App\Models\Orders\Orders;
use App\Models\BulkOrders\BulkOrders;
use Illuminate\Support\Facades\Storage;
use Exception;

class RetriggerPostAfterShipJob extends Command
{
    protected $signature = 'retrigger-post-aftership:job';
    protected $description = 'Retrigger post aftership';

    public function __construct()
    {
        parent::__construct();
        $this->Job = 'retrigger-post-aftership: ';
        $this->Log = \Log::channel('cronjob');
        $this->_aftership = new AftershipHelper;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $this->triggerOrder();
        // $this->triggerBulkOrder();
        // $this->checkAfterShipOrderDeliver();
        // $this->checkAfterShipBulkOrderDeliver();
    }
    public function triggerOrder()
    {
        $this->Log->info('Retrigger order post aftership start');
        $_order = Orders::where('status','Delivering')->whereNotNull('deliveryId')->whereNotNull('carrierAgent')->whereDate('created_at', '>=', '2020-05-01')->get();
        if ($_order) {
            foreach ($_order as $od) {
                try {
                    $this->_aftership->getTrackingOneByOne($od->carrierAgent, $od->deliveryId);
                } catch (Exception $ex) {
                    if ($ex->getMessage() == "NotFound: 4004 - Tracking does not exist.") {
                        try {
                            echo " o : ". $od->id;
                            $country = Countries::where('id', $od->CountryId)->first();
                            $user = User::where('id', $od->UserId)->first();
                            $ccode = "";
                            if(isset($country)){
                                if($country->codeIso){
                                $ccode = $country->codeIso;
                                }
                            }
                            $shipment = DeliveryAddresses::where('id', $od->DeliveryAddressId)->first();
                            $options = [
                                'order' => $od,
                                // 'country' => $country,
                                'shipment' => $shipment,
                                'user' => $user,
                            ];

                            $this->_aftership->postTracking($options , $ccode, false);
                        } catch (Exception $ex) {
                            $this->Log->error('Retrigger post aftership post ERROR: ' . $ex->getMessage());
                        }
                    } else {
                        $this->Log->error('Retrigger post aftership tracking ERROR: ' . $ex->getMessage());
                    }
                }
            }
        }
    }

    public function triggerBulkOrder()
    {
        $this->Log->info('Retrigger bulkorder post aftership start');
        $_order = BulkOrders::where('status','Delivering')->whereNotNull('deliveryId')->whereNotNull('carrierAgent')->whereDate('created_at', '>=', '2020-05-01')->get();
        if ($_order) {
            foreach ($_order as $od) {
                try {
                    $this->_aftership->getTrackingOneByOne($od->carrierAgent, $od->deliveryId);
                } catch (Exception $ex) {
                    if ($ex->getMessage() == "NotFound: 4004 - Tracking does not exist.") {
                        try {
                            echo " b : ". $od->id;
                            $country = Countries::where('id', $od->CountryId)->first();
                            $user = null;
                            $ccode = "";
                            if(isset($country)){
                                if($country->codeIso){
                                $ccode = $country->codeIso;
                                }
                            }
                            $shipment = DeliveryAddresses::where('id', $od->DeliveryAddressId)->first();
                            $options = [
                                'order' => $od,
                                // 'country' => $country,
                                'shipment' => $shipment,
                                'user' => $user,
                            ];

                            $this->_aftership->postTracking($options,$ccode, true);
                        } catch (Exception $ex) {
                            $this->Log->error('Retrigger post aftership post ERROR: ' . $ex->getMessage());
                        }
                    } else {
                        $this->Log->error('Retrigger post aftership tracking ERROR: ' . $ex->getMessage());
                    }
                }
            }
        }
    }

    public function checkAfterShipOrderDeliver(){
        $this->Log->info('checkAfterShipOrderDeliver');
        $_order = Orders::where('status','Delivering')
        ->get();
        if ($_order) {
            $localpath  = storage_path('app/report/ordernoupdate1.csv');
            $fp = fopen($localpath, 'w');
            fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            fputcsv($fp, [
                "orderid"
            ]);
            foreach ($_order as $od) {
                try {
                    echo $od->id;
                   $getdata = $this->_aftership->getTrackingOneByOne($od->carrierAgent, $od->deliveryId);
                   if(isset($getdata)){
                    echo $getdata["data"]["tracking"]["tag"]."/n";
                    if($getdata["data"]["tracking"]["tag"] == "Delivered"){
                        echo $od->id."/n";
                        fputcsv($fp, [ "orderid" => $od->id , "deleverdate" => $getdata["data"]["tracking"]["last_updated_at"]]);
                    }
                   }
               
                 } catch (Exception $ex) {
                    $this->Log->error('checkAfterShipOrderDeliver ERROR: ' . $ex->getMessage());
                 }
            }
          
            fclose($fp);
        }
    }
    public function checkAfterShipBulkOrderDeliver(){
        $this->Log->info('checkAfterShipBulkOrderDeliver');
        $_order = BulkOrders::where('status','Delivering')
        ->get();
        if ($_order) {
            $localpath  = storage_path('app/report/bulkordernoupdate1.csv');
            $fp = fopen($localpath, 'w');
            fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            fputcsv($fp, [
                "bulkorderid"
            ]);
            foreach ($_order as $od) {
                try {
                   
                   $getdata = $this->_aftership->getTrackingOneByOne($od->carrierAgent, $od->deliveryId);
                   if(isset($getdata)){
                    if($getdata["data"]["tracking"]["tag"] == "Delivered"){
                        echo $od->id."/n";
                        fputcsv($fp, [ "bulkorderid" => $od->id , "deleverdate" => $getdata["data"]["tracking"]["last_updated_at"]]);
                    }
                   }
               
                 } catch (Exception $ex) {
                    $this->Log->error('checkAfterShipBulkOrderDeliver ERROR: ' . $ex->getMessage());
                 }
            }
          
            fclose($fp);
        }
    }
}
