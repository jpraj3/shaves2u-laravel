<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Session\Store;

class CustomerRegisteredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;

    public $CountryAndLocaleData;
   
    public function __construct($data, array $CountryAndLocaleData)
    {

        $this->data = $data;
        $this->CountryAndLocaleData = $CountryAndLocaleData;
    }

}
