<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use App\Models\Ecommerce\User;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;

class UserResetPasswordNotification extends Notification
{

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }
        $moduleData = (object) array();
        $moduleData->email = $notifiable->getEmailForPasswordReset();

        $fullname = "";
        $userGender = User::select('gender', 'firstName', 'lastName')->where('email', $moduleData->email)->first();
  

        $moduleData->identifiers = (object) array();
        $moduleData->identifiers->isMale = false;
        $moduleData->identifiers->isFemale = false;

        if ($userGender) {
            switch ($userGender->gender) {
                case 'male':
                    $moduleData->identifiers->isMale = true;
                    break;

                case 'female':
                    $moduleData->identifiers->isFemale = true;
                    break;

                default:
                    $moduleData->identifiers->isMale = true;
                    break;
            }
            if ($userGender->firstName && $userGender->lastName) {
                $fullname = $userGender->firstName . " " . $userGender->lastName;
            } else if ($userGender->firstName) {
                $fullname = $userGender->firstName;
            } else if ($userGender->lastName) {
                $fullname = $userGender->lastName;
            }
        }
        $moduleData->fullname = $fullname;
        $moduleData->token = $this->token;
   
        //     $emailData = [];
        //     $emailData['title'] = 'password-reset';
        //     $emailData['email'] =  $notifiable->getEmailForPasswordReset();
        //     $emailData['token'] =  $this->token;
        //     // Initiate New CustomerRegisteredEvent
        //    $this->emailQueueHelper->buildEmailTemplate('password_reset', $emailData, array($currentCountry, $currentLocale));
        $codeIso = "";
        if (json_decode(session()->get('currentCountry'), true)['codeIso']) {
            $codeIso = json_decode(session()->get('currentCountry'), true)['codeIso'];
        } else {
            $codeIso = 'my';
        }
        $lang = "";
        if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
            $lang = json_decode(session()->get('currentCountry'), true)['defaultLang'];
        } else {
            $lang = 'en';
        }
        $subject = Lang::get('email.subject.password-reset');
        return (new MailMessage)
            ->subject($subject)
            ->view('email-templates.password-reset.template', [
                'moduleData' => $moduleData,
                'country' => json_decode(session()->get('currentCountry'), true),
                'countrycode' => $codeIso,
                'lang' => $lang
            ]);
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
