<?php

namespace App\Http\Controllers\Auth;

use App\Events\CustomerRegisteredEvent;

//Mails & Events
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Ecommerce\User;
use App\Models\Rebates\Referral;
use Auth;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
// use Illuminate\Foundation\Application;
// use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\LanguageDetails;
use App\Services\CountryService as CountryService;
use App;
use OpenGraph;
use SEOMeta;
use Carbon\Carbon;

// use Illuminate\Support\Str;

// use Illuminate\Validation\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $country_service;
    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->request = $request;
        $this->session = new SessionController();
        $this->emailQueueHelper = new EmailQueueHelper();
        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'day_of_birth' => ['required', 'int'],
            'month_of_birth' => ['required', 'int'],
            'year_of_birth' => ['required', 'int'],
        ]);
    }

    protected function api_validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Ecommerce\User
     */
    protected function create(array $data)
    {
        $currentCountryo = $this->request->session()->get('currentCountry');
        $currentCountry = json_decode($currentCountryo);
        if ($data['name'] == trim($data['name'])) {
            $first_name = $data['name'];
            $last_name = null;
        } else {
            $first_name = explode(' ', $data['name'])[0];
            $last_name = explode(' ', $data['name'])[1];
        }

        try {
            $user = User::create([
                'firstName' => $data['name'],
                'lastName' => null,
                'email' => $data['email'],
                'password' => md5($data['password']),
                'socialId' => null,
                'birthday' => $data['year_of_birth'] . '-' . $data['month_of_birth'] . '-' . $data['day_of_birth'],
                'gender' => null,
                'SSN' => null,
                'NRIC' => null,
                'phoneHome' => null,
                'phoneOffice' => null,
                'phone' => null,
                'defaultLanguage' => strtoupper(app()->getLocale()),
                'defaultDateFormat' => 'yyyy/MM/dd',
                'avatarUrl' => null,
                'notificationSettings' => [],
                'isActive' => 1,
                'defaultShipping' => null,
                'defaultBilling' => null,
                'hasReceiveOffer' => null,
                'CountryId' => $currentCountry->id,
                'NotificationTypeId' => null,
                'badgeId' => null,
                'isGuest' => 0,
                'UserTypeId' => null,
                'IsSG_old' => null,
                'createdBy' => null,
                'updateBy' => null,
                'Email_Sub' => isset($data['marketing_sub_agree']) ? 1 : (isset($data['email_sub_agree']) ? 1 : 0),
                'Sms_Sub' => isset($data['marketing_sub_agree']) ? 1 : (isset($data['sms_sub_agree']) ? 1 : 0),
                'referralId' => null,
                'inviteCode' => null,
                'HKG_Marketing_Sub' => 0,
                'registered_at' => Carbon::now(),
                'last_login_at' => Carbon::now(),
            ]);
            $currentLocale = strtoupper(app()->getLocale());
            $this->SendWelcomeEmail($data,$currentCountryo,$currentLocale);

            return $user;
        } catch (Exception $ex) {
            //errors
        }
    }

    protected function api_create(array $data)
    {
        $currentCountryo = $this->request->session()->get('currentCountry');
        $currentCountry = json_decode($currentCountryo);
        if ($data['name'] == trim($data['name'])) {
            $first_name = $data['name'];
            $last_name = null;
        } else {
            $first_name = explode(' ', $data['name'])[0];
            $last_name = explode(' ', $data['name'])[1];
        }

        try {
            $ccountryid = $currentCountry->id;
            $clang = strtoupper(app()->getLocale());

            if ($data['countryid'] && $data['langCode']) {
                $ccountryid = $data['countryid'];
                $clang = strtoupper($data['langCode']);
            }

            $activeCountry = Countries::where([['id', $ccountryid], ['isActive', true]])->first();
            // If country in database exist and isActive
            if (isset($activeCountry)) {
                $languageDetailsData = LanguageDetails::where('CountryId', $activeCountry->id)
                    ->where('sublanguageCode',  $clang)
                    ->first();

                // Convert stdClass Data to JSON
                if (isset($languageDetailsData)) {
                    $sessiondata = (object) array();
                    $sessiondata->id = $activeCountry->id;
                    $sessiondata->code = $activeCountry->code;
                    $sessiondata->codeIso = $activeCountry->codeIso;
                    $sessiondata->defaultLang = strtolower($languageDetailsData->sublanguageCode);
                    $sessiondata->urlLang = strtolower($languageDetailsData->mainlanguageCode);

                    $currentCountry = $sessiondata;
                    $currentCountryo =  json_encode($sessiondata);
                }
          
            }
            $user = User::create([
                'firstName' => $data['name'],
                'lastName' => null,
                'email' => $data['email'],
                'password' => md5($data['password']),
                'socialId' => null,
                'birthday' => $data['birthday'],
                'SSN' => null,
                'NRIC' => null,
                'phoneHome' => null,
                'phoneOffice' => null,
                'phone' => null,
                'defaultLanguage' => $clang,
                'defaultDateFormat' => 'yyyy/MM/dd',
                'avatarUrl' => null,
                'notificationSettings' => [],
                'isActive' => 1,
                'defaultShipping' => null,
                'defaultBilling' => null,
                'hasReceiveOffer' => null,
                'CountryId' =>  $ccountryid,
                'NotificationTypeId' => null,
                'badgeId' => null,
                'isGuest' => 0,
                'UserTypeId' => null,
                'IsSG_old' => null,
                'createdBy' => null,
                'updateBy' => null,
                'Email_Sub' => isset($data['marketing_sub_agree']) ? 1 : (isset($data['email_sub_agree']) ? 1 : 0),
                'Sms_Sub' => isset($data['marketing_sub_agree']) ? 1 : (isset($data['sms_sub_agree']) ? 1 : 0),
                'referralId' => null,
                'inviteCode' => null,
                'HKG_Marketing_Sub' => 0,
                'registered_at' => Carbon::now(),
                'last_login_at' => Carbon::now(),
            ]);

            $this->SendWelcomeEmail($data,$currentCountryo,$clang);

            return $user;
        } catch (Exception $ex) {
            //errors
        }
    }
    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function SendWelcomeEmail($data,$currentCountry,$currentLocale)
    {
        // Get Current Country From Session Variable
        $data['appType'] = config('app.appType');
        // Initiate New CustomerRegisteredEvent
        // hide email redis
        $this->emailQueueHelper->buildEmailTemplate('welcome', $data, array($currentCountry, $currentLocale));
    }
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());

        $locale = App::getLocale();
        $path = $langCode . '-' . $currentCountryIso;
        $url_parameters = json_encode(session()->get('url_parameters'));
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = $_SERVER["REQUEST_URI"];
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);
        return view('auth.register');
    }

    public function checkUserExists($request)
    {

        if ($request) {
            $check = User::where('email', $request->email)->first();
            return $check;
        }
    }

    public function register(Request $request)
    {
        try {
            $this->validator($request->all());

            $check = $this->checkUserExists($request);

            if (isset($check) && $check !== null) {
                if ($check["isActive"] === 0) {
                    return redirect()->back()->with('error', 'User Exists but not active !'); // registered but not active
                } else {
                    return redirect()->back()->with('error', 'User Exists & Active !'); //registered and active
                }
            } else {
                $user = $this->create($request->all());
                $this->guard()->login($user);
                return $this->registered($request, $user)
                    ?: redirect($this->redirectPath());
            }
        } catch (Exception $e) {
            return redirect('register')->withInput();
        }
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        // // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());

        // referral
        if (session()->get('referral_program_info')) {
            $referral_program_info = session()->get('referral_program_info');
            $current_user_info = Auth::user();
            $referrer_info = \App\Helpers\LaravelHelper::ConvertArraytoObject(User::where('inviteCode', $referral_program_info->code)->first());
            if ($referrer_info) {
                // start checking if referrer_id && referee_id exists in referral table
                $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $current_user_info->id)->get();
                if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                    // do nothing
                } else {
                    // check if referrer_id and referee_id are the same
                    if ($current_user_info->id == $referrer_info->id) {
                        // do nothing
                    } else {
                        //update user to be associated with referrer
                        User::where('id', $current_user_info->id)->update(["referralId" => $referrer_info->id]);
                        // create new association between referrer & referee
                        Referral::create([
                            'referralId' => $referrer_info->id,
                            'friendId' => $current_user_info->id,
                            'CountryId' => $referrer_info->CountryId,
                            'source' => $referral_program_info->src,
                            'firstPurchase' => 0,
                            'used' => 0,
                            'usedAmount' => 0,
                            'refereeDiscount' => 0,
                            'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                        ]);

                        session()->forget('referral_program_info');
                    }
                }
            }
        }

        // $this->session->_session('isLoggedIn', 'set', $user);
        return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function api_register(Request $request)
    {
        try {
            $this->api_validator($request->all());
            $check = $this->checkUserExists($request);
            if (isset($check) && $check !== null) {
                if ($check["isActive"] === 0) {
                    return redirect()->back()->with('error', 'User Exists but not active !'); // registered but not active
                } else {
                    return redirect()->back()->with('error', 'User Exists & Active !'); //registered and active
                }
            } else {
                $user = $this->api_create($request->all());
                $this->guard()->login($user);
                return $this->registered($request, $user)
                    ?: redirect($this->redirectPath());

                // referral
                if (session()->get('referral_program_info')) {
                    $referral_program_info = session()->get('referral_program_info');
                    $current_user_info = User::where('id', $user->id)->first();
                    $referrer_info = \App\Helpers\LaravelHelper::ConvertArraytoObject(User::where('inviteCode', $referral_program_info->code)->first());
                    if ($referrer_info) {
                        // start checking if referrer_id && referee_id exists in referral table
                        $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $current_user_info->id)->get();
                        if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                            // do nothing
                        } else {
                            // check if referrer_id and referee_id are the same
                            if ($current_user_info->id == $referrer_info->id) {
                                // do nothing
                            } else {
                                //update user to be associated with referrer
                                User::where('id', $current_user_info->id)->update(["referralId" => $referrer_info->id]);
                                // create new association between referrer & referee
                                Referral::create([
                                    'referralId' => $referrer_info->id,
                                    'friendId' => $current_user_info->id,
                                    'CountryId' => $referrer_info->CountryId,
                                    'source' => $referral_program_info->src,
                                    'firstPurchase' => 0,
                                    'used' => 0,
                                    'usedAmount' => 0,
                                    'refereeDiscount' => 0,
                                    'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                                ]);

                                session()->forget('referral_program_info');
                            }
                        }
                    }
                }

                $this->guard()->login($user);

                return $user;
            }
        } catch (Exception $e) {
            return redirect()->to('register')->withErrors(new \Illuminate\Support\MessageBag(['catch_exception' => $e]));
        }
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
