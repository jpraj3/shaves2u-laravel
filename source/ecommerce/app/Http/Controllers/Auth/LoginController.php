<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Rebates\Referral;
use Auth;
use Exception;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use \Illuminate\Validation\ValidationException as ValidationException;
use App\Models\Orders\Orders;
use App\Services\CountryService as CountryService;
use App;
use OpenGraph;
use SEOMeta;
use Carbon\Carbon;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $country_service;
    public function __construct(Application $app, Request $request)
    {
        $this->middleware('guest')->except('logout','logoutV2');
        $this->app = $app;
        $this->request = $request;
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
        $this->session = new SessionController();
        $this->login_type = null;
        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $guard = null;
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());

        $locale = App::getLocale();
        $path = $langCode . '-' . $currentCountryIso;
        $url_parameters = json_encode(session()->get('url_parameters'));
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = $_SERVER["REQUEST_URI"];
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        if($meta){
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);
        }
        if (Auth::guard($guard)->check()) {
            return redirect('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
        } else {
            return view('auth.login', compact('url_parameters'));
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            // Make sure the user is active
            if ($user->isActive && $this->attemptLogin($request)) {
                // Send the normal successful login response
                $user->last_login_at = Carbon::now();
                $user->save();
                return $this->sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);
                return redirect()
                    ->back()
                    ->withInput($request->only($this->username(), 'remember'))
                    ->withErrors(['active' => 'The email must be activated to login.']);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function api_login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // This section is the only change
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            // Make sure the user is active
            if ($this->attemptLogin($request)) {
                // Send the normal successful login response
                $user->last_login_at = Carbon::now();
                $user->save();
                return $this->api_sendLoginResponse($request);
            } else {
                // Increment the failed login attempts and redirect back to the
                // login form with an error message.
                $this->incrementLoginAttempts($request);

                $response = [
                    "email" => $request->only($this->username(), 'remember'),
                    "error" => 'Email Address does not exist',
                ];

                return $response;
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->api_sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
        ?: redirect()->intended($this->redirectPath());
    }

    protected function api_sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->api_authenticated($request, $this->guard()->user())
        ?: null;
    }

    protected function authenticated(Request $request, $user)
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());

        // referral
        if (session()->get('referral_program_info')) {
            $referral_program_info = session()->get('referral_program_info');
            $current_user_info = Auth::user();
            $referrer_info = User::where('inviteCode', $referral_program_info->code)->first();
            if ($referrer_info) {
                // start checking if referrer_id && referee_id exists in referral table
                $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $current_user_info->id)->get();
                if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                    // do nothing
                } else {
                    // check if referrer_id and referee_id are the same
                    if ($current_user_info->id == $referrer_info->id) {
                        // do nothing
                    }
                    else {
                        $orders = Orders::where('UserId' , $current_user_info->id )->count();
                        if($orders == 0){
                        //update user to be associated with referrer
                        User::where('id', $current_user_info->id)->update(["referralId" => $referrer_info->id]);
                        // create new association between referrer & referee
                        Referral::create([
                            'referralId' => $referrer_info->id,
                            'friendId' => $current_user_info->id,
                            'CountryId' => $referrer_info->CountryId,
                            'source' => $referral_program_info->src,
                            'firstPurchase' => 0,
                            'used' => 0,
                            'usedAmount' => 0,
                            'refereeDiscount' => 0,
                            'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                        ]);

                        session()->forget('referral_program_info');
                        }else{
                            session()->forget('referral_program_info');
                        }
                    }
                }
            }
        }
        $this->session->_session('isLoggedIn', 'set', $user);
        return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
    }

    protected function api_authenticated(Request $request, $user)
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $langCode = strtolower(app()->getLocale());
        // referral
        if (session()->get('referral_program_info')) {
            $referral_program_info = session()->get('referral_program_info');
            $current_user_info = Auth::user();
            $referrer_info = User::where('inviteCode', $referral_program_info->code)->first();
            if ($referrer_info) {
                // start checking if referrer_id && referee_id exists in referral table
                $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $current_user_info->id)->get();
                if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                    // do nothing
                } else {
                    // check if referrer_id and referee_id are the same
                    if ($current_user_info->id == $referrer_info->id) {
                        // do nothing
                    }
                    else {
                        $orders = Orders::where('UserId' , $current_user_info->id )->count();
                        if($orders == 0){
                        //update user to be associated with referrer
                        User::where('id', $current_user_info->id)->update(["referralId" => $referrer_info->id]);

                        // create new association between referrer & referee
                        Referral::create([
                            'referralId' => $referrer_info->id,
                            'friendId' => $current_user_info->id,
                            'CountryId' => $referrer_info->CountryId,
                            'source' => $referral_program_info->src,
                            'firstPurchase' => 0,
                            'used' => 0,
                            'usedAmount' => 0,
                            'refereeDiscount' => 0,
                            'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                        ]);

                        session()->forget('referral_program_info');
                        }else{
                            session()->forget('referral_program_info');
                        }
                    }
                }
            }
        }

        $this->session->_session('isLoggedIn', 'set', $user);
        return $user;
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        session()->put('loginerror', trans('auth.failed'));
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function api_sendFailedLoginResponse(Request $request)
    {
        session()->put('loginerror', trans('auth.failed'));
        return 0;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $sessionCountryData = session()->get('currentCountry');

        // If Data from session exist
        if (!empty($sessionCountryData)) {
            // Set Current country info based on session Data
            $urlLang = strtolower(json_decode($sessionCountryData, true)['urlLang']);
            $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
            $this->guard()->logout();

            $request->session()->invalidate();

            return $this->loggedOut($request) ?: redirect('/'.$urlLang."-".$currentCountryIso);

        }else{

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
        }
    }

    public function logoutV2(Request $request)
    {
        $this->guard()->logout();
        return 'success';
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function authenticate($countryCode)
    {
        $country = Countries::where('code', $countryCode)->first();
        return view('auth.login')->withCookie(cookie()->forever('country', $country))->with('country', $country);
    }

    // Social Login [Google, Facebook] -----------------------------------------------------------------------------

    public function authenticate_facebook(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function authenticate_facebook_callback(Request $request)
    {
        try {
            // // Get new country info from session
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
            $urlLang = strtolower($currentCountryData['urlLang']);
            $langCode = strtolower(app()->getLocale());

            $source_of_request = explode('/', $_SERVER["HTTP_REFERER"]);
            $get_source = end($source_of_request);
            $matches = array();
            preg_match_all('/\b(checkout)\b/i', $get_source, $matches);
            $source = implode('', $matches[0]);

            $user = Socialite::driver('facebook')->user();
            $user_ = User::where('email', $user->getEmail())->first();
            if ($user_ === null || $user_ === "") {
                $currentCountry = json_decode($this->request->session()->get('currentCountry'));
                if ($user->getName() == trim($user->getName())) {
                    $first_name = $user->getName();
                    $last_name = null;
                } else {
                    $first_name = explode(' ', $user->getName())[0];
                    $last_name = explode(' ', $user->getName())[1];
                }

                $user = User::create([
                    'firstName' => $user->getName(),
                    'lastName' => null,
                    'email' => $user->getEmail(),
                    'password' => null,
                    'socialId' => $user->getId(),
                    'birthday' => '1991-01-11',
                    'gender' => null,
                    'SSN' => null,
                    'NRIC' => null,
                    'phoneHome' => null,
                    'phoneOffice' => null,
                    'phone' => null,
                    'defaultLanguage' => strtoupper(app()->getLocale()),
                    'defaultDateFormat' => 'yyyy/MM/dd',
                    'avatarUrl' => null,
                    'notificationSettings' => [],
                    'isActive' => 1,
                    'defaultShipping' => null,
                    'defaultBilling' => null,
                    'hasReceiveOffer' => null,
                    'CountryId' => $currentCountry->id,
                    'NotificationTypeId' => null,
                    'badgeId' => null,
                    'isGuest' => 0,
                    'UserTypeId' => null,
                    'IsSG_old' => null,
                    'createdBy' => null,
                    'updateBy' => null,
                    'Email_Sub' => 0,
                    'Sms_Sub' => 0,
                    'referralId' => null,
                    'inviteCode' => null,
                    'HKG_Marketing_Sub' => 0,
                    'user_profile_img' => file_get_contents($user->avatar),
                ]);

                $this->guard()->login($user);

                if ($source === 'checkout') {
                    return redirect()->back();
                } else {
                    return $this->registered($user) ?: redirect($this->redirectPath());
                }
            } else {
                Auth::login($user_);

                if ($source === 'checkout') {
                    return redirect()->back();
                } else {
                    return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
                }
            }

        } catch (Exception $e) {
            return redirect('social/auth/facebook');
        }
    }

    protected function social_registered($user)
    {
        // // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());

        // referral
        if (session()->get('referral_program_info')) {
            $referral_program_info = session()->get('referral_program_info');
            $current_user_info = Auth::user();
            $referrer_info = \App\Helpers\LaravelHelper::ConvertArraytoObject(User::where('inviteCode', $referral_program_info->code)->first());
            if ($referrer_info) {
                // start checking if referrer_id && referee_id exists in referral table
                $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $current_user_info->id)->get();
                if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                    // do nothing
                } else {
                    // check if referrer_id and referee_id are the same
                    if ($current_user_info->id == $referrer_info->id) {
                        // do nothing
                    }
                    else {
                        $orders = Orders::where('UserId' , $current_user_info->id )->count();
                        if($orders == 0){
                        //update user to be associated with referrer
                        User::where('id', $current_user_info->id)->update(["referralId" => $referrer_info->id]);
                        // create new association between referrer & referee
                        Referral::create([
                            'referralId' => $referrer_info->id,
                            'friendId' => $current_user_info->id,
                            'CountryId' => $referrer_info->CountryId,
                            'source' => $referral_program_info->src,
                            'firstPurchase' => 0,
                            'used' => 0,
                            'usedAmount' => 0,
                            'refereeDiscount' => 0,
                            'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                        ]);

                        session()->forget('referral_program_info');
                        }else{
                            session()->forget('referral_program_info');
                        }
                    }
                }
            }
        }

        // $this->session->_session('isLoggedIn', 'set', $user);
        return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
    }

    public function authenticate_google($countryCode)
    {
        $country = Countries::where('code', $countryCode)->first();
        return view('auth.login')->withCookie(cookie()->forever('country', $country))->with('country', $country);
    }

    public function authenticate_google_callback($countryCode)
    {
        $country = Countries::where('code', $countryCode)->first();
        return view('auth.login')->withCookie(cookie()->forever('country', $country))->with('country', $country);
    }

    // ---------------------------------------------------------------------------------------------------------------
}
