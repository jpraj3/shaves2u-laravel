<?php

namespace App\Http\Controllers\Ecommerce\Rebates\Referral;

use App\Helpers\RebatesHelper;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Rebates\Referral;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Rebates\RewardsIn;
use App\Queue\SendEmailQueueService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Orders\Orders;
use App\Helpers\Email\EmailQueueHelper;

class ReferralController
{
    public function __construct()
    {
        $this->Log = \Log::channel('cronjob');
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    public function generateCode()
    {
        $code = \App\Helpers\RebatesHelper::generateUniqueCode(15, 'S2U');

        return $code;
    }

    public function share($type, $data)
    {
        if ($type && $data) {
            if ($type === 'share') {

            } else if ($type === 'email') {

            }
        }
    }

    public function sendReferralActiveCredits($data)
    {
        $referrer_country = Countries::where('id', $data->referrer->country_id)->first();
        $referrer_info = User::where('id', $data->referrer->id)->first();
        $userDefLang = $referrer_info["defaultLanguage"] ? strtolower($referrer_info["defaultLanguage"]) : strtolower($referrer_country["defaultLang"]);

        // send all emails
        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $referrer_country->id;
            $currentLocale = $userDefLang;
            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'referral-email-invite';
            $emailData['moduleData'] = (Object) array(
                'email' => $referrer_info->email,
                'referrerFName' => $data->referrer->first_name . ' ' . $data->referrer->last_name,
                'userFName' => $data->referee->first_name . ' ' . $data->referee->last_name,
                'currencySymbol' => $referrer_country->currencyDisplay,
                'refCredits' => $data->referrer->amount_credited,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            SendEmailQueueService::addHash('referral_invite_' . \App\Helpers\LaravelHelper::GenerateUUID(), 'referral_active_credits', $emailData);
            $this->Log->info('Dispatched referral active credit EDM for email: ' . $referrer_info->email . '.');
        } catch (Exception $e) {
            $this->Log->error('Dispatched referral active credit EDM for email: ' . $referrer_info->email . '.' . $e->getMessage());
        }
    }

    public function sendReferralInactiveCredits($data)
    {
        $referrer_country = Countries::where('id', $data->referrer->country_id)->first();
        $referrer_info = User::where('id', $data->referrer->id)->first();
        $userDefLang = $referrer_info["defaultLanguage"] ? strtolower($referrer_info["defaultLanguage"]) : strtolower($referrer_country["defaultLang"]);

        // send all emails
        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $referrer_country->id;
            $currentLocale = $userDefLang;
            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'referral-email-invite';
            $emailData['moduleData'] = (Object) array(
                'email' => $referrer_info->email,
                'referrerFName' => $data->referrer->first_name . ' ' . $data->referrer->last_name,
                'userFName' => $data->referee->first_name . ' ' . $data->referee->last_name,
                'currencySymbol' => $referrer_country->currencyDisplay,
                'refCredits' => $data->referrer->amount_credited,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            SendEmailQueueService::addHash('referral_invite_' . \App\Helpers\LaravelHelper::GenerateUUID(), 'referral_active_credits', $emailData);
            $this->Log->info('Dispatched referral active credit EDM for email: ' . $referrer_info->email . '.');
        } catch (Exception $e) {
            $this->Log->error('Dispatched referral active credit EDM for email: ' . $referrer_info->email . '.' . $e->getMessage());
        }
    }

    public function sendInvitationEmail(Request $request)
    {
        // loggingHelper.log('info', '[Referral] start sendInvitationEmail');
        //         loggingHelper.log('info', `param: req.body = ${req.body}`);
        //         loggingHelper.log('info', `param: req.query = ${req.query}`);
        $emails = explode(';', rtrim($request->referral_email_lists, ';'));

        $user = Auth::user();
        if ($user) {
            $referrerInfo = (Object) array("referrer" => $user, "invite_link" => $request->invite_link, "invite_code" => $request->invite_code);
            $country = Countries::where('id', $user->CountryId)->first();
            $options = (Object) array();
            $options->referrer = $referrerInfo->referrer;
            $options->referrer_user_id = $referrerInfo->referrer->id;
            $options->referrer_name = $referrerInfo->referrer->firstName;
            $options->referrer_email = $referrerInfo->referrer->email;
            $options->referrer_invite_code = $request->invite_code;
            $referrerInfo->invite_link= str_replace('&amp;', '&', $referrerInfo->invite_link);
            $options->invite_link = str_replace('&src=link', '&src=email', $referrerInfo->invite_link);
            $userDefLang = strtolower($user->defaultLanguage);
            $trialPrice = 0.00;
            $rewardscurrencies = RewardsCurrency::where('CountryId', $country->id)->first();
            if($rewardscurrencies){
            $trialPrice =  $rewardscurrencies->value;
            }
            // //Get email array from input textbox
            if ($emails !== "") {
                foreach ($emails as $email) {
                    $userDefLang = $user->defaultLanguage ? strtolower($user->defaultLanguage) : strtolower($country->defaultLang);
                    // send all emails
                    try {
                        // Getting Country & Locale Data From $subs
                        $currentCountry = $country;
                        $currentLocale = $userDefLang;
                        $emailData = [];
                        // Passing $subs Data into $moduleData for Email
                        $emailData['title'] = 'referral-email-invite';
                        $emailData['moduleData'] = (Object) array(
                            'firstName' => $options->referrer_name,
                            'referrerFName' => $options->referrer_name,
                            'refCredits' => $trialPrice,
                            'trialPrice' => $trialPrice,
                            'name' => $options->referrer_name,
                            'email' => $email,
                            'ownemail' => $user->email,
                            'countryId' => $country->id,
                            'inviteLink' => $options->invite_link,
                            'currencySymbol' =>  $country->currencyDisplay,
                        );
                        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                        // hide email redis
                        SendEmailQueueService::addHash('referral_invite_' . \App\Helpers\LaravelHelper::GenerateUUID(), 'referral_email_invite', $emailData);
                        $this->Log->info('Dispatched referral email invite EDM for email: ' . $email . '.');
                    } catch (Exception $e) {
                        $this->Log->error('Dispatched referral email invite EDM for email: ' . $email . '.' . $e->getMessage());
                    }
                }
            }
        }
    }

    public function getReferrerInfo(Request $request)
    {
        if ($request) {
            $user = User::where('inviteCode', $request->_referral_unique_code)->first();
            if ($user) {
                $referral_program_info = (Object) array(
                    "code" => $request->_referral_unique_code,
                    "src" => $request->_src,
                    "referrer" => $user,
                    "referee" => (Object) array("current_country" => session()->get('currentCountry')),
                );
                session()->put('referral_program_info', $referral_program_info);
                $current_user_info = Auth::user();
              if($current_user_info){
                    // start checking if referrer_id && referee_id exists in referral table
                    $_checkReferralAssociation = Referral::where('referralId', $user->id)->where('friendId', $current_user_info->id)->get();
                    if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                        // do nothing
                    } else {
                        // check if referrer_id and referee_id are the same
                        if ($current_user_info->id == $user->id) {
                            // do nothing
                        }
                        else {
                            $orders = Orders::where('UserId' , $current_user_info->id )->count();
                            if($orders == 0){
                            //update user to be associated with referrer
                            User::where('id', $current_user_info->id)->update(["referralId" => $user->id]);
                            // create new association between referrer & referee
                            Referral::create([
                                'referralId' => $user->id,
                                'friendId' => $current_user_info->id,
                                'CountryId' => $user->CountryId,
                                'source' => $referral_program_info->src,
                                'firstPurchase' => 0,
                                'used' => 0,
                                'usedAmount' => 0,
                                'refereeDiscount' => 0,
                                'status' => 'Inactive', //Active,Inactive,Redeemed,CashedOut
                            ]);
    
                            session()->forget('referral_program_info');
                            }else{
                                session()->forget('referral_program_info');
                            }
                        }
                    }
              }

            

                return $user;
            }
        }
    }

    public function cashOut(Request $request)
    {
        if ($request) {
            try {
                $user = User::where('id', Auth::user()->id)->first();
                $referralCash = $this->calculateReferralCash($user->id);
                if ($referralCash) {
                    $cashout = ReferralCashOut::create([
                        'name' => $request->name,
                        'amount' => $referralCash,
                        'status' => 'active', // active, in_process, withdrawn
                        'UserId' => $user->id,
                        'CountryId' => $user->CountryId,
                        'email' => $user->email,
                        'bank_name' => $request->bank_name,
                        'bank_account_no' => $request->bank_account_no,
                    ]);

                    if ($cashout !== null) {
                        $email = $this->sendCashoutEmail($cashout);
                    }

                    return 1;
                } else {
                    return 0;
                }
            } catch (Exception $ex) {
                return "error";
            }

        }
    }

    public function sendCashoutEmail($data)
    {
        $userData = User::where('id', $data->UserId)->first();
        $currentCountry = Countries::where('id', $userData->CountryId)->first();
        $currentLocale = 'en';
        try {
            // Getting Country & Locale Data From $subs
            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'referral-cashout-request';
            $emailData['moduleData'] = array(
                'email' => config('environment.email-blasting.referral-cashouts'),
                'user_email' => $userData->email,
                'country' => $currentCountry->name,
                'currency' => $currentCountry->currencyCode,
                'amount' => $data->amount,
                'bank_name' => $data->bank_name,
                'full_name' => $data->name,
                'bank_acc_no' => $data->bank_account_no,
                'datetime' => $data->created_at,
                'userId' => $data->UserId,
                'countryId' => $data->CountryId,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);

            $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);

            // Update hasSendFollowUp1 column
            ReferralCashout::where('id', $data->id)->update(['status' => 'in_process']);
        } catch (Exception $e) {
            $this->Log->error('Unable to send referral cashout email for: ' . $userData->email . '.' . $e->getMessage());
            return $e;
        }
    }

    public function calculateReferralCash($user_id)
    {
        if ($user_id) {
            $user = User::where('id', $user_id)->first();
            if ($user) {
                $referral = Referral::where('referralId', $user_id)
                    ->where('status', 'Active')
                    ->where('firstPurchase', 1)
                    ->where('CountryId', $user->CountryId)
                    ->get();
                if ($referral) {
                    $referral_total = RewardsIn::where('UserId', $user_id)->where('CountryId', $user->CountryId)->sum('value');
                    $withdrawn_total = ReferralCashOut::where('UserId', $user_id)->where('CountryId', $user->CountryId)->where('status', 'withdrawn')->sum('amount');
                    return $referral_total - $withdrawn_total;
                }
            }
        } else {
            return 0;
        }
    }

    public function retrieveLastCashOut($user_id)
    {
        $cash_out = ReferralCashOut::where('UserId', $user_id)->latest()->first();
        if ($cash_out) {
            return $cash_out;
        } else {
            return 0;
        }
    }

    public function retrieveTotalSignUps($user_id)
    {
        $a = Referral::where('referralId', $user_id)->count();
        if ($a) {
            return $a;
        } else {
            return 0;
        }
    }

    public function retrieveTotalSucessful($user_id)
    {
        $a = Referral::where('referralId', $user_id)->where('firstPurchase', 1)->where('status', 'Active')->count();
        if ($a) {
            return $a;
        } else {
            return 0;
        }
    }
}
