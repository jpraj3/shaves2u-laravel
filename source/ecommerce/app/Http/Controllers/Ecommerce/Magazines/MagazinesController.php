<?php

namespace App\Http\Controllers\Ecommerce\Magazines;

use App;
use App\Helpers\LaravelHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\GeoLocation\Countries;
use App\Models\Magazine\Magazine;
use App\Models\Magazine\MagazineType;
use App\Services\CountryService as CountryService;
use Illuminate\Foundation\Application;
use OpenGraph;
use SEOMeta;

class MagazinesController extends Controller
{

    public $country_service;
    public function __construct()
    {

        $this->laravelHelper = new LaravelHelper();
        $this->country_service = new CountryService();

        $this->country_info = $this->country_service->getCountryAndLangCode();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();

        $type = MagazineType::where('CountryId', $CountryId)->where('langCode', strtoupper($locale))->first();
        if ($type == null) {
            return redirect()->back();
        }
        $lists = Magazine::where('ArticleTypeId', $type->id)
            ->where('isFeatured', true)
            ->limit(20)
            ->select('id', 'title', 'bannerUrl', 'URL')
            ->orderBy('created_at', 'DESC')
            ->get();

        $currentPage = strtok($_SERVER["REQUEST_URI"], '?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage),
        ];
        // $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // // Set Meta Information
        // SEOMeta::setTitle($meta->metatitle);
        // SEOMeta::setDescription($meta->metaDescription);
        // // Set og Meta Info
        // OpenGraph::setDescription($meta->metaDescription);
        // OpenGraph::setTitle($meta->metatitle);
        // OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        return view('magazines.magazine-list', compact('lists'));
    }

    public function content($lang, $country, $magazine_name)
    {

        $content = Magazine::where('URL', 'like', '%' . $magazine_name . '%')->first();

        if ($content) {
            $currentPage = strtok($_SERVER["REQUEST_URI"], '?');
            // Set Meta Information
            SEOMeta::setTitle($content->SEO_title);
            SEOMeta::setDescription($content->META_description);
            // Set og Meta Info
            OpenGraph::setDescription($content->META_description);
            OpenGraph::setTitle($content->SEO_title);
            OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

            return view('magazines.magazine-content', compact('content'));
        } else {
            return redirect()->back();
        }
    }
}
