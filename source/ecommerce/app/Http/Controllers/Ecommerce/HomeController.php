<?php

namespace App\Http\Controllers\Ecommerce;

use App;
use App\Helpers\LaravelHelper;
use App\Helpers\ProductHelper;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\ContactForm;
use App\Models\Faqs\FaqTranslates;
use App\Models\GeoLocation\Countries;
use App\Models\Privacies\Privacies;
use App\Models\Products\ProductCountry;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Terms\Terms;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\CountryService as CountryService;

use OpenGraph;
use SEOMeta;

class HomeController extends Controller
{

    public $country_service;
    public function __construct()
    {
        $this->productHelper = new ProductHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->country_service = new CountryService();
        $this->emailQueueHelper = new EmailQueueHelper();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('plans.trial.free-trial');
    }

    public function career()
    {
        return view('career');
    }

    public function about()
    {
        return view('aboutus');
    }

    public function product()
    {
        $this->locale = App::getLocale();
        $isLoggedIn = Auth::check() ? "logged_in" : "guest";
        $currentCountry_ = json_decode(session()->get('currentCountry'), true);
        $currentCountry = Countries::where('id', $currentCountry_["id"])->first();

        // $country_id = $this->country['id'];

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);

        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        return view('products.product.product', compact('isLoggedIn', 'currentCountry'));
    }

    public function tnc()
    {
        $langCode = strtoupper(app()->getLocale());
        $currentCountryIso = strtoupper(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $Terms = Terms::where('langCode', $langCode)->where('CountryId', $currentCountryid)->select("content")->first();
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        return view('terms-conditions')->with('termcontent', $Terms->content);
    }

    public function faq()
    {
        $langCode = strtoupper(app()->getLocale());
        $currentCountryIso = strtoupper(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        $faq = FaqTranslates::join('faqs', 'faqs.id', 'faqtranslates.FaqId')
            ->select('faqs.id as faqidget', 'faqtranslates.id as faqtranid', 'faqtranslates.*', 'faqs.*')
            ->where('faqs.langCode', $langCode)
            ->where('faqs.CountryId', $currentCountryid)
            ->orderByRaw("faqs.id asc, faqtranslates.id asc")
            ->get()
            ->toArray();
        $FaqList = array();
        $count = 0;
        $insertFaqIdArray = [];
        $insertCombineFaqIdArray = [];
        if ($faq) {
            for ($i = 0; $i < count($faq); $i++) {
                $faqId = (string) $faq[$i]["faqidget"];
                if (in_array($faqId, $insertFaqIdArray)) { // if have faqId in array
                    foreach ($insertCombineFaqIdArray as $combineFaqIdList) { // if have faqId in array
                        $combineFaqId = explode(',', $combineFaqIdList);
                        if ($combineFaqId[0] == $faq[$i]["faqidget"]) { //check in which group

                            $FaqList[$combineFaqId[1]]["faqid"] = $faq[$i]["faqidget"];
                            $FaqList[$combineFaqId[1]]["type"] = $faq[$i]["type"];
                            $FaqList[$combineFaqId[1]]["title"] = $faq[$i]["title"];
                            $FaqList[$combineFaqId[1]]["subtitle"] = $faq[$i]["subtitle"];
                            $FaqList[$combineFaqId[1]]["type"] = $faq[$i]["type"];
                            $FaqList[$combineFaqId[1]]["faqtranslates"]["question"][] = $faq[$i]["question"];
                            $FaqList[$combineFaqId[1]]["faqtranslates"]["answer"][] = $faq[$i]["answer"];
                            // $FaqList[$count]["planduration"][] = $FaqList[$i]->planduration;
                        }
                    }
                } else { // if dun have faqId in array
                    $FaqList[$count]["faqid"] = $faq[$i]["faqidget"];
                    $FaqList[$count]["type"] = $faq[$i]["type"];
                    $FaqList[$count]["title"] = $faq[$i]["title"];
                    $FaqList[$count]["subtitle"] = $faq[$i]["subtitle"];
                    $FaqList[$count]["type"] = $faq[$i]["type"];
                    $FaqList[$count]["faqtranslates"]["question"][] = $faq[$i]["question"];
                    $FaqList[$count]["faqtranslates"]["answer"][] = $faq[$i]["answer"];

                    $insertFaqIdArray[] = (string) $faq[$i]["faqidget"];
                    $insertCombineFaqIdArray[] = (string) $faq[$i]["faqidget"] . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return view('faq')->with('faq', $FaqList);
    }

    public function privpolicy()
    {
        $langCode = strtoupper(app()->getLocale());
        $currentCountryIso = strtoupper(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $Privacies = Privacies::where('langCode', $langCode)->where('CountryId', $currentCountryid)->select("content")->first();

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        return view('privacy-policy')->with('privacycontent', $Privacies->content);
    }

    public function contact()
    {
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);


        return view('contact');
    }

    //Product inner page
    public function handle()
    {
        $items = [];
        $handle = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            ->whereIn('products.sku', config('global.all.handle_types.skus'))
            ->where('productcountries.CountryId', json_decode(session()->get('currentCountry'), true)['id'])
            ->select('productcountries.id')
            ->get()
            ->toArray();

        foreach ($handle as $item) {
            array_push($items, $item);
        }
        $productcountyidarray = array(
            "appType" => config('app.appType'),
            "isOnline" => 1,
            "isOffline" => 0,
            "product_country_ids" => $items,
        );

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);


        foreach ($data as $d) {if ($CountryId === 8) {$d->sellPrice = rtrim(rtrim(number_format($d->sellPrice,2),0),'.');}}
        return view('products.product.handle', compact('data', 'currentCountry'));
    }

    public function shave_cream()
    {
        $items = [];
        $shave_cream = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            ->whereIn('products.sku', config('global.all.shavecream_types.skus'))
            ->where('productcountries.CountryId', json_decode(session()->get('currentCountry'), true)['id'])
            ->select('productcountries.id')
            ->get()
            ->toArray();

        foreach ($shave_cream as $item) {
            array_push($items, $item);
        }
        $productcountyidarray = array(
            "appType" => config('app.appType'),
            "isOnline" => 1,
            "isOffline" => 0,
            "product_country_ids" => $items,
        );
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        foreach ($data as $d) {if ($CountryId === 8) {$d->sellPrice = rtrim(rtrim(number_format($d->sellPrice,2),0),'.');}}
        return view('products.product.shavecream', compact('data', 'currentCountry'));
    }

    public function aftershave()
    {
        $items = [];
        $aftershave = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            ->whereIn('products.sku', config('global.all.aftershavecream_types.skus'))
            ->where('productcountries.CountryId', json_decode(session()->get('currentCountry'), true)['id'])
            ->select('productcountries.id')
            ->get()
            ->toArray();
        foreach ($aftershave as $item) {
            array_push($items, $item);
        }
        $productcountyidarray = array(
            "appType" => config('app.appType'),
            "isOnline" => 1,
            "isOffline" => 1,
            "product_country_ids" => $items,
        );
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        foreach ($data as $d) {if ($CountryId === 8) {$d->sellPrice = rtrim(rtrim(number_format($d->sellPrice,2),0),'.');}}

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        return view('products.product.aftershave', compact('data', 'currentCountry'));
    }

    public function blade()
    {
        $items = [];
        $blades = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            ->whereIn('products.sku', config('global.all.blade_types.skus'))
            ->where('productcountries.CountryId', json_decode(session()->get('currentCountry'), true)['id'])
            ->select('productcountries.id')
            ->get()
            ->toArray();

        foreach ($blades as $item) {
            array_push($items, $item);
        }
        $productcountyidarray = array(
            "appType" => config('app.appType'),
            "isOnline" => 1,
            "isOffline" => 0,
            "product_country_ids" => $items,
        );
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        foreach ($data as $d) {if ($CountryId === 8) {$d->sellPrice = rtrim(rtrim(number_format($d->sellPrice,2),0),'.');}}

        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($CountryId, $urllangCode, $productcountyidarray));
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);


        return view('products.product.blade', compact('data', 'currentCountry'));
    }

    public function ask()
    {
        return view('products.product.ask');
    }

    public function referral()
    {
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"],'?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/".strtolower($urllangCode)."-".strtolower($currentCountryIso),"",$currentPage)
        ];
        $referralValues = RewardsCurrency::where('CountryId', $CountryId)->select('value')->first();
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);
        return view('referral', compact('referralValues','country'));
    }

    public function handleContactFormSubmit()
    {
        $contactFormInput = request()->all();
        $contactForm = new ContactForm();
        $contactForm->topic = $contactFormInput['c_e_topic'];
        $contactForm->name = $contactFormInput['c_e_name'];
        $contactForm->email = $contactFormInput['c_e_email'];
        $contactForm->contactNo = $contactFormInput['c_e_phone'];
        $contactForm->enquiryMessage = $contactFormInput['c_e_enquiry'];
        $contactForm->save();
        $emailData = [];
        $currentCountry = json_decode(session()->get('currentCountry'), true);

        $currentLocale = "";
        if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
            $currentLocale = json_decode(session()->get('currentCountry'), true)['defaultLang'];
        } else {
            $currentLocale = 'en';
        }
        $emailData['title'] = 'contact-us';

        $emailData['moduleData'] = array(
            'email' => "help.my@shaves2u.com",
            'topic' =>  $contactFormInput['c_e_topic'],
            'name' => $contactFormInput['c_e_name'],
            'emailuser' => $contactFormInput['c_e_email'],
            'contactNo' => $contactFormInput['c_e_phone'],
            'enquiryMessage' => $contactFormInput['c_e_enquiry'],
        );


        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
        // hide email redis
        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);

        return response('Success');

    }
}
