<?php

namespace App\Http\Controllers\Ecommerce;

use App;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Helpers\Payment\NicePayHelper;
use App\Helpers\Payment\Stripe;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\PromotionsHelper;
use App\Helpers\SubscriptionHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Ecommerce\Rebates\Referral\ReferralController;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PausePlanHistories;
use App\Models\Plans\PlanCategoryDetails;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductType;
use App\Models\Promotions\Promotions;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use App\Services\CountryService as CountryService;
use Carbon\Carbon;
use Exception;
use finfo;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jorenvh\Share\Share;
use Stripe\Subscription;
use \App\Helpers\RebatesHelper;
use \App\Helpers\UserHelper;

// Helpers
class LandingController extends Controller
{
    public $country_service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Application $app, Request $request)
    {
        $this->middleware(['locale', 'auth']);
        $this->userHelper = new UserHelper;
        $this->referralController = new ReferralController();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->promotionHelper = new PromotionsHelper();
        $this->subscriptionHelper = new SubscriptionHelper();
        $currentCountry = json_decode(session()->get('currentCountry'), true);
        if ($currentCountry["id"] !== 8) {
            $this->stripe = new Stripe();
        } else {
            $this->nicepayController = new NicePayController();
            $this->nicepay = new NicePayHelper();
        }
        $this->country_service = new CountryService();
        $this->socialShare = new Share();
        $this->emailQueueHelper = new EmailQueueHelper();
        $this->country_info = $this->country_service->getCountryAndLangCode();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // Get new country info from session
        $currentCountry = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountry['codeIso']);
        $langCode = strtoupper(app()->getLocale());

        if (Auth::check()) {
            return view('_authenticated.landing', compact('currentCountry'));
        } else {
            return view('_guest.landing', compact('currentCountry'));
        }
    }

    public function dashboard()
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $langCode = strtoupper(app()->getLocale());
        // $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . explode('/', explode('?', $_SERVER['REQUEST_URI'], 2)[0])[0];

        if (Auth::check()) {
            $_params = '';
            $user_info = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->userHelper->getUserDetails(Auth::user()->id));
            $referral_cash = $this->referralController->calculateReferralCash(Auth::user()->id);
            $retrieveLastCashOut = $this->referralController->retrieveLastCashOut(Auth::user()->id);
            $retrieveTotalSignUps = $this->referralController->retrieveTotalSignUps(Auth::user()->id);
            $retrieveTotalSucessful = $this->referralController->retrieveTotalSucessful(Auth::user()->id);
            $retrieveAllSubscriptions = $this->subscriptionHelper->RetrieveAllSubsInfo(Auth::user()->id);
            $retrieveLatestSubscription = $this->subscriptionHelper->RetrieveLatestSubsInfo(Auth::user()->id);
            return view('_authenticated.dashboard', compact('user_info', 'referral_cash', 'retrieveTotalSignUps', 'retrieveTotalSucessful', 'retrieveLatestSubscription'));
        } else {
            return view('_guest.landing');
        }
    }

    public function orderHistory()
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $langCode = strtoupper(app()->getLocale());
        $orders_info = [];
        if (Auth::check()) {
            $orders = Orders::where('UserId', Auth::user()->id)->orderBy('created_at', 'desc')->get();
            if ($orders) {
                foreach ($orders as $order) {
                    $receipt = Receipts::where('OrderId', $order->id)->first();
                    $country = Countries::where('id', $order->CountryId)->first();
                    $data = (object) array(
                        "order" => $order,
                        "receipt" => $receipt,
                        "country" => $country,
                    );
                    array_push($orders_info, $data);
                }
            }
            return view('_authenticated.orderhistory', compact('orders_info', 'currentCountryIso'));
        } else {
            return view('_guest.landing');
        }
    }

    public function orderHistoryInfo($lang, $country, $id)
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $langCode = strtoupper(app()->getLocale());
        $order_details = [];
        $order_details_ids = [];
        $freeproductsexists = false;
        if (Auth::check()) {
            if ($id) {
                $user_info = Auth::user();
                $order = Orders::where('id', $id)->first();
                if ($order) {
                    $receipt = Receipts::where('OrderId', $order->id)->first();
                    $orderdetails = OrderDetails::where('OrderId', $order->id)->get();
                    $orderhistory = OrderHistories::where('OrderId', $order->id)->get();
                    $country = Countries::where('id', $order->CountryId)->first();
                    $shipping = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                    $billing = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
                    if ($order->subscriptionIds) {
                        $subscription = Subscriptions::where('id', $order->subscriptionIds)->first();
                        $plandetails = $subscription ? $this->planHelper->getPlanDetailsByPlanId($subscription->PlanId, $country->id, $user_info->defaultLanguage) : null;
                    } else {
                        $subscription = null;
                        $plandetails = null;
                    }

                    foreach ($orderdetails as $od) {
                        array_push($order_details_ids, $od["ProductCountryId"]);
                        if ($od["isFreeProduct"] === 1) {
                            $freeproductsexists = true;
                        }
                    }

                    $productcountries_details = array(
                        "appType" => config('app.appType'),
                        "isOnline" => 1,
                        "isOffline" => 1,
                        "product_country_ids" => $order_details_ids,
                    );
                    $orderdetailsinfo = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->productHelper->getProductByProductCountries($order->CountryId, $langCode, $productcountries_details));

                    $order_details = (object) array(
                        "user" => $user_info,
                        "order" => $order,
                        "country" => $country,
                        "orderdetails" => $orderdetails,
                        "orderdetailsinfo" => $orderdetailsinfo,
                        "freeproductsexists" => $freeproductsexists,
                        "orderhistory" => $orderhistory,
                        "receipt" => $receipt,
                        "subscription" => (object) array("subscription" => $subscription, "plan" => $plandetails),
                        "address" => (object) array(
                            "shipping" => $shipping,
                            "billing" => $billing,
                        ),
                    );
                }
            }
            return view('_authenticated.orderhistoryinfo', compact('order_details', 'currentCountryIso'));
        } else {
            return view('_guest.landing');
        }
    }

    public function referrals()
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $urlLang = strtoupper($currentCountryData['urlLang']);
        $langCode = strtoupper(app()->getLocale());
        // $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . explode('/', explode('?', $_SERVER['REQUEST_URI'], 2)[0])[0];
        $url = $_SERVER['HTTP_HOST'] . explode('/', explode('?', $_SERVER['REQUEST_URI'], 2)[0])[0];
        if (Auth::check()) {
            $referral_link = '';
            $_params = '';
            $unique_link = '';
            $user_info = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->userHelper->getUserDetails(Auth::user()->id));
            $referral_cash = $this->referralController->calculateReferralCash(Auth::user()->id);
            $retrieveLastCashOut = $this->referralController->retrieveLastCashOut(Auth::user()->id);
            $retrieveTotalSignUps = $this->referralController->retrieveTotalSignUps(Auth::user()->id);
            $retrieveTotalSucessful = $this->referralController->retrieveTotalSucessful(Auth::user()->id);

            if ($user_info->user->inviteCode === null) {
                $unique_code = \App\Helpers\RebatesHelper::generateUniqueCode(20, 'S2U');
                if ($unique_code) {
                    User::where('id', $user_info->user->id)->update(['inviteCode' => $unique_code]);
                    $params = config('global.all.rebates.referral.params');
                    foreach ($params as $param) {
                        $param = $param === 'unique_code' ? $param = $unique_code : '';
                        $_params .= $param . '&' . $param;
                    }
                    $referral_link = route('locale.region.shave-plans.trial-plan.selection', [
                        'langCode' => strtolower($urlLang),
                        'countryCode' => strtolower($currentCountryIso),
                        'ref' => $unique_code,
                        'src' => 'link',
                    ]);
                    // $referral_link = $url . config('global.all.rebates.referral.invite_link') . '?' . $_params . '&src=link';
                    $referral_kakao_link = route('locale.region.shave-plans.trial-plan.selection', [
                        'langCode' => strtolower($urlLang),
                        'countryCode' => strtolower($currentCountryIso),
                        'ref' => $unique_code,
                        'src' => 'kakao',
                    ]);
                }
            } else {
                $params = config('global.all.rebates.referral.params');
                foreach ($params as $key => $param) {
                    if ($key === 'ref') {
                        $unique_link = $user_info->user->inviteCode;
                    }
                }
                $referral_link = route('locale.region.shave-plans.trial-plan.selection', [
                    'langCode' => strtolower($urlLang),
                    'countryCode' => strtolower($currentCountryIso),
                    'ref' => $unique_link,
                    'src' => 'link',
                ]);
                // $referral_link = $url . config('global.all.rebates.referral.invite_link') . '?' . $unique_link . '&src=link';
                $referral_kakao_link = route('locale.region.shave-plans.trial-plan.selection', [
                    'langCode' => strtolower($urlLang),
                    'countryCode' => strtolower($currentCountryIso),
                    'ref' => $unique_link,
                    'src' => 'kakao',
                ]);
            }

            $referral_share_template = $this->socialShare->page(
                route('locale.region.shave-plans.trial-plan.selection', [
                    'langCode' => strtolower($urlLang),
                    'countryCode' => strtolower($user_info->country->codeIso),
                    'ref' => $unique_link,
                    'src' => 'fb'
                ]),
                __('website_contents.referral_twitter_desc')
            )
                ->facebook('Shaves2U | Referral Program')
                ->twitter('Shaves2U | Referral Program')
                // ->linkedin('Shaves2U | Referral Program')
                ->whatsapp('Shaves2U | Referral Program');

            $locale = App::getLocale();
            $sessionCountryData = session()->get('currentCountry');
            $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
            $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
            $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);

            $country = Countries::where('id', json_decode(session()->get('currentCountry'), true)['id'])->first();
            $currentPage = strtok($_SERVER["REQUEST_URI"], '?');

            $options = [
                "CountryId" => $country['id'],
                "langCode" => strtoupper($locale),
                "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage),
            ];

            $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
            if ($meta) {
                // Set Meta Information
                // SEOMeta::setTitle($meta->metatitle);
                // SEOMeta::setDescription($meta->metaDescription);
                // Set og Meta Info
                // OpenGraph::setDescription($meta->metaDescription);
                // OpenGraph::setTitle($meta->metatitle);
                // OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

                // if (strtoupper(strtoupper($locale)) === 'EN') {
                //     OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-MY.jpg'));
                // } else if (strtoupper($locale) === 'KO') {
                //     OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-Korea.jpg'));
                // } else if (strtoupper($locale) === 'ZH-HK') {
                //     OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-HK.jpg'));
                // } else if (strtoupper($locale) === 'ZH-TW') {
                //     OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-ZH.jpg'));
            }

            if (config('app.env') !== 'staging') {
                $referral_kakao_link = explode('/', $referral_kakao_link);
                unset($referral_kakao_link[3]);
                $referral_kakao_link = implode('/', $referral_kakao_link);
                $referral_link = explode('/', $referral_link);
                unset($referral_link[3]);
                $referral_link = implode('/', $referral_link);
            } else {
                $referral_kakao_link = explode('/', $referral_kakao_link);
                unset($referral_kakao_link[5]);
                $referral_kakao_link = implode('/', $referral_kakao_link);
                $referral_link = explode('/', $referral_link);
                unset($referral_link[5]);
                $referral_link = implode('/', $referral_link);
            }

            return view('_authenticated.referrals', compact('user_info', 'referral_kakao_link', 'referral_link', 'referral_cash', 'retrieveLastCashOut', 'retrieveTotalSignUps', 'retrieveTotalSucessful', 'referral_share_template', ' currentCountryIso'));
        } else {
            return view('_guest.landing');
        }
    }

    public function getProfileImg($user)
    {
        // Return the image in the response with the correct MIME type
        return response()->make($user->user_profile_img, 200, array(
            'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($user->user_profile_img),
        ));
    }

    public function profile()
    {
        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $langCode = strtoupper(app()->getLocale());

        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $states = $this->country_service->getAllStates($currentCountryid);

        if (Auth::check()) {
            $user = Auth::user();
            $user_country = Countries::where('id', $user->CountryId)->first();
            $user_delivery = DeliveryAddresses::where('id', $user->defaultShipping)->where('isRemoved', 0)->first();
            $user_billing = DeliveryAddresses::where('id', $user->defaultBilling)->where('isRemoved', 0)->first();
            $user_default_card = Cards::where('UserId', $user->id)->where('isDefault', 1)->first();
            $cards = Cards::where('UserId', $user->id)->where('isRemoved', 0)->get();
            $display_picture = $user->user_profile_img !== null ? $this->getProfileImg($user) : '';
            $nondefault_addresses = DeliveryAddresses::where('UserId', $user->id)->where('id', '!=', $user->defaultShipping)->where('id', '!=', $user->defaultBilling)->where('isRemoved', 0)->get();
            return view('_authenticated.profile', compact('user', 'user_country', 'user_billing', 'user_delivery', 'user_default_card', 'display_picture', 'nondefault_addresses', 'cards', 'states'));
        } else {
            return view('_guest.landing');
        }
    }

    public function savedPlans()
    {
        if (Auth::check()) {
            // Get user Info
            $subs = $this->subscriptionHelper->RetrieveAllSubsInfo(Auth::user()->id);
            $latest_sub = $this->subscriptionHelper->RetrieveLatestSubsInfo(Auth::user()->id);
            $user_default_country = Auth::user()->CountryId;
            $latest_active_plan_origin_country = 0;
            $latest_active_plan_origin_country_matches = false;
            $plans_origin_country = 0;
            $plans_origin_country_matches = false;
            if ($latest_sub->latest_sub) {
                $plan_info = Plans::where('id', $latest_sub->latest_sub->PlanId)->first();
                $plan_country_info = Countries::where('id', $plan_info->CountryId)->first();
                if ($plan_info && $plan_country_info) {
                    $plan_country_id = $plan_info->CountryId;
                    $latest_sub->latest_sub->currencyDisplay = $plan_country_info->currencyDisplay;

                    $latest_active_plan_origin_country = $plan_country_id;
                    $latest_active_plan_origin_country_matches = $plan_country_id == $user_default_country ? true : false;
                    $latest_sub->latest_active_plan_origin_country = $latest_active_plan_origin_country;
                    $latest_sub->latest_active_plan_origin_country_matches = $latest_active_plan_origin_country_matches;

                    $totaldiscount = 0;
                    if ($latest_sub->latest_sub->temp_discountPercent != null && $latest_sub->latest_sub->temp_discountPercent > 0) {
                        $totaldiscount = ($latest_sub->latest_sub->price * $latest_sub->latest_sub->temp_discountPercent) / 100;
                    } else if ($latest_sub->latest_sub->promotionId) {
                        $promotion = Promotions::find($latest_sub->latest_sub->promotionId);
                        if ($promotion != null) {
                            if ($promotion->promotionType == 'Upcoming') {
                                $promotionuse = Orders::where('subscriptionIds', $latest_sub->latest_sub->id)
                                    ->where('PromotionId', $promotion->id)
                                    ->where('PromotionId', '!=', null)
                                    ->count();
                                if ($promotionuse == 0) {
                                    $totaldiscount = ($latest_sub->latest_sub->price * $promotion->discount) / 100;
                                }
                            } else if ($promotion->promotionType == 'Recurring') {
                                $totaldiscount = ($latest_sub->latest_sub->price * $promotion->discount) / 100;
                            }
                        }
                    }
                    $latest_sub->latest_sub->appliedDiscount = $totaldiscount;
                }
            }

            if (count($subs) > 0) {
                foreach ($subs as $sub) {
                    $plan_info = Plans::where('id', $sub->subscription->PlanId)->first();
                    $plan_country_info = Countries::where('id', $plan_info->CountryId)->first();
                    if ($plan_info && $plan_country_info) {
                        $plan_country_id = $plan_info->CountryId;
                        $plans_origin_country = $plan_country_id;
                        $plans_origin_country_matches = $plan_country_id == $user_default_country ? true : false;
                        $sub->plans_origin_country = $plans_origin_country;
                        $sub->plans_origin_country_matches = $plans_origin_country_matches;
                    } else {
                        $plans_origin_country = $plan_country_id;
                        $plans_origin_country_matches = $plan_country_id == $user_default_country ? true : false;
                        $sub->plans_origin_country = $plans_origin_country;
                        $sub->plans_origin_country_matches = $plans_origin_country_matches;
                    }
                }
            }

            return view('_authenticated.shave-plans', compact('subs', 'latest_sub'));
        }
    }

    public function EditShavePlans($lang, $country, $subscriptionId)
    {
        try {
            // Get new country info from session
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtoupper($currentCountryData['codeIso']);
            $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
            $langCode = Auth::user()->defaultLanguage;
            $userdefcounty = Auth::user()->CountryId;
            $states = $this->country_service->getAllStates($currentCountryid);
            $data = (object) array();
            $types_of_plan_frequency = json_decode(json_encode(PlanSKU::select('duration')->distinct()->get()), true);
            $data->types_of_plan_frequency = $types_of_plan_frequency;
            $data->countrystates = $states;
            $data->userdefcounty = $userdefcounty;
            $optional_product_ids = config('global.all.edit-shaves-plan.all.optional-product-ids');

            if (Auth::check()) {
                $data->user = Auth::user();
                // optional products for custom plan only
                $user_subscription = Subscriptions::where('subscriptions.id', $subscriptionId)
                    ->where('subscriptions.UserId', $data->user->id)
                    ->join('plans', 'plans.id', 'subscriptions.PlanId')
                    ->join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->select('subscriptions.*', 'plansku.duration')
                    ->first();

                if ($user_subscription->status == "Pending" || $user_subscription->status == 'Payment Failure' || $user_subscription->status == 'Cancelled') {
                    return redirect()->back();
                }
                // check if shave plan exists
                if ($user_subscription != null) {
                    $country = Countries::join('plans', 'plans.CountryId', 'countries.id')->where('plans.id', $user_subscription->PlanId)->select('countries.*')->first();

                    $data->country = $country;
                    $data->subscription = $user_subscription;

                    $totaldiscount = 0;
                    if ($data->subscription->temp_discountPercent != null && $data->subscription->temp_discountPercent > 0) {
                        $totaldiscount = ($data->subscription->pricePerCharge * $data->subscription->temp_discountPercent) / 100;
                    } else if ($data->subscription->promotionId) {
                        $promotion = Promotions::find($data->subscription->promotionId);
                        if ($promotion != null) {
                            if ($promotion->promotionType == 'Upcoming') {
                                $promotionuse = Orders::where('subscriptionIds', $data->subscription->id)
                                    ->where('PromotionId', $promotion->id)
                                    ->where('PromotionId', '!=', null)
                                    ->count();
                                if ($promotionuse == 0) {
                                    $totaldiscount = ($data->subscription->pricePerCharge * $promotion->discount) / 100;
                                }
                            } else if ($promotion->promotionType == 'Recurring') {
                                $totaldiscount = ($data->subscription->pricePerCharge * $promotion->discount) / 100;
                            }
                        }
                    }
                    $data->subscription->appliedDiscount = $totaldiscount;

                    $promotions = [];
                    // check for existing promotions for current shave plan
                    if ($user_subscription->promoCode && $user_subscription->promotionId) {
                        $_activePromo = $this->promotionHelper->promotionInfo($user_subscription->promoCode, $country);
                        $product_info = null;
                        if ($_activePromo !== null) {
                            if ($_activePromo->freeExistProductCountryIds !== null || $_activePromo->freeProductCountryIds !== null) {
                                $product_country_ids = json_decode($_activePromo->freeProductCountryIds ? $_activePromo->freeProductCountryIds : $_activePromo->freeExistProductCountryIds);

                                $get_product_info = array(
                                    "appType" => 'ecommerce', // appType = [ecommerce,baWebsite]
                                    "isOnline" => true,
                                    "isOffline" => false,
                                    "product_country_ids" => $product_country_ids,
                                );
                                $product_info = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->productHelper->getProductByProductCountriesV3(Auth::user()->CountryId, $langCode, $get_product_info));
                            }
                            array_push($promotions, (object) array(
                                "type" => "normal",
                                "subtype" =>
                                $_activePromo->freeProductCountryIds !== null ? "free_products" : ($_activePromo->freeExistProductCountryIds !== null ? "free_existing_products" : ($_activePromo->discount > 0 ? "discount" : ($_activePromo->freeExistProductCountryIds !== null && $_activePromo->discount > 0 ? "free_existing_products_and_discount" : ($_activePromo->freeProductCountryIds !== null && $_activePromo->discount > 0 ? "free_products_and_discount" : ($_activePromo->ProductBundleId !== null ? "product_bundle" : 'undefined'))))),
                                "cycle" => $_activePromo->promotionType,
                                "code" => $user_subscription->promoCode,
                                "discount" => $_activePromo->discount,
                                "info" => $_activePromo,
                                "product_info" => $product_info,
                            ));
                        }
                    }

                    // check for cancellation discounts in existing plan
                    if ($user_subscription->temp_discountPercent > 0) {
                        array_push($promotions, (object) array(
                            "type" => "cancellation",
                            "subtype" => null,
                            "cycle" => "Instant",
                            "code" => null,
                            "discount" => $user_subscription->temp_discountPercent,
                            "info" => null,
                            "product_info" => null,
                        ));
                    }

                    $data->existing_promotions = $promotions;
                    $plandetails = $this->planHelper->getPlanDetailsByPlanId($user_subscription->PlanId, $country->id, $langCode);

                    $data->plan_details = $plandetails;

                    $defaultplanproducts = [];
                    $defaultaddons = [];
                    foreach ($plandetails["product_info"] as $pd) {
                        array_push($defaultplanproducts, $pd["productcountryid"]);
                    }

                    $productAddons = [];
                    $productAddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                        ->join('products', 'products.id', 'productcountries.ProductId')
                        ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->select('subscriptions_productaddon.qty as AddonsQty', 'subscriptions_productaddon.cycle as AddonsCycle', 'subscriptions_productaddon.ProductCountryId as ProductCountryId', 'producttranslates.name', 'productimages.url', 'products.id as ProductId')
                        ->where('subscriptions_productaddon.subscriptionId', $user_subscription->id)
                        ->where('producttranslates.langCode', $langCode)
                        ->where('productimages.isDefault', 1)
                        ->where(function ($checkCurrentCycleProducts) use ($user_subscription) {
                            $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $user_subscription->currentCycle + 1);
                        })
                        ->get();
                    if ($productAddons) {
                        foreach ($productAddons as $pda) {
                            array_push($defaultplanproducts, $pda["ProductCountryId"]);
                            array_push($defaultaddons, $pda);
                        }
                    }

                    $data->defaultplanproducts = json_encode($defaultplanproducts);
                    $data->defaultaddons = $defaultaddons;
                    $data->defaultaddonsArray = [];
                    if ($data->defaultaddons) {
                        foreach ($data->defaultaddons as $das) {
                            $data->defaultaddonsArray[] = $das->ProductId;
                        }
                    }
                    // remove optional products if exists in plan details
                    foreach ($data->plan_details["product_info"] as $p_default) {
                        if (array_search((int) $p_default["productid"], $optional_product_ids)) {
                            $key = array_search((int) $p_default["productid"], $optional_product_ids);
                            unset($optional_product_ids[$key]);
                        }
                    }

                    // remove optional products if exists in default addons
                    // foreach ($data->defaultaddons as $key => $p_addons) {
                    //     foreach (array_keys($optional_product_ids, (int) $p_addons->ProductId, true) as $key) {
                    //         unset($optional_product_ids[$key]);
                    //     }
                    // }

                    $optional_products = Product::join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
                        ->whereIn('products.id', $optional_product_ids)
                        ->where('producttranslates.langCode', $langCode)
                        ->where('productcountries.CountryId', Auth::user()->CountryId)
                        ->where('productimages.isDefault', 1)
                        ->select('productcountries.id as ProductCountryId', 'products.id as ProductId', 'productcountries.*', 'productimages.url', 'producttranslates.*')
                        ->get();


                    foreach ($optional_products as $op) {
                        $op->isRecurring = false;
                        if (in_array($op->ProductCountryId, $defaultplanproducts)) {
                            foreach ($data->defaultaddons as $da) {
                                if ($da->ProductCountryId == $op->ProductCountryId && $da->AddonsCycle == "all") {
                                    $op->isRecurring = true;
                                }
                            }
                        }
                    }

                    $data->optional_products = $optional_products;

                    $subscription_card = Cards::where('id', $user_subscription->CardId)->first();

                    $shipping = DeliveryAddresses::where('id', Auth::user()->defaultShipping)->first();
                    $billing = DeliveryAddresses::where('id', Auth::user()->defaultBilling)->first();

                    $data->address = (object) array("shipping" => $shipping, "billing" => $billing);

                    $data->subscription_addons = $productAddons;

                    $data->current_subscription_duration = $user_subscription->duration;

                    $data->subscription_card = $subscription_card;

                    $data->available_plans = $this->getAllPlans(Auth::user()->id, $user_subscription->id);

                    $data->gender = $data->available_plans->gender;
                    if ($data->available_plans->gender == 'female') {
                        $data->optional_products = [];
                    }
                    $data->urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);

                    $data->langCode = strtoupper(app()->getLocale());

                    $data->usertype = 'web';

                    $data->promotion_type = 'shave_plans';

                    if ($productAddons) {
                        // push individual product info into items
                        foreach ($productAddons as $addon) {

                            $in_cycle = false;
                            $in_cycle_with_all = false;

                            if ($addon->cycle === "all") {
                                $plan_cycles = $addon->cycle;
                            } else {
                                $plan_cycles = json_decode($addon->cycle, true);
                                //check if all exists, if exists save to a new variable
                            }

                            // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                            if (is_array($plan_cycles)) {
                                foreach ($plan_cycles as $_p_cycles) {
                                    if ($_p_cycles["c"] === $user_subscription->currentCycle) {
                                        $in_cycle = true;
                                        $cycle_no = $_p_cycles["c"];
                                        $_qty = $_p_cycles["q"];
                                    } else {
                                        if ($_p_cycles["c"] === "all") {
                                            $in_cycle_with_all = true;
                                            $cycle_no = 1;
                                            $_qty = $_p_cycles["q"];
                                        }
                                    }
                                }
                            }

                            // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                            if (!is_array($plan_cycles)) {
                                //if cycle is in numbers / string
                                $_in_cycle = $plan_cycles;
                                if ($addon->cycle === "all") {
                                    $in_cycle = true;
                                    $cycle_no = "recurring";
                                    $_qty = $addon->product_qty;
                                } else {
                                    if ($_in_cycle === $user_subscription->currentCycle) {
                                        $in_cycle = true;
                                        $cycle_no = $_in_cycle;
                                        $_qty = $addon->product_qty;
                                    }
                                }
                            }

                            // if products are within the currentCycle, add query DB to get Addons
                            if ($in_cycle == true) {
                                $in_cycle == false;
                                $data->subscription_addon_product_name = (object) array("productcountryid" => $addon->ProductCountryId, "product_name" => $addon->name);
                            } else {
                                if ($in_cycle_with_all == true) {
                                    $data->subscription_addon_product_name = (object) array("productcountryid" => $addon->ProductCountryId, "product_name" => $addon->name);
                                }
                            }
                        }
                    }

                    return view('_authenticated.shave-plans-edit', compact('data'));
                } else {
                    return view('_guest.landing');
                }
            } else {
                return view('_guest.landing');
            }
        } catch (Exception $ex) {
            return redirect()->back();
        }
    }

    public function updateSubsProducts(Request $request)
    {
        try {
            if (Auth::check()) {
                $data = array(
                    "appType" => 'ecommerce', // appType = [ecommerce,baWebsite]
                    "isOnline" => true,
                    "isOffline" => false,
                    "product_country_ids" => $request->productcountryIds,
                );
                $info = $this->productHelper->getProductByProductCountriesV3(Auth::user()->CountryId, Auth::user()->defaultLanguage, $data);
                return $info;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getAllPlans($user_id, $subscription_id)
    {
        try {
            $blade_types = $handle_types = $bag_types = $addons = null;
            $blade_type = $handle_type = $bag_type = $addon_type = '';

            $available_frequencies = [];
            $non_filtered_planIds = [];
            $filtered_planIds = [];
            $current_plan_country_id = 0;
            $country_of_origin_matches = true;
            $filter_plans_country = 0;

            if ($user_id && $subscription_id) {
                $u = User::where('id', $user_id)->first();
                $filter_plans_country = $u->CountryId;
                $s = Subscriptions::where('id', $subscription_id)->first();
                $p_ctgry = 0;
                $p_drtns = [];
                if ($s->isCustom === 1 && $s->isTrial === 0) {
                    $p_ctgry = 2;
                }
                if ($s->isTrial === 1 && $s->isCustom === 0) {
                    $p_ctgry = 1;
                }
                $isAnnual = 0;
                $gender = 'male';
                $checkplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')->where('plans.id', $s->PlanId)->select('plansku.isAnnual')->first();
                $current_plan_info = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plans.PlanSkuId')
                    ->join('plan_categories', 'plan_categories.id', 'plan_category_details.PlanCategoryId')
                    ->where('plans.id', $s->PlanId)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId', 'plan_categories.id as PlanCategoryId', 'plan_categories.name as PlanCategoryName')
                    ->first();



                $current_plan_items = Plans::join('plan_details', 'plan_details.PlanId', 'plans.id')
                    ->where('plans.id', $s->PlanId)->get();

                foreach ($current_plan_items as $cpi) {
                    $current_plan_country_id = $cpi->CountryId;
                    $filter_plans_country = $cpi->CountryId;
                }

                if ($current_plan_country_id != $u->CountryId) {
                    $country_of_origin_matches = false;
                }

                if ($checkplans) {
                    $isAnnual = $checkplans->isAnnual;
                    if (strpos($current_plan_info->PlanCategoryName, 'Women') !== false && $s->isTrial == 1 && $s->isCustom == 0) {
                        $gender = 'female';
                        $p_ctgry = 3;
                    } else if (strpos($current_plan_info->PlanCategoryName, 'Women') !== false && $s->isCustom == 1 && $s->isTrial == 0) {
                        $gender = 'female';
                        $p_ctgry = 3;
                    }
                }

                if ($s->isCustom === 1 && $s->isTrial === 0 && ($isAnnual && $isAnnual == 1)) {
                    $p_ctgry = 8;
                }

                switch ($gender) {
                    case 'male':
                        $gp_ctgry = [config('global.all.plan_category_types.men.trial')];
                        $gp_products_ctgry = config('global.all.product_category_types.men');
                        $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.men.handles');
                        $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.men.blades');
                        $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.men.addons');
                        break;
                    case 'female':
                        $gp_ctgry = [config('global.all.plan_category_types.women.trial')];
                        $gp_products_ctgry = config('global.all.product_category_types.women');
                        $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.women.handles');
                        $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.women.blades');
                        $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.women.addons');
                        break;
                    default:
                        return null;
                }

                $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                    ->get();

                foreach ($p_relateds as $plan) {
                    array_push($p_drtns, (int) $plan->duration);
                }

                $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

                $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                    // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plansku.isAnnual', $isAnnual)
                    ->where('plans.CountryId', $filter_plans_country)
                    // ->where('plantranslates.langCode', $u->defaultLanguage)
                    ->whereIn('plansku.duration', $available_frequencies)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        // 'plantranslates.id as PlanTranslateId',
                        'plans.*',
                        // 'plantranslates.*',
                        'plansku.*'
                    )
                    ->get();
            
                foreach ($allplans as $plan) {
                    array_push($non_filtered_planIds, $plan->PlanId);
                }

                // Custom Plan Blade & Handle Types
                if ($p_ctgry === 2  || $p_ctgry === 8) {
                    $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Blade')
                        // ->where('productcountries.CountryId', $u->CountryId)
                        ->where('productcountries.CountryId', $current_plan_country_id)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();

                    $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons Handle')
                        // ->where('productcountries.CountryId', $u->CountryId)
                        ->where('productcountries.CountryId', $current_plan_country_id)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();

                    $bag_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons Bag')
                        // ->where('productcountries.CountryId', $u->CountryId)
                        ->where('productcountries.CountryId', $current_plan_country_id)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();

                    $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons')
                        // ->where('productcountries.CountryId', $u->CountryId)
                        ->where('productcountries.CountryId', $current_plan_country_id)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();
                }

                // Trial Plan Blade & Handle Types
                if ($p_ctgry === 1 || $p_ctgry === 3) {
                    $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', $gp_blade_type_names)
                        ->where('productcountries.CountryId', $filter_plans_country)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();

                    $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', $gp_handle_type_names)
                        ->where('productcountries.CountryId', $filter_plans_country)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();

                    $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', $gp_addon_type_names)
                        ->where('productcountries.CountryId', $filter_plans_country)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*',
                            'producttypes.*',
                            'products.*',
                            'productcountries.*'
                        )
                        ->get();
                }

                // get blade type
                if ($blade_types !== null) {
                    foreach ($blade_types as $b) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $b->ProductCountryId) {
                                $blade_type = $b;
                            }
                        }
                    }
                }

                // get handle type
                if ($handle_types !== null) {
                    foreach ($handle_types as $h) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $h->ProductCountryId) {
                                $handle_type = $h;
                            }
                        }
                    }
                }

                // get bag type
                if ($bag_types !== null) {
                    foreach ($bag_types as $bg) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $bg->ProductCountryId) {
                                $bag_type = $bg;
                            }
                        }
                    }
                }

                // get addon type
                if ($addons !== null) {
                    foreach ($addons as $a) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $a->ProductCountryId) {
                                $addon_type = $a;
                            }
                        }
                    }
                }

                foreach ($non_filtered_planIds as $planId) {
                    $items = Plans::join('plan_details', 'plan_details.PlanId', 'plans.id')
                        ->where('plans.id', $planId)
                        ->select('plans.id as PlanId', 'plan_details.id as PlanDetailId', 'plans.*', 'plan_details.*')
                        ->get();
                    // custom plan filter using blade only
                    if ($p_ctgry === 2 || $p_ctgry === 8 ) {
                        foreach ($items as $item) {
                            if ($item->ProductCountryId === $blade_type->id) {
                                array_push($filtered_planIds, $planId);
                            }
                        }
                    }

                    // trial plan filter using blade & handle type
                    if ($p_ctgry === 1 || $p_ctgry === 3) {
                        $handle_filter = [];
                        $blade_filter = [];
                        if (isset($handle_type)) {
                            foreach ($items as $item) {
                                if ($item->ProductCountryId === $handle_type->id) {
                                    array_push($handle_filter, $planId);
                                }
                            }
                        }

                        foreach ($items as $item) {
                            if ($item->ProductCountryId === $blade_type->id) {
                                array_push($blade_filter, $planId);
                            }
                        }

                        $match = array_intersect_key($handle_filter, $blade_filter);
                        if (!empty($match)) {
                            array_push($filtered_planIds, $match);
                        }
                    }
                }

                $filtered = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plans.CountryId', $filter_plans_country)
                    ->whereIn('plansku.duration', $available_frequencies)
                    ->whereIn('plans.id', $filtered_planIds)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        'plans.*',
                        'plansku.*'
                    )
                    ->groupBy('plansku.id')
                    ->get();

                $available_plans = (object) array(
                    "blade_type" => $blade_type,
                    "filtered_plans" => $filtered_planIds,
                    "active_plans" => $filtered,
                    "blade_types" => $blade_types,
                    "handle_types" => $handle_types,
                    "handle_type" => $handle_type,
                    "bag_types" => $bag_types,
                    "addons" => $addons,
                    "gender" => $gender,
                    "country_of_origin_matches" => $country_of_origin_matches
                );

                return $available_plans;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function updateShavePlan(Request $request)
    {
        // session()->forget('session_edit_shave_plans');
        $user = Auth::user();
        $success = [];
        // Get update session
        $updates = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode(session()->get('session_edit_shave_plans'), true));
        $updatesV2 = json_decode(session()->get('session_edit_shave_plans'), true);
        $subscription = $updates->user_data->subscription;
        $plan_country = Countries::join('plans', 'plans.CountryId', 'countries.id')->where('plans.id', $subscription->PlanId)->first();
        $plan_details = $updates->user_data->plan_details;
        $productcountryIds = explode(',', $updates->plan_products->productcountryIds);
        $cycles_changes = $updates->plan_products->cycles;
        $productcountryIds = array_map('intval', $productcountryIds);
        $subscription_product_addons_ids = [];
        $subscription_product_addons = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)
            ->where(function ($checkCurrentCycleProducts) use ($subscription) {
                $checkCurrentCycleProducts->orWhere('cycle', 'all')->orWhere('cycle', $subscription->currentCycle + 1);
            })->get();
        foreach ($subscription_product_addons as $subscription_product_addon) {
            array_push($subscription_product_addons_ids, (int) $subscription_product_addon->ProductCountryId);
        }
        $updated_products_list_for_add = [];
        $updated_products_list_for_remove = [];
        $original_price = $subscription->pricePerCharge;
        $modified_products_addon_price = 0;
        $original_products_addon_price = 0;
        $updated_price = 0;
        $canUpdateSubStatus = false;
        if ($subscription->status == "Processing" || $subscription->status == "On Hold" || $subscription->status == "Unrealized") {
            $canUpdateSubStatus = true;
        }


        // if subscription payment card was modified
        if ($updates->subscription_card->isModified === true) {
            if ($request->countryiso == 'kr') {

                $cardnumber = $updates->subscription_card->new->card_number;
                if (strpos($cardnumber, '-') !== false) {
                } else {
                    if (strpos($cardnumber, ' ') !== false) {
                        $cardnumber = str_replace(' ', '-', $cardnumber);
                    } else {
                        $seperatenumber = str_split($cardnumber, 4);
                        $cardnumber = $seperatenumber[0] . "-" . $seperatenumber[1] . "-" . $seperatenumber[2] . "-" . $seperatenumber[3];
                    }
                }

                $card_expiry_month = str_replace(' ', '', $updates->subscription_card->new->exp_month);
                $card_expiry_year = str_replace(' ', '', $updates->subscription_card->new->exp_year);
                $datalist = [
                    "customer_email" => $user->email,
                    "customer_name" => $user->firstName,
                    "card_name" => $user->firstName,
                    "card_number" => $cardnumber,
                    "card_password" => $updates->subscription_card->new->card_password,
                    "card_expiry_month" => $card_expiry_month,
                    "card_expiry_year" => $card_expiry_year,
                    "card_birth" => $updates->subscription_card->new->card_birth,

                ];

                $result = $this->nicepay->createBillingKey($datalist);

                if (isset($result)) {

                    if ($result["status"] == "success") {
                        $cards = new Cards();
                        $cards->customerId = $result["data"]["response"]["customer_uid"];
                        $cards->cardNumber = "-";
                        $cards->branchName = "-";
                        $cards->cardName = "-";
                        $cards->expiredYear =  "-";
                        $cards->expiredMonth = "-";
                        $cards->isDefault = 0;
                        $cards->payment_method = 'nicepay';
                        $cards->UserId = $user->id;
                        $cards->isRemoved = 0;
                        $cards->type = 'website';
                        $cards->CountryId = $user->CountryId;
                        $cards->save();

                        if (isset($cards->id)) {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $cards->id,
                            ]);

                            // cater for Audit Trail table
                            // SubscriptionHistories::create([
                            //     'message' => 'Modified Shave Plan',
                            //     'detail' => '[Update Card] XXXX XXXX XXXX ' . $updates->subscription_card->existing->cardNumber . ' to ' . $cards->cardNumber,
                            //     'subscriptionId' => $sub->id,
                            // ]);

                            $success["update_card"]["status"] = true;
                            $success["update_card"]["card"] = $result;
                        }
                    }
                }
            } else {
                $stripe_error_messages = isset($request->stripe_error) ? $request->stripe_error : null;
                if ( // card added, otp passed & refund succeeded
                    isset($request->otp_process) && $request->otp_process == "true" &&
                    isset($request->refund_process) && $request->refund_process  == "true" &&
                    isset($request->card_id) && $request->card_id != null
                ) {
                    $new_card = Cards::where('id', $request->card_id)->first();
                    $old_card = Cards::where('id', $updates->subscription_card->existing->id)->first();

                    if ($new_card && $old_card) {
                        if ($canUpdateSubStatus == false) {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id
                            ]);
                        } else {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id,
                                "status" => "Processing",
                                "recharge_date" => NULL,
                                "total_recharge" => 0,
                                "unrealizedCust" => 0
                            ]);
                        }
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Payment Card Update [' . $old_card->cardNumber . '] to [' . $new_card->cardNumber . ']',
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {

                        if ($canUpdateSubStatus == false) {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id
                            ]);
                        } else {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id,
                                "status" => "Processing",
                                "recharge_date" => NULL,
                                "total_recharge" => 0,
                                "unrealizedCust" => 0
                            ]);
                        }
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Payment Card Update',
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                } else if ( // card added, otp passed & refund failed
                    isset($request->otp_process) && $request->otp_process == "true"  &&
                    isset($request->refund_process) && $request->refund_process  == "false" &&
                    isset($request->card_id) && $request->card_id != null
                ) {
                    if ($new_card && $old_card) {
                        if ($canUpdateSubStatus == false) {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id
                            ]);
                        } else {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id,
                                "status" => "Processing",
                                "recharge_date" => NULL,
                                "total_recharge" => 0,
                                "unrealizedCust" => 0
                            ]);
                        }
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Payment Card Update [' . $old_card->cardNumber . '] to [' . $new_card->cardNumber . '] | Error => [' . $stripe_error_messages . ' ]',
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {
                        if ($canUpdateSubStatus == false) {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id
                            ]);
                        } else {
                            Subscriptions::where('id', $subscription->id)->update([
                                "CardId" => $request->card_id,
                                "status" => "Processing",
                                "recharge_date" => NULL,
                                "total_recharge" => 0,
                                "unrealizedCust" => 0
                            ]);
                        }
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Payment Card Update | Error => [' . $stripe_error_messages . ' ]',
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                } else if ( // card added, otp failed & refund failed
                    isset($request->otp_process) && $request->otp_process == "false"  &&
                    isset($request->refund_process) && $request->refund_process  == "false" &&
                    isset($request->card_id) && $request->card_id != null
                ) {
                    $new_card = Cards::where('id', $request->card_id)->first();
                    if ($new_card) {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Attempt to update payment card to [' . $new_card->cardNumber . '] failed. | Error => [' . $stripe_error_messages . ' ]',
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Attempt to update payment card failed | Error => [' . $stripe_error_messages . ' ]',
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                } else {
                    SubscriptionHistories::create([
                        'message' => 'Modified Shave Plan',
                        'detail' => '[Plan Update] Attempt to update payment card failed | Error => [' . $stripe_error_messages . ' ]',
                        'subscriptionId' => $subscription->id,
                    ]);
                }

                // $subscription_card = [];
                // $subscription_card["user"] = $user;
                // $subscription_card["add_card"] = (object) array(
                //     "name" => $user->firstName,
                //     "cvv" => (int) $updates->subscription_card->new->cvv,
                //     "exp_month" => (int) $updates->subscription_card->new->exp_month,
                //     "exp_year" => (int) $updates->subscription_card->new->exp_year,
                //     "number" => (int) $updates->subscription_card->new->card_number,
                // );

                // $success = [];

                // // Create customer card in stripe and get card token id
                // $createdCard = $this->stripe->createTokenV2($subscription_card["add_card"]);
                // if ($createdCard) {
                //     // Create customer in stripe with source linked to the card token id
                //     $createdCustomer = $this->stripe->createCustomer(config('app.appType'), $subscription_card, $createdCard);
                //     if ($createdCustomer) {
                //         $cards = new Cards();
                //         $cards->customerId = $createdCustomer["customer"]["id"];
                //         $cards->cardNumber = $createdCard["card"]->last4;
                //         $cards->branchName = $createdCard["card"]->brand;
                //         $cards->cardName = $createdCard["card"]->name;
                //         $cards->expiredYear = $createdCard["card"]->exp_year;
                //         $cards->expiredMonth = $createdCard["card"]->exp_month;
                //         $cards->isDefault = 0;
                //         $cards->payment_method = 'stripe';
                //         $cards->UserId = $user->id;
                //         $cards->isRemoved = 0;
                //         $cards->type = 'website';
                //         $cards->CountryId = $user->CountryId;
                //         $cards->save();

                //         if (isset($cards->id)) {

                //             Subscriptions::where('id', $subscription->id)->update([
                //                 "CardId" => $cards->id,
                //             ]);

                //             // cater for Audit Trail table
                //             // SubscriptionHistories::create([
                //             //     'message' => 'Modified Shave Plan',
                //             //     'detail' => '[Update Card] XXXX XXXX XXXX ' . $updates->subscription_card->existing->cardNumber . ' to ' . $cards->cardNumber,
                //             //     'subscriptionId' => $sub->id,
                //             // ]);

                //             $success["update_card"]["status"] = true;
                //             $success["update_card"]["customer"] = $createdCustomer;
                //             $success["update_card"]["card"] = $createdCard;
                //             $success["update_card"]["card_from_db"] = Cards::where('id', $cards->id)->first();
                //         }
                //     }
                // }
            }
        }

        // if addon products was added
        if ($updates->plan_products->isModified === true) {
            if (count($updatesV2["plan_products"]["cycles"]) > 0) {
                $updated_pcs = [];
                foreach ($productcountryIds as $ids) {
                    foreach ($subscription_product_addons as $addon) {
                        if ((int) $addon->ProductCountryId !== (int) $ids) {
                            if (!in_array((int) $ids, $updated_pcs)) {
                                array_push($updated_pcs, (int) $ids);
                            }
                        }
                    }
                }
                foreach ($subscription_product_addons as $addon_) {
                    if (!in_array((int) $addon_->ProductCountryId, $updated_pcs)) {
                        array_push($updated_pcs, (int) $addon_->ProductCountryId);
                    }
                }

                $addon = [];

                // if product is found in plan details, remove
                foreach ($updated_pcs as $key => $p) {
                    foreach ($plan_details->product_info as $original_item) {
                        if ($p === (int) $original_item->productcountryid) {
                            unset($updated_pcs[$key]);
                        }
                    }
                }

                $isExistingAddonsModified = false;
                $isExistingAddonsModifiedList = [];
                // if product is found in subscription addons, remove
                foreach ($updated_pcs as $key => $p) {
                    foreach ($subscription_product_addons as $original_addon) {
                        if ($p === (int) $original_addon->ProductCountryId) {
                            // check if the cycle is the same
                            foreach ($updates->plan_products->cycles as $addon) {
                                if ($original_addon->cycle == "all" &&  $addon->isRecurring === true) {
                                    unset($updated_pcs[$key]);
                                } else {
                                    $isExistingAddonsModified = true;
                                    if ($isExistingAddonsModified == true && $addon->isRecurring === true) {
                                        array_push($isExistingAddonsModifiedList, (int) $original_addon->ProductCountryId);
                                        SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)
                                            ->where('ProductCountryId', (int) $original_addon->ProductCountryId)
                                            ->update([
                                                "cycle" => "all"
                                            ]);
                                    }
                                }
                            }
                        }
                    }
                }

                if (count($isExistingAddonsModifiedList) > 0) {
                    $skuNames = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')->whereIn('productcountries.id', $isExistingAddonsModifiedList)->select('products.sku')->get();
                    $_skuNames = [];
                    foreach ($skuNames as $sN) {
                        array_push($_skuNames, $sN["sku"]);
                    }
                    $__skuNames = implode(',', $_skuNames);
                    SubscriptionHistories::create([
                        'message' => 'Addons',
                        'detail' => 'Modify Existing Addons to Recurring: ' . $__skuNames,
                        'subscriptionId' => $subscription->id,
                    ]);
                }



                foreach ($updated_pcs as $ppp) {
                    array_push($updated_products_list_for_add, (int) $ppp);
                }



                foreach ($updates->plan_products->removed as $key => $p) {
                    foreach ($updates->plan_products->cycles as $original_addon) {
                        if ($p === (int) $original_addon->product_country) {
                            unset($updates->plan_products->removed->{$key});
                        }
                    }
                }

                $isNewAddons = false;
                $isNewAddonsLists = [];
                foreach ($updates->plan_products->cycles as $addon) {
                    $isNewAddons = true;
                    if (!in_array((int) $addon->product_country, $subscription_product_addons_ids)) {
                        $cycle = $addon->isRecurring === true ? 'all' : (int) $subscription->currentCycle + 1;
                        array_push($isNewAddonsLists, (int) $addon->product_country);
                        SubscriptionsProductAddOns::updateOrCreate(
                            ["subscriptionId" => $subscription->id, "ProductCountryId" => $addon->product_country],
                            [
                                "qty" => 1,
                                "cycle" => $cycle,
                                "isWithTrialKit" => 0,
                                "skipFirstCycle" => 1,
                                "isAnnual" => 0,
                            ]
                        );
                    }
                }

                if (count($isNewAddonsLists) > 0) {
                    $skuNames = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')->whereIn('productcountries.id', $isNewAddonsLists)->select('products.sku')->get();
                    $_skuNames = [];
                    foreach ($skuNames as $sN) {
                        array_push($_skuNames, $sN["sku"]);
                    }
                    $__skuNames = implode(',', $_skuNames);
                    SubscriptionHistories::create([
                        'message' => 'Addons',
                        'detail' => 'New Addons : ' . $__skuNames,
                        'subscriptionId' => $subscription->id,
                    ]);
                }
            }

            if (count($updatesV2["plan_products"]["removed"]) > 0) {
                $updated_pcs = [];
                foreach ($productcountryIds as $ids) {
                    foreach ($subscription_product_addons as $addon) {
                        if ((int) $addon->ProductCountryId !== (int) $ids) {
                            if (!in_array((int) $ids, $updated_pcs)) {
                                array_push($updated_pcs, (int) $ids);
                            }
                        }
                    }
                }
                foreach ($subscription_product_addons as $addon_) {
                    if (!in_array((int) $addon_->ProductCountryId, $updated_pcs)) {
                        array_push($updated_pcs, (int) $addon_->ProductCountryId);
                    }
                }

                foreach ($updated_pcs as $ppp) {
                    array_push($updated_products_list_for_remove, (int) $ppp);
                }
                // $original_price = $subscription->pricePerCharge;
                // $updated_price = ProductCountry::whereIn('id', $updated_pcs)->select('sellPrice', 'id')->sum('sellPrice');

                $isRemovedAddonsLists = [];
                foreach ($updates->plan_products->removed as $remove_id) {
                    if (in_array((int) $remove_id, $subscription_product_addons_ids)) {
                        array_push($isRemovedAddonsLists, (int) $remove_id);
                        SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->where('ProductCountryId', $remove_id)->take(1)->delete();
                        // update Subscription price to reflect latest products
                        // Subscriptions::where('id', $subscription->id)->update([
                        //     "pricePerCharge" => $updated_price,
                        //     "price" => $updated_price,
                        // ]);
                        // // add into subscription history
                        // SubscriptionHistories::create([
                        //     'message' => 'Modified Shave Plan',
                        //     'detail' => '[Update Price] ' . $plan_country->currencyDisplay . $original_price . ' to ' . $plan_country->currencyDisplay . $updated_price,
                        //     'subscriptionId' => $subscription->id,
                        // ]);
                    }
                }

                if (count($isRemovedAddonsLists) > 0) {
                    $skuNames = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')->whereIn('productcountries.id', $isRemovedAddonsLists)->select('products.sku')->get();
                    $_skuNames = [];
                    foreach ($skuNames as $sN) {
                        array_push($_skuNames, $sN["sku"]);
                    }
                    $__skuNames = implode(',', $_skuNames);
                    SubscriptionHistories::create([
                        'message' => 'Addons',
                        'detail' => 'Remove Addons : ' . $__skuNames,
                        'subscriptionId' => $subscription->id,
                    ]);
                }
            }

            // re-calculate price
            $subscription_product_addons_ids_updated = [];
            $subscription_product_addons_updated = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)
                ->where(function ($checkCurrentCycleProducts) use ($subscription) {
                    $checkCurrentCycleProducts->orWhere('cycle', 'all')->orWhere('cycle', $subscription->currentCycle + 1);
                })
                ->get();

            foreach ($subscription_product_addons_updated as $subscription_product_addon) {
                array_push($subscription_product_addons_ids_updated, (int) $subscription_product_addon->ProductCountryId);
            }

            $original_products_addon_price = ProductCountry::whereIn('id', $subscription_product_addons_ids)->select('sellPrice', 'id')->sum('sellPrice');
            $modified_products_addon_price = ProductCountry::whereIn('id', $subscription_product_addons_ids_updated)->select('sellPrice', 'id')->sum('sellPrice');

            $updated_price = (float) $original_price - (float) $original_products_addon_price;
            $updated_price = (float) $updated_price + (float) $modified_products_addon_price;

            // dd($original_price, $original_products_addon_price, $modified_products_addon_price, $updated_price);
            // update Subscription price to reflect latest products
            if ($updates->frequency->isModified === true) {
            } else {
                Subscriptions::where('id', $subscription->id)->update([
                    "pricePerCharge" => number_format((float) $updated_price, 2, '.', ''),
                    "price" => number_format((float) $updated_price, 2, '.', ''),
                ]);
            }
            // add into subscription history
            SubscriptionHistories::create([
                'message' => 'Modified Shave Plan',
                'detail' => '[Update Product & Price] ' . $plan_country->currencyDisplay . $original_price . ' to ' . $plan_country->currencyDisplay . number_format((float) $updated_price, 2, '.', ''),
                'subscriptionId' => $subscription->id,
            ]);
        }

        // if billing address was modified
        if ($updates->address->billing->isModified === true) {
            $pcode = "";
            if ($updates->address->billing->new->portalCode != null || $updates->address->billing->new->portalCode != "") {
                $pcode = $updates->address->billing->new->portalCode;
            } else {
                $pcode = "00000";
            }
            DeliveryAddresses::where('id', $updates->address->billing->existing->id)->update([
                "address" => $updates->address->billing->new->address,
                "city" => $updates->address->billing->new->city,
                "portalCode" => $pcode,
                "state" => $updates->address->billing->new->state,
            ]);

            $success["update_billing"]["status"] = true;
        }

        // if shipping address was modified
        if ($updates->address->shipping->isModified === true) {
            $pcode = "";
            if ($updates->address->shipping->new->portalCode != null || $updates->address->shipping->new->portalCode != "") {
                $pcode = $updates->address->shipping->new->portalCode;
            } else {
                $pcode = "00000";
            }
            DeliveryAddresses::where('id', $updates->address->shipping->existing->id)->update([
                "SSN" => $updates->address->shipping->new->SSN,
                "address" => $updates->address->shipping->new->address,
                "city" => $updates->address->shipping->new->city,
                "portalCode" => $pcode,
                "state" => $updates->address->shipping->new->state,
            ]);

            $success["update_shipping"]["status"] = true;
        }

        // if shave plan frequency was modified
        if ($updates->frequency->isModified === true) {

            // re-calculate price
            $subscription_product_addons_ids_updated = [];
            $subscription_product_addons_updated = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)
                ->where(function ($checkCurrentCycleProducts) use ($subscription) {
                    $checkCurrentCycleProducts->orWhere('cycle', 'all')->orWhere('cycle', $subscription->currentCycle + 1);
                })
                ->get();

            foreach ($subscription_product_addons_updated as $subscription_product_addon) {
                array_push($subscription_product_addons_ids_updated, (int) $subscription_product_addon->ProductCountryId);
            }

            $original_products_addon_price = ProductCountry::whereIn('id', $subscription_product_addons_ids)->select('sellPrice', 'id')->sum('sellPrice');
            $modified_products_addon_price = ProductCountry::whereIn('id', $subscription_product_addons_ids_updated)->select('sellPrice', 'id')->sum('sellPrice');

            $updated_price = (float) $original_price - (float) $original_products_addon_price;
            $updated_price = (float) $updates->frequency->_modified->sellPrice + (float) $modified_products_addon_price;

            // $updated_price = (float) $updates->frequency->_modified->sellPrice + (float) $modified_products_addon_price;
            // $updated_price = (float) $updated_price + (float) $modified_products_addon_price;
            Subscriptions::where('id', $subscription->id)->update([
                "PlanId" => $updates->frequency->_modified->PlanId,
                "pricePerCharge" => $updated_price,
                "price" => $updated_price,
            ]);

            SubscriptionHistories::create([
                'message' => 'Modified Shave Plan',
                'detail' => '[Plan Update] ' . $updates->frequency->_original->description . ' to ' . $updates->frequency->_modified->description,
                'subscriptionId' => $subscription->id,
            ]);

            $success["update_plan_frequency"]["status"] = true;
            if ($updates->frequency->_original->PlanId != $updates->frequency->_modified->PlanId) {
                $currentCountry = json_decode(session()->get('currentCountry'), true);

                $currentLocale = "";
                if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
                    $currentLocale = json_decode(session()->get('currentCountry'), true)['defaultLang'];
                } else {
                    $currentLocale = 'en';
                }

                $countryid = "";
                if ($currentCountry['id']) {
                    $countryid = $currentCountry['id'];
                } else if ($currentCountry) {
                    $countryid = 1;
                }

                $now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
                $email = [];
                $email['email'] = Auth::user()->email;
                $email['currentdate'] = $now;
                $email['subscriptionId'] = $subscription->id;
                $email['userId'] = Auth::user()->id;
                $email['countryId'] = $countryid;
                $email['type'] = 1;
                $email['isPlan'] = 1;

                // Initiate New CustomerRegisteredEvent
                // hide email redis
                $this->emailQueueHelper->buildEmailTemplate('subscription-modified', $email, array($currentCountry, $currentLocale));
            }
        }

        // if addon products was added
        if ($updates->promotion->isModified === true) {

            $existing_promo_id = $subscription->promotionId;
            $existing_promo_code = $subscription->promoCode;
            $existing_cancellation = $subscription->temp_discountPercent;

            $selected_promo = $updates->promotion->selected;
            $selected_promo_data = $updates->promotion->checking;
            if ($selected_promo === 'cancellation_discount') {
                // do nothing
                Subscriptions::where('id', $subscription->id)->update([
                    "temp_discountPercent" => $updates->promotion->temp_discountPercent,
                    "promoCode" => null,
                    "promotionId" => null,
                ]);

                if ($existing_promo_code && $existing_promo_id) {
                    SubscriptionHistories::create([
                        'message' => 'Modified Shave Plan',
                        'detail' => '[Promotion] Replaced ' . $existing_promo_code . ' with Cancellation Discount',
                        'subscriptionId' => $subscription->id,
                    ]);
                } else {
                    SubscriptionHistories::create([
                        'message' => 'Modified Shave Plan',
                        'detail' => '[Promotion] Added Cancellation Discount',
                        'subscriptionId' => $subscription->id,
                    ]);
                }
            } else {
                if ($existing_promo_code !== null) {
                    if ($selected_promo === $existing_promo_code) {
                        // do nothing
                        // remove cancellation if exists
                        // new promotion
                        Subscriptions::where('id', $subscription->id)->update([
                            "temp_discountPercent" => 0,
                        ]);

                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Promotion] Replaced Cancellation Discount with  ' . $existing_promo_code,
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {
                        // new promotion
                        Subscriptions::where('id', $subscription->id)->update([
                            "promoCode" => $selected_promo,
                            "promotionId" => $selected_promo_data->pid,
                            "discountPercent" => $selected_promo_data->discount,
                        ]);

                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Promotion] Replaced ' . $existing_promo_code . ' with ' . $selected_promo,
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                } else {
                    // new promotion
                    Subscriptions::where('id', $subscription->id)->update([
                        "promoCode" => $selected_promo,
                        "promotionId" => $selected_promo_data->pid,
                        "discountPercent" => $selected_promo_data->discount,
                    ]);

                    SubscriptionHistories::create([
                        'message' => 'Modified Shave Plan',
                        'detail' => '[Promotion] Applied ' . $selected_promo,
                        'subscriptionId' => $subscription->id,
                    ]);
                }
            }
        }

        session()->forget('session_edit_shave_plans');

        return $success;
    }

    public function updateShavePlanAnnuals(Request $request)
    {
        try {
            // session()->forget('session_edit_shave_plans');
            $user = Auth::user();
            $success = [];
            // Get update session
            $updates = \App\Helpers\LaravelHelper::ConvertArraytoObject(json_decode(session()->get('session_edit_shave_plans'), true));
            $updatesV2 = json_decode(session()->get('session_edit_shave_plans'), true);
            $subscription = $updates->user_data->subscription;
            $plan_country = Countries::join('plans', 'plans.CountryId', 'countries.id')->where('plans.id', $subscription->PlanId)->first();
            $plan_details = $updates->user_data->plan_details;
            $productcountryIds = explode(',', $updates->plan_products->productcountryIds);
            $productcountryIds = array_map('intval', $productcountryIds);
            $subscription_product_addons_ids = [];
            $subscription_product_addons = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->get();
            foreach ($subscription_product_addons as $subscription_product_addon) {
                array_push($subscription_product_addons_ids, (int) $subscription_product_addon->ProductCountryId);
            }

            $updated_products_list_for_add = [];
            $updated_products_list_for_remove = [];
            $original_price = $subscription->pricePerCharge;
            $modified_products_addon_price = 0;
            $original_products_addon_price = 0;
            $updated_price = 0;
            $originalIsAnnual = $request->originalIsAnnual;
            $originalIsAnnual = $request->originalIsAnnual;
            $gender = $request->gender;

            // if subscription payment card was modified
            if ($updates->subscription_card->isModified === true) {
                if ($request->countryiso == 'kr') {

                    $cardnumber = $updates->subscription_card->new->card_number;
                    if (strpos($cardnumber, '-') !== false) {
                    } else {
                        if (strpos($cardnumber, ' ') !== false) {
                            $cardnumber = str_replace(' ', '-', $cardnumber);
                        } else {
                            $seperatenumber = str_split($cardnumber, 4);
                            $cardnumber = $seperatenumber[0] . "-" . $seperatenumber[1] . "-" . $seperatenumber[2] . "-" . $seperatenumber[3];
                        }
                    }

                    $card_expiry_month = str_replace(' ', '', $updates->subscription_card->new->exp_month);
                    $card_expiry_year = str_replace(' ', '', $updates->subscription_card->new->exp_year);
                    $datalist = [
                        "customer_email" => $user->email,
                        "customer_name" => $user->firstName,
                        "card_name" => $user->firstName,
                        "card_number" => $cardnumber,
                        "card_password" => $updates->subscription_card->new->card_password,
                        "card_expiry_month" => $card_expiry_month,
                        "card_expiry_year" => $card_expiry_year,
                        "card_birth" => $updates->subscription_card->new->card_birth,

                    ];

                    $result = $this->nicepay->createBillingKey($datalist);

                    if (isset($result)) {

                        if ($result["status"] == "success") {
                            $cards = new Cards();
                            $cards->customerId = $result["data"]["response"]["customer_uid"];
                            $cards->cardNumber = "-";
                            $cards->branchName = "-";
                            $cards->cardName = "-";
                            $cards->expiredYear = "-";
                            $cards->expiredMonth = "-";
                            $cards->isDefault = 0;
                            $cards->payment_method = 'nicepay';
                            $cards->UserId = $user->id;
                            $cards->isRemoved = 0;
                            $cards->type = 'website';
                            $cards->CountryId = $user->CountryId;
                            $cards->save();

                            if (isset($cards->id)) {
                                Subscriptions::where('id', $subscription->id)->update([
                                    "CardId" => $cards->id,
                                ]);

                                // cater for Audit Trail table
                                // SubscriptionHistories::create([
                                //     'message' => 'Modified Shave Plan',
                                //     'detail' => '[Update Card] XXXX XXXX XXXX ' . $updates->subscription_card->existing->cardNumber . ' to ' . $cards->cardNumber,
                                //     'subscriptionId' => $sub->id,
                                // ]);

                                $success["update_card"]["status"] = true;
                                $success["update_card"]["card"] = $result;
                            }
                        }
                    }
                } else {

                    if (isset($request->otp_process) && $request->otp_process == "true") {
                        Subscriptions::where('id', $subscription->id)->update([
                            "CardId" => $cards->id,
                        ]);
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Payment Card Update',
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Attempt to update payment card failed',
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                    // $subscription_card = [];
                    // $subscription_card["user"] = $user;
                    // $subscription_card["add_card"] = (object) array(
                    //     "name" => $user->firstName,
                    //     "cvv" => (int) $updates->subscription_card->new->cvv,
                    //     "exp_month" => (int) $updates->subscription_card->new->exp_month,
                    //     "exp_year" => (int) $updates->subscription_card->new->exp_year,
                    //     "number" => (int) $updates->subscription_card->new->card_number,
                    // );

                    // $success = [];

                    // // Create customer card in stripe and get card token id
                    // $createdCard = $this->stripe->createTokenV2($subscription_card["add_card"]);
                    // if ($createdCard) {
                    //     // Create customer in stripe with source linked to the card token id
                    //     $createdCustomer = $this->stripe->createCustomer(config('app.appType'), $subscription_card, $createdCard);
                    //     if ($createdCustomer) {
                    //         $cards = new Cards();
                    //         $cards->customerId = $createdCustomer["customer"]["id"];
                    //         $cards->cardNumber = $createdCard["card"]->last4;
                    //         $cards->branchName = $createdCard["card"]->brand;
                    //         $cards->cardName = $createdCard["card"]->name;
                    //         $cards->expiredYear = $createdCard["card"]->exp_year;
                    //         $cards->expiredMonth = $createdCard["card"]->exp_month;
                    //         $cards->isDefault = 0;
                    //         $cards->payment_method = 'stripe';
                    //         $cards->UserId = $user->id;
                    //         $cards->isRemoved = 0;
                    //         $cards->type = 'website';
                    //         $cards->CountryId = $user->CountryId;
                    //         $cards->save();

                    //         if (isset($cards->id)) {

                    //             Subscriptions::where('id', $subscription->id)->update([
                    //                 "CardId" => $cards->id,
                    //             ]);

                    //             // cater for Audit Trail table
                    //             // SubscriptionHistories::create([
                    //             //     'message' => 'Modified Shave Plan',
                    //             //     'detail' => '[Update Card] XXXX XXXX XXXX ' . $updates->subscription_card->existing->cardNumber . ' to ' . $cards->cardNumber,
                    //             //     'subscriptionId' => $sub->id,
                    //             // ]);

                    //             $success["update_card"]["status"] = true;
                    //             $success["update_card"]["customer"] = $createdCustomer;
                    //             $success["update_card"]["card"] = $createdCard;
                    //             $success["update_card"]["card_from_db"] = Cards::where('id', $cards->id)->first();
                    //         }
                    //     }
                    // }
                }
            }

            // if billing address was modified
            if ($updates->address->billing->isModified === true) {
                $pcode = "";
                if ($updates->address->billing->new->portalCode != null || $updates->address->billing->new->portalCode != "") {
                    $pcode = $updates->address->billing->new->portalCode;
                } else {
                    $pcode = "00000";
                }
                DeliveryAddresses::where('id', $updates->address->billing->existing->id)->update([
                    "address" => $updates->address->billing->new->address,
                    "city" => $updates->address->billing->new->city,
                    "portalCode" => $pcode,
                    "state" => $updates->address->billing->new->state,
                ]);

                $success["update_billing"]["status"] = true;
            }

            // if shipping address was modified
            if ($updates->address->shipping->isModified === true) {
                $pcode = "";
                if ($updates->address->shipping->new->portalCode != null || $updates->address->shipping->new->portalCode != "") {
                    $pcode = $updates->address->shipping->new->portalCode;
                } else {
                    $pcode = "00000";
                }
                DeliveryAddresses::where('id', $updates->address->shipping->existing->id)->update([
                    "SSN" => $updates->address->shipping->new->SSN,
                    "address" => $updates->address->shipping->new->address,
                    "city" => $updates->address->shipping->new->city,
                    "portalCode" => $pcode,
                    "state" => $updates->address->shipping->new->state,
                ]);

                $success["update_shipping"]["status"] = true;
            }

            // if promotion was modified
            if ($updates->promotion->isModified === true) {

                $existing_promo_id = $subscription->promotionId;
                $existing_promo_code = $subscription->promoCode;
                $existing_cancellation = $subscription->temp_discountPercent;

                $selected_promo = $updates->promotion->selected;
                $selected_promo_data = $updates->promotion->checking;
                if ($selected_promo === 'cancellation_discount') {
                    // do nothing
                    Subscriptions::where('id', $subscription->id)->update([
                        "temp_discountPercent" => $updates->promotion->temp_discountPercent,
                        "promoCode" => null,
                        "promotionId" => null,
                    ]);

                    if ($existing_promo_code && $existing_promo_id) {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Promotion] Replaced ' . $existing_promo_code . ' with Cancellation Discount',
                            'subscriptionId' => $subscription->id,
                        ]);
                    } else {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Promotion] Added Cancellation Discount',
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                } else {
                    if ($existing_promo_code !== null) {
                        if ($selected_promo === $existing_promo_code) {
                            // do nothing
                            // remove cancellation if exists
                            // new promotion
                            Subscriptions::where('id', $subscription->id)->update([
                                "temp_discountPercent" => 0,
                            ]);

                            SubscriptionHistories::create([
                                'message' => 'Modified Shave Plan',
                                'detail' => '[Promotion] Replaced Cancellation Discount with  ' . $existing_promo_code,
                                'subscriptionId' => $subscription->id,
                            ]);
                        } else {
                            // new promotion
                            Subscriptions::where('id', $subscription->id)->update([
                                "promoCode" => $selected_promo,
                                "promotionId" => $selected_promo_data->pid,
                                "discountPercent" => $selected_promo_data->discount,
                            ]);

                            SubscriptionHistories::create([
                                'message' => 'Modified Shave Plan',
                                'detail' => '[Promotion] Replaced ' . $existing_promo_code . ' with ' . $selected_promo,
                                'subscriptionId' => $subscription->id,
                            ]);
                        }
                    } else {
                        // new promotion
                        Subscriptions::where('id', $subscription->id)->update([
                            "promoCode" => $selected_promo,
                            "promotionId" => $selected_promo_data->pid,
                            "discountPercent" => $selected_promo_data->discount,
                        ]);

                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Promotion] Applied ' . $selected_promo,
                            'subscriptionId' => $subscription->id,
                        ]);
                    }
                }
            }

            // if products were modified [phase 2]
            if ($updates->plan_products->isModified === true && $updates->frequency->isModified !== true) {
                // $annualqty = isset($updates->frequency->_modified->duration) !== false && isset($updates->frequency->_original->duration) !== false ? 12 / (int) $updates->frequency->_modified->duration : 12 / (int) $plan_details->planduration;
                // // only products are modified (added/removed)
                // if (count($updatesV2["plan_products"]["cycles"]) > 0) {
                //     $updated_pcs = [];
                //     foreach ($productcountryIds as $ids) {
                //         foreach ($subscription_product_addons as $addon) {
                //             if ((int) $addon->ProductCountryId !== (int) $ids) {
                //                 if (!in_array((int) $ids, $updated_pcs)) {
                //                     array_push($updated_pcs, (int) $ids);
                //                 }
                //             }
                //         }
                //     }
                //     foreach ($subscription_product_addons as $addon_) {
                //         if (!in_array((int) $addon_->ProductCountryId, $updated_pcs)) {
                //             array_push($updated_pcs, (int) $addon_->ProductCountryId);
                //         }
                //     }

                //     $addon = [];

                //     // if product is found in plan details, remove
                //     foreach ($updated_pcs as $key => $p) {
                //         foreach ($plan_details->product_info as $original_item) {
                //             if ($p === (int) $original_item->productcountryid) {
                //                 unset($updated_pcs[$key]);
                //             }
                //         }
                //     }
                //     // if product is found in subscription addons, remove
                //     foreach ($updated_pcs as $key => $p) {
                //         foreach ($subscription_product_addons as $original_addon) {
                //             if ($p === (int) $original_addon->ProductCountryId) {
                //                 unset($updated_pcs[$key]);
                //             }
                //         }
                //     }

                //     foreach ($updated_pcs as $ppp) {
                //         array_push($updated_products_list_for_add, (int) $ppp);
                //     }

                //     foreach ($updates->plan_products->removed as $key => $p) {
                //         foreach ($updates->plan_products->cycles as $original_addon) {
                //             if ($p === (int) $original_addon->product_country) {
                //                 unset($updates->plan_products->removed->{$key});
                //             }
                //         }
                //     }
                //     if (isset($updates->frequency->_modified->duration) !== false && isset($updates->frequency->_original->duration) !== false) {
                //         if ((int) $updates->frequency->_modified->duration !== (int) $updates->frequency->_original->duration) {
                //             $subscription_product_addons_check = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->get();
                //             if ($subscription_product_addons_check) {
                //                 SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->update([
                //                     "qty" => 12 / (int) $updates->frequency->_modified->duration,
                //                 ]);
                //             }
                //         }
                //     }

                //     foreach ($updates->plan_products->cycles as $addon) {
                //         if (!in_array((int) $addon->product_country, $subscription_product_addons_ids)) {
                //             $cycle = $addon->isRecurring === true ? 'all' : 1;
                //             SubscriptionsProductAddOns::create([
                //                 "subscriptionId" => $subscription->id,
                //                 "ProductCountryId" => $addon->product_country,
                //                 "qty" => $annualqty,
                //                 "cycle" => $cycle,
                //                 "skipFirstCycle" => 0,
                //                 "isAnnual" => 1,
                //                 "isWithTrialKit" => 0,
                //             ]);
                //         }
                //     }
                // }

                // if (count($updatesV2["plan_products"]["removed"]) > 0) {
                //     $updated_pcs = [];
                //     foreach ($productcountryIds as $ids) {
                //         foreach ($subscription_product_addons as $addon) {
                //             if ((int) $addon->ProductCountryId !== (int) $ids) {
                //                 if (!in_array((int) $ids, $updated_pcs)) {
                //                     array_push($updated_pcs, (int) $ids);
                //                 }
                //             }
                //         }
                //     }
                //     foreach ($subscription_product_addons as $addon_) {
                //         if (!in_array((int) $addon_->ProductCountryId, $updated_pcs)) {
                //             array_push($updated_pcs, (int) $addon_->ProductCountryId);
                //         }
                //     }

                //     foreach ($updated_pcs as $ppp) {
                //         array_push($updated_products_list_for_remove, (int) $ppp);
                //     }
                //     // $original_price = $subscription->pricePerCharge;
                //     // $updated_price = ProductCountry::whereIn('id', $updated_pcs)->select('sellPrice', 'id')->sum('sellPrice');

                //     foreach ($updates->plan_products->removed as $remove_id) {
                //         if (in_array((int) $remove_id, $subscription_product_addons_ids)) {
                //             SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->where('ProductCountryId', $remove_id)->take(1)->delete();
                //             // update Subscription price to reflect latest products
                //             // Subscriptions::where('id', $subscription->id)->update([
                //             //     "pricePerCharge" => $updated_price,
                //             //     "price" => $updated_price,
                //             // ]);
                //             // // add into subscription history
                //             // SubscriptionHistories::create([
                //             //     'message' => 'Modified Shave Plan',
                //             //     'detail' => '[Update Price] ' . $plan_country->currencyDisplay . $original_price . ' to ' . $plan_country->currencyDisplay . $updated_price,
                //             //     'subscriptionId' => $subscription->id,
                //             // ]);
                //         }
                //     }
                // }

                // // re-calculate price
                // $subscription_product_addons_ids_updated = [];
                // $subscription_product_addons_updated = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->get();

                // foreach ($subscription_product_addons_updated as $subscription_product_addon) {
                //     array_push($subscription_product_addons_ids_updated, (int) $subscription_product_addon->ProductCountryId);
                // }

                // if ($originalIsAnnual == 1) {
                //     // use new quantity if frequency has been changed
                //     if ($updates->frequency->isModified === true) {
                //         $original_products_addon_price = ProductCountry::whereIn('id', $subscription_product_addons_ids)->select('sellPrice', 'id')->sum('sellPrice');
                //         $modified_products_addon_price = (12 / (int) $updates->frequency->_modified->duration) * ProductCountry::whereIn('id', $subscription_product_addons_ids_updated)->select('sellPrice', 'id')->sum('sellPrice');
                //         $updated_price = (float) $updates->frequency->_modified->sellPrice;
                //         $updated_price = (float) $updated_price + (float) $modified_products_addon_price;
                //     } else {
                //         $original_products_addon_price =  $annualqty * ProductCountry::whereIn('id', $subscription_product_addons_ids)->select('sellPrice', 'id')->sum('sellPrice');
                //         $modified_products_addon_price =  $annualqty * ProductCountry::whereIn('id', $subscription_product_addons_ids_updated)->select('sellPrice', 'id')->sum('sellPrice');
                //         $updated_price = (float) $original_price - (float) $original_products_addon_price;
                //         $updated_price = (float) $updated_price + (float) $modified_products_addon_price;
                //     }
                // }

                // dd($updated_price, $subscription_product_addons_updated, $original_products_addon_price, $modified_products_addon_price);

                // // update Subscription price to reflect latest products
                // Subscriptions::where('id', $subscription->id)->update([
                //     "pricePerCharge" => number_format((float) $updated_price, 2, '.', ''),
                //     "price" => number_format((float) $updated_price, 2, '.', ''),
                // ]);
                // // add into subscription history
                // SubscriptionHistories::create([
                //     'message' => 'Modified Shave Plan',
                //     'detail' => '[Update Product & Price] ' . $plan_country->currencyDisplay . $original_price . ' to ' . $plan_country->currencyDisplay . number_format((float) $updated_price, 2, '.', ''),
                //     'subscriptionId' => $subscription->id,
                // ]);
            }

            // if shave plan frequency was modified
            if ($updates->frequency->isModified === true && $updates->plan_products->isModified !== true) {
                $updated_price = (float) 0;
                $modified_products_addon_price = (float) 0;
                // update subscription addons with new quantities
                $subscription_product_addons_check = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->get();
                if ($subscription_product_addons_check) {
                    SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)
                        ->where('isAnnual', 1)
                        ->where('cycle', 'all')
                        ->update([
                            "qty" => 12 / (int) $updates->frequency->_modified->duration
                        ]);
                }

                // re-calculate price
                $subscription_product_addons_ids_updated = [];
                $subscription_product_addons_updated = SubscriptionsProductAddOns::where('subscriptionId', $subscription->id)->get();
                if ($subscription_product_addons_updated) {
                    foreach ($subscription_product_addons_updated as $subscription_product_addon) {
                        array_push($subscription_product_addons_ids_updated, (int) $subscription_product_addon->ProductCountryId);
                    }
                    $modified_products_addon_price = (12 / (int) $updates->frequency->_modified->duration) * ProductCountry::whereIn('id', $subscription_product_addons_ids_updated)->select('sellPrice', 'id')->sum('sellPrice');
                }

                $updated_price = (float) $updates->frequency->_modified->sellPrice + (float) $modified_products_addon_price;

                Subscriptions::where('id', $subscription->id)->update([
                    "PlanId" => $updates->frequency->_modified->PlanId,
                    "pricePerCharge" => $updated_price,
                    "price" => $updated_price,
                ]);

                SubscriptionHistories::create([
                    'message' => 'Modified Shave Plan',
                    'detail' => '[Plan Update] ' . $updates->frequency->_original->description . ' to ' . $updates->frequency->_modified->description,
                    'subscriptionId' => $subscription->id,
                ]);

                SubscriptionHistories::create([
                    'message' => 'Modified Shave Plan',
                    'detail' => '[Update Product & Price] ' . $plan_country->currencyDisplay . $original_price . ' to ' . $plan_country->currencyDisplay . number_format((float) $updated_price, 2, '.', ''),
                    'subscriptionId' => $subscription->id,
                ]);

                $success["update_plan_frequency"]["status"] = true;
                if ($updates->frequency->_original->PlanId != $updates->frequency->_modified->PlanId) {
                    $currentCountry = json_decode(session()->get('currentCountry'), true);

                    $currentLocale = "";
                    if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
                        $currentLocale = json_decode(session()->get('currentCountry'), true)['defaultLang'];
                    } else {
                        $currentLocale = 'en';
                    }

                    $countryid = "";
                    if ($currentCountry['id']) {
                        $countryid = $currentCountry['id'];
                    } else if ($currentCountry) {
                        $countryid = 1;
                    }

                    $now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
                    $email = [];
                    $email['email'] = Auth::user()->email;
                    $email['currentdate'] = $now;
                    $email['subscriptionId'] = $subscription->id;
                    $email['userId'] = Auth::user()->id;
                    $email['countryId'] = $countryid;
                    $email['type'] = 1;
                    $email['isPlan'] = 1;

                    // Initiate New CustomerRegisteredEvent
                    // hide email redis
                    $this->emailQueueHelper->buildEmailTemplate('subscription-modified', $email, array($currentCountry, $currentLocale));
                }
            }

            // if shave plan frequency & products was modified [phase 2]
            if ($updates->frequency->isModified === true && $updates->plan_products->isModified === true) {
            }
            session()->forget('session_edit_shave_plans');

            return $success;
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function pauseSubscription(Request $request)
    {

        try {
            if ($request) {
                $pause_months = (int) $request->pause_months;
                $subscription_id = $request->subscription_id;

                $sub = Subscriptions::where('id', $subscription_id)->first();

                $update_sub = Subscriptions::findorfail($subscription_id);
                if ($sub->nextChargeDate !== null) {
                    $update_sub->nextChargeDate = Carbon::parse($sub->nextChargeDate)->addMonths($pause_months)->format('Y-m-d');
                }
                if ($sub->nextDeliverDate !== null) {
                    $update_sub->nextDeliverDate = Carbon::parse($sub->nextDeliverDate)->addMonths($pause_months)->format('Y-m-d');
                }
                // if ($sub->startDeliverDate !== null) {
                //     $update_sub->startDeliverDate = Carbon::parse($sub->startDeliverDate)->addMonths($pause_months)->format('Y-m-d');
                // }

                $update_sub->save();

                SubscriptionHistories::create([
                    'message' => 'Modified Shave Plan',
                    'detail' => '[Pause Plan] ' . $sub->nextChargeDate . ' to ' . Carbon::parse($sub->nextChargeDate)->addMonths($pause_months)->format('Y-m-d'),
                    'subscriptionId' => $sub->id,
                ]);
                PausePlanHistories::create([
                    'subscriptionIds' => $sub->id,
                    'originaldate' => $sub->nextChargeDate,
                    'resumedate' => Carbon::parse($sub->nextChargeDate)->addMonths($pause_months)->format('Y-m-d'),
                    'pausemonth' => $pause_months,
                ]);

                return "success";
            } else {
                return "Not Applicable";
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getPlansBasedOnBlade(Request $request)
    {
        $user_id = (int) $request->user_id;
        $subscription_id = (int) $request->subscription_id;
        $selected_blade_type_id = (int) $request->ProductCountryId;
        $selected_handle_type_id = (int) $request->handle_type;

        try {
            $blade_types = $handle_types = $bag_types = $addons = null;
            $blade_type = $handle_type = $bag_type = $addon_type = '';
            $available_frequencies = [];
            $filtered_planIds = [];
            $original_addon = [];
            if ($user_id && $subscription_id) {

                $u = User::where('id', $user_id)->first();
                $s = Subscriptions::where('id', $subscription_id)->first();
                $original_addons = SubscriptionsProductAddOns::where('subscriptionId', $subscription_id)->get();
                foreach ($original_addons as $oa) {
                    array_push($original_addon, (int) $oa->ProductCountryId);
                }

                $current_plan_info = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plans.PlanSkuId')
                    ->join('plan_categories', 'plan_categories.id', 'plan_category_details.PlanCategoryId')
                    ->where('plans.id', $s->PlanId)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId', 'plan_categories.id as PlanCategoryId')
                    ->first();
                $isAnnual = isset($current_plan_info->isAnnual) && $current_plan_info->isAnnual === 1 ? 1 : 0;
                $p_ctgry = 0;
                $p_drtns = [];
                if ($s->isCustom === 1 && $s->isTrial === 0) {
                    $p_ctgry = (int) $current_plan_info->PlanCategoryId;
                }
                if ($s->isTrial === 1 && $s->isCustom === 0) {
                    $p_ctgry = (int) $current_plan_info->PlanCategoryId;
                }

                $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                    ->get();

                foreach ($p_relateds as $plan) {
                    array_push($p_drtns, (int) $plan->duration);
                }

                $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

                $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plansku.isAnnual', $isAnnual)
                    ->where('plans.CountryId', $u->CountryId)
                    ->whereIn('plansku.duration', $available_frequencies)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        'plans.*',
                        'plansku.*'
                    )
                    ->get();
                $plans = (object) array();
                foreach ($allplans as $key => $plan) {
                    $plans->$key = (object) array();
                    $plans->$key->plan_info = $plan->PlanId;
                    $get_plan_details = PlanDetails::where('PlanId', $plan->PlanId)->get();
                    $same_blade_tk = false;
                    $same_handle_tk = false;
                    foreach ($get_plan_details as $selected_blade_filtered_plan) {
                        $plans->$key->plan_products = (object) array((int) $selected_blade_filtered_plan->ProductCountryId);
                        // filter using bladetype & handle type for trial plans
                        if ($p_ctgry == 1) {
                            if ((int) $selected_blade_filtered_plan->ProductCountryId === (int) $selected_blade_type_id) {
                                $same_blade_tk = true;
                            }
                            if ((int) $selected_blade_filtered_plan->ProductCountryId === (int) $selected_handle_type_id) {
                                $same_handle_tk = true;
                            }
                        }
                        // filter using bladetype for custom plans
                        if ($p_ctgry == 2 || $p_ctgry == 8) {
                            if (in_array((int) $selected_blade_type_id, (array) $plans->$key->plan_products)) {
                                array_push($filtered_planIds, (int) $selected_blade_filtered_plan->PlanId);
                            }
                        }
                    }
                    if ($p_ctgry == 1) {
                        if ($same_blade_tk === true && $same_handle_tk === true) {
                            array_push($filtered_planIds, (int) $selected_blade_filtered_plan->PlanId);
                        }
                    }
                }

                $filtered = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plansku.isAnnual', $isAnnual)
                    ->where('plans.CountryId', $u->CountryId)
                    ->whereIn('plansku.duration', $available_frequencies)
                    ->whereIn('plans.id', $filtered_planIds)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        'plans.*',
                        'plansku.*'
                    )
                    ->groupBy('plansku.id')
                    ->get();
                $available_plans = (object) array(
                    "blade_type" => $blade_type,
                    "filtered_plans" => $filtered_planIds,
                    "active_plans" => $filtered,
                    "blade_types" => $blade_types,
                    "handle_types" => $handle_types,
                    "bag_types" => $bag_types,
                    "addons" => $addons,
                );

                return json_encode($available_plans);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function returnReferralShare()
    {
        return view('social-share-template.referral_share');
    }

    public function checkmaskqty(Request $req)
    {
        $checkallmask =  $this->productHelper->getFreeMaskSelection($req->countryid, $req->lang);
        $checkmask = 0;
        if($checkallmask){
          foreach ($checkallmask as $m) {
              if($req->pcid == $m->pcid){
                  if($m->qty > 0){
                      $checkmask = 1;
                  }
              }
          }
         }
      return $checkmask;
    }
}
