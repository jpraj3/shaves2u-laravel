<?php

namespace App\Http\Controllers\Ecommerce\User;

use App\Helpers\AWSHelper;
use App\Helpers\Email\EmailQueueHelper;
use App\Helpers\Payment\Stripe;
use App\Helpers\Payment\StripeBA;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\User\DeliveryAddresses;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\File;
use App\Services\CountryService;
use App\Models\Reports\SalesReport;
use App\Models\Orders\Orders;
use App\Models\Subscriptions\Subscriptions;
use App\Models\CancellationJourney\CancellationJourneys;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderHistories;
use App\Models\Subscriptions\SubscriptionHistories;
use Carbon\Carbon;

class UserController extends Request
{
    public $country_service;
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        if ($this->country_info["country_id"] !== 8) {
            $this->stripe = new Stripe();
        }
        $this->awsHelper = new AWSHelper();
        $this->aws_folder_path = config('environment.aws.settings.display_picture');
        $this->emailQueueHelper = new EmailQueueHelper();
        $this->stripe_without_session = new StripeBA();
    }

    public function uploadImage(Request $request)
    {
        $user = User::findorfail(Auth::user()->id);
        $file = $request->file('file2');
        // Get the contents of the file
        $contents = $file->openFile()->fread($file->getSize());
        $user->user_profile_img = $contents;
        $user->save();
    }

    public function randomGenerator($number)
    {
        $rand = substr(uniqid('', true), $number);
        return $rand;
    }

    public function updateUserProfile(Request $request)
    {
        try {
            $user = Auth::check() === true ? Auth::user() : null;

            if ($request) {
                $first_name = '';
                $last_name = '';
                $_phoneext = '';
                $_phone = '';
                $_birthday = '';
                $_password = '';
                $display_image = '';
                $user_profile_update = User::findorfail($user->id);

                if (isset($request->file)) {
                    $file = $request->file('file');
                    $name = "";
                    if (!empty($request->file)) {
                        $file = $request->file;
                        $randg = $this->randomGenerator(-5);

                        $Aname = $request->pu_e_fullname . "_" . $randg . "_" . $this->awsHelper->__uuid();
                        $name = preg_replace('/\s+/', '', $Aname);
                        $extension = $file->getClientOriginalExtension();
                        $upload = $this->awsHelper->__putImg($file, $this->aws_folder_path, $name, $extension);

                        if ($upload !== false) {
                            $user_profile_update->user_profile_img = $upload["ObjectURL"];
                        }
                    }
                }

                if (isset($request->pu_e_fullname)) {
                    if ($request->pu_e_fullname == trim($request->pu_e_fullname)) {
                        $first_name = $request->pu_e_fullname;
                        $last_name = null;
                    } else {
                        $first_name = explode(' ', $request->pu_e_fullname)[0];
                        $last_name = explode(' ', $request->pu_e_fullname)[1];
                    }
                    $user_profile_update->firstName = $request->pu_e_fullname;
                    $user_profile_update->lastName = null;
                }

                if (isset($request->pu_e_email)) {
                    $new_email = trim($request->pu_e_email);
                    $oldUserEmail = Auth::user()->email;
                    if ($oldUserEmail !== $new_email) {
                        $checkUser = User::where('email', $new_email)->first();
                        if (!$checkUser) {
                            $checkSubs = Subscriptions::where('email', $oldUserEmail)->get();
                            $checkSalesReport = SalesReport::where('email', $oldUserEmail)->get();
                            $checkOrders = Orders::where('email', $oldUserEmail)->get();
                            $checkCancellationJourneys = User::where('email', $oldUserEmail)->get();
                            $update_user_email = !$checkUser ? User::where('email', $oldUserEmail)->update(["email" => $new_email]) : false;
                            $update_user_subscriptions = $checkSubs ? Subscriptions::where('email', $oldUserEmail)->update(["email" => $new_email]) : false;
                            $update_user_salesreport = $checkSalesReport ? SalesReport::where('email', $oldUserEmail)->update(["email" => $new_email]) : false;
                            $update_user_orders =  $checkOrders ? Orders::where('email', $oldUserEmail)->update(["email" => $new_email]) : false;
                            $update_user_cancellationjourneys = $checkCancellationJourneys ? CancellationJourneys::where('email', $oldUserEmail)->update(["email" => $new_email]) : false;

                            if ($update_user_email !== false) {
                                // send email to operation team
                                $emailData = [];
                                $currentCountry = json_decode(session()->get('currentCountry'), true);

                                $currentLocale = "";
                                $codeIso = "MY";
                                if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
                                    $currentLocale = json_decode(session()->get('currentCountry'), true)['defaultLang'];
                                } else {
                                    $currentLocale = 'en';
                                }

                                if (json_decode(session()->get('currentCountry'), true)['codeIso']) {
                                    $codeIso = json_decode(session()->get('currentCountry'), true)['codeIso'];
                                } else {
                                    $codeIso = 'MY';
                                }
                                $emailData['title'] = 'user-email-update';

                                $emailList = config('environment.email-blasting.user-email-update.' . strtoupper($codeIso));
                                if (isset($emailList)) {
                                    foreach ($emailList as $eL) {
                                        $emailData['moduleData'] = array(
                                            'email' => $eL,
                                            'oldEmail' =>  $oldUserEmail,
                                            'newEmail' => $new_email,
                                        );
                                        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                                        // hide email redis
                                        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                                    }
                                }
                            }

                            if ($update_user_subscriptions !== false) {
                                foreach ($checkSubs as $cS) {
                                    SubscriptionHistories::create([
                                        'message' => 'Email Update',
                                        'detail' => 'Updated user email from ' . $oldUserEmail . ' to ' . $new_email,
                                        'subscriptionId' => $cS->id,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ]);
                                }
                            }

                            if ($update_user_orders !== false) {
                                foreach ($checkOrders as $cO) {
                                    OrderHistories::create([
                                        'message' => 'Email Update',
                                        'isRemark' => 0,
                                        'OrderId' => $cO->id,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ]);
                                }
                            }
                        } else {
                            return 'email exists';
                        }
                    } else {
                        // do nothing
                    }
                }

                if (isset($request->pu_e_password)) {
                    $_password = md5($request->pu_e_password);
                    $user_profile_update->password = $_password;
                }

                if (isset($request->pu_e_phone)) {
                    if (strpos($request->pu_e_phoneext, '+') !== false) {
                        $_phone = $request->pu_e_phoneext . $request->pu_e_phone;
                    } else {
                        $_phone = '+' . $request->pu_e_phoneext . $request->pu_e_phone;
                    }
                    $user_profile_update->phoneHome = $_phone;
                    $user_profile_update->phone = $_phone;
                }

                if (isset($request->pu_e_birthday_day) && isset($request->pu_e_birthday_month) && isset($request->pu_e_birthday_year)) {
                    $_birthday = $request->pu_e_birthday_year . '' . $request->pu_e_birthday_month . '' . $request->pu_e_birthday_day;
                    $user_profile_update->birthday = $_birthday;
                }

                $user_profile_update->save();
                if (isset($request->pu_e_password)) {
                    $currentCountry = json_decode(session()->get('currentCountry'), true);

                    $currentLocale = "";
                    if (json_decode(session()->get('currentCountry'), true)['defaultLang']) {
                        $currentLocale = json_decode(session()->get('currentCountry'), true)['defaultLang'];
                    } else {
                        $currentLocale = 'en';
                    }
                    $email = [];
                    $email['email'] = $user_profile_update->email;
                    // Initiate New CustomerRegisteredEvent
                    // hide email redis
                    $this->emailQueueHelper->buildEmailTemplate('password-updated', $email, array($currentCountry, $currentLocale));
                }
                return User::findorfail($user->id);
            }
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function updateUserAddress(Request $request)
    {
        try {
            $user = Auth::check() === true ? Auth::user() : null;
            if ($request) {
                // delivery address added from profile page
                if (isset($request->form_data)) {
                    if (count($request->form_data) > 1) {
                        $added_addresses = [];
                        foreach ($request->form_data as $address) {
                            if ($address["adding_method"] === "new delivery") {

                                if ($user->defaultShipping === null && $user->defaultBilling === null) {
                                    $new_address = new DeliveryAddresses();
                                    $new_address->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address->lastName = null;
                                    $new_address->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address->contactNumber = Auth::user()->phone;
                                    $new_address->SSN = isset($address["pd_e_ssn_popup"]) ? $address["pd_e_ssn_popup"] : null;
                                    $new_address->state = $address["pd_e_state_popup"];
                                    $new_address->address = $address["pd_e_address_popup"];
                                    $pcode = "";
                                    if ($address["pd_e_portalCode_popup"] != null || $address["pd_e_portalCode_popup"] != "") {
                                        $pcode = $address["pd_e_portalCode_popup"];
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_address->portalCode = $pcode;
                                    $new_address->city = $address["pd_e_city_popup"];
                                    $new_address->isRemoved = 0;
                                    $new_address->isBulkAddress = 0;
                                    $new_address->CountryId = Auth::user()->CountryId;
                                    $new_address->UserId = Auth::user()->id;
                                    $new_address->save();

                                    $new_address_billing = new DeliveryAddresses();
                                    $new_address_billing->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address_billing->lastName = null;
                                    $new_address_billing->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address_billing->contactNumber = Auth::user()->phone;
                                    $new_address_billing->SSN = isset($address["pd_e_ssn_popup"]) ? $address["pd_e_ssn_popup"] : null;
                                    $new_address_billing->state = $address["pd_e_state_popup"];
                                    $new_address_billing->address = $address["pd_e_address_popup"];
                                    $pcode = "";
                                    if ($address["pd_e_portalCode_popup"] != null || $address["pd_e_portalCode_popup"] != "") {
                                        $pcode = $address["pd_e_portalCode_popup"];
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_address_billing->portalCode = $pcode;
                                    $new_address_billing->city = $address["pd_e_city_popup"];
                                    $new_address_billing->isRemoved = 0;
                                    $new_address_billing->isBulkAddress = 0;
                                    $new_address_billing->CountryId = Auth::user()->CountryId;
                                    $new_address_billing->UserId = Auth::user()->id;
                                    $new_address_billing->save();
                                    if ($user->defaultShipping === null) {
                                        User::where('id', Auth::user()->id)->update(["defaultShipping" => $new_address->id]);
                                    }
                                    if ($user->defaultBilling === null) {
                                        User::where('id', Auth::user()->id)->update(["defaultBilling" => $new_address_billing->id]);
                                    }
                                    return [
                                        "address" => [
                                            "delivery" => DeliveryAddresses::where('id', $new_address->id)->first(),
                                            "billing" => DeliveryAddresses::where('id', $new_address_billing->id)->first()
                                        ]
                                    ];
                                } else {

                                    $new_address = new DeliveryAddresses();
                                    $new_address->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address->lastName = null;
                                    $new_address->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_address->contactNumber = Auth::user()->phone;
                                    $new_address->SSN = isset($address["pd_e_ssn_popup"]) ? $address["pd_e_ssn_popup"] : null;
                                    $new_address->state = $address["pd_e_state_popup"];
                                    $new_address->address = $address["pd_e_address_popup"];
                                    $pcode = "";
                                    if ($address["pd_e_portalCode_popup"] != null || $address["pd_e_portalCode_popup"] != "") {
                                        $pcode = $address["pd_e_portalCode_popup"];
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_address->portalCode = $pcode;
                                    $new_address->city = $address["pd_e_city_popup"];
                                    $new_address->isRemoved = 0;
                                    $new_address->isBulkAddress = 0;
                                    $new_address->CountryId = Auth::user()->CountryId;
                                    $new_address->UserId = Auth::user()->id;

                                    $new_address->save();
                                    if (!($user->defaultShipping !== null && $user->defaultBilling !== null)) {
                                        User::where('id', Auth::user()->id)->update(["defaultShipping" => $new_address->id]);
                                    }
                                    $data = DeliveryAddresses::findorfail($new_address->id);

                                    array_push($added_addresses, (object) array("delivery" => $data));
                                }
                            } else if ($address["adding_method"] === "new billing") {
                                $new_address = new DeliveryAddresses();
                                $new_address->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                $new_address->lastName = null;
                                $new_address->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                $new_address->contactNumber = Auth::user()->phone;
                                $new_address->SSN = isset($address["pd_e_ssn_popup"]) ? $address["pd_e_ssn_popup"] : null;
                                $new_address->state = $address["pb_e_state_popup"];
                                $new_address->address = $address["pb_e_address_popup"];
                                $pcode = "";
                                if ($address["pb_e_portalCode_popup"] != null || $address["pb_e_portalCode_popup"] != "") {
                                    $pcode = $address["pb_e_portalCode_popup"];
                                } else {
                                    $pcode = "00000";
                                }
                                $new_address->portalCode = $pcode;
                                $new_address->city = $address["pb_e_city_popup"];
                                $new_address->isRemoved = 0;
                                $new_address->isBulkAddress = 0;
                                $new_address->CountryId = Auth::user()->CountryId;
                                $new_address->UserId = Auth::user()->id;

                                $new_address->save();
                                if (!($user->defaultShipping !== null && $user->defaultBilling !== null)) {
                                    User::where('id', Auth::user()->id)->update(["defaultBilling" => $new_address->id]);
                                }
                                $data = DeliveryAddresses::findorfail($new_address->id);

                                array_push($added_addresses, (object) array("billing" => $data));
                            }
                        }

                        return ["address" => $added_addresses];
                    } else if (count($request->form_data) == 1) {
                        switch ($request->form_data[0]->update_address_type) {
                            case "delivery":
                                if (isset($request->adding_method) && $request->adding_method === "new delivery") {
                                    // if user does not have existing shipment & billing info
                                    $new_delivery = new DeliveryAddresses();
                                    $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->lastName = null;
                                    $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->contactNumber = Auth::user()->phone;
                                    $new_delivery->SSN = isset($request->form_data[0]->pd_e_ssn_popup) ? $request->form_data[0]->pd_e_ssn_popup : null;
                                    $new_delivery->state = $request->form_data[0]->pd_e_state_popup;
                                    $new_delivery->address = $request->form_data[0]->pd_e_address_popup;
                                    $pcode = "";
                                    if ($request->form_data[0]->pd_e_portalCode_popup != null || $request->form_data[0]->pd_e_portalCode_popup != "") {
                                        $pcode = $request->form_data[0]->pd_e_portalCode_popup;
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_delivery->portalCode = $pcode;
                                    $new_delivery->city = $request->form_data[0]->pd_e_city_popup;
                                    $new_delivery->isRemoved = 0;
                                    $new_delivery->isBulkAddress = 0;
                                    $new_delivery->CountryId = Auth::user()->CountryId;
                                    $new_delivery->UserId = Auth::user()->id;
                                    $new_delivery->save();

                                    // update User with new defaultDelivery
                                    return ["address" => ["delivery" => DeliveryAddresses::findorfail($new_delivery->id), "billing" => null]];
                                }
                                break;
                        }
                    } else {
                        return 0;
                    }
                } else {
                    if (isset($request->update_address_type)) {
                        switch ($request->update_address_type) {
                            case "delivery":
                                if (isset($request->adding_method) && $request->adding_method === "new delivery") {
                                    // if user does not have existing shipment & billing info
                                    $new_delivery = new DeliveryAddresses();
                                    $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->lastName = null;
                                    $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->contactNumber = Auth::user()->phone;
                                    $new_delivery->SSN = $request->pd_e_ssn_popup ? $request->pd_e_ssn_popup : null;
                                    $new_delivery->state = $request->pd_e_state_popup;
                                    $new_delivery->address = $request->pd_e_address_popup;
                                    $pcode = "";
                                    if ($request->pd_e_portalCode_popup != null || $request->pd_e_portalCode_popup != "") {
                                        $pcode = $request->pd_e_portalCode_popup;
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_delivery->portalCode = $pcode;
                                    $new_delivery->city = $request->pd_e_city_popup;
                                    $new_delivery->isRemoved = 0;
                                    $new_delivery->isBulkAddress = 0;
                                    $new_delivery->CountryId = Auth::user()->CountryId;
                                    $new_delivery->UserId = Auth::user()->id;
                                    $new_delivery->save();
                                    return ["address" => ["delivery" => DeliveryAddresses::findorfail($new_delivery->id), "billing" => null]];
                                } else {
                                    // dd("abcd");
                                    // if user does not have existing shipment & billing info
                                    if (!($user->defaultShipping !== null && $user->defaultBilling !== null)) {

                                        $new_delivery = new DeliveryAddresses();
                                        $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_delivery->lastName = null;
                                        $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_delivery->contactNumber = Auth::user()->phone;
                                        $new_delivery->SSN = $request->pd_e_ssn ? $request->pd_e_ssn : null;
                                        $new_delivery->state = $request->pd_e_state;
                                        $new_delivery->address = $request->pd_e_address;
                                        $pcode = "";
                                        if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                            $pcode = $request->pd_e_portalCode;
                                        } else {
                                            $pcode = "00000";
                                        }
                                        $new_delivery->portalCode = $pcode;
                                        $new_delivery->city = $request->pd_e_city;
                                        $new_delivery->isRemoved = 0;
                                        $new_delivery->isBulkAddress = 0;
                                        $new_delivery->CountryId = Auth::user()->CountryId;
                                        $new_delivery->UserId = Auth::user()->id;
                                        $new_delivery->save();

                                        $new_billing = new DeliveryAddresses();
                                        $new_billing->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_billing->lastName = null;
                                        $new_billing->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_billing->contactNumber = Auth::user()->phone;
                                        $new_billing->SSN = $request->pd_e_ssn ? $request->pd_e_ssn : null;
                                        $new_billing->state = $request->pd_e_state;
                                        $new_billing->address = $request->pd_e_address;
                                        $pcode = "";
                                        if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                            $pcode = $request->pd_e_portalCode;
                                        } else {
                                            $pcode = "00000";
                                        }
                                        $new_billing->portalCode = $pcode;
                                        $new_billing->city = $request->pd_e_city;
                                        $new_billing->isRemoved = 0;
                                        $new_billing->isBulkAddress = 0;
                                        $new_billing->CountryId = Auth::user()->CountryId;
                                        $new_billing->UserId = Auth::user()->id;
                                        $new_billing->save();

                                        // update User with new defaultDelivery
                                        User::where('id', Auth::user()->id)->update(["defaultShipping" => $new_delivery->id, "defaultBilling" => $new_billing->id]);
                                        return ["address" => ["delivery" => DeliveryAddresses::findorfail($new_delivery->id), "billing" => DeliveryAddresses::findorfail($new_billing->id)]];
                                    } else {

                                        if ($user->defaultShipping == null || $user->defaultShipping === "") {
                                            $new_delivery = new DeliveryAddresses();
                                            $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                            $new_delivery->lastName = null;
                                            $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                            $new_delivery->contactNumber = Auth::user()->phone;
                                            $new_delivery->SSN = $request->pd_e_ssn ? $request->pd_e_ssn : null;
                                            $new_delivery->state = $request->pd_e_state;
                                            $new_delivery->address = $request->pd_e_address;
                                            $pcode = "";
                                            if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                                $pcode = $request->pd_e_portalCode;
                                            } else {
                                                $pcode = "00000";
                                            }
                                            $new_delivery->portalCode = $pcode;
                                            $new_delivery->city = $request->pd_e_city;
                                            $new_delivery->isRemoved = 0;
                                            $new_delivery->isBulkAddress = 0;
                                            $new_delivery->CountryId = Auth::user()->CountryId;
                                            $new_delivery->UserId = Auth::user()->id;
                                            $new_delivery->save();

                                            // update User with new defaultDelivery
                                            User::where('id', Auth::user()->id)->update(["defaultShipping" => $new_delivery->id]);
                                            return ["address" => ["billing" => null, "delivery" => DeliveryAddresses::findorfail($new_delivery->id)]];
                                        } else {

                                            $update_default_delivery = DeliveryAddresses::where('id', $user->defaultShipping)->first();
                                            if ($update_default_delivery === null) {
                                                $new_delivery = new DeliveryAddresses();
                                                $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                                $new_delivery->lastName = null;
                                                $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                                $new_delivery->contactNumber = Auth::user()->phone;
                                                $new_delivery->SSN = $request->pd_e_ssn ? $request->pd_e_ssn : null;
                                                $new_delivery->state = $request->pd_e_state;
                                                $new_delivery->address = $request->pd_e_address;
                                                $pcode = "";
                                                if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                                    $pcode = $request->pd_e_portalCode;
                                                } else {
                                                    $pcode = "00000";
                                                }
                                                $new_delivery->portalCode = $pcode;
                                                $new_delivery->city = $request->pd_e_city;
                                                $new_delivery->isRemoved = 0;
                                                $new_delivery->isBulkAddress = 0;
                                                $new_delivery->CountryId = Auth::user()->CountryId;
                                                $new_delivery->UserId = Auth::user()->id;
                                                $new_delivery->save();

                                                // update User with new defaultDelivery
                                                User::where('id', Auth::user()->id)->update(["defaultShipping" => $new_delivery->id]);
                                                return ["address" => ["billing" => null, "delivery" => DeliveryAddresses::findorfail($new_delivery->id)]];
                                            } else {
                                                $update_defaultdelivery = DeliveryAddresses::where('id', $user->defaultShipping)->first();
                                                if (isset($request->pd_e_ssn)) {
                                                    $update_defaultdelivery->SSN = $request->pd_e_ssn;
                                                }
                                                if (isset($request->pd_e_address)) {
                                                    $update_defaultdelivery->address = $request->pd_e_address;
                                                }
                                                if (isset($request->pd_e_city)) {
                                                    $update_defaultdelivery->city = $request->pd_e_city;
                                                }
                                                if (isset($request->pd_e_portalCode)) {
                                                    $pcode = "";
                                                    if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                                        $pcode = $request->pd_e_portalCode;
                                                    } else {
                                                        $pcode = "00000";
                                                    }
                                                    $update_defaultdelivery->portalCode = $pcode;
                                                } else {
                                                    $pcode = "";
                                                    if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                                        $pcode = $request->pd_e_portalCode;
                                                    } else {
                                                        $pcode = "00000";
                                                    }
                                                    $update_defaultdelivery->portalCode = $pcode;
                                                }

                                                if (isset($request->pd_e_state)) {
                                                    $update_defaultdelivery->state = $request->pd_e_state;
                                                }

                                                $update_defaultdelivery->save();
                                                return ["address" => ["delivery" => DeliveryAddresses::where('id', $update_defaultdelivery->id)->first(), "billing" => null]];
                                            }
                                        }
                                    }
                                }
                                break;

                            case "billing":
                                if ($user->defaultBilling == null || $user->defaultBilling === "") {
                                    $new_delivery = new DeliveryAddresses();
                                    $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->lastName = null;
                                    $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                    $new_delivery->contactNumber = Auth::user()->phone;
                                    $new_delivery->SSN = $request->pb_e_ssn ? $request->pb_e_ssn : null;
                                    $new_delivery->state = $request->pb_e_state;
                                    $new_delivery->address = $request->pb_e_address;
                                    $pcode = "";
                                    if ($request->pb_e_portalCode != null || $request->pb_e_portalCode != "") {
                                        $pcode = $request->pb_e_portalCode;
                                    } else {
                                        $pcode = "00000";
                                    }
                                    $new_delivery->portalCode = $pcode;
                                    $new_delivery->city = $request->pb_e_city;
                                    $new_delivery->isRemoved = 0;
                                    $new_delivery->isBulkAddress = 0;
                                    $new_delivery->CountryId = Auth::user()->CountryId;
                                    $new_delivery->UserId = Auth::user()->id;
                                    $new_delivery->save();

                                    // update User with new defaultDelivery
                                    User::where('id', Auth::user()->id)->update(["defaultBilling" => $new_delivery->id]);
                                    return ["address" => ["delivery" => null, "billing" => DeliveryAddresses::findorfail($new_delivery->id)]];
                                } else {
                                    $update_default_billing = DeliveryAddresses::where('id', $user->defaultBilling)->first();
                                    if ($update_default_billing === null) {
                                        $new_delivery = new DeliveryAddresses();
                                        $new_delivery->firstName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_delivery->lastName = null;
                                        $new_delivery->fullName = Auth::user()->firstName . '' . Auth::user()->lastName;
                                        $new_delivery->contactNumber = Auth::user()->phone;
                                        $new_delivery->SSN = $request->pb_e_ssn ? $request->pb_e_ssn : null;
                                        $new_delivery->state = $request->pb_e_state;
                                        $new_delivery->address = $request->pb_e_address;
                                        $pcode = "";
                                        if ($request->pb_e_portalCode != null || $request->pb_e_portalCode != "") {
                                            $pcode = $request->pb_e_portalCode;
                                        } else {
                                            $pcode = "00000";
                                        }
                                        $new_delivery->portalCode = $pcode;
                                        $new_delivery->city = $request->pb_e_city;
                                        $new_delivery->isRemoved = 0;
                                        $new_delivery->isBulkAddress = 0;
                                        $new_delivery->CountryId = Auth::user()->CountryId;
                                        $new_delivery->UserId = Auth::user()->id;
                                        $new_delivery->save();

                                        // update User with new defaultDelivery
                                        User::where('id', Auth::user()->id)->update(["defaultBilling" => $new_delivery->id]);
                                        return ["address" => ["delivery" => null, "billing" => DeliveryAddresses::findorfail($new_delivery->id)]];
                                    } else {
                                        if (isset($request->pb_e_address)) {
                                            $update_default_billing->address = $request->pb_e_address;
                                        }
                                        if (isset($request->pb_e_city)) {
                                            $update_default_billing->city = $request->pb_e_city;
                                        }
                                        if (isset($request->pb_e_portalCode)) {
                                            $pcode = "";
                                            if ($request->pb_e_portalCode != null || $request->pb_e_portalCode != "") {
                                                $pcode = $request->pb_e_portalCode;
                                            } else {
                                                $pcode = "00000";
                                            }
                                            $update_default_billing->portalCode = $pcode;
                                        } else {
                                            $pcode = "";
                                            if ($request->pb_e_portalCode != null || $request->pb_e_portalCode != "") {
                                                $pcode = $request->pb_e_portalCode;
                                            } else {
                                                $pcode = "00000";
                                            }
                                            $update_default_billing->portalCode = $pcode;
                                        }
                                        if (isset($request->pb_e_state)) {
                                            $update_default_billing->state = $request->pb_e_state;
                                        }

                                        $update_default_billing->save();
                                        return DeliveryAddresses::findorfail($update_default_billing->id);

                                        $new_billing = new DeliveryAddresses();

                                        // update User with new defaultBilling
                                        User::where('id', Auth::user()->id)->update(["defaultBilling" => $new_billing->id]);
                                        return ["address" => ["delivery" => null, "billing" => DeliveryAddresses::findorfail($new_billing->id)]];
                                    }
                                }
                                break;
                            default:
                                // do nothing
                        }
                    } else {
                        $c = $request->count;
                        $update_address_type = "update_address_type_" . $c;
                        $adding_method = "adding_method_" . $c;
                        $update_address_id = "update_address_id_" . $c;
                        $pd_e_address = "pd_e_address_" . $c;
                        $pd_e_city = "pd_e_city_" . $c;
                        $pd_e_portalCode = "pd_e_portalCode_" . $c;
                        $pd_e_state = "pd_e_state_" . $c;

                        if ($request->$update_address_type === 'delivery') {
                            $_getAddress = DeliveryAddresses::where('id', $request->$update_address_id)->first();

                            if ($_getAddress !== null) {
                                $pcode = "";
                                if ($request->pd_e_portalCode != null || $request->pd_e_portalCode != "") {
                                    $pcode = $request->pd_e_portalCode;
                                } else {
                                    $pcode = "00000";
                                }
                                DeliveryAddresses::where('id', $request->$update_address_id)->update([
                                    "address" => $request->$pd_e_address,
                                    "city" => $request->$pd_e_city,
                                    "portalCode" => $pcode,
                                    "state" => $request->$pd_e_state,
                                ]);

                                return ["address" => ["delivery" => DeliveryAddresses::findorfail($request->$update_address_id), "billing" => null]];
                            }
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function updateDefaultAddress(Request $request)
    {
        try {
            if ($request->type === 'delivery') {
                $_default = DeliveryAddresses::where('id', $request->delivery_id)->where('isRemoved', 0)->first();

                if (isset($_default) && $_default) {
                    $checkDefaults = User::where('id', $_default->UserId)->first();
                    if (isset($checkDefaults) && $checkDefaults) {
                        if ($checkDefaults->defaultBilling == null || $checkDefaults->defaultBilling == "") {
                            User::where('id', $_default->UserId)->update(["defaultShipping" => $_default->id, "defaultBilling" => $_default->id]);
                        } else {
                            User::where('id', $_default->UserId)->update(["defaultShipping" => $_default->id]);
                        }

                        $updated = User::where('id', $_default->UserId)->first();
                        return $updated;
                    }
                }

                return redirect()->back();
            }

            if ($request->type === 'billing') {
                $_default = DeliveryAddresses::where('id', $request->delivery_id)->where('isRemoved', 0)->first();

                if (isset($_default) && $_default !== "") {
                    $checkDefaults = User::where('id', $_default->UserId)->first();
                    if (isset($checkDefaults) && $checkDefaults) {
                        if ($checkDefaults->defaultShipping == null || $checkDefaults->defaultShipping == "") {
                            User::where('id', $_default->UserId)->update(["defaultShipping" => $_default->id, "defaultBilling" => $_default->id]);
                        } else {
                            User::where('id', $_default->UserId)->update(["defaultBilling" => $_default->id]);
                        }

                        $updated = User::where('id', $_default->UserId)->first();
                        return $updated;
                    }
                }

                return redirect()->back();
            }
        } catch (Exception $ex) {
            return redirect()->back()->withErrors($ex);
        }
    }

    public function updateDefaultCard(Request $request)
    {
        try {
            $_default = Cards::where('id', $request->card_id)->where('isRemoved', 0)->first();

            if (isset($_default) && $_default !== "") {
                Cards::where('UserId', $_default->UserId)->update(["isDefault" => 0]);
                Cards::where('id', $_default->id)->update(["isDefault" => 1]);
                $all_subs = Subscriptions::where('UserId', $_default->UserId)->get();
                if ($all_subs) {
                    foreach ($all_subs as $as) {
                        SubscriptionHistories::create([
                            'message' => 'Modified Shave Plan',
                            'detail' => '[Plan Update] Set Default Card via Profile Page to ' . $_default->branchName . ' <' . $_default->cardNumber . '>',
                            'subscriptionId' => $as->id,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);
                    }
                }
                $updated = Cards::where('id', $request->card_id)->first();
                return $updated;
            }

            return redirect()->back();
        } catch (Exception $ex) {
            return redirect()->back()->withErrors($ex);
        }
    }

    public function addNewCard(Request $request)
    {
        $user = Auth::user();
        $data = [];
        $data["user"] = $user;
        $data["add_card"] = (object) array(
            "name" => $user->firstName,
            "cvv" => (int) $request->cvv,
            "exp_month" => (int) $request->exp_month,
            "exp_year" => (int) $request->exp_year,
            "number" => $request->card_number,
        );

        $success = [];

        // Create customer card in stripe and get card token id
        $createdCard = $this->stripe->createTokenV2($data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $this->stripe->createCustomer(config('app.appType'), $data, $createdCard);
            if ($createdCustomer) {
                $retrieve_all_cards = Cards::where('UserId', $user->id)->get();
                if (!empty($retrieve_all_cards)) {
                    foreach ($retrieve_all_cards as $retrieve_all_card) {
                        $retrieve_all_card->isDefault = 0;
                        $retrieve_all_card->save();
                    }
                }

                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;
                $cards->isDefault = 1;
                $cards->payment_method = 'stripe';
                $cards->UserId = $user->id;
                $cards->isRemoved = 0;
                $cards->type = 'website';
                $cards->CountryId = $user->CountryId;
                $cards->save();


                // Temporarily Charge & Immediately Refund
                // $this->StripeChargeRefund($createdCustomer, $createdCard);

                if (isset($cards->id)) {
                    $success["customer"] = $createdCustomer;
                    $success["card"] = $createdCard;
                    $success["card_from_db"] = Cards::where('id', $cards->id)->first();

                    return $success;
                }
            }

            return 0;
        }
    }

    public function addNewCardAPI(Request $request)
    {
        $user = Auth::user();
        $data = [];
        $data["user"] = $user;
        $data["add_card"] = [
            "card_name" => $user->firstName,
            "card_cvv" => (int) $request->cvv,
            "card_expiry_month" => (int) $request->exp_month,
            "card_expiry_year" => (int) $request->exp_year,
            "card_number" => $request->card_number,
        ];
        $success = [];

        // Create customer card in stripe and get card token id
        $createdCard = $this->stripe_without_session->createTokenV3_API(config('app.appType'), $user->CountryId, $data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $this->stripe_without_session->createCustomer(config('app.appType'), $user->CountryId, $data, $createdCard);
            if ($createdCustomer) {
                $retrieve_all_cards = Cards::where('UserId', $user->id)->get();
                if (!empty($retrieve_all_cards)) {
                    foreach ($retrieve_all_cards as $retrieve_all_card) {
                        $retrieve_all_card->isDefault = 0;
                        $retrieve_all_card->save();
                    }
                }

                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;
                $cards->isDefault = 1;
                $cards->payment_method = 'stripe';
                $cards->UserId = $user->id;
                $cards->isRemoved = 0;
                $cards->type = 'website';
                $cards->CountryId = $user->CountryId;
                $cards->save();


                // Temporarily Charge & Immediately Refund
                // $this->StripeChargeRefund($createdCustomer, $createdCard);

                if (isset($cards->id)) {
                    $success["customer"] = $createdCustomer;
                    $success["card"] = $createdCard;
                    $success["card_from_db"] = Cards::where('id', $cards->id)->first();

                    return $success;
                }
            }

            return 0;
        }
    }

    public function StripeChargeRefund($createdCustomer, $createdCard)
    {
        $user = Auth::user();
        $country = Countries::where('id', $user->CountryId)->first();
        $type = 'ecommerce';
        $chargeObjects = [
            "amount" => 1,
            "currency" => strtolower($country->currencyCode),
            "customer" => $createdCustomer["customer"]["id"],
            "description" => "[" . strtoupper($country->code) . "][" . $type . "] Initial charge for " . $user->email,
            "metadata" => "Intial Charge for OTP",
            "receipt_email" => $user->email,
        ];
        // Charge the Customer instead of the card:
        $charge = $this->stripe->createCharge($chargeObjects, strtoupper($country->code), $type, $createdCard, $createdCustomer);

        // dd($charge);
    }

    public function deleteCard(Request $request)
    {
        try {
            if ($request) {
                Cards::where('id', $request->card_id)->update(["isRemoved" => 1, "isDefault" => 0]);
                return "success";
            }
        } catch (Exception $ex) {
            return $ex;
        }
    }
}
