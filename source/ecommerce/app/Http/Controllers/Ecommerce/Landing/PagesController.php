<?php

namespace App\Http\Controllers\Ecommerce\Landing;

use App;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\SubscriptionHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Plans\CustomPlanController as CustomPlan;
use App\Http\Controllers\Globals\Plans\TrialPlanController as TrialPlan;
use App\Http\Controllers\Globals\Products\ASKController as ASK;
use App\Models\GeoLocation\Countries;
use App\Models\Products\ProductType;
use App\Models\Subscriptions\Subscriptions;
// Models
use App\Services\CountryService as CountryService;
use Illuminate\Support\Facades\Auth;

// Helpers
use OpenGraph;
use SEOMeta;

class PagesController extends Controller
{
    public $productHelper;
    public $planHelper;
    public $country_service;

    public function __construct()
    {
        $this->TrialPlan = new TrialPlan();
        $this->CustomPlan = new CustomPlan();
        $this->ASK = new ASK();
        $this->productHelper = new ProductHelper();
        $this->planHelper = new PlanHelper();
        $this->country_service = new CountryService();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->subscriptionHelper = new SubscriptionHelper();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }

    public function shavePlansView()
    {
        return view('plans.shave-plans');
    }

    public function trialPlanView()
    {

        return view('plans.trial.trial-plan');
    }

    public function trialPlanSelectionView()
    {
        //Get all products from Trial Plan Controller
        $free_shave_cream_id = $this->TrialPlan->getFreeShaveCream_ProductCountriesId();
        $handle_products = $this->TrialPlan->getHandleList();
        $blade_products = $this->TrialPlan->getBladeList();
        $frequency_list = $this->TrialPlan->getFrequencyList();
        $addon_products = $this->TrialPlan->getAddonList();
        $allplan = $this->TrialPlan->getAllTrialPlan();
        $country_id = $this->country_info['country_id'];
        $trial_price = $this->TrialPlan->getTrialPlanDisplayPrice();
        $trial_saving_price = $this->TrialPlan->getTrialPlanDisplaySavingPrice();
        $currency = $this->country_service->getCurrencyDisplay();
        $next_refill_date = $this->TrialPlan->getNextRefillDate();
        $plan_type_based_on_handle = [];
        $handle_based_on_country = "";
        $selected_trial_plan_type = "";
        if (isset($allplan) && $allplan !== null) {
            foreach ($allplan as $ap) {
                foreach ($ap["productCountryId"] as $pi) {
                    if (!in_array((int) $pi, $plan_type_based_on_handle)) {
                        array_push($plan_type_based_on_handle, (int) $pi);
                    }
                }
            }
        }

        if (count($plan_type_based_on_handle) > 0 && $handle_products !== null) {
            foreach ($handle_products as $hp) {
                if (in_array((int) $hp["productcountriesid"], $plan_type_based_on_handle)) {
                    $handle_based_on_country = $hp;
                 }
            }
        }

        $user_id = null;
        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }

        $locale = App::getLocale();
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);

        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"], '?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage)
        ];

        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        if (strtoupper(strtoupper($locale)) === 'EN' && $country['id'] == 1) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-EN-MY.jpg'));
        } else if (strtoupper(strtoupper($locale)) === 'EN' && $country['id'] == 2) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-EN-HK.jpg'));
        } else if (strtoupper(strtoupper($locale)) === 'EN' && $country['id'] == 7) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-EN-SG.jpg'));
        } else if (strtoupper($locale) === 'KO' && $country['id'] == 8) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-KO-KR.jpg'));
        } else if (strtoupper($locale) === 'ZH-HK' && $country['id'] == 2) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-ZH-HK.jpg'));
        } else if (strtoupper($locale) === 'ZH-TW' && $country['id'] == 9) {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-ZH-TW.jpg'));
        } else {
            OpenGraph::addImage(asset('images/common/referral_share/S2U-Referal-Image-EN-MY.jpg'));
        }

        session()->forget('stripe_payment_fail');
        return view('plans.trial.trial-plan-selection', compact(
            "free_shave_cream_id",
            "handle_products",
            "blade_products",
            "frequency_list",
            "addon_products",
            "allplan",
            "country_id",
            "currency",
            "trial_price",
            "trial_saving_price",
            "next_refill_date",
            "user_id",
            "plan_type_based_on_handle",
            "handle_based_on_country"
        ));
    }

    public function customPlanView()
    {
        $this->locale = App::getLocale();

        $sessionCountryData = session()->get('currentCountry');
        if (!empty($sessionCountryData)) {
            $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        } else {
            redirect()->route('locale.index');
        }

        $this->country = json_decode(session()->get('currentCountry'), true);
        $country_id = $this->country['id'];
        $currentCountryIso = strtoupper($this->country['codeIso']);

        session()->forget('stripe_payment_fail');
        // check for supportedlangs
        $this->_supportedLangs = $this->country_service->supportedLangs($country_id);
        //$langCode = $this->_supportedLangs ? $this->locale : $this->country['defaultLang'];
        $langCode = strtoupper(json_decode($sessionCountryData, true)['urlLang']);
        return view('plans.custom.custom-plan', compact("currentCountryIso", "langCode"));
    }

    public function customPlanSelectionView()
    {

        $this->locale = App::getLocale();

        $sessionCountryData = session()->get('currentCountry');
        if (!empty($sessionCountryData)) {
            $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        } else {
            redirect()->route('locale.index');
        }

        $this->country = json_decode(session()->get('currentCountry'), true);
        $country_id = $this->country['id'];
        $currentCountryIso = strtoupper($this->country['codeIso']);

        $langCode = $this->locale;
        $urllangCode = strtoupper($this->country['urlLang']);
        // products initialize
        //$product_type_id = ProductType::where('name', 'LIKE', '%.config('global.'.config("app.appType").'.CustomPlanGroup').%')->select('id')->first();
        $product_type_id = ProductType::where('name', 'LIKE', '%' . config('global.' . config("app.appType") . '.CustomPlan.CustomPlanGroup') . '%')->select('id')->get()->toArray();
        $data = array();
        //$data["ProductTypeId"] = $product_type_id["id"];
        $data["ProductTypeId"] = $product_type_id;
        $plancategories = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanGroup');
        $plantype = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanType');
        $product = $this->productHelper->getProductByProductType($country_id, $langCode, $data);
        $allplan = $this->planHelper->getPlanIdPriceByCategory($country_id, $langCode, $plantype, $plancategories);
        $frequency_list = $this->CustomPlan->getFrequencyList();
        $handle_products = array();
        $blade_products = array();
        $bag_products = array();
        $addon_products = array();
        $free_shave_cream_id = "";
        //$frequency_list = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanDuration');
        $getshippingfee = $this->country_service->shippingFee($country_id, $urllangCode);
        $shippingfee = "";
        $currency = $this->country_service->getCurrencyDisplay();
        $locale = App::getLocale();
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"], '?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);

        if ($getshippingfee) {
            $shippingfee = $getshippingfee["shippingfee"];
        }
        foreach ($product as $p) {
            if (strpos(strtoupper($p['name']), config('global.' . config("app.appType") . '.CustomPlan.CustomPlanHandle')) !== false) {
                array_push($handle_products, $p);
            } else if (strpos(strtoupper($p['name']), config('global.' . config("app.appType") . '.CustomPlan.CustomPlanBlade')) !== false) {
                array_push($blade_products, $p);
            } else if (strpos(strtoupper($p['name']), config('global.' . config("app.appType") . '.CustomPlan.CustomPlanBag')) !== false) {
                array_push($bag_products, $p);
            } else if (strpos(strtoupper($p['name']), config('global.' . config("app.appType") . '.CustomPlan.CustomPlanAddOn')) !== false) {
                array_push($addon_products, $p);
                if ($p['sku'] == config('global.' . config("app.appType") . '.ShaveCream')) {
                    $free_shave_cream_id = $p['productcountriesid'];
                }
            }
        }
        session()->forget('stripe_payment_fail');
        $keys = array_column($addon_products, 'sku');
        array_multisort($keys, SORT_DESC, $addon_products);
        return view('plans.custom.custom-plan-selection', compact("country_id", "langCode", "urllangCode", "currency", "shippingfee", "handle_products", "blade_products", "bag_products", "addon_products", "frequency_list", "free_shave_cream_id", "allplan"));
    }

    // Awesome Shave Kits General Page
    public function ASK()
    {
        $featured = $this->ASK->get();
        krsort($featured);
        $langCode = $this->lang_code;
        $countryIso = $this->country_info["country_iso"];
        $currency = $this->country_service->getCurrencyDisplay();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
        $url_parameters = session()->get('url_parameters');


        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtoupper(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        $CountryId = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountry = Countries::where('id', $CountryId)->first();
        $locale = App::getLocale();
        $country_id = $this->country_info['country_id'];
        $country = Countries::where('id', $country_id)->first();
        $currentPage = strtok($_SERVER["REQUEST_URI"], '?');
        $options = [
            "CountryId" => $country['id'],
            "langCode" => strtoupper($locale),
            "pageUrl" => str_replace("/" . strtolower($urllangCode) . "-" . strtolower($currentCountryIso), "", $currentPage)
        ];
        $meta = \App\Helpers\MetaTagHelper::getMetaTag(\App\Helpers\LaravelHelper::ConvertArraytoObject($options));
        // Set Meta Information
        if($meta){
        SEOMeta::setTitle($meta->metatitle);
        SEOMeta::setDescription($meta->metaDescription);
        // Set og Meta Info
        OpenGraph::setDescription($meta->metaDescription);
        OpenGraph::setTitle($meta->metatitle);
        OpenGraph::setUrl(isset($_SERVER['HTTPS']) ? 'https://' . $_SERVER['HTTP_HOST'] . $currentPage : 'http://' . $_SERVER['HTTP_HOST'] . $currentPage);
        }
        // old
        session()->forget('stripe_payment_fail');
        // return view('products.awesome-shave-kits.awesome-shave-kits', compact('featured', 'countryIso', 'langCode','url_parameters'));
        return view('products.product.ask', compact('featured', 'countryIso', 'langCode', 'url_parameters', 'currency'));
    }

    public function productAddons()
    {
        // Get Locale and Country Info
        $locale = App::getLocale();
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        // If User is logged in
        if (Auth::check()) {
            // Get Users latest subscription Info
            $latest_sub = $this->subscriptionHelper->RetrieveLatestSubsInfo(Auth::user()->id);

            // If latest subscription info is null
            if (is_null($latest_sub->latest_sub)) {
                // Redirect to Shave Plans Page
                return redirect()->route('locale.region.authenticated.savedplans', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            } else {
                // Else redirect to Edit Shave Plan Page
                return redirect()->route('locale.region.authenticated.savedplans.edit', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso, 'subscriptionId' => $latest_sub->latest_sub->SubscriptionId]);
            }
        } else {
            // Prompt Login
            return redirect()->route('login', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        }
    }

    public function productAddonsGetSubsList()
    {
        // Get Locale and Country Info
        if (Auth::check()) {
            // Get Users latest subscription Info
            $subs = $this->subscriptionHelper->RetrieveAllSubsInfoV2(Auth::user()->id);
            return ["status" => true, "data" => $subs];
        } else {
            return ["status" => false, "data" => null];
        }
    }

    public function productAddonsSelectedSubs($langCode, $country, $subscription_id)
    {
        // Get Locale and Country Info
        $locale = App::getLocale();
        $sessionCountryData = session()->get('currentCountry');
        $currentCountryIso = strtolower(json_decode($sessionCountryData, true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $url = route('locale.region.authenticated.savedplans.edit', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso, 'subscriptionId' => $subscription_id]);

        // dd($url);
        return $url;
    }
}
