<?php

namespace App\Http\Controllers\Globals\API;

use App\Http\Controllers\Controller as Controller;
use App\Models\GeoLocation\Countries;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        return Countries::all();
    }

}
