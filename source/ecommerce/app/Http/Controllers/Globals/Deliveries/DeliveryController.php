<?php

namespace App\Http\Controllers\Globals\Deliveries;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

use Illuminate\Foundation\Application;
use Config;
use App;
use App\Http\Controllers\Controller as Controller;

class DeliveryController extends Controller
{

    public function __construct(Application $app, Request $request)
    {
        $this->middleware(['locale','auth']);
        $this->app = $app;
        $this->request = $request;
        $this->isLoggedIn = Auth::check();
    }

    public function index()
    {
        $response = "";
        return view('test.deliveries', compact('response', $response));
    }

    public function sendDelivery()
    {
        // $client = new Client(["base_uri" => "https://staging-ims.urbanfox.asia"]);
        // $headers = [
        //     'X-CSRF-Token' => csrf_token(),
        //     'Content-Type' => 'application/json',
        //     'Authorization' => 'Bearer ' . "Ge48LNorBkfu/Dre0vzrQEzmy34KH/C4hN6qq+NFtGA="
        // ];
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json',
            'Authorization' => ['Bearer ' . "Ge48LNorBkfu/Dre0vzrQEzmy34KH/C4hN6qq+NFtGA="]]
        ]);
        //dummy JSON
        
        // $transaction = [
        //     'referenceNumber1' => "123456", //rn1, 
        //     'remarks' => "Test", //remarks, 
        //     'shippingAddress' => [
        //         'name' => "Test", //shipName, 
        //         'addressLine1' => "Test", //shipAddress,
        //         'city' => "Test", //shipCity,
        //         'country' => "Test", //shipCountry,
        //         'postalCode' => "Test", //shipPCode,
        //         'phone' => "12456789" //shipPNumber
        //     ],
        //     'billingAddress' => [
        //         'name' => "Test", //billName,
        //         'addressLine1' =>"Test", //billAddress,
        //         'city' => "Test", //billCity,
        //         'country' => "Test", //billCountry,
        //         'postalCode' => "Test", //billPCode,
        //         'phone' => "12456789" //billPNumber
        //     ],
        //     'customerAddress' => [
        //         'name' => "Test", //custName,
        //         'addressLine1' => "Test", //custAddress,
        //         'city' => "Test", //custCity,
        //         'country' => "Test", //custCountry,
        //         'postalCode' => "Test", //custPCode,
        //         'phone' => "12456789" //custPNumber
        //     ],
        //     'orderItems' => [
        //         'sku' => "Test", //sku,
        //         'quantity' => 1 //quantity
        //     ]
        // ];

        // $params = [
        //     'mutation' => [
        //         'createOrder' => [
        //             '__args' => $transaction,
        //             'id' => true, 
        //             'status' => true
        //         ]
        //     ]
        // ]; 

        $basic= ["test" => "test"];

        $query = "mutation _ { 
            createOrder( 
                referenceNumber1: '123458', 
                remarks: 'Test', 
                    shippingAddress: { 
                        name: 'Test', 
                        addressLine1: 'Test', 
                        city: 'Test', 
                        country: 'Test', 
                        postalCode: 'Test',
                        phone: '123456789'
                    },
                    billingAddress: {
                        name: 'Test', 
                        addressLine1: 'Test', 
                        city: 'Test', 
                        country: 'Test', 
                        postalCode: 'Test',
                        phone: '123456789'
                    },
                    customerAddress: {
                        name: 'Test', 
                        addressLine1: 'Test', 
                        city: 'Test', 
                        country: 'Test', 
                        postalCode: 'Test',
                        phone: '123456789'
                    },
                orderItems: {
                    sku: 'Test',
                    quantity: 0
                    }
            ) {
              id
            } 
        }";

        $transaction = [
        'query'=>'mutation _ {  createOrder(  referenceNumber1: "123462", remarks: "Test", shippingAddress: { name: "Test",  addressLine1: "Test",  city: "Test",  country: "Test", postalCode: "Test", phone: "123456789"}, billingAddress: { name: "Test", addressLine1: "Test", city: "Test", country: "Test", postalCode: "Test", phone: "123456789" },customerAddress: {name: "Test", addressLine1: "Test", city: "Test", country: "Test", postalCode: "Test",phone: "123456789"},orderItems: {sku: "Test", quantity: 1}) { id } }'
    ];

        $options = [
            //'headers' => $headers,
            'form_params' => [
                'json' => $basic
            ]
        ]; 
        try {
        $iamporttokenjson = json_encode($transaction);
        $response = $client->post("https://staging-ims.urbanfox.asia/graphiql", ['body' => $iamporttokenjson]);
    } catch (\Exception $e) {
        $errorlist = [
            "data" => $e->getMessage(),
            "status" => "error",
        ];
        return $errorlist;

    }
        $result = $response->getBody()->getContents();
        return view('test.deliveries', compact('result', $result));
    }
}
