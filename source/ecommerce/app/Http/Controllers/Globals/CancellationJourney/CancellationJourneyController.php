<?php

namespace App\Http\Controllers\Globals\CancellationJourney;

// Controllers & Services
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Plans\TrialPlanController;
use App\Http\Controllers\Globals\Utilities\SessionController;
use DB;

// Models
use App\Models\CancellationJourney\CancellationReasonTranslates;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Plans\PlanDetails;
use App\Models\Products\ProductCountry;
use App\Models\Products\Product;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\CancellationJourney\CancellationJourneys;
use App\Models\Plans\PlanTranslates;
use App\Models\CancellationJourney\CancellationFollowUp;
use App\Models\GeoLocation\Countries;

// Helpers
use Illuminate\Http\Request;
use App\Services\CountryService as CountryService;
use App\Helpers\SubscriptionHelper;
use \App\Helpers\UserHelper;
use Lang;
use Carbon\Carbon;
use App\Queue\SendEmailQueueService as EmailQueueService;

class CancellationJourneyController extends Controller
{

    // Constructor
    public function __construct()
    {
        $this->countryService = new CountryService();
        $this->langCode = strtoupper(app()->getLocale());
        $this->countryInfo = $this->countryService->getCountryAndLangCode();
        $this->countryId = $this->countryInfo['country_id'];
        $this->subscriptionHelper = new SubscriptionHelper();
        $this->sessionController = new SessionController();
        $this->userHelper = new UserHelper;
    }

    // Function: Main navigation for cancellation journey
    public function Navigate(Request $request)
    {
        // Get request
        $subscriptionId = $request->subscriptionid;
        $cancellationReasonId = $request->cancellationreasonid;
        $view = $request->view;

        // Initialize controllers and helpers
        $this->trialPlanController = new TrialPlanController();

        // Initialize data object
        $data = (Object) array();
        $data->country = $this->countryInfo['data_from_db'];
        $data->subscription = $this->GetSubscriptionData($subscriptionId);
        $data->currentPlanData = $this->subscriptionHelper->GetCurrentPlan($subscriptionId);

        // Get cancellation reason translates if cancellation reason id is more than 0
        if($cancellationReasonId > 0)
        {
            $data->cancellationReasonId = $cancellationReasonId;
            $data->cancellationReason = $this->GetCancellationReasonTranslate($cancellationReasonId);
        }

        // Determine whether the current plan is on max duration available
        $currentPlanFrequency = intval($data->currentPlanData->current_subscription_duration);
        $data->maxPlanFrequency = intval($this->GetMaxPlanFrequency($data->currentPlanData->types_of_plan_frequency));
        $data->hasMaximumPlanFrequency = false;
        if($currentPlanFrequency === $data->maxPlanFrequency)
        {
            $data->hasMaximumPlanFrequency = true;
        }

        // Get status of cancellation benefits
        $data->cancellationBenefitStatus = $this->GetCancellationBenefitStatus($subscriptionId);

        // Call specific method based on view name
        return $this->$view($view, $data);
    }

    // Modal: Test
    public function test($view, $data) {
        $test = $this->GetCancellationReasonTranslate(0);
    }

    // Modal: Root
    public function root($view, $data) {
        return $this->ReturnView($view,$data);
    }

    // Modal: Main Selection Page
    public function main($view, $data) {

        // Get all cancellation reasons
        $data->cancellationReasons = json_decode(CancellationReasonTranslates::where('countryId',$this->countryId)
        ->where('langCode',$this->langCode)
        ->where('isHidden',0)
        ->where('modalLevel',0)
        ->where('isHidden',0)
        ->orderBy('orderNumber', 'asc')
        ->get()
        ->toJson());

        // Get next page cancellation for other reason
        $data->nextPageCancellation = "";
        foreach ($data->cancellationReasons as $reason) {
            if($reason->viewName === "option5")
            {
                $data->nextPageCancellation = $reason;
            }
        }

        return $this->ReturnView($view,$data);
    }

    // Modal: Option 1
    public function option1($view, $data) {

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        return $this->ReturnView($view,$data);
    }

    // Modal: Option 2
    public function option2($view, $data) {

        // Get root cancellation reason
        $data->rootCancellationReason = $data->cancellationReason;

        // Get all cancellation reasons
        $data->cancellationReasons = json_decode(CancellationReasonTranslates::where('countryId',$this->countryId)
        ->where('langCode',$this->langCode)
        ->where('isHidden',0)
        ->where('modalLevel',1)
        ->where('isHidden',0)
        ->where('parentId',$data->cancellationReasonId)
        ->orderBy('orderNumber', 'asc')
        ->get()
        ->toJson());

        // Get all cancellation reasons
        $data->mainCancellationReasons = json_decode(CancellationReasonTranslates::where('countryId',$this->countryId)
        ->where('langCode',$this->langCode)
        ->where('isHidden',0)
        ->where('modalLevel',0)
        ->where('isHidden',0)
        ->orderBy('orderNumber', 'asc')
        ->get()
        ->toJson());

        // Get next page cancellation for other reason
        $data->nextPageCancellation = "";
        foreach ($data->mainCancellationReasons as $reason) {
            if($reason->viewName === "option5")
            {
                $data->nextPageCancellation = $reason;
            }
        }

        return $this->ReturnView($view,$data);
    }

    // Modal: Option 2.1
    public function option2_1($view, $data) {

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        return $this->ReturnView('option2.'.$view,$data);
    }

    // Modal: Option 2.2
    public function option2_2($view, $data) {

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        return $this->ReturnView('option2.'.$view,$data);
    }

    // Modal: Option 3
    public function option3($view, $data) {

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        // Get current plan frequency
        $data->currentPlanFrequency = intval($data->currentPlanData->current_subscription_duration);

        // Get all plan frequency list
        $data->planFrequencyList = $this->GetPlanFrequencyList($data->currentPlanFrequency, $data);

        // Get maximum plan frequency
        $data->maximumPlanFrequency = $this->GetMaxPlanFrequency($data->planFrequencyList);

        if($data->currentPlanFrequency === $data->maximumPlanFrequency) {
            return $this->ReturnView($view.'_pause',$data);
        } else {
            return $this->ReturnView($view,$data);
        }

    }

    // Modal: Option 4
    public function option4($view, $data) {

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        // Get current plan frequency
        $data->currentPlanFrequency = intval($data->currentPlanData->current_subscription_duration);

        // Get all plan frequency list
        $data->planFrequencyList = $this->GetPlanFrequencyList($data->currentPlanFrequency, $data);

        // Get maximum plan frequency
        $data->maximumPlanFrequency = $this->GetMaxPlanFrequency($data->planFrequencyList);

        if($data->currentPlanFrequency === $data->maximumPlanFrequency) {
            return $this->ReturnView($view.'_pause',$data);
        } else {
            return $this->ReturnView($view,$data);
        }
    }

    // Modal: Option 5
    public function option5($view, $data) {

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        return $this->ReturnView($view,$data);
    }

    // Modal: Option 6
    public function option6($view, $data) {

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        return $this->ReturnView($view,$data);
    }

    // Modal: Option 7
    public function option7($view, $data) {

        // Get current blade product country id
        $currentBladeId = $data->currentPlanData->available_plans->blade_type["ProductCountryId"];

        // Get all blade list
        $data->bladeList = $this->GetBladeList($currentBladeId, $data);

        // Get cancellation discount
        $data->cancellationDiscount = $this->GetCancellationDiscount();

        return $this->ReturnView($view,$data);
    }

    public function ReturnView($view, $data) {
        // Redirect to specific cancellation journey page
        return view('_authenticated.cancellation-journey-modal.'.$view)
        ->with('data', $data);
    }


    //***********************  Additional Functions for Cancellation Journey ***********************//
    // Function: Get cancellation reason translate
    public function GetCancellationReasonTranslate($cancellationReasonId)
    {
        $cancellationReason = json_decode(CancellationReasonTranslates::where('id',$cancellationReasonId)->first()->ToJson());
        return $cancellationReason;
    }

    // Function: Get subscription data
    public function GetSubscriptionData($subscriptionId) {
        $subs = json_decode(Subscriptions::where('id',$subscriptionId)->first()->ToJson());
        return $subs;
    }

    // Function: Get maximum plan frequency
    public function GetMaxPlanFrequency($planFrequencyList) {

        $highestDuration = 0;
        foreach ($planFrequencyList as $frequency) {
            $duration = $frequency['duration'];
            if($duration > $highestDuration) {
                $highestDuration = $duration;
            }
        }
        return $highestDuration;
    }

    // Function: Get cancellation discount
    public function GetCancellationDiscount(){
        return config('global.all.cancellation.discount');
    }

    // Function: Get list of blade
    public function GetBladeList($currentBladeId, $data) {
        $bladeList = [];
        $bladeList = $data->currentPlanData->available_plans->blade_types;
        foreach ($bladeList as &$blade) {
            if($blade['ProductCountryId'] === $currentBladeId) {
                $data->currentBlade = $blade;
            }
            $bladeNumber = config('global.all.blade_no.'.$blade['sku']);
            $blade['productTranslatesName'] =  $bladeNumber . ' ' . Lang::get('website_contents.blade_packV2');
            $blade['productDefaultName'] = $bladeNumber . ' Blade';
            $blade['product_default_image'] = asset(config('global.ecommerce.cancellation.blades_image_url.' . $bladeNumber));
        }
        return $bladeList;
    }

    // Function: Get plan frequency list
    public function GetPlanFrequencyList($currentFrequencyId, $data) {
        $frequencyList = [];
        $frequencyList = $data->currentPlanData->types_of_plan_frequency;
        foreach ($frequencyList as &$frequency) {
            $frequency['duration'] = intval($frequency['duration']);
            if($frequency['duration']=== $currentFrequencyId) {
                $data->currentFrequency = $frequency;
            }
            $frequency['durationTranslateName'] = strval($frequency['duration']) . ' ' . Lang::get('website_contents.Months');
            $frequency['durationDefaultName'] = strval($frequency['duration']) . ' ' . Lang::get('website_contents.Months');
        }
        usort($frequencyList, function($a, $b) {
            return $a['duration'] <=> $b['duration'];
        });
        return $frequencyList;
    }

    // API: Check status for cancellation benefits
    public function GetCancellationBenefitStatusAPI(Request $request)
    {
        return GetCancellationBenefitStatus($request->subscriptionId);
    }

    // Function: Check status for cancellation benefits
    public function GetCancellationBenefitStatus($subscriptionId)
    {
        $subs = json_decode(Subscriptions::where('id',$subscriptionId)->first()->ToJson());
        $isWithinTrialPeriod = $this->subscriptionHelper->IsWithinTrialPeriod($subscriptionId);
        $haveAppliedCancellationDiscount = $this->subscriptionHelper->HaveAppliedCancellationDiscount($subs->UserId);
        $haveAppliedFreeProduct = $this->subscriptionHelper->HaveAppliedFreeProduct($subs->UserId);
        $havePausedSubscription = $this->subscriptionHelper->HavePausedSubscription($subs->UserId);
        $isAnnualPlan = $this->subscriptionHelper->IsAnnualPlan($subscriptionId);
        $cancellationBenefitStatus = array();
        $cancellationBenefitStatus["isWithinTrialPeriod"] = $isWithinTrialPeriod;
        $cancellationBenefitStatus["haveAppliedCancellationDiscount"] = $haveAppliedCancellationDiscount;
        $cancellationBenefitStatus["haveAppliedFreeProduct"] = $haveAppliedFreeProduct;
        $cancellationBenefitStatus["havePausedSubscription"] = $havePausedSubscription;
        $cancellationBenefitStatus["isAnnualPlan"] = $isAnnualPlan;
        return $cancellationBenefitStatus;
    }

    //***********************  Additional Functions for Cancellation Journey ***********************//



    //***********************  User Actions ***********************//
    // Function: Cancel subscription immediately
    public function CancelSubscription(Request $request) {

        // Get request data
        $SubscriptionId = $request->SubscriptionId;
        $CancellationTranslatesId = $request->CancellationTranslatesId;
        $OtherReason = $request->OtherReason;

        // Get subscription details
        $subscriptionName = $this->subscriptionHelper->GetSubscriptionName($SubscriptionId);
        $subs = $this->GetSubscriptionData($SubscriptionId);

        // Get cancellation reason in English
        $subsCancellationReason = "";
        if($CancellationTranslatesId !== null) {
            $cancellationTranslates = CancellationReasonTranslates::where('id', $CancellationTranslatesId)->first();
            $subsCancellationReason = $cancellationTranslates->defaultContent;
        }

        // If got other reason from user, then take this reason instead
        $otherReason = null;
        if($OtherReason !== null) {
            $subsCancellationReason = $OtherReason;
            $otherReason = $subsCancellationReason;
        } else {
            $cancellationSession = $this->sessionController->_session('cancellation_journey','get',null);
            if($cancellationSession !== "" && $cancellationSession !== null){
                $subsCancellationReason = json_decode($cancellationSession)->other_reason;
                $otherReason = $subsCancellationReason;
                $this->sessionController->_session('cancellation_journey','clear',null);
            }
        }

        // Create cancellation journey
        $journey = new CancellationJourneys();
        $journey->email = $subs->email;
        $journey->CancellationTranslatesId = $CancellationTranslatesId;
        $journey->OtherReason = $otherReason;
        $journey->UserId = $subs->UserId;
        $journey->SubscriptionId = $SubscriptionId;
        $journey->planType = $subs->isTrial === 1 ? "Trial" : "Custom";
        $journey->subscriptionName = $subscriptionName;
        $journey->hasCancelled = 1;
        $journey->save();

        // Insert into followupcancels table (Starting point of CancellationFollowUp CronJob)
        $followup = new CancellationFollowUp();
        $followup->UserId = $subs->UserId;
        $followup->SubscriptionId = $SubscriptionId;
        $followup->CancelEdm1 = 1;
        $followup->NextJourneyStatus = 2;
        $followup->JourneyDesc = 'Journey EDM 1';
        $followup->ReminderDate = Carbon::now()->addDays(42)->toDateString();
        $followup->save();

        // Cancel subscription
        $this->subscriptionHelper->CancelSubscription($SubscriptionId, $subsCancellationReason, "cancellation_journey");

        // Send cancellation email only if the user got specific reason
        if($otherReason !== null) {

            // Get country and language details
            $plan = Plans::where('id', $subs->PlanId)->first();
            $country = Countries::where('id', $plan->CountryId)->first();
            $currentCountry = $country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->UserId, $subs->PlanId);

            // Prepare email data
            $emailData = [];
            $emailData['title'] = 'cancellation-history-report';
            $emailData['moduleData'] = (Object) array(
                'email' => $subs->email,
                'subscriptionId' => $SubscriptionId,
                'userId' => $subs->UserId,
                'countryId' => $country->id
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
            EmailQueueService::addHash($subs->UserId,'cancellation_history_report',$emailData);
        }

        return 'success';
    }

    // Function: Continue subscription
    public function ContinueSubscription(Request $request){

        // Get request data
        $Actions = $request->Actions;
        $ActionParameters = $request->ActionParameters;
        $SubscriptionId = $request->SubscriptionId;
        $CancellationTranslatesId = $request->CancellationTranslatesId;
        $OtherReason = $request->OtherReason;


        // Set proceed status
        $proceedCancellationJourney = true;

        // Get status for cancellation benefits
        $cancellationBenefitStatus = $this->GetCancellationBenefitStatus($SubscriptionId);

        // Block everything for annual plan
        if($cancellationBenefitStatus['isAnnualPlan'])
        {
            $proceedCancellationJourney = false;
            return 'not_allowed';
        }
        else
        {
            // Get current plan data for comparison
            $currentPlanData = $this->subscriptionHelper->GetCurrentPlan($SubscriptionId);
            $currentProducts = $currentPlanData->plan_details["product_info"];
            $subscriptionName = $this->subscriptionHelper->GetSubscriptionName($SubscriptionId);

            // Current plan id
            $currentPlanId = $currentPlanData->plan_details["planid"];
            $currentPlan = Plans::where('id', $currentPlanId)->first();

            // Current plan duration
            $currentPlanDuration = intval($currentPlanData->current_subscription_duration);

            // CurrentProductCountryIds
            $currentProductCountryIds = array();
            foreach ($currentProducts as $product) {
                array_push($currentProductCountryIds, $product["productcountryid"]);
            }

            // Available productCountryIds for blades
            $bladeProductCountryIds = array();
            $bladeList = $currentPlanData->available_plans->blade_types;
            foreach ($bladeList as $blade) {
                array_push($bladeProductCountryIds, $blade['ProductCountryId']);
            }

            // Initialize variable
            $hasChangedBlade = 0;
            $modifiedBladeName = null;
            $hasChangedFrequency = 0;
            $modifiedFrequencyName = null;
            $hasGetFreeProduct = 0;
            $hasGetDiscount = 0;
            $discountPercent = 0;
            $hasPausedSubscription = 0;

            // Initialize updated subscription variables
            $updatedProductCountryIds = $currentProductCountryIds;
            $updatedPlanId = $currentPlanId;
            $updatedPlanDuration = $currentPlanDuration;

            // Loop through actions
            foreach ($Actions as $action) {

                switch ($action) {
                    case 'apply_cancellation_discount':
                        {
                            if($cancellationBenefitStatus['isWithinTrialPeriod'] || $cancellationBenefitStatus['haveAppliedCancellationDiscount']){
                                $proceedCancellationJourney = false;
                            } else {
                                $hasGetDiscount = 1;
                                $discountPercent = $this->GetCancellationDiscount();
                                $ActionParameters["cancellationDiscount"] = $discountPercent;
                            }
                        }
                        break;

                    case 'change_blade':
                        {
                            // Action parameters
                            $hasChangedBlade = 1;
                            $modifiedBladeName = $ActionParameters["modifiedBladeName"];
                            $modifiedBladeSku = $ActionParameters["modifiedBladeSku"];
                            $modifiedBladeProductCountryId = intval($ActionParameters["modifiedBladeProductCountryId"]);

                            // Generate new planCountryIds
                            $newProductCountryIds = array();
                            foreach ($updatedProductCountryIds as $currentProductCountryId) {
                                if(in_array($currentProductCountryId, $bladeProductCountryIds))
                                {
                                    array_push($newProductCountryIds, $modifiedBladeProductCountryId);
                                }
                                else
                                {
                                    array_push($newProductCountryIds, $currentProductCountryId);
                                }
                            }

                            // Generate available planIds
                            $allPlans = DB::select("SELECT PlanId, GROUP_CONCAT(ProductCountryId SEPARATOR ',') AS ProductCountryIds FROM plan_details GROUP BY PlanId");
                            $availablePlanIds = array();
                            foreach ($allPlans as $plan) {
                                $combinedProductCountryIds = explode(",", $plan->ProductCountryIds);
                                if(array_diff($combinedProductCountryIds,$newProductCountryIds) === Array())
                                {
                                    array_push($availablePlanIds, $plan->PlanId);
                                }
                            }

                            // Select the correct planId
                            $availablePlanSku = PlanSKU::where('duration', $updatedPlanDuration)->where('status',1)->get()->pluck('id');
                            $newPlan = Plans::where('CountryId',$currentPlan->CountryId)
                                ->where('plantype', $currentPlan->plantype)
                                ->whereIn('PlanSkuId', $availablePlanSku)
                                ->whereIn('id', $availablePlanIds)
                                ->first();

                            $newPlanId = $newPlan["id"];

                            // Update subscription variables
                            if($newPlanId && $newPlanId !== null) {
                                $updatedProductCountryIds = $newProductCountryIds;
                                $updatedPlanId = $newPlanId;
                                $ActionParameters["modifiedPlanId"] = $updatedPlanId;
                            } else {
                                return 'invalid_plan';
                            }
                        }
                        break;

                    case 'change_frequency':
                        {
                            // Action parameters
                            $hasChangedFrequency = 1;
                            $modifiedFrequencyName = $ActionParameters["modifiedFrequencyName"];
                            $modifiedFrequencyDuration = $ActionParameters["modifiedFrequencyDuration"];

                            // Generate available planIds
                            $allPlans = DB::select("SELECT PlanId, GROUP_CONCAT(ProductCountryId SEPARATOR ',') AS ProductCountryIds FROM plan_details GROUP BY PlanId");
                            $availablePlanIds = array();
                            foreach ($allPlans as $plan) {
                                $combinedProductCountryIds = explode(",", $plan->ProductCountryIds);
                                if(array_diff($combinedProductCountryIds,$updatedProductCountryIds) === Array())
                                {
                                    array_push($availablePlanIds, $plan->PlanId);
                                }
                            }

                            // Select the correct planId
                            $availablePlanSku = PlanSKU::where('duration', $modifiedFrequencyDuration)->where('status',1)->get()->pluck('id');
                            $newPlan = Plans::where('CountryId',$currentPlan->CountryId)
                                ->where('plantype', $currentPlan->plantype)
                                ->whereIn('PlanSkuId', $availablePlanSku)
                                ->whereIn('id', $availablePlanIds)
                                ->first();

                            $newPlanId = $newPlan["id"];

                            // Update subscription variables
                            if($newPlanId && $newPlanId !== null) {
                                $updatedPlanDuration = $modifiedFrequencyDuration;
                                $updatedPlanId = $newPlanId;
                                $ActionParameters["modifiedPlanId"] = $updatedPlanId;
                            } else {
                                return 'invalid_plan';
                            }
                        }
                        break;

                    case 'apply_free_product':
                        {
                            if($cancellationBenefitStatus['haveAppliedFreeProduct']) {
                                $proceedCancellationJourney = false;
                            } else {
                                $hasGetFreeProduct = 1;
                            }
                        }
                        break;

                    case 'pause_subscription':
                        {
                            // Action parameters
                            $hasPausedSubscription = 1;
                            $pauseMonths = $ActionParameters["pauseMonths"];
                        }
                        break;

                    default:
                        break;
                }

            }

            // Get subscription details
            $subs = $this->GetSubscriptionData($SubscriptionId);

            if($proceedCancellationJourney) {
                // Create cancellation journey
                $journey = new CancellationJourneys();
                $journey->email = $subs->email;
                $journey->CancellationTranslatesId = $CancellationTranslatesId;
                $journey->OtherReason = $OtherReason;
                $journey->UserId = $subs->UserId;
                $journey->SubscriptionId = $SubscriptionId;
                $journey->planType = $subs->isTrial === 1 ? "Trial" : "Custom";
                $journey->subscriptionName = $subscriptionName;
                $journey->hasCancelled = 0;
                $journey->hasChangedBlade = $hasChangedBlade;
                $journey->modifiedBlade = $modifiedBladeName;
                $journey->hasChangedFrequency = $hasChangedFrequency;
                $journey->modifiedFrequency = $modifiedFrequencyName;
                $journey->hasGetFreeProduct = $hasGetFreeProduct;
                $journey->hasGetDiscount = $hasGetDiscount;
                $journey->discountPercent = $discountPercent;
                $journey->hasPausedSubscription = $hasPausedSubscription;
                $journey->save();

                // Update subscription details
                $this->subscriptionHelper->UpdateSubscriptionDetails($SubscriptionId, $Actions, $ActionParameters, 'cancellation_journey');

                return 'success';
            }
            else {
                return 'not_allowed';
            }
        }
    }

    //***********************  User Actions ***********************//


    //***********************  Cancellation Notifications ***********************//

    // Function: Notify subscription cancelled
    public function NotifyCancelled(Request $request)
    {
        return view('_authenticated.cancellation-journey-modal.result.cancelled');
    }

    // Function: Notify subscription cancelled
    public function NotifyChangedBlade(Request $request)
    {
        // Get request data
        $blade_sku = str_replace("_","/",$request->blade_sku);
        $blade_num = config('global.all.blade_no.'.$blade_sku);
        $blade_name = $blade_num . ' ' . Lang::get('website_contents.blade_packV3');
        $product_default_image_x4 = asset(config('global.ecommerce.cancellation.blades_x4_image_url.' . $blade_num));

        return view('_authenticated.cancellation-journey-modal.result.changed_blade')
            ->with('blade_name', $blade_name)
            ->with('product_default_image_x4', $product_default_image_x4);
    }

    // Function: Notify subscription cancelled
    public function NotifyAppliedFreeBlade(Request $request)
    {
        // Get request data
        $blade_sku = str_replace("_","/",$request->blade_sku);
        $blade_num = config('global.all.blade_no.'.$blade_sku);
        $blade_name = $blade_num . ' ' . Lang::get('website_contents.blade_packV3');
        $product_default_image_free_blade = asset(config('global.ecommerce.cancellation.free_blades_image_url.' . $blade_num));

        return view('_authenticated.cancellation-journey-modal.result.applied_free_blade')
            ->with('blade_name', $blade_name)
            ->with('product_default_image_free_blade', $product_default_image_free_blade);
    }

    //***********************  Cancellation Notifications ***********************//

}
