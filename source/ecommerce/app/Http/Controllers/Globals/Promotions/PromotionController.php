<?php

namespace App\Http\Controllers\Globals\Promotions;

use App\Helpers\LaravelHelper;
use App\Helpers\PromotionsHelper;
use App\Http\Controllers\Controller as Controller;

// Helpers
use App\Http\Controllers\Globals\Utilities\SessionController;

// Models
use App\Services\CountryService as CountryService;
use App\Services\TaxInvoiceService;
use Exception;

// Services
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->session = new SessionController();
        $this->laravelHelper = new LaravelHelper();
        $this->promotionsHelper = new PromotionsHelper();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->country_info = $this->laravelHelper->ConvertArraytoObject($this->country_service->getCountryAndLangCode());
        $this->country_id = $this->country_info->country_id;

        $this->appType = config('app.appType');
        $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;
        $this->taxInvoiceService = new TaxInvoiceService();
    }

    // Promotion
    public function checkPromotion(Request $request)
    {
        try {
            $oldPromocode ="";
            if ((isset($request->usertype) && $request->usertype == "web") && (isset($request->firstpromoapplyget)  && $request->firstpromoapplyget == "0")) {
                if (isset($request->type) && $request->type == "trial-plan") {
                    $session_data_checkout = $this->session->_session("checkout_trial_plan", "get", null);
                    if($session_data_checkout){
                    $session_data_checkout_decoded = json_decode($session_data_checkout);
                    if (array_key_exists('promo', $session_data_checkout_decoded->checkout)) {
                        if (array_key_exists('promo_code', $session_data_checkout_decoded->checkout->promo)) {
                            $oldPromocode =$session_data_checkout_decoded->checkout->promo->promo_code;
                        }
                    }
                 }
                }else if(isset($request->type) && $request->type == "custom-plan"){
                    $session_data_checkout = $this->session->_session("checkout_custom_plan", "get", null);
                    if($session_data_checkout){
                    $session_data_checkout_decoded = json_decode($session_data_checkout);
                    if (array_key_exists('promo', $session_data_checkout_decoded->checkout)) {
                        if (array_key_exists('promo_code', $session_data_checkout_decoded->checkout->promo)) {
                            $oldPromocode =$session_data_checkout_decoded->checkout->promo->promo_code;
                        }
                    }
                  }
                }else if(isset($request->type) && $request->type == "awesome-shave-kits"){

                    $session_data_checkout = $this->session->_session("checkout_ask", "get", null);
                    if($session_data_checkout){
                    $session_data_checkout_decoded = json_decode($session_data_checkout);
                     if (array_key_exists('promo', $session_data_checkout_decoded->checkout)) {
                        if (array_key_exists('promo_code', $session_data_checkout_decoded->checkout->promo)) {
                            $oldPromocode =$session_data_checkout_decoded->checkout->promo->promo_code;
                        }
                    }
                  }
                }
            }

            if(isset($oldPromocode) && $oldPromocode != ""){
                if(isset($request->promo_code) && $oldPromocode == $request->promo_code){
                    return "same";
                }
            }

            $result = $this->promotionsHelper->promotioncheckV2($request->promo_code, $request->lang, $request->country, $request->user["id"], $request->session_data, $request->type, $request->usertype);
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    // API Promotion
    public function checkPromotionAPI(Request $request)
    {
        try {
            $result = $this->promotionsHelper->promotioncheckV2($request->promo_code, $request->langCode, $request->CountryId, $request->user_id, null, null, $request->usertype);
            return response()->json([
                'success' => true,
                'payload' => $result,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        } catch (Exception $e) {
            throw $e;
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }
}
