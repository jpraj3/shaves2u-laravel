<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Subscriptions;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\SubscriptionAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class SubscriptionAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->subscriptionAPIHelper = new SubscriptionAPIHelper();
    }

    public function APISubscribersCount(Request $request)
    {
        $options = $request->all();
        $subscribersCount = $this->subscriptionAPIHelper->getSubscribersCount($options);

        return response()
        ->json($subscribersCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APISubscribers(Request $request)
    {
        $options = $request->all();
        $subscribers = $this->subscriptionAPIHelper->getSubscribersList($options);

        return response()
        ->json($subscribers)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APISubscriberDetails(Request $request)
    {
        $id = $request->input('id');
        $subscriber_details = $this->subscriptionAPIHelper->getSubscriberDetails($id);

        return response()
        ->json($subscriber_details)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APISubscriberStatusUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->subscriptionAPIHelper->updateSubscriptionStatus($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
