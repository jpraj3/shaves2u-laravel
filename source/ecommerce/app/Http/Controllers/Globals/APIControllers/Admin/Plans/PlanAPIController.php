<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Plans;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\PlanAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class PlanAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->planAPIHelper = new PlanAPIHelper();
    }
    
    public function APIPlanSKUs()
    {
        $skus = $this->planAPIHelper->getPlanSKUsList();

        return response()
        ->json($skus)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
