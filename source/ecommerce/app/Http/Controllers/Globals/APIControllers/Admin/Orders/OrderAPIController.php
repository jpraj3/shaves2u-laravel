<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Orders;

use App\Helpers\APIHelpers\Admin\OrderAPIHelper;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use PDF;

class OrderAPIController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->OrderAPIHelper = new OrderAPIHelper();
    }

    public function APIOrdersCount(Request $request)
    {
        $options = $request->all();
        $ordersCount = $this->OrderAPIHelper->getOrdersCount($options);

        return response()
        ->json($ordersCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIOrders(Request $request)
    {
        $options = $request->all();
        $orders = $this->OrderAPIHelper->getOrdersList($options);

        return response()
        ->json($orders)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIOrderDetails(Request $request)
    {
        $id = $request->input('id');
        $order_details = $this->OrderAPIHelper->getOrderDetails($id);

        return response()
        ->json($order_details)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIOrderStatusUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->OrderAPIHelper->updateOrderStatus($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIOrderPaymentStatusUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->OrderAPIHelper->updateOrderPaymentStatus($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIOrderTrackingUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->OrderAPIHelper->updateTrackingNumber($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
