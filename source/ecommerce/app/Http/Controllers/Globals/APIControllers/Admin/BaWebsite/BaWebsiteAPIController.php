<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\BaWebsite;

use App\Helpers\APIHelpers\Admin\BaWebsiteAPIHelper;
use App\Helpers\CSVHelper;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\GeoLocation\Countries;
use Carbon\Carbon;

class BaWebsiteAPIController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->BaWebsiteAPIHelper = new BaWebsiteAPIHelper();
        $this->CSVHelper = new CSVHelper();
        $this->CSVHelper = new CSVHelper();
    }

    public function APISellerUsersCount(Request $request)
    {
        $options = $request->all();
        $sellerusersCount = $this->BaWebsiteAPIHelper->getSellerUsersCount($options);

        return response()
        ->json($sellerusersCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APISellerUsers(Request $request)
    {
        $options = $request->all();
        $sellerusers = $this->BaWebsiteAPIHelper->getSellerUsersList($options);

        return response()
        ->json($sellerusers)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APISellerUserDetails(Request $request)
    {
        $options = $request->all();
        $sellerusers = $this->BaWebsiteAPIHelper->getSellerUsersList($options);

        return response()
        ->json($sellerusers)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICreateSellerUser(Request $request)
    {
        $data = $request->all();
        $message = $this->BaWebsiteAPIHelper->createSellerUser($data);

        return response()
        ->json($message)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIChangeSellerUserStatus(Request $request, $id)
    {
        $data = $request->all();
        $message = $this->BaWebsiteAPIHelper->changeSellerUserStatus($data, $id);

        return response()
        ->json($message)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIUploadSellerUsers(Request $request)
    {
        $datareturn = [];
        $newsellerdata = [];
        $bulkorderdata = "";
        $filename = $request->file('csv_file')->getClientOriginalName();
        $filetype = $request->file('csv_file')->getClientOriginalExtension();
        if (in_array($filetype,['csv'])) {
            if (strpos($filename,"_")) {
                $countryCode = explode("_",$filename)[0];
                $uploaddate = explode(".",explode("_",$filename)[1])[0];
                if (ctype_digit($uploaddate)) {
                    $countryId = Countries::where('code',$countryCode)->pluck('id')->first();
                    if ($countryId != null) {
                        $date = Carbon::createFromFormat("Ymd",$uploaddate)->startOfDay();
                        $now = Carbon::now()->startOfDay();

                        if ($date->diffInDays($now) == 0) {
                            $clear = $this->BaWebsiteAPIHelper->massInactiveSellerUser($countryId);

                            $csv = $this->CSVHelper->readCSV($request);
                            if ($csv["data"]) {
                                $data = $csv["data"];
                                foreach ($data as $key => $datarow) {
                                    if ($key == 0) { } else {
                                        // foreach ($datarow as $key => $value) {
                                        $selleruserdata = $this->BaWebsiteAPIHelper->checkSellerUser($datarow, $countryId);
                                        // }
                                    }
                                }
                            }
                            $message = ['success' => 'success', 200];
                        }
                        else {
                            $message = ['error' => 'Only files with the current date can be uploaded. Please update the file accordingly.', 400];
                        }
                    }
                    else {
                        $message = ['error' => 'The country code in the file does not exist. Please use only the codes in the directory.', 400];
                    }
                }
                else {
                    $message = ['error' => 'Wrong .csv format detected. Please revise according to the format provided.', 400];
                }
            }
            else {
                $message = ['error' => 'Wrong .csv format detected. Please revise according to the format provided.', 400];
            }
        }
        else {
            $message = ['error' => 'Only file types of .csv can be uploaded.', 400];
        }

        return response()
        ->json($message)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function getMOs(Request $request)
    {
        $mos = MarketingOffice::get()->all();

        return response()
        ->json($mos)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function createMO(Request $request)
    {
        $data = $request->all();
        $mo = new MarketingOffice();
        
        $mo->moCode = $data['add_mo_code'];
        $mo->organization = $data['add_mo_org'];

        $mo->save();

        $success = ['success' => 'success', 200];

        return response()
        ->json($success)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function updateMO(Request $request, $id)
    {
        $data = $request->all();
        $mo = MarketingOffice::where('id', $id)->first();
        
        $mo->moCode = $data['edit_mo_code'];
        $mo->organization = $data['edit_mo_org'];

        $mo->save();

        $success = ['success' => 'success', 200];

        return response()
        ->json($success)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}