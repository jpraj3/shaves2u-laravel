<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Recharges;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\RechargeAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class RechargeAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->rechargeAPIHelper = new RechargeAPIHelper();
    }

    public function APIRechargesCount(Request $request)
    {
        $options = $request->all();
        $rechargeCount = $this->rechargeAPIHelper->getRechargesCount($options);

        return response()
        ->json($rechargeCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIRecharges(Request $request)
    {
        $options = $request->all();
        $recharges = $this->rechargeAPIHelper->getRechargesList($options);

        return response()
        ->json($recharges)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}