<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Products;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\ProductAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class ProductAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->productAPIHelper = new ProductAPIHelper();
    }
    
    public function APISKUs()
    {
        $skus = $this->productAPIHelper->getSKUsList();

        return response()
        ->json($skus)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
