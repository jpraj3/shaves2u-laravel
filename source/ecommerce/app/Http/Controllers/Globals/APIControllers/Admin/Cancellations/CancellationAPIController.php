<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Cancellations;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\CancellationAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class CancellationAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->cancellationAPIHelper = new CancellationAPIHelper();
    }

    public function APICancellationsCount(Request $request)
    {
        $options = $request->all();
        $cancellationCount = $this->cancellationAPIHelper->getCancellationsCount($options);

        return response()
        ->json($cancellationCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APICancellations(Request $request)
    {
        $options = $request->all();
        $subscribers = $this->cancellationAPIHelper->getCancellationsList($options);

        return response()
        ->json($subscribers)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
}
