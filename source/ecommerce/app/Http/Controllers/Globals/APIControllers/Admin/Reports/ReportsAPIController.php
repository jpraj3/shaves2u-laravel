<?php

namespace App\Http\Controllers\Globals\APIControllers\Admin\Reports;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\ReportAPIHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\GeoLocation\Countries;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->$request = $request;
        $this->ReportAPIHelper = new ReportAPIHelper();
    }


    public function APISales(Request $request)
    {
        $options = $request->all();

        $sales = $this->ReportAPIHelper->getSales($options);

        return response()
        ->json($sales)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIMastersCount(Request $request)
    {
        $options = $request->all();
        $mastersCount = $this->ReportAPIHelper->getMastersCount($options);

        return response()
        ->json($mastersCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIMasters(Request $request)
    {
        $options = $request->all();
        $masters = $this->ReportAPIHelper->getMastersList($options);

        return response()
        ->json($masters)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    public function APIAppcoCount(Request $request)
    {
        $options = $request->all();
        $appcoCount = $this->ReportAPIHelper->getAppcoCount($options);

        return response()
        ->json($appcoCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIAppco(Request $request)
    {
        $options = $request->all();
        $appco = $this->ReportAPIHelper->getAppcoList($options);

        return response()
        ->json($appco)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIMOCount(Request $request)
    {
        $options = $request->all();
        $moCount = $this->ReportAPIHelper->getMOCount($options);

        return response()
        ->json($moCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIMO(Request $request)
    {
        $options = $request->all();
        $mo = $this->ReportAPIHelper->getMOList($options);

        return response()
        ->json($mo)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}