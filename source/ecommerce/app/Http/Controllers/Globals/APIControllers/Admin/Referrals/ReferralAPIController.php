<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Referrals;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\ReferralAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class ReferralAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->referralAPIHelper = new ReferralAPIHelper();
    }

    public function APIReferralsCount(Request $request)
    {
        $options = $request->all();
        $referralsCount = $this->referralAPIHelper->getReferralsCount($options);

        return response()
        ->json($referralsCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIReferrals(Request $request)
    {
        $options = $request->all();
        $referrals = $this->referralAPIHelper->getReferralsList($options);

        return response()
        ->json($referrals)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIReferralDetailsCount(Request $request)
    {
        $options = $request->all();
        $referralsCount = $this->referralAPIHelper->getReferralDetailsCount($options);

        return response()
        ->json($referralsCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIReferralDetails(Request $request)
    {
        $options = $request->all();
        $referrals = $this->referralAPIHelper->getReferralDetailsList($options);

        return response()
        ->json($referrals)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIReferralsCashOutCount(Request $request)
    {
        $options = $request->all();
        $referralsCount = $this->referralAPIHelper->getReferralsCashOutCount($options);

        return response()
        ->json($referralsCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIReferralsCashOut(Request $request)
    {
        $options = $request->all();
        $referrals = $this->referralAPIHelper->getReferralsCashOutList($options);

        return response()
        ->json($referrals)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIReferralCashOutDetailsCount(Request $request)
    {
        $options = $request->all();
        $referralsCount = $this->referralAPIHelper->getReferralCashOutDetailsCount($options);

        return response()
        ->json($referralsCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIReferralCashOutDetails(Request $request)
    {
        $id = $request->input('id');
        $referrals = $this->referralAPIHelper->getReferralCashOutDetails($id);

        return response()
        ->json($referrals)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIReferralCashoutStatusUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->referralAPIHelper->updateReferralCashoutStatus($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
