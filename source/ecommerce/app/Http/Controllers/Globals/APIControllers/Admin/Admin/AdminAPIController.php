<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Admin;

// Models
use App\Models\Admins\Admin;
use App\Models\Admins\Roles;
use App\Models\Admins\RoleDetails;
use App\Models\Admins\Paths;
use App\Models\BaWebsite\MarketingOffice;

use App\Services\CountryService;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

class AdminAPIController extends Controller
{
    public function __construct(Request $request)
    {
        $this->country_service = new CountryService();
        $this->request = $request;
    }

    public function getAdmins(Request $request)
    {
        $admins = Admin::get()->all();
        foreach ($admins as $admin) {
            $admin->country = $this->country_service->getCountryfromDB($admin->CountryId)->name;
            $admin->role = Roles::where('id',$admin->RoleId)->pluck('roleName')->first();
        }

        return response()
        ->json($admins)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function createAdmin(Request $request)
    {
        $data = $request->all();
        $admin = new Admin();
        
        $admin->firstName = $data['add_admin_firstname'];
        $admin->lastName = $data['add_admin_lastname'];
        $admin->phone = $data['add_admin_phone'];
        $admin->email = $data['add_admin_email'];
        $admin->password = md5($data['add_admin_password']);
        $admin->RoleId = $data['add_admin_role'];
        $admin->CountryId = $data['add_admin_country'];
        $admin->MarketingOfficeId = $data['add_admin_mo'];
        $admin->isActive = 1;
        $admin->viewAllCountry = isset($data['add_admin_viewall']) && $data['add_admin_viewall'] == 1 ? 1 : 0;

        $admin->save();

        $success = ['success' => 'success', 200];

        return response()
        ->json($success)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function updateAdmin(Request $request, $id)
    {
        $data = $request->all();
        $admin = Admin::findOrFail($id);
        
        $admin->RoleId = $data['edit_role'];
        $admin->CountryId = $data['edit_country'];
        $admin->MarketingOfficeId = $data['edit_mo'];
        $admin->viewAllCountry = $data['edit_viewall'] ? 1 : 0;

        $admin->save();

        $success = ['success' => 'success', 200];

        return response()
        ->json($success)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}