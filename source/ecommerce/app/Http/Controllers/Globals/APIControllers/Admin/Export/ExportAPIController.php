<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\Export;

use App\Helpers\APIHelpers\Admin\ExportAPIHelper;
use App\Http\Controllers\Controller as Controller;

// Helpers
use Illuminate\Foundation\Application;
// Models

// Services
use Illuminate\Http\Request;

class ExportAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->ExportAPIHelper = new ExportAPIHelper();
    }

    public function APIExportCount(Request $request)
    {
        $options = $request->all();
        $exportCount = $this->ExportAPIHelper->getExportsCount($options);

        return response()
            ->json($exportCount)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIExportList(Request $request)
    {
        $options = $request->all();
        $exports = $this->ExportAPIHelper->getExportList($options);

        return response()
            ->json($exports)
            ->header("Access-Control-Allow-Origin", "*");
    }

}
