<?php

namespace App\Http\Controllers\Globals\APIControllers\Admin\BulkOrders;

use App\Helpers\APIHelpers\Admin\BulkOrderAPIHelper;
use App\Helpers\CSVHelper;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use PDF;
use App\Models\BulkOrders\BulkOrders;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrderHistories;

class BulkOrderAPIController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->BulkOrderAPIHelper = new BulkOrderAPIHelper();
        $this->CSVHelper = new CSVHelper();
    }

    public function APIBulkOrdersCount(Request $request)
    {
        $options = $request->all();
        $bulkordersCount = $this->BulkOrderAPIHelper->getBulkOrdersCount($options);

        return response()
            ->json($bulkordersCount)
            ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIBulkOrders(Request $request)
    {
        $options = $request->all();
        $bulkorders = $this->BulkOrderAPIHelper->getBulkOrdersList($options);

        return response()
            ->json($bulkorders)
            ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIBulkOrderDetails(Request $request)
    {
        $id = $request->input('id');
        $bulkorder_details = $this->BulkOrderAPIHelper->getBulkOrderDetails($id);

        return response()
            ->json($bulkorder_details)
            ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIBulkOrderStatusUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->BulkOrderAPIHelper->updateBulkOrderStatus($params);

        return response()
            ->json($response)
            ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIBulkOrderTrackingUpdate(Request $request)
    {
        $params = $request->all();
        $response = $this->BulkOrderAPIHelper->updateTrackingNumber($params);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APIUploadBulkOrder(Request $request)
    {
        $datareturn = [];
        $newbulkdata = [];
        $bulkorderdata = "";
        $csv = $this->CSVHelper->readCSV($request);
        if ($csv["data"]) {
            $data = $csv["data"];
            foreach ($data as $key => $datarow) {
                if ($key == 0) { } else {
                    // foreach ($datarow as $key => $value) {
                    $bulkorderdata = $this->BulkOrderAPIHelper->csvCheckInsertData($datarow);
                    if ($bulkorderdata["status"] == "fail") {
                        $datareturn["status"] = $bulkorderdata["status"];
                        $datareturn["reason"] = $bulkorderdata["reason"];
                        return response()
                            ->json($datareturn)
                            ->header("Access-Control-Allow-Origin",  "*");
                    } else {
                        array_push($newbulkdata, $bulkorderdata);
                    }
                    // }
                }
            }
            $insertbulkorderdata = $this->BulkOrderAPIHelper->csvInsertData($newbulkdata);
            if ($insertbulkorderdata["status"] == "fail") {
                $datareturn["status"] = $insertbulkorderdata["status"];
                $datareturn["reason"] = $insertbulkorderdata["reason"];
                return response()
                    ->json($datareturn)
                    ->header("Access-Control-Allow-Origin",  "*");
            }
            else{
            $datareturn["status"] = "success";
            $datareturn["reason"] = "";
            return response()
                ->json($newbulkdata)
                ->header("Access-Control-Allow-Origin",  "*");
            }
        }
        $datareturn["status"] = "fail";
        $datareturn["reason"] = "Fail to read csv file";
        return response()
            ->json($datareturn)
            ->header("Access-Control-Allow-Origin",  "*");
    }
}
