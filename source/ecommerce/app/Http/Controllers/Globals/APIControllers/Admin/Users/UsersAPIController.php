<?php

namespace App\Http\Controllers\Globals\APIControllers\Admin\Users;

use App\Helpers\APIHelpers\Admin\UserAPIHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UsersAPIController extends Controller
{
    public function __construct(Request $request)
    {

        $this->request = $request;
        $this->userAPIHelper = new UserAPIHelper();
    }

    public function APIUsers(Request $request)
    {
        $options = $request->all();

        $users = $this->userAPIHelper->getUsersList($options);

        return response()
        ->json($users)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomersCount(Request $request)
    {
        $options = $request->all();

        $customersCount = $this->userAPIHelper->getCustomersCount($options);

        return response()
        ->json($customersCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomers(Request $request)
    {
        $options = $request->all();

        $customers = $this->userAPIHelper->getCustomersList($options);

        return response()
            ->json($customers)
            ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerDetails(Request $request)
    {
        $id = $request->input('id');
        $customer_details = $this->userAPIHelper->getCustomerDetails($id);

        return response()
        ->json($customer_details)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerStatusUpdate(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->updateCustomerStatus($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerProfileUpdate(Request $request, $id)
    {
        $request->validate([
            'customer_email' => 'required|unique:users,email,'.$id,
            'customer_name' => 'required',
        ]);

        $data = $request->all();
        $response = $this->userAPIHelper->updateCustomerProfile($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerSelectAddress(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->selectCustomerDefaultAddress($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerCreateAddress(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->addCustomerAddress($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerUpdateAddress(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->editCustomerAddress($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerRemoveAddress(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->removeCustomerAddress($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerSelectCard(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->selectCustomerDefaultCard($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerCreateCard(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->addCustomerCard($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }

    public function APICustomerRemoveCard(Request $request, $id)
    {
        $data = $request->all();
        $response = $this->userAPIHelper->removeCustomerCard($data, $id);

        return response()
        ->json($response)
        ->header("Access-Control-Allow-Origin",  "*");
    }
}
