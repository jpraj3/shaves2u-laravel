<?php

namespace App\Http\Controllers\Globals\APIControllers\Admin;

use App\Helpers\APIHelpers\Admin\DownloadAPIHelper;
use App\Helpers\APIHelpers\Utilities\ExportsHelper;
use App\Helpers\TaxInvoiceHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\FileUploads\FileUploads;
use App\Queue\CompressedFileToDownloadQueueService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Exception;

class DownloadController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->DownloadAPIHelper = new DownloadAPIHelper();
        $this->ExportsHelper = new ExportsHelper();
        $this->TaxInvoiceHelper = new TaxInvoiceHelper();
    }

    public function excel($type, $data)
    {
        $output = $this->ExportsHelper->$type($data);
        return $output;
    }

    public function APIOrders(Request $request)
    {

        $output = '';
        $options = $request->all();
        $orders = $this->DownloadAPIHelper->getOrdersList($options);
        if (!empty($orders)) {
            $output = $this->ExportsHelper->orders('orders', $options, $orders);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIBulkOrders(Request $request)
    {

        $output = '';
        $options = $request->all();
        $bulkorders = $this->DownloadAPIHelper->getBulkOrdersList($options);
        if (!empty($bulkorders)) {
            $output = $this->ExportsHelper->bulkorders('bulkorders', $options, $bulkorders);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIReferrals(Request $request)
    {

        $output = '';
        $options = $request->all();
        $referrals = $this->DownloadAPIHelper->getReferralsList($options);
        if (!empty($referrals)) {
            $output = $this->ExportsHelper->referrals('referrals', $options, $referrals);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIReferralsCashOut(Request $request)
    {

        $output = '';
        $options = $request->all();
        $referralscashout = $this->DownloadAPIHelper->getReferralsCashOutList($options);
        if (!empty($referralscashout)) {
            $output = $this->ExportsHelper->referralscashout('referralsCashout', $options, $referralscashout);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APISubscribers(Request $request)
    {

        $output = '';
        $options = $request->all();
        // $subscribers = $this->DownloadAPIHelper->getSubscribersList($options,'subscribers');
        $subscribers = $this->DownloadAPIHelper->sendSubscribersQueue($options, 'subscribers');

        // return response()
        //     ->json($subscribers)
        //     ->header("Access-Control-Allow-Origin", "*");
        // if (!empty($subscribers)) {
        //     $output = $this->ExportsHelper->subscribers('subscribers', $options, $subscribers);
        // }

        return response()
            ->json($subscribers)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APISubscribersSummary(Request $request)
    {

        $output = '';
        $options = $request->all();
        // $subscribers = $this->DownloadAPIHelper->getSubscribersList($options,'subscribers');
        $subscribers = $this->DownloadAPIHelper->sendSubscribersSummaryQueue($options, 'subscriberssummary');

        // return response()
        //     ->json($subscribers)
        //     ->header("Access-Control-Allow-Origin", "*");
        // if (!empty($subscribers)) {
        //     $output = $this->ExportsHelper->subscribers('subscribers', $options, $subscribers);
        // }

        return response()
            ->json($subscribers)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APICancellations(Request $request)
    {

        $output = '';
        $options = $request->all();
        $cancellations = $this->DownloadAPIHelper->getCancellationsList($options);
        if (!empty($cancellations)) {
            $output = $this->ExportsHelper->cancellations('cancellations', $options, $cancellations);
            return response()
                ->json($output)
                ->header("Access-Control-Allow-Origin", "*");
        } else {
            $data = ["url" => null, "error" => null];
            return response()
                ->json($data)
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function APIReportsMaster(Request $request)
    {
        $output = '';
        $options = $request->all();

        try {
            //$ReportsMaster = $this->DownloadAPIHelper->getReportsMasterList($options);
            $ReportsMaster = $this->DownloadAPIHelper->sendReportsMasterQueue($options, 'reportsmaster');
            //     if (!empty($ReportsMaster)) {
            //         $output = $this->ExportsHelper->reportsmaster('report-master', $options, $ReportsMaster);
            //     }

            //     return response()
            //         ->json('initiated')
            //         ->header("Access-Control-Allow-Origin", "*");
            // } catch (Exception $ex) {
            //     return response()
            //         ->json( $ex->getMessage())
            //         ->header("Access-Control-Allow-Origin", "*");
            // }

            return response()
                ->json($ReportsMaster)
                ->header("Access-Control-Allow-Origin", "*");
        } catch (Exception $ex) {
            return response()
                ->json($ex->getMessage())
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function APIReportsMasterSummary(Request $request)
    {
        $output = '';
        $options = $request->all();

        try {
        
            $ReportsMaster = $this->DownloadAPIHelper->sendReportsMasterSummaryQueue($options, 'reportsmastersummary');

            return response()
                ->json($ReportsMaster)
                ->header("Access-Control-Allow-Origin", "*");
        } catch (Exception $ex) {
            return response()
                ->json($ex->getMessage())
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function APIReportsAppco(Request $request)
    {
        $output = '';
        $options = $request->all();
        $ReportsAppco = $this->DownloadAPIHelper->getReportsAppcoList($options);
        if (!empty($ReportsAppco)) {
            $output = $this->ExportsHelper->reportsappco('report-appco', $options, $ReportsAppco);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");

        // $ReportsAppco = $this->DownloadAPIHelper->sendReportsAppcoQueue($options,'reportsappco');

        // return response()
        //     ->json($ReportsAppco)
        //     ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIReportsMO(Request $request)
    {
        $output = '';
        $options = $request->all();
        $ReportsMO = $this->DownloadAPIHelper->getReportsMOList($options);
        if (!empty($ReportsMO)) {
            $output = $this->ExportsHelper->reportsmo('report-mo', $options, $ReportsMO);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");

        // $ReportsMO = $this->DownloadAPIHelper->sendReportsMOQueue($options,'reportsmo');

        // return response()
        //     ->json($ReportsMO)
        //     ->header("Access-Control-Allow-Origin", "*");
    }

    public function APICustomers(Request $request)
    {

        $output = '';
        $options = $request->all();
        $customers = $this->DownloadAPIHelper->getCustomersList($options);
        if (!empty($customers)) {
            $output = $this->ExportsHelper->customers('customers', $options, $customers);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIRecharges(Request $request)
    {

        $output = '';
        $options = $request->all();
        $recharges = $this->DownloadAPIHelper->getRechargesList($options);
        if (!empty($recharges)) {
            $output = $this->ExportsHelper->recharges('recharges', $options, $recharges);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIPausePlans(Request $request)
    {

        $output = '';
        $options = $request->all();
        $pauseplans = $this->DownloadAPIHelper->getPausePlansList($options);
        if (!empty($pauseplans)) {
            $output = $this->ExportsHelper->pauseplans('pauseplans', $options, $pauseplans);
        }

        return response()
            ->json($output)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function APIOrderTaxInvoice(Request $request)
    {
        $output = '';
        $options = $request->all();
        if (!empty($options)) {
            $fromdate = "";
            $todate = "";

            if ($options['fromdate']) {
                $fromdate = date("Y-m-d", strtotime($options['fromdate']));
                $fromdate = ' from ' . Carbon::parse($fromdate)->format('Y-m-d');
            }
            if ($options['todate']) {
                $todate = date("Y-m-d", strtotime($options['todate']));
                $todate = ' to ' . Carbon::parse($todate)->format('Y-m-d');
            }

            $zipFilename = 'Tax Invoice for ' . ucfirst(strtolower($options["type"])) . $fromdate . $todate . Carbon::now()->format('(Y-m-d_hisv)') . '.zip';
            $file_id = Str::uuid()->toString();
            //Create new fileUpload entry
            FileUploads::create([
                'name' => $zipFilename,
                'isReport' => 0,
                'file_id' => $file_id,
                'status' => 'Processing',
                'isDownloaded' => 0,
                'AdminId' => (int) $options["adminId"],
            ]);

            $options["zipFilename"] = $zipFilename;
            $options["file_id"] = $file_id;
            $options["order_type"] = 'order';
            CompressedFileToDownloadQueueService::addHash($file_id, $options);
            return response()
                ->json('success')
                ->header("Access-Control-Allow-Origin", "*");
        } else {
            return response()
                ->json('fail')
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function APIBulkOrderTaxInvoice(Request $request)
    {
        $output = '';
        $options = $request->all();
        if (!empty($options)) {
            $fromdate = "";
            $todate = "";

            if ($options['fromdate']) {
                $fromdate = date("Y-m-d", strtotime($options['fromdate']));
                $fromdate = ' from ' . Carbon::parse($fromdate)->format('Y-m-d');
            }
            if ($options['todate']) {
                $todate = date("Y-m-d", strtotime($options['todate']));
                $todate = ' to ' . Carbon::parse($todate)->format('Y-m-d');
            }

            $zipFilename = 'Tax Invoice for ' . ucfirst(strtolower($options["type"])) . $fromdate . $todate . Carbon::now()->format('(Y-m-d_hisv)') . '.zip';
            $file_id = Str::uuid()->toString();
            //Create new fileUpload entry
            FileUploads::create([
                'name' => $zipFilename,
                'isReport' => 0,
                'file_id' => $file_id,
                'status' => 'Processing',
                'isDownloaded' => 0,
                'AdminId' => (int) $options["adminId"],
            ]);

            $options["zipFilename"] = $zipFilename;
            $options["file_id"] = $file_id;
            $options["order_type"] = 'bulkorder';
            CompressedFileToDownloadQueueService::addHash($file_id, $options);
            return response()
                ->json('success')
                ->header("Access-Control-Allow-Origin", "*");
        } else {
            return response()
                ->json('fail')
                ->header("Access-Control-Allow-Origin", "*");
        }
    }
}
