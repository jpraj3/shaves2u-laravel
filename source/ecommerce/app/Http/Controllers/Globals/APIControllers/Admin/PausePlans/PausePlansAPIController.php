<?php
namespace App\Http\Controllers\Globals\APIControllers\Admin\PausePlans;

use Illuminate\Foundation\Application;
use App\Helpers\APIHelpers\Admin\PausePlansAPIHelper;

// Helpers
use App\Http\Controllers\Controller as Controller;
// Models

// Services
use Illuminate\Http\Request;

class PausePlansAPIController extends Controller
{
    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->pauseplansAPIHelper = new PausePlansAPIHelper();
    }

    public function APIPausePlansCount(Request $request)
    {
        $options = $request->all();
        $pauseplansCount = $this->pauseplansAPIHelper->getPausePlansCount($options);

        return response()
        ->json($pauseplansCount)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
    public function APIPausePlans(Request $request)
    {
        $options = $request->all();
        $pauseplans = $this->pauseplansAPIHelper->getPausePlansList($options);

        return response()
        ->json($pauseplans)
        ->header("Access-Control-Allow-Origin",  "*");
    }
    
}
