<?php

namespace App\Http\Controllers\Globals\APIControllers\BaWebsite\Plans;

// Controllers & Services

use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelperAPI;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\APIStripeController;
use App\Models\GeoLocation\Countries;
// Models
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductType;
use App\Services\APICountryService;
// Helpers
use Carbon\Carbon;
use Illuminate\Http\Request;
use Lang;

class BATrialPlanController extends Controller
{
    public $product_helper;
    public $country_service;
    public $lang_code;
    public $country;
    public $country_id;

    // Constructor
    public function __construct()
    {
        $this->product_helper = new ProductHelper();
        $this->plan_helper = new PlanHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->apicountryservice = new APICountryService();
        $this->api_stripeController = new APIStripeController();
        $this->userHelper = new UserHelperAPI();

    }

    public function apiEntryPoint(Request $request)
    {
        try {
            if ($request) {
                $this->lang_code = $request->langCode;
                $this->country_id = $request->CountryId;
                $this->genderType = isset($request->gender) ? $request->gender : 'male';
                \App::setLocale(strtolower($this->lang_code));
                // SKUs and Product Type might differ based on country, so can do if else here
                if ($this->country_id) {
                    if ($this->genderType !== null && $this->genderType === 'female') {
                        $this->freeShaveCreamSKU = "A5";
                        $this->trialPlanHandle_ProductTypeName = "Womens Trial Plan Handle";
                        $this->trialPlanBlade_ProductTypeName = "Womens Trial Plan Blade";
                        $this->trialPlanAddons_ProductTypeName = "Womens Trial Plan Addons";
                    } else {
                        $this->freeShaveCreamSKU = "A5";
                        $this->trialPlanHandle_ProductTypeName = "Trial Plan Handle";
                        $this->trialPlanBlade_ProductTypeName = "Trial Plan Blade";
                        $this->trialPlanAddons_ProductTypeName = "Trial Plan Addons";
                    }

                }
                $this->langCode = $request->langCode;
                $this->appType = $request->appType;
                $data = [];
                // Get all products from Trial Plan Controller
                $data["free_shave_cream_id"] = $this->getFreeShaveCream_ProductCountriesId();
                $data["handle_products"] = $this->getHandleList();
                $data["blade_products"] = $this->getBladeList();
                $data["frequency_list"] = $this->getFrequencyList($this->genderType, $this->lang_code);
                // return response()->json($data)->header("Access-Control-Allow-Origin", "*");
                $data["addon_products"] = $this->getAddonList();
                $data["allplan"] = $this->getAllTrialPlan();
                $data["allannualplan"] = $this->getAllAnnualTrialPlan('baWebsite', $this->lang_code, $this->country_id);
                $this->country = Countries::where('id', $request->CountryId)->first();
                $this->country_iso = $this->country->codeIso;
                $data["trial_price"] = $this->getTrialPlanDisplayPrice();
                $data["trial_saving_price"] = $this->getTrialPlanDisplaySavingPrice();
                $data["trial_saving_price_list"] = $this->getAllTrialPlanDisplaySavingPrice();
                $data["currency"] = $this->country->currencyDisplay;
                $data["next_refill_date"] = $this->getNextRefillDate();
                $data["country"] = $this->country;
                $plan_type_based_on_handle = [];
                $selected_trial_plan_type = "";
                if (isset($data["allplan"]) && $data["allplan"] !== null) {
                    foreach ($data["allplan"] as $ap) {
                        foreach ($ap["productCountryId"] as $pi) {
                            if (!in_array((int) $pi, $plan_type_based_on_handle)) {
                                array_push($plan_type_based_on_handle, (int) $pi);
                            }
                        }
                    }
                }
                $data["plan_type_based_on_handle"] = $plan_type_based_on_handle;
                return response()->json([
                    'success' => true,
                    'payload' => $data,
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            }
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }

    }

    // Function: Get product sku for free shave cream
    public function getFreeShaveCream_SKU()
    {
        return $this->freeShaveCreamSKU;
    }

    // Function: Get product country id for free shave cream
    public function getFreeShaveCream_ProductCountriesId()
    {
        $freeShaveCreamProduct = Product::where('sku', $this->freeShaveCreamSKU)->first();
        $freeShaveCreamProductCountry = ProductCountry::where('ProductId', $freeShaveCreamProduct->id)
            ->where('CountryId', $this->country_id)
            ->first();
        return $freeShaveCreamProductCountry->id;
    }

    // Function: Get trial plan handles
    public function getHandleList()
    {
        $trialPlanHandle_ProductType = ProductType::where('name', $this->trialPlanHandle_ProductTypeName)->first();
        $handle_products = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanHandle_ProductType->id));
        return $this->product_helper->getProductDefaultImageWithData($handle_products);
    }

    // Function: Get trial plan blades
    public function getBladeList()
    {
        $trialPlanBlade_ProductType = ProductType::where('name', $this->trialPlanBlade_ProductTypeName)->first();
        // return $trialPlanBlade_ProductType;
        $blade_products = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanBlade_ProductType->id));
        $allBlades = $this->product_helper->getProductDefaultImageWithData($blade_products);

        $position = 0;
        foreach ($allBlades as &$blade) {
            $bladeSku = $blade["sku"];
            $blade["position"] = $position;
            if ($bladeSku == "S3/2018") {
                $blade["smallBladeImageUrl"] = "/images/common/checkoutAssets/3blades.png";
                $blade["bladeCount"] = "3";
                $blade["bladeName"] = "3 Blade";
            } else if ($bladeSku == "S5/2018") {
                $blade["smallBladeImageUrl"] = "/images/common/checkoutAssets/5blades.png";
                $blade["bladeCount"] = "5";
                $blade["bladeName"] = "5 Blade";
            } else if ($bladeSku == "S6/2018") {
                $blade["smallBladeImageUrl"] = "/images/common/checkoutAssets/6blades.png";
                $blade["bladeCount"] = "6";
                $blade["bladeName"] = "6 Blade";
            } else if ($bladeSku == "F5") {
                $blade["smallBladeImageUrl"] = "/images/common/checkoutAssetsWomen/5blades-w.png";
                $blade["bladeCount"] = "5";
                $blade["bladeName"] = "5 Blade";
            } else {
                $blade["smallBladeImageUrl"] = null;
                $blade["bladeCount"] = "-";
                $blade["bladeName"] = "- Blade";
            }
            $position++;
        }
        return $allBlades;
    }

    // Function: Get list of frequency
    public function getFrequencyList($gender = 'male', $lang_code)
    {
        $folderpath = $gender !== null && $gender === 'female' ? 'checkoutAssetsWomen' : 'checkoutAssets';
        $w = $gender !== null && $gender === 'female' ? 'w-' : '';
        $durations = config('global.baWebsite.TrialPlan.TrialPlanDuration');
        $frequency_list = [];
        foreach ($durations as $duration) {
            $data = array();
            $data["duration"] = $duration;
            if ($gender !== null && $gender === 'female') {
                $data["image"] = config('global.baWebsite.checkout.trial-w.frequency.' . $duration . '.image');
            } else {
                $data["image"] = config('global.baWebsite.checkout.trial.frequency.' . $duration . '.image');
            }
            $data["durationText"] = Lang::get('website_contents.frequency.' . $duration . '.durationText', ['months' => $duration], $lang_code);
            $data["detailsText"] = Lang::get('website_contents.frequency.' . $duration . '.detailsText', ['months' => $duration], $lang_code);
            $data["selectedDetailsText"] = Lang::get('website_contents.frequency.' . $duration . '.selectedDetailsText', ['months' => $duration], $lang_code);
            array_push($frequency_list, $data);
        }

        return $frequency_list;
    }

    // Function: Get list of addons
    public function getAddonList()
    {
        $trialPlanAddons_ProductType = ProductType::where('name', $this->trialPlanAddons_ProductTypeName)->first();
        $addon_products_temp = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanAddons_ProductType->id));
        $addon_products = array();

        // Get product country id for free shave cream
        $freeShaveCream_ProductCountryId = $this->getFreeShaveCream_ProductCountriesId();

        //Make sure the free shave cream is always on the top
        foreach ($addon_products_temp as $product) {
            if ($product['productcountriesid'] == $freeShaveCream_ProductCountryId) {
                array_push($addon_products, $product);
            }
        }

        foreach ($addon_products_temp as $product) {
            if ($product['productcountriesid'] != $freeShaveCream_ProductCountryId) {
                array_push($addon_products, $product);
            }
        }
        return $addon_products;
    }

    // Function: Get all trial plans based on countryId
    public function getAllTrialPlan()
    {

        $plancategories = $this->genderType !== null && $this->genderType === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanGroup') : config('global.baWebsite.TrialPlan.TrialPlanGroup');
        $plantype = $this->genderType !== null && $this->genderType === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanType') : config('global.baWebsite.TrialPlan.TrialPlanType');
        $allplan = $this->plan_helper->getPlanIdPriceByCategoryBAONLY($this->country_id, $this->lang_code, $plantype, $plancategories);
        return $allplan;
    }

    // Function: Get all trial plans based on countryId
    public function getAllAnnualTrialPlan($appType, $langCode, $CountryId)
    {
        $plancategories = $this->genderType !== null && $this->genderType === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanGroup') : config('global.baWebsite.TrialPlan.TrialPlanGroup');
        $plantype = $this->genderType !== null && $this->genderType === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanType') : config('global.baWebsite.TrialPlan.TrialPlanType');
        $allplan = $this->plan_helper->getPlanIdPriceByCategoryAnnual($CountryId, $langCode, $plantype, $plancategories);
        return response()->json($allplan);
    }

    // Function: Get all trial plans based on countryId
    public function getRelatedAnnualPlan(Request $request)
    {

        $langCode = strtoupper($request->get('langCode'));
        $CountryId = $request->get('CountryId');
        $existing_trial_plan = $request->get('existing_plan_info');
        if (isset($request->gender) && $request->gender == 'female') {
            $plancategories = config('global.baWebsite.TrialPlan-W.TrialPlanGroup');
            $plantype = config('global.baWebsite.TrialPlan-W.TrialPlanType');
        } else {
            $plancategories = config('global.baWebsite.TrialPlan.TrialPlanGroup');
            $plantype = config('global.baWebsite.TrialPlan.TrialPlanType');
        }
        $relevantAnnualPlan = $this->plan_helper->getPlanIdPriceByCategoryAnnualInfo($CountryId, $langCode, $plantype, $plancategories, $existing_trial_plan, isset($request->gender) ? $request->gender : null);

        return response()->json([
            'success' => true,
            'payload' => $relevantAnnualPlan,
        ])
            ->header("Access-Control-Allow-Origin", "*");
    }

    // Function: Get trial plan price by country, used in the main page
    public function getTrialPlanDisplayPrice()
    {
        switch (strtolower($this->country_iso)) {
            case "my":return $this->laravelHelper->ConvertToNDecimalPoints(6.50, 2);
                break;
            case "hk":return $this->laravelHelper->ConvertToNDecimalPoints(46.00, 2);
                break;
            case "sg":return $this->laravelHelper->ConvertToNDecimalPoints(5.00, 2);
                break;
            case "kr":return "4,000";
                break;
            case "tw":return $this->laravelHelper->ConvertToNDecimalPoints(175.00, 2);
                break;
            default:return $this->laravelHelper->ConvertToNDecimalPoints(6.50, 2);
        }
    }

    // Function: Get trial plan saving price by country, used in the main page
    public function getTrialPlanDisplaySavingPrice()
    {
        switch (strtolower($this->country_iso)) {
            case "my":return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
                break;
            case "hk":return $this->laravelHelper->ConvertToNDecimalPoints(55.00, 2);
                break;
            case "sg":return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
                break;
            case "kr":return "15,500";
                break;
            case "tw":return $this->laravelHelper->ConvertToNDecimalPoints(400.00, 2);
                break;
            default:return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
        }
    }
    // Function: Get all trial plan saving price by country, used in the main page
    public function getAllTrialPlanDisplaySavingPrice()
    {
        switch (strtolower($this->country_iso)) {
            case "my":
                return [
                    "male" => [
                        "3" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "5" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "6" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                    ],
                    "female" => [
                        "3" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "5" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "6" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                    ],
                ];
                break;
            case "hk":
                return [
                    "male" => [
                        "3" => $this->laravelHelper->ConvertToNDecimalPoints(55.00, 2),
                        "5" => $this->laravelHelper->ConvertToNDecimalPoints(55.00, 2),
                        "6" => $this->laravelHelper->ConvertToNDecimalPoints(55.00, 2),
                    ],
                    "female" => [],
                ];
                break;
            case "sg":
                return [
                    "male" => [
                        "3" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "5" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "6" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                    ],
                    "female" => [],
                ];
                break;
            case "kr":return "15,500";
                break;
            case "tw":return $this->laravelHelper->ConvertToNDecimalPoints(400.00, 2);
                break;
            default:
                return [
                    "male" => [
                        "3" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "5" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                        "6" => $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2),
                    ],
                    "female" => [],
                ];
                break;
        }
    }

    // Function: Get next refill date
    public function getNextRefillDate()
    {
        return Carbon::now()->addDays(14)->format('d M');
    }

    public function getNextRefillDateCheckout()
    {
        return Carbon::now()->addDays(14)->format('M d');
    }

    // Function: Proceed to checkout page
    public function proceedCheckout(Request $request)
    {
        $this->genderType = isset($request->data["gender"]) ? $request->data["gender"] : null;
        $session_data_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($request->data["session_data"]);
        $langCode = strtoupper($request->data["langCode"]);
        $currentCountryIso = strtolower($request->data["currentCountryIso"]);
        $currentCountryid = (int) $request->data["country_id"];

        if ($request->data["user_id"] !== null) {
            $this->user = $this->userHelper->getUserDetails($request->data["user_id"]);
        } else {
            $this->user = null;
        }

        if ($currentCountryid) {
            if ($this->genderType !== null && $this->genderType === 'female') {
                $this->freeShaveCreamSKU = "A5";
                $this->trialPlanHandle_ProductTypeName = "Womens Trial Plan Handle";
                $this->trialPlanBlade_ProductTypeName = "Womens Trial Plan Blade";
                $this->trialPlanAddons_ProductTypeName = "Womens Trial Plan Addons";
            } else {
                $this->freeShaveCreamSKU = "A5";
                $this->trialPlanHandle_ProductTypeName = "Trial Plan Handle";
                $this->trialPlanBlade_ProductTypeName = "Trial Plan Blade";
                $this->trialPlanAddons_ProductTypeName = "Trial Plan Addons";
            }
        }
        $urllangCode = $request->data["urllangCode"];
        // build tax data
        $tax = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->apicountryservice->tax($currentCountryid));
        $this->currency = $this->apicountryservice->getCurrencyDisplay($currentCountryid);
        $plandescription = $session_data_decoded->selection->step1->selected_blade->producttranslatesname;
        $current_price = $session_data_decoded->selection->summary->current_price;
        $shipping_fee = $session_data_decoded->selection->summary->shipping_fee;
        $shipping_fee = number_format($shipping_fee, 2);
        $next_price = $session_data_decoded->selection->summary->next_price;
        $current_totalprice = $current_price + $shipping_fee;
        $next_totalprice = $next_price + $shipping_fee;
        $taxAmount = $tax->taxRate / 100 * $next_totalprice;
        $next_totalprice = $next_totalprice + $taxAmount;
        $totalBlade = $session_data_decoded->selection->step1->selected_blade->bladeCount;
        $currentShippingImage = $session_data_decoded->selection->current_shipping_image;
        $nextBillingImage = $session_data_decoded->selection->next_billing_image;

        if ($session_data_decoded) {

            if (array_key_exists('planId', $session_data_decoded->selection->summary)) {
                // Next billing subsciption name
                $subscriptionName = "";
                $subscriptionName .= $totalBlade . " Blades";
                $addonList = $this->genderType === 'female' ? null : $session_data_decoded->selection->step2->selected_addon_list;
                if ($addonList !== null) {
                    foreach ($addonList as $addon) {
                        if ($addon->sku === $this->freeShaveCreamSKU) {
                            if ($session_data_decoded->selection->has_shave_cream_next_billing) {
                                $subscriptionName .= "<br>" . $addon->producttranslatesname;
                            }
                        } else {
                            $subscriptionName .= "<br>" . $addon->producttranslatesname;
                        }
                    }
                }

                $states = $this->apicountryservice->getAllStates($currentCountryid);

                if ($currentCountryid != 8) {

                    $payment_intent = $this->api_stripeController->api_initializePaymentIntents($this->user, $session_data_decoded, $langCode, $currentCountryid, $this->genderType);
                    $generate_payment_intent = $payment_intent["generate_payment_intent"];
                    $checkout_details = $payment_intent["plandetails"];
                } else {
                    $payment_intent = "";
                    $generate_payment_intent = "";
                    $checkout_details = "";
                }

                $data = [
                    'currentCountryIso' => $currentCountryIso,
                    'currentCountryid' => $currentCountryid,
                    'currency' => $this->currency,
                    'langCode' => $langCode,
                    'urllangCode' => $urllangCode,
                    'states' => $states,
                    'session_data' => json_encode($session_data_decoded),
                    'payment_intent' => $generate_payment_intent,
                    'checkout_details' => $checkout_details,
                    'plandescription' => $plandescription,
                    'current_price' => $current_price,
                    'shipping_fee' => $shipping_fee,
                    'next_price' => $next_price,
                    'current_totalprice' => $current_totalprice,
                    'next_totalprice' => $next_totalprice,
                    'taxAmount' => $taxAmount,
                    'taxRate' => $tax->taxRate,
                    'nextBillingDate' => $this->getNextRefillDateCheckout(),
                    'planFrequency' => $session_data_decoded->selection->step3->selected_frequency,
                    'subscriptionName' => $subscriptionName,
                    'currentShippingImage' => $currentShippingImage,
                    'nextBillingImage' => $nextBillingImage,
                    'gender' => $this->genderType,
                ];

                return response()->json([
                    'success' => true,
                    'payload' => $data,
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            } else {
                return response()->json([
                    'success' => true,
                    'payload' => '',
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            }
        } else {
            return response()->json([
                'success' => true,
                'payload' => '',
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    // generate new paymentIntents for BA
    public function generatePaymentIntents(Request $request)
    {
        $userdetails = $request->get('userdetails');
        $session_selection = $request->get('session_selection');
        $current_country = $request->get('current_country');
        $langCode = $request->get('langCode');
        $payment_intent = $this->api_stripeController->apiGeneratePaymentIntents($userdetails, $session_selection, strtolower($langCode), $current_country["id"], isset($request->gender) ? $request->gender : null);
        return response()->json($payment_intent)->header("Access-Control-Allow-Origin", "*");
    }

    // Function: Proceed to thank you page
    public function proceedThankYou(Request $request)
    {
        return view('plans.trial.trial-plan-thankyou');
    }
}
