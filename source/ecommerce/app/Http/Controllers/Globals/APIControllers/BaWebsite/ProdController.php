<?php

namespace App\Http\Controllers\Globals\APIControllers\BaWebsite;

// Controllers & Services
use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\StripeController;
use App\Http\Controllers\Globals\Payment\APIStripeController;

// Models
use App\Models\GeoLocation\Countries;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductType;
use App\Services\APICountryService;

// Helpers
use Exception;
use Illuminate\Http\Request;

class ProdController extends Controller
{
    public $country_service;
    public $lang_code;
    public $country;
    public $country_id;

    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->apicountryservice = new APICountryService();
        $this->api_stripeController = new APIStripeController();
    }

    // get products and plans based on male or female
    public function getListsBasedOnGender(Request $request)
    {
        try {
            $data = (Object) array();

            $options = (Object) array(
                "CountryId" => $request->CountryId,
                "langCode" => $request->langCode,
                "appType" => $request->appType,
                "gender" => $request->gender,
            );

            $data->gender = $options->gender;

            // get trial plan lists
            $filtered_tk_plans = $this->planHelper->getTrialPlansBasedOnGenderCount($options);
            $data->filtered_tk_plans = $filtered_tk_plans->original;

            // get custom plan lists
            $filtered_cp_plans = $this->planHelper->getCustomPlansBasedOnGenderCount($options);
            $data->filtered_cp_plans = $filtered_cp_plans->original;

            // get product lists
            $filtered_products = $this->productHelper->getProductsBasedOnGenderCount($options);
            $data->filtered_products = $filtered_products->original;

            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    // get basic info of the product types
    public function getMainProductTypeListsInfo(Request $request)
    {
        try {
            $data = (Object) array();

            $options = (Object) array(
                "CountryId" => (int) $request->CountryId,
                "langCode" => $request->langCode,
                "appType" => $request->appType,
                "gender" => $request->gender,
            );

            $data->gender = $options->gender;

            // get trial plan lists
            $filtered_tk_plans = $this->planHelper->getTrialPlansBasedOnGenderBasicInfo($options);
            $data->filtered_tk_plans = $filtered_tk_plans->original;

            // get custom plan lists
            $filtered_cp_plans = $this->planHelper->getCustomPlansBasedOnGenderBasicInfo($options);
            $data->filtered_cp_plans = $filtered_cp_plans->original;

            // get product lists
            $filtered_products = $this->productHelper->getProductsBasedOnGenderBasicInfo($options);
            $data->filtered_products = $filtered_products->original;
            $data->request_options = $options;
            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function getItemListsBasedOnGender(Request $request)
    {
        try {
            $data = (Object) array();

            $options = (Object) array(
                "CountryId" => (int) $request->CountryId,
                "langCode" => $request->langCode,
                "appType" => $request->appType,
                "gender" => $request->gender,
            );

            $data->gender = $options->gender;

            // get product lists
            $filtered_products = $this->productHelper->getProductsBasedOnGender($options);
            $data->filtered_products = $filtered_products->original;
            $data->request_options = $options;

            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    // generate new paymentIntents for BA
    public function generatePaymentIntents(Request $request)
    {

        $userdetails = $request->get('userdetails');
        $session_selection = $request->get('session_selection');
        $current_country = $request->get('current_country');
        $langCode = $request->get('langCode');
        $payment_intent = $this->api_stripeController->apiGeneratePaymentIntentsAlacarte($userdetails, $session_selection, strtolower($langCode), $current_country["id"]);
        return response()->json($payment_intent)->header("Access-Control-Allow-Origin", "*");
    }
    
    // Function: Proceed to checkout page
    public function proceedAlacarteCheckout(Request $request)
    {
        $session_data_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($request->data["session_data"]);
        $langCode = strtoupper($request->data["langCode"]);
        $currentCountryIso = strtolower($request->data["currentCountryIso"]);
        $currentCountryid = (int) $request->data["country_id"];

        $urllangCode = $request->data["urllangCode"];
        // build tax data
        $tax = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->apicountryservice->tax($currentCountryid));
        $this->currency = $this->apicountryservice->getCurrencyDisplay($currentCountryid);
        $shipping_fee = $session_data_decoded->selection->summary->shipping_fee;
        $shipping_fee = number_format($shipping_fee, 2);
        $current_price = $session_data_decoded->selection->summary->current_price;
        $shipping_price = $current_price + $shipping_fee;
        $taxAmount = $tax->taxRate / 100 * $shipping_price;

        if ($session_data_decoded) {
            $states = $this->apicountryservice->getAllStates($currentCountryid);

            if ($currentCountryid != 8) {
                $payment_intent = $this->api_stripeController->api_initializePaymentIntentsAlacarte(null, $session_data_decoded, $langCode, $currentCountryid);
                $generate_payment_intent = $payment_intent["generate_payment_intent"];
                $checkout_details = $payment_intent["productcountries"];
            } else {
                $payment_intent = "";
                $generate_payment_intent = "";
                $checkout_details = "";
            }

            $data = [
                'currentCountryIso' => $currentCountryIso,
                'currentCountryid' => $currentCountryid,
                'currency' => $this->currency,
                'langCode' => $langCode,
                'urllangCode' => $urllangCode,
                'states' => $states,
                'session_data' => json_encode($session_data_decoded),
                'payment_intent' => $generate_payment_intent,
                'checkout_details' => $checkout_details,
                'shipping_fee' => $shipping_fee,
                'current_price' => $current_price,
                'shipping_price' => $shipping_price,
                'taxAmount' => $taxAmount,
                'taxRate' => $tax->taxRate,
            ];

            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        } else {
            return response()->json([
                'success' => true,
                'payload' => '',
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }
}
