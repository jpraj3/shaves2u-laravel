<?php

namespace App\Http\Controllers\Globals\APIControllers\BaWebsite;

use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;

class PGController extends Controller
{
    public function __construct()
    {
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
    }

    // get products and plans based on male or female
    public function getListsBasedOnGender(Request $request)
    {
        try {
            $data = (Object) array();

            $options = (Object) array(
                "CountryId" => $request->CountryId,
                "langCode" => $request->langCode,
                "appType" => $request->appType,
                "gender" => $request->gender,
            );

            $data->gender = $options->gender;

            // get trial plan lists
            $filtered_tk_plans = $this->planHelper->getTrialPlansBasedOnGenderCount($options);
            $data->filtered_tk_plans = $filtered_tk_plans->original;

            // get custom plan lists
            $filtered_cp_plans = $this->planHelper->getCustomPlansBasedOnGenderCount($options);
            $data->filtered_cp_plans = $filtered_cp_plans->original;

            // get product lists
            $filtered_products = $this->productHelper->getProductsBasedOnGenderCount($options);
            $data->filtered_products = $filtered_products->original;

            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    // get basic info of the product types
    public function getMainProductTypeListsInfo(Request $request)
    {
        try {
            $data = (Object) array();

            $options = (Object) array(
                "CountryId" => (int) $request->CountryId,
                "langCode" => $request->langCode,
                "appType" => $request->appType,
                "gender" => $request->gender,
            );

            $data->gender = $options->gender;

            // get trial plan lists
            $filtered_tk_plans = $this->planHelper->getTrialPlansBasedOnGenderBasicInfo($options);
            $data->filtered_tk_plans = $filtered_tk_plans->original;

            // get custom plan lists
            $filtered_cp_plans = $this->planHelper->getCustomPlansBasedOnGenderBasicInfo($options);
            $data->filtered_cp_plans = $filtered_cp_plans->original;

            // get product lists
            $filtered_products = $this->productHelper->getProductsBasedOnGenderBasicInfo($options);
            $data->filtered_products = $filtered_products->original;
            $data->request_options = $options;
            return response()->json([
                'success' => true,
                'payload' => $data,
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function getItemListsBasedOnGender()
    {

    }

}
