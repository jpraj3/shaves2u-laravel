<?php

namespace App\Http\Controllers\Globals\APIControllers\BaWebsite;

use App\Http\Controllers\Controller;
use App\Models\BaWebsite\SellerUser;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AuthBaController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | BA Login Controller
    |--------------------------------------------------------------------------
     */
    use AuthenticatesUsers;
    protected $guard = 'bawebsite';
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:bawebsite')->except('logout');
    }

    // public function username()
    // {
    //     return 'email';
    // }

    // protected function guard()
    // {
    //     return Auth::guard('bawebsite');
    // }

    // protected function validateLogin(Request $request)
    // {
    //     $request->validate([
    //         $this->username() => 'required',
    //         'password' => 'required',
    //     ]);
    // }

    // protected function authenticated(Request $request, $user)
    // {
    //     //
    // }

    // public function login($request)
    // {
    //     return $this->sendLoginResponse($request);

    //     // // This section is the only change
    //     // if ($this->guard()->validate($this->credentials($request))) {
    //     //     // $user = $this->guard()->getLastAttempted();
    //     //     return response()->json([
    //     //         'success' => true,
    //     //         'payload' => 'asd',
    //     //     ]);
    //     //     // // Make sure the user is active
    //     //     // if ($user->isActive && $this->attemptLogin($request)) {
    //     //     //     // Send the normal successful login response
    //     //     //     return $this->sendLoginResponse($request);
    //     //     // } else {
    //     //     //     // Increment the failed login attempts and redirect back to the
    //     //     //     // login form with an error message.
    //     //     //     $this->incrementLoginAttempts($request);
    //     //     //     return redirect()
    //     //     //         ->back()
    //     //     //         ->withInput($request->only($this->username(), 'remember'))
    //     //     //         ->withErrors(['active' => 'The email must be activated to login.']);
    //     //     // }
    //     // } else {
    //     //     return response()->json([
    //     //         'success' => false,
    //     //         'payload' => 'fail',
    //     //     ]);
    //     // }

    //     // // If the login attempt was unsuccessful we will increment the number of attempts
    //     // // to login and redirect the user back to the login form. Of course, when this
    //     // // user surpasses their maximum number of attempts they will get locked out.
    //     // $this->incrementLoginAttempts($request);

    //     // return $this->sendFailedLoginResponse($request);
    // }

    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     throw ValidationException::withMessages([
    //         $this->username() => [trans('auth.failed')],
    //     ]);
    // }

    public function authenticate(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('bawebsite')->attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::login(auth('bawebsite')->user());
            session()->put('seller', auth('bawebsite')->user());

            return response()->json([
                'success' => true,
                'payload' => auth('bawebsite')->user(),
            ])
                ->header("Access-Control-Allow-Origin", "*");

        } else {
            return response()->json([
                'success' => false,
                'payload' => null,
                "error" => [
                    "code" => 123,
                    "message" => "An error occurred!",
                ],
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    // protected function attemptLogin(Request $request)
    // {
    //     return $this->guard()->attempt(
    //         $this->credentials($request),
    //         false
    //     );
    // }

    // protected function credentials(Request $request)
    // {
    //     return $request->only('badgeId', 'icNumber');
    // }

    // protected function sendLoginResponse(Request $request)
    // {
    //     $request->session()->regenerate();

    //     $this->clearLoginAttempts($request);

    //     return response()->json([
    //         'success' => true,
    //         'payload' => $this->guard('bawebsite')->attempt(['badgeId' => 1,'icNumber'=> 1]),
    //     ])
    //         ->header("Access-Control-Allow-Origin", "*");
    //     // return $this->authenticated($request, $this->guard()->user())
    //     // ?: redirect()->intended('/');
    // }

    // protected function incrementLoginAttempts(Request $request)
    // {
    //     $this->limiter()->hit(
    //         $this->throttleKey($request), $this->decayMinutes() * 60
    //     );
    // }

    public function checkAgentExists(Request $request)
    {

        $exists = SellerUser::where('email', $request->email)
            ->where('isActive', 1)
            ->first();

        return response()
            ->json($exists)
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function logout(Request $request)
    {
        $this->guard('bawebsite')->logout();

        $request->session()->invalidate();

        return response()->json([
            'success' => true,
            'payload' => auth('bawebsite')->user(),
        ])
            ->header("Access-Control-Allow-Origin", "*");
    }

    // protected function loggedOut(Request $request)
    // {
    //     //
    // }
}
