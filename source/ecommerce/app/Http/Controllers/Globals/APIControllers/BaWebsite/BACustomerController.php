<?php

namespace App\Http\Controllers\Globals\APIControllers\BaWebsite;

use App\Helpers\UserHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\User;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;

class BACustomerController extends Controller
{
    public function __construct()
    {
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->userHelper = new UserHelper();
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    public function checkEmailExists(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $type = $request->type;
        $appType = $request->appType;
        $passwordhash = md5($request->password);
        if ($appType === "baWebsite") {
            $checkEmail = User::where('email', $request->email)->first();
            if (!empty($checkEmail)) {
                if(isset($checkEmail->password) && $checkEmail->password ==  $passwordhash){
                    $response = ["user_data" => $checkEmail, "isActive" => $checkEmail["isActive"]];
                    return response()->json([
                        'success' => true,
                        'payload' => $response,
                    ])
                        ->header("Access-Control-Allow-Origin", "*");
                }else{
                    return response()->json([
                        'success' => false,
                        'payload' => 'not exist',
                        'error' => trans('auth.failed')
                    ])
                        ->header("Access-Control-Allow-Origin", "*");
                }
             
            } else {
                $response = ["user_data" => null, "isActive" => null];
                return response()->json([
                    'success' => true,
                    'payload' => $response,
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            }
        }
    }

    public function checkPhoneExists(Request $request)
    {
        $cphone = $request->cphone;
        $appType = $request->appType;

        if ($appType === "baWebsite") {
            $checkphone = User::where('phone', $cphone)->first();
            if (!empty($checkphone)) {
                $response = ["used" => "Yes"];
                return response()->json([
                    'success' => true,
                    'payload' => $response,
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            } else {
                $response = ["used" => "No"];
                return response()->json([
                    'success' => true,
                    'payload' => $response,
                ])
                    ->header("Access-Control-Allow-Origin", "*");
            }
        }
    }

    // Function: Check whether the user already purchase trial kit before
    public function checkTrialPlanExist(Request $request)
    {
        $userId = $request->userid;
        $trialPlanCount = Subscriptions::where('UserId', $userId)->where('isTrial', 1)->whereIn('status', ['Cancelled', 'On Hold', 'Unrealized', 'Processing'])->count();
        if ($trialPlanCount > 0) {
            return response()->json([
                'success' => true,
                'payload' => 'exist',
            ])
                ->header("Access-Control-Allow-Origin", "*");
        } else {
            return response()->json([
                'success' => true,
                'payload' => 'not exist',
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function registerBAUsers(Request $request)
    {
        try {
            $_phone = '';
            // // Get new country info from session
            if ($request->name == str_replace(' ', '', $request->name)) {
                $first_name = $request->name;
                $last_name = null;
            } else {
                $first_name = explode(' ', $request->name)[0];
                $last_name = explode(' ', $request->name)[1];
            }

            if (isset($request->phone)) {
                if (strpos($request->phone, '+') !== false) {
                    $_phone = $request->phone;
                } else {
                    $_phone = '+' . $request->phone;
                }
            }

            $user = User::create([
                'firstName' => $request->name,
                'lastName' => null,
                'email' => $request->email,
                'password' => null,
                'socialId' => null,
                'birthday' => $request->birthday,
                'gender' => $request->gender,
                'phone' => $_phone,
                'phoneHome' => $_phone,
                'SSN' => null,
                'defaultLanguage' => $request->current_locale,
                'defaultDateFormat' => 'yyyy/MM/dd',
                'notificationSettings' => [],
                'isActive' => 0,
                'CountryId' => $request->current_country["id"],
                'badgeId' => $request->seller_info["badgeId"],
                'isGuest' => 0,
                'Email_Sub' => $request->marketing_sub_agree != null ? 1 : ($request->email_sub_agree != null ? 1 : 0),
                'Sms_Sub' => $request->marketing_sub_agree != null ? 1 : ($request->sms_sub_agree != null ? 1 : 0),
                'HKG_Marketing_Sub' => 0,
            ]);

            $user_data = User::where('id', $user->id)->first();

            $currentCountry =  $request->current_country;
            $currentLocale =  $request->current_locale;
            $email = [];
            $email['email'] = $request->email;
            $email['appType'] = config('app.appType');
            // Initiate New CustomerRegisteredEvent
            // hide email redis
            $this->emailQueueHelper->buildEmailTemplate('welcome', $email, array($currentCountry, $currentLocale));

            return response()->json([
                'success' => true,
                'payload' => $user_data,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        } catch (Exception $e) {

            return response()->json([
                'success' => true,
                'payload' => $e,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function addDelivery(Request $request)
    {

        if ($request) {
            $user = User::where('id', $request->user_id)->first();
            if ($user) {
                // Name -> First & Last Name
                $fullname = explode(" ", $request->delivery_name);
                if (count($fullname) >= 2) {
                    $fName = explode(" ", $request->delivery_name)[0];
                    $lName = explode(" ", $request->delivery_name)[1];
                } else {
                    $fName = explode(" ", $request->delivery_name)[0];
                    $lName = "";
                }

                if (!isset($request->isDifferentBilling)) {
                    $delivery = new DeliveryAddresses();
                    $delivery->firstName = $request->delivery_name;
                    $delivery->lastName = null;
                    $delivery->fullName = $request->delivery_name;
                    $delivery->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                    $delivery->SSN = $request->SSN ? $request->SSN : null;
                    $delivery->state = $request->delivery_state;
                    $delivery->address = $request->delivery_address;
                    if ($request->delivery_postcode != null) {
                        $delivery->portalCode = $request->delivery_postcode;
                    }
                    $delivery->city = $request->delivery_city;
                    $delivery->company = null;
                    $delivery->isRemoved = 0;
                    $delivery->isBulkAddress = 0;
                    $delivery->CountryId = $user->CountryId;
                    $delivery->UserId = $user->id;
                    $delivery->SellerUserId = null;
                    $delivery->createdBy = null;
                    $delivery->updateBy = null;
                    $delivery->created_at = $this->now;
                    $delivery->updated_at = $this->now;

                    $delivery->save();

                    $billing = new DeliveryAddresses();
                    $billing->firstName = $request->delivery_name;
                    $billing->lastName = null;
                    $billing->fullName = $request->delivery_name;
                    $billing->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                    $billing->SSN = $request->SSN ? $request->SSN : null;
                    $billing->state = $request->delivery_state;
                    $billing->address = $request->delivery_address;
                    if ($request->delivery_postcode != null) {
                        $billing->portalCode = $request->delivery_postcode;
                    }
                    $billing->city = $request->delivery_city;
                    $billing->company = null;
                    $billing->isRemoved = 0;
                    $billing->isBulkAddress = 0;
                    $billing->CountryId = $user->CountryId;
                    $billing->UserId = $user->id;
                    $billing->SellerUserId = null;
                    $billing->createdBy = null;
                    $billing->updateBy = null;
                    $billing->created_at = $this->now;
                    $billing->updated_at = $this->now;

                    $billing->save();

                    if (($user->phoneHome === null && $user->phone === null) || ($user->phoneHome === "" && $user->phone === "") || ($user->phoneHome === null && $user->phone === "") || ($user->phoneHome === "" && $user->phone === null)) {
                        User::where('id', $user->id)->update(array(
                            'defaultShipping' => $delivery->id,
                            'defaultBilling' => $billing->id,
                            'phoneHome' => $request->delivery_phone,
                            'phone' => $request->delivery_phoneext . $request->delivery_phone,
                        ));
                    } else {
                        User::where('id', $user->id)->update(array(
                            'defaultShipping' => $delivery->id,
                            'defaultBilling' => $billing->id,
                        ));
                    }

                    $user_details = $this->userHelper->getUserDetails($user->id);

                    return response()->json($user_details)
                        ->header("Access-Control-Allow-Origin", "*");
                } else {

                    $delivery = new DeliveryAddresses();
                    $delivery->firstName = $request->delivery_name;
                    $delivery->lastName = null;
                    $delivery->fullName = $request->delivery_name;
                    $delivery->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                    $delivery->SSN = $request->SSN ? $request->SSN : null;
                    $delivery->state = $request->delivery_state;
                    $delivery->address = $request->delivery_address;
                    if ($request->delivery_postcode != null) {
                        $delivery->portalCode = $request->delivery_postcode;
                    }
                    $delivery->city = $request->delivery_city;
                    $delivery->company = null;
                    $delivery->isRemoved = 0;
                    $delivery->isBulkAddress = 0;
                    $delivery->CountryId = $user->CountryId;
                    $delivery->UserId = $user->id;
                    $delivery->SellerUserId = null;
                    $delivery->createdBy = null;
                    $delivery->updateBy = null;
                    $delivery->created_at = $this->now;
                    $delivery->updated_at = $this->now;

                    $delivery->save();

                    $billing = new DeliveryAddresses();
                    $billing->firstName = $request->delivery_name;
                    $billing->lastName = null;
                    $billing->fullName = $request->delivery_name;
                    $billing->contactNumber = $request->billing_phone ? $request->billing_phoneext . $request->billing_phone : $request->delivery_phoneext . $request->delivery_phone;
                    $billing->SSN = $request->SSN ? $request->SSN : null;
                    $billing->state = $request->billing_state ? $request->billing_state : $request->delivery_state;
                    $billing->address = $request->billing_address ? $request->billing_address : $request->delivery_address;
                    if ($request->billing_postcode != null) {
                        if ($request->billing_postcode) {
                            $billing->portalCode = $request->billing_postcode;
                        } else {
                            if ($request->delivery_postcode != null) {
                                $billing->portalCode =  $request->delivery_postcode;
                            }
                        }
                    }
                    $billing->city = $request->billing_city ? $request->billing_city : $request->delivery_city;
                    $billing->company = null;
                    $billing->isRemoved = 0;
                    $billing->isBulkAddress = 0;
                    $billing->CountryId = $user->CountryId;
                    $billing->UserId = $user->id;
                    $billing->SellerUserId = null;
                    $billing->createdBy = null;
                    $billing->updateBy = null;
                    $billing->created_at = $this->now;
                    $billing->updated_at = $this->now;

                    $billing->save();

                    if (($user->phoneHome === null && $user->phone === null) || ($user->phoneHome === "" && $user->phone === "") || ($user->phoneHome === null && $user->phone === "") || ($user->phoneHome === "" && $user->phone === null)) {
                        User::where('id', $user->id)->update(array(
                            'defaultShipping' => $delivery->id,
                            'defaultBilling' => $billing->id,
                            'phoneHome' => $request->delivery_phone,
                            'phone' => $request->delivery_phoneext . $request->delivery_phone,
                        ));
                    } else {
                        User::where('id', $user->id)->update(array(
                            'defaultShipping' => $delivery->id,
                            'defaultBilling' => $billing->id,
                        ));
                    }

                    $user_details = $this->userHelper->getUserDetails($user->id);

                    return response()->json($user_details)
                        ->header("Access-Control-Allow-Origin", "*");
                }
            }
        }
    }

    public function editDelivery(Request $request)
    {
        // return response()->json($request)->header("Access-Control-Allow-Origin", "*");
        if ($request) {
            if ($request->user_id) {
                // Name -> First & Last Name
                $user = User::where('id', $request->user_id)->first();
                if ($user) {
                    $fullname = explode(" ", $request->delivery_name);
                    if (count($fullname) >= 2) {
                        $fName = explode(" ", $request->delivery_name)[0];
                        $lName = explode(" ", $request->delivery_name)[1];
                    } else {

                        $fName = explode(" ", $request->delivery_name)[0];
                        $lName = "";
                    }

                    if (!isset($request->isDifferentBilling)) {
                        $pcode = '00000';
                        if ($request->delivery_postcode != null) {
                            $pcode = $request->delivery_postcode;
                        }
                        $delivery = DeliveryAddresses::where('id', $user->defaultShipping)
                            ->update(
                                array(
                                    'firstName' => $request->delivery_name,
                                    'lastName' => null,
                                    'fullName' => $request->delivery_name,
                                    'contactNumber' => $request->delivery_phoneext . $request->delivery_phone,
                                    'SSN' => $request->SSN ? $request->SSN : null,
                                    'state' => $request->delivery_state,
                                    'address' => $request->delivery_address,
                                    'portalCode' => $pcode,
                                    'city' => $request->delivery_city,
                                    'isRemoved' => 0,
                                    'updated_at' => $this->now,
                                )
                            );
                        User::where('id', $user->id)->update(array(
                            'defaultShipping' => $user->defaultShipping,
                        ));

                        $user_details = $this->userHelper->getUserDetails($user->id);

                        $address = [
                            "delivery_address" => end($user_details["delivery_address"]), "billing_address" => end($user_details["delivery_address"]), "diff" => "0",
                        ];

                        return response()->json($address)
                            ->header("Access-Control-Allow-Origin", "*");
                    } else {
                        $pcode = '00000';
                        if ($request->delivery_postcode != null) {
                            $pcode = $request->delivery_postcode;
                        }

                        DeliveryAddresses::where('id', $user->defaultShipping)
                            ->update(
                                array(
                                    'firstName' => $request->delivery_name,
                                    'lastName' => null,
                                    'fullName' => $request->delivery_name,
                                    'contactNumber' => $request->delivery_phoneext . $request->delivery_phone,
                                    'SSN' => $request->SSN ? $request->SSN : null,
                                    'state' => $request->delivery_state,
                                    'address' => $request->delivery_address,
                                    'portalCode' => $pcode,
                                    'city' => $request->delivery_city,
                                    'isRemoved' => 0,
                                    'updated_at' => $this->now,
                                )
                            );
                        if ($user->defaultShipping == $user->defaultBilling) {
                            $billing = new DeliveryAddresses();
                            $billing->firstName = $request->delivery_name;
                            $billing->lastName = null;
                            $billing->fullName = $request->delivery_name;
                            $billing->contactNumber = $request->billing_phone ? $request->billing_phoneext . $request->billing_phone : $request->delivery_phoneext . $request->delivery_phone;
                            $billing->SSN = $request->SSN ? $request->SSN : null;
                            $billing->state = $request->billing_state ? $request->billing_state : $request->delivery_state;
                            $billing->address = $request->billing_address ? $request->billing_address : $request->delivery_address;
                            if ($request->billing_postcode != null) {
                                if ($request->billing_postcode) {
                                    $billing->portalCode = $request->billing_postcode;
                                } else {
                                    if ($request->delivery_postcode != null) {
                                        $billing->portalCode =  $request->delivery_postcode;
                                    }
                                }
                            }

                            $billing->city = $request->billing_city ? $request->billing_city : $request->delivery_city;
                            $billing->company = null;
                            $billing->isRemoved = 0;
                            $billing->isBulkAddress = 0;
                            $billing->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                            $billing->UserId = $user->id;
                            $billing->SellerUserId = null;
                            $billing->createdBy = null;
                            $billing->updateBy = null;
                            $billing->created_at = $this->now;
                            $billing->updated_at = $this->now;

                            $billing->save();

                            User::where('id', $user->id)->update(array(
                                'defaultBilling' => $billing->id,
                            ));
                        } else {
                            $pcode = '00000';

                            if ($request->billing_postcode != null) {
                                if ($request->billing_postcode) {
                                    $pcode = $request->billing_postcode;
                                } else {
                                    if ($request->delivery_postcode != null) {
                                        $pcode =  $request->delivery_postcode;
                                    }
                                }
                            }

                            $billing = DeliveryAddresses::where('id', $user->defaultBilling)
                                ->update(
                                    array(
                                        'firstName' => $request->delivery_name,
                                        'lastName' => null,
                                        'fullName' => $request->delivery_name,
                                        'contactNumber' => $request->billing_phone ? $request->billing_phoneext . $request->billing_phone : $request->delivery_phoneext . $request->delivery_phone,
                                        'SSN' => $request->SSN ? $request->SSN : null,
                                        'state' => $request->billing_state ? $request->billing_state : $request->delivery_state,
                                        'address' => $request->billing_address ? $request->billing_address : $request->delivery_address,
                                        'portalCode' => $pcode,
                                        'city' => $request->billing_city ? $request->billing_city : $request->delivery_city,
                                        'isRemoved' => 0,
                                        'updated_at' => $this->now,
                                    )
                                );
                        }
                        $user_details = $this->userHelper->getUserDetails($user->id);

                        $address = ["delivery_address" => end($user_details["delivery_address"]), "billing_address" => end($user_details["billing_address"]), "diff" => "1"];

                        return response()->json($address)
                            ->header("Access-Control-Allow-Origin", "*");
                    }
                }
            }
        }
    }
}
