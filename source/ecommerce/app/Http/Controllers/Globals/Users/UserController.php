<?php

namespace App\Http\Controllers\Globals\Users;

use App\Helpers\UserHelper;
use App\Http\Controllers\Auth\LoginController as UserLoginController;
use App\Http\Controllers\Auth\RegisterController as UserRegisterController;
use App\Http\Controllers\Controller as Controller;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct(Application $app, Request $request)
    {

        $this->app = $app;
        $this->request = $request;
        $this->current_country = json_decode(session()->get('currentCountry'), true);
        $this->country_id = $this->current_country['id'];
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->userHelper = new UserHelper();
        $this->userLoginController = new UserLoginController($app, $request);
        $this->userRegisterController = new UserRegisterController($request);
    }
    protected function guard()
    {
        return Auth::guard();
    }

    public function activate($lang, $countcode, $email)
    {
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtolower($currentCountryData['codeIso']);
        $urlLang = strtolower($currentCountryData['urlLang']);
        $langCode = strtolower(app()->getLocale());
        $useremail = \Crypt::decrypt($email);
        $updateuser = User::where('email', $useremail)
            ->where('isActive', 0)
            ->update([
                'isActive' => 1,
            ]);
        if ($updateuser == 1) {
            if (Auth::check()) {
                $this->guard()->logout();
            }
            $user = User::where('email', $useremail)
                ->first();
            $this->guard()->login($user);

            return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
        } else {
            return redirect()->route('locale.index');
        }

        // sample old site link to activate
        // https://shaves2u.com/users/reset-password?email=andriodbenson@gmail.com&token=[$token]&isActive=true&nextPage=/user/subscriptions&utm_source=mandrill&utm_medium=email&utm_campaign=_mys_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web
    }

    public function create()
    { }

    public function add(Request $request)
    {
        if ($request) {

            // Name -> First & Last Name
            $fullname = explode(" ", $request->delivery_name);
            if (count($fullname) >= 2) {
                $fName = explode(" ", $request->delivery_name)[0];
                $lName = explode(" ", $request->delivery_name)[1];
            } else {
                $fName = explode(" ", $request->delivery_name)[0];
                $lName = "";
            }

            if (!isset($request->isDifferentBilling)) {
                $delivery = new DeliveryAddresses();
                $delivery->firstName = $request->delivery_name;
                $delivery->lastName = null;
                $delivery->fullName = $request->delivery_name;
                $delivery->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                $delivery->SSN = $request->SSN ? $request->SSN : null;
                $delivery->state = $request->delivery_state;
                $delivery->address = $request->delivery_address;
                if ($request->delivery_postcode != null) {
                    $delivery->portalCode = $request->delivery_postcode;
                }
                $delivery->city = $request->delivery_city;
                $delivery->company = null;
                $delivery->isRemoved = 0;
                $delivery->isBulkAddress = 0;
                $delivery->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                $delivery->UserId = Auth::user()->id;
                $delivery->SellerUserId = null;
                $delivery->createdBy = null;
                $delivery->updateBy = null;
                $delivery->created_at = $this->now;
                $delivery->updated_at = $this->now;

                $delivery->save();

                $billing = new DeliveryAddresses();
                $billing->firstName = $request->delivery_name;
                $billing->lastName = null;
                $billing->fullName = $request->delivery_name;
                $billing->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                $billing->SSN = $request->SSN ? $request->SSN : null;
                $billing->state = $request->delivery_state;
                $billing->address = $request->delivery_address;
                if ($request->delivery_postcode != null) {
                    $billing->portalCode = $request->delivery_postcode;
                }
                $billing->city = $request->delivery_city;
                $billing->company = null;
                $billing->isRemoved = 0;
                $billing->isBulkAddress = 0;
                $billing->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                $billing->UserId = Auth::user()->id;
                $billing->SellerUserId = null;
                $billing->createdBy = null;
                $billing->updateBy = null;
                $billing->created_at = $this->now;
                $billing->updated_at = $this->now;

                $billing->save();

                if ((Auth::user()->phoneHome === null && Auth::user()->phone === null) || (Auth::user()->phoneHome === "" && Auth::user()->phone === "") || (Auth::user()->phoneHome === null && Auth::user()->phone === "") || (Auth::user()->phoneHome === "" && Auth::user()->phone === null)) {
                    User::where('id', Auth::user()->id)->update(array(
                        'defaultShipping' => $delivery->id,
                        'defaultBilling' => $billing->id,
                        'phoneHome' => $request->delivery_phone,
                        'phone' => $request->delivery_phoneext . $request->delivery_phone,
                    ));
                } else {

                    User::where('id', Auth::user()->id)->update(array(
                        'defaultShipping' => $delivery->id,
                        'defaultBilling' => $billing->id,
                    ));
                }

                $user_details = $this->userHelper->getUserDetails(Auth::user()->id);

                return $user_details;
            } else {

                $delivery = new DeliveryAddresses();
                $delivery->firstName = $request->delivery_name;
                $delivery->lastName = null;
                $delivery->fullName = $request->delivery_name;
                $delivery->contactNumber = $request->delivery_phoneext . $request->delivery_phone;
                $delivery->SSN = $request->SSN ? $request->SSN : null;
                $delivery->state = $request->delivery_state;
                $delivery->address = $request->delivery_address;
                if ($request->delivery_postcode != null) {
                    $delivery->portalCode = $request->delivery_postcode;
                }

                $delivery->city = $request->delivery_city;
                $delivery->company = null;
                $delivery->isRemoved = 0;
                $delivery->isBulkAddress = 0;
                $delivery->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                $delivery->UserId = Auth::user()->id;
                $delivery->SellerUserId = null;
                $delivery->createdBy = null;
                $delivery->updateBy = null;
                $delivery->created_at = $this->now;
                $delivery->updated_at = $this->now;

                $delivery->save();

                $billing = new DeliveryAddresses();
                $billing->firstName = $request->delivery_name;
                $billing->lastName = null;
                $billing->fullName = $request->delivery_name;
                $billing->contactNumber = $request->billing_phoneext . $request->billing_phone;
                $billing->SSN = $request->SSN ? $request->SSN : null;
                $billing->state = $request->billing_state;
                $billing->address = $request->billing_address;
                if ($request->billing_postcode != null) {
                    $billing->portalCode = $request->billing_postcode;
                }

                $billing->city = $request->billing_city;
                $billing->company = null;
                $billing->isRemoved = 0;
                $billing->isBulkAddress = 0;
                $billing->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                $billing->UserId = Auth::user()->id;
                $billing->SellerUserId = null;
                $billing->createdBy = null;
                $billing->updateBy = null;
                $billing->created_at = $this->now;
                $billing->updated_at = $this->now;

                $billing->save();

                if ((Auth::user()->phoneHome === null && Auth::user()->phone === null) || (Auth::user()->phoneHome === "" && Auth::user()->phone === "") || (Auth::user()->phoneHome === null && Auth::user()->phone === "") || (Auth::user()->phoneHome === "" && Auth::user()->phone === null)) {
                    User::where('id', Auth::user()->id)->update(array(
                        'defaultShipping' => $delivery->id,
                        'defaultBilling' => $billing->id,
                        'phoneHome' => $request->delivery_phone,
                        'phone' => $request->delivery_phoneext . $request->delivery_phone,
                    ));
                } else {
                    User::where('id', Auth::user()->id)->update(array(
                        'defaultShipping' => $delivery->id,
                        'defaultBilling' => $billing->id,
                    ));
                }

                $user_details = $this->userHelper->getUserDetails(Auth::user()->id);

                return $user_details;
            }
        }
    }

    public function edit(Request $request)
    {
        if ($request) {
            if (Auth::check()) {
                // Name -> First & Last Name

                $fullname = explode(" ", $request->delivery_name);
                if (count($fullname) >= 2) {
                    $fName = explode(" ", $request->delivery_name)[0];
                    $lName = explode(" ", $request->delivery_name)[1];
                } else {

                    $fName = explode(" ", $request->delivery_name)[0];
                    $lName = "";
                }

                if (!isset($request->isDifferentBilling)) {
                    $pcode = '00000';
                    if ($request->delivery_postcode != null) {
                        $pcode = $request->delivery_postcode;
                    }
                    $delivery = DeliveryAddresses::where('id', Auth::user()->defaultShipping)
                        ->update(
                            array(
                                'firstName' => $request->delivery_name,
                                'lastName' => null,
                                'fullName' => $request->delivery_name,
                                'contactNumber' => $request->delivery_phoneext . $request->delivery_phone,
                                'SSN' => $request->SSN ? $request->SSN : null,
                                'state' => $request->delivery_state,
                                'address' => $request->delivery_address,
                                'portalCode' => $pcode,
                                'city' => $request->delivery_city,
                                'isRemoved' => 0,
                                'updated_at' => $this->now,
                            )
                        );
                    User::where('id', Auth::user()->id)->update(array(
                        'defaultShipping' => Auth::user()->defaultShipping,
                    ));

                    $user_details = $this->userHelper->getUserDetails(Auth::user()->id);

                    $address = [
                        "delivery_address" => end($user_details["delivery_address"]), "billing_address" => end($user_details["delivery_address"]), "diff" => "0",
                    ];

                    return $address;
                } else {
                    $pcode = '00000';
                    if ($request->delivery_postcode != null) {
                        $pcode = $request->delivery_postcode;
                    }
                    DeliveryAddresses::where('id', Auth::user()->defaultShipping)
                        ->update(
                            array(
                                'firstName' => $request->delivery_name,
                                'lastName' => null,
                                'fullName' => $request->delivery_name,
                                'contactNumber' => $request->delivery_phoneext . $request->delivery_phone,
                                'SSN' => $request->SSN ? $request->SSN : null,
                                'state' => $request->delivery_state,
                                'address' => $request->delivery_address,
                                'portalCode' => $pcode,
                                'city' => $request->delivery_city,
                                'isRemoved' => 0,
                                'updated_at' => $this->now,
                            )
                        );

                    if (Auth::user()->defaultShipping == Auth::user()->defaultBilling) {
                        $pcode = '00000';
                        if ($request->billing_postcode != null) {
                            $pcode = $request->billing_postcode;
                        }
                        $billing = new DeliveryAddresses();
                        $billing->firstName = $request->delivery_name;
                        $billing->lastName = null;
                        $billing->fullName = $request->delivery_name;
                        $billing->contactNumber = $request->billing_phoneext . $request->billing_phone;
                        $billing->SSN = $request->SSN ? $request->SSN : null;
                        $billing->state = $request->billing_state;
                        $billing->address = $request->billing_address;
                        $billing->portalCode = $pcode;
                        $billing->city = $request->billing_city;
                        $billing->company = null;
                        $billing->isRemoved = 0;
                        $billing->isBulkAddress = 0;
                        $billing->CountryId = $this->current_country ? $this->country_id : $this->default_country["id"];
                        $billing->UserId = Auth::user()->id;
                        $billing->SellerUserId = null;
                        $billing->createdBy = null;
                        $billing->updateBy = null;
                        $billing->created_at = $this->now;
                        $billing->updated_at = $this->now;

                        $billing->save();

                        User::where('id', Auth::user()->id)->update(array(
                            'defaultBilling' => $billing->id,
                        ));
                    } else {
                        $pcode = '00000';
                        if ($request->billing_postcode != null) {
                            $pcode = $request->billing_postcode;
                        }
                        $billing = DeliveryAddresses::where('id', Auth::user()->defaultBilling)
                            ->update(
                                array(
                                    'firstName' => $request->delivery_name,
                                    'lastName' => null,
                                    'fullName' => $request->delivery_name,
                                    'contactNumber' => $request->billing_phoneext . $request->billing_phone,
                                    'SSN' => $request->SSN ? $request->SSN : null,
                                    'state' => $request->billing_state,
                                    'address' => $request->billing_address,
                                    'portalCode' => $pcode,
                                    'city' => $request->billing_city,
                                    'isRemoved' => 0,
                                    'updated_at' => $this->now,
                                )
                            );
                    }
                    $user_details = $this->userHelper->getUserDetails(Auth::user()->id);

                    $address = ["delivery_address" => end($user_details["delivery_address"]), "billing_address" => end($user_details["billing_address"]), "diff" => "1"];

                    return $address;
                }
            }
        }
    }

    public function delete(Request $request)
    { }

    public function checkEmailExists(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $type = $request->type;

        if ($type === "login") {

            $checkEmail = User::where('email', $request->email)->first();
            if (!empty($checkEmail)) {
                $response = ["user_data" => $checkEmail, "isActive" => $checkEmail["isActive"]];
                if ($checkEmail !== null && $checkEmail["isActive"] == 1) {
                    $_tryLogin = $this->userLoginController->api_login($request);
                    return $response;
                } else {
                    $response = ["user_data" => $checkEmail, "isActive" => $checkEmail["isActive"]];
                    return $response;
                }
            } else {
                $response = ["user_data" => null, "isActive" => null];
                return $response;
            }

            if ($_tryLogin === 0) {
                return 0;
            } else {
                return $_tryLogin;
            }
        }

        if ($type === "register") {
            $checkEmail = User::where('email', $request->email)->first();
            if (!empty($checkEmail)) {
                $response = ["user_data" => $checkEmail, "isActive" => $checkEmail["isActive"]];
                return $response;
            } else {
                $response = ["user_data" => null, "isActive" => null];
                return $response;
            }
        }
    }

    public function checkEmailExistsOrActive(Request $request)
    {
        $checkEmail = User::where('email', $request->email)->first();
        if (!empty($checkEmail)) {
            $response = ["user_data" => $checkEmail, "isActive" => $checkEmail["isActive"]];
            return $response;
        } else {
            $response = ["user_data" => null, "isActive" => null];
            return $response;
        }
    }


    public function checkUniqueEmail(Request $request)
    {
        $email = $request->email;
        $checkEmail = User::where('email', $request->email)->first();
        if ($checkEmail) {
            return "false";
        } else {
            return "true";
        }
    }

    public function APIUserRegistration(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $credentials = [$email, $password];

        $_tryRegister = $this->userRegisterController->api_register($request);

        array_push($credentials, $_tryRegister);
        // if ($_tryRegister === 0) {
        //     return 0;
        // } else {
        return $credentials;
        // }
    }
}
