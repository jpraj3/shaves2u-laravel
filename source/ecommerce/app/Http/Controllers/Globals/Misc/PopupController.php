<?php

namespace App\Http\Controllers\Globals\Misc;

use Illuminate\Http\Request;
use App\Services\CountryService as CountryService;

class PopupController
{
    public function __construct()
    {
        $this->country_service = new CountryService();
    }

    public function retrieveTemplate(Request $request)
    {
        if ($request) {
            if ($request->data) {
                $currentCountryData = $request->data;
                session()->put('currentCountry', $currentCountryData);

                // Assign new locale value in session storage
                session()->put('currentLocale', json_decode($currentCountryData, true)['defaultLang']);

                // Set Locale based on Current Country default lang
                app()->setLocale(json_decode($currentCountryData, true)['defaultLang']);
            }

            $currentCountryIso = strtolower(json_decode($currentCountryData, true)['codeIso']);
            view()->share('currentCountryIso', $currentCountryIso);

            $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
            // $currentCountryid = $request->currentCountryid;
            $states = $this->country_service->getAllStates($currentCountryid);
            $template_name = $request->template_name;
            $data = $request->data;
            $render = view('layouts.popups.popup_' . $template_name, ['data' => $data, 'states' => $states])->render();
            return $render;
        }

    }

    public function retrieveTemplateKO(Request $request)
    {
        if ($request) {
            $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
            $states = $this->country_service->getAllStates($currentCountryid);
            $template_name = $request->template_name;
            $data = $request->data;
            $render = view('layouts.popups.popup_' . $template_name."_ko", ['data' => $data, 'states' => $states])->render();
            return $render;
        }

    }
}
