<?php

namespace App\Http\Controllers\Globals\Utilities;

use App\Http\Controllers\Controller as Controller;
use App\Services\CountryService as CountryService;
use Illuminate\Http\Request;

/* This controller is intended for AJAX usage for modifying session data */

class SessionController extends Controller
{
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->country_info = $this->country_service->getCountryAndLangCode();
    }

    //Function to be called from AJAX
    public function session(Request $req)
    {
        $session_key = $req->session_key;
        $method = $req->method;
        $data = $req->data;

        switch ($method) {
            case 'set':
                {
                    session()->put($session_key, $data);
                    return "Session set!";
                }
                break;
            case 'get':
                {
                    $session_data = session()->get($session_key);
                    return $session_data;
                }
                break;
            case 'clear':
                {
                    session()->forget($session_key);
                    return "Session cleared!";
                }
                break;
            default:
                {
                    return 'Invalid method.';
                }
                break;
        }
    }

    //Function to be called inside controller
    public function _session($session_key, $method, $data)
    {
        switch ($method) {
            case 'set':
                {
                    session()->put($session_key, $data);
                }
                break;
            case 'get':
                {
                    $session_data = session()->get($session_key);
                    return $session_data;
                }
                break;
            case 'clear':
                {
                    session()->forget($session_key);
                }
                break;
            default:
                {
                    return 'Invalid method.';
                }
                break;
        }

    }

}
