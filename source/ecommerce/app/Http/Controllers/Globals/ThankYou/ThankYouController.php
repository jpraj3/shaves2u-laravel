<?php

namespace App\Http\Controllers\Globals\ThankYou;

use App\Http\Controllers\Controller as Controller;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Foundation\Application;

// Models
use App\Models\Orders\Orders;
use App\Models\GeoLocation\Countries;

// Services
use Illuminate\Http\Request;

class ThankYouController extends Controller
{

    public function __construct(Application $app, Request $request)
    {
        $this->middleware(['locale']);
        $this->app = $app;
        $this->request = $request;
        $this->orderService = new OrderService;

    }

    public function index($lang, $country, $id)
    {

        $langCode = strtolower(app()->getLocale());
        $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);

        $deliveryDateFrom = Carbon::now("Asia/Kuala_Lumpur")->addDays(1)->format('Y-m-d');
        $deliveryDateTo = Carbon::now("Asia/Kuala_Lumpur")->addDays(7)->format('Y-m-d');

        // retrieve order info
        $_order = Orders::findorfail($id);

        // retrieve country info
        $_country = Countries::findorfail($_order->CountryId);
        session()->forget('stripe_payment_fail');
        return view('checkout.thankyou')
            ->with('orderid', $this->orderService->formatOrderNumber($_order, $_country, false))
            ->with('currentCountryIso', $currentCountryIso)
            ->with('langCode', $langCode)
            ->with('urllangCode', $urllangCode)
            ->with('deliveryDateFrom', $deliveryDateFrom)
            ->with('deliveryDateTo', $deliveryDateTo);
    }

}
