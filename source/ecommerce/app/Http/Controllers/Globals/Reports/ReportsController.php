<?php

namespace App\Http\Controllers\Globals\Reports;

use App\Helpers\ReportHelper;
use App\Http\Controllers\Controller as Controller;
use App\Models\GeoLocation\Countries;
use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    public function __construct(Application $app, Request $request)
    {

        $this->app = $app;
        $this->request = $request;
        $this->current_country = json_decode(session()->get('currentCountry'), true);
        $this->country_id = $this->current_country['id'];
        $this->default_country = Countries::where('codeIso', config('global.default.country.codeIso'))->first();
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->ReportHelper = new ReportHelper();
    }
}