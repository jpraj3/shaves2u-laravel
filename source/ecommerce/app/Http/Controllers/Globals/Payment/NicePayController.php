<?php

namespace App\Http\Controllers\Globals\Payment;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelper;
use App\Services\CountryService;

use App\Models\Cards\Cards;

use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Helpers\Payment\NicePayHelper;
use Carbon\Carbon;

class NicePayController extends Controller
{
    public $producthelper;
    public $countryService;
    public $nicepay;
    public function __construct()
    {
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->userHelper = new UserHelper();
        $this->producthelper = new ProductHelper();
        $this->countryService = new CountryService();
        $this->session = new SessionController();
        $this->nicepay = new NicePayHelper();
    }

    public function nicepayAddCard(Request $request)
    {
        $cardnumber = "";
        $card_expiry_month = "";
        $card_expiry_year = "";
        if ($request->card_number) {
            if (strpos($request->card_number, '-') !== false) { } else {
                if (strpos($request->card_number, ' ') !== false) {
                    $cardnumber = str_replace(' ', '-', $request->card_number);
                } else {
                    $seperatenumber = str_split($request->card_number, 4);
                    $cardnumber = $seperatenumber[0] . "-" . $seperatenumber[1] . "-" . $seperatenumber[2] . "-" . $seperatenumber[3];
                }
            }
        }
        if ($request->card_expiry_date) {
            if (strpos($request->card_expiry_date, '/') !== false) {
                $removespacedata = str_replace(' ', '', $request->card_expiry_date);
                $seperatedata = explode('/', $removespacedata);
                $card_expiry_month = $seperatedata[0];
                $card_expiry_year = $seperatedata[1];
            }
        } else {
            if ($request->card_expiry_month && $request->card_expiry_year) {
                $card_expiry_month = $request->card_expiry_month;
                $card_expiry_year = $request->card_expiry_year;
            }
        }
        $datalist = [
            "customer_name" => Auth::user()->firstName,
            "customer_email" => Auth::user()->email,
            "card_name" => $request->card_name,
            "card_number" => $cardnumber,
            "card_password" => $request->card_password,
            "card_expiry_month" => $card_expiry_month,
            "card_expiry_year" => $card_expiry_year,
            "card_birth" => $request->card_birth,

        ];
        $result = $this->nicepay->createBillingKey($datalist);
        $cus_id = '0';

        if ($result["status"] == "success") {
            // if (array_key_exists('data', $result)) {
            //     if (array_key_exists('response', $result->data)) {
            //         if (array_key_exists('customer_uid', $result->data->response)) {
            //             $cus_id =  $result["data"]["response"]["customer_uid"];
            //         }
            //     }
            // }
            $cards = new Cards();
            $cards->customerId =  $result["data"]["response"]["customer_uid"];
            $cards->cardNumber = "-";
            $cards->branchName = "-";
            $cards->cardName =  "-";
            $cards->expiredYear = "-";
            $cards->expiredMonth = "-";
            $cards->isDefault = 0;
            $cards->created_at =  $this->now;
            $cards->updated_at =  $this->now;
            $cards->UserId = Auth::user()->id;
            $cards->isRemoved = 0;
            $cards->type = 'website';
            $cards->payment_method = 'nicepay';
            $cards->CountryId = 8;
            $cards->save();
            $result['id'] = $cards->id;
            $result['cus_id'] = $result["data"]["response"]["customer_uid"];
        } else {
            $result['id'] = '0';
            $result['cus_id'] = '-';
        }
        return $result;
    }



    public function nicepayPayment($name, $email, $customerid, $amount, $mid)
    {
        //remember change price
        $datalist = [
            "customer_name" => $name,
            "customer_email" => $email,
            "merchant_uid" => 'ko_' . $mid . '_' . time(), // 새로 생성한 결제(재결제)용 주문 번호
            "customer_uid" => $customerid,
            "amount" => $amount,
        ];
        //"amount" => 100,
        // "amount" => $amount,
        $result = $this->nicepay->nicePayPayment($datalist);
        if ($result["status"] == "success") { } else { }
        return $result;
    }

    public function nicepayRefundOrder($imp_uid,$merchant_uid,$amount){
        $datalist = [
            "imp_uid" => $imp_uid,
            "merchant_uid" => $merchant_uid,
            "amount" => $amount,
        ];

        $result = $this->nicepay->cancelNicePayOrder($datalist);
        if ($result["status"] == "success") { } else { }
        return $result;
    }
}
