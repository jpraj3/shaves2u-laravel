<?php

namespace App\Http\Controllers\Globals\Payment;

use App;
use App\Events\SendReceiptsEvent;
use App\Helpers\LaravelHelper as LaravelHelper;
use App\Helpers\Payment\Stripe as Stripe;
use App\Helpers\Payment\StripeBA;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Plans\PayNowController;
use App\Http\Controllers\Globals\Plans\PlansController;
use App\Http\Controllers\Globals\Products\ProductController;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Cards\Cards;
use App\Models\Cards\PaymentRefunds;
use Exception;
//Helpers
use App\Models\Ecommerce\User;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Services\CountryService as CountryService;

//Models
use Carbon\Carbon;
use Config;

// Events
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StripeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['locale', 'auth']);
        $this->countryService = new CountryService();
        $this->app_type = config('app.appType') == 'ecommerce' ? 'website' : null;
        $this->userHelper = new UserHelper();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->stripe = new Stripe();
        $this->isLoggedIn = Auth::check();
        $this->payment_method = 'stripe';
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->current_country = $this->countryService->getCountryAndLangCode();
        $this->current_currency = $this->countryService->getCurrency();
        $this->product_type = '';
        $this->plan_type = '';
        $this->plan_id = 0;
        $this->plan_price = 0;
        $this->session = new SessionController();
        $this->plansController = new PlansController();
        $this->productController = new ProductController();
        $this->payNowController = new PayNowController();
        $this->stripe_without_session = new StripeBA();
    }

    public function initializePaymentIntents($User, $SessionKey)
    {
        //Get previous session data
        $session_data = json_decode($this->session->_session($SessionKey, "get", null));
        $isAnnual = 0 ; 
        //if session data exists, determine the product/plan type
        if ($session_data) {
            if (array_key_exists("product_type", $session_data->selection)) {
                $this->product_type = $session_data->selection->product_type->product_type;
                $productcountryIds = [];
                $_total_price_productcountryIds = [];
                foreach ($session_data->selection->step1 as $productcountry_id) {
                    array_push($productcountryIds, $productcountry_id->productcountryid);
                }
                $productcountries = $this->productHelper->getProductCountryDetails($productcountryIds);

                foreach ($productcountries as $xa) {
                    array_push($_total_price_productcountryIds, $xa->sellPrice);
                }
            } elseif (array_key_exists("plan_type", $session_data->selection)) {
                $this->plan_type = $session_data->selection->plan_type;

                if ($this->plan_type == "trial-plan") {
                    $this->plan_id = $session_data->selection->summary->planId;
                } else {
                    $this->plan_id = $session_data->selection->step5->planId;
                }
            }
        }

        $customer = "";
        $payment_data = (object) array();
        //Is default card exist, add customer id to the session storage
        if ($User !== null && !$User["default_card"]->isEmpty()) {
            $session_data->customer_id = $User["default_card"][0]->customerId;
            $customer = $User["default_card"][0]->customerId;

            // get plan/product price
            if ($this->product_type) {
                //Find product price here

                // initialize payment intent
                $payment_data->amount = array_sum($_total_price_productcountryIds);
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
                $payment_data->risk_level = $User["default_card"][0]->risk_level;
            } elseif ($this->plan_type) {

                // Addons
                $realAddonPrice = 0;

                if ($this->plan_type == "trial-plan") {
                    $getaddon = $session_data->selection->step2->selected_addon_list;
                } else {
                    $getaddon = $session_data->selection->step4->selected_addon_list;
                }

                $addonProductCountry = array();
                if (count($getaddon) > 0) {
                    foreach ($getaddon as $a) {
                        if ($this->plan_type == "trial-plan") {
                            if ($a->sku != "A5") {
                                array_push($addonProductCountry, $a->productcountriesid);
                            }
                        } else {
                            array_push($addonProductCountry, $a->productcountriesid);
                        }
                    }
                }
                if (count($addonProductCountry) > 0) {
                    $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                }

                // Plan
                if ($this->plan_type == "trial-plan") {
                    $plancategories = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanGroup');
                    $plantype = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanType');
                    $allplan = $this->planHelper->getPlanIdPriceByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                        }
                    }
                } else if ($this->plan_type == "custom-plan") {
                    if (array_key_exists("step5", $session_data->selection)) {
                        if (array_key_exists("isAnnual", $session_data->selection->step5)) {
                            $isAnnual = $session_data->selection->step5->isAnnual;
                        }
                    }
   
                    $plancategories = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanGroup');
                    $plantype = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanType');
                    if($isAnnual == 1){
                    $allplan = $this->planHelper->getPlanIdPriceAnnualByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, "CAnnual Plan");
                    }else{
                    $allplan = $this->planHelper->getPlanIdPriceByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);
                    }

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                        }
                    }
                }

                // initialize payment intent
                $payment_data->amount = $this->plan_price;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            }


            $this->isSameCountry = false;
            $this->isUserCountry = $User["country"]["id"];
            $this->isCurrentCountry = json_decode(session()->get('currentCountry'), true)["id"];
            if ($this->isUserCountry === $this->isCurrentCountry) {
                $this->isSameCountry = true;
            }
            if ($this->isSameCountry === false) {
                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = null;
                $data->payment_intents = null;
                $data = array('plandetails' => null, 'productcountries' => null, 'generate_payment_intent' => null);
                return $data;
            }

            // retrieve Stripe Customer and default Card Info
            $stripeCustomer = $this->stripe->retrieveCustomer($customer);

            $card_id = $stripeCustomer["sources"]["data"][0]["id"];
            $payment_data->risk_level = $User["default_card"][0]->risk_level;

            // build data for paymentIntent
            $data = (object) array();
            $data->default_card_id = (object) array();
            $data->payment_intents = (object) array();
            $data->default_card_id = $card_id;
            $data->payment_intents = $payment_data;

            /* DISABLED
            // Create payment intent only if it does not exist, otherwise update payment intent
            $do_create_payment_intent = true;
            $previous_payment_intent_id = '';
            if ($this->product_type && $this->product_type === 'awesome-shave-kits') {
            if ($User["default_card"][0]->pymt_intent_ask != null) {
            $do_create_payment_intent = false;
            $previous_payment_intent_id = $User["default_card"][0]->pymt_intent_ask;
            }
            } elseif ($this->product_type && $this->product_type === 'ala-carte') {
            if ($User["default_card"][0]->pymt_intent_alacarte != null) {
            $do_create_payment_intent = false;
            $previous_payment_intent_id = $User["default_card"][0]->pymt_intent_alacarte;
            }

            } elseif ($this->plan_type && $this->plan_type === 'trial-plan') {
            if ($User["default_card"][0]->pymt_intent_tk != null) {
            $do_create_payment_intent = false;
            $previous_payment_intent_id = $User["default_card"][0]->pymt_intent_tk;
            }

            } elseif ($this->plan_type && $this->plan_type === 'custom-plan') {
            if ($User["default_card"][0]->pymt_intent_cp != null) {
            $do_create_payment_intent = false;
            $previous_payment_intent_id = $User["default_card"][0]->pymt_intent_cp;
            }
            }

            if ($do_create_payment_intent) {
            // Create new payment intent
            $generate_payment_intent = $this->stripe->createPaymentIntents($data);
            } else {
            // Update existing payment intent with latest plan/product/card data
            $update_data = (object) array();
            $update_data->update_type = 'restart-checkout-journey';
            $update_data->payment_intent_id = $previous_payment_intent_id;
            $update_data->new_amount = $payment_data->amount;
            $update_data->stripe_customer = (object) array();
            $update_data->stripe_customer->id = $User["default_card"][0]->customerId;
            $update_data->stripe_card = (object) array();
            $update_data->stripe_card->id = $data->default_card_id;
            $update_data->stripe_card->risk_level = $User["default_card"][0]->risk_level;
            $update_data->user = LaravelHelper::ConvertArraytoObject($User["user"][0]);
            $generate_payment_intent = $this->stripe->updatePaymentIntents($update_data);
            } */

            $generate_payment_intent = $this->stripe->createPaymentIntents($data, true);

            // update paymentIntentId to Cards Table
            $card_update = Cards::where('customerId', $customer)->where('isDefault', 1)->where('isRemoved', 0)->first();

            if ($this->product_type && $this->product_type === 'awesome-shave-kits') {
                Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_ask' => null]);
                $card_update->pymt_intent_ask = $generate_payment_intent->id;
            } elseif ($this->product_type && $this->product_type === 'ala-carte') {
                Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_alacarte' => null]);
                $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
            } elseif ($this->plan_type && $this->plan_type === 'trial-plan') {
                Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_tk' => null]);
                $card_update->pymt_intent_tk = $generate_payment_intent->id;
            } elseif ($this->plan_type && $this->plan_type === 'custom-plan') {
                Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_cp' => null]);
                $card_update->pymt_intent_cp = $generate_payment_intent->id;
            }

            $card_update->save();

            if (isset($productcountries)) {
                $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
            } else if (isset($plandetails)) {
                $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
            }

            return $data;
        } else {
            // get plan/product price
            if ($this->product_type) {

                //Find product price here

                // initialize payment intent
                $payment_data->amount = array_sum($_total_price_productcountryIds);
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            } elseif ($this->plan_type) {

                // Addons
                $realAddonPrice = 0;

                if ($this->plan_type == "trial-plan") {
                    $getaddon = $session_data->selection->step2->selected_addon_list;
                } else {
                    $getaddon = $session_data->selection->step4->selected_addon_list;
                }

                $addonProductCountry = array();
                if (count($getaddon) > 0) {
                    foreach ($getaddon as $a) {
                        if ($a->sku != "A5") {
                            array_push($addonProductCountry, $a->productcountriesid);
                        }
                    }
                }
                if (count($addonProductCountry) > 0) {
                    $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                }

                // Plan
                if ($this->plan_type == "trial-plan") {
                    $plancategories = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanGroup');
                    $plantype = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanType');
                    $allplan = $this->planHelper->getPlanIdPriceByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                        }
                    }
                } else if ($this->plan_type == "custom-plan") {
                    $plancategories = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanGroup');
                    $plantype = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanType');
                    if (array_key_exists("step5", $session_data->selection)) {
                        if (array_key_exists("isAnnual", $session_data->selection->step5)) {
                            $isAnnual = $session_data->selection->step5->isAnnual;
                        }
                    }

                    if($isAnnual == 1){
                        $allplan = $this->planHelper->getPlanIdPriceAnnualByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, "CAnnual Plan");
                    }else{
                        $allplan = $this->planHelper->getPlanIdPriceByCategory($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);
                    }
                    
                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                        }
                    }
                }

                // initialize payment intent
                $payment_data->amount = $this->plan_price;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            }

            // build data for paymentIntent
            $data = (object) array();
            $data->default_card_id = (object) array();
            $data->payment_intents = (object) array();
            $data->default_card_id = '';
            $data->payment_intents = $payment_data;

            $generate_payment_intent = $this->stripe->createPaymentIntents($data, true);

            if (isset($productcountries)) {
                $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
            } else if (isset($plandetails)) {
                $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
            }
            return $data;
        }
    }

    public function updatePaymentIntent(Request $request)
    {
        // $session_data = json_decode($this->session->_session("checkout_selection_ask", "get", null));
        $payment_intent_type = $request->get("payment_intent_type");
        $payment_intent_details = LaravelHelper::ConvertArraytoObject($request->get("payment_intent_details"));
        $update_type = $request->get("update_type");
        $checkout_details = LaravelHelper::ConvertArraytoObject($request->get("checkout_details"));
        $data = LaravelHelper::ConvertArraytoObject($request->get("data"));
        $payment_intent_id = '';

        //dd($payment_intent_type,$payment_intent_details,$update_type,$checkout_details,$data,$payment_intent_id);
        if ($data) {

            $user = $data->customer->user;
            $stripe_customer = $data->customer->customer;
            $stripe_card = $data->card->card;
            // Remove paymentIntents under other cards, then add new payment intents
            if ($payment_intent_type === "awesome-shave-kits") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_ask' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_ask' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "ala-carte") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_alacarte' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_alacarte' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "trial-plan") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_tk' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_tk' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "custom-plan") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_cp' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_cp' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            }

            $updateData = (object) array();
            $updateData->payment_intent_type = $payment_intent_type;
            $updateData->payment_intent_id = $payment_intent_id;
            $updateData->checkout_details = $checkout_details;
            $updateData->user = $user;
            $updateData->stripe_customer = $stripe_customer;
            $updateData->stripe_card = $stripe_card;
            $updateData->update_type = $update_type;

            $update_payment_intent = $this->stripe->updatePaymentIntents($updateData);

            // If the payment intent already got customer id before but customer change the card, update payment intent in db
            if ($update_payment_intent->is_replaced_card) {
                if ($payment_intent_type === "awesome-shave-kits") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_ask" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "ala-carte") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_alacarte" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "trial-plan") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_tk" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "custom-plan") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_cp" => $update_payment_intent->id]);
                }
            }

            return $update_payment_intent;
        }
    }


    public function OneTimeChargeReturn(Request $request)
    {

        $stripe_return = (object) array(
            "status" => null,
            "card_data" => null,
            "response" => null,
            "refund_data" => null,
            "error" => null
        );

        if ($request) {
            $type = $request->type;
            $user = User::findorfail($request->user_id);
            $payment_intent = $request->payment_intent;
            $otp_fired = isset($request->non_otp) && $request->non_otp == true ? false : true;
            $otp = isset($request->otp) && $request->otp == 1 ? 1 : 0;
            $subscription_update_payment = isset($request->subscription_update_payment) && $request->subscription_update_payment == true ? true : false;
            $subscription_id = isset($request->subscription_id) && $request->subscription_id != null ? (int) $request->subscription_id : null;
            // Retrieve payment status and customer id from payment intent
            $stripe_data = $this->stripe_without_session->retrievePaymentIntents('ecommerce', $user->CountryId, $payment_intent);
            $payment_status = $stripe_data->status;
            $customer_id = $stripe_data->customer;
            $card_info = Cards::where('customerId', $customer_id)->first();
            if ($otp_fired == true) {
                if ($payment_status == "succeeded") {
                    // Update Cards
                    if ($card_info) {
                        $card_info->require_otp = true;
                        $card_info->require_otp_pass = true;
                        $card_info->require_otp_pymt_intent = $payment_intent;
                        $card_info->save();
                        if ($subscription_update_payment == true && $subscription_id != null) {
                            Subscriptions::where('id', $subscription_id)->update(["CardId" => $card_info->id]);
                        } else {
                            Subscriptions::where('UserId', $card_info->UserId)->update(["CardId" => $card_info->id]);
                            $all_subs = Subscriptions::where('UserId', $card_info->UserId)->get();
                            if ($all_subs) {
                                foreach ($all_subs as $as) {
                                    SubscriptionHistories::create([
                                        'message' => 'Modified Shave Plan',
                                        'detail' => '[Plan Update] Payment Card Update via Profile Page to ' . $card_info->branchName . ' <' . $card_info->cardNumber . '>',
                                        'subscriptionId' => $as->id,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ]);
                                }
                            }
                        }
                        // Refund charge and redirect back to original source
                        $initiate_refund = $this->stripe_without_session->refundOrderChargeFromAdminPanel('ecommerce', $user->CountryId, $stripe_data->charges->data[0]["id"]);
                        if ($initiate_refund->status == "succeeded") {
                            // Update PaymentRefund table
                            PaymentRefunds::create([
                                'amount' => $initiate_refund->amount / 100,
                                'currency' => $initiate_refund->currency,
                                'CardId' => $card_info->id,
                                'UserId' => $card_info->UserId,
                                'reason' => 'One Time Charge Initialize',
                                'type' => 'otp_refund',
                                'paymentType' => 'otp_refund',
                                'charge_id' => $stripe_data->charges->data[0]["id"]
                            ]);

                            Cards::where('customerId', $customer_id)->update(["otp_refund" => true]);

                            $stripe_return->status = 200;
                            $stripe_return->card_data = Cards::where('customerId', $customer_id)->first();
                            $stripe_return->response = $stripe_data;
                            $stripe_return->refund_data = $initiate_refund;
                            return view('_authenticated.stripe-otp-response', compact('stripe_return'));
                        } else {
                            $stripe_return->status = 200;
                            $stripe_return->card_data = Cards::where('customerId', $customer_id)->first();
                            $stripe_return->response = $stripe_data;
                            $stripe_return->error = "refund_fail";
                            return view('_authenticated.stripe-otp-response', compact('stripe_return'));
                        }
                    }
                } else {

                    if ($card_info) {
                        $card_info->require_otp = true;
                        $card_info->require_otp_pass = false;
                        $card_info->require_otp_pymt_intent = $payment_intent;
                        $card_info->save();
                    }

                    $error_object = (object) array();
                    //Only update risk level for certain stripe errors
                    if (isset($stripe_data->last_payment_error)) {
                        if (property_exists($stripe_data->last_payment_error, 'code')) {
                            if ($stripe_data->last_payment_error->code === "card_declined") {
                                if (property_exists($stripe_data->last_payment_error, 'decline_code')) {
                                    if (in_array($stripe_data->last_payment_error->decline_code, config('global.all.stripe.ignore_risk_error_codes'))) {
                                        $do_update_risk_level = false;
                                    }
                                }
                            }
                        }
                        if (property_exists($stripe_data->last_payment_error, 'message')) {
                            // Create error object with error message from stripe
                            $error_object->payment_error = $stripe_data->last_payment_error->message;
                        }
                    }

                    if ($card_info) {
                        Cards::where('id', $card_info->id)->update(["isRemoved" => 1, "isDefault" => 0]);
                        $revertOriginalCard = Cards::where('UserId', $card_info->UserId)->orderBy('created_at', 'DESC')->skip(1)->take(1)->first();
                        if ($revertOriginalCard) {
                            $revertOriginalCard->isDefault = 1;
                            $revertOriginalCard->save();
                        }
                    }
                    $stripe_return->status = 400;
                    $stripe_return->response = $stripe_data;
                    $stripe_return->error = $stripe_data->last_payment_error->message;
                    return view('_authenticated.stripe-otp-response', compact('stripe_return'));
                }
            } else {
                if ($payment_status == "succeeded") {
                    // Update Cards
                    if ($card_info) {

                        $card_info->require_otp = false;
                        $card_info->require_otp_pass = false;
                        $card_info->require_otp_pymt_intent = $payment_intent;
                        $card_info->save();
                        if ($subscription_update_payment == true && $subscription_id != null) {
                            Subscriptions::where('id', $subscription_id)->update(["CardId" => $card_info->id]);
                        } else {
                            Subscriptions::where('UserId', $card_info->UserId)->update(["CardId" => $card_info->id]);
                            $all_subs = Subscriptions::where('UserId', $card_info->UserId)->get();
                            if ($all_subs) {
                                foreach ($all_subs as $as) {
                                    SubscriptionHistories::create([
                                        'message' => 'Modified Shave Plan',
                                        'detail' => '[Plan Update] Payment Card Update via Profile Page to ' . $card_info->branchName . ' <' . $card_info->cardNumber . '>',
                                        'subscriptionId' => $as->id,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now()
                                    ]);
                                }
                            }
                        }

                        // Refund charge and redirect back to original source
                        $initiate_refund = $this->stripe_without_session->refundOrderChargeFromAdminPanel('ecommerce', $user->CountryId, $stripe_data->charges->data[0]["id"]);
                        if (isset($initiate_refund->status) && $initiate_refund->status == "succeeded") {
                            // Update PaymentRefund table
                            PaymentRefunds::create([
                                'amount' => $initiate_refund->amount / 100,
                                'currency' => $initiate_refund->currency,
                                'CardId' => $card_info->id,
                                'UserId' => $card_info->UserId,
                                'reason' => 'One Time Charge Initialize',
                                'type' => 'otp_refund',
                                'paymentType' => 'otp_refund',
                                'charge_id' => $stripe_data->charges->data[0]["id"]
                            ]);

                            Cards::where('customerId', $customer_id)->update(["otp_refund" => true]);

                            $stripe_return->status = 200;
                            $stripe_return->card_data = Cards::where('customerId', $customer_id)->first();
                            $stripe_return->response = $stripe_data;
                            $stripe_return->refund_data = $initiate_refund;
                        } else {
                            $stripe_return->status = 200;
                            $stripe_return->card_data = Cards::where('customerId', $customer_id)->first();
                            $stripe_return->response = $stripe_data;
                            $stripe_return->error = "refund_fail";
                        }
                    }
                } else {

                    $error_object = (object) array();
                    //Only update risk level for certain stripe errors
                    if (isset($stripe_data->last_payment_error)) {
                        if (property_exists($stripe_data->last_payment_error, 'code')) {
                            if ($stripe_data->last_payment_error->code === "card_declined") {
                                if (property_exists($stripe_data->last_payment_error, 'decline_code')) {
                                    if (in_array($stripe_data->last_payment_error->decline_code, config('global.all.stripe.ignore_risk_error_codes'))) {
                                        $do_update_risk_level = false;
                                    }
                                }
                            }
                        }
                        if (property_exists($stripe_data->last_payment_error, 'message')) {
                            // Create error object with error message from stripe
                            $error_object->payment_error = $stripe_data->last_payment_error->message;
                        }
                    }

                    if ($card_info) {
                        Cards::where('id', $card_info->id)->update(["isRemoved" => 1, "isDefault" => 0]);
                        $revertOriginalCard = Cards::where('UserId', $card_info->UserId)->orderBy('created_at', 'DESC')->skip(1)->take(1)->first();
                        if ($revertOriginalCard) {
                            $revertOriginalCard->isDefault = 1;
                            $revertOriginalCard->save();
                        }
                    }
                    $stripe_return->status = 400;
                    $stripe_return->response = $stripe_data;
                    $stripe_return->error = $stripe_data->last_payment_error->message;
                }

                return json_encode($stripe_return);
            }
        }
    }


    public function createOneTimeCharge(Request $request)
    {
        try {
            $one_time_charge = $this->stripe->OneTimeCharge(
                $request->type,
                isset($request->card_id) && $request->card_id !== null ? $request->card_id : null,
                isset($request->subscription_id) && $request->subscription_id !== null ? $request->subscription_id : null
            );
            return $one_time_charge;

            // public function confirmPaymentIntent(Request $request)
            // {
            //     $payment_intent_id = $request->payment_intent_id;
            //     $return_url = $request->return_url;
            //     $response = $this->stripe->confirmPaymentIntents($payment_intent_id, $return_url);
            //     return $response;
            // }
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function createOneTimeChargeAPI(Request $request)
    {
        $user = Auth::user();
        try {
            $one_time_charge = $this->stripe_without_session->OneTimeChargeAPI(
                $request->type,
                'ecommerce',
                $user,
                $user->CountryId,
                isset($request->card_id) && $request->card_id !== null ? $request->card_id : null,
                isset($request->subscription_id) && $request->subscription_id !== null ? $request->subscription_id : null
            );
            return $one_time_charge;

            // public function confirmPaymentIntent(Request $request)
            // {
            //     $payment_intent_id = $request->payment_intent_id;
            //     $return_url = $request->return_url;
            //     $response = $this->stripe->confirmPaymentIntents($payment_intent_id, $return_url);
            //     return $response;
            // }
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function listCard(Request $request)
    {
        $card_list = Cards::where('UserId', $request->id)->where('isRemoved', 0)->get();
        return $card_list;
    }

    public function getEachCard(Request $request)
    {
        return $this->userHelper->getCard($request->id);
    }

    // api for BA
    public function getEachCardBA(Request $request)
    {
        $card = Cards::where('id', $request->id)->first();
        return response()->json([
            'success' => true,
            'payload' => $card,
        ])
            ->header("Access-Control-Allow-Origin", "*");
    }

    public function addCardAPI(Request $request)
    {
        $user = Auth::user();

        $data = [];
        $data["user"] = $user;
        $data["add_card"] = $request->request;
        $success = [];

        // Create customer card in stripe and get card token id
        $createdCard = $this->stripe_without_session->createTokenV3_API(config('app.appType'), $user->CountryId, $data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $this->stripe_without_session->createCustomer(config('app.appType'), $user->CountryId, $data, $createdCard);

            $user = $this->userHelper->getUserDetails(Auth::user()->id);

            if ($createdCustomer) {
                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;

                if (count($user["cards"]) > 0) {

                    $cards->isDefault = 0;
                } else {
                    $cards->isDefault = 1;
                }

                $cards->payment_method = $this->payment_method;
                $cards->created_at = $this->now;
                $cards->updated_at = $this->now;
                $cards->UserId = $user["user"]["id"];
                $cards->isRemoved = 0;
                $cards->type = $this->app_type;
                $cards->CountryId = $this->current_country["country_id"];
                $cards->save();
            }

            $success["customer"] = $createdCustomer;
            $success["card"] = $createdCard;
            $success["card_from_db"] = Cards::where('id', $cards->id)->first();

            return $success;
        }
    }

    public function addCardBA(Request $request)
    {
        $_user = User::where('id', $request->user_id)->first();

        $data = [];
        $data["user"] = $_user;
        $data["add_card"] = $request->request;
        $success = [];

        // Create customer card in stripe and get card token id
        $createdCard = $this->stripe->createToken($data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $this->stripe->createCustomer('baWeb', $data, $createdCard);

            $user = $this->userHelper->getUserDetails($_user->id);

            if ($createdCustomer) {
                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;

                if (count($user["cards"]) > 0) {
                    $cards->isDefault = 0;
                } else {
                    $cards->isDefault = 1;
                }

                $cards->payment_method = $this->payment_method;
                $cards->created_at = $this->now;
                $cards->updated_at = $this->now;
                $cards->UserId = $_user->id;
                $cards->isRemoved = 0;
                $cards->type = 'baWeb';
                $cards->CountryId = $request->CountryId;
                $cards->save();
            }

            $success["customer"] = $createdCustomer;
            $success["card"] = $createdCard;
            $success["card_from_db"] = Cards::where('id', $cards->id)->first();

            return response()->json([
                'success' => true,
                'payload' => $success,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function addCard(Request $request)
    {
        $user = Auth::user();

        $data = [];
        $data["user"] = $user;
        $data["add_card"] = $request->request;
        $success = [];

        // Create customer card in stripe and get card token id
        $createdCard = $this->stripe->createToken($data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $this->stripe->createCustomer(config('app.appType'), $data, $createdCard);

            $user = $this->userHelper->getUserDetails(Auth::user()->id);

            if ($createdCustomer) {
                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;

                if (count($user["cards"]) > 0) {

                    $cards->isDefault = 0;
                } else {
                    $cards->isDefault = 1;
                }

                $cards->payment_method = $this->payment_method;
                $cards->created_at = $this->now;
                $cards->updated_at = $this->now;
                $cards->UserId = $user["user"]["id"];
                $cards->isRemoved = 0;
                $cards->type = $this->app_type;
                $cards->CountryId = $this->current_country["country_id"];
                $cards->save();
            }

            $success["customer"] = $createdCustomer;
            $success["card"] = $createdCard;
            $success["card_from_db"] = Cards::where('id', $cards->id)->first();

            return $success;
        }
    }

    public function selectCard(Request $request)
    {
        $payment_intent = $request->payment_intent;
        $card = Cards::where('id', $request->id)->first();
        $type = $request->type;

        $stripeCustomer = $this->stripe->retrieveCustomer($card->customerId);

        $previous_payment_data = $this->stripe->retrievePaymentIntents($payment_intent);

        if ($card) {
            if (isset($previous_payment_data)) {
                $payment_data = (object) array();
                $payment_data->amount = $previous_payment_data->amount;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $card->customerId;
                $payment_data->risk_level = $card->risk_level;

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $stripeCustomer->default_source;
                $data->payment_intents = $payment_data;

                $generate_payment_intent = $this->stripe->createPaymentIntents($data, false);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $generate_payment_intent->customer)->first();

                if ($type && $type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($type && $type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($type && $type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($type && $type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                return $generate_payment_intent;
            } else {
                $payment_data = (object) array();
                $payment_data->amount = $previous_payment_data->amount;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $card->customerId;
                $payment_data->risk_level = $card->risk_level;

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $stripeCustomer->default_source;
                $data->payment_intents = $payment_data;
                $generate_payment_intent = $this->stripe->createPaymentIntents($data, false);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $generate_payment_intent->customer)->first();

                if ($type && $type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($type && $type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($type && $type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($type && $type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                return $generate_payment_intent;
            }
        }
    }

    public function editCard($user_id, $data)
    {
    }
    public function destroyCard($user_id, $data)
    {
    }
    public function listCustomer($user_id, $data)
    {
    }
    public function addCustomer($user_id, $data)
    {
    }
    public function editCustomer($user_id, $data)
    {
    }
    public function addCharges($user_id, $data)
    {
    }
    public function editCharges($user_id, $data)
    {
    }
    public function addRefund($user_id, $data)
    {
    }

    public function confirmPaymentIntent(Request $request)
    {
        $payment_intent_id = $request->payment_intent_id;
        $return_url = $request->return_url;
        $response = $this->stripe->confirmPaymentIntents($payment_intent_id, $return_url);
        return $response;
    }

    public function confirmPaymentIntentAPI(Request $request)
    {
        $user = Auth::user();
        $payment_intent_id = $request->payment_intent_id;
        $return_url = $request->return_url;
        $response = $this->stripe_without_session->confirmPaymentIntents('ecommerce', $user->CountryId, $payment_intent_id, $return_url);
        return $response;
    }

    public function postPaymentRedirect(Request $request)
    {
        $type = $request->type;
        $payment_intent = $request->payment_intent;
        $is_pay_now = false;
        $is_free_purchase = false;
        $do_update_risk_level = true;

        if ($request->is_pay_now) {
            $is_pay_now = true;
            $subs_data = json_decode($this->session->_session("subscription_pay_now", "get", null));
        }

        if ($request->is_free_purchase) {
            $is_free_purchase = true;
        }

        if (!$is_free_purchase) {
            // Retrieve payment status and customer id from payment intent
            $stripe_data = $this->stripe->retrievePaymentIntents($payment_intent);
            sleep(3);

            $payment_status = $stripe_data->status;
            $customer_id = $stripe_data->customer;

            // If redirected from Pay Now flow, do the following
            // Else, do the First Time Purchase flow
            if ($is_pay_now) {
                $subs_data->charge = (object) array();
                $subs_data->charge = $stripe_data; // Reassign charge data

                if ($subs_data->charge->status !== "succeeded") {
                    $subs_data->charge->status = "payment_failed";
                    $subs_data->charge->message = $stripe_data->last_payment_error->message;
                    $subs_data->charge->id = null;
                    $subs_data->charge->currency = null;
                }

                $payNowResponse = $this->payNowController->UpdateSubscription($subs_data); // Call update subscription with latest subs data
                $this->session->_session('subscription_pay_now', "clear", null);

                if ($subs_data->charge->status !== "succeeded") {
                    $this->session->_session('subscription_pay_now', "set", json_encode($payNowResponse));
                    return redirect()->back();
                } else {
                    // Redirect to thank you page
                    $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
                    $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
                    return redirect()->route('locale.thankyou', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso, 'id' => $payNowResponse['orderId']]);
                }
            } else {
                if ($payment_status == "succeeded") {
                    // If payment success, clear payment intent id from card db
                    switch ($type) {
                        case "trial-plan": {
                                Cards::where("customerId", $customer_id)->update(["pymt_intent_tk" => null]);
                                $order_data = json_decode($this->session->_session("checkout_trial_plan", "get", null))->checkout->order_data;
                                $this->session->_session('selection_trial_plan', "clear", null);
                                $this->session->_session('checkout_trial_plan', "clear", null);
                                $this->plansController->updateStatus(true, $type, $order_data, $stripe_data);
                            }
                            break;
                        case "custom-plan": {
                                Cards::where("customerId", $customer_id)->update(["pymt_intent_cp" => null]);
                                $order_data = json_decode($this->session->_session("checkout_custom_plan", "get", null))->checkout->order_data;
                                $this->session->_session('selection_custom_plan', "clear", null);
                                $this->session->_session('checkout_custom_plan', "clear", null);
                                $this->plansController->updateStatus(true, $type, $order_data, $stripe_data);
                            }
                            break;
                        case "awesome-shave-kits": {
                                Cards::where("customerId", $customer_id)->update(["pymt_intent_ask" => null]);
                                $order_data = json_decode($this->session->_session("checkout_ask", "get", null))->checkout->order_data;
                                $this->session->_session('checkout_selection_ask', "clear", null);
                                $this->session->_session('checkout_ask', "clear", null);
                                $this->productController->updateStatus(true, $type, $order_data, $stripe_data);
                            }
                            break;
                        case "alacarte": {
                                Cards::where("customerId", $customer_id)->update(["pymt_intent_alacarte" => null]);
                                $order_data = json_decode($this->session->_session("checkout_alacarte", "get", null))->checkout->order_data;
                                $this->session->_session('checkout_alacarte', "clear", null);
                                $this->productController->updateStatus(true, $type, $order_data, $stripe_data);
                            }
                            break;
                    }

                    // call event to send receipts email
                    event(new SendReceiptsEvent($order_data));
                    // call event to  generate tax invoice pdf
                    // event(new GenerateTaxInvoice($order_data));

                    // Redirect to thank you page
                    $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
                    $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
                    return redirect()->route('locale.thankyou', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso, 'id' => $order_data->orderid]);
                } else {

                    $error_object = (object) array();

                    //Only update risk level for certain stripe errors
                    if (isset($stripe_data->last_payment_error)) {
                        if (property_exists($stripe_data->last_payment_error, 'code')) {
                            if ($stripe_data->last_payment_error->code === "card_declined") {
                                if (property_exists($stripe_data->last_payment_error, 'decline_code')) {
                                    if (in_array($stripe_data->last_payment_error->decline_code, config('global.all.stripe.ignore_risk_error_codes'))) {
                                        $do_update_risk_level = false;
                                    }
                                }
                            }
                        }
                        if (property_exists($stripe_data->last_payment_error, 'message')) {
                            // Create error object with error message from stripe
                            $error_object->payment_error = $stripe_data->last_payment_error->message;
                        }
                    }

                    if ($type === "trial-plan") {
                        $order_data = json_decode($this->session->_session("checkout_trial_plan", "get", null))->checkout->order_data;
                        $this->session->_session('checkout_trial_plan', "clear", null);
                        $this->session->_session('checkout_trial_plan', "set", json_encode($error_object));
                        if ($do_update_risk_level) {
                            Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                        }
                        $this->plansController->updateStatus(false, $type, $order_data, $stripe_data);
                    } else if ($type === "custom-plan") {
                        $order_data = json_decode($this->session->_session("checkout_custom_plan", "get", null))->checkout->order_data;
                        $this->session->_session('checkout_custom_plan', "clear", null);
                        $this->session->_session('checkout_custom_plan', "set", json_encode($error_object));
                        if ($do_update_risk_level) {
                            Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                        }
                        $this->plansController->updateStatus(false, $type, $order_data, $stripe_data);
                    } else if ($type === "awesome-shave-kits") {
                        $order_data = json_decode($this->session->_session("checkout_ask", "get", null))->checkout->order_data;
                        $this->session->_session('checkout_ask', "clear", null);
                        $this->session->_session('checkout_ask', "set", json_encode($error_object));
                        if ($do_update_risk_level) {
                            Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                        }
                        $this->productController->updateStatus(false, $type, $order_data, $stripe_data);
                    } else if ($type === "alacarte") {
                        $order_data = json_decode($this->session->_session("checkout_alacarte", "get", null))->checkout->order_data;
                        $this->session->_session('checkout_alacarte', "clear", null);
                        $this->session->_session('checkout_alacarte', "set", json_encode($error_object));
                        if ($do_update_risk_level) {
                            Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                        }
                        $this->productController->updateStatus(false, $type, $order_data, $stripe_data);
                    }

                    $errorObj = ["status" => 'fail', "order_id" => null, "error" => $error_object];
                    session()->put('stripe_payment_fail', $errorObj);
                    //Redirect back to the previous checkout page
                    return redirect()->back();
                }
            }
        } else {
            if ($type === "trial-plan") {
                $order_data = json_decode($this->session->_session("checkout_trial_plan", "get", null))->checkout->order_data;
            } else if ($type === "custom-plan") {
                $order_data = json_decode($this->session->_session("checkout_custom_plan", "get", null))->checkout->order_data;
            } else if ($type === "awesome-shave-kits") {
                $order_data = json_decode($this->session->_session("checkout_ask", "get", null))->checkout->order_data;
            } else if ($type === "alacarte") {
                $order_data = json_decode($this->session->_session("checkout_alacarte", "get", null))->checkout->order_data;
            }

            // call event to send receipts email
            event(new SendReceiptsEvent($order_data));
            // call event to  generate tax invoice pdf
            // event(new GenerateTaxInvoice($order_data));

            // Redirect to thank you page
            $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
            $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
            return redirect()->route('locale.thankyou', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso, 'id' => $order_data->orderid]);
        }
    }
}
