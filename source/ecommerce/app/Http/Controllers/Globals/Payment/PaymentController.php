<?php

namespace App\Http\Controllers\Globals\Payment;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Services\CountryPaymentService;
use App\Helpers\Payment\Stripe as Stripe;
use Closure;
use Illuminate\Foundation\Application;
use Config;
use App;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    public function __construct(Application $app, Request $request)
    {
        $this->middleware(['locale','auth']);
        $this->app = $app;
        $this->request = $request;
        $this->stripe = new Stripe();
        $this->isLoggedIn = Auth::check();
    }

    public function index()
    {
        return view('tests.payment');
    }

    public function stripe()
    {
        $data = [
            "name" => "Jason Paulraj",
            "email" => "jason@dci-agency.com",
            "additionalInfo" => "-",
            "phone" => "+60103711694",
            "shipping" => "No.19, Jalan PP 3/9, Taman Putra Prima, 47100 Puchong, Selangor",
            "tax_exempt" => "none",
            "tax_id_data" => "S2U-hJibe45MX0",

        ];

        $metadata = [
            "order_id" => '#MY000103451-20190625',
            "order_date" => '2018-11-21 03:59:23'
        ];

        $shippingObject = [
            "address" => []
        ];
        $chargeObjects = [
            "amount" => 123.00,
            "currency" => "RM",
            "customer" => "cus_FJmaNh39SmddIY",
            "description" => "[MYS][ecommerce] Charge for jason@dci-agency.com",
            "metadata" => "",
            "receipt_email" => "jason@dci-agency.com",
            // "shipping" => [
            //     "address" => [
            //         "line1" => "No.19, Jalan PP 3/9",
            //         "line2" => "Taman Putra Prima",
            //         "city" => "Puchong",
            //         "portal_code" => 47100,
            //         "state" => "Selangor",
            //         "country" => "Malaysia",
            //     ],
            //     "name" => "Jason Paulraj",
            //     "phone" => "0103711694"
            // ],
            // "source" => [
            //     "id" => "card_1EpU7X2eZvKYlo2CFGt5C008",
            //     "object" => "card",
            //     "brand" => "Visa",
            //     "country" => "US",
            //     "customer" => null,
            //     "cvc_check" => null,
            //     "dynamic_last4" => null,
            //     "exp_month" => 8,
            //     "exp_year" => 2020,
            //     "fingerprint" => "Xt5EWLLDS7FJjR1c",
            //     "funding" => "credit",
            //     "last4" => "4242",
            //     "metadata" => [],
            //     "name" => null,
            //     "tokenization_method" => null
            // ],
        ];

        if ($this->isLoggedIn) {
            $response = $this->stripe->createCharge($chargeObjects, 'MYS', 'ecommerce');
        } else {
            return view('_guest.landing');
        }

        // $response = $this->stripe->createCustomer('Single', 'Custom Plan', $data);
        // $response = $this->stripe->retrieveCustomer('cus_FJmaNh39SmddIY');
        // $response = $this->stripe->updateCustomer('cus_FJmaNh39SmddIY', $metadata);
        // $response = $this->stripe->createCharge($chargeObjects, 'MYS', 'ecommerce');
        $response = "asd";

        // return redirect()->route('locale.region.test.stripe', [$response]);
        return view('test.payment', compact('response', $response));
    }
}
