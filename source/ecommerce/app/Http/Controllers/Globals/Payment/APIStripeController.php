<?php

namespace App\Http\Controllers\Globals\Payment;

use App;
use App\Events\SendReceiptsEvent;
use App\Helpers\LaravelHelper as LaravelHelper;
use App\Helpers\Payment\StripeBA as StripeBA;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelperAPI;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Plans\PlansController;
use App\Http\Controllers\Globals\Products\ProductController;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
//Helpers

//Models
use Carbon\Carbon;
use Config;

// Events
use Illuminate\Http\Request;

class APIStripeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['api']);
        $this->app_type = null;
        $this->userHelper = new UserHelperAPI();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        // $this->stripe = new Stripe();
        $this->stripe_ba = new StripeBA();
        $this->payment_method = 'stripe';
        $this->now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $this->product_type = '';
        $this->plan_type = '';
        $this->plan_id = 0;
        $this->plan_price = 0;
        $this->current_country = null;
        $this->appType = 'baWebsite';
        $this->current_currency = null;
        $this->session = new SessionController();
        $this->plansController = new PlansController();
        $this->productController = new ProductController();
    }

    public function api_initializePaymentIntents($User, $session_data, $langCode, $CountryId, $gender = null)
    {

        $this->current_country = Countries::where('id', (int) $CountryId)->first();
        $this->current_currency = $this->current_country->currencyCode;
        $this->user_info = Countries::where('id', (int) $CountryId)->first();
        //Get previous session data
        //if session data exists, determine the product/plan type

        if ($session_data) {

            if (array_key_exists("product_type", $session_data->selection)) {
                $this->product_type = $session_data->selection->product_type->product_type;
                $productcountryIds = [];
                $_total_price_productcountryIds = [];
                foreach ($session_data->selection->step1 as $productcountry_id) {
                    array_push($productcountryIds, $productcountry_id->productcountryid);
                }
                $productcountries = $this->productHelper->getProductCountryDetails($productcountryIds);
                foreach ($productcountries as $xa) {
                    array_push($_total_price_productcountryIds, $xa->sellPrice);
                }
            } elseif (array_key_exists("plan_type", $session_data->selection)) {

                $this->plan_type = $session_data->selection->plan_type;
                if ($this->plan_type == "trial-plan") {
                    $this->plan_id = $session_data->selection->summary->planId;
                } else {
                    $this->plan_id = $session_data->selection->step5->planId;
                }
            }
        }

        $customer = "";
        $payment_data = (object) array();
        //Is default card exist, add customer id to the session storage

        if (isset($User)) {
            if ($User["user"] !== "" && !$User["defaults"]["card"] === null) {
                $session_data->customer_id = $User["default_card"][0]->customerId;
                $customer = $User["default_card"][0]->customerId;

                // get plan/product price
                if ($this->product_type) {
                    //Find product price here

                    // initialize payment intent
                    $payment_data->amount = array_sum($_total_price_productcountryIds);
                    $payment_data->currency = $this->current_currency;
                    $payment_data->customer = $customer;
                    $payment_data->risk_level = $User["default_card"][0]->risk_level;

                } elseif ($this->plan_type) {

                    // Addons
                    $realAddonPrice = 0;

                    if ($this->plan_type == "trial-plan") {
                        if ($gender !== null && $gender === 'female') {
                            $getaddon = [];
                        } else {
                            $getaddon = $session_data->selection->step2->selected_addon_list;
                        }
                    } else {
                        $getaddon = $session_data->selection->step4->selected_addon_list;
                    }

                    $addonProductCountry = array();
                    if (count($getaddon) > 0) {
                        foreach ($getaddon as $a) {
                            if ($this->plan_type == "trial-plan") {
                                if ($a->sku != "A5") {
                                    array_push($addonProductCountry, $a->productcountriesid);
                                }
                            } else {
                                array_push($addonProductCountry, $a->productcountriesid);
                            }
                        }
                    }
                    if (count($addonProductCountry) > 0) {
                        $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                    }

                    // Plan
                    if ($this->plan_type == "trial-plan") {
                        $plancategories = $gender !== null && $gender === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanGroup') : config('global.baWebsite.TrialPlan.TrialPlanGroup');
                        $plantype = $gender !== null && $gender === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanType') : config('global.baWebsite.TrialPlan.TrialPlanType');
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);

                        foreach ($allplan as $plan) {
                            if ($plan['planid'] == $this->plan_id) {
                                $plandetails = $plan;
                                $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                            }
                        }
                    } else if ($this->plan_type == "custom-plan") {
                        $plancategories = config('global.' . $this->appType . '.CustomPlan.CustomPlanGroup');
                        $plantype = config('global.' . $this->appType . '.CustomPlan.CustomPlanType');
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);

                        foreach ($allplan as $plan) {
                            if ($plan['planid'] == $this->plan_id) {
                                $plandetails = $plan;
                                $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                            }
                        }
                    }

                    // initialize payment intent
                    $payment_data->amount = $this->plan_price;
                    $payment_data->currency = $this->current_currency;
                    $payment_data->customer = $customer;
                }

                // retrieve Stripe Customer and default Card Info
                $stripeCustomer = $this->stripe_ba->retrieveCustomer($customer);
                $card_id = $stripeCustomer["sources"]["data"][0]["id"];
                $payment_data->risk_level = $User["default_card"][0]->risk_level;

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $card_id;
                $data->payment_intents = $payment_data;

                $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $CountryId, $data, true);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $customer)->where('isDefault', 1)->first();

                if ($this->product_type && $this->product_type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($this->product_type && $this->product_type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($this->plan_type && $this->plan_type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($this->plan_type && $this->plan_type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                if (isset($productcountries)) {
                    $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
                } else if (isset($plandetails)) {
                    $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
                }

                return $data;
            }
        } else {
            // get plan/product price
            if ($this->product_type) {
                //Find product price here
                // initialize payment intent
                $payment_data->amount = array_sum($_total_price_productcountryIds);
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            } elseif ($this->plan_type) {
                // Addons
                $realAddonPrice = 0;
                if ($this->plan_type == "trial-plan") {
                    if ($gender !== null && $gender === 'female') {
                        $getaddon = [];
                    } else {
                        $getaddon = $session_data->selection->step2->selected_addon_list;
                    }
                } else {
                    $getaddon = $session_data->selection->step4->selected_addon_list;
                }
                $addonProductCountry = array();

                if (count((array) $getaddon)) {
                    if ($gender !== null && $gender === 'female') {
                    } else {
                        foreach ($getaddon as $a) {
                            if ($a->sku != "A5") {
                                array_push($addonProductCountry, $a->productcountriesid);
                            }
                        }
                    }
                }

                if (count($addonProductCountry) > 0) {
                    $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                }

                // Plan
                if ($this->plan_type == "trial-plan") {

                    $plancategories = $gender !== null && $gender === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanGroup') : config('global.baWebsite.TrialPlan.TrialPlanGroup');
                    $plantype = $gender !== null && $gender === 'female' ? config('global.baWebsite.TrialPlan-W.TrialPlanType') : config('global.baWebsite.TrialPlan.TrialPlanType');
                    $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, $langCode, $plantype, $plancategories);

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                        }
                    }
                } else if ($this->plan_type == "custom-plan") {
                    $plancategories = config('global.baWebsite.CustomPlan.CustomPlanGroup');
                    $plantype = config('global.baWebsite.CustomPlan.CustomPlanType');
                    $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($this->current_country['country_id'], $this->current_country['default_lang'], $plantype, $plancategories);

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                        }
                    }
                }

                // initialize payment intent
                $payment_data->amount = $this->plan_price;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            }

            // build data for paymentIntent
            $data = (object) array();
            $data->default_card_id = (object) array();
            $data->payment_intents = (object) array();
            $data->default_card_id = '';
            $data->payment_intents = $payment_data;

            $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $CountryId, $data, true);

            if (isset($productcountries)) {
                $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
            } else if (isset($plandetails)) {
                $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
            }
            return $data;
        }
    }

    public function apiGeneratePaymentIntents($User, $session_data, $langCode, $CountryId, $gender = null)
    {
        $this->current_country = Countries::where('id', (int) $CountryId)->first();
        $this->current_currency = $this->current_country->currencyCode;
        $this->user_info = Countries::where('id', (int) $CountryId)->first();
        //Get previous session data
        //if session data exists, determine the product/plan type
        $session_data = json_decode($session_data);
        if ($session_data) {
            if (array_key_exists("product_type", $session_data->selection)) {
                $this->product_type = $session_data->selection->product_type->product_type;
                $productcountryIds = [];
                $_total_price_productcountryIds = [];
                foreach ($session_data->selection->step1 as $productcountry_id) {
                    array_push($productcountryIds, $productcountry_id->productcountryid);
                }
                $productcountries = $this->productHelper->getProductCountryDetails($productcountryIds);
                foreach ($productcountries as $xa) {
                    array_push($_total_price_productcountryIds, $xa->sellPrice);
                }
            } elseif (array_key_exists("plan_type", $session_data->selection)) {

                $this->plan_type = $session_data->selection->plan_type;
                if ($this->plan_type == "trial-plan") {
                    $this->plan_id = $session_data->selection->summary->planId;
                } else {
                    $this->plan_id = $session_data->selection->step5->planId;
                }
            }
        }

        $customer = "";
        $payment_data = (object) array();
        //Is default card exist, add customer id to the session storage

        if (isset($User)) {
            if ($User["user"] !== "" && !$User["defaults"]["card"] !== null) {
                $session_data->customer_id = $User["defaults"]["card"]["customerId"];
                $customer = $User["defaults"]["card"]["customerId"];
                // get plan/product price
                if ($this->product_type) {
                    //Find product price here
                    // initialize payment intent
                    $payment_data->amount = array_sum($_total_price_productcountryIds);
                    $payment_data->currency = $this->current_currency;
                    $payment_data->customer = $customer;
                    $payment_data->risk_level = $User["defaults"]["card"]["risk_level"];
                } elseif ($this->plan_type) {
                    // Addons
                    $realAddonPrice = 0;

                    if ($this->plan_type == "trial-plan") {
                        if ($gender !== null && $gender === 'female') {
                            $getaddon = [];
                        } else {
                            $getaddon = $session_data->selection->step2->selected_addon_list;
                        }
                    } else {
                        $getaddon = $session_data->selection->step4->selected_addon_list;
                    }

                    $addonProductCountry = array();

                    if (count(array($getaddon)) > 0) {
                        foreach ($getaddon as $a) {
                            if ($this->plan_type == "trial-plan") {
                                if ($a->sku != "A5") {
                                    array_push($addonProductCountry, $a->productcountriesid);
                                }
                            } else {
                                array_push($addonProductCountry, $a->productcountriesid);
                            }
                        }
                    }

                    if (count($addonProductCountry) > 0) {
                        $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                    }

                    // Plan
                    if ($this->plan_type == "trial-plan") {
                        $plancategories = $gender !== null && $gender === 'female' ? config('global.' . $this->appType . '.TrialPlan-W.TrialPlanGroup') : config('global.' . $this->appType . '.TrialPlan.TrialPlanGroup');
                        $plantype = $gender !== null && $gender === 'female' ? config('global.' . $this->appType . '.TrialPlan-W.TrialPlanType') : config('global.' . $this->appType . '.TrialPlan.TrialPlanType');

                        if (!isset($session_data->selection->switch_plans->isSwitched)) {
                            $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);
                        } else if (isset($session_data->selection->switch_plans->isSwitched) && $session_data->selection->switch_plans->isSwitched === false) {
                            $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);
                        } else if (isset($session_data->selection->switch_plans->isSwitched) && $session_data->selection->switch_plans->isSwitched === true) {
                            $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLYAnnual($CountryId, strtoupper($langCode), $plantype, $plancategories);
                        }

                        foreach ($allplan as $plan) {
                            if ($plan['planid'] == $this->plan_id) {
                                $plandetails = $plan;
                                $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                            }
                        }
                    } else if ($this->plan_type == "custom-plan") {
                        $plancategories = config('global.' . $this->appType . '.CustomPlan.CustomPlanGroup');
                        $plantype = config('global.' . $this->appType . '.CustomPlan.CustomPlanType');
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);

                        foreach ($allplan as $plan) {
                            if ($plan['planid'] == $this->plan_id) {
                                $plandetails = $plan;
                                $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                            }
                        }
                    }

                    // initialize payment intent
                    $payment_data->amount = $this->plan_price;
                    $payment_data->currency = $this->current_currency;
                    $payment_data->customer = $customer;
                }

                // retrieve Stripe Customer and default Card Info
                $stripeCustomer = $this->stripe_ba->retrieveCustomer('baWebsite', $CountryId, $customer);
                $card_id = $stripeCustomer["sources"]["data"][0]["id"];
                $payment_data->risk_level = $User["defaults"]["card"]["risk_level"];

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $card_id;
                $data->payment_intents = $payment_data;

                $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', $CountryId, $data, true);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $customer)->where('isDefault', 1)->first();

                if ($this->product_type && $this->product_type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($this->product_type && $this->product_type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($this->plan_type && $this->plan_type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($this->plan_type && $this->plan_type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                if (isset($productcountries)) {
                    $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
                } else if (isset($plandetails)) {
                    $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
                }
                return $data;
            }
        } else {
            // get plan/product price
            if ($this->product_type) {
                //Find product price here
                // initialize payment intent
                $payment_data->amount = array_sum($_total_price_productcountryIds);
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            } elseif ($this->plan_type) {
                // Addons
                $realAddonPrice = 0;
                if ($this->plan_type == "trial-plan") {
                    if ($gender !== null && $gender === 'female') {
                        $getaddon = [];
                    } else {
                        $getaddon = $session_data->selection->step2->selected_addon_list;
                    }
                } else {
                    $getaddon = $session_data->selection->step4->selected_addon_list;
                }
                $addonProductCountry = array();
                if (count((array) $getaddon)) {
                    foreach ($getaddon as $a) {
                        if ($a->sku != "A5") {
                            array_push($addonProductCountry, $a->productcountriesid);
                        }
                    }
                }

                if (count($addonProductCountry) > 0) {
                    $realAddonPrice = $this->productHelper->getProductSumPrice($addonProductCountry);
                }

                // Plan
                if ($this->plan_type == "trial-plan") {
                    $plancategories = $gender !== null && $gender === 'female' ? config('global.' . $this->appType . '.TrialPlan-W.TrialPlanGroup') : config('global.' . $this->appType . '.TrialPlan.TrialPlanGroup');
                    $plantype = $gender !== null && $gender === 'female' ? config('global.' . $this->appType . '.TrialPlan-W.TrialPlanType') : config('global.' . $this->appType . '.TrialPlan.TrialPlanType');
                    if (!isset($session_data->selection->switch_plans->isSwitched)) {
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);
                    } else if (isset($session_data->selection->switch_plans->isSwitched) && $session_data->selection->switch_plans->isSwitched === false) {
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);
                    } else if (isset($session_data->selection->switch_plans->isSwitched) && $session_data->selection->switch_plans->isSwitched === true) {
                        $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLYAnnual($CountryId, strtoupper($langCode), $plantype, $plancategories);
                    }
                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['trialPrice'] + $realAddonPrice;
                        }
                    }
                } else if ($this->plan_type == "custom-plan") {
                    $plancategories = config('global.baWebsite.CustomPlan.CustomPlanGroup');
                    $plantype = config('global.baWebsite.CustomPlan.CustomPlanType');
                    $allplan = $this->planHelper->getPlanIdPriceByCategoryBAONLY($CountryId, strtoupper($langCode), $plantype, $plancategories);

                    foreach ($allplan as $plan) {
                        if ($plan['planid'] == $this->plan_id) {
                            $plandetails = $plan;
                            $this->plan_price = $plan['sellPrice'] + $realAddonPrice;
                        }
                    }
                }

                // initialize payment intent
                $payment_data->amount = $this->plan_price;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
            }

            // build data for paymentIntent
            $data = (object) array();
            $data->default_card_id = (object) array();
            $data->payment_intents = (object) array();
            $data->default_card_id = '';
            $data->payment_intents = $payment_data;

            $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', $CountryId, $data, true);

            if (isset($productcountries)) {
                $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
            } else if (isset($plandetails)) {
                $data = array('plandetails' => $plandetails, 'generate_payment_intent' => $generate_payment_intent);
            }
            return $data;
        }
    }

    public function updatePaymentIntent(Request $request)
    {
        $payment_intent_type = $request->get("payment_intent_type");
        $payment_intent_details = LaravelHelper::ConvertArraytoObject($request->get("payment_intent_details"));
        $update_type = $request->get("update_type");
        $checkout_details = LaravelHelper::ConvertArraytoObject($request->get("checkout_details"));
        $data = LaravelHelper::ConvertArraytoObject($request->get("data"));
        $payment_intent_id = '';

        //dd($payment_intent_type,$payment_intent_details,$update_type,$checkout_details,$data,$payment_intent_id);
        if ($data) {

            $user = $data->customer->user;
            $stripe_customer = $data->customer->customer;
            $stripe_card = $data->card->card;
            // Remove paymentIntents under other cards, then add new payment intents
            if ($payment_intent_type === "awesome-shave-kits") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_ask' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_ask' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "ala-carte") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_alacarte' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_alacarte' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "trial-plan") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_tk' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_tk' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            } else if ($payment_intent_type === "custom-plan") {
                Cards::where('UserId', $user->id)->update(['pymt_intent_cp' => null]);
                Cards::where('customerId', $stripe_customer->id)->update(['pymt_intent_cp' => $payment_intent_details->id]);
                $payment_intent_id = $payment_intent_details->id;
            }

            $updateData = (object) array();
            $updateData->payment_intent_type = $payment_intent_type;
            $updateData->payment_intent_id = $payment_intent_id;
            $updateData->checkout_details = $checkout_details;
            $updateData->user = $user;
            $updateData->stripe_customer = $stripe_customer;
            $updateData->stripe_card = $stripe_card;
            $updateData->update_type = $update_type;

            $update_payment_intent = $this->stripe_ba->updatePaymentIntents($updateData);

            // If the payment intent already got customer id before but customer change the card, update payment intent in db
            if ($update_payment_intent->is_replaced_card) {
                if ($payment_intent_type === "awesome-shave-kits") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_ask" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "ala-carte") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_alacarte" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "trial-plan") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_tk" => $update_payment_intent->id]);
                } else if ($payment_intent_type === "custom-plan") {
                    Cards::where("customerId", $update_payment_intent->customer)->update(["pymt_intent_cp" => $update_payment_intent->id]);
                }
            }

            return $update_payment_intent;
        }
    }

    public function listCard(Request $request)
    {
        $card_list = Cards::where('UserId', $request->id)->where('isRemoved', 0)->get();
        return $card_list;
    }

    // api for BA
    public function getEachCardBA(Request $request)
    {
        $card = Cards::where('id', (int) $request->id)->first();
        return response()->json([
            'success' => true,
            'payload' => $card,
        ])
            ->header("Access-Control-Allow-Origin", "*");
    }
    public function addCardBA(Request $request)
    {
        $CountryId = (int) $request->countryid;
        $_user = User::where('id', $request->user_id)->first();

        $data = [];
        $data["user"] = $_user;
        $data["add_card"] = $request->all();
        $success = [];

        $stripe = new App\Helpers\Payment\StripeBA();
        // Create customer card in stripe and get card token id
        $createdCard = $stripe->createTokenV3_API('baWebsite', $CountryId, $data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $stripe->createCustomer('baWebsite', $CountryId, $data, $createdCard);

            $user = $this->userHelper->getUserDetails($_user->id);

            if ($createdCustomer) {
                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;

                if (count($user["cards"]) > 0) {
                    $cards->isDefault = 0;
                } else {
                    $cards->isDefault = 1;
                }

                $cards->payment_method = $this->payment_method;
                $cards->created_at = $this->now;
                $cards->updated_at = $this->now;
                $cards->UserId = $_user->id;
                $cards->isRemoved = 0;
                $cards->type = 'baWebsite';
                $cards->CountryId = (int) $request->countryid;
                $cards->save();
            }

            $success["customer"] = $createdCustomer;
            $success["card"] = $createdCard;
            $success["card_from_db"] = Cards::where('id', $cards->id)->first();
            return response()->json($success)->header("Access-Control-Allow-Origin", "*");

            return response()->json([
                'success' => true,
                'payload' => $success,
            ])
                ->header("Access-Control-Allow-Origin", "*");
        }
    }

    public function selectCardBA(Request $request)
    {
        $payment_intent = $request->payment_intent;
        $card = Cards::where('id', $request->id)->first();
        $type = $request->type;
        $this->current_country = Countries::where('id', (int) $request->CountryId)->first();
        $this->current_currency = $this->current_country->currencyCode;
        $stripeCustomer = $this->stripe_ba->retrieveCustomer('baWebsite', (int) $request->CountryId, $card->customerId);

        $previous_payment_data = $this->stripe_ba->retrievePaymentIntents('baWebsite', (int) $request->CountryId, $payment_intent);

        if ($card) {
            if (isset($previous_payment_data)) {
                $payment_data = (object) array();
                $payment_data->amount = $previous_payment_data->amount;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $card->customerId;
                $payment_data->risk_level = $card->risk_level;

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $stripeCustomer->default_source;
                $data->payment_intents = $payment_data;

                $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $request->CountryId, $data, false);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $generate_payment_intent->customer)->first();

                if ($type && $type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($type && $type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($type && $type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($type && $type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                return $generate_payment_intent;

            } else {
                $payment_data = (object) array();
                $payment_data->amount = $previous_payment_data->amount;
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $card->customerId;
                $payment_data->risk_level = $card->risk_level;

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $stripeCustomer->default_source;
                $data->payment_intents = $payment_data;
                $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $request->CountryId, $data, false);

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $generate_payment_intent->customer)->first();

                if ($type && $type === 'awesome-shave-kits') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_ask' => null]);
                    $card_update->pymt_intent_ask = $generate_payment_intent->id;
                } elseif ($type && $type === 'ala-carte') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_alacarte' => null]);
                    $card_update->pymt_intent_alacarte = $generate_payment_intent->id;
                } elseif ($type && $type === 'trial-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_tk' => null]);
                    $card_update->pymt_intent_tk = $generate_payment_intent->id;
                } elseif ($type && $type === 'custom-plan') {
                    Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $generate_payment_intent->customer)->update(['pymt_intent_cp' => null]);
                    $card_update->pymt_intent_cp = $generate_payment_intent->id;
                }

                $card_update->save();

                return $generate_payment_intent;
            }
        }
    }

    public function confirmPaymentIntent(Request $request)
    {
        // return $request;
        $payment_intent_id = $request->payment_intent_id;
        $return_url = $request->return_url;
        $countryid = $request->country_id;
        $response = $this->stripe_ba->confirmPaymentIntents('baWebsite', $countryid, $payment_intent_id, $return_url);
        return $response;
    }

    public function postPaymentRedirect(Request $request)
    {
        // return response()->json($request)->header("Access-Control-Allow-Origin", "*");
        $session_selection = json_decode($request->get('session_selection'));
        $session_checkout = json_decode($request->get('session_checkout'));

        if (isset($request->get('stripe_response')['orderid']) && isset($request->get('stripe_response')['subscriptionid']) && isset($request->get('stripe_response')['receiptid'])) {
            $orderid = (int) $request->get('stripe_response')['orderid'];
            $subscriptionid = (int) $request->get('stripe_response')['subscriptionid'];
            $receiptid = (int) $request->get('stripe_response')['receiptid'];
        }
        if (isset($request->get('stripe_response')['countryid'])) {
            $countryid = (int) $request->get('stripe_response')['countryid'];
        }

        $type = $request->get('stripe_response')['type'];

        $miscData = (object) array("appType" => "baWebsite", "appLocale" => $request->get('lang_code'));

        $payment_intent = $request->get('stripe_response')['payment_intent'];
        $is_pay_now = false;
        $is_free_purchase = false;
        $do_update_risk_level = true;
        if (isset($request->get('stripe_response')['is_free_purchase'])) {
            if ($request->get('stripe_response')['is_free_purchase']) {
                $is_free_purchase = true;
            }
        }

        if (!$is_free_purchase) {
            // Retrieve payment status and customer id from payment intent
            $stripe_data = $this->stripe_ba->retrievePaymentIntents('baWebsite', $countryid, $payment_intent);
            sleep(3);
            $payment_status = $stripe_data->status;
            $customer_id = $stripe_data->customer;

            // If redirected from Pay Now flow, do the following
            // Else, do the First Time Purchase flow
            if ($payment_status == "succeeded") {
                // If payment success, clear payment intent id from card db
                switch ($type) {
                    case "trial-plan": 
                        {
                            Cards::where("customerId", $customer_id)->update(["pymt_intent_tk" => null]);
                            $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
                            $this->plansController->updateStatus(true, $type, $order_data, $stripe_data, $miscData);
                        }
                        break;
                    case "custom-plan": 
                        {
                            Cards::where("customerId", $customer_id)->update(["pymt_intent_cp" => null]);
                            $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
                            $this->plansController->updateStatus(true, $type, $order_data, $stripe_data, $miscData);
                        }
                        break;
                    case "awesome-shave-kits": 
                        {
                            Cards::where("customerId", $customer_id)->update(["pymt_intent_ask" => null]);
                            $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
                            $this->productController->updateStatus(true, $type, $order_data, $stripe_data, $miscData);
                        }
                        break;
                    case "alacarte": 
                        {
                            Cards::where("customerId", $customer_id)->update(["pymt_intent_alacarte" => null]);
                            $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
                            $this->productController->updateStatus(true, $type, $order_data, $stripe_data, $miscData);
                        }
                        break;
                }

                // call event to send receipts email
                event(new SendReceiptsEvent($order_data));
                // call event to  generate tax invoice pdf
                // event(new GenerateTaxInvoice($order_data));

                // Redirect to thank you page
                return response()->json(["status" => 'success', "order_id" => $order_data->orderid, "error" => null])->header("Access-Control-Allow-Origin", "*");

            } else {
                //Only update risk level for certain stripe errors
                if (isset($stripe_data->last_payment_error)) {
                    if (property_exists($stripe_data->last_payment_error, 'code')) {
                        if ($stripe_data->last_payment_error->code === "card_declined") {
                            if (property_exists($stripe_data->last_payment_error, 'decline_code')) {
                                if (in_array($stripe_data->last_payment_error->decline_code, config('global.all.stripe.ignore_risk_error_codes'))) {
                                    $do_update_risk_level = false;
                                }
                            }
                        }
                    }
                }

                // Create error object with error message from stripe
                $error_object = (object) array();
                $error_object->payment_error = $stripe_data->last_payment_error->message;

                if ($type === "trial-plan") {
                    $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
                    if ($do_update_risk_level) {
                        Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                    }
                    $this->plansController->updateStatus(false, $type, $order_data, $stripe_data, 'baWebsite');
                } else if ($type === "custom-plan") {
                    $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
                    if ($do_update_risk_level) {
                        Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                    }
                    $this->plansController->updateStatus(false, $type, $order_data, $stripe_data, 'baWebsite');
                } else if ($type === "awesome-shave-kits") {
                    $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
                    if ($do_update_risk_level) {
                        Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                    }
                    $this->productController->updateStatus(false, $type, $order_data, $stripe_data, 'baWebsite');
                } else if ($type === "alacarte") {
                    $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
                    if ($do_update_risk_level) {
                        Cards::where("customerId", $customer_id)->update(["risk_level" => "highest"]);
                    }
                    $this->productController->updateStatus(false, $type, $order_data, $stripe_data, 'baWebsite');
                }

                //Redirect back to the previous checkout page
                return response()->json(["status" => 'fail', "order_id" => null, "error" => $error_object])->header("Access-Control-Allow-Origin", "*");
            }
        } else {
            if ($type === "trial-plan") {
                $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
            } else if ($type === "custom-plan") {
                $order_data = (object) array('orderid' => $orderid, 'subscriptionid' => $subscriptionid, 'receiptid' => $receiptid);
            } else if ($type === "awesome-shave-kits") {
                $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
            } else if ($type === "alacarte") {
                $order_data = (object) array('orderid' => $orderid, 'receiptid' => $receiptid);
            }

            // call event to send receipts email
            event(new SendReceiptsEvent($order_data));
            // call event to  generate tax invoice pdf
            // event(new GenerateTaxInvoice($order_data));

            // Redirect to thank you page
            return response()->json(["status" => 'success', "order_id" => $order_data->orderid, "error" => null])->header("Access-Control-Allow-Origin", "*");
        }
        
    }

    public function api_initializePaymentIntentsAlacarte($User, $session_data, $langCode, $CountryId)
    {
        $this->current_country = Countries::where('id', (int) $CountryId)->first();
        $this->current_currency = $this->current_country->currencyCode;
        //Get previous session data
        //if session data exists, determine the product/plan type
        $current_price = 0;
        $productcountries = [];

        if ($session_data) {
            $current_price = $session_data->selection->summary->current_price;

            if (isset($session_data->selection->selected_handle_list)) {
                foreach ($session_data->selection->selected_handle_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_blade_list)) {
                foreach ($session_data->selection->selected_blade_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_addon_list)) {
                foreach ($session_data->selection->selected_addon_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_ask_list)) {
                foreach ($session_data->selection->selected_ask_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }
        }

        $customer = "";
        $payment_data = (object) array();

        $payment_data->amount = $current_price;
        $payment_data->currency = $this->current_currency;
        $payment_data->customer = $customer;

        // build data for paymentIntent
        $data = (object) array();
        $data->default_card_id = (object) array();
        $data->payment_intents = (object) array();
        $data->default_card_id = '';
        $data->payment_intents = $payment_data;

        $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $CountryId, $data, true);

        if (isset($productcountries)) {
            $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
        }
        return $data;
    }

    public function apiGeneratePaymentIntentsAlacarte($User, $session_data, $langCode, $CountryId)
    {

        $this->current_country = Countries::where('id', (int) $CountryId)->first();
        $this->current_currency = $this->current_country->currencyCode;
        $this->user_info = Countries::where('id', (int) $CountryId)->first();
        //Get previous session data
        //if session data exists, determine the product/plan type
        $session_data = json_decode($session_data);
        if ($session_data) {
            $productcountries = [];
            $_total_price_productcountryIds = [];

            if (isset($session_data->selection->selected_handle_list)) {
                foreach ($session_data->selection->selected_handle_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_blade_list)) {
                foreach ($session_data->selection->selected_blade_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_addon_list)) {
                foreach ($session_data->selection->selected_addon_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            if (isset($session_data->selection->selected_ask_list)) {
                foreach ($session_data->selection->selected_ask_list as $productcountry) {
                    array_push($productcountries, $productcountry);
                }
            }

            foreach ($productcountries as $xa) {
                array_push($_total_price_productcountryIds, ($xa->Price * $xa->Quantity));
            }
        }

        $customer = "";
        $payment_data = (object) array();
        //Is default card exist, add customer id to the session storage

        if (isset($User)) {
            if ($User["user"] !== "" && !$User["defaults"]["card"] !== null) {
                $session_data->customer_id = $User["defaults"]["card"]["customerId"];
                $customer = $User["defaults"]["card"]["customerId"];
                // get plan/product price
                //Find product price here
                // initialize payment intent
                $payment_data->amount = array_sum($_total_price_productcountryIds);
                $payment_data->currency = $this->current_currency;
                $payment_data->customer = $customer;
                $payment_data->risk_level = $User["defaults"]["card"]["risk_level"];
                // retrieve Stripe Customer and default Card Info
                $stripeCustomer = $this->stripe_ba->retrieveCustomer('baWebsite', (int) $CountryId, $customer);
                $card_id = $stripeCustomer["sources"]["data"][0]["id"];
                $payment_data->risk_level = $User["defaults"]["card"]["risk_level"];

                // build data for paymentIntent
                $data = (object) array();
                $data->default_card_id = (object) array();
                $data->payment_intents = (object) array();
                $data->default_card_id = $card_id;
                $data->payment_intents = $payment_data;

                try {
                    $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $CountryId, $data, true);
                } catch (Exception $e) {
                    return $e;
                }

                // update paymentIntentId to Cards Table
                $card_update = Cards::where('customerId', $customer)->where('isDefault', 1)->first();

                Cards::where('UserId', $card_update->UserId)->where('customerId', '!=', $customer)->update(['pymt_intent_alacarte' => null]);
                $card_update->pymt_intent_alacarte = $generate_payment_intent->id;

                $card_update->save();

                if (isset($productcountries)) {
                    $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
                }
                return $data;
            }
        } else {
            $payment_data->amount = array_sum($_total_price_productcountryIds);
            $payment_data->currency = $this->current_currency;
            $payment_data->customer = $customer;

            // build data for paymentIntent
            $data = (object) array();
            $data->default_card_id = (object) array();
            $data->payment_intents = (object) array();
            $data->default_card_id = '';
            $data->payment_intents = $payment_data;

            $generate_payment_intent = $this->stripe_ba->createPaymentIntents('baWebsite', (int) $CountryId, $data, true);

            if (isset($productcountries)) {
                $data = array('productcountries' => $productcountries, 'generate_payment_intent' => $generate_payment_intent);
            }
            return $data;
        }
    }
}
