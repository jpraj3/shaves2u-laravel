<?php

namespace App\Http\Controllers\Globals\Products;

use App;
use App\Helpers\Payment\Stripe;
use App\Helpers\LaravelHelper;
use App\Helpers\ProductHelper;
use App\Helpers\TaxInvoiceHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\StripeController as StripeController;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Ecommerce\User;
use App\Models\Products\ProductType;
use App\Services\CountryService as CountryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Jenssegers\Agent\Agent;

class ASKController extends Controller
{
    public function __construct()
    {
        // helpers & services
        $this->productHelper = new ProductHelper();
        $this->countryService = new CountryService();
        $this->userHelper = new UserHelper();
        $this->session = new SessionController();

        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        if ($this->country_info["country_id"] !== 8) {
            $this->stripe = new Stripe();
            $this->stripeController = new StripeController();
        }
        $this->abcdef = new TaxInvoiceHelper();
        // get default locale
        $this->locale = App::getLocale();
        $this->laravelHelper = new LaravelHelper();
        $this->country = json_decode(session()->get('currentCountry'), true);
        $this->country_id = $this->country['id'];
        $this->country_iso = strtoupper($this->country['codeIso']);
        $this->currency = $this->countryService->getCurrencyDisplay();

        // check for supportedlangs
        $this->_supportedLangs = $this->countryService->supportedLangs($this->country_id);
        $this->langCode = $this->_supportedLangs ? $this->locale : $this->country['defaultLang'];

        // products initialize
        $this->product_type_id = ProductType::where('name', 'ASK')->select('id')->first();
        $this->data = array();
        $this->data["ProductTypeId"] = $this->product_type_id["id"];
        $this->product_list = $this->productHelper->getProductByProductType($this->country_id, $this->langCode, $this->data);
        $this->appType = config('app.appType');
        $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;
        $this->agent = new Agent();
    }

    // Get featured ASK Products
    public function get()
    {
        $this->product_list = $this->productHelper->getProductDefaultImageWithData($this->product_list);
        return $this->product_list;
    }

    public function checkout(Request $request)
    {
        $product_country_id = $request->product_country_id;
        $buildProduct = [];
        foreach ($this->product_list as $p) {
            if ($p["productcountriesid"] == $product_country_id) {
                array_push($buildProduct, $p);
            }
        }
            // build tax data
            $tax = $this->laravelHelper->ConvertArraytoObject($this->countryService->tax($this->country_id));

        // $upload = $this->abcdef->__upload();
        $view = "checkout.alacarte.checkout-ask-desktop";
        if($this->agent->isMobile()) {
            $view = "checkout.alacarte.checkout-ask-mobile";
        }
        $states = $this->countryService->getAllStates($this->country_id);
             // mask selection
             $maskselection = null;
             $countmaskqty = 0;
             if($this->country_id == 1 || $this->country_id == 7){
             $maskselection = $this->productHelper->getFreeMaskSelection($this->country_id, strtoupper(app()->getLocale()));
             if($maskselection){
              foreach ($maskselection as $m) {
                  $countmaskqty = $m->qty +  $countmaskqty;
              }
             }
             }
             // mask selection
        if (Auth::check()) {
            $User = $this->userHelper->getUserDetails(Auth::user()->id);
            if ($this->country_id != 8) {
                $payment_intent = $this->stripeController->initializePaymentIntents($User, 'checkout_selection_ask');
                $generate_payment_intent = $payment_intent["generate_payment_intent"];
                $checkout_details = $payment_intent["productcountries"];
            } else {
                $payment_intent = "";
                $generate_payment_intent = "";
                $checkout_details = "";
            }
            $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
            $session_data = json_decode($this->session->_session("checkout_selection_ask", "get", null));
            $productname = "";
            $productcount = 0;
            $products = $session_data->selection->step1;

            // push selected products into productcountry data array
            foreach ($products as $p) {
                if ($productcount == 0) {
                    $productname = $p->productname;
                    $productcount = $productcount + 1;
                } else {
                    $productname = $productname . "," . $p->productname;
                    $productcount = $productcount + 1;
                }
            }

            $price = $this->getPrice($session_data, $this->country_id, $this->langCode);
            $current_price = $price["originaltotalprice"];
            $shipping_fee = number_format($price["shippingfee"],2);
            $next_price = $price["originaltotalprice"];
            $current_totalprice = $current_price + $shipping_fee;
            $taxAmount = $tax->taxRate / 100 * $current_totalprice;
            $current_totalprice = $current_totalprice + $taxAmount;
            $next_totalprice = $next_price + $shipping_fee;
            return view($view)
                ->with('maskselection',$maskselection)
                ->with('countmaskqty',$countmaskqty)
                ->with('loginStatus', 'post-login')
                ->with('currentCountryIso', strtoupper(json_decode(session()->get('currentCountry'), true)['codeIso']))
                ->with('currentCountryid', $this->country_id)
                ->with('langCode', strtoupper(app()->getLocale()))
                ->with('urllangCode', $urllangCode)
                ->with('states',$states)
                ->with('buildProduct', $buildProduct)
                ->with('User', $User)
                ->with('session_data', json_encode($session_data))
                ->with('payment_intent', $generate_payment_intent)
                ->with('checkout_details', $checkout_details)
                ->with('productname', $productname)
                ->with('current_price', $current_price)
                ->with('shipping_fee', $shipping_fee)
                ->with('next_price', $next_price)
                ->with('current_totalprice', $current_totalprice)
                ->with('next_totalprice', $next_totalprice)
                ->with('taxAmount', $taxAmount)
                ->with('taxRate', $tax->taxRate)
                ->with('currency', $this->currency);
        } else {
            if ($this->country_id != 8) {
                $payment_intent = $this->stripeController->initializePaymentIntents(null, 'checkout_selection_ask');
                $generate_payment_intent = $payment_intent["generate_payment_intent"];
                $checkout_details = $payment_intent["productcountries"];
            } else {
                $payment_intent = "";
                $generate_payment_intent = "";
                $checkout_details = "";
            }

            $urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
            $session_data = json_decode($this->session->_session("checkout_selection_ask", "get", null));

            $productname = "";
            $productcount = 0;
            $products = $session_data->selection->step1;

            // push selected products into productcountry data array
            foreach ($products as $p) {
                if ($productcount == 0) {
                    $productname = $p->productname;
                    $productcount = $productcount + 1;
                } else {
                    $productname = $productname . "," . $p->productname;
                    $productcount = $productcount + 1;
                }
            }
            $price = $this->getPrice($session_data, $this->country_id, $this->langCode);
            $current_price = $price["originaltotalprice"];
            $shipping_fee = number_format($price["shippingfee"],2);
            $next_price = $price["originaltotalprice"];
            $current_totalprice = $current_price + $shipping_fee;
            $taxAmount = $tax->taxRate / 100 * $current_totalprice;
            $current_totalprice = $current_totalprice + $taxAmount;
            $next_totalprice = $next_price + $shipping_fee;
            return view($view)
                ->with('maskselection',$maskselection)
                ->with('countmaskqty',$countmaskqty)
                ->with('loginStatus', 'pre-login')
                ->with('currentCountryIso', strtoupper(json_decode(session()->get('currentCountry'), true)['codeIso']))
                ->with('currentCountryid', $this->country_id)
                ->with('langCode', strtoupper(app()->getLocale()))
                ->with('urllangCode', $urllangCode)
                ->with('states',$states)
                ->with('buildProduct', $buildProduct)
                ->with('session_data', json_encode($session_data))
                ->with('payment_intent', $generate_payment_intent)
                ->with('checkout_details', $checkout_details)
                ->with('productname', $productname)
                ->with('current_price', $current_price)
                ->with('shipping_fee', $shipping_fee)
                ->with('next_price', $next_price)
                ->with('current_totalprice', $current_totalprice)
                ->with('next_totalprice', $next_totalprice)
                ->with('taxAmount', $taxAmount)
                ->with('taxRate', $tax->taxRate)
                ->with('currency', $this->currency);
        }
    }

    // CHECKOUT PROCESS FOR ASK

    public function proceedPayment(Request $request)
    {
        $langCode = strtoupper(app()->getLocale());
        $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $session_data_selection = $this->session->_session("checkout_selection_ask", "get", null);
        $session_data_selection_decoded = json_decode($session_data_selection);
        $session_data_checkout = $this->session->_session("checkout_ask", "get", null);
        $session_data_checkout_decoded = json_decode($session_data_checkout);
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);

        $xs = $session_data_selection_decoded . step1;
        $_selection = (array) $xs;
        $xc = $session_data_selection_decoded . payment_data;
        $total_price = [];
        foreach ($xc as $p) {
            $test = array_search($p->id, $_selection, mandatory);
            var_dump($test);
            // $total_price . push($p . sellPrice);
        }

        if ($session_data_selection_decoded && $session_data_checkout_decoded) {
            if (array_key_exists('id', $session_data_checkout_decoded->checkout->selected_card) && array_key_exists('customer_id', $session_data_checkout_decoded->checkout->selected_card) && array_key_exists('current_price', $session_data_selection_decoded->selection->step5) && array_key_exists('shipping_fee', $session_data_selection_decoded->selection->step5) && array_key_exists('next_price', $session_data_selection_decoded->selection->step5)) {
                try {
                    $this->checkPromotion($session_data_selection_decoded, $session_data_checkout_decoded);
                } catch (Exception $e) {

                    throw $e;
                }
            } else {
                return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            }
        } else {
            return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        }
    }

    public function getPrice($selection_session, $country_id, $lang_code)
    {
        // get langCode & countryId from constructor
        $countryid = $country_id;
        $lang = $lang_code;

        // build shipping fee data
        $shippingfee = "";
        if ($selection_session->selection->product_type) {
            if ($selection_session->selection->product_type == "awesome-shave-kits") { //Awesome Shave Kits
                $getshippingfee = $this->countryService->shippingFee($countryid, $lang);
            } else if ($selection_session->selection->product_type == "alacarte") { // Ala Carte
                $getshippingfee = $this->countryService->shippingFeeTrial($countryid, $lang);
            } else { // Other possibilities
                $getshippingfee = $this->countryService->shippingFee($countryid, $lang);
            }
        } else {
            $getshippingfee = $this->countryService->shippingFee($countryid, $lang);
        }

        if ($getshippingfee) {
            $shippingfee = (float) $getshippingfee["shippingfee"];
        } else {
            $shippingfee = 0;
        }

        // build tax data
        $tax = $this->laravelHelper->ConvertArraytoObject($this->countryService->tax($countryid));

        // build data for productcountry search
        $products = $selection_session->selection->step1;
        $pcs = array(
            "appType" => $this->appType, // appType = [ecommerce,baWebsite]
            "isOnline" => $this->isOnline,
            "isOffline" => $this->isOffline,
            "product_country_ids" => [],
        );

        // push selected products into productcountry data array
        foreach ($products as $p) {
            array_push($pcs["product_country_ids"], $p->productcountryid);
        }

        // check for bundles for the selected products
        // $_isBundle = $this->productHelper->getProductBundles($pcs["product_country_ids"]);
        $_isBundle = false;
        // get productcountries data for all selected products
        $_getPCs = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($country_id, $lang_code, $pcs));
        if (!$_getPCs) {
            throw new Exception('ProductCountryIds [' . $pcs["product_country_ids"] . '] not found. Returns empty');
        } else {
            $originaltotalprice = 0;
            $taxAmount = 0;
            $totalprice = 0;
            $totalprice_bundle = 0;
            $discount = 0;
            $totaldiscount = 0;
            $discount_bundle = 0;
            $discounted_products = [];
            $discounted_products_in_bundle = [];

            // if contain bundle --
            if ($_isBundle) {
                // bundle logic goes here
            } else {

                // non-bundle logic goes here
                $payment_data = [
                    "payment_gateway" => "stripe",
                    "originaltotalprice" => 0,
                    "totalprice" => 0,
                    "discount" => 0,
                    "totaldiscount" => 0,
                    "discounted_products" => [],
                ];

                // calculate original {totalprice}
                $totalprice = $this->productHelper->getProductSumPrice($pcs["product_country_ids"]);

                // if ($promotion) {
                //     //promotion logic goes here
                // } else {
                //     // non-promotion goes here
                // }

                $originaltotalprice = $totalprice;
                $taxAmount = $tax->taxRate / 100 * $totalprice;
                $totalprice = $totalprice + $taxAmount - $totaldiscount + $shippingfee;

                $totalprice = number_format($totalprice, 2, '.', '');

                $payment_data["originaltotalprice"] = $originaltotalprice;
                $payment_data["totalprice"] = $totalprice;
                $payment_data["taxAmount"] = $taxAmount;
                $payment_data["discount"] = $discount;
                $payment_data["totaldiscount"] = $totaldiscount;
                $payment_data["shippingfee"] = $shippingfee;
                $payment_data["discounted_products"] = $discounted_products;
            }

            $this->laravelHelper->ConvertArraytoObject($payment_data);
            // next journey : Referral
            return $payment_data;
        }
    }

}
