<?php

namespace App\Http\Controllers\Globals\Products;

use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Queue\SaleReportQueueService;
use App\Queue\TaxInvoiceQueueService;
use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\Payment\Stripe as Stripe;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;

// Helpers
use App\Helpers\UserHelperAPI;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Orders\PaymentHistories;
use App\Models\Promotions\Promotions;
use App\Models\Promotions\PromotionCodes;
use App\Models\Receipts\Receipts;
use App\Services\APICountryService;
// Models
use App\Services\TaxInvoiceService;
use Carbon\Carbon;
use Exception;

// Services
use Illuminate\Http\Request;

class ProductCashController extends Controller
{
    public function __construct()
    {
        $this->api_country_service = new APICountryService();
        $this->session = new SessionController();
        $this->laravelHelper = new LaravelHelper();
        $this->lang_code = null;
        $this->country_id = null;
        $this->userHelper = new UserHelperAPI();
        $this->productHelper = new ProductHelper();
        $this->planhelper = new PlanHelper();
        $this->orderHelper = new OrderHelper();

        $this->stripe = new Stripe();
        $this->appType = null;
        $this->isOnline = 0;
        $this->isOffline = 1;
        $this->zero = "0";
        $this->taxInvoiceService = new TaxInvoiceService();
        $this->minspendresult = 0;
        $this->emailQueueHelper = new EmailQueueHelper();
    }

    public function confirmPurchase(Request $request)
    {

        if (isset($request->appType)) {
            if ($request->appType === 'baWebsite') {
                $session_data_selection = $request->session_selection;
                $session_data_checkout = $request->session_checkout;

                $this->session_data_selection_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($session_data_selection);
                $this->session_data_checkout_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($session_data_checkout);

                $urllangCode = strtoupper($request->urllangCode);
                $langCode = strtolower($request->langCode);
                $currentCountryIso = strtolower($request->country_code);
                $this->urllangCode = $urllangCode;
                $this->langCode = $this->lang_code = $langCode;
                $this->currentCountryIso = $currentCountryIso;
                $this->appType = $request->appType;
                $this->isOnline = 0;
                $this->isOffline = 1;
                $this->baCountryId = $this->country_id = (int) $request->country_id;
                $this->country_info = Countries::where('id', $this->baCountryId)->first();
                $this->seller = json_decode($request->seller, true);
                $this->event_data = $request->event_data;
                $this->isDirectTrial = $request->isDirectTrial === "true" ? 1 : 0;
                return $this->checkPromotion($this->session_data_selection_decoded, $this->session_data_checkout_decoded);
            }
        }
    }

    public function checkPromotion($selection_session, $checkout_session)
    {
        // Promotion
        try {
            $promotion = "";
            if (array_key_exists('promo', $checkout_session->checkout)) {
                if (array_key_exists('promo_id', $checkout_session->checkout->promo)) {
                    $promotion = $checkout_session->checkout->promo;
                }
            }
            return $this->checkReferral($selection_session, $checkout_session, $promotion);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function checkReferral($selection_session, $checkout_session, $promotion)
    {

        try {
            $referral = "";
            return $this->checkProductPrice($selection_session, $checkout_session, $promotion, $referral);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function checkProductPrice($selection_session, $checkout_session, $promotion, $referral)
    {
        try {
            // get langCode & countryId from constructor
            $countryid = $this->country_id;
            $lang = $this->lang_code;

            // build shipping fee data
            $shippingfee = "";

            if ($this->appType === 'baWebsite') {
                $getshippingfee = $this->api_country_service->shippingFee($countryid, $lang);
            } else {
                if ($selection_session->selection->product_type) {
                    if ($selection_session->selection->product_type == "awesome-shave-kits") { //Awesome Shave Kits
                        $getshippingfee = $this->api_country_service->shippingFee($countryid, $lang);
                    } else if ($selection_session->selection->product_type == "alacarte") { // Ala Carte
                        $getshippingfee = $this->api_country_service->shippingFeeTrial($countryid, $lang);
                    } else { // Other possibilities
                        $getshippingfee = $this->api_country_service->shippingFee($countryid, $lang);
                    }
                } else {
                    $getshippingfee = $this->api_country_service->shippingFee($countryid, $lang);
                }
            }

            if ($getshippingfee) {
                $shippingfee = (float) $getshippingfee["shippingfee"];
            } else {
                $shippingfee = 0;
            }

            // build tax data
            $tax = $this->laravelHelper->ConvertArraytoObject($this->api_country_service->tax($countryid));
            $this->tax = $tax;
            // build data for productcountry search

            if ($this->appType === 'baWebsite') {
                $products = [];

                if (isset($selection_session->selection->selected_handle_list)) {
                    foreach ($selection_session->selection->selected_handle_list as $handle) {
                        array_push($products, $handle);
                    }
                }

                if (isset($selection_session->selection->selected_blade_list)) {
                    foreach ($selection_session->selection->selected_blade_list as $blade) {
                        array_push($products, $blade);
                    }
                }

                if (isset($selection_session->selection->selected_addon_list)) {
                    foreach ($selection_session->selection->selected_addon_list as $addon) {
                        array_push($products, $addon);
                    }
                }

                if (isset($selection_session->selection->selected_ask_list)) {
                    foreach ($selection_session->selection->selected_ask_list as $ask) {
                        array_push($products, $ask);
                    }
                }
            } else {
                $products = $selection_session->selection->step1;
            }

            $pcs = array(
                "appType" => $this->appType, // appType = [ecommerce,baWebsite]
                "isOnline" => $this->isOnline,
                "isOffline" => $this->isOffline,
                "product_country_ids" => [],
            );

            if ($this->appType === 'baWebsite') {
                // push selected products into productcountry data array
                foreach ($products as $p) {
                    array_push($pcs["product_country_ids"], $p->ProductCountryId);
                }
            } else {
                foreach ($products as $p) {
                    array_push($pcs["product_country_ids"], $p->productcountryid);
                }
            }

            // check for bundles for the selected products
            // $_isBundle = $this->productHelper->getProductBundles($pcs["product_country_ids"]);
            $_isBundle = false;
            // get productcountries data for all selected products
            $_getPCs = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($this->country_id, $this->lang_code, $pcs));
            if (!$_getPCs) {
                throw new Exception('ProductCountryIds [' . $pcs["product_country_ids"] . '] not found. Returns empty');
            } else {
                $originaltotalprice = 0;
                $taxAmount = 0;
                $totalprice = 0;
                $totalprice_bundle = 0;
                $discount = 0;
                $totaldiscount = 0;
                $discount_bundle = 0;
                $discounted_products = [];
                $discounted_products_in_bundle = [];
                $subtotalprice = 0;

                // if contain bundle --
                if ($_isBundle) {
                    // bundle logic goes here
                } else {

                    // non-bundle logic goes here
                    $payment_data = [
                        "payment_gateway" => "stripe",
                        "originaltotalprice" => 0,
                        "totalprice" => 0,
                        "discount" => 0,
                        "totaldiscount" => 0,
                        "discounted_products" => [],
                    ];

                    if ($this->appType === 'baWebsite') {
                        $totalprice = 0;
                        foreach ($products as $p) {
                            $totalprice += $p->Price * $p->Quantity;
                        }
                    } else {
                        // calculate original {totalprice}
                        $totalprice = $this->productHelper->getProductSumPrice($pcs["product_country_ids"]);
                    }

                    // if ($promotion) {
                    //     //promotion logic goes here
                    // } else {
                    //     // non-promotion goes here
                    // }

                    // Promotion
                    if ($promotion) {
                        // Promotion min spend block
                        if ($promotion->promo_minSpend) {
                            if ($promotion->promo_minSpend > $totalprice) {
                                $this->minspendresult = 1;
                            }
                        }
                    }
                    // Promotion free shippingfee
                    if ($promotion) {
                        if ($promotion->promo_isFreeShipping) {
                            if ($promotion->promo_isFreeShipping === 1) {
                                if (($promotion->promo_promotionType == "Instant" || $promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                                    $shippingfee = 0;
                                }
                            }
                        }
                    }
                    $discountforfreeexistproduct = 0;
                    $originaltotalprice = $totalprice;

                    $subtotalprice = $totalprice;
                    // Promotion free exist product
                    if ($promotion) {
                        if (($promotion->promo_promotionType == "Instant" || $promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0 && $promotion->promo_ablefreeexistproduct === 1) {
                            $discountforfreeexistproduct = $discountforfreeexistproduct + $promotion->promo_freeexistresultproductprice;
                        }
                    }
                    if ($discountforfreeexistproduct > 0) {
                        $totalprice = $totalprice - $discountforfreeexistproduct;
                    }
                    $totaldiscountprice = 0;
                    // Promotion
                    if ($promotion) {
                        $totaldiscount = (($totalprice * $promotion->promo_discount) / 100);
                        $totaldiscountprice = $totalprice - $totaldiscount;

                        $totaldiscountprice = $totaldiscountprice + $shippingfee;
                        $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                        $totaldiscountprice = $totaldiscountprice + 0.00;

                        $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                        // Promotion min spend block
                        if ($this->minspendresult == 1) {
                            $totaldiscount = 0;
                            $totaldiscountprice = $totalprice + $shippingfee;
                            $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                            $totaldiscountprice = $totaldiscountprice + 0.00;
                            $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                        }
                        // Promotion maxdiscount
                        if ($promotion->promo_maxDiscount) {
                            if ($promotion->promo_maxDiscount < $totaldiscount) {
                                $totaldiscount = $promotion->promo_maxDiscount;
                                $totaldiscountprice = $totalprice - $promotion->promo_maxDiscount;
                                $totaldiscountprice = $totaldiscountprice + $shippingfee;
                                $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                                $totaldiscountprice = $totaldiscountprice + 0.00;
                                $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                            }
                        }
                        // Promotion free exist product
                        if ($discountforfreeexistproduct > 0) {
                            $totaldiscount = $totaldiscount + $discountforfreeexistproduct;
                        }

                        // Promotion no negative number
                        if ($totaldiscountprice <= 0) {
                            $totaldiscountprice = 0;
                            $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                        }
                    }

                    $totalprice = $totalprice + $shippingfee;
                    $taxAmount = $tax->taxRate / 100 * $totalprice;
                    $totalprice = $totalprice + 0.00;

                    $totalprice = number_format($totalprice, 2, '.', '');

                    $payment_data["originaltotalprice"] = $originaltotalprice;
                    $payment_data["totalprice"] = $totalprice;
                    $payment_data["subtotal"] = $subtotalprice;
                    $payment_data["taxAmount"] = $taxAmount;
                    $payment_data["discount"] = $discount;
                    $payment_data["totaldiscount"] = $totaldiscount;
                    $payment_data["totaldiscountprice"] = $totaldiscountprice;
                    $payment_data["shippingfee"] = $shippingfee;
                    $payment_data["discounted_products"] = $discounted_products;
                }

                $this->laravelHelper->ConvertArraytoObject($payment_data);
                // next journey : Referral
                return $this->getUserInfo($selection_session, $checkout_session, $promotion, $referral, $_getPCs, $payment_data, $shippingfee, $tax);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getUserInfo($selection_session, $checkout_session, $promotion, $referral, $_getPCs, $payment_data, $shippingfee, $tax)
    {

        $d_address_id = ''; // delivery address id
        $b_address_id = ''; // billing address id

        $selection_session = $selection_session;
        $checkout_session = $checkout_session;

        // build address info from checkout session
        if ($checkout_session->checkout->selected_address) {
            $d_address_id = $checkout_session->checkout->selected_address->delivery_address;
            $b_address_id = $checkout_session->checkout->selected_address->billing_address;

            // retrieve data from database
            $deliveryInfo = $this->userHelper->getUserInfoFromDeliveryId($d_address_id);
            $billingInfo = $this->userHelper->getUserInfoFromBillingId($b_address_id);

            // retrieve user info from database using delivery & billing address id
            $UserId = $deliveryInfo ? $deliveryInfo->UserId : ($billingInfo ? $billingInfo->UserId : '');

            if (!$UserId) {
                throw new Exception('UserId not found. Returns empty');
            } else {
                // $this->userHelper->getUserDetails -> returns data for delivery, billing, cards, user, default_card etc
                $user_info = $this->laravelHelper->ConvertArraytoObject($this->userHelper->getUserDetails($UserId));
                return $this->getReferralDiscount($selection_session, $checkout_session, $promotion, $referral, $deliveryInfo, $billingInfo, $shippingfee, $_getPCs, $payment_data, $tax, $user_info);
            }
        }
    }

    public function getReferralDiscount($selection_session, $checkout_session, $promotion, $referral, $deliveryInfo, $billingInfo, $shippingfee, $_getPCs, $payment_data, $tax, $user_info)
    {

        try {
            $referral_discount = "";
            return $this->createOrder($selection_session, $checkout_session, $promotion, $referral, $deliveryInfo, $billingInfo, $shippingfee, $_getPCs, $payment_data, $tax, $user_info, $referral_discount);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function createOrder($selection_session, $checkout_session, $promotion, $referral, $deliveryInfo, $billingInfo, $shippingfee, $_getPCs, $payment_data, $tax, $user_info, $referral_discount)
    {

        try {
            //remember email phone full name ssn
            //remember google api source medium campaign term content promotion discpont

            $data = null;
            $utm_parameters = $this->session->_session("utm_parameters", "get", null);
            if($utm_parameters && !is_array($utm_parameters)){
                // if ($this->checkJsondecode($utm_parameters)) {
                $utm_parameters = json_decode($utm_parameters);
                $data = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
                // }
            }

            // build order data
            $order_info = [
                "OrderId" => '',
            ];

            // create orders
            $orders = new Orders();

            // insert data for promotion in orders
            if ($promotion) {
                $orders->promoCode = null;
                $orders->PromotionId = null;
            }

            // insert data for delivery in orders
            if ($deliveryInfo || $billingInfo) {
                if ($deliveryInfo->SSN !== null || $billingInfo->SSN !== null) {
                    $orders->SSN = $deliveryInfo->SSN ? $deliveryInfo->SSN : ($billingInfo->SSN ? $billingInfo->SSN : null);
                } else {
                    $orders->SSN = null;
                }
            }
            // Promotion
            if ($promotion) {
                if (array_key_exists('promo_promotionType', $promotion)) {
                    if (($promotion->promo_promotionType == "Instant" || $promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                        if (array_key_exists('promo_code', $promotion) && array_key_exists('promo_id', $promotion)) {
                            $orders->promoCode = $promotion->promo_code;
                            $orders->PromotionId = $promotion->promo_id;
                        }
                    }
                }
            }
            $orders->email = $user_info->user->email;
            $orders->phone = $user_info->user->phone;
            $orders->fullName = $user_info->user->firstName . $user_info->user->lastName;
            $orders->DeliveryAddressId = $deliveryInfo->id;
            $orders->BillingAddressId = $billingInfo->id;
            $orders->taxRate = $tax->taxRate;
            $orders->taxName = $tax->taxName;
            $orders->CountryId = $this->country_id;
            $orders->subscriptionIds = null;
            $orders->source = $data ? (isset($data->utm_source) ? $data->utm_source : null) : null;
            $orders->medium = $data ? (isset($data->utm_medium) ? $data->utm_medium : null) : null;
            $orders->campaign = $data ? (isset($data->utm_campaign) ? $data->utm_campaign : null) : null;
            $orders->term = $data ? (isset($data->utm_term) ? $data->utm_term : null) : null;
            $orders->content = $data ? (isset($data->utm_content) ? $data->utm_content : null) : null;
            $orders->status = "Payment Received";
            $orders->payment_status = 'Cash';
            $orders->paymentType = 'cash';
            if ($this->appType === 'baWebsite') {
                $orders->SellerUserId = $this->seller["id"];
                $orders->channelType = $this->event_data["ba_channel_type"];
                $orders->eventLocationCode = $this->event_data["ba_event_location_code"];
                $orders->MarketingOfficeId = $this->seller["MarketingOfficeId"];
                $orders->isDirectTrial = $this->isDirectTrial;
                $orders->status = $this->isDirectTrial === 1 ? "Completed" : "Payment Received";
            }
            if ($this->country_id == 7) {
                $orders->carrierAgent = "courex";
            } else {
                $orders->carrierAgent = null;
            }

            $orders->UserId = $user_info->user->id;
            $orders->SellerUserId = $this->appType === 'ecommerce' ? null : ($this->appType === 'baWebsite' ? $this->seller["id"] : null);
            $orders->save();
            $order_info["OrderId"] = $orders->id;

            // create orderdetails
            foreach ($_getPCs as $_o) {
                $orderdetails = new OrderDetails();
                $orderdetails->qty = 1;
                $orderdetails->price = $_o->sellPrice;
                if ($this->appType === 'baWebsite') {
                    $orderdetails->currency = $this->country_info->currencyDisplay;
                }
                $orderdetails->startDeliverDate = null;
                $orderdetails->created_at = Carbon::now();
                $orderdetails->updated_at = Carbon::now();
                $orderdetails->OrderId = $order_info["OrderId"];
                $orderdetails->ProductCountryId = $_o->ProductCountryId;
                // Promotion free exist product
                if ($promotion) {
                    if ($promotion->promo_freeexistresultproduct) {
                        if (in_array($_o->ProductCountryId, $promotion->promo_freeexistresultproduct)) {
                            $orderdetails->isFreeProduct = 1;
                        }
                    }
                }
                $orderdetails->save();
            }

            // create orderhistories
            $ordershistories = new OrderHistories();
            $ordershistories->message = $orders->status;
            $ordershistories->OrderId = $order_info["OrderId"];
            $ordershistories->isRemark = 0;
            $ordershistories->created_at = Carbon::now();
            $ordershistories->updated_at = Carbon::now();
            $ordershistories->save();
            $this->session->_session("utm_parameters", "clear", null);
            return $this->createReceipt($selection_session, $checkout_session, $promotion, $referral, $shippingfee, $_getPCs, $payment_data, $tax, $user_info, $referral_discount, $order_info);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    // public function checkJsondecode($data){
    //     if (!empty($data)) {
    //         json_decode($data);
    //         return (json_last_error() === JSON_ERROR_NONE);
    // }
    // return false;  
    // }
    
    public function createReceipt($selection_session, $checkout_session, $promotion, $referral, $shippingfee, $_getPCs, $payment_data, $tax, $user_info, $referral_discount, $order_info)
    {

        try {
            $receipts = new Receipts();
            // Promotion
            $receipts->totalPrice = $payment_data["totalprice"];
            $receipts->discountAmount = 0;
            $receipts->subTotalPrice = $payment_data["subtotal"];
            if ($promotion) {
                if (array_key_exists('promo_promotionType', $promotion)) {
                    if (($promotion->promo_promotionType == "Instant" || $promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                        $receipts->totalPrice = $payment_data["totaldiscountprice"];
                        $receipts->subTotalPrice = $payment_data["subtotal"];
                        $receipts->discountAmount = $payment_data["totaldiscount"];
                    }
                }
            }

            if ($this->appType === 'baWebsite') {
                $receipts->currency = $this->country_info->currencyDisplay;
            }
            $receipts->originPrice = $payment_data["originaltotalprice"];
            $receipts->taxAmount = $payment_data["taxAmount"];
            $receipts->shippingFee = $shippingfee;
            $receipts->OrderId = $order_info["OrderId"];
            $receipts->save();
            $receiptid = $receipts->id;

            $prepayment_data = [
                'receiptId' => $receiptid,
                'orderId' => $order_info["OrderId"],
                'final_price' => $receipts->totalPrice,
            ];

            $prepayment_data = $this->laravelHelper->ConvertArraytoObject($prepayment_data);

            $order_data = (object) array();
            $order_data->orderid = $prepayment_data->orderId;
            $order_data->receiptid = $prepayment_data->receiptId;
            $checkout_session->checkout->order_data = (object) array();
            $checkout_session->checkout->order_data = $order_data;

            return $this->processPaymentSuccess($order_data, false);
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Process after successful payment
    public function processPaymentSuccess($orderData, $isFree)
    {
        $orderid = $orderData->orderid;
        $receiptid = $orderData->receiptid;
        Orders::where('id', $orderid)
            ->update([
                'status' => $this->isDirectTrial === 1 ? "Completed" : "Payment Received",
                'payment_status' => 'Cash',
                'imp_uid' => '',
                'merchant_uid' => '',
            ]);
        $ordershistories = new OrderHistories();
        $ordershistories->message = $this->isDirectTrial === 1 ? "Completed" : "Payment Received";
        $ordershistories->OrderId = $orderid;
        $ordershistories->save();


        $paymentHistory = new PaymentHistories();
        $paymentHistory->OrderId = $orderData->orderid;
        $paymentHistory->message = 'Cash';
        $paymentHistory->isRemark = 0;
        $paymentHistory->created_by = null;
        $paymentHistory->updated_by = null;
        $paymentHistory->save();
        // add order to queue service
        $_redis_order_data = (object) array(
            'OrderId' => $orderid,
            'CountryId' => $this->country_id,
            'CountryCode' => strtoupper($this->country_info->country_iso),
        );

        $this->orderHelper->addTaskToOrderQueueService($_redis_order_data);
        $_redis_sales_report_data = [ 'id' => $orderid, 'bulkOrder' => false];
        SaleReportQueueService::addHash($orderid, $_redis_sales_report_data);
        $_redis_tax_invoice_data = [ 'id' => $orderid, 'CountryId' => $this->appType === 'baWebsite' ? $this->baCountryId : $this->country_id];
        TaxInvoiceQueueService::addHash($orderid, $_redis_tax_invoice_data);
        // Promotion
        $orderdata = Orders::where('id', $orderid)->first();
        $userid = $orderdata->UserId;
        if ($orderdata->PromotionId) {
            $PromotionCodes = Promotions::where('id', $orderdata->PromotionId)
                ->first();
                if ($orderdata->promoCode) {
                    if ($PromotionCodes->isGeneric != 1) {
                        PromotionCodes::where('code', $orderdata->promoCode)
                        ->where("PromotionId", $orderdata->PromotionId)
                        ->update([
                            'isValid' => 0
                        ]);
                    }   
                } 
            $PromotionCodes->appliedTo = str_replace("{", "", $PromotionCodes->appliedTo);
            $PromotionCodes->appliedTo = str_replace("}", "", $PromotionCodes->appliedTo);
            $splitapplyto = $PromotionCodes->appliedTo;
            // $peruse = $PromotionCodes->timePerUser;
            if (strpos($splitapplyto, ',') !== false) {
                $splitapplyto = explode(",", $PromotionCodes->appliedTo);
            }
            // $promoapplyupdate = ",".$userid.":1";
            $promoapplyupdate = "";
            $combineapply = "{";
            $count = 1;
            if (is_array($splitapplyto)) {
                foreach ($splitapplyto as $at) {
                    if (strpos($at, ':') !== false) {
                        $splitqty = explode(":", $at);
                        if ($userid == $splitqty[0]) {
                            $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                            if ($count == 1) {
                                $combineapply = $combineapply . $promoapplyupdate;
                                $count++;
                            } else {
                                $combineapply = $combineapply . "," . $promoapplyupdate;
                                $count++;
                            }
                        } else {
                            if ($count == 1) {
                                $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                                $count++;
                            } else {
                                $combineapply = $combineapply . "," . $splitqty[0] . ":" . $splitqty[1];
                                $count++;
                            }
                        }
                    }
                }
            } else {
                if (strpos($splitapplyto, ':') !== false) {
                    $splitqty = explode(":", $splitapplyto);
                    if ($userid == $splitqty[0]) {
                        $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                        $combineapply = $combineapply . $promoapplyupdate;
                    } else {
                        $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                    }
                }
            }
            if ($promoapplyupdate) {
                $combineapply = $combineapply . "}";
            } else {
                if ($splitapplyto) {
                    $combineapply = $combineapply . "," . $userid . ":1" . "}";
                } else {
                    $combineapply = $combineapply . $userid . ":1" . "}";
                }
            }

            // Promotion Free Product
            if ($PromotionCodes->freeProductCountryIds) {
                if (($PromotionCodes->promotionType == "Instant" || $PromotionCodes->promotionType == "Recurring")) {
                    $PromotionCodes->freeProductCountryIds = str_replace("[", "", $PromotionCodes->freeProductCountryIds);
                    $PromotionCodes->freeProductCountryIds = str_replace("]", "", $PromotionCodes->freeProductCountryIds);
                    $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);
                    $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);

                    $splitproduct = $PromotionCodes->freeProductCountryIds;
                    if (strpos($splitproduct, ',') !== false) {
                        $splitproduct = explode(",", $PromotionCodes->freeProductCountryIds);
                    }
                    $productseperate = [];
                    if (is_array($splitproduct)) {
                        foreach ($splitproduct as $product) {
                            array_push($productseperate, $product);
                        }
                    } else {
                        array_push($productseperate, $splitproduct);
                    }

                    // Prepare parameters for trial plan order details
                    $productcountyidarray = array(
                        "appType" => $this->appType,
                        "isOnline" => 1,
                        "isOffline" => 1,
                        "product_country_ids" => $productseperate,
                    );
                    $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($this->country_id, $this->lang_code, $productcountyidarray));

                    foreach ($product as $pci) {
                        $orderdetails = new OrderDetails();
                        $orderdetails->qty = 1;
                        $orderdetails->price = $pci->sellPrice;
                        $orderdetails->currency = $this->country_info->data_from_db->currencyDisplay;
                        $orderdetails->startDeliverDate = null;
                        $orderdetails->created_at = Carbon::now();
                        $orderdetails->updated_at = Carbon::now();
                        $orderdetails->OrderId = $orderid;
                        $orderdetails->ProductCountryId = $pci->ProductCountryId;
                        $orderdetails->isFreeProduct = 1;
                        $orderdetails->isAddon = 0;
                        $orderdetails->save();
                    }
                }
            }

            Promotions::where('id', $orderdata->PromotionId)
                ->update([
                    'appliedTo' => $combineapply,
                ]);
        };
        $this->buildOrderConfirmationEdm($orderData);

        return response()->json($orderData)->header("Access-Control-Allow-Origin", "*");
    }

    public function buildOrderConfirmationEdm($orderData)
    {
        $getOrderData = Orders::where('id', $orderData->orderid)->first();

        $getUserData = User::where('id', $getOrderData->UserId)->first();
        $emailData = [];
        $emailData['title'] = 'receipt-order-confirmed';

        $emailData['moduleData'] = array(
            'email' => $getUserData->email,
            'orderId' => $orderData->orderid,
            'receiptId' => $orderData->receiptid,
            'userId' => $getUserData->id,
            'countryId' => $getUserData->CountryId,
            'isPlan' => 0,
        );

        $currentCountry = json_encode(Countries::where('id', $getUserData->CountryId)->first());
        $currentLocale = strtolower($getUserData->defaultLanguage);
        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
        // hide email redis
        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
    }

}
