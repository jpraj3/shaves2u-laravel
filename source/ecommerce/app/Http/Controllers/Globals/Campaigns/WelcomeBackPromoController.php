<?php

namespace App\Http\Controllers\Globals\Campaigns;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\SubscriptionHelper;
use App\Models\Products\ProductType;
use App\Models\GeoLocation\LanguageDetails;
use App\Models\Campaigns\CampaignReactiveHistories;
use Carbon\Carbon;

class WelcomeBackPromoController extends Controller
{

   public function __construct()
   {
      $this->planHelper = new PlanHelper();
      $this->productHelper = new ProductHelper();
      $this->subscriptionHelper = new SubscriptionHelper();
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
   public function index()
   {
   }

   public function annualPlanPromoPage(Request $request)
   {
      $email = $request->query('email');
      $subid = $request->query('TJKTzDfkOrOQbStadzOb');

      $utm_source = $request->query('utm_source');
      $utm_medium = $request->query('utm_medium');
      $utm_campaign = $request->query('utm_campaign');
      $utm_content = $request->query('utm_content');
      $utm_term = $request->query('utm_term');

      $subscriptions = $this->subscriptionHelper->campaignGetSubscription($email, $subid);
      if (isset($subscriptions)) {

         $subscriptionsAddOn = $this->subscriptionHelper->campaignGetSubscriptionAddon($email, $subid);
         $country_id = $subscriptions->CountryId;
         $langCode = $subscriptions->udefaultLanguage;
         $currency = $subscriptions->currencyDisplay;
         $codeIso=  strtolower($subscriptions->codeIso);
         $languageDetailsData = LanguageDetails::select('mainlanguageCode')
         ->where('CountryId', $country_id)
         ->where('sublanguageCode', strtoupper($langCode))
         ->first();
         $urllang=  strtolower($languageDetailsData->mainlanguageCode);
         \App::setLocale(strtolower($langCode));
         $plangroup = config('global.ecommerce.Campaign.PlanGroup');

         $productgroup = config('global.ecommerce.Campaign.ProductGroup');
         $product_type_id = ProductType::where('name', $productgroup)->select('id')->get()->toArray();
         $data = array();
         $data["ProductTypeId"] = $product_type_id;

         $bladegroup = config('global.ecommerce.Campaign.BladeGroup');
         $bladeno = config('global.ecommerce.Campaign.Blade');
         $monthno = config('global.ecommerce.Campaign.Duration');
         $firstmonthno = config('global.ecommerce.Campaign.FirstDuration');
         $blade_type_id = ProductType::where('name', $bladegroup)->select('id')->get()->toArray();
         $data1 = array();
         $data1["ProductTypeId"] = $blade_type_id;

         $products = $this->productHelper->getProductByProductType($country_id, $langCode, $data);
         $blades = $this->productHelper->getProductByProductType($country_id, $langCode, $data1);
         $allplan = $this->planHelper->getPlanIdPriceByCategoryCampaign($country_id, $langCode, $plangroup);

         $datac = (Object) array();
         foreach ($bladeno as $blade) {
            $combineblade = $blade;
            $datac->$combineblade = (Object) array();
         }
         foreach ($allplan as $plans) {
            if (isset($plans["plansku"])) {
               $splitsku = explode("-", $plans["plansku"]);
               if (count($splitsku) == 4) {
                  $sMonth = $splitsku[2];
                  $sBlade = $splitsku[3];
                  foreach ($bladeno as $blade) {
                     if ($blade == $sBlade) {
                        $monthInt =  (int) str_replace("M","",$sMonth);
                        $catridgeqty = 12/$monthInt;
                        $productCid = ""; 
                        $count=0;
  
                        usort($plans["productCountryId"], function($a, $b) {
                           return $a <=> $b;
                       });
       
                        foreach ($plans["productCountryId"] as $pc) {
                           if($count == 0){
                              $productCid=$pc;
                           }else{
                              $productCid=","+$pc;
                           }
                          
                           $count++;
                        }
                        $monthM = "M".$monthInt;
                           $datac->$sBlade->$monthM=(Object) array(
                              "planSku" =>  $plans["plansku"],
                              "catridgeQTY" =>  $catridgeqty,
                              "cashback" => $plans["basePrice"] - $plans["sellPrice"],
                              "month" => $monthInt,
                              "blade" => $sBlade,
                              "orgPrice" => $plans["basePrice"],
                              "sellPrice" => $plans["sellPrice"],
                              "planId" => $plans["planid"],
                              "productCountryId" => $productCid,
                           );
                      
                     }
                  }
               }
            }
         }

        $CheckCampaignReactiveHistories = CampaignReactiveHistories::where('UserId', $subscriptions->userid)
        ->where('email', $email)
        ->where('subscriptionId', $subid)
        ->first();

        $UnrealizedDate = $this->subscriptionHelper->campaignGetSubscriptionUnrealizedDate($subid);
        $getUnrealizedDate = "";
        if(isset($UnrealizedDate)){
           $getUnrealizedDate = $UnrealizedDate->created_at;
        }


        $productAddons = $this->subscriptionHelper->campaignGetSubscriptionAddonCombine($subid);

        $productaddoncombine = "";
        if(isset($productAddons)){
           $productaddoncombine = $productAddons->addon;
        }
        $CampaignReactiveHistoriesid = 0 ;
 
        if(isset($CheckCampaignReactiveHistories)){
         CampaignReactiveHistories::where('id', $CheckCampaignReactiveHistories->id)->update(
            ['UserId' => $subscriptions->userid,'email' => $email,'subscriptionId' => $subid,'CountryId' => $country_id,
            'LanguageCode' => $langCode,'status_Old' => $subscriptions->status,'PlanId_Old' => $subscriptions->PlanId,'AddOn_Old' => $productaddoncombine,
            'pricePerCharge_Old' => $subscriptions->pricePerCharge,'unrealized_Date' =>  $getUnrealizedDate,'nextDeliverDate_old' => $subscriptions->nextDeliverDate,'currentCycle_old' => $subscriptions->currentCycle,
            'nextChargeDate_old' => $subscriptions->nextChargeDate,'recharge_date_old' => $subscriptions->recharge_date,'total_recharge_old' => $subscriptions->total_recharge,'campaign_type' => "Welcome Back Promo",
            'created_at' => Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s'),'updated_at' => Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s')
            ]);
            $CampaignReactiveHistoriesid = $CheckCampaignReactiveHistories->id;
        }
        else{ 
        
         $CampaignReactiveHistories = new CampaignReactiveHistories();
         $CampaignReactiveHistories->UserId = $subscriptions->userid;
         $CampaignReactiveHistories->email = $email;
         $CampaignReactiveHistories->subscriptionId = $subid;
         $CampaignReactiveHistories->CountryId = $country_id;
         $CampaignReactiveHistories->LanguageCode = $langCode;
         $CampaignReactiveHistories->status_Old = $subscriptions->status;
         $CampaignReactiveHistories->PlanId_Old = $subscriptions->PlanId;
         $CampaignReactiveHistories->AddOn_Old = $productaddoncombine;
         $CampaignReactiveHistories->pricePerCharge_Old = $subscriptions->pricePerCharge;
         $CampaignReactiveHistories->unrealized_Date = $getUnrealizedDate;
         $CampaignReactiveHistories->nextDeliverDate_old = $subscriptions->nextDeliverDate;
         $CampaignReactiveHistories->currentCycle_old = $subscriptions->currentCycle;
         $CampaignReactiveHistories->nextChargeDate_old = $subscriptions->nextChargeDate;
         $CampaignReactiveHistories->recharge_date_old = $subscriptions->recharge_date;
         $CampaignReactiveHistories->total_recharge_old = $subscriptions->total_recharge;
         $CampaignReactiveHistories->campaign_type = "Welcome Back Promo";
         $CampaignReactiveHistories->created_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
         $CampaignReactiveHistories->updated_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
         $CampaignReactiveHistories->save();

         $CampaignReactiveHistoriesid = $CampaignReactiveHistories->id;
        }

         return view('_campaign.welcomebackpromo', compact('utm_source','utm_medium','utm_campaign','utm_content','utm_term','email','subid','country_id','langCode', 'subscriptions','currency','subscriptionsAddOn', 'products', 'blades', 'allplan', 'datac', 'bladeno', 'monthno', 'firstmonthno','codeIso','urllang','CampaignReactiveHistoriesid'));
      } else {
         return redirect('/');
      }
   }

   public function annualPlanPromoUpdateSubscription(Request $request)
   {
      $country_id = $request->countrypost;
      $langCode = $request->langpost;
      $email = $request->urlparam["email"];
      $subid = $request->urlparam["TJKTzDfkOrOQbStadzOb"];
      $planid = $request->planidpost;
      $campaignid = $request->campaignidpost;
      $productaddon = $request->productaddonpost;

      $utm_source = $request->utm_source;
      $utm_medium = $request->utm_medium;
      $utm_campaign = $request->utm_campaign;
      $utm_content = $request->utm_content;
      $utm_term = $request->utm_term;

      $productgroup = config('global.ecommerce.Campaign.ProductGroup');
      $product_type_id = ProductType::where('name', $productgroup)->select('id')->get()->toArray();
      $data = array();
      $data["ProductTypeId"] = $product_type_id;


      $plangroup = config('global.ecommerce.Campaign.PlanGroup');
      $products = $this->productHelper->getProductByProductType($country_id, $langCode, $data);
      $allplan = $this->planHelper->getPlanIdPriceByCategoryCampaign($country_id, $langCode, $plangroup);
      $chackplanid = 0;
      $checkproductaddonnumber = 0;
      $checkproductaddon = 0;
      $planprice = 0;
      $addonprice = 0;
      $totalprice = 0;

      foreach ($allplan as $plans) {
         if($plans["planid"] == $planid){
            $chackplanid = 1;
            $planprice = (float) $plans["sellPrice"];
         }
      }
 
      $splitproductaddon = explode(",", $productaddon);
      if($productaddon == "" || $productaddon == null){
      $totalproductaddon = 0;
      $checkproductaddon = 1;
      }else{
      $totalproductaddon =  count($splitproductaddon);

      foreach ($splitproductaddon as $productaddonidcycle) {
         foreach ($products as $product) {
          $splitproductaddonidcycle = explode("@", $productaddonidcycle);
          if($product["productcountriesid"] == $splitproductaddonidcycle[0]){
             $checkproductaddonnumber = $checkproductaddonnumber + 1;
             $addonprice = (float) $addonprice + (float) $product["sellPrice"] ;
           }
        }
       }
       if($totalproductaddon == $checkproductaddonnumber){
         $checkproductaddon = 1;
       }
      }
      $totalprice =  (float) $planprice + (float) $addonprice;
      $subscriptions = $this->subscriptionHelper->campaignCheckSubscription($email, $subid, $country_id);
      if (isset($subscriptions) && $chackplanid == 1 && $checkproductaddon == 1 && $totalprice > 0) {
         // $updatesubscriptions = "success";
          $updatesubscriptions = $this->subscriptionHelper->campaignUpdateSubscription($email,$subid,$country_id,$planid,$planprice,$totalprice,$productaddon,$subscriptions->currentCycle);
    
          if($updatesubscriptions =="success"){
            $nextdate=  Carbon::now()->addDays(1);
            CampaignReactiveHistories::where('id', $campaignid)->update(
               ['status_New' => 'Processing','PlanId_New' => $planid,'AddOn_New' => $productaddon,
               'pricePerCharge_New' => $totalprice,'discountPercent'=>"20",'updated_at' => Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s'),
               'isChargeSuccess'=>0 ,'nextDeliverDate_new' => $nextdate,'nextChargeDate_new' => $nextdate,'source' => $utm_source,'medium' => $utm_medium,'campaign' => $utm_campaign,'content' => $utm_content,'term' => $utm_term
               ]);
          }
          return $updatesubscriptions;
      }

   }

   public function annualPlanPromoThankyouPage()
   {
      return view('_campaign.thankyou');
   }
}
