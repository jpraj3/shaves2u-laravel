<?php

namespace App\Http\Controllers\Globals\Warehouse;
use App\Services\CountryService;
use App\Helpers\Warehouse\UrbanFoxHelper;
use App\Http\Controllers\Controller as Controller;
use App;


class UrbanFoxController extends Controller
{
    public function __construct()
    {
        $this->urbanFoxHelper = new UrbanFoxHelper();
        $this->locale = App::getLocale();
        $this->countryService = new CountryService();
        $this->country = json_decode(session()->get('currentCountry'), true);
        $this->country_id = $this->country['id'];
        $this->country_iso = strtoupper($this->country['codeIso']);

        // check for supportedlangs
        $this->_supportedLangs = $this->countryService->supportedLangs($this->country_id);
        $this->langCode = $this->_supportedLangs ? $this->locale : $this->country['defaultLang'];
    }

    public function index()
    {
        $this->user = json_decode(session()->get('isLoggedIn'), true);
        $options = [
            'OrderId' => 1,
            'User' => $this->user,
            'CountryId' => $this->country_id,
            'LangCode' => strtoupper($this->langCode),
            'isBA' => false
         ];
        //  $response = $this->urbanFoxHelper->buildData();
        $response = $this->urbanFoxHelper->createTransactions($options, false);
        return view('test.urbanfox', compact('response', $response));
    }
}
