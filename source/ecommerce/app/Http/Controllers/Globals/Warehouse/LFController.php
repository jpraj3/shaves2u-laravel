<?php

namespace App\Http\Controllers\Globals\Warehouse;

use App;
use App\Helpers\Warehouse\FtpHelperMY;
use App\Http\Controllers\Controller as Controller;
use App\Services\CountryService;
use App\Webhooks\AftershipWebhooks;
use Illuminate\Http\Request;

class LFController extends Controller
{
    public function __construct()
    {
        // $this->ftpHelperMY = new FTPHelperMY();
        // $this->warehouseHelperMY = new WarehouseHelperMY();
        $this->aftershipWebhooks = new AftershipWebhooks();

        // $this->locale = App::getLocale();
        // $this->countryService = new CountryService();
        // $this->country = json_decode(session()->get('currentCountry'), true);
        // $this->country_id = $this->country['id'];
        // $this->country_iso = strtoupper($this->country['codeIso']);

        // // check for supportedlangs
        // $this->_supportedLangs = $this->countryService->supportedLangs($this->country_id);
        // $this->langCode = $this->_supportedLangs ? $this->locale : $this->country['defaultLang'];
    }

    public function index()
    {
        // return $this->aftershipWebhooks->postUpdateOrder(1);
        // $response = $this->ftpHelperMY;
        // $response = $this->warehouseHelperMY;
        // $response = $this->aftershipWebhooks;
        // return view('test.lf', compact('response', $response));
    }

    public function updateOrderTrackings(Request $request)
    {
        $this->Log = \Log::channel('cronjob');
        $this->Log->info('LFController | updateOrderTrackings | start LF Tracking Update using Aftership Webhooks');
        $this->aftershipWebhooks->handle($request);
    }
}
