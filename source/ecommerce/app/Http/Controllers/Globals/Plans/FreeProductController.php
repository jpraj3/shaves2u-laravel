<?php

namespace App\Http\Controllers\Globals\Plans;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\Payment\Stripe;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\TaxInvoiceHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Ecommerce\Rebates\Referral\ReferralController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanSKU;
use App\Models\Rebates\Referral;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Rebates\RewardsIn;
use App\Models\Receipts\Receipts;
use App\Models\Reports\SalesReport;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Queue\SendEmailQueueService as EmailQueueService;
use App\Services\CountryService as CountryService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use App\Helpers\SubscriptionHelper;

class FreeProductController extends Controller
{
    // Constructor
    public function __construct()
    {
        $this->userHelper = new UserHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->planHelper = new PlanHelper();
        $this->stripe = new Stripe();
        $this->country_service = new CountryService();
        $this->productHelper = new ProductHelper();
        $this->taxInvoiceHelper = new TaxInvoiceHelper();
        $this->orderHelper = new OrderHelper();
        $this->referralController = new ReferralController();
        $this->subscriptionHelper = new SubscriptionHelper();
    }

    // Function: Main Process
    public function MainProcess($subscriptionId)
    {
        return $this->GetSubscriptions($subscriptionId);
    }

    // Function: Get subscriptions
    public function GetSubscriptions($subscription_id)
    {
        try {

            // Get subscription based on several filters
            $subscriptions = Subscriptions::leftJoin('promotions', 'promotions.id', 'subscriptions.promotionId')
            ->select('subscriptions.*', 'promotions.*', 'subscriptions.id as subscriptionsid', 'promotions.id as promotionsid')
            ->where('subscriptions.id',$subscription_id)
            ->where('subscriptions.nextChargeDate','!=',null)
            ->where('subscriptions.nextDeliverDate','!=',null)
            ->where('CardId','!=',null)
            ->get();

            // Build all required information
            $subs = $subscriptions[0];
            $subs->user = (object) array(User::where('id', $subs->UserId)->first())["0"];
            $subs->user->fullname = $subs->user->firstName;
            $subs->card = (object) array(Cards::where('id', $subs->CardId)->first())["0"];
            $subs->deliveryAddress = (object) array(DeliveryAddresses::where('id', $subs->DeliveryAddressId)->first())["0"];
            $subs->billingAddress = (object) array(DeliveryAddresses::where('id', $subs->BillingAddressId)->first())["0"];
            $subs->plan = $this->planHelper->getOnePlanData($subs->PlanId);
            $subs->plan_sku = (object) array(PlanSKU::where('id', $subs->plan->PlanSkuId)->first())["0"];
            $subs->country = (object) array(Countries::where('id', $subs->plan->CountryId)->first())["0"];
            $subs->post_payment = (object) array();
            $subs->isOnline = $subs->isOffline === 1 ? 0 : 1;
            $subs->appType = $subs->isOffline === 1 ? 'baWebsite' : 'ecommerce';
            $subs->tax = (object) array($this->laravelHelper->ConvertArraytoObject($this->country_service->tax($subs->country->id)))["0"];

            // Continue the subscription charge job for these countries
            if(in_array($subs->country->id,[1,2,7,9]))
            {
                return $this->CreateOrder($subs);
            }
            else
            {
                return "country_not_allowed";
            }

        } catch (Exception $e) {
            return $this->onSendFreeProductError('Get subscription error: '. $e->getMessage());
        }
    }

    // Function: Create order
    public function CreateOrder($subs)
    {
        try {

            $data = null;
            // $utm_parameters = $this->session->_session("utm_parameters", "get", null);
            // if($utm_parameters && !is_array($utm_parameters) && !is_string($utm_parameters)){
            //     // if ($this->checkJsondecode($utm_parameters)) {
            //     $utm_parameters = json_decode($utm_parameters);
            //     $data = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
            //     // }
            // }
            
            // Create order
            $orders = new Orders();
            $orders->promoCode = 'REPLACEMENT';
            $orders->PromotionId = null;
            $orders->SSN = null;
            $orders->email = $subs->user->email;
            $orders->phone = $subs->user->phone;
            $orders->fullName = $subs->user->fullname;
            $orders->DeliveryAddressId = $subs->deliveryAddress->id;
            $orders->BillingAddressId = $subs->billingAddress->id;
            $orders->taxRate = $subs->tax->taxRate;
            $orders->taxName = $subs->tax->taxName;
            $orders->CountryId = $subs->country->id;
            $orders->subscriptionIds = $subs->subscriptionsid;
            $orders->source = $data ? (isset($data->utm_source) ? $data->utm_source : null) : null;
            $orders->medium = $data ? (isset($data->utm_medium) ? $data->utm_medium : null) : null;
            $orders->campaign = $data ? (isset($data->utm_campaign) ? $data->utm_campaign : null) : null;
            $orders->term = $data ? (isset($data->utm_term) ? $data->utm_term : null) : null;
            $orders->content = $data ? (isset($data->utm_content) ? $data->utm_content : null) : null;
            $orders->status = "Payment Received";
            $orders->payment_status = "Charged";
            $orders->paymentType = "cash";
            if ($subs->country->id == "7") {
                $orders->carrierAgent = "courex";
            } else {
                $orders->carrierAgent = null;
            }
            $orders->UserId = $subs->user->id;
            $orders->SellerUserId = null;
            $orders->pymt_intent = null;
            $orders->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->order = $orders;
       
            // Add order to queue service
            $_redis_order_data = (object) array(
                'OrderId' => $subs->post_payment->order->id,
                'CountryId' => $subs->country->id,
                'CountryCode' => strtolower($subs->country->codeIso),
            );
            //$this->orderHelper->addTaskToOrderQueueService($_redis_order_data);
            // session()->forget("utm_parameters");
            // Update current order time and id in subscription
            Subscriptions::where('id', $subs->subscriptionsid)
                ->update([
                    'currentOrderTime' => Carbon::now(),
                    'currentOrderId' => $subs->post_payment->order->id,
                ]);

            return $this->CreateOrderHistory($subs);
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create order error: '. $e->getMessage());
        }
    }

    // public function checkJsondecode($data){
    //     if (!empty($data)) {
    //         json_decode($data);
    //         return (json_last_error() === JSON_ERROR_NONE);
    // }
    // return false;  
    // }

    // Function: Create order history
    public function CreateOrderHistory($subs)
    {
        try {
            // Create order history
            $ordershistories = new OrderHistories();
            $ordershistories->message = 'Payment Received';
            $ordershistories->OrderId = $subs->post_payment->order->id;
            $ordershistories->save();

            return $this->CreateOrderDetails($subs);
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create order history error: '. $e->getMessage());
        }
    }

    // Function: Create order details
    public function CreateOrderDetails($subs)
    {
        try {

            // Only insert blade from current plan
            $currentPlanData = $this->subscriptionHelper->GetCurrentPlan($subs->subscriptionsid);

            $bladeProductCountryId = $currentPlanData->available_plans->blade_type["ProductCountryId"];
            $bladeSellPrice = $currentPlanData->available_plans->blade_type["sellPrice"];

            $orderdetails = new OrderDetails();
            $orderdetails->qty = 1;
            $orderdetails->price = $bladeSellPrice;
            $orderdetails->currency = $subs->country->currencyDisplay;
            $orderdetails->startDeliverDate = null;
            $orderdetails->created_at = Carbon::now();
            $orderdetails->updated_at = Carbon::now();
            $orderdetails->OrderId = $subs->post_payment->order->id;
            $orderdetails->ProductCountryId = $bladeProductCountryId;
            $orderdetails->isAddon = 0;
            $orderdetails->isFreeProduct = 1;
            $orderdetails->save();

            return $this->CreateReceipt($subs);
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create order details error: '. $e->getMessage());
        }
    }

    // Function: Create receipt
    public function CreateReceipt($subs)
    {
        try {
            // Create receipt
            $receipts = new Receipts();
            $receipts->totalPrice = 0.00;
            $receipts->subTotalPrice = 0.00;
            $receipts->discountAmount = 0.00;
            $receipts->currency = $subs->country->currencyDisplay;
            $receipts->originPrice = 0.00;
            $receipts->taxAmount = 0.00;
            $receipts->shippingFee = 0.00;
            $receipts->OrderId = $subs->post_payment->order->id;
            $receipts->chargeFee = 0.00;
            $receipts->SubscriptionId = $subs->subscriptionsid;
            $receipts->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->receipt = $receipts;

            // $receiptdata =$subs->post_payment->receipt;
            return $this->SendReceiptEDM($subs);
            // \Log::debug('Subscription Charge Job Ran!');
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create receipt error: '. $e->getMessage());
        }
    }

    // Function: Send receipt edm
    public function SendReceiptEDM($subs)
    {
        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'receipt-order';
            $emailData['moduleData'] = (Object) array(
                'email' => $subs->email,
                'subscriptionId' => $subs->subscriptionsid,
                'orderId' => $subs->post_payment->order->id,
                'receiptId' => $subs->post_payment->receipt->id,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);

            //EmailQueueService::addHash($subs->user->id,'receipt_order_confirmation',$emailData);

            return $this->CreateTaxInvoice($subs);
        } catch (Exception $e) {
            $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Send receipt edm error: '. $e->getMessage());
        }
    }

    // Function: Create tax invoice
    public function CreateTaxInvoice($subs)
    {
        try {
            //$this->taxInvoiceHelper->__generateHTML($subs->post_payment->order,$subs->post_payment->receipt,$subs->country);
            return $this->CreateSalesReport($subs);
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create tax invoice error: '. $e->getMessage());
        }
    }

    // Function: Create sales report
    public function CreateSalesReport($subs)
    {
        try {
            // Create sales report (Disabled in old site)
            return 'success';
        } catch (Exception $e) {
            return $this->onSendFreeProductError('SubscriptionId: '.$subs->subscriptionsid.'. Create sales report error: '. $e->getMessage());
        }
    }

    /*** Additional helper functions ***/
    public function GetCurrentDateTime()
    {
        return Carbon::now();
    }

    public function GetCurrentDate()
    {
        return Carbon::now()->toDateString();
    }

    public function onSendFreeProductError($message)
    {
        $returnResponse = array();
        $returnResponse["status"] = "failure";
        $returnResponse["message"] = $message;
        return $returnResponse;
    }
    /*** Additional helper functions ***/

}
