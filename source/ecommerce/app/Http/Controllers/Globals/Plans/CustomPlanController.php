<?php

namespace App\Http\Controllers\Globals\Plans;

use App;
use App\Helpers\LaravelHelper;
use App\Helpers\Payment\NicePayHelper;
use App\Helpers\Payment\Stripe;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Http\Controllers\Globals\Payment\StripeController;
use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Plans\PlanCategory;
use App\Models\Plans\PlanCategoryDetails;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanImages;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Services\CountryService;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Lang;
use Exception;

class CustomPlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $producthelper;
    public $planhelper;
    public $country_service;
    public $nicepay;
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
        $this->userHelper = new UserHelper();
        $this->producthelper = new ProductHelper();
        $this->planhelper = new PlanHelper();
        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        if ($this->country_info["country_id"] !== 8) {
            $this->stripe = new Stripe();
            $this->stripeController = new StripeController();
        }
        $this->session = new SessionController();

        $this->nicepayController = new NicePayController();
        $this->nicepay = new NicePayHelper();
        $this->currency = $this->country_service->getCurrencyDisplay();
        $this->agent = new Agent();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {}

    public function proceedCheckout(Request $request)
    {
        $session_data = $this->session->_session("selection_custom_plan", "get", null);
        $session_data_decoded = json_decode($session_data);
        $langCode = strtolower(app()->getLocale());
        $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $loginstatus = "";
        // build tax data
        $tax = $this->laravelHelper->ConvertArraytoObject($this->country_service->tax($currentCountryid));

        $view = "checkout.subscriptions.checkout-custom-plan-desktop";
        if ($this->agent->isMobile()) {
            $view = "checkout.subscriptions.checkout-custom-plan-mobile";
        }
          // mask selection
          $maskselection = null;
          $countmaskqty = 0;
          if($currentCountryid == 1 || $currentCountryid == 7){
          $maskselection = $this->producthelper->getFreeMaskSelection($currentCountryid, $langCode);
          if($maskselection){
           foreach ($maskselection as $m) {
               $countmaskqty = $m->qty +  $countmaskqty;
           }
          }
          }
          // mask selection
        if ($session_data_decoded) {
            if (array_key_exists('planId', $session_data_decoded->selection->step5) && array_key_exists('current_price', $session_data_decoded->selection->step5) && array_key_exists('shipping_fee', $session_data_decoded->selection->step5) && array_key_exists('next_price', $session_data_decoded->selection->step5)) {

                $isAnnual = $session_data_decoded->selection->step5->isAnnual;
                $Annualqty = $session_data_decoded->selection->step5->Annualqty;
                if($isAnnual == "1"){
                $current_price = $session_data_decoded->selection->step5->original_price;
                $next_price = $session_data_decoded->selection->step5->noriginal_price;
                }else{
                $current_price = $session_data_decoded->selection->step5->current_price;
                $next_price = $session_data_decoded->selection->step5->next_price;
                }
                $shipping_fee = $session_data_decoded->selection->step5->shipping_fee;
                $discount_per = $session_data_decoded->selection->step5->discountpecentage;
                $discount_amount = $session_data_decoded->selection->step5->discountamount;
                $ndiscount_amount = $session_data_decoded->selection->step5->ndiscountamount;
                $current_totalprice = $current_price + $shipping_fee;
                $taxAmount = $tax->taxRate / 100 * $current_totalprice;
                $current_totalprice = $current_totalprice + $taxAmount;

                $next_totalprice = $next_price + $shipping_fee;
                $ntaxAmount = $tax->taxRate / 100 * $next_totalprice;
                $next_totalprice = $next_totalprice + $taxAmount;
                
                $current_tprice = $session_data_decoded->selection->step5->current_price;
                $next_tprice = $session_data_decoded->selection->step5->next_price;
                
                $User = null;
                $states = $this->country_service->getAllStates($currentCountryid);

                if (Auth::check()) {

                    $User = $this->userHelper->getUserDetails(Auth::user()->id);
                    if ($currentCountryid != 8) {
                        $payment_intent = $this->stripeController->initializePaymentIntents($User, "selection_custom_plan");
                        $generate_payment_intent = $payment_intent["generate_payment_intent"];
                        $checkout_details = $payment_intent["plandetails"];
                    } else {
                        $payment_intent = null;
                        $generate_payment_intent = null;
                        $checkout_details = null;
                    }
                    $loginstatus = "post-login";
                } else {
                    if ($currentCountryid != 8) {
                        $payment_intent = $this->stripeController->initializePaymentIntents(null, "selection_custom_plan");
                        $generate_payment_intent = $payment_intent["generate_payment_intent"];
                        $checkout_details = $payment_intent["plandetails"];
                    } else {
                        $payment_intent = null;
                        $generate_payment_intent = null;
                        $checkout_details = null;
                    }
                    $loginstatus = "pre-login";
                }
                $bladeaddon = '';
                $addon = '';
                $naddon = '';
                $frequencyaddon = '';
                if (array_key_exists('selected_blade', $session_data_decoded->selection->step2)) {
                    if (array_key_exists('producttranslatesname', $session_data_decoded->selection->step2->selected_blade)) {
                        $blade = $session_data_decoded->selection->step2->selected_blade->producttranslatesname;
                        if($isAnnual == "1"){
                            $blade = $Annualqty. " x " .$blade ;
                        }
                    }
                }
                if (array_key_exists('selected_frequency', $session_data_decoded->selection->step3)) {
                    $frequency = $session_data_decoded->selection->step3->selected_frequency;
                }
                if (array_key_exists('selected_addon_list', $session_data_decoded->selection->step4)) {
                    $countAddOn = 0;
                    foreach ($session_data_decoded->selection->step4->selected_addon_list as $p) {
                        if($isAnnual == "1"){
                            $productnameqty = $p->producttranslatesname;
                            if(isset($p->cycle)){
                                if($p->cycle == 1){
                                    $productnameqty = "1 x " . $productnameqty;
                                }
                                else{
                                    $productnameqty =   $Annualqty. " x " . $productnameqty;
                                }
                            }else{
                                $productnameqty =   $Annualqty. " x " . $productnameqty;
                            }
                            if ($countAddOn == 0) {
                                $addon = $productnameqty;
                            } else {
                                $addon = $addon . "," . $productnameqty;
                            }

                        }else{
                        if ($countAddOn == 0) {
                            $addon = $p->producttranslatesname;
                        } else {
                            $addon = $addon . "," . $p->producttranslatesname;
                        }
                       }
                        $countAddOn++;
                    }

                    $ncountAddOn = 0;
                    foreach ($session_data_decoded->selection->step4->selected_addon_list as $p) {
                        $productnameqty = $p->producttranslatesname;
                        if($isAnnual == "1"){
                            $productnameqty =  $Annualqty. " x " . $p->producttranslatesname;
                        }
                        if(isset($p->cycle)){
                         if($p->cycle == 1){
                         }
                         else{
                            if ($ncountAddOn == 0) {
                                $naddon = $productnameqty;
                            } else {
                                $naddon = $naddon . "," . $productnameqty;
                            }
                            $ncountAddOn++;
                         }
                        }else{
                        if ($ncountAddOn == 0) {
                            $naddon = $productnameqty;
                        } else {
                            $naddon = $naddon . "," . $productnameqty;
                        }
                        $ncountAddOn++;
                      }
                    }
                }
                if ($addon) {
                    $plandescription =  $blade . "," . $addon;
                    $nplandescription =  $blade . "," . $naddon;
                } else {
                    $plandescription =  $blade;
                    $nplandescription = $plandescription;
                }
                return view($view)
                ->with('maskselection',$maskselection)
                ->with('countmaskqty',$countmaskqty)
                    ->with('loginStatus', $loginstatus)
                    ->with('currency', $this->currency)
                    ->with('currentCountryIso', $currentCountryIso)
                    ->with('currentCountryid', $currentCountryid)
                    ->with('langCode', $langCode)
                    ->with('urllangCode', $urllangCode)
                    ->with('states', $states)
                    ->with('User', $User)
                    ->with('session_data', $session_data_decoded)
                    ->with('plandescription', $plandescription)
                    ->with('nplandescription', $nplandescription)
                    ->with('payment_intent', $generate_payment_intent)
                    ->with('checkout_details', $checkout_details)
                    ->with('current_price', $current_price)
                    ->with('shipping_fee', $shipping_fee)
                    ->with('next_price', $next_price)
                    ->with('current_totalprice', $current_totalprice)
                    ->with('next_totalprice', $next_totalprice)
                    ->with('taxAmount', $taxAmount)
                    ->with('ntaxAmount', $ntaxAmount)
                    ->with('taxRate', $tax->taxRate)
                    ->with('planFrequency', $frequency)
                    ->with('current_tprice',$current_tprice)
                    ->with('next_tprice',$next_tprice)
                    ->with('isAnnual', $isAnnual)
                    ->with('Annualqty', $Annualqty)
                    ->with('discount_per', $discount_per)
                    ->with('discount_amount', $discount_amount)
                    ->with('ndiscount_amount', $ndiscount_amount);

            } else {
                $langCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
                return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            }
        } else {
            $langCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
            return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        }
    }

    public function autoCreatePlan(Request $req)
    {
        // Audit - auto Create Plan Function

        if ($req->plantype == "custom") {
            $planskuget = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanSku');

            $planskuname = $planskuget . $req->bladetype;

            $planskuid = '';
            $planid = '';
            $plancategoryidget = PlanCategory::where('name', $req->plangroup)->select("id")->first();
            $planskucheck = PlanSKU::where('sku', $planskuname)->where('duration', $req->selected_frequency)->select("id")->first();
            $plancategoryid = $plancategoryidget->id;
            $plandescription = $req->plangroup . " " . $req->selected_frequency . "-" . $planskuname;
            if ($planskucheck) {
                $planskuid = $planskucheck->id;
            } else {
                $plansku = new PlanSKU();
                $plansku->sku = $planskuname;
                $plansku->rating = 0;
                $plansku->isFeatured = 1;
                $plansku->status = "1";
                $plansku->slug = $planskuname;
                $plansku->order = 1;
                $plansku->duration = $req->selected_frequency;
                $plansku->save();
                $planskuid = $plansku->id;
            }
            if ($planskuid && $plancategoryid) {
                if ($planskucheck) {} else {
                    $plancategorydetail = new PlanCategoryDetails();
                    $plancategorydetail->PlanSkuId = $planskuid;
                    $plancategorydetail->PlanCategoryId = $plancategoryid;
                    $plancategorydetail->save();
                }
                $plancheck = Plans::where('plantype', $req->plantype)->where('PlanSkuId', $planskuid)->where('CountryId', $req->country_id)->select("id")->first();
                if ($plancheck) {
                    $planid = $plancheck->id;
                } else {
                    $plans = new Plans();
                    $plans->price = $req->currentprice;
                    $plans->sellPrice = $req->currentprice;
                    $plans->trialPrice = null;
                    $plans->discountPercent = 0;
                    $plans->discountAmount = 0.00;
                    $plans->description = $plandescription;
                    $plans->tax = 0;
                    $plans->taxAmount = 0.00;
                    $plans->plantype = $req->plantype;
                    $plans->isOnline = 1;
                    $plans->isOffline = 1;
                    $plans->rating = 0;
                    $plans->order = 1;
                    $plans->PlanSkuId = $planskuid;
                    $plans->CountryId = $req->country_id;
                    $plans->save();
                    $planid = $plans->id;
                    if ($planid) {
                        $planimage = new PlanImages;
                        $planimage->url = $req->imagesurl;
                        $planimage->isDefault = 1;
                        $planimage->PlanId = $planid;

                        foreach ($req->ProductCountriesId as $p) {
                            $plandetail = new PlanDetails;
                            $plandetail->PlanId = $planid;
                            $plandetail->ProductCountryId = $p;
                            $plandetail->qty = 1;
                            $plandetail->cycle = "all";
                            $plandetail->isAnnual = 0;
                            $plandetail->duration = null;
                            $plandetail->totalCycle = null;
                            $plandetail->save();
                        }
                    }
                }
            }
        }

        return $planid;
    }

    public function nicepayAddCard(Request $request)
    {
        $datalist = [
            "card_name" => $request->card_name,
            "card_number" => $request->card_number,
            "card_password" => $request->card_password,
            "card_expiry_month" => $request->card_expiry_month,
            "card_expiry_year" => $request->card_expiry_year,
            "card_birth" => $request->card_birth,

        ];
        $result = $this->nicepay->createBillingKey($datalist);
        return $result;
    }

    // Function: Get list of frequency
    public function getFrequencyList()
    {
        $durations = config('global.' . config("app.appType") . '.CustomPlan.CustomPlanDuration');
        $frequency_list = [];
        foreach ($durations as $duration) {
            $data = array();
            $data["duration"] = $duration;
            $data["image"] = config('global.' . config("app.appType") . '.checkout.custom.frequency.' . $duration . '.image');
            $data["durationText"] = Lang::get('website_contents.frequency.' . $duration . '.durationText');
            $data["detailsText"] = Lang::get('website_contents.frequency.' . $duration . '.detailsText');
            $data["selectedDetailsText"] = Lang::get('website_contents.frequency.' . $duration . '.selectedDetailsText');
            array_push($frequency_list, $data);
        }
        return $frequency_list;
    }

    public function proceedSummary()
    {

        if (Auth::check()) {
            //Get selected planid from session storage
            $session_data = $this->session->_session("checkout_custom_plan", "get", null);
            $session_data_decoded = json_decode($session_data);

            // $payment_intent_id = $session_data_decoded->payment_intents->id;
            // $stripe_client_secret = $session_data_decoded->payment_intents->client_secret;

            // $stripe_secret_key = $this->stripe->getStripeSecretKey();

            //$response = $this->stripe->confirmPaymentIntents($payment_intent_id);

            return view('checkout.subscriptions.checkout-custom-plan-summary')
                ->with('Session', $session_data);
            // ->with('stripe_client_secret', $stripe_client_secret)
            // ->with('stripe_secret_key', $stripe_secret_key);
        } else {
            return view('auth.login');
        }
    }

    public function convertcustomtoannual(Request $request)
    {
        $session_data = $this->session->_session("selection_custom_plan", "get", null);
        $plan_type = "";
        $plan_id = "";
        $bladecountryid = "";
        $countryid = "";
        $langid = "";
        $selected_frequency = "";
        $result = "";
        try{
        if(isset($session_data) && $request->isAnnual == "1"){
            $session_data = json_decode($session_data);
           if (array_key_exists("plan_type", $session_data->selection)) {
            $category = "CAnnual Plan" ;
            $plan_type = $session_data->selection->plan_type ="custom-plan" ? "custom" : "" ;
            $plan_id = $session_data->selection->step5->planId;
            $bladecountryid  = $session_data->selection->step2->selected_blade->productcountriesid;
            $selected_frequency = $session_data->selection->step3->selected_frequency;
            $result = $this->planhelper->getConvertedAnnual($category,$plan_type,$bladecountryid,$selected_frequency);
            }
        }
        return $result;
    } catch (Exception $ex) {
        return "";
    }
    
    }
}
