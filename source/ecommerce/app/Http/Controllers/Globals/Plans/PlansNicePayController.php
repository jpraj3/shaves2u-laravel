<?php

namespace App\Http\Controllers\Globals\Plans;

// Helpers
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Queue\SaleReportQueueService;
use App\Queue\TaxInvoiceQueueService;
use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\Payment\NicePayHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelper;

// Controller
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Http\Controllers\Globals\Plans\TrialPlanController as TrialPlan;
use App\Http\Controllers\Globals\Utilities\SessionController;
// Models
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Orders\PaymentHistories;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\Plans;
use App\Models\Promotions\Promotions;
use App\Models\Promotions\PromotionCodes;
use App\Models\Rebates\Referral;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Services\APICountryService as APICountryService;
use App\Services\CountryService as CountryService;
use App\Models\Rebates\RewardsIn;
use Carbon\Carbon;
use Exception;

// Queues
use Illuminate\Http\Request;

class PlansNicePayController extends Controller
{
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->api_country_service = new APICountryService();
        $this->session = new SessionController();
        $this->userHelper = new UserHelper();
        $this->productHelper = new ProductHelper();
        $this->planhelper = new PlanHelper();
        $this->userhelper = new UserHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->country_info = $this->laravelHelper->ConvertArraytoObject($this->country_service->getCountryAndLangCode());
        $this->country_id = $this->country_info->country_id;
        $this->lang_code = strtoupper(app()->getLocale());
        $this->orderHelper = new OrderHelper();
        $this->emailQueueHelper = new EmailQueueHelper();

        //Initialize data
        $this->promotion = "";
        $this->referral = "";
        $this->planData = "";
        $this->priceData = "";
        $this->addonProductCountry = "";
        $this->subscriptionId = "";
        $this->referralDiscount = "";
        $this->orderId = "";
        $this->addonProductCountryList = [];
        $this->productCountryList = [];
        $this->planId = '';
        $this->appType = config('app.appType');
        $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;
        $this->minspendresult = 0;
        $this->planSelected = null;
        $this->nicepayController = new NicePayController();
        $this->nicepay = new NicePayHelper();
                //for custom annual
                $this->getisannual = 0;
                $this->getannualQTY = 0;
                $this->getdiscountamount = 0;
                $this->getdiscountpercentage = 0;
    }

    /***** PART I: BEFORE PAYMENT BEGINS *****/

    // Function: Confirm purchase
    public function confirmPurchase(Request $request)
    {

        // Get request data
        $this->planType = $request->type;

        if (isset($request->appType)) {
            if ($request->appType === 'baWebsite') {
                $urllangCode = strtoupper($request->urllangCode);
                $langCode = strtolower($request->langCode);
                $currentCountryIso = strtolower($request->country_code);
                $this->urllangCode = $urllangCode;
                $this->langCode = $langCode;
                $this->currentCountryIso = $currentCountryIso;
                $this->appType = $request->appType;
                $this->isOnline = 0;
                $this->isOffline = 1;
                $this->baCountryId = (int) $request->country_id;
                $this->country_info = Countries::where('id', $this->baCountryId)->first();
                $this->seller = json_decode($request->seller, true);
                $this->event_data = $request->event_data;
                $this->isDirectTrial = $request->isDirectTrial === "true" ? 1 : 0;
                return $this->initializePurchaseDataAPI($request);
            } else {
                $this->baCountryId = null;
                $this->seller = null;
                return $this->initializePurchaseData();
            }
        } else {
            $this->baCountryId = null;
            $this->seller = null;
            return $this->initializePurchaseData();
        }
    }

    // Function: Initialize purchase data
    public function initializePurchaseDataAPI($request)
    {

        $session_data_selection = $request->session_selection;
        $session_data_checkout = $request->session_checkout;

        $this->session_data_selection_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($session_data_selection);
        $this->session_data_checkout_decoded = \App\Helpers\LaravelHelper::ConvertArraytoObject($session_data_checkout);

        if ($this->session_data_checkout_decoded->checkout->switch_plans->isSwitched === "true") {
            if ($this->session_data_checkout_decoded->checkout->switch_plans->isSelected === "annual") {
                $this->planSelected = $this->session_data_checkout_decoded->checkout->switch_plans->_annual;
            } else {
                $this->planSelected = null;
            }
        }

        // Get user data
        $user = $this->userhelper->getUserDetails((int) $this->session_data_checkout_decoded->checkout->user->id);

        $this->userId = $user["user"]["id"];
        $this->userFullname = $user["user"]["firstName"];
        $this->userEmail = $user["user"]["email"];
        $this->userPhone = $user["user"]["phone"];

        $this->deliveryAddress = (int) $this->session_data_checkout_decoded->checkout->selected_address->delivery_address;
        $this->billingAddress = (int) $this->session_data_checkout_decoded->checkout->selected_address->billing_address;

        if ($this->billingAddress == 0) {
            $this->billingAddress = $this->deliveryAddress;
        }

        $allowStatus = false;
        if ($this->planType == "trial-plan") {
            $allowStatus = array_key_exists('planId', $this->session_data_selection_decoded->selection->summary);
        } else if ($this->planType == "custom-plan") {
            $allowStatus = array_key_exists('planId', $this->session_data_selection_decoded->selection->step5);
        }

        // Get plan data
        if ($this->session_data_selection_decoded && $this->session_data_checkout_decoded) {
            if ($allowStatus) {
                try {
                    if ($this->planType == "trial-plan") {
                        $this->planId = $this->session_data_selection_decoded->selection->summary->planId;
                        $this->addonList = $this->session_data_selection_decoded->selection->step2->selected_addon_list;
                        $this->frequency = $this->session_data_selection_decoded->selection->step3->selected_frequency;
                    } else if ($this->planType == "custom-plan") {
                        $this->planId = $this->session_data_selection_decoded->selection->step5->planId;
                        $this->addonList = $this->session_data_selection_decoded->selection->step4->selected_addon_list;
                        $this->frequency = $this->session_data_selection_decoded->selection->step3->selected_frequency;
                    }

                    $this->plan = Plans::where('id', $this->planId)->first();
                    $planDetails = PlanDetails::where('PlanId', $this->planId)->get();
                    // Get tax data
                    $this->tax = $this->laravelHelper->ConvertArraytoObject($this->api_country_service->tax($this->baCountryId));
                    // Get card data
                    $this->customer_id = $this->session_data_checkout_decoded->checkout->selected_card->customer_id;
                    $this->cardData = Cards::where('customerId', $this->customer_id)->first();

                    // Get all product country ids for addon
                    if ($this->addonList && !empty($this->addonList)) {
                        foreach ($this->addonList as $addon) {
                            array_push($this->productCountryList, $addon->productcountriesid);
                            array_push($this->addonProductCountryList, $addon->productcountriesid);
                        }
                    }

                    // Get all product country ids for plan
                    if ($planDetails && !empty($planDetails)) {
                        foreach ($planDetails as $product) {
                            array_push($this->productCountryList, $product->ProductCountryId);
                        }
                    }

                    // Prepare parameters for trial plan order details
                    $this->addonProductCountryDetailsParams = array(
                        "appType" => $this->appType,
                        "isOnline" => $this->isOnline,
                        "isOffline" => $this->isOffline,
                        "product_country_ids" => $this->addonProductCountryList,
                    );

                    // Prepare parameters for custom plan order details
                    $this->productCountryDetailsParams = array(
                        "appType" => $this->appType,
                        "isOnline" => $this->isOnline,
                        "isOffline" => $this->isOffline,
                        "product_country_ids" => $this->productCountryList,
                    );

                    // For trial plan, check if customer want shave cream on the next billing
                    if ($this->planType == "trial-plan") {
                        if ($this->session_data_selection_decoded->selection->has_shave_cream_next_billing) {
                            $this->freeShaveCreamCycle = "all";
                        } else {
                            $this->freeShaveCreamCycle = "1";
                        }
                    }

                    // Get free shave cream sku for trial plan
                    $this->trialPlan = new TrialPlan();
                    $this->freeShaveCreamSKU = $this->trialPlan->getFreeShaveCream_SKU();

                    return $this->checkPromotion();
                } catch (Exception $e) {

                    throw $e;
                }
            } else {
                if ($this->planType == "trial-plan") {
                    return response()->json('redirect back to selection')->header("Access-Control-Allow-Origin", "*");
                } else {
                    return response()->json('redirect back to selection')->header("Access-Control-Allow-Origin", "*");
                }
            }
        } else {
            if ($this->planType == "trial-plan") {
                return response()->json('redirect back to selection')->header("Access-Control-Allow-Origin", "*");
            } else {
                return response()->json('redirect back to selection')->header("Access-Control-Allow-Origin", "*");
            }
        }
    }
    // Function: Initialize purchase data
    public function initializePurchaseData()
    {
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);

        // Get corresponding session data (selection & checkout)
        if ($this->planType == "trial-plan") {
            $session_data_selection = $this->session->_session("selection_trial_plan", "get", null);
            $session_data_checkout = $this->session->_session("checkout_trial_plan", "get", null);
        } else if ($this->planType == "custom-plan") {
            $session_data_selection = $this->session->_session("selection_custom_plan", "get", null);
            $session_data_checkout = $this->session->_session("checkout_custom_plan", "get", null);
        }

        // Decode session data
        $this->session_data_selection_decoded = json_decode($session_data_selection);
        $this->session_data_checkout_decoded = json_decode($session_data_checkout);

        // Get user data
        $user = $this->userhelper->getUserDetails($this->session_data_checkout_decoded->checkout->user->id);
        $this->userId = $user["user"]["id"];
        $this->userFullname = $user["user"]["firstName"] . $user["user"]["lastName"];
        $this->userEmail = $user["user"]["email"];
        $this->userPhone = $user["user"]["phone"];
        $this->deliveryAddress = $this->session_data_checkout_decoded->checkout->selected_address->delivery_address;
        $this->billingAddress = $this->session_data_checkout_decoded->checkout->selected_address->billing_address;

        $allowStatus = false;
        if ($this->planType == "trial-plan") {
            $allowStatus = array_key_exists('planId', $this->session_data_selection_decoded->selection->summary);
        } else if ($this->planType == "custom-plan") {
            $allowStatus = array_key_exists('planId', $this->session_data_selection_decoded->selection->step5);
        }

        // Get plan data
        if ($this->session_data_selection_decoded && $this->session_data_checkout_decoded) {
            if ($allowStatus) {
                try {
                    if ($this->planType == "trial-plan") {
                        $this->planId = $this->session_data_selection_decoded->selection->summary->planId;
                        $this->addonList = $this->session_data_selection_decoded->selection->step2->selected_addon_list;
                        $this->frequency = $this->session_data_selection_decoded->selection->step3->selected_frequency;
                    } else if ($this->planType == "custom-plan") {
                        $this->planId = $this->session_data_selection_decoded->selection->step5->planId;
                        $this->addonList = $this->session_data_selection_decoded->selection->step4->selected_addon_list;
                        $this->frequency = $this->session_data_selection_decoded->selection->step3->selected_frequency;
                         //for custom annual
                         $this->getisannual = $this->session_data_selection_decoded->selection->step5->isAnnual;
                         $this->getannualQTY = $this->session_data_selection_decoded->selection->step5->Annualqty;
                         $this->getdiscountamount = $this->session_data_selection_decoded->selection->step5->discountamount;
                         $this->getdiscountpercentage = $this->session_data_selection_decoded->selection->step5->discountpecentage;
                         if((int) $this->getisannual == 1 ){
                            $this->frequency = 12;
                        }
                    }
                    $this->plan = Plans::where('id', $this->planId)->first();
                    $planDetails = PlanDetails::where('PlanId', $this->planId)->get();

                    // Get tax data
                    $this->tax = $this->laravelHelper->ConvertArraytoObject($this->country_service->tax($this->country_id));

                    // Get card data
                    $this->customer_id = $this->session_data_checkout_decoded->checkout->selected_card->customer_id;
                    $this->cardData = Cards::where('customerId', $this->customer_id)->first();

                    // Get all product country ids for addon
                    if ($this->addonList && !empty($this->addonList)) {
                        foreach ($this->addonList as $addon) {
                            array_push($this->productCountryList, $addon->productcountriesid);
                            array_push($this->addonProductCountryList, $addon->productcountriesid);
                        }
                    }

                    // Get all product country ids for plan
                    if ($planDetails && !empty($planDetails)) {
                        foreach ($planDetails as $product) {
                            array_push($this->productCountryList, $product->ProductCountryId);
                        }
                    }

                    // Prepare parameters for trial plan order details
                    $this->addonProductCountryDetailsParams = array(
                        "appType" => $this->appType,
                        "isOnline" => $this->isOnline,
                        "isOffline" => $this->isOffline,
                        "product_country_ids" => $this->addonProductCountryList,
                    );

                    // Prepare parameters for custom plan order details
                    $this->productCountryDetailsParams = array(
                        "appType" => $this->appType,
                        "isOnline" => $this->isOnline,
                        "isOffline" => $this->isOffline,
                        "product_country_ids" => $this->productCountryList,
                    );

                    // For trial plan, check if customer want shave cream on the next billing
                    if ($this->planType == "trial-plan") {
                        if ($this->session_data_selection_decoded->selection->has_shave_cream_next_billing) {
                            $this->freeShaveCreamCycle = "all";
                        } else {
                            $this->freeShaveCreamCycle = "1";
                        }
                    }

                    // Get free shave cream sku for trial plan
                    $this->trialPlan = new TrialPlan();
                    $this->freeShaveCreamSKU = $this->trialPlan->getFreeShaveCream_SKU();

                    return $this->checkPromotion();
                } catch (Exception $e) {

                    throw $e;
                }
            } else {
                if ($this->planType == "trial-plan") {
                    return redirect()->route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
                } else {
                    return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
                }
            }
        } else {
            if ($this->planType == "trial-plan") {
                return redirect()->route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            } else {
                return redirect()->route('locale.region.shave-plans.custom-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            }
        }
    }

    // Function: Check promotion
    public function checkPromotion()
    {
        // If got promo, set it into promotion variable
        try {
            $this->promotion = "";
            if (array_key_exists('promo', $this->session_data_checkout_decoded->checkout)) {
                if (array_key_exists('promo_id', $this->session_data_checkout_decoded->checkout->promo)) {
                    $this->promotion = $this->session_data_checkout_decoded->checkout->promo;
                }
            }
            return $this->checkReferral();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Check referral
    public function checkReferral()
    {
        try {
            $this->referral = "";
            return $this->checkProductPlanPrice();
        } catch (Exception $e) {
            throw $e;
        }
    }


    // Function: Calculate plan price
    public function checkProductPlanPrice()
    {
        try {

            // Get plan data
            $planidarray[] = $this->planId;
            $this->planData = $this->planhelper->getOnePlanData($planidarray);

            // Get base price for plan (variable with 'n' is for the next billing cycle)
            if ($this->planType == "trial-plan") {
                $realplanprice = $this->planData->trialPrice;
                $realnplanprice = $this->planData->sellPrice;
                $realplanoriprice = $this->planData->price;
                $realdiscountpercent = 0;
                if((int) $this->getisannual == 1 ){
                    $realdiscountpercent = $this->planData->discountPercent;
                }
            } else {
                $realplanprice = $this->planData->sellPrice;
                $realnplanprice = $this->planData->sellPrice;
                $realplanoriprice = $this->planData->price;
                $realdiscountpercent = 0;
                if((int) $this->getisannual == 1 ){
                    $realdiscountpercent = $this->planData->discountPercent;
                }
            }

            // Initialize variables
            $trialAddonPrice = 0;
            $realAddonPrice = 0;
            $totalprice = 0;
            $totalnprice = 0;
            $subtotalprice = 0;
            $totaldiscount = 0;
            $totaldiscountprice = 0;
            $oritotalprice = 0;
            $nexttotalprice = 0;
            if ($this->appType === 'baWebsite') {
                // Get shipping fee
                if ($this->planType) {
                    if ($this->planType == "trial-plan") {
                        $getshippingfee = $this->api_country_service->shippingFeeTrial($this->baCountryId, $this->langCode);
                    } else {
                        $getshippingfee = $this->api_country_service->shippingFee($this->baCountryId, $this->langCode);
                    }
                } else {
                    $getshippingfee = $this->api_country_service->shippingFee($this->baCountryId, $this->langCode);
                }
                $shippingfee = "";
                if ($getshippingfee) {
                    $shippingfee = (float) $getshippingfee["shippingfee"];
                }
            } else {
                // Get shipping fee
                if ($this->planType) {
                    if ($this->planType == "trial-plan") {
                        $getshippingfee = $this->country_service->shippingFeeTrial($this->country_id, $this->lang_code);
                    } else {
                        $getshippingfee = $this->country_service->shippingFee($this->country_id, $this->lang_code);
                    }
                } else {
                    $getshippingfee = $this->country_service->shippingFee($this->country_id, $this->lang_code);
                }
                $shippingfee = "";
                if ($getshippingfee) {
                    $shippingfee = (float) $getshippingfee["shippingfee"];
                }
            }

            // Get addon product country
            $this->addonProductCountry = array();
            $this->addonProductCountryWithOne = array();
            $this->trialAddonProductCountry = array(); // Only for current purchase of trial plan
            if (count(array($this->addonList)) > 0) {
                foreach ($this->addonList as $a) {
                    if ($this->planType == "trial-plan") {
                        if ($a->sku == $this->freeShaveCreamSKU) {
                            if ($this->freeShaveCreamCycle == "all") {
                                array_push($this->addonProductCountry, $a->productcountriesid);
                            }
                        } else {
                            //array_push($this->trialAddonProductCountry, $a->productcountriesid); // Addons other than free shave cream only start at next billing
                            array_push($this->addonProductCountry, $a->productcountriesid);
                        }
                    } else {
                        if(isset($a->cycle)){
                            if($a->cycle == "1"){
                                array_push($this->addonProductCountryWithOne, $a->productcountriesid);
                            }else{
                                array_push($this->addonProductCountry, $a->productcountriesid);
                            }
                            }else{
                                array_push($this->addonProductCountry, $a->productcountriesid);
                            }
                        array_push($this->trialAddonProductCountry, $a->productcountriesid);
                    }
                }
            }
            $nexttotalprice= $realplanoriprice;
            // Calculate price for trial plan first purchase only
            if (count(array($this->trialAddonProductCountry)) > 0) {
                $trialAddonPrice = $this->productHelper->getProductSumPrice($this->trialAddonProductCountry);
            }

            // Calculate price for custom plan and next billing
            if (count(array($this->addonProductCountry)) > 0) {
                $realAddonPriceone = 0 ;
                if (count(array($this->addonProductCountryWithOne)) > 0) {
                $realAddonPriceone = $this->productHelper->getProductSumPrice($this->addonProductCountryWithOne);
                }
                $realAddonPrice = $this->productHelper->getProductSumPrice($this->addonProductCountry);
                $nextrealAddonPrice = $realAddonPrice;
                if((int) $this->getisannual == 1 ){
                    $realAddonPrice = $realAddonPrice * $this->getannualQTY;
                    $nextrealAddonPrice  = $nextrealAddonPrice * $this->getannualQTY;
                }
                $nexttotalprice= $nextrealAddonPrice + $nexttotalprice;
                $realAddonPrice =  $realAddonPrice + $realAddonPriceone;
            }

            // Promotion
            $totalpriceproduct = $realAddonPrice + $realplanprice;
            if((int) $this->getisannual == 1 ){
                $totalpriceproduct = $realAddonPrice + $realplanoriprice;
                if((int) $realdiscountpercent > 0 ){
                    $dtotalpriceproduct =  (($totalpriceproduct * $realdiscountpercent) / 100);
                    $totalpriceproduct =  $totalpriceproduct -  $dtotalpriceproduct;
                }
               
            }
            // Promotion min spend block
            if ($this->promotion) {
                if ($this->promotion->promo_minSpend) {
                    if ($this->promotion->promo_minSpend > $totalpriceproduct) {
                        $this->minspendresult = 1;
                    }
                }
            }

            // Promotion free shippingfee
            if ($this->promotion) {
                if ($this->promotion->promo_isFreeShipping) {
                    if ($this->promotion->promo_isFreeShipping === 1) {
                        if (($this->promotion->promo_promotionType == "Instant" || $this->promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                            $shippingfee = 0;
                        }
                    }
                }
            }

            $discountforfreeexistproduct = 0;
            // Calculate tax amount and total price for first time purchase
            if ($this->planType == "trial-plan") {
                $itemprice = $trialAddonPrice + $realplanprice;
                $totalprice = $totalprice + $itemprice;
            } else {
                $itemprice = $realAddonPrice + $realplanprice;
                if((int) $this->getisannual == 1 ){
                    $itemprice = $realAddonPrice + $realplanoriprice;
                    if((int) $realdiscountpercent > 0 ){
                        $ditemprice =  (($itemprice * $realdiscountpercent) / 100);
                        $itemprice =  $itemprice -  $ditemprice;
                    } 
                }
                // Promotion free exist product
                if ($this->promotion) {
                    if (($this->promotion->promo_promotionType == "Instant" || $this->promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0 && $this->promotion->promo_ablefreeexistproduct === 1) {
                        $discountforfreeexistproduct = $discountforfreeexistproduct + $this->promotion->promo_freeexistresultproductprice;
                    }
                }
                $totalprice = $totalprice + $itemprice;
                if ($discountforfreeexistproduct > 0) {
                    $totalprice = $totalprice - $discountforfreeexistproduct;
                }
            }

            // Calculate tax amount and total price for next billing
            $totalnprice = $realAddonPrice + $realnplanprice;

            // Use subtotalprice for custom plan
            $subtotalprice = $realAddonPrice + $realplanprice;
            if((int) $this->getisannual == 1 ){
                $subtotalprice = $realAddonPrice + $realplanoriprice;
                if((int) $realdiscountpercent > 0 ){
                    $dsubtotalprice =  (($subtotalprice * $realdiscountpercent) / 100);
                    $subtotalprice =  $subtotalprice -  $dsubtotalprice;
                    
                    $dnexttotalprice= (($nexttotalprice * $realdiscountpercent) / 100);
                    $nexttotalprice =  $nexttotalprice -  $dnexttotalprice;
                } 
            }
            $oritotalprice  = $realAddonPrice + $realplanoriprice;
            // Promotion
            if ($this->promotion) {
                $totaldiscount = (($totalprice * $this->promotion->promo_discount) / 100);
                $totaldiscountprice = $totalprice - $totaldiscount;

                $totaldiscountprice = $totaldiscountprice + $shippingfee;

                // trial plan tax hide
                if ($this->planType != "trial-plan") {
                    $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                    $totaldiscountprice = $totaldiscountprice + 0.00;
                }

                $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');

                // Promotion min spend block
                if ($this->minspendresult == 1) {
                    $totaldiscount = 0;
                    $totaldiscountprice = $totalprice + $shippingfee;

                    // trial plan tax hide
                    if ($this->planType != "trial-plan") {
                        $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                        $totaldiscountprice = $totaldiscountprice + 0.00;
                    }

                    $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                }
                // Promotion maxdiscount
                if ($this->promotion->promo_maxDiscount) {
                    if ($this->promotion->promo_maxDiscount < $totaldiscount) {
                        $totaldiscount = $this->promotion->promo_maxDiscount;
                        $totaldiscountprice = $totalprice - $this->promotion->promo_maxDiscount;
                        $totaldiscountprice = $totaldiscountprice + $shippingfee;

                        // trial plan tax hide
                        if ($this->planType != "trial-plan") {
                            $taxAmount = $this->tax->taxRate / 100 * ($totaldiscountprice);
                            $totaldiscountprice = $totaldiscountprice + 0.00;
                        }

                        $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                    }
                }
                // Promotion free exist product
                if ($discountforfreeexistproduct > 0) {
                    $totaldiscount = $totaldiscount + $discountforfreeexistproduct;
                }

                // Promotion no negative number
                if ($totaldiscountprice <= 0) {
                    $totaldiscountprice = 0;
                    $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                }
            }

            $totalprice = $totalprice + $shippingfee;

            // trial plan tax hide
            if ($this->planType != "trial-plan") {
                $taxAmount = $this->tax->taxRate / 100 * ($totalprice);
                $totalprice = $totalprice + 0.00;
            }

            // Finalize total price
            $totalprice = number_format($totalprice, 2, '.', '');

            // Insert into priceData variable for later access
            $this->priceData = (object) array();
            $this->priceData->totalprice = $totalprice;
            $this->priceData->totalnprice = $totalnprice;
            $this->priceData->subtotal = $subtotalprice;
            $this->priceData->totaldiscount = $totaldiscount;
            $this->priceData->totaldiscountprice = $totaldiscountprice;
            $this->priceData->shippingfee = $shippingfee;
            $this->priceData->oritotalprice = $oritotalprice;
            $this->priceData->nexttotalprice = $nexttotalprice;
            // trial plan tax hide
            if ($this->planType != "trial-plan") {
                $this->priceData->taxAmount = $taxAmount;
            } else {
                $this->priceData->taxAmount = 0;
            }

            return $this->createSubscription();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function createSubscription()
    {
        try {
            //remember email phone full name
            // Initialize dates
            $nextDeliverDate = Carbon::now("Asia/Kuala_Lumpur")->addMonths($this->frequency)->format('Y-m-d');
            $nextChargeDate = Carbon::now("Asia/Kuala_Lumpur")->addMonths($this->frequency)->format('Y-m-d');
            $startDeliverDate = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
            $expiredDateAt = Carbon::now("Asia/Kuala_Lumpur")->addYear(1)->format('Y-m-d');

            // Get card id
            if ($this->cardData) {
                $cardid = $this->cardData->id;
            } else {
                throw new Exception('');
            }

            // Create subscription
            $subscription = new Subscriptions();
            $subscription->DeliveryAddressId = $this->deliveryAddress;
            $subscription->BillingAddressId = $this->billingAddress;
            $subscription->status = "Pending";
            $subscription->fullname = $this->userFullname;
            $subscription->email = $this->userEmail;
            $subscription->phone = $this->userPhone;
            $subscription->qty = 1;
            $subscription->currentDeliverNumber = 0;
            $subscription->currentCycle = 0;
            $subscription->currentChargeNumber = 0;
            $subscription->nextDeliverDate = $this->planType == "trial-plan" ? null : $nextDeliverDate;
            $subscription->nextChargeDate = $this->planType == "trial-plan" ? null : $nextChargeDate;
            $subscription->startDeliverDate = $this->planType == "trial-plan" ? null : $startDeliverDate;
            $subscription->price = $this->planType == "trial-plan" ? $this->priceData->totalnprice : $this->priceData->subtotal;
            $subscription->pricePerCharge = $this->planType == "trial-plan" ? $this->priceData->totalnprice : $this->priceData->nexttotalprice;
            $subscription->PlanId = $this->planId;
            $subscription->isTrial = $this->planType == "trial-plan";
            $subscription->isCustom = $this->planType == "custom-plan";
            $subscription->isOffline = false;
            $subscription->expiredDateAt = $expiredDateAt;
            $subscription->CardId = $cardid;
            $subscription->UserId = $this->userId;
            $subscription->SellerUserId = $this->seller === null ? null : $this->seller["id"];
            $subscription->MarketingOfficeId = $this->seller === null ? null : $this->seller["MarketingOfficeId"];
            if ($this->promotion) {
                if (array_key_exists('promo_code', $this->promotion) && array_key_exists('promo_id', $this->promotion) && $this->minspendresult === 0) {
                    $subscription->promoCode = $this->promotion->promo_code;
                    $subscription->promotionId = $this->promotion->promo_id;
                }
            }
            $subscription->SkuId = $this->plan->PlanSkuId;

            if ($this->appType === 'baWebsite') {
                $subscription->channelType = $this->event_data["ba_channel_type"];
                $subscription->eventLocationCode = $this->event_data["ba_event_location_code"];
                $subscription->MarketingOfficeId = $this->seller["MarketingOfficeId"];
                $subscription->isDirectTrial = $this->isDirectTrial;
            }
            if((int) $this->getisannual == 1 ){
                $subscription->isAnnualDiscountIncludeAddon = 1;
            }
            $subscription->save();

            // Save subscription details into a variable
            $this->subscription = $subscription;

            return $this->createSubscriptionHistory();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Create subscription history
    public function createSubscriptionHistory()
    {
        try {
            // Create subscription history
            $subscriptionhistories = new SubscriptionHistories();
            $subscriptionhistories->message = "Pending";
            $subscriptionhistories->subscriptionId = $this->subscription->id;
            $subscriptionhistories->save();

            return $this->createSubscriptionAddons();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Create subscription addons
    public function createSubscriptionAddons()
    {
        try {
            // Create subscription addons
            foreach ($this->addonList as $addon) {
                $subsAddons = new SubscriptionsProductAddOns();
                $subsAddons->subscriptionId = $this->subscription->id;
                $subsAddons->ProductCountryId = $addon->productcountriesid;

                // for annual plans selected -> product quantity is set
                if ($this->planSelected !== null && $this->planType === "trial-plan" && $this->appType === "baWebsite") {
                    $getMaxQty = [];
                    foreach ($this->planSelected->product_info as $original_products) {
                        array_push($getMaxQty, (int) $original_products->product_qty);
                    }
                    $subsAddons->qty = max($getMaxQty);
                } else {
                    if((int) $this->getisannual == 0 ){
                        $subsAddons->qty = 1;
                        }
                }
                if ($this->planType == "trial-plan") {
                    if ($addon->sku == $this->freeShaveCreamSKU) {
                        $subsAddons->cycle = $this->freeShaveCreamCycle;
                        $subsAddons->isWithTrialKit = 1;
                    } else {
                        $subsAddons->cycle = 'all';
                        $subsAddons->isWithTrialKit = 0;
                        $subsAddons->skipFirstCycle = 1;
                    }
                } else {
                    if (array_key_exists('cycle', $addon)) {
                        if ($addon->cycle == "1") {
                            $subsAddons->cycle = '1';
                            if((int) $this->getisannual == 1 ){
                                $subsAddons->qty = 1;
                            }
                        } else {
                            $subsAddons->cycle = 'all';
                            if((int) $this->getisannual == 1 ){
                                $subsAddons->qty = $this->getannualQTY;
                            }
                        }
                    } else {
                        $subsAddons->cycle = 'all';
                        if((int) $this->getisannual == 1 ){
                            $subsAddons->qty = $this->getannualQTY;
                        }
                    }
                }
                $subsAddons->save();
            }

            return $this->getReferralDiscount();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getReferralDiscount()
    {
        try {
            $this->referralDiscount = "";
            return $this->createOrder();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function createOrder()
    {
        try {
            //remember email phone full name ssn
            //remember google api source medium campaign term content promotion discpont
            $data = null;
            $utm_parameters = $this->session->_session("utm_parameters", "get", null);
            if($utm_parameters && !is_array($utm_parameters)){
                // if ($this->checkJsondecode($utm_parameters)) {
                $utm_parameters = json_decode($utm_parameters);
                $data = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
                // }
            }

            // Create order
            $orders = new Orders();
            $orders->promoCode = null;
            $orders->PromotionId = null;
            // Apply promotion
            if ($this->promotion) {
                if (array_key_exists('promo_promotionType', $this->promotion)) {
                    if (($this->promotion->promo_promotionType == "Instant" || $this->promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                        if (array_key_exists('promo_code', $this->promotion) && array_key_exists('promo_id', $this->promotion)) {
                            $orders->promoCode = $this->promotion->promo_code;
                            $orders->PromotionId = $this->promotion->promo_id;
                        }
                    }
                }
            }
            $orders->SSN = null;
            $orders->email = $this->userEmail;
            $orders->phone = $this->userPhone;
            $orders->fullName = $this->userFullname;
            $orders->DeliveryAddressId = $this->deliveryAddress;
            $orders->BillingAddressId = $this->billingAddress;
            $orders->taxRate = $this->tax->taxRate;
            $orders->taxName = $this->tax->taxName;
            $orders->CountryId = $this->appType === 'baWebsite' ? $this->baCountryId : $this->country_id;
            $orders->subscriptionIds = $this->subscription->id;
            $orders->source = $data ? (isset($data->utm_source) ? $data->utm_source : null) : null;
            $orders->medium = $data ? (isset($data->utm_medium) ? $data->utm_medium : null) : null;
            $orders->campaign = $data ? (isset($data->utm_campaign) ? $data->utm_campaign : null) : null;
            $orders->term = $data ? (isset($data->utm_term) ? $data->utm_term : null) : null;
            $orders->content = $data ? (isset($data->utm_content) ? $data->utm_content : null) : null;
            $orders->status = "Pending";
            $orders->paymentType = "nicepay";
            $orders->isSubsequentOrder = isset($this->subscription) && $this->subscription->isTrial == 1 && $this->subscription->currentChargeNumber == 0 ? 0 : 1;

            if ($orders->CountryId  == "7") {
                $orders->carrierAgent = "courex";
            } else {
                $orders->carrierAgent = null;
            }

            if ($this->appType === 'baWebsite') {
                $orders->SellerUserId = $this->seller["id"];
                $orders->channelType = $this->event_data["ba_channel_type"];
                $orders->eventLocationCode = $this->event_data["ba_event_location_code"];
                $orders->MarketingOfficeId = $this->seller["MarketingOfficeId"];
                $orders->isDirectTrial = $this->isDirectTrial;
                $orders->status = $this->isDirectTrial === 1 ? "Completed" : "Pending";
            }

            $orders->UserId = $this->userId;

            $orders->save();

            // Save order into a variable
            $this->order = $orders;
            $this->session->_session("utm_parameters", "clear", null);
            return $this->createOrderDetails();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // public function checkJsondecode($data){
    //     if (!empty($data)) {
    //         json_decode($data);
    //         return (json_last_error() === JSON_ERROR_NONE);
    // }
    // return false;  
    // }


    // Function: Create order details
    public function createOrderDetails()
    {
        try {
            // Create order details
            if ($this->planType == "trial-plan") {
                // Handle, blade and free shave cream is treated as 1 order detail during trial plan
                $planOrderdetails = new OrderDetails();
                $planOrderdetails->qty = 1;
                $planOrderdetails->price = $this->plan->trialPrice;
                $planOrderdetails->currency = $this->appType === 'baWebsite' ? $this->country_info->currencyDisplay : $this->country_info->data_from_db->currencyDisplay;
                $planOrderdetails->startDeliverDate = null;
                $planOrderdetails->created_at = Carbon::now();
                $planOrderdetails->updated_at = Carbon::now();
                $planOrderdetails->OrderId = $this->order->id;
                $planOrderdetails->PlanId = $this->plan->id;
                $planOrderdetails->save();

                // For additional addons
                // $addonOrderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($this->country_id, $this->lang_code, $this->addonProductCountryDetailsParams));
                // foreach ($addonOrderDetailList as $detail) {
                //     if ($detail->sku != $this->freeShaveCreamSKU) {
                //         $orderdetails = new OrderDetails();
                //         $orderdetails->qty = 1;
                //         $orderdetails->price = $detail->sellPrice;
                //         $orderdetails->currency = $this->country_info->data_from_db->currencyDisplay;
                //         $orderdetails->startDeliverDate = null;
                //         $orderdetails->created_at = Carbon::now();
                //         $orderdetails->updated_at = Carbon::now();
                //         $orderdetails->OrderId = $this->order->id;
                //         $orderdetails->ProductCountryId = $detail->ProductCountryId;
                //         $orderdetails->isAddon = 1;
                //         $orderdetails->save();
                //     }
                // }
            } else {
                // Custom plan will include all products
                $orderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($this->appType === 'baWebsite' ? $this->baCountryId : $this->country_id, $this->lang_code, $this->productCountryDetailsParams));
                foreach ($orderDetailList as $detail) {
                    $orderdetails = new OrderDetails();
                    
                    if((int) $this->getisannual == 1 ){
                        $orderdetails->qty = $this->getannualQTY;
                        foreach ($this->addonList as $addon) {
                            if($addon->productcountriesid == $detail->ProductCountryId){
                            if (array_key_exists('cycle', $addon)) {
                                if ($addon->cycle == "1") {
                                    $orderdetails->qty = 1;
                                }
                            }
                        }
                    }
                       
                    }else{
                    $orderdetails->qty = 1;
                    }
                    $orderdetails->price = $detail->sellPrice;
                    $orderdetails->currency = $this->appType === 'baWebsite' ? $this->country_info->currencyDisplay : $this->country_info->data_from_db->currencyDisplay;
                    $orderdetails->startDeliverDate = null;
                    $orderdetails->created_at = Carbon::now();
                    $orderdetails->updated_at = Carbon::now();
                    $orderdetails->OrderId = $this->order->id;
                    $orderdetails->ProductCountryId = $detail->ProductCountryId;
                    $orderdetails->isAddon = in_array($detail->ProductCountryId, $this->addonProductCountryList) ? 1 : 0;
                    // Promotion free exist product
                    if ($this->promotion) {
                        if ($this->promotion->promo_freeexistresultproduct) {
                            if (in_array($detail->ProductCountryId, $this->promotion->promo_freeexistresultproduct)) {
                                $orderdetails->isFreeProduct = 1;
                            }
                        }
                    }
                    $orderdetails->save();
                }
            }

            return $this->createOrderHistory();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Create order history
    public function createOrderHistory()
    {
        try {
            // Create order history
            $ordershistories = new OrderHistories();
            if ($this->appType === 'baWebsite') {
                $ordershistories->message = $this->isDirectTrial === 1 ? "Completed" : "Pending";
            } else {
                $ordershistories->message = "Pending";
            }
            $ordershistories->OrderId = $this->order->id;
            $ordershistories->save();

            return $this->createReceipt();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function createReceipt()
    {
        try {
            // Create receipt
            $receipts = new Receipts();
            $receipts->totalPrice = $this->priceData->totalprice;
            $receipts->subTotalPrice = $this->planType == "trial-plan" ? $this->priceData->totalprice : $this->priceData->subtotal;
            $receipts->discountAmount = 0;
            if($this->getdiscountamount > 0 ){
                $receipts->discountAmount = $this->getdiscountamount;
           }
            // Apply promotion
            if ($this->promotion) {
                if (array_key_exists('promo_promotionType', $this->promotion)) {
                    if (($this->promotion->promo_promotionType == "Instant" || $this->promotion->promo_promotionType == "Recurring") && $this->minspendresult === 0) {
                        $receipts->totalPrice = $this->priceData->totaldiscountprice;
                        $receipts->subTotalPrice = $this->planType == "trial-plan" ? $this->priceData->totalprice : $this->priceData->subtotal;
                        $receipts->discountAmount =  (int) $this->getisannual == 1 ? $this->priceData->totaldiscount +  $this->getdiscountamount : $this->priceData->totaldiscount;
                    }
                }
            }
            $receipts->currency = $this->appType === 'baWebsite' ? $this->country_info->currencyDisplay : $this->country_info->data_from_db->currencyDisplay;
            $receipts->originPrice = $this->planType == "trial-plan" ? $this->priceData->totalprice : ((int) $this->getisannual == 1 ? $this->priceData->oritotalprice : $this->priceData->subtotal);
            $receipts->taxAmount = $this->priceData->taxAmount;
            $receipts->shippingFee = $this->priceData->shippingfee;
            $receipts->OrderId = $this->order->id;
            $receipts->SubscriptionId = $this->subscription->id;
            $receipts->save();

            // Save receipt details into a variable
            $this->receipt = $receipts;

            return $this->createPrepaymentData();
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Create prepayment data
    public function createPrepaymentData()
    {
        try {
            // Initialize data needed before payment process begins
            $this->prepayment_data = [
                'receiptId' => $this->receipt->id,
                'orderId' => $this->order->id,
                'subscriptionId' => $this->subscription->id,
                'final_price' => $this->receipt->totalPrice,
            ];
            $this->prepayment_data = $this->laravelHelper->ConvertArraytoObject($this->prepayment_data);

            return $this->updateCheckoutSessionData();
        } catch (Exception $e) {
            throw $e;
        }
    }
    // Function: Update checkout session data
    public function updateCheckoutSessionData()
    {
        try {
            // Update session with order data
            $order_data = (object) array();
            $order_data->subscriptionid = $this->prepayment_data->subscriptionId;
            $order_data->orderid = $this->prepayment_data->orderId;
            $order_data->receiptid = $this->prepayment_data->receiptId;
            $this->session_data_checkout_decoded->checkout->order_data = (object) array();
            $this->session_data_checkout_decoded->checkout->order_data = $order_data;

            if ($this->appType !== 'baWebsite') {
                if ($this->planType == "trial-plan") {
                    $this->session->_session("checkout_trial_plan", "set", json_encode($this->session_data_checkout_decoded));
                } else if ($this->planType == "custom-plan") {
                    $this->session->_session("checkout_custom_plan", "set", json_encode($this->session_data_checkout_decoded));
                }
            }

            // If final price is more than 0, go to update payment intent one more time
            // Else no need to go through payment and immediately update status
            if ($this->prepayment_data->final_price > 0) {
                return $this->nicepaypayment();
            } else {
                $orderData = (object) array();
                $orderData->subscriptionid = $order_data->subscriptionid;
                $orderData->orderid = $order_data->orderid;
                $orderData->receiptid = $order_data->receiptid;
                return $this->nicepaypayment();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function nicepaypayment()
    {
        try {
            $isfree = 0;
            $amount = $this->prepayment_data->final_price;
            $orderid = $this->prepayment_data->orderId;
            $receiptid = $this->prepayment_data->receiptId;
            $subscriptionid = $this->prepayment_data->subscriptionId;

            $fullName = $this->userFullname;
            $email = $this->userEmail;
            $status1 = "";
            $customer_id = $this->session_data_checkout_decoded->checkout->selected_card->customer_id;
            if ($amount > 0) {
                $paymentresult = $this->nicepayController->nicepayPayment($fullName, $email, $customer_id, $amount, $orderid);
                $status1 = $paymentresult["status"];
            } else {
                $status1 = "isfree";
                $isfree = 1;
            }
            if ($status1 == "success" || $status1 = "isfree") {
                if ($isfree == 1) {
                    $getnicepay_imp = "zero price";
                    $getnicepay_mid = "zero price";
                    $branchName = "-";
                    $chargecurreny = "krw";
                } else {
                    $getnicepay_imp = $paymentresult["data"]["response"]["imp_uid"];
                    $getnicepay_mid = $paymentresult["data"]["response"]["merchant_uid"];
                    $branchName = $paymentresult["data"]["response"]["card_name"];
                    $chargecurreny = "krw";
                }

                $order_status = 'Payment Received';
                $order_data = Orders::findorfail($orderid);
                $order_isdirecttrial = isset($order_data) && $order_data->isDirectTrial == 1 ? 1 : 0;
                $order_status = isset($order_isdirecttrial) && $order_isdirecttrial == 1 ? "Completed" : "Payment Received";
                $this->country_info = Countries::findorfail($this->country_id);
                $this->country_id = $order_data->CountryId;
                Orders::where('id', $orderid)
                    ->update([
                        'status' => $order_status,
                        'payment_status' => 'Charged',
                        'imp_uid' => $getnicepay_imp,
                        'merchant_uid' => $getnicepay_mid,
                    ]);

                $paymentHistory = new PaymentHistories();
                $paymentHistory->OrderId = $orderid;
                $paymentHistory->message = 'Charged';
                $paymentHistory->isRemark = 0;
                $paymentHistory->created_by = null;
                $paymentHistory->updated_by = null;
                $paymentHistory->save();
                // add order to queue service
                $_redis_order_data = (object) array(
                    'OrderId' => $orderid,
                    'CountryId' => $this->country_id,
                    'CountryCode' => strtolower($this->appType === 'baWebsite' ? $this->country_info->codeIso : $this->country_info->country_iso),
                );
                $this->orderHelper->addTaskToOrderQueueService($_redis_order_data);
                $_redis_sales_report_data = ['id' => $orderid, 'bulkOrder' => false];
                SaleReportQueueService::addHash($orderid, $_redis_sales_report_data);
                $_redis_tax_invoice_data = ['id' => $orderid, 'CountryId' => $this->appType === 'baWebsite' ? $this->baCountryId : $this->country_id];
                TaxInvoiceQueueService::addHash($orderid, $_redis_tax_invoice_data);
                // Create order history
                $ordershistories = new OrderHistories();
                $ordershistories->message = 'Payment Received';
                $ordershistories->OrderId = $orderid;
                $ordershistories->save();
                // Update nicepay charge and card information in receipt
                Receipts::where('id', $receiptid)
                    ->update([
                        'chargeFee' => 0,
                        'chargeCurrency' => $chargecurreny,
                        'branchName' => $branchName,
                        'chargeId' => $this->cardData->customerId,
                        'last4' => $this->cardData->cardNumber,
                        'expiredYear' => $this->cardData->expiredYear,
                        'expiredMonth' => $this->cardData->expiredMonth,
                    ]);

                // Update subscription status
                Subscriptions::where('id', $subscriptionid)
                    ->update([
                        'status' => 'Processing',
                        'currentDeliverNumber' => $this->planType === "trial-plan" ? 0 : 1,
                        'currentChargeNumber' => $this->planType === "trial-plan" ? 0 : 1,
                        'currentCycle' => 1,
                        'currentOrderTime' => Carbon::now(),
                        'currentOrderId' => $orderid,
                    ]);

                // Create subscription history
                $subscriptionhistories = new SubscriptionHistories();
                $subscriptionhistories->message = "Processing";
                $subscriptionhistories->subscriptionId = $subscriptionid;
                $subscriptionhistories->save();

                // Promotion logic
                $subscriptiondata = Subscriptions::where('id', $subscriptionid)->first();
                $userid = $subscriptiondata->UserId;
                if ($subscriptiondata->promotionId) {
                    $PromotionCodes = Promotions::where('id', $subscriptiondata->promotionId)
                        ->first();
                    if ($subscriptiondata->promoCode) {
                        if ($PromotionCodes->isGeneric != 1) {
                            PromotionCodes::where('code', $subscriptiondata->promoCode)
                                ->where("PromotionId", $subscriptiondata->promotionId)
                                ->update([
                                    'isValid' => 0
                                ]);
                        }
                    }
                    $PromotionCodes->appliedTo = str_replace("{", "", $PromotionCodes->appliedTo);
                    $PromotionCodes->appliedTo = str_replace("}", "", $PromotionCodes->appliedTo);
                    $splitapplyto = $PromotionCodes->appliedTo;
                    // $peruse = $PromotionCodes->timePerUser;
                    if (strpos($splitapplyto, ',') !== false) {
                        $splitapplyto = explode(",", $PromotionCodes->appliedTo);
                    }
                    // $promoapplyupdate = ",".$userid.":1";
                    $promoapplyupdate = "";
                    $combineapply = "{";
                    $count = 1;
                    if (is_array($splitapplyto)) {
                        foreach ($splitapplyto as $at) {
                            if (strpos($at, ':') !== false) {
                                $splitqty = explode(":", $at);
                                if ($userid == $splitqty[0]) {
                                    $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                                    if ($count == 1) {
                                        $combineapply = $combineapply . $promoapplyupdate;
                                        $count++;
                                    } else {
                                        $combineapply = $combineapply . "," . $promoapplyupdate;
                                        $count++;
                                    }
                                } else {
                                    if ($count == 1) {
                                        $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                                        $count++;
                                    } else {
                                        $combineapply = $combineapply . "," . $splitqty[0] . ":" . $splitqty[1];
                                        $count++;
                                    }
                                }
                            }
                        }
                    } else {
                        if (strpos($splitapplyto, ':') !== false) {
                            $splitqty = explode(":", $splitapplyto);
                            if ($userid == $splitqty[0]) {
                                $promoapplyupdate = $splitqty[0] . ":" . intval($splitqty[1] + 1);
                                $combineapply = $combineapply . $promoapplyupdate;
                            } else {
                                $combineapply = $combineapply . $splitqty[0] . ":" . $splitqty[1];
                            }
                        }
                    }
                    if ($promoapplyupdate) {
                        $combineapply = $combineapply . "}";
                    } else {
                        if ($splitapplyto) {
                            $combineapply = $combineapply . "," . $userid . ":1" . "}";
                        } else {
                            $combineapply = $combineapply . $userid . ":1" . "}";
                        }
                    }

                    // Promotion Free Product
                    if ($PromotionCodes->freeProductCountryIds) {
                        if (($PromotionCodes->promotionType == "Instant" || $PromotionCodes->promotionType == "Recurring")) {
                            $PromotionCodes->freeProductCountryIds = str_replace("[", "", $PromotionCodes->freeProductCountryIds);
                            $PromotionCodes->freeProductCountryIds = str_replace("]", "", $PromotionCodes->freeProductCountryIds);
                            $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);
                            $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);

                            $splitproduct = $PromotionCodes->freeProductCountryIds;
                            if (strpos($splitproduct, ',') !== false) {
                                $splitproduct = explode(",", $PromotionCodes->freeProductCountryIds);
                            }
                            $productseperate = [];
                            if (is_array($splitproduct)) {
                                foreach ($splitproduct as $product) {
                                    array_push($productseperate, $product);
                                }
                            } else {
                                array_push($productseperate, $splitproduct);
                            }

                            // Prepare parameters for trial plan order details
                            $productcountyidarray = array(
                                "appType" => $this->appType,
                                "isOnline" => 1,
                                "isOffline" => 1,
                                "product_country_ids" => $productseperate,
                            );
                            $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($this->country_id, $this->lang_code, $productcountyidarray));

                            foreach ($product as $pci) {
                                $orderdetails = new OrderDetails();
                                $orderdetails->qty = 1;
                                $orderdetails->price = $pci->sellPrice;
                                $orderdetails->currency = $this->appType === 'baWebsite' ? $this->country_info->currencyDisplay : $this->country_info->data_from_db->currencyDisplay;
                                $orderdetails->startDeliverDate = null;
                                $orderdetails->created_at = Carbon::now();
                                $orderdetails->updated_at = Carbon::now();
                                $orderdetails->OrderId = $orderid;
                                $orderdetails->ProductCountryId = $pci->ProductCountryId;
                                $orderdetails->isFreeProduct = 1;
                                $orderdetails->isAddon = 0;
                                $orderdetails->save();
                            }
                        }
                    }

                    Promotions::where('id', $subscriptiondata->promotionId)
                        ->update([
                            'appliedTo' => $combineapply,
                        ]);
                }

                $result = [
                    'status' => 'success',
                    'orderid' => $orderid,
                ];

                // update referral if session exists.
                if ($orderid && isset($orderid) && $orderid !== null) {
                    // get subscription type
                    $current_subs = Subscriptions::where('id', $this->prepayment_data->subscriptionId)->first();
                    // if session does not exists check db for cronjob
                    $order_info = Orders::where('id', $orderid)->first();
                    if ($order_info) {
                        $referee_info = User::where('id', $order_info->UserId)->first();
                        if ($referee_info) {
                            $_check_referral = Referral::where('friendId', $referee_info->id)
                                ->where('referralId', $referee_info->referralId)->where('firstPurchase', 0)
                                ->first();
                            $referrer_info = User::where('id', $referee_info->referralId)->first();

                            if (!empty($_check_referral) && !empty($referrer_info)) {
                                $referrer_country = Countries::where('id', $_check_referral->CountryId)->first();
                                if ($current_subs["isTrial"] === 1 && $current_subs["isCustom"] === 0) {
                                } else if ($current_subs["isCustom"] === 1 && $current_subs["isTrial"] === 0) {
                                    Referral::where('friendId', $_check_referral->friendId)
                                        ->where('referralId', $_check_referral->referralId)
                                        ->update(['firstPurchase' => 1, 'status' => 'Active']);
                                }
                                $rewardscurrencies = RewardsCurrency::where('CountryId', $_check_referral->CountryId)->first();
                                if (!empty($rewardscurrencies)) {
                                    $_insert_into_rewardsin = RewardsIn::create([
                                        'CountryId' => $_check_referral->CountryId,
                                        'UserId' => $_check_referral->referralId,
                                        'value' => $rewardscurrencies->id,
                                        'OrderId' => $order_info->id,
                                    ]);

                                    if ($_insert_into_rewardsin) {
                                        if ($current_subs["isCustom"] === 1 && $current_subs["isTrial"] === 0) {
                                            Referral::where('referralId', $_check_referral->referralId)->update(["firstPurchase" => 1, "rewardId" => $_insert_into_rewardsin->id, "status" => 'Active']);
                                        }
                                        //send email to referrer for active credits
                                        // $data = (Object) array(
                                        //     'referrer' => (Object) array(
                                        //         'id' => $_check_referral->id,
                                        //         'first_name' => $_check_referral->firstName,
                                        //         'last_name' => null,
                                        //         'country_id' => $_check_referral->CountryId,
                                        //         'amount_credited' => $rewardscurrencies->value,
                                        //     ),
                                        //     'referee' => (Object) array(
                                        //         'id' => $referee_info->id,
                                        //         'first_name' => $referee_info->firstName,
                                        //         'last_name' => null,
                                        //         'country_id' => $referee_info->CountryId,
                                        //     ),
                                        // );

                                        $emailData = [];
                                        if ($current_subs["isTrial"] === 1 && $current_subs["isCustom"] === 0) {
                                            $emailData['title'] = 'referral-inactive-credits';
                                            $emailData['moduleData'] = array(
                                                'email' => $referrer_info->email,
                                                'refuserId' =>  $referee_info->id,
                                                'countryId' => $_check_referral->CountryId,
                                                'isPlan' => 1,
                                            );

                                            $currentCountry = json_encode($referrer_country);
                                            $currentLocale = $referrer_info->defaultLanguage;
                                            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                                            // hide email redis
                                            $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                                        }
                                        if ($current_subs["isCustom"] === 1 && $current_subs["isTrial"] === 0) {
                                            $emailData['title'] = 'referral-active-credits';

                                            $emailData['moduleData'] = array(
                                                'email' => $referrer_info->email,
                                                'refuserId' =>  $referee_info->id,
                                                'countryId' => $_check_referral->CountryId,
                                                'isPlan' => 1,
                                            );

                                            $currentCountry = json_encode($referrer_country);
                                            $currentLocale = $referrer_info->defaultLanguage;
                                            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                                            // hide email redis
                                            $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $this->buildOrderConfirmationEdm($orderid, $receiptid, $subscriptionid);
                if ($this->planType == "trial-plan") {
                    $this->session->_session('selection_trial_plan', "clear", null);
                    $this->session->_session('checkout_trial_plan', "clear", null);
                } else if ($this->planType == "custom-plan") {
                    $this->session->_session('selection_custom_plan', "clear", null);
                    $this->session->_session('checkout_custom_plan', "clear", null);
                }
                return $result;

                //taxInvoiceQueueService
            } else {
                // Update order status to payment failure
                Orders::where('id', $orderid)
                    ->update([
                        'status' => 'Payment Failure',
                        'promoCode' => null,
                        'PromotionId' => null,
                    ]);
                // Create order history
                $ordershistories = new OrderHistories();
                $ordershistories->message = 'Payment Failure';
                $ordershistories->OrderId = $orderid;
                $ordershistories->save();
                // Update subscription status to payment failure
                Subscriptions::where('id', $subscriptionid)
                    ->update([
                        'status' => 'Payment Failure',
                        'promoCode' => null,
                        'promotionId' => null,
                    ]);

                // Create subscription history
                $subscriptionhistories = new SubscriptionHistories();
                $subscriptionhistories->message = "Payment Failure";
                $subscriptionhistories->subscriptionId = $subscriptionid;
                $subscriptionhistories->save();

                $paymentHistory = new PaymentHistories();
                $paymentHistory->OrderId = $orderid;
                $paymentHistory->message = 'Failed';
                $paymentHistory->isRemark = 0;
                $paymentHistory->created_by = null;
                $paymentHistory->updated_by = null;
                $paymentHistory->save();

                $this->buildPaymentFailureEdm($orderid, $receiptid, $subscriptionid);
                throw new Exception($paymentresult["data"]["message"]);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function buildOrderConfirmationEdm($orderid, $receiptid, $subscriptionid)
    {
        $subscriptionData = Subscriptions::where('id', $subscriptionid)
            ->first();

        $userData = User::where('id', $subscriptionData->UserId)->first();
        $emailData = [];
        $emailData['title'] = 'receipt-order-confirmed';

        $emailData['moduleData'] = array(
            'email' => $userData->email,
            'subscriptionId' => $subscriptionid,
            'orderId' => $orderid,
            'receiptId' => $receiptid,
            'userId' => $userData->id,
            'countryId' => $userData->CountryId,
            'isPlan' => 1,
        );

        $currentCountry = json_encode(Countries::where('id', $userData->CountryId)->first());
        $currentLocale = $this->userHelper->getEmailDefaultLanguage($userData->id, $subscriptionid);
        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
        // hide email redis
        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
    }

    public function buildPaymentFailureEdm($orderid, $receiptid, $subscriptionid)
    {
        $subscriptionData = Subscriptions::where('id', $subscriptionid)
            ->first();

        $userData = User::where('id', $subscriptionData->UserId)->first();
        $emailData = [];
        $emailData['title'] = 'receipt-payment-failure-subscription';

        $emailData['moduleData'] = array(
            'email' => $userData->email,
            'subscriptionId' => $subscriptionid,
            'orderId' => $orderid,
            'receiptId' =>  $receiptid,
            'userId' => $userData->id,
            'countryId' => $userData->CountryId,
            'isPlan' => 1,
        );

        $currentCountry = json_encode(Countries::where('id', $userData->CountryId)->first());
        $currentLocale = $this->userHelper->getEmailDefaultLanguage($userData->id, $subscriptionid);
        $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
        // hide email redis
        $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
    }
}
