<?php

namespace App\Http\Controllers\Globals\Plans;

// Controllers & Services

use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Globals\Payment\StripeController;
use App\Http\Controllers\Globals\Utilities\SessionController;
// Models
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductType;
use App\Models\Subscriptions\Subscriptions;

// Helpers
use App\Services\CountryService as CountryService;
use Carbon\Carbon;
use function GuzzleHttp\json_decode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use Lang;

class TrialPlanController extends Controller
{
    public $product_helper;
    public $country_service;
    public $lang_code;
    public $country_id;

    // Constructor
    public function __construct()
    {
        $this->product_helper = new ProductHelper();
        $this->plan_helper = new PlanHelper();
        $this->country_service = new CountryService();
        $this->userHelper = new UserHelper();
        $this->session = new SessionController();
        $this->laravelHelper = new LaravelHelper();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->country_info = $this->country_service->getCountryAndLangCode();
        $this->country_id = $this->country_info['country_id'];
        $this->country_iso = $this->country_info['country_iso'];
        $this->currency = $this->country_service->getCurrencyDisplay();
        $this->agent = new Agent();

        // if Not Korea
        if ($this->country_id !== 8) {
            $this->stripeController = new StripeController();
        }
        // SKUs and Product Type might differ based on country, so can do if else here
        if ($this->country_id) {
            $this->freeShaveCreamSKU = config('global.' . config("app.appType") . '.ShaveCream');
            $this->trialPlanHandle_ProductTypeName = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanHandle');
            $this->trialPlanBlade_ProductTypeName = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanBlade');
            $this->trialPlanAddons_ProductTypeName = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanAddOn');
        }
    }

    // Function: Get product sku for free shave cream
    public function getFreeShaveCream_SKU()
    {
        return $this->freeShaveCreamSKU;
    }

    // Function: Get product country id for free shave cream
    public function getFreeShaveCream_ProductCountriesId()
    {
        $freeShaveCreamProduct = Product::where('sku', $this->freeShaveCreamSKU)->first();
        $freeShaveCreamProductCountry = ProductCountry::where('ProductId', $freeShaveCreamProduct->id)
            ->where('CountryId', $this->country_id)
            ->first();
        return $freeShaveCreamProductCountry->id;
    }

    // Function: Get trial plan handles
    public function getHandleList()
    {
        $trialPlanHandle_ProductType = ProductType::where('name', $this->trialPlanHandle_ProductTypeName)->first();
        $handle_products = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanHandle_ProductType->id));
        return $this->product_helper->getProductDefaultImageWithData($handle_products);
    }

    // Function: Get trial plan blades
    public function getBladeList()
    {
        $trialPlanBlade_ProductType = ProductType::where('name', $this->trialPlanBlade_ProductTypeName)->first();
        $blade_products = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanBlade_ProductType->id));
        $allBlades = $this->product_helper->getProductDefaultImageWithData($blade_products);

        $position = 0;
        foreach ($allBlades as &$blade) {
            $bladeSku = $blade["sku"];
            $blade["position"] = $position;
            $bladeCount = config('global.all.blade_no.' . $bladeSku);
            $blade["smallBladeImageUrl"] = config('global.' . config("app.appType") . '.checkout.trial.blade.' . $bladeCount . '.smallBladeImageUrl');
            $blade["bladeCount"] = $bladeCount;
            $blade["bladeName"] = $bladeCount . ' ' . Lang::get('website_contents.blade_packV4');
            $position++;
        }
        return $allBlades;
    }

    // Function: Get list of frequency
    public function getFrequencyList()
    {
        $durations = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanDuration');
        $frequency_list = [];
        foreach ($durations as $duration) {
            $data = array();
            $data["duration"] = $duration;
            $data["image"] = config('global.' . config("app.appType") . '.checkout.trial.frequency.' . $duration . '.image');
            $data["durationText"] = Lang::get('website_contents.frequency.' . $duration . '.durationText');
            $data["detailsText"] = Lang::get('website_contents.frequency.' . $duration . '.detailsText');
            $data["selectedDetailsText"] = Lang::get('website_contents.frequency.' . $duration . '.selectedDetailsText');
            array_push($frequency_list, $data);
        }
        return $frequency_list;
    }

    // Function: Get list of addons
    public function getAddonList()
    {
        $trialPlanAddons_ProductType = ProductType::where('name', $this->trialPlanAddons_ProductTypeName)->first();
        $addon_products_temp = $this->product_helper->getProductByProductType($this->country_id, $this->lang_code, array("ProductTypeId" => $trialPlanAddons_ProductType->id));
        $addon_products = array();

        // Get product country id for free shave cream
        $freeShaveCream_ProductCountryId = $this->getFreeShaveCream_ProductCountriesId();

        //Make sure the free shave cream is always on the top
        foreach ($addon_products_temp as $product) {
            if ($product['productcountriesid'] == $freeShaveCream_ProductCountryId) {
                array_push($addon_products, $product);
            }
        }

        foreach ($addon_products_temp as $product) {
            if ($product['productcountriesid'] != $freeShaveCream_ProductCountryId) {
                array_push($addon_products, $product);
            }
        }
        return $addon_products;
    }

    // Function: Get all trial plans based on countryId
    public function getAllTrialPlan()
    {
        $plancategories = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanGroup');
        $plantype = config('global.' . config("app.appType") . '.TrialPlan.TrialPlanType');
        $allplan = $this->plan_helper->getPlanIdPriceByCategory($this->country_id, $this->lang_code, $plantype, $plancategories);
        return $allplan;
    }

    // Function: Get trial plan price by country, used in the main page
    public function getTrialPlanDisplayPrice()
    {
        switch (strtolower($this->country_iso)) {
            case "my":return $this->laravelHelper->ConvertToNDecimalPoints(6.50, 2);
                break;
            case "hk":return $this->laravelHelper->ConvertToNDecimalPoints(46.00, 2);
                break;
            case "sg":return $this->laravelHelper->ConvertToNDecimalPoints(5.00, 2);
                break;
            case "kr":return "4,000";
                break;
            case "tw":return $this->laravelHelper->ConvertToNDecimalPoints(175.00, 2);
                break;
            default:return $this->laravelHelper->ConvertToNDecimalPoints(6.50, 2);
        }
    }

    // Function: Get trial plan saving price by country, used in the main page
    public function getTrialPlanDisplaySavingPrice()
    {
        switch (strtolower($this->country_iso)) {
            case "my":return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
                break;
            case "hk":return $this->laravelHelper->ConvertToNDecimalPoints(55.00, 2);
                break;
            case "sg":return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
                break;
            case "kr":return "15,500";
                break;
            case "tw":return $this->laravelHelper->ConvertToNDecimalPoints(400.00, 2);
                break;
            default:return $this->laravelHelper->ConvertToNDecimalPoints(15.00, 2);
        }
    }

    // Function: Check whether the user already purchase trial kit before
    public function checkTrialPlanExist(Request $request)
    {
        $userId = $request->userid;
        $trialPlanCount = Subscriptions::where('UserId', $userId)->where('isTrial', 1)->whereIn('status', ['Cancelled', 'On Hold', 'Unrealized', 'Processing'])->count();
        if ($trialPlanCount > 0) {
            return "exist";
        } else {
            return "not exist";
        }
    }

    // Function: Get next refill date
    public function getNextRefillDate()
    {
        return Carbon::now()->addDays(14)->format('d M');
    }

    public function getNextRefillDateCheckout()
    {
        return Carbon::now()->addDays(14)->format('M d');
    }

    // Function: Proceed to checkout page
    public function proceedCheckout(Request $request)
    {
        $session_data = $this->session->_session("selection_trial_plan", "get", null);

        $session_data_decoded = json_decode($session_data);
        $langCode = strtolower(app()->getLocale());
        $currentCountryIso = strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']);
        $currentCountryid = json_decode(session()->get('currentCountry'), true)['id'];
        $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);

        // build tax data
        $tax = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->country_service->tax($currentCountryid));
        $plandescription = $session_data_decoded->selection->step1->selected_blade->producttranslatesname;
        $current_price = $session_data_decoded->selection->summary->current_price;
        $shipping_fee = $session_data_decoded->selection->summary->shipping_fee;
        $shipping_fee = number_format($shipping_fee, 2);
        $next_price = $session_data_decoded->selection->summary->next_price;
        $current_totalprice = $current_price + $shipping_fee;
        $next_totalprice = $next_price + $shipping_fee;
        $taxAmount = $tax->taxRate / 100 * $next_totalprice;
        $next_totalprice = $next_totalprice + $taxAmount;
        $totalBlade = $session_data_decoded->selection->step1->selected_blade->bladeCount;
        $currentShippingImage = $session_data_decoded->selection->current_shipping_image;
        $nextBillingImage = $session_data_decoded->selection->next_billing_image;
      // mask selection
      $maskselection = null;
      $countmaskqty = 0;
      if($currentCountryid == 1 || $currentCountryid == 7){
      $maskselection = $this->product_helper->getFreeMaskSelection($currentCountryid, $langCode);
      if($maskselection){
       foreach ($maskselection as $m) {
           $countmaskqty = $m->qty +  $countmaskqty;
       }
      }
      }
      // mask selection
        if ($session_data_decoded) {
            if (array_key_exists('planId', $session_data_decoded->selection->summary)) {

                // Next billing subsciption name
                $subscriptionName = "";

                $subscriptionName .= $totalBlade . ' ' . Lang::get('website_contents.blade_packV4');
                $addonList = $session_data_decoded->selection->step2->selected_addon_list;
                foreach ($addonList as $addon) {
                    if ($addon->sku === $this->freeShaveCreamSKU) {
                        if ($session_data_decoded->selection->has_shave_cream_next_billing) {
                            $subscriptionName .= ", 1 " . $addon->producttranslatesname;
                        }
                    } else {
                        $subscriptionName .= ", 1 " . $addon->producttranslatesname;
                    }
                }

                $states = $this->country_service->getAllStates($currentCountryid);
                // If already login
                if (Auth::check()) {
                    $User = $this->userHelper->getUserDetails(Auth::user()->id);

                    if ($currentCountryid != 8) {
                        $payment_intent = $this->stripeController->initializePaymentIntents($User, "selection_trial_plan");
                        $generate_payment_intent = $payment_intent["generate_payment_intent"];
                        $checkout_details = $payment_intent["plandetails"];
                    } else {
                        $payment_intent = "";
                        $generate_payment_intent = "";
                        $checkout_details = "";
                    }

                    if (count($User["default_card"]) > 0) {
                        $default_card = $User["default_card"][0];
                    } else {
                        $default_card = "";
                    }

                    // Post-login payment page

                    $view = "plans.trial.trial-plan-checkout-desktop";
                    if ($this->agent->isMobile()) {
                        $view = "plans.trial.trial-plan-checkout-mobile";
                    }

                    return view($view)
                    ->with('maskselection',$maskselection)
                    ->with('countmaskqty',$countmaskqty)
                        ->with('loginStatus', 'post-login')
                        ->with('currentCountryIso', $currentCountryIso)
                        ->with('currentCountryid', $currentCountryid)
                        ->with('currency', $this->currency)
                        ->with('langCode', $langCode)
                        ->with('urllangCode', $urllangCode)
                        ->with('states', $states)
                        ->with('User', $User)
                        ->with('session_data', json_encode($session_data_decoded))
                        ->with('payment_intent', $generate_payment_intent)
                        ->with('checkout_details', $checkout_details)
                        ->with('plandescription', $plandescription)
                        ->with('current_price', $current_price)
                        ->with('shipping_fee', $shipping_fee)
                        ->with('next_price', $next_price)
                        ->with('current_totalprice', $current_totalprice)
                        ->with('next_totalprice', $next_totalprice)
                        ->with('taxAmount', $taxAmount)
                        ->with('taxRate', $tax->taxRate)
                        ->with('nextBillingDate', $this->getNextRefillDateCheckout())
                        ->with('planFrequency', $session_data_decoded->selection->step3->selected_frequency)
                        ->with('subscriptionName', $subscriptionName)
                        ->with('currentShippingImage', $currentShippingImage)
                        ->with('nextBillingImage', $nextBillingImage);

                } else {
                    if ($currentCountryid != 8) {
                        $payment_intent = $this->stripeController->initializePaymentIntents(null, "selection_trial_plan");
                        $generate_payment_intent = $payment_intent["generate_payment_intent"];
                        $checkout_details = $payment_intent["plandetails"];
                    } else {
                        $payment_intent = "";
                        $generate_payment_intent = "";
                        $checkout_details = "";
                    }

                    $view = "plans.trial.trial-plan-checkout-desktop";
                    if ($this->agent->isMobile()) {
                        $view = "plans.trial.trial-plan-checkout-mobile";
                    }

                    // Pre-login payment page
                    return view($view)
                    ->with('maskselection',$maskselection)
                    ->with('countmaskqty',$countmaskqty)
                        ->with('loginStatus', 'pre-login')
                        ->with('currentCountryIso', $currentCountryIso)
                        ->with('currentCountryid', $currentCountryid)
                        ->with('currency', $this->currency)
                        ->with('langCode', $langCode)
                        ->with('urllangCode', $urllangCode)
                        ->with('states', $states)
                        ->with('session_data', json_encode($session_data_decoded))
                        ->with('payment_intent', $generate_payment_intent)
                        ->with('checkout_details', $checkout_details)
                        ->with('plandescription', $plandescription)
                        ->with('current_price', $current_price)
                        ->with('shipping_fee', $shipping_fee)
                        ->with('next_price', $next_price)
                        ->with('current_totalprice', $current_totalprice)
                        ->with('next_totalprice', $next_totalprice)
                        ->with('taxAmount', $taxAmount)
                        ->with('taxRate', $tax->taxRate)
                        ->with('nextBillingDate', $this->getNextRefillDateCheckout())
                        ->with('planFrequency', $session_data_decoded->selection->step3->selected_frequency)
                        ->with('subscriptionName', $subscriptionName)
                        ->with('currentShippingImage', $currentShippingImage)
                        ->with('nextBillingImage', $nextBillingImage);

                }
            } else {
                return redirect()->route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
            }
        } else {
            return redirect()->route('locale.region.shave-plans.trial-plan.selection', ['langCode' => $urllangCode, 'countryCode' => $currentCountryIso]);
        }
    }

    // Function: Proceed to thank you page
    public function proceedThankYou(Request $request)
    {
        return view('plans.trial.trial-plan-thankyou');
    }

    public function converttrialtoannual(Request $request)
    {
        // $session_data = $this->session->_session("selection_trial_plan", "get", null);
        // $plan_type = "";
        // $plan_id = "";
        // $bladecountryid = "";
        // $selected_frequency = "";
        // if(isset($session_data)){
        //     $session_data = json_decode($session_data);
        //    if (array_key_exists("plan_type", $session_data->selection)) {
        //     $plan_type = $session_data->selection->plan_type;
        //     $plan_id = $session_data->selection->summary->planId;
        //     $bladecountryid  = $session_data->selection->step1->selected_blade->productcountriesid;
        //     }

        // }

        return 0;
    }
}

// FOOTNOTES
/* Expected json format:

1. $this->country_info
{
"country_id": 1,
"country_iso": "my",
"default_lang": "en",
"supported_langs": [],
"data_from_db": {
"id": 1,
"code": "MYS",
"codeIso": "MY",
"name": "Malaysia",
"currencyCode": "MYR",
"currencyDisplay": "RM",
"defaultLang": "EN",
"callingCode": "60",
"isActive": 1,
"companyName": "Shaves2u (M) Sdn. Bhd.",
"companyRegistrationNumber": "(1037174-T)",
"companyAddress": "Level 18, Axiata Tower, No. 9, Jalan Stesen Sentral 5, Kuala Lumpur Sentral, 50470, KL, Malaysia.",
"taxRate": 0,
"taxName": "GST",
"taxCode": "SR",
"includedTaxToProduct": 0,
"taxNumber": "12334",
"email": "help@shaves2u.com",
"phone": "+603 2262 2330",
"isBaEcommerce": 1,
"isBaSubscription": 1,
"isBaAlaCarte": 1,
"isBaStripe": 1,
"isWebEcommerce": 1,
"isWebSubscription": 1,
"isWebAlaCarte": 1,
"isWebStripe": 1,
"shippingMinAmount": "0.00",
"shippingFee": "0.00",
"shippingTrialFee": "0.00",
"order": 1
}
}

2. getAllTrialPlan function:
[
{
"plansku":"PR-2M-TK2",
"planid":1,
"planduration":"2",
"trialPrice":"6.50",
"sellPrice":"26.00",
"isOnline":1,
"isOffline":1,
"productCountryId":[2,147]},...

 */
