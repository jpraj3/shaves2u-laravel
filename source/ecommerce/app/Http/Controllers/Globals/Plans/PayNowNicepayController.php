<?php

namespace App\Http\Controllers\Globals\Plans;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Queue\SaleReportQueueService;
use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\Payment\NicePayHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\TaxInvoiceHelper;
use App\Helpers\UserHelper;
use App\Http\Controllers\Ecommerce\Rebates\Referral\ReferralController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanSKU;
use App\Models\Rebates\Referral;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Rebates\RewardsIn;
use App\Models\Receipts\Receipts;
use App\Models\Reports\SalesReport;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use App\Queue\SendEmailQueueService as EmailQueueService;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Services\CountryService as CountryService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use App\Helpers\Email\EmailQueueHelper as EmailQueueHelper;
use App\Models\Orders\PaymentHistories;
use App\Queue\TaxInvoiceQueueService;
use App\Http\Controllers\Globals\Utilities\SessionController;

class PayNowNicepayController extends Controller
{
    // Constructor
    public function __construct()
    {
        $this->userHelper = new UserHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->planHelper = new PlanHelper();
        $this->country_service = new CountryService();
        $this->productHelper = new ProductHelper();
        $this->taxInvoiceHelper = new TaxInvoiceHelper();
        $this->orderHelper = new OrderHelper();
        $this->referralController = new ReferralController();
        $this->nicepayController = new NicePayController();
        $this->nicepay = new NicePayHelper();
        $this->emailQueueHelper = new EmailQueueHelper();
        $this->session = new SessionController();
    }

    // Function: Main Process
    public function MainProcess(Request $request)
    {
        return $this->GetSubscriptions($request->subscription_id);
    }

    // Function: Get subscriptions
    public function GetSubscriptions($subscription_id)
    {
        try {

            // Get subscription based on several filters
            $subscriptions = Subscriptions::leftJoin('promotions', 'promotions.id', 'subscriptions.promotionId')
                ->select('subscriptions.*', 'promotions.*', 'subscriptions.id as subscriptionsid', 'promotions.id as promotionsid')
                ->where('subscriptions.id', $subscription_id)
                ->whereIn('status', ['Processing', 'On Hold', 'Unrealized'])
                ->where('CardId', '!=', null)
                ->get();

            // Build all required information
            $subs = $subscriptions[0];
            $subs->user = (object)array(User::where('id', $subs->UserId)->first())["0"];
            $subs->user->fullname = $subs->user->firstName;
            $subs->user->email = $subs->user->email;
            $subs->card = (object)array(Cards::where('id', $subs->CardId)->first())["0"];
            $subs->deliveryAddress = (object)array(DeliveryAddresses::where('id', $subs->DeliveryAddressId)->first())["0"];
            $subs->billingAddress = (object)array(DeliveryAddresses::where('id', $subs->BillingAddressId)->first())["0"];
            $subs->plan = $this->planHelper->getOnePlanData($subs->PlanId);
            $subs->plan_sku = (object)array(PlanSKU::where('id', $subs->plan->PlanSkuId)->first())["0"];
            $subs->country = (object)array(Countries::where('id', $subs->plan->CountryId)->first())["0"];
            $subs->post_payment = (object)array();
            $subs->isOnline = $subs->isOffline === 1 ? 0 : 1;
            $subs->appType = $subs->isOffline === 1 ? 'baWebsite' : 'ecommerce';
            $subs->tax = (object)array($this->laravelHelper->ConvertArraytoObject($this->country_service->tax($subs->country->id)))["0"];

            // // Set stripe return url
            // $subs->return_url = $return_url;

            // Promotion
            $subs->promotionallow = 0;
            if ($subs->temp_discountPercent != null && $subs->temp_discountPercent > 0) {
                $subs->promotionallow = 1;
            }
            else if ($subs->promotionId) {
                if ($subs->promotionType == 'Upcoming') {
                    $promotionuse = Orders::where('subscriptionIds', $subs->subscriptionsid)
                        ->where('PromotionId', $subs->promotionsid)
                        ->where('PromotionId', '!=', null)
                        ->count();
                    if ($promotionuse == 0) {
                        $subs->promotionallow = 1;
                    }
                } else if ($subs->promotionType == 'Recurring') {
                    $subs->promotionallow = 1;
                }
            }

            // Continue the subscription charge job for these countries
            if (in_array($subs->country->id, [8])) {
                return $this->CheckPlanPrice($subs);
            } else {
                return "country_not_allowed";
            }

        } catch (Exception $e) {
            return $this->onPayNowError('Get subscription error: ' . $e->getMessage());
        }
    }

    // Function: Check plan price
    public function CheckPlanPrice($subs)
    {
        try {
            //Initialize parameters to get product country details
            $checkproduct = array(
                "appType" => 'ecommerce',
                "isOnline" => $subs->isOnline,
                "isOffline" => $subs->isOffline,
                "product_country_ids" => $this->planHelper->getAllAvailableProductsByCycle($subs->subscriptionsid, $subs->currentCycle + 1, $subs->country->id, $subs->user->defaultLanguage),
            );
            // Get all order details
            $checkorderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $checkproduct));

            $discountforfreeexistproduct = 0;
            $promofreeexistresultproduct = [];
            // Promotion free existing product
            if ($subs->promotionallow == 1) {
                $productseperate = [];
                if ($subs->freeExistProductCountryIds) {
                    $subs->freeExistProductCountryIds = str_replace("[", "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace("]", "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace('"', "", $subs->freeExistProductCountryIds);
                    $subs->freeExistProductCountryIds = str_replace('"', "", $subs->freeExistProductCountryIds);

                    $splitproduct = $subs->freeExistProductCountryIds;
                    if (strpos($splitproduct, ',') !== false) {
                        $splitproduct = explode(",", $subs->freeExistProductCountryIds);
                    }
                    if (is_array($splitproduct)) {
                        foreach ($splitproduct as $product) {
                            array_push($productseperate, $product);
                        }
                    } else {
                        array_push($productseperate, $splitproduct);
                    }
                }
                // Create order details
                foreach ($checkorderDetailList as $product) {

                    if (in_array($product->ProductCountryId, $productseperate)) {
                        array_push($promofreeexistresultproduct, $product->ProductCountryId);
                        $discountforfreeexistproduct = $discountforfreeexistproduct + $product->sellPrice;
                    }
                }
            }
            $subs->promo_freeexistresultproduct = [];
            if ($promofreeexistresultproduct) {
                $subs->promo_freeexistresultproduct = $promofreeexistresultproduct;
            }
            // Get price from subscription
            $subscriptionPrice = $subs->pricePerCharge;

            // Initialize variables
            $totalprice = 0;
            $totaldiscount = 0;
            $totaldiscountprice = 0;

            // Get shipping fee
            $getshippingfee = $this->country_service->shippingFee($subs->country->id, $subs->country->lang_code);
            $shippingfee = "";
            if ($getshippingfee) {
                $shippingfee = (float)$getshippingfee["shippingfee"];
            }
            $totalprice = $totalprice + floatval($subscriptionPrice);
     
            // Promotion free existing product
            if ($subs->promotionallow == 1) {
                if ($discountforfreeexistproduct > 0) {
                    $totalprice = $totalprice - $discountforfreeexistproduct;
                }

            }
  
            // Promotion free shippingfee
            if ($subs->promotionallow == 1) {
                if ($subs->isFreeShipping) {
                    if ($subs->isFreeShipping === 1) {
                        $shippingfee = 0;
                    }
                }
                // Promotion
                if ($subs->temp_discountPercent != null && $subs->temp_discountPercent > 0) {
                    $totaldiscount = (($totalprice * $subs->temp_discountPercent) / 100);
                }
                else {
                    $totaldiscount = (($totalprice * $subs->discount) / 100);
                }
                $totaldiscountprice = $totalprice - $totaldiscount;

                $totaldiscountprice = $totaldiscountprice + $shippingfee;
                $taxAmount = $subs->tax->taxRate / 100 * $totaldiscountprice;
                $totaldiscountprice = $totaldiscountprice + 0.00;

                $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');

                // Promotion maxdiscount
                if ($subs->maxDiscount) {
                    if ($subs->maxDiscount < $totaldiscount) {
                        $totaldiscount = $subs->maxDiscount;
                        $totaldiscountprice = $totalprice - $subs->maxDiscount;
                        $totaldiscountprice = $totaldiscountprice + $shippingfee;
                        $taxAmount = $subs->tax->taxRate / 100 * ($totaldiscountprice);
                        $totaldiscountprice = $totaldiscountprice + 0.00;
                        $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                    }
                }
                // Promotion free exist product
                if ($discountforfreeexistproduct > 0) {
                    $totaldiscount = $totaldiscount + $discountforfreeexistproduct;
                }

                // Promotion no negative number
                if ($totaldiscountprice <= 0) {
                    $totaldiscountprice = 0;
                    $totaldiscountprice = number_format($totaldiscountprice, 2, '.', '');
                }
            }
            // Calculate tax amount and total price
            $totalprice = $totalprice + $shippingfee;
   
            $taxAmount = $subs->tax->taxRate / 100 * ($totalprice);
            $totalprice = $totalprice + 0.00;

            // $totalprice = number_format($totalprice, 2);
            // Insert into priceData variable for later access
            $subs->priceData = (object)array();
            $subs->priceData->totalprice = $totalprice;
            $subs->priceData->subtotal = $subscriptionPrice;
            $subs->priceData->totaldiscount = $totaldiscount;
            $subs->priceData->totaldiscountprice = $totaldiscountprice;
            $subs->priceData->shippingfee = $shippingfee;
            $subs->priceData->taxAmount = $taxAmount;
            return $this->PerformCharge($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Check plan price error: ' . $e->getMessage());
        }
    }

    // Function: Perform charge
    public function PerformCharge($subs)
    {
        try {
            // Initialize data for stripe
            $customer_id = $subs->card->customerId;
            $currency = $subs->country->currencyCode;

            if ($subs->promotionallow == 1) {
                // Promotion
                $amount = $subs->priceData->totaldiscountprice;
            } else {
                $amount = $subs->priceData->totalprice;
            }

            if ($amount != 0) {
                // Perform normal charge if amount is more than 0
                $charge = (object) array();
                try {
               
                    $paymentresult = $this->nicepayController->nicepayPayment($subs->user->fullname, $subs->user->email, $customer_id, $amount, $subs->subscriptionsid);

                    $charge->status =  $paymentresult["status"];
    
                    $subs->isfree = 0;
                        $subs->getnicepay_imp = $paymentresult["data"]["response"]["imp_uid"];
                        $subs->getnicepay_mid = $paymentresult["data"]["response"]["merchant_uid"];
                        $subs->branchName = $paymentresult["data"]["response"]["card_name"];
                        $subs->chargecurreny = "krw";
                        $charge->message = $paymentresult["data"]["message"];
                        if(empty($charge->message)){
                        $charge->message = $paymentresult["data"]["response"]["fail_reason"];
                        }
                } catch (Exception $ex) {
                    $charge = (object)array();
                    $charge->status = "payment_failed";
                    $charge->message = $ex->getMessage();
                    $charge->id = null;
                    $charge->currency = null;
                }
            } else {
                // If amount is 0, skip the stripe and continue with status 'free'
                $charge = (object)array();
                $charge->status = "free";
                $charge->id = null;
                $charge->currency = null;
                $subs->isfree = 1;
                $subs->getnicepay_imp = "zero price";
                $subs->getnicepay_mid = "zero price";
                $subs->branchName = "-";
                $subs->chargecurreny = "krw";
            }

            // Insert charge object into a variable
            $subs->charge = $charge;

            return $this->UpdateSubscription($subs);

        } catch (Exception $e) {

            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Perform charge error: ' . $e->getMessage());
        }
    }

    // Function: Update subscription
    public function UpdateSubscription($subs)
    {
        try {
            // Get payment status and charge duration
            $payment_status = $subs->charge->status;
            $charge_duration = $subs->plan_sku->isAnnual == 1 ? 12 : $subs->plan_sku->duration;

            // If payment successful or no need to pay anything, do the following
            if ($payment_status == 'success' || $payment_status == 'free') {
                // Initialize data
                $next_charge_date = Carbon::now()->addMonths($charge_duration);
                $current_charge_number = $subs->currentChargeNumber + 1;
                $current_cycle = $subs->currentCycle + 1;
                $recharge_date = null;
                $total_recharge = 0;
                $unrealized_customer = 0;
                $has_recharge_flag = null;
                $temp_discount_percent = 0;


                $update_subs_discountPercent = $subs->discountPercent;
                $update_subs_promoCode = $subs->promoCode;
                $update_subs_promotionId = $subs->promotionId;

                if ($subs->promotionType == 'Upcoming') {
                    $update_subs_discountPercent = 0;
                    $update_subs_promoCode = null;
                    $update_subs_promotionId = null;
                }

                // Update pricePerCharge for Subscriptions for next billing
                $current_charge = $subs->pricePerCharge;
                $next_billing_charge = 0;
                $next_billing_addons_total_price = 0;
                $current_addons_total_price = 0;

                if ($subs->plan_sku->isAnnual == 1) {
                    $next_billing_charge = $current_charge;
                } else {
                    // Calculate Addons Total for next billing
                    try {
                        $current_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                            ->where('subscriptionId', $subs->subscriptionsid)
                            ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle);
                            })
                            ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
                            ->sum('productcountries.sellPrice');

                        $next_billing_addons_total_price = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                            ->where('subscriptionId', $subs->subscriptionsid)
                            ->where(function ($checkCurrentCycleProducts) use ($current_cycle) {
                                $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $current_cycle + 1);
                            })
                            ->select('subscriptions_productaddon.*', 'productcountries.sellPrice')
                            ->sum('productcountries.sellPrice');


                        if ($next_billing_addons_total_price == 0 && $current_addons_total_price != 0) {
                            $next_billing_charge = (int) $current_charge - (int) $current_addons_total_price;
                        } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price == 0) {
                            $next_billing_charge = (int) $current_charge + (int) $next_billing_addons_total_price;
                        } else if ($next_billing_addons_total_price != 0 && $current_addons_total_price != 0) {
                            $next_billing_charge = ((int) $current_charge - (int) $current_addons_total_price) + ((int) $next_billing_addons_total_price);
                        } else if ($next_billing_addons_total_price == 0 && $current_addons_total_price == 0) {
                            $next_billing_charge = (int) $subs->pricePerCharge;
                        } else {
                            $next_billing_charge = (int) $subs->pricePerCharge;
                        }
                    } catch (Exception $ex) {
                        $this->Log->info($this->Job . 'Update subscription pricePerCharge failed: ' . $ex->getMessage());
                    }
                }

                // Update subscription status to processing
                Subscriptions::where('id', $subs->subscriptionsid)
                    ->update([
                        'pricePerCharge' => (int) $next_billing_charge,
                        'price' => (int) $next_billing_charge,
                        'nextChargeDate' => $next_charge_date,
                        'currentCycle' => $current_cycle,
                        'currentChargeNumber' => $current_charge_number,
                        'recharge_date' => $recharge_date,
                        'total_recharge' => $total_recharge,
                        'unrealizedCust' => $unrealized_customer,
                        'hasRechargeFlag' => $has_recharge_flag,
                        'temp_discountPercent' => $temp_discount_percent,
                        'discountPercent' => $update_subs_discountPercent,
                        'promoCode' => $update_subs_promoCode,
                        'promotionId' => $update_subs_promotionId,
                        'status' => "Processing",
                        'convertTrial2Subs' => $subs->isTrial === 1 && $subs->currentCycle === 1 ? $this->GetCurrentDate() : $subs->convertTrial2Subs,
                        'UnrealizedDate' => null,
                        'OnHoldDate' => null
                    ]);

                    if ((int) $current_charge != (int) $next_billing_charge) {
                        $subshistory = new SubscriptionHistories();
                        $subshistory->message = 'Next Billing Update';
                        $subshistory->detail = $subs->country->currencyDisplay . number_format($current_charge, 2, '.', '') . ' to ' . $subs->country->currencyDisplay . number_format($next_billing_charge, 2, '.', '');
                        $subshistory->subscriptionId = $subs->subscriptionsid;
                        $subshistory->save();
                    }
    

                // Send Subscription Renewal EDM only for Trial Plan first billing
                if ($subs->isTrial === 1 && $subs->currentCycle === 1) {
                    $this->SendSubscriptionRenewalEDM($subs);
                }

            }
             else {
                // If payment failed, update subscription status to onhold
                Subscriptions::where('id', $subs->subscriptionsid)
                    ->update([
                        'status' => "On Hold",
                        'recharge_date' => Carbon::now()->addDays(7)->toDateString(),
                        'total_recharge' => 1,
                        'hasRechargeFlag' => 1,
                        'unrealizedCust' => 0,
                        'hasFlag' => 1,
                        'OnHoldDate' => Carbon::now()
                    ]);
            }

            // Save latest subscription date into post payment object
            $subs->post_payment->subscription = (object)array(Subscriptions::where('id', $subs->subscriptionsid)->first())["0"];

            return $this->CreateSubscriptionHistory($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Update subscription error: ' . $e->getMessage());
        }
    }

    // Function: Create subscription history
    public function CreateSubscriptionHistory($subs)
    {
        try {
            // Create subscription history
            $subshistory = new SubscriptionHistories();
            $subshistory->message = $subs->post_payment->subscription->status;
            $subshistory->subscriptionId = $subs->subscriptionsid;
            $subshistory->save();

            // If subscription status is processing, continue to create order
            // Else, continue to send payment failure edm
            if ($subs->post_payment->subscription->status == "Processing") {
                return $this->CreateOrder($subs);
            } else {
                return $this->SendPaymentFailureEDM($subs);
            }
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create subscription history error: ' . $e->getMessage());
        }
    }

    // Function: Create order
    public function CreateOrder($subs)
    {
        try {
            
            $data = null;
            $utm_parameters = $this->session->_session("utm_parameters", "get", null);
            if($utm_parameters && !is_array($utm_parameters)){
                // if ($this->checkJsondecode($utm_parameters)) {
                $utm_parameters = json_decode($utm_parameters);
                $data = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
                // }
            }

            // Create order
            $orders = new Orders();
            if ($subs->promotionallow == 1) {
                $orders->promoCode = $subs->promoCode;
                $orders->PromotionId = $subs->promotionId;
            } else {
                $orders->promoCode = null;
                $orders->PromotionId = null;
            }
            $orders->SSN = null;
            $orders->email = $subs->user->email;
            $orders->phone = $subs->user->phone;
            $orders->fullName = $subs->user->fullname;
            $orders->DeliveryAddressId = $subs->deliveryAddress->id;
            $orders->BillingAddressId = $subs->billingAddress->id;
            $orders->taxRate = $subs->tax->taxRate;
            $orders->taxName = $subs->tax->taxName;
            $orders->CountryId = $subs->country->id;
            $orders->subscriptionIds = $subs->subscriptionsid;
            $orders->source = $data ? (isset($data->utm_source) ? $data->utm_source : null) : null;
            $orders->medium = $data ? (isset($data->utm_medium) ? $data->utm_medium : null) : null;
            $orders->campaign = $data ? (isset($data->utm_campaign) ? $data->utm_campaign : null) : null;
            $orders->term = $data ? (isset($data->utm_term) ? $data->utm_term : null) : null;
            $orders->content = $data ? (isset($data->utm_content) ? $data->utm_content : null) : null;
            $orders->status = "Payment Received";
            $orders->payment_status = "Charged";
            $orders->paymentType = "nicepay";
            $orders->imp_uid = $subs->getnicepay_imp;
            $orders->merchant_uid = $subs->getnicepay_mid;
            if ($subs->country->id == "7") {
                $orders->carrierAgent = "courex";
            } else {
                $orders->carrierAgent = null;
            }
            $orders->isSubsequentOrder = 1;

            $orders->UserId = $subs->user->id;

            if (isset($subs->SellerUserId) && $subs->SellerUserId !== null) {
                $orders->SellerUserId = $subs->SellerUserId;
                $orders->channelType = $subs->channelType;
                $orders->eventLocationCode = $subs->eventLocationCode;
                $orders->MarketingOfficeId = $subs->MarketingOfficeId;
                // $seller_data = SellerUser::where($subs->SellerUserId)->first();
                // if ($seller_data) {
                //     $mo_data = MarketingOffice::where($seller_data->MarketingOfficeId)->first();
                //     if ($mo_data) {
                //         $orders->MarketingOfficeId = $mo_data->id;
                //     }
                // }
            } else {
                $orders->SellerUserId = null;
                $orders->channelType = null;
                $orders->eventLocationCode = null;
                $orders->MarketingOfficeId = null;
            }

            // $orders->pymt_intent = $subs->charge->id;
            $orders->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->order = $orders;
            $paymentHistory = new PaymentHistories();
            $paymentHistory->OrderId = $subs->post_payment->order->id;
            $paymentHistory->message = 'Charged';
            $paymentHistory->isRemark = 0;
            $paymentHistory->created_by = null;
            $paymentHistory->updated_by = null;
            $paymentHistory->save();
            // Add order to queue service
            $_redis_order_data = (object)array(
                'OrderId' => $subs->post_payment->order->id,
                'CountryId' => $subs->country->id,
                'CountryCode' => strtolower($subs->country->codeIso),
            );
            $this->orderHelper->addTaskToOrderQueueService($_redis_order_data);

            $charge_duration = $subs->plan_sku->isAnnual == 1 ? 12 : $subs->plan_sku->duration;

            $nextDeliverDate = Carbon::now()->addMonths($charge_duration);
            $current_deliver_number = $subs->currentDeliverNumber + 1;
            // Update current order time and id in subscription
            Subscriptions::where('id', $subs->subscriptionsid)
                ->update([
                    'currentDeliverNumber' => $current_deliver_number,
                    'nextDeliverDate' => $nextDeliverDate,
                    'currentOrderTime' => Carbon::now(),
                    'currentOrderId' => $subs->post_payment->order->id,
                ]);
                $this->session->_session("utm_parameters", "clear", null);
            return $this->CreateOrderHistory($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order error: ' . $e->getMessage());
        }
    }

    // public function checkJsondecode($data){
    //     if (!empty($data)) {
    //         json_decode($data);
    //         return (json_last_error() === JSON_ERROR_NONE);
    // }
    // return false;  
    // }

    // Function: Create order history
    public function CreateOrderHistory($subs)
    {
        try {
            // Create order history
            $ordershistories = new OrderHistories();
            $ordershistories->message = 'Payment Received';
            $ordershistories->OrderId = $subs->post_payment->order->id;
            $ordershistories->save();

            return $this->CreateOrderDetails($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order history error: ' . $e->getMessage());
        }
    }

    // Function: Create order details
    public function CreateOrderDetails($subs)
    {
        try {
            //Initialize parameters to get product country details
            $subs->productCountryDetailsParams = array(
                "appType" => 'ecommerce',
                "isOnline" => $subs->isOnline,
                "isOffline" => $subs->isOffline,
                "product_country_ids" => $this->planHelper->getAllAvailableProductsByCycle($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage),
            );

            // Get all order details
            $orderDetailList = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $subs->productCountryDetailsParams));

            // Get product country ids for addons
            $addonsProductCountryIds = $this->planHelper->getAvailableAddonProductsByCycle($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage);
            $productQuantities = $this->planHelper->getAllAvailableProductsByCycleV2($subs->subscriptionsid, $subs->post_payment->subscription->currentCycle, $subs->country->id, $subs->user->defaultLanguage);

            // Create order details
            foreach ($orderDetailList as $product) {
                $orderdetails = new OrderDetails();
                $orderdetails->qty = $productQuantities[$product->ProductCountryId];
                $orderdetails->price = $product->sellPrice;
                $orderdetails->currency = $subs->country->currencyDisplay;
                $orderdetails->startDeliverDate = null;
                $orderdetails->created_at = Carbon::now();
                $orderdetails->updated_at = Carbon::now();
                $orderdetails->OrderId = $subs->post_payment->order->id;
                $orderdetails->ProductCountryId = $product->ProductCountryId;
                $orderdetails->isAddon = in_array($product->ProductCountryId, $addonsProductCountryIds) ? 1 : 0;
                // Promotion free exist product
                if ($subs->promotionallow == 1) {
                    if ($subs->promo_freeexistresultproduct) {
                        if (in_array($product->ProductCountryId, $subs->promo_freeexistresultproduct)) {
                            $orderdetails->isFreeProduct = 1;
                        }
                    }
                }
                $orderdetails->save();
            }

            // Promotion Free Product
            if ($subs->promotionallow == 1) {
                if ($subs->freeProductCountryIds) {
                    $subs->freeProductCountryIds = str_replace("[", "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace("]", "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace('"', "", $subs->freeProductCountryIds);
                    $subs->freeProductCountryIds = str_replace('"', "", $subs->freeProductCountryIds);

                    $splitproduct = $subs->freeProductCountryIds;
                    if (strpos($splitproduct, ',') !== false) {
                        $splitproduct = explode(",", $subs->freeProductCountryIds);
                    }
                    $productseperate = [];
                    if (is_array($splitproduct)) {
                        foreach ($splitproduct as $product) {
                            array_push($productseperate, $product);
                        }
                    } else {
                        array_push($productseperate, $splitproduct);
                    }

                    // Prepare parameters for trial plan order details
                    $productcountyidarray = array(
                        "appType" => "job",
                        "isOnline" => 1,
                        "isOffline" => 1,
                        "product_country_ids" => $productseperate,
                    );
                    $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($subs->country->id, $subs->user->defaultLanguage, $productcountyidarray));

                    foreach ($product as $pci) {
                        $orderdetails = new OrderDetails();
                        $orderdetails->qty = 1;
                        $orderdetails->price = $pci->sellPrice;
                        $orderdetails->currency = $subs->country->currencyDisplay;
                        $orderdetails->startDeliverDate = null;
                        $orderdetails->created_at = Carbon::now();
                        $orderdetails->updated_at = Carbon::now();
                        $orderdetails->OrderId = $subs->post_payment->order->id;
                        $orderdetails->ProductCountryId = $pci->ProductCountryId;
                        $orderdetails->isFreeProduct = 1;
                        $orderdetails->isAddon = 0;
                        $orderdetails->save();
                    }
                }
            }

            return $this->UpdateReferral($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create order details error: ' . $e->getMessage());
        }
    }

    // Function: Update Referral (optional)
    public function UpdateReferral($subs)
    {
        try {
            if ($subs && $subs->charge->status === 'success') {
                $referee_info = User::where('id', $subs->user->id)->first();
                if ($referee_info && $referee_info->referralId) {
                    $referrer_info = User::where('id', $referee_info->referralId)->first();

                    // start checking if referrer_id && referee_id exists in referral table
                    $_checkReferralAssociation = Referral::where('referralId', $referrer_info->id)->where('friendId', $referee_info->id)->whereNull('rewardId')->get();
                    if (!empty($_checkReferralAssociation) && count($_checkReferralAssociation) > 0) {
                        Referral::where('referralId', $referrer_info->id)->update(["firstPurchase" => 1, "status" => "Active"]);
                        $rewardscurrency = RewardsCurrency::where('CountryId', $referrer_info->CountryId)->first();
                        if ($rewardscurrency) {
                            $referrer_country = Countries::where('id', $referrer_info->CountryId)->first();
                            $currentOrder = Orders::where('id', $subs->post_payment->order->id)->first();
                            if ($currentOrder) {
                                RewardsIn::create([
                                    "CountryId" => $referrer_info->CountryId,
                                    "UserId" => $referrer_info->id,
                                    "value" => $rewardscurrency->value,
                                    "rewardscurrencyId" => $rewardscurrency->id,
                                    "OrderId" => $currentOrder->id,
                                ]);
                            }

                            //send email to referrer for active credits
                            // $data = (Object)array(
                            //     'referrer' => (Object)array(
                            //         'id' => $referrer_info->id,
                            //         'first_name' => $referrer_info->firstName,
                            //         'last_name' => null,
                            //         'country_id' => $referrer_info->CountryId,
                            //         'amount_credited' => $rewardscurrency->value
                            //     ),
                            //     'referee' => (Object)array(
                            //         'id' => $referee_info->id,
                            //         'first_name' => $referee_info->firstName,
                            //         'last_name' => null,
                            //         'country_id' => $referee_info->CountryId,
                            //     )
                            // );
                            $emailData = [];
                            $emailData['title'] = 'referral-active-credits';

                            $emailData['moduleData'] = array(
                                'email' => $referrer_info->email,
                                'refuserId' =>  $referee_info->id,
                                'countryId' => $referrer_info->CountryId,
                                'isPlan' => 1,
                            );

                            $currentCountry = json_encode($referrer_country) ;
                            $currentLocale = $referrer_info->defaultLanguage;
                            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
                            // hide email redis
                            $this->emailQueueHelper->buildEmailTemplate($emailData['title'], $emailData['moduleData'], $emailData['CountryAndLocaleData']);
                            // $this->referralController->sendReferralActiveCredits($data);
                        }
                    }

                }
            }
            return $this->CreateReceipt($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. UpdateReferral for referrer id: ' . $referrer_info->id . ' => ' . $e->getMessage());
        }
    }

    // Function: Create receipt
    public function CreateReceipt($subs)
    {
        try {
            // Create receipt
            $receipts = new Receipts();
            $receipts->totalPrice = $subs->priceData->totalprice;
            $receipts->subTotalPrice = $subs->priceData->subtotal;
            $receipts->originPrice = $subs->priceData->subtotal;
            $receipts->discountAmount = 0;
            // Apply promotion
            if ($subs->promotionallow == 1) {
                $receipts->totalPrice = $subs->priceData->totaldiscountprice;
                $receipts->subTotalPrice = $subs->priceData->totaldiscountprice;
                $receipts->discountAmount = $subs->priceData->totaldiscount;
            }
            if (isset($subs->plan) && $subs->plan !== null && $subs->plan->discountPercent > 0){
                $receipts->totalPrice = $subs->priceData->totalprice;
                $receipts->originPrice = ($subs->priceData->totalprice) +  $subs->plan->discountAmount;
                if ($subs->promotionallow == 0){
                    $receipts->discountAmount = $receipts->discountAmount + $subs->plan->discountAmount;
                } else {
                    $receipts->discountAmount = $receipts->discountAmount + $subs->plan->discountAmount;
                }
            } 
 
            $receipts->currency = $subs->country->currencyDisplay;
            $receipts->taxAmount = $subs->priceData->taxAmount;
            $receipts->shippingFee = $subs->priceData->shippingfee;
            $receipts->OrderId = $subs->post_payment->order->id;
            $receipts->chargeFee = 0;
            $receipts->chargeCurrency = $subs->chargecurreny;
            $receipts->branchName = $subs->card->branchName;
            $receipts->chargeId = $subs->charge->status != "free" ?  $subs->card->customerId : null;
            $receipts->last4 = $subs->card->cardNumber;
            $receipts->expiredYear = $subs->card->expiredYear;
            $receipts->expiredMonth = $subs->card->expiredMonth;
            $receipts->SubscriptionId = $subs->subscriptionsid;
            $receipts->save();

            // Save latest receipt data into post payment object
            $subs->post_payment->receipt = $receipts;

            // $receiptdata =$subs->post_payment->receipt;
            return $this->SendReceiptEDM($subs);
            // \Log::debug('Subscription Charge Job Ran!');
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create receipt error: ' . $e->getMessage());
        }
    }

    // Function: Send receipt edm
    public function SendReceiptEDM($subs)
    {
        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'receipt-order';
            $emailData['moduleData'] = (Object)array(
                'email' => $subs->email,
                'subscriptionId' => $subs->post_payment->subscription->id,
                'orderId' => $subs->post_payment->order->id,
                'receiptId' => $subs->post_payment->receipt->id,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);

            EmailQueueService::addHash($subs->user->id,'receipt_order_confirmation',$emailData);

            //$this->Log->info($this->Job . 'Dispatched receipt EDM for subscription id ' . $subs->subscriptionsid . '.');

            return $this->CreateTaxInvoice($subs);
        } catch (Exception $e) {
            $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Send receipt edm error: ' . $e->getMessage());
        }
    }

    // Function: Create tax invoice
    public function CreateTaxInvoice($subs)
    {
        try {
            //$this->taxInvoiceHelper->__generateHTML($subs->post_payment->order,$subs->post_payment->receipt,$subs->country);
            return $this->CreateSalesReport($subs);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create tax invoice error: ' . $e->getMessage());
        }
    }

    // Function: Create sales report
    public function CreateSalesReport($subs)
    {
        try {
            $_redis_sales_report_data = [ 'id' =>  $subs->post_payment->order->id, 'bulkOrder' => false];
            SaleReportQueueService::addHash( $subs->post_payment->order->id, $_redis_sales_report_data);
            $_redis_tax_invoice_data = ['id' => $subs->post_payment->order->id, 'CountryId' => $subs->country->id];
            TaxInvoiceQueueService::addHash($subs->post_payment->order->id, $_redis_tax_invoice_data);
            // Create sales report
            // $salesReport = new SalesReport();
            //$salesReport->badgeId = "";
            //$salesReport->agentName = "";
            //$salesReport->channelType = "";
            //$salesReport->eventLocationCode = "";
            //$salesReport->states = "";
            //$salesReport->deliveryId = "";
            //$salesReport->carrierAgent = "";
            //$salesReport->customerName = "";
            // $salesReport->deliveryAddress = $subs->deliveryAddress->state;
            // $salesReport->deliveryContact = $subs->deliveryAddress->contactNumber;
            // $salesReport->billingAddress = $subs->billingAddress->state;
            // $salesReport->billingContact = $subs->billingAddress->contactNumber;
            // $salesReport->email = $subs->user->email;
            // //$salesReport->productCategory = "";
            // $salesReport->sku = $subs->plan_sku->sku;
            // $salesReport->qty = 1;
            // $salesReport->productName = "";
            // $salesReport->currency = $subs->country->currencyCode;
            // $salesReport->paymentType = "card";
            // $salesReport->region = "";
            // //$salesReport->category = "";
            // $salesReport->saleDate = $this->GetCurrentDate();
            // $salesReport->orderId = $subs->post_payment->order->id;
            //$salesReport->bulkOrderId = "";
            //$salesReport->promoCode = "";
            // Apply promotion
            // if ($subs->promotionallow == 1) {
            //     $salesReport->totalPrice = number_format($subs->priceData->totaldiscountprice, 2, '.', '');
            //     $salesReport->subTotalPrice = number_format($subs->priceData->subtotal, 2, '.', '');
            //     $salesReport->discountAmount = number_format($subs->priceData->totaldiscount, 2, '.', '');
            // } else {
            //     $salesReport->totalPrice = number_format($subs->priceData->totalprice, 2, '.', '');
            //     $salesReport->subTotalPrice = number_format($subs->priceData->subtotal, 2, '.', '');
            //     $salesReport->discountAmount = 0;
            // }
            // $salesReport->shippingFee = $subs->priceData->shippingfee;
            // $salesReport->taxAmount = $subs->priceData->taxAmount;
            //$salesReport->grandTotalPrice = "";
            //$salesReport->source = "";
            // $salesReport->medium = "";
            // $salesReport->campaign = "";
            // $salesReport->term = "";
            // $salesReport->content = "";
            // $salesReport->CountryId = $subs->country->id;
            //$salesReport->taxInvoiceNo = "";
            //$salesReport->MarketingOfficeId = "";
            //$salesReport->moCode = "";
            //$salesReport->taxRate = "";
            //$salesReport->isDirectTrial = "";
            // $salesReport->created_at = $this->GetCurrentDateTime();
            // $salesReport->updated_at = $this->GetCurrentDateTime();
            // $salesReport->save();
            //$this->Log->info($salesReport);

            $returnResponse = array();
            $returnResponse["status"] = "success";
            $returnResponse["orderId"] = $subs->post_payment->order->id;
            return $returnResponse;
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Create sales report error: ' . $e->getMessage());
        }
    }

    // Function: Send payment failure edm
    public function SendPaymentFailureEDM($subs)
    {
        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'receipt-payment-failure-subscription';
            $emailData['moduleData'] = (Object)array(
                'email' => $subs->email,
                'subscriptionId' => $subs->subscriptionsid,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            EmailQueueService::addHash($subs->user->id, 'receipt_subscription_payment_failure', $emailData);

            $returnResponse = array();
            $returnResponse["status"] = "failure";
            $returnResponse["message"] =  $subs->charge->message;
            return $returnResponse;
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Send payment failure edm error: ' . $e->getMessage());
        }
    }

    // Function: Send subscription renewal EDM
    public function SendSubscriptionRenewalEDM($subs)
    {

        //$this->Log->info($this->Job . 'Send subscription renewal EDM for subscription id ' . $subs->subscriptionsid . '.');

        try {
            // Getting Country & Locale Data From $subs
            $currentCountry = $subs->country;
            $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->user->id, $subs->PlanId);

            $emailData = [];
            // Passing $subs Data into $moduleData for Email
            $emailData['title'] = 'subscription-renewal';
            $emailData['moduleData'] = (Object)array(
                'email' => $subs->email,
                'subscriptionId' => $subs->subscriptionsid,
                'userId' => $subs->user->id,
                'countryId' => $subs->country->id,
                'type' => 1,
                'isPlan' => 1,
                'isMale' => 1,
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry, $currentLocale);
            EmailQueueService::addHash($subs->user->id, 'subscription_renewal', $emailData);
        } catch (Exception $e) {
            return $this->onPayNowError('SubscriptionId: ' . $subs->subscriptionsid . '. Send subscription renewal edm error: ' . $e->getMessage());
        }
    }

    /*** Additional helper functions ***/
    public function GetCurrentDateTime()
    {
        return Carbon::now();
    }

    public function GetCurrentDate()
    {
        return Carbon::now()->toDateString();
    }

    public function onPayNowError($message)
    {
        $returnResponse = array();
        $returnResponse["status"] = "failure";
        $returnResponse["message"] = $message;
        return $returnResponse;
    }
    /*** Additional helper functions ***/

}
