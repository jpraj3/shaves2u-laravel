<?php

namespace App\Http\Controllers\Errors;

use App;
use App\Http\Controllers\Controller as Controller;
use App\Services\CountryService as CountryService;
// Models
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

// Helpers

class PageExistsController extends Controller
{
    public $productHelper;
    public $planHelper;
    public $country_service;
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->country_info = $this->country_service->getCountryAndLangCode();
    }

    public function pageExists(Request $request, $route_name)
    {
        $_prefix = Route::current()->action["prefix"];
        $actual_link = parse_url((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}", PHP_URL_PATH);
        if (isset($_prefix)) {
            try {
                $routes = Route::getRoutes();
                $request = Request::create('/{langCode}-{countryCode}/' . $route_name);
                $_check = $routes->match($request);
                if (explode('/', $_check->uri)[1] === "{route_name}") {
                    return redirect('/');
                } else {
                    // check if url_params exists
                    if (count(Input::all()) > 0) {
                        $append_params = '';
                        $url_params = Input::all();
                        $_count = 0;
                        foreach ($url_params as $key => $param) {
                            if ($_count === 0) {
                                $append_params = $append_params . '?' . $key . '=' . $param;
                            } else {
                                $append_params = $append_params . '&' . $key . '=' . $param;
                            }
                            $_count++;
                        }

                        return redirect('/' . $this->lang_code . '-' . $this->country_info["country_iso"] . '/' . $route_name . $append_params);
                    } else {
                        return redirect('/' . $this->lang_code . '-' . $this->country_info["country_iso"] . '/' . $route_name);
                    }
                }

            } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
                // route doesn't exist
            }
        } else {
            try {
                $routes = Route::getRoutes();
                $request = Request::create('/{langCode}-{countryCode}/' . $route_name);
                $_check = $routes->match($request);
                if (explode('/', $_check->uri)[1] === "{route_name}") {
                    return redirect('/');
                } else {
                    // check if url_params exists
                    if (count(Input::all()) > 0) {
                        $append_params = '';
                        $url_params = Input::all();
                        $_count = 0;
                        foreach ($url_params as $key => $param) {
                            if ($_count === 0) {
                                $append_params = $append_params . '?' . $key . '=' . $param;
                            } else {
                                $append_params = $append_params . '&' . $key . '=' . $param;
                            }
                            $_count++;
                        }

                        return redirect('/' . $this->lang_code . '-' . $this->country_info["country_iso"] . '/' . $route_name . $append_params);
                    } else {
                        return redirect('/' . $this->lang_code . '-' . $this->country_info["country_iso"] . '/' . $route_name);
                    }
                }

            } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $e) {
                // route doesn't exist
            }
        }

    }
}
