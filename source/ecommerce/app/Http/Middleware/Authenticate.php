<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
            return route('login', ['langCode' => $urllangCode, 'countryCode' => strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]);
        } else {
            $urllangCode = strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']);
            return redirect()->intended('/' .  $urllangCode . '-' . strtolower(json_decode(session()->get('currentCountry'), true)['codeIso']) . '/user/dashboard');
        }
    }
}
