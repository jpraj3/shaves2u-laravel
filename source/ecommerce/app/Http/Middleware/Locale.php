<?php

namespace App\Http\Middleware;

use App;
use App\Models\Ecommerce\MetaTags;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\LanguageDetails;
use App\Models\Products\Product;
use App\Services\CountryService;
use Closure;
use Config;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Input;
use OpenGraph;
use Request;
use SEOMeta;

class Locale
{

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
        $this->country_service = new CountryService();
        // \App\Helpers\URLParamsHelper::saveURLParamsToSession();
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = explode("/", $_SERVER['REQUEST_URI']);
        $this->checkUTMSources($_SERVER['REQUEST_URI']);

        $spliturl = '';
        $urllang = 'en';
        $urlcountry = 'my';
        $check = 'true';
        $countryget = '';
        if ($url) {

            if (count($url) >= 2) {
                if ($_SERVER["SERVER_NAME"] === 'dcistaging.com') {
                    $spliturl = $url[3];
                } else {

                    $spliturl = $url[1];
                }

                if ($spliturl) {
                    if (strpos($spliturl, '-') !== false) {
                        $splitLangAndCountry = explode("-", $spliturl);
                        $urllang = strtolower($splitLangAndCountry[0]);
                        $urlcountry = strtolower($splitLangAndCountry[1]);
                    } else {
                        // return redirect('/');
                    }
                }
            }
        }

        // Check if current country session Exist
        $countrySession = $request->session()->get('currentCountry');

        // If Country session data does not exist
        if (!isset($countrySession)) {
            // Getting Client Public IP
            $clientIp = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : null);

            // for development purposes, get public ip address from external source
            if (App::environment(['local'])) {

                $clientIp = file_get_contents("https://ipecho.net/plain");
            }
            if ($urlcountry) {
                $languageDetailsData = '';
                $countryDataUrl = Countries::leftJoin('supportedlanguages', 'supportedlanguages.CountryId', 'countries.id')
                    ->where(function ($query) use ($urllang) {
                        $query->orWhereRaw('LOWER(countries.defaultLang) = ?', $urllang)
                            ->orWhereRaw('LOWER(supportedlanguages.languageCode) = ?', $urllang);
                    })
                    ->whereRaw('LOWER(countries.codeIso) = ?', $urlcountry)
                    ->where('countries.isActive', 1)
                    ->select('countries.id as countryid', 'countries.*', 'supportedlanguages.*')
                    ->first();
                if ($countryDataUrl) {
                    if ($countryDataUrl->languageCode) {
                        if (strtolower($countryDataUrl->languageCode) == $urllang) {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->languageCode)
                                ->first();
                        } else {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                                ->first();
                        }
                    } else {
                        $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                            ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                            ->first();
                    }

                    if ($languageDetailsData) {
                        $sessiondata = [
                            'id' => $countryDataUrl->countryid,
                            'code' => $countryDataUrl->code,
                            'codeIso' => $countryDataUrl->codeIso,
                            'defaultLang' => $languageDetailsData->sublanguageCode,
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                        $clientCountryJson = json_encode($sessiondata);
                        $clientCountryArray = json_decode($clientCountryJson, true);
                        $countryLang = $languageDetailsData->sublanguageCode;
                    }
                }

                if (!$countryDataUrl || !$languageDetailsData) {
                    // Curl Init
                    $api_key = trim(json_encode(config('environment.IpStack.accesskey')), '"');
                    $api_url = trim(json_encode(config('environment.IpStack.url')), '"');
                    $api_url = str_replace("\\", "", $api_url);
                    $ip_stack_request = curl_init($api_url . $clientIp . '?access_key=' . $api_key);
                    // $ip_stack_request = curl_init('http://api.ipstack.com/' . $clientIp . '?access_key=' . '2c878ac336a0a28ab63caed03dc92cc1');

                    curl_setopt($ip_stack_request, CURLOPT_RETURNTRANSFER, true);

                    // Storing Data (in JSON)
                    $ip_stack_response = curl_exec($ip_stack_request);
                    curl_close($ip_stack_request);

                    // Decode JSON Response
                    $ipStackResults = json_decode($ip_stack_response, true);

                    // Get Country Info from Countries Table Based on JSON response (returns stdClass)
                    $clientCountryName = $ipStackResults["country_name"];
                    $clientCountryData = Countries::where('name', $clientCountryName)
                        ->first();
                    if ($clientCountryData) {
                        $countryLang = strtolower($clientCountryData->defaultLang);
                    } else {
                        $countryLang = 'en';
                    }
                    // If Country data available in countries table
                    if (isset($clientCountryData)) {
                        // Convert stdClass from query to JSON (For Session Storage)
                        $languageDetailsData = LanguageDetails::where('CountryId', $clientCountryData->id)
                            ->where('mainlanguageCode', $clientCountryData->defaultLang)
                            ->first();
                        if (isset($languageDetailsData)) {
                            $countryLang = strtolower($languageDetailsData->sublanguageCode);
                        } else {
                            $countryLang = strtolower($clientCountryData->defaultLang);
                        }
                        $sessiondata = [
                            'id' => $clientCountryData->id,
                            'code' => $clientCountryData->code,
                            'codeIso' => $clientCountryData->codeIso,
                            'defaultLang' => $countryLang,
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                        $clientCountryJson = json_encode($sessiondata);
                        // Convert JSON data to array
                        $clientCountryArray = json_decode($clientCountryJson, true);
                        // Then get the country code (for redirect)
                    } else {
                        /*
                         * If Country data not available in countries table,
                         * Empty contry code (Or set default)
                         */
                        // Convert default values from config to JSON
                        $clientCountryJson = json_encode(config('global.default.country'));
                        $clientCountryArray = json_decode($clientCountryJson, true);
                        $countryLang = 'en';
                    }

                    $check = 'false';
                }
            } else {

                // Curl Init
                $api_key = trim(json_encode(config('environment.IpStack.accesskey')), '"');
                $api_url = trim(json_encode(config('environment.IpStack.url')), '"');
                $api_url = str_replace("\\", "", $api_url);
                $ip_stack_request = curl_init($api_url . $clientIp . '?access_key=' . $api_key);
                // $ip_stack_request = curl_init('http://api.ipstack.com/' . $clientIp . '?access_key=' . '2c878ac336a0a28ab63caed03dc92cc1');

                curl_setopt($ip_stack_request, CURLOPT_RETURNTRANSFER, true);

                // Storing Data (in JSON)
                $ip_stack_response = curl_exec($ip_stack_request);
                curl_close($ip_stack_request);

                // Decode JSON Response
                $ipStackResults = json_decode($ip_stack_response, true);

                // Get Country Info from Countries Table Based on JSON response (returns stdClass)
                $clientCountryName = $ipStackResults["country_name"];
                $clientCountryData = Countries::where('name', $clientCountryName)
                    ->first();
                $countryLang = 'en';
                // If Country data available in countries table

                if (isset($clientCountryData)) {
                    // Convert stdClass from query to JSON (For Session Storage)
                    $languageDetailsData = LanguageDetails::where('CountryId', $clientCountryData->id)
                        ->where('mainlanguageCode', $clientCountryData->defaultLang)
                        ->first();

                    if (isset($languageDetailsData)) {
                        $countryLang = strtolower($languageDetailsData->sublanguageCode);

                        $sessiondata = [
                            'id' => $clientCountryData->countryid,
                            'code' => $clientCountryData->code,
                            'codeIso' => $clientCountryData->codeIso,
                            'defaultLang' => $countryLang,
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                        $clientCountryJson = json_encode($sessiondata);
                        // Convert JSON data to array
                        $clientCountryArray = json_decode($clientCountryJson, true);
                    } else {
                        $clientCountryJson = json_encode(config('global.default.country'));
                        $clientCountryArray = json_decode($clientCountryJson, true);
                        $countryLang = 'en';
                        $check = 'false';
                    }
                    // Then get the country code (for redirect)
                } else {
                    /*
                     * If Country data not available in countries table,
                     * Empty contry code (Or set default)
                     */
                    // Convert default values from config to JSON
                    $clientCountryJson = json_encode(config('global.default.country'));
                    $clientCountryArray = json_decode($clientCountryJson, true);
                    $countryLang = 'en';
                    $check = 'false';
                }
            }

            // Store url parameters in session
            // // $this->checkUTMSources();

            // Store Client Country JSON Data to Session Storage
            $request->session()->put('currentCountry', $clientCountryJson);

            // Store Client Locale in Session Storage
            $request->session()->put('currentLocale', $countryLang);

            // Set Client Locale based on $countryLang
            App::setLocale($countryLang);
        } // If Country session data does exist
        else {
            // Pass Session Data into variable
            $clientCountryArray = json_decode(($request->session()->all())['currentCountry'], true);
            $countryLang = strtolower($request->session()->get('currentLocale'));
            $mainlang = strtolower($clientCountryArray["urlLang"]);

            if (($urllang && $urlcountry) && (($mainlang != $urllang) || (strtolower($clientCountryArray["codeIso"]) != $urlcountry))) {

                $countryDataUrl = Countries::leftJoin('supportedlanguages', 'supportedlanguages.CountryId', 'countries.id')
                    ->where(function ($query) use ($urllang) {
                        $query->orWhereRaw('LOWER(countries.defaultLang) = ?', $urllang)
                            ->orWhereRaw('LOWER(supportedlanguages.languageCode) = ?', $urllang);
                    })
                    ->whereRaw('LOWER(countries.codeIso) = ?', $urlcountry)
                    ->where('countries.isActive', 1)
                    ->select('countries.id as countryid', 'countries.*', 'supportedlanguages.*')
                    ->first();

                if ($countryDataUrl) {
                    if ($countryDataUrl->languageCode) {
                        if (strtolower($countryDataUrl->languageCode) == $urllang) {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->languageCode)
                                ->first();
                        } else {
                            $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                                ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                                ->first();
                        }
                    } else {
                        $languageDetailsData = LanguageDetails::where('CountryId', $countryDataUrl->countryid)
                            ->where('mainlanguageCode', $countryDataUrl->defaultLang)
                            ->first();
                    }

                    if ($languageDetailsData) {
                        $sessiondata = [
                            'id' => $countryDataUrl->countryid,
                            'code' => $countryDataUrl->code,
                            'codeIso' => $countryDataUrl->codeIso,
                            'defaultLang' => $languageDetailsData->sublanguageCode,
                            'urlLang' => strtolower($languageDetailsData->mainlanguageCode),
                        ];
                        $urlLang = strtolower($languageDetailsData->mainlanguageCode);
                        // Store url parameters in session
                        // // $this->checkUTMSources();
                        $request->session()->put('currentCountry', json_encode($sessiondata));
                        $request->session()->put('currentLocale', $languageDetailsData->sublanguageCode);
                        $clientCountryArray = json_decode(($request->session()->all())['currentCountry'], true);

                        $countryLang = $languageDetailsData->sublanguageCode;
                    } else {
                        $check = 'false';
                    }
                } else {
                    $check = 'false';
                }
            }
            // Get country Language based on locale
            App::setLocale($countryLang);
        }

        // Get SEO Info for Current Country
        $clientCountrySeoData = json_decode(json_encode(MetaTags::where('CountryId', $clientCountryArray['id'])->first()), true);

        // Remove Default Title from SEO tools (Library Bug workaround)
        config(['seotools.meta.defaults.title' => false]);

        $allcountry = $this->country_service->getAllCountryLang();
        $callcode = $this->country_service->getCountryPhoneExt($clientCountryArray['id']);

        // Set Meta Information
        // SEOMeta::setTitle($clientCountrySeoData['metatitle']);
        // SEOMeta::setDescription($clientCountrySeoData['metaDescription']);

        // // Set og Meta Info
        // OpenGraph::setDescription($clientCountrySeoData['metaDescription']);
        // OpenGraph::setTitle($clientCountrySeoData['ogTitle']);
        // OpenGraph::setUrl($clientCountrySeoData['ogUrl']);
        // OpenGraph::addProperty('type', $clientCountrySeoData['ogType']);
        // OpenGraph::addImage($clientCountrySeoData['ogImage']);
        $this->initSalesNotificationProductList($countryLang);
        view()->share('currentCountryIso', strtolower($clientCountryArray['codeIso']));
        view()->share('url', strtolower($urllang));
        view()->share('langCode', strtolower($countryLang));
        view()->share('allcountry', $allcountry);
        view()->share('callcode', $callcode);

        if ($check == "false") {
            $REQUEST_URI = strtok($_SERVER["REQUEST_URI"],'?');
            $checkServer = isset($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : $REQUEST_URI;
            $checkQuery = isset($_SERVER["QUERY_STRING"]) ? $_SERVER["QUERY_STRING"] : $_SERVER["REDIRECT_QUERY_STRING"];
            if (
                $checkServer == "/shave-plans/trial-plan/selection"
                || $checkServer == "/shaves2u/ecommerce/shave-plans/trial-plan/selection"
            ) {
                return redirect('/' . strtolower($clientCountryArray["urlLang"]) . '-' . strtolower($clientCountryArray["codeIso"]) . '/shave-plans/trial-plan/selection?'.$checkQuery);
            } else {
                return redirect('/');
            }
        }

        // $this->checkUTMSources();
        return $next($request);
    }

    public function checkUTMSources($urlrequest)
    {
        // get utm_medium
        $params = Input::all();
       // $test2 = "https://shaves2u.com/users/reset-password?email=kkshore@hotmail.com&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjIwMTktMDgtMjJUMDk6NDU6MzIuMDU1WiIsImVtYWlsIjoia2tzaG9yZUBob3RtYWlsLmNvbSIsImlhdCI6MTU2NjM4MDczMn0.QFQ2tCKvmv9aQaLrtRYMmV7DyCUGDszo2ak0EPRRFd4&isActive=true&nextPage=/user/subscriptions&utm_source=mandrill&utm_medium=email&utm_campaign=_sgp_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web";
       $session_params = (object) array();
        if (count($params) > 0) {

            $session_count_utm=0;
            foreach ($params as $key => $param) {
                if($key == "utm_source" || $key == "utm_medium"|| $key == "utm_campaign" || $key == "utm_term" || $key == "utm_content"){
                    $session_params->$key = $param;
                    $session_count_utm++;
                }
             
            }
            if($session_count_utm > 0){
            $session_params->fromurl = "1";   

            session()->put('utm_parameters', json_encode($session_params));
            }
            else{
                $urldata = null;
                $utm_parameters = session()->get("utm_parameters");
                if($utm_parameters){
                    if ($this->checkJsondecode($utm_parameters)) {
                    $utm_parameters = json_decode($utm_parameters);
                    $urldata = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
                    }
                }
                if(isset($urldata->fromurl) && $urldata->fromurl == "0")
                {
                     if(!isset($_COOKIE['__utmz'])) {
                     }else{
                     $utmzvalue = $this->parse_ga_cookie($_COOKIE['__utmz']);
                     if(isset($utmzvalue) && (isset($utmzvalue["source"]) || isset($utmzvalue["campaign"]) || isset($utmzvalue["medium"]) || isset($utmzvalue["term"]) || isset($utmzvalue["content"]))){
                        $session_params->utm_source = isset($utmzvalue["source"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["source"]) : "-";
                        $session_params->utm_medium = isset($utmzvalue["medium"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["medium"]) : "-";
                        $session_params->utm_campaign = isset($utmzvalue["campaign"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["campaign"]) : "-";
                        $session_params->utm_term = isset($utmzvalue["term"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["term"]) : "-";
                        $session_params->utm_content = isset($utmzvalue["content"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["content"]) : "-";
                        $session_params->fromurl = "0";   
                        session()->put('utm_parameters', json_encode($session_params));
                     }
                     }  
                }else if(isset($urldata->fromurl) && $urldata->fromurl == "1"){
                }  
                else{
                    if(!isset($_COOKIE['__utmz'])) {
                    }else{
                    $utmzvalue = $this->parse_ga_cookie($_COOKIE['__utmz']);
                    if(isset($utmzvalue) && (isset($utmzvalue["source"]) || isset($utmzvalue["campaign"]) || isset($utmzvalue["medium"]) || isset($utmzvalue["term"]) || isset($utmzvalue["content"]))){
                        $session_params->utm_source = isset($utmzvalue["source"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["source"]) : "-";
                        $session_params->utm_medium = isset($utmzvalue["medium"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["medium"]) : "-";
                        $session_params->utm_campaign = isset($utmzvalue["campaign"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["campaign"]) : "-";
                        $session_params->utm_term = isset($utmzvalue["term"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["term"]) : "-";
                        $session_params->utm_content = isset($utmzvalue["content"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["content"]) : "-";
                        $session_params->fromurl = "0";   
                        session()->put('utm_parameters', json_encode($session_params));
                     }
                    }
                }
                // session()->get($session_key);
            }
        } else {
  
            $urldata = null;
            $utm_parameters = session()->get("utm_parameters");
            if($utm_parameters){
                if ($this->checkJsondecode($utm_parameters)) {
                $utm_parameters = json_decode($utm_parameters);
                $urldata = $utm_parameters ? (isset($utm_parameters->data) ? $utm_parameters->data : $utm_parameters ) : null;
                }
            }
            if(isset($urldata->fromurl) && $urldata->fromurl == "0")
            {
                if($urldata->fromurl == "0"){
                 if(!isset($_COOKIE['__utmz'])) {
                 }else{
                 $utmzvalue = $this->parse_ga_cookie($_COOKIE['__utmz']);
                 if(isset($utmzvalue) && (isset($utmzvalue["source"]) || isset($utmzvalue["campaign"]) || isset($utmzvalue["medium"]) || isset($utmzvalue["term"]) || isset($utmzvalue["content"]))){
                    $session_params->utm_source = isset($utmzvalue["source"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["source"]) : "-";
                    $session_params->utm_medium = isset($utmzvalue["medium"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["medium"]) : "-";
                    $session_params->utm_campaign = isset($utmzvalue["campaign"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["campaign"]) : "-";
                    $session_params->utm_term = isset($utmzvalue["term"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["term"]) : "-";
                    $session_params->utm_content = isset($utmzvalue["content"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["content"]) : "-";
                    $session_params->fromurl = "0";   
                    session()->put('utm_parameters', json_encode($session_params));
                 }
                 }
                }
            }else if(isset($urldata->fromurl) && $urldata->fromurl == "1"){
            }  
            else{
                if(!isset($_COOKIE['__utmz'])) {
                }else{
                $utmzvalue = $this->parse_ga_cookie($_COOKIE['__utmz']);
                if(isset($utmzvalue) && (isset($utmzvalue["source"]) || isset($utmzvalue["campaign"]) || isset($utmzvalue["medium"]) || isset($utmzvalue["term"]) || isset($utmzvalue["content"]))){
                    $session_params->utm_source = isset($utmzvalue["source"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["source"]) : "-";
                    $session_params->utm_medium = isset($utmzvalue["medium"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["medium"]) : "-";
                    $session_params->utm_campaign = isset($utmzvalue["campaign"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["campaign"]) : "-";
                    $session_params->utm_term = isset($utmzvalue["term"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["term"]) : "-";
                    $session_params->utm_content = isset($utmzvalue["content"])? preg_replace('/[^A-Za-z0-9]/', '', $utmzvalue["content"]) : "-";
                    $session_params->fromurl = "0";   
                    session()->put('utm_parameters', json_encode($session_params));
                 }
                }
            }
            // session()->get($session_key);
        }

     
    }

    public function parse_ga_cookie($cookie)
    {
    $values = sscanf($cookie, '%d.%d.%d.%d.utmcsr=%[^|]|utmccn=%[^|]|utmcmd=%[^|]|utmctr=%[^|]|utmcct=%[^|]');
    if (count($values) !== 9) {
        // return false; trigger_error(...); ... or whatever you like
        // throw new InvalidArgumentException("Cookie value '$cookie' does not conform to the __utmz pattern");
    }
    $keys = array('domain', 'timestamp', 'visits', 'camnumber', 'source', 'campaign', 'medium', 'term', 'content');
    return array_combine($keys, $values);
    }

    public function checkJsondecode($data){
        if (!empty($data)) {
            json_decode($data);
            return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;  
    }

    public function initSalesNotificationProductList($countryLang)
    {
        // If Session Data for SalesNotificationProductList is not null
        if (!is_null(session()->get('SalesNotificationProductList'))) {
            //Do Nothing
        } else {
            // Else, Query dB to obtain Required Data
            $salesNotificationList = Product::join('producttranslates', 'producttranslates.ProductId', 'products.id') // products JOIN producttranslates
                ->join('productimages', 'productimages.ProductId', 'products.id') // products JOIN productimages
                ->select('producttranslates.name', 'productimages.url') // Select Name & URL columns
                ->where(['products.status' => 'active', 'producttranslates.langCode' => $countryLang]) // Where product status = active & producttranslate = current $CountryLang
                ->inRandomOrder() // Randomise
                ->limit(10) // Limit 10 records
                ->get();

            // Save Query results into session
            session()->put('SalesNotificationProductList', json_encode($salesNotificationList));
        }
    }
}
