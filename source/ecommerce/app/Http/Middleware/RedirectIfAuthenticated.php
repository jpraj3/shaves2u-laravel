<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard('admin')->check()) {
            return redirect('/');
        } else if (Auth::guard('bawebsite')->check()) {
            return redirect('/dashboard');
        } else {
            // Get new country info from session
            $currentCountryData = json_decode(session()->get('currentCountry'), true);
            $currentCountryIso = strtolower($currentCountryData['codeIso']);
            $langCode = strtolower(app()->getLocale());
            $urlLang = strtolower($currentCountryData['urlLang']);
            if (Auth::guard($guard)->check()) {
                return redirect()->intended('/' . $urlLang . '-' . $currentCountryIso . '/user/dashboard');
            }

        }
        return $next($request);
    }
}
