<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }
        // Blade::withoutDoubleEncoding();
        // Blade::setEchoFormat('e(utf8_encode(%s))');
        // Blade::setEchoFormat('e(htmlentities(%s,ENT_QUOTES,"UTF-8"))');
    }
}
