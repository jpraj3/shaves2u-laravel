<?php

namespace App\Providers;

use App\Events\CustomerRegisteredEvent;
use App\Events\SendReceiptsEvent;
use App\Events\GenerateTaxInvoice;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
        ],
        CustomerRegisteredEvent::class => [
            'App\Listeners\WelcomeEDMListener@handle',
        ],
        SendReceiptsEvent::class => [
            App\Listeners\SendOrderReceiptListener::class,
        ],
        GenerateTaxInvoice::class => [
            'App\Listeners\TaxInvcGenerateListener@handle'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
