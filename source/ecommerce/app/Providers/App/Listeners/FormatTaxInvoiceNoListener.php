<?php

namespace App\Providers\App\Listeners;

use App\Providers\GenerateTaxInvoice;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormatTaxInvoiceNoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GenerateTaxInvoice  $event
     * @return void
     */
    public function handle(GenerateTaxInvoice $event)
    {
        //
    }
}
