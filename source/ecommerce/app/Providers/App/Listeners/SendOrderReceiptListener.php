<?php

namespace App\Providers\App\Listeners;

use App\Events\SendReceiptsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderReceiptListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendReceiptsEvent  $event
     * @return void
     */
    public function handle(SendReceiptsEvent $event)
    {
        //
    }
}
