<?php

namespace App\Services;

use App\Helpers\LaravelHelper;

use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\SupportedLang;
use App\Models\GeoLocation\States;
use DB;

class APICountryService
{
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
    }

    public function supportedLangs($CountryId)
    {
        $_list = SupportedLang::where('CountryId', $CountryId)->get();

        return $_list;
    }

    public function getAllStates($CountryId)
    {
        $_list = States::where('CountryId', $CountryId)->orderByRaw("isDefault desc", "name asc")->get();

        return $_list;
    }

    public function getCurrency($CountryId)
    {
        return Countries::where('id', $CountryId)->pluck('currencyCode')->first();
    }

    public function getCurrencyDisplay($CountryId)
    {
        return Countries::where('id', $CountryId)->pluck('currencyDisplay')->first();
    }

    public function getCountryfromDB($id)
    {
        $country_from_db = Countries::where('id', $id)->first();

        return $country_from_db;
    }

    public function shippingFee($countryid, $lang)
    {
        $result = Countries::where('id', $countryid)->select('shippingFee')->first();
        $data = [
            'shippingfee' => $result->shippingFee,
        ];
        return $data;
    }

    public function shippingFeeTrial($countryid, $lang)
    {
        $result = Countries::where('id', $countryid)->select('shippingTrialFee')->first();
        $data = [
            'shippingfee' => $result->shippingFee,
        ];
        return $data;
    }

    public function tax($countryid)
    {
        $result = Countries::where('id', $countryid)->select('taxRate', 'taxName', 'taxCode', 'includedTaxToProduct', 'taxNumber')->first();
        $data = [
            'taxRate' => $result->taxRate,
            'taxName' => $result->taxName,
            'taxCode' => $result->taxCode,
            'includedTaxToProduct' => $result->includedTaxToProduct,
            'taxNumber' => $result->taxNumber,
        ];
        return $data;
    }
}
