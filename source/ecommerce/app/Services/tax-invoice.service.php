<?php

namespace App\Services;

use App\Helpers\LaravelHelper;

// Models
use App\Models\BaWebsite\SellerUser;
use App\Models\BulkOrders\BulkOrders;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\Orders;

class TaxInvoiceService
{
    public function __construct()
    {
        $this->TAX_LENGTH = 9;
        $this->BULK_ORDER_TAX_LENGTH = 8;
        $this->laravelHelper = new LaravelHelper();
        $this->Log = \Log::channel('cronjob');
    }

    public function formatTaxInvoiceOrder($taxNo, $countryCode, $SellerUserId, $moCode)
    {
        $iso2 = $countryCode;
        $zeros = '';
        for ($i = 0; $i < $this->TAX_LENGTH - strlen($taxNo); $i++) {
            $zeros = $zeros . '0';
        }
        if ($SellerUserId) {
            if ($moCode) {
                return $moCode . '/SH/D' . $zeros . $taxNo;
            } else {
                return null;
            }
        } else {
            return 'S2U-' . $iso2 . '-' . $zeros . $taxNo;
        }
    }

    public function formatTaxInvoiceBulkOrder($taxNo, $countryCode)
    {
        $iso2 = $countryCode;
        $zeros = '';
        for ($i = 0; $i < $this->BULK_ORDER_TAX_LENGTH - strlen($taxNo); $i++) {
            $zeros = $zeros . '0';
        }
        return 'S2U-' . $iso2 . 'B-' . $zeros . $taxNo;
    }

    public function updateTaxInvoiceNoOrder($order_id)
    {
        $this->Log->info('TaxInvoiceService | updateTaxInvoiceNoOrder' . $order_id);
        // get country info
        $order = Orders::where('id', $order_id)->first();
        if ($order) {
            $country = Countries::findorfail($order->CountryId);
            $_seller = [];
            // for BA-Web
            if ($order->SellerUserId) {
                // get ba agent & marketing office info
                $_seller = SellerUser::join('marketingoffices', 'marketingoffices.id', 'sellerusers.MarketingOfficeId')
                    ->where('sellerusers.id', $order->SellerUserId)
                    ->select('sellerusers.*', 'marketingoffices.id as MarketingOfficeId', 'marketingoffices.*')
                    ->first();
                if ($_seller) {
                    // get marketing office code
                    $moCode = $_seller->moCode;
                    // find max taxInvoiceNo of BA-web
                    $maxTaxInvoice = Orders::whereNotNull('SellerUserId')
                        ->where('taxInvoiceNo', 'like', '%/SH/D%')
                        ->where('MarketingOfficeId', $_seller->MarketingOfficeId)
                        ->orderBy('taxInvoiceNo', 'desc')
                        ->select('taxInvoiceNo')
                        ->first();

                    $maxTaxInvoice = $maxTaxInvoice["taxInvoiceNo"] === null ? 0 : explode('/', $maxTaxInvoice["taxInvoiceNo"])[2];
                    $maxTaxInvoice = (int) preg_replace('~\D~', '', $maxTaxInvoice);
                    // generate taxInvoiceNo for BA-web
                    $formatted_tax_invoice_no = $this->formatTaxInvoiceOrder($maxTaxInvoice + 1, $country->codeIso, $_seller->id, $moCode);
                    // update current order with generated taxInvoiceNo
                    Orders::where('id', $order->id)->update(['taxInvoiceNo' => $formatted_tax_invoice_no]);
                }
            } else {
                // find max taxInvoiceNo of Ecommerce
                $maxTaxInvoice = Orders::whereNull('SellerUserId')
                    ->where('CountryId', $order->CountryId)
                    ->orderBy('taxInvoiceNo', 'desc')
                    ->select('taxInvoiceNo')
                    ->first();

                $maxTaxInvoice = $maxTaxInvoice["taxInvoiceNo"] === null ? 0 : (int) explode('-', $maxTaxInvoice["taxInvoiceNo"])[2];
                // // generate taxInvoiceNo for Ecommerce
                $formatted_tax_invoice_no = $this->formatTaxInvoiceOrder($maxTaxInvoice + 1, $country->codeIso, null, null);
                $this->Log->info('5 | => ' . json_encode($formatted_tax_invoice_no));
                // update current order with generated taxInvoiceNo
                Orders::where('id', $order->id)->update(['taxInvoiceNo' => $formatted_tax_invoice_no]);
            }
        }
    }

    public function updateTaxInvoiceNoBulkOrder($bulkOrder)
    {
        $this->Log->info('TaxInvoiceService | updateTaxInvoiceNoBulkOrder' . json_encode($bulkOrder->id));
        // get country info
        $country = Countries::findorfail($bulkOrder->CountryId);

        // find max taxInvoiceNo of BulkOrder
        $maxTaxInvoice = BulkOrders::where('taxInvoiceNo', 'like', 'S2U-' . strtoupper($country->codeIso) . 'B-%')->where('CountryId', $country->id)->orderBy('taxInvoiceNo', 'desc')->first();

        // generate taxInvoiceNo for BulkOrder
        $maxTaxInvoice = ($bulkOrder && $bulkOrder->taxInvoiceNo) ? preg_match('/^(S2U\-\w{3}\-)(\d+)/', '$2', $bulkOrder->taxInvoiceNo) : 0;
        $maxTaxInvoice = (int) $maxTaxInvoice;

        // update current order with generated taxInvoiceNo
        return BulkOrders::where('id', $bulkOrder->id)->update(['taxInvoiceNo' => $this->formatTaxInvoiceBulkOrder($maxTaxInvoice + 1, $country->codeIso)]);
    }
}
