<?php

namespace App\Services;

use App\Helpers\LaravelHelper;
use GuzzleHttp\Client;

class UrbanFoxService
{
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
    }

    public function getUrbanFoxService($apiKey)
    {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json',
                'Authorization' => ['Bearer ' . $apiKey]],
        ]);

        return $client;
    }

    public function getUrbanFoxConfigData()
    {
        return $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'))->UrbanFox;
    }
}
