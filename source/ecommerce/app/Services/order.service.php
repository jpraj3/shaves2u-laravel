<?php

namespace App\Services;

use App\Helpers\LaravelHelper;
use App\Models\GeoLocation\Countries;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class OrderService
{

    public function __construct()
    {
        $this->ORDER_LENGTH = 9;
        $this->BULK_ORDER_LENGTH = 8;
        $this->laravelHelper = new LaravelHelper();
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatOrderNumber($order, $country, $isFull)
    {
        $code = $country ? $country->code : 'NaN';
        $countryCode = $country ? $country->codeIso : 'NaN';
        $orderId = $order ? $order->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($order->created_at)));
        }
    }

    public static function formatOrderNumberV2($order, $country, $isFull)
    {
        $ORDER_LENGTH = 9;
        if ($country === null) {
            $country = Countries::where('id', $order->CountryId)->first();
            $code = $country ? $country->code : 'NaN';
            $countryCode = $country ? $country->codeIso : 'NaN';
            $orderId = $order ? $order->id : 'NaN';
            $returnValue = '';
            for ($i = 0; $i < $ORDER_LENGTH - strlen($orderId); $i++) {
                $returnValue = $returnValue . '0';
            }

            if (!$isFull) {
                return $countryCode . $returnValue . $orderId;
            } else {
                return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', Carbon::parse($order->created_at)->format('Ymd')));
            }
        } else {
            $code = $country ? $country->code : 'NaN';
            $countryCode = $country ? $country->codeIso : 'NaN';
            $orderId = $order ? $order->id : 'NaN';
            $returnValue = '';
            for ($i = 0; $i < $ORDER_LENGTH - strlen($orderId); $i++) {
                $returnValue = $returnValue . '0';
            }

            if (!$isFull) {
                return $countryCode . $returnValue . $orderId;
            } else {
                return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', Carbon::parse($order->created_at)->format('Ymd')));
            }
        }
    }

    public function formatOrderNumberV3($orderid, $orderidCreateAt, $countrycodeIso, $isFull)
    {
        $countryCode = $countrycodeIso ? $countrycodeIso : 'NaN';
        $orderId = $orderid ? $orderid : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($orderidCreateAt)));
        }
    }

    public function formatDate($datetime)
    {
        $isArray = is_array($datetime);

        if (!$isArray) {
            $datetime = Carbon::parse($datetime)->format('Ymd');

        } else {
            $datetime = Carbon::parse($datetime)->format('Ymd');
        }

        return $datetime;
    }

    /**
     * Extract order number for display format
     * Example MY000000001-20171127 -> 000000001
     * @param {*} orderNumber
     */
    public function getOrderNumber($orderNumber)
    {
        // Log::info('getOrderNumber start');
        Log::info('param: orderNumber = ' . $orderNumber);
        return substr($orderNumber, 2, $this->ORDER_LENGTH);
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatInvoiceNumber($order)
    {
        // Log::info('formatInvoiceNumber start');
        $code = $order->Country ? $order->Country->code : $order->region;
        $countryCode = $order->Country ? $order->Country->code : $order->region;
        $orderId = $order->orderId ? $order->orderId : $order->id;
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        return 'S2U-' . $countryCode . '-' . $returnValue . $orderId;
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatBulkOrderNumber($bulkorder, $country, $isFull)
    {
        // Log::info('formatBulkOrderNumber start');
        $countryCode = $country ? $country->codeIso : 'NaN';
        $orderId = $bulkorder->id ? $bulkorder->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->BULK_ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . 'B' . $returnValue . $orderId;
        } else {
            return $countryCode . 'B' . $returnValue . $orderId . '-' . $this->formatDate($bulkorder->created_at);
        }
    }

    /**
     * Extract order number for display format
     * Example MY000000001-20171127 -> 000000001
     * @param {*} orderNumber
     */
    public function getBulkOrderNumber($orderNumber)
    {
        Log::info('getBulkOrderNumber start');
        Log::info('param: orderNumber = $orderNumber');
        if (strpos($orderNumber, "-") !== false) {
            return substr($orderNumber, 3, strpos($orderNumber, "-") - 3);
        } else {
            return substr($orderNumber, 3, $this->BULK_ORDER_LENGTH);
        }
    }

    /**
     * Check type of order base on the order number
     * MY0000000001-20170101 -> order
     * MYB000000001-20170101 -> bulkOrder
     * @param {*} orderNumber
     */
    public function checkOrderNumberType($orderNumber)
    {
        // Log::info('checkOrderNumberType start');
        Log::info('Aftership Webhook | checkOrderNumberType | param: orderNumber =' . $orderNumber);
        if ($orderNumber[2] === 'B') {
            return 'bulkOrder';
        } else {
            return 'order';
        }
    }

    public function formatOrderNumberV4($order, $codeIso, $isFull)
    {
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $order ? $order->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($order->created_at)));
        }
    }

    public function formatBulkOrderNumberV4($bulkorder, $codeIso, $isFull)
    {
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $bulkorder->id ? $bulkorder->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->BULK_ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . 'B' . $returnValue . $orderId;
        } else {
            return $countryCode . 'B' . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($order->created_at)));
        }
    }

    public function formatOrderNumberV5($orderid, $codeIso, $timestamp, $isFull)
    {
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $orderid ? $orderid : 'NaN';
        $created_at = $timestamp ? $this->formatDate($timestamp) : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $created_at));
        }
    }

    public function formatBulkOrderNumberV5($bulkorderid, $codeIso, $timestamp, $isFull)
    {
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $bulkorderid ? $bulkorderid : 'NaN';
        $created_at = $timestamp ? $this->formatDate($timestamp) : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->BULK_ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . 'B' . $returnValue . $orderId;
        } else {
            return $countryCode . 'B' . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $created_at));
        }
    }

    /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatAfterShipOrderNumber($order, $codeIso, $isFull)
    {
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $order ? $order->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . $returnValue . $orderId;
        } else {
            return $countryCode . $returnValue . $orderId . '-' . str_replace(':', '', str_replace(' ', '_', $this->formatDate($order->created_at)));
        }
    }

        /**
     * Display order as format {COUNTRY_CODE_ISO2}{9 digits order number}-{order date}
     * Example: MY000000001-20171127 || SG000000002-20171127 || MY000000001
     * @param {*} order
     */
    public function formatAfterShipBulkOrderNumber($bulkorder, $codeIso, $isFull)
    {
        // Log::info('formatBulkOrderNumber start');
        $countryCode = $codeIso ? $codeIso : 'NaN';
        $orderId = $bulkorder->id ? $bulkorder->id : 'NaN';
        $returnValue = '';
        for ($i = 0; $i < $this->BULK_ORDER_LENGTH - strlen($orderId); $i++) {
            $returnValue = $returnValue . '0';
        }

        if (!$isFull) {
            return $countryCode . 'B' . $returnValue . $orderId;
        } else {
            return $countryCode . 'B' . $returnValue . $orderId . '-' . $this->formatDate($bulkorder->created_at);
        }
    }
}
