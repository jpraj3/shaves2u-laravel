<?php

namespace App\Services;

/* Reference:
- https://laravel.com/docs/5.8/session
*/

class SessionService
{

    public function __construct()
    {

    }

    public function session($session_key, $method, $data) 
    {
        switch ($method) {
            case 'set':
                {
                    session()->put($session_key, $data);
                }
                break;
            case 'get':
                {
                    $session_data = session()->get($session_key);
                    return $session_data;
                }
                break;
            case 'clear':
                {
                    session()->forget($session_key);
                }
                break;
            default:
                {
                    return 'Invalid method.'; 
                }
                break;
        }
    }
}