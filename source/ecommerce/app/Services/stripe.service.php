<?php
namespace App\Services;

use App\Helpers\LaravelHelper;
use App\Models\GeoLocation\Countries;
use Config;

class StripeService
{
    public $stripeEnabledList;
    public $stripeEnabled;
    public $countryCode;
    public $type;
    public $currentStripeCountry;
    public $currentStripeCountryCode;
    public $error_msg;

    // Build All stripe data here
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
        $this->stripeEnabled = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'))->stripe->enabled;
        $this->stripe_countries = $this->stripeCountries($this->stripeEnabled);
        $this->type = config('app.appType');
        $this->currentCountry = json_decode(session()->get('currentCountry'), true);
        $this->currentCountryCode = $this->currentCountry["code"];
        $this->error_msg = "Stripe service is not enabled for current country";
        $this->getActiveCountries();
    }

    // Check for Active Stripe Countries
    public function stripeCountries($country_list)
    {
        $list = [];
        foreach ($country_list as $c) {
            array_push($list, $c);
        }
        return $list;
    }

    // List all Active Countries with Stripe Payment Gateway
    public function getActiveCountries()
    {
        $active = Countries::where('isActive', 1)->whereIn('code', $this->stripe_countries)->get();
        return $this->isStripeEnabled($active);
    }

    // Check if Stripe is Enabled for current country session
    public function isStripeEnabled($active)
    {
        $this->currentStripeCountry="";
        $total = count($active);
        for ($x = 0; $x < $total; $x++) {
            if ($active[$x]["code"] === $this->currentCountryCode) {
                $this->currentStripeCountryCode = $this->currentCountryCode;
                $this->currentStripeCountry = $active[$x];
                return $this->stripeServiceInfo($this->currentStripeCountry);
            } 
        }
        if(!isset($this->currentStripeCountry)){
            $this->error_msg = "Current Country [" . $active[$x]["code"] . "] Not Enabled for Stripe Service.";
            return $this->error_msg;
        }
    }

    // Build Stripe Service Data for Current Country
    public function stripeServiceInfo($activeCountry)
    {
        $info = [];
        if ($activeCountry["isWebEcommerce"] === 1 || $activeCountry["isBaEcommerce"] === 1) {
            $info = [
                'isBaEcommerce' => $activeCountry["isWebEcommerce"],
                'isBaAlaCarte' => $activeCountry["isBaAlaCarte"],
                'isBaStripe' => $activeCountry["isBaStripe"],
                'isBaSubscription' => $activeCountry["isBaSubscription"],
                'isWebEcommerce' => $activeCountry["isWebEcommerce"],
                'isWebAlaCarte' => $activeCountry["isWebAlaCarte"],
                'isWebStripe' => $activeCountry["isWebStripe"],
                'isWebSubscription' => $activeCountry["isWebSubscription"],
                'countryCode' => $activeCountry["code"],
                'type' => config('app.appType'),
            ];

            return $info;
        } else {
            return $this->error_msg;
        }
    }
}
