<?php

namespace App\Traits\Models;

trait UserAdminTraits {
    /*
      |--------------------------------------------------------------------------
      | UserAdminModelTraits
      |--------------------------------------------------------------------------
      |
      | This Traits handles some function which is will use in Auth::user()
      | [App\Models\Admin\Admin.php & App\Models\User.php]
      |
     */

    /*
      |--------------------------------------------------------------------------
      | Code Below
      |--------------------------------------------------------------------------
      |
      | Those function will be use by Auth::user()
      | [App\Models\Admin\Admin.php & App\Models\User.php]
      | Those function is tie with Models fields
      |
     */

    /**
     * check login applicant user type it is Super Admin
     * @return boolean
     */
    public function isSA() {
        return (_eq($this->user_type, "superadmin"));
    }

    /**
     * check login applicant user type it is Sub Admin
     * @return boolean
     */
    public function isSubAdmin() {
        return (_eq($this->user_type, "subadmin"));
    }

    /**
     * check login applicant user type it is User
     * @return boolean
     */
    public function isUser() {
        return (empty($this->user_type));
    }

    /*
      |--------------------------------------------------------------------------
      | Code Below
      |--------------------------------------------------------------------------
      |
      | Those function will be use by Auth::user()
      | [App\Models\Admin\Admin.php & App\Models\User.php]
      | and based on parameter, didn't have any relationship will Models
      |
     */

    /**
     * check $id === Auth::user()->id
     * @param integer $id
     * @return boolean
     */
    public function isSelf($id) {
        return (_eq($id, $this->id, TRUE) === FALSE);
    }

    /**
     * print out applicant account status
     * @param integer $status
     * @return string
     */
    public function accountStatus($status) {
        $list = config("global.users.accounts.status");
        return (!empty($list[$status])) ? ucwords($list[$status]) : "";
    }

    /**
     * print out login applicant user type
     * @param string $user_type
     * @return string
     */
    public function userType($user_type = "") {
        if (empty($user_type)) {
            return "User";
        }

        return ((_eq($user_type, "superadmin")) ? "Super" : "Sub") . " admin";
    }

}
