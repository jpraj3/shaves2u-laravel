<?php

namespace App\Queue;

use Redis;

class CSVReportToDownloadQueueService {

    protected static $CSV_FILE_TO_DOWNLOAD_LIST_KEY = 'csv-file-to-download-list-key';

    // Function: Add single hash 
    public static function addHash($field, $value)
    {
        // $data = (Object) array();
        // if (is_string($unique_identifier) || is_numeric($unique_identifier)) {
        //     $data->unique_identifier = $unique_identifier;
        //     $data->options = $options;
        //     $data->retry = 0;
        // }
        Redis::hmset(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY, $field, json_encode($value));
    }

    // Function: Get single hash
    public static function getHash($field) 
    {
        return Redis::hmget(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(CSVReportToDownloadQueueService::$CSV_FILE_TO_DOWNLOAD_LIST_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */