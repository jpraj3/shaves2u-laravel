<?php

namespace App\Queue;

use Redis;

class TaxInvoiceQueueService {

    protected static $TAX_INVOICE_LIST_KEY = 'tax-invoice-order-list-key';

    // Function: Add single hash
    public static function addHash($field, $value)
    {
        Redis::hmset(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY, $field, json_encode($value));
    }

    // Function: Get single hash
    public static function getHash($field)
    {
        return Redis::hmget(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(TaxInvoiceQueueService::$TAX_INVOICE_LIST_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */
