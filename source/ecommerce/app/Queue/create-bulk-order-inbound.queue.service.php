<?php

namespace App\Queue;

use Redis;

class BulkOrderInboundQueueService {

    protected static $BULK_ORDER_INBOUND_LIST_KEY_MY = 'bulk-order-inbound-list-key-my';
    protected static $BULK_ORDER_INBOUND_LIST_KEY_SG= 'bulk-order-inbound-list-key-sg';
    protected static $BULK_ORDER_INBOUND_LIST_KEY_KR = 'bulk-order-inbound-list-key-kr';
    protected static $BULK_ORDER_INBOUND_LIST_KEY_HK = 'bulk-order-inbound-list-key-hk';
    protected static $BULK_ORDER_INBOUND_LIST_KEY_TW = 'bulk-order-inbound-list-key-tw';

    /***** MALAYSIA QUEUE SERVICE *****/

    // Function: Add single hash 
    public static function addHashMY($orderId, $countryId)
    {    
        $data = (Object) array();
        if (is_string($orderId) || is_numeric($orderId)) {
            $data->orderId = $orderId;
            $data->countryId = $countryId;
            $data->retry = 0;
        }
        Redis::hmset(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY, $orderId, json_encode($data));
    }

    // Function: Get single hash
    public static function getHashMY($field) 
    {
        return Redis::hmget(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY, $field);
    }

    // Function: Get all hash
    public static function getAllHashMY()
    {
        return Redis::hgetall(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHashMY($field)
    {
        return Redis::hexists(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY, $field);
    }

    // Function: Delete single hash
    public static function deleteHashMY($field)
    {
        Redis::hdel(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHashMY($fields)
    {   
        foreach ($fields as $field) {
            deleteHashMY($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHashMY()
    {
        Redis::del(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_MY);
    }


        /***** SINGAPORE QUEUE SERVICE *****/

    // Function: Add single hash 
    public static function addHashSG($orderId, $countryId)
    {    
        $data = (Object) array();
        if (is_string($orderId) || is_numeric($orderId)) {
            $data->orderId = $orderId;
            $data->countryId = $countryId;
            $data->retry = 0;
        }
        Redis::hmset(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG, $orderId, json_encode($data));
    }

    // Function: Get single hash
    public static function getHashSG($field) 
    {
        return Redis::hmget(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG, $field);
    }

    // Function: Get all hash
    public static function getAllHashSG()
    {
        return Redis::hgetall(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHashSG($field)
    {
        return Redis::hexists(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG, $field);
    }

    // Function: Delete single hash
    public static function deleteHashSG($field)
    {
        Redis::hdel(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHashSG($fields)
    {   
        foreach ($fields as $field) {
            deleteHashSG($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHashSG()
    {
        Redis::del(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_SG);
    }



    /*****  KOREAN QUEUE SERVICE *****/

    // Function: Add single hash 
    public static function addHashKR($orderId, $countryId)
    {    
        $data = (Object) array();
        if (is_string($orderId) || is_numeric($orderId)) {
            $data->orderId = $orderId;
            $data->countryId = $countryId;
            $data->retry = 0;
        }
        Redis::hmset(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR, $orderId, json_encode($data));
    }

    // Function: Get single hash
    public static function getHashKR($field) 
    {
        return Redis::hmget(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR, $field);
    }

    // Function: Get all hash
    public static function getAllHashKR()
    {
        return Redis::hgetall(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHashKR($field)
    {
        return Redis::hexists(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR, $field);
    }

    // Function: Delete single hash
    public static function deleteHashKR($field)
    {
        Redis::hdel(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHashKR($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHashKR()
    {
        Redis::del(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_KR);
    }



    /*****  HONG KONG QUEUE SERVICE *****/

    // Function: Add single hash 
    public static function addHashHK($orderId, $countryId)
    {    
        $data = (Object) array();
        if (is_string($orderId) || is_numeric($orderId)) {
            $data->orderId = $orderId;
            $data->countryId = $countryId;
            $data->retry = 0;
        }
        Redis::hmset(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK, $orderId, json_encode($data));
    }

    // Function: Get single hash
    public static function getHashHK($field) 
    {
        return Redis::hmget(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK, $field);
    }

    // Function: Get all hash
    public static function getAllHashHK()
    {
        return Redis::hgetall(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHashHK($field)
    {
        return Redis::hexists(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK, $field);
    }

    // Function: Delete single hash
    public static function deleteHashHK($field)
    {
        Redis::hdel(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHashHK($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHashHK()
    {
        Redis::del(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_HK);
    }



    /*****  TAIWAN QUEUE SERVICE *****/

    // Function: Add single hash 
    public static function addHashTW($orderId, $countryId)
    {    
        $data = (Object) array();
        if (is_string($orderId) || is_numeric($orderId)) {
            $data->orderId = $orderId;
            $data->countryId = $countryId;
            $data->retry = 0;
        }
        Redis::hmset(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW, $orderId, json_encode($data));
    }

    // Function: Get single hash
    public static function getHashTW($field) 
    {
        return Redis::hmget(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW, $field);
    }

    // Function: Get all hash
    public static function getAllHashTW()
    {
        return Redis::hgetall(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHashTW($field)
    {
        return Redis::hexists(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW, $field);
    }

    // Function: Delete single hash
    public static function deleteHashTW($field)
    {
        Redis::hdel(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHashTW($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHashTW()
    {
        Redis::del(BulkOrderInboundQueueService::$BULK_ORDER_INBOUND_LIST_KEY_TW);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */