<?php

namespace App\Queue;

use Redis;
use Carbon\Carbon;

class SendEmailQueueService {

    protected static $EMAIL_LIST_KEY = 'send-email-list-key';

    // Function: Generate a unique key for email hash (userid_emailtype_datetime)
    public static function generateUniqueEmailKey($userId, $emailType)
    {
        $emailtypevalues = config('global.all.email-type');
        if(in_array($emailType, $emailtypevalues)) 
        {
            $datetimestamp = Carbon::now()->format('Ymdhis');
            return $userId."-".$emailType."-".$datetimestamp;
        } 
        else 
        {
            return null;
        }
    }

    // Function: Add single hash 
    public static function addHash($userId, $emailType, $value)
    {
        if($userId && $emailType && $value)
        {
            $uniqueEmailKey = SendEmailQueueService::generateUniqueEmailKey($userId, $emailType);
            if($uniqueEmailKey)
            {
                Redis::hmset(SendEmailQueueService::$EMAIL_LIST_KEY, $uniqueEmailKey, json_encode($value));
            }
        }
    }

    // Function: Get single hash
    public static function getHash($field) 
    {
        return Redis::hmget(SendEmailQueueService::$EMAIL_LIST_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(SendEmailQueueService::$EMAIL_LIST_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(SendEmailQueueService::$EMAIL_LIST_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(SendEmailQueueService::$EMAIL_LIST_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(SendEmailQueueService::$EMAIL_LIST_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */