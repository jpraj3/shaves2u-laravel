<?php

namespace App\Queue;

use Redis;

class SaleReportQueueService {

    protected static $SALE_REPORT_LIST_KEY = 'sale-report-list-key';

    // Function: Add single hash
    public static function addHash($field, $value)
    {
        Redis::hmset(SaleReportQueueService::$SALE_REPORT_LIST_KEY, $field, json_encode($value));
    }

    // Function: Get single hash
    public static function getHash($field)
    {
        return Redis::hmget(SaleReportQueueService::$SALE_REPORT_LIST_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(SaleReportQueueService::$SALE_REPORT_LIST_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(SaleReportQueueService::$SALE_REPORT_LIST_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(SaleReportQueueService::$SALE_REPORT_LIST_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(SaleReportQueueService::$SALE_REPORT_LIST_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */
