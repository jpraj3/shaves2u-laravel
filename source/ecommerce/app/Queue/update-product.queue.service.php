<?php

namespace App\Queue;

use Redis;

class UpdateProductQueueService {

    protected static $UPDATE_PRODUCT_KEY = 'update-product';
    protected static $UPDATE_PRODUCT_LIST_KEY = 'update-product-list';

    // Function: Add single hash 
    public static function addHash($field, $value)
    {
        Redis::hmset(UpdateProductQueueService::$UPDATE_PRODUCT_KEY, $field, $value);
    }

    // Function: Get single hash
    public static function getHash($field) 
    {
        return Redis::hmget(UpdateProductQueueService::$UPDATE_PRODUCT_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(UpdateProductQueueService::$UPDATE_PRODUCT_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(UpdateProductQueueService::$UPDATE_PRODUCT_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(UpdateProductQueueService::$UPDATE_PRODUCT_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(UpdateProductQueueService::$UPDATE_PRODUCT_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */