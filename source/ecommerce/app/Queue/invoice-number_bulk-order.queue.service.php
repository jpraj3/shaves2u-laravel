<?php

namespace App\Queue;

use Redis;

class TaxInvoiceBulkOrderQueueService {

    protected static $TAX_INVOICE_BULK_ORDER_LIST_KEY = 'tax-invoice-bulk-order-list-key';

    // Function: Add single hash 
    public static function addHash($field, $value)
    {
        Redis::hmset(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY, $field, json_encode($value));
    }

    // Function: Get single hash
    public static function getHash($field) 
    {
        return Redis::hmget(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY, $field);
    }

    // Function: Get all hash
    public static function getAllHash()
    {
        return Redis::hgetall(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY);
    }

    // Function: Check if hash with certain field name exist
    public static function isExistHash($field)
    {
        return Redis::hexists(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY, $field);
    }

    // Function: Delete single hash
    public static function deleteHash($field)
    {
        Redis::hdel(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY, $field);
    }

    // Function: Delete multiple hash
    public static function deleteMultipleHash($fields)
    {   
        foreach ($fields as $field) {
            deleteHash($field);
        }
    }

    // Function: Delete all hash
    public static function deleteAllHash()
    {
        Redis::del(TaxInvoiceBulkOrderQueueService::$TAX_INVOICE_BULK_ORDER_LIST_KEY);
    }

}

/* Reference: https://divinglaravel.com/introduction-to-redis-hashes */