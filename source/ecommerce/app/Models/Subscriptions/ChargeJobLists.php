<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class ChargeJobLists extends Model {
    protected $table = 'chargejoblist';
    //
    protected $fillable = [
       'charge_date',
       'total_to_charge',
       'JobType',
       'subscriptionId',
       'email',
       'CountryId',
       'status',
       'function',
       'created_at',
       'updated_at'
    ];

}
