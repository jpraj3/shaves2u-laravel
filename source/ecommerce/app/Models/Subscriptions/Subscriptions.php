<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model {
    protected $table = 'subscriptions';
    //
    protected $fillable = [
        'nextDeliverDate',
        'startDeliverDate',
        'expiredDateAt',
        'currentDeliverNumber',
        'totalDeliverTimes',
        'currentCycle',
        'nextChargeDate',
        'currentChargeNumber',
        'totalChargeTimes',
        'recharge_date',
        'total_recharge',
        'unrealizedCust',
        'pricePerCharge',
        'currentOrderTime',
        'currentOrderId',
        'status',
        'daily_charge',
        'fullname',
        'email',
        'phone',
        'discountPercent',
        'qty',
        'CardId',
        'PlanId',
        'SellerUserId',
        'SkuId',
        'UserId',
        'DeliveryAddressId',
        'BillingAddressId',
        'PlanCountryId',
        'lastDeliverydate',
        'isTrial',
        'isCustom',
        'price',
        'convertTrial2Subs',
        'hasSendReferralInvite',
        'hasSendFollowUp1',
        'hasSendFollowUp2',
        'hasSendFollowUp5Days',
        'hasSendCancelRequest',
        'hasFlag',
        'hasRechargeFlag',
        'isOffline',
        'channelType',
        'eventLocationCode',
        'MarketingOfficeId',
        'cancellationReason',
        'promoCode',
        'promotionId',
        'isDirectTrial',
        'imp_uid',
        'merchant_uid',
        'temp_discountPercent',
        'hasTriedCancel',
        'hasMaximumTenure',
        'OnHoldDate',
        'UnrealizedDate',
        'isAnnualDiscountIncludeAddon'
    ];

}
