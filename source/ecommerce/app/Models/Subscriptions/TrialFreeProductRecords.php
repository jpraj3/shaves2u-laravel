<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class TrialFreeProductRecords extends Model
{
    protected $table = 'trial_free_product_record';
    //
    protected $fillable = [
        'subscriptionId',
        'UserId',
        'qty',
        'price',
        'currency',
        'ProductCountryId',
        'PlanId',
        'isFreeProduct',
        'isAddon',
        'isUse',
        'category',
        'created_at',
        'updated_at'
    ];

}
