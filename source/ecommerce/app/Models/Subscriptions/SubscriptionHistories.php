<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SubscriptionHistories extends Model {
    protected $table = 'subscriptionhistories';
    //
    protected $fillable = [
        'message',
        'detail',
        'subscriptionId'
    ];

}
