<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class SummarySubscriber extends Model
{
    //
    protected $table = 'summary_subscribers';

    protected $fillable = [
        'id',
        'SubID',
        'UserID', 
        'CustomerName', 
        'Email', 
        'Country', 
        'Category', 
        'MOCode', 
        'BadgeID', 
        'DeliveryAddress', 
        'PlanType', 
        'BladeType', 
        'DeliveryInterval', 
        'CassetteCycle', 
        'CycleCount', 
        'AfterShaveCream', 
        'ShaveCream', 
        'Pouch', 
        'RubberHandle', 
        'SignUp', 
        'Conversion', 
        'LastDelivery', 
        'LastCharge', 
        'NextCharge', 
        'RechargeAttempt', 
        'RechargeCount', 
        'CancellationDate', 
        'Status', 
        'CancellationReason', 
        'OtherReason', 
        'PauseSubscription', 
        'OnHoldDate',
        'UnrealizedDate'
        ];
}
