<?php

namespace App\Models\Subscriptions;

use Illuminate\Database\Eloquent\Model;

class ChargeJobChecks extends Model {
    protected $table = 'chargejobchecks';
    //
    protected $fillable = [
        'subscriptionIds',
        'UserId',
        'email',
        'CountryId',
        'JobType',
        'ReferralDiscount',
        'ChargeFunction',
        'CreateReceiptSub',
        'CreateOrder',
        'UpdateReceiptSub',
        'ErrorOccur',
        'ErrorCheck'
    ];

}
