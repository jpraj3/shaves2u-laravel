<?php

namespace App\Models\Cards;

use Illuminate\Database\Eloquent\Model;

class PaymentRefunds extends Model
{
    protected $table = 'paymentrefunds';
    //
    protected $fillable = [
        'amount',
        'currency',
        'adjustment',
        'partial_refund',
        'OrderId',
        'SubscriptionId',
        'CardId',
        'UserId',
        'reason',
        'type',
        'paymentType',
        'charge_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
