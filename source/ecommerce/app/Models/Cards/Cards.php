<?php

namespace App\Models\Cards;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    protected $table = 'cards';
    //
    protected $fillable = [
        'customerId',
        'cardNumber',
        'branchName',
        'cardName',
        'expiredYear',
        'expiredMonth',
        'birth',
        'isDefault',
        'created_at',
        'updated_at',
        'UserId',
        'isRemoved',
        'type',
        'pymt_intent_tk',
        'pymt_intent_cp',
        'pymt_intent_ask',
        'pymt_intent_alacarte',
        'risk_level',
        'payment_method',
        'CountryId',
        'createdBy',
        'updatedBy',
        'require_otp',
        'require_otp_pass',
        'require_otp_pymt_intent',
        'otp_refund'
    ];
}
