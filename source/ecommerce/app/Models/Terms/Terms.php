<?php

namespace App\Models\Terms;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    protected $table = 'terms';
     public $timestamps = false;
    protected $fillable = [
        'content',
        'langCode',
        'CountryId',
    ];
}
