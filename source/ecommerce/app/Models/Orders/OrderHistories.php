<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderHistories extends Model {
    protected $table = 'orderhistories';
    //
    protected $fillable = [
        'message',
        'isRemark',
        'OrderId',
        'cancellationReason',
    ];

}
