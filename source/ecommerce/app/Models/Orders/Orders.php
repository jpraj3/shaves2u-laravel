<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model {
    protected $table = 'orders';
    //
    protected $fillable = [
        'status',
        'payment_status',
        'deliveryId',
        'email',
        'phone',
        'fullName',
        'SSN',
        'paymentType',
        'promoCode',
        'subscriptionIds',
        'taxRate',
        'taxName',
        'carrierKey',
        'carrierAgent',
        'isBulkCreated',
        'vendor',
        'DeliveryAddressId',
        'BillingAddressId',
        'CountryId',
        'PromotionId',
        'UserId',
        'SellerUserId',
        'source',
        'medium',
        'campaign',
        'term',
        'content',
        'taxInvoiceNo',
        'channelType',
        'eventLocationCode',
        'MarketingOfficeId',
        'isDirectTrial',
        'isSubsequentOrder',
        'imp_uid',
        'merchant_uid',
        'pymt_intent'
    ];

    public function getOrderNumber($value)
    {
        return ucfirst($value);
    }

}
