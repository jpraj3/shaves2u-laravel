<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class DeliveryTracking extends Model
{
    protected $table = 'deliverytrackings';
    //
    protected $fillable = [
        'OrderId',
        'BulkOrderId',
        'warehouse_id',
        'status',
        'deliveryId',
        'api_data',
        'delivery_edm',
        'issue'
    ];
}
