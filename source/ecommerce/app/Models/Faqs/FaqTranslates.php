<?php

namespace App\Models\Faqs;

use Illuminate\Database\Eloquent\Model;

class FaqTranslates extends Model
{
    protected $table = 'faqtranslates';
    public $timestamps = false;
    protected $fillable = [
        'question',
        'answer',
        'FaqId',
    ];
}
