<?php

namespace App\Models\Ecommerce;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'password',
        'socialId',
        'birthday',
        'gender',
        'SSN',
        'NRIC',
        'phoneHome',
        'phoneOffice',
        'phone',
        'defaultLanguage',
        'isActive',
        'defaultShipping',
        'defaultBilling',
        'hasReceiveOffer',
        'CountryId',
        'NotificationTypeId',
        'badgeId',
        'isGuest',
        'UserTypeId',
        'IsSG_old',
        'createdBy',
        'updateBy',
        'Email_Sub',
        'Sms_Sub',
        'referralId',
        'inviteCode',
        'HKG_Marketing_Sub',
        'welcome_edm',
        'welcome_edm_time',
        'registered_at',
        'last_login_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\UserResetPasswordNotification($token));
    }
}
