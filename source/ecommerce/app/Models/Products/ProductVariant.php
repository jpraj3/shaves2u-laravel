<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model {
    public $timestamps = false;
    protected $table = 'productvariants';
    //
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'ProductId'
    ];

}
