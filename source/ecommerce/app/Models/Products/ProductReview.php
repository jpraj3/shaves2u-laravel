<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model {
    public $timestamps = false;
    protected $table = 'productreviews';
    //
    protected $fillable = [
        'comment',
        'rating',
        'created_at',
        'updated_at',
        'ProductId'
    ];

}
