<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model {
    public $timestamps = false;
    protected $table = 'producttypes';
    //
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'order',
        'url',
        'status'
    ];

}
