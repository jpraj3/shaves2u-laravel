<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductTypeDetail extends Model {

    protected $table = 'producttypedetails';
    //
    protected $fillable = [
        'ProductTypeId',
        'ProductId',
        'Order'
    ];

}
