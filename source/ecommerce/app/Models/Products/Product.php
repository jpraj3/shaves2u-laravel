<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    public $timestamps = false;
    protected $table = 'products';
    //
    protected $fillable = [
        'sku',
        'rating',
        'isFeatured',
        'status',
        'created_at',
        'updated_at',
        'ProductTypeId',
        'slug',
        'order',
        'long',
        'width',
        'hight',
        'weight',
        'isFeaturedProduct',
        'isFeaturedAsk',
    ];

}
