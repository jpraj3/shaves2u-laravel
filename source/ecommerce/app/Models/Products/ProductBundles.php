<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductBundles extends Model
{
    protected $table = 'productbundles';
    //
    protected $fillable = [
        'bundle_name',
        'isOnline',
        'isOffline',
        'promotion',
        'PromotionId',
        'discountPercent',
        'bundle_price',
        'isActive',
        'updated_at',
        'created_at',
    ];
}
