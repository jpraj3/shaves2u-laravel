<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductRelated extends Model {
    public $timestamps = false;
    protected $table = 'productrelateds';
    //
    protected $fillable = [
        'ProductCountryId',
        'RelatedProductCountryId'
    ];

}
