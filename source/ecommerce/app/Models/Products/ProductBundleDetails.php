<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductBundleDetails extends Model
{
    protected $table = 'productbundledetails';
    //
    protected $fillable = [
        'ProductCountryId',
        'ProductBundleId',
        'PromotionId',
        'image_url',
        'qty',
        'startAt',
        'expireAt',
        'created_at',
        'updated_at'
    ];
}
