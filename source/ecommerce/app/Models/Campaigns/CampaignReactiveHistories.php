<?php

namespace App\Models\Campaigns;

use Illuminate\Database\Eloquent\Model;

class CampaignReactiveHistories extends Model {
    protected $table = 'campaign_reactive_histories';
    //
    protected $fillable = [
        'UserId',
        'email',
        'subscriptionId',
        'CountryId',
        'LanguageCode',
        'status_Old',
        'status_New',
        'PlanId_Old',
        'PlanId_New',
        'AddOn_Old',
        'AddOn_New',
        'eventLocationCode',
        'pricePerCharge_Old',
        'pricePerCharge_New',
        'discountPercent',
        'isChangeCard',
        'unrealized_Date',
        'nextDeliverDate_old',
        'currentCycle_old',
        'nextChargeDate_old',
        'recharge_date_old',
        'total_recharge_old',
        'extra_info',
        'campaign_type',
        'nextDeliverDate_new',
        'nextChargeDate_new',
        'isChargeSuccess',
        'source',
        'medium',
        'campaign',
        'term',
        'content'
    ];

}
