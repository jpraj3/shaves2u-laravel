<?php

namespace App\Models\Rebates;

use Illuminate\Database\Eloquent\Model;

class ReferralCashOut extends Model {
    protected $table = 'referralcashouts';
    //
    protected $fillable = [
        'name',
        'amount',
        'status',
        'UserId',
        'CountryId',
        'email',
        'bank_name',
        'bank_account_no'
    ];

}
