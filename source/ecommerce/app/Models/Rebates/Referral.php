<?php

namespace App\Models\Rebates;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model {
    protected $table = 'referrals';
    //
    protected $fillable = [
        'referralId',
        'friendId',
        'CountryId',
        'source',
        'firstPurchase',
        'used',
        'usedAmount',
        'usedTime',
        'usedType',
        'rewardId',
        'refereeDiscount',
        'refereeDiscountApplied',
        'status',
    ];

}
