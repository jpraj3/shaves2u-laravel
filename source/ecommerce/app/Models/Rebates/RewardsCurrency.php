<?php

namespace App\Models\Rebates;

use Illuminate\Database\Eloquent\Model;

class RewardsCurrency extends Model {
    protected $table = 'rewardscurrencies';
    //
    protected $fillable = [
        'rewardtype',
        'value',
        'CountryId',
    ];

}
