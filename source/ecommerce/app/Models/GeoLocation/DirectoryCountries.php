<?php

namespace App\Models\GeoLocation;

use Illuminate\Database\Eloquent\Model;

class DirectoryCountries extends Model {
    public $timestamps = false;
    // Table Name
    protected $table = 'directorycountries';
    // Table Column
    protected $fillable = [
        'code',
        'codeIso2',
        'name',
        'nativeName',
        'currencyCode',
        'languageCode',
        'languageName',
        'flag',
        'callingCode',
        'CountryId',
    ];

}
