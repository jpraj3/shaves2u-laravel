<?php

namespace App\Models\GeoLocation;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    // Table Name
    protected $table = 'states';
    // Table Column
    protected $fillable = [
        'name',
        'isDefault',
        'CountryId'
    ];
}
