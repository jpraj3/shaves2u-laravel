<?php

namespace App\Models\CancellationJourney;

use Illuminate\Database\Eloquent\Model;

class CancellationReasonTranslates extends Model {
    protected $table = 'cancellation_reason_translates';
    //
    protected $fillable = [
        'content',
        'defaultContent',
        'countryId',
        'langCode',
        'isHidden',
        'viewName',
        'modalLevel',
        'parentId',
        'orderNumber'
    ];

}