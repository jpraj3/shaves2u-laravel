<?php

namespace App\Models\CancellationJourney;

use Illuminate\Database\Eloquent\Model;

class CancellationFollowUp extends Model {
    protected $table = 'cancellation_follow_up';
    //
    protected $fillable = [
        'SubscriptionId',
        'UserId',
        'CancelEdm1',
        'CancelEdm2',
        'CancelEdm3',
        'CancelEdm4',
        'CancelEdm5',
        'ReminderDate',
        'ReminderStatus',
        'NextJourneyStatus',
        'JourneyDesc',
        'Status',
    ];

}