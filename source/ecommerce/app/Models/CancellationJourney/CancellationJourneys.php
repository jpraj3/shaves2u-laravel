<?php

namespace App\Models\CancellationJourney;

use Illuminate\Database\Eloquent\Model;

class CancellationJourneys extends Model {
    protected $table = 'cancellation_journeys';
    //
    protected $fillable = [
        'email',
        'CancellationTranslatesId',
        'OtherReason',
        'UserId',
        'OrderId',
        'SubscriptionId',
        'planType',
        'subscriptionName',
        'hasCancelled',
        'hasChangedBlade',
        'modifiedBlade',
        'hasChangedFrequency',
        'modifiedFrequency',
        'hasGetFreeProduct',
        'hasGetDiscount',
        'discountPercent',
        'hasPausedSubscription'
    ];

}