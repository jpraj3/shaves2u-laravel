<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class SalesReport extends Model {
    public $timestamps = false;
    protected $table = 'salereports';
    //
    protected $fillable = [
        'id',
        'badgeId',
        'agentName',
        'channelType',
        'eventLocationCode',
        'status',
        'deliveryId',
        'carrierAgent',
        'customerName',
        'deliveryAddress',
        'deliveryContact',
        'billingAddress',
        'billingContact',
        'email',
        'productCategory',
        'sku',
        'qty',
        'productName',
        'currency',
        'paymentType',
        'region',
        'category',
        'saleDate',
        'orderId',
        'bulkOrderId',
        'promoCode',
        'totalPrice',
        'subTotalPrice',
        'discountAmount',
        'shippingFee',
        'taxAmount',
        'grandTotalPrice',
        'source',
        'medium',
        'campaign',
        'term',
        'content',
        'created_at',
        'updated_at',
        'CountryId',
        'taxInvoiceNo',
        'MarketingOfficeId',
        'moCode',
        'taxRate',
        'isDirectTrial'
    ];
}
