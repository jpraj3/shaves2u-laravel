<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class SummarySalesReport extends Model
{
    protected $table = 'summary_salesreport';

    protected $fillable = [
        'OrderId','BulkOrderId','OrderNumber',
        'OrderStatus', 'SaleDate', 'CompletionDate', 'Region', 'Category', 'MOCode', 'BadgeId', 'AgentName', 'ChannelType', 'Event/LocationCode', 'DeliveryMethod',
        'TrackingNumber', 'UserID', 'CustomerName', 'Email', 
        'DeliveryAddress', 'DeliveryPostcode', 'DeliveryContactNumber', 'BillingAddress', 'BillingPostcode', 'BillingContactNumber',
        'ProductCategory', 
        'A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 
        'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 
        'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY',
        'UnitTotal', 'PaymentType', 'CardBrand', 'CardType', 'Currency', 'PromoCode', 'TotalPrice',
        'DiscountAmount', 'CashRebate', 'SubTotalPrice', 'ProcessingFee', 'GrandTotalWithTax', 'GrandTotalWithoutTax', 'TaxAmount', 'PaymentStatus',
        'ChargeID', 'TaxInvoicenumber', 
        'Source', 'Medium', 'Campaign', 'Term', 'Content'

    ];
}
