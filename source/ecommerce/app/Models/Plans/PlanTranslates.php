<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanTranslates extends Model
{
    protected $table = 'plantranslates';
    //
    protected $fillable = [
        'name',
        'translates',
        'trialPrice',
        'langCode',
        'PlanId'
    ];
}
