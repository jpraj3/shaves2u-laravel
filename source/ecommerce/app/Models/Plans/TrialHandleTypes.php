<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class TrialHandleTypes extends Model
{
    protected $table = 'trial_handle_types';
    //
    protected $fillable = [
        'ProductIds',
        'CountryId',
    ];
}
