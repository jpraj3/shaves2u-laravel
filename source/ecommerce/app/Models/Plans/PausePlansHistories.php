<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PausePlanHistories extends Model
{
    protected $table = 'pause_plan_histories';
    //
    protected $fillable = [
        'subscriptionIds',
        'originaldate',
        'resumedate',
        'pausemonth',
    ];
}
