<?php

namespace App\Models\Plans;

use Illuminate\Database\Eloquent\Model;

class PlanImages extends Model
{
    //
 protected $table = 'planimages';
 public $timestamps = false;
    protected $fillable = [
        'url',
        'isDefault',
        'PlanId'
    ];

}
