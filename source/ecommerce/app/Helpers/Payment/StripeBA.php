<?php

namespace App\Helpers\Payment;

use App\Helpers\LaravelHelper;
use App\Models\Cards\Cards;
use App\Models\GeoLocation\Countries;
use App\Models\Subscriptions\Subscriptions;
use App\Services\APIStripeService;
use Exception;
use \Stripe\Charge as StripeCharge;
use \Stripe\Customer as StripeCustomerHelper;
use \Stripe\PaymentIntent as PaymentIntents;
use \Stripe\Stripe as StripeHelper;
use \Stripe\Token as StripeToken;
use \Stripe\Refund as StripeRefund;

class StripeBA
{
    public $type;
    public $countryCode;

    // Build All stripe data here
    public function __construct()
    {
        $this->stripeHelper = new StripeHelper();
        $this->stripeHelperCustomer = new StripeCustomerHelper();
        $this->stripeHelperCharge = new StripeCharge();
        $this->stripeToken = new StripeToken();
        $this->paymentIntents = new PaymentIntents();
        $this->stripeRefund = new StripeRefund();
        $this->stripeService = new APIStripeService();
    }

    public function initializeStripeService($type, $country, $country_code)
    {
        $this->stripeInfo = $this->stripeService->getActiveCountries($type, $country, $country_code);
        $this->countryCode = $this->stripeInfo["countryCode"];
        $this->type = $type;
        $this->laravelHelper = new LaravelHelper();
        $this->stripeEnabled = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'))->stripe;
        $this->getStripeService($this->countryCode, $this->type);
    }

    // Look for Stripe Service according to current Country
    public function getStripeService($countryCode, $type)
    {
        //$this->stripe_config = $this->stripeEnabled->MYS->ecommerce;
        $this->stripe_config = $this->stripeEnabled->$countryCode->$type;
        $this->stripeKey = $this->stripe_config->secretKey;

        $this->stripeHelper->setApiKey($this->stripeKey);
        if ($this->stripe_config) {
            return $this->stripe_config;
        } else {
            $default = $this->stripeEnabled->default->ecommerce;
            $this->stripe_config = $default;
            return $this->stripe_config;
        }
    }

    // ====================================================================================================================

    // [Stripe][Customer] Create
    public function createCustomer($type, $CountryId, $data, $createdCard)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));

        $createdCustomer = $this->stripeHelperCustomer->create([
            "description" => "[Shaves2U] - | Type: " . $type . " | User: " . $data["user"]["email"],
            "email" => $data["user"]["email"],
            "name" => $data["user"]["firstName"],
            "phone" => $data["user"]["phone"],
            "source" => $createdCard["id"],
        ]);
        $data["customer"] = $createdCustomer;
        $data["card_details"] = $data["add_card"];
        return $data;
    }
    // ====================================================================================================================

    // [Stripe][Card] Create
    public function createToken($card_details)
    {
        $createdCard = $this->stripeToken->create([
            //'customer' => $create_card["customer"]["customer"]["id"],
            'card' => [
                'number' => $card_details->get('card_number'),
                'exp_month' => $card_details->get('card_expiry_month'),
                'exp_year' => $card_details->get('card_expiry_year'),
                'cvc' => $card_details->get('card_cvv'),
                'name' => $card_details->get('card_name'),
            ],
        ]);
        return $createdCard;
    }

    public function createTokenV2($card_details)
    {
        return $card_details;
        $createdCard = $this->stripeToken->create([
            'card' => [
                'number' => $card_details->number,
                'exp_month' => $card_details->exp_month,
                'exp_year' => $card_details->exp_year,
                'cvc' => $card_details->cvv,
                'name' => $card_details->name,
            ],
        ]);
        return $createdCard;
    }

    public function createTokenV3_API($type, $CountryId, $card_details)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));
        $createdCard = $this->stripeToken->create([
            'card' => [
                'number' => (int) $card_details["card_number"],
                'exp_month' => (int) $card_details["card_expiry_month"],
                'exp_year' => (int) $card_details["card_expiry_year"],
                'cvc' => (int) $card_details["card_cvv"],
                'name' => $card_details["card_name"],
            ],
        ]);
        return $createdCard;
    }

    public function OneTimeChargeAPI($type, $platform, $user, $CountryId, $cardId, $subscriptionId = null)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($platform, $country, strtoupper($country->code));
        $amount_in_cents = 0.5 * 100;
        $stripe_customer = [];
        if ($subscriptionId !== null && $type === 'new') {
            $customerId = Cards::where('id', $cardId)
                ->select('customerId', 'risk_level')
                ->first();

            if ($customerId) {
                $stripe_customer = $this->stripeHelperCustomer::retrieve($customerId->customerId);
            }

            $paymentIntents = $this->paymentIntents->create([
                'amount' => $amount_in_cents,
                'currency' => 'usd',
                'customer' => $stripe_customer,
                'description' => 'Shaves2u OTP - Update Payment for Sub ID : [' . $subscriptionId . ']',
                'receipt_email' => $user->email,
                'save_payment_method' => true,
                'payment_method_types' => ['card'],
                'payment_method' => $stripe_customer->sources->data[0]["id"],
                'metadata' => ['risk_level' => $customerId->risk_level],
            ]);

            return $paymentIntents;
        } else if ($cardId !== null && $type === 'new') {
            $customerId = Cards::where('id', $cardId)
                ->select('customerId', 'risk_level')
                ->first();
            if ($customerId) {
                $stripe_customer = $this->stripeHelperCustomer::retrieve($customerId->customerId);
            }

            $paymentIntents = $this->paymentIntents->create([
                'amount' => $amount_in_cents,
                'currency' => 'usd',
                'customer' => $stripe_customer,
                'description' => 'Shaves2u One Time Payment',
                'receipt_email' => $user->email,
                'save_payment_method' => true,
                'payment_method_types' => ['card'],
                'payment_method' => $stripe_customer->sources->data[0]["id"],
                'metadata' => ['risk_level' => $customerId->risk_level],
            ]);

            return $paymentIntents;
        }
    }

    public function updateCustomerCard($customerId, $card_token_id)
    {
        $updatedCard = $this->stripeHelperCustomer->update(
            $customerId,
            ['source' => $card_token_id]
        );
        return $updatedCard;
    }

    // [Stripe][Customer] Get
    public function retrieveCustomer($type, $CountryId, $customerId)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));
        $get = $this->stripeHelperCustomer::retrieve($customerId);
        return $get;
    }

    // [Stripe][Customer] Update
    public function updateCustomer($customerId, $updatedData)
    {
        $update = $this->stripeHelperCustomer->update(
            $customerId,
            [
                // 'metadata' => $metadata,
            ]
        );

        return $update;
    }

    // [Stripe][Customer] Delete
    public function deleteCustomer($customerId)
    {

        $customer = $this->stripeHelperCustomer->retrieve($customerId);
        $customer->delete();

        return $customer;
    }

    // ====================================================================================================================

    // [Stripe][Charge] Create
    public function createCharge($chargeOptions, $countryCode, $type, $cardInfo, $customerInfo)
    {
        $chargeObject = $this->stripeHelperCharge->create([
            "amount" => 2000,
            "currency" => "usd",
            "source" => 'tok_visa', // obtained with Stripe.js
            "description" => "[" . $countryCode . "][" . $type . "] Charge for " . $chargeOptions["receipt_email"],
            "customer" => $chargeOptions["customer"],
            // "metadata" => $chargeOptions->metadata,
            "receipt_email" => $chargeOptions["receipt_email"],
            // "shipping" => $chargeOptions->shipping
        ]);
        return $chargeObject;
    }

    // [Stripe][Charge] Get
    public function retrieveCharge($type, $CountryId, $charge_id)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));

        $retrieveChargeObject = $this->stripeHelperCharge->retrieve($charge_id);

        return $retrieveChargeObject;
    }

    // [Stripe][Charge] Update
    public function updateCharge($charge_id)
    {
        $updateChargeObject = $this->StripeCharge->update(
            $charge_id,
            [
                'metadata' => ['order_id' => '6735'],
            ]
        );

        return $updateChargeObject;
    }

    // [Stripe][Charge] Retrieve Transaction
    public function retrieveBalanceTrxn($trxnId, $countryCode, $type)
    {
    }

    // ====================================================================================================================

    // Possible Uses of Stripe Helper

    public function listAllCustomers()
    {
        $allCustomers = $this->StripeCustomerHelper->all(["limit" => 3]);

        return $allCustomers;
    }

    public function listAllCharges()
    {
        $allCustomersCharge = $this->StripeCharge->all(["limit" => 3]);

        return $allCustomersCharge;
    }

    public function createPaymentIntents($type, $CountryId, $data, $required_multiplier)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));

        $multiplier = 1;
        if ($required_multiplier === true) {
            $multiplier = 100;
        }

        if ($data->payment_intents->customer == "") {
            $amount_in_cents = ($data->payment_intents->amount) * $multiplier;
            $paymentIntents = $this->paymentIntents->create([
                'amount' => $amount_in_cents,
                'currency' => $data->payment_intents->currency,
                //'confirm' => true,
            ]);
        } else {
            $amount_in_cents = ($data->payment_intents->amount) * $multiplier;

            $paymentIntents = $this->paymentIntents->create([
                'amount' => $amount_in_cents,
                'currency' => $data->payment_intents->currency,
                'customer' => $data->payment_intents->customer,
                'description' => 'Shaves2u Payment',
                'receipt_email' => 'superuser@shaves2u.com',
                'save_payment_method' => true,
                'payment_method_types' => ['card'],
                'payment_method' => $data->default_card_id,
                'metadata' => ['risk_level' => $data->payment_intents->risk_level],
                //'confirm' => true,
            ]);
        }

        return $paymentIntents;
    }

    public function updatePaymentIntents($data)
    {
        $payment_intent_id = $data->payment_intent_id;
        $update_type = $data->update_type;
        $CountryId = $data->user->CountryId;
        $type = $data->user->CountryId;

        // Retrieve PaymentIntent Info
        // $retrievePaymentIntentInfo = $this->retrievePaymentIntent($payment_intent_id);
        $retrievePaymentIntentInfo = $this->retrievePaymentIntents('baWebsite', $CountryId, $payment_intent_id);

        // Update
        $_updatePaymentIntent = $this->_updatePaymentIntent($data, $retrievePaymentIntentInfo, $update_type);
        return $_updatePaymentIntent;
    }

    public function retrievePaymentIntent($payment_intent_id)
    {
        $data = $this->paymentIntents->retrieve($payment_intent_id);
        return $data;
    }

    public function _updatePaymentIntent($data, $payment_intent_data, $update_type)
    {

        $customer_id = $data->stripe_customer->id;
        $card_id = $data->stripe_card->id;
        $payment_intent_id = $data->payment_intent_id;
        $payment_method_type = ['card'];
        $user_email = $data->user->email;
        $metadata = [
            'user' =>
            [
                'delivery_details' => '',
                'billing_details' => '',
            ],
        ];
        $description = 'Shaves2u Payment';
        $amount = '';

        if ($payment_intent_data) {
            // First Update
            // Payment Intent already has Customer Id connected.
            if (isset($payment_intent_data->customer) && $update_type === "first-update") {
                // Note: If the payment intent already got customer id before, we cannot replace it with different customer id
                $old_data = $this->paymentIntents->retrieve($payment_intent_id);
                $new_data = (object) array();
                $new_data->default_card_id = $card_id;
                $new_data->payment_intents = (object) array();
                $new_data->payment_intents->currency = $old_data->currency;
                $new_data->payment_intents->customer = $customer_id;
                $new_data->payment_intents->amount = $old_data->amount;
                $new_data->payment_intents->risk_level = 'normal';
                $updatedPaymentIntents = $this->createPaymentIntents('baWebsite', $data->user->CountryId, $new_data, false);
                $updatedPaymentIntents->is_replaced_card = true;
            }

            // Payment Intent does not have Customer Id connected.
            if (!isset($payment_intent_data->customer) && $update_type === "first-update") {
                $updatedPaymentIntents = $this->paymentIntents->update(
                    $payment_intent_id,
                    [
                        "customer" => $customer_id,
                        "description" => $description,
                        "payment_method_types" => $payment_method_type,
                        "payment_method" => $card_id,
                        "receipt_email" => $user_email,
                        'metadata' => ['risk_level' => 'normal'],
                    ]
                );
            }

            // PaymentIntent | Update Price
            if (isset($payment_intent_data->customer) && $update_type === "price-update") {
                $amount = $data->new_amount;
                $updatedPaymentIntents = $this->paymentIntents->update(
                    $payment_intent_id,
                    [
                        "description" => $description,
                        "amount" => $amount,
                    ]
                );
            }

            // PaymentIntent | Restart checkout journey
            // if (isset($payment_intent_data->customer) && $update_type === "restart-checkout-journey") {
            //     $amount = $data->new_amount;
            //     $updatedPaymentIntents = $this->paymentIntents->update(
            //         $payment_intent_id,
            //         [
            //             "description" => $description,
            //             "amount" => $amount,
            //             'metadata' => ['risk_level' => $data->stripe_card->risk_level],
            //         ]
            //     );
            // }

            // // PaymentIntent | Update Card
            // if (!isset($payment_intent_data->customer) && $update_type === "card-update") {
            //     $updatedPaymentIntents = $this->paymentIntents->update(
            //         $payment_intent_id,
            //         [
            //             "payment_method_types" => $payment_method_type,
            //             "payment_method" => $card_id,
            //         ]
            //     );
            // }

            return $updatedPaymentIntents;
        }
    }

    public function updatePaymentIntent_PaymentFailed($paymentIntentID, $customerId, $cardId)
    {
        $this->paymentIntents->update(
            $paymentIntentID,
            [
                "customer" => $customerId,
                "payment_method" => $cardId,
                'metadata' => ['risk_level' => 'highest'],
            ]
        );
    }

    public function updatePaymentIntent_PrePayment($paymentIntentID, $prepayment_data)
    {
        $CountryId = $prepayment_data->CountryId;
        $retrievePaymentIntentInfo = $this->retrievePaymentIntents('baWebsite', $CountryId, $paymentIntentID);

        $finalPrice = $prepayment_data->final_price * 100;
        if (isset($prepayment_data->subscriptionId)) {

            return $this->paymentIntents->update(
                $paymentIntentID,
                [
                    "description" => "Shaves2u charge for orderId: " . $this->countryCode . "000000" . $prepayment_data->orderId,
                    "amount" => $finalPrice,
                    'metadata' => [
                        'subscriptionid' => $prepayment_data->subscriptionId,
                        'orderid' => $prepayment_data->orderId,
                        'receiptid' => $prepayment_data->receiptId,
                    ],
                ]
            );
        } else {

            $update_payment_intent = $this->paymentIntents->update(
                $paymentIntentID,
                [
                    "description" => "Shaves2u charge for orderId: " . $this->countryCode . "000000" . $prepayment_data->orderId,
                    "amount" => $finalPrice,
                    'metadata' => [
                        'orderid' => $prepayment_data->orderId,
                        'receiptid' => $prepayment_data->receiptId,
                    ],
                ]
            );
            return $update_payment_intent;
        }
    }

    public function retrievePaymentIntents($type, $CountryId, $payment_intent_id)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));
        $data = $this->paymentIntents->retrieve($payment_intent_id);
        return $data;
    }

    public function confirmPaymentIntents($type, $CountryId, $paymentIntentID, $return_url)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));

        $intent = $this->paymentIntents->retrieve($paymentIntentID);
        $confirmedPayment = $intent->confirm([
            'payment_method' => $intent->payment_method,
            'return_url' => $return_url,
        ]);
        return $confirmedPayment;
    }

    public function getStripeSecretKey()
    {
        return $this->stripe_config["secretKey"];
    }

    public function createAndConfirmPaymentIntent($type, $CountryId, $customer_id, $currency, $amount, $return_url, $subscriptionBillingEmail)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));
        $chargeDescription = 'Shaves2U - BaWebsite';
        if ($type === 'ecommerce') {
            $chargeDescription = 'Shaves2U - Subscription Charge';
        }
        // email
        if (!isset($subscriptionBillingEmail) || $subscriptionBillingEmail == null) {
            $subscriptionBillingEmail = 'undefined@undefined.com';
        } else {
            $subscriptionBillingEmail = $subscriptionBillingEmail;
        }
        $customer = $this->stripeHelperCustomer::retrieve($customer_id);
        $card_id = $customer->default_source;
        $amount_in_cents = $amount * 100;
        $paymentIntents = $this->paymentIntents->create([
            'amount' => $amount_in_cents,
            'currency' => $currency,
            'customer' => $customer_id,
            'description' => $chargeDescription,
            'receipt_email' => $subscriptionBillingEmail,
            'save_payment_method' => true,
            'payment_method_types' => ['card'],
            'payment_method' => $card_id,
            'metadata' => ['risk_level' => 'normal'],
            'return_url' => $return_url,
            'confirm' => true,
        ]);
        return $paymentIntents;
    }

    public function createAndConfirmPaymentIntentV2($subscriptionId, $type, $CountryId, $customer_id, $currency, $amount, $return_url, $subscriptionBillingEmail)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));
        $chargeDescription = 'Shaves2U - BaWebsite';
        if ($type === 'ecommerce') {
            $chargeDescription = 'Shaves2U - Subscription Charge';
        }
        // email
        if (!isset($subscriptionBillingEmail) || $subscriptionBillingEmail == null) {
            $subscriptionBillingEmail = 'undefined@undefined.com';
        } else {
            $subscriptionBillingEmail = $subscriptionBillingEmail;
        }
        $customer = $this->stripeHelperCustomer::retrieve($customer_id);
        $card_id = $customer->default_source;
        $amount_in_cents = $amount * 100;
        $paymentIntents = $this->paymentIntents->create([
            'amount' => $amount_in_cents,
            'currency' => $currency,
            'customer' => $customer_id,
            'description' => $chargeDescription,
            'receipt_email' => $subscriptionBillingEmail,
            'save_payment_method' => true,
            'payment_method_types' => ['card'],
            'payment_method' => $card_id,
            'metadata' => ['risk_level' => 'normal'],
            'return_url' => $return_url,
            'confirm' => true,
        ]);

        // if ($paymentIntents) {
        //     try {
        //         Subscriptions::where('id', $subscriptionId)->update(['last_payment_intent' => $paymentIntents->id]);

        //         $confirm_payment_intent = \Stripe\PaymentIntent::retrieve($paymentIntents->id);
        //         $confirm_payment_intent->confirm([
        //             'payment_method' => $paymentIntents->payment_method,
        //         ]);

        //         $confirmed = \Stripe\PaymentIntent::retrieve($paymentIntents->id);
        //         return $confirmed;
        //     } catch (Exception $ex) {
        //         var_dump(json_encode($ex->getMessage()));
        //         return $ex->getMessage();
        //     }
        // }
    }

    public function refundOrderChargeFromAdminPanel($type, $CountryId, $charge_id, $amount = null)
    {
        $country = Countries::where('id', $CountryId)->first();
        $this->initializeStripeService($type, $country, strtoupper($country->code));

        if ($amount === null) {
            try {
                $refund = $this->stripeRefund::create([
                    'charge' => $charge_id,
                ]);

                return $refund;
            } catch (Exception $ex) {
                return "refund_fail";
            }
        } else {

            try {
                $refund = $this->stripeRefund::create([
                    'charge' => $charge_id,
                    'amount' => $amount,
                ]);

                return $refund;
            } catch (Exception $ex) {
                return "partial_refund_fail";
            }
        }
    }
}
