<?php

namespace App\Helpers\Payment;

use App\Helpers\LaravelHelper;
use GuzzleHttp\Client;

class NicePayHelper
{
    public function __construct()
    {
        $this->laravelHelper = new LaravelHelper();
        $this->nicepayEnabled = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'))->nicepay;
    }

    public function linkingIMP()
    {
        $nicepay_config = $this->nicepayEnabled->default->direct;
        $impkey = $nicepay_config->imp_key;
        $impsecret = $nicepay_config->imp_secret;
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $iamporttoken = [
            "imp_key" => $impkey,
            "imp_secret" => $impsecret,
        ];
        try {
            $iamporttokenjson = json_encode($iamporttoken);
            $response = $client->postAsync('https://api.iamport.kr/users/getToken', ['body' => $iamporttokenjson]);
            //$result = $response->getBody()->getContents();
            $response = $response->wait();
            $result = json_decode($response->getBody(), true);
            if ($response->getStatusCode() !== 200) {
                $datalist = [
                    "data" => $result,
                    "status" => "error",
                ];
            } else {
                if ($result) {
                    $datalist = [
                        "data" => $result,
                        "status" => "success",
                    ];
                } else {
                    $datalist = [
                        "data" => $result,
                        "status" => "error",
                    ];
                }
            }
            return $datalist;
        } catch (\Exception $e) {
            $errorlist = [
                "data" => $e->getMessage(),
                "status" => "error",
            ];
            return $errorlist;
        }
    }

    public function createBillingKey($data)
    {
        $randomstring = $this->random_str();
        $customerid = "np_" . $randomstring . time();

        $expiry = $data["card_expiry_year"] . "-" . $data["card_expiry_month"];
        $linkIMP = $this->linkingIMP();
        if ($linkIMP['status'] != "error" && $linkIMP['data']) {
            if ($linkIMP['data']['response']) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);
                $datalist = [
                    "customer_uid" => $customerid,
                    "card_number" => $data["card_number"],
                    "expiry" => $expiry,
                    "birth" => $data["card_birth"],
                    "pwd_2digit" => $data["card_password"],
                    "customer_name" => $data["customer_name"],
                    "customer_email" => $data["customer_email"],
                ];

                $access_token = $linkIMP['data']['response']['access_token'];
                try {
                    $datalist = json_encode($datalist);
                    $response = $client->postAsync('https://api.iamport.kr/subscribe/customers/' . $customerid . '?_token=' . $access_token, ['body' => $datalist]);
                    $response = $response->wait();

                    $result = json_decode($response->getBody(), true);
                    if ($response->getStatusCode() !== 200) {
                        $datalist = [
                            "data" => $result,
                            "status" => "error",
                        ];
                    } else {
                        if ($result) {
                            if ($result['code'] === 0) {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "success",
                                ];
                            } else {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "error",
                                ];
                            }
                        } else {
                            $datalist = [
                                "data" => $result,
                                "status" => "error",
                            ];
                        }
                    }
                    return $datalist;
                } catch (\Exception $e) {
                    $errorlist = [
                        "data" => $e->getMessage(),
                        "status" => "error",
                    ];
                    return $errorlist;
                }

            }
        }
    }

    public function nicePayPayment($data)
    {
        $linkIMP = $this->linkingIMP();
        if ($linkIMP['status'] != "error" && $linkIMP['data']) {
            if ($linkIMP['data']['response']) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);
                $datalist = [
                    "customer_uid" => $data["customer_uid"],
                    "merchant_uid" => $data["merchant_uid"],
                    "amount" => $data["amount"],
                    "name" => $data["customer_name"],
                    "buyer_email" => $data["customer_email"],
                ];

                $access_token = $linkIMP['data']['response']['access_token'];
                try {
                    $datalist = json_encode($datalist);
                    $response = $client->postAsync('https://api.iamport.kr/subscribe/payments/again?_token=' . $access_token, ['body' => $datalist]);
                    $response = $response->wait();

                    $result = json_decode($response->getBody(), true);
                    if ($response->getStatusCode() !== 200 || $result['response'] == "failed") {
                        $datalist = [
                            "data" => $result,
                            "status" => "error",
                        ];
                    } else {
                        if ($result) {
                            if ($result['code'] === 0) {
                                if ($result['response']['status'] == "failed") {
                                    $datalist = [
                                        "data" => $result,
                                        "status" => "error",
                                    ];
                                } else {
                                    $datalist = [
                                        "data" => $result,
                                        "status" => "success",
                                    ];
                                }
                            } else {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "error",
                                ];
                            }
                        } else {
                            $datalist = [
                                "data" => $result,
                                "status" => "error",
                            ];
                        }
                    }
                    return $datalist;
                } catch (\Exception $e) {
                    $errorlist = [
                        "data" => $e->getMessage(),
                        "status" => "error",
                    ];
                    return $errorlist;
                }

            }
        }
    }

    public function cancelNicePayOrder($data)
    {
        $linkIMP = $this->linkingIMP();
        if ($linkIMP['status'] != "error" && $linkIMP['data']) {
            if ($linkIMP['data']['response']) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);

                $access_token = $linkIMP['data']['response']['access_token'];
                try {
                    $datalist = json_encode($data);
                    $response = $client->postAsync('https://api.iamport.kr/payments/cancel?_token=' . $access_token, ['body' => $datalist]);
                    $response = $response->wait();

                    $result = json_decode($response->getBody(), true);
                    if ($response->getStatusCode() !== 200) {
                        $datalist = [
                            "data" => $result,
                            "status" => "error",
                        ];
                    } else {
                        if ($result) {
                            if ($result['code'] === 0) {

                                $datalist = [
                                    "data" => $result,
                                    "status" => "success",
                                ];

                            } else {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "error",
                                ];
                            }
                        } else {
                            $datalist = [
                                "data" => $result,
                                "status" => "error",
                            ];
                        }
                    }
                    return $datalist;
                } catch (\Exception $e) {
                    $errorlist = [
                        "data" => $e->getMessage(),
                        "status" => "error",
                    ];
                    return $errorlist;
                }

            }
        }
    }

    public function getPaymentDetailByimp_uid($data)
    {
        $linkIMP = $this->linkingIMP();
        if ($linkIMP['status'] != "error" && $linkIMP['data']) {
            if ($linkIMP['data']['response']) {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                ]);
                $datalist = [
                    "imp_uid" => "imps_051224912883",
                ];
                $imp_uid = 'imps_051224912883';
                $access_token = $linkIMP['data']['response']['access_token'];
                try {
                    $datalist = json_encode($datalist);
                    $response = $client->postAsync('https://api.iamport.kr/payments/' . $imp_uid . '?_token=' . $access_token, ['body' => $datalist]);
                    $response = $response->wait();

                    $result = json_decode($response->getBody(), true);
                    if ($response->getStatusCode() !== 200) {
                        $datalist = [
                            "data" => $result,
                            "status" => "error",
                        ];
                    } else {
                        if ($result) {
                            if ($result['code'] === 0) {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "success",
                                ];
                            } else {
                                $datalist = [
                                    "data" => $result,
                                    "status" => "error",
                                ];
                            }
                        } else {
                            $datalist = [
                                "data" => $result,
                                "status" => "error",
                            ];
                        }
                    }
                    return $datalist;
                } catch (\Exception $e) {
                    $errorlist = [
                        "data" => $e->getMessage(),
                        "status" => "error",
                    ];
                    return $errorlist;
                }

            }
        }
    }

    public function random_str()
    {
        $keyspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < 7; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

}
