<?php

namespace App\Helpers;

use App\Http\Controllers\Globals\Utilities\SessionController;
use App\Models\Plans\PlanDetails;
use App\Models\Products\Product;
use App\Models\Products\ProductBundles;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductImage;
use App\Models\Products\ProductTranslate;
use App\Models\Products\ProductType;
use App\Models\Products\ProductTypeDetail;
use App\Models\Products\ProductTypeTranslate;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Services\CountryService as CountryService;

/*
use App\Helpers\PlanHelper;
public $plan;
$this->plan = new PlanHelper();

$this->plan->getPlanId($data);
 */

class ProductHelper
{
    public function __construct()
    {
        $this->country_service = new CountryService();
        $this->session = new SessionController();
        $this->lang_code = strtoupper(app()->getLocale());
        $this->country_info = $this->country_service->getCountryAndLangCode();
        $this->country_id = $this->country_info['country_id'];
        $this->currency = $this->country_service->getCurrencyDisplay();
        $this->appType = config('app.appType');
        $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;
    }
    // no using
    public function getProductByPlanID($data)
    {
        $planid = 2;
        $LangCode = 'EN';
        $productlist = PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->select('productcountries.*', 'products.*')
            ->where('plan_details.PlanId', $planid) // plan id
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->orderByRaw("productcountries.id asc")
            ->get()->toArray();

        $productimages = PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('productimages', 'productimages.ProductId', 'products.id')
            ->select('productimages.*')
            ->where('plan_details.PlanId', $planid) // plan id
            ->orderByRaw("productcountries.id asc, productimages.ProductId asc")
            ->get();
        if ($productimages) {
            for ($i = 0; $i < count($productlist); $i++) {
                for ($a = 0; $a < count($productimages); $a++) {
                    if ($productlist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                        $productlist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                        $productlist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                    }
                }
            }

            return $productlist;
        }
    }
    public function getProductByProductType($CountryId, $LangCode, $data)
    {

        $producttypeid = $data["ProductTypeId"];

        $producttypearray = "";

        if (is_array($producttypeid)) {
            $producttypearray = $producttypeid;
        } else {
            $producttypearray = array('producttypeid' => $producttypeid);
            // array_push($test,$producttypeid );
        }

        $productlist = ProductTypeDetail::join('products', 'products.id', 'producttypedetails.ProductId')
            ->leftjoin('productdetails', 'productdetails.ProductId', 'products.id')
            ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
        //->join('producttypetranslates', 'producttypetranslates.ProductTypeId', 'producttypes.id')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->join('productcountries', 'productcountries.ProductId', 'products.id')
            ->select('producttypes.name', 'producttypes.id as ProductTypeId', 'products.id as ProductId', 'producttranslates.name as producttranslatesname', 'producttranslates.description as producttranslatesdescription', 'producttranslates.shortDescription as producttranslatesshortDescription', 'producttranslates.langCode as producttranslateslangCode', 'producttranslates.seoTitle as producttranslatesseoTitle', 'productcountries.id as productcountriesid', 'productcountries.*', 'productdetails.*',
            'products.sku', 'products.rating', 'products.isFeatured', 'products.status', 'products.created_at', 'products.updated_at', 'products.slug', 'products.order', 'products.isFeaturedProduct', 'products.isFeaturedAsk')
            ->whereIn('producttypedetails.ProductTypeId', $producttypearray) // product type id
            ->where('productdetails.langCode', $LangCode) // product type id
        //->where('producttypetranslates.langCode', $LangCode)  // product type id
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->where('productcountries.CountryId', $CountryId) // country
            ->where('productcountries.isOnline', 1) // country
        //->where('products.isFeaturedProduct', 1)  // country
            ->orderBy('products.id','ASC')
            ->get()->toArray();

        $productimages = ProductTypeDetail::join('products', 'products.id', 'producttypedetails.ProductId')
            ->leftjoin('productdetails', 'productdetails.ProductId', 'products.id')
            ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
        //->join('producttypetranslates', 'producttypetranslates.ProductTypeId', 'producttypes.id')
            ->join('productimages', 'productimages.ProductId', 'products.id')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->join('productcountries', 'productcountries.ProductId', 'products.id')
            ->select('productimages.*')
            ->whereIn('producttypedetails.ProductTypeId', $producttypearray) // product type id
            ->where('productdetails.langCode', $LangCode) // product type id
        //->where('producttypetranslates.langCode', $LangCode)  // product type id
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->where('productcountries.CountryId', $CountryId) // country
        //->where('products.isFeaturedProduct', 1)  // country
            ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
            ->get();

        if ($productimages) {
            for ($i = 0; $i < count($productlist); $i++) {
                for ($a = 0; $a < count($productimages); $a++) {
                    if ($productlist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                        if ($productimages[$a]["isDefault"] == "1") {
                            $productlist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                        }
                        $productlist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                        $productlist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                    }
                }
            }
        }

        return $productlist;
    }
    public function getProductImages($data)
    {
        $ProductId = 1019;
        $productimages = ProductImage::where('ProductId', $ProductId)
            ->get();

        return $productimages;
    }
    public function getProductTranslate($data)
    {
        $ProductId = 1019;
        $producttranslate = ProductTranslate::where('ProductId', $ProductId)
            ->get();

        return $producttranslate;
    }

    public function getProductTypeTranslate($data)
    {
        $ProductTypeId = 1;
        $LangCode = 'EN';
        $ProductTypeTranslate = ProductTypeTranslate::where('ProductTypeId', 1)
            ->where('langCode', $LangCode) // product type id
            ->get();

        return $ProductTypeTranslate;
    }

    public function getProductCountryDetails($product_countries_array)
    {
        $product_country_details = ProductCountry::whereIn('id', $product_countries_array)->get();
        return $product_country_details;
    }

    public function getProductSumPrice($data)
    {
        $productcountyid = $data;
        $productcountyidarray = "";

        if (is_array($productcountyid)) {
            $productcountyidarray = $productcountyid;
        } else {
            $productcountyidarray = array('producttypeid' => $productcountyid);
        }
        $productlist = ProductCountry::whereIn('id', $productcountyidarray)->sum('sellPrice'); // plan ids list in array
        return $productlist;
    }

    public function getProductDefaultImageWithData($product_list)
    {
        $isDefaultImg = "";
        $build_data = [];
        foreach ($product_list as $p) {
            foreach ($p["productimageisDefault"] as $key => $x) {
                if ($x === 1) {
                    // image is default for the product
                    $isDefaultImg = $p["productimagesurl"][$key];
                }
            }
            $p["default_image_url"] = $isDefaultImg;
            array_push($build_data, $p);
        }

        return $build_data;
    }

    public function getProductByProductCountries($CountryId, $langCode, $productcountries_details)
    {
        if ($productcountries_details["appType"] === 'ecommerce') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOnline', $productcountries_details["isOnline"]) // isOnline
                ->where('products.status', 'active') //
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOnline', $productcountries_details["isOnline"]) // isOnline
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'baWebsite') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('countries', 'countries.id', 'productcountries.CountryId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOffline', $productcountries_details["isOffline"]) // isOffline
                ->where('products.status', 'active') //
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOffline', $productcountries_details["isOffline"]) // isOffline
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'job') {

            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('countries', 'countries.id', 'productcountries.CountryId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('products.status', 'active') //
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }
            return $productcountrylist;
        }



    }

    public function getProductByProductCountriesV3($CountryId, $langCode, $productcountries_details)
    {
        $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            // ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
            ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
            // ->where('productdetails.langCode', strtoupper($langCode)) // product type id
            ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
            ->where('productcountries.CountryId', $CountryId) // country
            ->where('products.status', 'active') //
            ->select(
                'productcountries.id as ProductCountryId',
                'producttranslates.id as ProductTranslateId',
                // 'productdetails.id as ProductDetailId',
                'products.id as ProductId',
                'producttranslates.name as producttranslatesname',
                'producttranslates.description as producttranslatesdescription',
                'producttranslates.shortDescription as producttranslatesshortDescription',
                'producttranslates.langCode as producttranslateslangCode',
                'producttranslates.seoTitle as producttranslatesseoTitle',
                'products.*', 'productcountries.*',
                // 'productdetails.*',
                'producttranslates.*'
            )
            ->get()->toArray();

        $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
            ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
            ->select('productimages.*')
            ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
            ->where('productdetails.langCode', strtoupper($langCode)) // product type id
            ->where('productcountries.CountryId', $CountryId) // country
            ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
            ->get()->toArray();

        if ($productimages) {
            for ($i = 0; $i < count($productcountrylist); $i++) {
                for ($a = 0; $a < count($productimages); $a++) {
                    if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                        if ($productimages[$a]["isDefault"] == "1") {
                            $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                        }
                        $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                        $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                    }
                }
            }
        }

        return $productcountrylist;

    }

    public function getProductByProductCountriesV2($CountryId, $productcountries_details)
    {
        if ($productcountries_details["appType"] === 'ecommerce') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            // ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttypedetails', 'producttypedetails.ProductId', 'productcountries.ProductId')
                ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOnline', $productcountries_details["isOnline"]) // isOnline
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId', 'producttypes.name',
                    'products.*', 'productcountries.*',
                    'producttypes.*',
                    'producttypedetails.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            // ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttypedetails', 'producttypedetails.ProductId', 'productcountries.ProductId')
                ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOnline', $productcountries_details["isOnline"]) // isOnline
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'baWebsite') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            // ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttypedetails', 'producttypedetails.ProductId', 'productcountries.ProductId')
                ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOffline', $productcountries_details["isOffline"]) // isOnline
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId', 'producttypes.name',
                    'products.*', 'productcountries.*',
                    'producttypes.*',
                    'producttypedetails.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
            // ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttypedetails', 'producttypedetails.ProductId', 'productcountries.ProductId')
                ->join('producttypes', 'producttypes.id', 'producttypedetails.ProductTypeId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->where('productcountries.isOnline', $productcountries_details["isOnline"]) // isOnline
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'job') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->select(
                    'productcountries.id as ProductCountryId',
                    'products.id as ProductId',
                    'products.*', 'productcountries.*'
                )
                ->get()->toArray();

            return $productcountrylist;
        }

    }

    public function getProductByProductCountriesV4($CountryId, $langCode, $productcountries_details)
    {
        if ($productcountries_details["appType"] === 'ecommerce') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'baWebsite') {
            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('countries', 'countries.id', 'productcountries.CountryId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }

            return $productcountrylist;
        }

        if ($productcountries_details["appType"] === 'job') {

            $productcountrylist = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('countries', 'countries.id', 'productcountries.CountryId')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('producttranslates.langCode', strtoupper($langCode)) // translate lang code
                ->where('productcountries.CountryId', $CountryId) // country
                ->select(
                    'productcountries.id as ProductCountryId',
                    'producttranslates.id as ProductTranslateId',
                    'productdetails.id as ProductDetailId',
                    'products.id as ProductId',
                    'producttranslates.name as producttranslatesname',
                    'producttranslates.description as producttranslatesdescription',
                    'producttranslates.shortDescription as producttranslatesshortDescription',
                    'producttranslates.langCode as producttranslateslangCode',
                    'producttranslates.seoTitle as producttranslatesseoTitle',
                    'products.*', 'productcountries.*',
                    'productdetails.*',
                    'producttranslates.*'
                )
                ->get()->toArray();

            $productimages = ProductCountry::join('products', 'products.id', 'productcountries.ProductId')
                ->leftjoin('productdetails', 'productdetails.ProductId', 'productcountries.ProductId')
                ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
                ->select('productimages.*')
                ->whereIn('productcountries.id', $productcountries_details["product_country_ids"]) // product type id
                ->where('productdetails.langCode', strtoupper($langCode)) // product type id
                ->where('productcountries.CountryId', $CountryId) // country
                ->orderByRaw("productimages.isDefault desc", "productimages.ProductId asc")
                ->get()->toArray();

            if ($productimages) {
                for ($i = 0; $i < count($productcountrylist); $i++) {
                    for ($a = 0; $a < count($productimages); $a++) {
                        if ($productcountrylist[$i]["ProductId"] == $productimages[$a]["ProductId"]) {
                            if ($productimages[$a]["isDefault"] == "1") {
                                $productcountrylist[$i]["productDefaultImage"] = $productimages[$a]["url"];
                            }
                            $productcountrylist[$i]["productimagesurl"][] = $productimages[$a]["url"];
                            $productcountrylist[$i]["productimageisDefault"][] = $productimages[$a]["isDefault"];
                        }
                    }
                }
            }
            return $productcountrylist;
        }



    }

    public function getProductBundles($productIds)
    {
        if ($this->appType === 'ecommerce') {
            $bundles = ProductBundles::join('productbundledetails', 'productbundledetails.ProductBundleId', 'productbundles.id')
                ->leftJoin('productbundletranslates', 'productbundletranslates.ProductBundleId', 'productbundles.id')
                ->whereIn('productbundledetails.ProductCountryId', $productIds)
                ->where('productbundles.isActive', 1)
                ->where('productbundles.isOnline', 1)
                ->where('productbundledetails.isActive', 1)
            // ->whereTime('productbundledetails.startAt', '>=', Carbon::now())
            // ->whereTime('productbundledetails.expireAt', '>', Carbon::now())
                ->where('productbundletranslates.isDefault', 1)
                ->where('productbundletranslates.isActive', 1)
                ->where('productbundletranslates.isDefault', 1)
                ->where('productbundletranslates.langCode', $this->lang_code)
                ->select(
                    'productbundles.*',
                    'productbundledetails.id as ProductBundleDetailsId', 'productbundledetails.*',
                    'productbundletranslates.id as ProductBundleTranslatesId',
                    'productbundletranslates.isDefault as ProductBundleTranslates_isDefault',
                    'productbundletranslates.isActive as ProductBundleTranslates_isActive',
                    'productbundletranslates.*')
                ->get();

            return false;
        } elseif ($this->appType === 'baWebsite') {
            $bundles = ProductBundles::join('productbundledetails', 'productbundledetails.ProductBundleId', 'productbundles.id')
                ->leftJoin('productbundletranslates', 'productbundletranslates.ProductBundleId', 'productbundles.id')
                ->whereIn('productbundledetails.ProductCountryId', $productIds)
                ->where('productbundles.isActive', 1)
                ->where('productbundles.isOffline', 1)
                ->where('productbundledetails.isActive', 1)
            // ->whereTime('productbundledetails.startAt', '>=', Carbon::now())
            // ->whereTime('productbundledetails.expireAt', '>', Carbon::now())
                ->where('productbundletranslates.isDefault', 1)
                ->where('productbundletranslates.isActive', 1)
                ->where('productbundletranslates.isDefault', 1)
                ->where('productbundletranslates.langCode', $this->lang_code)
                ->select(
                    'productbundles.*',
                    'productbundledetails.id as ProductBundleDetailsId', 'productbundledetails.*',
                    'productbundletranslates.id as ProductBundleTranslatesId',
                    'productbundletranslates.isDefault as ProductBundleTranslates_isDefault',
                    'productbundletranslates.isActive as ProductBundleTranslates_isActive',
                    'productbundletranslates.*')
                ->get();

            return false;
        }
    }

    public function getSKUsList()
    {
        $skus = Product::select('sku')->groupBy('sku')->get();

        return $skus;
    }

    public function getProductWithoutImageByPlanID($data)
    {
        $planid = $data->id;
        $LangCode = $data->lang;
        $productlist = PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->select('productcountries.*', 'products.*', 'producttranslates.name as productname')
            ->where('plan_details.PlanId', $planid) // plan id
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->orderByRaw("productcountries.id asc")
            ->get();

        return $productlist;
    }
    public function getProductWithoutImageByAddOn($data)
    {
        $subscriptionId = $data->id;
        $LangCode = $data->lang;
        $productlist = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->select('productcountries.*', 'products.*', 'producttranslates.name as productname')
            ->where('subscriptions_productaddon.subscriptionId', $subscriptionId) // plan id
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->orderByRaw("productcountries.id asc")
            ->get();

        return $productlist;
    }

    public function getProductsBasedOnGender($options)
    {
        try {
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = config('global.all.product_category_types.men');
                    $gp_handle_type_names = config('global.all.product_type_names.men.handles');
                    $gp_blade_type_names = config('global.all.product_type_names.men.blades');
                    $gp_addon_type_names = config('global.all.product_type_names.men.addons');
                    $gp_ask_type_names = config('global.all.product_type_names.men.ask');
                    break;
                case 'female':
                    $gp_ctgry = config('global.all.product_category_types.women');
                    $gp_handle_type_names = config('global.all.product_type_names.women.handles');
                    $gp_blade_type_names = config('global.all.product_type_names.women.blades');
                    $gp_addon_type_names = config('global.all.product_type_names.women.addons');
                    $gp_ask_type_names = config('global.all.product_type_names.women.ask');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    $gp_handle_type_names = [config('global.all.product_type_names.men.handles'), config('global.all.product_type_names.women.handles')];
                    $gp_blade_type_names = [config('global.all.product_type_names.men.blades'), config('global.all.product_type_names.wpmen.blades')];
                    $gp_addon_type_names = [config('global.all.product_type_names.men.addons'), config('global.all.product_type_names.women.addons')];
                    $gp_ask_type_names = [config('global.all.product_type_names.men.ask'), config('global.all.product_type_names.women.ask')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = ProductType::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = ProductType::whereIn('id', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = ProductTypeDetail::join('productcountries', 'productcountries.ProductId', 'producttypedetails.ProductId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('producttypetranslates', 'producttypetranslates.ProductTypeId', 'producttypedetails.ProductTypeId')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->where('producttypetranslates.langCode', $options->langCode)
                ->where('productcountries.isOffline', 1)
                ->select(
                    'producttypedetails.id as ProductTypeDetailsId',
                    'productcountries.id as ProductCountryId',
                    'products.id as ProductId',
                    'producttranslates.id as ProductTranslateId',
                    'producttypetranslates.id as ProductTypeTranslateId',
                    'producttranslates.name as ProductName',
                    'producttypetranslates.name as ProductTypeName',
                    'productcountries.*',
                    'products.*'
                )
                ->get();

            $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_blade_type_names)
                ->where('productcountries.isOffline', 1)
                ->where('products.status', 'active')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'producttypes.name as ProductTypeName',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*', 'producttranslates.*')
                ->get();

            $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_handle_type_names)
                ->where('productcountries.isOffline', 1)
                ->where('products.status', 'active')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*', 'producttranslates.*')
                ->get();

            // $bag_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
            //     ->join('products', 'products.id', 'producttypedetails.ProductId')
            //     ->join('productcountries', 'productcountries.ProductId', 'products.id')
            //     ->join('productimages', 'productimages.ProductId', 'products.id')
            //     ->where('productimages.isDefault', 1)
            //     ->where('producttypes.name', 'Custom Plan Addons Bag')
            //     ->where('productcountries.isOffline', 1)
            //     ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
            //     ->where('productcountries.CountryId', $options->CountryId)
            //     ->select(
            //         'producttypedetails.id as ProductTypeDetailId',
            //         'producttypes.id as ProductTypeId',
            //         'products.id as ProductId',
            //         'productcountries.id as ProductCountryId',
            //         'productimages.url as product_default_image',
            //         'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*')
            //     ->get();

            $addon_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_addon_type_names)
                ->where('productcountries.isOffline', 1)
                ->where('products.status', 'active')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*', 'producttranslates.*')
                ->get();

            $ask_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_ask_type_names)
                ->where('productcountries.isOffline', 1)
                ->where('products.status', 'active')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*', 'producttranslates.*')
                ->get();

            $this->country_id = $options->CountryId;
            $this->country_service->country_id = $options->CountryId;
            $this->currency = $this->country_service->getCurrencyDisplay();

            $available_plans = (Object) array(
                // "active_products" => $p_relateds,
                "handle_products" => $handle_types,
                "blade_products" => $blade_types,
                // "bag_types" => $bag_types,
                "addon_products" => $addon_types,
                "ask_products" => $ask_types,
                "currency" => $this->currency,
                "country_id" => $this->country_id,
            );

            return response()->json($available_plans);

        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getProductsBasedOnGenderCount($options)
    {
        try {
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = config('global.all.product_category_types.men');
                    break;
                case 'female':
                    $gp_ctgry = config('global.all.product_category_types.women');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = ProductType::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = ProductType::whereIn('id', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = ProductTypeDetail::join('productcountries', 'productcountries.ProductId', 'producttypedetails.ProductId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->join('producttypetranslates', 'producttypetranslates.ProductTypeId', 'producttypedetails.ProductTypeId')
                ->whereIn('producttypedetails.ProductTypeId', $p_ctgry)
                ->where('productcountries.CountryId', $options->CountryId)
                ->where('producttranslates.langCode', $options->langCode)
                ->where('producttypetranslates.langCode', $options->langCode)
                ->where('productcountries.isOffline', 1)
                ->select('productcountries.id')
                ->count();

            $available_plans = (Object) array(
                "active_products" => $p_relateds,
            );

            return response()->json($available_plans);

        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getProductsBasedOnGenderBasicInfo($options)
    {
        try {
            $p_ctgry = [];
            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = config('global.all.product_category_types.men');
                    break;
                case 'female':
                    $gp_ctgry = config('global.all.product_category_types.women');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    break;
                default:
                    return null;
            }

            // get related categories
            $g_p_category_infos = ProductType::join('producttypetranslates', 'producttypetranslates.ProductTypeId', 'producttypes.id')
                ->whereIn('producttypes.id', $gp_ctgry)
                ->where('producttypetranslates.langCode', $options->langCode)
                ->select('producttypes.id as ProductTypeId', 'producttypetranslates.id as ProductTypeTranslateId', 'producttypes.*', 'producttypetranslates.*')
                ->get();

            return response()->json($g_p_category_infos);

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getProductCountriesBySKU($sku,$countryid)
    {
        try {
        $productlist = Product::join('productcountries', 'productcountries.ProductId', 'products.id')
            ->select('productcountries.id as pcid')
            ->where('products.sku', $sku) // sku name
            ->where('productcountries.CountryId', $countryid) // countryid
            ->first();

        return $productlist;
     } catch (Exception $e) {
        throw $e;
     }

    }

    // mask selection

    public function getFreeMaskSelection($CountryId, $langCode)
    {
        try {
        $mask = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
        ->join('products', 'products.id', 'producttypedetails.ProductId')
        ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
        ->join('productcountries', 'productcountries.ProductId', 'products.id')
        ->where('producttypes.name', "Mask")
        ->where('products.status', "active")
        ->where('productcountries.CountryId', $CountryId)
        ->where('producttranslates.langCode', $langCode)
        ->select('products.id as pid','products.sku','producttranslates.name','productcountries.id as pcid','productcountries.sellPrice','productcountries.qty','productcountries.maxQty')
        ->orderBy('productcountries.qty', 'DESC')
        ->get();
        return $mask;
       } catch (Exception $e) {
       throw $e;
       }
    }

    // mask selection
}
