<?php

namespace App\Helpers\Warehouse;

use AfterShip\Couriers;
use AfterShip\LastCheckPoint;
use AfterShip\Trackings;
use App\Helpers\LaravelHelper;
use App\Services\OrderService;
use Exception;
use App\Http\Controllers\Webhooks\AftershipWebhook;

function getAftershipConfig()
{
    try {
        return config('environment.CUSTOM_CONFIG_DATA.Aftership');
    } catch (Exception $ex) {
        $this->Log->error('Aftership | getAftershipConfig | error => ' . $ex->getMessage());
        return $ex->getMessage();
    }
}

class AftershipHelper
{
    public function __construct()
    {
        $this->Log = \Log::channel('cronjob');
        $this->laravelHelper = new LaravelHelper();
        $this->init = $this->laravelHelper->ConvertArraytoObject(getAftershipConfig());

        try {
            $this->orderService = new OrderService();
            $this->couriers = new \AfterShip\Couriers($this->init->apiKey);
            $this->trackings = new \AfterShip\Trackings($this->init->apiKey);
            $this->last_check_point = new \AfterShip\LastCheckPoint($this->init->apiKey);

            return $this->init;
        } catch (Exception $ex) {
            $this->Log->error('Aftership | Failed to Initialize | error => ' . $ex->getMessage());
            return $ex->getMessage();
        }
    }

    public function getEnabledCourierLists()
    {
        try {
            $response = $this->couriers->get();
            return $response;
        } catch (Exception $ex) {
            $this->Log->error('Aftership | getEnabledCourierLists | error => ' . $ex->getMessage());
            return $ex->getMessage();
        }
    }

    public function getSupportedCourierLists()
    {
        try {
            $response = $this->couriers->all();
            return $response;
        } catch (Exception $ex) {
            $this->Log->error('Aftership | getSupportedCourierLists | error => ' . $ex->getMessage());
            return $ex->getMessage();
        }
    }

    public function getCourierByTrackingNo($_tracking_no)
    {
        $response = $this->couriers->detect($_tracking_no);
        return $response;
    }

    public function getTrackingOneByOne($slug,$_tracking_no)
    {
        $response = $this->trackings->get($slug,$_tracking_no);
        return $response;
    }

    public function postTracking($options ,$codeIso, $isBulk)
    {
        $options = $this->laravelHelper->ConvertArraytoObject($options);
        $_formatted_order_id = $isBulk === true ? $this->orderService->formatAfterShipBulkOrderNumber($options->order, $codeIso, false) : $this->orderService->formatAfterShipOrderNumber($options->order, $codeIso, false);
        try {
            $tracking_info = [
                'slug' => $options->order->carrierAgent,
                'tracking_number' => $options->order->deliveryId,
                'smses' => [isset($options->order->phone) ? $options->order->phone: $options->shipment->contactNumber],
                'emails' => [$options->order->email],
                'order_id' => $_formatted_order_id,
                'order_id_path' => $this->init->path . $_formatted_order_id,
                'customer_name' => isset($options->user) === true ?  ($options->user->firstName) : null
            ];

            $response = $this->trackings->create($options->order->deliveryId, $tracking_info);
            return $response;
        } catch (Exception $ex) {
            $this->Log->error('Aftership | postTracking | Create tracking aftership error => ' . $_formatted_order_id . " " . $ex->getMessage());
            return $ex->getMessage();
        }
    }

    public function doGetTracking($options)
    {
        // options => https://docs.aftership.com/api/4/trackings/get-trackings
        try {
            $response = $this->trackings->all($options);
            return $response;

        } catch (Exception $ex) {
            $this->Log->error('Aftership | doGetTracking | GET tracking status after ship error => ' . $ex->getMessage());
            return $ex->getMessage();
        }

    }

    public function getTrackings($type, $limit = null, $pagination = null)
    {
        $this->Log->info('Aftership | getTrackings | start');
        $trackings = [];

        $options = [
            'page' => $pagination,
            'limit' => $limit,
            'tag' => $type, // options => Pending,  InfoReceived,  InTransit,  OutForDelivery,  AttemptFail,  Delivered,  Exception,  Expired
        ];

        if ($pagination === null) {
            unset($options['page']);
        }

        if ($limit === null) {
            unset($options['limit']);
        }

        try {
            $_results = $this->doGetTracking($options);
            $this->Log->info('Aftership | getTrackings | _results ' . json_encode($_results));
            if (count($_results["data"]) > 0) {
                // loggingHelper.log('info', 'getTrackings ===== ${JSON.stringify(result)';);
                $data = $_results["data"];
                $trackings = $_results["data"]["trackings"];

                if (count($_results["data"]) <= 100) {
                    return $trackings;
                } else {
                    $queries = [];
                    $totalpage = ceil(count($_results["data"]) / 100);
                    for ($page = 2; $page <= $totalpage; $page++) {
                        array_push($queries, (Object) array($page));
                    }
                    foreach ($queries as $query) {
                        $_results = $this->doGetTracking($query);
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Log->error('Aftership | getTrackings | error => ' . $ex->getMessage());
            return $ex->getMessage();
        }
    }
}
