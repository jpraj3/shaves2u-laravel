<?php

namespace App\Helpers\Warehouse;

use App;
use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Services\OrderService;
use App\Services\UrbanFoxService;
use GuzzleHttp\Client;
use Exception;
class UrbanFoxHelper
{

    public function __construct()
    {
        $this->urbanFoxService = new UrbanFoxService();
        $this->urbanfox = $this->urbanFoxService->getUrbanFoxConfigData();
        $this->client = $this->urbanFoxService->getUrbanFoxService($this->urbanfox->apiKey);
        $this->orderService = new OrderService();
        $this->orderHelper = new OrderHelper();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->err_msg = '[UrbanFox] OrderId Not Found in Database Record';
        $this->Log = \Log::channel('cronjob');
    }

    public function buildTransaction($query, $order_data, $isBulk)
    {
          // $this->Log->info('buildTransaction: ' . json_encode($query));
        $transaction = [
            'query' => $query,
        ];

        $build = ["query" => $query, "transaction" => json_encode($transaction)];

        return $this->sendDelivery($build, $order_data, $isBulk);
    }

    public function sendDelivery($data, $order_data, $isBulk)
    {
            // $this->Log->info('sendDelivery: ' . json_encode($data));
            $response = $this->client->postAsync($this->urbanfox->url, ['body' => $data["transaction"]]);
            $_response = $response->wait();
            $result = json_decode($_response->getBody(), true);
            $this->Log->info('sendDelivery -> order_data: ' . json_encode($order_data));
            if (!in_array("errors", $result)) {
                // Update Orders to Processing
                if ($isBulk === false) {
                    $this->Log->info('$order_data->order: ' . json_encode($order_data->order));
                    $update_if_status_payment_received = Orders::where('id', $order_data->order->id)->where('status','Payment Received')->first();
                    if($update_if_status_payment_received){
                        OrderHistories::where('OrderId', $order_data->order->id)->create([
                            'message' => 'Processing', 'isRemark' => 0, 'OrderId' => $order_data->order->id,
                        ]);
                    }
                    Orders::where('id', $order_data->order->id)->where('status','Payment Received')->update(['status' => 'Processing']);
                    
                } else {
                    $this->Log->info('$order_data->bulkorder: ' . json_encode($order_data->bulkorder));
                    BulkOrders::where('id', $order_data->bulkorder->id)->where('status','Payment Received')->update(['status' => 'Processing']);
                    BulkOrderHistories::where('BulkOrderId', $order_data->bulkorder->id)->create([
                        'message' => 'Processing', 'isRemark' => 0, 'OrderId' => $order_data->bulkorder->id,
                    ]);
                }
                $this->Log->info('result: ' . json_encode($result));
                return $result;
            } else {

                if ($result["errors"][0]["message"] === '"referenceNumber1" has already been taken') {
                    $_message = "Order Number already exists in UrbanFox";
                } else {
                    $_message = $result["errors"][0]["message"];
                }
                $errors = [
                    "error_message" => $_message,
                    "data" => $result,
                ];
                $this->Log->info('errors: ' . json_encode($errors));
                return $errors;
            }
        }

    public function createOrderInbound($order_id, $country_id, $isBulk)
    {
        // $this->Log->info('createOrderInbound: ' . json_encode($order_id));
        if ($order_id) {
            if (!$isBulk) {
                $order = Orders::where('id', $order_id)->where('status','Payment Received')->first();
                if ($order) {
                    $user = User::where('id', $order->UserId)->first();
                    // $country = Countries::where('id', $order->CountryId)->first();
                    $isPlan = $order->subscriptionIds !== null ? true : false;
                    $options = [
                        'OrderId' => $order_id,
                        'User' => $user,
                        'CountryId' => $country_id,
                        'LangCode' => strtoupper($user->defaultLanguage),
                        'isBA' => $order->SellerUserId !== null ? true : false,
                    ];

                    return $this->createTransactions($options, $isBulk, $isPlan);
                }else{
                    return "statuschange";
                }
            } else {
                $order = BulkOrders::where('id', $order_id)->where('status','Payment Received')->first();
                if ($order) {
                    $country = Countries::where('id', $order->CountryId)->first();
                    $isPlan = false;
                    $options = [
                        'OrderId' => $order_id,
                        'User' => null,
                        'CountryId' => $country_id,
                        'LangCode' => strtoupper($country->defaultLang),
                        'isBA' => $order->SellerUserId !== null ? true : false,
                    ];

                    return $this->createTransactions($options, $isBulk, $isPlan);
                }else{
                    return "statuschange";
                }
            }

        }

    }
    /**
     * Allow create multiple transaction to urbanfox
     * @param {*} options = {OrderId,User,CountryId,LangCode}, $isPlan, $isBulk
     */
    public function createTransactions($options, $isBulk, $isPlan)
    {
        $data = null;
        // $this->Log->info('createTransactions: ' . json_encode($options));
        if (!$isBulk) {
            $data = $this->orderHelper->buildWarehouseOrderDetails($options, $isPlan);
        } else {
            $data = $this->orderHelper->buildWarehouseBulkOrderDetails($options, $isPlan);
        }
        $data = $this->laravelHelper->ConvertArrayToObject($data);
        if ($data) {
            // shortened data
            $_user = $isBulk ? null : $data->user;
            $_warehouse_items = $data->generate_sales_order_data;
            $_shipment_info_d = $data->shipment_info->delivery;
            $_shipment_info_b = $data->shipment_info->billing !== null ? $data->shipment_info->billing : $data->shipment_info->delivery;
            $_metadata = $data->metadata;
            $receipt = $isBulk ? null : $data->receipt;
            $order = $isBulk ? $data->bulkorder : $data->order;
            $plan_detail = $isBulk ? null : $data->plan;
            $order_details = $isBulk ? $data->bulkorderdetails : $data->orderdetails;
            // build order items
            $order_items = [];
            $orderObjects = [];
            $total_price = array();
            foreach ($data->generate_sales_order_data->items as $item) {
                $__obj = (Object) array();
                $__obj->sku = $item->sku;
                $__obj->quantity = $item->qty;
                $__obj->serviceType = strip_tags("LITE");
                array_push($order_items, $__obj);
                array_push($total_price, $item->sellPrice);
            }
            // assumptions
            // 1. insert to db contact number format : +60101234343 -> remove + before sending to warehouse
            // format phone numbers
            $deliveryPhone = $billingPhone = '';
            $d_length = $d_plus_sign = $d_length_order = $d_plus_sign_order = $b_length = $b_plus_sign = false;

            $deliveryPhone = $_shipment_info_d ? $_shipment_info_d->contactNumber : '-';
            if(!isset($deliveryPhone) || $deliveryPhone == '-')
            {
                if(isset($order)){
                    $deliveryPhone = $order->phone ? $order->phone : '-';
                }
            }
            if(!isset($deliveryPhone) || $deliveryPhone == '-')
            {
                if(isset($_user)){
                    $deliveryPhone = $_user->phone ? $_user->phone : '-';
                }
            }

            $billingPhone = $_shipment_info_b ? $_shipment_info_b->contactNumber : '-';
            if(!isset($billingPhone) || $billingPhone == '-')
            {
                if(isset($order)){
                    $billingPhone = $order->phone ? $order->phone : '-';
                }
            }
            if(!isset($billingPhone) || $billingPhone == '-')
            {
                if(isset($_user)){
                    $billingPhone = $_user->phone ? $_user->phone : '-';
                }
            }
            // if ($order->phone != null) {
            //     // check if delivery & billing contact contains +
            //     if (strpos($order->phone, '+') !== false) {$d_plus_sign_order = true;} else { $order->phone = '+' . $order->phone; $d_plus_sign_order = true;}
            //     if (strpos($_shipment_info_b->contactNumber, '+') !== false) {$b_plus_sign = true;} else { $_shipment_info_b->contactNumber = '+' . $_shipment_info_b->contactNumber; $b_plus_sign = true;}
            //     // check if delivery & billing contact length is 10
            //     if ($d_plus_sign_order) {if (preg_match('/^\d{10}$/', explode("+", $order->phone)[1]) === 1) {$d_length_order = true;}}
            //     if ($b_plus_sign) {if (preg_match('/^\d{10}$/', explode("+", $_shipment_info_b->contactNumber)[1]) === 1) {$b_length = true;}}
            //     if ($d_plus_sign_order === true && $d_length_order === false ||
            //         $d_plus_sign_order === false && $d_length_order === true ||
            //         $d_plus_sign_order === true && $d_length_order === false ||
            //         !$order->phone
            //     ) {
            //         $deliveryPhone = 'N/A';
            //     } else if ($d_length === true && $d_plus_sign === true) {
            //         $deliveryPhone = preg_match('/^\d{10}$/', explode("+", $order->phone))[1] ? explode("+", $order->phone)[1] : $order->phone;
            //     }
            //     if ($b_plus_sign === true && $b_length === false ||
            //         $b_plus_sign === false && $b_length === true ||
            //         $b_plus_sign === true && $b_length === false ||
            //         !$_shipment_info_b->contactNumber
            //     ) {
            //         $billingPhone = 'N/A';
            //     } else if ($d_length === true && $d_plus_sign === true) {
            //         $billingPhone = preg_match('/^\d{10}$/', explode("+", $_shipment_info_b->contactNumber))[1] ? explode("+", $_shipment_info_b->contactNumber)[1] : $_shipment_info_b->contactNumber;
            //     }
            // } else {
            //     // check if delivery & billing contact contains +
            //     if (strpos($_shipment_info_d->contactNumber, '+') !== false) {$d_plus_sign = true;} else { $_shipment_info_d->contactNumber = '+' . $_shipment_info_d->contactNumber;
            //         $d_plus_sign = true;}
            //     if (strpos($_shipment_info_b->contactNumber, '+') !== false) {$b_plus_sign = true;} else { $_shipment_info_b->contactNumber = '+' . $_shipment_info_b->contactNumber;
            //         $b_plus_sign = true;}
            //     // check if delivery & billing contact length is 10
            //     if ($d_plus_sign) {if (preg_match('/^\d{10}$/', explode("+", $_shipment_info_d->contactNumber)[1]) === 1) {$d_length = true;}}
            //     if ($b_plus_sign) {if (preg_match('/^\d{10}$/', explode("+", $_shipment_info_b->contactNumber)[1]) === 1) {$b_length = true;}}
            //     if ($d_plus_sign === true && $d_length === false ||
            //         $d_plus_sign === false && $d_length === true ||
            //         $d_plus_sign === true && $d_length === false ||
            //         !$_shipment_info_d->contactNumber
            //     ) {
            //         $deliveryPhone = 'N/A';
            //     } else if ($d_length === true && $d_plus_sign === true) {
            //         $deliveryPhone = preg_match('/^\d{10}$/', explode("+", $_shipment_info_d->contactNumber)[1]) === 1 ? explode("+", $_shipment_info_d->contactNumber)[1] : $_shipment_info_d->contactNumber;
            //     }
            //     if ($b_plus_sign === true && $b_length === false ||
            //         $b_plus_sign === false && $b_length === true ||
            //         $b_plus_sign === true && $b_length === false ||
            //         !$_shipment_info_b->contactNumber
            //     ) {
            //         $billingPhone = 'N/A';
            //     } else if ($d_length === true && $d_plus_sign === true) {
            //         $billingPhone = preg_match('/^\d{10}$/', explode("+", $_shipment_info_b->contactNumber)[1]) ? explode("+", $_shipment_info_b->contactNumber)[1] : $_shipment_info_b->contactNumber;
            //     }
            // }
            // format customer name
            $billingName = $deliveryName = '';

            if (!$isBulk) {
                $deliveryName = $order->SellerUserId ? $order->fullName : $_shipment_info_d->firstName;
                $billingName = $order->SellerUserId ? $order->fullName : $_shipment_info_b->firstName;
            } else {
                $deliveryName = $_shipment_info_d->firstName;
                $billingName = $_shipment_info_b->firstName;
            }
            $deliveryName = str_replace('null', '', $deliveryName);
            $billingName = str_replace('null', '', $deliveryName);
            // format CountryId
            $d_country = Countries::findorfail($_shipment_info_d->CountryId);
            $b_country = Countries::findorfail($_shipment_info_b->CountryId);
            $order_country = Countries::findorfail($order->CountryId);
            $_orderObjects = str_replace('\\', '', json_encode($order_items, true));
            $_orderObjects = str_replace('"LITE"', 'LITE', $_orderObjects);
            $_orderObjects = str_replace('"sku"', 'sku', $_orderObjects);
            $_orderObjects = str_replace('"quantity"', 'quantity', $_orderObjects);
            $_orderObjects = str_replace('"serviceType"', 'serviceType', $_orderObjects);
            $transaction = '';
            if (!$isBulk) {
                // build input data & transaction
                $transaction = 'mutation _ {
                    createOrder(
                        referenceNumber1: "' . $this->orderService->formatOrderNumber($order, $order_country, true) . '",
                        remarks: "Fulfilment for order ' . $this->orderService->formatOrderNumber($order, $order_country, true) . '",
                        priceTotal: ' . number_format(floatval(array_sum($total_price)), 2, '.', '') . ',
                        doNumber: "",
                        orderItems: ' . $_orderObjects . ',
                        paymentType: CREDIT_CARD,
                        shippingMethod: UF_DELIVERY,
                        billingAddress: {
                            name: "' . $billingName . '",
                            addressLine1: "' . str_replace(array("\n"), ' ',  $_shipment_info_b->address) . '",
                            addressLine2: "' . $_shipment_info_b->city . '",
                            city: "' . $_shipment_info_b->city . '",
                            country: "' . $b_country->name . '",
                            countryCode: "' . $b_country->code . '",
                            postalCode: "' . (int) $_shipment_info_b->portalCode . '",
                            phone: "' . $billingPhone . '"
                        },
                        shippingAddress:{
                            name: "' . $deliveryName . '",
                            addressLine1: "' . str_replace(array("\n"), ' ',  $_shipment_info_d->address) . '",
                            addressLine2: "' . $_shipment_info_d->city . '",
                            city: "' . $_shipment_info_d->city . '",
                            country: "' . $d_country->name . '",
                            countryCode: "' . $d_country->code . '",
                            postalCode: "' . (int) $_shipment_info_d->portalCode . '",
                            phone: "' . $deliveryPhone . '"
                        },
                        customerAddress: {
                            name: "' . $deliveryName . '",
                            addressLine1: "' . str_replace(array("\n"), ' ',  $_shipment_info_d->address) . '",
                            addressLine2: "' . $_shipment_info_d->city . '",
                            city: "' . $_shipment_info_d->city . '",
                            country: "' . $d_country->name . '",
                            countryCode: "' . $d_country->code . '",
                            postalCode: "' . (int) $_shipment_info_d->portalCode . '",
                            phone: "' . $deliveryPhone . '"
                        },
                    ) { id }
                }';

                // save query into a text-file
                $path = config('environment.warehouse_paths.SGP.local_upload_folder');
                file_put_contents($path . '/' . 'UrbanFox_' . $this->orderService->formatOrderNumber($order, $order_country, true) . '.txt', $transaction, FILE_APPEND | LOCK_EX);
                return $this->buildTransaction($transaction, $data, false);

            } else {
                // build input data & transaction
                $transaction = 'mutation _ {
                    createOrder(
                        referenceNumber1: "' . $this->orderService->formatBulkOrderNumber($order, $order_country, true) . '",
                        remarks: "Fulfilment for order ' . $this->orderService->formatBulkOrderNumber($order, $order_country, true) . '",
                        priceTotal: ' . number_format(floatval(array_sum($total_price)), 2, '.', '') . ',
                        doNumber: "",
                        orderItems: ' . $_orderObjects . ',
                        paymentType: CREDIT_CARD,
                        shippingMethod: UF_DELIVERY,
                        billingAddress: {
                            name: "' . $billingName . '",
                            addressLine1: "' . str_replace('/\n/g', ' ', $_shipment_info_b->address) . '",
                            addressLine2: "' . $_shipment_info_b->city . '",
                            city: "' . $_shipment_info_b->city . '",
                            country: "' . $b_country->name . '",
                            countryCode: "' . $b_country->code . '",
                            postalCode: "' . (int) $_shipment_info_b->portalCode . '",
                            phone: "' . $billingPhone . '"
                        },
                        shippingAddress:{
                            name: "' . $deliveryName . '",
                            addressLine1: "' . str_replace('/\n/g', ' ', $_shipment_info_d->address) . '",
                            addressLine2: "' . $_shipment_info_d->city . '",
                            city: "' . $_shipment_info_d->city . '",
                            country: "' . $d_country->name . '",
                            countryCode: "' . $d_country->code . '",
                            postalCode: "' . (int) $_shipment_info_d->portalCode . '",
                            phone: "' . $deliveryPhone . '"
                        },
                        customerAddress: {
                            name: "' . $deliveryName . '",
                            addressLine1: "' . str_replace('/\n/g', ' ', $_shipment_info_d->address) . '",
                            addressLine2: "' . $_shipment_info_d->city . '",
                            city: "' . $_shipment_info_d->city . '",
                            country: "' . $d_country->name . '",
                            countryCode: "' . $d_country->code . '",
                            postalCode: "' . (int) $_shipment_info_d->portalCode . '",
                            phone: "' . $deliveryPhone . '"
                        },
                    ) { id }
                }';
                // save query into a text-file
                $path = config('environment.warehouse_paths.SGP.local_upload_folder');
                file_put_contents($path . '/' . 'UrbanFox_' . $this->orderService->formatBulkOrderNumber($order, $order_country, true) . '.txt', $transaction, FILE_APPEND | LOCK_EX);
                return $this->buildTransaction($transaction, $data, true);
            }

        } else {
            $this->Log->error('SG | createTransactions empty data');
            return $this->err_msg;
        }
    }

    public function getTransactionInfo($options, $country, $isBulk)
    {
        try {
            // $test = 'MY000000001-20190806-test-18';
            $formatted_order_number = $isBulk ? $this->orderService->formatBulkOrderNumber($options, $country, true) : $this->orderService->formatOrderNumber($options, $country, true);
            // $this->Log->info('getTransactionInfo from urbanfox: ' . $formatted_order_number);
            $params = '
                {
                     getOrder(referenceNumber1: "' . $formatted_order_number . '") {
                         referenceNumber1
                         status
                         remarks
                         fulfillments {
                            id
                            trackingNumber
                         }
                    }
                }';

            $transaction = [
                'query' => $params,
            ];

            $build = ["query" => $params, "transaction" => json_encode($transaction)];

            $response = $this->client->postAsync($this->urbanfox->url, ['body' => $build["transaction"]]);
            $_response = $response->wait();
            $result = json_decode($_response->getBody(), true);

            return $result;

        } catch (Exception $ex) {
            $this->Log->error('UrbanFox | getTransactionInfo: ' . $ex->getMessage());
        }
    }

    // public function getUrbanFoxOrder($formatted_order_number)
    // {
    //     $query = '
    //     {
    //         getOrder(referenceNumber1: "' . $formatted_order_number . '") {
    //           id
    //           referenceNumber1
    //           referenceNumber2
    //           orderItems {
    //             quantity
    //             serviceType
    //             sku
    //             priceTotal
    //           },
    //           billingAddress {
    //             addressLine1
    //             addressLine2
    //             city
    //             country
    //             countryCode
    //             name
    //             phone
    //             postalCode
    //             countryCode
    //           },
    //           shippingAddress {
    //             addressLine1
    //             addressLine2
    //             city
    //             country
    //             countryCode
    //             name
    //             phone
    //             postalCode
    //           },
    //           customerAddress {
    //             addressLine1
    //             addressLine2
    //             city
    //             country
    //             countryCode
    //             name
    //             phone
    //             postalCode
    //           },
    //           shippingMethod
    //           priceTotal
    //           doNumber
    //           paymentType
    //           fulfillments {
    //             trackingNumber
    //             status
    //           }
    //         }
    //       }';

    //     return $query;
    // }
}

//TESTING
// public function buildData()
// {
//     $this->buildQuery();
// }

// public function buildQuery()
// {
//     $query = 'mutation _ {
//         createOrder(
//             referenceNumber1: "123456970",
//             priceTotal: 123,
//             doNumber: "testtest",
//             orderItems: [{sku:"S6/2018",quantity:1},{sku:"A2/2018",quantity:1},{sku:"A5/2018",quantity:1}],
//             shippingMethod: UF_DELIVERY,
//                           billingAddress: {
//                     name: "Test",
//                     addressLine1: "Test",
//                       addressLine2: "Test",
//                     city: "Test",
//                     country: "Test",
//                     postalCode: "Test",
//                     phone: "123456789"
//             },
//             shippingAddress:{
//                     name: "Test",
//                     addressLine1: "Test",
//                       addressLine2: "Test",
//                     city: "Test",
//                     country: "Test",
//                     postalCode: "Test",
//                     phone: "123456789"
//             },
//             customerAddress: {
//                     name: "Test",
//                     addressLine1: "Test",
//                     addressLine2: "Test",
//                     city: "Test",
//                     country: "Test",
//                     postalCode: "Test",
//                     phone: "123456789"
//             },
//             remarks: "Test",
//         ) { id }
// }';

//     return $this->buildTransaction($query);
// }
