<?php
namespace App\Helpers\Warehouse;

use App\Helpers\LaravelHelper;
use App\Helpers\OrderHelper;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Orders\OrderUploads;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use App\Services\OrderService;
use Carbon\Carbon;
use App\Queue\SendEmailQueueService as EmailQueueService;
use Exception;
use App\Helpers\Warehouse\AftershipHelper;
class WarehouseHelperHK
{
    public function __construct()
    {
        $this->Log = \Log::channel('cronjob');
        $this->orderHelper = new OrderHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->orderService = new OrderService();
        $this->ftpHelperHK = new FtpHelperHK();
        $this->_aftership = new AftershipHelper();
        $this->className = 'createOrderInbound';
        $this->functionName = 'createOrderInbound';
        $this->Log = \Log::channel('cronjob');
    }

    public function _readFileAsync($fileName, $dir = null)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('$this->_readFileAsync start');
        $Log->info('$this->_readFileAsync | fileName = ' . $fileName . ', dir =' . $dir);

        $dirName = $dir ? $dir : base_path() . '/warehouse/warehouse-interface-hk';
        if ($dir !== null) {
            $data = file_get_contents($fileName, FILE_USE_INCLUDE_PATH);
        } else {
            $data = file_get_contents($dirName . '/' . $fileName, FILE_USE_INCLUDE_PATH);
        }
        return $data;
    }

    public function _addSpace($len, $value)
    {
        $Log = \Log::channel('cronjob');
        // $Log->info('_addSpace start');
        // $Log->info('_addSpace value = ' . $value . ', length = ' . mb_strlen($value));

        $value = $value ? $value : ($value === "0" ? "0" : '');
        for ($i = mb_strlen($value); $i < $len; $i++) {
            $value = "" . $value;
        }
        if (mb_strlen($value) > $len) {
            $value = substr($value, 0, $len);
        }
        if (mb_strlen($value) < $len) {
            $_whitespace = $len - mb_strlen($value);
            $value = $value . str_repeat(" ", $_whitespace);
        }
        return $value;
    }

    public function _fillZero($len, $value)
    {
        $_whitespace = $len > strlen($value) ? $len - strlen($value) : 0;
        $value = $value ? $value : '';
        for ($i = strlen($value); $i < $len; $i++) {
            $value = $value;
        }
        if (strlen($value) > $len) {
            $value = substr($value, 0, $len);
        }
        $value = $_whitespace > 0 ? str_repeat("0", $_whitespace) . $value : $value;
        return $value;
    }

    public function _createData($options)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('$this->_createData start');
        // $Log->info('$this->_createData | param = ' . json_encode($options));
        $returnObj = '';
        $data = $this->_readFileAsync($options->fileName);
        $dataArray = preg_split('/\r?\n/', $data);
        foreach ($dataArray as $dA) {
            $values = explode(',', $dA);
            $_value1 = $values[1];
            // if variable from csv is found in data
            if (array_key_exists($_value1, (array) $options->data) === true) {
                $returnObj = $returnObj . "" . $this->_addSpace($values[4], $options->data->$_value1);
            } else {
                $_len = $values[4];
                if ($_len > 0) {
                    $returnObj = $returnObj . str_repeat(" ", $_len);
                }
            }
        }

        return $returnObj;
    }

    public function _readData($options)
    {

        $Log = \Log::channel('cronjob');
        $Log->info('_readData start');
        // $Log->info('_readData | param = ' . json_encode($options));

        $data = $this->_readFileAsync($options->fileName);
        $dataArray = preg_split('/\r?\n/', $data);

        if ($options->fileName === 'order-confirmation-outbound/file-header.csv') {
            $_count = 0;
            $returnObj = array();
            foreach ($dataArray as $object) {
                $keys = explode(',', $object);
                array_push($returnObj, $keys);
                if ($_count == 0) {
                    $_count++;
                    $returnObj[$keys[1]] = trim(substr($options->values, $keys[5] - 1, $keys[4]));
                }
            }
            return $returnObj;
        } else if ($options->fileName === 'order-confirmation-outbound/header.csv') {
            $returnObj = (Object) array();
            foreach ($dataArray as $object) {
                $keys = explode(',', $object);
                foreach ($options->values as $x => $y) {
                    $_extract = trim(substr($y, $keys[5] - 1, $keys[4]));
                    if (trim(substr($y, $keys[5] - 1, $keys[4]))) {
                        $returnObj->{str_replace('"', '', json_encode($keys[1], true))} = $_extract;
                    } else {
                        $returnObj->{str_replace('"', '', json_encode($keys[1], true))} = null;
                    }
                }
            }
            return $returnObj;
        }

    }

    public function _encode($str, $encode, $decode)
    {
        if ($decode !== null) {
            return mb_convert_encoding($str, $encode, $decode);
        } else {
            return mb_convert_encoding($str, $encode);
        }
    }

    public function createOrderInbound($id)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('HK | createOrderInbound start');
        // $Log->info('HK | createOrderInbound | $id = ' . json_encode($id));

        try {
            $_info = Orders::where('status','Payment Received')->where('id', $id)->select('UserId', 'CountryId', 'SellerUserId')->first();
            if ($_info) {
                // $Log->info('HK | order_info | $id = ' . json_encode($_info));
                $isPlan = '';
                $isBA = $_info->SellerUserId ? true : false;
                $_user = User::findorfail($_info->UserId);
                $_country = Countries::findorfail($_info->CountryId);

                $dataOrderUpload = [];
                $options = [
                    'OrderId' => $id,
                    'User' => $_user,
                    'CountryId' => $_country->id,
                    'isBA' => $isBA,
                ];

                $data = $this->orderHelper->buildWarehouseOrderDetails($options, true);
                $data = $this->laravelHelper->ConvertArrayToObject($data);

                if ($data) {
                    // $Log->info('HK | warehouse_data | $data = ' . json_encode($data));
                    // shortened data
                    $_user = $data->user;
                    $_warehouse_items = $data->generate_sales_order_data;
                    $_shipment_info_d = $data->shipment_info->delivery;
                    $_shipment_info_b = $data->shipment_info->billing;
                    $_metadata = $data->metadata;
                    $receipt = $data->receipt;
                    $order = $data->order;
                    $plan_detail = $data->plan;
                    $order_details = $data->orderdetails;

                    // build order items
                    $order_items = [];
                    $orderObjects = [];
                    $total_price = array();
                    $items = [];
                    $_sumOrder = [];
                    foreach ($_warehouse_items->items as $item) {
                        array_push($items, $item);
                    }

                    // update delivery address
                    $deliveryAddress = "";
                    $billingAddress = "";

                    if(isset($_shipment_info_d) && isset($_shipment_info_d->address)){
                    $deliveryAddress = explode(',', str_replace('/\n/g', '', $_shipment_info_d->address));
                    }

                    if(isset($_shipment_info_b) && isset($_shipment_info_b->address)){
                    $billingAddress = explode(',', str_replace('/\n/g', '', $_shipment_info_b->address));
                    }

                    //--------------------------- Updates for Warehouse HK & TW - START
                    $_totalSumWithQty = 0;
                    $_totalSum = 0;
                    $InvoiceAmount = 0;
                    $_finalPrice = 0;
                    if (count($items) > 0) { //If Custom Plan & ASK
                        $_totalSumWithQty = 0;
                        $_totalSum = 0;
                        foreach ($items as $obj) {
                            if (isset($obj->trialPrice)) {
                                $_totalSumWithQty = (($obj->trialPrice) * ($obj->qty));
                                $_totalSum = $_totalSum + (floatval($_totalSumWithQty));
                            } else if (isset($obj->sellPrice)) {
                                $_totalSumWithQty = (($obj->sellPrice) * ($obj->qty));
                                $_totalSum = $_totalSum + ($_totalSumWithQty);
                            }
                        }

                        $_finalPrice = floatval($_totalSum);
                        if ($receipt) {
                            if ($receipt->totalPrice < $_finalPrice) {
                                $InvoiceAmount = number_format($receipt->totalPrice, 2);
                            } else {
                                $InvoiceAmount = number_format($_finalPrice, 2);
                            }
                        } else {
                            $InvoiceAmount = number_format($_finalPrice, 2);
                        }
                    }

                    // create orderHeader
                    $data->orderHeader = (Object) array(
                        'HeaderFlag' => 'ORDHD',
                        'InterfaceActionFlag' => 'A',
                        'StorerKey' => 'SHAVES2U',
                        'ExternOrderKey' => $this->orderService->formatOrderNumber($order, $_country, true),
                        'OrderDate' => Carbon::now()->format('Ymdhis'),
                        'DeliveryDate' => Carbon::now()->format('Ymdhis'),
                        'C_contact1' => $order->SellerUserId ? $order->fullName : $_shipment_info_d->firstName,
                        'C_Address1' => $deliveryAddress[0],
                        'C_Address2' => count($deliveryAddress) > 1 ? $deliveryAddress[1] : '',
                        'C_Address3' => count($deliveryAddress) > 2 ? $deliveryAddress[2] : '',
                        'C_Address4' => count($deliveryAddress) > 3 ? $deliveryAddress[3] : '',
                        'C_City' => str_replace('/\n/g', '', $_shipment_info_d->city),
                        // 'C_State' => $$_shipment_info_d->state,
                        'C_Zip' => $_shipment_info_d->portalCode,
                        'C_Country' => $_country->name,
                        'C_ISOCntryCode' => $_country->code,
                        'C_Phone1' => $order->SellerUserId ? $order->phone : (isset($order->phone) && $order->phone !== null ? $order->phone : $_shipment_info_d->contactNumber),
                        'B_contact1' => $order->SellerUserId ? $order->fullName : $_shipment_info_b->firstName,
                        'B_Address1' => $billingAddress[0],
                        'B_Address2' => count($billingAddress) > 1 ? $billingAddress[1] : '',
                        'B_Address3' => count($billingAddress) > 2 ? $billingAddress[2] : '',
                        'B_Address4' => count($billingAddress) > 3 ? $billingAddress[3] : '',
                        'B_City' => str_replace('/\n/g', '', $_shipment_info_b->city),
                        // 'B_State' => $_shipment_info_b->state,
                        'B_Zip' => $_shipment_info_b->portalCode,
                        'B_Country' => $_country->name,
                        'B_ISOCntryCode' => $_country->code,
                        'B_Phone1' => $order->SellerUserId ? $order->phone : (isset($order->phone) && $order->phone !== null ? $order->phone : $_shipment_info_b->contactNumber),
                        'Status' => '0',
                        'Type' => '0',
                        'SOStatus' => '0',
                        'IncoTerm' => 'DDU',
                        'InvoiceAmount' => '' . $InvoiceAmount,
                        'UserDefine01' => 'HKD',
                        'UserDefine02' => $order->SSN ? $order->SSN : ($_shipment_info_d->SSN ? $_shipment_info_d->SSN : "not_found"),
                    );

                    // $Log->info('HK | build_data | $data->orderHeader = ' . json_encode($data->orderHeader));

                    // generate orderDetails
                    // build order detail
                    $data->orderDetails = [];
                    // foreach $items
                    foreach ($items as $idx => $item) {
                        array_push($data->orderDetails, (Object) array(
                            'HeaderFlag' => 'ORDDT',
                            'InterfaceActionFlag' => 'A',
                            'ExternOrderKey' => $this->orderService->formatOrderNumber($order, $_country, true),
                            'ExternLineNo' => $idx + 1,
                            'Sku' => $item->sku,
                            'UnitPrice' => isset($item->trialPrice) ? $item->trialPrice : $item->sellPrice,
                            'StorerKey' => 'SHAVES2U',
                            'OriginalQty' => $item->qty,
                            'OpenQty' => $item->qty,
                            'ShippedQty' => '0',
                            'UOM' => 'EA',
                            'Lottable03' => 'UR',
                        ));
                        array_push($dataOrderUpload, (Object) array(
                            'orderNumber' => $this->orderService->formatOrderNumber($order, $_country, true),
                            'orderId' => $order->id ? $order->id : $order->id,
                            'sku' => $item->sku,
                            'unit_price' => isset($item->trialPrice) ? $item->trialPrice : $item->sellPrice,
                            'qty' => $item->qty,
                            'status' => $order->status,
                            'region' => $_country ? $_country->code : $order->region,
                        ));

                    }

                    // $Log->info('HK | build_data | $data->orderDetails = ' . json_encode($data->orderDetails));
                    // generate file header
                    $_create = (Object) array(
                        'fileName' => 'order-inbound/file-header.csv',
                        'data' => (Object) array(
                            'TableID' => 'WMSORD',
                            'InterfaceType' => 'I',
                            'CreateDateTime' => Carbon::now()->format('Ymdhis'),
                            'ClientID' => 'SHAVES2U',
                            'ClientCountry' => 'HK',
                            'InterfaceName' => 'SO Inbound',
                        ));

                    $orderFileHeaderData = $this->_createData($_create);
                    $data->orderFileHeaderData = $orderFileHeaderData;
                    // $Log->info('HK | build_data | $data->orderHeader = ' . json_encode($data->orderHeader));

                    // generate order info
                    $_create_orderHeader = (Object) array(
                        'fileName' => 'order-inbound/header.csv',
                        'data' => $data->orderHeader,
                    );

                    $orderHeaderData = $this->_createData($_create_orderHeader);
                    $data->orderHeaderData = $orderHeaderData;
                    // $Log->info('HK | build_data | $data->orderHeaderData = ' . json_encode($data->orderHeaderData));

                    // generate order details data
                    $data->orderDetailsData = [];
                    foreach ($data->orderDetails as $details) {
                        $_create_orderdetails = (Object) array(
                            'fileName' => 'order-inbound/details.csv',
                            'data' => $details,
                        );
                        $orderDetailsData = $this->_createData($_create_orderdetails);
                        array_push($data->orderDetailsData, $orderDetailsData);
                    }

                    // generate footer details
                    $create_orderFooterData = (Object) array(
                        'fileName' => 'order-inbound/file-footer.csv',
                        'data' => (Object) array(
                            'FileFooter' => 'ORDTR',
                            'TotalRecordsCount' => $this->_fillZero(10, count($data->orderDetailsData) + 1),
                        ));

                    $orderFooterData = $this->_createData($create_orderFooterData);
                    $data->orderFooterData = $orderFooterData;
                    // $Log->info('HK | build_data | $data->orderFooterData = ' . json_encode($data->orderFooterData));

                    // upload file to warehouse's FTP server
                    $path = config('environment.warehouse_paths.HKG.local_upload_folder');
                    $remote = config('environment.warehouse_paths.HKG.ftp_outbox_folder');
                    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    $unique = "";
                    for ($i = 0; $i < 6; $i++) {
                        $unique .= $characters[mt_rand(0, 6)];
                    }
                    $data->inboundFileName = 'WMSORD_' . $_country->code . '_' . Carbon::now()->format('Ymdhis') . '_' . $unique . '.txt';
                    // $data->inboundFileName = 'WMSORD_' . $_country->code . '_' . Carbon::now()->format('Ymdhis') . '.txt';
                    foreach ($dataOrderUpload as $element) {
                        $element->doFileName = $data->inboundFileName;
                    }
                    // $_fileHeader = mb_convert_encoding($data->orderFileHeaderData, 'UTF-8');
                    // $_orderHeader = mb_convert_encoding($data->orderHeaderData, 'UTF-8');
                    // $_orderDetailsData = mb_convert_encoding($data->orderDetailsData, 'UTF-8');
                    // $_orderFooterData = mb_convert_encoding($data->orderFooterData, 'UTF-8');
                    $_fileHeader = $data->orderFileHeaderData;
                    $_orderHeader = $data->orderHeaderData;
                    $_orderDetailsData = $data->orderDetailsData;
                    $_orderFooterData = $data->orderFooterData;

                    file_put_contents($path . '/' . $data->inboundFileName, $_fileHeader, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, $_orderHeader, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    foreach ($_orderDetailsData as $oDD) {
                        file_put_contents($path . '/' . $data->inboundFileName, $oDD, FILE_APPEND | LOCK_EX);
                        file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    }
                    file_put_contents($path . '/' . $data->inboundFileName, $_orderFooterData, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);

                    $Log->info('HK | upload to FTP | path => ' . $remote . '/' . $data->inboundFileName);
                    // upload to FTP
                    try {
                        $this->ftpHelperHK->putFile($path . '/' . $data->inboundFileName, $remote . '/' . $data->inboundFileName, $data->inboundFileName); // put file to ftp server
                        // throw new Exception("Exceptional!");
                    } catch (Exception $ex) {
                        $Log->error('HK | upload to FTP | error => ' . $ex->getMessage());
                    }

                    // insert to oder & orderhistory table -> status: Processing
                    try {
                        Orders::where('id', $order->id)->update(['status' => 'Processing']);
                        OrderHistories::create(['message' => 'Processing', 'isRemark' => 0, 'OrderId' => $order->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                        foreach ($dataOrderUpload as $orderUploads) {
                            OrderUploads::create([
                                'doFileName' => $orderUploads->doFileName,
                                'orderNumber' => $orderUploads->orderNumber,
                                'orderId' => $orderUploads->orderId,
                                'sku' => $orderUploads->sku,
                                'qty' => $orderUploads->qty,
                                'status' => $orderUploads->status,
                                'region' => $orderUploads->region,
                            ]);
                        }
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $Log->error('HK | Create Order/OrderHistory/OrderUploads | error => ' . $ex->getMessage());
                    }

                    return 1;
                } else {
                    $Log->error('HK | createOrderInbound | error => buildWarehouseOrderDetails error for OrderId' . $id);
                    return 0;
                }
            } else {
                $Log->error('HK | createOrderInbound | error => order_info error for OrderId' . $id);
                return 1;
            }
        } catch (Exception $ex) {
            $Log->error('HK | createOrderInbound | error => ' . $ex->getMessage());
            return 0;
        }

    }

    public function createBulkOrderInbound($id)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('HK | createBulkOrderInbound start');
        $Log->info('HK | createBulkOrderInbound | $id = ' . json_encode($id));
        try {
            $_info = BulkOrders::where('status','Payment Received')->where('id', $id)->first();
            if ($_info) {
                $_country = Countries::findorfail($_info->CountryId);

                $dataOrderUpload = [];
                $options = [
                    'OrderId' => $id,
                    'CountryId' => $_country->id,
                ];

                $data = $this->orderHelper->buildWarehouseBulkOrderDetails($options, true);

                $data = $this->laravelHelper->ConvertArrayToObject($data);

                if ($data) {
                    // shortened data
                    $_warehouse_items = $data->generate_sales_order_data;
                    $_shipment_info_d = $data->shipment_info->delivery;
                    $_shipment_info_b = $data->shipment_info->billing;
                    $_metadata = $data->metadata;
                    $bulkorder = $data->bulkorder;
                    $plan_detail = $data->plan;
                    $order_details = $data->bulkorderdetails;

                    // build order items
                    $order_items = [];
                    $orderObjects = [];
                    $total_price = array();
                    $items = [];

                    foreach ($_warehouse_items->items as $item) {
                        array_push($items, $item);
                    }

                    // update delivery address
                    $deliveryAddress = "";
                    $billingAddress = "";

                    if(isset($_shipment_info_d) && isset($_shipment_info_d->address)){
                        $deliveryAddress = explode(',', str_replace('/\n/g', '', $_shipment_info_d->address));
                    }

                    if(isset($_shipment_info_b) && isset($_shipment_info_b->address)){
                        $billingAddress = explode(',', str_replace('/\n/g', '', $_shipment_info_b->address));
                    }

                    //--------------------------- Updates for Warehouse HK & TW - START
                    $_totalSumWithQty = 0;
                    $_totalSum = 0;
                    $InvoiceAmount = 0;
                    $_finalPrice = 0;
                    if (count($items) > 0) {
                        $_totalSumWithQty = 0;
                        $_totalSum = 0;
                        foreach ($items as $obj) {
                            if (isset($obj->sellPrice)) {
                                $_totalSumWithQty = (($obj->sellPrice) * ($obj->qty));
                                $_totalSum = $_totalSum + ($_totalSumWithQty);
                            }
                        }

                        $InvoiceAmount = $_finalPrice = floatval($_totalSum);
                    }

                    // create orderHeader
                    $data->orderHeader = (Object) array(
                        'HeaderFlag' => 'ORDHD',
                        'InterfaceActionFlag' => 'A',
                        'StorerKey' => 'SHAVES2U',
                        'ExternOrderKey' => $this->orderService->formatBulkOrderNumber($bulkorder, $_country, true),
                        'OrderDate' => Carbon::now()->format('Ymdhis'),
                        'DeliveryDate' => Carbon::now()->format('Ymdhis'),
                        'C_contact1' => $bulkorder->fullName ? $bulkorder->fullName : $_shipment_info_d->firstName,
                        'C_Address1' => $deliveryAddress[0],
                        'C_Address2' => count($deliveryAddress) > 1 ? $deliveryAddress[1] : '',
                        'C_Address3' => count($deliveryAddress) > 2 ? $deliveryAddress[2] : '',
                        'C_Address4' => count($deliveryAddress) > 3 ? $deliveryAddress[3] : '',
                        'C_City' => str_replace('/\n/g', '', $_shipment_info_d->city),
                        // 'C_State' => $$_shipment_info_d->state,
                        'C_Zip' => $_shipment_info_d->portalCode,
                        'C_Country' => $_country->name,
                        'C_ISOCntryCode' => $_country->code,
                        'C_Phone1' => $bulkorder->phone ? $bulkorder->phone : (isset($bulkorder->phone) && $bulkorder->phone !== null ? $bulkorder->phone : $_shipment_info_d->contactNumber),
                        'B_contact1' => $bulkorder->fullName ? $bulkorder->fullName : $_shipment_info_b->firstName,
                        'B_Address1' => $billingAddress[0],
                        'B_Address2' => count($billingAddress) > 1 ? $billingAddress[1] : '',
                        'B_Address3' => count($billingAddress) > 2 ? $billingAddress[2] : '',
                        'B_Address4' => count($billingAddress) > 3 ? $billingAddress[3] : '',
                        'B_City' => str_replace('/\n/g', '', $_shipment_info_b->city),
                        // 'B_State' => $_shipment_info_b->state,
                        'B_Zip' => $_shipment_info_b->portalCode,
                        'B_Country' => $_country->name,
                        'B_ISOCntryCode' => $_country->code,
                        'B_Phone1' => $bulkorder->phone ? $bulkorder->phone : (isset($bulkorder->phone) && $bulkorder->phone !== null ? $bulkorder->phone : $_shipment_info_b->contactNumber),
                        'Status' => '0',
                        'Type' => '0',
                        'SOStatus' => '0',
                        'IncoTerm' => 'DDU',
                        'InvoiceAmount' => '' . $InvoiceAmount,
                        'UserDefine01' => 'HKD',
                        'UserDefine02' => $bulkorder->SSN ? $bulkorder->SSN : ($_shipment_info_d->SSN ? $_shipment_info_d->SSN : "not_found"),
                    );

                    // generate orderDetails
                    // build order detail
                    $data->orderDetails = [];
                    // foreach $items
                    foreach ($items as $idx => $item) {
                        array_push($data->orderDetails, (Object) array(
                            'HeaderFlag' => 'ORDDT',
                            'InterfaceActionFlag' => 'A',
                            'ExternOrderKey' => $this->orderService->formatBulkOrderNumber($bulkorder, $_country, true),
                            'ExternLineNo' => $idx + 1,
                            'Sku' => $item->sku,
                            'UnitPrice' => $item->sellPrice,
                            'StorerKey' => 'SHAVES2U',
                            'OriginalQty' => $item->qty,
                            'OpenQty' => $item->qty,
                            'ShippedQty' => '0',
                            'UOM' => 'EA',
                            'Lottable03' => 'UR',
                        ));
                        array_push($dataOrderUpload, (Object) array(
                            'orderNumber' => $this->orderService->formatBulkOrderNumber($bulkorder, $_country, true),
                            'orderId' => $bulkorder->id ? $bulkorder->id : $bulkorder->id,
                            'sku' => $item->sku,
                            'qty' => $item->qty,
                            'status' => $bulkorder->status,
                            'region' => $_country ? $_country->code : $bulkorder->region,
                        ));

                    }

                    // generate file header
                    $_create = (Object) array(
                        'fileName' => 'order-inbound/file-header.csv',
                        'data' => (Object) array(
                            'TableID' => 'WMSORD',
                            'InterfaceType' => 'I',
                            'CreateDateTime' => Carbon::now()->format('Ymdhis'),
                            'ClientID' => 'SHAVES2U',
                            'ClientCountry' => 'HK',
                            'InterfaceName' => 'SO Inbound',
                        ));

                    $orderFileHeaderData = $this->_createData($_create);
                    $data->orderFileHeaderData = $orderFileHeaderData;

                    // generate order info
                    $_create_orderHeader = (Object) array(
                        'fileName' => 'order-inbound/header.csv',
                        'data' => $data->orderHeader,
                    );

                    $orderHeaderData = $this->_createData($_create_orderHeader);
                    $data->orderHeaderData = $orderHeaderData;

                    // generate order details data
                    $data->orderDetailsData = [];
                    foreach ($data->orderDetails as $details) {
                        $_create_orderdetails = (Object) array(
                            'fileName' => 'order-inbound/details.csv',
                            'data' => $details,
                        );
                        $orderDetailsData = $this->_createData($_create_orderdetails);
                        array_push($data->orderDetailsData, $orderDetailsData);
                    }

                    // generate footer details
                    $create_orderFooterData = (Object) array(
                        'fileName' => 'order-inbound/file-footer.csv',
                        'data' => (Object) array(
                            'FileFooter' => 'ORDTR',
                            'TotalRecordsCount' => $this->_fillZero(10, count($data->orderDetailsData) + 1),
                        ));

                    $orderFooterData = $this->_createData($create_orderFooterData);

                    $data->orderFooterData = $orderFooterData;
                    // upload file to warehouse's FTP server
                    $path = config('environment.warehouse_paths.HKG.local_upload_folder');
                    $remote = config('environment.warehouse_paths.HKG.ftp_outbox_folder');
                    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    $unique = "";
                    for ($i = 0; $i < 6; $i++) {
                        $unique .= $characters[mt_rand(0, 6)];
                    }
                    $data->inboundFileName = 'WMSORD_' . $_country->code . '_' . Carbon::now()->format('Ymdhis') . '_' . $unique . '.txt';
                    // $data->inboundFileName = 'WMSORD_' . $_country->code . '_' . Carbon::now()->format('Ymdhis') . '.txt';
                    foreach ($dataOrderUpload as $element) {
                        $element->doFileName = $data->inboundFileName;
                    }
                    $_fileHeader = mb_convert_encoding($data->orderFileHeaderData, 'UTF-8');
                    $_orderHeader = mb_convert_encoding($data->orderHeaderData, 'UTF-8');
                    $_orderDetailsData = mb_convert_encoding($data->orderDetailsData, 'UTF-8');
                    $_orderFooterData = mb_convert_encoding($data->orderFooterData, 'UTF-8');

                    file_put_contents($path . '/' . $data->inboundFileName, $_fileHeader, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, $_orderHeader, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    foreach ($_orderDetailsData as $oDD) {
                        file_put_contents($path . '/' . $data->inboundFileName, $oDD, FILE_APPEND | LOCK_EX);
                        file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);
                    }
                    file_put_contents($path . '/' . $data->inboundFileName, $_orderFooterData, FILE_APPEND | LOCK_EX);
                    file_put_contents($path . '/' . $data->inboundFileName, PHP_EOL, FILE_APPEND | LOCK_EX);

                    $Log->info('HK | upload to FTP | path => ' . $remote . '/' . $data->inboundFileName);
                    // upload to FTP
                    try {
                        $this->ftpHelperHK->putFile($path . '/' . $data->inboundFileName, $remote . '/' . $data->inboundFileName, $data->inboundFileName); // put file to ftp server
                        // throw new Exception("Exceptional!");
                    } catch (Exception $ex) {
                        $Log->error('HK | upload to FTP | error => ' . $ex->getMessage());
                    }

                    // insert to oder & orderhistory table -> status: Processing
                    try {
                        // insert to oder & orderhistory table -> status: Processing
                        BulkOrders::where('id', $bulkorder->id)->update(['status' => 'Processing']);
                        BulkOrderHistories::create(['message' => 'Processing', 'isRemark' => 0, 'BulkOrderId' => $bulkorder->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

                        foreach ($dataOrderUpload as $orderUploads) {
                            OrderUploads::create([
                                'doFileName' => $orderUploads->doFileName,
                                'orderNumber' => $orderUploads->orderNumber,
                                'bulkOrderId' => $orderUploads->orderId,
                                'sku' => $orderUploads->sku,
                                'qty' => $orderUploads->qty,
                                'status' => $orderUploads->status,
                                'region' => $orderUploads->region,
                            ]);
                        }
                    } catch (\Illuminate\Database\QueryException $ex) {
                        $Log->error('HK | Create BulkOrder/BulkOrderHistory/BulkOrderUploads | error => ' . $ex->getMessage());
                    }

                    return 1;
                } else {
                    $Log->error('HK | createBulkOrderInbound | error => buildWarehouseBulkOrderDetails error for OrderId' . $id);
                    return 0;
                }
            } else {
                $Log->error('HK | createBulkOrderInbound | error => bulkorder_info error for BulkOrderId' . $id);
                return 1;
            }
        } catch (Exception $ex) {
            $Log->error('HK | createBulkOrderInbound | error => ' . $ex->getMessage());
            return 0;
        }
    }

    public function readOrderConfirmation($fileName)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('HK | readOrderConfirmation start');
        $Log->info('HK | readOrderConfirmation | fileName = ' . json_encode($fileName));

        $this->functionName = 'readOrderConfirmation';
        $this->Log->info($this->className . ' => ' . $this->functionName . ' | ' . 'start');
        $this->Log->info($this->className . ' => ' . $this->functionName . ' | ' . 'fileName = ' . json_encode($fileName));

        try {
            // read outbound file
            $outBoundData = (Object) array(
                'detailsStrs' => [],
                'headerStrs' => [],
                'fileHeaderData' => [],
                'headersData' => []
            );
            $dirName = config('environment.warehouse_paths.HKG.local_download_folder');
            $_getFiles = $this->_readFileAsync($fileName, $dirName);
            $dataArray = preg_split('/\r?\n/', $_getFiles);

            foreach ($dataArray as $key => $value) {
                $idx = $key;
                if ($idx === 0) {
                    $outBoundData->fileHeaderStr = array($key => $value);
                } else if (substr($value, 0, 5) === 'SHPHD') {
                    array_push($outBoundData->headerStrs, array($key => $value));
                }
            }

            $fileHeaderData =$this->_readData((Object) array(
                'fileName' => 'order-confirmation-outbound/file-header.csv',
                'values' => $outBoundData->fileHeaderStr[0],
            ));
            array_push($outBoundData->fileHeaderData, $fileHeaderData);

            foreach ($outBoundData->headerStrs as $headerStr) {
                $headersData =$this->_readData((Object) array(
                    'fileName' => 'order-confirmation-outbound/header.csv',
                    'values' => $headerStr,
                ));
                array_push($outBoundData->headersData, $headersData);
            }

            foreach ($outBoundData->headersData as $headerData) {
                $orderId = '';
                if ($headerData->ExternOrderKey[2] === 'B') {
                    $orderId = $this->orderService->getBulkOrderNumber($headerData->ExternOrderKey);
                    if ($headerData->SOStatus === 'CANC') {
                        // for bulkorder => if SOStatus === CANC, update BulkOrder & BulkOrderHistories to Cancelled
                        BulkOrders::where('id', $orderId)->update(['status' => 'Cancelled']);
                        try {
                            BulkOrderHistories::create(['message' => 'Cancelled', 'isRemark' => 0, 'BulkOrderId' => $orderId, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                            // sendEmailQueueService.addTask({method: 'sendCancelBulkOrderEmail'data: order,langCode: $order->Country->defaultLang.toLowerCase()}));
                        } catch (\Illuminate\Database\QueryException $ex) {
                            $Log->error('HK | Create BulkOrder/BulkOrderHistory/BulkOrderUploads | error => ' . $ex->getMessage());
                        }
                    } else {
                        $order = '';
                        $carrierAgent = $headerData->Carrieragent ? $headerData->Carrieragent : 'not_found';
                        BulkOrders::where('id', $orderId)->whereOr('status', 'Processing')->whereOr('status', 'Delivering')->update([
                            'status' => 'Delivering',
                            'deliveryId' => $headerData->OtherReference ? $headerData->OtherReference : 'not_found',
                            'carrierKey' => $headerData->CarrierKey ? $headerData->CarrierKey : 'not_found',
                            'carrierAgent' => $headerData->Carrieragent ? $headerData->Carrieragent : 'not_found',
                        ]);

                        $order = BulkOrders::select('bulkorders.id as BulkOrderId', 'bulkorders.DeliveryAddressId as DeliveryAddressId', 'bulkorders.BillingAddressId as BillingAddressId', 'bulkorders.CountryId as cid','bulkorders.*')
                            ->where('id', $orderId)
                            ->get();

                        if ($order) {
                            BulkOrderHistories::create(['message' => 'Delivering', 'isRemark' => 0, 'BulkOrderId' => $orderId, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                        }

                        // Update OrderUpload table
                        try {
                            OrderUploads::where('orderNumber', $orderId)->update(['wareHouseFileName' => $fileName]);
                        } catch (\Illuminate\Database\QueryException $ex) {
                            $Log->error('HK | Create BulkOrder/BulkOrderHistory/BulkOrderUploads | error => ' . $ex->getMessage());
                        }
                        // post order to aftership

                        try {
                            if ($order) {
                            $country = Countries::where('id', $order->cid)->first();
                            $user =  null;
                            $shipment = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                            $options = [
                                'order' => $order,
                                // 'country' => $country,
                                'shipment' => $shipment,
                                'user' => $user,
                            ];
                            
                            $ccode = "";
                            if(isset($country)){
                                if($country->codeIso){
                                $ccode = $country->codeIso;
                                }
                            }

                            $carrierAgent !== 'not_found' ? $this->_aftership->postTracking($options,$ccode, true) : $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: BulkOrderId = [' . $orderId . '] => CarrierAgent not_found');
                            }else{
                                $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: BulkOrderId = [' . $orderId . '] => id not_found');
                            }
                        } catch (Exception $ex) {
                            $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: BulkOrderId = [' . $orderId . '] => ' . $ex->getMessage());
                        }

                    }
                } else {
                    $orderId = $this->orderService->getOrderNumber($headerData->ExternOrderKey);
                    if ($headerData->SOStatus === 'CANC') {
                        // cancelOrderQueueService.addTask({orderId,canceledFrom: 'warehouse'}));
                    } else {
                        $order = [
                            'order' => [],
                            'User' => [],
                            'Country' => [],
                            'DeliveryAddress' => [
                                'Delivery' => '',
                                'Billing' => '',
                            ],
                        ];

                        $order_info = Orders::where('id', $orderId)->first();

                        //if order number found
                        if ($order_info) {
                            $carrierAgent = $headerData->Carrieragent ? $headerData->Carrieragent : 'not_found';
                            if (isset($headerData->OtherReference)) {
                                $order_info->deliveryId = $headerData->OtherReference;
                            }
                            if (isset($headerData->Carrieragent)) {
                                $order_info->carrierAgent = $headerData->Carrieragent;
                            }
                            Orders::where('id', $orderId)->whereOr('status', 'Processing')->whereOr('status', 'Delivering')->update([
                                'status' => 'Delivering',
                                'deliveryId' => $headerData->OtherReference ? $headerData->OtherReference : 'not_found',
                                'carrierKey' => $headerData->CarrierKey ? $headerData->CarrierKey : 'not_found',
                                'carrierAgent' => $headerData->Carrieragent ? $headerData->Carrieragent : 'not_found',
                            ]);
                
                            $order["order"] = $order_info;
                            $user_info = User::findorfail($order_info->UserId);
                            $order["User"] = $user_info;
                            $country_info = Countries::where('id', $order_info->CountryId)->first();
                            $order["Country"] = $country_info;
                            $delivery_info = DeliveryAddresses::findorfail($order_info->DeliveryAddressId);
                            $order["DeliveryAddress"]["Delivery"] = (array) json_decode($delivery_info, true);
                            $billing_info = User::findorfail($order_info->BillingAddressId);
                            $order["DeliveryAddress"]["Billing"] = (array) json_decode($billing_info, true);

                            $order = $this->laravelHelper->ConvertArraytoObject($order);

                            if ($order->order) {
                                OrderHistories::create(['message' => 'Delivering', 'isRemark' => 0, 'OrderId' => $orderId, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                            }

                            // Update OrderUpload table
                            OrderUploads::where('orderNumber', $orderId)->update(['wareHouseFileName' => $fileName]);
                            // post order to aftership

                            try {
                                if($order && $order_info){
                                $country = Countries::where('id', $order_info->CountryId)->first();
                                $user = User::where('id', $order_info->UserId)->first();
                                $shipment = DeliveryAddresses::where('id', $order_info->DeliveryAddressId)->first();
                                $options = [
                                    'order' => $order->order,
                                    // 'country' => $country,
                                    'shipment' => $shipment,
                                    'user' => $user,
                                ];
                                   
                            $ccode = "";
                            if(isset($country)){
                                if($country->codeIso){
                                $ccode = $country->codeIso;
                                }
                            }

                                $carrierAgent !== 'not_found' ? $this->_aftership->postTracking($options,$ccode, false) : $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: OrderId = [' . $orderId . '] => CarrierAgent not_found');
                            }else{
                                $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: OrderId = [' . $orderId . '] => id not_found');
                            }
                            } catch (Exception $ex) {
                                $this->Log->error('HK | readOrderConfirmation | post Aftership ERROR: OrderId = [' . $orderId . '] => ' . $ex->getMessage());
                            }

                            if ($order->order) {
                                $defLang = '';
                                if ($order->User) {
                                    if ($order->User->defaultLanguage) {
                                        $defLang = strtolower($order->User->defaultLanguage);
                                    } else {
                                        $defLang = strtolower($order->Country->defaultLang);
                                    }
                                } else {
                                    $defLang = strtolower($order->Country->defaultLang);
                                }

                                // Initiate array to store email Data.
                                $emailData = [];

                                // Query Receipts info Based on orderId
                                $receiptId = Receipts::select('id')->where('OrderId', $order->order->id)->first();

                                if ($order->order->subscriptionIds) {
                                    $subscriptions = Subscriptions::findorfail($order->order->subscriptionIds);
                                    if ($subscriptions) {
                                        if ($subscriptions->isTrial) {
                                            // Passing subs Data into $emailData
                                            $emailData['title'] = 'receipt-order-confirmed';
                                            $emailData['moduleData'] = (object)array(
                                                'email' => $order->order->email,
                                                'subscriptionId' => $order->order->subscriptionIds,
                                                'orderId' => $order->order->id,
                                                'receiptId' => $receiptId->id,
                                                'userId' => $order->order->UserId,
                                                'countryId' => $order->order->CountryId,
                                                'isTrial' => 1
                                            );
                                            $emailData['CountryAndLocaleData'] = array($order->Country, $order->User->defaultLanguage);
                                            // hide email redis
                                            EmailQueueService::addHash($receiptId, 'receipt_order_confirmation', $emailData);
                                        } else {
                                            // sendEmailQueueService.addTask({method: 'sendReceiptsEmail',data: {orderId: order.id}, langCode: defLang}));
                                            $emailData['title'] = 'receipt-order-confirmed';
                                            $emailData['moduleData'] = (object)array(
                                                'email' => $order->order->email,
                                                'subscriptionId' => $order->order->subscriptionIds,
                                                'orderId' => $order->order->id,
                                                'receiptId' => $receiptId->id,
                                                'userId' => $order->order->UserId,
                                                'countryId' => $order->order->CountryId,
                                                'isPlan' => 1
                                            );
                                            $emailData['CountryAndLocaleData'] = array($order->Country, $order->User->defaultLanguage);
                                            // hide email redis
                                            EmailQueueService::addHash($receiptId, 'receipt_order_confirmation', $emailData);
                                        }
                                    }
                                } else {
                                    // sendEmailQueueService.addTask({method: 'sendReceiptsEmail',data: {orderId: order.id}, langCode: defLang}));
                                    $emailData['title'] = 'receipt-order-confirmed';
                                    $emailData['moduleData'] = (object)array(
                                        'email' => $order->order->email,
                                        'subscriptionId' => $order->order->email,
                                        'orderId' => $order->order->id,
                                        'receiptId' => $receiptId->id,
                                        'userId' => $order->order->UserId,
                                        'countryId' => $order->order->CountryId,
                                        'isPlan' => 1
                                    );
                                    $emailData['CountryAndLocaleData'] = array($order->Country, $order->User->defaultLanguage);
                                    // hide email redis
                                    EmailQueueService::addHash($receiptId, 'receipt_order_confirmation', $emailData);
                                }

                            }

                        } else {
                            $this->Log->error('HK | readOrderConfirmation | Read order outbound ERROR: OrderId = [' . $orderId . '] not found in database.');
                        }

                    }

                }

            }
        } catch (Exception $ex) {
            $Log->error('HK | readOrderConfirmation | error => ' . $ex->getMessage());
            return 0;
        }

    }
}
