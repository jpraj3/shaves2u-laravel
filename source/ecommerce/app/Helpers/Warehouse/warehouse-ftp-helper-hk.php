<?php

namespace App\Helpers\Warehouse;

use App\Helpers\LaravelHelper;

class FtpHelperHK
{

    public function __construct()
    {
        $this->Log = \Log::channel('cronjob');
        $this->Job = 'create-order-inbound.job / create-bulkorder-inbound.job | ';
        $this->Group = 'HK | ';
        $this->laravelHelper = new LaravelHelper();
        $this->environment_variables = $this->laravelHelper->ConvertArraytoObject(config('environment'));
        $this->config = $this->laravelHelper->ConvertArraytoObject(config('environment.CUSTOM_CONFIG_DATA'));

        // initialize FTP objects
        $this->options = (Object) array();
        $this->options->host = $this->config->Warehouse->HKG->ftp->host;
        $this->options->user = $this->config->Warehouse->HKG->ftp->username;
        $this->options->password = $this->config->Warehouse->HKG->ftp->password;
        $this->options->port = $this->config->Warehouse->HKG->ftp->port;
        $this->options->local_upload = $this->environment_variables->warehouse_paths->HKG->local_upload_folder;
        $this->options->local_download = $this->environment_variables->warehouse_paths->HKG->local_download_folder;
        $this->options->ftp_upload = $this->environment_variables->warehouse_paths->HKG->ftp_outbox_folder;
        $this->options->ftp_download = $this->environment_variables->warehouse_paths->HKG->ftp_inbox_folder;
        $this->options->maxFiles = $this->environment_variables->warehouse_paths->HKG->maxFiles;
        $this->options->retry = $this->environment_variables->warehouse_paths->HKG->retry;

        // status to trigger ftp_close
        $this->canEnd = false;
    }

    // TESTING
    // public function readfromFTP()
    // {
    //     $remotePath = $this->options->ftp_download;
    //     $localPath = $this->options->local_download;
    //     $source = '';
    //     $files = $this->listFiles($remotePath);
    //     $fileNames = [];
    //     foreach ($files as $file) {
    //         if (strrpos($file, 'WMSSHP') !== false) {
    //             array_push($fileNames, $file);
    //         }
    //     }
    //     if (count($fileNames) > 0) {
    //         $this->Log->info($this->Job . $this->Group . "[HK] ==== | Warehouse output files: " . json_encode($fileNames));
    //     }
    //     foreach ($fileNames as $filename) {
    //         $this->getFile($remotePath, $localPath, $filename);
    //         // readOrderConfirmation($localPath,$filename);
    //     }
    // }

    // public function writetoFTP()
    // {
    //     $remotePath = $this->options->ftp_upload;
    //     $localPath = $this->options->local_upload;
    //     $source = $localPath . '/WMSORD_HKG_20190530150441_test_upload.txt';
    //     $filename = 'WMSORD_HKG_20190530150441_test_upload.txt';
    //     $this->putFile($source, $remotePath, $filename); // put file to ftp server
    // }

    public function checkFtp($host, $username, $password, $port, $timeout = 10)
    {
        try {
            $conn = @ftp_connect($host, $port, $timeout);
            if (false === $conn) {
                $this->Log->info($this->Job . $this->Group . "checkFtp => Unable to connect to FTP Server.");
            }
            $try_login = @ftp_login($conn, $username, $password);
            if (true === $try_login) {
                //$this->Log->info($this->Job . $this->Group . "checkFtp => Connected as " . $username);
                return true;
            } else {
                $this->Log->error($this->Job . $this->Group . "checkFtp => Couldn't connect as " . $username);
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }

    }

    // Connection status : 0 - NORMAL , 1 - ABORTED , 2 - TIMEOUT , 3 - ABORTED and TIMEOUT
    public function connect($remotePath = null)
    {
        try {
            $check = $this->checkFtp($this->options->host, $this->options->user, $this->options->password, $this->options->port);
            if ($check === true) {
                //$this->Log->info($this->Job . $this->Group . "connect => connect start");
                $conn = ftp_connect($this->options->host) or die("Couldn't connect to " . $this->options->host);
                $login = ftp_login($conn, $this->options->user, $this->options->password);
                $mode = ftp_pasv($conn, true);
                if ((!$conn) || (!$login) || (!$mode)) {
                    die("FTP connection has failed !");
                } else {
                    $status = connection_status($conn);
                    if ($status === 0) {
                        $this->Log->info($this->Job . $this->Group . "connect => connection success");
                        $remotePath !== null ? ftp_chdir($conn, $remotePath) : '';
                        $this->Log->info($this->Job . $this->Group . "connect => cd into : " . ftp_pwd($conn));
                        return $conn;
                    } else {
                        $this->Log->error($this->Job . $this->Group . "connect => connection failed");
                        return connection_aborted($conn);
                    }
                }
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }
    }

    public function disconnect($conn)
    {
        if ($this->canEnd) {
            $this->Log->info($this->Job . $this->Group . "disconnect => disconnect start");
            return ftp_close($conn);
        } else {
            $this->Log->error($this->Job . $this->Group . "disconnect => FTP cannot be ended");
            return "FTP cannot be ended";
        }
    }

    /**
     * List all files in remote path
     * @param {*} remotePath
     */
    public function listFiles($remotePath)
    {
        $this->Log->info($this->Job . $this->Group . "listFiles => listFiles start");
        $this->Log->info($this->Job . $this->Group . "listFiles => remotePath = " . $remotePath);
        $outputFiles = [];

        try {
            $conn = $this->connect($remotePath);
            if ($conn) {
                $this->canEnd = false;
                $this->Log->info($this->Job . $this->Group . "listFiles => List all files in current directory");
                $files = ftp_nlist($conn, $remotePath);
                if ($files) {
                    foreach ($files as $file) {
                        array_push($outputFiles, $file);
                    }
                    $this->canEnd = true;
                    $this->disconnect($conn);
                    $this->Log->info($this->Job . $this->Group . "listFiles => Lists : " . json_encode($outputFiles));
                    return $outputFiles;
                }
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }
    }

    /**
     * Download and delete file on FTP server
     * @param {*} remotePath
     * @param {*} localPath
     */
    public function getFile($remotePath, $localPath, $filename)
    {
        $this->Log->info($this->Job . $this->Group . "getFile => getFile start");
        $this->Log->info($this->Job . $this->Group . "getFile => remotePath = " . $remotePath);
        try {
            $conn = $this->connect($remotePath);
            if ($conn) {
                $this->canEnd = false;
                $this->Log->info($this->Job . $this->Group . "getFile => List all files in current directory");
                if (ftp_get($conn, $localPath . '/' . $filename, $remotePath . '/' . $filename, FTP_BINARY)) {
                    $this->Log->info($this->Job . $this->Group . "Successfully written to " . $localPath . '/' . $filename);
                    // try to delete $file
                    if (ftp_delete($conn, $remotePath . '/' . $filename)) {
                        $this->Log->info($this->Job . $this->Group . $remotePath . '/' . $filename . 'deleted successful\n');
                    } else {
                        $this->Log->info($this->Job . $this->Group . 'could not delete ' . $remotePath . '/' . $filename);
                    }
                } else {
                    $this->Log->info($this->Job . $this->Group . "There was a problem");
                }
                $this->canEnd = true;
                return $this->disconnect($conn);
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }
    }

    public function putFile($source, $remotePath, $filename)
    {
        $this->Log->info($this->Job . $this->Group . "putFile => putFile start");
        $this->Log->info($this->Job . $this->Group . "putFile => remotePath = " . $remotePath);
        try {
            $conn = $this->connect();
            if ($conn) {
                $this->canEnd = false;
                $this->Log->info($this->Job . $this->Group . "putFile => transfer files to directory : " . $remotePath);
                ftp_put($conn, $remotePath, $source, FTP_ASCII);
                $this->canEnd = true;
                return $this->disconnect($conn);
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }
    }

    public function deleteLocalFile($dir, $file)
    {
        $this->Log->info($this->Job . $this->Group . "deleteLocalFile start");
        $this->Log->info($this->Job . $this->Group . "file = " . $file . ' , dir' . $dir);
        try {
            // Create the user folder if missing
            if (!file_exists($dir)) {
                mkdir($dir, 0777, false);
            }

            if (file_exists($dir . '/' . $file)) {
                unlink($dir . '/' . $file);
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }

    }

    public function deleteRemoteFile($conn, $remotePath, $file)
    {
        $this->Log->info($this->Job . $this->Group . "deleteRemoteFile start");
        $this->Log->info($this->Job . $this->Group . "remotePath = " . $remotePath);
        try {
            if ($conn) {
                $this->canEnd = false;
                $this->Log->info($this->Job . $this->Group . "deleteFile => delete " . $file . " from " . $remotePath);
                ftp_delete($conn, $remotePath . "/" . $file);
                $this->canEnd = true;
                return $this->disconnect($conn);
            }
        } catch (Exception $ex) {
            $this->Log->error($this->Job . $this->Group . 'error => ' . $ex->getMessage());
        }
    }
}
