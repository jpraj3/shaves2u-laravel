<?php

namespace App\Helpers;

use App\Helpers\LaravelHelper;
use App\Helpers\ProductHelper;
use App\Models\Promotions\PromotionCodes;
use Carbon\Carbon;
use Exception;

class PromotionsHelper
{
    // Promotion
    public function promotioncheckV2($promocode, $lang, $country, $userid, $session_data, $type, $usertype)
    {
        try {
            $now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
            $PromotionCodes = "";
            $planid = "";
            $typeid = null;
            if($type){
                if ($type == "trial-plan" ) {
                    $typeid= "1";
                }else if( $type == "custom-plan"){
                    $typeid= "2";
                }else if($type == "awesome-shave-kits"){
                    $typeid= "3";
                }

            }
            if ($usertype == "web") {
                $PromotionCodes = PromotionCodes::join('promotions', 'promotions.id', 'promotioncodes.PromotionId')
                    ->select('promotions.id as pid', 'promotioncodes.id as pcid', 'promotions.*', 'promotioncodes.*')
                    ->where('promotions.CountryId', $country)
                    ->where('promotioncodes.code', $promocode)
                    ->where('promotioncodes.isValid', 1)
                    ->where('promotions.isOnline', 1)
                    ->where('promotions.activeAt', '<=', $now)
                    ->where('promotions.expiredAt', '>=', $now)
                    ->when($typeid != null, function ($q) use ($typeid) {
                        return $q->where(function ($promotion_type) use ($typeid) {
                            $promotion_type->orWhere('promotioncodes.promotion_type', $typeid)->orWhereNull('promotioncodes.promotion_type');
                        });
                    })
                    ->first();
            } else {
                $PromotionCodes = PromotionCodes::join('promotions', 'promotions.id', 'promotioncodes.PromotionId')
                    ->select('promotions.id as pid', 'promotioncodes.id as pcid', 'promotions.*', 'promotioncodes.*')
                    ->where('promotions.CountryId', $country)
                    ->where('promotioncodes.code', $promocode)
                    ->where('promotioncodes.isValid', 1)
                    ->where('promotions.isOffline', 1)
                    ->where('promotions.activeAt', '<=', $now)
                    ->where('promotions.expiredAt', '>=', $now)
                    ->when($typeid != null, function ($q) use ($typeid) {
                        return $q->where(function ($promotion_type) use ($typeid) {
                            $promotion_type->orWhere('promotioncodes.promotion_type', $typeid)->orWhereNull('promotioncodes.promotion_type');
                        });
                    })
                    ->first();
            }
            if ($PromotionCodes) {

                // if ($type == "shave_plans"){
                //     // dd($type);
                // }
                if ($type == "trial-plan") {
                    if ($PromotionCodes->planCountryIds !== "all") {
                        if ($session_data) {
                            $planid = $session_data;
                            $PromotionCodes->planCountryIds = str_replace("[", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace("]", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $splitpromotion = $PromotionCodes->planCountryIds;
                            if (strpos($splitpromotion, ',') !== false) {
                                $splitpromotion = explode(",", $PromotionCodes->planCountryIds);
                            }

                            if (is_array($splitpromotion)) {
                                if (in_array($planid, $splitpromotion, true)) {} else {
                                    return "invalidcode";
                                }
                            } else {
                                if (strpos($splitpromotion, $planid) !== false) {} else {
                                    return "invalidcode";
                                }
                            }
                        } else {
                            return "invalidcode";
                        }
                    }
                } 
                else if ($type == "custom-plan") {
                    if ($PromotionCodes->planCountryIds !== "all") {
                        if ($session_data) {
                            $planid = $session_data;
                            $PromotionCodes->planCountryIds = str_replace("[", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace("]", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $splitpromotion = $PromotionCodes->planCountryIds;
                            if (strpos($splitpromotion, ',') !== false) {
                                $splitpromotion = explode(",", $PromotionCodes->planCountryIds);
                            }

                            if (is_array($splitpromotion)) {
                                if (in_array($planid, $splitpromotion, true)) {} else {
                                    return "invalidcode";
                                }
                            } else {
                                if (strpos($splitpromotion, $planid) !== false) {} else {
                                    return "invalidcode";
                                }
                            }
                        } else {
                            return "invalidcode";
                        }
                    }
                } 
                
                else if ($type == "awesome-shave-kits") {
                    if ($PromotionCodes->productCountryIds !== "all") {
                        if ($session_data) {
                            $productid = $session_data;
                            $PromotionCodes->productCountryIds = str_replace("[", "", $PromotionCodes->productCountryIds);
                            $PromotionCodes->productCountryIds = str_replace("]", "", $PromotionCodes->productCountryIds);
                            $PromotionCodes->productCountryIds = str_replace('"', "", $PromotionCodes->productCountryIds);
                            $PromotionCodes->productCountryIds = str_replace('"', "", $PromotionCodes->productCountryIds);
                            $splitpromotion = $PromotionCodes->productCountryIds;
                            if (strpos($splitpromotion, ',') !== false) {
                                $splitpromotion = explode(",", $PromotionCodes->productCountryIds);
                            }
                            if (is_array($splitpromotion)) {
                                if (in_array($productid, $splitpromotion, true)) {} else {
                                    return "invalidcode";
                                }
                            } else {
                                if (strpos($splitpromotion, $productid) !== false) {} else {
                                    return "invalidcode";
                                }
                            }
                        } else {
                            return "invalidcode";
                        }
                    }
                }

                else if ($type == "shave_plans") {
                    if ($PromotionCodes->planCountryIds !== "all") {
                        if ($session_data) {
                            $planid = $session_data["user_data"]["subscription"]["PlanId"];
                            $PromotionCodes->planCountryIds = str_replace("[", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace("]", "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
                            $splitpromotion = $PromotionCodes->planCountryIds;
                            if (strpos($splitpromotion, ',') !== false) {
                                $splitpromotion = explode(",", $PromotionCodes->planCountryIds);
                            }

                            if (is_array($splitpromotion)) {
                                if (in_array($planid, $splitpromotion, true)) {} else {
                                    return "invalidcode";
                                }
                            } else {
                                if (strpos($splitpromotion, $planid) !== false) {} else {
                                    return "invalidcode";
                                }
                            }
                        } else {
                            return "invalidcode";
                        }
                    }
                } 
                //  else {
                //     throw "invalidcode";
                // }

                    $PromotionCodes->appliedTo = str_replace("{", "", $PromotionCodes->appliedTo);
                    $PromotionCodes->appliedTo = str_replace("}", "", $PromotionCodes->appliedTo);
                    $splitapplyto = $PromotionCodes->appliedTo;
                    $peruse = $PromotionCodes->timePerUser;
                    if (strpos($splitapplyto, ',') !== false) {
                        $splitapplyto = explode(",", $PromotionCodes->appliedTo);
                    }

                    if (is_array($splitapplyto)) {
                        foreach ($splitapplyto as $at) {
                            if (strpos($at, ':') !== false) {
                                $splitqty = explode(":", $at);
                                // [0] UserId, [1] Quantity
                                if ($userid === $splitqty[0]) {
                                    if ($splitqty[1] >= $peruse) {
                                        return 'code_used';
                                    }
                                }
                            }
                        }
                    } else {
                        if (strpos($splitapplyto, ':') !== false) {
                            $splitqty = explode(":", $splitapplyto);
                            if ($userid === $splitqty[0]) {
                                // [0] UserId, [1] Quantity
                                if ($splitqty[1] >= $peruse) {
                                    return 'code_used';
                                }
                            }
                        }
                    }

                    if ($PromotionCodes->freeProductCountryIds) {
                        $PromotionCodes->freeProductCountryIds = str_replace("[", "", $PromotionCodes->freeProductCountryIds);
                        $PromotionCodes->freeProductCountryIds = str_replace("]", "", $PromotionCodes->freeProductCountryIds);
                        $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);
                        $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);

                        $splitproduct = $PromotionCodes->freeProductCountryIds;
                        if (strpos($splitproduct, ',') !== false) {
                            $splitproduct = explode(",", $PromotionCodes->freeProductCountryIds);
                        }
                        $productseperate = [];
                        if (is_array($splitproduct)) {
                            foreach ($splitproduct as $product) {
                                array_push($productseperate, $product);
                            }
                        } else {
                            array_push($productseperate, $splitproduct);
                        }

                        $this->productHelper = new ProductHelper();
                        $this->laravelHelper = new LaravelHelper();
                        $this->appType = $usertype === 'web' ? 'ecommerce' : 'baWebsite';
                        $this->isOnline = $this->appType === 'web' ? 1 : 0;
                        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;

                        // Prepare parameters for trial plan order details
                        $productcountyidarray = array(
                            "appType" => $this->appType,
                            "isOnline" => 1,
                            "isOffline" => 1,
                            "product_country_ids" => $productseperate,
                        );

                        $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($country, $lang, $productcountyidarray));

                        $freeproductid = [];
                        $freeproductcid = [];
                        $freeproductsku = [];
                        $freeproductname = [];
                        $freeproductimage = [];
                        $freeproductsellprice = [];
                        foreach ($product as $p) {
                            array_push($freeproductid, $p->ProductId);
                            array_push($freeproductcid, $p->ProductCountryId);
                            array_push($freeproductsku, $p->sku);
                            array_push($freeproductname, $p->producttranslatesname);
                            array_push($freeproductimage, $p->productDefaultImage);
                            array_push($freeproductsellprice, $p->sellPrice);
                        }
                        $PromotionCodes["freeproductid"] = $freeproductid;
                        $PromotionCodes["freeproductcid"] = $freeproductcid;
                        $PromotionCodes["freeproductsku"] = $freeproductsku;
                        $PromotionCodes["freeproductname"] = $freeproductname;
                        $PromotionCodes["freeproductimage"] = $freeproductimage;
                        $PromotionCodes["freeproductsellprice"] = $freeproductsellprice;
                    } else {
                        $PromotionCodes["freeproductid"] = [];
                        $PromotionCodes["freeproductcid"] = [];
                        $PromotionCodes["freeproductsku"] = [];
                        $PromotionCodes["freeproductname"] = [];
                        $PromotionCodes["freeproductimage"] = [];
                        $PromotionCodes["freeproductsellprice"] = [];
                    }

                    if ($PromotionCodes->freeExistProductCountryIds) {
                        $PromotionCodes->freeExistProductCountryIds = str_replace("[", "", $PromotionCodes->freeExistProductCountryIds);
                        $PromotionCodes->freeExistProductCountryIds = str_replace("]", "", $PromotionCodes->freeExistProductCountryIds);
                        $PromotionCodes->freeExistProductCountryIds = str_replace('"', "", $PromotionCodes->freeExistProductCountryIds);
                        $PromotionCodes->freeExistProductCountryIds = str_replace('"', "", $PromotionCodes->freeExistProductCountryIds);

                        $splitproduct = $PromotionCodes->freeExistProductCountryIds;
                        if (strpos($splitproduct, ',') !== false) {
                            $splitproduct = explode(",", $PromotionCodes->freeExistProductCountryIds);
                        }
                        $productseperate = [];
                        if (is_array($splitproduct)) {
                            foreach ($splitproduct as $product) {
                                array_push($productseperate, $product);
                            }
                        } else {
                            array_push($productseperate, $splitproduct);
                        }

                        $this->productHelper = new ProductHelper();
                        $this->laravelHelper = new LaravelHelper();
                        $this->appType = $usertype === 'web' ? 'ecommerce' : 'baWebsite';
                        $this->isOnline = $this->appType === 'web' ? 1 : 0;
                        $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;

                        // Prepare parameters for trial plan order details
                        $productcountyidarray = array(
                            "appType" => $this->appType,
                            "isOnline" => 1,
                            "isOffline" => 1,
                            "product_country_ids" => $productseperate,
                        );
                        $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($country, $lang, $productcountyidarray));

                        $freeexistproductid = [];
                        $freeexistproductcid = [];
                        $freeexistproductsku = [];
                        $freeexistproductname = [];
                        $freeexistproductimage = [];
                        $freeexistproductsellprice = [];
                        foreach ($product as $p) {
                            array_push($freeexistproductid, $p->ProductId);
                            array_push($freeexistproductcid, $p->ProductCountryId);
                            array_push($freeexistproductsku, $p->sku);
                            array_push($freeexistproductname, $p->producttranslatesname);
                            array_push($freeexistproductimage, $p->productDefaultImage);
                            array_push($freeexistproductsellprice, $p->sellPrice);
                        }

                        $PromotionCodes["freeexistproductid"] = $freeexistproductid;
                        $PromotionCodes["freeexistproductcid"] = $freeexistproductcid;
                        $PromotionCodes["freeexistproductsku"] = $freeexistproductsku;
                        $PromotionCodes["freeexistproductname"] = $freeexistproductname;
                        $PromotionCodes["freeexistproductimage"] = $freeexistproductimage;
                        $PromotionCodes["freeexistproductsellprice"] = $freeexistproductsellprice;
                    } else {
                        $PromotionCodes["freeexistproductid"] = [];
                        $PromotionCodes["freeexistproductcid"] = [];
                        $PromotionCodes["freeexistproductsku"] = [];
                        $PromotionCodes["freeexistproductname"] = [];
                        $PromotionCodes["freeexistproductimage"] = [];
                        $PromotionCodes["freeexistproductsellprice"] = [];
                    }

                    return $PromotionCodes;
                    // if (identity && appliedTo && appliedTo[identity.id] && appliedTo[identity.id] >= promotion.timePerUser) {
                    //   throw new Error('code_used');
                    // }
                
            } else {
                return "invalidcode";
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function promotionInfo($promocode, $country)
    {
        $now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
        $PromotionCodes = "";
        $planid = "";
        $PromotionCodes = PromotionCodes::join('promotions', 'promotions.id', 'promotioncodes.PromotionId')
            ->select('promotions.id as pid', 'promotioncodes.id as pcid', 'promotions.*', 'promotioncodes.*')
            ->where('promotions.CountryId', $country->id)
            ->where('promotioncodes.code', $promocode)
            ->where('promotioncodes.isValid', 1)
        // ->where('promotions.activeAt', '<=', $now)
        // ->where('promotions.expiredAt', '>=', $now)
            ->first();

        return $PromotionCodes;

    }


    // public function promotioncheck($promocode, $lang, $country, $userid, $session_data, $type, $usertype)
    // {
    //     try {
    //         $now = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d');
    //         $PromotionCodes = "";
    //         $planid = "";
    //         $typeid = null;
    //         if($type){
    //             if ($type == "trial-plan" ) {
    //                 $typeid= "1";
    //             }else if( $type == "custom-plan"){
    //                 $typeid= "2";
    //             }else if($type == "awesome-shave-kits"){
    //                 $typeid= "3";
    //             }

    //         }
           
    //         if ($usertype == "web") {
    //             $PromotionCodes = PromotionCodes::join('promotions', 'promotions.id', 'promotioncodes.PromotionId')
    //                 ->select('promotions.id as pid', 'promotioncodes.id as pcid', 'promotions.*', 'promotioncodes.*')
    //                 ->where('promotions.CountryId', $country)
    //                 ->where('promotioncodes.code', $promocode)
    //                 ->where('promotioncodes.isValid', 1)
    //                 ->where('promotions.isOnline', 1)
    //                 ->where('promotions.activeAt', '<=', $now)
    //                 ->where('promotions.expiredAt', '>=', $now)
    //                 ->when($typeid != null, function ($q) use ($typeid) {
    //                     return $q->where(function ($promotion_type) use ($typeid) {
    //                         $promotion_type->orWhere('promotioncodes.promotion_type', $typeid)->orWhereNull('promotioncodes.promotion_type');
    //                     });
    //                 })
    //                 ->first();
    //         } else {
    //             $PromotionCodes = PromotionCodes::join('promotions', 'promotions.id', 'promotioncodes.PromotionId')
    //                 ->select('promotions.id as pid', 'promotioncodes.id as pcid', 'promotions.*', 'promotioncodes.*')
    //                 ->where('promotions.CountryId', $country)
    //                 ->where('promotioncodes.code', $promocode)
    //                 ->where('promotioncodes.isValid', 1)
    //                 ->where('promotions.isOffline', 1)
    //                 ->where('promotions.activeAt', '<=', $now)
    //                 ->where('promotions.expiredAt', '>=', $now)
    //                 ->when($typeid != null, function ($q) use ($typeid) {
    //                     return $q->where(function ($promotion_type) use ($typeid) {
    //                         $promotion_type->orWhere('promotioncodes.promotion_type', $typeid)->orWhereNull('promotioncodes.promotion_type');
    //                     });
    //                 })
    //                 ->first();
    //         }
    //         if ($PromotionCodes) {
    //             if ($type == "custom-plan" || $type == "trial-plan") {
    //                 if ($PromotionCodes->planCountryIds !== "all") {
    //                     if ($session_data["selection"]["step5"]["planId"]) {
    //                         $planid = $session_data["selection"]["step5"]["planId"];
    //                         $PromotionCodes->planCountryIds = str_replace("[", "", $PromotionCodes->planCountryIds);
    //                         $PromotionCodes->planCountryIds = str_replace("]", "", $PromotionCodes->planCountryIds);
    //                         $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
    //                         $PromotionCodes->planCountryIds = str_replace('"', "", $PromotionCodes->planCountryIds);
    //                         $splitpromotion = $PromotionCodes->planCountryIds;
    //                         if (strpos($splitpromotion, ',') !== false) {
    //                             $splitpromotion = explode(",", $PromotionCodes->planCountryIds);
    //                         }

    //                         if (is_array($splitpromotion)) {
    //                             if (in_array($planid, $splitpromotion, true)) {} else {
    //                                 throw "invalidcode";
    //                             }
    //                         } else {
    //                             if (strpos($splitpromotion, $planid) !== false) {} else {
    //                                 throw "invalidcode";
    //                             }
    //                         }
    //                     } else {
    //                         throw "invalidcode";
    //                     }
    //                 }
    //             } else if ($type == "awesome-shave-kits") {
    //                 if ($PromotionCodes->productCountryIds !== "all") {
    //                     if ($session_data["selection"]["step1"][0]["productcountryid"]) {
    //                         $productid = $session_data["selection"]["step1"][0]["productcountryid"];
    //                         $PromotionCodes->productCountryIds = str_replace("[", "", $PromotionCodes->productCountryIds);
    //                         $PromotionCodes->productCountryIds = str_replace("]", "", $PromotionCodes->productCountryIds);
    //                         $PromotionCodes->productCountryIds = str_replace('"', "", $PromotionCodes->productCountryIds);
    //                         $PromotionCodes->productCountryIds = str_replace('"', "", $PromotionCodes->productCountryIds);
    //                         $splitpromotion = $PromotionCodes->productCountryIds;
    //                         if (strpos($splitpromotion, ',') !== false) {
    //                             $splitpromotion = explode(",", $PromotionCodes->productCountryIds);
    //                         }
    //                         if (is_array($splitpromotion)) {
    //                             if (in_array($productid, $splitpromotion, true)) {} else {
    //                                 throw "invalidcode";
    //                             }
    //                         } else {
    //                             if (strpos($splitpromotion, $productid) !== false) {} else {
    //                                 throw "invalidcode";
    //                             }
    //                         }
    //                     } else {
    //                         throw "invalidcode";
    //                     }
    //                 }
    //             } else {
    //                 throw "invalidcode";
    //             }
    //             if ($PromotionCodes->isGeneric === 1) {
    //                 $PromotionCodes->appliedTo = str_replace("{", "", $PromotionCodes->appliedTo);
    //                 $PromotionCodes->appliedTo = str_replace("}", "", $PromotionCodes->appliedTo);
    //                 $splitapplyto = $PromotionCodes->appliedTo;
    //                 $peruse = $PromotionCodes->timePerUser;
    //                 if (strpos($splitapplyto, ',') !== false) {
    //                     $splitapplyto = explode(",", $PromotionCodes->appliedTo);
    //                 }

    //                 if (is_array($splitapplyto)) {
    //                     foreach ($splitapplyto as $at) {
    //                         if (strpos($at, ':') !== false) {
    //                             $splitqty = explode(":", $at);
    //                             // [0] UserId, [1] Quantity
    //                             if ($userid === $splitqty[0]) {
    //                                 if ($splitqty[1] >= $peruse) {
    //                                     throw new Exception('code_used');
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 } else {
    //                     if (strpos($splitapplyto, ':') !== false) {
    //                         $splitqty = explode(":", $splitapplyto);
    //                         if ($userid === $splitqty[0]) {
    //                             // [0] UserId, [1] Quantity
    //                             if ($splitqty[1] >= $peruse) {
    //                                 throw new Exception('code_used');
    //                             }
    //                         }
    //                     }
    //                 }

    //                 if ($PromotionCodes->freeProductCountryIds) {
    //                     $PromotionCodes->freeProductCountryIds = str_replace("[", "", $PromotionCodes->freeProductCountryIds);
    //                     $PromotionCodes->freeProductCountryIds = str_replace("]", "", $PromotionCodes->freeProductCountryIds);
    //                     $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);
    //                     $PromotionCodes->freeProductCountryIds = str_replace('"', "", $PromotionCodes->freeProductCountryIds);

    //                     $splitproduct = $PromotionCodes->freeProductCountryIds;
    //                     if (strpos($splitproduct, ',') !== false) {
    //                         $splitproduct = explode(",", $PromotionCodes->freeProductCountryIds);
    //                     }
    //                     $productseperate = [];
    //                     if (is_array($splitproduct)) {
    //                         foreach ($splitproduct as $product) {
    //                             array_push($productseperate, $product);
    //                         }
    //                     } else {
    //                         array_push($productseperate, $splitproduct);
    //                     }

    //                     $this->productHelper = new ProductHelper();
    //                     $this->laravelHelper = new LaravelHelper();
    //                     $this->appType = config('app.appType');
    //                     $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
    //                     $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;

    //                     // Prepare parameters for trial plan order details
    //                     $productcountyidarray = array(
    //                         "appType" => $this->appType,
    //                         "isOnline" => 1,
    //                         "isOffline" => 1,
    //                         "product_country_ids" => $productseperate,
    //                     );
    //                     $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($country, $lang, $productcountyidarray));

    //                     $freeproductid = [];
    //                     $freeproductcid = [];
    //                     $freeproductsku = [];
    //                     $freeproductname = [];
    //                     $freeproductimage = [];
    //                     $freeproductsellprice = [];
    //                     foreach ($product as $p) {
    //                         array_push($freeproductid, $p->ProductId);
    //                         array_push($freeproductcid, $p->ProductCountryId);
    //                         array_push($freeproductsku, $p->sku);
    //                         array_push($freeproductname, $p->producttranslatesname);
    //                         array_push($freeproductimage, $p->productDefaultImage);
    //                         array_push($freeproductsellprice, $p->sellPrice);
    //                     }
    //                     $PromotionCodes["freeproductid"] = $freeproductid;
    //                     $PromotionCodes["freeproductcid"] = $freeproductcid;
    //                     $PromotionCodes["freeproductsku"] = $freeproductsku;
    //                     $PromotionCodes["freeproductname"] = $freeproductname;
    //                     $PromotionCodes["freeproductimage"] = $freeproductimage;
    //                     $PromotionCodes["freeproductsellprice"] = $freeproductsellprice;
    //                 } else {
    //                     $PromotionCodes["freeproductid"] = [];
    //                     $PromotionCodes["freeproductcid"] = [];
    //                     $PromotionCodes["freeproductsku"] = [];
    //                     $PromotionCodes["freeproductname"] = [];
    //                     $PromotionCodes["freeproductimage"] = [];
    //                     $PromotionCodes["freeproductsellprice"] = [];
    //                 }

    //                 if ($PromotionCodes->freeExistProductCountryIds) {
    //                     $PromotionCodes->freeExistProductCountryIds = str_replace("[", "", $PromotionCodes->freeExistProductCountryIds);
    //                     $PromotionCodes->freeExistProductCountryIds = str_replace("]", "", $PromotionCodes->freeExistProductCountryIds);
    //                     $PromotionCodes->freeExistProductCountryIds = str_replace('"', "", $PromotionCodes->freeExistProductCountryIds);
    //                     $PromotionCodes->freeExistProductCountryIds = str_replace('"', "", $PromotionCodes->freeExistProductCountryIds);

    //                     $splitproduct = $PromotionCodes->freeExistProductCountryIds;
    //                     if (strpos($splitproduct, ',') !== false) {
    //                         $splitproduct = explode(",", $PromotionCodes->freeExistProductCountryIds);
    //                     }
    //                     $productseperate = [];
    //                     if (is_array($splitproduct)) {
    //                         foreach ($splitproduct as $product) {
    //                             array_push($productseperate, $product);
    //                         }
    //                     } else {
    //                         array_push($productseperate, $splitproduct);
    //                     }

    //                     $this->productHelper = new ProductHelper();
    //                     $this->laravelHelper = new LaravelHelper();
    //                     $this->appType = config('app.appType');
    //                     $this->isOnline = $this->appType === 'ecommerce' ? 1 : 0;
    //                     $this->isOffline = $this->appType === 'baWebsite' ? 1 : 0;

    //                     // Prepare parameters for trial plan order details
    //                     $productcountyidarray = array(
    //                         "appType" => $this->appType,
    //                         "isOnline" => 1,
    //                         "isOffline" => 1,
    //                         "product_country_ids" => $productseperate,
    //                     );
    //                     $product = $this->laravelHelper->ConvertArraytoObject($this->productHelper->getProductByProductCountries($country, $lang, $productcountyidarray));

    //                     $freeexistproductid = [];
    //                     $freeexistproductcid = [];
    //                     $freeexistproductsku = [];
    //                     $freeexistproductname = [];
    //                     $freeexistproductimage = [];
    //                     $freeexistproductsellprice = [];
    //                     foreach ($product as $p) {
    //                         array_push($freeexistproductid, $p->ProductId);
    //                         array_push($freeexistproductcid, $p->ProductCountryId);
    //                         array_push($freeexistproductsku, $p->sku);
    //                         array_push($freeexistproductname, $p->producttranslatesname);
    //                         array_push($freeexistproductimage, $p->productDefaultImage);
    //                         array_push($freeexistproductsellprice, $p->sellPrice);
    //                     }

    //                     $PromotionCodes["freeexistproductid"] = $freeexistproductid;
    //                     $PromotionCodes["freeexistproductcid"] = $freeexistproductcid;
    //                     $PromotionCodes["freeexistproductsku"] = $freeexistproductsku;
    //                     $PromotionCodes["freeexistproductname"] = $freeexistproductname;
    //                     $PromotionCodes["freeexistproductimage"] = $freeexistproductimage;
    //                     $PromotionCodes["freeexistproductsellprice"] = $freeexistproductsellprice;
    //                 } else {
    //                     $PromotionCodes["freeexistproductid"] = [];
    //                     $PromotionCodes["freeexistproductcid"] = [];
    //                     $PromotionCodes["freeexistproductsku"] = [];
    //                     $PromotionCodes["freeexistproductname"] = [];
    //                     $PromotionCodes["freeexistproductimage"] = [];
    //                     $PromotionCodes["freeexistproductsellprice"] = [];
    //                 }

    //                 return $PromotionCodes;
    //                 // if (identity && appliedTo && appliedTo[identity.id] && appliedTo[identity.id] >= promotion.timePerUser) {
    //                 //   throw new Error('code_used');
    //                 // }
    //             }
    //         } else {
    //             throw "invalidcode";
    //         }
    //     } catch (Exception $e) {
    //         throw $e;
    //     }
    // }
}
