<?php

namespace App\Helpers;

use App\Models\Plans\PlanCategory;
use App\Models\Plans\PlanCategoryDetails;
use App\Models\Plans\PlanImages;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Products\ProductType;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use DB;

/*
use App\Helpers\PlanHelper;
public $plan;
$this->plan = new PlanHelper();

$this->plan->getPlanId($data);
 */

class PlanHelper
{
    public function getPlanId($data)
    {

        $productcountryget = "119,135"; // combine productCountry by comma
        $planid = '';
        $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
            //->select('plans.id',DB::raw('group_concat(plan_details.ProductCountryId) as names'))
            ->select('plans.id', DB::raw('(SELECT group_concat(plan_details.ProductCountryId order by plan_details.ProductCountryId) FROM plan_details WHERE plan_details.PlanId = plans.id  GROUP BY plan_details.PlanId) as productlist'))
            ->where('plansku.duration', 4) // month
            ->where('plans.CountryId', 1) // country (may do in here)
            ->where('plans.plantype', 't') // type(trial or custom)
            ->get();

        foreach ($planlist as $pl) {
            if ($productcountryget == $pl->productlist) {
                $planid = $pl->id;
            }
        }
        return $planid;
    }

    public function getPlanDetailByCategory($data)
    {
        $category_name = 'test2';
        $CountryId = '1';
        $LangCode = 'EN';
        $planid = '';
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'productcountries.id as productcid', 'products.sku as productsku', 'producttranslates.name as translatename', 'products.id as productId')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.CountryId', $CountryId) // country
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productc"][] = $planlist[$i]->productcid;
                            $PlanList[$combinePlanId[1]]["product"][] = $planlist[$i]->productsku;
                            $PlanList[$combinePlanId[1]]["translatename"][] = $planlist[$i]->translatename;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["isAnnual"] = $planlist[$i]->isAnnual;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;

                    $PlanList[$count]["productc"][] = $planlist[$i]->productcid;
                    $PlanList[$count]["product"][] = $planlist[$i]->productsku;
                    $PlanList[$count]["translatename"][] = $planlist[$i]->translatename;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        $planimages = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('planimages', 'planimages.PlanId', 'plans.id')
            ->select('planimages.*')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.CountryId', $CountryId) // country
            ->orderByRaw("planimages.PlanId asc, planimages.url asc")
            ->get();
        if ($planimages) {
            for ($i = 0; $i < count($PlanList); $i++) {
                for ($a = 0; $a < count($planimages); $a++) {
                    if ($PlanList[$i]["planid"] == $planimages[$a]["PlanId"]) {
                        $PlanList[$i]["planimageurl"][] = $planimages[$a]["url"];
                        $PlanList[$i]["planimageisDefault"][] = $planimages[$a]["isDefault"];
                    }
                }
            }
        }

        // get function in the page u need
        // $plandurationarray = array();
        // for ($i = 0; $i < count($PlanList); $i++) {
        //     echo ' a' . $PlanList[$i]["planid"];  // get planid
        //     echo ' b' . $PlanList[$i]["planduration"];  // get planid
        //     $plandurationarray[] = $PlanList[$i]["planduration"] ;
        //     for ($a = 0; $a < count($PlanList[$i]["productc"]); $a++) {  //get product
        //         echo ' c' . $PlanList[$i]["productc"][$a];
        //         echo ' d' . $PlanList[$i]["product"][$a];
        //         echo ' e' . $PlanList[$i]["translatename"][$a];
        //     }
        // }
        // $month = array_unique($plandurationarray);
        // sort($month);
        return $PlanList;
    }

    public function getPlanIdPriceByCategory($CountryId, $LangCode, $PlanType, $plancategories)
    {
        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;
        $PlanType = $PlanType;
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.trialPrice', 'plans.sellPrice as sellPrice', 'plans.isOnline as isOnline', 'plans.isOffline as isOffline', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.plantype', $PlanType) // type
            ->where('plans.CountryId', $CountryId) // country
            ->where('plansku.isAnnual', 0) // non annual plans
            ->where('plansku.status', 1) // non annual plans
            ->where('plans.isOnline', 1) // non annual plans
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["trialPrice"] = $planlist[$i]->trialPrice;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["isOnline"] = $planlist[$i]->isOnline;
                    $PlanList[$count]["isOffline"] = $planlist[$i]->isOffline;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return $PlanList;
    }

    public function getPlanIdPriceAnnualByCategory($CountryId, $LangCode, $PlanType, $plancategories)
    {
        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;
        $PlanType = $PlanType;
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.trialPrice', 'plans.sellPrice as sellPrice', 'plans.isOnline as isOnline', 'plans.isOffline as isOffline', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.plantype', $PlanType) // type
            ->where('plans.CountryId', $CountryId) // country
            ->where('plansku.isAnnual', 1) // non annual plans
            ->where('plansku.status', 1) // non annual plans
            ->where('plans.isOnline', 1) // non annual plans
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["trialPrice"] = $planlist[$i]->trialPrice;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["isOnline"] = $planlist[$i]->isOnline;
                    $PlanList[$count]["isOffline"] = $planlist[$i]->isOffline;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return $PlanList;
    }

    public function getPlanIdPriceByCategoryBAONLY($CountryId, $LangCode, $PlanType, $plancategories)
    {
        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;
        $PlanType = $PlanType;
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.trialPrice', 'plans.sellPrice as sellPrice', 'plans.isOnline as isOnline', 'plans.isOffline as isOffline', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.plantype', $PlanType) // type
            ->where('plans.CountryId', $CountryId) // country
            ->where('plansku.isAnnual', 0) // country
            ->where('plans.isOffline', 1) // country
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["trialPrice"] = $planlist[$i]->trialPrice;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["isOnline"] = $planlist[$i]->isOnline;
                    $PlanList[$count]["isOffline"] = $planlist[$i]->isOffline;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return $PlanList;
    }

    public function getPlanIdPriceByCategoryAnnualInfo($CountryId, $LangCode, $PlanType, $plancategories, $existing_trial_plan, $gender = null)
    {

        if ($existing_trial_plan) {
            $session_selection = json_decode($existing_trial_plan["session_data"]);
            $selected_blade = $session_selection->selection->step1->selected_blade;
            $selected_addon_list = $gender !== null && $gender === 'female' ? [] : $session_selection->selection->step2->selected_addon_list;
            $selected_frequency = (int) $session_selection->selection->step3->selected_frequency;
        }

        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;
        $PlanType = $PlanType;
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select(
                'plansku.duration as planduration',
                'plansku.sku as plansku',
                'plans.id as planid',
                'plans.trialPrice',
                'plans.sellPrice as sellPrice',
                'plans.isOnline as isOnline',
                'plans.isOffline as isOffline',
                'productcountries.id as productcid',
                'plan_details.qty as productcqty'
            )
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.isOffline', 1) // group name
            ->where('plansku.isAnnual', 1) // annual plans
            ->where('plans.plantype', $PlanType) // type
            ->where('plans.CountryId', $CountryId) // country
            ->where('plansku.duration', $selected_frequency) // country
            ->where('plansku.status', 1) // country
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        // return $planlist;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["trialPrice"] = $planlist[$i]->trialPrice;
                    $PlanList[$count]["basedPrice"] = $planlist[$i]->basedPrice;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["isOnline"] = $planlist[$i]->isOnline;
                    $PlanList[$count]["isOffline"] = $planlist[$i]->isOffline;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        $relevantAnnualPlan = null;
        foreach ($PlanList as $PL) {
            foreach ($PL["productCountryId"] as $PC) {
                if ($PC === (int) $selected_blade->productcountriesid) {
                    $relevantAnnualPlan = $PL;
                }
            }
        }

        return $this->getPlanDetailsByPlanId($relevantAnnualPlan["planid"], $CountryId, $LangCode);
    }

    public function getPlanIdPriceByCategoryAnnual($CountryId, $LangCode, $PlanType, $plancategories)
    {
        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;
        $PlanType = $PlanType;
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.trialPrice', 'plans.sellPrice as sellPrice', 'plans.isOnline as isOnline', 'plans.isOffline as isOffline', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plansku.isAnnual', 1) // annual plans
            ->where('plans.plantype', $PlanType) // type
            ->where('plans.CountryId', $CountryId) // country
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["trialPrice"] = $planlist[$i]->trialPrice;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["isOnline"] = $planlist[$i]->isOnline;
                    $PlanList[$count]["isOffline"] = $planlist[$i]->isOffline;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return $PlanList;
    }

    public function getPlanIdPriceByCategoryCampaign($CountryId, $LangCode, $plancategories)
    {
        $category_name = $plancategories;
        $CountryId = $CountryId;
        $LangCode = $LangCode;

        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plans.price as base_price','plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.sellPrice as sellPrice', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.CountryId', $CountryId) // country
            ->where('plansku.status', 1) 
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->get();

        $PlanList = array();
        $insertPlanIdArray = [];
        $insertCombinePlanIdArray = [];
        $count = 0;

        if ($planlist) {
            for ($i = 0; $i < count($planlist); $i++) {
                $planId = (string) $planlist[$i]->planid;
                if (in_array($planId, $insertPlanIdArray)) { // if have planid in array
                    foreach ($insertCombinePlanIdArray as $combinePlanIdList) { // if have planid in array
                        $combinePlanId = explode(',', $combinePlanIdList);
                        if ($combinePlanId[0] == $planlist[$i]->planid) { //check in which group
                            $PlanList[$combinePlanId[1]]["productCountryId"][] = $planlist[$i]->productcid;
                            // $PlanList[$count]["planduration"][] = $planlist[$i]->planduration;
                        }
                    }
                } else { // if dun have planid in array
                    $PlanList[$count]["plansku"] = $planlist[$i]->plansku;
                    $PlanList[$count]["planid"] = $planlist[$i]->planid;
                    $PlanList[$count]["planduration"] = $planlist[$i]->planduration;
                    $PlanList[$count]["basePrice"] = $planlist[$i]->base_price;
                    $PlanList[$count]["sellPrice"] = $planlist[$i]->sellPrice;
                    $PlanList[$count]["productCountryId"][] = $planlist[$i]->productcid;

                    $insertPlanIdArray[] = (string) $planlist[$i]->planid;
                    $insertCombinePlanIdArray[] = (string) $planlist[$i]->planid . "," . $count;
                    $count = $count + 1;
                }
            }
        }

        return $PlanList;
    }

    public function getPlanDurationByCategory($data)
    {
        $category_name = 'test2';
        $CountryId = '1';
        $LangCode = 'EN';
        $planid = '';
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->select('plansku.duration as planduration')
            ->where('plan_categories.name', $category_name) // group name
            ->where('plans.CountryId', $CountryId) // country
            ->orderByRaw("planduration asc")
            ->get();

        // get function in the page u need
        $plandurationarray = array();
        for ($i = 0; $i < count($planlist); $i++) {
            $plandurationarray[] = $planlist[$i]["planduration"];
        }
        $month = array_unique($plandurationarray);
        sort($month);
    }

    public function getPlanData($data)
    {
        $planlist = Plans::whereIn('id', $data) // plan ids list in array
            ->get();
        return $planlist;
    }

    public function getOnePlanData($data)
    {
        $planlist = Plans::where('id', $data) // plan id
            ->first();
        return $planlist;
    }

    public function getPlanSKUDetail($data)
    {
        $plansku = 1;

        $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->where('plansku.id', $plansku) // sku id
            ->get();

        return $planlist;
    }

    public function getPlanImage($data)
    {
        $planid = 1;

        $planlist = PlanImages::where('PlanId', $planid) // plan id
            ->get();

        return $planlist;
    }

    public function getPlanDetailsByPlanId($PlanId, $CountryId, $LangCode)
    {
        $plan = Plans::join('plan_category_details', 'plan_category_details.PlanSkuId', 'plans.PlanSkuId')
            ->join('plan_categories', 'plan_categories.id', 'plan_category_details.PlanCategoryId')
            ->join('plansku', 'plansku.id', 'plans.PlanSkuId')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->leftJoin('plantrialproducts', 'plantrialproducts.PlanId', '=', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->select(
                'plansku.isAnnual as PlanSkuIsAnnual',
                'plansku.duration as planduration',
                'plansku.sku as plansku',
                'plans.id as planid',
                'plans.taxAmount as tax_amount',
                'plans.discountAmount as discount_amount',
                'plans.trialPrice as trial_price',
                'plans.sellPrice as sales_price',
                'plans.price as base_price',
                'plans.discountPercent as discountPercent',
                'plans.discountAmount as discountAmount',
                'plans.planType as planType',
                'plan_details.cycle as productcountrycycle',
                'plan_details.isAnnual',
                'plan_details.qty as ProductCountryQty',
                'productcountries.id as productcountryid',
                'products.sku as productsku',
                'products.id as productid',
                'products.*',
                'producttranslates.name as ProductName',
                'productimages.url as product_default_image',
                'plantrialproducts.ProductId as PlanTrialProductId',
                DB::raw('(SELECT group_concat(productcountries.sellPrice order by productcountries.id) FROM productcountries WHERE plan_details.ProductCountryId = productcountries.id  GROUP BY plan_details.PlanId) as sellPrice'),
                DB::raw('(SELECT group_concat(productcountries.basedPrice order by productcountries.id) FROM productcountries WHERE plan_details.ProductCountryId = productcountries.id  GROUP BY plan_details.PlanId) as basedPrice')
            )
            ->where('plans.id', $PlanId) // country
            ->where('plans.CountryId', $CountryId) // country
            ->where('productimages.isDefault', 1) // default product images
            ->where('producttranslates.langCode', $LangCode) // translate lang code
            ->orderByRaw("planduration asc, planid asc, productcountryid asc, productid asc")
            ->get();

        $_build = [
            "plantrialproductid" => "",
            "plan_gender_type" => "",
            "plantype" => "",
            "isAnnual" => 0,
            "planduration" => "",
            "plansku" => "",
            "planid" => "",
            "trial_price" => "",
            "sales_price" => "",
            "discount_percent" => "",
            "discount_amount" => "",
            "base_price" => "",
            "PlanSkuIsAnnual" => "",
            "product_info" => [],
        ];

        $gender = "male";

        foreach ($plan as $_p) {
            if (empty($_build['plantrialproductid'])) {
                $_build["plantrialproductid"] = isset($_p->PlanTrialProductId) ? $_p->PlanTrialProductId : "";
            }
            if (empty($_build['plantype'])) {
                $_build["plantype"] = $_p->planType;
            }
            if (empty($_build['isAnnual'])) {
                $_build["isAnnual"] = $_p->PlanSkuIsAnnual;
            }
            if (empty($_build['PlanSkuIsAnnual'])) {
                $_build["PlanSkuIsAnnual"] = $_p->PlanSkuIsAnnual;
            }
            if (empty($_build['planduration'])) {
                $_build["planduration"] = $_p->planduration;
            }
            if (empty($_build['plansku'])) {
                if (strtoupper(substr($_p->plansku, -2)) === '-W') {
                    if (empty($_build['plan_gender_type'])) {
                        $_build["plan_gender_type"] = "female";
                    }
                } else {
                    if (empty($_build['plan_gender_type'])) {
                        $_build["plan_gender_type"] = $gender;
                    }
                }
                $_build["plansku"] = $_p->plansku;
            }

            if (empty($_build['planid'])) {
                $_build["planid"] = $_p->planid;
            }
            if (empty($_build['trial_price'])) {
                $_build["trial_price"] = $_p->trial_price;
            }
            if (empty($_build['base_price'])) {
                $_build["base_price"] = $_p->base_price;
            }
            if (empty($_build['sales_price'])) {
                $_build["sales_price"] = $_p->sales_price;
            }
            if (empty($_build['discount_percent'])) {
                $_build["discount_percent"] = $_p->discountPercent;
            }
            if (empty($_build['discount_amount'])) {
                $_build["discount_amount"] = $_p->discountAmount;
            }

            array_push($_build["product_info"], array(
                "productcountryid" => $_p->productcountryid,
                "productname" => $_p->ProductName,
                "product_default_image" => $_p->product_default_image,
                "productid" => $_p->productid,
                "sku" => $_p->productsku,
                "cycles" => $_p->productcountrycycle,
                "basedPrice" => $_p->basedPrice,
                "sellPrice" => $_p->sellPrice,
                "product_qty" => $_p->ProductCountryQty,
                "sku" => $_p->sku,
                "long" => $_p->long,
                "width" => $_p->width,
                "hight" => $_p->hight,
                "weight" => $_p->weight,
            ));
        }

        return $_build;
    }

    public function getPlanDetailsByPlanIdV2($PlanId, $CountryId)
    {
        $plan = Plans::
            // ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plans.PlanSkuId')
            // ->join('plan_categories', 'plan_categories.id', 'plan_category_details.PlanCategoryId')
            join('plansku', 'plansku.id', 'plans.PlanSkuId')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->leftJoin('plantrialproducts', 'plantrialproducts.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('productimages', 'productimages.ProductId', 'productcountries.ProductId')
            ->select(
                'plansku.isAnnual as PlanSkuIsAnnual',
                'plansku.duration as planduration',
                'plansku.sku as plansku',
                'plans.id as planid',
                'plans.taxAmount as tax_amount',
                'plans.discountAmount as discount_amount',
                'plans.trialPrice as trial_price',
                'plans.sellPrice as sales_price',
                'plans.price as base_price',
                'plans.discountPercent as discountPercent',
                'plans.discountAmount as discountAmount',
                'plans.planType as planType',
                'plan_details.cycle as productcountrycycle',
                'plan_details.isAnnual',
                'plan_details.qty as ProductCountryQty',
                'productcountries.id as productcountryid',
                'products.sku as productsku',
                'products.id as productid',
                'products.*',
                'productimages.url as product_default_image',
                'plantrialproducts.ProductId as PlanTrialProductId',
                DB::raw('(SELECT group_concat(productcountries.sellPrice order by productcountries.id) FROM productcountries WHERE plan_details.ProductCountryId = productcountries.id  GROUP BY plan_details.PlanId) as sellPrice'),
                DB::raw('(SELECT group_concat(productcountries.basedPrice order by productcountries.id) FROM productcountries WHERE plan_details.ProductCountryId = productcountries.id  GROUP BY plan_details.PlanId) as basedPrice')
            )
            ->where('plans.id', $PlanId)
            ->where('plans.CountryId', $CountryId) // country
            ->where('productimages.isDefault', 1) // default product images
            ->orderByRaw("planduration asc, planid asc, productcountryid asc, productid asc")
            ->get();
        $_build = [
            "plantrialproductid" => "",
            "plan_gender_type" => "",
            "plantype" => "",
            "isAnnual" => 0,
            "planduration" => "",
            "plansku" => "",
            "planid" => "",
            "trial_price" => "",
            "sales_price" => "",
            "discount_percent" => "",
            "discount_amount" => "",
            "base_price" => "",
            "product_info" => [],
        ];

        $gender = "male";

        foreach ($plan as $_p) {
            if (empty($_build['plantrialproductid'])) {
                $_build["plantrialproductid"] = isset($_p->PlanTrialProductId) ? $_p->PlanTrialProductId : "";
            }
            if (empty($_build['plantype'])) {
                $_build["plantype"] = $_p->planType;
            }
            if (empty($_build['isAnnual'])) {
                $_build["isAnnual"] = $_p->PlanSkuIsAnnual;
            }
            if (empty($_build['planduration'])) {
                $_build["planduration"] = $_p->planduration;
            }
            if (empty($_build['plansku'])) {
                if (strtoupper(substr($_p->plansku, -2)) === '-W') {
                    if (empty($_build['plan_gender_type'])) {
                        $_build["plan_gender_type"] = "female";
                    } else {
                        $_build["plan_gender_type"] = $gender;
                    }
                }
                $_build["plansku"] = $_p->plansku;
            }

            if (empty($_build['planid'])) {
                $_build["planid"] = $_p->planid;
            }
            if (empty($_build['trial_price'])) {
                $_build["trial_price"] = $_p->trial_price;
            }
            if (empty($_build['base_price'])) {
                $_build["base_price"] = $_p->base_price;
            }
            if (empty($_build['sales_price'])) {
                $_build["sales_price"] = $_p->sales_price;
            }
            if (empty($_build['discount_percent'])) {
                $_build["discount_percent"] = $_p->discountPercent;
            }
            if (empty($_build['discount_amount'])) {
                $_build["discount_amount"] = $_p->discountAmount;
            }

            array_push($_build["product_info"], array(
                "productcountryid" => $_p->productcountryid,
                "productname" => $_p->ProductName,
                "product_default_image" => $_p->product_default_image,
                "productid" => $_p->productid,
                "sku" => $_p->productsku,
                "cycles" => $_p->productcountrycycle,
                "basedPrice" => $_p->basedPrice,
                "sellPrice" => $_p->sellPrice,
                "product_qty" => $_p->ProductCountryQty,
                "sku" => $_p->sku,
                "long" => $_p->long,
                "width" => $_p->width,
                "hight" => $_p->hight,
                "weight" => $_p->weight,
            ));
        }

        return $_build;
    }

    public function getAvailablePlanProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode)
    {
        $availableProductCountryIds = [];
        $cycleNumber = strval($cycleNumber);
        $subs = Subscriptions::where('id', $subscriptionId)->first();

        // Filter plan products
        $plan_details = $this->getPlanDetailsByPlanId($subs->PlanId, $countryId, $langCode);
        foreach ($plan_details["product_info"] as $detail) {
            if (!in_array($detail["sku"], config('global.all.handle_types.skusV2'))) {
                if ($detail["cycles"] == "all" || $detail["cycles"] == $cycleNumber) {
                    array_push($availableProductCountryIds, $detail["productcountryid"]);
                } else {
                    if (is_array(json_decode($detail["cycles"]))) {
                        $cycleList = json_decode($detail["cycles"]);
                        foreach ($cycleList as $cycle) {
                            $c = $cycle->c;
                            if ($c == strval($cycleNumber)) {
                                array_push($availableProductCountryIds, $detail["productcountryid"]);
                            }
                        }
                    }
                }
            }
        }
        return $availableProductCountryIds;
    }

    public function getAvailablePlanProductsByCycleV2($subscriptionId, $cycleNumber, $countryId, $langCode)
    {
        $availableProductCountries = [];
        $cycleNumber = strval($cycleNumber);
        $subs = Subscriptions::where('id', $subscriptionId)->first();

        // Filter plan products
        $plan_details = $this->getPlanDetailsByPlanId($subs->PlanId, $countryId, $langCode);
        foreach ($plan_details["product_info"] as $detail) {
            if (!in_array($detail["sku"], config('global.all.handle_types.skusV2'))) {
                if ($detail["cycles"] == "all" || $detail["cycles"] == $cycleNumber) {
                    $availableProductCountries[$detail["productcountryid"]] = $detail["product_qty"];
                } else {
                    if (is_array(json_decode($detail["cycles"]))) {
                        $cycleList = json_decode($detail["cycles"]);
                        foreach ($cycleList as $cycle) {
                            $c = $cycle->c;
                            if ($c == strval($cycleNumber)) {
                                $availableProductCountries[$detail["productcountryid"]] = $detail["product_qty"];
                            }
                        }
                    }
                }
            }
        }
        return $availableProductCountries;
    }

    public function getAvailableAddonProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode)
    {

        $availableProductCountryIds = [];
        $cycleNumber = strval($cycleNumber);
        $subs = Subscriptions::where('id', $subscriptionId)->first();

        // Filter addon products
        $addons_details = SubscriptionsProductAddOns::where('subscriptionId', $subs->id)->get();
        foreach ($addons_details as $addons) {
            if ($addons->cycle == "all" || $addons->cycle == $cycleNumber) {
                array_push($availableProductCountryIds, $addons->ProductCountryId);
            } else {
                if (is_array(json_decode($addons->cycle))) {
                    $cycleList = json_decode($addons->cycle);
                    foreach ($cycleList as $cycle) {
                        $c = $cycle->c;
                        if ($c == strval($cycleNumber)) {
                            array_push($availableProductCountryIds, $addons->ProductCountryId);
                        }
                    }
                }
            }
        }
        return $availableProductCountryIds;
    }

    public function getAvailableAddonProductsByCycleV2($subscriptionId, $cycleNumber, $countryId, $langCode)
    {

        $availableProductCountries = [];
        $cycleNumber = strval($cycleNumber);
        $subs = Subscriptions::where('id', $subscriptionId)->first();

        // Filter addon products
        $addons_details = SubscriptionsProductAddOns::where('subscriptionId', $subs->id)->get();
        foreach ($addons_details as $addons) {
            if ($addons->cycle == "all" || $addons->cycle == $cycleNumber) {
                $availableProductCountries[$addons->ProductCountryId] = $addons->qty;
            } else {
                if (is_array(json_decode($addons->cycle))) {
                    $cycleList = json_decode($addons->cycle);
                    foreach ($cycleList as $cycle) {
                        $c = $cycle->c;
                        if ($c == strval($cycleNumber)) {
                            $availableProductCountries[$addons->ProductCountryId] = $addons->qty;
                        }
                    }
                }
            }
        }
        return $availableProductCountries;
    }

    public function getAllAvailableProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode)
    {
        $availablePlanProductCountryIds = $this->getAvailablePlanProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode);
        $availableAddonProductCountryIds = $this->getAvailableAddonProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode);
        return array_merge($availablePlanProductCountryIds, $availableAddonProductCountryIds);
    }

    public function getAllAvailableProductsByCycleV2($subscriptionId, $cycleNumber, $countryId, $langCode)
    {
        $availablePlanProductCountryIds = $this->getAvailablePlanProductsByCycleV2($subscriptionId, $cycleNumber, $countryId, $langCode);
        $availableAddonProductCountryIds = $this->getAvailableAddonProductsByCycleV2($subscriptionId, $cycleNumber, $countryId, $langCode);
        return array_replace($availablePlanProductCountryIds, $availableAddonProductCountryIds);
    }

    public function getAllAvailableAddOnProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode)
    {
        $availableAddonProductCountryIds = $this->getAvailableAddonProductsByCycle($subscriptionId, $cycleNumber, $countryId, $langCode);
        return $availableAddonProductCountryIds;
    }

    public function getPlanDuration($data)
    {

        $planid = $data->id;
        $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->select('plansku.duration', 'plans.*', 'plansku.*')
            ->where('plans.id', $planid) // country (may do in here)
            ->first();

        return $planlist;
    }

    // public function getSubscriptionPlanLangCodeCountry($plan_id)
    // {
    //     return Plans::join('countries','countries.id', 'plans.CountryId')->
    // }

    public function getTrialPlansBasedOnGender($options)
    {
        try {
            $blade_types = $handle_types = $bag_types = $addons = null;
            $available_frequencies = [];
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial')];
                    $gp_products_ctgry = config('global.all.product_category_types.men');
                    $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.men.handles');
                    $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.men.blades');
                    $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.men.addons');
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.trial')];
                    $gp_products_ctgry = config('global.all.product_category_types.women');
                    $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.women.handles');
                    $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.women.blades');
                    $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.women.addons');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial'), config('global.all.plan_category_types.women.trial')];
                    $gp_products_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    $gp_handle_type_names = [config('global.all.product_based_on_plan_type_names.men.handles'), config('global.all.product_based_on_plan_type_names.women.handles')];
                    $gp_blade_type_names = [config('global.all.product_based_on_plan_type_names.men.blades'), config('global.all.product_based_on_plan_type_names.wpmen.blades')];
                    $gp_addon_type_names = [config('global.all.product_based_on_plan_type_names.men.addons'), config('global.all.product_based_on_plan_type_names.women.addons')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = PlanCategory::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = PlanCategory::whereIn('name', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                ->whereIn('plan_category_details.PlanCategoryId', $p_ctgry)
                ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                ->get();

            foreach ($p_relateds as $plan) {
                array_push($p_drtns, (int) $plan->duration);
            }

            $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

            $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                ->where('plansku.status', 1)
                ->where('plans.CountryId', $options->CountryId)
                // ->where('plantranslates.langCode', $options->langCode)
                ->whereIn('plansku.duration', $available_frequencies)
                ->select('plans.id')
                // ->select(
                //     'plans.id as PlanId',
                //     'plansku.id as PlanSkuId',
                //     'plan_category_details.id as PlanCategoryId',
                //     // 'plantranslates.id as PlanTranslateId',
                //     'plans.*',
                //     // 'plantranslates.*',
                //     'plansku.*')
                ->get();

            $combinations = \App\Helpers\LaravelHelper::uniqueCombination($all_p_ctgry);

            $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_blade_type_names)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_handle_type_names)
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_handle_type_names)
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', $gp_addon_type_names)
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $available_plans = (object) array(
                "active_plans" => $allplans,
                // "blade_types" => $blade_types,
                // "handle_types" => $handle_types,
                // "bag_types" => null,
                // "addons" => $addons,
            );

            return response()->json($available_plans);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getTrialPlansBasedOnGenderCount($options)
    {
        try {
            $available_frequencies = [];
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial')];
                    $gp_products_ctgry = config('global.all.product_category_types.men');
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.trial')];
                    $gp_products_ctgry = config('global.all.product_category_types.women');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial'), config('global.all.plan_category_types.women.trial')];
                    $gp_products_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = PlanCategory::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = PlanCategory::whereIn('name', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                ->whereIn('plan_category_details.PlanCategoryId', $p_ctgry)
                ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                ->get();

            foreach ($p_relateds as $plan) {
                array_push($p_drtns, (int) $plan->duration);
            }

            $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

            $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                ->where('plansku.status', 1)
                ->where('plans.CountryId', $options->CountryId)
                // ->where('plantranslates.langCode', $options->langCode)
                ->whereIn('plansku.duration', $available_frequencies)
                ->select('plans.id')
                ->count();
            $available_plans = (object) array(
                "active_plans" => $allplans,
            );

            return response()->json($available_plans);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getTrialPlansBasedOnGenderBasicInfo($options)
    {
        try {
            $p_ctgry = [];
            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial')];
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.trial')];
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.trial'), config('global.all.plan_category_types.women.trial')];
                    break;
                default:
                    return null;
            }
            // get related categories
            $g_p_category_infos = PlanCategory::join('plan_category_translates', 'plan_category_translates.PlancategoryId', 'plan_categories.id')
                ->whereIn('plan_categories.name', $gp_ctgry)
                ->where('plan_category_translates.langCode', $options->langCode)
                ->select('plan_categories.id as PlanCategoryId', 'plan_category_translates.id as PlanCategoryTranslateId', 'plan_categories.*', 'plan_category_translates.*')
                ->first();

            return response()->json($g_p_category_infos);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getCustomPlansBasedOnGender($options)
    {
        try {
            $blade_types = $handle_types = $bag_types = $addons = null;
            $available_frequencies = [];
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom')];
                    $gp_products_ctgry = config('global.all.product_category_types.men');
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.custom')];
                    $gp_products_ctgry = config('global.all.product_category_types.women');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom'), config('global.all.plan_category_types.women.custom')];
                    $gp_products_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = PlanCategory::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = PlanCategory::whereIn('name', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                ->whereIn('plan_category_details.PlanCategoryId', $p_ctgry)
                ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                ->get();

            foreach ($p_relateds as $plan) {
                array_push($p_drtns, (int) $plan->duration);
            }

            $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

            $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                ->where('plansku.status', 1)
                ->where('plans.CountryId', $options->CountryId)
                // ->where('plantranslates.langCode', $options->langCode)
                ->whereIn('plansku.duration', $available_frequencies)
                ->select('plans.id')
                // ->select(
                //     'plans.id as PlanId',
                //     'plansku.id as PlanSkuId',
                //     'plan_category_details.id as PlanCategoryId',
                //     // 'plantranslates.id as PlanTranslateId',
                //     'plans.*',
                //     // 'plantranslates.*',
                //     'plansku.*')
                ->get();

            $combinations = \App\Helpers\LaravelHelper::uniqueCombination($all_p_ctgry);

            $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', 'Custom Plan Blade')
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', 'Custom Plan Handle')
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $bag_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', 'Custom Plan Addons Bag')
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                ->join('products', 'products.id', 'producttypedetails.ProductId')
                ->join('productcountries', 'productcountries.ProductId', 'products.id')
                ->join('productimages', 'productimages.ProductId', 'products.id')
                ->whereIn('producttypedetails.ProductTypeId', $gp_products_ctgry)
                ->where('productimages.isDefault', 1)
                ->where('producttypes.name', 'Custom Plan Addons')
                ->where('productcountries.CountryId', $options->CountryId)
                ->select(
                    'producttypedetails.id as ProductTypeDetailId',
                    'producttypes.id as ProductTypeId',
                    'products.id as ProductId',
                    'productcountries.id as ProductCountryId',
                    'productimages.url as product_default_image',
                    'producttypedetails.*',
                    'producttypes.*',
                    'products.*',
                    'productcountries.*'
                )
                ->get();

            $available_plans = (object) array(
                "active_plans" => $allplans,
                // "blade_types" => $blade_types,
                // "handle_types" => $handle_types,
                // "bag_types" => $bag_types,
                // "addons" => $addons,
            );

            return response()->json($available_plans);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getCustomPlansBasedOnGenderCount($options)
    {
        try {
            $available_frequencies = [];
            $all_p_ctgry = [];
            $p_ctgry = [];
            $p_drtns = [];

            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom')];
                    $gp_products_ctgry = config('global.all.product_category_types.men');
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.custom')];
                    $gp_products_ctgry = config('global.all.product_category_types.women');
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom'), config('global.all.plan_category_types.women.custom')];
                    $gp_products_ctgry = [config('global.all.product_category_types.men'), config('global.all.product_category_types.women')];
                    break;
                default:
                    return null;
            }

            // get all categories
            $all_categories = PlanCategory::get('id');
            foreach ($all_categories as $category) {
                array_push($all_p_ctgry, $category->id);
            }

            // get related categories
            $g_p_categories = PlanCategory::whereIn('name', $gp_ctgry)->get('id');
            foreach ($g_p_categories as $g_p_category) {
                array_push($p_ctgry, $g_p_category->id);
            }

            // get plansku related to selected plan categories
            $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                ->whereIn('plan_category_details.PlanCategoryId', $p_ctgry)
                ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                ->get();

            foreach ($p_relateds as $plan) {
                array_push($p_drtns, (int) $plan->duration);
            }

            $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

            $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                ->where('plansku.status', 1)
                ->where('plans.CountryId', $options->CountryId)
                // ->where('plantranslates.langCode', $options->langCode)
                ->whereIn('plansku.duration', $available_frequencies)
                ->select('plans.id')
                ->count();

            $available_plans = (object) array(
                "active_plans" => $allplans,
            );

            return response()->json($available_plans);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function getCustomPlansBasedOnGenderBasicInfo($options)
    {
        try {
            $p_ctgry = [];
            switch ($options->gender) {
                case 'male':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom')];
                    break;
                case 'female':
                    $gp_ctgry = [config('global.all.plan_category_types.women.custom')];
                    break;
                case 'neutral':
                    $gp_ctgry = [config('global.all.plan_category_types.men.custom'), config('global.all.plan_category_types.women.custom')];
                    break;
                default:
                    return null;
            }

            // get related categories
            $g_p_category_infos = PlanCategory::join('plan_category_translates', 'plan_category_translates.PlancategoryId', 'plan_categories.id')
                ->whereIn('plan_categories.name', $gp_ctgry)
                ->where('plan_category_translates.langCode', $options->langCode)
                ->select('plan_categories.id as PlanCategoryId', 'plan_category_translates.id as PlanCategoryTranslateId', 'plan_categories.*', 'plan_category_translates.*')
                ->first();

            return response()->json($g_p_category_infos);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getPlanBySKU($sku, $countryid)
    {
        try {
            $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
                ->select('plans.id as planid')
                ->where('plansku.sku', $sku) // sku name
                ->where('plans.CountryId', $countryid) // countryid
                ->first();

            return $planlist;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getPlanProductBySKU($sku, $countryid)
    {
        try {
            $planlist = PlanSKU::join('plans', 'plans.PlanSkuId', 'plansku.id')
                ->join('plan_details', 'plan_details.PlanId', 'plans.id')
                ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->select('products.sku as productssku', 'plan_details.qty as qty')
                ->where('plansku.sku', $sku) // sku name
                ->where('plans.CountryId', $countryid) // countryid
                ->where('productcountries.CountryId', $countryid) // countryid
                ->distinct()
                ->get();

            return $planlist;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getConvertedAnnual($category,$plan_type,$bladecountryid,$selected_frequency)
    {
    
        $planlist = PlanCategory::join('plan_category_details', 'plan_category_details.PlanCategoryId', 'plan_categories.id')
            ->join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
            ->join('plans', 'plans.PlanSkuId', 'plansku.id')
            ->join('plan_details', 'plan_details.PlanId', 'plans.id')
            ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
            ->select('plansku.duration as planduration', 'plansku.sku as plansku', 'plans.id as planid', 'plans.trialPrice', 'plans.price as price', 'plans.sellPrice as sellPrice', 'plans.discountPercent as discountPercent', 'plans.discountAmount as discountAmount', 'plans.isOnline as isOnline', 'plans.isOffline as isOffline', 'productcountries.id as productcid')
            ->where('plan_categories.name', $category) // group name
            ->where('plans.plantype', $plan_type) // type
            ->where('plan_details.ProductCountryId', $bladecountryid) // country
            ->where('plansku.duration', $selected_frequency)
            ->where('plansku.isAnnual', 1) // annual plans
            ->where('plansku.status', 1) 
            ->where('plans.isOnline', 1)
            ->where('plans.discountPercent', "!=" , 0)
            ->orderByRaw("planduration asc, planid asc, productcid asc, productId asc")
            ->first();
            if(isset($planlist)){
                $planlist->annualqty = 12/(int)$selected_frequency;
            }

            return $planlist;
    }
}
