<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateTimeHelper
{
    public static function getFutureDates($method, $period, $isNow = false)
    {
        if ($isNow == false) {
            $now = Carbon::now();
            // number
            $time = (int) explode(' ', $period)[0];
            // seconds/minutes/days/months/years
            $type = explode(' ', $period)[1];

            if ($method === 'add') {
                switch ($type) {
                    case 'seconds':
                        return $now->addSeconds($time);
                        break;
                    case 'minutes':
                        return $now->addMinutes($time);
                        break;
                    case 'days':
                        return $now->addDays($time);
                        break;
                    case 'months':
                        return $now->addMonths($time);
                        break;
                    case 'years':
                        return $now->addYears($time);
                        break;
                    default:
                        return null;
                }
            } elseif ($method === 'subtract') {
                switch ($type) {
                    case 'seconds':
                        return $now->subSeconds($time);
                        break;
                    case 'minutes':
                        return $now->subMinutes($time);
                        break;
                    case 'days':
                        return $now->subDays($time);
                        break;
                    case 'months':
                        return $now->subMonths($time);
                        break;
                    case 'years':
                        return $now->subYears($time);
                        break;
                    default:
                        return null;
                }
            } else {
                return null;
            }
        } else {
            $now = Carbon::now();
            // number
            $time = (int) explode(' ', $period)[0];
            // seconds/minutes/days/months/years
            $type = explode(' ', $period)[1];

            if ($method === 'add') {
                switch ($type) {
                    case 'seconds':
                        return $now->addSeconds($time);
                        break;
                    case 'minutes':
                        return $now->addMinutes($time);
                        break;
                    case 'days':
                        return $now->addDays($time);
                        break;
                    case 'months':
                        return $now->addMonths($time);
                        break;
                    case 'years':
                        return $now->addYears($time);
                        break;
                    default:
                        return null;
                }
            } elseif ($method === 'subtract') {
                switch ($type) {
                    case 'seconds':
                        return $now->subSeconds($time);
                        break;
                    case 'minutes':
                        return $now->subMinutes($time);
                        break;
                    case 'days':
                        return $now->subDays($time);
                        break;
                    case 'months':
                        return $now->subMonths($time);
                        break;
                    case 'years':
                        return $now->subYears($time);
                        break;
                    default:
                        return null;
                }
            } else {
                return null;
            }
        }
    }

    /*** Additional helper functions ***/
    public static function GetCurrentDateTime()
    {
        return Carbon::now();
    }

    public static function GetCurrentDate()
    {
        return Carbon::now()->toDateString();
    }
}
