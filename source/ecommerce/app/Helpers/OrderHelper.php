<?php

namespace App\Helpers;

use App\Helpers\LaravelHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\Orders;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use App\Queue\BulkOrderInboundQueueService;
use App\Queue\OrderInboundQueueService;

class OrderHelper
{
    public function __construct()
    {
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->productHelper = new ProductHelper();
        $this->laravelHelper = new LaravelHelper();
        $this->order_inbound_queue_service = new OrderInboundQueueService();
        $this->bulkorder_inbound_queue_service = new BulkOrderInboundQueueService();
        $this->error_message = '';
        $this->Log = \Log::channel('cronjob');
    }

    public function addTaskToOrderQueueService($options)
    {
        if ($options->CountryCode === 'my') {
            $this->order_inbound_queue_service->addHashMY($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'hk') {
            $this->order_inbound_queue_service->addHashHK($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'sg') {
            $this->order_inbound_queue_service->addHashSG($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'kr') {
            $this->order_inbound_queue_service->addHashKR($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'tw') {
            $this->order_inbound_queue_service->addHashTW($options->OrderId, $options->CountryId);
        }
    }

    public function addTaskToBulkOrderQueueService($options)
    {
        if ($options->CountryCode === 'my') {
            $this->bulkorder_inbound_queue_service->addHashMY($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'hk') {
            $this->bulkorder_inbound_queue_service->addHashHK($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'sg') {
            $this->bulkorder_inbound_queue_service->addHashSG($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'kr') {
            $this->bulkorder_inbound_queue_service->addHashKR($options->OrderId, $options->CountryId);
        } else if ($options->CountryCode === 'tw') {
            $this->bulkorder_inbound_queue_service->addHashTW($options->OrderId, $options->CountryId);
        }
    }

    public function addTaskToOrderQueueServiceJob($options)
    {
        if ($options->CountryId === 1) {
            $this->order_inbound_queue_service->addHashMY($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 2) {
            $this->order_inbound_queue_service->addHashHK($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 7) {
            $this->order_inbound_queue_service->addHashSG($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 8) {
            $this->order_inbound_queue_service->addHashKR($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 9) {
            $this->order_inbound_queue_service->addHashTW($options->OrderId, $options->CountryId);
        }
    }

    public function addTaskToBulkOrderQueueServiceJob($options)
    {
        if ($options->CountryId === 1) {
            $this->bulkorder_inbound_queue_service->addHashMY($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 2) {
            $this->bulkorder_inbound_queue_service->addHashHK($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 7) {
            $this->bulkorder_inbound_queue_service->addHashSG($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 8) {
            $this->bulkorder_inbound_queue_service->addHashKR($options->OrderId, $options->CountryId);
        } else if ($options->CountryId === 9) {
            $this->bulkorder_inbound_queue_service->addHashTW($options->OrderId, $options->CountryId);
        }
    }

    public function buildWarehouseOrderDetails($options, $isPlan)
    {
        $Log = \Log::channel('cronjob');
        $Log->info('buildWarehouseOrderDetails start');
        $in_cycle_with_all = false;
        $in_cycle = false;
        $_qty = "";
        $_warehouse_data = [
            "user" => "",
            "shipment_info" => [
                "delivery" => [],
                "billing" => [],
            ],
            "metadata" => [
                "SSN" => "",
            ],
            "generate_sales_order_data" => [
                "type" => "",
                "product_country_ids" => [],
                "items" => [],
            ],
            "plan" => [],
            "products" => [],
            "order" => [],
            "orderdetails" => [],
            "receipt" => [],
        ];
        $options = json_decode(json_encode($options, JSON_FORCE_OBJECT));
        $_order = Orders::where('id', $options->OrderId)->first();
        $_order_details = OrderDetails::leftJoin('productcountries', 'productcountries.id', 'orderdetails.ProductCountryId')
            ->leftJoin('products', 'products.id', 'productcountries.ProductId')
            ->leftJoin('plans', 'plans.id', 'orderdetails.PlanId')
            ->where('OrderId', $options->OrderId)
            ->select(
                'products.id as _ProductId',
                'products.sku as _ProductSKU',
                'productcountries.id as _ProductCountryId',
                'productcountries.basedPrice as basedPrice',
                'productcountries.sellPrice as sellPrice',
                'orderdetails.*',
                'products.*'
            )
            ->get();
        // ->toArray();
        // dd($_order_details);
        $_receipt = Receipts::where('OrderId', $options->OrderId)->first();
        if (!empty($_order)) {
            $_user = User::where('id', $_order->UserId)->first();
            $_shipment = DeliveryAddresses::where('id', $_order->DeliveryAddressId)->first();
            $_billing = DeliveryAddresses::where('id', $_order->BillingAddressId)->first();
            $_ssn = $_order->SSN ? $_order->SSN : ($_shipment->SSN ? $_shipment->SSN : null);

            // push order, order_details & receipt data to array
            $_warehouse_data["order"] = $_order;
            $_warehouse_data["orderdetails"] = $_order_details;

            $_warehouse_data["receipt"] = $_receipt;
            $_warehouse_data["user"] = $_user;
            $_warehouse_data["shipment_info"]["delivery"] = $_shipment;
            $_warehouse_data["shipment_info"]["billing"] = $_billing;
            $_warehouse_data["metadata"]["SSN"] = $_ssn;
            if ($isPlan) {
                $_subs = Subscriptions::where('id', $_order->subscriptionIds)->first();
                if (!empty($_subs)) {
                    // initiate currentCycle
                    $currentCycle = $_subs->currentCycle;
                    // get addon products for current subscription
                    $_subs_productaddon = SubscriptionsProductAddOns::where('subscriptionId', $_subs->id)->where('isWithTrialKit', false)->get();
                    // get plan details
                    $_plan = $this->laravelHelper->ConvertArrayToObject($this->planHelper->getPlanDetailsByPlanIdV2($_subs->PlanId, $options->CountryId));

                    $plantrialproductid = "";
                    $plantrialproductinfo = "";
                    // get trial product info
                    if (isset($_plan) && isset($_plan->plantrialproductid)) {
                        $plantrialproductid = (int) $_plan->plantrialproductid;
                        $plantrialproductinfo = Product::where('id', $plantrialproductid)->first();
                    }

                    $_warehouse_data["plan"] = $_plan;
                    if ($_subs->isTrial && $currentCycle === 1 && !$_subs->isCustom) { // if trial plan & before first billing
                        // set type
                        $_warehouse_data["generate_sales_order_data"]["type"] = "trial";
                        foreach ($_plan->product_info as $tk_items) {
                            foreach (config('global.all.blade_types.skus') as $active_blade) {
                                if ($tk_items->sku === $active_blade) {
                                    $_plan->plansku = $plantrialproductinfo->sku;
                                }
                            }
                        }

                        // push plan sku info into item_group
                        array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                            "sku" => $_plan->plansku,
                            "trialPrice" => $_plan->trial_price,
                            "sellPrice" => $_plan->sales_price,
                            "qty" => $_order_details[0]["qty"],
                        ));
                    } else if (($_subs->isTrial && $currentCycle > 1 && !$_subs->isCustom) || ($_subs->isCustom && !$_subs->isTrial)) { // if trial plan / custom plan & recurring billing
                        // set type
                        $_warehouse_data["generate_sales_order_data"]["type"] = "subscription";

                        // push individual product info into items
                        foreach ($_plan->product_info as $___pc) {
                            if (!in_array($___pc->sku, config('global.all.handle_types.skusV2'))) {
                                if ($___pc->cycles === "all") {
                                    $plan_cycles = $___pc->cycles;
                                } else {
                                    $plan_cycles = (int) json_decode($___pc->cycles, true);
                                    //check if all exists, if exists save to a new variable
                                }

                                // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                                if (is_array($plan_cycles)) {
                                    foreach ($plan_cycles as $_p_cycles) {
                                        if ($_p_cycles["c"] === $currentCycle) {
                                            $in_cycle = true;
                                            $cycle_no = $_p_cycles["c"];
                                            $_qty = $_p_cycles["q"];
                                        }

                                        if ($_p_cycles["c"] === "all") {
                                            $in_cycle_with_all = true;
                                            $cycle_no = 1;
                                            $_qty = $_p_cycles["q"];
                                        }
                                    }
                                }

                                // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                                if (!is_array($plan_cycles)) {
                                    //if cycle is in numbers / string
                                    $_in_cycle = $plan_cycles;
                                    if ($___pc->cycles === "all") {
                                        $in_cycle = true;
                                        $cycle_no = "recurring";
                                        $_qty = $___pc->product_qty;
                                    } else {
                                        if ($_in_cycle === $currentCycle) {
                                            $in_cycle = true;
                                            $cycle_no = $_in_cycle;
                                            $_qty = $___pc->product_qty;
                                        }
                                    }
                                }

                                // if products are within the currentCycle, add into generate_sales_order_data array
                                if ($in_cycle == true) {
                                    $in_cycle == false;
                                    array_push($_warehouse_data["generate_sales_order_data"]["product_country_ids"], $___pc->productcountryid);
                                    array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                                        "productcountryid" => $___pc->productcountryid,
                                        "sku" => $___pc->sku,
                                        "long" => $___pc->long,
                                        "width" => $___pc->width,
                                        "hight" => $___pc->hight,
                                        "weight" => $___pc->weight,
                                        "basedPrice" => $___pc->basedPrice,
                                        "sellPrice" => $___pc->sellPrice,
                                        "qty" => $_qty ? $_qty : null,
                                    ));
                                }

                                if ($in_cycle_with_all == true) {
                                    $in_cycle_with_all == false;
                                    array_push($_warehouse_data["generate_sales_order_data"]["product_country_ids"], $___pc->productcountryid);
                                    array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                                        "productcountryid" => $___pc->productcountryid,
                                        "sku" => $___pc->sku,
                                        "long" => $___pc->long,
                                        "width" => $___pc->width,
                                        "hight" => $___pc->hight,
                                        "weight" => $___pc->weight,
                                        "basedPrice" => $___pc->basedPrice,
                                        "sellPrice" => $___pc->sellPrice,
                                        "qty" => $_qty ? $_qty : null,
                                    ));
                                }
                            }
                            // check for subscription_productaddons
                            if ($_subs_productaddon) {
                                $_addons_productcountryIds = array(
                                    "appType" => $options->isBA === true ? 'baWebsite' : 'ecommerce', // appType = [ecommerce,baWebsite]
                                    "isOnline" => $options->isBA === false ? 1 : 0,
                                    "isOffline" => $options->isBA === true ? 1 : 0,
                                    "product_country_ids" => [],
                                );
                                
                                foreach ($_subs_productaddon as $__p_addon) {
                                    array_push($_addons_productcountryIds["product_country_ids"], $__p_addon->ProductCountryId);
                                }
                                $__addon_item_info = $this->laravelHelper->ConvertArrayToObject($this->productHelper->getProductByProductCountriesV2($options->CountryId, $_addons_productcountryIds));
                                foreach ($_subs_productaddon as $__p_addon) {
                                    foreach ($__addon_item_info as $__addon_pcs) {
                                        if ($__addon_pcs->ProductCountryId === $__p_addon->ProductCountryId) {
                                            if ($__p_addon->cycle === "all") {
                                                $plan_cycles = $__p_addon->cycle;
                                            } else {
                                                $plan_cycles = (int) json_decode($__p_addon->cycle, true);
                                            }

                                            // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                                            if (is_array($plan_cycles)) {
                                                foreach ($plan_cycles as $_p_cycles) {
                                                    if ($_p_cycles["c"] === $currentCycle) {
                                                        $in_cycle = true;
                                                        $cycle_no = $_p_cycles["c"];
                                                    }
                                                }
                                            }

                                            // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                                            if (!is_array($plan_cycles)) {
                                                //if cycle is in numbers / string
                                                $_in_cycle = $plan_cycles;
                                                if ($__p_addon->cycle === "all") {
                                                    $in_cycle = true;
                                                    $cycle_no = "recurring";
                                                    $_qty = $__p_addon->qty;
                                                } else {
                                                    if ($_in_cycle === $currentCycle) {
                                                        $in_cycle = true;
                                                        $cycle_no = $_in_cycle;
                                                        $_qty = $__p_addon->qty;
                                                    }
                                                }
                                            }

                                            if ($in_cycle == true) {
                                                $in_cycle == false;
                                                if (!in_array($__addon_pcs->ProductCountryId, $_warehouse_data["generate_sales_order_data"]["product_country_ids"])) {
                                                    array_push($_warehouse_data["generate_sales_order_data"]["product_country_ids"], $__addon_pcs->ProductCountryId);
                                                    array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                                                        "productcountryid" => $__addon_pcs->ProductCountryId,
                                                        "sku" => $__addon_pcs->sku,
                                                        "long" => $__addon_pcs->long,
                                                        "width" => $__addon_pcs->width,
                                                        "hight" => $__addon_pcs->hight,
                                                        "weight" => $__addon_pcs->weight,
                                                        "basedPrice" => $__addon_pcs->basedPrice,
                                                        "sellPrice" => $__addon_pcs->sellPrice,
                                                        "qty" => $_qty,
                                                    ));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // add items not found under plan or subscription_productaddon
                if ($_order_details) {
                    foreach ($_order_details as $od) {
                        if ($od["ProductCountryId"] !== null) {
                            if (!in_array($od["ProductCountryId"], $_warehouse_data["generate_sales_order_data"]["product_country_ids"])) {
                                array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                                    "productcountryid" => $od["ProductCountryId"],
                                    "sku" => $od["sku"],
                                    "long" => $od["long"],
                                    "width" => $od["width"],
                                    "hight" => $od["hight"],
                                    "weight" => $od["weight"],
                                    "basedPrice" => $od["basedPrice"],
                                    "sellPrice" => $od["sellPrice"],
                                    "qty" => $od["qty"],
                                ));
                            }
                        }
                    }
                }
            } else {
                // ala carte & bulk order goes here
                if ($_order_details) {
                    foreach ($_order_details as $od) {

                        // get productcountry & product info
                        $_productcountry_info = ProductCountry::where('id', $od->ProductCountryId)->first();
                        if ($_productcountry_info) {
                            $_product_info = Product::where('id', $_productcountry_info->ProductId)->first();
                        }

                        if ($_productcountry_info && $_product_info) {
                            // build data
                            array_push($_warehouse_data["products"], array(
                                "productcountryid" => $_productcountry_info->id,
                                "sku" => $_product_info->sku,
                                "long" => $_product_info->long,
                                "width" => $_product_info->width,
                                "hight" => $_product_info->hight,
                                "weight" => $_product_info->weight,
                                "basedPrice" => $_productcountry_info->basedPrice,
                                "sellPrice" => $_productcountry_info->sellPrice,
                                "qty" => $od->qty,
                            ));

                            array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                                "productcountryid" => $_productcountry_info->id,
                                "sku" => $_product_info->sku,
                                "long" => $_product_info->long,
                                "width" => $_product_info->width,
                                "hight" => $_product_info->hight,
                                "weight" => $_product_info->weight,
                                "basedPrice" => $_productcountry_info->basedPrice,
                                "sellPrice" => $_productcountry_info->sellPrice,
                                "qty" => $od->qty,
                            ));
                        }
                    }
                }
            }
        }
        return $_warehouse_data;
    }

    public function buildWarehouseBulkOrderDetails($options)
    {
        $in_cycle = false;
        $_qty = "";
        $_warehouse_data = [
            "user" => "",
            "shipment_info" => [
                "delivery" => [],
                "billing" => [],
            ],
            "metadata" => [
                "SSN" => "",
            ],
            "generate_sales_order_data" => [
                "type" => "",
                "product_country_ids" => [],
                "items" => [],
            ],
            "plan" => [],
            "products" => [],
            "order" => [],
            "orderdetails" => [],
            "receipt" => [],
        ];

        $options = json_decode(json_encode($options, JSON_FORCE_OBJECT));
        $_bulkorder = BulkOrders::where('id', $options->OrderId)->first();
        $_bulkorder_details = BulkOrderDetails::leftJoin('productcountries', 'productcountries.id', 'bulkorderdetails.ProductCountryId')
            ->leftJoin('products', 'products.id', 'productcountries.ProductId')
            ->leftJoin('plans', 'plans.id', 'bulkorderdetails.PlanId')
            ->where('BulkOrderId', $options->OrderId)
            ->select(
                'products.id as _ProductId',
                'products.sku as _ProductSKU',
                'productcountries.id as _ProductCountryId',
                'productcountries.basedPrice as basedPrice',
                'productcountries.sellPrice as sellPrice',
                'bulkorderdetails.*',
                'products.*'
            )
            ->get()
            ->toArray();
        if (!empty($_bulkorder)) {

            $_shipment = DeliveryAddresses::where('id', $_bulkorder->DeliveryAddressId)->first();
            $_billing = DeliveryAddresses::where('id', $_bulkorder->BillingAddressId)->first();
            $_ssn = $_bulkorder->SSN ? $_bulkorder->SSN : ($_shipment->SSN ? $_shipment->SSN : null);

            // push order, order_details & receipt data to array
            $_warehouse_data["bulkorder"] = $_bulkorder;
            $_warehouse_data["bulkorderdetails"] = $_bulkorder_details;
            $_warehouse_data["shipment_info"]["delivery"] = $_shipment;
            $_warehouse_data["shipment_info"]["billing"] = $_billing;
            $_warehouse_data["metadata"]["SSN"] = $_ssn;
            // ala carte & bulk order goes here
            if ($_bulkorder_details) {

                foreach ($_bulkorder_details as $bod) {

                    // get productcountry & product info
                    $_productcountry_info = ProductCountry::where('id', $bod["_ProductCountryId"])->first();

                    if ($_productcountry_info) {
                        $_product_info = Product::where('id', $_productcountry_info->ProductId)->first();
                    }

                    if ($_productcountry_info && $_product_info) {
                        // build data
                        array_push($_warehouse_data["products"], array(
                            "productcountryid" => $_productcountry_info->id,
                            "sku" => $_product_info->sku,
                            "long" => $_product_info->long,
                            "width" => $_product_info->width,
                            "hight" => $_product_info->hight,
                            "weight" => $_product_info->weight,
                            "basedPrice" => $_productcountry_info->basedPrice,
                            "sellPrice" => $_productcountry_info->sellPrice,
                            "qty" => $bod["qty"],
                        ));

                        array_push($_warehouse_data["generate_sales_order_data"]["items"], array(
                            "productcountryid" => $_productcountry_info->id,
                            "sku" => $_product_info->sku,
                            "long" => $_product_info->long,
                            "width" => $_product_info->width,
                            "hight" => $_product_info->hight,
                            "weight" => $_product_info->weight,
                            "basedPrice" => $_productcountry_info->basedPrice,
                            "sellPrice" => $_productcountry_info->sellPrice,
                            "qty" => $bod["qty"],
                        ));
                    }
                }
            }
        }
        return $_warehouse_data;
    }
}
