<?php

namespace App\Helpers;

class RebatesHelper
{
    public function __construct()
    {

    }

    public static function generateUniqueCode($code_length, $special_chars)
    {
        $code = "";
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code_length = $code_length;
        for ($i = 0; $i < $code_length; $i++) {
            $code .= $characters[mt_rand(0, $code_length)];
        }

        return $special_chars . '-' . $code;
    }

    public function build_referral()
    {

    }

    public function build_referee()
    {

    }

    public function build_referer()
    {

    }
}
