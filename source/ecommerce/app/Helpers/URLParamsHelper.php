<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Input;

class URLParamsHelper
{
    public static function getURLParams()
    {
        $response = (Object) array("data" => (Object) array(), "param_strings" => "");
        if (count(Input::all()) > 0) {
            $url_params = Input::all();

            // if (isset($url_params) && count($url_params) > 0) {
            //     foreach ($url_params as $key => $param) {
            //         if ($key === "_token") {
            //             unset($url_params[$key]);
            //         }
            //     }
            // }
            $_count = 0;
            $_retrieve = session()->get('url_parameters');
            if ($_retrieve) {
                $response = $_retrieve;
            }
            if (isset($_retrieve->param_strings) && $_retrieve->param_strings !== "") {
                $append_params = $_retrieve->param_strings;
            } else {
                $append_params = '';
            }
            // var_dump("count: => ", $_count);

            if (isset($_retrieve->data) && $_retrieve->data !== "") {
                // if old url_params exists
                foreach ($_retrieve->data as $key1 => $value1) {
                    if (isset($url_params) && $url_params !== null || isset($url_params) && $url_params !== "") {
                        // if new url_params exists
                        foreach ($url_params as $key2 => $value2) {
                            $key2array = explode(';', $key2);
                            $key2formatted = end($key2array);
                            // if key exists
                            if ($key1 == $key2formatted) {
                                if ($value1 == $value2) {
                                    // do nothing
                                } else {
                                    // update key value
                                    $response->data->$key1 = $value2;
                                    if (stripos($append_params, $key2formatted . '=' . $value2) === true) {
                                        // do nothing
                                    } else {
                                        // $append_params = $append_params . $key1 . '=' . $value2 . '&';
                                        // echo '<br>';
                                        // var_dump("append_params1 => ", $append_params);
                                        // echo '<br>';
                                        // $response->param_strings = $append_params;
                                    }
                                }
                            } else {
                                $response->data->$key2formatted = $value2;
                                if (stripos($append_params, $key2formatted . '=' . $value2) === true) {
                                    // do nothing
                                } else {
                                    $append_params = $append_params . $key2formatted . '=' . $value2 . '&';
                                    // echo '<br>';
                                    // var_dump("append_params2 => ", $append_params);
                                    // echo '<br>';
                                    $response->param_strings = $append_params;
                                }
                            }
                        }
                        return $response;
                    } else {
                        // do nothing
                        return $response;

                    }
                }
                // dd($_retrieve, $url_params, $key1, $value1, $key2, $value2, $response);

            } else {
                if (!$_retrieve) {
                // if no existing url_params, then take from URL and insert into session
                foreach ($url_params as $key => $param) {
                    if ($_count === 0) {
                        $response->data->$key = $param;
                        if (stripos($append_params, $key . '=' . $param) === true) {
                            // do nothing
                        } else {
                            $append_params = $append_params . $key . '=' . $param . '&';
                        }
                    } else {
                        $response->data->$key = $param;
                        if (stripos($append_params, $key . '=' . $param) === true) {
                            // do nothing
                        } else {
                            $append_params = $append_params . $key . '=' . $param . '&';
                        }
                    }
                    $_count++;
                }
                $response->param_strings = $append_params;
                return $response;
            }
            }

        } else {
            $response = session()->get('url_parameters');
            return $response;
        }

    }

    public static function saveURLParamsToSession()
    {

        $session_params = self::getURLParams();
        session()->put('url_parameters', $session_params);
        $_retrieve = json_encode(session()->get('url_parameters'));
        // dd($_retrieve);
        return $_retrieve;
    }
}
