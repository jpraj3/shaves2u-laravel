<?php

namespace App\Helpers;
use App\Models\Audits\Audits;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AuditsHelper {

/*
use App\Helpers\AuditsHelper;

public $audit;
 $this->audit = new AuditsHelper();  // in construct

//anywhere u insert
       $test = '1';
        $data = [
            "user_type" => "$test",
           "user_id" => "$test",
            "event" => "$test",
           "auditable_type" => "$test",
            "audit_title" => "$test",
            "audit_description" => "$test",
            "old_values" => "$test",
            "new_values" => "$test",
    ];
        $this->audit->create($data);

*/

    public function create($data) {
    
        $auditdata = $data;
        $audits = new Audits();
        $audits->user_type = $auditdata['user_type'];
        $audits->user_id = $auditdata['user_id'];
        $audits->event = $auditdata['event'];
        $audits->auditable_type = $auditdata['auditable_type'];
        $audits->audit_title = $auditdata['audit_title'];
        $audits->audit_description = $auditdata['audit_description'];
        $audits->old_values = $auditdata['old_values'];
        $audits->new_values = $auditdata['new_values'];
        $audits->ip_address = \Request::ip();
        $audits->url = \Request::url();
        $audits->user_agent = \Request::userAgent();
        $audits->save();
    }

}
