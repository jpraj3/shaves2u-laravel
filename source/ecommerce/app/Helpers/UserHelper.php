<?php

namespace App\Helpers;

use App;
use App\Helpers\ProductHelper;
use App\Models\Cards\Cards;

// Helpers
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;

// Services
use App\Models\Rebates\Referral;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Rebates\RewardsIn;
use App\Models\Rebates\RewardsOut;
use App\Models\Plans\Plans;

// Models
use App\Models\User\DeliveryAddresses;
use App\Services\CountryService as CountryService;
use Illuminate\Support\Facades\Request;

class UserHelper
{
    public $type;
    public $countryCode;

    // Build All stripe data here
    public function __construct()
    {
        // $this->productHelper = new ProductHelper();
        // $this->countryService = new CountryService();

        // get default locale
        // $this->locale = App::getLocale();

        // $this->country = json_decode(session()->get('currentCountry'), true);
        // $this->country_id = $this->country['id'];
        // $this->country_iso = strtoupper($this->country['codeIso']);

        // check for supportedlangs
        // $this->_supportedLangs = $this->countryService->supportedLangs($this->country_id);
        // $this->langCode = $this->_supportedLangs ? $this->locale : $this->country['defaultLang'];
    }

    public function getUserDetails($user_id)
    {
        $user_details = [];
        $getDeliveryDetails = $this->getDeliveryDetails($user_id);
        $getBillingDetails = $this->getBillingDetails($user_id);
        $getUserInfo = $this->getUserInfo($user_id);
        $getCountryInfo = $this->getCountryInfo($user_id);
        $getCards = $this->getCards($user_id);
        $getDefaultCard = $this->getDefaultCard($user_id);
        $referral_info = $this->referral_info($user_id);

        $user_details["user"] = $getUserInfo;
        $user_details["country"] = $getCountryInfo;
        $user_details["referral_info"] = $referral_info;
        $user_details["delivery_address"] = $getDeliveryDetails;
        $user_details["billing_address"] = $getBillingDetails;
        $user_details["cards"] = $getCards;
        $user_details["default_card"] = $getDefaultCard;

        return $user_details;
    }

    public function getUserInfo($user_id)
    {
        $getUserInfo = User::leftJoin('languagedetails', 'languagedetails.sublanguageCode', 'users.defaultLanguage')
        ->where('users.id', $user_id)
        ->select(
            'users.id',
            'users.email',
            'users.password',
            'users.firstName',
            'users.lastName',
            'users.socialId',
            'users.birthday',
            'users.gender',
            'users.SSN',
            'users.NRIC',
            'users.phoneHome',
            'users.phoneOffice',
            'users.phone',
            'users.defaultLanguage',
            'users.isActive',
            'users.defaultShipping',
            'users.defaultBilling',
            'users.hasReceiveOffer',
            'users.CountryId',
            'users.badgeId',
            'users.isGuest',
            'users.UserTypeId',
            'users.Email_Sub',
            'users.Sms_Sub',
            'users.referralId',
            'users.inviteCode',
            'users.HKG_Marketing_Sub',
            'users.remember_token',
            'users.created_at',
            'users.updated_at',
            'languagedetails.mainlanguageCode as urllang'
        )
        ->first();
        return $getUserInfo;
    }

    public function getDeliveryDetails($user_id)
    {
        $getShipmentDetails = DeliveryAddresses::leftJoin('users', 'users.defaultShipping', 'deliveryaddresses.id')
            ->select('deliveryaddresses.*')
            ->where('users.id', $user_id)
            ->get()
            ->toArray();

        return $getShipmentDetails;
    }

    public function getBillingDetails($user_id)
    {
        $getShipmentDetails = DeliveryAddresses::leftJoin('users', 'users.defaultBilling', 'deliveryaddresses.id')
            ->select('deliveryaddresses.*')
            ->where('users.id', $user_id)
            ->get()
            ->toArray();

        return $getShipmentDetails;
    }

    public function getCards($user_id)
    {
        $cards = Cards::where('UserId', $user_id)->orderBy('isDefault', 'desc')->get();
        return $cards;
    }

    public function getDefaultCard($user_id)
    {
        $defaultCard = Cards::where('UserId', $user_id)->where('isDefault', 1)->get();
        return $defaultCard;
    }

    public function getCard($id)
    {
        $card = Cards::where('id', $id)->first();
        return $card;
    }

    // get user info from delivery id
    public function getUserInfoFromDeliveryId($delivery_id)
    {
        $getUserInfoFromDeliveryId = DeliveryAddresses::join('users', 'users.id', 'deliveryaddresses.UserId')
            ->select('users.id as UserId', 'deliveryaddresses.*')
            ->where('deliveryaddresses.id', $delivery_id)
            ->first();

        return $getUserInfoFromDeliveryId;
    }

    // get user info from billing id
    public function getUserInfoFromBillingId($billing_id)
    {
        $getUserInfoFromBillingId = DeliveryAddresses::join('users', 'users.id', 'deliveryaddresses.UserId')
            ->select('users.id as UserId', 'deliveryaddresses.*')
            ->where('deliveryaddresses.id', $billing_id)
            ->first();

        return $getUserInfoFromBillingId;
    }

    public function getCountryInfo($user_id)
    {
        $user = User::where('id', $user_id)->first();
        return Countries::where('id', $user->CountryId)->first();
    }

    public function referral_info($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $data = (Object) array();
        if ($user) {
            $referral = Referral::where('referralId', $user->id)->get();
            $ref_currency = RewardsCurrency::where('CountryId', $user->CountryId)->first();
            $rewardsIn = RewardsIn::where('UserId', $user->id)->get();
            $rewardsOut = RewardsOut::where('UserId', $user->id)->get();
            $_cash = ReferralCashOut::where('UserId', $user->id)->where('status', 'active')->sum('amount');

            $data->referral = $referral;
            $data->ref_currency = $ref_currency;
            $data->rewardsIn = $rewardsIn;
            $data->rewardsOut = $rewardsOut;
            $data->_cash = $_cash;
        }
        return $data;
    }

    public function getEmailDefaultLanguage($userId, $planId) {
        $defaultLanguage = "";
        if($userId && $planId) {
            $user = json_decode(User::where('id', $userId)->first()->ToJson());
            if($user->defaultLanguage) {
                $defaultLanguage = $user->defaultLanguage;
            } else {
                $countryId = Plans::where('id', $planId)->pluck('CountryId')->first();
                $defaultLanguage = strtoupper(Countries::where('id', $countryId)->pluck('defaultLang')->first());
            }
        }
        return $defaultLanguage;
    }
    
    public function getUsersList($options)
    {
        $maxresults = $options["maxresults"];
        $users = User::latest()
                        ->when($maxresults > 0, function ($q) use ($maxresults) {
                            return $q->take($maxresults);
                        })
                        ->get();

        return ($users);
    }

}
