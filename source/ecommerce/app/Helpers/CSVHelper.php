<?php

namespace App\Helpers;

use App;

class CSVHelper
{
   
    public function __construct()
    {
        
    }

    public function readCSV($request)
    {
        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));
    
        $csvname = $request->file('csv_file')->getClientOriginalName();
        $csvheader = $request->has('header');
        $csvdata = [];
        $csvdata["csvheader"] = $csvheader;
        $csvdata["csvname"] = $csvname;
        $csvdata["data"] = $data;

        return $csvdata;
    }

}
