<?php

namespace App\Helpers;

use Aws;
use Aws\S3\S3Client;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Uuid;
use Exception;
// Instantiate an Amazon S3 client.

class AWSHelper
{
    public function __construct()
    {
        $credentials = new Aws\Credentials\Credentials(config('aws.credentials.key'), config('aws.credentials.secret'));
        $this->s3 = new Aws\S3\S3Client([
            'version' => 'latest',
            'region' => config('aws.region'),
            'credentials' => $credentials,
        ]);

        $this->bucketName = config('environment.aws.settings.bucketName');

    }

    public function __folderPath($type)
    {
        return config('environment.aws.settings' . $type);
    }

    public function __uuid()
    {

        // Generate a version 4, truly random, UUID
        // Uuid::generate(4);

        // Generate a version 1, time-based, UUID. You can set the optional node to the MAC address. If not supplied it will generate a random MAC address.
        // Uuid::generate(1, '00:11:22:33:44:55');

        // Generate a version 3, name-based using MD5 hashing, UUID
        // Uuid::generate(3, 'test', Uuid::NS_DNS);

        //Generate a version 5, name-based using SHA-1 hashing, UUID
        // Uuid::generate(5, 'test', Uuid::NS_DNS);

        return Uuid::generate(5, '_', Uuid::NS_DNS);

    }

    public function __getuuid()
    {
        // To import a UUID
        $uuid = Uuid::import('d3d29d70-1d25-11e3-8591-034165a3a613');

        // Extract the time for a time-based UUID (version 1)
        // $uuid = Uuid::generate(1);
        // dd($uuid->time);

        // Extract the version of an UUID
        // $uuid = Uuid::generate(4);
        // dd($uuid->version);
    }

    public function __image()
    {
        return response()->file(public_path('images/common/Avatar.png'));
    }

    public function __putPDF($file, $source_path, $upload_path, $filename, $visibility)
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $contents = file_get_contents($source_path);
        if (!$filename) {
            $_filename = $this->__uuid() . '.' . $ext;
        } else {
            $_filename = $filename;
        }
        try {
            //Upload File to s3
            $__upload = $this->s3->putObject(array(
                'Bucket' => $this->bucketName,
                'Key' => $upload_path . '/' . $_filename,
                'Body' => $contents,
                'ACL' => 'public-read',
            ));
            \Log::channel('cronjob')->info('AWS Helper | __putPDF | 4 ');
            if ($__upload["@metadata"]["statusCode"] === 200) {
                unlink($source_path);
                // Print the URL to the object.
                return $__upload['ObjectURL'] . PHP_EOL;
            }

        } catch (Exception $e) {
            \Log::channel('cronjob')->error('AWS Helper | __putPDF | error ' . $e->getMessage());
            \Log::channel('cronjob')->error('AWS Helper | __putPDF | error ' . $e->getMessage() . PHP_EOL);
            return $e;
        }
    }

    public function __putImg($source_path, $upload_path, $filename, $ext)
    {
        $filename = $filename ? $filename : $this->__uuid();
        $contents = file_get_contents($source_path);
        $_file = $filename . '.' . $ext;

        //Upload File to s3
        $__upload = $this->s3->putObject(array(
            'Bucket' => $this->bucketName,
            'Key' => $upload_path . '/' . $_file,
            'Body' => $contents,
            'ACL' => 'public-read',
        ));

        if ($__upload["@metadata"]["statusCode"] === 200) {
            return $__upload;
        } else {
            return false;
        }
    }

    public function __putExcel($file, $source_path, $upload_path, $filename, $visibility)
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $contents = file_get_contents($source_path);

        if (!$filename) {
            $_filename = $this->__uuid() . '.' . $ext;
        } else {
            $_filename = $filename;
        }

        try {
            //Upload File to s3
            $__upload = $this->s3->putObject(array(
                'Bucket' => $this->bucketName,
                'Key' => $upload_path . '/' . $_filename,
                'Body' => $contents,
                'ACL' => 'public-read',
            ));

            if ($__upload["@metadata"]["statusCode"] === 200) {
                unlink($source_path);
                // Print the URL to the object.
                return $__upload['ObjectURL'] . PHP_EOL;
            }

        } catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    public function __get($file)
    {
        $file = Storage::disk('s3')->exists($file);

        return $file;

        // $files = Storage::files($directory);
        // $files = Storage::allFiles($directory);
        // $directories = Storage::directories($directory);
        // Recursive...
        // $directories = Storage::allDirectories($directory);
    }

    public function __makeDirectories($directory)
    {

        Storage::makeDirectory($directory);
    }

    public function __download($filename, $name, $headers)
    {
        return Storage::download($filename, $name, $headers);
    }

    public function __tempURLs($file)
    {
        $url = Storage::temporaryUrl($file, now()->addMinutes(5), ['ResponseContentType' => 'application/octet-stream']);
        return $url;
    }

    /**
     * save the avatar for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function __storeAvatars(Request $request)
    {
        $path = $request->file('avatar')->store('avatars');
        return $path;
    }

    public function __destruct()
    {
        // Delete File
        // Storage::delete('file.jpg');

        // Delete Multiple Files
        // Storage::delete(['file.jpg', 'file2.jpg']);

        // Delete File with specified folder
        // Storage::disk('s3')->delete('folder_path/file_name.jpg');

        // Delete Directory
        // Storage::deleteDirectory($directory);
    }

    public function __resize($file)
    {
        $img = Image::make($file)->resize(300, 200);

        return $img->response('jpg');
    }

}