<?php

namespace App\Helpers\Email;

/**
 * Dependancies
 */

use App\Models\GeoLocation\Countries;

/**
 * Relevant Models
 */

use App\Models\GeoLocation\SupportedLang;
use Lang;

/**
 * Mailable Classes
 */

use Mail;

class EmailQueueHelper
{
    /**
     * Constructor
     * Gets The Country & Locale info from Session
     */
    public function __construct()
    {
        // $this->sessionCountry = json_decode(session()->get('currentCountry'), true);
        // $this->currentLocale = strtolower(session()->get('currentLocale'));
    }

    /**
     * Building Email Template Function
     *
     * @param string $title = The module title of the email sent, needs to match with folder name in resources/views/email-templates
     *
     * @param array $moduleData = Should include all variables which will be passed to the email template
     *
     * @param array $CountryAndLocaleData = Country JSON & Locale Data
     *
     * @return null
     */
    public function buildEmailTemplate($title, $moduleData, $CountryAndLocaleData)
    {
        /**
         *  Handle Country & Locale Data
         *  $CountryAndLocaleData[0] Contains Current country information
         *  $CountryAndLocaleData[1] Contains Current Locale
         */
        $CountryDataType = gettype($CountryAndLocaleData[0]);

        // If $CountryDataType is (JSON) Object
        if ($CountryDataType == 'object') {

            // json_decode to array and assign value to $currentCountry
            $currentCountry = json_decode($CountryAndLocaleData[0], true);
        } // If $CountryDataType is String
        else if ($CountryDataType == 'string') {

            // json_decode and pass values to intermediate variable
            $result = json_decode($CountryAndLocaleData[0], true);

            // if no errors running json_encode, string is valid JSON
            if (json_last_error() === JSON_ERROR_NONE) {
                // Assign values of intermediate variable to $currentCountry
                $currentCountry = $result;
            } // if error when json_encode
            else {
                // Parse String into array
                parse_str($CountryAndLocaleData[0], $currentCountry);
            }
        } // For other data types
        else {
            // No Further Actions for other Data Types
            $currentCountry = $CountryAndLocaleData[0];
        }

        // Pass Locale Information to $currentLocale variable
        $currentLocale = strtolower($CountryAndLocaleData[1]);

        // Set Local Based on $currentLocale
        \App::setLocale($currentLocale);

        // Get Country default Lang in Database Based on Locale Language
        $getActiveCountryByMainLang = Countries::where([['id', $currentCountry['id']], ['defaultLang', $currentLocale]])->first();

        // If default lang doesn't match Locale Language
        if (empty($getActiveCountryByMainLang)) {
            // Get Supported(sub) Language based on locale Language
            $getActiveCountrySubLang = SupportedLang::where([['languageCode', $currentLocale], ['CountryId', $currentCountry['id']]])->first();

            // Store Email Language based on Supported Language query results
            $emailLang = strtolower($getActiveCountrySubLang['languageCode']);
        } else {
            // Store Email Language based on Country table query results
            $emailLang = strtolower($getActiveCountryByMainLang['defaultLang']);
        }

        // Configure email Headers
        $emailHeaders = [];

        // If email value exist in $moduleData @param
        if ($moduleData['email'] !== null) {

            // configure recipient to $moduleData['email'] value
            $emailHeaders['mailto'] = $moduleData['email'];
        }

        // Build Email Title From Translation Files, Based on current Language
        $emailHeaders['subject'] = Lang::get('email.subject.' . $title);

        $mailableClass = $this->initMailableClass($title);
        
        // Call the Send Email Function
        // Pass relevant parameters to function queueTransactionalEmail() below
        $this->queueTransactionalEmail($mailableClass, $moduleData, $currentCountry, $emailHeaders);

        // Pass relevant parameters to function sendTransactionalEmail() below (Not in use.)
        // $this->sendTransactionalEmail($title, $moduleData, $currentCountry, $emailHeaders);
    }

    public function initMailableClass($title)
    {
        // Store new Mailable Class Namespace & Name based on $title Parameter (required for queue)
        switch ($title) {
                // Declaring new class with variables requires Full Namespace
            case 'welcome':
                $mailableClass = 'App\Mail\WelcomeMail';
                break;
            case 'receipt-order-confirmed':
            case 'receipt-order':
            case 'receipt-order-shipped':
            case 'receipt-order-delivered':
                $mailableClass = 'App\Mail\ReceiptsMail';
                break;
            case 'receipt-payment-failure-subscription':
            case 'receipt-payment-failure-product':
                $mailableClass = 'App\Mail\SubcriptionChargeFailureMail';
                break;
            case 'referral-email-invite':
                $mailableClass = 'App\Mail\ReferralEmailInvite';
                break;
            case 'referral-subscription-invite':
                $mailableClass = 'App\Mail\ReferralSubscriptionInviteMail';
                break;
            case 'referral-inactive-credits':
                $mailableClass = 'App\Mail\ReferralInactiveCreditsMail';
                break;
            case 'referral-active-credits':
                $mailableClass = 'App\Mail\ReferralActiveCreditsMail';
                break;
            case 'subscription-renewal':
                $mailableClass = 'App\Mail\SubscriptionRenewalMail';
                break;
            case 'subscription-modified':
                $mailableClass = 'App\Mail\SubscriptionModifiedMail';
                break;
            case 'subscription-renewal-annual':
                $mailableClass = 'App\Mail\SubcriptionAnnualBillingMail';
                break;
            case 'subscription-recharge-nicepay-email-report':
            case 'subscription-recharge-email-report':
                $mailableClass = 'App\Mail\SubscriptionRechargeEmailReport';
                break;
            case 'subscription-cancellation':
                $mailableClass = 'App\Mail\SubscriptionCancellationMail';
                break;
            case 'password-reset':
                $mailableClass = 'App\Mail\ResetPasswordMail';
                break;
            case 'password-updated':
                $mailableClass = 'App\Mail\UpdatePasswordMail';
                break;
            case 'reminder-promo-3day':
                $mailableClass = 'App\Mail\ReminderPromo3dayMail';
                break;
            case 'reminder-promo-7day':
                $mailableClass = 'App\Mail\ReminderPromo7dayMail';
                break;
            case 'card-expire':
                $mailableClass = 'App\Mail\CardExpiryMail';
                break;
            case 'reactivate':
                $mailableClass = 'App\Mail\ReactivateMail';
                break;
            case 'goodbye':
                $mailableClass = 'App\Mail\GoodBye';
                break;
            case 'order-cancellation':
                $mailableClass = 'App\Mail\OrderCancellationMail';
                break;
            case 'order-cancelled':
                $mailableClass = 'App\Mail\OrderCancelledMail';
                break;
            case 'cancellation-history-report':
                $mailableClass = 'App\Mail\CancellationHistoryReportMail';
                break;
            case 'contact-us':
                $mailableClass = 'App\Mail\ContactUsMail';
                break;
            case 'user-email-update':
                $mailableClass = 'App\Mail\UserEmailUpdate';
                break;
            case 'ops-team-notification':
                $mailableClass = 'App\Mail\OpsTeamNotificationMail';
                break;
            case 'ops-team-notification-function':
                $mailableClass = 'App\Mail\OpsTeamNotificationCheckFunctionMail';
                break;
            case 'referral-cashout-request':
                $mailableClass = 'App\Mail\ReferralCashoutMail';
                break;
            default:
                // Currently No Fallback Setup if mailable class does not exist
                return 'Email Template Not Found!';
        }
        return $mailableClass;
    }

    // Send Transactional Email
    public function sendTransactionalEmail($title, $moduleData, $country, $emailHeaders)
    {
        // Send Email Function
        Mail::send('email-templates.' . $title . '.template', ['moduleData' => $moduleData, 'country' => $country], function ($message) use ($emailHeaders) {
            // Email Recepient
            $message->to($emailHeaders['mailto'])
                // Email Subject
                ->subject($emailHeaders['subject']);
        });
        \Log::channel('cronjob')->info('Sent Mail '. $title);
    }

    // Queue Transactional email
    public function queueTransactionalEmail($mailableClass, $moduleData, $country, $emailHeaders) //$getEmailTemplatePath

    {
        /**
         * Queue Email Function with Class Name = $mailableClass;
         * The rest is managed in Mailable Class Function, Under app/Mail/{YourMailable}.php
         */
        Mail::queue(new $mailableClass($moduleData, $country, $emailHeaders));
        \Log::channel('cronjob')->info('Queued Mail');
    }
}
