<?php

namespace App\Helpers;

use App\Helpers\Payment\Stripe;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Helpers\PromotionsHelper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Controllers\Ecommerce\Rebates\Referral\ReferralController;
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanCategoryDetails;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductType;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Exception;
use Lang;
use finfo;
use DB;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Subscription;
use \App\Helpers\RebatesHelper;
use \App\Helpers\UserHelper;
use App\Models\CancellationJourney\CancellationJourneys;
use App\Http\Controllers\Globals\Plans\FreeProductController;
use App\Queue\SendEmailQueueService as EmailQueueService;
use App\Services\CountryService;
class SubscriptionHelper
{
    public function __construct()
    {
        $this->userHelper = new UserHelper;
        $this->referralController = new ReferralController();
        $this->planHelper = new PlanHelper();
        $this->productHelper = new ProductHelper();
        $this->promotionHelper = new PromotionsHelper();
        $this->country_service = new CountryService();
        $this->country_info = $this->country_service->getCountryAndLangCode();
        $this->country_id = $this->country_info['country_id'];
        $this->country_iso = $this->country_info['country_iso'];
         // if Not Korea
         if ($this->country_id !== 8) {
            $this->stripe = new Stripe();
        }
    }

    // Function: Check if user already have applied trial plan before
    public function HaveAppliedTrialPlan($userId)
    {
        $trialPlanCount = Subscriptions::where('UserId',$userId)->where('isTrial',1)->whereIn('status',['Cancelled','On Hold','Unrealized','Processing'])->count();
        $status = false;
        if($trialPlanCount > 0) {
            $status = true;
        }
        return $status;
    }

    // Function: Check if user is still within trial period
    public function IsWithinTrialPeriod($subscriptionId)
    {
        $subs = Subscriptions::where('id',$subscriptionId)->first();
        $status = true;
        if($subs["nextChargeDate"] !== null && $subs["nextDeliverDate"] !== null && $subs["currentChargeNumber"] > 0) {
            $status = false;
        }
        return $status;
    }

    // Function: Check if user already apply cancellation journey
    public function HaveAppliedCancellationDiscount($userId)
    {
        $rowCount = CancellationJourneys::where('UserId',$userId)->where('hasGetDiscount',1)->count();
        $status = false;
        if($rowCount > 0) {
            $status = true;
        }
        return $status;
    }

    // Function: Check if user already apply free product
    public function HaveAppliedFreeProduct($userId)
    {
        $rowCount = CancellationJourneys::where('UserId',$userId)->where('hasGetFreeProduct',1)->count();
        $status = false;
        if($rowCount > 0) {
            $status = true;
        }
        return $status;
    }

    // Function: Check if the user already paused their subscription before
    public function HavePausedSubscription($userId)
    {
        $rowCount = CancellationJourneys::where('UserId',$userId)->where('hasPausedSubscription',1)->count();
        $status = false;
        if($rowCount > 0) {
            $status = true;
        }
        return $status;
    }

    // Function: Check if subscription is an annual plan
    public function IsAnnualPlan($subscriptionId)
    {
        $subs = Subscriptions::where('id',$subscriptionId)->first();
        $plan = Plans::where('id',$subs->PlanId)->first();
        $planSku = PlanSKU::where('id',$plan->PlanSkuId)->first();
        $status = false;
        if($planSku["isAnnual"] === 1) {
            $status = true;
        }
        return $status;
    }

    // Function : Get subscription name
    public function GetSubscriptionName($subscriptionId)
    {
        $currentPlan = $this->GetCurrentPlan($subscriptionId);

        $currentBladeSku = $currentPlan->available_plans->blade_type["sku"];
        $currentBladeNumber = config('global.all.blade_no.'. $currentBladeSku);
        $currentBladeName = $currentBladeNumber . ' ' . Lang::get('website_contents.blade_packV2');

        $currentDuration = $currentPlan->current_subscription_duration;
        $currentDurationName = Lang::get('website_contents.every') . ' ' . $currentDuration . ' ' . Lang::get('website_contents.Months');

        $currentSubscriptionName = $currentBladeName . ' ' . $currentDurationName;

        return $currentSubscriptionName;
    }

    // Function: Cancel subscription
    public function CancelSubscription($subscriptionId, $cancellationReason, $frontendOrigin)
    {
        // Setup cancellation reason for subscription table
        $subscriptionCancellationReason = "";
        $subscriptionHistoryMessage = "";
        if($frontendOrigin === 'cancellation_journey') {
            $subscriptionCancellationReason = $cancellationReason;
            $subscriptionHistoryMessage = "Cancelled from cancellation journey. Reason: " . $cancellationReason;
        } else {
            $subscriptionCancellationReason = "Cancelled from admin panel.";
            $subscriptionHistoryMessage = "Cancelled from admin panel.";
        }

        // Get subscription details
        $subs = Subscriptions::where('id', $subscriptionId)->first();
        $plan = Plans::where('id', $subs->PlanId)->first();

        // Get country details
        $country = Countries::where('id', $plan->CountryId)->first();

        // Update subscription status to payment failure
        Subscriptions::where('id', $subscriptionId)
        ->update([
            'status' => 'Cancelled',
            'cancellationReason' => $subscriptionCancellationReason
        ]);

        // Determine whether the subscription is still in trial period
        $isWithinTrialPeriod = $this->IsWithinTrialPeriod($subscriptionId);

        // Send Cancellation Journey EDM 1 to user
        $currentCountry = $country;
        $currentLocale = $this->userHelper->getEmailDefaultLanguage($subs->UserId, $subs->PlanId);
        $emailData = [];

        if($isWithinTrialPeriod) {
            $emailData['title'] = 'order-cancelled';
            $emailData['moduleData'] = (Object) array(
                'email' => $subs->email,
                'subscriptionId' => $subscriptionId,
                'userId' => $subs->UserId,
                'countryId' => $country->id
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
            EmailQueueService::addHash($subs->UserId,'order_cancelled',$emailData);
        } else {
            $emailData['title'] = 'subscription-cancellation';
            $emailData['moduleData'] = (Object) array(
                'email' => $subs->email,
                'subscriptionId' => $subscriptionId,
                'userId' => $subs->UserId,
                'countryId' => $country->id
            );
            $emailData['CountryAndLocaleData'] = array($currentCountry,$currentLocale);
            EmailQueueService::addHash($subs->UserId,'subscription_cancellation',$emailData);
        }

        // Create subscription history
        $this->CreateSubscriptionHistory($subscriptionId, 'Cancelled', $subscriptionHistoryMessage);
    }

    // Function: Update subscription
    // Note: Please pass $actions and $actionParameters in array (Refer available actions at the end of this file)
    public function UpdateSubscriptionDetails($subscriptionId, $actions, $actionParameters, $frontendOrigin)
    {
        // Get current plan details for comparison
        $currentPlanData = $this->GetCurrentPlan($subscriptionId);

        // Prepare updates variables
        $updatedPlanId = $currentPlanData->subscription->PlanId;
        $updatedTempDiscountPercent = $currentPlanData->subscription->temp_discountPercent;
        $updatedHasTriedCancel = 0;
        $updatedNextChargeDate = $currentPlanData->subscription->nextChargeDate;
        $updatedNextDeliverDate = $currentPlanData->subscription->nextDeliverDate;
        $updatedStartDeliverDate = $currentPlanData->subscription->startDeliverDate;
        $updatedHasMaximumTenure = $currentPlanData->subscription->hasMaximumTenure;

        // Initialize subscription history details
        $subscriptionHistoryDetails = "";

        // If update comes from cancellation journey, update hasTriedCancel to 1
        if($frontendOrigin === 'cancellation_journey') {
            $updatedHasTriedCancel = 1;
            $subscriptionHistoryDetails .= "Modified from cancellation journey. ";
        }

        // Loop through actions
        foreach ($actions as $action) {

            switch ($action) {
                case 'apply_cancellation_discount':
                    {
                        // Action parametes
                        $updatedTempDiscountPercent = $actionParameters["cancellationDiscount"];

                        $subscriptionHistoryDetails .= "Apply " . $updatedTempDiscountPercent . "%" . " cancellation discount. ";
                    }
                    break;

                case 'change_blade':
                    {
                        // Action parametes
                        $updatedBladeSku = $actionParameters["modifiedBladeSku"];
                        $updatedPlanId = $actionParameters["modifiedPlanId"];

                        $subscriptionHistoryDetails .= "Modified blade sku from " . $currentPlanData->available_plans->blade_type["sku"] . " into " . $updatedBladeSku . ". ";
                    }
                    break;

                case 'change_frequency':
                    {
                        // Action parametes
                        $updatedFrequencyDuration = $actionParameters["modifiedFrequencyDuration"];
                        $updatedPlanId = $actionParameters["modifiedPlanId"];

                        $subscriptionHistoryDetails .= "Modified plan duration from " . $currentPlanData->current_subscription_duration . " months" . " into " . $updatedFrequencyDuration . " months" . ". ";
                    }
                    break;

                case 'apply_free_product':
                    {
                        $subscriptionHistoryDetails .= "Applied free blade replacement. " ;
                    }
                    break;

                case 'pause_subscription':
                    {
                        // Action parametes
                        $pause_months = $actionParameters["pauseMonths"];
                        $updatedHasMaximumTenure = 1;

                        if ($currentPlanData->subscription->nextChargeDate !== null) {
                            $updatedNextChargeDate = Carbon::parse($currentPlanData->subscription->nextChargeDate)->addMonths($pause_months)->format('Y-m-d');
                        }
                        if ($currentPlanData->subscription->nextDeliverDate !== null) {
                            $updatedNextDeliverDate = Carbon::parse($currentPlanData->subscription->nextDeliverDate)->addMonths($pause_months)->format('Y-m-d');
                        }
                        if ($currentPlanData->subscription->startDeliverDate !== null) {
                            $updatedStartDeliverDate = Carbon::parse($currentPlanData->subscription->startDeliverDate)->addMonths($pause_months)->format('Y-m-d');
                        }

                        $subscriptionHistoryDetails .= "Paused plan duration from " . $currentPlanData->subscription->nextChargeDate . ' to ' . $updatedNextChargeDate . ". ";
                    }
                    break;

                default:
                    break;
            }

        }

        // Update subscriptions
        Subscriptions::where('id', $subscriptionId)
        ->update([
            'PlanId' => $updatedPlanId,
            'temp_discountPercent' => $updatedTempDiscountPercent,
            'hasTriedCancel' => $updatedHasTriedCancel,
            'nextChargeDate' => $updatedNextChargeDate,
            'nextDeliverDate' => $updatedNextDeliverDate,
            'startDeliverDate' => $updatedStartDeliverDate,
            'hasMaximumTenure' => $updatedHasMaximumTenure
        ]);

        // Create subscription history
        $this->CreateSubscriptionHistory($subscriptionId, "Modified", $subscriptionHistoryDetails);

        // Additional actions for free product replacement
        if(in_array('apply_free_product', $actions)){
            $freeProductController = new FreeProductController();
            $freeProductController->MainProcess($subscriptionId);
        }
    }

    // Function: Create subscription history
    public function CreateSubscriptionHistory($subscriptionId, $message, $details)
    {
        $subshistory = new SubscriptionHistories();
        $subshistory->message = $message;
        $subshistory->detail = $details;
        $subshistory->subscriptionId = $subscriptionId;
        $subshistory->save();
    }

    public function GetCurrentPlan($subscriptionId)
    {
        $data = (Object) array();
        $data->user = Auth::user();

        // Get new country info from session
        $currentCountryData = json_decode(session()->get('currentCountry'), true);
        $currentCountryIso = strtoupper($currentCountryData['codeIso']);
        $langCode = Auth::user()->defaultLanguage;
        $types_of_plan_frequency = json_decode(json_encode(PlanSKU::join('plans','plans.PlanSkuId','plansku.id')->where('plansku.status',1)->where(function ($q) {
            $q->where('plans.isOnline', "!=", 0)->orWhere('plans.isOffline', "!=", 0);})->select('plansku.duration')->distinct()->get()), true);
        $data->types_of_plan_frequency = $types_of_plan_frequency;

        // optional products for custom plan only
        $optional_product_ids = [1026, 1025, 34];

        $user_subscription = Subscriptions::where('subscriptions.id', $subscriptionId)->join('plans', 'plans.id', 'subscriptions.PlanId')
            ->join('plansku', 'plansku.id', 'plans.PlanSkuId')->select('subscriptions.*', 'plansku.duration')->first();

        $country = Countries::join('plans', 'plans.CountryId', 'countries.id')->where('plans.id', $user_subscription->PlanId)->select('countries.*')->first();

        $data->country = $country;
        $data->subscription = $user_subscription;

        $promotions = [];
        // check for existing promotions for current shave plan
        if ($user_subscription->promoCode && $user_subscription->promotionId) {
            $_activePromo = $this->promotionHelper->promotionInfo($user_subscription->promoCode, $country);
            if ($_activePromo !== null) {
                $product_info = null;
                if ($_activePromo->freeExistProductCountryIds !== null || $_activePromo->freeProductCountryIds !== null) {
                    $product_country_ids = json_decode($_activePromo->freeProductCountryIds ? $_activePromo->freeProductCountryIds : $_activePromo->freeExistProductCountryIds);

                    $get_product_info = array(
                        "appType" => 'ecommerce', // appType = [ecommerce,baWebsite]
                        "isOnline" => true,
                        "isOffline" => false,
                        "product_country_ids" => $product_country_ids,
                    );
                    $product_info = \App\Helpers\LaravelHelper::ConvertArraytoObject($this->productHelper->getProductByProductCountriesV3(Auth::user()->CountryId, $langCode, $get_product_info));
                }
                array_push($promotions, (Object) array(
                    "type" => "normal",
                    "subtype" =>
                    $_activePromo->freeProductCountryIds !== null ? "free_products" :
                    ($_activePromo->freeExistProductCountryIds !== null ? "free_existing_products" :
                        ($_activePromo->discount > 0 ? "discount" :
                            ($_activePromo->freeExistProductCountryIds !== null && $_activePromo->discount > 0 ? "free_existing_products_and_discount" :
                                ($_activePromo->freeProductCountryIds !== null && $_activePromo->discount > 0 ? "free_products_and_discount" :
                                    ($_activePromo->ProductBundleId !== null ? "product_bundle" : 'undefined'))))),
                    "cycle" => $_activePromo->promotionType,
                    "code" => $user_subscription->promoCode,
                    "discount" => $_activePromo->discount,
                    "info" => $_activePromo,
                    "product_info" => $product_info,
                ));
            }
        }

        // check for cancellation discounts in existing plan
        if ($user_subscription->temp_discountPercent > 0) {
            array_push($promotions, (Object) array(
                "type" => "cancellation",
                "subtype" => null,
                "cycle" => "Instant",
                "code" => null,
                "discount" => $user_subscription->temp_discountPercent,
                "info" => null,
                "product_info" => null,
            ));
        }

        $data->existing_promotions = $promotions;

        $plandetails = $this->planHelper->getPlanDetailsByPlanId($user_subscription->PlanId, $country->id, $langCode);

        $data->plan_details = $plandetails;

        $defaultplanproducts = [];
        $defaultaddons = [];
        foreach ($plandetails["product_info"] as $pd) {
            array_push($defaultplanproducts, $pd["productcountryid"]);
        }

        $productAddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->join('productimages', 'productimages.ProductId', 'products.id')
            ->select('subscriptions_productaddon.ProductCountryId as ProductCountryId', 'producttranslates.name', 'productimages.url', 'products.id as ProductId')
            ->where('subscriptionId', $user_subscription->id)
            ->where('producttranslates.langCode', $langCode)
            ->where('productimages.isDefault', 1)
            ->get();

        foreach ($productAddons as $pda) {
            array_push($defaultplanproducts, $pda["ProductCountryId"]);
            array_push($defaultaddons, $pda);
        }

        $data->defaultplanproducts = json_encode($defaultplanproducts);
        $data->defaultaddons = $defaultaddons;

        // remove optional products if exists in plan details
        foreach ($data->plan_details["product_info"] as $p_default) {
            if (array_search((int) $p_default["productid"], $optional_product_ids)) {
                $key = array_search((int) $p_default["productid"], $optional_product_ids);
                unset($optional_product_ids[$key]);
            }
        }

        // remove optional products if exists in default addons
        foreach ($data->defaultaddons as $key => $p_addons) {
            foreach (array_keys($optional_product_ids, (int) $p_addons->ProductId, true) as $key) {
                unset($optional_product_ids[$key]);
            }
        }

        $optional_products = Product::join('productcountries', 'productcountries.ProductId', 'products.id')
            ->join('productimages', 'productimages.ProductId', 'products.id')
            ->join('producttranslates', 'producttranslates.ProductId', 'products.id')
            ->whereIn('products.id', $optional_product_ids)
            ->where('producttranslates.langCode', $langCode)
            ->where('productcountries.CountryId', Auth::user()->CountryId)
            ->where('productimages.isDefault', 1)
            ->select('productcountries.id as ProductCountryId', 'products.id as ProductId', 'productcountries.*', 'productimages.url', 'producttranslates.*')
            ->get();

        $data->optional_products = $optional_products;

        $subscription_card = Cards::where('id', $user_subscription->CardId)->first();

        $shipping = DeliveryAddresses::where('id', Auth::user()->defaultShipping)->first();
        $billing = DeliveryAddresses::where('id', Auth::user()->defaultBilling)->first();

        $data->address = (Object) array("shipping" => $shipping, "billing" => $billing);
        $data->subscription_addons = $productAddons;
        $data->current_subscription_duration = $user_subscription->duration;
        $data->subscription_card = $subscription_card;
        $data->available_plans = $this->GetAvailablePlans(Auth::user()->id, $user_subscription->id);

        $data->urllangCode = strtoupper(json_decode(session()->get('currentCountry'), true)['urlLang']);
        $data->langCode = strtoupper(app()->getLocale());
        $data->usertype = 'web';
        $data->promotion_type = 'shave_plans';

        if ($productAddons) {
            // push individual product info into items
            foreach ($productAddons as $addon) {

                $in_cycle = false;
                $in_cycle_with_all = false;

                if ($addon->cycle === "all") {
                    $plan_cycles = $addon->cycle;
                } else {
                    $plan_cycles = json_decode($addon->cycle, true);
                    //check if all exists, if exists save to a new variable
                }

                // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                if (is_array($plan_cycles)) {
                    foreach ($plan_cycles as $_p_cycles) {
                        if ($_p_cycles["c"] === $user_subscription->currentCycle) {
                            $in_cycle = true;
                            $cycle_no = $_p_cycles["c"];
                            $_qty = $_p_cycles["q"];
                        } else {
                            if ($_p_cycles["c"] === "all") {
                                $in_cycle_with_all = true;
                                $cycle_no = 1;
                                $_qty = $_p_cycles["q"];
                            }
                        }
                    }
                }

                // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                if (!is_array($plan_cycles)) {
                    //if cycle is in numbers / string
                    $_in_cycle = $plan_cycles;
                    if ($addon->cycle === "all") {
                        $in_cycle = true;
                        $cycle_no = "recurring";
                        $_qty = $addon->product_qty;
                    } else {
                        if ($_in_cycle === $user_subscription->currentCycle) {
                            $in_cycle = true;
                            $cycle_no = $_in_cycle;
                            $_qty = $addon->product_qty;
                        }
                    }
                }

                // if products are within the currentCycle, add query DB to get Addons
                if ($in_cycle == true) {
                    $in_cycle == false;
                    $data->subscription_addon_product_name = (Object) array("productcountryid" => $addon->ProductCountryId, "product_name" => $addon->name);
                } else {
                    if ($in_cycle_with_all == true) {
                        $data->subscription_addon_product_name = (Object) array("productcountryid" => $addon->ProductCountryId, "product_name" => $addon->name);
                    }
                }
            }
        }
        return $data;
    }

    public function GetAvailablePlans($user_id, $subscription_id)
    {
        try {
            $blade_types = $handle_types = $bag_types = $addons = null;
            $blade_type = $handle_type = $bag_type = $addon_type = '';

            $available_frequencies = [];
            $non_filtered_planIds = [];
            $filtered_planIds = [];

            if ($user_id && $subscription_id) {
                $u = User::where('id', $user_id)->first();
                $s = Subscriptions::where('id', $subscription_id)->first();
                $p_ctgry = 0;
                $p_drtns = [];
                if ($s->isCustom === 1 && $s->isTrial === 0) {$p_ctgry = 2;}
                if ($s->isTrial === 1 && $s->isCustom === 0) {$p_ctgry = 1;}

                $isAnnual = 0;
                $gender = 'male';
                $checkplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')->where('plans.id', $s->PlanId)->select('plansku.isAnnual')->first();
                $current_plan_info = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plans.PlanSkuId')
                    ->join('plan_categories', 'plan_categories.id', 'plan_category_details.PlanCategoryId')
                    ->where('plans.id', $s->PlanId)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId', 'plan_categories.id as PlanCategoryId', 'plan_categories.name as PlanCategoryName')
                    ->first();
                if ($checkplans) {
                    $isAnnual = $checkplans->isAnnual;
                    if (strpos($current_plan_info->PlanCategoryName, 'Women') !== false && $s->isTrial == 1 && $s->isCustom == 0) {
                        $gender = 'female';
                        $p_ctgry = 3;
                    } else if (strpos($current_plan_info->PlanCategoryName, 'Women') !== false && $s->isCustom == 1 && $s->isTrial == 0) {
                        $gender = 'female';
                        $p_ctgry = 3;
                    }
                }

                switch ($gender) {
                    case 'male':
                        $gp_ctgry = [config('global.all.plan_category_types.men.trial')];
                        $gp_products_ctgry = config('global.all.product_category_types.men');
                        $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.men.handles');
                        $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.men.blades');
                        $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.men.addons');
                        break;
                    case 'female':
                        $gp_ctgry = [config('global.all.plan_category_types.women.trial')];
                        $gp_products_ctgry = config('global.all.product_category_types.women');
                        $gp_handle_type_names = config('global.all.product_based_on_plan_type_names.women.handles');
                        $gp_blade_type_names = config('global.all.product_based_on_plan_type_names.women.blades');
                        $gp_addon_type_names = config('global.all.product_based_on_plan_type_names.women.addons');
                        break;
                    default:
                        return null;
                }


                $p_relateds = PlanCategoryDetails::join('plansku', 'plansku.id', 'plan_category_details.PlanSkuId')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->select('plansku.*', 'plan_category_details.PlanCategoryId as PlanCategoryId', 'plansku.id as PlanSkuId')
                    ->get();

                foreach ($p_relateds as $plan) {
                    array_push($p_drtns, (int) $plan->duration);
                }

                $available_frequencies = $p_drtns = array_unique($p_drtns, SORT_NUMERIC);

                $allplans = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plans.CountryId', $u->CountryId)
                    ->where('plansku.isAnnual', $isAnnual)
                // ->where('plantranslates.langCode', $u->defaultLanguage)
                    ->whereIn('plansku.duration', $p_drtns)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        // 'plantranslates.id as PlanTranslateId',
                        'plans.*',
                        // 'plantranslates.*',
                        'plansku.*')
                    ->get();

                foreach ($allplans as $plan) {
                    array_push($non_filtered_planIds, $plan->PlanId);
                }

                // Custom Plan Blade & Handle Types
                if ($p_ctgry === 2) {
                    $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Blade')
                        ->where('productcountries.CountryId', $u->CountryId)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*')
                        ->get();

                    $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons Handle')
                        ->where('productcountries.CountryId', $u->CountryId)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*')
                        ->get();

                    $bag_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons Bag')
                        ->where('productcountries.CountryId', $u->CountryId)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*')
                        ->get();

                    $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                        ->join('products', 'products.id', 'producttypedetails.ProductId')
                        ->join('productcountries', 'productcountries.ProductId', 'products.id')
                        ->join('productimages', 'productimages.ProductId', 'products.id')
                        ->where('productimages.isDefault', 1)
                        ->where('producttypes.name', 'Custom Plan Addons')
                        ->where('productcountries.CountryId', $u->CountryId)
                        ->select(
                            'producttypedetails.id as ProductTypeDetailId',
                            'producttypes.id as ProductTypeId',
                            'products.id as ProductId',
                            'productcountries.id as ProductCountryId',
                            'productimages.url as product_default_image',
                            'producttypedetails.*', 'producttypes.*', 'products.*', 'productcountries.*')
                        ->get();

                }

               // Trial Plan Blade & Handle Types
               if ($p_ctgry === 1 || $p_ctgry === 3) {
                $blade_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                    ->join('products', 'products.id', 'producttypedetails.ProductId')
                    ->join('productcountries', 'productcountries.ProductId', 'products.id')
                    ->join('productimages', 'productimages.ProductId', 'products.id')
                    ->where('productimages.isDefault', 1)
                    ->where('producttypes.name', $gp_blade_type_names)
                    ->where('productcountries.CountryId', $u->CountryId)
                    ->select(
                        'producttypedetails.id as ProductTypeDetailId',
                        'producttypes.id as ProductTypeId',
                        'products.id as ProductId',
                        'productcountries.id as ProductCountryId',
                        'productimages.url as product_default_image',
                        'producttypedetails.*',
                        'producttypes.*',
                        'products.*',
                        'productcountries.*'
                    )
                    ->get();

                $handle_types = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                    ->join('products', 'products.id', 'producttypedetails.ProductId')
                    ->join('productcountries', 'productcountries.ProductId', 'products.id')
                    ->join('productimages', 'productimages.ProductId', 'products.id')
                    ->where('productimages.isDefault', 1)
                    ->where('producttypes.name', $gp_handle_type_names)
                    ->where('productcountries.CountryId', $u->CountryId)
                    ->select(
                        'producttypedetails.id as ProductTypeDetailId',
                        'producttypes.id as ProductTypeId',
                        'products.id as ProductId',
                        'productcountries.id as ProductCountryId',
                        'productimages.url as product_default_image',
                        'producttypedetails.*',
                        'producttypes.*',
                        'products.*',
                        'productcountries.*'
                    )
                    ->get();

                $addons = ProductType::join('producttypedetails', 'producttypedetails.ProductTypeId', 'producttypes.id')
                    ->join('products', 'products.id', 'producttypedetails.ProductId')
                    ->join('productcountries', 'productcountries.ProductId', 'products.id')
                    ->join('productimages', 'productimages.ProductId', 'products.id')
                    ->where('productimages.isDefault', 1)
                    ->where('producttypes.name', $gp_addon_type_names)
                    ->where('productcountries.CountryId', $u->CountryId)
                    ->select(
                        'producttypedetails.id as ProductTypeDetailId',
                        'producttypes.id as ProductTypeId',
                        'products.id as ProductId',
                        'productcountries.id as ProductCountryId',
                        'productimages.url as product_default_image',
                        'producttypedetails.*',
                        'producttypes.*',
                        'products.*',
                        'productcountries.*'
                    )
                    ->get();
            }

                $current_plan_items = Plans::join('plan_details', 'plan_details.PlanId', 'plans.id')
                    ->where('plans.id', $s->PlanId)->get();

                // get blade type
                if ($blade_types !== null) {
                    foreach ($blade_types as $b) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $b->ProductCountryId) {
                                $blade_type = $b;
                            }
                        }
                    }
                }

                // get handle type
                if ($handle_types !== null) {
                    foreach ($handle_types as $h) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $h->ProductCountryId) {
                                $handle_type = $h;
                            }
                        }
                    }
                }

                // get bag type
                if ($bag_types !== null) {
                    foreach ($bag_types as $bg) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $bg->ProductCountryId) {
                                $bag_type = $bg;
                            }
                        }
                    }
                }

                // get addon type
                if ($addons !== null) {
                    foreach ($addons as $a) {
                        foreach ($current_plan_items as $item) {
                            if ($item->ProductCountryId === $a->ProductCountryId) {
                                $addon_type = $a;
                            }
                        }
                    }
                }

                foreach ($non_filtered_planIds as $planId) {
                    $items = Plans::join('plan_details', 'plan_details.PlanId', 'plans.id')
                        ->where('plans.id', $planId)
                        ->select('plans.id as PlanId', 'plan_details.id as PlanDetailId', 'plans.*', 'plan_details.*')
                        ->get();

                    // custom plan filter using blade only
                    if ($p_ctgry === 2) {
                        foreach ($items as $item) {
                            if ($item->ProductCountryId === $blade_type->id) {
                                array_push($filtered_planIds, $planId);
                            }
                        }
                    }

                    // trial plan filter using blade & handle type
                    if ($p_ctgry === 1 || $p_ctgry === 3) {
                        $handle_filter = [];
                        $blade_filter = [];
                        foreach ($items as $item) {
                            if ($item->ProductCountryId === $handle_type->id) {
                                array_push($handle_filter, $planId);
                            }
                        }

                        foreach ($items as $item) {
                            if ($item->ProductCountryId === $blade_type->id) {
                                array_push($blade_filter, $planId);
                            }
                        }

                        $match = array_intersect_key($handle_filter, $blade_filter);
                        if (!empty($match)) {
                            array_push($filtered_planIds, $match);
                        }
                    }

                }

                $filtered = Plans::join('plansku', 'plansku.id', 'plans.PlanSkuId')
                    ->join('plan_category_details', 'plan_category_details.PlanSkuId', 'plansku.id')
                // ->leftJoin('plantranslates', 'plantranslates.PlanId', 'plans.id')
                    ->where('plan_category_details.PlanCategoryId', $p_ctgry)
                    ->where('plansku.status', 1)
                    ->where('plans.CountryId', $u->CountryId)
                // ->where('plantranslates.langCode', $u->defaultLanguage)
                    ->whereIn('plansku.duration', $p_drtns)
                    ->whereIn('plans.id', $filtered_planIds)
                    ->select(
                        'plans.id as PlanId',
                        'plansku.id as PlanSkuId',
                        'plan_category_details.id as PlanCategoryId',
                        // 'plantranslates.id as PlanTranslateId',
                        'plans.*',
                        // 'plantranslates.*',
                        'plansku.*')
                    ->get();

                $available_plans = (Object) array(
                    "blade_type" => $blade_type,
                    "filtered_plans" => $filtered_planIds,
                    "active_plans" => $filtered,
                    "blade_types" => $blade_types,
                    "handle_types" => $handle_types,
                    "bag_types" => $bag_types,
                    "addons" => $addons,
                );

                return $available_plans;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    // Function: Retrieve latest subscription and plan details
    public function RetrieveLatestSubsInfo($user_id)
    {
        $data = (Object) array();
        $user = User::where('id', $user_id)->first();
        $data->latest_sub = Subscriptions::where('subscriptions.UserId', $user_id)
            ->join('plans', 'plans.id', 'subscriptions.PlanId')
            ->join('plansku', 'plansku.id', 'plans.PlanSkuId')
            ->select(
                'subscriptions.id as SubscriptionId',
                'plans.id as PlanId',
                'plansku.id as PlanSkuId',
                'plansku.duration as PlanDuration',
                'subscriptions.*')
            ->orderBy('subscriptions.created_at','DESC')
            ->first();

        if (!empty($data->latest_sub)) {
            $data->product_details = PlanDetails::where('PlanId', $data->latest_sub->PlanId)
                ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('producttranslates.langCode', $user->defaultLanguage)
                ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                ->get();

            $product_addons = SubscriptionsProductAddOns::where('subscriptionId', $data->latest_sub->id)
                ->join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('producttranslates.langCode', $user->defaultLanguage)
                ->where(function ($checkCurrentCycleProducts) use ($data) {
                    $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $data->latest_sub->currentCycle + 1);
                })
                ->select('subscriptions_productaddon.*', 'producttranslates.name as name')
                ->get();

            $data->active_addons = '';
            $countaddon=0;
            $active_addons_array = array();
            // push individual product info into items
            foreach ($product_addons as $addon) {
                $in_cycle = false;
                $in_cycle_with_all = false;

                if ($addon->cycle === "all") {
                    $plan_cycles = $addon->cycle;
                } else {
                    $plan_cycles = json_decode($addon->cycle, true);
                    //check if all exists, if exists save to a new variable
                }

                // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                if (is_array($plan_cycles)) {
                    foreach ($plan_cycles as $_p_cycles) {
                        if ($_p_cycles["c"] === $data->latest_sub->currentCycle + 1) {
                            $in_cycle = true;
                            $cycle_no = $_p_cycles["c"];
                            $_qty = $_p_cycles["q"];
                        } else {
                            if ($_p_cycles["c"] === "all") {
                                $in_cycle_with_all = true;
                                $cycle_no = 1;
                                $_qty = $_p_cycles["q"];
                            }
                        }
                    }
                }

                // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                if (!is_array($plan_cycles)) {
                    //if cycle is in numbers / string
                    $_in_cycle = $plan_cycles;
                    if ($addon->cycle === "all") {
                        $in_cycle = true;
                        $cycle_no = "recurring";
                        $_qty = 1;
                    } else {
                        if ($_in_cycle === $data->latest_sub->currentCycle + 1) {
                            $in_cycle = true;
                            $cycle_no = $_in_cycle;
                            $_qty = 1;
                        }
                    }
                }

                // if products are within the currentCycle, add query DB to get Addons
                if ($in_cycle == true) {
                    $in_cycle == false;
                    $active_addons_array[$countaddon] = $addon;

                } else {
                    if ($in_cycle_with_all == true) {
                        $in_cycle_with_all == false;
                        $active_addons_array[$countaddon] = $addon;
                    }
                }
                $countaddon++;
            }
            $data->active_addons =$active_addons_array;
            $data->blade_type = PlanDetails::where('PlanId', $data->latest_sub->PlanId)
                ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                ->join('products', 'products.id', 'productcountries.ProductId')
                ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                ->where('producttranslates.langCode', $user->defaultLanguage)
                // ->where('producttranslates.name', 'like', '%Blade%')
                ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                ->first();

            $data->card_info = Cards::where('id', $data->latest_sub->CardId)->first();
        }
        return $data;
    }

    // Function: Retrieve all subscriptions and plan details
    public function RetrieveAllSubsInfo($user_id)
    {
        $data = array();
        $user = User::where('id', $user_id)->first();
        $subs = Subscriptions::where('subscriptions.UserId', $user_id)
            ->join('plans', 'plans.id', 'subscriptions.PlanId')
            ->join('plansku', 'plansku.id', 'plans.PlanSkuId')
            ->select(
                'subscriptions.id as SubscriptionId',
                'plans.id as PlanId',
                'plansku.id as PlanSkuId',
                'plansku.duration as PlanDuration',
                'subscriptions.*')
            ->orderBy('subscriptions.created_at','DESC')
            ->get();

        if (!empty($subs)) {
            unset($subs[0]);
            if (count($subs) > 0) {
                foreach ($subs as $sub) {
                    $temp = (Object) array();
                    $temp->subscription = $sub;
                    $temp->product_details = PlanDetails::where('PlanId', $sub->PlanId)
                        ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                        ->join('products', 'products.id', 'productcountries.ProductId')
                        ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                        ->where('producttranslates.langCode', $user->defaultLanguage)
                        ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                        ->get();

                    $product_addons = SubscriptionsProductAddOns::where('subscriptionId', $sub->id)
                        ->join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                        ->join('products', 'products.id', 'productcountries.ProductId')
                        ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                        ->where('producttranslates.langCode', $user->defaultLanguage)
                        ->where(function ($checkCurrentCycleProducts) use ($sub) {
                            $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $sub->currentCycle + 1);
                        })
                        ->select('subscriptions_productaddon.*', 'producttranslates.name as name')
                        ->get();

                    $temp->active_addons = '';
                    $countaddon=0;
                    $active_addons_array = array();
                    // push individual product info into items
                    foreach ($product_addons as $addon) {
                        $in_cycle = false;
                        $in_cycle_with_all = false;

                        if ($addon->cycle === "all") {
                            $plan_cycles = $addon->cycle;
                        } else {
                            $plan_cycles = json_decode($addon->cycle, true);
                            //check if all exists, if exists save to a new variable
                        }

                        // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                        if (is_array($plan_cycles)) {
                            foreach ($plan_cycles as $_p_cycles) {
                                if ($_p_cycles["c"] === $sub->currentCycle + 1) {
                                    $in_cycle = true;
                                    $cycle_no = $_p_cycles["c"];
                                    $_qty = $_p_cycles["q"];
                                } else {
                                    if ($_p_cycles["c"] === "all") {
                                        $in_cycle_with_all = true;
                                        $cycle_no = 1;
                                        $_qty = $_p_cycles["q"];
                                    }
                                }
                            }
                        }

                        // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                        if (!is_array($plan_cycles)) {
                            //if cycle is in numbers / string
                            $_in_cycle = $plan_cycles;
                            if ($addon->cycle === "all") {
                                $in_cycle = true;
                                $cycle_no = "recurring";
                                $_qty = 1;
                            } else {
                                if ($_in_cycle === $sub->currentCycle + 1) {
                                    $in_cycle = true;
                                    $cycle_no = $_in_cycle;
                                    $_qty = 1;
                                }
                            }
                        }

                        // if products are within the currentCycle, add query DB to get Addons
                        if ($in_cycle == true) {
                            $in_cycle == false;
                            $active_addons_array[$countaddon] = $addon;
                        } else {
                            if ($in_cycle_with_all == true) {
                                $in_cycle_with_all == false;
                                $active_addons_array[$countaddon] = $addon;
                            }
                        }
                    }
                    $countaddon++;
                    $temp->active_addons =$active_addons_array;

                    $temp->blade_type = PlanDetails::where('PlanId', $sub->PlanId)
                        ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                        ->join('products', 'products.id', 'productcountries.ProductId')
                        ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                        ->where('producttranslates.langCode', $user->defaultLanguage)
                        // ->where('producttranslates.name', 'like', '%Blade%')
                        ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                        ->first();

                    $temp->card_info = Cards::where('id', $sub->CardId)->first();
                    array_push($data, $temp);
                }
            }
        }
        return $data;
    }

      // Function: Retrieve all subscriptions and plan details
      public function RetrieveAllSubsInfoV2($user_id)
      {
          $data = array();
          $user = User::where('id', $user_id)->first();
          $subs = Subscriptions::where('subscriptions.UserId', $user_id)
              ->join('plans', 'plans.id', 'subscriptions.PlanId')
              ->join('plansku', 'plansku.id', 'plans.PlanSkuId')
              ->whereNotIn('subscriptions.status', ['Cancelled','Payment Failure'])
              ->select(
                  'subscriptions.id as SubscriptionId',
                  'plans.id as PlanId',
                  'plansku.id as PlanSkuId',
                  'plansku.duration as PlanDuration',
                  'subscriptions.*')
              ->orderBy('subscriptions.created_at')
              ->get();

          if (!empty($subs)) {
              if (count($subs) > 0) {
                  foreach ($subs as $sub) {
                      $temp = (Object) array();
                      $temp->subscription = $sub;
                      $temp->product_details = PlanDetails::where('PlanId', $sub->PlanId)
                          ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                          ->join('products', 'products.id', 'productcountries.ProductId')
                          ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                          ->where('producttranslates.langCode', $user->defaultLanguage)
                          ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                          ->get();

                      $product_addons = SubscriptionsProductAddOns::where('subscriptionId', $sub->id)
                          ->join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                          ->join('products', 'products.id', 'productcountries.ProductId')
                          ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                          ->where('producttranslates.langCode', $user->defaultLanguage)
                          ->where(function ($checkCurrentCycleProducts) use ($sub) {
                              $checkCurrentCycleProducts->orWhere('subscriptions_productaddon.cycle', 'all')->orWhere('subscriptions_productaddon.cycle', $sub->currentCycle + 1);
                          })
                          ->select('subscriptions_productaddon.*', 'producttranslates.name as name')
                          ->get();

                      $temp->active_addons = '';
                      $countaddon=0;
                      $active_addons_array = array();
                      // push individual product info into items
                      foreach ($product_addons as $addon) {
                          $in_cycle = false;
                          $in_cycle_with_all = false;

                          if ($addon->cycle === "all") {
                              $plan_cycles = $addon->cycle;
                          } else {
                              $plan_cycles = json_decode($addon->cycle, true);
                              //check if all exists, if exists save to a new variable
                          }

                          // in_cycle is an array, therefore we need to determine the cycle & quantity accordingly.
                          if (is_array($plan_cycles)) {
                              foreach ($plan_cycles as $_p_cycles) {
                                  if ($_p_cycles["c"] === $sub->currentCycle + 1) {
                                      $in_cycle = true;
                                      $cycle_no = $_p_cycles["c"];
                                      $_qty = $_p_cycles["q"];
                                  } else {
                                      if ($_p_cycles["c"] === "all") {
                                          $in_cycle_with_all = true;
                                          $cycle_no = 1;
                                          $_qty = $_p_cycles["q"];
                                      }
                                  }
                              }
                          }

                          // set $in_cycle to yes if value in cycle is the same as the subscriptions currentCycle
                          if (!is_array($plan_cycles)) {
                              //if cycle is in numbers / string
                              $_in_cycle = $plan_cycles;
                              if ($addon->cycle === "all") {
                                  $in_cycle = true;
                                  $cycle_no = "recurring";
                                  $_qty = 1;
                              } else {
                                  if ($_in_cycle === $sub->currentCycle + 1) {
                                      $in_cycle = true;
                                      $cycle_no = $_in_cycle;
                                      $_qty = 1;
                                  }
                              }
                          }

                          // if products are within the currentCycle, add query DB to get Addons
                          if ($in_cycle == true) {
                              $in_cycle == false;
                              $active_addons_array[$countaddon] = $addon;
                          } else {
                              if ($in_cycle_with_all == true) {
                                  $in_cycle_with_all == false;
                                  $active_addons_array[$countaddon] = $addon;
                              }
                          }
                      }
                      $countaddon++;
                      $temp->active_addons =$active_addons_array;

                      $temp->blade_type = PlanDetails::where('PlanId', $sub->PlanId)
                          ->join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                          ->join('products', 'products.id', 'productcountries.ProductId')
                          ->join('producttranslates', 'producttranslates.ProductId', 'productcountries.ProductId')
                          ->where('producttranslates.langCode', $user->defaultLanguage)
                        //   ->where('producttranslates.name', 'like', '%Blade%')
                          ->select('productcountries.*', 'products.*', 'producttranslates.name as name')
                          ->first();

                      $temp->card_info = Cards::where('id', $sub->CardId)->first();
                      array_push($data, $temp);
                  }
              }
          }
          return $data;
      }

      
    public function getProductAddonsbySubscription($subid, $countryid) {
        $productAddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
            ->join('products', 'products.id', 'productcountries.ProductId')
            ->select('products.sku as sku')
            ->where('subscriptions_productaddon.subscriptionId', $subid)
            ->where('productcountries.CountryId', $countryid)
            ->get();
            
        return $productAddons;
    }

    public function campaignGetSubscription($email,$subid)
    {
        $subscriptions = Subscriptions::leftJoin('users', 'users.id', 'subscriptions.UserId')
        ->leftJoin('countries', 'countries.id', 'users.CountryId')
        ->select('subscriptions.*', 'subscriptions.id as subscriptionsid', 'users.id as userid', 'users.CountryId', 'users.defaultLanguage as udefaultLanguage', 'countries.currencyDisplay', 'countries.codeIso')
        ->where('subscriptions.id', $subid)
        ->where('subscriptions.email',$email)
        ->where(function ($query) {
            $query->orWhere('subscriptions.status',  'Cancelled')
                ->orWhere('subscriptions.status', 'Unrealized');
        })
        ->limit(1)
        ->first();
  
        return $subscriptions;
    }

    public function campaignGetSubscriptionAddonCombine($subid)
    {
        $subscriptions = SubscriptionsProductAddOns::select(DB::raw("group_concat(CONCAT(ProductCountryId,'@',qty,'@',cycle)) AS addon"))
        ->where('subscriptionId', $subid)
        ->groupBy('subscriptionId')
        ->first();
  
        return $subscriptions;
    }

    public function campaignGetSubscriptionUnrealizedDate($subid)
    {
        $subscriptions = SubscriptionHistories::where('message', 'LIKE', '%Unrealized%')
        ->where('subscriptionId', $subid)
        ->orderBy('created_at','DESC')
        ->first();
  
        return $subscriptions;
    }

    public function campaignCheckSubscription($email,$subid,$countryid)
    {
        $subscriptions = Subscriptions::leftJoin('users', 'users.id', 'subscriptions.UserId')
        ->leftJoin('countries', 'countries.id', 'users.CountryId')
        ->select('subscriptions.*', 'subscriptions.id as subscriptionsid', 'users.id as userid', 'users.CountryId', 'users.defaultLanguage', 'countries.currencyDisplay')
        ->where('subscriptions.id', $subid)
        ->where('subscriptions.email',$email)
        ->where('users.CountryId', $countryid)
        ->where(function ($query) {
            $query->orWhere('subscriptions.status',  'Cancelled')
                ->orWhere('subscriptions.status', 'Unrealized');
        })
        ->limit(1)
        ->first();
  
        return $subscriptions;
    }

    public function campaignGetSubscriptionAddon($email,$subid)
    {
        $productAddons = SubscriptionsProductAddOns::select('*')
        ->where('subscriptionId', $subid)
        ->get();
        
         return $productAddons;
    }

    public function campaignUpdateSubscription($email,$subid,$country_id,$planid,$totalplanprice,$totalprice,$productaddon,$currentCycle)
    {
        $nextdate=  Carbon::now()->addDays(1);
        if($email == "vietsoon@dci-digital.com"){
            Subscriptions::where('id', $subid)->where('email', $email)->update(
                ['nextDeliverDate' => $nextdate,'nextChargeDate' => $nextdate,'recharge_date' => null,'total_recharge' => 0
                ,'unrealizedCust' => 0,'status' => 'Unrealized','PlanId' => $planid,'pricePerCharge' =>$totalprice, 'price'=>$totalplanprice, 'isCustom'=>0, 'isTrial'=>1]);
        }
        else{
        Subscriptions::where('id', $subid)->where('email', $email)->update(
        ['nextDeliverDate' => $nextdate,'nextChargeDate' => $nextdate,'recharge_date' => null,'total_recharge' => 0
        ,'unrealizedCust' => 0,'status' => 'Processing','PlanId' => $planid,'pricePerCharge' =>$totalprice, 'price'=>$totalplanprice, 'isCustom'=>0, 'isTrial'=>1]);
        }
        
        $SubscriptionHistories = new SubscriptionHistories();
        $SubscriptionHistories->message = "Processing";
        $SubscriptionHistories->detail = "Reactive(Welcome Back Promo)";
        $SubscriptionHistories->subscriptionId = $subid;
        $SubscriptionHistories->created_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $SubscriptionHistories->updated_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
        $SubscriptionHistories->save();

        SubscriptionsProductAddOns::where('subscriptionId', $subid)->delete();
        $aftershavecrem = config('global.' . config('app.appType') . '.AfterShaveCream');
        $freeaftershavecrem = $this->productHelper->getProductCountriesBySKU($aftershavecrem,$country_id);
        if(isset($freeaftershavecrem)){
            $e = [
                "subscriptionId" => $subid,
                "ProductCountryId" =>  $freeaftershavecrem->pcid,
                "qty" => 1,
                "cycle" => 1 + (int) $currentCycle,
                "skipFirstCycle" => 1,
                "isAnnual" => 1,
                "isWithTrialKit" => 0,
            ];

            SubscriptionsProductAddOns::insert($e);
        }
        $splitproductaddon = explode(",", $productaddon);
        if($productaddon == "" || $productaddon == null){
        }else{
        $totalproductaddon =  count($splitproductaddon);
  
        foreach ($splitproductaddon as $productaddonidcycle) {
          
            $splitproductaddonidcycle = explode("@", $productaddonidcycle);
           
            if($splitproductaddonidcycle[1] == "all"){
                $cycle = "all";
            }else{
                $cycle = (int) $splitproductaddonidcycle[1] + (int) $currentCycle;
            }

            $e = [
                "subscriptionId" => $subid,
                "ProductCountryId" =>  $splitproductaddonidcycle[0],
                "qty" => 1,
                "cycle" => $cycle,
                "skipFirstCycle" => 1,
                "isAnnual" => 1,
                "isWithTrialKit" => 0,
            ];

            SubscriptionsProductAddOns::insert($e);
          
         }
        }
         return 'success' ;
    }
}
