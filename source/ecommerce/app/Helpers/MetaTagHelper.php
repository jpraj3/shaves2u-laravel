<?php

namespace App\Helpers;

use App;
use App\Models\Ecommerce\MetaTags;

class MetaTagHelper
{
    public static function getMetaTag($options)
    {
        if (config('app.env') === 'staging') {
            $options->pageUrl = str_replace("/shaves2u/ecommerce", "", $options->pageUrl);
        }

        $getMetaTag = MetaTags::where('CountryId', $options->CountryId)
            ->where('langCode', $options->langCode)
            ->where('pageUrl', $options->pageUrl)
            ->first();

        if ($getMetaTag === null) {
            $getMetaTag = MetaTags::where('CountryId', 1)
                ->where('langCode', 'EN')
                ->where('pageUrl', $options->pageUrl)
                ->first();
        }
        return $getMetaTag;
    }

}
