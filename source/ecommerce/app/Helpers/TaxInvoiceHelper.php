<?php

namespace App\Helpers;

use App\Helpers\AWSHelper;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Ecommerce\User;
use App\Models\FileUploads\FileUploads;
use App\Models\GeoLocation\Countries;
use App\Models\GeoLocation\LanguageDetails;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\Orders;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductImage;
use App\Models\Products\ProductTranslate;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Models\Promotions\Promotions;
use App\Services\OrderService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PDF;
use Zip;

class TaxInvoiceHelper
{
    public function __construct()
    {
        $this->awsHelper = new AWSHelper();
        $this->folderPath = config('environment.aws.settings.filesUploadAlbum');
        $this->taxInvoiceFolderPath = config('environment.aws.settings.filesUploadAlbum');
        $this->taxInvoice_path = config('environment.taxInvoice.taxInvoicePath');
        $this->orderService = new OrderService();
        $this->Log = \Log::channel('cronjob');
    }

    public function __listFiles()
    {
        $files = array_diff(scandir($this->taxInvoice_path), array('.', '..'));
        return $files;
    }

    public function __upload($file_id)
    {

        \Log::channel('cronjob')->info(' __upload start');
        try {
            $currentFile = FileUploads::where('file_id', $file_id)->select('name')->first();
            $pdfs = $this->__listFiles();
            if ($pdfs) {
                foreach ($pdfs as $pdf) {
                    $filename = pathinfo($pdf, PATHINFO_FILENAME) . '.' . pathinfo($pdf, PATHINFO_EXTENSION);
                    if ($currentFile->name === $filename) {
                        $ext = pathinfo($pdf, PATHINFO_EXTENSION);
                        $_file = $filename;
                        $source_path = $this->taxInvoice_path . '/' . $filename;
                        \Log::channel('cronjob')->info(' __putPDF start');
                        try {
                            $upload = $this->awsHelper->__putPDF($_file, $source_path, $this->folderPath, $filename, 'public');
                            \Log::channel('cronjob')->info(' __putPDF start - upload success ' . json_encode($upload));
                            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.filesUploadAlbum') . '/' . $filename;
                            // update fileuploads entry
                            FileUploads::where('file_id', $file_id)->update([
                                'url' => $url,
                                'status' => 'Completed',
                                'isDownloaded' => 1,
                            ]);

                            return 1;
                        } catch (Exception $e) {
                            \Log::channel('cronjob')->error(' __putPDF upload start error ' . $e);
                            return 0;
                        }
                    }
                }
            }
        } catch (Exception $e) {
            \Log::channel('cronjob')->info(' __putPDF start' . json_encode($e));
            return $e;
        }
    }

    public function __generatePDF($template, $filename, $file_id)
    {
        try {
            PDF::setOptions([
                'dpi' => 96,
                'default_media_type' => 'print',
                'defaultFont' => 'Muli',
                'fontDir' => asset('fonts/pdf-fonts'),
                'isRemoteEnabled' => true,
                'isPhpEnabled' => true,
                'isJavascriptEnabled' => true,
                'isHtml5ParserEnabled' => true,
                'isFontSubsettingEnabled' => true,
            ]);
            \Log::channel('cronjob')->info(' __generatePDF 1');
            // return PDF::loadFile(public_path() . '/myfile.html')->save('/path-to/my_stored_file.pdf')->stream('download.pdf');
            // PDF::loadHTML($template)->setPaper('a4', 'potrait')->setWarnings(false)->save($this->taxInvoice_path . '/' . $filename . '.pdf');
            // $template = mb_convert_encoding($template,'UTF-8');
            PDF::loadHTML($template)->setPaper('a3', 'potrait')->setWarnings(false)->save($this->taxInvoice_path . '/' . $filename);
            \Log::channel('cronjob')->info(' __generatePDF 2');
            $this->__upload($file_id); //Uncomment this to upload to AWS

            return 1;
        } catch (Exception $e) {
            \Log::channel('cronjob')->error(' __generatePDF 2' . json_encode($e->getMessage()));
            return 0;
        }
    }

    public function __generateHTML($order_data, $isBulk)
    {
        \Log::channel('cronjob')->info('This $content is ' . json_encode($order_data));
        $order_data = json_decode($order_data);

        if ($isBulk === false) {

            $order = Orders::where('id', $order_data->id)->first();
            $country = Countries::where('id', $order->CountryId)->first();
            $receipt = Receipts::where('OrderId', $order->id)->first();
            $subscription = $order->subscriptionIds !== null ? Subscriptions::where('id', $order->subscriptionIds)->first() : null;
            $user = User::where('id', $order->UserId)->first();

            if ($user) {
                \App::setLocale(strtolower($user->defaultLanguage));
            }

            // Subscription && Trial Plan
            if ($subscription !== null && $subscription->isTrial === 1 && $subscription->isCustom === 0) {
                $data = (object) array(
                    'email' => $order->email,
                    'subscriptionId' => $order->subscriptionIds,
                    'orderId' => $order->id,
                    'receiptId' => $receipt->id,
                    'userId' => $order->UserId,
                    'countryId' => $order->CountryId,
                    'isPlan' => 1,
                );
            }

            // Subscription && Custom Plan
            if ($subscription !== null && $subscription->isTrial === 0 && $subscription->isCustom === 1) {
                $data = (object) array(
                    'email' => $order->email,
                    'subscriptionId' => $order->subscriptionIds,
                    'orderId' => $order->id,
                    'receiptId' => $receipt->id,
                    'userId' => $order->UserId,
                    'countryId' => $order->CountryId,
                    'isPlan' => 1,
                );
            }

            // Ala Carte Products
            if ($subscription === null) {
                $data = (object) array(
                    'email' => $order->email,
                    'subscriptionId' => $order->subscriptionIds,
                    'orderId' => $order->id,
                    'receiptId' => $receipt->id,
                    'userId' => $order->UserId,
                    'countryId' => $order->CountryId,
                    'isPlan' => 0,
                );
            }

            $filename = 'TaxInvoice_' . $this->orderService->formatOrderNumber($order, $country, false) . '_' . Carbon::now()->format('Y-m-d_hisv') . '.pdf';

            $file_id = Str::uuid()->toString();
            $upload = null; //FileUploads::where('orderId', (int) $order_data->id)->first();

            if ($upload == null) {
                //Create new fileUpload entry
                FileUploads::updateOrCreate([
                    'orderId' => (int) $order_data->id
                ], [
                    'name' => $filename,
                    'isReport' => 0,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                ]);
            }
            $template = $this->taxInvoiceTemplate($data, $country);
            return $this->__generatePDF($template, $filename, $file_id);
        } else {

            $bulkorder = BulkOrders::where('id', $order_data->id)->first();
            $country = Countries::where('id', $bulkorder->CountryId)->first();
            $data = (object) array(
                'email' => $bulkorder->email,
                'orderId' => $bulkorder->id,
                'countryId' => $bulkorder->CountryId,
            );
            if ($country) {
                $languageDetailsData = LanguageDetails::where('CountryId', $country->id)
                ->where('sublanguageCode', $country->defaultLang)
                ->first();
                if ($languageDetailsData) {
                \App::setLocale(strtolower($languageDetailsData->sublanguageCode));
                }
            }
            $filename = 'TaxInvoice_' . $this->orderService->formatBulkOrderNumber($bulkorder, $country, false) . '_' . Carbon::now()->format('Y-m-d_hisv') . '.pdf';

            $file_id = Str::uuid()->toString();
            $upload = null;

            if ($upload == null) {
                //Create new fileUpload entry
                FileUploads::updateOrCreate([
                    'bulkOrderId' => (int) $order_data->id
                ], [
                    'name' => $filename,
                    'isReport' => 0,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                ]);
            }
            $template = $this->taxInvoiceTemplateBulkOrder($data, $country);
            return $this->__generatePDF($template, $filename, $file_id);
        }
    }

    public function __RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    public function taxInvoiceTemplate($moduleData, $country)
    {
        \Log::channel('cronjob')->info('taxInvoiceTemplate ' . json_encode($moduleData));
        // Initialize helper
        $this->moduleData = $moduleData;

        $user = User::where('id', $this->moduleData->userId)->first();
        // Initialize email contents
        $content = (object) array();
        $content->order = json_decode(Orders::where('id', $this->moduleData->orderId)->first()->toJson());
        $content->orderDetails = json_decode(OrderDetails::where('OrderId', $this->moduleData->orderId)->get()->toJson());
        if ($this->moduleData->isPlan == 1) {
            $content->subscription = json_decode(Subscriptions::where('id', $this->moduleData->subscriptionId)->first()->toJson());
        }
        $content->receipt = json_decode(Receipts::where('id', $this->moduleData->receiptId)->first()->toJson());
        $content->user = json_decode(User::where('id', $this->moduleData->userId)->first()->toJson());
        $content->country = json_decode(Countries::where('id', $this->moduleData->countryId)->first()->toJson());
        $content->order->orderNo = $this->orderService->formatOrderNumber($content->order, $content->country, true);
        \Log::channel('cronjob')->info('$content->country');
        \Log::channel('cronjob')->info(json_encode($content->country));
        \Log::channel('cronjob')->info('End $content->country');
        if ($this->moduleData->isPlan == 1) {
            $content->subscription->isOnline = $content->subscription->isOffline === 1 ? 0 : 1;
            $content->subscription->appType = $content->subscription->isOffline === 1 ? 'baWebsite' : 'ecommerce';
        }
        \Log::channel('cronjob')->info('1 ' . json_encode($content));

        $content->promotion = (object) array();
        $content->promotion->id = null;
        $content->promotion->promoCode = "";

        $content->referral = (object) array();
        $content->referral->referral_rewards = 0.00;

        $content->links = (object) array();
        $content->links->orderDetailsLink = "";
        $content->links->trackOrderLink = "";

        $content->identifiers = (object) array();
        $content->identifiers->isOldUser = false;
        $content->identifiers->isActiveUser = false;
        $content->identifiers->isNewUser = false;
        $content->identifiers->isEcommerce = false;
        $content->identifiers->checkTaxInvoice = false;
        $content->identifiers->isBaSale = false;
        $content->identifiers->isBaSubSaleMys = false;
        $content->identifiers->isTaxInvoice = false;
        $content->identifiers->orderConfirmed = false;
        $content->identifiers->orderDelivering = false;
        $content->identifiers->orderCompleted = false;
        $content->identifiers->isTrial = false;
        $content->identifiers->NoGstCheck = false;
        $content->identifiers->isOffline = false;

        $content->email = (object) array();
        $content->email->title = "";
        $content->email->subject = "";
        $content->email->ccList = [];
        $content->email->local_carrier = "";
        $content->email->link_mailto_title = "";
        $content->email->link_mailto = "";
        $content->email->template = "";
        $content->deliveryAddress = null;
        $checkDeliveryAddresses = DeliveryAddresses::where('id', $content->order->DeliveryAddressId)->first();
        if (isset($checkDeliveryAddresses)) {
            $checkDeliveryAddresses = $checkDeliveryAddresses->toJson();
            $content->deliveryAddress = json_decode($checkDeliveryAddresses);
        }
        $content->billingAddress = null;
        $checkBillingAddresses = DeliveryAddresses::where('id', $content->order->BillingAddressId)->first();
        if (isset($checkBillingAddresses)) {
            $checkBillingAddresses = $checkBillingAddresses->toJson();
            $content->billingAddress = json_decode($checkBillingAddresses);
        }
        // Initialize config data
        $userCountryCodeISO = null;
        $checkuserCountryCodeISO = Countries::where('id', $content->user->CountryId)->first();
        if (isset($checkuserCountryCodeISO)) {
            $checkuserCountryCodeISO = $checkuserCountryCodeISO->toJson();
            $userCountryCodeISO = strtolower(json_decode($checkuserCountryCodeISO)->codeIso);
        }

        $config_webUrl = config('environment.webUrl') . $content->user->defaultLanguage . "-" . $userCountryCodeISO . "/";
        $config_apiToken = csrf_token(); // Old site use like this: jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)
        $config_ccList = [];
        $config_urbanFox_TrackingURL = config('environment.urbanFox.trackingUrl');
        $config_afterShip_TrackingURL = config('environment.aftership.trackingUrl');
        $config_deliverDateForKorea = config('environment.deliverDateForKorea');

        // Initialize email text assets
        $order_confirmed_subject = Lang::get('email.subject.receipt-order.order_confirmed');
        $order_delivering_subject = Lang::get('email.subject.receipt-order.order_delivering');
        $order_completed_subject = Lang::get('email.subject.receipt-order.order_completed');
        $order_tax_invoice_subject = Lang::get('email.subject.receipt-order.order_tax_invoice');
        $pre_order_confirmed_kr_subject = Lang::get('email.subject.receipt-order.pre_order_confirmed_kr');

        $order_confirmed_mys_title = Lang::get('email.title.receipt-order.order_confirmed_mys');
        $order_confirmed_title = Lang::get('email.title.receipt-order.order_confirmed');
        $order_delivering_title = Lang::get('email.title.receipt-order.order_delivering');
        $order_completed_title = Lang::get('email.title.receipt-order.order_completed');
        $pre_order_confirmed_kr_title = Lang::get('email.title.receipt-order.pre_order_confirmed_kr');

        // Set promotion if any
        if ($content->order->PromotionId) {
            $content->promotion = null;
            $checkpromotion = Promotions::where('id', $content->order->PromotionId)->first();
            if (isset($checkpromotion)) {
                $checkpromotion = $checkpromotion->toJson();
                $content->promotion = json_decode($checkpromotion);
            }
            $content->promotion->promoCode = $content->order->promoCode;
        }

        // Set link to order detail page
        if ($content->user->isActive) {
            $content->links->orderDetailsLink = $config_webUrl . 'user/orders/' . $content->order->id . '?utm_source=mandrill&utm_medium=email&utm_campaign=_' . strtolower($content->country->code) . '_vieworder&utm_content=S2U_html_vieworder&utm_term=_vieworder_web';
            $content->identifiers->isOldUser = true;
        } else if (!$content->user->isGuest) {
            $content->links->orderDetailsLink = $config_webUrl . 'login/' . $config_apiToken . '?nextPage=/user/orders/' . $content->order->id . '&utm_source=mandrill&utm_medium=email&utm_campaign=_' . strtolower($content->country->code) . '_activateaccount&utm_content=S2U_html_activateaccount&utm_term=_newusersignup_web';
            $content->identifiers->isActiveUser = true;
        } else {
            $content->links->orderDetailsLink = $config_webUrl . 'users/reset-password?email=' . $content->user->email . '&token=' . $config_apiToken . '&isActive=true&nextPage=/user/orders/' . $content->order->id . '&utm_source=mandrill&utm_medium=email&utm_campaign=_' . strtolower($content->country->code) . '_passwordupdate&utm_content=S2U_html_passwordupdate&utm_term=_passwordupdate_web';
            $content->identifiers->isNewUser = true;
        }

        // Check if is ecommerce order
        if ($content->country->isWebEcommerce) {
            $content->identifiers->isEcommerce = true;
        } else {
            $content->links->orderDetailsLink = $config_webUrl;
        }

        // Enable text invoice checking for some countries
        $content->identifiers->checkTaxInvoice = in_array($content->country->code, ['MYS', 'SGP']);
        \Log::channel('cronjob')->info('$content->order->status is ' . json_encode($content->order->status));
        // Check order status
        if ($content->order->status == "Payment Received" || $content->order->status == "Processing" || $content->order->status == "Delivering" || $content->order->status == "Completed" || $content->order->status == "Cancelled" || $content->order->status == "Returned") {
            //$upload = FileUploads::where('orderId', $content->order->id)->first();

            if ($this->moduleData->countryId != 8) {
                \Log::channel('cronjob')->info('Processing case');
                $content->identifiers->orderConfirmed = true;

                // Check tax invoice
                if ($content->identifiers->checkTaxInvoice) {
                    $content->email->title = $order_confirmed_mys_title;
                    $content->identifiers->isTaxInvoice = true;
                    $content->email->ccList = $config_ccList;
                    if ($content->order->SellerUserId) {
                        $content->identifiers->isBaSale = true;
                        $content->identifiers->isBaSubSaleMys = true;
                        $content->links->orderDetailsLink = $config_webUrl;
                    }
                } else {
                    $content->email->title = $order_confirmed_title;
                    $content->identifiers->isTaxInvoice = true;
                }

                // Check subscription
                if ($content->order->subscriptionIds) {
                    if ($content->identifiers->checkTaxInvoice) {
                        $content->email->subject = $order_tax_invoice_subject;
                    } else {
                        $content->email->subject = $order_confirmed_subject;
                    }
                } else {
                    $content->email->subject = $order_confirmed_subject;
                }
                $content->email->template = 'email-templates.receipt-order-confirmed-tax-invoice.template';

                // Get order details
                foreach ($content->orderDetails as $detail) {
                    $detail->trialPrice = null;
                    $detail->NoGstCheck = false;
        
                    if ($detail->PlanId) {
                        $content->identifiers->isTrial = true;
                        $detail->trialPrice = $content->receipt->originPrice;
                    }

                    $detail->taxCode = $content->country->taxCode;
                    $detail->taxRate = $content->country->taxRate;
                    $detail->taxName = $content->country->taxName;

                    if (in_array($content->order->CountryId, [1, 2])) {
                        $detail->NoGstCheck = true;
                    }
                    $content->identifiers->NoGstCheck = $detail->NoGstCheck;

                    $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                    $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);

                    if (isset($content->country->includedTaxToProduct) && $content->country->includedTaxToProduct == 1) {
                        $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                        $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($detail->price - $detail->priceExcludeTax), 2);
                    }
                }

                $content->receipt->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                if (isset($content->country->includedTaxToProduct) && $content->country->includedTaxToProduct == 1) {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->totalPrice * ($content->country->taxRate / 100)), 2);
                } else {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                }

                // Cash rebate calculations
                $content->receipt->cashRebate = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                if ($content->promotion->promoCode === "referral") {
                    $content->receipt->cashRebate = $content->receipt->discountAmount;
                    $content->receipt->discountAmount = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                }

                $content->receipt->priceAfterDiscount = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount), 2);
                $content->receipt->priceAfterDiscountAndRebate = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount - $content->receipt->cashRebate), 2);

                $content->identifiers->isOffline = !!$content->order->SellerUserId;

                if ($this->moduleData->isPlan == 1) {
                    $content->order->trialShippingDate = $content->subscription->startDeliverDate;
                }

                $content->links->referralEarnXX = $config_webUrl . 'user/referrals';
                $content->links->viewOrder = $config_webUrl . 'user/orders/' . $content->order->id;
                $content->links->viewProfile = $config_webUrl . 'user/settings';

                // Set carrier and mailto settings
                switch ($content->country->code) {
                    case 'SGP': {
                            $content->email->local_carrier = "GDEX";
                            $content->email->link_mailto_title = "help.sg@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.sg@shaves2u.com";
                        }
                        break;
                    case 'KOR': {
                            $content->email->local_carrier = "LOTTE";
                            $content->email->link_mailto_title = "help.kr@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.kr@shaves2u.com";
                        }
                        break;
                    case 'HKG': {
                            $content->email->local_carrier = "TAQBIN-HK";
                            $content->email->link_mailto_title = "help.hk@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.hk@shaves2u.com";
                        }
                        break;
                    case 'TWN': {
                            $content->email->local_carrier = "S.F Express";
                            $content->email->link_mailto_title = "help.tw@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.tw@shaves2u.com";
                        }
                        break;

                    default: {
                            $content->email->local_carrier = "GDEX";
                            $content->email->link_mailto_title = "help.my@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.my@shaves2u.com";
                        }
                        break;
                }
                $content->receipt->pricewithouttax = 0;
  
                // Remove decimal number and add comma separation for Korea prices
                if ($content->country->code === "SGP") {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(1.07, 2);
                }
                if ($content->country->code === "KOR") {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(1.10, 2);
                    $content->receipt->pricewithouttax = $content->receipt->priceAfterDiscount/$content->receipt->taxPrice;
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscount - $content->receipt->pricewithouttax, 2);

                    $content->receipt->pricewithouttax = LaravelHelper::ConvertToNDecimalPoints($content->receipt->pricewithouttax , 2);
                    $content->receipt->discountAmount = LaravelHelper::ConvertToNDecimalPoints($content->receipt->discountAmount, 2);
                    $content->receipt->subTotalPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->subTotalPrice, 2);
                    $content->receipt->priceAfterDiscount = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscount, 2);
                    $content->receipt->cashRebate = LaravelHelper::ConvertToNDecimalPoints($content->receipt->cashRebate, 2);
                    $content->receipt->originPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->originPrice, 2);
                    $content->receipt->priceAfterDiscountAndRebate = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscountAndRebate, 2);
                    $content->receipt->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceExcludeTax, 2);
     
                    foreach ($content->orderDetails as $detail) {
                        $detail->price = LaravelHelper::ConvertToNDecimalPoints($detail->price, 2);
                        // $detail->totalPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($detail->totalPrice), 0));
                    }
                    $content->referral->referral_rewards = LaravelHelper::ConvertToNDecimalPoints($content->referral->referral_rewards, 2);
                }

                // Free product
                if ($content->promotion->id) {
                    if ($content->promotion->freeProductCountryIds) {
                        $freeProductList = [];
                        foreach (json_decode($content->promotion->freeProductCountryIds) as $freeProductIdStr) {
                            array_push($freeProductList, (int) $freeProductIdStr);
                        }

                        // Change every order detail price to 0 for matching product country ids
                        foreach ($content->orderDetails as $orderDetail) {
                            if (in_array($orderDetail->ProductCountryId, $freeProductList)) {
                                $orderDetail->price = LaravelHelper::ConvertToNDecimalPoints(0, 2);
                            }
                        }
                    }
                }

                // Build additional order details
                foreach ($content->orderDetails as $orderDetail) {
                    // If is a product
                    if ($orderDetail->ProductCountryId) {
                        $orderDetail->details = (object) array();
                        $productId = json_decode(ProductCountry::where('id', $orderDetail->ProductCountryId)->first()->toJson())->ProductId;
                        $orderDetail->details->translatedName = json_decode(ProductTranslate::where('ProductId', $productId)->where('langCode', $content->user->defaultLanguage)->first()->toJson())->name;
                        $orderDetail->details->sku = json_decode(Product::where('id', $productId)->first()->toJson())->sku;
                        $orderDetail->details->imageUrl = ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson() !== null ? json_decode(ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson())->url : null;
                    }

                    if ($orderDetail->PlanId) {
                        $orderDetail->details = (object) array();
                        $planSkuId = json_decode(Plans::where('id', $orderDetail->PlanId)->first()->toJson())->PlanSkuId;
                        // $orderDetail->details->translatedName = json_decode(PlanTranslates::where('PlanId', $orderDetail->PlanId)->where('langCode', $content->country->defaultLang)->first()->toJson())->name;
                        $orderDetail->details->sku = json_decode(PlanSKU::where('id', $planSkuId)->first()->toJson())->sku;
                        $orderDetail->details->planduration = json_decode(PlanSKU::where('id', $planSkuId)->first()->toJson())->duration;
                        // $orderDetail->details->imageUrl = json_decode(PlanImages::where('PlanId', $orderDetail->PlanId)->where('isDefault', 1)->first()->toJson())->url;
                        $orderDetail->details->imageUrl = 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/524f3020-c707-11e8-8e47-efb823843057.png';

                        $this->productHelper = new ProductHelper();
                        $plandata = (object) array();
                        $plandata->id = $orderDetail->PlanId;
                        $plandata->lang = $content->country->defaultLang;
                        $plantypeget = Lang::get('email.content.common.custom-plan');
                        if ($content->subscription->isTrial == true) {
                            $plantypeget = Lang::get('email.content.common.trial-plan');
                        }

                        $planproduct = $this->productHelper->getProductWithoutImageByPlanID($plandata);
                        $countpp = 0;
                        $productnamet = "";
                        foreach ($planproduct as $pd) {
                            if ($pd->productname) {
                                if ($countpp == 0) {
                                    $productnamet = $productnamet . $pd->productname;
                                } else {
                                    $productnamet = $productnamet . ", " . $pd->productname;
                                }
                            }
                            $countpp++;
                        }

                        $orderDetail->details->translatedName = Lang::get('email.content.common.plan-detail', ['shaveplan' => $plantypeget, 'product' => $productnamet, 'duration' => $orderDetail->details->planduration]);
                    }
                    $orderDetail->details->totalproductprice = $orderDetail->price * (int)$orderDetail->qty;
                    if($orderDetail->details->totalproductprice){
                        if ($content->country->code === "KOR") {
                            $orderDetail->details->totalproductprice = number_format(LaravelHelper::ConvertToNDecimalPoints(($orderDetail->details->totalproductprice), 0));
                        }else{
                            $orderDetail->details->totalproductprice = LaravelHelper::ConvertToNDecimalPoints($orderDetail->details->totalproductprice, 2);
                        }
                 
                    }
                }

                // Run Function to filter gender for Current User
                $this->getWomanProductFilter($content);

                $content->orderdate = Carbon::parse($content->order->created_at)->format('d M Y');
                $content->shippingdate = Carbon::parse($content->order->created_at)->addDay(1)->format('d M Y');

                $codeIso = "";
                if ($country['codeIso']) {
                    $codeIso = strtoupper($country['codeIso']);
                } else {
                    $codeIso = 'MY';
                }
                $lang = "";
                if ($user) {
                    if ($user['defaultLanguage']) {
                        $lang = $user['defaultLanguage'];
                    } else if ($country) {
                        $lang = $country['defaultLang'];
                    }
                } else {
                    if ($country['defaultLang']) {
                        $lang = $country['defaultLang'];
                    }
                }
                // \Log::channel('cronjob')->info('Built Data ==> ' . json_encode($content));
                // App::setLocale('ko');
                $moduleData = $content;
                $countrycode = $codeIso;
                // $template = view($content->email->template, compact('moduleData','country','countrycode','lang'));

                \Log::channel('cronjob')->info('$content->email->template => ' . json_encode($content->email->template));
                $template = view($content->email->template)->with(['moduleData' => $content, 'country' => $country, 'countrycode' => $codeIso, 'lang' => $lang])->render();

                // \Log::channel('cronjob')->info('template ==> ' . json_encode($template));

                $test = mb_convert_encoding($template, 'UTF8');
                // \Log::channel('cronjob')->info('template ==> ' . json_encode($test));

                return $template;
                // return mb_convert_encoding($template,'UTF-8');
            } else if ($this->moduleData->countryId == 8) {
                \Log::channel('cronjob')->info('Processing case kr');
                $content->identifiers->orderConfirmed = true;

                // Check tax invoice
                if ($content->identifiers->checkTaxInvoice) {
                    $content->email->title = $order_confirmed_mys_title;
                    $content->identifiers->isTaxInvoice = true;
                    $content->email->ccList = $config_ccList;
                    if ($content->order->SellerUserId) {
                        $content->identifiers->isBaSale = true;
                        $content->identifiers->isBaSubSaleMys = true;
                        $content->links->orderDetailsLink = $config_webUrl;
                    }
                } else {
                    $content->email->title = $order_confirmed_title;
                    $content->identifiers->isTaxInvoice = true;
                }

                // Check subscription
                if ($content->order->subscriptionIds) {
                    if ($content->identifiers->checkTaxInvoice) {
                        $content->email->subject = $order_tax_invoice_subject;
                    } else {
                        $content->email->subject = $order_confirmed_subject;
                    }
                } else {
                    $content->email->subject = $order_confirmed_subject;
                }
                $content->email->template = 'email-templates.receipt-order-confirmed-tax-invoice.template';

                // Get order details
                foreach ($content->orderDetails as $detail) {
                    $detail->trialPrice = null;
                    $detail->NoGstCheck = false;

                    if ($detail->PlanId) {
                        $content->identifiers->isTrial = true;
                        $detail->trialPrice = $content->receipt->originPrice;
                    }

                    $detail->taxCode = $content->country->taxCode;
                    $detail->taxRate = $content->country->taxRate;
                    $detail->taxName = $content->country->taxName;

                    if (in_array($content->order->CountryId, [1, 2])) {
                        $detail->NoGstCheck = true;
                    }
                    $content->identifiers->NoGstCheck = $detail->NoGstCheck;

                    $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                    $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);

                    if (isset($content->country->includedTaxToProduct) && $content->country->includedTaxToProduct  == 1) {
                        $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                        $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($detail->price - $detail->priceExcludeTax), 2);
                    }
                }

                $content->receipt->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                if (isset($content->country->includedTaxToProduct) && $content->country->includedTaxToProduct == 1) {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->totalPrice * ($content->country->taxRate / 100)), 2);
                } else {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                }

                // Cash rebate calculations
                $content->receipt->cashRebate = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                if ($content->promotion->promoCode === "referral") {
                    $content->receipt->cashRebate = $content->receipt->discountAmount;
                    $content->receipt->discountAmount = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);
                }

                $content->receipt->priceAfterDiscount = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount), 2);
                $content->receipt->priceAfterDiscountAndRebate = LaravelHelper::ConvertToNDecimalPoints(($content->receipt->originPrice - $content->receipt->discountAmount - $content->receipt->cashRebate), 2);

                $content->identifiers->isOffline = !!$content->order->SellerUserId;

                if ($this->moduleData->isPlan == 1) {
                    $content->order->trialShippingDate = $content->subscription->startDeliverDate;
                }

                $content->links->referralEarnXX = $config_webUrl . 'user/referrals';
                $content->links->viewOrder = $config_webUrl . 'user/orders/' . $content->order->id;
                $content->links->viewProfile = $config_webUrl . 'user/settings';

                // Set carrier and mailto settings
                switch ($content->country->code) {
                    case 'SGP': {
                            $content->email->local_carrier = "GDEX";
                            $content->email->link_mailto_title = "help.sg@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.sg@shaves2u.com";
                        }
                        break;
                    case 'KOR': {
                            $content->email->local_carrier = "LOTTE";
                            $content->email->link_mailto_title = "help.kr@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.kr@shaves2u.com";
                        }
                        break;
                    case 'HKG': {
                            $content->email->local_carrier = "TAQBIN-HK";
                            $content->email->link_mailto_title = "help.hk@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.hk@shaves2u.com";
                        }
                        break;
                    case 'TWN': {
                            $content->email->local_carrier = "S.F Express";
                            $content->email->link_mailto_title = "help.tw@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.tw@shaves2u.com";
                        }
                        break;

                    default: {
                            $content->email->local_carrier = "GDEX";
                            $content->email->link_mailto_title = "help.my@shaves2u.com";
                            $content->email->link_mailto = "mailto:help.my@shaves2u.com";
                        }
                        break;
                }
              
                // Remove decimal number and add comma separation for Korea prices
                $content->receipt->pricewithouttax = 0;
                if ($content->country->code === "SGP") {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(1.07, 2);
                }
                if ($content->country->code === "KOR") {
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints(1.10, 2);
                    $content->receipt->pricewithouttax = $content->receipt->priceAfterDiscount/$content->receipt->taxPrice ;
                    $content->receipt->taxPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscount - $content->receipt->pricewithouttax, 2);
                    $content->receipt->pricewithouttax = LaravelHelper::ConvertToNDecimalPoints($content->receipt->pricewithouttax, 2);
                    
                    $content->receipt->discountAmount = LaravelHelper::ConvertToNDecimalPoints($content->receipt->discountAmount, 2);
                    $content->receipt->subTotalPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->subTotalPrice, 2);
                    $content->receipt->priceAfterDiscount = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscount, 2);
                    $content->receipt->cashRebate = LaravelHelper::ConvertToNDecimalPoints($content->receipt->cashRebate, 2);
                    $content->receipt->originPrice = LaravelHelper::ConvertToNDecimalPoints($content->receipt->originPrice, 2);
                    $content->receipt->priceAfterDiscountAndRebate = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceAfterDiscountAndRebate, 2);
                    $content->receipt->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints($content->receipt->priceExcludeTax, 2);
        
                    foreach ($content->orderDetails as $detail) {
                        $detail->price = LaravelHelper::ConvertToNDecimalPoints(($detail->price), 2);
                        // $detail->totalPrice = number_format(LaravelHelper::ConvertToNDecimalPoints(($detail->totalPrice), 0));
                    }
                    $content->referral->referral_rewards = LaravelHelper::ConvertToNDecimalPoints($content->referral->referral_rewards, 2);
                }

                // Free product
                if ($content->promotion->id) {
                    if ($content->promotion->freeProductCountryIds) {
                        $freeProductList = [];
                        foreach (json_decode($content->promotion->freeProductCountryIds) as $freeProductIdStr) {
                            array_push($freeProductList, (int) $freeProductIdStr);
                        }

                        // Change every order detail price to 0 for matching product country ids
                        foreach ($content->orderDetails as $orderDetail) {
                            if (in_array($orderDetail->ProductCountryId, $freeProductList)) {
                                $orderDetail->price = LaravelHelper::ConvertToNDecimalPoints(0, 2);
                            }
                        }
                    }
                }

                // Build additional order details
                foreach ($content->orderDetails as $orderDetail) {
                    // If is a product
                    if ($orderDetail->ProductCountryId) {
                        $orderDetail->details = (object) array();
                        $productId = json_decode(ProductCountry::where('id', $orderDetail->ProductCountryId)->first()->toJson())->ProductId;
                        $orderDetail->details->translatedName = json_decode(ProductTranslate::where('ProductId', $productId)->where('langCode', $content->user->defaultLanguage)->first()->toJson())->name;
                        $orderDetail->details->sku = json_decode(Product::where('id', $productId)->first()->toJson())->sku;
                        $orderDetail->details->imageUrl = ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson() !== null ? json_decode(ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson())->url : null;
                    }

                    if ($orderDetail->PlanId) {
                        $orderDetail->details = (object) array();
                        $planSkuId = json_decode(Plans::where('id', $orderDetail->PlanId)->first()->toJson())->PlanSkuId;
                        // $orderDetail->details->translatedName = json_decode(PlanTranslates::where('PlanId', $orderDetail->PlanId)->where('langCode', $content->country->defaultLang)->first()->toJson())->name;
                        $orderDetail->details->sku = json_decode(PlanSKU::where('id', $planSkuId)->first()->toJson())->sku;
                        $orderDetail->details->planduration = json_decode(PlanSKU::where('id', $planSkuId)->first()->toJson())->duration;
                        // $orderDetail->details->imageUrl = json_decode(PlanImages::where('PlanId', $orderDetail->PlanId)->where('isDefault', 1)->first()->toJson())->url;
                        $orderDetail->details->imageUrl = 'https://shaves2u-dci.s3.ap-southeast-1.amazonaws.com/plan-images/524f3020-c707-11e8-8e47-efb823843057.png';

                        $this->productHelper = new ProductHelper();
                        $plandata = (object) array();
                        $plandata->id = $orderDetail->PlanId;
                        $plandata->lang = $content->country->defaultLang;
                        $plantypeget = Lang::get('email.content.common.custom-plan');
                        if ($content->subscription->isTrial == true) {
                            $plantypeget = Lang::get('email.content.common.trial-plan');
                        }

                        $planproduct = $this->productHelper->getProductWithoutImageByPlanID($plandata);
                        $countpp = 0;
                        $productnamet = "";
                        foreach ($planproduct as $pd) {
                            if ($pd->productname) {
                                if ($countpp == 0) {
                                    $productnamet = $productnamet . $pd->productname;
                                } else {
                                    $productnamet = $productnamet . ", " . $pd->productname;
                                }
                            }
                            $countpp++;
                        }

                        $orderDetail->details->translatedName = Lang::get('email.content.common.plan-detail', ['shaveplan' => $plantypeget, 'product' => $productnamet, 'duration' => $orderDetail->details->planduration]);
                    }
                    $orderDetail->details->totalproductprice = $orderDetail->price * (int)$orderDetail->qty;
                    if($orderDetail->details->totalproductprice){
                        if ($content->country->code === "KOR") {
                            $orderDetail->details->totalproductprice = number_format(LaravelHelper::ConvertToNDecimalPoints(($orderDetail->details->totalproductprice), 0));
                        }else{
                            $orderDetail->details->totalproductprice = LaravelHelper::ConvertToNDecimalPoints($orderDetail->details->totalproductprice, 2);
                        }
                 
                    }
                }

                // Run Function to filter gender for Current User
                $this->getWomanProductFilter($content);

                $content->orderdate = Carbon::parse($content->order->created_at)->format('d M Y');
                $content->shippingdate = Carbon::parse($content->order->created_at)->addDay(1)->format('d M Y');

                $codeIso = "";
                if ($country['codeIso']) {
                    $codeIso = strtoupper($country['codeIso']);
                } else {
                    $codeIso = 'MY';
                }
                $lang = "";
                if ($user) {
                    if ($user['defaultLanguage']) {
                        $lang = $user['defaultLanguage'];
                    } else if ($country) {
                        $lang = $country['defaultLang'];
                    }
                } else {
                    if ($country['defaultLang']) {
                        $lang = $country['defaultLang'];
                    }
                }
                // \Log::channel('cronjob')->info('Built Data ==> ' . json_encode($content));
                // App::setLocale('ko');
                $moduleData = $content;
                $countrycode = $codeIso;
                // $template = view($content->email->template, compact('moduleData','country','countrycode','lang'));

                \Log::channel('cronjob')->info('$content->email->template => ' . json_encode($content->email->template));
                $template = view($content->email->template)->with(['moduleData' => $content, 'country' => $country, 'countrycode' => $codeIso, 'lang' => $lang])->render();

                // \Log::channel('cronjob')->info('template ==> ' . json_encode($template));

                $test = mb_convert_encoding($template, 'UTF8');
                // \Log::channel('cronjob')->info('template ==> ' . json_encode($test));

                return $template;
                // return mb_convert_encoding($template,'UTF-8');
            } else {
                \Log::channel('cronjob')->info('taxInvoiceTemplate cancelled');
            }
        } else {
            \Log::channel('cronjob')->info('taxInvoiceTemplate cancelled');
        }
    }

    public function taxInvoiceTemplateBulkOrder($moduleData, $country)
    {
        \Log::channel('cronjob')->info('taxInvoiceTemplate ' . json_encode($moduleData));
        // Initialize helper
        $this->moduleData = $moduleData;
        // Initialize email contents
        $content = (object) array();

        $content->order = json_decode(BulkOrders::where('id', $this->moduleData->orderId)->first()->toJson());
        // $content->user = null;
        // $user = User::where('id', $content->order->email)->get();
        // if(!empty($user)){
        //     $content->user = $user;
        // }
        $content->orderDetails = json_decode(BulkOrderDetails::where('BulkOrderId', $this->moduleData->orderId)->get()->toJson());
        $content->country = json_decode(Countries::where('id', $this->moduleData->countryId)->first()->toJson());
        $content->order->orderNo = $this->orderService->formatBulkOrderNumber($content->order, $content->country, true);
        \Log::channel('cronjob')->info('$content->country');
        \Log::channel('cronjob')->info(json_encode($content->country));
        \Log::channel('cronjob')->info('End $content->country');
        \Log::channel('cronjob')->info('1 ' . json_encode($content));

        $content->promotion = (object) array();
        $content->promotion->id = null;
        $content->promotion->promoCode = "";

        $content->referral = (object) array();
        $content->referral_rewards = 0.00;

        $content->links = (object) array();
        $content->links->orderDetailsLink = "";
        $content->links->trackOrderLink = "";

        $content->identifiers = (object) array();
        $content->identifiers->isOldUser = false;
        $content->identifiers->isActiveUser = false;
        $content->identifiers->isNewUser = false;
        $content->identifiers->isEcommerce = false;
        $content->identifiers->checkTaxInvoice = false;
        $content->identifiers->isBaSale = false;
        $content->identifiers->isBaSubSaleMys = false;
        $content->identifiers->isTaxInvoice = false;
        $content->identifiers->orderConfirmed = false;
        $content->identifiers->orderDelivering = false;
        $content->identifiers->orderCompleted = false;
        $content->identifiers->isTrial = false;
        $content->identifiers->NoGstCheck = false;
        $content->identifiers->isOffline = false;

        $content->email = (object) array();
        $content->email->title = "";
        $content->email->subject = "";
        $content->email->ccList = [];
        $content->email->local_carrier = "";
        $content->email->link_mailto_title = "";
        $content->email->link_mailto = "";
        $content->email->template = "";

        $content->deliveryAddress = json_decode(DeliveryAddresses::where('id', $content->order->DeliveryAddressId)->first()->toJson());
        $content->billingAddress = json_decode(DeliveryAddresses::where('id', $content->order->BillingAddressId)->first()->toJson());

        // Initialize config data
        $userCountryCodeISO = strtolower(json_decode(Countries::where('id', $content->country->id)->first()->toJson())->codeIso);
        $config_webUrl = '';
        $config_apiToken = csrf_token(); // Old site use like this: jwt.sign({ token: expireAt, email: order.User.email }, config.accessToken.secret)
        $config_ccList = [];
        $config_urbanFox_TrackingURL = config('environment.urbanFox.trackingUrl');
        $config_afterShip_TrackingURL = config('environment.aftership.trackingUrl');
        $config_deliverDateForKorea = config('environment.deliverDateForKorea');

        // Initialize email text assets
        $order_confirmed_subject = Lang::get('email.subject.receipt-order.order_confirmed');
        $order_delivering_subject = Lang::get('email.subject.receipt-order.order_delivering');
        $order_completed_subject = Lang::get('email.subject.receipt-order.order_completed');
        $order_tax_invoice_subject = Lang::get('email.subject.receipt-order.order_tax_invoice');
        $pre_order_confirmed_kr_subject = Lang::get('email.subject.receipt-order.pre_order_confirmed_kr');

        $order_confirmed_mys_title = Lang::get('email.title.receipt-order.order_confirmed_mys');
        $order_confirmed_title = Lang::get('email.title.receipt-order.order_confirmed');
        $order_delivering_title = Lang::get('email.title.receipt-order.order_delivering');
        $order_completed_title = Lang::get('email.title.receipt-order.order_completed');
        $pre_order_confirmed_kr_title = Lang::get('email.title.receipt-order.pre_order_confirmed_kr');

        // Set promotion if any
        // if ($content->order->PromotionId) {
        //     $content->promotion = json_decode(Promotions::where('id', $content->order->PromotionId)->first()->toJson());
        //     $content->promotion->promoCode = $content->order->promoCode;
        // }

        // Set link to order detail page
        $content->links->orderDetailsLink = '';
        $content->identifiers->isNewUser = true;

        // Check if is ecommerce order
        if ($content->country->isWebEcommerce) {
            $content->identifiers->isEcommerce = true;
        } else {
            $content->links->orderDetailsLink = $config_webUrl;
        }

        // Enable text invoice checking for some countries
        $content->identifiers->checkTaxInvoice = in_array($content->country->code, ['MYS', 'SGP']);
        \Log::channel('cronjob')->info('$content->order->status is ' . json_encode($content->order->status));
        // Check order status
        if ($content->order->status == "Payment Received" || $content->order->status == "Processing" || $content->order->status == "Delivering" || $content->order->status == "Completed" || $content->order->status == "Cancelled" || $content->order->status == "Returned") {
            //$upload = FileUploads::where('bulkOrderId', $content->order->id)->first();
            \Log::channel('cronjob')->info('Processing case');
            $content->identifiers->orderConfirmed = true;
            // Check tax invoice
            if ($content->identifiers->checkTaxInvoice) {
                $content->email->title = $order_confirmed_mys_title;
                $content->identifiers->isTaxInvoice = true;
                $content->email->ccList = $config_ccList;
                if (isset($content->order->SellerUserId)) {
                    $content->identifiers->isBaSale = true;
                    $content->identifiers->isBaSubSaleMys = true;
                    $content->links->orderDetailsLink = $config_webUrl;
                }
            } else {
                $content->email->title = $order_confirmed_title;
                $content->identifiers->isTaxInvoice = true;
            }

            // Check subscription
            if ($content->order->subscriptionIds) {
                if ($content->identifiers->checkTaxInvoice) {
                    $content->email->subject = $order_tax_invoice_subject;
                } else {
                    $content->email->subject = $order_confirmed_subject;
                }
            } else {
                $content->email->subject = $order_confirmed_subject;
            }
            $content->email->template = 'email-templates.receipt-bulkorder-confirmed-tax-invoice.template';

            // Get order details
            foreach ($content->orderDetails as $detail) {
                $detail->trialPrice = null;
                $detail->NoGstCheck = false;
                $detail->taxCode = $content->country->taxCode;
                $detail->taxRate = $content->country->taxRate;
                $detail->taxName = $content->country->taxName;

                if (in_array($content->order->CountryId, [1, 2])) {
                    $detail->NoGstCheck = true;
                }
                $content->identifiers->NoGstCheck = $detail->NoGstCheck;

                $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(0.00, 2);

                if (isset($content->country->includedTaxToProduct) && $content->country->includedTaxToProduct == 1) {
                    $detail->priceExcludeTax = LaravelHelper::ConvertToNDecimalPoints(($detail->price / (1 + ($content->country->taxRate / 100))), 2);
                    $detail->taxPrice = LaravelHelper::ConvertToNDecimalPoints(($detail->price - $detail->priceExcludeTax), 2);
                }
            }

            $content->identifiers->isOffline = false;

            // Set carrier and mailto settings
            switch ($content->country->code) {
                case 'SGP': {
                        $content->email->local_carrier = "GDEX";
                        $content->email->link_mailto_title = "help.sg@shaves2u.com";
                        $content->email->link_mailto = "mailto:help.sg@shaves2u.com";
                    }
                    break;
                case 'KOR': {
                        $content->email->local_carrier = "LOTTE";
                        $content->email->link_mailto_title = "help.kr@shaves2u.com";
                        $content->email->link_mailto = "mailto:help.kr@shaves2u.com";
                    }
                    break;
                case 'HKG': {
                        $content->email->local_carrier = "TAQBIN-HK";
                        $content->email->link_mailto_title = "help.hk@shaves2u.com";
                        $content->email->link_mailto = "mailto:help.hk@shaves2u.com";
                    }
                    break;
                case 'TWN': {
                        $content->email->local_carrier = "S.F Express";
                        $content->email->link_mailto_title = "help.tw@shaves2u.com";
                        $content->email->link_mailto = "mailto:help.tw@shaves2u.com";
                    }
                    break;

                default: {
                        $content->email->local_carrier = "GDEX";
                        $content->email->link_mailto_title = "help.my@shaves2u.com";
                        $content->email->link_mailto = "mailto:help.my@shaves2u.com";
                    }
                    break;
            }

            // Free product
            // if ($content->promotion->id) {
            //     if ($content->promotion->freeProductCountryIds) {
            //         $freeProductList = [];
            //         foreach (json_decode($content->promotion->freeProductCountryIds) as $freeProductIdStr) {
            //             array_push($freeProductList, (int) $freeProductIdStr);
            //         }

            //         // Change every order detail price to 0 for matching product country ids
            //         foreach ($content->orderDetails as $orderDetail) {
            //             if (in_array($orderDetail->ProductCountryId, $freeProductList)) {
            //                 $orderDetail->price = LaravelHelper::ConvertToNDecimalPoints(0, 2);
            //             }
            //         }
            //     }
            // }

            // Build additional order details
            foreach ($content->orderDetails as $orderDetail) {
                // If is a product
                if ($orderDetail->ProductCountryId) {
                    $orderDetail->details = (object) array();
                    $productId = json_decode(ProductCountry::where('id', $orderDetail->ProductCountryId)->first()->toJson())->ProductId;
                    $orderDetail->details->translatedName = json_decode(ProductTranslate::where('ProductId', $productId)->where('langCode', $content->country->defaultLang)->first()->toJson())->name;
                    $orderDetail->details->sku = json_decode(Product::where('id', $productId)->first()->toJson())->sku;
                    // $orderDetail->details->imageUrl = json_decode(ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson())->url;
                    $orderDetail->details->imageUrl = ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson() !== null ? json_decode(ProductImage::where('ProductId', $productId)->where('isDefault', 1)->first()->toJson())->url : null;
                    $orderDetail->details->totalproductprice = $orderDetail->price * (int)$orderDetail->qty;
                    if($orderDetail->details->totalproductprice){
                        if ($content->country->code === "KOR") {
                            $orderDetail->details->totalproductprice = number_format(LaravelHelper::ConvertToNDecimalPoints(($orderDetail->details->totalproductprice), 0));
                        }else{
                            $orderDetail->details->totalproductprice = LaravelHelper::ConvertToNDecimalPoints($orderDetail->details->totalproductprice, 2);
                        }
                 
                    }
                }
            }

            $content->orderdate = Carbon::parse($content->order->created_at)->format('d M Y');
            $content->shippingdate = Carbon::parse($content->order->created_at)->addDay(1)->format('d M Y');

            $codeIso = "";
            $lang = "";
            if ($content->country->codeIso) {
                $codeIso = strtoupper($content->country->codeIso);
                $lang = $content->country->defaultLang;
            } else {
                $codeIso = 'MY';
                $lang = 'EN';
            }

            \Log::channel('cronjob')->info('Built Data ==> ' . json_encode($content));
            $moduleData = $content;
            $template = view($content->email->template)->with(['moduleData' => $content, 'country' => $country, 'countrycode' => $codeIso, 'lang' => $lang])->render();
            \Log::channel('cronjob')->info('template ==> ' . json_encode($template));
            return $template;
        } else {
            \Log::channel('cronjob')->info('taxInvoiceTemplateBulkOrder cancelled');
        }
    }

    public function getWomanProductFilter($content)
    {
        // Default Female Indentifier
        $content->identifiers->isFemale = false;
        $content->identifiers->isMale = true;

        if ($content->user) {
            if ($content->user->gender) {
                switch ($content->user->gender) {
                    case 'female':
                        $content->identifiers->isFemale = true;
                        $content->identifiers->isMale = false;
                        break;

                    default:
                        $content->identifiers->isFemale = false;
                        $content->identifiers->isMale = true;
                        break;
                }
            }
        }

        /**
         * Filter Using SKUs to test feature. This method is pretty hacky cause the identifiers is based on hardcoded SKU
         * Ideally Product Filtering should be done based on product categories,
         * but needs to scructured based on how The women products is displayed
         * */
        // $womansProductSkuArr = (object) ["F5"];
        // foreach ($content->orderDetails as $orderDetails) {
        //     foreach ($womansProductSkuArr as $womansProductSku) {
        //         if( $womansProductSku == $orderDetails->details->sku){
        //             $content->identifiers->isFemale = true;
        //         }
        //     }

        // }
        return $content;
    }

    public function compressedFile($option)
    {
        try {
            \Log::channel('cronjob')->info('compressedFile ==> ' . $option);
            $options = json_decode($option);
            if ($options->fromdate !== null) {
                $options->fromdate = date("Y-m-d", strtotime($options->fromdate)) . ' 00:00:00';
                $options->fromdate = Carbon::parse($options->fromdate)->format('Y-m-d 00:00:00');
            }
            if ($options->todate !== null) {
                $options->todate = date("Y-m-d", strtotime($options->todate)) . ' 00:00:00';
                $options->todate = Carbon::parse($options->todate)->add(1, 'day')->format('Y-m-d 00:00:00');
            }
            \Log::channel('cronjob')->info('compressedFile options ==> ' . json_encode($options));
            $FilesToBeDownloaded = [];

            if ($options->type == 'order' && $options->country != 'all') {
                $lists = Orders::where('CountryId', $options->country)
                    ->when($options->fromdate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '>=', $options->fromdate);
                    })
                    ->when($options->todate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '<=', $options->todate);
                    })
                    ->get();

                if (count($lists) > 0) {
                    $orderIds = [];
                    foreach ($lists as $list) {
                        array_push($orderIds, $list->id);
                    }

                    // read options & get all rows from database
                    $files = FileUploads::where('name', 'like', '%TaxInvoice_%')
                        ->where('isReport', 0)
                        ->where('status', 'Completed')
                        ->whereIn('orderId', $orderIds)
                        ->whereNotNull('url')
                        ->when($options->fromdate != null, function ($q) use ($options) {
                            return $q->whereDate('created_at', '>=', $options->fromdate);
                        })
                        ->when($options->todate != null, function ($q) use ($options) {
                            return $q->whereDate('created_at', '<=', $options->todate);
                        })
                        ->pluck('name');


                    \Log::channel('cronjob')->info('files found from db ==> ' . json_encode($files));
                    // // get all files info

                    if (!empty($files) && $files !== null && $files !== "undefined" && $files !== "") {

                        $zipFilename = $options->zipFilename;
                        $file_id = $options->file_id;

                        $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
                        $localdownloadpath = "/downloads/tax-invoices/";

                        // download each file from aws to local
                        foreach ($files as $file) {
                            $s3_file = Storage::disk('s3')->get($this->taxInvoiceFolderPath . '/' . $file);
                            $s3 = Storage::disk('public');
                            $s3->put($localdownloadpath . $file, $s3_file);
                            $filename = $storagePath . $localdownloadpath . $file;
                            array_push($FilesToBeDownloaded, $filename);
                        }

                        \Log::channel('cronjob')->info('files in array ==> ' . json_encode($FilesToBeDownloaded));
                        // create zip file using $options
                        $zip = Zip::create($storagePath . $localdownloadpath . $zipFilename);
                        $zip->add($FilesToBeDownloaded);
                        $zip->close();

                        // delete all files
                        if (count($FilesToBeDownloaded) > 0) {
                            foreach ($FilesToBeDownloaded as $file) {
                                $this->deleteFile($file);
                            }
                        }

                        $localFilepath = file_get_contents($storagePath . $localdownloadpath . $zipFilename);
                        if ($localFilepath) {
                            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.filesUploadAlbum') . '/' . $zipFilename;
                            // upload to aws
                            $s3_file = Storage::disk('s3')->put($this->taxInvoiceFolderPath . "/" . $zipFilename, $localFilepath);
                            // update fileuploads entry
                            FileUploads::where('file_id', $file_id)->update([
                                'url' => $url,
                                'status' => 'Completed',
                                'isDownloaded' => 1,
                            ]);

                            $this->deleteFile($storagePath . $localdownloadpath . $zipFilename);
                        }
                    } else {
                        return "no tax-invoices found in date range";
                    }
                }
            } else if ($options->type == 'bulkorder' && $options->country != 'all') {
                $lists = BulkOrders::where('CountryId', $options->country)
                    ->when($options->fromdate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '>=', $options->fromdate);
                    })
                    ->when($options->todate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '<=', $options->todate);
                    })
                    ->get();

                if (count($lists) > 0) {
                    $orderIds = [];
                    foreach ($lists as $list) {
                        array_push($orderIds, $list->id);
                    }

                    // read options & get all rows from database
                    $files = FileUploads::where('name', 'like', '%TaxInvoice_%')
                        ->where('isReport', 0)
                        ->where('status', 'Completed')
                        ->whereIn('orderId', $orderIds)
                        ->whereNotNull('url')
                        ->when($options->fromdate != null, function ($q) use ($options) {
                            return $q->whereDate('created_at', '>=', $options->fromdate);
                        })
                        ->when($options->todate != null, function ($q) use ($options) {
                            return $q->whereDate('created_at', '<=', $options->todate);
                        })
                        ->pluck('name');


                    \Log::channel('cronjob')->info('files found from db ==> ' . json_encode($files));
                    // // get all files info

                    if (!empty($files) && $files !== null && $files !== "undefined" && $files !== "") {

                        $zipFilename = $options->zipFilename;
                        $file_id = $options->file_id;

                        $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
                        $localdownloadpath = "/downloads/tax-invoices/";

                        // download each file from aws to local
                        foreach ($files as $file) {
                            $s3_file = Storage::disk('s3')->get($this->taxInvoiceFolderPath . '/' . $file);
                            $s3 = Storage::disk('public');
                            $s3->put($localdownloadpath . $file, $s3_file);
                            $filename = $storagePath . $localdownloadpath . $file;
                            array_push($FilesToBeDownloaded, $filename);
                        }

                        \Log::channel('cronjob')->info('files in array ==> ' . json_encode($FilesToBeDownloaded));
                        // create zip file using $options
                        $zip = Zip::create($storagePath . $localdownloadpath . $zipFilename);
                        $zip->add($FilesToBeDownloaded);
                        $zip->close();

                        // delete all files
                        if (count($FilesToBeDownloaded) > 0) {
                            foreach ($FilesToBeDownloaded as $file) {
                                $this->deleteFile($file);
                            }
                        }

                        $localFilepath = file_get_contents($storagePath . $localdownloadpath . $zipFilename);
                        if ($localFilepath) {
                            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.filesUploadAlbum') . '/' . $zipFilename;
                            // upload to aws
                            $s3_file = Storage::disk('s3')->put($this->taxInvoiceFolderPath . "/" . $zipFilename, $localFilepath);
                            // update fileuploads entry
                            FileUploads::where('file_id', $file_id)->update([
                                'url' => $url,
                                'status' => 'Completed',
                                'isDownloaded' => 1,
                            ]);

                            $this->deleteFile($storagePath . $localdownloadpath . $zipFilename);
                        }
                    } else {
                        return "no tax-invoices found in date range";
                    }
                }
            } else {
                // read options & get all rows from database
                $files = FileUploads::where('name', 'like', '%TaxInvoice_%')
                    ->where('isReport', 0)
                    ->where('status', 'Completed')
                    ->whereNotNull('url')
                    ->when($options->fromdate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '>=', $options->fromdate);
                    })
                    ->when($options->todate != null, function ($q) use ($options) {
                        return $q->whereDate('created_at', '<=', $options->todate);
                    })
                    ->pluck('name');

                \Log::channel('cronjob')->info('files found from db ==> ' . json_encode($files));
                // // get all files info

                if (!empty($files) && $files !== null && $files !== "undefined" && $files !== "") {

                    $zipFilename = $options->zipFilename;
                    $file_id = $options->file_id;

                    $storagePath = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
                    $localdownloadpath = "/downloads/tax-invoices/";

                    // download each file from aws to local
                    foreach ($files as $file) {
                        $s3_file = Storage::disk('s3')->get($this->taxInvoiceFolderPath . '/' . $file);
                        $s3 = Storage::disk('public');
                        $s3->put($localdownloadpath . $file, $s3_file);
                        $filename = $storagePath . $localdownloadpath . $file;
                        array_push($FilesToBeDownloaded, $filename);
                    }

                    \Log::channel('cronjob')->info('files in array ==> ' . json_encode($FilesToBeDownloaded));
                    // create zip file using $options
                    $zip = Zip::create($storagePath . $localdownloadpath . $zipFilename);
                    $zip->add($FilesToBeDownloaded);
                    $zip->close();

                    // delete all files
                    if (count($FilesToBeDownloaded) > 0) {
                        foreach ($FilesToBeDownloaded as $file) {
                            $this->deleteFile($file);
                        }
                    }

                    $localFilepath = file_get_contents($storagePath . $localdownloadpath . $zipFilename);
                    if ($localFilepath) {
                        $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.filesUploadAlbum') . '/' . $zipFilename;
                        // upload to aws
                        $s3_file = Storage::disk('s3')->put($this->taxInvoiceFolderPath . "/" . $zipFilename, $localFilepath);
                        // update fileuploads entry
                        FileUploads::where('file_id', $file_id)->update([
                            'url' => $url,
                            'status' => 'Completed',
                            'isDownloaded' => 1,
                        ]);

                        $this->deleteFile($storagePath . $localdownloadpath . $zipFilename);
                    }
                } else {
                }
                return "no tax-invoices found in date range";
            }
        } catch (Exception $e) {
            \Log::channel('cronjob')->error('compressedFile error => ' . $e->getMessage());
            return $e;
        }
    }

    public function deleteFile($path)
    {
        return is_file($path) ?
            @unlink($path) :
            array_map('rrmdir', glob($path . '/*')) == @rmdir($path);
    }
}
