<?php
namespace App\Helpers\APIHelpers\Admin;
use App\Services\BulkOrderService;
use App\Services\OrderService;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Orders\Orders;
use App\Models\Geolocation\Countries;
use App\Models\Reports\SalesReport;
use Carbon\Carbon;
use DB;

class ReportAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }
    
    public function getSales($options)
    {
        $timeperiod = null;
        $countryid = null;

        if (isset($options['timeperiod'])) {
            $timeperiod = $options['timeperiod'];
        }

        if (isset($options['countryid'])) {
            $countryid = $options['countryid'];
        }

        $sales = SalesReport::where( 'CountryId', $countryid )
                        ->when($timeperiod === "day", function ($q) {
                            return $q->where( 'saleDate', '>=', Carbon::now()->subDay()->toDateString() );
                        })
                        ->when($timeperiod === "week", function ($q) {
                            return $q->where( 'saleDate', '>=', Carbon::now()->subWeek()->toDateString() );
                        })
                        ->when($timeperiod === "month", function ($q) {
                            return $q->where( 'saleDate', '>=', Carbon::now()->subMonth()->toDateString() );
                        })
                        ->when($timeperiod === "year", function ($q) {
                            return $q->where( 'saleDate', '>=', Carbon::now()->subYear()->toDateString() );
                        })
                        ->where( 'saleDate', '<=', Carbon::now() )
                        ->select(\DB::raw('Date(saleDate) as date_group'), \DB::raw('sum(grandTotalPrice) as total_day_sales'))
                        ->groupBy(\DB::raw('Date(saleDate)') )
                        ->get();

        $currency = Countries::where('id', $countryid)->pluck('currencyDisplay')->first();

        $response = (object) array(
            "currency" => $currency,
            "sales" => $sales
        );

        return $response;
    }

    public function getMastersCount($options) {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }


        $reportsmasterlist = SalesReport::when($channel != null, function ($q) use ($channel) {
                            if ($channel === "all") {
                                return $q;
                            }
                            else if ($channel === "seller") {
                                return $q->where('category', "sales app");
                            }
                            else if ($channel === "customer") {
                                return $q->where('category', "client");
                            }
                            else if ($channel === "bulk") {
                                return $q->where('category', "Bulk Order");
                            }
                        })
                        ->when($country != null, function ($q) use ($country) {
                            if ($country === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('CountryId', $country);
                            }
                        })
                       
                        ->when($fromdate != null, function ($q) use ($fromdate) {
                            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                        })
                        ->when($todate != null, function ($q) use ($todate) {
                            return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
                        })
                        ->when($status != null, function ($q) use ($status) {
                            if ($status === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('status', $status);
                            }
                        })
                        // ->distinct()
                        ->get();

        foreach ($reportsmasterlist as $rm) {

            $_country = Countries::findorfail($rm->CountryId);
            if($rm->category == "Bulk Order"){
            $_rm = BulkOrders::findorfail($rm->bulkOrderId);
            $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
            }else{
            $_rm = Orders::findorfail($rm->orderId);
            $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
            }
        }
        
        $reportsmasterlist_final = $reportsmasterlist->filter(function($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
            }
            else {
                return true;
            }
        });

        $reportsmaster = (array) null;
        $reportsmaster['total'] = count($reportsmasterlist_final);
        $reportsmaster['pages'] = ceil(count($reportsmasterlist_final) / $maxresults);

        return $reportsmaster;
    }

    public function getMastersList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["length"])) {
            $maxresults = $options["length"];
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["start"])) {
            $pagenum = intVal($options["start"]) / $maxresults + 1;
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;    
        }

        $reports = collect();
        DB::table('salereports')
            ->select('CountryId','category','orderId','bulkOrderId','created_at','taxInvoiceNo','sku','region','status')
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                }
                else if ($channel === "seller") {
                    return $q->where('category', "sales app");
                }
                else if ($channel === "customer") {
                    return $q->where('category', "client");
                }
                else if ($channel === "bulk") {
                    return $q->where('category', "Bulk Order");
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                }
                else {
                    return $q->where('CountryId', $country);
                }
            })
            
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                }
                else {
                    return $q->where('status', $status);
                }
            })
            ->orderBy('created_at', 'DESC')
            // ->distinct()
            // ->get();
            ->chunk(1000, function ($reportsmaster) use (&$reports, $search) {
                foreach ($reportsmaster as $key => $rm) {
                    $_country = Countries::where('id',$rm->CountryId)->first();

                    if($rm->category === "Bulk Order"){
                        $_rm = BulkOrders::where('id',$rm->bulkOrderId)->first();
                        $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                    }else{
                        $_rm = Orders::where('id',$rm->orderId)->first();
                        $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                    }

                    if ($search !== null && $search !== "") {
                        if (strpos($rm->order_no.$rm->taxInvoiceNo.$rm->sku, $search) !== false) {//if found
                            $rm->created_at =$rm->created_at;
                        }
                        else { //remove record
                            $reportsmaster->forget($key);
                        }
                    }
                    else {
                        $rm->created_at =$rm->created_at;
                    }
                }
                $reports = $reports->merge($reportsmaster);
                
                // if ($search !== null && $search !== "") {
                //     $reportsmaster_final = $reportsmaster->filter(function($report) use ($search) {
                //             return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
                //     });
                // }
                // else {
                //     $reportsmaster_final = $reportsmaster;
                // }
            });

        $response = array(
            "draw" => intVal($options['draw']),
            "iTotalRecords" => count($reports),
            "iTotalDisplayRecords" => count($reports),
            "data" => array_values($reports->forPage($pagenum, $maxresults)->values()->toArray())
        );

        return $response;
    }

    public function getAppcoCount($options) {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $reportsappcolist = SalesReport::when($country != null, function ($q) use ($country) {
                            if ($country === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('CountryId', $country);
                            }
                        })
                       
                        ->when($fromdate != null, function ($q) use ($fromdate) {
                            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                        })
                        ->when($todate != null, function ($q) use ($todate) {
                            return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
                        })
                        ->when($status != null, function ($q) use ($status) {
                            if ($status === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('status', $status);
                            }
                        })
                        ->where('category', "sales app")
                        ->distinct()
                        ->get();

        foreach ($reportsappcolist as $rm) {
            // retrieve country info
            $_country = Countries::findorfail($rm->CountryId);

            if($rm->category == "Bulk Order"){
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }else{
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }
        }
        
        $reportsappcolist_final = $reportsappcolist->filter(function($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
            }
            else {
                return true;
            }
        });

        $reportsappco = (array) null;
        $reportsappco['total'] = count($reportsappcolist_final);
        $reportsappco['pages'] = ceil(count($reportsappcolist_final) / $maxresults);

        return $reportsappco;
    }

    public function getAppcoList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }


        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

       if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }
        else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }
        $reportsappco = DB::table('salereports')
                        ->when($country != null, function ($q) use ($country) {
                            if ($country === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('CountryId', $country);
                            }
                        })
                       
                        ->when($fromdate != null, function ($q) use ($fromdate) {
                            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                        })
                        ->when($todate != null, function ($q) use ($todate) {
                            return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
                        })
                        ->when($status != null, function ($q) use ($status) {
                            if ($status === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('status', $status);
                            }
                        })
                        ->where('category', "sales app")
                        ->orderBy('created_at', 'DESC')
                        ->distinct()
                        ->get()->forPage($pagenum, $maxresults)->values();

        foreach ($reportsappco as $rm) {
            // retrieve country info
            $_country = Countries::findorfail($rm->CountryId);

            if($rm->category == "Bulk Order"){
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }else{
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }
            // $rm->created_at = date('d-m-y H:i', strtotime($rm->created_at));
        }
        
        $reportsappco_final = $reportsappco->filter(function($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
            }
            else {
                return true;
            }
        });

        return $reportsappco_final;
    }

    public function getMOCount($options) {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $reportsmolist = SalesReport::when($country != null, function ($q) use ($country) {
                            if ($country === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('CountryId', $country);
                            }
                        })
                       
                        ->when($fromdate != null, function ($q) use ($fromdate) {
                            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                        })
                        ->when($todate != null, function ($q) use ($todate) {
                            return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
                        })
                        ->when($status != null, function ($q) use ($status) {
                            if ($status === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('status', $status);
                            }
                        })
                        ->where('category', "sales app")
                        ->distinct()
                        ->get();


        foreach ($reportsmolist as $rm) {
            // retrieve country info
            $_country = Countries::findorfail($rm->CountryId);

            if($rm->category == "Bulk Order"){
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }else{
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }
        }
        
        $reportsmolist_final = $reportsmolist->filter(function($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
            }
            else {
                return true;
            }
        });

        $reportsmo = (array) null;
        $reportsmo['total'] = count($reportsmolist);
        $reportsmo['pages'] = ceil(count($reportsmolist) / $maxresults);

        return $reportsmo;
    }

    public function getMOList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $reportsmo = DB::table('salereports')
                        ->when($country != null, function ($q) use ($country) {
                            if ($country === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('CountryId', $country);
                            }
                        })
                       
                        ->when($fromdate != null, function ($q) use ($fromdate) {
                            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
                        })
                        ->when($todate != null, function ($q) use ($todate) {
                            return $q->whereDate('updated_at', '<=', date("Y-m-d", strtotime($todate)));
                        })
                        ->when($status != null, function ($q) use ($status) {
                            if ($status === "all") {
                                return $q;
                            }
                            else {
                                return $q->where('status', $status);
                            }
                        })
                        ->where('category', "sales app")
                        ->orderBy('created_at', 'DESC')
                        // ->distinct()
                        ->get()->forPage($pagenum, $maxresults)->values();

        foreach ($reportsmo as $rm) {
            // retrieve country info
            $_country = Countries::findorfail($rm->CountryId);

            if($rm->category == "Bulk Order"){
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }else{
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false).'-'.date('Ymd', strtotime($rm->created_at));
                }
            // $rm->created_at = date('d-m-y H:i', strtotime($rm->created_at));
        }
        
        $reportsmo_final = $reportsmo->filter(function($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no.$report->taxInvoiceNo.$report->sku, $search) >= 0;
            }
            else {
                return true;
            }
        });

        return $reportsmo_final;
    }

}
