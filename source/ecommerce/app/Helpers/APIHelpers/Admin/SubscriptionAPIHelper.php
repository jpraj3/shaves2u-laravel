<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App;

// Helpers
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Orders\Orders;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\User\DeliveryAddresses;
use App\Queue\SendEmailQueueService as EmailQueueService;

use Illuminate\Support\Facades\Redis;
use DB;

// Services

// Models

class SubscriptionAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getSubscribersCount($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $plantype = null;
        $sku = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["plantype"])) {
            $plantype = $options["plantype"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $subscriptionlist = DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->select('subscriptions.id', 'subscriptions.email', 'countries.name as countryName', 'subscriptions.created_at', 'subscriptions.updated_at', 'plansku.duration', 'plansku.sku', 'subscriptions.isTrial', 'subscriptions.isCustom', 'subscriptions.status')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('subscriptions.email', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('plans.CountryId', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('plans.PlanSkuId', $sku);
                }
            })
            ->when($plantype != null, function ($q) use ($plantype) {
                if ($plantype === "trial") {
                    return $q->where('subscriptions.isTrial', 1);
                } else if ($plantype === "custom") {
                    return $q->where('subscriptions.isCustom', 1);
                } else {
                    return $q;
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptions.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptions.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('subscriptions.status', $status);
                }
            })
            ->distinct()
            ->get();

        $subscriptions = (array) null;
        $subscriptions['total'] = count($subscriptionlist);
        $subscriptions['pages'] = ceil(count($subscriptionlist) / $maxresults);

        return $subscriptions;
    }

    public function getSubscribersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $plantype = null;
        $sku = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["length"])) {
            $maxresults = intVal($options["length"]);
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["plantype"])) {
            $plantype = $options["plantype"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["start"])) {
            $pagenum = intVal($options["start"]) / $maxresults + 1;
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $subscriptions = DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->select('subscriptions.id', 'subscriptions.email', 'countries.name as countryName', 'subscriptions.created_at', 'subscriptions.updated_at', 'plansku.duration', 'plansku.isAnnual', 'plansku.sku', 'subscriptions.isTrial', 'subscriptions.isCustom', 'subscriptions.status', 'subscriptions.currentCycle')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('subscriptions.email', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('plans.CountryId', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('plans.PlanSkuId', $sku);
                }
            })
            ->when($plantype != null, function ($q) use ($plantype) {
                if ($plantype === "trial") {
                    return $q->where('subscriptions.isTrial', 1);
                } else if ($plantype === "custom") {
                    return $q->where('subscriptions.isCustom', 1);
                } else {
                    return $q;
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptions.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptions.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('subscriptions.status', $status);
                }
            })
            ->orderBy('subscriptions.created_at', 'DESC')
            // ->distinct()
            // ->for($maxresults, ['*'], 'page', $pagenum);
            ->get();

        // $subs_data = array();
        foreach ($subscriptions as $sub) {
            $sub->created_date = $sub->created_at;
            $sub->updated_date = $sub->updated_at;
            if ($sub->isAnnual == 1) {
                $sub->duration = 12;
            }
            if ($sub->isTrial == 1 && $sub->isCustom != 1) {
                $sub->plantype = "Trial";
            } else {
                $sub->plantype = "Custom";
            }
        }

        $response = array(
            "draw" => intVal($options['draw']),
            "iTotalRecords" => count($subscriptions),
            "iTotalDisplayRecords" => count($subscriptions),
            "data" => array_values($subscriptions->forPage($pagenum, $maxresults)->values()->toArray())
        );

        return $response;
    }

    public function getSubscriberDetails($id)
    {
        // $subscriberdetails = Redis::get('subscribers:details:'.$id);

        // if ($subscriberdetails == null) {
        $subscriber = DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->where('subscriptions.id', $id)
            ->select('subscriptions.email', 'subscriptions.id', 'subscriptions.status', 'subscriptions.isTrial', 'subscriptions.isCustom', 'subscriptions.SellerUserId', 'plansku.duration', 'plansku.isAnnual', 'subscriptions.isOffline', 'subscriptions.created_at', 'subscriptions.updated_at', 'subscriptions.nextChargeDate', 'subscriptions.DeliveryAddressId', 'subscriptions.PlanId', 'subscriptions.CardId')
            ->first();

        $subscriber->orders = Orders::where('subscriptionIds', $id)
            ->orderBy('created_at', 'DESC')
            ->get();

        foreach ($subscriber->orders as $order) {
            // retrieve order info
            $_order = Orders::findorfail($order->id);

            // retrieve country info
            $_country = Countries::findorfail($order->CountryId);

            $order->order_no = $this->orderService->formatOrderNumber($_order, $_country, false);
        }

        $subscriber->rechargehistory = SubscriptionHistories::where('subscriptionId', $id)
            ->where('message', 'Processing')
            ->select('message', 'created_at', 'created_at as created_at_date')
            ->get();

        $subscriber->history = SubscriptionHistories::where('subscriptionId', $id)
            ->select('message', 'detail', 'created_at', 'created_at as created_at_date')
            ->get();

        $subscriber->invoice = Receipts::where('OrderId', $subscriber->orders->first()->id)
            ->select('currency', 'chargeId', 'branchName', 'last4', 'originPrice', 'discountAmount', 'chargeFee', 'totalPrice')
            ->first();

        $subscriber->card = Cards::where('id', $subscriber->CardId)
            ->select('cardNumber', 'cardType', 'branchName', 'expiredYear', 'expiredMonth')
            ->first();

        $subscriber->deliverydetails = DeliveryAddresses::where('id', $subscriber->DeliveryAddressId)
            ->select('CountryId', 'firstName', 'contactNumber', 'address')
            ->first();

        $subscriber->plandetails = DB::table('plan_details')
            ->leftJoin('productcountries', 'plan_details.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            // ->leftJoin('producttranslates', 'productcountries.ProductId', '=', 'producttranslates.ProductId')
            ->leftJoin('countries', 'productcountries.CountryId', '=', 'countries.id')
            ->where('plan_details.PlanId', $subscriber->PlanId)
            // ->where('producttranslates.langCode', 'EN')
            ->select(
                'products.sku',
                // 'producttranslates.name',
                'productcountries.sellPrice',
                'countries.currencyDisplay',
                'plan_details.qty'
            )
            ->get();

        $subscriber->addons = DB::table('subscriptions_productaddon')
            ->leftJoin('productcountries', 'subscriptions_productaddon.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            // ->leftJoin('producttranslates', 'productcountries.ProductId', '=', 'producttranslates.ProductId')
            ->leftJoin('countries', 'productcountries.CountryId', '=', 'countries.id')
            ->where('subscriptions_productaddon.subscriptionId', $subscriber->id)
            // ->where('producttranslates.langCode', 'EN')
            ->select(
                'products.sku',
                // 'producttranslates.name',
                'productcountries.sellPrice',
                'countries.currencyDisplay'
            )
            ->get();

        // Redis::set('subscribers:details:'.$id, json_encode($subscriber));
        $subscriber->created_date = $subscriber->created_at;
        $subscriber->updated_date = $subscriber->updated_at;

        // $subscriberdetails = Redis::get('subscribers:details:'.$id);
        $subscriberdetails = json_encode($subscriber);
        // }

        return json_decode($subscriberdetails);
    }

    public function updateSubscriptionStatus($params)
    {
        $isRemark = 0;

        if (isset($params["isRemark"])) {
            $isRemark = $params["isRemark"];
        }

        $subscriptionHistory = new SubscriptionHistories();
        $subscriptionHistory->subscriptionId = $params["id"];
        $subscriptionHistory->message = (string) $params["status"];
        $subscriptionHistory->detail = isset($params["comment"]) ? (string) $params["comment"] : null;
        $subscriptionHistory->save();

        $sublog = SubscriptionHistories::where('subscriptionId', $params["id"])
            ->select('message', 'detail', 'created_at as created_at_date')
            ->get();

        if ($isRemark === 0 || $isRemark === "0") {
            $updateSub = Subscriptions::find($params["id"]);
            $updateSub->status = $params["status"];
            if ($params["status"] === "Cancelled") {
                $updateSub->cancellationReason = "Cancelled by Admin";

                $subscriptionHistoryCancelled = new SubscriptionHistories();
                $subscriptionHistoryCancelled->subscriptionId = $params["id"];
                $subscriptionHistoryCancelled->message = (string) $params["status"];
                $subscriptionHistoryCancelled->detail = "Cancelled by Admin";
                $subscriptionHistoryCancelled->save();
            } else {
                $newHistory = new SubscriptionHistories();
                $newHistory->subscriptionId = $params["id"];
                $newHistory->message = (string) $params["status"];
                $newHistory->detail = "Subscription status <" . (string) $params["status"] . "> updated by Admin";
                $newHistory->save();
            }

            $updateSub->save();

            $user = User::find($updateSub->UserId);
            $country = Countries::leftJoin('plans', 'countries.id', '=', 'plans.CountryId')
                ->where('plans.id', $updateSub->PlanId)
                ->first();

            $emailData = [];

            if ($params["status"] === "Cancelled") {
                $emailData['title'] = 'subscription-cancellation';
                $emailData['moduleData'] = (object) array(
                    'email' => $updateSub->email,
                    'subscriptionId' => $updateSub->id,
                    'userId' => $updateSub->UserId,
                    'countryId' => $country->id
                );
                $emailData['CountryAndLocaleData'] = array($country->id, $user->defaultLanguage);
                EmailQueueService::addHash($updateSub->UserId, 'subscription_cancellation', $emailData);
            }
        }

        return $sublog;
    }
}
