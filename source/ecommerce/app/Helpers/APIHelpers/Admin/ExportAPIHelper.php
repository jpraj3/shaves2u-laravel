<?php

namespace App\Helpers\APIHelpers\Admin;

use App;
use DB;

class ExportAPIHelper
{
    public function __construct()
    {
    }

    public function getExportsCount($options)
    {
        $maxresults = 50;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $adminId = null;

        if (isset($options["adminId"])) {
            $adminId = $options["adminId"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $exportlist = DB::table('fileuploads')
            ->where('AdminId', $adminId)
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('fileuploads.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('fileuploads.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('fileuploads.status', $status);
                }
            })
            ->distinct()
            ->get();

        $export = (array) null;
        $export['total'] = count($exportlist);
        $export['pages'] = ceil(count($exportlist) / $maxresults);

        return $export;
    }

    public function getExportList($options)
    {
        $maxresults = 50;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $adminId = null;

        if (isset($options["adminId"])) {
            $adminId = $options["adminId"];
        }
        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $exports = DB::table('fileuploads')
            ->where('AdminId', $adminId)
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('fileuploads.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('fileuploads.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('fileuploads.status', $status);
                }
            })
            ->orderBy('fileuploads.created_at', 'DESC')
            ->distinct()
            ->get()->forPage($pagenum, $maxresults)->values();

        foreach ($exports as $export) {
            $export->created_date = $export->created_at;
            $export->updated_date = $export->updated_at;
        }

        return $exports;
    }
}
