<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App\Models\Ecommerce\User;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Plans\PlanDetails;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Receipts\Receipts;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;
use App\Models\Geolocation\Countries;
use App\Models\FileUploads\FileUploads;
use App\Models\Orders\PaymentHistories;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use DB;
use Exception;
use App\Helpers\Payment\StripeBA;
use App\Http\Controllers\Globals\Payment\NicePayController;
use App\Queue\SendEmailQueueService as EmailQueueService;

class OrderAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getOrdersCount($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $orderlist = DB::table('orders')
            ->leftJoin('orderdetails', 'orders.id', '=', 'orderdetails.OrderId')
            ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
            ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
            ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
            ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
            ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
            ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->select('orders.id', 'orders.UserId', 'orders.CountryId', 'countries.codeIso', 'orders.created_at', 'orders.taxInvoiceNo', 'orders.email', 'orders.status', 'receipts.totalPrice', 'receipts.currency', 'pl_products.sku', 'pr_products.sku')
            ->when($search != null, function ($q) use ($search) {
                return $q->orWhere('orders.email', 'LIKE', '%' . $search . '%')->orWhere('orders.taxInvoiceNo', 'LIKE', '%' . $search . '%')->orWhere('orders.id', 'LIKE', '%' . $search . '%');
            })
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                } else if ($channel === "seller") {
                    return $q->whereNotNull('orders.SellerUserID');
                } else if ($channel === "customer") {
                    return $q->whereNull('orders.SellerUserID');
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('pl_products.sku', $sku)->orWhere('pr_products.sku', $sku);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('orders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('orders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('orders.status', $status);
                }
            })
            ->groupBy('orders.id', 'receipts.totalPrice', 'receipts.currency')
            ->distinct()
            ->get();

        foreach ($orderlist as $ord) {
            // retrieve order info
            $_order = Orders::findorfail($ord->id);

            // retrieve country info
            $_country = Countries::findorfail($ord->CountryId);

            $ord->order_no = $this->orderService->formatOrderNumber($_order, $_country, false);
        }

        // $orderlist_final = $orderlist->filter(function ($order) use ($search) {
        //     if ($search !== null && $search !== "") {
        //         return strpos($order->email.$order->taxInvoiceNo.$order->order_no, $search) >= 0;
        //     } else {
        //         return true;
        //     }
        // });

        $orders = (array) null;
        $orders['total'] = count($orderlist);
        $orders['pages'] = ceil(count($orderlist) / $maxresults);

        return $orders;
    }

    public function getOrdersList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["length"])) {
            $maxresults = $options["length"];
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["start"])) {
            $pagenum = intVal($options["start"]) / $maxresults + 1;
        }

        if (isset($options["search"]) && $options["search"] !== null && $options["search"] !== "") {
            $search = $options["search"];
            $channel = null;
            $country = null;
            $status = null;
            $sku = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $orders_final = collect();
        DB::table('orders')
            // ->leftJoin('orderdetails', 'orders.id', '=', 'orderdetails.OrderId')
            // ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
            // ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
            // ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
            // ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
            // ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
            ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->select('orders.id', 'orders.UserId', 'orders.CountryId', 'countries.codeIso', 'orders.created_at', 'orders.taxInvoiceNo', 'orders.email', 'orders.status', 'receipts.totalPrice', 'receipts.currency')
            ->when($search != null, function ($q) use ($search) {
                return $q->orWhere('orders.email', 'LIKE', '%' . $search . '%')->orWhere('orders.taxInvoiceNo', 'LIKE', '%' . $search . '%')->orWhere('orders.id', 'LIKE', '%' . $search . '%');
            })
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                } else if ($channel === "seller") {
                    return $q->whereNotNull('orders.SellerUserID');
                } else if ($channel === "customer") {
                    return $q->whereNull('orders.SellerUserID');
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            // ->when($sku != null, function ($q) use ($sku) {
            //     if ($sku === "all") {
            //         return $q;
            //     } else {
            //         return $q->where('pl_products.sku', $sku)->orWhere('pr_products.sku', $sku);
            //     }
            // })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('orders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('orders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('orders.status', $status);
                }
            })
            ->groupBy('orders.id', 'receipts.totalPrice', 'receipts.currency')
            ->orderBy('orders.created_at', 'DESC')
            // ->distinct()
            ->chunk(1000, function ($orders) use (&$orders_final, $search, $sku) {
                foreach ($orders as $key => $ord) {
                    // retrieve order info
                    // $_order = Orders::findorfail($ord->id);

                    // retrieve country info
                    // $_country = Countries::findorfail($ord->CountryId);

                    $ord->order_no = $this->orderService->formatOrderNumberV5($ord->id, $ord->codeIso, $ord->created_at, false);

                    if ($search !== null && $search !== "") {
                        if (strpos($ord->email . $ord->taxInvoiceNo . $ord->order_no, $search) !== false) { //if found
                            $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
                                ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
                                ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
                                ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
                                ->leftJoin('products as pl_products', function ($join) {
                                    $join->on('pl_pcs.ProductId', '=', 'pl_products.id');
                                    $join->whereNotIn('pl_products.sku', config('global.all.handle_types.skusV2'));
                                })
                                ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
                                ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku, ""),ifnull(pr_products.sku, "")) separator ", ") as skus'))
                                ->groupBy('orderdetails.OrderId')->pluck('skus')->first();

                            $ord->skus = trim($ord->skus, ", ");
                        } else { //remove record
                            $orders->forget($key);
                        }
                    } else {
                        $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
                            ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
                            ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
                            ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
                            ->leftJoin('products as pl_products', function ($join) {
                                $join->on('pl_pcs.ProductId', '=', 'pl_products.id');
                                $join->whereNotIn('pl_products.sku', config('global.all.handle_types.skusV2'));
                            })
                            ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
                            ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku,""),ifnull(pr_products.sku, "")) separator ", ") as skus'))
                            ->groupBy('orderdetails.OrderId')->pluck('skus')->first();

                        $ord->skus = trim($ord->skus, ", ");
                    }

                    if ($sku !== null && $sku !== "all") {
                        if (in_array($sku, explode(', ', $ord->skus))) { //if found
                            $ord->taxinvoiceurl = FileUploads::where('orderId', $ord->id)->pluck('url')->first();
                        } else {
                            $orders->forget($key);
                        }
                    } else {
                        $ord->taxinvoiceurl = FileUploads::where('orderId', $ord->id)->pluck('url')->first();
                    }
                }

                $orders_final = $orders_final->merge($orders);
            });

        // foreach ($orders as $ord) {

        //     $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
        //         ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
        //         ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
        //         ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
        //         ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
        //         ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
        //         ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku, ""),ifnull(pr_products.sku,"")) separator ", ") as skus'))
        //         ->groupBy('orderdetails.OrderId')->pluck('skus');

        //     $ord->taxinvoiceurl = FileUploads::where('orderId', $ord->id)->pluck('url')->first();;

        //     // retrieve order info
        //     $_order = Orders::findorfail($ord->id);

        //     // retrieve country info
        //     $_country = Countries::findorfail($ord->CountryId);

        //     $ord->order_no = $this->orderService->formatOrderNumber($_order, $_country, false);
        //     // $ord->created_at = date('d-m-y H:i', strtotime($ord->created_at));
        // }

        // $orders_final = $orders->filter(function ($order) use ($search) {
        //     if ($search !== null && $search !== "") {
        //         return strpos($order->email.$order->taxInvoiceNo.$order->order_no, $search) >= 0;
        //     } else {
        //         return true;
        //     }
        // });

        $response = array(
            "draw" => intVal($options['draw']),
            "iTotalRecords" => count($orders_final),
            "iTotalDisplayRecords" => count($orders_final),
            "data" => array_values($orders_final->forPage($pagenum, $maxresults)->values()->toArray())
        );

        return $response;
    }

    public function getOrderDetails($id)
    {
        // $orderdetails = json_decode(Redis::get('orders:details:'.$id));

        // if ($orderdetails == null) {
        $orderdetails = $this->setOrderDetailsDirectDB($id);
        // }

        return $orderdetails;
    }

    public function setOrderDetailsDirectDB($id)
    {
        $order = Orders::where('id', $id)
            ->select(
                'id',
                'fullName',
                'email',
                'subscriptionIds',
                'deliveryId',
                'taxInvoiceNo',
                'SellerUserId',
                'paymentType',
                'created_at as created_at_date',
                'created_at',
                'UserId',
                'CountryId',
                'DeliveryAddressId',
                'status',
                'payment_status',
                'phone',
                'carrierAgent',
                'isSubsequentOrder'
            )
            ->first();

        $order['orderlog'] = OrderHistories::where('OrderId', $id)
            ->select('message', 'created_at as created_at_date')
            ->get();

        $order['paymentlog'] = PaymentHistories::where('OrderId', $id)
            ->select('message', 'created_at as created_at_date')
            ->get();

        $order['deliverydetails'] = DeliveryAddresses::where('id', $order['DeliveryAddressId'])
            ->select('CountryId', 'firstName', 'lastName', 'contactNumber', 'address')
            ->first();

        $order['countryCode'] = Countries::where('id', $order['CountryId'])
            ->pluck('codeIso')
            ->first();

        $product_orderdetails = OrderDetails::leftJoin('productcountries', 'orderdetails.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            // ->leftJoin('producttranslates', 'products.id', '=', 'producttranslates.ProductId')
            ->where('orderdetails.OrderId', $id)
            ->whereNotNull('orderdetails.ProductCountryId')
            // ->where('producttranslates.langCode', 'EN')
            ->select(
                'products.sku',
                // 'producttranslates.name', 
                'orderdetails.qty',
                'orderdetails.price',
                \DB::raw('(orderdetails.qty * orderdetails.price) as total_price')
            )
            ->get()->toArray();
        if ($order->isSubsequentOrder == 0) {
            $plan_orderdetails = OrderDetails::leftJoin('plantrialproducts', 'orderdetails.PlanId', '=', 'plantrialproducts.PlanId')
                ->leftJoin('products', 'plantrialproducts.ProductId', '=', 'products.id')
                // ->leftJoin('producttranslates', 'products.id', '=', 'producttranslates.ProductId')
                ->where('orderdetails.OrderId', $id)
                ->whereNotNull('orderdetails.PlanId')
                // ->where('producttranslates.langCode', 'EN')
                ->select(
                    'products.sku',
                    // 'producttranslates.name', 
                    'orderdetails.qty',
                    'orderdetails.price',
                    \DB::raw('(orderdetails.qty * orderdetails.price) as total_price')
                )
                ->get()->toArray();
        } else {
            $plan_orderdetails = OrderDetails::leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
                ->leftJoin('productcountries', 'plan_details.ProductCountryId', '=', 'productcountries.id')
                ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
                // ->leftJoin('producttranslates', 'products.id', '=', 'producttranslates.ProductId')
                ->where('orderdetails.OrderId', $id)
                ->whereNotNull('orderdetails.PlanId')
                // ->where('producttranslates.langCode', 'EN')
                ->select(
                    'products.sku',
                    // 'producttranslates.name', 
                    'orderdetails.qty',
                    'orderdetails.price',
                    \DB::raw('(orderdetails.qty * orderdetails.price) as total_price')
                )
                ->get()->toArray();
        }

        $order['orderdetails'] = array_merge($product_orderdetails, $plan_orderdetails);

        $order['invoice'] = Receipts::where('OrderId', $id)
            ->select('currency', 'chargeId', 'branchName', 'last4', 'originPrice', 'subTotalPrice', 'discountAmount', 'shippingFee', 'totalPrice')
            ->first();

        // retrieve order info
        $_order = Orders::findorfail($id);

        // retrieve country info
        $_country = Countries::findorfail($order['CountryId']);

        $order['order_no'] = $this->orderService->formatOrderNumber($_order, $_country, false);

        $order['taxInvoiceUrl'] = FileUploads::where('orderId', $order['id'])->pluck('url')->first();

        // Redis::set('orders:details:'.$id, json_encode($order));

        return $order;
    }

    public function updateOrderStatus($params)
    {
        $isRemark = 0;

        if (isset($params["isRemark"])) {
            $isRemark = $params["isRemark"];
        }

        $orderHistory = new OrderHistories();
        $orderHistory->OrderId = $params["id"];
        $orderHistory->message = (string) $params["status"];
        $orderHistory->isRemark = $isRemark;
        $orderHistory->save();

        $orderlog = OrderHistories::where('OrderId', $params["id"])
            ->select('message', 'created_at as created_at_date')
            ->get();

        if ($isRemark === 0 || $isRemark === "0") {
            $updateOrder = Orders::find($params["id"]);
            $updateOrder->status = $params["status"];
            $updateOrder->save();

            $newHistory = new OrderHistories();
            $newHistory->OrderId = $params["id"];
            $newHistory->message = "Order status <" . $params["status"] . "> updated by Admin";
            $newHistory->isRemark = 1;
            $newHistory->save();

            $receiptId = Receipts::where('OrderId', $params["id"])->pluck('id')->first();
            $user = User::find($updateOrder->UserId);

            $emailData = [];
            if ($updateOrder->subscriptionIds !== null) {
                $emailData['moduleData'] = (object) array(
                    'email' => $updateOrder->email,
                    'subscriptionId' => $updateOrder->subscriptionIds,
                    'orderId' => $updateOrder->id,
                    'receiptId' => $receiptId,
                    'userId' => $updateOrder->UserId,
                    'countryId' => $updateOrder->CountryId,
                    'isPlan' => 1
                );
            } else {
                $emailData['moduleData'] = (object) array(
                    'email' => $updateOrder->email,
                    'orderId' => $updateOrder->id,
                    'receiptId' => $receiptId,
                    'userId' => $updateOrder->UserId,
                    'countryId' => $updateOrder->CountryId,
                    'isPlan' => 0
                );
            }
            $emailData['CountryAndLocaleData'] = array($updateOrder->CountryId, $user->defaultLanguage);

            if ($params["status"] === "Delivering") {
                $updatedBilling = $this->updateCycle($updateOrder);
                $emailData['title'] = 'receipt-order-shipped';
                EmailQueueService::addHash($updateOrder->UserId, 'receipt_order_delivery', $emailData);
            }
            if ($params["status"] === "Completed") {
                $updatedBilling = $this->updateCycle($updateOrder);
                $emailData['title'] = 'receipt-order-delivered';
                EmailQueueService::addHash($updateOrder->UserId, 'receipt_order_completed', $emailData);
            }

            if ($params["status"] === "Cancelled") {
                // $updatedBilling = $this->updateCycle($updateOrder);
                $emailData['title'] = 'order-cancelled';
                EmailQueueService::addHash($updateOrder->UserId, 'order_cancelled', $emailData);
            }
        }

        // $this->setOrderDetailsDirectDB($params["id"]);

        return $orderlog;
    }

    public function updateOrderPaymentStatus($params)
    {
        $isRemark = 0;

        if (isset($params["isRemark"])) {
            $isRemark = $params["isRemark"];
        }

        if ($isRemark === 0 || $isRemark === "0") {

            if ($params["status"] === "Refunded") {
                try {
                    $order_country_of_origin = Orders::where('id', (int) $params["id"])->first();
                    if ($order_country_of_origin && $order_country_of_origin->CountryId !== 8) {
                        $order_country = Countries::where('id', $order_country_of_origin->CountryId)->first();
                        $receipt = Receipts::where('OrderId', $order_country_of_origin->id)->first();
                        if ($order_country && $receipt) {
                            $stripe_refund = new StripeBA();
                            $try_refund = $stripe_refund->refundOrderChargeFromAdminPanel(config('app.appType'), $order_country_of_origin->CountryId, $receipt->chargeId);
                            if ($try_refund !== "refund_fail" && $try_refund !== "partial_refund_fail" && $try_refund["status"] === "succeeded") {

                                $updateOrder = Orders::find($params["id"]);
                                $updateOrder->payment_status = $params["status"];
                                $updateOrder->save();

                                $paymentHistory = new PaymentHistories();
                                $paymentHistory->OrderId = $params["id"];
                                $paymentHistory->message = $params["status"];
                                $paymentHistory->isRemark = (int) $isRemark;
                                $paymentHistory->created_by = (int) $params["adminId"];
                                $paymentHistory->updated_by = (int) $params["adminId"];
                                $paymentHistory->save();

                                $paymentlog = PaymentHistories::where('OrderId', $params["id"])
                                    ->select('message', 'created_at as created_at_date')
                                    ->get();

                                return $paymentlog;
                            }
                        }
                    } else if ($order_country_of_origin && $order_country_of_origin->CountryId === 8) {
                        $order_country = Countries::where('id', $order_country_of_origin->CountryId)->first();
                        $receipt = Receipts::where('OrderId', $order_country_of_origin->id)->first();
                        if ($order_country && $receipt) {
                            $nicepay_refund = new NicePayController();
                            $imp_uid = $order_country_of_origin->imp_uid;
                            $merchant_uid = $order_country_of_origin->merchant_uid;
                            $amount = $receipt->totalPrice;
                            $try_refund = $nicepay_refund->nicepayRefundOrder($imp_uid, $merchant_uid, $amount);
                            if ($try_refund["status"] === "success") {
                                $updateOrder = Orders::find($params["id"]);
                                $updateOrder->payment_status = $params["status"];
                                $updateOrder->save();
                                $paymentHistory = new PaymentHistories();
                                $paymentHistory->OrderId = $params["id"];
                                $paymentHistory->message = $params["status"];
                                $paymentHistory->isRemark = (int) $isRemark;
                                $paymentHistory->created_by = (int) $params["adminId"];
                                $paymentHistory->updated_by = (int) $params["adminId"];
                                $paymentHistory->save();

                                $paymentlog = PaymentHistories::where('OrderId', $params["id"])
                                    ->select('message', 'created_at as created_at_date')
                                    ->get();

                                return $paymentlog;
                            }
                        }
                    }
                } catch (Exception $ex) {
                    return $ex;
                }
            }
        } else {
            $paymentHistory = new PaymentHistories();
            $paymentHistory->OrderId = $params["id"];
            $paymentHistory->message = (string) $params["status"];
            $paymentHistory->isRemark = (int) $isRemark;
            $paymentHistory->created_by = (int) $params["adminId"];
            $paymentHistory->updated_by = (int) $params["adminId"];
            $paymentHistory->save();

            $paymentlog = PaymentHistories::where('OrderId', $params["id"])
                ->select('message', 'created_at as created_at_date')
                ->get();

            return $paymentlog;
        }
    }

    public function updateCycle($_order)
    {
        $_subs = Subscriptions::where('id', $_order->subscriptionIds)->first();

        if (isset($_subs)) {
            if ($_subs->startDeliverDate == null && $_subs->isTrial === 1 && $_subs->currentCycle === 1 && $_subs->currentChargeNumber === 0) {
                $trial_duration = '';
                if ($_order->CountryId === 2) {
                    $trial_duration = config('environment.trialPlan.trial_first_delivery_hk');
                } else {
                    $trial_duration = config('environment.trialPlan.trial_first_delivery');
                }
                $future_delivery_date = \App\Helpers\DateTimeHelper::getFutureDates('add', $trial_duration);

                // update subscription for Trial (Start Subscription Journey)
                Subscriptions::where('id', $_order->subscriptionIds)
                    ->where('startDeliverDate', null)
                    ->update([
                        "lastDeliverydate" => Carbon::now()->toDateTime(),
                        "nextDeliverDate" => $future_delivery_date,
                        "nextChargeDate" => $future_delivery_date,
                        "startDeliverDate" => $future_delivery_date,
                    ]);
            }
        }

        $this->setOrderDetailsDirectDB($_order->id);

        return ($_subs !== null ? $_subs->id : "");
    }

    public function updateTrackingNumber($params)
    {

        $updateOrder = Orders::find($params["id"]);
        $updateOrder->deliveryId = (string) $params["trackingNumber"];
        $updateOrder->save();

        $params['status'] = "Delivering";
        $params['isRemark'] = 0;

        $orderlog = $this->updateOrderStatus($params);

        $response = (object) array();
        $response->success = true;
        $response->payload = (object) array(
            'trackingNumber' => $updateOrder->deliveryId,
            'orderlog' => $orderlog,
        );

        return $response;
    }
}
