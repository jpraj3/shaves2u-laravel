<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;
use App\Helpers\PlanHelper;
use App\Helpers\Payment\StripeBA as StripeBA;
use App\Helpers\SubscriptionHelper;

// use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\Products\Product;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductTranslate;
use App\Models\Plans\Plans;
//remove master report
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanTrialProducts;
//remove master report
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use App\Models\Geolocation\Countries;
use App\Models\Geolocation\LanguageDetails;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\FileUploads\FileUploads;
use App\Models\CancellationJourney\CancellationReasonTranslates;
use Illuminate\Support\Facades\Redis;
use App\Models\Rebates\Referral;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Rebates\RewardsIn;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\Subscriptions\SubscriptionsProductAddOns;
use App\Models\Plans\PausePlanHistories;
use App\Models\Reports\QueueReport;
use App\Queue\SendEmailQueueService;
use App\Queue\CSVReportToDownloadQueueService;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;
use Exception;

class DownloadAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
        $this->planHelper = new PlanHelper;
        $this->stripeBA = new StripeBA();
        $this->subscriptionHelper = new SubscriptionHelper();
    }

    public function getOrdersList($options)
    {
        $maxresults = null;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $orders = collect();
        DB::table('orders')
            ->leftJoin('orderdetails', 'orders.id', '=', 'orderdetails.OrderId')
            ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
            ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
            ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
            ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
            ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
            ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->select('orders.id', 'orders.UserId', 'orders.CountryId', 'countries.codeIso', 'orders.created_at', 'orders.taxInvoiceNo', 'orders.email', 'orders.status', 'receipts.totalPrice', 'receipts.currency', 'pl_products.sku', 'pr_products.sku')
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                } else if ($channel === "seller") {
                    return $q->whereNotNull('orders.SellerUserID');
                } else if ($channel === "customer") {
                    return $q->whereNull('orders.SellerUserID');
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('pl_products.sku', $sku)->orWhere('pr_products.sku', $sku);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('orders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('orders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('orders.status', $status);
                }
            })
            ->groupBy('orders.id', 'receipts.totalPrice', 'receipts.currency')
            ->orderBy('orders.created_at', 'DESC')
            ->distinct()
            // ->get();
            ->chunk(1000, function ($orders) use (&$orders_final, $search) {
                foreach ($orders as $key => $ord) {
                    // retrieve order info
                    $_order = Orders::findorfail($ord->id);

                    // retrieve country info
                    $_country = Countries::findorfail($ord->CountryId);

                    $ord->order_no = $this->orderService->formatOrderNumber($_order, $_country, false);

                    if ($search !== null && $search !== "") {
                        if (strpos($ord->email . $ord->taxInvoiceNo . $ord->order_no, $search) !== false) { //if found
                            $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
                                ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
                                ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
                                ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
                                ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
                                ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
                                ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku, ""),ifnull(pr_products.sku,"")) separator ", ") as skus'))
                                ->groupBy('orderdetails.OrderId')->pluck('skus');

                            $ord->taxinvoiceurl = FileUploads::where('orderId', $ord->id)->pluck('url')->first();
                        } else { //remove record
                            $orders->forget($key);
                        }
                    } else {
                        $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
                            ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
                            ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
                            ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
                            ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
                            ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
                            ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku, ""),ifnull(pr_products.sku,"")) separator ", ") as skus'))
                            ->groupBy('orderdetails.OrderId')->pluck('skus');

                        $ord->taxinvoiceurl = FileUploads::where('orderId', $ord->id)->pluck('url')->first();
                    }
                }

                $orders_final = $orders_final->merge($orders);
            });

        // foreach ($orders as $ord) {

        //     $ord->skus = OrderDetails::where('orderdetails.OrderId', $ord->id)
        //         ->leftJoin('plan_details', 'orderdetails.PlanId', '=', 'plan_details.PlanId')
        //         ->leftJoin('productcountries as pl_pcs', 'plan_details.ProductCountryId', '=', 'pl_pcs.id')
        //         ->leftJoin('productcountries as pr_pcs', 'orderdetails.ProductCountryId', '=', 'pr_pcs.id')
        //         ->leftJoin('products as pl_products', 'pl_pcs.ProductId', '=', 'pl_products.id')
        //         ->leftJoin('products as pr_products', 'pr_pcs.ProductId', '=', 'pr_products.id')
        //         ->select(DB::raw('group_concat(distinct concat(ifnull(pl_products.sku, ""),ifnull(pr_products.sku,"")) separator ", ") as skus'))
        //         ->groupBy('orderdetails.OrderId')->pluck('skus');

        //     // retrieve order info
        //     $_order = Orders::findorfail($ord->id);

        //     // retrieve country info
        //     $_country = Countries::findorfail($ord->CountryId);

        //     $ord->order_no = $this->orderService->formatOrderNumber($_order, $_country, false);
        //     // $ord->created_at = date('d-m-y H:i', strtotime($ord->created_at));
        // }

        // $orders_final = $orders->filter(function ($order) use ($search) {
        //     if ($search !== null && $search !== "") {
        //         return (strpos($order->email, $search) || strpos($order->taxInvoiceNo, $search) || strpos($order->order_no, $search));
        //     } else {
        //         return true;
        //     }
        // });

        return $orders_final;
    }

    public function getBulkOrdersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $bulkorders = DB::table('bulkorders')
            ->leftJoin('bulkorderdetails', 'bulkorders.id', '=', 'bulkorderdetails.BulkOrderId')
            ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            ->leftJoin('plans', 'bulkorderdetails.PlanId', '=', 'plans.id')
            ->leftJoin('deliveryaddresses', 'bulkorders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->select('products.id as proid', 'plans.description as plandescription', 'bulkorderdetails.ProductCountryId as ProductCountryId', 'bulkorderdetails.PlanCountryId as PlanCountryId', 'bulkorderdetails.qty as qty', 'bulkorderdetails.price as price', 'bulkorderdetails.currency as currency', 'bulkorders.paymentType as paymentType', 'bulkorders.id', 'countries.code', 'countries.codeIso', 'countries.id as cid', 'bulkorders.created_at', 'bulkorders.taxInvoiceNo', 'bulkorders.email', 'bulkorders.status', 'products.sku')
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('products.sku', $sku);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('bulkorders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('bulkorders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('bulkorders.status', $status);
                }
            })
            ->groupBy('bulkorders.id')
            ->orderBy('bulkorders.created_at', 'DESC')
            ->distinct()
            ->get();

        foreach ($bulkorders as $ord) {
            $ord->skus = BulkOrderDetails::where('bulkorderdetails.BulkOrderId', $ord->id)
                ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
                ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
                ->select(DB::raw('group_concat(distinct products.sku separator ", ") as skus'))
                ->groupBy('bulkorderdetails.BulkOrderId')
                ->pluck('skus');

            if ($ord->ProductCountryId) {
                $ord->pname = ProductTranslate::select('name')->where('ProductId', $ord->proid)->where('langCode', 'EN')->first()->name;
            } else if ($ord->plandescription) {
                $ord->pname = $ord->plandescription;
            }
            $ord->totalPrice = $ord->qty * $ord->price;
            if ($ord->totalPrice <= 0) {
                $ord->totalPrice = "0";
            }
            $_order = BulkOrders::findorfail($ord->id);
            $_country = Countries::findorfail($ord->cid);
            $ord->bulkorder_no = $this->orderService->formatBulkOrderNumber($_order, $_country, false) . '-' . date('Ymd', strtotime($ord->created_at));
            // $ord->created_at = date('d-m-y H:i', strtotime($ord->created_at));
        }

        $bulkorders_final = $bulkorders->filter(function ($bulkorder) use ($search) {
            if ($search !== null && $search !== "") {
                return (strpos($bulkorder->email, $search) || strpos($bulkorder->taxInvoiceNo, $search) || strpos($bulkorder->bulkorder_no, $search));
            } else {
                return true;
            }
        });

        return $bulkorders;
    }

    public function getReferralsList($options)
    {
        $maxresults = 50;
        $id = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $referrals = DB::table('referrals')->leftJoin('users', 'referrals.friendId', '=', 'users.id')
            ->leftJoin('users as ruser', 'referrals.referralId', '=', 'ruser.id')
            ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
            ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->select('ruser.email as remail', 'referrals.created_at as created_at', 'referrals.rewardId as rewardId', 'referrals.status as status', 'referrals.firstPurchase as firstPurchase', 'referrals.source as source', 'referrals.referralId as rid', 'referrals.friendId as fid', 'users.email as femail', 'countries.id as cid', 'countries.code as countrycode', 'rewardsins.OrderId as OrderId', 'rewardsins.value as rewvalue', 'countries.currencyDisplay as currencyDisplay')
            ->get();
        foreach ($referrals as $r) {
            $referrallist = Referral::leftJoin('users', 'referrals.referralId', '=', 'users.id')
                ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
                ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
                ->where('referrals.referralId', $r->rid)
                ->select(DB::raw('count(*) as conversion'), DB::raw('SUM(rewardsins.value) as rewvalue'), 'countries.currencyDisplay as currencyDisplay')
                ->groupBy('referrals.referralId', 'countries.code')->first();
            $r->currencyget = $referrallist->currencyDisplay;
            $r->conversion = $referrallist->conversion;
            if ($r->conversion < 0) {
                $r->conversion = "0";
            }
            $r->totalearn = $referrallist->rewvalue;
            $r->withdrawn_total = ReferralCashOut::where('UserId', $r->rid)->where('CountryId', $r->cid)->where('status', 'withdrawn')->sum('amount');
            $withdrawal = ReferralCashOut::where('UserId', $r->rid)->where('CountryId', $r->cid)->where('status', 'withdrawn')->first();
            if ($withdrawal) {
                $r->withdrawn_date = date('Ymd', strtotime($withdrawal->created_at));
            } else {
                $r->withdrawn_date = null;
            }
            $r->curenttotal =  $r->totalearn - $r->withdrawn_total;
            if ($r->curenttotal) {
                if ($r->curenttotal < 0) {
                    $r->curenttotal = $r->currencyget . " 0";
                } else {
                    $r->curenttotal = $r->currencyget . " " . $r->curenttotal;
                }
            } else {
                $r->curenttotal = $r->currencyget . " 0";
            }
            if ($r->totalearn) {
                if ($r->totalearn < 0) {
                    $r->totalearn = $r->currencyget . " 0";
                } else {
                    $r->totalearn = $r->currencyget . " " . $r->totalearn;
                }
            } else {
                $r->totalearn = $r->currencyget . " 0";
            }
            // $r->created_at = date('d-m-y H:i', strtotime($r->created_at));
            if ($r->OrderId) {
                $_order = Orders::findorfail($r->OrderId);
                // retrieve country info
                $_country = Countries::findorfail($r->cid);
                $r->OrderId = $this->orderService->formatOrderNumber($_order, $_country, false) . '-' . date('Ymd', strtotime($r->created_at));
            } else {
                $r->OrderId = null;
            }
            if ($r->source == "link") {
                $r->source = "Link";
            } else if ($r->source == "fb") {
                $r->source = "Facebook";
            } else if ($r->source == "email") {
                $r->source = "Facebook";
            }

            if ($r->firstPurchase == "1" && $r->rewardId != null && $r->status == 'Active') {
                $r->newstatus = "Registered & Purchased";
            } else if ($r->firstPurchase == "0" && $r->rewardId == null && $r->status == 'Inactive') {
                $r->newstatus = "Registered";
            } else if ($r->firstPurchase != null && $r->rewardId == null && $r->status == 'Active') {
                $r->newstatus = "Registered";
            } else if ($r->firstPurchase == null && $r->rewardId == null && $r->status == null) {
                $r->newstatus = "-";
            } else {
                $r->newstatus = "-";
            }

            $_firstorder = Orders::where('UserId', $r->fid)->first();
            if ($withdrawal) {
                $r->firstorderDate = date('Ymd', strtotime($_firstorder->created_at));
            } else {
                $r->firstorderDate = null;
            }
        }

        return $referrals;
    }

    public function getReferralsCashOutList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $status = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["search"]) && $options["search"] !== null && $options["search"] !== "") {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $referrals = DB::table('referralcashouts')->leftJoin('countries', 'referralcashouts.CountryId', '=', 'countries.id')
        ->select('countries.currencyDisplay as currencyDisplay','countries.name as cname','referralcashouts.*')
           ->when($search != null, function ($q) use ($search) {
               return $q->where('referralcashouts.email', 'LIKE', '%'.$search.'%');
           })
           ->when($status != null, function ($q) use ($status) {
               if ($status === "all") {
                   return $q;
               } else {
                   return $q->where('referralcashouts.status', $status);
               }
           })
           ->when($fromdate != null, function ($q) use ($fromdate) {
               return $q->whereDate('referralcashouts.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
           })
           ->when($todate != null, function ($q) use ($todate) {
               return $q->whereDate('referralcashouts.created_at', '<=', date("Y-m-d", strtotime($todate)));
           })
           // ->groupBy('referrals.referralId', 'countries.code')
           ->paginate($maxresults, ['*'], 'page', $pagenum);
           foreach ($referrals as $r) {
               $withdrawn_total = ReferralCashOut::where('UserId', $r->UserId)->where('CountryId', $r->CountryId)->where('status', 'withdrawn')->sum('amount');
               $rewards_total = RewardsIn::where('UserId', $r->UserId)->where('CountryId', $r->CountryId)->sum('value');
               if($withdrawn_total){
                   if($rewards_total){
                       $r->availablemoney = (float) $rewards_total - (float) $withdrawn_total;
                    }else{
                    $r->availablemoney = 0;
                    }
               }
               else{
                if($rewards_total){
                   $r->availablemoney = $rewards_total;
                }else{
                $r->availablemoney = 0;
                }
             }
           }
       return $referrals;
    }

    public function getSubscribersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $plantype = null;
        $sku = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["plantype"])) {
            $plantype = $options["plantype"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        // DB::raw('(SELECT subscriptionhistories.created_at FROM subscriptionhistories WHERE subscriptionhistories.subscriptionId = subscriptions.id and subscriptionhistories.message = "Canceled" or subscriptionhistories.message = "Cancelled" order by subscriptionhistories.created_at desc limit 1)')

        $subs_final = collect();
        DB::table('subscriptions')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->leftJoin('rechargehistories', 'rechargehistories.subscriptionId', 'subscriptions.id')
            ->leftJoin('countries', 'plans.CountryId', '=', 'countries.id')
            ->leftJoin('plansku', 'plans.PlanSkuId', '=', 'plansku.id')
            ->leftJoin('deliveryaddresses as ShippingAddress', 'subscriptions.DeliveryAddressId', '=', 'ShippingAddress.id')
            ->leftJoin('deliveryaddresses as BillingAddress', 'subscriptions.BillingAddressId', '=', 'BillingAddress.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->leftJoin('sellerusers', 'subscriptions.SellerUserId', '=', 'sellerusers.id')
            ->leftJoin('marketingoffices', 'subscriptions.MarketingOfficeId', '=', 'marketingoffices.id')
            ->leftJoin('marketingoffices as smarketingoffices', 'sellerusers.MarketingOfficeId', '=', 'smarketingoffices.id')
            ->leftJoin('pause_plan_histories', function ($query) {
                $query->on('pause_plan_histories.subscriptionIds', '=', 'subscriptions.id');
                $query->take(1);
            })
            ->select(
                'subscriptions.id as id',
                'subscriptions.*',
                'subscriptions.status as substatus',
                'subscriptions.created_at as subcreatedat',
                'subscriptions.updated_at as subupdatedAt',
                'subscriptions.cancellationReason as subcancellationReason',
                'ShippingAddress.id as shippingaddressid',
                'ShippingAddress.address as saddress',
                'ShippingAddress.portalCode  as sportalCode',
                'ShippingAddress.city as scity',
                'ShippingAddress.state as sstate',
                'BillingAddress.id as billingaddressid',
                'BillingAddress.address as baddress',
                'BillingAddress.portalCode  as bportalCode',
                'BillingAddress.city as bcity',
                'BillingAddress.state as bstate',
                'countries.codeIso as countrycodeIso',
                'countries.name as countryName',
                'plansku.duration',
                'plansku.sku',
                'plansku.isAnnual',
                'sellerusers.badgeId',
                'marketingoffices.moCode',
                'smarketingoffices.moCode as smoCode',
                'pause_plan_histories.id as pauseid',
                'users.id as uid',
                'users.firstName as ufirstName',
                'users.lastName as ulastName'
            )
            ->when($search != null, function ($q) use ($search) {
                return $q->where('subscriptions.email', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('plans.CountryId', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('plans.PlanSkuId', $sku);
                }
            })
            ->when($plantype != null, function ($q) use ($plantype) {
                if ($plantype === "trial") {
                    return $q->where('subscriptions.isTrial', 1);
                } else if ($plantype === "custom") {
                    return $q->where('subscriptions.isCustom', 1);
                } else {
                    return $q;
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptions.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptions.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('subscriptions.status', $status);
                }
            })
            ->orderBy('subscriptions.created_at', 'DESC')
            // ->distinct()
            ->chunk(1000, function ($subscriptions) use (&$subs_final) {
                foreach ($subscriptions as $sub) {
                    $_order = Orders::select('id', 'created_at')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    // $current_order = Orders::select('status')->where("subscriptionIds", $sub->id)->orderBy('created_at', 'desc')->first();
                    if (isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ufirstName . " " . $sub->ulastName;
                    } else if (!isset($sub->ufirstName) && isset($sub->ulastName)) {
                        $sub->ufullname = $sub->ulastName;
                    } else {
                        $sub->ufullname = $sub->ufirstName;
                    }

                    if ($_order !== null) {
                        $sub->formattedId = $this->orderService->formatOrderNumberV3($_order->id, $_order->created_at, $sub->countrycodeIso, true);
                        if ($sub->isTrial === 1 && $sub->convertTrial2Subs) {
                            //take second order
                            $sub->conversion_date = isset($sub->convertTrial2Subs) ? $sub->convertTrial2Subs : '-';
                        } else {
                            $sub->conversion_date = null;
                        }
                        // $sub->currentOrderStatus = null;
                        // foreach ($_order as $order) {
                        //     if($order->id ==  $sub->currentOrderId){
                        //         $sub->currentOrderStatus = $order->status;
                        //     }
                        // }
                    } else {
                        $sub->formattedId = "";
                    }

                    // if ($current_order !== null) {
                    //     $sub->currentOrderStatus = $current_order->status;
                    // }


                    $sub->created_date = $sub->subcreatedat;

                    if ($sub->shippingaddressid) {
                        $sub->deliveryaddress = $sub->saddress . " " . $sub->sportalCode . " " . $sub->scity . " " . $sub->sstate;
                    } else {
                        $sub->deliveryaddress = "";
                    }

                    if ($sub->billingaddressid) {
                        $sub->billingaddress = $sub->baddress . " " . $sub->bportalCode . " " . $sub->bcity . " " . $sub->bstate;
                    } else {
                        $sub->billingaddress = "";
                    }

                    if ($sub->MarketingOfficeId) {
                        $sub->moCode = $sub->moCode;
                    } else {
                        if ($sub->smoCode) {
                            $sub->moCode = $sub->smoCode;
                        } else {
                            $sub->moCode = "";
                        }
                    }

                    // $sub->cancellationReason = "";
                    // $sub->canceledDate = "";
                    // if ($sub->substatus == 'Cancelled') {
                    //     $sub->cancellationReason = $sub->subcancellationReason;
                    //     if ($sub->canceldate) {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->canceldate));
                    //     }
                    //     else {
                    //         $sub->canceledDate = date('d/m/y', strtotime($sub->subupdatedAt));
                    //     }
                    //   }

                    if ($sub->substatus == "Cancelled") {
                        $cancellation = DB::table('cancellation_journeys')->where('SubscriptionId', $sub->id)->orderBy('created_at', 'DESC')->first();
        
                        if (isset($cancellation)) {
                            $sub->cancellationcreated_date = $cancellation->created_at;
                            $sub->cancellationotherreason = $cancellation->OtherReason;
                            $cancellationTranslates = CancellationReasonTranslates::where("id", $cancellation->CancellationTranslatesId)->first();
                            if ($cancellationTranslates) {
                                $sub->cancellationdefaultContent = $cancellationTranslates->defaultContent;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        } else {
                            $cancellation1 = DB::table('subscriptionhistories')
                                ->where('subscriptionId', $sub->id)
                                ->where(function ($query) {
                                    $query->orWhere('message',  'Canceled')
                                        ->orWhere('message', 'Cancelled');
                                })
                                ->orderBy('created_at', 'DESC')
                                ->first();

                            if (isset($cancellation1)) {
                                $sub->cancellationcreated_date = $cancellation1->created_at;
                            }
                        }

                        if (isset($sub->cancellationdefaultContent)) {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason . "," . $sub->cancellationdefaultContent;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        } else {
                            if (isset($sub->subcancellationReason) && $sub->subcancellationReason != null) {
                                $sub->cancellationdefaultContent = $sub->subcancellationReason;
                            } else {
                                $sub->cancellationdefaultContent = "";
                            }
                        }
                    } else if ($sub->substatus === "Payment Failure") {
                        $sub->cancellation = DB::table('subscriptionhistories')
                            ->where('subscriptionId', $sub->id)
                            ->where('message', 'Payment Failure')
                            ->orderBy('created_at', 'DESC')
                            ->first();

                        if (isset($sub->cancellation)) {
                            $sub->cancellationcreated_date = $sub->cancellation->created_at;
                        }

                        if ($sub->subcancellationReason === "Cancelled due to payment failed" || $sub->subcancellationReason === "Canceled due to payment failed" || $sub->subcancellationReason === "Cancelled due to user card has insufficient funds.") {
                            $sub->cancellationdefaultContent = $sub->subcancellationReason;
                        } else {
                            $sub->cancellationdefaultContent = "-";
                        }
                    }

                    $sub->blade_type = null;
                    $sub->shave_cream = null;
                    $sub->after_shave_cream = null;
                    $sub->pouch = null;
                    $sub->rubber_handle = null;

                    $products = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('plan_details', 'productcountries.id', '=', 'plan_details.ProductCountryId')
                        ->where('plan_details.PlanId', $sub->PlanId)
                        ->select('products.*', 'plan_details.qty as pd_qty')
                        ->get();

                    foreach ($products as $product) {
                        if (in_array($product->sku, config('global.all.blade_types.skus'))) {
                            $sub->blade_type = config('global.all.blade_no_v2.' . $product->sku) . ' blade';
                            $sub->blade_quantity = $product->pd_qty;
                        }
                    }

                    // if ($sub->isTrial === 1 && ($sub->currentCycle === 1 || $sub->currentCycle === 0)) {
                    //     $sub->blade_quantity = 1;
                    // }

                    $addons = DB::table('products')->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
                        ->leftJoin('subscriptions_productaddon', 'productcountries.id', '=', 'subscriptions_productaddon.ProductCountryId')
                        ->where('subscriptions_productaddon.subscriptionId', $sub->id)
                        ->select('products.*')
                        ->get();

                    foreach ($addons as $addon) {
                        if (in_array($addon->sku, config('global.all.aftershavecream_types.skus'))) {
                            $sub->after_shave_cream = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.shavecream_types.skus'))) {
                            $sub->shave_cream = $product->id;
                        }
                        if (in_array($addon->sku, config('global.all.pouch_types.skus'))) {
                            $sub->pouch = $addon->id;
                        }
                        if (in_array($addon->sku, config('global.all.razor_potector.skus'))) {
                            $sub->rubber_handle = $addon->id;
                        }
                    }


                    if ($sub->pauseid) {
                        $sub->pauseplan = "Yes";
                    } else {
                        $sub->pauseplan = "No";
                    }
                    $sub->reactivatedplan = null;

                    if ($sub->currentDeliverNumber == 0 && $sub->isCustom == 1 && $sub->isTrial != 1 && ($sub->substatus !== "Payment Failure" && $sub->substatus !== "On Hold" && $sub->substatus !== "Pending")) {
                        $sub->currentDeliverNumber = "1";
                    }
                }
                $subs_final = $subs_final->merge($subscriptions);
            });

        return $subs_final;
    }

    public function sendSubscribersQueue($options, $reportType)
    {
        try {
            $maxresults = 50;
            $search = null;
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($options["maxresults"])) {
                $maxresults = $options["maxresults"];
            }

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } else {
                if (isset($options["fromdate"])) {
                    $fromdate = $options["fromdate"];
                }

                if (isset($options["todate"])) {
                    $todate = $options["todate"];
                }
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["plantype"])) {
                $plantype = $options["plantype"];
            }

            if (isset($options["sku"])) {
                $sku = $options["sku"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }

            if (isset($options["pagenum"])) {
                $pagenum = $options["pagenum"];
            }

            if ($options["fromdate"] !== null) {
                $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
            }
            if ($options["todate"] !== null) {
                $options["todate"] = str_replace("/", "-", $options["todate"]);
            }

            //format filters
            $filter_status = "subscribers-report-";
            $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
            $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
            $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $file_id = Str::uuid()->toString();
            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 1;
            $fileupload->reportType = 'subscribers-report';
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry

            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();
            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'maxresults' => $maxresults,
                'search' => $search,
                'country' => $country,
                'plantype' =>  $plantype,
                'sku' => $sku,
                'status' => $status,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'pagenum' => $pagenum,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function sendSubscribersSummaryQueue($options, $reportType)
    {
        try {
            $maxresults = 50;
            $search = null;
            $country = null;
            $plantype = null;
            $sku = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($options["maxresults"])) {
                $maxresults = $options["maxresults"];
            }

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } else {
                if (isset($options["fromdate"])) {
                    $fromdate = $options["fromdate"];
                }

                if (isset($options["todate"])) {
                    $todate = $options["todate"];
                }
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["plantype"])) {
                $plantype = $options["plantype"];
            }

            if (isset($options["sku"])) {
                $sku = $options["sku"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }

            if (isset($options["pagenum"])) {
                $pagenum = $options["pagenum"];
            }

            //format filters
            $filter_status = "subscribers-report-";
            $filter_fromdate = "11/01/2017";
            $filter_todate = Carbon::now()->format('m/d/Y');
            $filter_fromdate2 = "11-01-2017";
            $filter_todate2 = Carbon::now()->format('m-d-Y');
            $date_text = $filter_fromdate2 . '-to-' . $filter_todate2 ;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.csv';
            $fromdate =   $filter_fromdate;
            $todate = $filter_todate;
            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $file_id = Str::uuid()->toString();
            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 1;
            $fileupload->reportType = 'subscribers-summary-report';
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry

            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();
            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'maxresults' => $maxresults,
                'search' => $search,
                'country' => $country,
                'plantype' =>  $plantype,
                'sku' => $sku,
                'status' => $status,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'pagenum' => $pagenum,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function sendReportsMasterQueue($options, $reportType)
    {
        try {
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } else {
                if (isset($options["fromdate"])) {
                    $fromdate = $options["fromdate"];
                }

                if (isset($options["todate"])) {
                    $todate = $options["todate"];
                }
            }

            if (isset($options["channel"])) {
                $channel = $options["channel"];
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }

            if ($options["fromdate"] !== null) {
                $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
            }
            if ($options["todate"] !== null) {
                $options["todate"] = str_replace("/", "-", $options["todate"]);
            }
            //format filters
            $filter_status = "sales report-";
            $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
            $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
            $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
            $file_id = Str::uuid()->toString();

            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 1;
            $fileupload->reportType = 'sales-report';
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry
            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();


            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'search' => $search,
                'country' => $country,
                'channel' => $channel,
                'status' => $status,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function sendReportsMasterSummaryQueue($options, $reportType)
    {
        try {
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } 

            if (isset($options["channel"])) {
                $channel = $options["channel"];
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }
          
  
            //format filters
            $filter_status = "sales report-";
            $filter_fromdate = "11/01/2017";
            $filter_todate = Carbon::now()->format('m/d/Y');
            $filter_fromdate2 = "11-01-2017";
            $filter_todate2 = Carbon::now()->format('m-d-Y');
            $date_text = $filter_fromdate2 . '-to-' . $filter_todate2 ;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.csv';
            $file_id = Str::uuid()->toString();
            $fromdate =   $filter_fromdate;
            $todate = $filter_todate;
            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 1;
            $fileupload->reportType = 'sales-summary-report';
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry
            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();


            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'search' => $search,
                'country' => $country,
                'channel' => $channel,
                'status' => $status,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function sendReportsAppcoQueue($options, $reportType)
    {
        try {
            $maxresults = 50;
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($options["maxresults"])) {
                $maxresults = $options["maxresults"];
            }

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } else {
                if (isset($options["fromdate"])) {
                    $fromdate = $options["fromdate"];
                }

                if (isset($options["todate"])) {
                    $todate = $options["todate"];
                }
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }

            if (isset($options["pagenum"])) {
                $pagenum = $options["pagenum"];
            }
            if ($options["fromdate"] !== null) {
                $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
            }
            if ($options["todate"] !== null) {
                $options["todate"] = str_replace("/", "-", $options["todate"]);
            }

            //format filters
            $filter_status = "sales report(appco)-";
            $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
            $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
            $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
            $file_id = Str::uuid()->toString();

            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 0;
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry
            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();


            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'search' => $search,
                'country' => $country,
                'channel' => $channel,
                'status' => $status,
                'pagenum' => $pagenum,
                'maxresults' => $maxresults,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function sendReportsMOQueue($options, $reportType)
    {
        try {
            $maxresults = 50;
            $search = null;
            $channel = null;
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;

            if (isset($options["maxresults"])) {
                $maxresults = $options["maxresults"];
            }

            if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
                $search = $options["search"];
            } else {
                if (isset($options["fromdate"])) {
                    $fromdate = $options["fromdate"];
                }

                if (isset($options["todate"])) {
                    $todate = $options["todate"];
                }
            }

            if (isset($options["country"])) {
                $country = $options["country"];
            }

            if (isset($options["status"])) {
                $status = $options["status"];
            }

            if (isset($options["pagenum"])) {
                $pagenum = $options["pagenum"];
            }
            if ($options["fromdate"] !== null) {
                $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
            }
            if ($options["todate"] !== null) {
                $options["todate"] = str_replace("/", "-", $options["todate"]);
            }
            //format filters
            $filter_status = "sales report(mo)-";
            $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
            $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
            $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
            $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
            $file_id = Str::uuid()->toString();

            $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

            $fileupload = new FileUploads();
            $fileupload->name = $filename;
            $fileupload->isReport = 0;
            $fileupload->file_id = $file_id;
            $fileupload->status = 'Processing';
            $fileupload->isDownloaded = 0;
            $fileupload->AdminId = (int) $options["adminId"];
            $fileupload->save();
            //Create new fileUpload entry
            $queuereport = new QueueReport();
            $queuereport->type = $reportType;
            $queuereport->status = 'Pending';
            $queuereport->fileuploadsId =  $fileupload->id;
            $queuereport->created_at = Carbon::now();
            $queuereport->updated_at = Carbon::now();
            $queuereport->save();


            $_redis_report_data = [
                'reportType' =>  $reportType,
                'queueid' =>  $queuereport->id,
                'fileuploadid' =>  $fileupload->id,
                'search' => $search,
                'country' => $country,
                'channel' => $channel,
                'status' => $status,
                'pagenum' => $pagenum,
                'maxresults' => $maxresults,
                'fromdate' => $fromdate,
                'todate' => $todate,
                'filename' => $filename,
                'file_id' => $file_id,
                'url' => $url
            ];
            CSVReportToDownloadQueueService::addHash($queuereport->id, $_redis_report_data);
            return 0;
        } catch (Exception $e) {
            // FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function getCancellationsList($options)
    {
        $maxresults = 50;

        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $cancellations = DB::table('cancellation_journeys')->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
        })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->orderBy('created_at', 'DESC')
            ->distinct()
            ->get();

        $cancellation_journeys_Ids = [];
        foreach ($cancellations as $subIds) {
            array_push($cancellation_journeys_Ids, $subIds->SubscriptionId);
        }

        $admincancellations = Subscriptions::join('subscriptionhistories', 'subscriptionhistories.subscriptionId', 'subscriptions.id')
            ->where('subscriptions.status', 'Cancelled')
            ->whereIn('subscriptionhistories.message', ['Canceled', 'Cancelled'])
            ->whereNotNull('subscriptionhistories.detail')
            ->where('subscriptionhistories.detail', 'Cancelled by Admin')
            ->whereNotIn('subscriptions.id', $cancellation_journeys_Ids)
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptionhistories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptionhistories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->select(
                'subscriptions.id',
                'subscriptions.email',
                'subscriptions.UserId',
                'subscriptions.id as SubscriptionId',
                'subscriptions.isTrial',
                'subscriptions.isCustom',
                'subscriptionhistories.message',
                'subscriptionhistories.detail',
                'subscriptionhistories.created_at',
                'subscriptionhistories.updated_at'
            )
            ->orderBy('subscriptionhistories.created_at', 'DESC')
            ->distinct()
            ->get();

        // $paymentfailures = Subscriptions::join('subscriptionhistories', 'subscriptionhistories.subscriptionId', 'subscriptions.id')
        //     ->where('subscriptions.status', 'Payment Failure')
        //     ->whereIn('subscriptionhistories.message', ['Payment Failure'])
        //     ->when($fromdate != null, function ($q) use ($fromdate) {
        //         return $q->whereDate('subscriptionhistories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
        //     })
        //     ->when($todate != null, function ($q) use ($todate) {
        //         return $q->whereDate('subscriptionhistories.created_at', '<=', date("Y-m-d", strtotime($todate)));
        //     })
        //     ->select('subscriptions.id as SubscriptionId', 'subscriptionhistories.message', 'subscriptionhistories.detail')
        //     ->orderBy('subscriptionhistories.created_at', 'DESC')
        //     ->distinct()
        //     ->get();


        $cancellationreasontranslate = CancellationReasonTranslates::get();
        foreach ($cancellations as $can) {
            $content = "";
            $defaultContent = "";
            // $can->created_date = date('d-m-y H:i', strtotime($can->created_at));
            // $can->updated_date = date('d-m-y H:i', strtotime($can->updated_at));
            if ($can->CancellationTranslatesId) {

                if (strpos($can->CancellationTranslatesId, ',') !== false) {
                    $splittranslate = explode(",", $can->CancellationTranslatesId);
                    $countst = 0;
                    foreach ($splittranslate as $st) {
                        foreach ($cancellationreasontranslate as $ct) {
                            if ($st == $ct->id) {
                                if ($countst == 0) {
                                    $content = $content . $ct->content;
                                    $defaultContent = $defaultContent . $ct->defaultContent;
                                } else {
                                    $content = $content . '<br>' . $ct->content;
                                    $defaultContent = $defaultContent . '<br>' . $ct->defaultContent;
                                }
                                $countst++;
                            }
                        }
                    }
                } else {
                    foreach ($cancellationreasontranslate as $ct) {
                        if ($can->CancellationTranslatesId == $ct->id) {
                            $content = $content . $ct->content;
                            $defaultContent = $defaultContent . $ct->defaultContent;
                        }
                    }
                }
            }
            $can->content = $content;
            $can->defaultContent = $defaultContent;
            $can->userActionText = $this->getUserCancellationActions($can->modifiedBlade, $can->modifiedFrequency, $can->discountPercent, $can->hasGetFreeProduct, $can->hasPausedSubscription);

            $can->subscription = Subscriptions::find($can->SubscriptionId);
            $can->plan = Plans::find($can->subscription->PlanId);
            $can->country = Countries::find($can->plan->CountryId);
            $can->plandetails = $this->planHelper->getPlanDetailsByPlanIdV2($can->plan->id, $can->country->id);

            $can->blade_type = "-";
            $can->blade_quantity = "-";
            $can->frequency = "-";
            if ($can->plandetails != null) {
                $can->frequency = $can->plandetails['planduration'] . " months";
                $products = $can->plandetails['product_info'];

                foreach ($products as $product) {
                    if (in_array($product['sku'], config('global.all.blade_types.skus'))) {
                        $can->blade_type = config('global.all.blade_no.' . $product['sku']) . ' blade';
                        $can->blade_quantity = $product['product_qty'];
                    }
                }
            }
        }

        foreach ($admincancellations as $aCan) {
            $aCan->CancellationTranslatesId = null;
            $aCan->OtherReason = null;
            $aCan->OrderId = null;
            $aCan->hasCancelled = 1;
            $aCan->hasChangedBlade = 0;
            $aCan->modifiedBlade = 0;
            $aCan->hasChangedFrequency = 0;
            $aCan->modifiedFrequency = 0;
            $aCan->hasGetFreeProduct = 0;
            $aCan->hasGetDiscount = 0;
            $aCan->discountPercent = null;
            $aCan->hasPausedSubscription = 0;
            $content = $aCan->detail;
            $defaultContent = $aCan->detail;
            $aCan->content = $content;
            $aCan->defaultContent = $defaultContent;
            $aCan->userActionText = "";
            $aCan->planType = "-";
            $aCan->subscriptionName = "-";
            $aCan->subscription = Subscriptions::find($can->SubscriptionId);
            $aCan->plan = Plans::find($can->subscription->PlanId);
            $aCan->country = Countries::find($can->plan->CountryId);
            $aCan->plandetails = $this->planHelper->getPlanDetailsByPlanIdV2($aCan->plan->id, $aCan->country->id);
            $aCan->blade_type = "-";
            $aCan->blade_quantity = "-";
            $aCan->frequency = "-";
            if ($aCan->plandetails != null) {
                $aCan->frequency = $aCan->plandetails['planduration'] . " months";
                $products = $aCan->plandetails['product_info'];

                foreach ($products as $product) {
                    if (in_array($product['sku'], config('global.all.blade_types.skus'))) {
                        $aCan->blade_type = config('global.all.blade_no.' . $product['sku']) . ' blade';
                        $aCan->blade_quantity = $product['product_qty'];
                    }
                }

                $aCan->planType = $aCan->isTrial == 1 ? 'Trial' : 'Custom';
                $aCan->subscriptionName = $aCan->blade_type . " every " . $aCan->frequency;
            }
        }

        $merged_data = [];

        foreach ($cancellations as $c) {
            array_push($merged_data, $c);
        }
        foreach ($admincancellations as $cA) {
            array_push($merged_data, $cA);
        }
        return $merged_data;
    }

    public function getUserCancellationActions($changedCartridge, $changedPlan, $getDiscount, $getNewBlade, $onHoldSubscription)
    {
        $userActionText = "";
        if ($changedCartridge !== null) {
            $userActionText = $userActionText . "Changed to " . $changedCartridge . ". ";
        }
        if ($changedPlan !== null) {
            $userActionText = $userActionText . "Changed to " . $changedPlan . ". ";
        }
        if ($getDiscount !== null) {
            $userActionText = $userActionText . "Get " . $getDiscount . "% discount. ";
        }
        if ($getNewBlade !== 0) {
            $userActionText = $userActionText . "Get new blade. ";
        }
        if ($onHoldSubscription !== 0) {
            $userActionText = $userActionText . "Onhold subscription. ";
        }
        if ($changedCartridge === null && $changedPlan === null && $getDiscount === null && $getNewBlade === 0 && $onHoldSubscription === 0) {
            $userActionText = "-";
        }
        return $userActionText;
    }

    public function getReportsMasterList($options)
    {
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        $reports = collect();
        DB::table('salereports')
            ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
            ->leftJoin('bulkorders', 'salereports.bulkOrderId', '=', 'bulkorders.id')
            // ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            // ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->leftJoin('subscriptions', 'subscriptions.id', '=', 'orders.subscriptionIds')
            ->leftJoin('cards', 'cards.id', '=', 'subscriptions.CardId')
            ->leftJoin('countries', 'countries.id', '=', 'salereports.CountryId')
            // ->leftJoin('cards', function ($join) {
            //     $join->on('orders.UserId', '=', 'cards.UserId');
            //     $join->on('receipts.last4', '=', 'cards.cardNumber');
            //     $join->on('receipts.branchName', '=', 'cards.branchName');
            //     $join->on('receipts.expiredYear', '=', 'cards.expiredYear');
            //     $join->on('receipts.expiredMonth', '=', 'cards.expiredMonth');
            // })
            ->select(
                'countries.codeIso as OrderCountryCodeIso',
                'countries.includedTaxToProduct as isIncludedTaxToProduct',
                'subscriptions.isTrial as SubscriptionIsTrial',
                //remove master report
                'subscriptions.id as Subscriptionid',
                'subscriptions.PlanId as SubscriptionPlanId',
                //remove master report
                // 'deliveryaddresses.portalCode as portalCode',
                // 'billing.portalCode as bportalCode',
                'salereports.*',
                'salereports.created_at as screated_at',
                'orders.subscriptionIds as subscriptionIds',
                'orders.imp_uid as imp_uid',
                'orders.payment_status as paymentStatus',
                'orders.carrierAgent as carrierAgent',
                'orders.UserId as UserId',
                'orders.isSubsequentOrder as isSubsequentOrder',
                'orders.DeliveryAddressId as odaid',
                'orders.BillingAddressId as obaid',
                'orders.created_at as order_created_at',
                'bulkorders.carrierAgent as bulkCarrierAgent',
                'bulkorders.created_at as bulkorder_created_at',
                'receipts.chargeId as rchargeId',
                'receipts.branchName as rbranchName',
                'cards.cardType as cCardType'
            )
            ->when($channel != null, function ($q) use ($channel) {
                if ($channel === "all") {
                    return $q;
                } else if ($channel === "seller") {
                    return $q->where('salereports.category', "sales app");
                } else if ($channel === "customer") {
                    return $q->where('salereports.category', "client");
                } else if ($channel === "bulk") {
                    return $q->where('salereports.category', "Bulk Order");
                }
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.CountryId', $country);
                }
            })

            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.status', $status);
                }
            })
            ->orderBy('salereports.created_at', 'DESC')
            // ->distinct()
            ->chunk(1000, function ($reportsmaster) use (&$reports, $search) {
                foreach ($reportsmaster as $key => $rm) {
                    // $_country = Countries::findorfail($rm->CountryId);
                    $rm->includedTaxToProduct =  $rm->isIncludedTaxToProduct;
                    if ($rm->category == "Bulk Order") {
                        // $_rm = BulkOrders::find($rm->bulkOrderId);
                        // if ($_rm !== null) {
                        $rm->order_no =  $this->orderService->formatBulkOrderNumberV5($rm->bulkOrderId, strtoupper($rm->OrderCountryCodeIso), $rm->bulkorder_created_at, false);
                        $rm->isSubsequentOrder = 0;
                        // } else {
                        //     $rm->order_no =  '';
                        //     $rm->isSubsequentOrder = 0;
                        // }
                        $rm->carrierAgent = $rm->bulkCarrierAgent;
                    } else {

                        //remove master report
                        if (strpos($rm->sku, ',') !== false) {
                        } else {
                            $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL',"MASK/2019-BLACK","MASK/2019-BLUE","MASK/2019-GREEN","MASK/2019-GREY"];

                            if (!in_array($rm->sku, $srplans)) {
                                $combineqty = "";
                                $combinesku = "";
                                $countcombinesku = 0;
                                if ($rm->Subscriptionid) {
                                    if ($rm->SubscriptionPlanId) {
                                        if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {

                                            $getPlanProduct =  PlanTrialProducts::join('products', 'products.id', 'plantrialproducts.ProductId')
                                                ->where('plantrialproducts.PlanId', $rm->SubscriptionPlanId)
                                                ->select('plantrialproducts.qty as pqty', 'products.sku as psku')
                                                ->orderBy('products.sku')
                                                ->get();
                                            if ($getPlanProduct) {
                                                foreach ($getPlanProduct as $_getPlanProduct) {
                                                    if ($countcombinesku == 0) {
                                                        $combinesku = $_getPlanProduct->psku;
                                                        $combineqty = $_getPlanProduct->pqty;
                                                    } else {
                                                        $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                        $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                    }
                                                    $countcombinesku++;
                                                }
                                            }
                                        } else {
                                            $getPlanProduct =  PlanDetails::join('productcountries', 'productcountries.id', 'plan_details.ProductCountryId')
                                                ->join('products', 'products.id', 'productcountries.ProductId')
                                                ->where('plan_details.PlanId', $rm->SubscriptionPlanId)
                                                ->select('plan_details.qty as pqty', 'products.sku as psku')
                                                ->orderBy('products.sku')
                                                ->get();
                                            if ($getPlanProduct) {
                                                if ($rm->SubscriptionIsTrial == 1) {
                                                    foreach ($getPlanProduct as $_getPlanProduct) {
                                                        if (!in_array($_getPlanProduct->psku, config('global.all.handle_types.skusV2'))) {
                                                            if ($countcombinesku == 0) {
                                                                $combinesku = $_getPlanProduct->psku;
                                                                $combineqty = $_getPlanProduct->pqty;
                                                            } else {
                                                                $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                                $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                            }
                                                        }
                                                        $countcombinesku++;
                                                    }
                                                } else {

                                                    foreach ($getPlanProduct as $_getPlanProduct) {
                                                        if ($countcombinesku == 0) {
                                                            $combinesku = $_getPlanProduct->psku;
                                                            $combineqty = $_getPlanProduct->pqty;
                                                        } else {
                                                            $combinesku =  $combinesku . "," . $_getPlanProduct->psku;
                                                            $combineqty = $combineqty . "," . $_getPlanProduct->pqty;
                                                        }
                                                        $countcombinesku++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if ($rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0) {
                                    } else {
                                        $subaddons = SubscriptionsProductAddOns::join('productcountries', 'productcountries.id', 'subscriptions_productaddon.ProductCountryId')
                                            ->join('products', 'products.id', 'productcountries.ProductId')
                                            ->where("subscriptions_productaddon.subscriptionId", $rm->Subscriptionid)
                                            ->select('subscriptions_productaddon.qty as pqty', 'products.sku as psku')
                                            ->orderBy('products.sku')
                                            ->get();

                                        if ($subaddons) {
                                            foreach ($subaddons as $_subaddons) {
                                                if ($countcombinesku == 0) {
                                                    $combinesku = $_subaddons->psku;
                                                    $combineqty = $_subaddons->pqty;
                                                } else {
                                                    $combinesku = $combinesku . "," . $_subaddons->psku;
                                                    $combineqty = $combineqty . "," . $_subaddons->pqty;
                                                }
                                                $countcombinesku++;
                                            }
                                        }
                                    }
                                    if ($combineqty != "" && $combinesku != "") {
                                        $rm->sku = $combinesku;
                                        $rm->qty = $combineqty;
                                    }
                                }
                            }
                        }
                        //remove master report

                        // $_rm = Orders::find($rm->orderId);
                        // $rm->order_no = $this->orderService->formatOrderNumberV4($_rm, strtoupper($rm->OrderCountryCodeIso), false);
                        // $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                        // if ($_rm !== null) {
                        $rm->order_no = $this->orderService->formatOrderNumberV5($rm->orderId, strtoupper($rm->OrderCountryCodeIso), $rm->order_created_at, false);
                        if (isset($rm->deliveryContact)) {
                        } else {
                            $getDeliveryAddresses = DeliveryAddresses::where('id', $rm->odaid)
                                ->select('contactNumber')
                                ->first();
                            if (isset($getDeliveryAddresses)) {
                                $rm->deliveryContact = $getDeliveryAddresses->contactNumber;
                            }
                        }
                        if (isset($rm->billingContact)) {
                        } else {
                            $getBillingAddresses = DeliveryAddresses::where('id', $rm->obaid)
                                ->select('contactNumber')
                                ->first();
                            if (isset($getBillingAddresses)) {
                                $rm->deliveryContact = $getBillingAddresses->contactNumber;
                            }
                        }
                        // $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
                        // } else {
                        //     $rm->order_no =  '';
                        //     $rm->isSubsequentOrder = 0;
                        // }
                    }

                    $canproceed = null;
                    if ($search !== null && $search !== "") {
                        if (strpos($rm->order_no . $rm->taxInvoiceNo . $rm->sku, $search) !== false) { //if found
                            $canproceed = true;
                        } else {
                            $reportsmaster->forget($key);
                            $canproceed = false;
                        }
                    } else {
                        $canproceed = true;
                    }

                    if ($canproceed === true) {
                        // $rm->created_at = date('d-m-y H:i', strtotime($rm->created_at));
                        $rm->completed_date = null;
                        if ($rm->status == "Completed") {
                            if ($rm->category != "Bulk Order") {
                                $rm->completed_at = OrderHistories::where('OrderId', $rm->orderId)->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                                if ($rm->completed_at != null) {
                                    $rm->completed_date = date('Y-m-d', strtotime($rm->completed_at));
                                }
                            } else {
                                $rm->completed_at = BulkOrderHistories::where('BulkOrderId', $rm->bulkOrderId)->where('message', 'Completed')->where('isRemark', '!=', '1')->pluck('created_at')->first();
                                if ($rm->completed_at != null) {
                                    $rm->completed_date = date('Y-m-d', strtotime($rm->completed_at));
                                }
                            }
                        }

                        // $_subscription = Subscriptions::find($rm->subscriptionIds);

                        $rm->isActiveTrial = 0;
                        if (isset($rm->SubscriptionIsTrial) && $rm->SubscriptionIsTrial != null) {
                            $rm->isTrial = $rm->SubscriptionIsTrial;
                            $rm->isActiveTrial = $rm->SubscriptionIsTrial == 1 && $rm->isSubsequentOrder == 0 ? 1 : 0;
                        }

                        // $rm->cCardType = null;
                        // get card details

                        // if ($rm->paymentType === "stripe" && $rm->rchargeId != null && $rm->rchargeId != "") {
                        //     $apptype = ($rm->category === 'sales app') ? 'baWebsite' : (($rm->category === 'client') ? 'ecommerce' : 'ecommerce');
                        //     $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $rm->CountryId, $rm->rchargeId);
                        //     $rm->cCardType = $chargeDetails ? $chargeDetails->payment_method_details->card->funding : null;
                        // }
                        // else if ($rm->paymentType === "nicepay") {
                        //     $rm->cCardType = null;
                        // }
                        // else {
                        //     $rm->cCardType = null;
                        // }
                    }
                    // if ($_subscription != null) {
                    //     $rm->isTrial = $_subscription->isTrial;
                    //     $rm->gender = $this->planHelper->getPlanDetailsByPlanIdV2($_subscription->PlanId, $_country->id)['plan_gender_type'];
                    // }
                    // else {
                    //     $rm->gender = null;
                    //     $skulist = explode(",", $rm->sku);

                    //     $productcategories = [];
                    //     foreach ($skulist as $sku) {
                    //         $producttypes = Product::where("sku", $sku)
                    //         ->leftJoin('producttypedetails','products.id', '=', 'producttypedetails.ProductId')
                    //         ->pluck('producttypedetails.ProductTypeId');

                    //         foreach ($producttypes as $pt) {
                    //             array_push($productcategories, $pt);
                    //         }
                    //     }

                    //     if (count(array_intersect($productcategories, config('global.all.product_category_types.men'))) >= 1) {
                    //         $rm->gender = "male";
                    //     }
                    //     else if (count(array_intersect($productcategories, config('global.all.product_category_types.women'))) >= 1) {
                    //         $rm->gender = "female";
                    //     }
                    // }
                }

                // foreach ($reportsmaster_final as $rmf) {
                //     $rmf->created_at = date('d-m-y H:i', strtotime($rmf->created_at));
                //     $rmf->completed_date = null;
                //     if ($rmf->status == "Completed") {
                //         if ($rmf->productCategory != "Bulk Order") {
                //             $rmf->completed_at = OrderHistories::where('OrderId', $rmf->orderId)->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                //             if ($rmf->completed_at != null) {
                //                 $rmf->completed_date = date('d-m-y H:i', strtotime($rmf->completed_at));
                //             }
                //         }
                //         else {
                //             $rmf->completed_at = BulkOrderHistories::where('OrderId', $rmf->orderId)->where('message', 'Completed')->where('isRemark', 0)->pluck('created_at')->first();
                //             if ($rmf->completed_at != null) {
                //                 $rmf->completed_date = date('d-m-y H:i', strtotime($rmf->completed_at));
                //             }
                //         }
                //     }

                //     $_subscription = Subscriptions::find($rmf->subscriptionIds);

                //     $rmf->isActiveTrial = 0;
                //     if ($_subscription != null) {
                //         $rmf->isTrial = $_subscription->isTrial;
                //         $rmf->isActiveTrial = $rmf->isTrial == 1 && $rmf->isSubsequentOrder == 0 ? 1 : 0;
                //     }

                //     $rmf->cCardType = null;

                //     if ($rmf->paymentType === "stripe" && $rmf->rchargeId != null && $rmf->rchargeId != "") {
                //         $apptype = ($rmf->category === 'sales app') ? 'baWebsite' : (($rmf->category === 'client') ? 'ecommerce' : 'ecommerce');
                //         $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $rmf->CountryId, $rmf->rchargeId);
                //         $rmf->cCardType = $chargeDetails ? $chargeDetails->payment_method_details->card->funding : null;
                //     }
                //     else if ($rmf->paymentType === "nicepay") {
                //         $rmf->cCardType = null;
                //     }
                //     else {
                //         $rmf->cCardType = null;
                //     }
                // }
                $reports = $reports->merge($reportsmaster);
            });
        return $reports;
    }

    public function getReportsAppcoList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $reportsmaster = DB::table('salereports')
            ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
            ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->select('deliveryaddresses.portalCode as portalCode', 'billing.portalCode as bportalCode', 'salereports.*', 'orders.subscriptionIds as subscriptionIds', 'orders.imp_uid as imp_uid', 'orders.payment_status as paymentStatus', 'orders.carrierAgent as carrierAgent', 'orders.UserId as UserId', 'receipts.chargeId as rchargeId', 'receipts.branchName as rbranchName')
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.CountryId', $country);
                }
            })

            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.status', $status);
                }
            })
            ->where('salereports.category', "sales app")
            ->orderBy('salereports.created_at', 'DESC')
            ->distinct()
            ->get();

        foreach ($reportsmaster as $rm) {

            $_country = Countries::findorfail($rm->CountryId);
            $rm->includedTaxToProduct =  $_country->includedTaxToProduct;
            if ($rm->category == "Bulk Order") {
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false);
                $rm->isSubsequentOrder = 0;
            } else {
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false);
                $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
            }
        }

        $reportsmaster_final = $reportsmaster->filter(function ($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no . $report->taxInvoiceNo . $report->sku, $search) >= 0;
            } else {
                return true;
            }
        });

        foreach ($reportsmaster_final as $rmf) {
            // $rmf->created_at = date('d-m-y H:i', strtotime($rmf->created_at));

            $_subscription = Subscriptions::find($rmf->subscriptionIds);

            $rmf->isActiveTrial = 0;
            if ($_subscription != null) {
                $rmf->isTrial = $_subscription->isTrial;
                $rmf->isActiveTrial = $rmf->isTrial == 1 && $rmf->isSubsequentOrder == 0 ? 1 : 0;
            }

            $rmf->cCardType = null;

            // if ($_subscription != null) {
            //     $rm->isTrial = $_subscription->isTrial;
            //     $rm->gender = $this->planHelper->getPlanDetailsByPlanIdV2($_subscription->PlanId, $_country->id)['plan_gender_type'];
            // }
            // else {
            //     $rm->gender = null;
            //     $skulist = explode(",", $rm->sku);

            //     $productcategories = [];
            //     foreach ($skulist as $sku) {
            //         $producttypes = Product::where("sku", $sku)
            //         ->leftJoin('producttypedetails','products.id', '=', 'producttypedetails.ProductId')
            //         ->pluck('producttypedetails.ProductTypeId');

            //         foreach ($producttypes as $pt) {
            //             array_push($productcategories, $pt);
            //         }
            //     }

            //     if (count(array_intersect($productcategories, config('global.all.product_category_types.men'))) >= 1) {
            //         $rm->gender = "male";
            //     }
            //     else if (count(array_intersect($productcategories, config('global.all.product_category_types.women'))) >= 1) {
            //         $rm->gender = "female";
            //     }
            // }

            if ($rmf->paymentType === "stripe" && $rmf->rchargeId != null && $rmf->rchargeId != "") {
                $apptype = ($rmf->category === 'sales app') ? 'baWebsite' : (($rm->category === 'client') ? 'ecommerce' : 'ecommerce');
                $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $rmf->CountryId, $rmf->rchargeId);
                $rmf->cCardType = $chargeDetails ? $chargeDetails->payment_method_details->card->funding : null;
            } else if ($rm->paymentType === "nicepay") {
                $rmf->cCardType = null;
            } else {
                $rmf->cCardType = null;
            }
        }


        return $reportsmaster;
    }

    public function getReportsMOList($options)
    {
        $maxresults = 50;
        $search = null;
        $channel = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        } else {
            if (isset($options["fromdate"])) {
                $fromdate = $options["fromdate"];
            }

            if (isset($options["todate"])) {
                $todate = $options["todate"];
            }
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $reportsmaster = DB::table('salereports')
            ->leftJoin('orders', 'salereports.orderId', '=', 'orders.id')
            ->leftJoin('deliveryaddresses', 'orders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('deliveryaddresses as billing', 'orders.BillingAddressId', '=', 'billing.id')
            ->leftJoin('receipts', 'orders.id', '=', 'receipts.OrderId')
            ->select('deliveryaddresses.portalCode as portalCode', 'billing.portalCode as bportalCode', 'salereports.*', 'orders.subscriptionIds as subscriptionIds', 'orders.imp_uid as imp_uid', 'orders.payment_status as paymentStatus', 'orders.carrierAgent as carrierAgent', 'orders.UserId as UserId', 'receipts.chargeId as rchargeId', 'receipts.branchName as rbranchName')
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.CountryId', $country);
                }
            })

            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('salereports.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('salereports.updated_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('salereports.status', $status);
                }
            })
            ->where('salereports.category', "sales app")
            ->orderBy('salereports.created_at', 'DESC')
            ->distinct()
            ->get();

        foreach ($reportsmaster as $rm) {

            $_country = Countries::findorfail($rm->CountryId);
            $rm->includedTaxToProduct =  $_country->includedTaxToProduct;
            if ($rm->category == "Bulk Order") {
                $_rm = BulkOrders::findorfail($rm->bulkOrderId);
                $rm->order_no = $this->orderService->formatBulkOrderNumber($_rm, $_country, false);
                $rm->isSubsequentOrder = 0;
            } else {
                $_rm = Orders::findorfail($rm->orderId);
                $rm->order_no = $this->orderService->formatOrderNumber($_rm, $_country, false);
                $rm->isSubsequentOrder = $_rm->isSubsequentOrder;
            }
        }

        $reportsmaster_final = $reportsmaster->filter(function ($report) use ($search) {
            if ($search !== null && $search !== "") {
                return strpos($report->order_no . $report->taxInvoiceNo . $report->sku, $search) >= 0;
            } else {
                return true;
            }
        });

        foreach ($reportsmaster_final as $rmf) {
            // $rmf->created_at = date('d-m-y H:i', strtotime($rmf->created_at));

            $_subscription = Subscriptions::find($rmf->subscriptionIds);

            $rmf->isActiveTrial = 0;
            if ($_subscription != null) {
                $rmf->isTrial = $_subscription->isTrial;
                $rmf->isActiveTrial = $rmf->isTrial == 1 && $rmf->isSubsequentOrder == 0 ? 1 : 0;
            }

            $rmf->cCardType = null;

            // if ($_subscription != null) {
            //     $rm->isTrial = $_subscription->isTrial;
            //     $rm->gender = $this->planHelper->getPlanDetailsByPlanIdV2($_subscription->PlanId, $_country->id)['plan_gender_type'];
            // }
            // else {
            //     $rm->gender = null;
            //     $skulist = explode(",", $rm->sku);

            //     $productcategories = [];
            //     foreach ($skulist as $sku) {
            //         $producttypes = Product::where("sku", $sku)
            //         ->leftJoin('producttypedetails','products.id', '=', 'producttypedetails.ProductId')
            //         ->pluck('producttypedetails.ProductTypeId');

            //         foreach ($producttypes as $pt) {
            //             array_push($productcategories, $pt);
            //         }
            //     }

            //     if (count(array_intersect($productcategories, config('global.all.product_category_types.men'))) >= 1) {
            //         $rm->gender = "male";
            //     }
            //     else if (count(array_intersect($productcategories, config('global.all.product_category_types.women'))) >= 1) {
            //         $rm->gender = "female";
            //     }
            // }

            if ($rmf->paymentType === "stripe" && $rmf->rchargeId != null && $rmf->rchargeId != "") {
                $apptype = ($rmf->category === 'sales app') ? 'baWebsite' : (($rmf->category === 'client') ? 'ecommerce' : 'ecommerce');
                $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $rmf->CountryId, $rmf->rchargeId);
                $rmf->cCardType = $chargeDetails ? $chargeDetails->payment_method_details->card->funding : null;
            } else if ($rmf->paymentType === "nicepay") {
                $rmf->cCardType = null;
            } else {
                $rmf->cCardType = null;
            }
        }

        return $reportsmaster_final;
    }

    public function getCustomersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $customers = User::when($search != null, function ($q) use ($search) {
            return $q->where('email', 'LIKE', '%' . $search . '%')->orWhere(DB::raw('concat(firstName," ",lastName)'), 'LIKE', '%' . $search . '%');
        })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('CountryId', $country);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('isActive', $status);
                }
            })
            ->distinct()
            ->get();

        foreach ($customers as $cust) {
            $cust->country = Countries::find($cust->CountryId);
            $cust->address = DeliveryAddresses::find($cust->isDefaultShipping);
            $cust->consent = ($cust->Email_Sub == 1 && $cust->Sms_Sub == 1) ? "Email & SMS" : ($cust->Email_Sub == 1) ? "Email" : ($cust->Sms_Sub == true) ? "SMS" : ($cust->consentHKG_Marketing_Sub == 1) ? "HK Marketing" : "None";
            $cust->subscriptions = Subscriptions::where("UserId", $cust->id)->get();
            $cust->hasTrial = Subscriptions::where("UserId", $cust->id)->where('isTrial', 1)->first();
            $cust->hasCustom = Subscriptions::where("UserId", $cust->id)->where('isCustom', 1)->first();
            $cust->hasAlacarte = Orders::where("UserId", $cust->id)->whereNull('subscriptionIds')->first();
            $cust->referrals = Referral::where("referralId", $cust->id)->get();
            $addons_array = [];
            $pauseplan_array = [];

            foreach ($cust->subscriptions as $sub) {
                $addons = SubscriptionsProductAddOns::where("subscriptionId", $sub->id)->get();
                array_push($addons_array, $addons);

                $pauseplan = PausePlanHistories::where("subscriptionIds", $sub->id)->get();
                array_push($pauseplan_array, $pauseplan);
            }
            $cust->addons = [];
            $cust->pauseplan = [];
        }

        return $customers;
    }

    public function getRechargesList($options)
    {
        $maxresults = 50;
        $search = null;
        $status = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $recharges = Subscriptions::when($search != null, function ($q) use ($search) {
            if ($search === "all") {
                return $q;
            } else {
                return $q->where('email', 'LIKE', $search);
            }
        })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('status', $status);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('recharge_date', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('recharge_date', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->distinct()
            ->get();

        foreach ($recharges as $recharge) {
            $recharge->user = User::find($recharge->UserId);
            $recharge->country = Countries::find($recharge->user->CountryId);
            $recharge->plan = $this->planHelper->getPlanDetailsByPlanIdV2($recharge->PlanId, $recharge->country->id);

            $recharge->currentorder = Orders::find($recharge->currentOrderId);
            $recharge->rchargeId = Receipts::where('OrderId', $recharge->currentOrderId)->pluck('chargeId')->first();

            if ($recharge->currentorder != null) {
                if ($recharge->currentorder->paymentType === "stripe" && $recharge->rchargeId != null && $recharge->rchargeId != "") {
                    $apptype = ($recharge->currentorder->SellerUserId != null) ? 'ecommerce' : 'baWebsite';
                    $chargeDetails = $this->stripeBA->retrieveCharge($apptype, $recharge->currentorder->CountryId, $recharge->rchargeId);
                    $recharge->chargeDetails = $chargeDetails ? $chargeDetails : null;
                    $recharge->card = $chargeDetails ? $chargeDetails->payment_method_details->card : null;
                } else {
                    $recharge->card = null;
                }
            } else {
                $recharge->card = null;
            }
        }

        return $recharges;
    }

    public function getPausePlansList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $fmonth = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        }

        if (isset($options["fmonth"])) {
            $fmonth = $options["fmonth"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $pauseplans = DB::table('pause_plan_histories')->leftJoin('subscriptions', 'pause_plan_histories.subscriptionIds', '=', 'subscriptions.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->leftJoin('plans', 'subscriptions.PlanId', '=', 'plans.id')
            ->when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                } else {
                    return $q->where('users.email', 'LIKE', $search);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('pause_plan_histories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('pause_plan_histories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($fmonth != null, function ($q) use ($fmonth) {
                if ($fmonth === "all") {
                    return $q;
                } else {
                    return $q->where('pause_plan_histories.pausemonth', $fmonth);
                }
            })
            ->select('users.*', 'subscriptions.*', 'subscriptions.created_at as screated_at', 'pause_plan_histories.*', 'pause_plan_histories.created_at as pcreated_at', 'plans.CountryId as pCountryId', DB::raw('count(*) as totalCount'))
            ->groupBy('subscriptions.id')
            ->distinct()
            ->get();

        foreach ($pauseplans as $p) {
            // $p->screated_date = date('d-m-y H:i', strtotime($p->screated_at));
            // $p->pcreated_date = date('d-m-y H:i', strtotime($p->pcreated_at));
            $p->country = Countries::find($p->pCountryId);
        }

        return $pauseplans;
    }

    // public function getTaxInvoice($id)
    // {
    //     $data = Orders::where('id', $id)
    //         ->select('taxInvoiceNo', 'DeliveryAddressId', 'BillingAddressId', DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y") as created_at_date'))
    //         ->first();

    //     $data['deliverydetails'] = DeliveryAddresses::where('id', $data['DeliveryAddressId'])
    //         ->select('CountryId', 'firstName', 'contactNumber', 'address')
    //         ->first();

    //     $data['billingdetails'] = DeliveryAddresses::where('id', $data['BillingAddressId'])
    //         ->select('contactNumber', 'address')
    //         ->first();

    //     $data['orderdetails'] = \DB::table('producttranslates')
    //         ->leftJoin('products', 'producttranslates.ProductId', '=', 'products.id')
    //         ->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
    //         ->leftJoin('orderdetails', 'productcountries.id', '=', 'orderdetails.ProductCountryId')
    //         ->select('producttranslates.name', 'orderdetails.qty', 'orderdetails.price', \DB::raw('(orderdetails.qty * orderdetails.price) as total_price'))
    //         ->where('producttranslates.langCode', 'EN')
    //         ->where('orderdetails.OrderId', $id)
    //         ->get();

    //     $data['invoice'] = Receipts::where('OrderId', $id)
    //         ->select('currency', 'chargeId', 'branchName', 'last4', 'originPrice', 'discountAmount', 'chargeFee', 'totalPrice')
    //         ->first();

    //     return $data;
    // }
}
