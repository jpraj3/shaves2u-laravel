<?php
namespace App\Helpers\APIHelpers\Admin;

use App\Models\BaWebsite\SellerUser;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\Geolocation\Countries;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use DB;
use PDF;

class BaWebsiteAPIHelper
{
    public function __construct()
    {
    }
    
    public function getSellerUsersCount($options)
    {
        $maxresults = 50;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        
        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }
        
        if (isset($options["channel"])) {
            $channel = $options["channel"];
        }
        
        if (isset($options["country"])) {
            $country = $options["country"];
        }
        
        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }
        
        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }
        
        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        $selleruserlist = DB::table('sellerusers')
        ->leftJoin('countries', 'sellerusers.CountryId', '=', 'countries.id')
        ->when($country != null, function ($q) use ($country) {
            if ($country === "all") {
                return $q;
            }
            else {
                return $q->where('sellerusers.CountryId', $country);
            }
        })
        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('sellerusers.latestUpdatedDate', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('sellerusers.latestUpdatedDate', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            }
            else {
                return $q->where('sellerusers.isActive', $status);
            }
        })
        ->distinct()
        ->get();

        $sellerusers = (array) null;
        $sellerusers['total'] = count($selleruserlist);
        $sellerusers['pages'] = ceil(count($selleruserlist) / $maxresults);

        return $sellerusers;
    }
    
    public function getSellerUsersList($options)
    {
        $maxresults = 50;
        $search = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        
        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
        }
        
        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }
        
        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        $sellerusers = DB::table('sellerusers')
        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('sellerusers.latestUpdatedDate', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('sellerusers.latestUpdatedDate', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            }
            else {
                return $q->where('sellerusers.isActive', $status);
            }
        })
        ->distinct()
        ->get()->forPage($pagenum, $maxresults)->values();

        foreach ($sellerusers as $selleruser) {
            $selleruser->country = Countries::where('id',$selleruser->CountryId)->pluck('name')->first();
        }

        return $sellerusers;
    }

    public function createSellerUser($data) {
        $result = SellerUser::where('badgeID',$data['badge_id'])->first();

        if (!$result) {
            $selleruser = new SellerUser();

            $selleruser->badgeID = $data['badge_id'];
            $selleruser->icNumber = $data['ic_number'];
            $selleruser->agentName = $data['agent_name'] ? $data['agent_name'] : null;
            $selleruser->campaign = $data['campaign'] ? $data['campaign'] : null;
            $selleruser->email = $data['email'];
            $selleruser->CountryId = $data['country'];
            $selleruser->MarketingOfficeID = $data['marketing_office'];
            $selleruser->isActive = 1;
            $selleruser->save();
        }

        $success = ['success' => 'success', 200];

        return $success;
    }

    public function changeSellerUserStatus($data, $id) {
        $selleruser = SellerUser::findOrfail($id);

        if ($selleruser) {
            $selleruser->isActive = $data['isActive'];
            $selleruser->save();
        }

        $success = ['success' => 'success', 'payload' => $selleruser, 200];

        return $success;
    }

    public function massInactiveSellerUser($countryId = null) {
        try {
            //only set to inactive seller users in current country
            $sellerusers = SellerUser::when($countryId != null, function ($q) use ($countryId) {
                if ($countryId == null) {
                    return $q;
                }
                else {
                    return $q->where('sellerusers.CountryId', $countryId);
                }
            })
            //ignore badgeIds for testing
            ->whereNotIn('badgeId', config('global.admin.testBadges'))
            ->get();
            
            foreach ($sellerusers as $selleruser) {
                $selleruser->isActive = 0;
                $selleruser->save();
            }

            $success = ['success' => 'success', 200];

            return $sellerusers;

        } catch (Exception $e) {
            $error = ['error' => $e, 400];
            return $error;
         }
    }

    public function checkSellerUser($data, $countryid = null) {
        try {
            $selleruser = SellerUser::where('badgeId', $data[1])
            ->where('email', trim($data[5]))
            ->when($countryid != null, function ($q) use ($countryid) {
                if ($countryid == null) {
                    return $q;
                }
                else {
                    return $q->where('CountryId', trim($countryid));
                }
            })->first();

            if ($selleruser != null) {
                //if found, set to active
                $selleruser->latestUpdatedDate = Carbon::today()->toDateString();
                $selleruser->isActive = 1;
                $selleruser->save();

                return null;
            }
            else {
                // if not found, create seller user
                $newselleruser = $this->createSellerUserFromCSV($data, $countryid);
                return $newselleruser;
            }

        } catch (Exception $e) {
            $error = ['error' => $e, 400];
            return $error;
         }
    }

    public function createSellerUserFromCSV($data, $countryid = null) {
        $mo = MarketingOffice::where('moCode',$data[6])->pluck('id')->first();

        if ($mo != null) {
            $selleruser = new SellerUser();

            $selleruser->campaign = $data[0];
            $selleruser->badgeID = $data[1];
            $selleruser->icNumber = $data[2];
            $selleruser->agentName = $data[3];
            $selleruser->startDate = $data[4];
            $selleruser->email = trim($data[5]);
            $selleruser->MarketingOfficeID = $mo;
            $selleruser->latestUpdatedDate = Carbon::today()->toDateString();

            $selleruser->CountryId = $countryid;
            $selleruser->isActive = 1;
            $selleruser->save();
            
            $message = $selleruser;
        }
        else {
            $message = ['error' => 'error', 400];
        }
        
        return $message;
    }
}