<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App;

// Helpers
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Orders\Orders;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\CancellationJourney\CancellationJourneys;
use App\Models\CancellationJourney\CancellationReasonTranslates;
use App\Models\Subscriptions\Subscriptions;
use App\Models\Subscriptions\SubscriptionHistories;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use DB;

// Services

// Models

class CancellationAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getCancellationsCount($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $cancellationlist = CancellationJourneys::when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                }
                else {
                    return $q->where('email', 'LIKE', '%'.$search.'%');
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->distinct()
            ->get();

            $cancellation_journeys_Ids = [];
            foreach ($cancellationlist as $subIds) {
                array_push($cancellation_journeys_Ids, $subIds->SubscriptionId);
            }

            $admincancellations = Subscriptions::join('subscriptionhistories', 'subscriptionhistories.subscriptionId', 'subscriptions.id')
            ->where('subscriptions.status', 'Cancelled')
            ->whereIn('subscriptionhistories.message', ['Canceled', 'Cancelled'])
            ->whereNotNull('subscriptionhistories.detail')
            ->where('subscriptionhistories.detail', 'Cancelled by Admin')
            ->whereNotIn('subscriptions.id', $cancellation_journeys_Ids)
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptionhistories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptionhistories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->select(
                'subscriptions.id',
                'subscriptions.email',
                'subscriptionhistories.message',
                'subscriptionhistories.detail',
                'subscriptionhistories.created_at',
                'subscriptionhistories.updated_at'
            )
            ->orderBy('subscriptionhistories.created_at', 'DESC')
            ->distinct()
            ->get();
        
        $countcancel = count($cancellationlist);
        $countadmin = count($admincancellations);
        $counttotal = $countcancel + $countadmin;
        $cancellations = (array) null;
        $cancellations['total'] = $counttotal;
        
        $cancellations['pages'] = ceil($counttotal / $maxresults);

        return $cancellations;
    }

    public function getCancellationsList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $cancellations = DB::table('cancellation_journeys')->when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                }
                else {
                    return $q->where('email', 'LIKE', '%'.$search.'%');
                }
            })->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->orderBy('created_at', 'DESC')
            ->distinct()
            ->paginate($maxresults, ['*'], 'page', $pagenum);
            $cancellationreasontranslate = CancellationReasonTranslates::get();
            $cancellation_journeys_Ids = [];
        foreach ($cancellations as $can) {
            array_push($cancellation_journeys_Ids, $can->SubscriptionId);
            $content="";
            $defaultContent="";
             $can->created_date = $can->created_at;
             $can->updated_date = $can->updated_at;
            if($can->CancellationTranslatesId){
      
                if (strpos($can->CancellationTranslatesId, ',') !== false) {
                    $splittranslate = explode(",", $can->CancellationTranslatesId);
                    $countst = 0;
                    foreach ($splittranslate as $st) {
                        foreach ($cancellationreasontranslate as $ct) {
                            if($st == $ct->id){
                                if( $countst == 0 ){
                                    $content= $content.$ct->content;
                                    $defaultContent= $defaultContent.$ct->defaultContent;
                                }else{
                                    $content= $content.'<br>'.$ct->content;
                                    $defaultContent= $defaultContent.'<br>'.$ct->defaultContent;
                                }
                                $countst++;
                            }
                        }
                    }
                }else{
                    foreach ($cancellationreasontranslate as $ct) {
                        if($can->CancellationTranslatesId == $ct->id){
                            $content= $content.$ct->content;
                            $defaultContent= $defaultContent.$ct->defaultContent;
                        }
                    }
                }
           }
           $can->content = $content;
           $can->defaultContent = $defaultContent;
           $can->userActionText = $this->getUserCancellationActions($can->modifiedBlade,$can->modifiedFrequency,$can->discountPercent,$can->hasGetFreeProduct,$can->hasPausedSubscription);
        }
     

        $admincancellations = Subscriptions::join('subscriptionhistories', 'subscriptionhistories.subscriptionId', 'subscriptions.id')
            ->where('subscriptions.status', 'Cancelled')
            ->whereIn('subscriptionhistories.message', ['Canceled', 'Cancelled'])
            ->whereNotNull('subscriptionhistories.detail')
            ->where('subscriptionhistories.detail', 'Cancelled by Admin')
            ->whereNotIn('subscriptions.id', $cancellation_journeys_Ids)
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('subscriptionhistories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('subscriptionhistories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->select(
                'subscriptions.id',
                'subscriptions.email',
                'subscriptionhistories.message',
                'subscriptionhistories.detail',
                'subscriptionhistories.created_at',
                'subscriptionhistories.updated_at'
            )
            ->orderBy('subscriptionhistories.created_at', 'DESC')
            ->distinct()
            ->get();
            foreach ($admincancellations as $aCan) {
                $aCan->defaultContent =  $aCan->detail;
                $aCan->hasCancelled = 1;
                $aCan->userActionText = "-";
                $aCan->planType = "-";
                $aCan->email =  $aCan->email;
                $aCan->created_date =  Carbon::parse($aCan->created_at)->format('Y-m-d H:i:s');
            }
      
            $merged_data = [];

            foreach ($cancellations as $c) {
                array_push($merged_data, $c);
            }
            foreach ($admincancellations as $cA) {
                array_push($merged_data, $cA);
            }
            return $merged_data;
    }

    
    public function getUserCancellationActions($changedCartridge,$changedPlan,$getDiscount,$getNewBlade,$onHoldSubscription)
    {
        $userActionText = "";
        if($changedCartridge !== null){
          $userActionText = $userActionText."Changed to ".$changedCartridge.". ";
        }
        if($changedPlan !== null){
          $userActionText = $userActionText."Changed to ".$changedPlan.". ";
        }
        if($getDiscount !== null){
          $userActionText = $userActionText."Get ".$getDiscount."% discount. ";
        }
        if($getNewBlade !== 0){
          $userActionText = $userActionText."Get new blade. ";
        }
        if($onHoldSubscription !== 0){
          $userActionText = $userActionText."Onhold subscription. ";
        }
        if($changedCartridge === null && $changedPlan === null && $getDiscount === null && $getNewBlade === 0 && $onHoldSubscription === 0){
          $userActionText = "-";
        }
        return $userActionText;
    }
}