<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Models\Products\Product;
use App\Models\Products\ProductBundles;
use App\Models\Products\ProductCountry;
use App\Models\Products\ProductImage;
use App\Models\Products\ProductTranslate;
use App\Models\Products\ProductTypeDetail;
use App\Models\Products\ProductTypeTranslate;

class ProductAPIHelper
{
    public function __construct()
    {
    }


    public function getSKUsList()
    {
        $skus = Product::select('sku')->groupBy('sku')->get();

        return $skus;
    }
}
