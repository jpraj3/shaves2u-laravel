<?php

namespace App\Helpers\APIHelpers\Admin;

use App;

// Helpers
use App\Helpers\SubscriptionHelper;

use App\Models\Ecommerce\User;
use App\Models\User\DeliveryAddresses;
use App\Models\Cards\Cards;
use App\Models\Geolocation\Countries;
use App\Models\Geolocation\States;
use App\Models\Subscriptions\Subscriptions;
use DB;

// Services

// Models

class UserAPIHelper
{
    public function __construct()
    {
        $this->subscriptionHelper = new SubscriptionHelper();
    }

    public function getUsersList($options)
    {
        $maxresults = null;
        
        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        $users = User::latest()
            ->when($maxresults > 0, function ($q) use ($maxresults) {
                return $q->take($maxresults);
            })
            ->get();

        return ($users);
    }

    public function getCustomersCount($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
        }

        $customerlist = User::when($search != null, function ($q) use ($search) {
            return $q->where('email', 'LIKE' , '%'.$search.'%')->orWhere(DB::raw('concat(firstName," ",lastName)'), 'LIKE' , '%'.$search.'%');
        })
        ->when($country != null, function ($q) use ($country) {
            if ($country === "all") {
                return $q;
            } else {
                return $q->where('CountryId', $country);
            }
        })
        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            } else {
                return $q->where('isActive', $status);
            }
        })
        ->distinct()
        ->get();

            
        $customers = (array) null;
        $customers['total'] = count($customerlist);
        $customers['pages'] = ceil(count($customerlist) / $maxresults);
        
        return ($customers);
    }

    public function getCustomersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $status = null;
            $fromdate = null;
            $todate = null;
        }

        $customers = User::when($search != null, function ($q) use ($search) {
            return $q->where('email', 'LIKE' , '%'.$search.'%')->orWhere(DB::raw('concat(firstName," ",lastName)'), 'LIKE' , '%'.$search.'%');
        })
        ->when($country != null, function ($q) use ($country) {
            if ($country === "all") {
                return $q;
            } else {
                return $q->where('CountryId', $country);
            }
        })
        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('created_at', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('created_at', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            } else {
                return $q->where('isActive', $status);
            }
        })
        ->distinct()
        ->get();

        return $customers;
    }
    
    public function getCustomerDetails($id)
    {
        $customer = User::findOrfail($id);
        $customer->country = Countries::find($customer->CountryId);
        $customer->d_address_details = DeliveryAddresses::find($customer->defaultShipping);
        $customer->b_address_details = DeliveryAddresses::find($customer->defaultBilling);
        $customer->addresses = DeliveryAddresses::where('Userid', $customer->id)->where('isRemoved', 0)->whereNotIn('id',[$customer->d_address_details->id,$customer->b_address_details->id])->get();
        $customer->card_details = Cards::where('UserId',$customer->id)->where('isDefault', 1)->where('isDefault', 1)->first();
        $customer->cards = Cards::where('UserId',$customer->id)->where('isDefault', 0)->where('isRemoved', 0)->get();
        $customer->subscription = $this->subscriptionHelper->RetrieveLatestSubsInfo($customer->id)->latest_sub;
        $customer->registered_date = $customer->registered_at;
        $customer->last_login_date = $customer->last_login_at;
        $customer->states = States::where('CountryId', $customer->CountryId)->get();

        $customer->orderHistories = DB::table('orderhistories')
        ->leftJoin('orders','orderhistories.OrderId','=','orders.id')
        ->where('orders.UserId', $customer->id)
        ->select('orderhistories.message', 'orderhistories.created_at as created_at_date')
        ->get();

        return $customer;
    }
    
    public function updateCustomerStatus($data, $id)
    {
        $customer = User::findOrfail($id);

        if ($customer) {
            $customer->isActive = $data['isActive'];
            $customer->save();
        }

        $success = ['success' => 'success', 'payload' => $customer, 200];

        return $success;
    }
    
    public function updateCustomerProfile($data, $id)
    {
        $customer = User::findOrfail($id);

        if ($customer) {
            if ($customer->email != $data['customer_email']) {
                $customer->email = $data['customer_email'];
                //send email logic
            }
            $customer->firstName = $data['customer_name'];
            $customer->phone = $data['customer_phone'];
            $customer->save();
        }

        $success = ['success' => 'success', 'payload' => $customer, 200];

        return $success;
    }

    public function selectCustomerDefaultAddress($data, $id)
    {
        $customer = User::findOrfail($id);

        if ($customer) {
            if ($data['address_type'] === 'shipping') {
                $customer->defaultShipping = $data['address_id'];
            }
            else if ($data['address_type'] === 'billing') {
                $customer->defaultBilling = $data['address_id'];
            }
            $customer->save();
        }
        $success = ['success' => 'success', 200];

        return $success;
    }

    public function addCustomerAddress($data, $id)
    {
        $customer = User::findOrfail($id);
        $name = $customer->firstName.($customer->lastName !== null &&  $customer->lastName !== "" ? " ".$customer->lastName : "");

        if ($customer) {
            if ($customer->defaultShipping == null || $customer->defaultBilling == null) {
                if ($customer->isDefaultShipping == null) {
                    $new_daddress = new DeliveryAddresses();
                    $new_daddress->firstName = $name;
                    $new_daddress->lastName = null;
                    $new_daddress->fullName = $name;
                    $new_daddress->contactNumber = $customer->phone;
                    $new_daddress->SSN = $data['SSN'] != null && $data['SSN'] != "" ? $data['SSN'] : null;
                    $new_daddress->address = $data['address_detail'];
                    $new_daddress->city = $data['city'];
                    if ($data['portalCode'] != null || $data['portalCode']) {
                        $new_daddress->portalCode = $data['portalCode'];
                    } else {
                        $new_daddress->portalCode = "00000";
                    }
                    $new_daddress->state = $data['state'];
                    $new_daddress->isRemoved = 0;
                    $new_daddress->isBulkAddress = 0;
                    $new_daddress->CountryId = $customer->CountryId;
                    $new_daddress->UserId = $customer->id;
                    $new_daddress->save();

                    $customer->defaultShipping = $new_daddress->id;
                    $customer->save();
                }
                if ( $customer->defaultBilling == null) {
                    $new_baddress = new DeliveryAddresses();
                    $new_baddress->firstName = $name;
                    $new_baddress->lastName = null;
                    $new_baddress->fullName = $name;
                    $new_daddress->contactNumber = $customer->phone;
                    $new_baddress->SSN = $data['SSN'] != null && $data['SSN'] != "" ? $data['SSN'] : null;
                    $new_baddress->address = $data['address_detail'];
                    $new_baddress->city = $data['city'];
                    if ($data['portalCode'] != null || $data['portalCode']) {
                        $new_baddress->portalCode = $data['portalCode'];
                    } else {
                        $new_baddress->portalCode = "00000";
                    }
                    $new_baddress->state = $data['state'];
                    $new_baddress->isRemoved = 0;
                    $new_baddress->isBulkAddress = 0;
                    $new_baddress->CountryId = $customer->CountryId;
                    $new_baddress->UserId = $customer->id;
                    $new_baddress->save();

                    $customer->defaultBilling = $new_baddress->id;
                    $customer->save();
                }
            }
            else {
                $new_address = new DeliveryAddresses();
                $new_address->firstName = $name;
                $new_address->lastName = null;
                $new_address->fullName = $name;
                $new_address->contactNumber = $customer->phone;
                $new_address->SSN = $data['SSN'] != null && $data['SSN'] != "" ? $data['SSN'] : null;
                $new_address->address = $data['address_detail'];
                $new_address->city = $data['city'];
                if ($data['portalCode'] != null || $data['portalCode']) {
                    $new_address->portalCode = $data['portalCode'];
                } else {
                    $new_address->portalCode = "00000";
                }
                $new_address->state = $data['state'];
                $new_address->isRemoved = 0;
                $new_address->isBulkAddress = 0;
                $new_address->CountryId = $customer->CountryId;
                $new_address->UserId = $customer->id;
                $new_address->save();
            }
        }

        $success = ['success' => 'success', 200];

        return $success;
    }

    public function editCustomerAddress($data, $id)
    {
        $customer = User::findOrfail($id);
        $address = DeliveryAddresses::findOrfail($data['address_id']);

        if ($address) {
            $address->address = $data['address_detail'];
            $address->city = $data['city'];
            $address->portalCode = $data['portalCode'];
            $address->state = $data['state'];
            $address->save();
        }
        $success = ['success' => 'success', 200];

        return $success;
    }

    public function removeCustomerAddress($data, $id)
    {
        DeliveryAddresses::where('id', $data['address_id'])->update(["isRemoved" => 1]);
        $success = ['success' => 'success', 200];

        return $success;
    }

    public function selectCustomerDefaultCard($id)
    {
        $cards = Cards::where('UserId', $id)->get();

        foreach ($cards as $card) {
            if ($card->id != $data['card_id']) {
                $card->isDefault = 0;
            }
            else {
                $card->isDefault = 1;
            }
            $card->save();
        }
        $success = ['success' => 'success', 200];

        return $success;
    }

    public function addCustomerCard($data, $id)
    {
        $customer = User::findOrfail($id);
        $CountryId = $customer->CountryId;

        $data = [];
        $data["user"] = $customer;
        $data["add_card"] = $data;
        $success = [];

        $stripe = new App\Helpers\Payment\StripeBA();
        // Create customer card in stripe and get card token id
        $createdCard = $stripe->createTokenV3_API('baWebsite', $CountryId, $data["add_card"]);

        if ($createdCard) {
            // Create customer in stripe with source linked to the card token id
            $createdCustomer = $stripe->createCustomer('baWebsite', $CountryId, $data, $createdCard);

            $user = $this->userHelper->getUserDetails($customer->id);

            if ($createdCustomer) {
                $cards = new Cards();
                $cards->customerId = $createdCustomer["customer"]["id"];
                $cards->cardNumber = $createdCard["card"]->last4;
                $cards->branchName = $createdCard["card"]->brand;
                $cards->cardType = $createdCard["card"]->funding;
                $cards->cardName = $createdCard["card"]->name;
                $cards->expiredYear = $createdCard["card"]->exp_year;
                $cards->expiredMonth = $createdCard["card"]->exp_month;

                if (count($user["cards"]) > 0) {
                    $cards->isDefault = 0;
                } else {
                    $cards->isDefault = 1;
                }

                $cards->payment_method = 'stripe';
                $cards->created_at = Carbon::now();
                $cards->updated_at = Carbon::now();
                $cards->UserId = $customer->id;
                $cards->isRemoved = 0;
                $cards->type = 'baWebsite';
                $cards->CountryId = $CountryId;
                $cards->save();
            }

            $success = ['success' => 'success', 200];

            return $success;
        } else {
            return 0;
        }
    }

    public function removeCustomerCard($data, $id)
    {
        Cards::where('id', $data['card_id'])->update(["isRemoved" => 1]);
        $success = ['success' => 'success', 200];

        return $success;
    }
}
