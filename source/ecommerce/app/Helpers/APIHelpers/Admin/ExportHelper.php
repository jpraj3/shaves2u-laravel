<?php
namespace App\Helpers\APIHelpers\Admin;

use App\Helpers\AWSHelper;
use Illuminate\Http\Request;
use Response;

class ExportHelper
{
    public function __construct()
    {
        $this->awsHelper = new AWSHelper();
    }

    public function orders($data)
    {
        $file = file_get_contents('D:/laragon/www/dev/shaves2u-ecommerce/source/admin/public/download/info.pdf');
        $source_path = 'D:/laragon/www/dev/shaves2u-ecommerce/source/admin/public/download/info.pdf';
        $folderpath = $this->awsHelper->__folderPath('csvReportsUpload');
        $filename = 'test_excel';
        $output = $this->awsHelper->__putExcel($file, $source_path, $folderpath, $filename . '.xls', 'public');
        return $output;

        // orders.forEach(order => {
        //     let badgeId = (order.SellerUser) ? order.SellerUser.badgeId : '';
        //     let agentName = (order.SellerUser) ? order.SellerUser.agentName : '';
        //     let deliveryAddress = '';
        //     let deliveryContactNumber = '';
        //     let billingAddress = '';
        //     let billingContactNumber = '';
        //     let customerName = '';
        //     if (order.SellerUserId) {
        //       customerName = order.fullName;
        //     } else {
        //       customerName = order.User ? `${order.User.firstName ? order.User.firstName : ''} ${order.User.lastName ? order.User.lastName : ''}` : '';
        //     }

        //     if (order.DeliveryAddress) {
        //       deliveryAddress = `${order.DeliveryAddress.address} ${order.DeliveryAddress.city} ${order.DeliveryAddress.portalCode} ${order.DeliveryAddress.state}`;
        //       deliveryContactNumber = order.DeliveryAddress.contactNumber;
        //     }
        //     if (order.BillingAddress) {
        //       billingAddress = `${order.BillingAddress.address} ${order.BillingAddress.city} ${order.BillingAddress.portalCode} ${order.BillingAddress.state}`;
        //       billingContactNumber = order.BillingAddress.contactNumber;
        //     }
        //     if (order.SellerUserId) {
        //       deliveryContactNumber = order.phone;
        //       billingContactNumber = order.phone;
        //     }
        //     let trackingNumber = order.deliveryId;
        //     let promoCode = order.promoCode;
        //     let discountPrice = order.Receipt.discountAmount;
        //     let productName = '';
        //     let region = order.Country.code;
        //     let category = (order.SellerUser) ? 'Sales app' : 'ecommerce';
        //     let sku = '';
        //     let email;

        //     let cancellationReason = '';
        //     let canceledDate = '';

        //     if (order.User) {
        //       email = order.User.email;
        //     } else if (order.SellerUser) {
        //       email = order.SellerUser.email;
        //     } else {
        //       email = order.email;
        //     }
        //     let marketingOffice = order.moCodeGet;
        //     order.orderDetail.forEach((orderDetail, index) => {
        //       let prodName = (orderDetail.ProductCountry) ? _.findWhere(orderDetail.ProductCountry.Product.productTranslate, { langCode: 'EN' }).name : '';
        //       let planName = (orderDetail.PlanCountry) ? _.findWhere(orderDetail.PlanCountry.Plan.planTranslate, { langCode: 'EN' }).name : '';
        //       if (index !== 0) {
        //         productName += ', ';
        //       }
        //       productName += (prodName !== '' ? prodName : planName);

        //       if (index !== 0) {
        //         sku += ', ';
        //       }
        //       if (orderDetail.PlanCountry) {
        //         sku = sku + orderDetail.PlanCountry.Plan.sku;
        //       } else if (orderDetail.ProductCountry) {
        //         sku = sku + orderDetail.ProductCountry.Product.sku;
        //       }
        //     });

        //     // get cancellationReason if order was canceled from orderHistories
        //     if (order.states == 'Canceled') {
        //       for (var i = order.orderHistories.length - 1; i >= 0; i--) {
        //         let orderHistory = order.orderHistories[i];

        //         // get the if lastest history was canceled
        //         if (i == order.orderHistories.length - 1 && orderHistory.message == 'Canceled') {
        //           cancellationReason = orderHistory.cancellationReason;
        //           canceledDate = moment(orderHistory.createdAt).format('YYYY-MM-DD');
        //           break;
        //         }
        //       }
        //     }

        //     tmp = {
        //       orderId: OrderHelper.fromatOrderNumber(order),
        //       badgeId,
        //       agentName,
        //       channelType: order.channelType,
        //       eventLocationCode: order.eventLocationCode,
        //       moCode: marketingOffice ? marketingOffice : null,
        //       customerName,
        //       email,
        //       SSN: order.SSN,
        //       deliveryAddress,
        //       deliveryContactNumber,
        //       billingAddress,
        //       billingContactNumber,
        //       trackingNumber,
        //       promoCode,
        //       discountPrice,
        //       productName,
        //       sku,
        //       region,
        //       category,
        //       receiptId: order.Receipt.id,
        //       currency: order.Country.currencyCode,
        //       createdAt: order.createdAt,
        //       saleDate: moment(order.createdAt).format('YYYY-MM-DD'),
        //       paymentType: order.paymentType.toUpperCase(),
        //       states: order.states,
        //       cancellationReason,
        //       canceledDate,
        //       totalPrice: order.Receipt.originPrice,
        //       subTotalPrice: order.Receipt.subTotalPrice,
        //       discountAmount: order.Receipt.discountAmount,
        //       taxAmount: order.Receipt.taxAmount,
        //       shippingFee: order.Receipt.shippingFee,
        //       grandTotalPrice: order.Receipt.totalPrice,
        //       source: order.source,
        //       medium: order.medium,
        //       campaign: order.campaign,
        //       term: order.term,
        //       content: order.content,
        //       isDirectTrial: order.isDirectTrial ? 'Yes' : (((!order.subscriptionIds || order.subscriptionIds === '') && order.SellerUserId) ? 'Yes' : 'No')
        //     };
        //     orderTmpnew.push(tmp);
        //   });
    }
}
