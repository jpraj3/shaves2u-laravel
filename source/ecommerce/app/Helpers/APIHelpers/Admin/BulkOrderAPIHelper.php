<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Helpers\OrderHelper;
use App\Helpers\PlanHelper;
use App\Helpers\ProductHelper;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Geolocation\Countries;
use App\Models\Promotions\Promotions;
use App\Models\Promotions\PromotionCodes;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use App\Models\FileUploads\FileUploads;
use App\Queue\SaleReportQueueService;
use App\Queue\TaxInvoiceBulkOrderQueueService;
use App\Services\OrderService;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Redis;

class BulkOrderAPIHelper
{
    public function __construct()
    {
        $this->productHelper = new ProductHelper();
        $this->planhelper = new PlanHelper();
        $this->orderHelper = new OrderHelper();
        $this->orderService = new OrderService;
        $this->Log = \Log::channel('cronjob');
    }

    public function getBulkOrdersCount($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $status = null;
            $sku = null;
            $fromdate = null;
            $todate = null;
        }

        $bulkorderlist = DB::table('bulkorders')
            ->leftJoin('bulkorderdetails', 'bulkorders.id', '=', 'bulkorderdetails.BulkOrderId')
            ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            ->leftJoin('deliveryaddresses', 'bulkorders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->select('bulkorders.id', 'countries.codeIso', 'bulkorders.created_at', 'bulkorders.taxInvoiceNo', 'bulkorders.email', 'bulkorders.status', 'products.sku')
            ->when($search != null, function ($q) use ($search) {
                return $q->orWhere('bulkorders.email', 'LIKE', '%' . $search . '%')->orWhere('bulkorders.taxInvoiceNo', 'LIKE', '%' . $search . '%')->orWhere('bulkorders.id', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('products.sku', $sku);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('bulkorders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('bulkorders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('bulkorders.status', $status);
                }
            })
            ->groupBy('bulkorders.id')
            ->distinct()
            ->get();

        foreach ($bulkorderlist as $ord) {
            $_order = BulkOrders::findorfail($ord->id);
            $_country = Countries::findorfail($_order->CountryId);
            $ord->bulkorder_no = $this->orderService->formatBulkOrderNumber($_order, $_country, false) . '-' . date('Ymd', strtotime($ord->created_at));
        }

        // $bulkorderlist_final = $bulkorderlist->filter(function($bulkorder) use ($search) {
        //     if ($search !== null && $search !== "") {
        //         return strpos($bulkorder->email.$bulkorder->taxInvoiceNo.$bulkorder->bulkorder_no, $search) >= 0;
        //     }
        //     else {
        //         return true;
        //     }
        // });

        $bulkorders = (array) null;
        $bulkorders['total'] = count($bulkorderlist);
        $bulkorders['pages'] = ceil(count($bulkorderlist) / $maxresults);

        return $bulkorders;
    }

    public function getBulkOrdersList($options)
    {
        $maxresults = 50;
        $search = null;
        $country = null;
        $status = null;
        $sku = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["country"])) {
            $country = $options["country"];
        }

        if (isset($options["sku"])) {
            $sku = $options["sku"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $country = null;
            $status = null;
            $sku = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $bulkorders = DB::table('bulkorders')
            ->leftJoin('bulkorderdetails', 'bulkorders.id', '=', 'bulkorderdetails.BulkOrderId')
            ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            ->leftJoin('deliveryaddresses', 'bulkorders.DeliveryAddressId', '=', 'deliveryaddresses.id')
            ->leftJoin('countries', 'deliveryaddresses.CountryId', '=', 'countries.id')
            ->select('bulkorderdetails.qty as qty', 'bulkorderdetails.price as price', DB::raw('SUM(bulkorderdetails.price) as totalprice'), 'bulkorderdetails.currency as currency', 'bulkorders.id', 'countries.codeIso', 'countries.id as cid', 'bulkorders.created_at', 'bulkorders.taxInvoiceNo', 'bulkorders.email', 'bulkorders.status', 'products.sku')
            ->when($search != null, function ($q) use ($search) {
                return $q->orWhere('bulkorders.email', 'LIKE', '%' . $search . '%')->orWhere('bulkorders.taxInvoiceNo', 'LIKE', '%' . $search . '%')->orWhere('bulkorders.id', 'LIKE', '%' . $search . '%');
            })
            ->when($country != null, function ($q) use ($country) {
                if ($country === "all") {
                    return $q;
                } else {
                    return $q->where('countries.id', $country);
                }
            })
            ->when($sku != null, function ($q) use ($sku) {
                if ($sku === "all") {
                    return $q;
                } else {
                    return $q->where('products.sku', $sku);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('bulkorders.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('bulkorders.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('bulkorders.status', $status);
                }
            })
            ->groupBy('bulkorders.id')
            ->distinct()
            ->orderBy('bulkorders.created_at', 'DESC')
            ->get()->forPage($pagenum, $maxresults)->values();

        foreach ($bulkorders as $ord) {
            $ord->skus = BulkOrderDetails::where('bulkorderdetails.BulkOrderId', $ord->id)
                ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
                ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
                ->select(DB::raw('group_concat(distinct products.sku separator ", ") as skus'))
                ->groupBy('bulkorderdetails.BulkOrderId')
                ->pluck('skus');

            $ord->taxinvoiceurl = FileUploads::where('bulkOrderId', $ord->id)->pluck('url')->first();

            $ord->totalPrice = number_format($ord->totalprice, 2);

            $_order = BulkOrders::findorfail($ord->id);
            $_country = Countries::findorfail($_order->CountryId);
            $ord->bulkorder_no = $this->orderService->formatBulkOrderNumber($_order, $_country, false) . '-' . date('Ymd', strtotime($ord->created_at));
            // $ord->created_at = date('d-m-y H:i', strtotime($ord->created_at));
        }

        // $bulkorders_final = $bulkorders->filter(function($bulkorder) use ($search) {
        //     if ($search !== null && $search !== "") {
        //         return strpos($bulkorder->email.$bulkorder->taxInvoiceNo.$bulkorder->bulkorder_no, $search) >= 0;
        //     }
        //     else {
        //         return true;
        //     }
        // });

        return $bulkorders;
    }

    public function getBulkOrderDetails($id)
    {
        //$bulkorderdetails = Redis::get('bulkorders:details:'.$id);

        //if ($bulkorderdetails == null) {
        $bulkorder = BulkOrders::where('id', $id)
            ->select(
                'id',
                'fullName',
                'email',
                'subscriptionIds',
                'deliveryId',
                'taxInvoiceNo',
                'paymentType',
                'created_at as created_at_date',
                'created_at',
                'DeliveryAddressId',
                'status',
                'carrierAgent'
            )
            ->first();


        $bulkorder['bulkorderlog'] = BulkOrderHistories::where('BulkOrderId', $id)
            ->select('message', 'created_at as created_at_date')
            ->get();

        $bulkorder['deliverydetails'] = DeliveryAddresses::where('id', $bulkorder['DeliveryAddressId'])
            ->select('CountryId', 'firstName', 'contactNumber', 'address')
            ->first();

        $bulkorder['countryCode'] = Countries::where('id', $bulkorder['deliverydetails']['CountryId'])
            ->pluck('codeIso')
            ->first();

        // $bulkorder['bulkorderdetails'] = DB::table('producttranslates')
        //     ->leftJoin('products', 'producttranslates.ProductId', '=', 'products.id')
        //     ->leftJoin('productcountries', 'products.id', '=', 'productcountries.ProductId')
        //     ->leftJoin('bulkorderdetails', 'productcountries.id', '=', 'bulkorderdetails.ProductCountryId')
        //     ->select('producttranslates.name', 'bulkorderdetails.qty', 'bulkorderdetails.price')
        //     ->where('producttranslates.langCode', 'EN')
        //     ->where('bulkorderdetails.BulkOrderId', $id)
        //     ->get();

        $bulkorder['bulkorderdetails'] = DB::table('bulkorderdetails')
            ->leftJoin('productcountries', 'bulkorderdetails.ProductCountryId', '=', 'productcountries.id')
            ->leftJoin('products', 'productcountries.ProductId', '=', 'products.id')
            ->select('products.sku', 'bulkorderdetails.qty', 'bulkorderdetails.price')
            ->where('bulkorderdetails.BulkOrderId', $id)
            ->get();

        $totalprice = 0;

        foreach ($bulkorder['bulkorderdetails'] as $details) {
            $totalprice += $details->price;
            $details->unit_price = number_format($details->price / $details->qty, 2);
            $details->price = number_format($details->price, 2);
        }

        $bulkorder['totalprice'] = number_format($totalprice, 2);

        $discount = PromotionCodes::where('code', $bulkorder['promoCode'])
            ->leftJoin('promotions', 'promotioncodes.PromotionId', '=', 'promotions.id')
            ->pluck('promotions.discount')->first();

        if ($discount) {
            $discountPercent = ($discount) / 100;
            $reversePercent = 1 + $discountPercent;
            $bulkorder['discountAmount'] = number_format($discountPercent * $bulkorder['totalprice'], 2);
            $bulkorder['subTotal'] = number_format($reversePercent * $bulkorder['totalprice'], 2);
        } else {
            $bulkorder['discountAmount'] = number_format(0.00, 2);
            $bulkorder['subTotal'] = $bulkorder['totalprice'];
        }

        $bulkorder['currency'] = BulkOrderDetails::where('BulkOrderId', $id)
            ->pluck('currency')
            ->first();

        $_order = BulkOrders::findorfail($id);

        $_country = Countries::findorfail($_order['CountryId']);

        $bulkorder['order_no'] = $this->orderService->formatBulkOrderNumber($_order, $_country, false);

        $bulkorder['taxInvoiceUrl'] = FileUploads::where('bulkOrderId', $bulkorder['id'])->pluck('url')->first();

        // Redis::set('bulkorders:details:' . $id, json_encode($bulkorder));

        // $bulkorderdetails = Redis::get('bulkorders:details:' . $id);
        //}

        return $bulkorder;
    }

    public function updateBulkOrderStatus($params)
    {
        $isRemark = 0;

        if (isset($params["isRemark"])) {
            $isRemark = $params["isRemark"];
        }

        $bulkorderHistory = new BulkOrderHistories();
        $bulkorderHistory->BulkOrderId = $params["id"];
        $bulkorderHistory->message = $params["status"];
        $bulkorderHistory->isRemark = $isRemark;
        $bulkorderHistory->save();

        if ($isRemark == 0) {
            $updateBulkOrder = BulkOrders::find($params["id"]);
            $updateBulkOrder->status = $params["status"];
            $updateBulkOrder->save();

            $newHistory = new BulkOrderHistories();
            $newHistory->OrderId = $params["id"];
            $newHistory->message = "BulkOrder status <" . $params["status"] . "> updated by Admin";
            $newHistory->isRemark = 1;
            $newHistory->save();
        }

        $bulkorderlog = BulkOrderHistories::where('BulkOrderId', $params["id"])
            ->select('message', 'created_at as created_at_date')
            ->get();

        return $bulkorderlog;
    }

    public function updateTrackingNumber($params)
    {

        $updateBulkOrder = BulkOrders::find($params["id"]);
        $updateBulkOrder->deliveryId = (string) $params["trackingNumber"];
        $updateBulkOrder->save();

        $params['status'] = "Delivering";
        $params['isRemark'] = 0;

        $bulkorderlog = $this->updateBulkOrderStatus($params);

        $response = (object) array();
        $response->success = true;
        $response->payload = (object) array(
            'trackingNumber' => $updateBulkOrder->deliveryId,
            'bulkorderlog' => $bulkorderlog,
        );

        return $response;
    }

    public function csvCheckInsertData($datarow)
    {
        try {
            $data = [];
            $email = $datarow[0];
            $vendor = $datarow[1];
            $countryCode = $datarow[2];
            $sku = $datarow[3];
            $qty = $datarow[4];
            $price = $datarow[5];
            $dFirstName = $datarow[6];
            $dLastName = $datarow[7];
            $dAddress = $datarow[8];
            $dCity = $datarow[9];
            $dPortalCode = $datarow[10];
            $dContactNumber = $datarow[11];
            $bFirstName = $datarow[12];
            $bLastName = $datarow[13];
            $bAddress = $datarow[14];
            $bCity = $datarow[15];
            $bPortalCode = $datarow[16];
            $bContactNumber = $datarow[17];
            $taxinvoice = $datarow[18];
            $combineda = trim($dAddress . $dCity . $dPortalCode);
            $combineba = trim($bAddress . $bCity . $bPortalCode);
            $daid = "";
            $baid = "";

            $country = Countries::select("id", "currencyCode", "codeIso")
                ->where('code', $countryCode)
                ->first();
            if ($country) {
                if ($combineda == $combineba) {
                    $da = DeliveryAddresses::where('firstName', $dFirstName)
                        ->where('lastName', $dLastName)
                        ->where('address', $dAddress)
                        ->where('city', $dCity)
                        ->where('portalCode', $dPortalCode)
                        ->where('contactNumber', $dContactNumber)
                        ->where('isBulkAddress', true)
                        ->where('CountryId', $country->id)
                        ->select('id')
                        ->first();
                    if ($da) {
                        $daid = $da->id;
                        $baid = $da->id;
                    } else {
                        $deliveryaddresses = new DeliveryAddresses();
                        $deliveryaddresses->firstName = $dFirstName;
                        $deliveryaddresses->lastName = $dLastName;
                        $deliveryaddresses->contactNumber = $dContactNumber;
                        $deliveryaddresses->address = $dAddress;
                        $deliveryaddresses->portalCode = $dPortalCode;
                        $deliveryaddresses->city = $dCity;
                        $deliveryaddresses->isBulkAddress = 1;
                        $deliveryaddresses->CountryId = $country->id;
                        $deliveryaddresses->save();
                        $daid = $deliveryaddresses->id;
                        $baid = $deliveryaddresses->id;
                    }
                } else {
                    $da = DeliveryAddresses::where('firstName', $dFirstName)
                        ->where('lastName', $dLastName)
                        ->where('address', $dAddress)
                        ->where('city', $dCity)
                        ->where('portalCode', $dPortalCode)
                        ->where('contactNumber', $dContactNumber)
                        ->where('isBulkAddress', true)
                        ->where('CountryId', $country->id)
                        ->select('id')
                        ->first();
                    if ($da) {
                        $daid = $da->id;
                    } else {
                        $deliveryaddresses = new DeliveryAddresses();
                        $deliveryaddresses->firstName = $dFirstName;
                        $deliveryaddresses->lastName = $dLastName;
                        $deliveryaddresses->contactNumber = $dContactNumber;
                        $deliveryaddresses->address = $dAddress;
                        $deliveryaddresses->portalCode = $dPortalCode;
                        $deliveryaddresses->city = $dCity;
                        $deliveryaddresses->isBulkAddress = 1;
                        $deliveryaddresses->CountryId = $country->id;
                        $deliveryaddresses->save();
                        $daid = $deliveryaddresses->id;
                    }
                    $ba = DeliveryAddresses::where('firstName', $bFirstName)
                        ->where('lastName', $bLastName)
                        ->where('address', $bAddress)
                        ->where('city', $bCity)
                        ->where('portalCode', $bPortalCode)
                        ->where('contactNumber', $bContactNumber)
                        ->where('isBulkAddress', true)
                        ->where('CountryId', $country->id)
                        ->select('id')
                        ->first();
                    if ($ba) {
                        $baid = $ba->id;
                    } else {
                        $billingaddresses = new DeliveryAddresses();
                        $billingaddresses->firstName = $bFirstName;
                        $billingaddresses->lastName = $bLastName;
                        $billingaddresses->contactNumber = $bContactNumber;
                        $billingaddresses->address = $bAddress;
                        $billingaddresses->portalCode = $bPortalCode;
                        $billingaddresses->city = $bCity;
                        $billingaddresses->isBulkAddress = 1;
                        $billingaddresses->CountryId = $country->id;
                        $billingaddresses->save();
                        $baid = $billingaddresses->id;
                    }
                }

                $planData = "";
                $productData = "";
                $planData = $this->planhelper->getPlanBySKU($sku, $country->id);
                if (empty($planData)) {
                    $productData = $this->productHelper->getProductCountriesBySKU($sku, $country->id);
                    if (empty($productData)) {
                        $data["status"] = "fail";
                        $data["reason"] = "SKU : (" . $sku . ") Not Found";
                        return $data;
                    } else {
                        $productData = $productData->pcid;
                    }
                } else {
                    $planData = $planData->planid;
                }
                $currentCountryDetails = config('global.all.bulk_order_tax_invoice.' . strtoupper($country->codeIso));
                if ($currentCountryDetails) {
                    $CheckingCurrentCountry = strtolower($currentCountryDetails);
                } else {
                    $CheckingCurrentCountry = 'n';
                }
                $data["email"] = $email;
                $data["vendor"] = $vendor;
                $data["countryCode"] = $countryCode;
                $data["country"] = $country->id;
                $data["codeIso"] = $country->codeIso;
                $data["countrycurrency"] = $country->currencyCode;
                $data["sku"] = $sku;
                $data["qty"] = $qty;
                $data["price"] = $price;
                $data["taxinvoice"] = $taxinvoice;
                $data["CheckingCurrentCountry"] = $CheckingCurrentCountry;
                $data["planData"] = $planData;
                $data["productData"] = $productData;
                $data["daid"] = $daid;
                $data["baid"] = $baid;
                $data["combineda"] = $combineda;
                $data["combineba"] = $combineba;
                $data["status"] = "success";
                $data["reason"] = "";
                $data["dContactNumber"] = $dContactNumber;
                $data["bContactNumber"] = $bContactNumber;
                return $data;
            } else {
                $data["status"] = "fail";
                $data["reason"] = "country missing";
                return $data;
            }
        } catch (Exception $e) {
            $data = [];
            $data["status"] = "fail";
            $data["reason"] = $e;
            return $data;
        }
    }

    public function csvInsertData($datarow)
    {
        try {
            $group = [];
            foreach ($datarow as $key => $row) {
                $group[$row["email"]][] = $row;
            }

            foreach ($group as $key => $borow) {
                $bulkorder = new BulkOrders();
                $bulkorder->status = "Payment Received";
                $bulkorder->paymentType = "cash";
                $bulkorder->email = $borow[0]["email"];
                $bulkorder->vendor = $borow[0]["vendor"];
                $bulkorder->created_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                $bulkorder->updated_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                $bulkorder->CountryId = $borow[0]["country"];
                if ($bulkorder->CountryId == "7") {
                    $bulkorder->carrierAgent = "courex";
                }
                $bulkorder->DeliveryAddressId = $borow[0]["daid"];
                $bulkorder->BillingAddressId = $borow[0]["baid"];
                $bulkorder->save();
                $bulkorderid = $bulkorder->id;

                $bulkorderhistory = new BulkOrderHistories();
                $bulkorderhistory->message = "Payment Received";
                $bulkorderhistory->created_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                $bulkorderhistory->updated_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                $bulkorderhistory->BulkOrderId = $bulkorderid;
                $bulkorderhistory->save();

                foreach ($borow as $key => $row) {
                    $bulkorderdetails = new BulkOrderDetails();
                    $bulkorderdetails->qty = $row["qty"];
                    $bulkorderdetails->price = $row["price"];
                    $bulkorderdetails->currency = $row["countrycurrency"];
                    $bulkorderdetails->created_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                    $bulkorderdetails->updated_at = Carbon::now("Asia/Kuala_Lumpur")->format('Y-m-d H:i:s');
                    $bulkorderdetails->BulkOrderId = $bulkorderid;
                    $bulkorderdetails->ProductCountryId = $row["productData"];
                    $bulkorderdetails->PlanCountryId = $row["planData"];
                    $bulkorderdetails->save();
                }

                $_redis_order_data = (object) array(
                    'OrderId' => $bulkorderid,
                    'CountryId' => strtolower($borow[0]["country"]),
                    'CountryCode' => strtolower($borow[0]["codeIso"]),
                );

                $this->Log->info('send to bulkorder queue -> ' . json_encode($_redis_order_data));
                $this->orderHelper->addTaskToBulkOrderQueueService($_redis_order_data);
                $_redis_sales_report_data = ['id' => $bulkorderid, 'bulkOrder' => true];
                SaleReportQueueService::addHash($bulkorderid, $_redis_sales_report_data);
                if ((strtolower($borow[0]["CheckingCurrentCountry"]) == 'y') && (strtolower($borow[0]["taxinvoice"]) == 'y')) {
                    $_redis_tax_invoice_data = ['id' => $bulkorderid, 'CountryId' => $borow[0]["country"]];
                    TaxInvoiceBulkOrderQueueService::addHash($bulkorderid, $_redis_tax_invoice_data);
                    // hide email redis
                    //sendReceiptBulkOrdersEmail email
                }
            }
            $data = [];
            $data["status"] = "success";
            $data["reason"] = "";
            return $data;
        } catch (Exception $e) {
            $data = [];
            $data["status"] = "fail";
            $data["reason"] = $e;
            return $data;
        }
    }
}
