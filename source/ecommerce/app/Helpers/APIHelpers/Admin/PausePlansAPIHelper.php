<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App;

// Helpers
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Orders\Orders;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use App\Models\Plans\PausePlanHistories;

use Illuminate\Support\Facades\Redis;
use DB;

// Services

// Models

class PausePlansAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getPausePlansCount($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $fmonth = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["fmonth"])) {
            $fmonth = $options["fmonth"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
            $fmonth = null;
        }

        $pauseplanlist = PausePlanHistories::leftJoin('subscriptions', 'pause_plan_histories.subscriptionIds', '=', 'subscriptions.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                }
                else {
                    return $q->where('users.email', 'LIKE', '%'.$search.'%');
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('pause_plan_histories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('pause_plan_histories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($fmonth != null, function ($q) use ($fmonth) {
                if ($fmonth === "all") {
                    return $q;
                } else {
                    return $q->where('pause_plan_histories.pausemonth', $fmonth);
                }
            })
            ->distinct()
            ->get();

        $pauseplans = (array) null;
        $pauseplans['total'] = count($pauseplanlist);
        $pauseplans['pages'] = ceil(count($pauseplanlist) / $maxresults);

        return $pauseplans;
    }

    public function getPausePlansList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $fmonth = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["fmonth"])) {
            $fmonth = $options["fmonth"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
            $fmonth = null;
        }

        $pauseplans = DB::table('pause_plan_histories')->leftJoin('subscriptions', 'pause_plan_histories.subscriptionIds', '=', 'subscriptions.id')
            ->leftJoin('users', 'subscriptions.UserId', '=', 'users.id')
            ->when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                }
                else {
                    return $q->where('users.email', 'LIKE', '%'.$search.'%');
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('pause_plan_histories.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('pause_plan_histories.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->when($fmonth != null, function ($q) use ($fmonth) {
                if ($fmonth === "all") {
                    return $q;
                } else {
                    return $q->where('pause_plan_histories.pausemonth', $fmonth);
                }
            })
            ->select('users.email as email','subscriptions.id as sid','pause_plan_histories.originaldate as originaldate','pause_plan_histories.resumedate as resumedate','pause_plan_histories.pausemonth as pausemonth','pause_plan_histories.created_at as pcreated_at')
            ->distinct()
            ->paginate($maxresults, ['*'], 'page', $pagenum);

        foreach ($pauseplans as $p) {
            $p->created_date = $p->pcreated_at;
        }

        return $pauseplans;
    }
}
