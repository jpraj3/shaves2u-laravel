<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App;

// Helpers
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Orders\Orders;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanSKU;
use App\Models\Receipts\Receipts;
use App\Models\Rebates\Referral;
use App\Models\Rebates\ReferralCashOut;
use App\Models\Rebates\RewardsCurrency;
use App\Models\Rebates\RewardsIn;
use App\Models\User\DeliveryAddresses;

use Illuminate\Support\Facades\Redis;
use DB;

// Services

// Models

class ReferralAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getReferralsCount($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $referrallist = Referral::leftJoin('users', 'referrals.referralId', '=', 'users.id')
            ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
            ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->select('users.email as email', 'referrals.created_at as created_at')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('users.email', 'LIKE', '%'.$search.'%');
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('referrals.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('referrals.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->groupBy('referrals.referralId', 'countries.code')
            ->get();

        $referrals = (array) null;
        $referrals['total'] = count($referrallist);
        $referrals['pages'] = ceil(count($referrallist) / $maxresults);

        return $referrals;
    }

    public function getReferralsList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        
        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }
        
        if (isset($options["search"]) && $options["search"] !== null && $options["search"] !== "") {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $referrals = DB::table('referrals')->leftJoin('users', 'referrals.referralId', '=', 'users.id')
            ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
            ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->select('referrals.referralId as rid', 'users.email as email', DB::raw('count(*) as conversion'), 'countries.id as cid', 'countries.code as countrycode', DB::raw('SUM(rewardsins.value) as rewvalue'), 'countries.currencyDisplay as currencyDisplay','referrals.created_at as created_at')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('users.email', 'LIKE', '%'.$search.'%');
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('referrals.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('referrals.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->groupBy('referrals.referralId', 'countries.code')
            ->paginate($maxresults, ['*'], 'page', $pagenum);
            foreach ($referrals as $r) {
                $withdrawn_total = ReferralCashOut::where('UserId', $r->rid)->where('CountryId', $r->cid)->where('status', 'withdrawn')->sum('amount');
                if($withdrawn_total){
                $r->cashoutmoney = $withdrawn_total;
                }
                else{
                $r->cashoutmoney = 0;
                }
            }
        return $referrals;
    }

    public function getReferralDetailsCount($options)
    {
        $maxresults = 50;
        $id = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["id"])) {
            $id = $options["id"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $referrallist = Referral::leftJoin('users', 'referrals.friendId', '=', 'users.id')
            ->leftJoin('users as ruser', 'referrals.referralId', '=', 'ruser.id')
            ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
            ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->where('referrals.referralId', $id)
            ->get();

        $referrals = (array) null;
        $referrals['total'] = count($referrallist);
        $referrals['pages'] = ceil(count($referrallist) / $maxresults);

        return $referrals;
    }

    public function getReferralDetailsList($options)
    {
        $maxresults = 50;
        $id = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["id"])) {
            $id = $options["id"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        $referrals = DB::table('referrals')->leftJoin('users', 'referrals.friendId', '=', 'users.id')
            ->leftJoin('users as ruser', 'referrals.referralId', '=', 'ruser.id')
            ->leftJoin('countries', 'referrals.CountryId', '=', 'countries.id')
            ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->where('referrals.referralId', $id)
            ->select('ruser.email as remail','referrals.created_at as created_at', 'referrals.rewardId as rewardId', 'referrals.status as status', 'referrals.firstPurchase as firstPurchase', 'referrals.source as source', 'referrals.referralId as rid', 'referrals.friendId as fid', 'users.email as femail', 'countries.id as cid', 'countries.code as countrycode', 'rewardsins.OrderId as OrderId', 'rewardsins.value as rewvalue', 'countries.currencyDisplay as currencyDisplay')
            ->paginate($maxresults, ['*'], 'page', $pagenum);
        foreach ($referrals as $r) {
            // $r->created_at = $r->created_at;
          if($r->OrderId){
            $_order = Orders::findorfail($r->OrderId);

            // retrieve country info
            $_country = Countries::findorfail($r->cid);
            $r->OrderId = $this->orderService->formatOrderNumber($_order, $_country, false);
          }
          else{
            $r->OrderId =null;
          }
            if ($r->source == "link") {
                $r->source = "Link";
            } else if ($r->source == "fb") {
                $r->source = "Facebook";
            } else if ($r->source == "email") {
                $r->source = "Facebook";
            }

            if ($r->firstPurchase == "1" && $r->rewardId != null && $r->status =='Active') {
                $r->newstatus = "Registered & Purchased";
            } else if ($r->firstPurchase == "0" && $r->rewardId == null && $r->status =='Inactive') {
                $r->newstatus = "Registered";
            } else if ($r->firstPurchase != null && $r->rewardId == null && $r->status =='Active') {
                $r->newstatus = "Registered";
            } else if ($r->firstPurchase == null && $r->rewardId == null && $r->status == null) {
                $r->newstatus = "-";
            }
            else{
                $r->newstatus = "-";
            }
        }

        return $referrals;
    }


    public function getReferralsCashOutCount($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $status = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $referrallist = ReferralCashOut::leftJoin('countries', 'referralcashouts.CountryId', '=', 'countries.id')
            // ->leftJoin('rewardsins', 'referrals.rewardId', '=', 'rewardsins.id')
            ->select('referralcashouts.email as email','referralcashouts.status', 'referralcashouts.created_at as created_at')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('referralcashouts.email', 'LIKE', '%'.$search.'%');
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('referralcashouts.status', $status);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('referralcashouts.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('referralcashouts.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            // ->groupBy('referralcashouts.referralId', 'countries.code')
            ->get();

        $referrals = (array) null;
        $referrals['total'] = count($referrallist);
        $referrals['pages'] = ceil(count($referrallist) / $maxresults);

        return $referrals;
    }

    public function getReferralsCashOutList($options)
    {
        $maxresults = 50;

        $search = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;
        $status = null;

        if (isset($options["maxresults"])) {
            $maxresults = $options["maxresults"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }
        
        if (isset($options["search"]) && $options["search"] !== null && $options["search"] !== "") {
            $search = $options["search"];
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $referrals = DB::table('referralcashouts')->leftJoin('countries', 'referralcashouts.CountryId', '=', 'countries.id')
         ->select('countries.currencyDisplay as currencyDisplay','countries.name as cname','referralcashouts.*')
            ->when($search != null, function ($q) use ($search) {
                return $q->where('referralcashouts.email', 'LIKE', '%'.$search.'%');
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                } else {
                    return $q->where('referralcashouts.status', $status);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('referralcashouts.created_at', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('referralcashouts.created_at', '<=', date("Y-m-d", strtotime($todate)));
            })
            // ->groupBy('referrals.referralId', 'countries.code')
            ->paginate($maxresults, ['*'], 'page', $pagenum);
            foreach ($referrals as $r) {
                $withdrawn_total = ReferralCashOut::where('UserId', $r->UserId)->where('CountryId', $r->CountryId)->where('status', 'withdrawn')->sum('amount');
                $rewards_total = RewardsIn::where('UserId', $r->UserId)->where('CountryId', $r->CountryId)->sum('value');
                if($withdrawn_total){
                    if($rewards_total){
                        $r->availablemoney = (float) $rewards_total - (float) $withdrawn_total;
                     }else{
                     $r->availablemoney = 0;
                     }
                }
                else{
                 if($rewards_total){
                    $r->availablemoney = $rewards_total;
                 }else{
                 $r->availablemoney = 0;
                 }
              }
            }
        return $referrals;
    }

    public function getReferralCashOutDetails($id)
    {
        $referralcashout = ReferralCashOut::where('id', $id)->select('*','created_at as created_at_date')->first();
        $referralcashout->country = Countries::where('id', $referralcashout->CountryId)->select('name','currencyCode')->first();

        return $referralcashout;
    }

    public function updateReferralCashoutStatus($params)
    {
            $updateRC = ReferralCashOut::find($params["id"]);
            $updateRC->status = $params["status"];
            $updateRC->save();

        return $updateRC->status;
    }
}
