<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Models\Plans\PlanCategory;
use App\Models\Plans\PlanCategoryDetails;
use App\Models\Plans\PlanCategoryTranslates;
use App\Models\Plans\PlanDetails;
use App\Models\Plans\PlanFrequency;
use App\Models\Plans\Plans;
use App\Models\Plans\PlanSKU;
use App\Models\Plans\PlanTranslates;

class PlanAPIHelper
{
    public function __construct()
    {
    }

    public function getPlanSKUsList()
    {
        $skus = PlanSKU::select('sku')->groupBy('sku')->get();

        return $skus;
    }
}
