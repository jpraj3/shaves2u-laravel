<?php

namespace App\Helpers\APIHelpers\Admin;

use App\Services\OrderService;

use App;

// Helpers
use App\Models\Cards\Cards;
use App\Models\Ecommerce\User;
use App\Models\Geolocation\Countries;
use App\Models\Subscriptions\Subscriptions;
use App\Models\User\DeliveryAddresses;

use Illuminate\Support\Facades\Redis;
use DB;

// Services

// Models

class RechargeAPIHelper
{
    public function __construct()
    {
        $this->orderService = new OrderService;
    }

    public function getRechargesCount($options)
    {
        $maxresults = 50;
        $search = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;    
        }

        $rechargelist = Subscriptions::select('email','recharge_date','total_recharge','status','unrealizedCust')
            ->when($search != null, function ($q) use ($search) {
                if ($search === "all") {
                    return $q;
                }
                else {
                    return $q->where('email', 'LIKE', $search);
                }
            })
            ->when($status != null, function ($q) use ($status) {
                if ($status === "all") {
                    return $q;
                }
                else {
                    return $q->where('status', $status);
                }
            })
            ->when($fromdate != null, function ($q) use ($fromdate) {
                return $q->whereDate('recharge_date', '>=', date("Y-m-d", strtotime($fromdate)));
            })
            ->when($todate != null, function ($q) use ($todate) {
                return $q->whereDate('recharge_date', '<=', date("Y-m-d", strtotime($todate)));
            })
            ->distinct()
            ->get();

        $recharge = (array) null;
        $recharge['total'] = count($rechargelist);
        $recharge['pages'] = ceil(count($rechargelist) / $maxresults);

        return $recharge;
    }

    public function getRechargesList($options)
    {
        $maxresults = 50;
        $search = null;
        $status = null;
        $fromdate = null;
        $todate = null;
        $pagenum = null;

        if (isset($options["maxresults"])) {
            $country = $options["maxresults"];
        }

        if (isset($options["status"])) {
            $status = $options["status"];
        }

        if (isset($options["fromdate"])) {
            $fromdate = $options["fromdate"];
        }

        if (isset($options["todate"])) {
            $todate = $options["todate"];
        }

        if (isset($options["pagenum"])) {
            $pagenum = $options["pagenum"];
        }

        if (isset($options["search"]) && ($options["search"] !== null || $options["search"] !== "")) {
            $search = $options["search"];
            $status = null;
            $fromdate = null;
            $todate = null;
            $pagenum = null;
        }

        $recharges = Subscriptions::select('id','email','recharge_date','total_recharge','status','unrealizedCust')
        ->when($search != null, function ($q) use ($search) {
            if ($search === "all") {
                return $q;
            }
            else {
                return $q->where('email', 'LIKE', $search);
            }
        })
        ->when($status != null, function ($q) use ($status) {
            if ($status === "all") {
                return $q;
            }
            else {
                return $q->where('status', $status);
            }
        })
        ->when($fromdate != null, function ($q) use ($fromdate) {
            return $q->whereDate('recharge_date', '>=', date("Y-m-d", strtotime($fromdate)));
        })
        ->when($todate != null, function ($q) use ($todate) {
            return $q->whereDate('recharge_date', '<=', date("Y-m-d", strtotime($todate)));
        })
        ->distinct()
        ->paginate($maxresults, ['*'], 'page', $pagenum)->values();

        return $recharges;
    }
}