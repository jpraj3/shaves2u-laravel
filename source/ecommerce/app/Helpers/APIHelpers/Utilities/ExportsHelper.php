<?php

namespace App\Helpers\APIHelpers\Utilities;

use App\Exports\OrdersExport;
use App\Exports\BulkOrdersExport;
use App\Exports\CancellationsExport;
use App\Exports\ReferralsExport;
use App\Exports\ReferralsCashOutExport;
use App\Exports\ReportMasterExport;
use App\Exports\ReportAppcoExport;
use App\Exports\ReportMOExport;
use App\Exports\SubscribersExport;
use App\Exports\CustomersExport;
use App\Exports\RechargesExport;
use App\Exports\PausePlansExport;
use App\Helpers\AWSHelper;
use App\Helpers\PlanHelper;
use App\Helpers\SubscriptionHelper;
use App\Models\BaWebsite\MarketingOffice;
use App\Models\BaWebsite\SellerUser;
use App\Models\FileUploads\FileUploads;
use App\Models\GeoLocation\Countries;
use App\Models\Orders\OrderDetails;
use App\Models\Orders\OrderHistories;
use App\Models\Orders\Orders;
use App\Models\BulkOrders\BulkOrderDetails;
use App\Models\BulkOrders\BulkOrderHistories;
use App\Models\BulkOrders\BulkOrders;
use App\Models\Promotions\PromotionCodes;
use App\Models\Receipts\Receipts;
use App\Models\User\DeliveryAddresses;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Str;
use Exception;

class ExportsHelper
{
    public function __construct()
    {
        $this->awsHelper = new AWSHelper();
        $this->planHelper = new PlanHelper();
        $this->subscriptionHelper = new SubscriptionHelper();
        $this->Log = \Log::channel('cronjob');
    }

    public function orders($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = $options["status"] !== "all" ? strtolower($options["status"]) . "-orders-report-" : "orders-report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $order_) {
                    // Initialize variables
                    $order = null;
                    $delivery_address = null;
                    $billing_address = null;
                    $order_details = null;
                    $receipt = null;
                    $country = null;
                    $promotion = null;
                    $seller = null;
                    $marketing_office = null;
                    $cancellationReason = null;
                    $cancelledDate = null;

                    // build data
                    $order = Orders::where('id', $order_->id)->first();
                    $delivery_address = DeliveryAddresses::where('id', $order->DeliveryAddressId)->first();
                    $billing_address = DeliveryAddresses::where('id', $order->BillingAddressId)->first();
                    $order_details = OrderDetails::where('OrderId', $order->id)->get();
                    $orderhistories = OrderHistories::where('OrderId', $order->id)->get();
                    $receipt = Receipts::where('OrderId', $order->id)->first();
                    $country = Countries::where('id', $order->CountryId)->first();
                    if ($order->PromotionId !== null) {
                        $promotion = PromotionCodes::where('PromotionId', $order->PromotionId)->first();
                    }

                    // BA Info
                    if ($order->SellerUserId !== null) {
                        $seller = SellerUser::where('id', $order->SellerUserId)->first();
                        $marketing_office = MarketingOffice::where('id', $seller->MarketingOfficeId)->first();
                    }

                    // get cancellationReason if order was canceled from orderHistories
                    if ($order->status == 'Cancelled') {
                        for ($i = count($orderhistories) - 1; $i >= 0; $i--) {
                            $orderHistory = $orderhistories[$i];
                            // get the if lastest history was canceled
                            if ($i == count($orderhistories) - 1 && $orderHistory->message == 'Cancelled') {
                                $cancellationReason = $orderHistory->cancellationReason;
                                $cancelledDate = Carbon::parse($orderHistory->created_at)->format('YYYY-MM-DD');
                                break;
                            }
                        }
                    }

                    $e = [
                        "Order" => $order !== null ? $order->id : null,
                        "SaleDate" => $order !== null ? $order->created_at : null,
                        "Region" => $country !== null ? $country->CodeIso : null,
                        "OrderStatus" => $order !== null ? $order->status : null,
                        "CanceledDate" => $cancelledDate,
                        "CancellationReason" => $cancellationReason,
                        "Category" => $order !== null && $seller !== null ? 'Sales app' : 'ecommerce',
                        "BadgeId" => $seller !== null ? $seller->badgeId : null,
                        "AgentName" => $seller !== null ? $seller->agentName : null,
                        "ChannelType" => $order !== null ? $order->channelType : null,
                        "EventLocationCode" => $order !== null ? $order->eventLocationCode : null,
                        "DirectDelivery" => $order !== null && isset($order->isDirectTrial) ? ($order->isDirectTrial === 0 ? 'No' : 'Yes') : null,
                        "MOCode" => $marketing_office !== null ? $marketing_office->moCode : null,
                        "CustomerName" => $order !== null ? $order->fullName : null,
                        "Email" => $order !== null ? $order->email : null,
                        "SSN" => $order !== null ? $order->SSN : null,
                        "DeliveryAddress" => $delivery_address !== null ? $delivery_address->address . ',' . $delivery_address->portalCode . ',' . $delivery_address->city . ' ' . $delivery_address->state : null,
                        "DeliveryContactNumber" => $delivery_address !== null ? $delivery_address->contactNumber : null,
                        "BillingAddress" => $billing_address !== null ? $billing_address->address . ',' . $billing_address->portalCode . ',' . $billing_address->city . ' ' . $billing_address->state : null,
                        "BillingContactNumber" => $billing_address !== null ? $billing_address->contactNumber : null,
                        "TrackingNumber" => $order->deliveryId,
                        "Sku" => "test",
                        "ProductName" => "test",
                        "PaymentType" => $order !== null ? $order->paymentType : null,
                        "Currency" => $country !== null ? $country->currencyDisplay : null,
                        "PromoCode" => $promotion !== null ? $promotion->code : null,
                        "TotalPrice" => $receipt->totalPrice,
                        "DiscountAmount" => $receipt->discountAmount,
                        "SubTotalPrice" => $receipt->subTotalPrice,
                        "TaxAmount" => $receipt->taxAmount,
                        "ShippingFee" => $receipt->shippingFee,
                        "GrandTotal" => $receipt->totalPrice,
                        "Source" => $order !== null ? $order->source : null,
                        "Medium" => $order !== null ? $order->medium : null,
                        "Campaign" => $order !== null ? $order->campaign : null,
                        "Term" => $order !== null ? $order->term : null,
                        "Content" => $order !== null ? $order->content : null,
                        // "discountPrice" => $receipt !== null ? $receipt->subTotalPrice - $receipt->discountAmount : $receipt->subTotalPrice,
                        // "createdAt" => $order !== null ? $order->created_at : null,
                        // "cancellationReason" => $cancellationReason,
                        // "canceledDate" => $cancelledDate,

                    ];

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new OrdersExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function bulkorders($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = $options["status"] !== "all" ? strtolower($options["status"]) . "-bulkorders-report-" : "bulkorders-report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $bulkorder_) {
                    // Initialize variables


                    $e = [
                        "Order" => $bulkorder_ !== null ? $bulkorder_->bulkorder_no : null,
                        "Sku" =>  $bulkorder_ !== null ? $bulkorder_->skus : null,
                        "AgentName" => null,
                        "Email" => $bulkorder_ !== null ? $bulkorder_->email : null,
                        "ProductName" =>  $bulkorder_ !== null ? $bulkorder_->pname : null,
                        "ProductUnits" => $bulkorder_ !== null ? $bulkorder_->qty : null,
                        "ProductPrice" => $bulkorder_ !== null ? $bulkorder_->price : null,
                        "TotalPrice" => $bulkorder_ !== null ? $bulkorder_->totalPrice : null,
                        "Currency" =>  $bulkorder_ !== null ? $bulkorder_->currency : null,
                        "PaymentType" =>  $bulkorder_ !== null ? $bulkorder_->paymentType : null,
                        "Region" => $bulkorder_ !== null ? $bulkorder_->code : null,
                        "Category" => "client",
                        "State" => $bulkorder_ !== null ? $bulkorder_->status : null,
                        "SaleDate" => $bulkorder_ !== null ? $bulkorder_->created_at : null,
                    ];

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new BulkOrdersExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function referrals($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                // if ($options["fromdate"] !== null) {
                //     $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                // }
                // if ($options["todate"] !== null) {
                //     $options["todate"] = str_replace("/", "-", $options["todate"]);
                // }

                // //format filters
                // $filter_status = $options["status"] !== "all" ? strtolower($options["status"]) . "-referrals-report-" : "referrals-report-";
                // $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                // $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                // $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = "referrals-report-" . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $referral_) {
                    // Initialize variables


                    $e = [
                        "Referee mails" => $referral_ !== null ? $referral_->femail : '-',
                        "OrderNumbers" =>  $referral_ !== null ? $referral_->OrderId !== null ? $referral_->OrderId : '-' : '-',
                        "Channel" => $referral_ !== null ? $referral_->source : '-',
                        "Status" => $referral_ !== null ? $referral_->newstatus : '-',
                        "DateOfRegistration" =>  $referral_ !== null ? $referral_->created_at : '-',
                        "DateOfFirstOrder" =>  $referral_ !== null ? ($referral_->firstorderDate != null ? $referral_->firstorderDate : '-') : '-',
                        "Refereremails" => $referral_ !== null ? $referral_->remail : '-',
                        "RefererCountry" => $referral_ !== null ? $referral_->countrycode : '-',
                        "RefererConversion" => $referral_ !== null ? $referral_->conversion : '-',
                        "earned_each" => $referral_ !== null ? $referral_->totalearn : '-',
                        "Referercurrentcreditbalance" =>  $referral_ !== null ? $referral_->curenttotal : '-',
                        "Referertotalcreditearned" =>  $referral_ !== null ? $referral_->totalearn : '-',
                        "Referercashoutall" =>  $referral_ !== null ? ($referral_->totalearn == $referral_->withdrawn_total ? 'Yes' : 'No') : 'No',
                        "Referercashoutdate" =>  $referral_ !== null ? ($referral_->withdrawn_date != null ? $referral_->withdrawn_date : '-') : '-',
                    ];

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new ReferralsExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function referralscashout($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
            
                $filename = "referrals-cash-out-report-" . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $referral_) {
                    // Initialize variables


                    $e = [
                        "Email" => $referral_ !== null ? $referral_->email : '-',
                        "UserID" =>  $referral_ !== null ? $referral_->UserId : '-',
                        "Country" => $referral_ !== null ? $referral_->cname : '-',
                        "TotalCreditAvailable" => $referral_ !== null ? $referral_->availablemoney : '-',
                        "CashoutAmount" =>  $referral_ !== null ? $referral_->amount : '-',
                        "BankName" =>  $referral_ !== null ? $referral_->bank_name : '-',
                        "FullNameAsPerBank" => $referral_ !== null ? $referral_->name : '-',
                        "BankAccountNumber" => $referral_ !== null ? $referral_->bank_account_no : '-',
                        "Status" => $referral_ !== null ? $referral_->status : '-',
                        "RequestDateTime" => $referral_ !== null ? $referral_->created_at : '-',
                    ];

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new ReferralsCashOutExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function subscribers($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "subscribers-report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'reportType' => 'subscribers-report',
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $subscriber_) {
                    // Initialize variables
                    $e = [
                        "UserID" => $subscriber_ !== null ? (isset($subscriber_->uid) && $subscriber_->uid  != "" ? $subscriber_->uid  : '-') : '-',
                        "SubscriptionId" =>  $subscriber_ !== null ? (isset($subscriber_->formattedId) && $subscriber_->formattedId  != "" ? $subscriber_->formattedId  : '-') : '-',
                        "CustomerName" => $subscriber_ !== null ? (isset($subscriber_->ufullname) && $subscriber_->ufullname  != "" ? $subscriber_->ufullname  : '-') : '-',
                        "Email" =>  $subscriber_ !== null ? (isset($subscriber_->email) && $subscriber_->email  != "" ? $subscriber_->email  : '-') : '-',
                        "Country" => $subscriber_ !== null ? (isset($subscriber_->countryName) && $subscriber_->countryName  != "" ? $subscriber_->countryName  : '-') : '-',
                        "Category" => $subscriber_ !== null ? (isset($subscriber_->isOffline) && $subscriber_->isOffline == 1 ? "BA" : "eCommerce") : '-',
                        "MOCode" => $subscriber_ !== null ? (isset($subscriber_->moCode) && $subscriber_->moCode  != "" ? $subscriber_->moCode  : '-') : '-',
                        "BadgeID" => $subscriber_ !== null ? (isset($subscriber_->badgeId) && $subscriber_->badgeId  != "" ? $subscriber_->badgeId  : '-') : '-',
                        "DeliveryAddress" => $subscriber_ !== null ? (isset($subscriber_->deliveryaddress) && $subscriber_->deliveryaddress  != "" ? $subscriber_->deliveryaddress  : '-') : '-',
                        "PlanType" => $subscriber_ !== null ? (isset($subscriber_->isTrial) && $subscriber_->isTrial == 1 ? "TK" : (isset($subscriber_->isCustom) && $subscriber_->isCustom == 1 ? "Custom" : "Custom")) : 'Custom',
                        "BladeType" => $subscriber_ !== null ? (isset($subscriber_->blade_type) && $subscriber_->blade_type !== null ? $subscriber_->blade_type : "0") : '-',
                        "DeliveryInterval" => $subscriber_ !== null ? (isset($subscriber_->isAnnual) && $subscriber_->isAnnual != 0 ? '12' : (isset($subscriber_->duration) && $subscriber_->duration != "" ? $subscriber_->duration  : '-')) : '-',
                        "CassetteCycle" => $subscriber_ !== null ? (isset($subscriber_->blade_quantity) && $subscriber_->blade_quantity  != "" ? $subscriber_->blade_quantity  : '-') : '-',
                        "CycleCount" => $subscriber_ !== null ?  (isset($subscriber_->currentDeliverNumber) && $subscriber_->currentDeliverNumber  != "" ? $subscriber_->currentDeliverNumber  : '0') : '0',
                        "AfterShaveCream" => $subscriber_ !== null ? (isset($subscriber_->after_shave_cream) && $subscriber_->after_shave_cream !== null ? "1" : "0") : "0",
                        "ShaveCream" => $subscriber_ !== null ? (isset($subscriber_->shave_cream) && $subscriber_->shave_cream !== null ? "1" : "0") : "0",
                        "Pouch" => $subscriber_ !== null ? (isset($subscriber_->pouch) && $subscriber_->pouch !== null ? "1" : "0") : "0",
                        "RubberHandle" => $subscriber_ !== null ? (isset($subscriber_->rubber_handle) && $subscriber_->rubber_handle !== null ? "1" : "0") : "0",
                        "SignUp" => $subscriber_ !== null ? (isset($subscriber_->created_date) && $subscriber_->created_date  != "" ? Carbon::parse($subscriber_->created_date)->format('Y-m-d')  : '-') : '-',
                        "Conversion" => $subscriber_ !== null ? (isset($subscriber_->conversion_date) && $subscriber_->conversion_date  != "" ? $subscriber_->conversion_date  : '-') : '-',
                        "LastDelivery" => $subscriber_ !== null ? (isset($subscriber_->lastDeliverydate) && $subscriber_->lastDeliverydate  != "" ? Carbon::parse($subscriber_->lastDeliverydate)->format('Y-m-d')  : '-') : '-',
                        "LastCharge" => $subscriber_ !== null ? (isset($subscriber_->currentOrderTime) && $subscriber_->currentOrderTime  != "" ? Carbon::parse($subscriber_->currentOrderTime)->format('Y-m-d')  : '-') : '-',
                        "NextCharge" => $subscriber_ !== null ? (isset($subscriber_->nextChargeDate) && $subscriber_->nextChargeDate  != "" ? $subscriber_->nextChargeDate  : '-') : '-',
                        "RechargeAttempt" => $subscriber_ !== null ? (isset($subscriber_->recharge_date) && $subscriber_->recharge_date !== null ? "Yes" : "No") : "No",
                        "RechargeCount" => $subscriber_ !== null ? (isset($subscriber_->total_recharge) && $subscriber_->total_recharge !== null ? $subscriber_->total_recharge : "0") : '0',
                        "Cancellation Date" => $subscriber_ !== null ? (isset($subscriber_->cancellationcreated_date) && $subscriber_->cancellationcreated_date  != "" ? Carbon::parse($subscriber_->cancellationcreated_date)->format('Y-m-d')  : '-') : '-',
                        "Status" => $subscriber_ !== null ? (isset($subscriber_->substatus) && $subscriber_->substatus  != "" ? $subscriber_->substatus  : '-') : '-',
                        "CancellationReason" => $subscriber_ !== null ? (isset($subscriber_->cancellationdefaultContent) && $subscriber_->cancellationdefaultContent  != "" ? $subscriber_->cancellationdefaultContent  : '-') : '-',
                        "Other Reason" => $subscriber_ !== null ? (isset($subscriber_->cancellationotherreason) && $subscriber_->cancellationotherreason  != "" ? $subscriber_->cancellationotherreason  : '-') : '-',
                        "PauseSubscription" => $subscriber_ !== null ? (isset($subscriber_->pauseplan) && $subscriber_->pauseplan  != "" ? $subscriber_->pauseplan  : '-') : '-',
                        // "ReactivatedSubscription" => null,
                        // "ReactivatedSubscription" => $subscriber_ !== null ? ($subscriber_->reactivatedplan != 1 ? "Yes" : "No") : null,
                    ];
                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new SubscribersExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // $csv_data = new SubscribersExport($data);

                // Excel::store(function($excel) use ($csv_data) {
                //     foreach (array_chunk($csv_data, 1000) as $chunked)
                //     {
                //         foreach ($chunked as $row)
                //         {
                //             $excel->appendRow($row);
                //         }
                //     }
                // },
                // config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function cancellations($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "cancellations-report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'reportType' => 'cancellation-report',
                    'AdminId' => (int) $options["adminId"]
                ]);

                foreach ($result as $cancellation_) {
                    // Initialize variables
                    $plantype = $cancellation_ !== null ? $cancellation_->planType : null;
                    $plansub = $cancellation_ !== null ? $cancellation_->subscriptionName : null;
                    $e = [
                        "UserID" => $cancellation_ !== null ? $cancellation_->UserId : "-",
                        "SubscriptionId" => $cancellation_ !== null ? $cancellation_->SubscriptionId : "-",
                        "Country" =>  $cancellation_ !== null ? ($cancellation_->country != null ? $cancellation_->country->name : "-") : "-",
                        "Email Address" => $cancellation_ !== null ? $cancellation_->email : "-",
                        "Reason" => $cancellation_ !== null ? $cancellation_->defaultContent ? $cancellation_->defaultContent : "-" : "-",
                        "OtherReason" =>  $cancellation_ !== null ? $cancellation_->OtherReason ? $cancellation_->OtherReason : "-" : "-",
                        "PlanType" => $plantype . " Plan",
                        "BladeType" => $cancellation_ !== null ? $cancellation_->blade_type : "-",
                        "DeliveryInterval" => $cancellation_ !== null ? $cancellation_->frequency : "-",
                        "CassetteCycle" => $cancellation_ !== null ? $cancellation_->blade_quantity : "-",
                        "CreatedDayTime" => $cancellation_ !== null ? $cancellation_->created_at : null,
                        "CanceledPlan" => $cancellation_ !== null ? $cancellation_->hasCancelled === 1 ? "Yes" : "No" : "No",
                        "ChangedBlade" => $cancellation_ !== null ? $cancellation_->modifiedBlade ? $cancellation_->modifiedBlade : "-" : "-",
                        "DeliveryMonth" => $cancellation_ !== null ? $cancellation_->modifiedFrequency ? $cancellation_->modifiedFrequency : "-" : "-",
                        "GetDiscount" => $cancellation_ !== null ? $cancellation_->discountPercent ? $cancellation_->discountPercent : "-" : "-",
                        "GetNewBlade" => $cancellation_ !== null ? $cancellation_->hasGetFreeProduct === 1 ? "Yes" : "No" : "No",
                        "OnHoldPlan" => $cancellation_ !== null ? $cancellation_->hasPausedSubscription  === 1 ? "Yes" : "No" : "No",
                    ];

                    array_push($data, $e);
                }
                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new CancellationsExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                    'isReport' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('file_id', $file_id)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e->getMessage()];
        }
    }

    public function reportsmaster($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "sales report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'reportType' => 'sales-report',
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);
                $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];

                foreach ($result as $report_) {

                    // Initialize variables
                    $getChargeid = '';
                    if ($report_ != null && $report_ != '') {
                        if ($report_->CountryId == 8) {
                            if ($report_->imp_uid != null && $report_->imp_uid !== '') {
                                $getChargeid = $report_->imp_uid;
                            } else {
                                $getChargeid = '';
                            }
                        } else {
                            $getChargeid =  $report_->rchargeId;
                        }
                    }

                    // Tax amount calculation (Based on edm)
                    $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                    $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                    // Let this be referral Discount Section
                    $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                    if ($report_->promoCode && $report_->promoCode == "referral") {
                        // For referral Discounts, Set cash rebate to equal to Discount Amount
                        $cashRebate = $report_->discountAmount;
                        // Then set Discount ammount to 0
                        $discountAmount = number_format((float) 0, 2, '.', '');
                    } else {
                        // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                        $cashRebate = number_format((float) 0, 2, '.', '');
                    }

                    $portalCode = $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? explode(',', $report_->deliveryAddress) : '-') : '-';
                    if ($portalCode !== '-') {
                        array_pop($portalCode);
                        $portalCode = end($portalCode);
                    }
                    $bportalCode = $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? explode(',', $report_->billingAddress) : '-') : '-';
                    if ($bportalCode !== '-') {
                        array_pop($bportalCode);
                        $bportalCode = end($bportalCode);
                    }
                    $category = (isset($report_->category) && $report_->category === 'sales app') ? 'BA' : ((isset($report_->category) && $report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                    $e = [
                        "Order" => $report_ !== null ? (isset($report_->order_no) && $report_->order_no != "" ? $report_->order_no : '-') : '-',
                        "OrderStatus" =>  $report_ !== null ? (isset($report_->status) && $report_->status != "" ? $report_->status : '-') : '-',
                        "SaleDate" => $report_ !== null ? (isset($report_->saleDate) && $report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                        "CompletionDate" => $report_ !== null ? (isset($report_->completed_date) && $report_->completed_date != null ? $report_->completed_date : '-') : '-',
                        "Region" => $report_ !== null ? (isset($report_->region) && $report_->region != "" ? $report_->region : '-') : '-',
                        "Category" => isset($category) ? $category : '-',
                        "MOCode" => $report_ !== null ? (isset($report_->moCode) && $report_->moCode != "" ? $report_->moCode : '-') : '-',
                        "BadgeId" =>  $report_ !== null ? (isset($report_->badgeId) && $report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                        "AgentName" =>  $report_ !== null ? (isset($report_->agentName) && $report_->agentName != "" ? $report_->agentName : '-') : '-',
                        "ChannelType" =>  $report_ !== null ? (isset($report_->channelType) && $report_->channelType != "" ? $report_->channelType : '-') : '-',
                        "EventLocationCode" => $report_ !== null ? (isset($report_->eventLocationCode) && $report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                        "DeliveryMethod" =>  $report_ !== null ? (isset($report_->isDirectTrial) && $report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                        "TrackingNumber" => $report_ !== null ? (isset($report_->deliveryId) && $report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                        "UserID" => $report_ !== null ? (isset($report_->UserId) && $report_->UserId != "" ? $report_->UserId : '-') : '-',
                        "CustomerName" => $report_ !== null ? (isset($report_->customerName) && $report_->customerName != "" ? $report_->customerName : '-') : '-',
                        "Email" => $report_ !== null ? (isset($report_->email) && $report_->email != "" ? $report_->email : '-') : '-',
                        "DeliveryAddress" => $report_ !== null ? (isset($report_->deliveryAddress) && $report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                        "DeliveryPostcode" => $portalCode,
                        "DeliveryContactNumber" => $report_ !== null ? (isset($report_->deliveryContact) && $report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                        "BillingAddress" => $report_ !== null ? (isset($report_->billingAddress) && $report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                        "BillingPostcode" => $bportalCode,
                        "BillingContactNumber" => $report_ !== null ? (isset($report_->billingContact) && $report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                        "ProductCategory" => $report_ !== null ? (isset($report_->productCategory) && $report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                    ];

                    $unitcount = 0;
                    $checkskuget = 0;
                    $unitcount_array = [];

                    if (strpos($report_->sku, ',') !== false) {
                        $report_->sku = str_replace(" ", "", $report_->sku);
                        $report_->qty = str_replace(" ", "", $report_->qty);
                        $splitproduct = explode(",", $report_->sku);
                        $splitqty = explode(",", $report_->qty);
                        foreach ($srplans as $sp) {
                            $checkskuget = 0;
                            if ($report_->SubscriptionIsTrial == 1) {
                                foreach ($splitproduct as $key => $p) {
                                    if (($sp == $p) && !in_array($p, config('global.all.handle_types.skusV2'))) {
                                        $unitcount = $unitcount + (int) $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] = $splitqty[$key];
                                    }
                                }
                            } else {
                                foreach ($splitproduct as $key => $p) {
                                    if ($sp == $p) {
                                        $unitcount = $unitcount + (int) $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] = $splitqty[$key];
                                    }
                                }
                            }
                            if ($checkskuget == 1) {
                            } else {
                                $e["" . $sp] =  "0";
                            }
                        }
                    } else {
                        if ($report_->SubscriptionIsTrial == 1) {
                            foreach ($srplans as $sp) {
                                if (($sp == $report_->sku) && !in_array($report_->sku, config('global.all.handle_types.skusV2'))) {
                                    $unitcount = $unitcount + $report_->qty;

                                    $e["" . $sp] = $report_->qty;
                                } else {

                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {
                            foreach ($srplans as $sp) {
                                if ($sp == $report_->sku) {
                                    $unitcount = $unitcount + $report_->qty;

                                    $e["" . $sp] = $report_->qty;
                                } else {

                                    $e["" . $sp] =  "0";
                                }
                            }
                        }
                    }
                    // if ($report_->productCategory === 'Subscription') {
                    //     $foundhandle = false;
                    //     $foundblade = false;

                    //     if (strpos($report_->sku, ',') !== false) {

                    //         $splitproduct = explode(",", $report_->sku);
                    //         $splitqty = explode(",", $report_->qty);

                    //         if ($report_->isTrial == 1 && $report_->isSubsequentOrder == 0 ) {
                    //             // if is Trial Plan and before first billing
                    //             $trial_skus = config('global.all.trial_types.'.$report_->gender);

                    //             foreach ($trial_skus['combinations'] as $trs) {
                    //                 if (count(array_intersect($trs, $splitproduct)) == count($splitproduct)) {
                    //                     array_push($productslist, ["sku" => $trial_skus['trial_skus'], "qty" => $splitqty['0']]);
                    //                 }
                    //             }
                    //         }
                    //         else {
                    //             foreach ($splitproduct as $key => $p) {
                    //                 array_push($productslist, ["sku" => $splitproduct[$key], "qty" => $splitqty[$key]]);
                    //             }
                    //         }

                    //         foreach ($srplans as $sp) {
                    //             $checkskuget = 0;
                    //             foreach ($productslist as $product) {
                    //                 if ($sp == $p) {
                    //                     $unitcount = $unitcount + $product['sku'];
                    //                     $checkskuget = 1;

                    //                     $e["" . $sp] = $product['sku'];
                    //                 }
                    //             }
                    //             if ($checkskuget == 1) { } else {
                    //                 $e["" . $sp] =  "0";
                    //             }
                    //         }
                    //     } else {

                    //         $planproduct = $this->planHelper->getPlanProductBySKU($report_->sku, $report_->CountryId);
                    //         $addons = $this->subscriptionHelper->getProductAddonsbySubscription($report_->subscriptionIds, $report_->CountryId);

                    //         $planqty = 1;

                    //         if ($report_->isTrial == 1 && $report_->isSubsequentOrder == 0 ) {
                    //             // if is Trial Plan and before first billing
                    //             $trial_skus = config('global.all.trial_types.'.$report_->gender);

                    //             $planproduct_array = [];
                    //             foreach ($planproduct as $p) {
                    //                 array_push($planproduct_array, $p->productssku);
                    //                 $planqty = $p->qty;
                    //             }
                    //             foreach ($trial_skus['combine_skus'] as $key => $trs) {
                    //                 if (count(array_intersect($trs, $planproduct_array)) == count($planproduct_array)) {
                    //                     array_push($productslist, ["sku" => $key, "qty" => $planqty]);
                    //                 }
                    //             }
                    //         }
                    //         else {
                    //             foreach ($planproduct as $p) {
                    //                 array_push($productslist, ["sku" => $p->productssku, "qty" => $p->qty]);
                    //                 $planqty = $p->qty;
                    //             }

                    //             foreach ($addons as $a) {
                    //                 array_push($productslist, ["sku" => $a->sku, "qty" => $planqty]);
                    //             }
                    //         }

                    //         if (!empty($productslist)) {
                    //             foreach ($srplans as $sp) {
                    //                 $checkskuget = 0;
                    //                 foreach ($productslist as $product) {
                    //                     if ($sp == $product['sku']) {

                    //                         $unitcount = $unitcount + $product['qty'];
                    //                         $checkskuget = 1;
                    //                         $e["" . $sp] =  $product['qty'];
                    //                     }
                    //                 }
                    //                 if ($checkskuget == 1) { } else {
                    //                     $e["" . $sp] =  "0";
                    //                 }
                    //             }
                    //         } else {
                    //             foreach ($srplans as $sp) {
                    //                 if ($sp == $report_->sku) {

                    //                     $unitcount = $unitcount + $report_->qty;
                    //                     $e["" . $sp] = $report_->qty;
                    //                 } else {
                    //                     $e["" . $sp] =  "0";
                    //                 }
                    //             }
                    //         }
                    //     }
                    // } else {
                    //     if (strpos($report_->sku, ',') !== false) {
                    //         $splitproduct = explode(",", $report_->sku);
                    //         $splitqty = explode(",", $report_->qty);
                    //         return $splitproduct;

                    //         foreach ($splitproduct as $key => $p) {
                    //             array_push($productslist, ["sku" => $splitproduct[$key], "qty" => $splitqty[$key]]);
                    //         }

                    //         foreach ($srplans as $sp) {
                    //             $checkskuget = 0;
                    //             foreach ($splitproduct as $key => $p) {
                    //                 if ($sp == $p) {
                    //                     $unitcount = $unitcount +  $splitqty[$key];
                    //                     $checkskuget = 1;

                    //                     $e["" . $sp] = $splitqty[$key];
                    //                 }
                    //             }
                    //             if ($checkskuget == 1) { } else {
                    //                 $e["" . $sp] =  "0";
                    //             }
                    //         }
                    //     } else {
                    //         foreach ($srplans as $sp) {
                    //             if ($sp == $report_->sku) {
                    //                 $unitcount = $unitcount + $report_->qty;

                    //                 $e["" . $sp] = $report_->qty;
                    //             } else {

                    //                 $e["" . $sp] =  "0";
                    //             }
                    //         }
                    //     }
                    // }
                    $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                    $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');
                    $processingfee = ['5', '6.5', '46', '4000', '175', '3000', '40', '150'];

                    $e["UnitTotal"] = (string) $unitcount;
                    $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                    $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                    $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" ? $report_->cCardType : '-') : '-';
                    $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                    $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                    $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                    $e["DiscountAmount"] = $discountAmount;
                    $e["CashRebate"] = $cashRebate;
                    $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';

                    $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                    if (isset($report_->screated_at)) {
                        if ($report_->screated_at <= "2019-12-08 19:00:00") {
                            $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->shippingFee != "" ? $report_->shippingFee : '-') : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                        }
                    }
                    if (!in_array($e["ProcessingFee"], $processingfee)) {
                        $e["ProcessingFee"] = "0";
                    }

                    $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                    $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                    $e["TaxAmount"] = $taxAmount;
                    $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                    $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                    $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                    $e["Source"] = $report_->source ? $report_->source : "none";
                    $e["Medium"] = $report_->medium ? $report_->medium : "none";
                    $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                    $e["Term"] = $report_->term ? $report_->term : "none";
                    $e["Content"] = $report_->content ? $report_->content : "none";

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                // $csv_data->chunk(1000, function ($items) use ($export, $csv)
                // {
                //     foreach ($items as $item) {
                //         fputcsv($csv, $export->map($item));
                //     }
                // });

                // $csv_data = new ReportMasterExport($data);

                // Excel::store(function($excel) use ($csv_data) {
                //     $excel->sheet(function($sheet) use ($csv_data) {
                //         foreach (array_chunk($csv_data, 1000) as $chunked)
                //         {
                //             foreach ($chunked as $row)
                //             {
                //                 $sheet->appendRow($row);
                //             }
                //         }
                //     });
                // },
                // config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);


                try {
                    // return new ReportMasterExport($data);
                    // Excel::load($path . '/exported.xls', function ($reader) {
                    //     $reader->sheet(function ($sheet) {
                    //         $sheet->appendRow([
                    //             'test1', 'test2',
                    //         ]);
                    //     });
                    // })->export('xls');
                    // $emptydata = [];
                    // $generate_csv = new ReportMasterExport($data);
                    // $generate_csv_data = $generate_csv->options;

                    // Excel::store(new ReportMasterExport($emptydata), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);
                    // Excel::load(config('environment.aws.settings.csvReportsUpload') . '/' . $filename, function ($reader) {
                    //     $reader->sheet('ReportsMaster', function ($sheet, $generate_csv_data) {
                    //         foreach ($generate_csv_data as $row) {
                    //             $sheet->appendRow([$row]);
                    //         }
                    //     });
                    // })->export('xls');

                    // $generate_csv = new ReportMasterExport($data);
                    // return count($generate_csv->options);
                    Excel::store(new ReportMasterExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);
                } catch (Exception $e) {
                    $this->Log->info('Download Report | reportsmaster process error 1 : ' . $e->getMessage());
                }
                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);
            } else {
                FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
                $this->Log->info('Download Report | reportsmaster process Cancelled : ' . $filename);
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            $this->Log->info('Download Report | reportsmaster process error 2 : ' . $e->getMessage());
        }
    }

    public function reportsappco($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "sales report(appco)-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);
                $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];

                foreach ($result as $report_) {

                    // Initialize variables
                    $getChargeid = '';
                    if ($report_ != null && $report_ != '') {
                        if ($report_->CountryId == 8) {
                            if ($report_->imp_uid != null && $report_->imp_uid != '') {
                                $getChargeid = $report_->imp_uid;
                            } else {
                                $getChargeid = '';
                            }
                        } else {
                            $getChargeid =  $report_->rchargeId;
                        }
                    }

                    // Tax amount calculation (Based on edm)
                    $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                    $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                    // Let this be referral Discount Section
                    $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                    if ($report_->promoCode && $report_->promoCode == "referral") {
                        // For referral Discounts, Set cash rebate to equal to Discount Amount
                        $cashRebate = $report_->discountAmount;
                        // Then set Discount ammount to 0
                        $discountAmount = number_format((float) 0, 2, '.', '');
                    } else {
                        // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                        $cashRebate = number_format((float) 0, 2, '.', '');
                    }

                    $category = ($report_->category === 'sales app') ? 'BA' : (($report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                    $e = [
                        "Order" => $report_ !== null ? ($report_->order_no != "" ? $report_->order_no : '-') : '-',
                        "OrderStatus" =>  $report_ !== null ? ($report_->status != "" ? $report_->status : '-') : '-',
                        "SaleDate" => $report_ !== null ? ($report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                        "Region" => $report_ !== null ? ($report_->region != "" ? $report_->region : '-') : '-',
                        "Category" => $category,
                        "MOCode" => $report_ !== null ? ($report_->moCode != "" ? $report_->moCode : '-') : '-',
                        "BadgeId" =>  $report_ !== null ? ($report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                        "AgentName" =>  $report_ !== null ? ($report_->agentName != "" ? $report_->agentName : '-') : '-',
                        "ChannelType" =>  $report_ !== null ? ($report_->channelType != "" ? $report_->channelType : '-') : '-',
                        "EventLocationCode" => $report_ !== null ? ($report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                        "DeliveryMethod" =>  $report_ !== null ? ($report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                        "TrackingNumber" => $report_ !== null ? ($report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                        "UserID" => $report_ !== null ? ($report_->UserId != "" ? $report_->UserId : '-') : '-',
                        "CustomerName" => $report_ !== null ? ($report_->customerName != "" ? $report_->customerName : '-') : '-',
                        "Email" => $report_ !== null ? ($report_->email != "" ? $report_->email : '-') : '-',
                        "DeliveryAddress" => $report_ !== null ? ($report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                        "DeliveryPostcode" => $report_ !== null ? ($report_->portalCode != "" ? $report_->portalCode : '-') : '-',
                        "DeliveryContactNumber" => $report_ !== null ? ($report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                        "BillingAddress" => $report_ !== null ? ($report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                        "BillingPostcode" => $report_ !== null ? ($report_->bportalCode != "" ? $report_->bportalCode : '-') : '-',
                        "BillingContactNumber" => $report_ !== null ? ($report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                        "ProductCategory" => $report_ !== null ? ($report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                    ];

                    $unitcount = 0;
                    $checkskuget = 0;
                    if ($report_->productCategory === 'Subscription') {
                        if (strpos($report_->sku, ',') !== false) {
                            $splitproduct = explode(",", $report_->sku);
                            $splitqty = explode(",", $report_->qty);
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($splitproduct as $key => $p) {
                                    if ($sp == $p) {
                                        $unitcount = $unitcount +  $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] =  $splitqty[$key];
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {

                            $planproduct = $this->planHelper->getPlanProductBySKU($report_->sku, $report_->CountryId);

                            if (!empty($planproduct)) {
                                foreach ($srplans as $sp) {
                                    $checkskuget = 0;
                                    foreach ($planproduct as $p) {

                                        if ($sp == $p->productssku) {
                                            $unitcount = $unitcount + $p->qty;
                                            $checkskuget = 1;
                                            $e["" . $sp] =  $p->qty;
                                        }
                                    }
                                    if ($checkskuget == 1) {
                                    } else {
                                        $e["" . $sp] =  "0";
                                    }
                                }
                            } else {
                                foreach ($srplans as $sp) {
                                    if ($sp == $report_->sku) {
                                        $unitcount = $unitcount + $report_->qty;
                                        $e["" . $sp] = $report_->qty;
                                    } else {
                                        $e["" . $sp] =  "0";
                                    }
                                }
                            }
                        }
                    } else {
                        if (strpos($report_->sku, ',') !== false) {
                            $splitproduct = explode(",", $report_->sku);
                            $splitqty = explode(",", $report_->qty);
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($splitproduct as $key => $p) {
                                    if ($sp == $p) {
                                        $unitcount = $unitcount +  $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] = $splitqty[$key];
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {
                            foreach ($srplans as $sp) {
                                if ($sp == $report_->sku) {
                                    $unitcount = $unitcount + $report_->qty;

                                    $e["" . $sp] = $report_->qty;
                                } else {

                                    $e["" . $sp] =  "0";
                                }
                            }
                        }
                    }
                    $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                    $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');

                    $e["UnitTotal"] = (string) $unitcount;
                    $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                    $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                    $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" && $report_->cCardType != null ? $report_->cCardType : '-') : '-';
                    $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                    $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                    $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';
                    $e["DiscountAmount"] = $discountAmount;
                    $e["CashRebate"] = $cashRebate;
                    $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                    $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                    $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                    $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                    $e["TaxAmount"] = $taxAmount;
                    $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                    $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                    $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                    $e["Source"] = $report_->source ? $report_->source : "none";
                    $e["Medium"] = $report_->medium ? $report_->medium : "none";
                    $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                    $e["Term"] = $report_->term ? $report_->term : "none";
                    $e["Content"] = $report_->content ? $report_->content : "none";

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new ReportAppcoExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function reportsmo($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "sales report(mo)-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);
                $srplans = ['A1', 'A2', 'A2/2018', 'A3', 'A4-2017', 'A5', 'A5/2018', 'ASK-2017', 'ASK3/2018', 'ASK5/2018', 'ASK6/2018', 'F5', 'FK5/2019', 'H1', 'H1S3/2018', 'H1S5/2018', 'H1S6/2018', 'H2', 'H3', 'H3S3/2018', 'H3S5/2018', 'H3S6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'PF', 'PS', 'S3', 'S3/2018', 'S5', 'S5/2018', 'S6', 'S6/2018', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'POUCH/2019', 'PTC-HDL','MASK/2019-BLACK','MASK/2019-BLUE','MASK/2019-GREEN','MASK/2019-GREY'];

                foreach ($result as $report_) {

                    // Initialize variables
                    $getChargeid = '';
                    if ($report_ != null && $report_ != '') {
                        if ($report_->CountryId == 8) {
                            if ($report_->imp_uid != null && $report_->imp_uid != '') {
                                $getChargeid = $report_->imp_uid;
                            } else {
                                $getChargeid = '';
                            }
                        } else {
                            $getChargeid =  $report_->rchargeId;
                        }
                    }

                    // Tax amount calculation (Based on edm)
                    $taxAmount = ($report_->grandTotalPrice - ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100))));
                    $taxAmount = number_format((float) $taxAmount, 2, '.', '');
                    // Let this be referral Discount Section
                    $discountAmount = $report_ !== null ? $report_->discountAmount : null;
                    if ($report_->promoCode && $report_->promoCode == "referral") {
                        // For referral Discounts, Set cash rebate to equal to Discount Amount
                        $cashRebate = $report_->discountAmount;
                        // Then set Discount ammount to 0
                        $discountAmount = number_format((float) 0, 2, '.', '');
                    } else {
                        // If No Discount Or non Referral Discounts, Set Cash Rebate to 0
                        $cashRebate = number_format((float) 0, 2, '.', '');
                    }

                    $category = ($report_->category === 'sales app') ? 'BA' : (($report_->category === 'client') ? 'Online / E-Commerce' : 'Bulk Order');
                    $e = [
                        "Order" => $report_ !== null ? ($report_->order_no != "" ? $report_->order_no : '-') : '-',
                        "OrderStatus" =>  $report_ !== null ? ($report_->status != "" ? $report_->status : '-') : '-',
                        "SaleDate" => $report_ !== null ? ($report_->saleDate != "" ? $report_->saleDate : '-') : '-',
                        "Region" => $report_ !== null ? ($report_->region != "" ? $report_->region : '-') : '-',
                        "Category" => $category,
                        "MOCode" => $report_ !== null ? ($report_->moCode != "" ? $report_->moCode : '-') : '-',
                        "BadgeId" =>  $report_ !== null ? ($report_->badgeId != "" ? $report_->badgeId : '-') : '-',
                        "AgentName" =>  $report_ !== null ? ($report_->agentName != "" ? $report_->agentName : '-') : '-',
                        "ChannelType" =>  $report_ !== null ? ($report_->channelType != "" ? $report_->channelType : '-') : '-',
                        "EventLocationCode" => $report_ !== null ? ($report_->eventLocationCode != "" ? $report_->eventLocationCode : '-') : '-',
                        "DeliveryMethod" =>  $report_ !== null ? ($report_->isDirectTrial == 1 ? 'Collection' : 'Delivery') : 'Delivery',
                        "TrackingNumber" => $report_ !== null ? ($report_->deliveryId != "" ? $report_->deliveryId : '-') : '-',
                        "UserID" => $report_ !== null ? ($report_->UserId != "" ? $report_->UserId : '-') : '-',
                        "CustomerName" => $report_ !== null ? ($report_->customerName != "" ? $report_->customerName : '-') : '-',
                        "Email" => $report_ !== null ? ($report_->email != "" ? $report_->email : '-') : '-',
                        "DeliveryAddress" => $report_ !== null ? ($report_->deliveryAddress != "" ? $report_->deliveryAddress : '-') : '-',
                        "DeliveryPostcode" => $report_ !== null ? ($report_->portalCode != "" ? $report_->portalCode : '-') : '-',
                        "DeliveryContactNumber" => $report_ !== null ? ($report_->deliveryContact != "" ? (string) $report_->deliveryContact : '-') : '-',
                        "BillingAddress" => $report_ !== null ? ($report_->billingAddress != "" ? $report_->billingAddress : '-') : '-',
                        "BillingPostcode" => $report_ !== null ? ($report_->bportalCode != "" ? $report_->bportalCode : '-') : '-',
                        "BillingContactNumber" => $report_ !== null ? ($report_->billingContact != "" ? (string) $report_->billingContact : '-') : '-',
                        "ProductCategory" => $report_ !== null ? ($report_->productCategory != "" ? $report_->productCategory : '-') : '-',
                    ];

                    $unitcount = 0;
                    $checkskuget = 0;
                    if ($report_->productCategory === 'Subscription') {
                        if (strpos($report_->sku, ',') !== false) {
                            $splitproduct = explode(",", $report_->sku);
                            $splitqty = explode(",", $report_->qty);
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($splitproduct as $key => $p) {
                                    if ($sp == $p) {
                                        $unitcount = $unitcount +  $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] =  $splitqty[$key];
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {

                            $planproduct = $this->planHelper->getPlanProductBySKU($report_->sku, $report_->CountryId);

                            if (!empty($planproduct)) {
                                foreach ($srplans as $sp) {
                                    $checkskuget = 0;
                                    foreach ($planproduct as $p) {

                                        if ($sp == $p->productssku) {
                                            $unitcount = $unitcount + $p->qty;
                                            $checkskuget = 1;
                                            $e["" . $sp] =  $p->qty;
                                        }
                                    }
                                    if ($checkskuget == 1) {
                                    } else {
                                        $e["" . $sp] =  "0";
                                    }
                                }
                            } else {
                                foreach ($srplans as $sp) {
                                    if ($sp == $report_->sku) {
                                        $unitcount = $unitcount + $report_->qty;
                                        $e["" . $sp] = $report_->qty;
                                    } else {
                                        $e["" . $sp] =  "0";
                                    }
                                }
                            }
                        }
                    } else {
                        if (strpos($report_->sku, ',') !== false) {
                            $splitproduct = explode(",", $report_->sku);
                            $splitqty = explode(",", $report_->qty);
                            foreach ($srplans as $sp) {
                                $checkskuget = 0;
                                foreach ($splitproduct as $key => $p) {
                                    if ($sp == $p) {
                                        $unitcount = $unitcount +  $splitqty[$key];
                                        $checkskuget = 1;

                                        $e["" . $sp] = $splitqty[$key];
                                    }
                                }
                                if ($checkskuget == 1) {
                                } else {
                                    $e["" . $sp] =  "0";
                                }
                            }
                        } else {
                            foreach ($srplans as $sp) {
                                if ($sp == $report_->sku) {
                                    $unitcount = $unitcount + $report_->qty;

                                    $e["" . $sp] = $report_->qty;
                                } else {

                                    $e["" . $sp] =  "0";
                                }
                            }
                        }
                    }
                    $GrandTotalBeforetax = ($report_->grandTotalPrice / (1 + ($report_->taxRate / 100)));
                    $GrandTotalBeforetax = number_format((float) $GrandTotalBeforetax, 2, '.', '');

                    $e["UnitTotal"] = (string) $unitcount;
                    $e["PaymentType"] = $report_ !== null ? ($report_->paymentType != "" ? $report_->paymentType : '-') : '-';
                    $e["CardBrand"] = $report_ !== null ? ($report_->rbranchName != "" ? $report_->rbranchName : '-') : '-';
                    $e["CardType"] = $report_ !== null ? ($report_->cCardType != "" ? $report_->cCardType : '-') : '-';
                    $e["Currency"] = $report_ !== null ? ($report_->currency != "" ? $report_->currency : '-') : '-';
                    $e["PromoCode"] = $report_ !== null ? ($report_->promoCode != "" ? $report_->promoCode : '-') : '-';
                    $e["TotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->subTotalPrice != "" ? $report_->subTotalPrice : '-')) : '-';
                    $e["DiscountAmount"] = $discountAmount;
                    $e["CashRebate"] = $cashRebate;
                    $e["SubTotalPrice"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? '0' : ($report_->totalPrice != "" ? $report_->totalPrice : '-')) : '-';
                    $e["ProcessingFee"] = $report_ !== null ? ($report_->isActiveTrial == 1 ? ($report_->totalPrice != "" ? $report_->totalPrice : '-') : ($report_->shippingFee != "" ? $report_->shippingFee : '-')) : '-';
                    $e["GrandTotalIncltax"] = $report_ !== null ? ($report_->grandTotalPrice != "" ? $report_->grandTotalPrice : '-') : '-';
                    $e["GrandTotalBeforetax"] = ($report_->includedTaxToProduct && $report_->grandTotalPrice) ? $GrandTotalBeforetax : $report_->grandTotalPrice;
                    $e["TaxAmount"] = $taxAmount;
                    $e["Payment Status"] =  $report_ !== null ? ($report_->paymentStatus != "" ? $report_->paymentStatus : '-') : '-';
                    $e["ChargeID"] = $getChargeid != null && $getChargeid !== '' ? $getChargeid : '-';
                    $e["TaxInvoicenumber"] =   $report_->region === 'MYS' ?  $report_->taxInvoiceNo : $report_->region === 'KOR' ?  $report_->taxInvoiceNo : $report_->region === 'SGP' ?  $report_->taxInvoiceNo : $report_->region === 'HKG' ?  $report_->taxInvoiceNo : $report_->region === 'TWN' ?  $report_->taxInvoiceNo : '';
                    $e["Source"] = $report_->source ? $report_->source : "none";
                    $e["Medium"] = $report_->medium ? $report_->medium : "none";
                    $e["Campaign"] = $report_->campaign ? $report_->campaign : "none";
                    $e["Term"] = $report_->term ? $report_->term : "none";
                    $e["Content"] = $report_->content ? $report_->content : "none";

                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new ReportMOExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function customers($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "customers report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $customer_) {
                    // Initialize variables
                    $e = [
                        "CustomerEmail" => $customer_ !== null ? $customer_->email : '-',
                        "Country" => $customer_ !== null ? ($customer_->country != null ? $customer_->country->name : '-') : '-',
                        "PhoneNumber" => $customer_ !== null ? $customer_->phone : '-',
                        "Address" => $customer_ !== null ? ($customer_->address != null ? $customer_->address->address . ', ' . $customer_->address->portalCode . ', ' . $customer_->address->city . ', ' . $customer_->address->state : '-') : '-',
                        "Consent" => $customer_ !== null ? $customer_->consent : '-',
                        "PreferedContact" => $customer_ !== null ? '-' : '-',
                        "Status" => $customer_ !== null ? ($customer_->isActive != 0 ? 'Active' : 'Inactive') : '-',
                        "RegisterDate" => $customer_ !== null ? date('d-m-y', strtotime($customer_->registered_at)) : '-',
                        "NumberSubscriptions" => $customer_ !== null ? (!empty($customer_->subscriptions) ? count($customer_->subscriptions) : '0') : '0',
                        "TKPlanSubscription" => $customer_ !== null ? ($customer_->hasTrial != null ? 'Yes' : 'No') : 'No',
                        "CustomPlanSubscription" => $customer_ !== null ? ($customer_->hasCustom != null ? 'Yes' : 'No') : 'No',
                        "PurchasedAlacarte" => $customer_ !== null ? ($customer_->hasAlacarte != null ? 'Yes' : 'No') : 'No',
                        "ReferredCustomers" => $customer_ !== null ? ($customer_->referrals != null ? 'Yes' : 'No') : 'No',
                        "TotalReferredCustomers" => $customer_ !== null ? (!empty($customer_->referrals) ? count($customer_->referrals) : '0') : '0',
                        "Addons" => $customer_ !== null ? (!empty($customer_->addons) ? 'Yes' : 'No') : 'No',
                        "PausedSubscription" => $customer_ !== null ? (!empty($customer_->pauseplan) != null ? 'Yes' : 'No') : 'No',
                    ];
                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new CustomersExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function recharges($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "recharge report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $recharge_) {
                    // Initialize variables
                    $e = [
                        "CustomerID" => $recharge_ !== null ? ($recharge_->user ? $recharge_->user->id : '-') : '-',
                        "SubscriptionID" => $recharge_ !== null ? $recharge_->id  : '-',
                        "CustomerName" => $recharge_ !== null ? $recharge_->fullname : '-',
                        "CustomerEmail" => $recharge_ !== null ? $recharge_->email : '-',
                        "Country" => $recharge_ !== null ? ($recharge_->country != null ? $recharge_->country->name : '-') : '-',
                        "SubscriptionCreatedDate" => $recharge_ !== null ? date('d-m-y', strtotime($recharge_->created_at)) : '-',
                        "SubscriptionType" => $recharge_ !== null ? ($recharge_->isTrial ? 'TK' : ($recharge_->isCustom ? 'Custom Plan' : '-')) : '-',
                        "SubscriptionTypeAcquisitionSource" => $recharge_ !== null ? ($recharge_->SellerUserId != null ? 'BA'  : 'Online / E-Commerce') : '-',
                        "NoOfDeliveries" => $recharge_ !== null ? ($recharge_->totalDeliverTimes != null ? $recharge_->totalDeliverTimes : '-') : '-',
                        "RechargeTrialNumber" => $recharge_ !== null ? ($recharge_->currentCycle ? $recharge_->currentCycle : '-') : '-',
                        "RechargeDate" => $recharge_ !== null ? date('d-m-y', strtotime($recharge_->currentOrderTime)) : '-',
                        "RechargeSuccess" => $recharge_ !== null ? ($recharge_->currentorder != null ? ($recharge_->currentorder->status === "Payment Failure" ? 'Yes' : 'No') : '-') : '-',
                        "FailureReason" => $recharge_ !== null ? ($recharge_->chargeDetails != null ? ($recharge_->chargeDetails->failure_message != null ? $recharge_->chargeDetails->failure_message : '-') : '-') : '-',
                        "CardBrand" => $recharge_ !== null ? ($recharge_->card != null ? $recharge_->card->brand  : '-') : '-',
                        "CardType" => $recharge_ !== null ? ($recharge_->card != null ? $recharge_->card->funding  : '-') : '-',
                    ];
                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new RechargesExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }

    public function pauseplans($type, $options, $result)
    {
        $data = [];
        try {
            if (!empty($result)) {
                if ($options["fromdate"] !== null) {
                    $options["fromdate"] = str_replace("/", "-", $options["fromdate"]);
                }
                if ($options["todate"] !== null) {
                    $options["todate"] = str_replace("/", "-", $options["todate"]);
                }

                //format filters
                $filter_status = "pause plans report-";
                $filter_fromdate = $options["fromdate"] !== null ? $options["fromdate"] : "for-all";
                $filter_todate = $options["todate"] !== null ? $options["todate"] : "";
                $date_text = $options["fromdate"] !== null ? $filter_fromdate . '-to-' . $filter_todate : $filter_fromdate;
                $filename = $filter_status . $date_text . '-' . Carbon::now()->format('YmdHis') . '.xlsx';
                $file_id = Str::uuid()->toString();

                //Create new fileUpload entry
                FileUploads::create([
                    'name' => $filename,
                    'isReport' => 1,
                    'file_id' => $file_id,
                    'status' => 'Processing',
                    'isDownloaded' => 0,
                    'AdminId' => (int) $options["adminId"],
                ]);

                foreach ($result as $pauseplan_) {
                    // Initialize variables
                    $e = [
                        "CustomerUniqueNumber" => $pauseplan_ ? $pauseplan_->UserId : "-",
                        "Subscription Number" => $pauseplan_ ? $pauseplan_->subscriptionIds : "-",
                        "CustomerName" => $pauseplan_ ? $pauseplan_->fullname : "-",
                        "CustomerEmail" => $pauseplan_ ? $pauseplan_->email : "-",
                        "Country" => $pauseplan_ ? ($pauseplan_->country ? $pauseplan_->country->name : "-") : "-",
                        "SubscriptionCreatedDate" => $pauseplan_ ? $pauseplan_->screated_date : "-",
                        "InitialSignupFrequency" => $pauseplan_ ? "-" : "-",
                        "ChangedFrequency" => $pauseplan_ ? "-" : "-",
                        "OptedinForpausing" => $pauseplan_ ? "Yes" : "No",
                        "SubscriptionOnholddate " => $pauseplan_ ? $pauseplan_->originaldate . " - " . $pauseplan_->resumedate : "-",
                        "TotalNumberofpauses " => $pauseplan_ ? $pauseplan_->totalCount : "-",
                    ];
                    array_push($data, $e);
                }

                $url = env("AWS_URL", "https://shaves2u-dci.s3-ap-southeast-1.amazonaws.com/") . config('environment.aws.settings.csvReportsUpload') . '/' . $filename;

                Excel::store(new PausePlansExport($data), config('environment.aws.settings.csvReportsUpload') . '/' . $filename, 's3', \Maatwebsite\Excel\Excel::XLSX);

                // update fileuploads entry
                FileUploads::where('file_id', $file_id)->update([
                    'url' => $url,
                    'status' => 'Completed',
                    'isDownloaded' => 1,
                ]);

                return ["url" => $url, "data" => $result, "error" => null];
            }
        } catch (Exception $e) {
            FileUploads::where('name', $filename)->update(['status' => 'Cancelled']);
            return ["url" => null, "error" => $e];
        }
    }
}
