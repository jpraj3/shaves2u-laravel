<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SubscribersExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "User ID",
            "Customer Name",
            "Email",
            "Country",
            "Category",
            "MO Code",
            "Badge ID",
            "Delivery Address",
            "Plan Type",
            "Blade Type",
            "Delivery Interval",
            "Cassette / Cycle",
            "No. of Cycle",
            "After Shave Cream",
            "Shave Cream",
            "Toiletry Bag",
            "Razor Protector",
            "Sign Up",
            "Conversion",
            "Last Delivery",
            "Last Charge",
            "Next Charge",
            "Recharge Attempt",
            "No. of Recharge",
            "Cancellation Date",
            "Status",
            "Cancellation Reason",
            "Other Reason",
            "Opted in For pausing subscription",
            "OnHold Date",
            "Unrealized Date"
            // "Opted in For Reactivating subscription plan",
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //     ];
    // }


    public function array(): array
    {
        return $this->options;
    }

}
