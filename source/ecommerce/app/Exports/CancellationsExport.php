<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class CancellationsExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "User ID","Subscription ID","Country","Email Address","Reason","Other Reason","Plan Type","Blade Type","Delivery Interval","Cassette / Cycle","Created Day/Time","Canceled Plan",
            "Changed Blade","Delivery Month","Get Discount","Get New Blade","On Hold Plan"
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //     ];
    // }


    public function array(): array
    {
        return $this->options;
    }

}
