<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportAppcoExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "Order Number","Order Status","Sale Date","Region",
            "Category","MO Code","Badge Id","Agent Name","Channel Type","Event/Location Code","Delivery Method","Tracking Number",
            "User ID","Customer Name","Email","Delivery Address","Delivery Postcode","Delivery Contact Number","Billing Address",
            "Billing Postcode","Billing Contact Number","Product Category",
            "A1", "A2", "A2/2018", "A3", "A4-2017", "A5", "A5/2018", "ASK-2017", "ASK3/2018", "ASK5/2018", "ASK6/2018", "F5", "FK5/2019", "H1", "H1S3/2018", "H1S5/2018", "H1S6/2018", "H2", "H3", "H3S3/2018", "H3S5/2018",  "H3S6/2018", "H3TK3/2018", "H3TK5/2018", "H3TK6/2018", "PF", "PS", "S3", "S3/2018", "S5", "S5/2018", "S6", "S6/2018", "TK3/2018", "TK5/2018", "TK6/2018","POUCH/2019","PTC-HDL","MASK/2019-BLACK","MASK/2019-BLUE","MASK/2019-GREEN","MASK/2019-GREY",
            "Unit Total","Payment Type","Card Brand","Card Type","Currency","Promo Code","Total Price","Discount Amount",
            "Cash Rebate","Sub Total Price","Processing Fee","Grand Total(Incl. tax)","Grand Total(Before tax)","Tax Amount",
            "Payment Status","Charge ID","Tax Invoice number","Source","Medium","Campaign","Term","Content",
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         "B" => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         "C" => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //     ];
    // }


    public function array(): array
    {
        return $this->options;
    }

}
