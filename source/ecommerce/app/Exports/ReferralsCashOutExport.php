<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReferralsCashOutExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "Email","User ID","Country","Total credit available","Cash-out amount","Bank Name","Full name as per bank","Bank account number","Status","Request date time"
        ];
    }

    public function array(): array
    {
        return $this->options;
    }

}
