<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrdersExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromArray, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function headings(): array
    {
        return [
            "Order","Sale Date","Region","Order Status","Canceled Date","Cancellation Reason","Category",
            "Badge Id","AgentName","Channel Type","Event/Location Code","Direct Delivery","MO Code","Customer Name","Email","SSN",
            "Delivery Address","Delivery Contact Number","Billing Address","Billing Contact Number","Tracking Number","Sku","ProductName","Payment Type",
            "Currency", "Promo Code","Total Price","Discount Amount","Sub Total Price","Tax Amount","Shipping Fee","Grand Total(Incl. tax)","Source","Medium","Campaign","Term",
            "Content"
        ];
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    //         'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    //     ];
    // }


    public function array(): array
    {
        return $this->options;
    }

}
