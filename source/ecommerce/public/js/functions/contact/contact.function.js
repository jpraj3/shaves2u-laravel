// Contact Form Element
let form_ce_contact = document.getElementById("form_ce_contact");

// Contact Form Input Elements
let c_e_topic = document.getElementById("c_e_topic");
let c_e_name = document.getElementById("c_e_name");
let c_e_email = document.getElementById("c_e_email");
let c_e_enquiry = document.getElementById("c_e_enquiry");

// Contact Form Action Elements
let c_a_submit = document.getElementById("c_a_submit");

$(document).ready(function () {
    $('#form_ce_contact').validate({
        rules: {
            c_e_topic: {
                required: true
            },
            c_e_name: {
                required: true
            },
            c_e_email: {
                required: true
            },
            c_e_phone: {
                required: true,
            },
            c_e_enquiry: {
                required: true
            },
        },

        messages: {
            c_e_topic:
            {
                required: trans('validation.custom.validation.contact.name.required'),
            },
            c_e_name: {
                required: trans('validation.custom.validation.contact.name.required'),
            },
            c_e_email: {
                required: trans('validation.custom.validation.contact.email.required'),
            },
            c_e_phone: {
                required: trans('validation.custom.validation.contact.phone.required'),
                // maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
            },
            c_e_enquiry: {
                required: trans('validation.custom.validation.contact.enquiry.required')
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "c_e_topic") {
                error.appendTo('#c_e_topic_error');
            } else if (element.attr("name") == "c_e_name") {
                error.appendTo('#c_e_name_error');
            } else if (element.attr("name") == "c_e_email") {
                error.appendTo('#c_e_email_error');
            } else if (element.attr("name") == "c_e_phone") {
                error.appendTo('#c_e_phone_error');
            } else if (element.attr("name") == "c_e_enquiry") {
                error.appendTo('#c_e_enquiry_error');
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            // $('#form_ce_contact').on("submit", function (event) {
            // event.preventDefault();
            // console.log($('#form_ce_contact').serialize());
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: GLOBAL_URL + '/contact-us',
                data: $('#form_ce_contact').serialize(),
                cache: false,
                success: function (data) {
                    // alert('Contact Form Submitted!');
                    Swal.fire({
                        title: 'Thank you for Contacting Us',
                        html: 'We will be in contact with you shortly <br /> regarding your inquiry.',
                        type: 'success',
                    }
                    ).then((result) => {
                        window.location.reload();
                    })
                    // console.log(data);
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    // alert(error);
                    // alert(status);
                    // alert(xhr);
                    // console.log(JSON.stringify(xhr));
                }

            });
            // });
        }
    });
})
