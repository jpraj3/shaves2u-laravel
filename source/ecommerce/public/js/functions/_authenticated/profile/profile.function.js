let loading = document.getElementById("loading");
// User Info Elements
let pu_e_fullname = document.getElementById('pu_e_fullname');
let pu_e_email = document.getElementById('pu_e_email');
let pu_e_password = document.getElementById('pu_e_password');
let pu_e_phone = document.getElementById('pu_e_phone');
let pu_e_phoneext = document.getElementById('pu_e_phoneext');
let pu_e_birthday_day = document.getElementById('pu_e_birthday_day');
let pu_e_birthday_month = document.getElementById('pu_e_birthday_month');
let pu_e_birthday_year = document.getElementById('pu_e_birthday_year');
let uploadTrigger = document.getElementById('uploadTrigger');
let pu_a_edit = document.getElementById('pu_a_edit');
let pu_a_save = document.getElementById('pu_a_save');
let form_pe_user_update_fail = document.getElementById('form_pe_user_update_fail');

// Delivery & Billing Address Elements
let pd_e_ssn = document.getElementById('pd_e_ssn');
let pd_e_address = document.getElementById('pd_e_address');
let pd_e_city = document.getElementById('pd_e_city');
let pd_e_portalCode = document.getElementById('pd_e_portalCode');
let pd_e_state = document.getElementById('pd_e_state');
let pd_a_edit = document.getElementById('pd_a_edit');
let pd_a_save = document.getElementById('pd_a_save');
let pb_e_address = document.getElementById('pb_e_address');
let pb_e_city = document.getElementById('pb_e_city');
let pb_e_portalCode = document.getElementById('pb_e_portalCode');
let pb_e_state = document.getElementById('pb_e_state');
let pb_a_edit = document.getElementById('pb_a_edit');
let pb_a_save = document.getElementById('pb_a_save');
let pu_a_edit_mob = document.getElementById('pu_a_edit_mob');
let pu_a_save_mob = document.getElementById('pu_a_save_mob');
let show_more_addresses = $('#show_more_addresses');
let btn_show_more_address = document.getElementById('btn_show_more_address');
let btn_cancel_show_more_address = document.getElementById('btn_cancel_show_more_address');

// Credit Card Info Elements
let pc_e_cardnumber = document.getElementById('pc_e_cardnumber');
let pc_e_cvv = document.getElementById('pc_e_cvv');
let pc_e_expiry = document.getElementById('pc_e_expiry');
let pc_a_edit = document.getElementById('pc_a_edit');
let pc_a_save = document.getElementById('pc_a_save');
let add_new_card = document.getElementById('add_new_card');
// let cancel_add_new_card = document.getElementById('cancel_add_new_card');

let isAddCardActionTriggered = false;

let redirectcard = JSON.parse(window.localStorage.getItem('redirectcard'));

if(redirectcard == true){
    window.localStorage.removeItem('redirectcard');
    window.location.href = "#add-card-section";
}

$('#pu_e_password').on('keyup', function () {
    if ($(this).val() == 0) {
        document.getElementById("password-eye").style.display = "none";
    } else {
        document.getElementById("password-eye").style.display = "block";
    }
})

function p_user_actions(type) {
    if (type == 'edit') {
        loading.style.display = "block";
        pu_e_fullname.removeAttribute('disabled');
        pu_e_email.removeAttribute('disabled');
        pu_e_password.removeAttribute('disabled');
        pu_e_phone.removeAttribute('disabled');
        pu_e_phoneext.removeAttribute('disabled');
        pu_e_birthday_day.removeAttribute('disabled');
        pu_e_birthday_month.removeAttribute('disabled');
        pu_e_birthday_year.removeAttribute('disabled');
        uploadTrigger.removeAttribute("hidden");
        pu_a_edit.setAttribute("hidden", null);
        pu_a_save.removeAttribute("hidden");
        pu_a_edit_mob.setAttribute("hidden", null);
        pu_a_save_mob.removeAttribute("hidden");

        if ($("#form_pe_user").length) {
            v_p_user("form_pe_user");
            loading.style.display = "none";
        } else {
            loading.style.display = "none";
        }
    } else if (type == 'save') {
        loading.style.display = "block";
        // $("#form_pe_user").trackChanges();
        // if ($("#form_pe_user").isChanged()) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: GLOBAL_URL + '/user/updates/info',
            method: "POST",
            cache: false,
            data: new FormData($("#form_pe_user")[0]),
            cache: false,
            contentType: false,
            processData: false,

            // Custom XMLHttpRequest
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable) {
                            $('progress').attr({
                                value: e.loaded,
                                max: e.total,
                            });
                        }
                    }, false);
                }
                return myXhr;
            }
        })
            .done(function (response) {
                if (response) {

                    pu_e_fullname.value = (response.firstName) + (response.lastName !== null ? response.lastName : '');
                    pu_e_fullname.setAttribute('disabled', null);
                    pu_e_email.value = response.email;
                    pu_e_email.setAttribute('disabled', null);
                    pu_e_password.value = "";
                    pu_e_password.setAttribute('disabled', null);
                    phone_formatted = (response.phone).replace(("+" + phoneExt), "");
                    pu_e_phone.value = phone_formatted;
                    pu_e_phone.setAttribute('disabled', null);
                    pu_e_phoneext.setAttribute('disabled', null);
                    pu_e_birthday_day.value = response.birthday.split("-")[2];
                    pu_e_birthday_day.setAttribute('disabled', null);
                    pu_e_birthday_month.value = response.birthday.split("-")[1];
                    pu_e_birthday_month.setAttribute('disabled', null);
                    pu_e_birthday_year.value = response.birthday.split("-")[0];
                    pu_e_birthday_year.setAttribute('disabled', null);
                    uploadTrigger.setAttribute("hidden", null);
                    pu_a_edit.removeAttribute("hidden");
                    pu_a_save.setAttribute("hidden", null);
                    pu_a_save_mob.setAttribute("hidden", null);
                    pu_a_edit_mob.removeAttribute("hidden");
                }
                loading.style.display = "none";
            })
            .fail(function (error) {
                // console.log(error);
                form_pe_user_update_fail.innerHTML = '';
                form_pe_user_update_fail.innerHTML = '<p style="width:100%;background:red;color:white;font-weight:700"><Error> Failed to update profile.</p>';
                pu_e_fullname.setAttribute('disabled', null);
                pu_e_email.setAttribute('disabled', null);
                pu_e_password.setAttribute('disabled', null);
                pu_e_phone.setAttribute('disabled', null);
                pu_e_phoneext.setAttribute('disabled', null);
                pu_e_birthday_day.setAttribute('disabled', null);
                pu_e_birthday_month.setAttribute('disabled', null);
                pu_e_birthday_year.setAttribute('disabled', null);
                uploadTrigger.setAttribute("hidden", null);
                pu_a_edit.removeAttribute("hidden");
                pu_a_save.setAttribute("hidden", null);
                pu_a_edit_mob.setAttribute("hidden", null);
                pu_a_save_mob.removeAttribute("hidden");
                loading.style.display = "none";
            })
        // } else {
        //     pu_e_fullname.setAttribute('disabled', null);
        //     pu_e_password.setAttribute('disabled', null);
        //     pu_e_phone.setAttribute('disabled', null);
        //     pu_e_birthday_day.setAttribute('disabled', null);
        //     pu_e_birthday_month.setAttribute('disabled', null);
        //     pu_e_birthday_year.setAttribute('disabled', null);
        //     pu_a_edit.removeAttribute("hidden");
        //     pu_a_save.setAttribute("hidden", null);
        //     loading.style.display = "none";
        // }
    }
}

function p_address_actions(type, address_edit_type, count = null) {
    if (type == 'edit') {

        loading.style.display = "block";
        if ($("#show_edit_form_other_addresses_" + count).length > 0) {
            $("#show_edit_form_other_addresses_" + count).removeAttr('hidden');
            $("#hide_address_data_" + count).hide();
        }
        if (address_edit_type == 'delivery') {

            $("#pd_e_ssn_" + count).removeAttr('disabled');
            $("#pd_e_address_" + count).removeAttr('disabled');
            $("#pd_e_city_" + count).removeAttr('disabled');
            $("#pd_e_portalCode_" + count).removeAttr('disabled');
            $("#pd_e_state_" + count).removeAttr('disabled');
            $("#pd_a_edit_" + count).hide();
            $("#pd_a_save_" + count).removeAttr('hidden');
        }

        if ($("#form_pe_delivery_" + count).length) {
            // v_p_delivery_multiple("form_pe_delivery_" + count, count);
            loading.style.display = "none";
        } else {
            loading.style.display = "none";
        }
    } else if (type == 'edit2') {
        loading.style.display = "block";

        if (address_edit_type === 'delivery') {
            if ($("#show_edit_form_for_delivery").length > 0) {
                $("#show_edit_form_for_delivery").removeAttr('hidden');
                $("#hide_address_data_delivery").hide();
            }
            if (pd_e_ssn) {
                pd_e_ssn.removeAttribute('disabled');
            }
            pd_e_address.removeAttribute('disabled');
            pd_e_city.removeAttribute('disabled');
            pd_e_portalCode.removeAttribute('disabled');
            pd_e_state.removeAttribute('disabled');
            pd_a_edit.setAttribute("hidden", null);
            pd_a_save.removeAttribute("hidden");

        }

        if (address_edit_type === 'billing') {
            if ($("#show_edit_form_for_billing").length > 0) {
                $("#show_edit_form_for_billing").removeAttr('hidden');
                $("#hide_address_data_billing").hide();
            }

            pb_e_address.removeAttribute('disabled');
            pb_e_city.removeAttribute('disabled');
            pb_e_portalCode.removeAttribute('disabled');
            pb_e_state.removeAttribute('disabled');
            pb_a_edit.setAttribute("hidden", null);
            pb_a_save.removeAttribute("hidden");
        }


        if ($("#form_pe_delivery").length) {
            v_p_delivery("form_pe_delivery");
            loading.style.display = "none";
        } else {
            loading.style.display = "none";
        }

        if ($("#form_pe_billing").length) {
            v_p_billing("form_pe_billing");
            loading.style.display = "none";
        } else {
            loading.style.display = "none";
        }
    } else if (type == 'save') {
        loading.style.display = "block";
        if (address_edit_type === 'delivery') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/user/updates/delivery',
                method: "POST",
                cache: false,
                data: $("#form_pe_delivery").serialize(),
            })
                .done(function (response) {
                    if (response) {
                        if ($("#show_edit_form_for_delivery").is(':visible')) {
                            $("#show_edit_form_for_delivery").hide();
                            $("#hide_address_data_delivery").removeAttr('hidden');
                        }
                        if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                            $("#show_edit_form_other_addresses_" + count).hide();
                            $("#hide_address_data_" + count).removeAttr('hidden');
                        }
                        setTimeout(window.location.reload(), 3000);
                    }
                    loading.style.display = "none";
                })
                .fail(function (error) {
                    if ($("#show_edit_form_for_delivery").is(':visible')) {
                        $("#show_edit_form_for_delivery").hide();
                        $("#hide_address_data_delivery").removeAttr('hidden');
                    }
                    if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                        $("#show_edit_form_other_addresses_" + count).hide();
                        $("#hide_address_data_" + count).removeAttr('hidden');
                    }
                    setTimeout(window.location.reload(), 3000);

                    loading.style.display = "none";
                })

        }

        if (address_edit_type === 'delivery_multiple') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/user/updates/delivery',
                method: "POST",
                cache: false,
                data: $("#form_pe_delivery_" + count).serialize(),
            })
                .done(function (response) {
                    if (response) {
                        if ($("#show_edit_form_for_delivery").is(':visible')) {
                            $("#show_edit_form_for_delivery").hide();
                            $("#hide_address_data_delivery").removeAttr('hidden');
                        }
                        if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                            $("#show_edit_form_other_addresses_" + count).hide();
                            $("#hide_address_data_" + count).removeAttr('hidden');
                        }
                        window.location.reload();
                    }
                    loading.style.display = "none";
                })
                .fail(function (error) {
                    if ($("#show_edit_form_for_delivery").is(':visible')) {
                        $("#show_edit_form_for_delivery").hide();
                        $("#hide_address_data_delivery").removeAttr('hidden');
                    }
                    if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                        $("#show_edit_form_other_addresses_" + count).hide();
                        $("#hide_address_data_" + count).removeAttr('hidden');
                    }
                    window.location.reload();

                    loading.style.display = "none";
                })

        }

        if (address_edit_type === 'billing') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/user/updates/delivery',
                method: "POST",
                cache: false,
                data: $("#form_pe_billing").serialize(),
            })
                .done(function (response) {
                    if ($("#show_edit_form_for_billing").is(':visible')) {
                        $("#show_edit_form_for_billing").hide();
                        $("#show_edit_form_for_billing").removeAttr('hidden');
                    }
                    setTimeout(window.location.reload(), 3000);
                    loading.style.display = "none";
                })
                .fail(function (error) {
                    if ($("#show_edit_form_for_billing").is(':visible')) {
                        $("#show_edit_form_for_billing").hide();
                        $("#show_edit_form_for_billing").removeAttr('hidden');
                    }
                    loading.style.display = "none";
                })

        }
    } else if (type == 'save2') {
        v_e_o_delivery("form_pe_delivery_" + count, count);
        if ($('#form_pe_delivery_' + count).valid() === true) { // Calling validation function
            loading.style.display = "block";
            if (address_edit_type === 'delivery') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/delivery',
                    method: "POST",
                    cache: false,
                    data: $("#form_pe_delivery").serialize(),
                })
                    .done(function (response) {
                        if (response) {
                            if ($("#show_edit_form_for_delivery").is(':visible')) {
                                $("#show_edit_form_for_delivery").hide();
                                $("#hide_address_data_delivery").removeAttr('hidden');
                            }
                            if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                                $("#show_edit_form_other_addresses_" + count).hide();
                                $("#hide_address_data_" + count).removeAttr('hidden');
                            }
                            setTimeout(window.location.reload(), 3000);
                        }
                        loading.style.display = "none";
                    })
                    .fail(function (error) {
                        if ($("#show_edit_form_for_delivery").is(':visible')) {
                            $("#show_edit_form_for_delivery").hide();
                            $("#hide_address_data_delivery").removeAttr('hidden');
                        }
                        if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                            $("#show_edit_form_other_addresses_" + count).hide();
                            $("#hide_address_data_" + count).removeAttr('hidden');
                        }
                        setTimeout(window.location.reload(), 3000);

                        loading.style.display = "none";
                    })

            }

            if (address_edit_type === 'delivery_multiple') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/delivery',
                    method: "POST",
                    cache: false,
                    data: $("#form_pe_delivery_" + count).serialize(),
                })
                    .done(function (response) {
                        if (response) {
                            if ($("#show_edit_form_for_delivery").is(':visible')) {
                                $("#show_edit_form_for_delivery").hide();
                                $("#hide_address_data_delivery").removeAttr('hidden');
                            }
                            if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                                $("#show_edit_form_other_addresses_" + count).hide();
                                $("#hide_address_data_" + count).removeAttr('hidden');
                            }
                            window.location.reload();
                        }
                        loading.style.display = "none";
                    })
                    .fail(function (error) {
                        if ($("#show_edit_form_for_delivery").is(':visible')) {
                            $("#show_edit_form_for_delivery").hide();
                            $("#hide_address_data_delivery").removeAttr('hidden');
                        }
                        if ($("#show_edit_form_other_addresses_" + count).length > 0 && $("#show_edit_form_other_addresses_" + count).is(':visible')) {
                            $("#show_edit_form_other_addresses_" + count).hide();
                            $("#hide_address_data_" + count).removeAttr('hidden');
                        }
                        window.location.reload();

                        loading.style.display = "none";
                    })

            }

            if (address_edit_type === 'billing') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/delivery',
                    method: "POST",
                    cache: false,
                    data: $("#form_pe_billing").serialize(),
                })
                    .done(function (response) {
                        if ($("#show_edit_form_for_billing").is(':visible')) {
                            $("#show_edit_form_for_billing").hide();
                            $("#show_edit_form_for_billing").removeAttr('hidden');
                        }
                        setTimeout(window.location.reload(), 3000);
                        loading.style.display = "none";
                    })
                    .fail(function (error) {
                        if ($("#show_edit_form_for_billing").is(':visible')) {
                            $("#show_edit_form_for_billing").hide();
                            $("#show_edit_form_for_billing").removeAttr('hidden');
                        }
                        loading.style.display = "none";
                    })

            }
        }
    }
}

function popup_address_Validation() {
    let different_billing = $("#popup_different_billing").is(':checked');
    if (different_billing) {
        let _delivery = popup_address_delivery();
        let _billing = popup_address_billing();
        // console.log(_delivery, _billing);
        if (_delivery === true && _billing === true) {
            // console.log(_delivery, _billing);
            let delivery_data = {
                "update_address_type": document.getElementById("update_address_type_popup_delivery").value,
                "adding_method": document.getElementById("adding_method_popup_delivery").value,
                "pd_e_ssn_popup": document.getElementById("pd_e_ssn_popup") ? document.getElementById("pd_e_ssn_popup").value : null,
                "pd_e_address_popup": document.getElementById("pd_e_address_popup").value,
                "pd_e_city_popup": document.getElementById("pd_e_city_popup").value,
                "pd_e_portalCode_popup": document.getElementById("pd_e_portalCode_popup").value,
                "pd_e_state_popup": document.getElementById("pd_e_state_popup").value,
            };

            let billing_data = {
                "update_address_type": document.getElementById("update_address_type_popup_billing").value,
                "adding_method": document.getElementById("adding_method_popup_billing").value,
                "pb_e_address_popup": document.getElementById("pb_e_address_popup").value,
                "pb_e_city_popup": document.getElementById("pb_e_city_popup").value,
                "pb_e_portalCode_popup": document.getElementById("pb_e_portalCode_popup").value,
                "pb_e_state_popup": document.getElementById("pb_e_state_popup").value,
            };

            let form_data = [delivery_data, billing_data];
            return result = [true, "delivery and shipping", form_data];
        } else {
            // console.log(_delivery, _billing, null);
            return result = [false, null, null];
        }
    } else {
        let _delivery = popup_address_delivery();
        // console.log(_delivery);
        if (_delivery === true) {

            let delivery_data = {
                "update_address_type": document.getElementById("update_address_type_popup_delivery").value,
                "adding_method": document.getElementById("adding_method_popup_delivery").value,
                "pd_e_ssn_popup": document.getElementById("pd_e_ssn_popup") ? document.getElementById("pd_e_ssn_popup").value : null,
                "pd_e_address_popup": document.getElementById("pd_e_address_popup").value,
                "pd_e_city_popup": document.getElementById("pd_e_city_popup").value,
                "pd_e_portalCode_popup": document.getElementById("pd_e_portalCode_popup").value,
                "pd_e_state_popup": document.getElementById("pd_e_state_popup").value,
            };

            let billing_data = null;
            let form_data = [delivery_data, billing_data];
            // console.log(_delivery);
            return result = [true, "delivery only", form_data];
        } else {
            // console.log(_delivery);
            return result = [false, null, null];
        }
    }
}

function popup_address_delivery() {
    v_p_delivery_popup("form_pe_delivery_popup");
    if ($('#form_pe_delivery_popup').valid() === true) {
        // console.log("pass validation");
        _user_profile_delivery_valid_popup = true;
        return true;
    } else {
        // console.log("fail validation");
        _user_profile_delivery_valid_popup = false;
        return false;
    }
}

function popup_address_billing() {
    v_p_billing_popup("form_pe_billing_popup");
    if ($('#form_pe_billing_popup').valid() === true) {
        // console.log("pass validation");
        _user_profile_billing_valid_popup = true;
        return true;
    } else {
        // console.log("fail validation");
        _user_profile_billing_valid_popup = false;
        return false;
    }
}

function popup_check_if_different_billing_checked(type) {
    switch (type) {
        case "delivery":
            if ($("#popup_different_billing").is(':checked')) {
                // console.log('checked');
                document.getElementById('popup_a_b').style.setProperty("display", "block", "important")
            } else if ($("#popup_different_billing").not(':checked')) {
                // console.log('not checked');
                document.getElementById('popup_a_b').style.setProperty("display", "none", "important")
            }
            break;
        default:
        // do nothing
    }
}

function p_address_actions_popup(type, address_edit_type, form_data = null) {
    if (type == 'save') {
        loading.style.display = "block";
        if (address_edit_type === 'delivery') {
            $("#form_pe_delivery_popup").trackChanges();
            if ($("#form_pe_delivery_popup").isChanged()) {
                // console.log("check 1");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/delivery',
                    method: "POST",
                    cache: false,
                    data: $("#form_pe_delivery_popup").serialize(),
                })
                    .done(function (response) {
                        loading.style.display = "none";
                        setTimeout(window.location.reload(), 2000);
                    })
                    .fail(function (error) {
                        loading.style.display = "none";
                    })
            } else {
                loading.style.display = "none";
            }
        }
        if (address_edit_type === 'billing') {
            $("#form_pe_billing_popup").trackChanges();
            if ($("#form_pe_billing_popup").isChanged()) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/delivery',
                    method: "POST",
                    cache: false,
                    data: $("#form_pe_billing_popup").serialize(),
                })
                    .done(function (response) {
                        loading.style.display = "none";
                        setTimeout(window.location.reload(), 2000);
                    })
                    .fail(function (error) {
                        loading.style.display = "none";
                    })
            } else {
                loading.style.display = "none";
            }
        }
        if (address_edit_type === 'delivery only') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/user/updates/delivery',
                method: "POST",
                cache: false,
                data: { form_data: form_data },
            })
                .done(function (response) {
                    loading.style.display = "none";
                    setTimeout(window.location.reload(), 2000);
                })
                .fail(function (error) {
                    loading.style.display = "none";
                })
        }
        if (address_edit_type === 'delivery and shipping') {
            // console.log("delivery_data");
            let delivery_data = {
                "update_address_type": document.getElementById("update_address_type_popup_delivery").value,
                "adding_method": document.getElementById("adding_method_popup_delivery").value,
                "pd_e_ssn_popup": document.getElementById("pd_e_ssn_popup").value,
                "pd_e_address_popup": document.getElementById("pd_e_address_popup").value,
                "pd_e_city_popup": document.getElementById("pd_e_city_popup").value,
                "pd_e_portalCode_popup": document.getElementById("pd_e_portalCode_popup").value,
                "pd_e_state_popup": document.getElementById("pd_e_state_popup").value,
            };

            let billing_data = {
                "update_address_type": document.getElementById("update_address_type_popup_billing").value,
                "adding_method": document.getElementById("adding_method_popup_billing").value,
                "pb_e_address_popup": document.getElementById("pb_e_address_popup").value,
                "pb_e_city_popup": document.getElementById("pb_e_city_popup").value,
                "pb_e_portalCode_popup": document.getElementById("pb_e_portalCode_popup").value,
                "pb_e_state_popup": document.getElementById("pb_e_state_popup").value,
            };

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/user/updates/delivery',
                method: "POST",
                cache: false,
                data: { form_data: form_data },
            })
                .done(function (response) {
                    loading.style.display = "none";
                    setTimeout(window.location.reload(), 2000);
                })
                .fail(function (error) {
                    loading.style.display = "none";
                })
        }
    }
}

function p_card_actions(type, card_id = null) {
    if (type == 'change_default_card') {
        loading.style.display = "block";
        card_id ? card_id : null;
        // console.log(card_id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: GLOBAL_URL + '/user/updates/cards/default',
            method: "POST",
            cache: false,
            data: { card_id: card_id },
        })
            .done(function (response) {
                loading.style.display = "none";
                // console.log(response);
                defaultcardupdated(response);
            })
            .fail(function (error) {
            })
        loading.style.display = "block";
    } else if (type == 'show_add_form') {
        $('#pc_a_show_form').show(1000);
        $('html,body').animate({ scrollTop: $("#pc_a_show_form").offset().top }, 'slow');
        // cancel_add_new_card.removeAttribute("hidden");
        add_new_card.setAttribute("hidden", null);
    } else if (type == 'cancel_add_card_form') {
        $('#pc_a_show_form').hide(1000);
        add_new_card.removeAttribute("hidden");
        // cancel_add_new_card.setAttribute("hidden", null);
    } else if (type == 'save') {
        // console.log(type);
        loading.style.display = "block";
        if ($("#form_pe_card").serialize() !== "") {
            let card_number = $("#pc_e_cardnumber").val().trim().replace(/\s/g, '');
            // console.log($("#pc_e_expiry").val());
            // console.log($("#pc_e_expiry").val().trim());
            // console.log($("#pc_e_expiry").val().trim().split("/")[0]);
            // console.log($("#pc_e_expiry").val().trim().split("/")[0].slice(-2));
            let exp_month = 0;
            if (parseInt($("#pc_e_expiry").val().trim().split("/")[0]) < 10) {
                exp_month = ('0' + $("#pc_e_expiry").val().trim().split("/")[0].slice(-2));
            }
            else {
                exp_month = $("#pc_e_expiry").val().trim().split("/")[0];
            }
            // console.log(exp_month);
            let exp_year = ('20' + $("#pc_e_expiry").val().trim().split("/")[1].slice(-2));
            let cvv = $("#pc_e_cvv").val().trim();


            if (isAddCardActionTriggered === false) {
                $("#add_new_card").attr("disabled", 'disabled');
                $("#pc_a_save").attr("disabled", 'disabled');
                isAddCardActionTriggered = true;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + '/user/updates/cards/add-api',
                    method: "POST",
                    cache: false,
                    data: { card_number: card_number, exp_month: exp_month, exp_year: exp_year, cvv: cvv },
                })
                    .done(function (response) {

                        if (response) {
                            let card_id = response.card_from_db.id;
                            generatePaymentIntents('new', card_id);
                        } else {
                            Swal.fire(
                                '',
                                trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                'error'
                            ).then((result) => {

                                $("#add_new_card").attr("disabled", null);
                                $("#pc_a_save").attr("disabled", null);
                                isAddCardActionTriggered = false;
                                window.location.reload();
                            })
                        }


                        loading.style.display = "none";
                        // setTimeout(window.location.reload(), 2000);

                    })
                    .fail(function (error) {
                        console.log(error);
                        add_new_card.removeAttribute("hidden");
                        // cancel_add_new_card.setAttribute("hidden", null);
                        let err_response = '';
                        if (error.responseJSON.message == 'Server Error') {
                            err_response = trans('website_contents.global.invalid_card');
                        } else {
                            err_response = error.responseJSON.message;
                        }
                        $("#pc_e_card_error").html('<label id="pc_e_card-error" class="error" for="pc_e_card" style="">' + err_response + '<label>');
                        loading.style.display = "none";
                        $("#pc_a_save").attr("disabled", null);
                        $("#add_new_card").attr("disabled", null);
                        isAddCardActionTriggered = false;
                    })

            } else {
                // do nothing
            }


        } else {
            // console.log(error);
            add_new_card.removeAttribute("hidden");
            // cancel_add_new_card.setAttribute("hidden", null);
            loading.style.display = "none";
        }

    }
}

function generatePaymentIntents(type, card_id) {
    $("#loading").css("display", "block");
    let OTP_URL = window.location.origin + GLOBAL_URL + "/stripe/confirm-api";
    let RETURN_URL_WITH_OTP = window.location.origin + GLOBAL_URL + `/stripe/payment-intent/otp-return?user_id=${user_id}`;
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + "/stripe/payment-intent/otp-charge-api",
        data: { card_id: card_id, type: type },
        type: 'POST',
        cache: false,
    })
        .done(function (response) {
            console.log(response);
            if (response != null && response.status == 'requires_confirmation') {
                console.log('initiate payment confirm');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: OTP_URL,
                    data: {
                        payment_intent_id: response.id,
                        return_url: RETURN_URL_WITH_OTP,
                    },
                    type: 'POST',
                    cache: false,
                })
                    .done(function (data) {
                        console.log(data);
                        if (data.next_action != null) {
                            if (data.status == "requires_source_action" || data.status == "requires_action") {
                                // If got OTP
                                // window.location = data.next_action.redirect_to_url.url;
                                var iframe = document.createElement('iframe');
                                var container = document.getElementById('stripe_otp_iframe');
                                iframe.src = data.next_action.redirect_to_url.url;
                                iframe.width = 600;
                                iframe.height = 400;
                                container.appendChild(iframe);
                                $("#loading").css("display", "none");
                                $("#stripe_otp_container").modal('show');
                            } else {
                                // If no OTP
                                if (data.status == "succeeded") {
                                    $.ajax({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: RETURN_URL_WITH_OTP,
                                        data: {
                                            payment_intent: data.id,
                                            non_otp: true,
                                            user_id: user_id
                                        },
                                        type: 'GET',
                                        cache: false,
                                    })
                                        .done(function (data2) {
                                            if (data2) {
                                                let non_otp_response = JSON.parse(data2);
                                                $("#loading").css("display", "none");
                                                if (non_otp_response.status == 200) {
                                                    // OTP One Time Charge Succeeded & Refunded
                                                    add_new_card.removeAttribute("hidden");
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_added', {}),
                                                        'success'
                                                    ).then((result) => {
                                                        window.location.reload();
                                                    });
                                                } else if (non_otp_response.status == 400) {
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                        'error'
                                                    ).then((result) => {
                                                        window.location.reload();
                                                    });
                                                }
                                            }
                                        })
                                        .fail(function (error) {
                                            // console.log(error);
                                        })
                                }
                            }
                        } else {
                            // If no OTP
                            if (data.status == "succeeded") {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: RETURN_URL_WITH_OTP,
                                    data: {
                                        payment_intent: data.id,
                                        non_otp: true,
                                        user_id: user_id
                                    },
                                    type: 'GET',
                                    cache: false,
                                })
                                    .done(function (data2) {
                                        if (data2) {
                                            let non_otp_response = JSON.parse(data2);
                                            $("#loading").css("display", "none");
                                            if (non_otp_response.status == 200) {
                                                // OTP One Time Charge Succeeded & Refunded
                                                add_new_card.removeAttribute("hidden");
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_added', {}),
                                                    'success'
                                                ).then((result) => {
                                                    window.location.reload();
                                                });
                                            } else if (non_otp_response.status == 400) {
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                    'error'
                                                ).then((result) => {
                                                    window.location.reload();
                                                });
                                            }
                                        }
                                    })
                                    .fail(function (error) {
                                        // console.log(error);
                                    })
                            }
                        }
                    })
                    .fail(function (error) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: RETURN_URL_WITH_OTP,
                            data: {
                                payment_intent: response.id,
                                non_otp: true
                            },
                            type: 'GET',
                            cache: false,
                        })
                            .done(function (data2) {
                                if (data2) {
                                    let non_otp_response = JSON.parse(data2);
                                    $("#loading").css("display", "none");
                                    if (non_otp_response.status == 200) {
                                        // OTP One Time Charge Succeeded & Refunded
                                        add_new_card.removeAttribute("hidden");
                                        Swal.fire(
                                            '',
                                            trans('website_contents.global.card_action_messages.card_added', {}),
                                            'success'
                                        ).then((result) => {
                                            window.location.reload();
                                        });
                                    } else if (non_otp_response.status == 400) {
                                        Swal.fire(
                                            '',
                                            trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                            'error'
                                        ).then((result) => {
                                            window.location.reload();
                                        });
                                    }
                                }
                            })
                            .fail(function (error) {
                                console.log(error);
                            })
                    })
            }
        })
        .fail(function (error) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: RETURN_URL_WITH_OTP,
                data: {
                    payment_intent: response.id,
                    non_otp: true
                },
                type: 'GET',
                cache: false,
            })
                .done(function (data2) {
                    if (data2) {
                        let non_otp_response = JSON.parse(data2);
                        $("#pc_a_save").attr("disabled", null);
                        $("#add_new_card").attr("disabled", null);
                        isAddCardActionTriggered = false;
                        $("#loading").css("display", "none");
                        if (non_otp_response.status == 200) {
                            // OTP One Time Charge Succeeded & Refunded
                            add_new_card.removeAttribute("hidden");
                            Swal.fire(
                                '',
                                trans('website_contents.global.card_action_messages.card_added', {}),
                                'success'
                            ).then((result) => {
                                window.location.reload();
                            });
                        } else if (non_otp_response.status == 400) {
                            Swal.fire(
                                '',
                                trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                'error'
                            ).then((result) => {
                                window.location.reload();
                            });
                        }
                    }
                })
                .fail(function (error) {
                    $("#pc_a_save").attr("disabled", null);
                    $("#add_new_card").attr("disabled", null);
                    isAddCardActionTriggered = false;
                    console.log(error);
                })
        })
}


window.stripeOneTimeChargeReturn = function (data) {
    $('#stripe_otp_container').modal('hide');
    console.log(JSON.stringify(data));
    $("#pc_a_save").attr("disabled", null);
    $("#add_new_card").attr("disabled", null);
    isAddCardActionTriggered = false;
    if (data.status == 200) {
        // OTP One Time Charge Succeeded & Refunded
        add_new_card.removeAttribute("hidden");
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_added', {}),
            'success'
        ).then((result) => {
            window.location.reload();
        });
    } else if (data.status == 400) {
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            window.location.reload();
        });
    } else {
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            window.location.reload();
        });
    }
};

function showOtherAddresses(type) {
    if (type == 'show') {
        btn_show_more_address.setAttribute('hidden', null);
        btn_cancel_show_more_address.removeAttribute('hidden');
        $('#show_more_addresses').show(1000);
    } else if (type == 'close') {
        btn_show_more_address.removeAttribute('hidden');
        btn_cancel_show_more_address.setAttribute('hidden', null);
        $('#show_more_addresses').hide(1000);
    }
}

function deleteCard(id) {
    // console.log(id);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/user/updates/cards/delete',
        method: "POST",
        cache: false,
        data: { card_id: id },
    })
        .done(function (response) {
            // console.log(response);
            if (response === "success") {
                carddeleted_popup();
            }
            loading.style.display = "none";

            // setTimeout(window.location.reload, 3000);
        })
        .fail(function (error) {
        })
}

function setDefaultDelivery(delivery_id, type) {
    loading.style.display = "block";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/user/updates/delivery/default',
        method: "POST",
        cache: false,
        data: { delivery_id: delivery_id, type: type },
    }).done(function (response) {
        // console.log(response);
        if (response) {
            Swal.fire(
                '',
                trans('website_contents.global.address_action_messages.set_as_default_shipment', {}),
                'success'
            ).then((result) => {
                window.location.reload();
            })
        }
        loading.style.display = "none";

        // setTimeout(window.location.reload, 3000);
    })
        .fail(function (error) {
        })
}

function setDefaultBilling(delivery_id, type) {
    loading.style.display = "block";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/user/updates/delivery/default',
        method: "POST",
        cache: false,
        data: { delivery_id: delivery_id, type: type },
    }).done(function (response) {
        // console.log(response);
        if (response) {
            Swal.fire(
                '',
                trans('website_contents.global.address_action_messages.set_as_default_billing', {}),
                'success'
            ).then((result) => {
                window.location.reload();
            })
        }
        loading.style.display = "none";

        // setTimeout(window.location.reload, 3000);
    })
        .fail(function (error) {
        })
}

/////////////////////////////// ON LOAD /////////////////////////////////////////////////////////
$(document).ready(function () {
    $("#show_hide_password a").on('click', function (event) {
        event.preventDefault();
        if ($('#show_hide_password input').attr("type") == "text") {
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password input').attr('placeholder', '●●●●●●●●●●');
            $('#show_hide_password i').addClass("fa-eye-slash");
            $('#show_hide_password i').removeClass("fa-eye");
        } else if ($('#show_hide_password input').attr("type") == "password") {
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password input').attr('placeholder', '●●●●●●●●●●');
            $('#show_hide_password i').removeClass("fa-eye-slash");
            $('#show_hide_password i').addClass("fa-eye");
        }
    });
});



