$(document).ready(function(){
    $('#navbarSupportedContent_authenticated').on('shown.bs.collapse', function() {
        $('.current-page').addClass("d-none");
        $('#navbarSupportedContent_authenticated').css({"margin-top": "-43px"});
    });

    $('#navbarSupportedContent_authenticated').on('hidden.bs.collapse', function() {
        $('.current-page').removeClass("d-none");
        $('#navbarSupportedContent_authenticated').css({"margin-top": "0"});
    });
});