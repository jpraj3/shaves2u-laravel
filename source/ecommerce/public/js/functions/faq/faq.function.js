$(document).ready(function() {
    $(".faq-tabs .nav-link").click(function(){
        $("#faq-dropdown").html($(this).text() + ' <i class="fa fa-angle-down" style="font-size: 1.5rem; position: absolute; right: 30px;"></i>');
    });

    $('.nav').on('shown.bs.tab', 'a', function (e) {
        $(".nav-link").removeClass('active');
        $(".nav-link[aria-controls=" + $(this).attr('aria-controls') + "]").addClass('active');
    });

    
    var hash = window.location.hash;
    if (hash != "") {
        $('a[href="' + hash + '"]').tab('show');
        $("#faq-dropdown").html($('a[href="' + hash + '"]:first').text() + ' <i class="fa fa-angle-down" style="font-size: 1.5rem; position: absolute; right: 30px;"></i>');
    }
    else {
        $('a:first').tab('show');
    }
    if(currentCountryIso == "my"){
        let faq_amex_logo = document.getElementById("faq-payment-amex");
        if(faq_amex_logo){
            faq_amex_logo.style.display = "none";
        }
    }
});

$.extend($.expr[':'], {
    'containsi': function(elem, i, match, array) {
      return (elem.textContent || elem.innerText || '').toLowerCase()
          .indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});

$(function() {
    jQuery('.faq-url').each(function () {
        let newpath=String(this.href);
        let wo = window.location.origin;
        let wh = window.location.hostname;
        if(newpath.includes(String(wo))){
            newpath = newpath.replace(wo,'');
            newpath = GLOBAL_URL + newpath;
        }
        else if(newpath.includes(String(wh))){
            newpath = newpath.replace(wh,'');
            newpath = GLOBAL_URL + newpath;
        }
        else{
            newpath = GLOBAL_URL + newpath;
        }
        jQuery(this).attr('href',  newpath);
    });

    jQuery('.faq-img').each(function () {
        let newpath=String(this.src);
        let wo = window.location.origin;
        let wh = window.location.hostname;
        if(newpath.includes(String(wo))){
            newpath = newpath.replace(wo,'');
            newpath = GLOBAL_URL_V2 + newpath;
        }
        else if(newpath.includes(String(wh))){
            newpath = newpath.replace(wh,'');
            newpath = GLOBAL_URL_V2 + newpath;
        }
        else{
            newpath = GLOBAL_URL_V2 + newpath;
        }
        jQuery(this).attr('src',  newpath);
    });

});


function searchfaq(value){
    if(value)
    {
        $('.faq-content').hide();
        $('.faq-content-search').show();
        var cards = $('.faq-content .card:has( h5:containsi("' + value +'") )');
        searchresults = cards.clone();
        $('#tab-content-search').empty();
        $('#tab-content-search').append(searchresults);
    }
    else
    {
        $('.faq-content').show();
        $('.faq-content-search').hide();
    }
}