$(document).ready(function() {
    let chevronUp =
        '<span class="panel-arrow-indicator-up pull-right" style="margin: 1%;color:grey !important;"><i class="fa fa-chevron-right"></i></span>';
    let chevronDown =
        '<span class="panel-arrow-indicator-down pull-right" style="margin: 1%;color:grey !important;"><i class="fa fa-chevron-down"></i></span>';
    $(".panel-default").prepend(chevronUp);
    $(".panel-arrow-indicator-up:last").remove();
    $("#privacy-policy-load-more-btn").click(function() {
        $("li.d-none")
            .first()
            .removeClass("d-none");
        if ($("li.d-none").length == 0) {
            $("#privacy-policy-load-more-btn").addClass("d-none");
        }
    });

    $(document).on("click", ".panel-default", function(event) {
        let exists = parseInt($(this).find("i.fa-chevron-right").length);
        if (exists === 1) {
            // console.log("show panel");
            $(this)
                .find("span.panel-arrow-indicator-up")
                .remove();
            $(this).prepend(chevronDown);
        } else {
            // console.log("hide panel");
            $(this)
                .find("span.panel-arrow-indicator-down")
                .remove();
            $(this).prepend(chevronUp);
        }
    });
});
