
let current_step = 1;

//Set selected variables
let selected_handle;
let selected_blade;
let selected_frequency;
let selected_addon_list = [];
let selected_addon1;
let selected_addon2;
let selected_plan_id;
let current_shipping_image = "";
let next_billing_image = "";
let hasShaveCreamNextBilling = false;
let current_p;
let next_p;
let shipping;

// On page load
$(function () {

    // Initialize the url has to #step1
    history.replaceState(undefined, undefined, "#step1")

    handle_products.forEach(handle => {
        if (plan_type_based_on_handle && plan_type_based_on_handle.includes(handle.productcountriesid)) {
            selected_handle = handle;
        } else {
            selected_handle = handle_products[0];
        }
    });

    // Select default handle
    // if (handle_products.length > 1) {
    //     selected_handle = handle_products[0];
    // } else {
    //     selected_handle = handle_products[0];
    // }

    // Always select free shave cream
    selected_addon_list.push(addon_products[0]);

    //On select panel
    // for (let i = 1; i <= 5; i++) {
    //     $("#step" + i + "-heading.mobile_heading" + i).click(SelectPanel(i));
    //     $("#step" + i + "-heading.topnav-" + i + "-heading").click(SelectPanel(i));
    // }

    //On select handle
    for (let i = 0; i < handle_products.length; i++) {
        let productcountriesid = handle_products[i].productcountriesid;
        $("#handle-" + productcountriesid).click(SelectHandle(productcountriesid));

        if (i == 0) {
            $("#handle-" + productcountriesid).click();
        }
    }

    //On select blade
    for (let i = 0; i < blade_products.length; i++) {
        let productcountriesid = blade_products[i].productcountriesid;
        $(".blade-" + productcountriesid).click(SelectBlade(productcountriesid));

        if (i == 2) {
            $(".blade-" + productcountriesid).click();
        }
    }

    //On select refill
    for (let i = 0; i < 3; i++) {
        $(".refill-" + i).click(SelectRefill(i));

        if (i == 1) {
            $(".refill-" + i).click();
        }
    }

    //On select frequency
    for (let i = 0; i < frequency_list.length; i++) {
        let frequency_id = frequency_list[i].duration;
        $(".frequency-" + frequency_id).click(SelectFrequency(frequency_id));

        if (i == 0) {
            $(".frequency-" + frequency_id).click();
        }
    }

    //On select addon
    // for (let i = 0; i < addon_products.length; i++) {
    //     let productcountriesid = addon_products[i].productcountriesid;
    //     $("#addon-" + productcountriesid).click(SelectAddon(addon_products[i]));

    //     if (productcountriesid == free_shave_cream_id) {
    //         selected_addon_list.push(addon_products[i]);
    //         $("#addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
    //         $("#check_addon_always_deliver").change(function() {
    //             if(this.checked) {
    //                 hasShaveCreamNextBilling = true;
    //                 selected_addon1 = addon_products[i];
    //             }
    //             else {
    //                 hasShaveCreamNextBilling = false;
    //                 selected_addon1 = null;
    //             }
    //         });
    //     }

    //     if (i == 0) {
    //         $("#addon-" + productcountriesid).click(SelectAddon(addon_products[i]));
    //     }
    // }

    //On click next
    $(".button-next").click(function () {

        // If no user id, skip the ajax
        if (user_id === null) {
            if (current_step != 3) {
                current_step++;
                history.replaceState(undefined, undefined, `#step${current_step}`)
                UpdateStep(current_step);
            }

            if (current_step != 1) {
                $("#button-back").removeClass('hidden');
            }

            if (current_step == 3) {
                history.replaceState(undefined, undefined, `#step3`)
                GetSessionData()
            }
        }
        else {
            // Check if the user have already subscribed for trial plan before
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                url: GLOBAL_URL + '/shave-plans/trial-plan/check-trial-plan-exist/' + user_id,
                data: null,
                json: true,
                cache: false,
                success: function (data) {

                    if (data === "exist") {
                        $('#notification_trial_plan_exist').modal('show');
                    }
                    else {
                        if (current_step != 3) {
                            current_step++;
                            history.replaceState(undefined, undefined, `#step${current_step}`)
                            UpdateStep(current_step);
                        }

                        if (current_step != 1) {
                            $("#button-back").removeClass('hidden');
                        }

                        if (current_step == 3) {
                            history.replaceState(undefined, undefined, `#step3`)
                            GetSessionData()
                        }
                    }

                },
                error: function (jqXHR) {

                }
            });
        }

    });

    //On click back
    $("#button-back").click(function () {
        if (current_step != 1) {
            current_step--;
            history.replaceState(undefined, undefined, `#step${current_step}`)
            UpdateStep(current_step);
        }

        if (current_step == 1) {
            history.replaceState(undefined, undefined, `#step1`)
            $("#button-back").addClass('hidden');
        }
    });

    $("#check").click(function () {

    });

    $("#clear").click(function () {
    });

    $(".button-annual-pop-out").click(function () {
        $("#change-annual-modal").modal("show");
        // document.getElementById("button-proceed-checkout").click();
     });

    $(".button-annual-pop-out2").click(function () {
        $("#change-annual-modal").modal("show");
    //    document.getElementById("button-proceed-checkout2").click();
    });

    $("#button-proceed-checkout").click(function () {
        UpdateSessionData(true);
    });

    // if Blade Refill selected
    var hash = window.location.hash;
    if (hash == ".refill") {
        $(".button-next").click();
    }
})

function changeAnnualCheckbox() {

    var isAnnual = document.getElementById("change-annual-checkbox");

    if (isAnnual.checked == true){
        document.getElementById("change-annual-value").value = "1";
    } else {
        document.getElementById("change-annual-value").value = "0";
    }
  }

  function submitAnnualModal() {
    var isAnnual = document.getElementById("change-annual-value").value;
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/shave-plans/trial-plan/converttrialtoannual',
        data: {
            isAnnual: isAnnual,

        },
        type: 'POST',
        json: true,
        cache: false,
        success: function (data) {
            // document.getElementById("button-proceed-checkout").click();

        },
        error: function (jqXHR) {

        }
    });
        // document.getElementById("button-proceed-checkout2").click();
  }
  
// Function: Select handle
function SelectHandle(productcountriesid) {
    return function () {
        $("#handle-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        $("#handle-tick-" + productcountriesid).removeClass('d-none');
        handle_products.forEach(product => {
            if (product.productcountriesid != productcountriesid) {
                $("#handle-" + product.productcountriesid).removeClass('item-selected').addClass('item-unselected');
                $("#handle-tick-" + product.productcountriesid).addClass('d-none');
            } else {
                selected_handle = product;
            }
        });
    };
};
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
// Function: Select blade
function SelectBlade(productcountriesid) {
    return function () {
        $(".blade-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        $("#blade-tick-" + productcountriesid).removeClass('d-none');
        blade_products.forEach(product => {
            // console.log(productcountriesid,product);
            // console.log(product.productcountriesid,productcountriesid);
            if (product.productcountriesid != productcountriesid) {
                $(".blade-" + product.productcountriesid).removeClass('item-selected').addClass('item-unselected');
                $("#blade-tick-" + product.productcountriesid).addClass('d-none');
            } else {
                selected_blade = product;
                // console.log(selected_blade.producttranslatesname,addon_products[0].producttranslatesname,addon_products[1].producttranslatesname);
                // Update refill page data
                $(".refill-0-title").html(`${selected_blade.producttranslatesname}`);
                $(".refill-1-title").html(`${selected_blade.producttranslatesname}, 1 ${addon_products[0].producttranslatesname}`);
                $(".refill-2-title").html(`${selected_blade.producttranslatesname}, 1 ${addon_products[0].producttranslatesname} <br> 1 ${addon_products[1].producttranslatesname}`);

                if (country_id == 8) {
                    //Currency and price for kr
                    $rp1 = formatNumber(Number(selected_blade.sellPrice));
                    $rp2 = formatNumber(Number(selected_blade.sellPrice) + Number(addon_products[0].sellPrice));
                    $rp3 = formatNumber(Number(selected_blade.sellPrice) + Number(addon_products[0].sellPrice) + Number(addon_products[1].sellPrice));
                    $(".refill-0-price").html(`${$rp1}${currency}`);
                    $(".refill-1-price").html(`${$rp2}${currency} `);
                    $(".refill-2-price").html(`${$rp3}${currency}`);
                } else {
                    $(".refill-0-price").html(`${currency} ${(Number(selected_blade.sellPrice)).toFixed(2)}`);
                    $(".refill-1-price").html(`${currency} ${(Number(selected_blade.sellPrice) + Number(addon_products[0].sellPrice)).toFixed(2)}`);
                    $(".refill-2-price").html(`${currency} ${(Number(selected_blade.sellPrice) + Number(addon_products[0].sellPrice) + Number(addon_products[1].sellPrice)).toFixed(2)}`);
                }

                $(".refill-0-image").attr("src", GLOBAL_URL_V2 + "/images/common/checkoutAssets/checkout-package1-" + selected_blade.bladeCount + "blade.png");
                $(".refill-1-image").attr("src", GLOBAL_URL_V2 + "/images/common/checkoutAssets/checkout-package2-" + selected_blade.bladeCount + "blade.png");
                $(".refill-2-image").attr("src", GLOBAL_URL_V2 + "/images/common/checkoutAssets/checkout-package3-" + selected_blade.bladeCount + "blade.png");

                if (country_handles && country_handles == 'H1') {
                    $(".package-image").attr("src", GLOBAL_URL_V2 + "/images/common/premium/product/TrialKit-" + selected_blade.bladeCount + "blade.png");
                    current_shipping_image = "/images/common/premium/product/TrialKit-" + selected_blade.bladeCount + "blade.png";
                } else {
                    $(".package-image").attr("src", GLOBAL_URL_V2 + "/images/common/checkoutAssets/TrialKit-" + selected_blade.bladeCount + "blade.png");
                    current_shipping_image = "/images/common/checkoutAssets/TrialKit-" + selected_blade.bladeCount + "blade.png";
                }


                GetPlanIdAndPrice();
            }
        });
    };
};

// Function: Select refill
function SelectRefill(position) {
    return function () {
        $(".refill-" + position).removeClass('item-unselected').addClass('item-selected');
        $("#refill-tick-" + position).removeClass('d-none');
        var list_refill_position = [0, 1, 2];
        list_refill_position.forEach(num => {
            if (position != num) {
                $(".refill-" + num).removeClass('item-selected').addClass('item-unselected');
                $("#refill-tick-" + num).addClass('d-none');
            }
        });

        if (position == 0) {
            hasShaveCreamNextBilling = false;
            selected_addon_list = selected_addon_list.filter((word, index, arr) => {
                return index == 0;
            });;
            next_billing_image = "/images/common/checkoutAssets/checkout-package1-" + selected_blade.bladeCount + "blade-small.png";
        }
        else if (position == 1) {
            hasShaveCreamNextBilling = true;
            selected_addon_list = selected_addon_list.filter((word, index, arr) => {
                return index == 0;
            });;
            next_billing_image = "/images/common/checkoutAssets/checkout-package2-" + selected_blade.bladeCount + "blade-small.png";
        }
        else if (position == 2) {
            hasShaveCreamNextBilling = true;
            selected_addon_list.push(addon_products[1]);
            next_billing_image = "/images/common/checkoutAssets/checkout-package3-" + selected_blade.bladeCount + "blade-small.png";
        }

        GetPlanIdAndPrice();
    };
};

// Function: Select frequency
function SelectFrequency(frequency_id) {
    return function () {
        $(".frequency-" + frequency_id).removeClass('item-unselected').addClass('item-selected');
        if (country_id == 8) {
            $(".frequency-" + frequency_id + " .frequency-p").html(trans('website_contents.frequency.'+frequency_id+'.selectedDetailsText', { months: frequency_id }));
        }else{
        $(".frequency-" + frequency_id + " .frequency-p").html(trans('website_contents.frequency.2.selectedDetailsText', { months: frequency_id }));
        }
        $("#frequency-tick-" + frequency_id).removeClass('d-none');
        let imagepath = $(".frequency-" + frequency_id + " img").attr('src');
        var res = imagepath.split(".png");
        var res1 = res[0].replace("-over", "");
        $(".frequency-" + frequency_id + " img").attr('src', res1 + "-over.png");
        frequency_list.forEach(frequency => {
            var duration = frequency.duration;
            if (duration != frequency_id) {
                let imagepath = $(".frequency-" + duration + " img").attr('src');
                var res = imagepath.split(".png");
                var res1 = res[0].replace("-over", "");
                $(".frequency-" + duration + " img").attr('src', res1 + ".png");
                $(".frequency-" + duration).removeClass('item-selected').addClass('item-unselected');
                $(".frequency-" + duration + " .frequency-p").html(trans('website_contents.frequency.2.detailsText', { months: duration }));
                $("#frequency-tick-" + duration).addClass('d-none');
            } else {
                selected_frequency = duration;

            }
        });
        GetPlanIdAndPrice();
    };
};

// Function: Select addon
function SelectAddon(addon) {
    return function () {
        let productcountriesid = addon.productcountriesid;

        if (productcountriesid != free_shave_cream_id) {
            if ($("#addon-" + productcountriesid).hasClass('item-unselected')) {
                $("#addon-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
                $("#addon-tick-" + productcountriesid).removeClass('d-none');
                selected_addon_list.push(addon);
                selected_addon2 = addon;
            } else {
                $("#addon-" + productcountriesid).removeClass('item-selected').addClass('item-unselected');
                $("#addon-tick-" + productcountriesid).addClass('d-none');
                selected_addon_list = selected_addon_list.filter(function (value, index, arr) {
                    return value != addon;
                });
                selected_addon2 = null;
            }
        }
    };
};

//Function: Update step
function UpdateStep(current_step) {
    // Only show selected panel and hide others
    for (let i = 1; i <= 5; i++) {
        var z = current_step - 1;
        if (i == current_step) {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-unselected').addClass('panel-title-selected');
            $("#step" + i + '-heading.topnav-' + i + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
            $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
            $("#collapse" + i).addClass('show');
        } else {
            $("#step" + i + '-heading.mobile_heading' + i).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
            $("#step" + i + '-title.mobile_title' + i).removeClass('panel-title-selected').addClass('panel-title-unselected');
            $("#collapse" + i).removeClass('show');
        }
    }

    // Do something according to current_step id
    switch (current_step) {
        case 1:
            {
                $("#step1-title").removeClass('bord-thick');
                $("#step2-heading").removeClass('bord-thick');
                $("#step2-title").removeClass('bord-thick');
                $("#step3-heading").removeClass('bord-thick');
            }
            break;
        case 2:
            {
                $("#step2-title").removeClass('bord-thick');
                $("#step3-heading").removeClass('bord-thick');
            }
            break;
        case 3:
            {
                //do nothing now
            }
            break;
        case 4:
            {
                $(".button-next").removeClass('display-none');
                $("#form-checkout").addClass('display-none');
            }
            break;
        case 5:
            {
                $(".button-next").addClass('display-none');
                $("#form-checkout").removeClass('display-none');
                DisplaySelectedItems();
                GetPlanIdAndPrice();
            }
            break;
        default:
            break;
    }
    GetSessionData()
}

//Function: Display Selected Handle,Blade,Frequency and Addons
function DisplaySelectedItems() {
    $(".text_selected_handle").text(selected_handle['producttranslatesname']);
    $(".text_selected_blade").text(selected_blade['producttranslatesname']);
    $(".text_selected_frequency").text(selected_frequency + " months");
    if (selected_addon1 != null) {
        $(".selected-addon1").removeClass('d-none');
        $(".text_selected_addon1").text(selected_addon1['producttranslatesname']);
    }
    if (selected_addon2 != null) {
        $(".selected-addon2").removeClass('d-none');;
        $(".text_selected_addon2").text(selected_addon2['producttranslatesname']);
    }
}

//Function: Calculate summary of plan price
function GetPlanIdAndPrice() {
    let today_subtotal_price = 0.00;
    let next_month_subtotal_price = 0.00;
    let shipping_price = 0.00;
    let payment_today_price = 0.00;

    let ProductCountriesId = [];
    let ProductCountriesIdGet = "";
    let count1 = 0;
    let count2 = 0;
    let planCombineCountry = "";

    let totalprice = 0.00;
    let nextmonthprice = 0.00;
    let shippingfee = 0.00;
    let currentprice = 0.00;
    let planpriceget = 0.00;
    let ProductSellPrice;
    let ProductCountriesIdAddOn = [];
    let planskuget = "";
    let planidget = "";

    // Proceed only if all handle, blade and frequency is selected
    if (selected_handle && selected_blade && selected_frequency) {

        // Generate combinations of selected product countries id in ascending order
        ProductCountriesId.push(selected_handle["productcountriesid"]);
        ProductCountriesId.push(selected_blade["productcountriesid"]);
        if (ProductCountriesId.length > 1) {
            ProductCountriesId.sort(function (a, b) { return a - b; });
        }
        ProductCountriesId.forEach(function (productCountryId) {
            if (count1 == 0) {
                ProductCountriesIdGet = ProductCountriesIdGet + productCountryId + '';
            } else {
                ProductCountriesIdGet = ProductCountriesIdGet + "," + productCountryId + '';
            }
            count1++;
        });

        // Loop through all Trial Plans
        if (allplan) {
            allplan.forEach(function (plan) {
                planCombineCountry = "";

                // Filter selected plan duration (frequency)
                if (plan.planduration == selected_frequency) {

                    // Generate combinations of product countries id from selected plan
                    plan.productCountryId.forEach(function (productCountryId) {
                        if (count2 == 0) {
                            planCombineCountry = planCombineCountry + productCountryId;
                        } else {
                            planCombineCountry = planCombineCountry + "," + productCountryId;
                        }
                        count2++;
                    });

                    // If selected product combinations matches with the plan product combinations, get the plan id, sku and price
                    if (ProductCountriesIdGet == planCombineCountry) {
                        planidget = plan.planid;
                        selected_plan_id = planidget;
                        planpriceget = parseFloat(plan.trialPrice);
                        planskuget = plan.plansku;
                    }
                }
                count2 = 0;
            });
        }

        // Calculate price for plan summary
        ProductSellPrice = parseFloat(selected_blade["sellPrice"]);
        nextmonthprice = nextmonthprice + ProductSellPrice;

        // Add price for premium addons
        selected_addon_list.forEach(function (data) {
            ProductCountriesIdAddOn.push(data["productcountriesid"]);
            if (data["productcountriesid"] == free_shave_cream_id) {
                if (hasShaveCreamNextBilling) {
                    nextmonthprice = nextmonthprice + parseFloat(data["sellPrice"]);
                }
            } else {
                //planpriceget = planpriceget + parseFloat(data["sellPrice"]);
                nextmonthprice = nextmonthprice + parseFloat(data["sellPrice"]);
            }
        });

        // Show price in summary details
        $(".today-subtotal").text(planpriceget.toFixed(2) + '');
        $(".next-month-subtotal").text(nextmonthprice.toFixed(2) + '');
        $(".shipping-total").text(shippingfee.toFixed(2) + '');
        $(".today-payment-total").text(planpriceget.toFixed(2) + '');
        $(".next-month-payment-total").text(nextmonthprice.toFixed(2) + '');

        current_p = planpriceget.toFixed(2) + '';
        next_p = nextmonthprice.toFixed(2) + '';
        shipping = shippingfee.toFixed(2) + '';

        UpdateSessionData(false);
    }
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_GET, null).done(function (data) {
        if (data) {
            // console.log(data);
            //let session_data = JSON.parse(data);
            //let previous_step = session_data["selection"]["current_step"];
            //current_step = previous_step;
            //UpdateStep(previous_step);
        }
    });
}

// Function: Update session data
function UpdateSessionData(isProceedCheckout) {
    let session_data = {};

    // New session
    session_data["selection"] = {};
    session_data["selection"]["step1"] = { "selected_blade": selected_blade };
    session_data["selection"]["step2"] = { "selected_addon_list": selected_addon_list };
    session_data["selection"]["step3"] = { "selected_frequency": selected_frequency };
    session_data["selection"]["summary"] = { "planId": selected_plan_id, "current_price": current_p, "shipping_fee": shipping, "next_price": next_p };
    session_data["selection"]["has_shave_cream_next_billing"] = hasShaveCreamNextBilling;
    session_data["selection"]["current_step"] = current_step;
    session_data["selection"]["plan_type"] = "trial-plan";
    session_data["selection"]["journey_type"] = "selection";
    session_data["selection"]["next_billing_image"] = next_billing_image;
    session_data["selection"]["current_shipping_image"] = current_shipping_image;

    // Old session
    // session_data["selection"] = {};
    // session_data["selection"]["step1"] = { "selected_handle": selected_handle };
    // session_data["selection"]["step2"] = { "selected_blade": selected_blade };
    // session_data["selection"]["step3"] = { "selected_frequency": selected_frequency };
    // session_data["selection"]["step4"] = { "selected_addon_list": selected_addon_list };
    // session_data["selection"]["step5"] = { "planId": selected_plan_id,"current_price" :current_p,"shipping_fee" : shipping,"next_price" : next_p };
    // session_data["selection"]["current_step"] = current_step;
    // session_data["selection"]["plan_type"] = "trial-plan";
    // session_data["selection"]["journey_type"] = "selection";
    // session_data["selection"]["has_shave_cream_next_billing"] = hasShaveCreamNextBilling;

    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_SET, JSON.stringify(session_data)).done(function () {
        if (isProceedCheckout) {
            $("#loading").css("display", "block");

            $("#form-proceed-checkout").submit();
        }
    });
}

// Function: Clear session data
function ClearSessionData() {
    session(SESSION_SELECTION_TRIAL_PLAN, SESSION_CLEAR, null).done(function () {
        //console.log("Successfully cleared session.");
    });
}

// Function: Select panel
function SelectPanel(panel_id) {
    return function () {
        //Current step is ahead of the selected panel id
        if (panel_id <= current_step) {
            // Show or hide selected panel
            var z = current_step - 1;
            if (!$("#collapse" + panel_id).hasClass('show')) {
                $("#step1-heading.mobile_heading1").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step1-title.mobile_title1").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse1").removeClass('show');
                $("#step2-heading.mobile_heading2").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step2-title.mobile_title2").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse2").removeClass('show');
                $("#step3-heading.mobile_heading3").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step3-title.mobile_title3").removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse3").removeClass('show');
                // $("#step4-heading.mobile_heading4").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                // $("#step4-title.mobile_title4").removeClass('panel-title-selected').addClass('panel-title-unselected');
                // $("#collapse4").removeClass('show');
                // $("#step5-heading.mobile_heading5").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                // $("#step5-title.mobile_title5").removeClass('panel-title-selected').addClass('panel-title-unselected');
                // $("#collapse5").removeClass('show');
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-unselected').addClass('panel-heading-selected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-unselected').addClass('panel-title-selected');
                $("#step" + panel_id + '-heading.topnav-' + panel_id + "-heading").removeClass('bord-thick-white').addClass('bord-thick');
                $("#step" + z + '-title.topnav-' + z + "-arrow").removeClass('bord-thick-white').addClass('bord-thick');
                $("#collapse" + panel_id).addClass('show');
            } else {
                $("#step" + panel_id + '-heading.mobile_heading' + panel_id).removeClass('panel-heading-selected').addClass('panel-heading-unselected');
                $("#step" + panel_id + '-title.mobile_title' + panel_id).removeClass('panel-title-selected').addClass('panel-title-unselected');
                $("#collapse" + panel_id).removeClass('show');
            }
        }
    }
}
