
// check for referral params
function checkReferralParams() {
    if (window.location.pathname.includes('/shave-plans/trial-plan/selection') === true) {
        if (getUrlParam(window.location.search, 'ref', '') !== undefined && getUrlParam(window.location.search, 'src', '') === "link" || getUrlParam(window.location.search, 'ref', '') !== undefined && getUrlParam(window.location.search, 'src', '') === "email") {
            _referral_unique_code = getUrlParam(window.location.search, 'ref', '');
            _src = getUrlParam(window.location.search, 'src', '');

            if (_referral_unique_code) {
                $.ajax({
                    url: GLOBAL_URL + '/referral/get-referrer-info',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    method: "POST",
                    cache: false,
                    data: { "_referral_unique_code": _referral_unique_code, "_src": _src }
                })
                    .done(function (response) {
                        let referral_program_info = {
                            "code": _referral_unique_code,
                            "src": _src,
                            "referrer": response,
                            "referee": { "current_country": window.currentCountryData }
                        };

                        window.addStorage("referral_program_info", JSON.stringify(referral_program_info));
                    })
            }
        }
    }
}



// ======================================== ON LOAD =======================================================
$(function () {
    // startup functions to run
    checkReferralParams();

    $("#email-share-button").click(function () {
        $("#emailShare").modal("show");
        return false;
    });

    $('#send_referral_invitation_email').validate({
        rules: {
            referral_email_lists: {
                required: true
            }
        },
        messages: {
            referral_email_lists: {
                required: trans('validation.custom.validation.email.required', {})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "referral_email_lists") {
                error.appendTo('#email_error');
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#submit_referral_cash_withdrawals').validate({
        rules: {
            withdrawal_name: {
                required: true,
                maxlength: 50
            },
            bank_name: {
                required: true,
                maxlength: 50
            },
            bank_account_no: {
                required: true,
                maxlength: 30
            },
        },
        messages: {
            withdrawal_name: {
                required: trans('validation.custom.validation.referral.withdrawal_name.required', {})
            },
            bank_name: {
                required: trans('validation.custom.validation.referral.bank_name.required', {})
            },
            bank_account_no: {
                required: trans('validation.custom.validation.referral.bank_account_no.required', {})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "withdrawal_name") {
                error.appendTo('#withdrawal_name_error');
            } else if (element.attr("name") == "bank_name") {
                    error.appendTo('#bank_name_error');
            } else if (element.attr("name") == "bank_account_no") {
                        error.appendTo('#bank_account_no_error');
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#submit_referral_cash_withdrawals").on("submit", function (event) {
        if ($('#submit_referral_cash_withdrawals').valid() === true) {
            $("#loading").css("display", "block");
            event.preventDefault();
            let bank_name = $("#bank_name").val();
            let bank_account_no = $("#bank_account_no").val();
            let cashout_name = $("#withdrawal_name").val();
            $.ajax({
                url: GLOBAL_URL + '/user/referrals/withdraw-cash',
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                method: "POST",
                cache: false,
                data: { "bank_name": bank_name, "bank_account_no": bank_account_no, "name": cashout_name }
            })
            .done(function (response) {

                $("#loading").css("display", "none");
                $('#cashOut').modal('hide');
                if (response == 1) {
                    Swal.fire(
                        '',
                        '',
                        'success'
                    ).then((result) => {
                        window.location.reload();
                    })
                } else {
                    Swal.fire(
                        '',
                        '',
                        'error'
                    ).then((result) => {
                        window.location.reload();
                    })
                }
            })
            .fail(function (jqXHR, textStatus, error) {

                $("#loading").css("display", "none");
                $('#cashOut').modal('hide');
                Swal.fire(
                    '',
                    '',
                    'error'
                ).then((result) => {
                    window.location.reload();
                })
                console.log(error);
            });
        }
    });

    $("#send_referral_invitation_email").on("submit", function (event) {

        if ($('#send_referral_invitation_email').valid() === true) {
            $("#loading").css("display", "block");
            event.preventDefault();
            let referral_email_lists = $("#referral_email_lists").val();
            let invite_link = $("#referrals_unique_link").html();

            $.ajax({
                url: GLOBAL_URL + '/user/referrals/send-invitation-email',
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                method: "POST",
                cache: false,
                data: { "referral_email_lists": referral_email_lists, "invite_link": invite_link, "invite_code": getUrlParam(invite_link, 'ref', '') }
            })
                .done(function (response) {
                    document.getElementById("referral_email_lists").value = "";
                    // $('#emailShare').modal('hide');

                    $("#loading").css("display", "none");
                    document.getElementById("email-send-msg").innerHTML = trans('website_contents.authenticated.referrals.success_invite', {});

                    setTimeout(function () {
                        document.getElementById("email-send-msg").innerHTML = '';
                    }, 3000);

                    // console.log(response);
                })
                .fail(function (jqXHR, textStatus, error) {
                    document.getElementById("referral_email_lists").value = "";
                    // $('#emailShare').modal('hide');
                    $("#loading").css("display", "none");
                    // console.log(error);
                });
        }

    });
});
