var getParams = function(url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};
$("#proceed-update-subscription").click(function(event) {
    event.preventDefault();
    $getcountry = document.getElementById("countrypost").value;
    $getlang = document.getElementById("langpost").value;
    $getplanid = document.getElementById("planidpost").value;
    $getproductaddon = document.getElementById("productaddonpost").value;
    $getcampaignid = document.getElementById("campaignidpost").value;

    $utm_source = document.getElementById("utm_source").value;
    $utm_medium = document.getElementById("utm_medium").value;
    $utm_campaign = document.getElementById("utm_campaign").value;
    $utm_content = document.getElementById("utm_content").value;
    $utm_term = document.getElementById("utm_term").value;

    var loading = document.getElementById("loading");
    loading.style.display = "block";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: GLOBAL_URL_V2 + '/welcomebackpromo/updatesubscription',
        data: {
            countrypost: $getcountry,
            langpost: $getlang,
            planidpost: $getplanid,
            productaddonpost: $getproductaddon,
            campaignidpost: $getcampaignid,
            utm_source: $utm_source,
            utm_medium: $utm_medium,
            utm_campaign: $utm_campaign,
            utm_content: $utm_content,
            utm_term: $utm_term,
            urlparam: getParams(window.location.href)
        },
        json: true,
        cache: false,
        success: function(data) {
            loading.style.display = "none";
            if(data == "success"){ 
                document.getElementById("redirect-card-blade").innerHTML = document.getElementById("blade-selection-name").textContent;
                document.getElementById("redirect-card-quantity").innerHTML = document.getElementById("quantity-selection-qty-full").textContent;
                document.getElementById("redirect-card-addon").innerHTML = addon_name.join(",");
                document.getElementById("redirect-card-price").innerHTML = (total_price + addon_total_price).toFixed(2);
                $('#redirect-card-modal').modal('show');
            }
        },
        error: function(jqXHR) {
            // $("#error-card").text(jqXHR.responseJSON.message);
            loading.style.display = "none";
        }
    });
});

window.addEventListener('load', function() {
    var classList = document.getElementById('blade-selection').className.split(/\s+/);
    var bladename = document.getElementById("blade-selection-name").textContent;
    showQuantityOption(bladename);

    for (var i = 0; i < classList.length; i++) {
        if (classList[i].includes("blade-h-")) {
            var hidebladeid = classList[i].split("blade-h-");
            $("#blade-" + hidebladeid[1]).hide();
        }
    }
})

function selectBlade(bladeid, name, image, sku) {
    var bladeheaderclass = "";
    var hidebladeid = "";
    var classList = document.getElementById('blade-selection').className.split(/\s+/);
    for (var i = 0; i < classList.length; i++) {
        if (classList[i].includes("blade-h-")) {
            bladeheaderclass = classList[i];
            hidebladeid = classList[i].split("blade-h-");
            $("#blade-selection").removeClass("blade-h-" + hidebladeid[1]);
            $("#blade-selection").addClass("blade-h-" + bladeid);
        }
    }
    $("#blade-selection-name").html("<b>" + name + "</b>");
    $("#blade-selection-sku").html(sku);
    document.getElementById("blade-selection-img").src = image;
    document.getElementById("blade-selection-img").setAttribute('alt', name);
    $("#blade-" + hidebladeid[1]).show();
    $("#blade-" + bladeid).hide();
    $("#blade_collapse").collapse('hide');
    $("#quantity_collapse").collapse('hide');
    showQuantityOption(name);
}

function showQuantityOption(bladename) {
    var blade = bladename.match(/\d+/)[0];
    $("[id^='quantity-TK" + blade + "']").show();
    $(".quantity-option:not([id^='quantity-TK" + blade + "'])").hide();

    var newskuname = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-skuname").textContent;
    var newcashback = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-cashback").textContent;
    var newsku = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-sku").textContent;
    var newqty = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-qty").textContent;
    var newsellprice = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-sellPrice").textContent;
    var newplanid = document.getElementById("quantity-option-TK" + blade + "M" + order_first_duration + "-planid").textContent;

    $("#quantity-selection-skuname").html("<b>" + newskuname + "</b>");
    $("#quantity-selection-cashback").html("<b>" + newcashback + "</b>");
    $("#quantity-selection-sku").html(newsku);
    $("#quantity-selection-qty").html("<b>" + newqty + "</b>");
    $("#quantity-selection-sellPrice").html("<b>" + newsellprice + "</b>");
    $("#quantity-selection-planid").html(newplanid);

    total_price = parseFloat(newsellprice);
    var classList1 = document.getElementById('quantity-selection').className.split(/\s+/);
    for (var i = 0; i < classList1.length; i++) {
        if (classList1[i].includes("quantity-h-")) {
            bladeheaderclass = classList1[i];
            hidebladeid = classList1[i].split("quantity-h-");
            $("#quantity-selection").removeClass("quantity-h-" + hidebladeid[1]);
            $("#quantity-selection").addClass("quantity-h-TK" + blade + "M" + order_first_duration);
        }
    }


    var classList = document.getElementById('quantity-selection').className.split(/\s+/);
    for (var i = 0; i < classList.length; i++) {
        if (classList[i].includes("quantity-h-")) {
            var hidebladeid = classList[i].split("quantity-h-");
            $("#quantity-" + hidebladeid[1]).hide();
        }
    }
    document.getElementById("planidpost").value = newplanid;
    $("#campaign-totalprice").html(parseFloat(total_price) + parseFloat(addon_total_price));
}

function selectQuantity(planSku, cashback, catridgeQTY, sellPrice, blade, month, planid) {
    var quantityheaderclass = "";
    var hidequantityid = "";
    var classList = document.getElementById('quantity-selection').className.split(/\s+/);
    for (var i = 0; i < classList.length; i++) {
        if (classList[i].includes("quantity-h-")) {
            bladeheaderclass = classList[i];
            hidebladeid = classList[i].split("quantity-h-");
            $("#quantity-selection").removeClass("quantity-h-" + hidebladeid[1]);
            $("#quantity-selection").addClass("quantity-h-" + blade + "M" + month);
        }
    }
    $("#quantity-selection-skuname").html("<b>" + planSku + "</b>");
    $("#quantity-selection-cashback").html("<b>" + cashback + "</b>");
    $("#quantity-selection-sku").html(planSku);
    $("#quantity-selection-qty").html("<b>" + catridgeQTY + "</b>");
    $("#quantity-selection-sellPrice").html("<b>" + sellPrice + "</b>");
    total_price = parseFloat(sellPrice);
    $("#quantity-" + hidebladeid[1]).show();
    $("#quantity-" + blade + "M" + month).hide();
    document.getElementById("planidpost").value = planid;
    $("#blade_collapse").collapse('hide');
    $("#quantity_collapse").collapse('hide');
    $("#campaign-totalprice").html(parseFloat(total_price) + parseFloat(addon_total_price));
}

function selectAddOn(productCountryId, name, images, sellprice) {
    var addonClassName = 'addon-' + productCountryId;
    var classList = document.getElementById(addonClassName).className.split(/\s+/);
    let addoncycleimg = document.getElementById("img-addon-cycle-modal");
    if (addoncycleimg) {
        addoncycleimg.src = "";
        addoncycleimg.src = images;
    }
    let addoncyclename = document.getElementById("name-addon-cycle-modal");
    if (addoncyclename) {
        addoncyclename.innerHTML = "";
        addoncyclename.innerHTML = name;
    }
    let addonidhidden = document.getElementById("addonid-hidden");
    if (addonidhidden) {
        addonidhidden.value = "";
        addonidhidden.value = productCountryId;
    }

    let addonpricehidden = document.getElementById("addonprice-hidden");
    if (addonpricehidden) {
        addonpricehidden.value = "";
        addonpricehidden.value = sellprice;
    }

    for (var i = 0; i < classList.length; i++) {
        if (classList[i].includes("item-selected")) {
            //unselect the addon
            $("#addon-" + productCountryId).removeClass("item-selected");
            $("#addon-" + productCountryId).addClass("item-unselected");
            addon_total_price = parseFloat(addon_total_price) - parseFloat(sellprice);

            let productaddonposthidden = document.getElementById("productaddonpost");
            let productaddonposthiddenget = "";
            if (productaddonposthidden) {
                productaddonposthiddenget = productaddonposthidden.value;
                if (productaddonposthiddenget != "" & productaddonposthiddenget != null) {
                    let newAddonid = "";
                    let countnewAddonid = 0;
                    let productaddonposthiddensplit = productaddonposthiddenget.split(",");
                    productaddonposthiddensplit.forEach((data) => {
                        if (!data.includes(productCountryId)) {
                            if (countnewAddonid == 0) {
                                newAddonid = data;
                            } else {
                                newAddonid = newAddonid + "," + data;
                            }
                            countnewAddonid++;
                        }
                    })

                    document.getElementById("productaddonpost").value = newAddonid;
                }
            }
            addon_name = addon_name.filter(e => e !== name); 

            $("#campaign-totalprice").html(total_price + addon_total_price);
        } else if (classList[i].includes("item-unselected")) {
            $("#addon-" + productCountryId).removeClass("item-unselected");
            $("#addon-" + productCountryId).addClass("item-selected");
            $('#addon-cycle-modal').modal('show');
        }
    }
}

function addonCycleClose() {

    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }
    if ($(".addon-" + addonidhiddenget).hasClass('item-selected')) {
        $("#addon-" + addonidhiddenget).removeClass("item-selected");
        $("#addon-" + addonidhiddenget).addClass("item-unselected");
    }
    let addoncycleimg = document.getElementById("img-addon-cycle-modal");
    if (addoncycleimg) {
        addoncycleimg.src = "";
    }
    let addoncyclename = document.getElementById("name-addon-cycle-modal");
    if (addoncyclename) {
        addoncyclename.innerHTML = "";
    }

    if (addonidhidden) {
        addonidhidden.value = "";
    }

    let addonpricehidden = document.getElementById("addonprice-hidden");
    if (addonpricehidden) {
        addonpricehidden.value = "";
    }
}

function addonCycleEvery() {
    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }

    let addonpricehidden = document.getElementById("addonprice-hidden");
    let addonpricehiddenget = "";
    if (addonpricehidden) {
        addonpricehiddenget = addonpricehidden.value;
    }

    let addoncyclename = document.getElementById("name-addon-cycle-modal");
    let addoncyclenameget = "";
    if (addoncyclename) {
        addoncyclenameget = addoncyclename.textContent;
    }

    let productaddonposthidden = document.getElementById("productaddonpost");
    let productaddonposthiddenget = "";
    if (productaddonposthidden) {
        productaddonposthiddenget = productaddonposthidden.value;
        if (productaddonposthiddenget != "" & productaddonposthiddenget != null) {
            document.getElementById("productaddonpost").value = productaddonposthiddenget + "," + addonidhiddenget + "@all";
        } else {
            document.getElementById("productaddonpost").value = addonidhiddenget + "@all";
        }
    }

    if ($(".addon-" + addonidhiddenget).hasClass('item-selected')) {
        if(addoncyclenameget){
        addon_name.push(addoncyclenameget);
        }
        addon_total_price = parseFloat(addon_total_price) + parseFloat(addonpricehiddenget);
        $("#campaign-totalprice").html(total_price + addon_total_price);
    }
}

function addonCycleOne() {

    let addonidhidden = document.getElementById("addonid-hidden");
    let addonidhiddenget = "";
    if (addonidhidden) {
        addonidhiddenget = addonidhidden.value;
    }

    let addonpricehidden = document.getElementById("addonprice-hidden");
    let addonpricehiddenget = "";
    if (addonpricehidden) {
        addonpricehiddenget = addonpricehidden.value;
    }

    let addoncyclename = document.getElementById("name-addon-cycle-modal");
    let addoncyclenameget = "";
    if (addoncyclename) {
        addoncyclenameget = addoncyclename.textContent;
    }

    let productaddonposthidden = document.getElementById("productaddonpost");
    let productaddonposthiddenget = "";
    if (productaddonposthidden) {
        productaddonposthiddenget = productaddonposthidden.value;
        if (productaddonposthiddenget != "" & productaddonposthiddenget != null) {
            document.getElementById("productaddonpost").value = productaddonposthiddenget + "," + addonidhiddenget + "@1";
        } else {
            document.getElementById("productaddonpost").value = addonidhiddenget + "@1";
        }
    }

    if ($(".addon-" + addonidhiddenget).hasClass('item-selected')) {
        if(addoncyclenameget){
        addon_name.push(addoncyclenameget);
        }
        addon_total_price = parseFloat(addon_total_price) + parseFloat(addonpricehiddenget);
        $("#campaign-totalprice").html(total_price + addon_total_price);
    }
}

function redirectToEditCard(){
    loading.style.display = "block";

    let urlcountrylang = document.getElementById("urlpost");
    let urlcountrylangget = "";
    if (urlcountrylang) {
        urlcountrylangget = urlcountrylang.value;
    }
    addStorage("redirectcard", "true");
    window.location.href = GLOBAL_URL_V2 + "/" + urlcountrylangget + '/user/profile-info';
}

function redirectToThankyou(){
    loading.style.display = "block";

    window.location.href = GLOBAL_URL_V2 + '/welcomebackpromo/thankyou';
}