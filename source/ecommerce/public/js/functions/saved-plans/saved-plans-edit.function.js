let isSubscriptionCardModifyAttempted = false;
let loading = document.getElementById("loading");
// Frequency Form Elements
let spf_e_frequency = document.getElementById("spf_e_frequency");
// Promotion Form Elements
let applied_promo2 = document.getElementById("applied_promo");
let spp_e_promoCode = document.getElementById("spp_e_promoCode");
// Card Form Elements
let spc_e_cardNumber_masked = document.getElementById("spc_e_cardNumber_masked");
let spc_e_cardNumber = document.getElementById("spc_e_cardNumber");
let spc_e_expirydate = document.getElementById("spc_e_expirydate");
let spc_e_expireYear = document.getElementById("spc_e_expireYear");
let spc_e_ccv = document.getElementById("spc_e_ccv");
// Delivering / Billing Address Elements
let spd_e_ssn = document.getElementById('spd_e_ssn');
let spd_e_address = document.getElementById('spd_e_address');
let spd_e_city = document.getElementById('spd_e_city');
let spd_e_portalCode = document.getElementById('spd_e_portalCode');
let spd_e_state = document.getElementById('spd_e_state');
let spb_e_address = document.getElementById('spb_e_address');
let spb_e_city = document.getElementById('spb_e_city');
let spb_e_portalCode = document.getElementById('spb_e_portalCode');
let spb_e_state = document.getElementById('spb_e_state');
// Action Buttons
let sp_a_edit = document.getElementById('sp_a_edit');
let sp_a_save = document.getElementById('sp_a_save');
let spe_presaved_products_arr = '';
let addon_products_to_subscription = document.getElementById('addon_products_to_subscription');
let add_product_close = document.getElementById('add_product_close');
// ----------------------------------------------------------------------------------------------------------
// Jason ammendments
let _session_data = '';
let check_promotion = '';
let spe_existing_promo_name = $("#spe_existing_promo_name");
let spe_existing_promo_id = $("#spe_existing_promo_id");
let spe_applied_promo_name = $("#spe_applied_promo_name");
let spe_applied_promo_id = $("#spe_applied_promo_id");
let spe_applied_cancellation = $("#spe_applied_cancellation");
let spe_applied_temp_discount = $("#spe_applied_temp_discount");
let applied_promo = $("#applied_promo");
let spe_new_promo_details = $("#spe_new_promo_details");
let spe_selected_promo_id = $("#spe_selected_promo_id");
let pre_selectNewPromo_btn = $("#pre_selectNewPromo_btn");
let diff_billingaddress = document.getElementById('diff_billingaddress');
let blade_pack_type_has_been_changed = false;
let new_blade_type_plan_lists = '';
let products_to_be_removed = [];
let value_update = "";
let total_subprice = document.getElementById('total-subprice');
let modified_plan_on_change = '';
let original_plan_on_change = '';
addon_products_to_subscription.onclick = function () {
    let old_values_get = $("#spe_presaved_products").val();
    $("#ori_spe_presaved_products").val(old_values_get);
};


add_product_close.onclick = function () {
    let old_values_get = $("#spe_presaved_products").val();
    let values_get = $("#ori_spe_presaved_products").val();

    var valuesarr = old_values_get.split(',');
    var valuesarr1 = values_get.split(',');

    $(".addonselection").removeClass("item-selected").addClass('item-unselected');
    valuesarr1.forEach(function (original) {

        $("#product-" + original).removeClass('item-unselected').addClass('item-selected');


    });


    $("#spe_presaved_products").val(values_get);
};


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

// Function: Select product
function SelectHandle(productcountryid) {
    let values = '';
    let i;
    let old_values = $("#spe_presaved_products").val();

    // console.log("productcountryid => " + productcountryid);

    if ($("#addon_products_" + productcountryid).val() === "false") {
        $("#addon_products_" + productcountryid).val("true");
        if (old_values !== "") {
            old_values = old_values.split(",");
            old_values2 = Array.from(new Set(old_values))
            if (old_values2.length) {
                for (i = 0; i < old_values2.length; i++) {
                    if (old_values2[i] !== productcountryid) {
                        values += old_values2[i] + ",";
                    }
                }
            }
            values += productcountryid + ",";
            values = values.substr(0, values.length - 1);
            $("#spe_presaved_products").val(values);
        } else {
            $("#spe_presaved_products").val(productcountryid);
        }
    } else {
        var valuesarr = old_values.split(',');
        var count_value = 0;
        var new_value_update = '';
        valuesarr.forEach(function (original) {
            if (original != productcountryid) {
                if (count_value == 0) {
                    new_value_update = new_value_update + original;
                } else {
                    new_value_update = new_value_update + "," + original;
                }
            }
            count_value++;
        });
        values = new_value_update;
        old_values = new_value_update;
        // value_update = new_value_update;
        $("#spe_presaved_products").val(old_values);
        $("#addon_products_" + productcountryid).val("false");
    }

    if ($("#product-" + productcountryid).hasClass('item-unselected')) {
        $("#product-" + productcountryid).removeClass('item-unselected');
        $("#product-" + productcountryid).addClass('item-selected');
        $("#product-tick-" + productcountryid).removeClass('d-none');
    } else {
        $("#product-" + productcountryid).removeClass('item-selected');
        $("#product-" + productcountryid).addClass('item-unselected');
        $("#product-tick-" + productcountryid).addClass('d-none');
    }
};

// Function: Select months
function SelectMonths(monthid) {
    return function () {
        for (let i = 1; i < 4; i++) {
            $("#months-" + i).removeClass('item-selected').addClass('item-unselected');
            $("#months-tick-" + i).addClass('d-none');
        }
        if ($("#months-" + monthid).hasClass('item-unselected')) {
            $("#months-" + monthid).removeClass('item-unselected').addClass('item-selected');
            $("#months-tick-" + monthid).removeClass('d-none');
        }
        else {
            $("#months-" + monthid).removeClass('item-selected').addClass('item-unselected');
            $("#months-tick-" + monthid).addClass('d-none');
        }
    };
};

// Function: Select product
function SelectReason(reasonid) {
    return function () {
        for (let i = 1; i < 9; i++) {
            $("#cancel-reason-" + i).removeClass('item-selected').addClass('item-unselected');
            $("#cancel-reason-tick-" + i).addClass('d-none');
        }
        if ($("#cancel-reason-" + reasonid).hasClass('item-unselected')) {
            $("#cancel-reason-" + reasonid).removeClass('item-unselected').addClass('item-selected');
            $("#cancel-reason-tick-" + reasonid).removeClass('d-none');
        }
        else {
            $("#cancel-reason-" + reasonid).removeClass('item-selected').addClass('item-unselected');
            $("#cancel-reason-tick-" + reasonid).addClass('d-none');
        }
    };
};

function sp_user_actions(type) {
    if (type == 'edit') {
        loading.style.display = "block";
        spf_e_frequency.removeAttribute('disabled');
        addon_products_to_subscription.removeAttribute('disabled');
        applied_promo2.removeAttribute('disabled');
        diff_billingaddress.removeAttribute('disabled');
        spp_e_promoCode.removeAttribute('disabled');
        spc_e_cardNumber.removeAttribute('disabled');
        spc_e_expirydate.removeAttribute('disabled');
        spc_e_ccv.removeAttribute('disabled');
        spd_e_ssn.removeAttribute('disabled');
        spd_e_address.removeAttribute('disabled');
        spd_e_city.removeAttribute('disabled');
        spd_e_portalCode.removeAttribute('disabled');
        spd_e_state.removeAttribute('disabled');
        spb_e_address.removeAttribute('disabled');
        spb_e_city.removeAttribute('disabled');
        spb_e_portalCode.removeAttribute('disabled');
        spb_e_state.removeAttribute('disabled');
        sp_a_edit.setAttribute("hidden", null);
        sp_a_save.removeAttribute("hidden");
        $('.remove_selected_blade').removeAttr('disabled');
        $('#apply_promo_use_btn').removeAttr('disabled');
        loading.style.display = "none";
    } else if (type == 'save') {

        loading.style.display = "block";
        if ($("#spf_e_frequency").val() === 'select') {
            loading.style.display = "none";
            $("#spf_e_frequency_error").show();
        } else {
            loading.style.display = "block";
            // for all countries using stripe payments, and subscription payment card has been modified
            if (country_iso !== "kr" && isSubscriptionCardModifyAttempted == true && spc_e_cardNumber != "" && spc_e_expirydate != "" && spc_e_ccv !== "") {
                spe_card_actions();
                addCard(); // add new stripe card
            } else {
                let update_by_plan_structure = isAnnual == 1 ? 'confirm_shave_plan_annuals' : 'confirm_shave_plan';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: GLOBAL_URL + "/user/updates/" + update_by_plan_structure,
                    data: { countryiso: country_iso, originalIsAnnual: isAnnual, gender: gender },
                    type: 'POST',
                    cache: false,
                })
                    .done(function (response) {

                        window.location.reload();
                    })
                    .fail(function (error) {
                        $("#error-edit-shave").html(error.responseJSON.message);
                    })
            }

        }

    }
}

function addCard() {

    loading.style.display = "block";
    let card_number = $("#spc_e_cardNumber").val().trim().replace(/\s/g, '');
    let exp_month = 0;
    if (parseInt($("#spc_e_expirydate").val().trim().split("/")[0]) < 10) {
        exp_month = ('0' + $("#spc_e_expirydate").val().trim().split("/")[0].slice(-2));
    } else {
        exp_month = $("#spc_e_expirydate").val().trim().split("/")[0];
    }

    let exp_year = ('20' + $("#spc_e_expirydate").val().trim().split("/")[1].slice(-2));
    let cvv = $("#spc_e_ccv").val().trim();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/user/updates/cards/add-api',
        method: "POST",
        cache: false,
        data: { card_number: card_number, exp_month: exp_month, exp_year: exp_year, cvv: cvv },
    })
        .done(function (response) {
            console.log(response);
            if (response) {
                let card_id = response.card_from_db.id;
                generatePaymentIntents('new', card_id, spe_data.subscription.id);
            } else {
                Swal.fire(
                    '',
                    trans('website_contents.global.card_action_messages.card_add_failed', {}),
                    'error'
                ).then((result) => {

                })
            }

            loading.style.display = "none";
        })
        .fail(function (error) {
            console.log(error);
            let err_response = '';
            if (error.responseJSON.message == 'Server Error') {
                err_response = trans('website_contents.global.invalid_card');
            } else {
                err_response = error.responseJSON.message;
            }
            $("#spc_e_cardNumber_error").html('<label id="pc_e_card-error" class="error" for="pc_e_card" style="">' + err_response + '<label>');
            loading.style.display = "none";
        })
}

function generatePaymentIntents(type, card_id, subscription_id) {
    $("#loading").css("display", "block");
    let OTP_URL = window.location.origin + GLOBAL_URL + "/stripe/confirm-api";
    let RETURN_URL_WITH_OTP = window.location.origin + GLOBAL_URL + `/stripe/payment-intent/otp-return?user_id=${user_id}&type=${type}&card_id=${card_id}&otp=1&subscription_id=${subscription_id}&subscription_update_payment=true`;
    let RETURN_URL_WITH_NON_OTP = window.location.origin + GLOBAL_URL + "/stripe/payment-intent/otp-return";
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + "/stripe/payment-intent/otp-charge-api",
        data: { card_id: card_id, type: type, subscription_id: subscription_id },
        type: 'POST',
        cache: false,
    })
        .done(function (response) {
            console.log(response);
            console.log(response.status);
            if (response != null && response.status == 'requires_confirmation') {
                console.log('initiate payment confirm');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: OTP_URL,
                    data: {
                        payment_intent_id: response.id,
                        return_url: RETURN_URL_WITH_OTP,
                    },
                    type: 'POST',
                    cache: false,
                })
                    .done(function (data) {
                        console.log(data);
                        if (data.next_action != null) {
                            if (data.status == "requires_source_action" || data.status == "requires_action") {
                                // If got OTP
                                // window.location = data.next_action.redirect_to_url.url;
                                var iframe = document.createElement('iframe');
                                var container = document.getElementById('stripe_otp_iframe');
                                iframe.src = data.next_action.redirect_to_url.url;
                                iframe.width = 600;
                                iframe.height = 400;
                                container.appendChild(iframe);
                                loading.style.display = "none";
                                $("#stripe_otp_container").modal('show');
                            } else {
                                // If no OTP
                                if (data.status == "succeeded") {
                                    $.ajax({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        url: RETURN_URL_WITH_NON_OTP,
                                        data: {
                                            payment_intent: data.id,
                                            non_otp: true,
                                            user_id : user_id,
                                            subscription_update_payment: true,
                                            subscription_id: spe_data.subscription.id
                                        },
                                        type: 'GET',
                                        cache: false,
                                    })
                                        .done(function (data2) {
                                            if (data2) {
                                                let non_otp_response = JSON.parse(data2);
                                                $("#loading").css("display", "none");
                                                if (non_otp_response.status == 200 && non_otp_response.error == null) {
                                                    // OTP One Time Charge Succeeded & Refunded
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_added_subscriptions', {}),
                                                        'success'
                                                    ).then((result) => {
                                                        stripeUpdateSubscription(card_id, true, true, null);
                                                    });
                                                } else if (non_otp_response.status == 200 && non_otp_response.error == "refund_fail") {
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                        'error'
                                                    ).then((result) => {
                                                        stripeUpdateSubscription(card_id, true, false, null);
                                                    });
                                                } else if (data.status == 400) {
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                        'error'
                                                    ).then((result) => {
                                                        stripeUpdateSubscription(data.card_data.id, false, false, non_otp_response.error);
                                                    });
                                                } else {
                                                    Swal.fire(
                                                        '',
                                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                        'error'
                                                    ).then((result) => {
                                                        stripeUpdateSubscription(card_id, false, false, null);
                                                    });
                                                }
                                            }
                                        })
                                        .fail(function (error) {
                                            stripeUpdateSubscription(card_id, false, false, error.responseJSON.message);
                                        })
                                }
                            }
                        } else {
                            // If no OTP
                            if (data.status == "succeeded") {
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: RETURN_URL_WITH_NON_OTP,
                                    data: {
                                        payment_intent: data.id,
                                        non_otp: true,
                                        user_id : user_id,
                                        subscription_update_payment: true,
                                        subscription_id: spe_data.subscription.id
                                    },
                                    type: 'GET',
                                    cache: false,
                                })
                                    .done(function (data2) {
                                        if (data2) {
                                            let non_otp_response = JSON.parse(data2);
                                            $("#loading").css("display", "none");
                                            if (non_otp_response.status == 200 && non_otp_response.error == null) {
                                                // OTP One Time Charge Succeeded & Refunded
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_added_subscriptions', {}),
                                                    'success'
                                                ).then((result) => {
                                                    stripeUpdateSubscription(card_id, true, true, null);
                                                });
                                            } else if (non_otp_response.status == 200 && non_otp_response.error == "refund_fail") {
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                    'error'
                                                ).then((result) => {
                                                    stripeUpdateSubscription(card_id, true, false, null);
                                                });
                                            } else if (data.status == 400) {
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                    'error'
                                                ).then((result) => {
                                                    stripeUpdateSubscription(data.card_data.id, false, false, non_otp_response.error);
                                                });
                                            } else {
                                                Swal.fire(
                                                    '',
                                                    trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                                    'error'
                                                ).then((result) => {
                                                    stripeUpdateSubscription(card_id, false, false, null);
                                                });
                                            }
                                        }
                                    })
                                    .fail(function (error) {
                                        stripeUpdateSubscription(card_id, false, false, error.responseJSON.message);
                                    })
                            }
                        }
                    })
                    .fail(function (error) {
                        loading.style.display = "none";
                        console.log(error);
                        $("#error-edit-shave").html(error.responseJSON.message);
                        stripeUpdateSubscription(card_id, false, false, error.responseJSON.message);
                    })
            } else {
                if (response.status == "succeeded") {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: RETURN_URL_WITH_NON_OTP,
                        data: {
                            payment_intent: response.id,
                            non_otp: true,
                            user_id : user_id,
                            subscription_update_payment: true,
                            subscription_id: spe_data.subscription.id
                        },
                        type: 'GET',
                        cache: false,
                    })
                        .done(function (data2) {
                            if (data2) {
                                let non_otp_response = JSON.parse(data2);
                                $("#loading").css("display", "none");
                                if (non_otp_response.status == 200 && non_otp_response.error == null) {
                                    // OTP One Time Charge Succeeded & Refunded
                                    Swal.fire(
                                        '',
                                        trans('website_contents.global.card_action_messages.card_added_subscriptions', {}),
                                        'success'
                                    ).then((result) => {
                                        stripeUpdateSubscription(card_id, true, true, null);
                                    });
                                } else if (non_otp_response.status == 200 && non_otp_response.error == "refund_fail") {
                                    Swal.fire(
                                        '',
                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                        'error'
                                    ).then((result) => {
                                        stripeUpdateSubscription(card_id, true, false, null);
                                    });
                                } else if (data.status == 400) {
                                    Swal.fire(
                                        '',
                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                        'error'
                                    ).then((result) => {
                                        stripeUpdateSubscription(data.card_data.id, false, false, non_otp_response.error);
                                    });
                                } else {
                                    Swal.fire(
                                        '',
                                        trans('website_contents.global.card_action_messages.card_add_failed', {}),
                                        'error'
                                    ).then((result) => {
                                        stripeUpdateSubscription(card_id, false, false, null);
                                    });
                                }
                            }
                        })
                        .fail(function (error) {
                            loading.style.display = "none";
                            console.log(error);
                            $("#error-edit-shave").html(error.responseJSON.message);
                            stripeUpdateSubscription(card_id, false, false, error.responseJSON.message);
                        })
                }
            }
        })
        .fail(function (error) {
            loading.style.display = "none";
            console.log(error);
            $("#error-edit-shave").html(error.responseJSON.message);
            stripeUpdateSubscription(card_id, false, false, error.responseJSON.message);
        })
}

function stripeUpdateSubscription(card_id, otp_process, refund_process, stripe_error) {
    console.log(card_id, otp_process, refund_process, stripe_error);
    let update_by_plan_structure = isAnnual == 1 ? 'confirm_shave_plan_annuals' : 'confirm_shave_plan';
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + "/user/updates/" + update_by_plan_structure,
        data: { countryiso: country_iso, originalIsAnnual: isAnnual, gender: gender, otp_process: otp_process, card_id: card_id, refund_process: refund_process, stripe_error: stripe_error },
        type: 'POST',
        cache: false,
    })
        .done(function (response) {
            window.location.reload();
        })
        .fail(function (error) {
            console.log(error);
            $("#error-edit-shave").html(error.responseText.message);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            });
        })
}


window.stripeOneTimeChargeReturn = function (data) {
    $('#stripe_otp_container').modal('hide');

    if (data.status == 200 && data.error == null) {
        // OTP One Time Charge Succeeded & Refunded
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_added_subscriptions', {}),
            'success'
        ).then((result) => {
            stripeUpdateSubscription(data.card_data.id, true, true, null);
        });
    } else if (data.status == 200 && data.error == "refund_fail") {
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            stripeUpdateSubscription(data.card_data.id, true, false);
        });
    } else if (data.status == 400) {
        console.log(data);
        $("#error-edit-shave").html(data.error);
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            stripeUpdateSubscription(data.card_data.id, false, false, data.error);
        });
    } else {

        $("#error-edit-shave").html(data.error);
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            stripeUpdateSubscription(null, false, false, null);
        });
    }
};

// ----------------------------------------------------------------------------------------------------------
// Jason ammendments

function pauseSubscription() {
    let pause_months = $("#spe_pause_months").val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + "/user/updates/pause_shave_plan",
        data: { pause_months: pause_months, subscription_id: spe_data.subscription.id },
        type: 'POST',
        cache: false,
    })
        .done(function (response) {
            // console.log(response);
            if (response === "success") {
                $("#pausePlan").modal('hide');
                Swal.fire({
                    text: parseInt(pause_months) > 1 ? trans('website_contents.authenticated.shaveplansedit.pause_plan_success', { month: pause_months }) : trans('website_contents.authenticated.shaveplansedit.pause_plan_successV2', { month: pause_months }),
                    // width: '30%',
                    // height: '20%',
                    padding: '0 1em',
                    heightAuto: true,
                    backdrop: true,
                    background: 'white',
                    position: 'left',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: false,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: trans('website_contents.global.close'),
                    customClass: {
                        confirmButton: 'btn btn-black2',
                    },
                }).then((result) => {
                    window.location.reload();
                })
            }
        })
        .fail(function (error) { })

}

function selectPause(months) {
    $("#spe_pause_months").val(months);
    if (months === 1) {
        $("#months_" + months).addClass('item-selected');
        $("#months_" + months).removeClass('item-unselected');
        $("#months_2").addClass('item-unselected');
        $("#months_2").removeClass('item-selected');
        $("#months_3").addClass('item-unselected');
        $("#months_3").removeClass('item-selected');
        $("#pause_plan_date").html("<b>" + pauseplanone + "</b>");
    } else if (months === 2) {
        $("#months_" + months).addClass('item-selected');
        $("#months_" + months).removeClass('item-unselected');
        $("#months_1").addClass('item-unselected');
        $("#months_1").removeClass('item-selected');
        $("#months_3").addClass('item-unselected');
        $("#months_3").removeClass('item-selected');
        $("#pause_plan_date").html("<b>" + pauseplantwo + "</b>");
    } else if (months === 3) {
        $("#months_" + months).addClass('item-selected');
        $("#months_" + months).removeClass('item-unselected');
        $("#months_1").addClass('item-unselected');
        $("#months_1").removeClass('item-selected');
        $("#months_2").addClass('item-unselected');
        $("#months_2").removeClass('item-selected');
        $("#pause_plan_date").html("<b>" + pauseplanthree + "</b>");
    }
}

function spe_card_actions() {

    loading.style.display = "none";
    v_sp_cards("form_spe_card");
    if ($("#form_spe_card").serialize() !== "" && $("#form_spe_card").valid()) {

        if (country_iso == 'kr') {
            let card_number = $("#spc_e_cardNumber").val() !== '' ? $("#spc_e_cardNumber").val() : '';

            let exp_month = 0;
            if (parseInt($("#spc_e_expirydate").val().trim().split("/")[0]) < 10) {
                exp_month = ('0' + $("#spc_e_expirydate").val().trim().split("/")[0].slice(-2));
            }
            else {
                exp_month = $("#spc_e_expirydate").val().trim().split("/")[0];
            }
            let exp_year = $("#spc_e_expirydate").val() !== '' ? ($("#spc_e_expirydate").val().trim().split("/")[1].slice(-4)) : '';
            let card_birth = $("#spc_e_card_birth").val();
            let card_password = $("#spc_e_card_password").val();

            if (card_number !== '' && exp_month !== '' && exp_year !== '' && card_birth !== '' && card_password !== '') {

                let new_card_details = {
                    "card_number": card_number,
                    "exp_month": exp_month,
                    "exp_year": exp_year,
                    "card_birth": card_birth,
                    "card_password": card_password
                }

                let update = {
                    "type": 'spe_subscription_card',
                    "subtype": "subscription_card",
                    "updates": {
                        "isModified": true,
                        "": spe_data.subscription_card,
                        "new": new_card_details
                    }

                };

                isSubscriptionCardModifyAttempted = true;

                console.log(update);

                updateSession(update);
            } else {
                v_sp_cards("form_spe_card");
            }
        } else {
            let card_number = $("#spc_e_cardNumber").val() !== '' ? $("#spc_e_cardNumber").val().trim().replace(/\s/g, '') : '';
            let exp_month = 0;
            if (parseInt($("#spc_e_expirydate").val().trim().split("/")[0]) < 10) {
                exp_month = ('0' + $("#spc_e_expirydate").val().trim().split("/")[0].slice(-2));
            }
            else {
                exp_month = $("#spc_e_expirydate").val().trim().split("/")[0];
            }
            let exp_year = $("#spc_e_expirydate").val() !== '' ? ('20' + $("#spc_e_expirydate").val().trim().split("/")[1].slice(-2)) : '';
            let cvv = $("#spc_e_ccv").val() !== '' ? $("#spc_e_ccv").val().trim() : '';
            if (card_number !== '' && exp_month !== '' && exp_year !== '' && cvv !== '') {

                let new_card_details = {
                    "card_number": card_number,
                    "exp_month": exp_month,
                    "exp_year": exp_year,
                    "cvv": cvv
                }

                let update = {
                    "type": 'spe_subscription_card',
                    "subtype": "subscription_card",
                    "updates": {
                        "isModified": true,
                        "existing": spe_data.subscription_card,
                        "new": new_card_details
                    }

                };

                // console.log(update);

                isSubscriptionCardModifyAttempted = true;
                updateSession(update);
            } else {
                v_sp_cards("form_spe_card");
            }
        }

    } else {

    }
}

function pre_applyPromo() {
    let spp_e_promoCode = $("#spp_e_promoCode");
    checkPromotion(spp_e_promoCode.val(), _session_data, spe_data, 'normal');
}

function pre_applyPromoCheck(result) {

    $("#error-promotion").text("");
    if (spe_existing_promo_name.val() !== "" && spe_existing_promo_id.val() !== "") {
        $("#success-promotion").text(trans('validation.custom.validation.promo.valid'));

        spe_new_promo_details.html('');
        let new_promo_details =
            '<h4 class="MuliExtraBold mb-4">' + trans('website_contents.authenticated.shaveplansedit.new_promotion') + '</h4>' +
            '<h3 class="MuliExtraBold" id="spe_new_promo_discount_percent">' + trans('website_contents.authenticated.shaveplansedit.promotion_discount', { promo: result.discount }) + '</h3>' +
            '<p class="Muli fs-16" id="spe_new_promo_discount_name">(' + result.code + ')</p>';
        spe_new_promo_details.html(new_promo_details);
        spe_selected_promo_id.val(result.id);
        pre_selectNewPromo_btn.attr('onClick', 'pre_selectNewPromo(' + JSON.stringify(result) + ')');
        // show popup
        $("#selectPromoToBeApplied").modal('show');
    } else {
        $("#success-promotion").text(trans('validation.custom.validation.promo.valid'));

        $("#promotion_selection").removeClass('d-none');
        applied_promo.html('');
        spe_applied_promo_id.val(result.PromotionId);
        spe_applied_promo_name.val(result.code);
        spe_data.existing_promotions.forEach(function (obj, index) {
            if (obj.type === 'normal') {

                let options = '<option value="' + obj.pid + '">' + obj.code + '</option>';
                applied_promo.append(options);
            }
            if (obj.type === 'cancellation') {
                let options = '<option value="cancellation">' + trans('website_contents.authenticated.shaveplansedit.voucher', { discount: obj.discount }) + '</option>';
                applied_promo.append(options);
            }
        });

        if (result) {

            let options = '<option value="' + result.pid + '">' + result.code + '</option>';
            applied_promo.append(options);
        }

        spp_e_promoCode.value = '';
        let update = {
            "type": 'spe_promotion',
            "subtype": "promotion",
            "updates": {
                "isModified": true,
                "existing": spe_existing_promo_name.val() !== "" ? spe_existing_promo_name.val() : null,
                "isCancellation": spe_applied_cancellation.val() === "1" ? true : false,
                "temp_discountPercent": spe_applied_temp_discount.val(),
                "applied": result.code,
                "selected": result.code,
                "type": result.promotionType,
                "isFreeProducts": result.freeProductCountryIds !== null ? true : false,
                "isFreeExistingProducts": result.freeExistProductCountryIds !== null ? true : false,
                "isDiscount": result.discount > 0 ? true : false,
                "checking": result
            }
        };

        // console.log(update);
        updateSession(update);
    }
}

function pre_selectPromoToBeApplied(result, type) {
    // console.log(result, type);
    if (type === 'normal') {
        let update = {
            "type": 'spe_promotion',
            "subtype": "promotion",
            "updates": {
                "isModified": true,
                "existing": spe_existing_promo_name.val() !== "" ? spe_existing_promo_name.val() : null,
                "isCancellation": spe_applied_cancellation.val() === "1" ? true : false,
                "temp_discountPercent": spe_applied_temp_discount.val(),
                "applied": result.code,
                "selected": result.code,
                "type": result.promotionType,
                "isFreeProducts": result.freeProductCountryIds !== null ? true : false,
                "isFreeExistingProducts": result.freeExistProductCountryIds !== null ? true : false,
                "isDiscount": result.discount > 0 ? true : false,
                "checking": result
            }
        };
        updateSession(update);
    } else if (type === 'cancellation') {
        let update = {
            "type": 'spe_promotion',
            "subtype": "promotion",
            "updates": {
                "isModified": true,
                "existing": spe_existing_promo_name.val() !== "" ? spe_existing_promo_name.val() : null,
                "isCancellation": spe_applied_cancellation.val() === "1" ? true : false,
                "temp_discountPercent": spe_applied_temp_discount.val(),
                "applied": null,
                "selected": "cancellation_discount",
                "type": "Instant",
                "isFreeProducts": null,
                "isFreeExistingProducts": null,
                "isDiscount": null,
                "checking": null
            }
        };

        // console.log(update);
        updateSession(update);
    }

}

function pre_selectNewPromo(result) {
    applied_promo.html('');
    spe_data.existing_promotions.forEach(function (obj, index) {
        if (obj.type === 'normal') {
            let options = '<option value="' + obj.pid + '">' + obj.code + '</option>';
            applied_promo.append(options);
        }
        if (obj.type === 'cancellation') {
            let options = '<option value="cancellation">' + obj.discount + '% OFF Voucher</option>';
            applied_promo.append(options);
        }
    });

    if (result) {
        let options = '<option selected value="' + result.pid + '">' + result.code + '</option>';
        applied_promo.append(options);
    }

    spe_applied_promo_id.val(result.PromotionId);
    spe_applied_promo_name.val(result.code);

    $("#selectPromoToBeApplied").modal('hide');

    let update = {
        "type": 'spe_promotion',
        "subtype": "promotion",
        "updates": {
            "isModified": true,
            "existing": spe_existing_promo_name.val() !== "" ? { "code": spe_existing_promo_name.val(), "promotion_id": spe_existing_promo_id.val() } : null,
            "isCancellation": spe_applied_cancellation.val() === "1" ? true : false,
            "temp_discountPercent": spe_applied_temp_discount.val(),
            "applied": result.code,
            "selected": result.code,
            "type": result.promotionType,
            "isFreeProducts": result.freeProductCountryIds !== null ? true : false,
            "isFreeExistingProducts": result.freeExistProductCountryIds !== null ? true : false,
            "isDiscount": result.discount > 0 ? true : false,
            "checking": result
        }
    };

    // console.log(update);
    updateSession(update);
}
// Promotion
function onApplyPromo(data, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, status) {
    let checkout_data_promo = {
        "journey_type": 'checkout',
        "update_type": "selected_promo",
        "data": data,
        "total": total,
        "ntotal": ntotal,
        "ablefreeexistproduct": ablefreeexistproduct,
        "freeexistresultproduct": freeexistresultproduct,
        "freeexistresultproductprice": freeexistresultproductprice,
        "status": status
    };
    UpdateSessionData(checkout_data_promo);
}

function pre_updateFrequency(frequency) {
    let current_plan_id = spe_data.subscription.PlanId;
    let current_plan = '';
    let modified_plan = '';


    original_plan_on_change = current_plan = spe_data.available_plans.active_plans.find(x => {
        return x.PlanId === current_plan_id;
    });

    // console.log("frequency", frequency);
    if (blade_pack_type_has_been_changed) {
        modified_plan_on_change = modified_plan = new_blade_type_plan_lists.active_plans.find(x => {
            return x.duration === frequency.toString();
        });
    } else {
        modified_plan_on_change = modified_plan = spe_data.available_plans.active_plans.find(x => {
            return x.duration === frequency.toString();
        });
    }

    let plan_price_original = current_plan.sellPrice;
    let plan_duration_original = current_plan.duration;
    let plan_price_new = modified_plan.sellPrice;
    let plan_duration_new = modified_plan.duration;
    if (isAnnual == 1 && spe_data.subscription.isTrial == 1 && spe_data.subscription.isCustom == 0) {
        let addons_total = (12 / parseInt(plan_duration_new)) * parseFloat(total_subprice.innerHTML);
        let new_price = parseFloat(plan_price_new) + parseFloat(addons_total);
        total_subprice.innerHTML = new_price.toFixed(2);

    }


    let update = {
        "type": 'spe_frequency',
        "subtype": "frequency",
        "updates": {
            "isModified": true,
            "_original": current_plan,
            "_modified": modified_plan
        }
    };

    // console.log(update);
    updateSession(update);
    spf_e_frequency.setAttribute("disabled", null)

}

function pre_defaultSubsProducts() {
    values = '';
    // console.log(default_products);
    default_products.forEach(function (default_product) {
        values += default_product.productcountryid + ",";
    });
    default_productaddons.forEach(function (default_product) {
        values += default_product.ProductCountryId + ",";
    });

    values = values.substr(0, values.length - 1);
    let update = {
        "type": 'default',
        "subtype": "plan_products",
        "updates": {
            "productcountryIds": values,
            "test1": values,
        }

    };
    $("#spe_presaved_products").val(values);
    updateSession(update);
}

function pre_defaultSubsCard() {
    let update = {
        "type": 'default',
        "subtype": "subscription_card",
        "updates": {
            "existing": spe_data.subscription_card
        }

    };
    // console.log(update);
    updateSession(update);
}

function pre_defaultAddress_shipping() {
    let update1 = {
        "type": 'default',
        "subtype": "shipping_address",
        "updates": {
            "existing": spe_data.address.shipping,
        },
    };
    // console.log("default shipping", update1);
    updateSession(update1);
}

function pre_defaultAddress_billing() {

    let update2 = {
        "type": 'default',
        "subtype": "billing_address",
        "updates": {
            "existing": spe_data.address.billing,
        },
    };
    // console.log("default billing", update2);
    updateSession(update2);
}

function pre_editAddresses(type) {
    if (type === 'shipping') {
        loading.style.display = "block";
        v_sp_delivery("form_spe_shipping");
        // console.log($("#form_spe_shipping").serialize());
        // console.log($("#form_spe_shipping").valid());
        if ($("#form_spe_shipping").serialize() !== "" && $("#form_spe_shipping").valid()) {
            let SSN = "";
            if (country_iso == 'tw') {
                SSN = spd_e_ssn.value;
            }
            let address = spd_e_address.value;
            let city = spd_e_city.value;
            let portalCode = spd_e_portalCode.value;
            let state = spd_e_state.value;

            let new_delivery_address = {
                "SSN": SSN,
                "address": address,
                "city": city,
                "portalCode": portalCode,
                "state": state
            }

            let update = {
                "type": 'spe_address',
                "subtype": "shipping_address",
                "updates": {
                    "isModified": true,
                    "existing": spe_data.address.shipping,
                    "new": new_delivery_address,
                },
            };
            loading.style.display = "none";
            updateSession(update);
        } else {
            loading.style.display = "none";
        }
    }

    if (type === 'billing') {
        loading.style.display = "block";
        v_sp_billing("form_spe_billing");
        // console.log($("#form_spe_shipping").serialize());
        // console.log($("#form_spe_shipping").valid());
        if ($("#form_spe_billing").serialize() !== "" && $("#form_spe_billing").valid()) {
            let address = spb_e_address.value;
            let city = spb_e_city.value;
            let portalCode = spb_e_portalCode.value;
            let state = spb_e_state.value;

            let new_shipping_address = {
                "address": address,
                "city": city,
                "portalCode": portalCode,
                "state": state
            }

            let update = {
                "type": 'spe_address',
                "subtype": "billing_address",
                "updates": {
                    "isModified": true,
                    "existing": spe_data.address.billing,
                    "new": new_shipping_address,
                },
            };

            loading.style.display = "none";
            updateSession(update);
        } else {
            loading.style.display = "none";
        }
    }
}

function pre_removeSubsProducts(ProductCountryId) {
    let old_values = $("#spe_to_update_products").val();
    products_to_be_removed.push(ProductCountryId);
    let update_values = '';
    if (old_values !== "") {
        update_values = old_values.split(",");
        for (var i in update_values) {
            if (update_values[i] == ProductCountryId) {
                update_values.splice(i, 1);
                break;
            }
        }
        $("#spe_to_update_products").val(update_values);
        getProductCountryIds(update_values, 'remove', ProductCountryId, "pre_removeSubsProducts");
    } else {
        let old_values = $("#spe_presaved_products").val();
        update_values = old_values.split(",");
        for (var i in update_values) {
            if (update_values[i] == ProductCountryId) {
                update_values.splice(i, 1);
                break;
            }
        }
        $("#spe_to_update_products").val(update_values);
        getProductCountryIds(update_values, 'remove', ProductCountryId, "pre_removeSubsProducts");
    }
}

function pre_changeBlade(ProductCountryId, type) {
    if (type === 2) {
        loading.style.display = "block";
        blade_pack_type_has_been_changed = true;
        let user_id = spe_data.user.id;
        let subscription_id = spe_data.subscription.id;
        let current_plan_id = spe_data.subscription.PlanId;
        let current_plan = '';
        let handle_type = spe_data.available_plans.handle_type.ProductCountryId;
        let oldfrequency = '';
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: GLOBAL_URL + "/user/checkbladetype",
            data: { user_id: user_id, subscription_id: subscription_id, ProductCountryId: ProductCountryId, handle_type: handle_type },
            type: 'POST',
            cache: false,
        })
            .done(function (response) {
                response = JSON.parse(response);
                // console.log("blade plans", response);
                new_blade_type_plan_lists = response;
                current_plan = spe_data.available_plans.active_plans.find(x => {
                    return x.PlanId === current_plan_id;
                });

                if (response !== "") {
                    oldfrequency = spf_e_frequency.value;
                    if (response.active_plans !== null || response.active_plans !== "") {
                        spf_e_frequency.innerHTML = '';
                        // let append_new_frequencies2 = document.createElement("OPTION");
                        // append_new_frequencies2.text = '';
                        // append_new_frequencies2.value = 'select';
                        // spf_e_frequency.value = 'select'
                        // spf_e_frequency.appendChild(append_new_frequencies2);

                        response.active_plans.forEach(function (obj) {

                            // update blade type & plan product list
                            pre_updateBladePackType(ProductCountryId, spe_data.available_plans.blade_type.id, oldfrequency)

                            // update frequencies for new blade type
                            let append_new_frequencies = document.createElement("OPTION");
                            append_new_frequencies.text = (isAnnual == 1 ? 12 : obj.duration) + ' ' + trans('website_contents.Months');
                            append_new_frequencies.value = obj.duration;
                            spf_e_frequency.appendChild(append_new_frequencies);
                            if (current_plan.duration === obj.duration) {
                                spf_e_frequency.value = obj.duration;
                            }
                        });
                    }
                    loading.style.display = "none";
                } else {
                    loading.style.display = "none";
                }
            })
            .fail(function (error) {
                loading.style.display = "none";
            });
    }
    if (type === 1) {
        loading.style.display = "block";
        if ($("#show_blade_options").is(':visible')) {
            $("#show_blade_options").hide("slow");
            loading.style.display = "none";
        } else {
            $("#show_blade_options").show("slow");
            loading.style.display = "none";
        }

    }
}

function setSelectedValue(selectObj, valueToSet, type) {
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
            if (type == "changeblade") {
                pre_updateFrequency(selectObj.options[i].value);
            }
            return;
        }
    }
    if (selectObj.value == "" || selectObj.value == null || selectObj.value == "select") {
        if (selectObj.options.length > 1) {
            selectObj.options[1].selected = true;
            if (type == "changeblade") {
                pre_updateFrequency(selectObj.options[1].value);
            }
            return;
        } else {
            selectObj.options[0].selected = true;
            if (type == "changeblade") {
                pre_updateFrequency(selectObj.options[0].value);
            }
            return;
        }
    }
}

function pre_updateSubsProducts() {
    let spe_e_product_addon = $("#spe_presaved_products").val();
    // console.log(spe_e_product_addon);
    let productcountryIds = [];
    if (spe_e_product_addon) {
        spe_e_product_addon = spe_e_product_addon.split(",");
        for (i = 0; i < spe_e_product_addon.length; i++) {
            productcountryIds.push(parseInt(spe_e_product_addon[i]));
        }
    }

    $("show_blade_options").html('');
    getProductCountryIds(productcountryIds, 'add', '', "pre_updateSubsProducts");
}

function pre_updateBladePackType(new_blade, old_blade, oldfrequency) {
    let spe_e_product_addon = $("#spe_presaved_products").val();

    let new_product_country_ids = spe_e_product_addon.split(',');
    let old_blade_index = new_product_country_ids.indexOf(old_blade.toString());

    if (~old_blade_index) {
        new_product_country_ids[old_blade_index] = new_blade.toString();
    }

    let productcountryIds = [];
    if (spe_e_product_addon) {
        for (i = 0; i < new_product_country_ids.length; i++) {
            productcountryIds.push(parseInt(new_product_country_ids[i]));
        }
    }

    getProductCountryIds(productcountryIds, 'add', oldfrequency, "pre_updateBladePackType");
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function getProductCountryIds(productcountryIds, type, originalvalue, functiontype) {
    // console.log(productcountryIds, type);
    let spe_total_optional_products = $("#spe_total_optional_products");
    // console.log(productcountryIds);
    let spe_to_update_products_arr = '';
    let to_update_ids = '';
    let buildPlanProducts = '';
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + "/user/ssss",
        data: { productcountryIds: productcountryIds },
        type: 'POST',
        cache: false,
    })
        .done(function (response) {
            console.log(response);
            if (response !== 0) {
                if (document.getElementById('spe_updateSubsProductList')) {
                    $("#spe_updateSubsProductList").empty();
                }

                let length = 21;
                for (zf = 1; zf < length; zf++) {
                    if (document.getElementById('spe_updateSubsProductList_' + zf)) {
                        $("#spe_updateSubsProductList_" + zf).empty();
                    }
                }

                $("#show_blade_options").remove();
                let blade_name = '';

                let _productcountryIds = '';
                let totalsubprice = parseFloat(0.00);
                // if(total_subprice){
                // totalsubprice = totalsubprice + total_subprice.innerHTML;
                // }
                response.forEach(function (product) {
                    if (spe_data.subscription.isTrial === 1 && spe_data.subscription.isCustom === 0) {
                        // if trial, do not include price of premium handle, sviwel handle & female handle
                        if (product.sku !== "H1" && product.sku !== "H2" && product.sku !== "H3") {
                            totalsubprice = parseFloat(product.sellPrice) + parseFloat(totalsubprice);
                            if (isAnnual === 1) {
                                if (product.sku === "S3/2018" || product.sku === "S5/2018" || product.sku === "S6/2018" || product.sku === "F5") {
                                    totalsubprice = totalsubprice - parseFloat(product.sellPrice);
                                }
                            }
                        }
                    } else {
                        totalsubprice = parseFloat(product.sellPrice) + parseFloat(totalsubprice);
                    }
                    to_update_ids += "" + product.ProductCountryId + ',';
                    if (product.sku === 'S3/2018') {
                        blade_name = trans('website_contents.global.blade', { blade: 3 });
                    } else if (product.sku === 'S5/2018') {
                        blade_name = trans('website_contents.global.blade', { blade: 5 });
                    } else if (product.sku === 'S6/2018') {
                        blade_name = trans('website_contents.global.blade', { blade: 6 });
                    } else if (product.sku === 'F5') {
                        blade_name = trans('website_contents.global.blade', { blade: 5 });
                    }
                    if (product.sku === "S3/2018" || product.sku === "S5/2018" || product.sku === "S6/2018" || product.sku === "F5") {
                        buildPlanProducts =
                            '<div class="row pt-4 mx-0 border-bottom align-items-center" >' +
                            '<div class="col-4 text-center">' +
                            '<img class="img-fluid image_selected_blade" style="max-height:100px;" src="' + product.productDefaultImage + '" />' +
                            '</div>' +
                            '<div class="col-6">' +
                            '<b><span class="text_selected_blade">' + blade_name + (isAnnual == 1 ? ' x ' + (12 / parseInt(modified_plan_on_change.duration)) : '') + '</span></b>' +
                            '</div>' +
                            '<div class="col-2 text-right padd0">' +
                            '<button type="button" onClick="pre_changeBlade(' + product.ProductCountryId + ',1)" id="" class="remove_selected_blade btn btn-circle" ><i class="fa fa-pencil"></i></button>' +
                            '</div>' +
                            '</div>' +
                            '<div style="display:none;padding:0px;" class="col-12" id="show_blade_options"></div>';
                        $("#spe_updateSubsProductList").append(buildPlanProducts);
                    } else if ((spe_data.subscription.isTrial === 1 && spe_data.subscription.isCustom === 0) && (product.sku !== "H1" && product.sku !== "H2" && product.sku !== "H3")) {
                        buildPlanProducts =
                            '<div class="row pt-4 mx-0 border-bottom align-items-center" >' +
                            '<div class="col-4 text-center">' +
                            '<img class="img-fluid image_selected_blade" style="max-height:100px;" src="' + product.productDefaultImage + '" />' +
                            '</div>' +
                            '<div class="col-6">' +
                            '<b><span class="text_selected_blade">' + product.name + (isAnnual == 1 ? ' x ' + (12 / parseInt(modified_plan_on_change.duration)) : '') + '</span></b>' +
                            '</div>' +
                            '<div class="col-2 text-right padd0">' +
                            '<button type="button" onClick="pre_removeSubsProducts(' + product.ProductCountryId + ')" id="" class="remove_selected_blade btn btn-circle spe_close_btn"></button>' +
                            '</div>' +
                            '</div>';
                        $("#spe_updateSubsProductList").append(buildPlanProducts);
                    } else {
                        if ((spe_data.subscription.isTrial === 1 && spe_data.subscription.isCustom === 0) && (product.sku !== "H1" && product.sku !== "H2" && product.sku !== "H3")) {
                            buildPlanProducts =
                                '<div class="row pt-4 mx-0 border-bottom align-items-center" >' +
                                '<div class="col-4 text-center">' +
                                '<img class="img-fluid image_selected_blade" style="max-height:100px;" src="' + product.productDefaultImage + '" />' +
                                '</div>' +
                                '<div class="col-6">' +
                                '<b><span class="text_selected_blade">' + product.name + '</span></b>' +
                                '</div>' +
                                '<div class="col-2 text-right padd0">' +
                                '<button type="button" onClick="pre_removeSubsProducts(' + product.ProductCountryId + ')" id="" class="remove_selected_blade btn btn-circle spe_close_btn"></button>' +
                                '</div>' +
                                '</div>';
                            $("#spe_updateSubsProductList").append(buildPlanProducts);
                        } else if ((spe_data.subscription.isCustom === 1 && spe_data.subscription.isTrial === 0) && (product.sku !== "H1" && product.sku !== "H2" && product.sku !== "H3")) {
                            buildPlanProducts =
                                '<div class="row pt-4 mx-0 border-bottom align-items-center" >' +
                                '<div class="col-4 text-center">' +
                                '<img class="img-fluid image_selected_blade" style="max-height:100px;" src="' + product.productDefaultImage + '" />' +
                                '</div>' +
                                '<div class="col-6">' +
                                '<b><span class="text_selected_blade">' + product.name + '</span></b>' +
                                '</div>' +
                                '<div class="col-2 text-right padd0">' +
                                '<button type="button" onClick="pre_removeSubsProducts(' + product.ProductCountryId + ')" id="" class="remove_selected_blade btn btn-circle spe_close_btn"></button>' +
                                '</div>' +
                                '</div>';
                            $("#spe_updateSubsProductList").append(buildPlanProducts);
                        }
                    }
                    _productcountryIds += product.ProductCountryId + ',';
                });

                if (spe_data.subscription.isTrial === 1 && spe_data.subscription.isCustom === 0) {
                    if (country_iso == 'kr') {
                        //Currency and price for kr
                        totalsubprice = formatNumber(totalsubprice);
                        total_subprice.innerHTML = totalsubprice;
                    } else {
                        total_subprice.innerHTML = totalsubprice.toFixed(2);
                    }
                } else {
                    if (country_iso == 'kr') {
                        //Currency and price for kr
                        totalsubprice = formatNumber(totalsubprice);
                        total_subprice.innerHTML = totalsubprice;
                    } else {
                        total_subprice.innerHTML = totalsubprice.toFixed(2);
                    }
                }

                _productcountryIds = _productcountryIds.substr(0, _productcountryIds.length - 1);
                let next_delivery_products = [];
                let ids = _productcountryIds.split(',');
                ids.forEach(function (obj, index) {
                    // console.log("obj", obj);
                    if (document.getElementById("addon_products_" + obj) !== null) {
                        let recurring_identifier_pcs = obj;
                        let isRecurring = document.getElementById("spe_optional_products_recurring_" + obj).checked;
                        next_delivery_products.push({ "product_country": recurring_identifier_pcs, "isRecurring": isRecurring });
                    }
                });



                let removeArray = [];
                if (value_update) {

                } else {
                    value_update = values;
                }
                var new_value_update = '';
                var count_value = 0;
                var valuesarr = value_update.split(',');

                if (type == "remove") {
                    valuesarr.forEach(function (original) {
                        // console.log("original + " + original);
                        if ($.inArray(original, productcountryIds) == -1) {
                            $("#product-" + original).removeClass('item-selected').addClass('item-unselected');
                            $("#addon_products_" + original).val("false");
                        }
                        else {
                            if (count_value == 0) {
                                new_value_update = new_value_update + original;
                            } else {
                                new_value_update = new_value_update + "," + original;
                            }
                            count_value++;
                        }
                    });
                    value_update = new_value_update;
                }
                else if (type == "add") {
                    value_update = $("#spe_presaved_products").val();
                }
                // console.log("after + " + value_update);
                let update = {}

                if (functiontype == "pre_updateBladePackType") {
                    setSelectedValue(spf_e_frequency, originalvalue, "changeblade");
                } else {
                    update = {
                        "type": 'modify_plan_products',
                        "subtype": "plan_products",
                        "updates": {
                            "isModified": true,
                            "productcountryIds": _productcountryIds,
                            "cycles": next_delivery_products,
                        }

                    };
                    updateSession(update);
                }



                spe_to_update_products_arr = to_update_ids.substr(0, to_update_ids.length - 1);
                $("#spe_to_update_products").val(spe_to_update_products_arr);
                $("#spe_presaved_products").val(value_update);
                // console.log(values);


                // update new blade type selections
                let blade_type_count = 0;
                spe_data.available_plans.blade_types.forEach(function (obj) {
                    let existing_items = $("#spe_presaved_products").val().split(',');
                    // console.log(existing_items.includes(obj.ProductCountryId.toString()));
                    if (ids.includes(obj.ProductCountryId.toString()) === false) {
                        blade_type_count++;
                        // console.log(obj);
                        if (obj.sku === 'S3/2018') {
                            blade_name = trans('website_contents.global.blade', { blade: 3 });
                        } else if (obj.sku === 'S5/2018') {
                            blade_name = trans('website_contents.global.blade', { blade: 5 });
                        } else if (obj.sku === 'S6/2018') {
                            blade_name = trans('website_contents.global.blade', { blade: 6 });
                        } else if (obj.sku === 'F5') {
                            blade_name = trans('website_contents.global.blade', { blade: 5 });
                        }

                        let blade_selection =
                            '<div class="row pt-4 mx-0 border-bottom" id="blade_type_' + blade_type_count + '" onClick="pre_changeBlade(' + obj.ProductCountryId + ',2)" style="cursor:pointer;">' +
                            '<div class="col-4 text-center">' +
                            '<img class="img-fluid image_selected_blade" src="' + obj.product_default_image + '">' +
                            '</div>' +
                            '<div class="col-6">' +
                            '   <b><span class="text_selected_blade">' + blade_name + '</span></b>' +
                            '</div>' +
                            '</div>';

                        $("#show_blade_options").append(blade_selection);
                    }
                });
                $("#addProduct").modal('hide');
            }
        })
        .fail(function (error) { })
}

// Function: Get session data
function GetSessionDataV2() {
    let session = [];
    window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            _session_data = data;
            session = data;
            // console.log(data);
        } else {
        }
    });
    return session;
}

function currentSession() {
    GetSessionDataV2();
}

function updateSession(data) {
    // session structure
    let _spe_e_session = {
        "plan_gender_type": "",
        "plan_products": {
            "isModified": false,
            "productcountryIds": {},
            "cycles": "",
            "removed": "",
            "modified": {}
        },
        "frequency": {
            "isModified": false,
            "_original": {},
            "_modified": {}
        },
        "promotion": {
            "isModified": false,
            "existing": {},
            "isCancellation": false,
            "temp_discountPercent": 0,
            "applied": {},
            "selected": {},
            "type": {},
            "isFreeProducts": false,
            "isFreeExistingProducts": false,
            "isDiscount": false,
            "checking": ''
        },
        "subscription_card": {
            "isModified": false,
            "existing": {},
            "new": {}
        },
        "address": {
            "shipping": {
                "isModified": false,
                "existing": {},
                "new": {}
            },
            "billing": {
                "isModified": false,
                "existing": {},
                "new": {}
            }
        },
        "user_data": spe_data
    }

    // get all default data and build session first.
    if (data.type === 'default') {
        if (data.subtype === 'plan_products') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.plan_products.hasOwnProperty(key)) {
                    _spe_e_session.plan_products[key] = arr[key];
                }
            });
        }
        if (data.subtype === 'subscription_card') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.subscription_card.hasOwnProperty(key)) {
                    _spe_e_session.subscription_card[key] = arr[key];
                }
            });
        }
        if (data.subtype === 'shipping_address') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.address.shipping.hasOwnProperty(key)) {
                    _spe_e_session.address.shipping[key] = arr[key];
                }
            });
        }
        if (data.subtype === 'billing_address') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.address.billing.hasOwnProperty(key)) {
                    _spe_e_session.address.billing[key] = arr[key];
                }
            });
        }
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }

    // add products
    if (data.type === 'modify_plan_products') {
        try {
            _spe_e_session = JSON.parse(_session_data);
        } catch (err) {
            console.log(err);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            })
        }

        _spe_e_session.plan_products.removed = products_to_be_removed;
        if (data.subtype === 'plan_products') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });
            // console.log(arr);
            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.plan_products.hasOwnProperty(key)) {
                    _spe_e_session.plan_products[key] = arr[key];
                }
            });

        }
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }

    // update frequency
    if (data.type === 'spe_frequency') {
        try {
            _spe_e_session = JSON.parse(_session_data);
        } catch (err) {
            console.log(err);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            })
        }
        if (data.subtype === 'frequency') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });
            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {
                // if property exists, update.
                if (_spe_e_session.frequency.hasOwnProperty(key)) {
                    _spe_e_session.frequency[key] = arr[key];
                }
            });

        }
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }

    // update promotion
    if (data.type === 'spe_promotion') {
        try {
            _spe_e_session = JSON.parse(_session_data);
        } catch (err) {
            console.log(err);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            })
        }
        if (data.subtype === 'promotion') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });
            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {
                // if property exists, update.
                if (_spe_e_session.promotion.hasOwnProperty(key)) {
                    _spe_e_session.promotion[key] = arr[key];
                }
            });

        }
        // console.log(_spe_e_session.promotion);
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }

    // update subscription_card
    if (data.type === 'spe_subscription_card') {
        try {
            _spe_e_session = JSON.parse(_session_data);
        } catch (err) {
            console.log(err);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            })
        }
        if (data.subtype === 'subscription_card') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });
            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {
                // if property exists, update.
                if (_spe_e_session.subscription_card.hasOwnProperty(key)) {
                    _spe_e_session.subscription_card[key] = arr[key];
                }
            });

        }
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }

    // update address
    if (data.type === 'spe_address') {
        try {
            _spe_e_session = JSON.parse(_session_data);
        } catch (err) {
            console.log(err);
            Swal.fire(
                '',
                '',
                'error'
            ).then((result) => {
                window.location.reload();
            })
        }
        if (data.subtype === 'shipping_address') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.address.shipping.hasOwnProperty(key)) {
                    _spe_e_session.address.shipping[key] = arr[key];
                }
            });
        }
        if (data.subtype === 'billing_address') {
            let arr = [];
            Object.keys(data.updates).forEach((key) => {
                arr[key] = data.updates[key];
            });

            // for each object inside data.updates, update existing session data with the same key.
            Object.keys(arr).forEach(function (key) {

                // if property exists, update.
                if (_spe_e_session.address.billing.hasOwnProperty(key)) {
                    _spe_e_session.address.billing[key] = arr[key];
                }
            });
        }
        window.session(SESSION_EDIT_SHAVE_PLANS, SESSION_SET, JSON.stringify(_spe_e_session)).done(function () {
            currentSession();
        });
    }
}

//CREDIT CARD INPUT MASK
function CreditCardFormatMasked(value) {
    var v_masked = value.replace(/\s/g, '').replace(/.(?!$)/gi, '•');
    var matches_masked = v_masked.match(/.{4,16}/g);
    var match_masked = matches_masked && matches_masked[0] || '';
    var parts_masked = [];

    for (i = 0, len = match_masked.length; i < len; i += 4) {
        parts_masked.push(match_masked.substring(i, i + 4));
    }

    if (parts_masked.length) {
        return parts_masked.join(' ');
    } else {
        return v_masked;
    }
}

function CreditCardMaskAll(value) {
    var v_masked = value.replace(/\s/g, '').replace(/./gi, '•');
    var matches_masked = v_masked.match(/.{4,16}/g);
    var match_masked = matches_masked && matches_masked[0] || '';
    var parts_masked = [];

    for (i = 0, len = match_masked.length; i < len; i += 4) {
        parts_masked.push(match_masked.substring(i, i + 4));
    }

    if (parts_masked.length) {
        return parts_masked.join(' ');
    } else {
        return v_masked;
    }
}

function CreditCardFormat(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');

    var matches = v.match(/.{4,16}/g);
    var match = matches && matches[0] || '';
    var parts = [];

    for (i = 0, len = match.length; i < len; i += 4) {
        parts.push(match.substring(i, i + 4));
    }

    if (parts.length) {
        return parts.join('');
    } else {
        return v;
    }
}

//EXPIRY DATE INPUT MASK
function ExpiryDateFormat(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    if (country_iso == 'kr') {
        var matches = v.match(/\d{2,6}/g);
    } else {
        var matches = v.match(/\d{2,4}/g);
    }
    var match = matches && matches[0] || ''
    var parts = []
    var ExpiryDateFormatCount = 0;
    if (country_iso == 'kr') {
        for (i = 0, len = match.length; i < len; i += 2) {
            if (ExpiryDateFormatCount == 0) {
                parts.push(match.substring(i, i + 2))
            } else {
                parts.push(match.substring(i, i + 4))
                i = i + 2;
            }
            ExpiryDateFormatCount++;
        }
    }
    else {
        for (i = 0, len = match.length; i < len; i += 2) {
            parts.push(match.substring(i, i + 2))
        }
    }

    if (parts.length) {
        return parts.join(' / ')
    } else {
        return value
    }
}

$(function () {

    $("#form_spe_shipping,#spd_e_ssn,#spd_e_address,#spd_e_city,#spd_e_portalCode,#spd_e_state").on('change', function () {
        pre_editAddresses('shipping');
    });

    $("#form_spe_billing").on('change', function () {
        pre_editAddresses('billing');
    });

    $('#spc_e_cardNumber_masked').blur(function (event) {
        // deselectd control
        $(this).val(CreditCardMaskAll($(this).val()));
    });

    $('#spc_e_cardNumber_masked').keydown(function (event) {
        $(this).val(CreditCardFormat($('#spc_e_cardNumber').val()));
    });

    $('#spc_e_cardNumber_masked').on('input', function (event) {
        $('#spc_e_cardNumber').val(CreditCardFormat($(this).val()));
        $(this).val(CreditCardFormatMasked($('#spc_e_cardNumber').val()));
    });

    $('#spc_e_expirydate').keyup(function () {
        $(this).val(ExpiryDateFormat($(this).val()));
    })

    $("#spc_e_expirydate").click(function () {
        $("#spc_e_expirydate_error").hide();
    });

    $("#spc_e_cardNumber").click(function () {
        $("#spc_e_cardNumber_error").hide();
    });

    $("#spc_e_ccv").click(function () {
        $("#spc_e_ccv_error").hide();
    });

    $("#spc_e_expirydate,#spc_e_cardNumber_masked,#spc_e_ccv,#spc_e_card_birth,#spc_e_card_password").on('change', function () {
        spe_card_actions();
    })
    //ENABLE NUMBER ONLY

    $(".number-only").on('input', function (event) {
        if ((value.which < 48 || value.which > 57) && (value.which !== 8) && (value.which !== 0)) {
            return false;
        }
        return true;
    });

    $("#applied_promo").on('change', function () {
        if ($("#applied_promo option:selected").val() === "cancellation") {
            pre_selectPromoToBeApplied(null, 'cancellation');
        } else {
            let promo_code = $("#applied_promo option:selected").text();
            checkPromotion(promo_code, _session_data, spe_data, 'onchange');
        }
    });

    $("#form-spe-frequency").change(function () {
        $("#spf_e_frequency_error").hide();
        let selected_duration = $("#spf_e_frequency").val();
        pre_updateFrequency(selected_duration);
    });

    // On Load
    pre_defaultSubsProducts();
    // pre_defaultSubsCard();
    // pre_defaultAddress_shipping();
    // pre_defaultAddress_billing();
    // ----------------------------------------

    //On select product
    for (let i = 0; i < 2; i++) {
        $("#product-" + i).click(SelectHandle(i));
    }

    //On select months pause
    for (let i = 1; i < 4; i++) {
        $("#months-" + i).click(SelectMonths(i));
    }

    //On select reason
    for (let i = 1; i < 9; i++) {
        $("#cancel-reason-" + i).click(SelectReason(i));
    }

    // On click cancel plan
    $(".start-cancellation-journey").click(function () {
        $("#loading").css("display", "block");
        $("#cancellationJourneyModalContent").load(window.location.origin + GLOBAL_URL + "/cancellation-journey/navigate/" + spe_data.subscription.id + "/-1/root", function () {
            setTimeout(function () {
                $("#loading").css("display", "none");
                $("#cancellationJourneyModal").modal("show");
            }, 1000);
        });
    });

    // On click don't cancel
    $("#stop-cancellation-journey").click(function () {
        setTimeout(function () {
            $("#pausePlan").modal("show");
        }, 500);

    });
})

$(document).ready(function () {
    // console.log("diff billing addr check is " + this.is(':checked'));
    if (document.getElementById('diff_billingaddress').checked) {
        $("#billing-address-toggle").removeClass("d-none");
        $("#billing-address-placeholder").addClass("d-none");
        $('#combined-address').addClass('d-none');
        $('#separate-address').removeClass('d-none');
    }
    else {
        $("#billing-address-toggle").addClass("d-none");
        $("#billing-address-placeholder").removeClass("d-none");
        $("#shipping-address-toggle").click();
        $('#separate-address').addClass('d-none');
        $('#combined-address').removeClass('d-none');
    }
    $("#diff_billingaddress").change(function () {
        if (document.getElementById('diff_billingaddress').checked) {
            $("#billing-address-toggle").removeClass("d-none");
            $("#billing-address-placeholder").addClass("d-none");
            $('#combined-address').addClass('d-none');
            $('#separate-address').removeClass('d-none');
        }
        else {
            $("#billing-address-toggle").addClass("d-none");
            $("#billing-address-placeholder").removeClass("d-none");
            $("#shipping-address-toggle").click();
            $('#separate-address').addClass('d-none');
            $('#combined-address').removeClass('d-none');
        }
    });
})

