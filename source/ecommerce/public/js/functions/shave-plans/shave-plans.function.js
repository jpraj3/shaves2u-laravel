// Function: Select product
function SelectTrialKit(productid) {
    return function () {
        if ($("#trial_kit").hasClass('item-unselected')) {
            $("#trial_kit").removeClass('item-unselected').addClass('item-selected');
            $("#trial_kit_check").prop( "checked", true );
            $("#blade_refill").removeClass('item-selected').addClass('item-unselected');
            $("#blade_refill_check").prop( "checked", false );
        }
    };
};

function SelectBladeRefill(productid) {
    return function () {
        if ($("#blade_refill").hasClass('item-unselected')) {
            $("#blade_refill").removeClass('item-unselected').addClass('item-selected');
            $("#blade_refill_check").prop( "checked", true );
            $("#trial_kit").removeClass('item-selected').addClass('item-unselected');
            $("#trial_kit_check").prop( "checked", false );
        }
    };
};

$(function () {
    //On select Trial kit
    $("#trial_kit").click(SelectTrialKit());
    //$("#trial_kit_check").change(SelectTrialKit());
    $("#blade_refill").click(SelectBladeRefill());
    //$("#blade_refill_check").change(SelectBladeRefill());

    $("#blade_refill").click();
})
