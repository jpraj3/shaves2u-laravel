let saved_session_data = {};
let current_step = 1;
let productid;
let productcountryId;
let quantity;
let journey_type;
let current_session_data;
let _selection = [];
let _reinitialize_selection = [];
let _reinitialize_checkout = [];
let session_data_for_selection = {};
let session_data_for_checkout = {};
let isfirstclick_selection = false;
let isfirstclick_checkout = false;
let get_selection_session;
let get_checkout_session;
let payment_intent_next_update_type;
let total_price = 0.00;
let getloginsession;
let maskpcidselection = "";
//elements for checkout-ask
let element_collapse_1 = document.getElementById("collapse1");
let element_collapse_2 = document.getElementById("collapse2");
let element_collapse_3 = document.getElementById("collapse3");
let element_collapse_4 = document.getElementById("collapse4");
let element_main_1 = document.getElementById("account-body");
let element_main_2 = document.getElementById("shipping-address-body");
let element_main_3 = document.getElementById("payment-body");
let element_main_4 = document.getElementById('account-header');
let element_main_5 = document.getElementById("shipping-address-header");
let element_main_6 = document.getElementById("payment-header");
let element_1 = document.getElementById("shipping-address-show");
let element_2 = document.getElementById("shipping-address-edit");
let element_3 = document.getElementById("shipping-address-add");
let element_4 = document.getElementById("enable-address-edit");
let element_5 = document.getElementById("enable-address-add");
let element_6 = document.getElementById("cancel-address-edit");
let element_7 = document.getElementById("billing-address-block_for_edit");
let element_7_1 = document.getElementById("billing-address-block_for_add");
let element_8 = document.getElementById("show_card_details_container");
let element_9 = document.getElementById("card_selection_list");
let element_10 = document.getElementById("add_additional_card");
let element_11 = document.getElementById("enableBillingEditCheckBox_for_edit");
let element_11_1 = document.getElementById("enableBillingEditCheckBox_for_add");
let element_12 = document.getElementById("delivery_address_id");
let element_13 = document.getElementById("billing_address_id");
let element_14 = document.getElementById("payment_intent_id");
let element_15 = document.getElementById("payment_intent_next_update_type");
let element_16 = document.getElementById("appendAddress");
let element_17 = document.getElementById("product_country_id");
let element_18 = document.getElementById("appendCard");
let element_19 = document.getElementById("appendCardList");
let element_20 = document.getElementById("add-card");
let element_21 = document.getElementById("new_user_register");
let element_22 = document.getElementById("sign_in_sign_up_email");
let element_23 = document.getElementById("sign_in_sign_up_password");
let element_24 = document.getElementById("new_user_login");
let element_25 = document.getElementById("sign_in_sign_up_email_2");
let element_26 = document.getElementById("sign_in_sign_up_password_2");
let element_27 = document.getElementById("day-of-birth");
let element_28 = document.getElementById("month-of-birth");
let element_29 = document.getElementById("year-of-birth");
let element_30 = document.getElementById("register_name");

//errors
let error_element_1 = document.getElementById("error_login");
let error_element_2 = document.getElementById("error_register");

//input for add & edit addresses
let add_delivery_name = document.getElementById("add_delivery_name");
let add_delivery_address = document.getElementById("add_delivery_address");
let add_delivery_city = document.getElementById("add_delivery_city");
let add_delivery_state = document.getElementById("add_delivery_state");
let add_delivery_postcode = document.getElementById("add_delivery_postcode");
let add_delivery_phone = document.getElementById("add_delivery_phone");
let add_delivery_phoneext = document.getElementById("add_delivery_phoneext");

let add_billing_address = document.getElementById("add_billing_address");
let add_billing_city = document.getElementById("add_billing_city");
let add_billing_state = document.getElementById("add_billing_state");
let add_billing_postcode = document.getElementById("add_billing_postcode");
let add_billing_phone = document.getElementById("add_billing_phone");
let add_billing_phoneext = document.getElementById("add_billing_phoneext");

let edit_delivery_name = document.getElementById("edit_delivery_name");
let edit_delivery_address = document.getElementById("edit_delivery_address");
let edit_delivery_city = document.getElementById("edit_delivery_city");
let edit_delivery_state = document.getElementById("edit_delivery_state");
let edit_delivery_postcode = document.getElementById("edit_delivery_postcode");
let edit_delivery_phone = document.getElementById("edit_delivery_phone");
let edit_delivery_phoneext = document.getElementById("edit_delivery_phoneext");

let edit_billing_address = document.getElementById("edit_billing_address");
let edit_billing_city = document.getElementById("edit_billing_city");
let edit_billing_state = document.getElementById("edit_billing_state");
let edit_billing_postcode = document.getElementById("edit_billing_postcode");
let edit_billing_phone = document.getElementById("edit_billing_phone");
let edit_billing_phoneext = document.getElementById("edit_billing_phoneext");

// korean address -------------
let add_delivery_name1 = document.getElementById("add_delivery_name1");
let add_delivery_address1 = document.getElementById("add_delivery_address1");
let add_delivery_city1 = document.getElementById("add_delivery_city1");
let add_delivery_state1 = document.getElementById("add_delivery_state1");
let add_delivery_postcode1 = document.getElementById("add_delivery_postcode1");
let add_delivery_phone1 = document.getElementById("add_delivery_phone1");
let add_delivery_phoneext1 = document.getElementById("add_delivery_phoneext1");

let add_billing_address1 = document.getElementById("add_billing_address1");
let add_billing_city1 = document.getElementById("add_billing_city1");
let add_billing_state1 = document.getElementById("add_billing_state1");
let add_billing_postcode1 = document.getElementById("add_billing_postcode1");
let add_billing_phone1 = document.getElementById("add_billing_phone1");
let add_billing_phoneext1 = document.getElementById("add_billing_phoneext1");

let edit_delivery_name1 = document.getElementById("edit_delivery_name1");
let edit_delivery_address1 = document.getElementById("edit_delivery_address1");
let edit_delivery_city1 = document.getElementById("edit_delivery_city1");
let edit_delivery_state1 = document.getElementById("edit_delivery_state1");
let edit_delivery_postcode1 = document.getElementById("edit_delivery_postcode1");
let edit_delivery_phone1 = document.getElementById("edit_delivery_phone1");
let edit_delivery_phoneext1 = document.getElementById("edit_delivery_phoneext1");

let edit_billing_address1 = document.getElementById("edit_billing_address1");
let edit_billing_city1 = document.getElementById("edit_billing_city1");
let edit_billing_state1 = document.getElementById("edit_billing_state1");
let edit_billing_postcode1 = document.getElementById("edit_billing_postcode1");
let edit_billing_phone1 = document.getElementById("edit_billing_phone1");
let edit_billing_phoneext1 = document.getElementById("edit_billing_phoneext1");
let firstpromoapply = 0;

function checkout_login() {
    element_24.removeAttribute("hidden");
    element_21.setAttribute("hidden", "");
}
function checkout_register() {
    element_21.removeAttribute("hidden");
    element_24.setAttribute("hidden", "");
}


function checkSessionPromo() {
    session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            if (JSON.parse(data).checkout) {
                checkoutpromo = JSON.parse(data).checkout.promo;
                if (!$.isEmptyObject(checkoutpromo)) {
                    $("#promo_code").val(checkoutpromo.promo_code);
                    firstpromoapply = 1;
                    applyPromotion();
                }
            }

        }
    });
}

//Register functions
function onAccountFill_register() {
    let email = element_25.value;
    let password = element_26.value;
    let name = element_30.value;
    let birthday = '20' + element_29.value.padStart(2, '0') + '-' + element_28.value.padStart(2, '0') + '-' + element_27.value.padStart(2, '0');
    checkEmail(email, password, "register").then(status => {


        let user_data = status.user_data;
        let isActive = status.isActive;

        if (user_data === null) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: window.location.origin + GLOBAL_URL_V2 + '/api/user/register',
                method: "POST",
                cache: false,
                data: { password: password, email: email, name: name, birthday: birthday, countryid: countryid, langCode: langCode.toUpperCase() },
            }).done(function (response) {

                // location.reload();
            })
        } else {

            error_element_2.innerHTML = '<p class="hasError_msg">User exists. Unable to Register.</p>';
            error_element_2.removeAttribute("hidden");
        }
    });

}

//Login functions
function onAccountFill_login() {
    let email = element_22.value;
    let password = element_23.value;
    checkEmail(email, password, "login");
}

function checkEmail(email, password, type) {

    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: window.location.origin + GLOBAL_URL_V2 + '/api/check/email',
        method: "POST",
        cache: false,
        data: { password: password, email: email, type: type },
    }).done(function (response) {
        if (type === "login") {
            if (response.email === email) {
                location.reload();
            } else {
                error_element_1.innerHTML = '<p class="hasError_msg">User not found. Email or Password incorrect.</p>';
                error_element_1.removeAttribute("hidden");
            }
        }

        if (type === "register") {

            if (response["user_data"] === null) {
                return 0;
            } else {
                // error_element_2.innerHTML = '<p class="hasError_msg">User exists. Unable to Register.</p>';
                // error_element_2.removeAttribute("hidden");
                return 1;
            }
        }
    })
}

function onUpdateCard(data) {
    let x =
        '<div class="form-group">' +
        '<label for="">Card Name</label>' +
        '<p> ' + data.cardName + '</p>' +
        '</div>' +
        '<div class="form-group">' +
        '<label for="">Card Number</label>' +
        '<p> XXXX XXXX XXXX ' + data.cardNumber + '</p>' +
        '</div>' +
        '<div class="form-group>' +
        ' <label for="">Exp Month/Year</label>' +
        ' <p> 0' + data.expiredMonth + ' / ' + data.expiredYear + '</p>' +
        '</div>' +
        '<br><br>' +
        '<div class="form-group" id="add-card" style="text-align:center;">' +
        '<button type="submit" class="btn btn-primary" onclick="editCard()">Edit Card</button>' +
        '</div>';

    if (document.body.contains(element_8)) {
        element_18.innerHTML = '';
        element_18.innerHTML = x;
        if (element_8.offsetParent !== null) {
            element_8.classList.add("show");
            element_9.classList.remove("show");
            element_10.classList.remove("show");
        } else {
            element_8.classList.remove("show");
            element_9.classList.remove("show");
            element_10.classList.add("show");
        }
    }
}

async function updateCardSelectionList(user_id) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/stripe/card',
        method: "POST",
        cache: false,
        data: { "id": user_id },
    }).done(function (response) {

        if (response) {
            if (document.body.contains(element_8)) {
                element_19.innerHTML = '';
                response.forEach(function (el) {

                    elChild = document.createElement('div');
                    let x =
                        '<div class="container col-12 individual-card-selection" style="border: 2px solid black; margin:1%;padding:10px;cursor: pointer;" onClick="onUpdateCardSelection(' + el.id + ')">' +
                        '<div class="form-group">' +
                        '<label for="">Card Name</label>' +
                        '<p> ' + el.cardName + '</p>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="">Card Number</label>' +
                        '<p> XXXX XXXX XXXX ' + el.cardNumber + '</p>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label for="">Card Number</label>' +
                        '<p> ' + el.expiredMonth + ' / ' + el.expiredYear + '</p>' +
                        '</div>' +
                        '</div>';
                    elChild.innerHTML = x;
                    element_19.appendChild(elChild);
                });
            }
        } else {
            return card_list = 0;
        }
    }).fail(function (jqXHR, textStatus, error) {
        if (error) {
            console.log(error);
            return card_list = 0;
        }
    });
}


// on proceed payment button
function onProceedPayment(session_data, payment_intent, data) {
    $("#loading").css("display", "block");

    // Check for origin and current country before payment proceeding
    let isSameCountry = false;
    let isUserCountry = null;
    let isCurrentCountry = null;
    let isUserCountryInfo = null;
    if (user) {
        isUserCountry = user.CountryId;
        $.ajax({
            url: GLOBAL_URL_V2 + '/api/check/country-list',
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            method: "GET",
            cache: false,
        }).done(function (data) {
            data.forEach(function (country) {
                if (country.id === user.CountryId) {
                    isUserCountryInfo = country;
                }
            });
        });
    }
    if (countryid) { isCurrentCountry = countryid; }
    if (isCurrentCountry === isUserCountry) { isSameCountry = true; }
    if (!isSameCountry) {
        // User is attempting to purchase while not originating from same source country
        $("#loading").css("display", "none");
        //show popup
        Swal.fire({
            html: trans('website_contents.global.not_country_origin_purchase', {}),
            width: '40%',
            padding: '3em',
            heightAuto: false,
            backdrop: true,
            background: 'white',
            position: 'left',
            allowOutsideClick: true,
            allowEscapeKey: true,
            allowEnterKey: true,
            showCloseButton: true,
            showCancelButton: false,
            showConfirmButton: false,
            focusConfirm: false,
        }).then(() => {
            window.location.replace(window.location.origin + GLOBAL_URL_V3 + user.defaultLanguage.toLowerCase() + '-' + isUserCountryInfo.codeIso.toLowerCase());
        })
    } else {
        $("#loading").css("display", "block");
        var type = "awesome-shave-kits";
        let payment_intent_id = payment_intent.id;
        let card_id = payment_intent.payment_method;
        let CONFIRM_PURCHASE_URL =
            window.location.origin +
            GLOBAL_URL +
            "/ask/checkout/confirm-purchase";
        let OTP_URL = window.location.origin + GLOBAL_URL + "/stripe/confirm";
        let RETURN_URL_WITH_OTP =
            window.location.origin +
            GLOBAL_URL +
            `/stripe/redirect?type=${type}&card_id=${card_id}&otp=1`;
        let RETURN_URL_WITHOUT_OTP =
            window.location.origin +
            GLOBAL_URL +
            `/stripe/redirect?type=${type}&card_id=${card_id}&otp=0&payment_intent=${payment_intent_id}`;

        $.ajax({
            url: CONFIRM_PURCHASE_URL,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            method: "POST",
            cache: false,
            data: {
                "type": type,
                "payment_intent": payment_intent_id
            }
        })
            .done(function (data) {
                //If success creating subscription, orders and receipt, proceed to stripe payment
                $.ajax({
                    url: OTP_URL,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    method: "POST",
                    cache: false,
                    data: {
                        payment_intent_id: payment_intent_id,
                        return_url: RETURN_URL_WITH_OTP
                    }
                })
                    .done(function (data) {
                        if (data.next_action != null) {
                            if (data.status == "requires_source_action" || data.status == "requires_action") {
                                // If got OTP
                                window.location = data.next_action.redirect_to_url.url;
                            } else {
                                // If no OTP
                                window.location = RETURN_URL_WITHOUT_OTP;
                            }
                        } else {
                            // If no OTP
                            window.location = RETURN_URL_WITHOUT_OTP;
                        }
                        // $("#loading").css("display", "none");
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        // $("#loading").css("display", "none");
                        window.location = RETURN_URL_WITHOUT_OTP;
                    });
            })
            .fail(function (jqXHR, textStatus, error) {
                $("#loading").css("display", "none");
                console.log("Failed to confirm purchase.");
            });
    }
}

// on product selection -> insert product_country_id into input
function onSelect(productcountryId, productname, el) {
    journey_type = 'selection';
    element_17.value = productcountryId;
    quantity = 1;
    product_data = {
        "productcountryId": productcountryId,
        "productname": productname,
        "quantity": quantity,
        "journey_type": journey_type,
        "product_type": "awesome-shave-kits"
    };
    UpdateSessionData(product_data);
}

// Function: Get session data
function GetSessionData() {
    session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            return data;
        } else {
        }
    });

    session(SESSION_SELECTION_ASK, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            get_selection_session = data;
            return data;
        } else {
        }
    });
}

// Function: Get session data
function GetSessionDataV2() {
    let sessionABC = [];
    session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            sessionABC[0] = get_checkout_session;
        } else {
        }
    });

    session(SESSION_SELECTION_ASK, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            get_selection_session = data;
            sessionABC[1] = get_selection_session;
        } else {
        }
    });

    return sessionABC;

}


function CheckLoginSessionData() {
    let sessionABC = [];
    session(SESSION_LOGIN, SESSION_GET, null).done(function (data) {
        // Do something with the session data
        if (data) {
            get_checkout_session = data;
            sessionABC[0] = get_checkout_session;
        } else {
        }
        getloginsession = sessionABC;
        return sessionABC;
    });


}

function onUpdateCardSelection(card_id) {
    $("#loading").css("display", "block");
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/stripe/card/each',
        method: "POST",
        cache: false,
        data: { "id": card_id },
    }).done(function (response) {

        if (response) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: GLOBAL_URL + '/stripe/card/select',
                method: "POST",
                cache: false,
                data: { "id": card_id, "type": "awesome-shave-kits", "payment_intent": payment_intent.id },
            }).done(function (response2) {

                payment_intent.id = response2.id;

                checkout_data_card = {
                    "journey_type": 'checkout',
                    "update_type": "selected_card",
                    "card_id": card_id,
                    "card": response,
                };
                onUpdateCard(response);
                UpdateSessionData(checkout_data_card);
                CardBack();

                // update new card last 4 digits
                let last4digits = response.cardNumber ? response.cardNumber : '1234';
                $(".cc_last4digits").html(last4digits);
                let expirymy = response.expiredMonth && response.expiredYear ? response.expiredMonth + '/' + response.expiredYear.slice(-2) : 'xx/xx';
                $(".cc_expmy").html(expirymy);
                ChangeCardBackground(response.branchName);
            });
        } else {
            return 0;
        }
    }).fail(function (jqXHR, textStatus, error) {
        $("#loading").css("display", "none");
        if (error) {
            console.log(error);
            return 0;
        }
    });

}

function onAddCard(card_id, card, payment_intent_id) {

    checkout_data_card = {
        "journey_type": 'checkout',
        "update_type": "selected_card",
        "card_id": card_id,
        "card": card,
        "payment_intent_id": payment_intent_id
    };
    UpdateSessionData(checkout_data_card);
    CardBack();
}

function onUpdateDeliverySelection(addressUpdates, type) {
    if (type === "default") {
        let checkout_data_address = {
            "journey_type": 'checkout',
            "update_type": "selected_address",
            "delivery_address_id": addressUpdates.selected_delivery_address_id ? addressUpdates.selected_delivery_address_id : null,
            "billing_address_id": addressUpdates.selected_billing_address_id ? addressUpdates.selected_billing_address_id : null
        };
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
    if (type === "update") {
        let checkout_data_address = {
            "journey_type": 'checkout',
            "update_type": "selected_address",
            "delivery_address_id": addressUpdates.selected_delivery_address_id ? addressUpdates.selected_delivery_address_id : null,
            "billing_address_id": addressUpdates.selected_billing_address_id ? addressUpdates.selected_billing_address_id : null
        };
        UpdateSessionData(checkout_data_address);
        $("#shipping-address-add").hide();
        $("#shipping-address-edit").show();
        AddressBack(type);
    }
    if (type === "address_update_only") {
        let checkout_data_address = {}
        if (isfirstclick_checkout === false) {
            let xe1 = checkout_details ? checkout_details : '';
            let xf1 = payment_intent ? payment_intent : '';
            let xg1 = user_id ? user_id : '';
            let xh1 = country_id ? country_id : '';
            let xi1 = user ? user : '';
            let xj1 = delivery_address ? delivery_address : '';
            let xk1 = billing_address ? billing_address : '';
            let xl1 = default_card ? default_card : '';
            let xm1 = session_data ? session_data : '';

            checkout_data_address = {
                "checkout_details": xe1,
                "payment_intents": xf1,
                "user_id": xg1,
                "country_id": xh1,
                "user": xi1,
                "delivery_address": xj1,
                "billing_address": xk1,
                "default_card": xl1,
                "session_data": xm1,
                "journey_type": "checkout",
                "update_type": "default"
            };
        } else {
            checkout_data_address = {
                "journey_type": 'checkout',
                "update_type": "selected_address",
                "delivery_address_id": addressUpdates.selected_delivery_address_id ? addressUpdates.selected_delivery_address_id : null,
                "billing_address_id": addressUpdates.selected_billing_address_id ? addressUpdates.selected_billing_address_id : null
            };
        }
        UpdateSessionData(checkout_data_address);
        AddressBack(type);
    }
}

// Function: Update payment intent
function onUpdatePaymentIntent(payment_intent_data, next_update_type) {
    if (next_update_type === "first-update") {
        payment_intent_next_update_type = "first-update";
    } else if (next_update_type === "price-update") {
        payment_intent_next_update_type = "price-update";
    } else if (next_update_type === "card-update") {
        payment_intent_next_update_type = "card-update";
    }

    return payment_intent_next_update_type;
}

// Function: Update session data
function UpdateSessionData(data) {
    // session selection
    let _session_selection = {};
    let _session_selection_step1_add_products = {};

    // session checkout
    let _session_checkout = {};
    let _session_checkout_user = {};
    let _session_checkout_promo = {};
    let _session_checkout_selected_card = {};
    let _session_checkout_selected_delivery_address = {};

    var journey_type = data.journey_type;
    if (journey_type === 'selection') {
        var product_country_id = data.productcountryId.toString();
        var productname = data.productname.toString();
        var quantity = data.quantity;
        var product_type = data.product_type;

        if (!isfirstclick_selection) {
            isfirstclick_selection = true;
            _session_selection = {
                "selection": {
                    "step1": [{
                        "productcountryid": product_country_id,
                        "productname": productname,
                        "quantity": quantity
                    }],
                    "current_step": { current_step },
                    "journey_type": { journey_type },
                    "product_type": { product_type },
                },
            };
            session_data_for_selection = _session_selection;
        }
        else {
            _session_selection = {
                "selection": {
                    "step1": [{
                        "productcountryid": product_country_id,
                        "productname": productname,
                        "quantity": quantity
                    }],
                    "current_step": { current_step },
                    "journey_type": { journey_type },
                    "product_type": { product_type },
                },
            };
            session_data_for_selection = _session_selection;
            // _session_selection_step1_add_products = {
            //     "productcountryid": product_country_id,
            //     "productname": productname,
            //     "quantity": quantity
            // };
            // session_data_for_selection.selection.step1.push(_session_selection_step1_add_products);
        }

        return session(SESSION_SELECTION_ASK, SESSION_SET, JSON.stringify(session_data_for_selection)).done(function () {
            currentSession();
            var loading = document.getElementById("loading");
            if (loading) {
                loading.style.display = "none";
            }
            return true;
        });
    } else if (journey_type === 'checkout') {
        if (isfirstclick_checkout === false) {

            isfirstclick_checkout = true;
            _session_checkout = {
                "checkout": {
                    "user": {},
                    "selected_card": {
                        "id": '',
                        "risk_level": '',
                        "payment_intent_id": '',
                        "type": '',
                        "customer_id": '',
                    },
                    "selected_address": {},
                    "promo": {},
                    "payment_data": {}
                }
            }
            session_data_for_checkout = _session_checkout;

            // mask selection
            session_data_for_checkout.checkout.free_mask = {};
            // mask selection

            if (data.update_type === 'default') {
                if (data.user && data.user.length !== 0) {
                    _session_checkout_user = {
                        "id": data.user.id,
                        "email": data.user.email,
                        "defaultLanguage": data.user.defaultLanguage,
                        "IsActive": data.user.isActive,
                        "CountryId": data.user.CountryId
                    };
                    session_data_for_checkout.checkout.user = _session_checkout_user;
                }


                if (data.promo && data.promo.length !== 0) {
                    // _session_checkout_promo = { "promo": data.promo };
                    session_data_for_checkout.checkout.promo = _session_checkout_promo;
                }


                if (data.delivery_address && data.delivery_address.length !== 0) {
                    _session_checkout_selected_delivery_address = {
                        "delivery_address": data.delivery_address ? data.delivery_address[0].id : '',
                        "billing_address": data.billing_address ? data.billing_address[0].id : ''
                    };
                    session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                }


                if (data.default_card && data.default_card.length !== 0) {
                    _session_checkout_selected_card = {
                        "id": data.default_card[0] ? data.default_card[0].id : null,
                        "payment_intent_id": data.payment_intents ? data.payment_intents.id : null,
                        "customer_id": data.default_card[0] ? data.default_card[0].customerId : null,
                        "type": 'awesome-shave-kits',
                        "risk_level": data.default_card[0] ? data.default_card[0].risk_level : null,
                    };
                    session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
                }
                

                if (data.checkout_details.length !== 0) {
                    _session_checkout_payment_data = data.checkout_details;
                    session_data_for_checkout.checkout.payment_data = _session_checkout_payment_data;
                }

            }

            return session(SESSION_CHECKOUT_ASK, SESSION_SET, JSON.stringify(session_data_for_checkout)).done(function () {
                $("#loading").css("display", "none");
                currentSession();
                return true;
            });
        } else {

            return session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (response) {
                // Do something with the session data
                if (response) {
                    get_checkout_session = response;
                    session_data_for_checkout = JSON.parse(response);
                    if (data.update_type === 'selected_user') {
                        _session_checkout_user = { "test": "test" };
                        session_data_for_checkout.checkout.user.push(_session_checkout_user);
                    }
                    if (data.update_type === 'selected_promo') {
                        if (data.status === "error") {
                            _session_checkout_promo = {};
                        } else {
                            _session_checkout_promo = { "promo_id": data.data.pid, "promo_code": data.data.code, "promo_isGeneric": data.data.isGeneric, "promo_discount": data.data.discount, "promo_free_product_id": data.data.freeProductCountryIds, "promo_free_exist_product_id": data.data.freeExistProductCountryIds, "promo_ablefreeexistproduct": data.ablefreeexistproduct, "promo_freeexistresultproduct": data.freeexistresultproduct, "promo_freeexistresultproductprice": data.freeexistresultproductprice, "promo_product_bundle_id": data.data.ProductBundleId, "promo_promotionType": data.data.promotionType, "promo_planIds": data.data.planIds, "promo_minSpend": data.data.minSpend, "promo_maxDiscount": data.data.maxDiscount, "promo_isFreeShipping": data.data.isFreeShipping, "promo_timePerUser": data.data.timePerUser, "promo_total_price": data.total, "promo_next_total_price": data.ntotal };
                        }
                        session_data_for_checkout.checkout.promo = _session_checkout_promo;
                    }
                    if (data.update_type === 'selected_address') {
                        _session_checkout_selected_delivery_address = {
                            "delivery_address": data.delivery_address_id,
                            "billing_address": data.billing_address_id
                        };
                        session_data_for_checkout.checkout.selected_address = _session_checkout_selected_delivery_address;
                    }
                    if (data.update_type === 'selected_card') {
                        _session_checkout_selected_card = {
                            "id": data.card.id,
                            "payment_intent_id": data.payment_intent_id,
                            "customer_id": data.card.customerId,
                            "type": data.card.payment_method,
                            "risk_level": data.card.risk_level,
                        };
                        session_data_for_checkout.checkout.selected_card = _session_checkout_selected_card;
                    }

                    return session(SESSION_CHECKOUT_ASK, SESSION_SET, JSON.stringify(session_data_for_checkout)).done(function () {
                        $("#loading").css("display", "none");
                        currentSession();
                        return true;
                    });
                } else {
                    return true;
                }
            });


        }
    }
}

function currentSession() {

    let result = GetSessionDataV2();
    if (!getloginsession) {
        CheckLoginSessionData();
    }

}

// Function: get session data for selection
function setSessionData_SELECTION() {
    session(SESSION_SELECTION_ASK, SESSION_SET, null).done(function () {
    });
}

// Function: get session data for checkout
function setSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_ASK, SESSION_SET, null).done(function () {
    });
}

// Function: get session data for selection
function getSessionData_SELECTION() {
    session(SESSION_SELECTION_ASK, SESSION_GET, null).done(function (response) {
    });
}

// Function: get session data for checkout
function getSessionData_CHECKOUT() {
    session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (response) {

    });
}

// Function: Clear session data
function ClearSessionData_SELECTION() {
    session(SESSION_SELECTION_ASK, SESSION_CLEAR, null).done(function () {
    });
}

// Function: Clear session data
function ClearSessionData_CHECKOUT() {
    return session(SESSION_CHECKOUT_ASK, SESSION_CLEAR, null).done(function (response) {
        if (response) {
            return response;
        }
    });
}

//Function: Update step
function UpdateStep(current_step) {
    if (current_step == 1) {
        $("#button-next").text(trans('website_contents.checkout.user.m_next_btn'));
    }
    else if (current_step == 2) {
        $("#button-next").text(trans('website_contents.checkout.address.m_next_btn'));
    }
    else if (current_step == 3) {
        $("#button-next").text(trans('website_contents.checkout.payment_method.m_next_btn'));
    }
    $(".step1-heading").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
    $(".step1-title").removeClass('panel-title-selected').addClass('panel-title-unselected');
    $(".step2-heading").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
    $(".step2-title").removeClass('panel-title-selected').addClass('panel-title-unselected');
    $(".step3-heading").removeClass('panel-heading-selected').addClass('panel-heading-unselected');
    $(".step3-title").removeClass('panel-title-selected').addClass('panel-title-unselected');
    $(".step" + current_step + '-heading').removeClass('panel-heading-unselected').addClass('panel-heading-selected');
    $(".step" + current_step + '-title').removeClass('panel-title-unselected').addClass('panel-title-selected');
}

function steps() {
    //On click next
    $("#button-next").click(function () {
        if (current_step == 4) {
            //do nothing
        } else {
            current_step++;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            if (current_step == 2) {
                if (user) {
                    shippingaddress();
                    UpdateStep(current_step);
                } else {
                    validateRegistration("form_register");
                    if ($('#form_register').valid() === true) {
                        location.href = "#btn-create-account-desktop";
                        current_step--;
                    } else {
                        current_step--;
                    }
                }
            }
            if (country_id == 'kr') {
                if (current_step == 3) {

                    paymentSummary();
                    UpdateStep(current_step);
                    $("#button-next").addClass('display-none');
                    $("#form-pay").removeClass('hidden');
                    $("#form-pay-kr").removeClass('hidden');

                }
            }
            else {
                if (current_step == 3) {
                    if (window.getComputedStyle(document.getElementById('shipping-address-add')).display == "block") {
                        v_checkout_address("add-addresses-block");
                        if ($('#add-addresses-block').valid() === true) {
                            location.href = "#btn-address-add";
                            current_step--;
                        } else {
                            current_step--;
                        }
                    }
                    else {
                        payment();
                        UpdateStep(current_step);
                    }

                }
                if (current_step == 4) {
                    if (window.getComputedStyle(document.getElementById('add-card-container')).display == "block") {
                        v_checkout_cards("form_card");
                        if ($('#form_card').valid() === true) {
                            location.href = "#button-add-card";
                            current_step--;
                        }
                        else {
                            current_step--;
                        }
                    } else {
                        paymentSummary();
                        UpdateStep(current_step);
                        $("#button-next").addClass('display-none');
                        $("#form-pay").removeClass('hidden');
                        $("#form-pay-kr").removeClass('hidden');
                    }
                }
            }
            UpdateStep(current_step);
        }
    });

    //On click back
    $("#button-back").click(function () {
        if (current_step == 1) {
            let selection =
                window.location.origin +
                GLOBAL_URL +
                "/product/ask";
            window.location = selection;
        } else {
            current_step--;
            if (current_step == 1) {
                account();
                UpdateStep(current_step);
            }
            else if (current_step == 2) {
                shippingaddress();
                UpdateStep(current_step);
            }
            else if (current_step == 3) {
                payment();
                UpdateStep(current_step);
            }
            else {

            }

        }
    });
}

// Begin: Functions related to shipping address
function account() {
    current_step = 1;
    $("#button-next").removeClass('display-none');
    //$("#form-pay").addClass('display-none');
    // if (country_id == 'kr') {
    //     let formpaykr = document.getElementById("form-pay-kr");
    //     if(formpaykr){
    //     formpaykr.style.display = "none";
    //     }
    // }
    if (element_collapse_1.offsetParent !== null) {
        element_collapse_2.classList.remove("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
        element_collapse_4.classList.remove("show");
    } else {
        element_collapse_1.classList.add("show");
        element_collapse_2.classList.remove("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
        element_collapse_4.classList.remove("show");
    }
}

function shippingaddress() {
    current_step = 2;
    $("#button-next").removeClass('display-none');
    //$("#form-pay").addClass('display-none');
    // if (country_id == 'kr') {
    //     let formpaykr = document.getElementById("form-pay-kr");
    //     if(formpaykr){
    //     formpaykr.style.display = "none";
    //     }
    // }
    if (element_collapse_2.offsetParent !== null) {
        element_collapse_1.classList.remove("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
        element_collapse_4.classList.remove("show");
    } else {
        element_collapse_1.classList.remove("show");
        element_collapse_2.classList.add("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
        element_collapse_4.classList.remove("show");
    }
}

function editAddress() {
    if (element_1.offsetParent !== null) {
        element_1.style.display = "none";

        element_2.style.display = "block";

        element_4.style.display = "none";
        element_5.style.display = "none";
        element_6.style.display = "block";
    } else {
        element_1.style.display = "block";

        element_2.style.display = "none";

        element_4.style.display = "block";
        element_5.style.display = "none";
        element_6.style.display = "none";
    }
}

function onAddDeliveryAddress(data) {
    edit_delivery_name.value = data.delivery_address_fullname;
    edit_delivery_address.value = data.delivery_address_address;
    edit_delivery_city.value = data.delivery_address_city;
    edit_delivery_state.value = data.delivery_address_state;
    if (data.delivery_address_portalCode != "00000") {
        edit_delivery_postcode.value = data.delivery_address_portalCode;
    } else {
        edit_delivery_postcode.value = null;
    }
    edit_delivery_phone.value = data.delivery_address_contactNumber.replace(("+" + phoneExt), "");
    edit_billing_address.value = data.billing_address_address;
    edit_billing_city.value = data.billing_address_city;
    edit_billing_state.value = data.billing_address_state;
    if (data.billing_address_portalCode != "00000") {
        edit_billing_postcode.value = data.billing_address_portalCode;
    } else {
        edit_billing_postcode.value = null;
    }
    edit_billing_phone.value = data.billing_address_contactNumber.replace(("+" + phoneExt), "");

    if (edit_delivery_address1) {
        edit_delivery_address1.value = data.delivery_address_address;
    }
    if (edit_delivery_city1) {
        edit_delivery_city1.value = data.delivery_address_city;
    }
    if (edit_delivery_state1) {
        edit_delivery_state1.value = data.delivery_address_state;
    }
    if (edit_delivery_postcode1) {
        if (data.delivery_address_portalCode != "00000") {
            edit_delivery_postcode1.value = data.delivery_address_portalCode;
        } else {
            edit_delivery_postcode1.value = null;
        }
    }
    if (edit_delivery_phone1) {
        edit_delivery_phone1.value = data.delivery_address_contactNumber.replace(("+" + phoneExt), "");
    }
    if (edit_billing_address1) {
        edit_billing_address1.value = data.billing_address_address;
    }
    if (edit_billing_city1) {
        edit_billing_city1.value = data.billing_address_city;
    }
    if (edit_billing_state1) {
        edit_billing_state1.value = data.billing_address_state;
    }
    if (edit_billing_postcode1) {
        if (data.billing_address_portalCode != "00000") {
            edit_billing_postcode1.value = data.billing_address_portalCode;
        } else {
            edit_billing_postcode1.value = null;
        }
    }
    if (edit_billing_phone1) {
        edit_billing_phone1.value = data.billing_address_contactNumber.replace(("+" + phoneExt), "");
    }
}

function cancelEditAddress() {
    if (element_4) {
        if (element_4.style.display === 'block') {
            if (element_1.offsetParent !== null) {
                element_1.style.display = "none";
                element_2.style.display = "block";
                element_3.style.display = "none";
                element_4.style.display = "none";
                element_5.style.display = "none";
                element_6.style.display = "block";
            } else {
                element_1.style.display = "block";
                element_2.style.display = "none";
                element_3.style.display = "none";
                element_4.style.display = "none";
                element_5.style.display = "none";
                element_6.style.display = "none";
            }
        } else {
            if (element_1.offsetParent !== null) {
                element_1.style.display = "none";
                element_2.style.display = "none";
                element_3.style.display = "block";
                element_5.style.display = "none";
                element_6.style.display = "block";
            } else {
                element_1.style.display = "block";
                element_2.style.display = "none";
                element_3.style.display = "none";
                element_4.style.display = "block";
                element_5.style.display = "block";
                element_6.style.display = "none";
            }
        }
    } else {
        if (element_1.offsetParent !== null) {
            element_1.style.display = "none";
            element_2.style.display = "block";
            element_3.style.display = "block";
            element_4.style.display = "block";
            element_5.style.display = "block";
            element_6.style.display = "block";
        } else {
            element_1.style.display = "block";
            element_2.style.display = "none";
            element_3.style.display = "block";
            element_4.style.display = "block";
            element_5.style.display = "block";
            element_6.style.display = "none";
        }
    }
}

function addAddress() {
    add_delivery_name.value = '';
    add_delivery_address.value = '';
    add_delivery_city.value = '';
    add_delivery_state.value = '';
    add_delivery_postcode.value = '';
    add_delivery_phone.value = '';
    add_billing_address.value = '';
    add_billing_city.value = '';
    add_billing_state.value = '';
    add_billing_postcode.value = '';
    add_billing_phone.value = '';

    if (element_3.offsetParent !== null) {
        element_3.style.display = "none";
        element_4.style.display = "block";
        element_5.style.display = "block";
        element_6.style.display = "none";
        element_1.style.display = "block";
    } else {
        element_3.style.display = "block";
        element_4.style.display = "none";
        element_5.style.display = "none";
        element_6.style.display = "block";
        element_1.style.display = "none";
    }
}


function enableBilling() {
    if (document.body.contains(element_11) && element_11.checked == true) {
        element_7.style.display = "block";
    } else if (document.body.contains(element_11) && element_11.checked == false) {
        element_7.style.display = "none";
    }

    if (document.body.contains(element_11_1) && element_11_1.checked == true) {
        element_7_1.style.display = "block";
    } else if (document.body.contains(element_11_1) && element_11.checked == false) {
        element_7_1.style.display = "none";
    }
}
// End: Functions related to shipping address

function payment() {
    current_step = 3;
    // if (country_id == 'kr') {
    //     let formpaykr = document.getElementById("form-pay-kr");
    //     if(formpaykr){
    //     formpaykr.style.display = "block";
    //     }
    // }
    if (country_id != 'kr') {
        if (element_collapse_3.offsetParent !== null) {
            element_collapse_1.classList.remove("show");
            element_collapse_2.classList.remove("show");
            element_collapse_4.classList.remove("show");
        } else {
            element_collapse_1.classList.remove("show");
            element_collapse_2.classList.remove("show");
            element_collapse_3.classList.add("show");
            element_collapse_4.classList.remove("show");
        }
    }
}

function paymentSummary() {
    current_step = 4;
    $("#button-next").addClass('display-none');
    $("#form-pay").removeClass('display-none');
    // if (country_id == 'kr') {
    //     let formpaykr = document.getElementById("form-pay-kr");
    //     if(formpaykr){
    //     formpaykr.style.display = "block";
    //     }
    // }
    if (element_collapse_4.offsetParent !== null) {
        element_collapse_1.classList.remove("show");
        element_collapse_2.classList.remove("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
    } else {
        element_collapse_1.classList.remove("show");
        element_collapse_2.classList.remove("show");
        if (country_id != 'kr') {
            element_collapse_3.classList.remove("show");
        }
        element_collapse_4.classList.add("show");
    }
}

//Card Functions
function editCard() {
    if (document.body.contains(element_8) && document.body.contains(element_9) && document.body.contains(element_10)) {
        if (element_8.offsetParent !== null) {
            element_8.style.display = "none";
            element_9 ? element_9.style.display = "block" : null;
            element_10 ? element_10.style.display = "none" : null;
        } else {
            element_8.style.display = "block";
            element_9 ? element_9.style.display = "none" : null;
            element_10 ? element_10.style.display = "none" : null;
        }
    } else if (document.body.contains(element_8_1) && document.body.contains(element_9_1) && document.body.contains(element_10_1)) {
        if (element_8_1.offsetParent !== null) {
            element_8_1.style.display = "block";
            element_9_1 ? element_9_1.style.display = "block" : null;
            element_10_1 ? element_10_1.style.display = "block" : null;
        } else {
            element_8_1.style.display = "block";
            element_9_1 ? element_9_1.style.display = "block" : null;
            element_10_1 ? element_10_1.style.display = "block" : null;
        }
    }
}

function selectionAddCard() {
    if (element_8.offsetParent !== null) {
        element_8.style.display = "none";
        element_9 ? element_9.style.display = "none" : null;
        element_10 ? element_10.style.display = "block" : null;
    } else {
        element_8.style.display = "none";
        element_9 ? element_9.style.display = "none" : null;
        element_10 ? element_10.style.display = "block" : null;
    }
}

function CardBack() {
    if (element_8) {
        if (element_8.offsetParent == null) {
            element_8.style.display = "block";
            element_9 ? element_9.style.display = "none" : null;
            element_10 ? element_10.style.display = "none" : null;
        }
    }
}

function AddressBack(type) {

    if (type === "default") {
        if (element_1) {
            if (element_1.offsetParent == null) {

                element_1.style.display = "block";
                element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "none" : null;
                element_4 ? element_4.style.display = "block" : null;
                element_5 ? element_5.style.display = "block" : null;
                element_6 ? element_6.style.display = "none" : null;
            }

            if (element_1.offsetParent != null) {

                element_1.style.display = "block";
                element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "none" : null;
                element_6 ? element_6.style.display = "none" : null;
            }
        }
    }
    if (type === "update") {
        if (element_1) {
            if (element_1.offsetParent != null) {

                element_1.style.display = "block";
                element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "none" : null;
                element_6 ? element_6.style.display = "none" : null;
            } else {

                element_1.style.display = "block";
                element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "block" : null;
                element_4 ? element_4.style.display = "none" : null;
                element_5 ? element_5.style.display = "none" : null;
                element_6 ? element_6.style.display = "block" : null;
            }
        }
    }
    if (type === "address_update_only") {
        if (element_1) {
            if (element_1.offsetParent != null) {

                element_1.style.display = "block";
                //element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "none" : null;
                element_4 ? element_4.style.display = "block" : null;
                element_5 ? element_5.style.display = "block" : null;
                element_6 ? element_6.style.display = "none" : null;
            } else {

                element_1.style.display = "block";
                //element_2 ? element_2.style.display = "none" : null;
                element_3 ? element_3.style.display = "none" : null;
                element_4 ? element_4.style.display = "block" : null;
                element_5 ? element_5.style.display = "block" : null;
                element_6 ? element_6.style.display = "none" : null;
            }
        }
    }

}


// Promotion
function onApplyPromo(data, total, ntotal, ablefreeexistproduct, freeexistresultproduct, freeexistresultproductprice, status) {
    let checkout_data_promo = {
        "journey_type": 'checkout',
        "update_type": "selected_promo",
        "data": data,
        "total": total,
        "ntotal": ntotal,
        "ablefreeexistproduct": ablefreeexistproduct,
        "freeexistresultproduct": freeexistresultproduct,
        "freeexistresultproductprice": freeexistresultproductprice,
        "status": status
    };
    UpdateSessionData(checkout_data_promo);
}

// Promotion
function applyPromotion() {
    var loading = document.getElementById("loading");
    if (loading) {
        loading.style.display = "block";
        $prosession = '';
        if (isJSON(session_data)) {
            $prosession = JSON.parse(session_data);
        } else {
            $prosession = session_data;
        }
    
        if($prosession["selection"]["step1"][0]["productcountryid"]){
        Promotion("web", "awesome-shave-kits", "combinesession",$prosession["selection"]["step1"][0]["productcountryid"]);
        firstpromoapply = 0;
    }
    }
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function SelectBlade(productcountriesid) {
    return function () {
        $("#blade-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        $("#blade-tick-" + productcountriesid).removeClass('d-none');
        var productcountryid = $("#product_country_id_" + productcountriesid).val();
        var productname = $("#product_name_" + productcountriesid).val();
        var productprice = $("#product_price_" + productcountriesid).val();
        journey_type = 'selection';
        quantity = 1;
        product_data = {
            "productcountryId": productcountryid,
            "productname": productname,
            "quantity": quantity,
            "journey_type": journey_type,
            "product_type": "awesome-shave-kits"
        };
        var loading = document.getElementById("loading");
        if (loading) {
            loading.style.display = "block";
        }
        UpdateSessionData(product_data);

        for (let i = 0; i < 3; i++) {
            if (i != productcountriesid) {
                $("#blade-" + i).removeClass('item-selected').addClass('item-unselected');
                $("#blade-tick-" + i).addClass('d-none');
            }
        }

        if (codeiso == "kr") {
            productprice = formatNumber(productprice);
            productprice = productprice.replace(".00", '');
        }

        $('#selection_price').text(productprice);
    };
};


function DisplayPaymentError() {
    if (typeof all_sessions.stripe_payment_fail !== "undefined") {
        let dataParsed = all_sessions.stripe_payment_fail;
        if (dataParsed.status === "fail") {
            $("#notification_payment_failed_text").html(trans('website_contents.global.stripe_payment_fail'));
            $("#notification_payment_failed").modal("show");
        }

    }
}

 // mask selection

function UpdateMaskSessionData(data) {
  
 
    let _session_checkout_selected_mask = {};

    var journey_type = data.journey_type;
    if (journey_type === 'checkout') {
            return session(SESSION_CHECKOUT_ASK, SESSION_GET, null).done(function (response) {
                // Do something with the session data
                if (response) {
                    get_checkout_session = response;
                    session_data_for_checkout = JSON.parse(response);
    
                if (data.update_type === 'empty_selected_mask') {
                    _session_checkout_selected_mask = {};
                    session_data_for_checkout.checkout.free_mask = _session_checkout_selected_mask;
                }
                else if (data.update_type === 'selected_mask') {
                    _session_checkout_selected_mask = {
                        "pcid": data.pcid,
                        "price": data.price,
                        "sku": data.sku,
                        "name": data.name,
                        "qty": data.qty
                    };
                    session_data_for_checkout.checkout.free_mask = _session_checkout_selected_mask;
                }

                    return session(SESSION_CHECKOUT_ASK, SESSION_SET, JSON.stringify(session_data_for_checkout)).done(function () {
                        newpaynow()
                         return true;
                    });
                } else {
                    return true;
                }
            });


        
    }
}

function SelectMask(productcountriesid,productid) {
    return function () {
        $("#selection-mask-img").attr(
            "src",
            GLOBAL_URL_V2 +
            "/images/mask/mask-" +
            productid +
            ".png"
        );
        $(".maskselection-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        // let imagepath = $(".maskselection-" + productcountriesid + " img").attr('src');
        // imageget = imagepath.split("/").pop(-1);
        // $(".custom-blade-img").attr('src',imageassetspath + "/customcheck_"+imageget);

        maskselection.forEach(product => {
            if (product.pcid != productcountriesid) {
                $(".maskselection-" + product.pcid).removeClass('item-selected').addClass('item-unselected');
            } else {
                maskpcidselection = product;
            }
        });
    };
};

function submitWithoutMask(){
    checkout_data_free_mask = {
        "journey_type": 'checkout',
        "update_type": "empty_selected_mask",
    };

    UpdateMaskSessionData(checkout_data_free_mask);
}



function submitWithMask(){
    checkout_data_free_mask = {
        "journey_type": 'checkout',
        "update_type": "selected_mask",
        "pcid": maskpcidselection.pcid,
        "price": maskpcidselection.sellPrice,
        "sku": maskpcidselection.sku,
        "name": maskpcidselection.name,
        "qty": '1'
    };

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: GLOBAL_URL + '/check/mask/qty',
        method: "POST",
        cache: false,
        data: { pcid: maskpcidselection.pcid,countryid : countryid, lang: langCode},
    })
        .done(function (data) {
            if(data == 1){
                UpdateMaskSessionData(checkout_data_free_mask);
            }else{
                location.reload();
            }
    });

}

function newpaynow(){
    let data = "";
    //for kr
    if (country_id == 'kr') {
        let CONFIRM_PURCHASE_URL =
            window.location.origin +
            GLOBAL_URL +
            "/ask/checkout/nicepay/confirm-purchase";
        let Redirect_URL =
            window.location.origin +
            GLOBAL_URL +
            "/thankyou/";
        let etotal = document.getElementById("c-total");
        let total = 0;
        if (etotal) {
            total = etotal.innerText;
            //Currency and price for kr
            total = total.replace(/\,/g, '');
        }

        let ecn = document.getElementById("d_contact_number");
        let cn = 0;
        if (ecn) {
            cn = ecn.innerText;
        }
        let useremail = "";
        let username = "";
        if (user) {
            useremail = user.email;
            username = user.firstName;
        } else {
            useremail = getloginsession[0].email;
            username = getloginsession[0].firstName;
        }


        product_data = {
            "email": useremail,
            "name": username,
            "phone": cn,
            "amount": Math.round(total),
            "type": "awesome-shave-kits"
        };

        nicepayAlacarte(product_data, CONFIRM_PURCHASE_URL, Redirect_URL);
    } else {

        onProceedPayment(session_data, payment_intent, data);
    }
}
// mask selection


// ========================================================================================================
// CHECKOUT PAGE ON LOAD ==================================================================================
// ========================================================================================================

$(function () {
    $("#sign_in_sign_up_email").on("focus", function (event) { error_element_1.setAttribute("hidden", ""); });
    $("#sign_in_sign_up_password").on("focus", function (event) { error_element_1.setAttribute("hidden", ""); });
    $("#sign_in_sign_up_email_2").on("focus", function (event) { error_element_2.setAttribute("hidden", ""); });
    $("#sign_in_sign_up_password_2").on("focus", function (event) { error_element_2.setAttribute("hidden", ""); });
    // Action: On submit email & password info
    $("#new_user_login").on("submit", function (event) { console.log("Login | On submit email & password info"); event.preventDefault(); onAccountFill_login(); });
    $("#new_user_register").on("submit", function (event) { console.log("Register | On submit email & password info"); event.preventDefault(); if (_register_checkout_valid === true) { onAccountFill_register(); } });

    ClearSessionData_CHECKOUT().done(function (response) {
        if (response) {

            // Check Current Steps and do following function
            steps();

            //On select blade
            for (let i = 0; i < 3; i++) {
                $("#blade-" + i).click(SelectBlade(i));

                if (i == 0) {
                    $("#blade-" + i).click();
                }
            }

            if (current_step == 1) {
                var link = element_main_4;
                if (link) {
                    link.click();
                    $("#button-next").text('NEXT');
                }
            }

            if (element_3) {
                if (element_3.offset !== null) {
                    if (window.getComputedStyle(document.getElementById('shipping-address-show')).display == "none") {
                        element_4.style.display = "none";
                    }
                    element_5.style.display = "none";
                    element_6.style.display = "none";
                }
            }

            // Enable billing if check box is checked
            if (document.body.contains(element_11) && element_11.checked == true) { element_7.style.display = "block"; }
            if (document.body.contains(element_11) && element_11.checked == false) { element_7.style.display = "none"; }
            if (document.body.contains(element_11_1) && element_11_1.checked == true) { element_7_1.style.display = "block"; }
            if (document.body.contains(element_11_1) && element_11_1.checked == false) { element_7_1.style.display = "none"; }

            // ADD All Default Data to Checkout Session
            if (!(typeof checkout_details === 'undefined'
                && typeof payment_intent === 'undefined'
                && typeof user_id === 'undefined'
                && typeof country_id === 'undefined'
                && typeof user === 'undefined'
                && typeof delivery_address === 'undefined'
                && typeof billing_address === 'undefined'
                && typeof default_card === 'undefined'
                && typeof session_data === 'undefined'
            )) {
                let xe = checkout_details ? checkout_details : '';
                let xf = payment_intent ? payment_intent : '';
                let xg = user_id ? user_id : '';
                let xh = country_id ? country_id : '';
                let xi = user ? user : '';
                let xj = delivery_address ? delivery_address : '';
                let xk = billing_address ? billing_address : '';
                let xl = default_card ? default_card : '';
                let xm = session_data ? session_data : '';
                if (country_id != 'kr') {
                    if (xe && xf && xg && xh && xi) {
                        let data = {
                            "checkout_details": xe,
                            "payment_intents": xf,
                            "user_id": xg,
                            "country_id": xh,
                            "user": xi,
                            "delivery_address": xj,
                            "billing_address": xk,
                            "default_card": xl,
                            "session_data": xm,
                            "journey_type": "checkout",
                            "update_type": "default"
                        };
                        UpdateSessionData(data).done(function () {
                            // Save default delivery & billing address to session checkout
                            if (element_12 && element_13) {
                                if (element_12.value && element_13.value) {
                                    let addressUpdates = {
                                        "selected_delivery_address_id": element_12.value ? element_12.value : '',
                                        "selected_billing_address_id": element_13.value ? element_13.value : '',
                                    };

                                    if (delivery_address[0].address !== billing_address[0].address) {
                                        $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                        element_7.style.display = "block";
                                    } else {
                                        $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                        element_7.style.display = "none";
                                    }

                                    onUpdateDeliverySelection(addressUpdates, "address_update_only");
                                }
                            }

                            // PaymentIntent | Get [ID] & [UPDATE_TYPE]
                            if (element_14 && element_15) {
                                let paymentIntent_updates = {
                                    "payment_intent_id": element_14.value ? element_14.value : '',
                                    "next_update_type": element_15.value ? element_15.value : '',
                                };
                                onUpdatePaymentIntent(paymentIntent_updates, element_15.value);
                            }
                        });
                    }
                } else {
                    if (xg && xh && xi) {
                        let data = {
                            "checkout_details": xe,
                            "payment_intents": xf,
                            "user_id": xg,
                            "country_id": xh,
                            "user": xi,
                            "delivery_address": xj,
                            "billing_address": xk,
                            "default_card": xl,
                            "session_data": xm,
                            "journey_type": "checkout",
                            "update_type": "default"
                        };
                        UpdateSessionData(data).done(function () {
                            // Save default delivery & billing address to session checkout
                            if (element_12 && element_13) {
                                if (element_12.value && element_13.value) {
                                    let addressUpdates = {
                                        "selected_delivery_address_id": element_12.value ? element_12.value : '',
                                        "selected_billing_address_id": element_13.value ? element_13.value : '',
                                    };

                                    if (delivery_address[0].address !== billing_address[0].address) {
                                        $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                        element_7.style.display = "block";
                                    } else {
                                        $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                        element_7.style.display = "none";
                                    }

                                    onUpdateDeliverySelection(addressUpdates, "address_update_only");
                                }
                            }

                            // PaymentIntent | Get [ID] & [UPDATE_TYPE]
                            if (element_14 && element_15) {
                                let paymentIntent_updates = {
                                    "payment_intent_id": element_14.value ? element_14.value : '',
                                    "next_update_type": element_15.value ? element_15.value : '',
                                };
                                onUpdatePaymentIntent(paymentIntent_updates, element_15.value);
                            }
                        });
                    }
                }
            }
        }
    });

    // Action: On submit edit address
    $("#edit-addresses-block").on("submit", function (event) {
        event.preventDefault();
        v_checkout_e_address("edit-addresses-block");
        if ($('#edit-addresses-block').valid() === true) {
            $("#loading").css("display", "block");

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: GLOBAL_URL + '/delivery-address/edit/' + user_id,
                data: $(this).serialize(),
                cache: false,
                success: function (data) {

                    element_1.innerHTML = '';
                    // All countries except korea
                    if (country_id != 'kr') {
                        if (data["delivery_address"]) {
                            element_1.innerHTML =
                                '<div class="panel-body">' +
                                '<div class="" id="appendAddress">' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.delivery_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.fullName + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.portalCode + ', ' + data.delivery_address.city + ', ' + data.delivery_address.state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.contactNumber + '</p><br>' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.billing_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.portalCode + ', ' + data.billing_address.city + ', ' + data.billing_address.state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.contactNumber + '</p><br>' +
                                '</div>' +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">' + trans('website_contents.checkout.address.edit_address') + '</button></div>' +
                                '</div>';
                        }



                        if (element_1 !== 'undefined' && element_1 !== null) {
                            element_1.style.display = "block";
                        }
                        if (element_2 !== 'undefined' && element_2 !== null) {
                            element_2.style.display = "none";
                        }
                        if (element_4 !== 'undefined' && element_4 !== null) {
                            element_4.style.display = "block";
                        }
                        if (element_3 !== 'undefined' && element_3 !== null) {
                            element_3.style.display = "none";
                        }
                        if (element_5 !== 'undefined' && element_5 !== null) {
                            element_5.style.display = "block";
                        }
                        if (element_6 !== 'undefined' && element_6 !== null) {
                            element_6.style.display = "none";
                        }

                        // append address values into edit-form
                        let updateEditForm = {
                            "delivery_address_fullname": data.delivery_address.fullName,
                            "delivery_address_address": data.delivery_address.address,
                            "delivery_address_portalCode": data.delivery_address.portalCode,
                            "delivery_address_city": data.delivery_address.city,
                            "delivery_address_state": data.delivery_address.state,
                            "delivery_address_contactNumber": data.delivery_address.contactNumber,
                            "billing_address_contactNumberExt": data.billing_address.contactNumberExt,
                            "billing_address_address": data.billing_address.address,
                            "billing_address_portalCode": data.billing_address.portalCode,
                            "billing_address_city": data.billing_address.city,
                            "billing_address_state": data.billing_address.state,
                            "billing_address_contactNumber": data.billing_address.contactNumber,
                            "billing_address_contactNumberExt": data.billing_address.contactNumberExt,
                        };

                        onAddDeliveryAddress(updateEditForm);
                        let addressUpdates = {
                            "selected_delivery_address_id": data.delivery_address ? data.delivery_address.id : '',
                            "selected_billing_address_id": data.billing_address ? data.billing_address.id : '',
                        };
                        onUpdateDeliverySelection(addressUpdates, "update");

                        if (updateEditForm.delivery_address_address !== updateEditForm.billing_address_address) {
                            $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                            element_7.style.display = "block";
                        } else {
                            $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                            element_7.style.display = "none";
                        }
                        $("#loading").css("display", "none");
                    } else if (country_id == 'kr') {
                        if (data["delivery_address"]) {
                            element_1.innerHTML =
                                '<div class="panel-body">' +
                                '<div class="" id="appendAddress">' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.delivery_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.fullName + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.portalCode + ', ' + data.delivery_address.city + ', ' + data.delivery_address.state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address.contactNumber + '</p><br>' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.billing_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.portalCode + ', ' + data.billing_address.city + ', ' + data.billing_address.state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address.contactNumber + '</p><br>' +
                                '</div>' +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">' + trans('website_contents.checkout.address.edit_address') + '</button></div>' +
                                '</div>';
                        }

                        if (data["delivery_address"] && data["billing_address"]) {
                            var ada1 = document.getElementById("add_delivery_address1");
                            var ada = document.getElementById("add_delivery_address");
                            var adf = document.getElementById("add_delivery_flat");
                            ada1.value = "";
                            ada.value = "";
                            adf.value = "";

                            var aba1 = document.getElementById("add_billing_address1");
                            var aba = document.getElementById("add_billing_address");
                            var abf = document.getElementById("add_billing_flat");
                            aba1.value = "";
                            aba.value = "";
                            abf.value = "";

                            var dfn = document.getElementById("edit_delivery_name");
                            dfn.value = data["delivery_address"]["fullName"];
                            var dda = document.getElementById("edit_delivery_address");

                            var getdda = data["delivery_address"]["address"];
                            dda.value = data["delivery_address"]["address"];
                            var dda1 = document.getElementById("edit_delivery_address1");
                            var ddf = document.getElementById("edit_delivery_flat");
                            if (getdda.includes(",")) {
                                var splitdda = getdda.split(",");
                                dda1.value = splitdda[0];
                                ddf.value = splitdda[1];
                            } else {
                                dda1.value = getdda;
                            }
                            var addcheckbox = document.getElementById("enableBillingEditCheckBox_for_add");
                            if (addcheckbox.checked == true) {
                                addcheckbox.click();
                            }
                            var ddc = document.getElementById("edit_delivery_city");
                            ddc.value = data["delivery_address"]["city"];

                            var dds = document.getElementById("edit_delivery_state");
                            dds.value = data["delivery_address"]["state"];

                            var dpc = document.getElementById("edit_delivery_postcode");
                            dpc.value = data["delivery_address"]["portalCode"];

                            var ddp = document.getElementById("edit_delivery_phone");
                            ddp.value = data["delivery_address"]["contactNumber"].replace(("+" + phoneExt), "");

                            var bba = document.getElementById("edit_billing_address");

                            var getbba = data["billing_address"]["address"];
                            bba.value = data["billing_address"]["address"];
                            var bba1 = document.getElementById("edit_billing_address1");
                            var bbf = document.getElementById("edit_billing_flat");
                            if (getbba.includes(",")) {
                                var splitbba = getbba.split(",");
                                bba1.value = splitbba[0];
                                bbf.value = splitbba[1];
                            } else {
                                bba1.value = getbba;
                            }

                            var bbc = document.getElementById("edit_billing_city");
                            bbc.value = data["billing_address"]["city"];

                            var bbs = document.getElementById("edit_billing_state");
                            bbs.value = data["billing_address"]["state"];

                            var bpc = document.getElementById("edit_billing_postcode");
                            bpc.value = data["billing_address"]["portalCode"];

                            var bbp = document.getElementById("edit_billing_phone");
                            bbp.value = data["billing_address"]["contactNumber"].replace(("+" + phoneExt), "");

                            // append address values into edit-form
                            let updateEditForm = {
                                "delivery_address_fullname": data.delivery_address.fullName,
                                "delivery_address_address": data.delivery_address.address,
                                "delivery_address_portalCode": data.delivery_address.portalCode,
                                "delivery_address_city": data.delivery_address.city,
                                "delivery_address_state": data.delivery_address.state,
                                "delivery_address_contactNumber": data.delivery_address.contactNumber,
                                "delivery_address_contactNumberExt": data.delivery_address.contactNumberExt,
                                "billing_address_address": data.billing_address.address,
                                "billing_address_portalCode": data.billing_address.portalCode,
                                "billing_address_city": data.billing_address.city,
                                "billing_address_state": data.billing_address.state,
                                "billing_address_contactNumber": data.billing_address.contactNumber,
                                "billing_address_contactNumberExt": data.billing_address.contactNumberExt,
                            };
                            //onAddDeliveryAddress(updateEditForm);

                            let addressUpdates = {
                                "selected_delivery_address_id": data.delivery_address ? data.delivery_address.id : '',
                                "selected_billing_address_id": data.billing_address ? data.billing_address.id : '',
                            };
                            onUpdateDeliverySelection(addressUpdates, "update");

                            if (updateEditForm.delivery_address_address !== updateEditForm.billing_address_address) {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                element_7.style.display = "block";
                            } else {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                element_7.style.display = "none";
                            }
                            $("#loading").css("display", "none");
                        }

                        if (element_1 !== 'undefined' && element_1 !== null) {
                            element_1.style.display = "block";
                        }
                        if (element_2 !== 'undefined' && element_2 !== null) {
                            element_2.style.display = "none";
                        }
                        if (element_4 !== 'undefined' && element_4 !== null) {
                            element_4.style.display = "block";
                        }
                        if (element_3 !== 'undefined' && element_3 !== null) {
                            element_3.style.display = "none";
                        }
                        if (element_5 !== 'undefined' && element_5 !== null) {
                            element_5.style.display = "block";
                        }
                        if (element_6 !== 'undefined' && element_6 !== null) {
                            element_6.style.display = "none";
                        }

                    }
                },
                error: function () {
                    $("#loading").css("display", "none");
                }
            });
        }
    });

    // Action: On submit add address
    $("#add-addresses-block").on("submit", function (event) {
        event.preventDefault();
        v_checkout_address("add-addresses-block");
        if ($('#add-addresses-block').valid() === true) {
            $("#loading").css("display", "block");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: GLOBAL_URL + '/delivery-address/add',
                data: $(this).serialize(),
                cache: false,
                success: function (data) {

                    element_1.innerHTML = '';

                    // All countries except korea
                    if (country_id != 'kr') {
                        if (data["delivery_address"] && data["billing_address"]) {
                            element_1.innerHTML =
                                '<div class="panel-body">' +
                                '<div class="" id="appendAddress">' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.delivery_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].fullName + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].portalCode + ', ' + data.delivery_address[0].city + ', ' + data.delivery_address[0].state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].contactNumber + '</p><br>' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.billing_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].portalCode + ', ' + data.billing_address[0].city + ', ' + data.billing_address[0].state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].contactNumber + '</p><br>' +
                                '</div>' +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">' + trans('website_contents.checkout.address.edit_address') + '</button></div>' +
                                '</div>';

                            // append address values into edit-form
                            let updateEditForm = {
                                "delivery_address_fullname": data.delivery_address[0].fullName,
                                "delivery_address_address": data.delivery_address[0].address,
                                "delivery_address_portalCode": data.delivery_address[0].portalCode,
                                "delivery_address_city": data.delivery_address[0].city,
                                "delivery_address_state": data.delivery_address[0].state,
                                "delivery_address_contactNumber": data.delivery_address[0].contactNumber,
                                "delivery_address_contactNumberExt": data.delivery_address[0].contactNumberExt,
                                "billing_address_address": data.billing_address[0].address,
                                "billing_address_portalCode": data.billing_address[0].portalCode,
                                "billing_address_city": data.billing_address[0].city,
                                "billing_address_state": data.billing_address[0].state,
                                "billing_address_contactNumber": data.billing_address[0].contactNumber,
                                "billing_address_contactNumberExt": data.billing_address[0].contactNumberExt,
                            };
                            onAddDeliveryAddress(updateEditForm);
                            let addressUpdates = {
                                "selected_delivery_address_id": data.delivery_address[0] ? data.delivery_address[0].id : '',
                                "selected_billing_address_id": data.billing_address[0] ? data.billing_address[0].id : '',
                            };
                            onUpdateDeliverySelection(addressUpdates, "update");

                            if (updateEditForm.delivery_address_address !== updateEditForm.billing_address_address) {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                element_7.style.display = "block";
                            } else {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                element_7.style.display = "none";
                            }
                            $("#loading").css("display", "none");
                        }

                        if (element_1 !== 'undefined' && element_1 !== null) {
                            element_1.style.display = "block";
                            element_1.removeAttribute("hidden");
                        }
                        if (element_2 !== 'undefined' && element_2 !== null) {
                            element_2.style.display = "none";
                        }
                        if (element_4 !== 'undefined' && element_4 !== null) {
                            element_4.style.display = "block";
                        }
                        if (element_3 !== 'undefined' && element_3 !== null) {
                            element_3.style.display = "none";
                        }
                        if (element_5 !== 'undefined' && element_5 !== null) {
                            element_5.style.display = "block";
                        }
                        if (element_6 !== 'undefined' && element_6 !== null) {
                            element_6.style.display = "none";
                        }

                    } else if (country_id == 'kr') {

                        if (data["delivery_address"] && data["billing_address"]) {
                            element_1.innerHTML =
                                '<div class="panel-body">' +
                                '<div class="" id="appendAddress">' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.delivery_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].fullName + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].portalCode + ', ' + data.delivery_address[0].city + ', ' + data.delivery_address[0].state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.delivery_address[0].contactNumber + '</p><br>' +
                                '<br>' +
                                '<label style="font-weight: bold">' + trans('website_contents.checkout.address.billing_address') + '</label><br>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].address + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].portalCode + ', ' + data.billing_address[0].city + ', ' + data.billing_address[0].state + '</p>' +
                                '<p style="margin-bottom:0;">' + data.billing_address[0].contactNumber + '</p><br>' +
                                '</div>' +
                                '<div><button id="enable-address-edit" class="btn btn-load-more col-12" onclick="editAddress()">' + trans('website_contents.checkout.address.edit_address') + '</button></div>' +
                                '</div>';

                            var ada1 = document.getElementById("add_delivery_address1");
                            var ada = document.getElementById("add_delivery_address");
                            var adf = document.getElementById("add_delivery_flat");
                            ada1.value = "";
                            ada.value = "";
                            adf.value = "";

                            var aba1 = document.getElementById("add_billing_address1");
                            var aba = document.getElementById("add_billing_address");
                            var abf = document.getElementById("add_billing_flat");
                            aba1.value = "";
                            aba.value = "";
                            abf.value = "";

                            var dfn = document.getElementById("edit_delivery_name");
                            dfn.value = data["delivery_address"][0]["fullName"];
                            var dda = document.getElementById("edit_delivery_address");

                            var getdda = data["delivery_address"][0]["address"];
                            dda.value = data["delivery_address"][0]["address"];
                            var dda1 = document.getElementById("edit_delivery_address1");
                            var ddf = document.getElementById("edit_delivery_flat");
                            if (getdda.includes(",")) {
                                var splitdda = getdda.split(",");
                                dda1.value = splitdda[0];
                                ddf.value = splitdda[1];
                            } else {
                                dda1.value = getdda;
                            }
                            var addcheckbox = document.getElementById("enableBillingEditCheckBox_for_add");
                            if (addcheckbox.checked == true) {
                                addcheckbox.click();
                            }
                            var ddc = document.getElementById("edit_delivery_city");
                            ddc.value = data["delivery_address"][0]["city"];

                            var dds = document.getElementById("edit_delivery_state");
                            dds.value = data["delivery_address"][0]["state"];

                            var dpc = document.getElementById("edit_delivery_postcode");
                            dpc.value = data["delivery_address"][0]["portalCode"];

                            var ddp = document.getElementById("edit_delivery_phone");
                            ddp.value = data["delivery_address"][0]["contactNumber"].replace(("+" + phoneExt), "");

                            var bba = document.getElementById("edit_billing_address");

                            var getbba = data["billing_address"][0]["address"];
                            bba.value = data["billing_address"][0]["address"];
                            var bba1 = document.getElementById("edit_billing_address1");
                            var bbf = document.getElementById("edit_billing_flat");
                            if (getbba.includes(",")) {
                                var splitbba = getbba.split(",");
                                bba1.value = splitbba[0];
                                bbf.value = splitbba[1];
                            } else {
                                bba1.value = getbba;
                            }

                            var bbc = document.getElementById("edit_billing_city");
                            bbc.value = data["billing_address"][0]["city"];

                            var bbs = document.getElementById("edit_billing_state");
                            bbs.value = data["billing_address"][0]["state"];

                            var bpc = document.getElementById("edit_billing_postcode");
                            bpc.value = data["billing_address"][0]["portalCode"];

                            var bbp = document.getElementById("edit_billing_phone");
                            bbp.value = data["billing_address"][0]["contactNumber"].replace(("+" + phoneExt), "");
                            let updateEditForm = {
                                "delivery_address_fullname": data.delivery_address[0].fullName,
                                "delivery_address_address": data.delivery_address[0].address,
                                "delivery_address_portalCode": data.delivery_address[0].portalCode,
                                "delivery_address_city": data.delivery_address[0].city,
                                "delivery_address_state": data.delivery_address[0].state,
                                "delivery_address_contactNumber": data.delivery_address[0].contactNumber,
                                "delivery_address_contactNumberExt": data.delivery_address[0].contactNumberExt,
                                "billing_address_address": data.billing_address[0].address,
                                "billing_address_portalCode": data.billing_address[0].portalCode,
                                "billing_address_city": data.billing_address[0].city,
                                "billing_address_state": data.billing_address[0].state,
                                "billing_address_contactNumber": data.billing_address[0].contactNumber,
                                "billing_address_contactNumberExt": data.billing_address[0].contactNumberExt,
                            };
                            // UpdateSessionCPAddressData(data["delivery_address"], data["billing_address"]);
                            // onAddDeliveryAddress(updateEditForm);
                            let addressUpdates = {
                                "selected_delivery_address_id": data.delivery_address[0] ? data.delivery_address[0].id : '',
                                "selected_billing_address_id": data.billing_address[0] ? data.billing_address[0].id : '',
                            };
                            onUpdateDeliverySelection(addressUpdates, "update");

                            if (updateEditForm.delivery_address_address !== updateEditForm.billing_address_address) {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", true);
                                element_7.style.display = "block";
                            } else {
                                $("#enableBillingEditCheckBox_for_edit").prop("checked", false);
                                element_7.style.display = "none";
                            }

                            $("#loading").css("display", "none");
                        }

                        if (element_1 !== 'undefined' && element_1 !== null) {
                            element_1.style.display = "block";
                            element_1.removeAttribute("hidden");
                        }
                        if (element_2 !== 'undefined' && element_2 !== null) {
                            element_2.style.display = "none";
                        }
                        if (element_4 !== 'undefined' && element_4 !== null) {
                            element_4.style.display = "block";
                        }
                        if (element_3 !== 'undefined' && element_3 !== null) {
                            element_3.style.display = "none";
                        }
                        if (element_5 !== 'undefined' && element_5 !== null) {
                            element_5.style.display = "block";
                        }
                        if (element_6 !== 'undefined' && element_6 !== null) {
                            element_6.style.display = "none";
                        }

                        // append address values into edit-form

                    }

                },
                error: function () {
                    $("#loading").css("display", "none");
                }
            });
        }
    });

    // On add card
    $("#button-add-card").click(function (event) {
        event.preventDefault();
        v_checkout_cards("form_card");
        if ($('#form_card').valid() === true) {
            $("#loading").css("display", "block");
            let card_name = user.firstName + (user.lastName ? user.lastName : "");
            let card_number = $("#card-number").val().replace(/\s/g, '');
            let card_expiry_array = $("#card-expiry").val().split(" / ");
            let card_expiry_month = card_expiry_array[0];
            let card_expiry_year = card_expiry_array[1];
            let card_cvv = $("#card-cvv").val()

            if (card_number && card_number.length == 16 && card_expiry_month && card_expiry_year && card_cvv) {

                let stripeData = { card_name, card_number, card_expiry_month, card_expiry_year, card_cvv };

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: GLOBAL_URL + '/stripe/card/add',
                    data: stripeData,
                    json: true,
                    cache: false,
                    success: function (data) {

                        if (checkout_details) {
                            if (Array.isArray(checkout_details)) {
                                checkout_details.forEach(function (el) {
                                    total_price = parseFloat(total_price) + parseFloat(el.sellPrice);
                                });
                            } else {
                                total_price = parseFloat(total_price) + parseFloat(checkout_details.sellPrice);
                            }

                            let _checkout_details = {
                                "total_price": total_price
                            };
                            if (data.card && data.customer) {
                                // update session for checkout - card_details
                                onUpdateCard(data.card_from_db);
                                updateCardSelectionList(data.card_from_db.UserId);

                                // update paymentIntent if customerId has not been linked yet
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: 'POST',
                                    url: GLOBAL_URL + '/stripe/payment-intent/update',
                                    data: {
                                        "payment_intent_type": "awesome-shave-kits",
                                        "payment_intent_details": payment_intent,
                                        "update_type": payment_intent_next_update_type,
                                        "checkout_details": _checkout_details,
                                        "data": data,
                                        "additional_card": 0
                                    },
                                    json: true,
                                    cache: false,
                                    success: function (response) {

                                        payment_intent = response;
                                        checkout_data_card = {
                                            "journey_type": 'checkout',
                                            "update_type": "selected_card",
                                            "card_id": data.card_from_db.id,
                                            "card": data.card_from_db,
                                            "payment_intent_id": response.id
                                        };

                                        //Update card selection lists
                                        UpdateCardSelectionList(checkout_data_card.card)

                                        UpdateSessionData(checkout_data_card);

                                        $("#edit-card-container").removeClass("hidden");
                                        $("#add-card-container").addClass("hidden");

                                        $("#error-payment-method").addClass("hidden");
                                        $("#error-payment-method").html("");
                                        $("#loading").css("display", "none");

                                        $("#card-number").val("");
                                        $("#card-expiry").val("");
                                        $("#card-cvv").val("");

                                        // update new card last 4 digits
                                        let last4digits = data.card_from_db.cardNumber ? data.card_from_db.cardNumber : '1234';
                                        $(".cc_last4digits").html(last4digits);
                                        let expirymy = data.card_from_db.expiredMonth && data.card_from_db.expiredYear ? data.card_from_db.expiredMonth + '/' + data.card_from_db.expiredYear.slice(-2) : 'xx/xx';
                                        $(".cc_expmy").html(expirymy);
                                    },
                                    error: function (response) {
                                        $("#loading").css("display", "none");
                                        $("#error-payment-method").removeClass("hidden");
                                        if (response.responseJSON.message == 'Server Error') {
                                            $("#error-payment-method").html(trans('website_contents.global.invalid_card'));
                                        } else {
                                            $("#error-payment-method").html(response.responseJSON.message);
                                        }
                                    }
                                });

                            }
                            else {
                                $("#loading").css("display", "none");
                            }
                        }
                        else {
                            $("#loading").css("display", "none");
                        }
                    },
                    error: function (response) {
                        $("#loading").css("display", "none");
                        $("#error-payment-method").removeClass("hidden");
                        if (response.responseJSON.message == 'Server Error') {
                            $("#error-payment-method").html(trans('website_contents.global.invalid_card'));
                        } else {
                            $("#error-payment-method").html(response.responseJSON.message);
                        }
                    }
                });
            }
            else {
                $("#loading").css("display", "none");
            }
        }
    });

    // On add card
    $("#add-card-details").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: GLOBAL_URL + '/stripe/card/add',
            data: $(this).serialize(),
            json: true,
            cache: false,
            success: function (data) {

                if (checkout_details) {
                    checkout_details.forEach(function (el) {
                        total_price = parseFloat(total_price) + parseFloat(el.sellPrice);
                    });

                    let _checkout_details = {
                        "total_price": total_price
                    };

                    if (data.card && data.customer) {
                        // update session for checkout - card_details
                        onUpdateCard(data.card_from_db);
                        updateCardSelectionList(data.card_from_db.UserId);

                        // update paymentIntent if customerId has not been linked yet
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            url: GLOBAL_URL + '/stripe/payment-intent/update',
                            data: {
                                "payment_intent_type": "awesome-shave-kits",
                                "payment_intent_details": payment_intent,
                                "update_type": payment_intent_next_update_type,
                                "checkout_details": _checkout_details,
                                "data": data,
                                "additional_card": 0
                            },
                            json: true,
                            cache: false,
                            success: function (response) {

                                payment_intent = response;
                                onAddCard(data.card_from_db.id, data.card_from_db, response.id);

                            },
                            error: function () {

                            }
                        });

                    }
                }

            },
            error: function () {

            }
        });
    });

    //On click back
    $("#button-back").click(function () {
        let selection =
            window.location.origin +
            GLOBAL_URL +
            "/product/ask";
        window.location = selection;
    });

    // Proceed to Payment
   
    $("#form-pay").click(function (event) {
        event.preventDefault();
        let data = "";
        //for kr
        if (country_id == 'kr') {
            let CONFIRM_PURCHASE_URL =
                window.location.origin +
                GLOBAL_URL +
                "/ask/checkout/nicepay/confirm-purchase";
            let Redirect_URL =
                window.location.origin +
                GLOBAL_URL +
                "/thankyou/";
            let etotal = document.getElementById("c-total");
            let total = 0;
            if (etotal) {
                total = etotal.innerText;
                //Currency and price for kr
                total = total.replace(/\,/g, '');
            }

            let ecn = document.getElementById("d_contact_number");
            let cn = 0;
            if (ecn) {
                cn = ecn.innerText;
            }
            let useremail = "";
            let username = "";
            if (user) {
                useremail = user.email;
                username = user.firstName;
            } else {
                useremail = getloginsession[0].email;
                username = getloginsession[0].firstName;
            }


            product_data = {
                "email": useremail,
                "name": username,
                "phone": cn,
                "amount": Math.round(total),
                "type": "awesome-shave-kits"
            };

            nicepayAlacarte(product_data, CONFIRM_PURCHASE_URL, Redirect_URL);
        } else {

            onProceedPayment(session_data, payment_intent, data);
        }
    });
    

    // mask selection
    if (typeof maskselection !== 'undefined') {
    if(maskselection){
    for (let i = 0; i < maskselection.length; i++) {
        let productcountriesid = maskselection[i].pcid;
        let productid =  maskselection[i].pid;
        if(i==0)
        {
            $("#selection-mask-img").attr(
                "src",
                GLOBAL_URL_V2 +
                "/images/mask/mask-" +
                maskselection[i].pid +
                ".png"
            );
            maskpcidselection =  maskselection[i];
            $(".maskselection-" + productcountriesid).removeClass('item-unselected').addClass('item-selected');
        }
        $(".maskselection-" + productcountriesid).click(SelectMask(productcountriesid,productid))

        // if (i == 2) {
        //     $(".maskselection-" + productcountriesid).click();
        // }
    }
   }
}
 /*
    $("#form-pay").click(function (event) {
        event.preventDefault();
        if((countryid == 1 || countryid == 7) && countmaskqty > 0){
            $("#model-mask-selection").modal("show");
        }else{
        let data = "";
        //for kr
        if (country_id == 'kr') {
            let CONFIRM_PURCHASE_URL =
                window.location.origin +
                GLOBAL_URL +
                "/ask/checkout/nicepay/confirm-purchase";
            let Redirect_URL =
                window.location.origin +
                GLOBAL_URL +
                "/thankyou/";
            let etotal = document.getElementById("c-total");
            let total = 0;
            if (etotal) {
                total = etotal.innerText;
                //Currency and price for kr
                total = total.replace(/\,/g, '');
            }

            let ecn = document.getElementById("d_contact_number");
            let cn = 0;
            if (ecn) {
                cn = ecn.innerText;
            }
            let useremail = "";
            let username = "";
            if (user) {
                useremail = user.email;
                username = user.firstName;
            } else {
                useremail = getloginsession[0].email;
                username = getloginsession[0].firstName;
            }


            product_data = {
                "email": useremail,
                "name": username,
                "phone": cn,
                "amount": Math.round(total),
                "type": "awesome-shave-kits"
            };

            nicepayAlacarte(product_data, CONFIRM_PURCHASE_URL, Redirect_URL);
        } else {

            onProceedPayment(session_data, payment_intent, data);
        }
     }
    });
    */
    // mask selection
    checkSessionPromo();
});
