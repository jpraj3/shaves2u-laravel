$(document).ready(function () {
    $('form').validate({
        rules: {
            password: {
                required: true,
                minlength: 8
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"
            },
        },
        messages: {
            password:
            {
                required: 'Password is required.',
                minlength: 'Your password must be at least 8 characters long'
            },
            password_confirmation: {
                required: 'Password is required.',
                equalTo: "Please enter the same password as above"
            }
        },
        submitHandler: function(form, event) { // <- pass 'form' argument in
            // $(".submit").attr("disabled", true);    
            if($("#reset_password_form").valid()){
                form.submit(); // <- use 'form' argument here.
            }
        }
    });
})
