// Initialize Elements

let _register_checkout_valid = false;
let _register_valid = false;
let _login_checkout_valid = false;
let _login_valid = false;
let _user_profile_valid = false;
let _user_profile_delivery_valid = false;
let _user_profile_billing_valid = false;
let _user_profile_cards_valid = false;
let _user_profile_delivery_valid_popup = false;
let _user_profile_billing_valid_popup = false;

$.fn.extend({
    trackChanges: function () {
        $(":input", this).change(function () {
            $(this.form).data("changed", true);
        });
    }
    ,
    isChanged: function () {
        return this.data("changed");
    }
});

function initialLandingItems() {
    // console.log("check for forms that are validated");
    if ($("#normal_ecommerce_registration").length) {
        $(':focus').blur();
        validateRegistration("normal_ecommerce_registration");
        let form = document.getElementById('normal_ecommerce_registration');
        let _register_btn = document.getElementById('_register_btn');
        let _x = "";
        if ($('#normal_ecommerce_registration').valid() === true) {
            // console.log("pass validation");
            _x = "{{ route('ecommerce.registration', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}";
            form.action = _x;
            _register_btn.removeAttribute("disabled");
            _register_valid = true;
        } else {
            // console.log("fail validation");
            _x = "";
            form.action = _x;
            _register_btn.setAttribute("disabled", null);
            _register_valid = false;
        }
    }

    if ($("#normal_ecommerce_login").length) {
        $(':focus').blur();
        validateLogin("normal_ecommerce_login");
        let form = document.getElementById('normal_ecommerce_login');
        let _register_btn = document.getElementById('_register_btn');
        let _x = "";
        if ($('#normal_ecommerce_login').valid() === true) {
            // console.log("pass validation");
            _x = "{{ route('login', ['langCode'=>strtolower(json_decode(session()->get('currentCountry'), true)['urlLang']), 'countryCode'=> strtolower(json_decode(session()->get('currentCountry'), true)['codeIso'])]) }}";
            form.action = _x;
            _register_btn.removeAttribute("disabled");
            _register_valid = true;
        } else {
            // console.log("fail validation");
            _x = "";
            form.action = _x;
            _register_btn.setAttribute("disabled", null);
            _register_valid = false;
        }
    }

    if ($("#form_pe_user").length) {
        // console.log("form_pe_user found");
        $(':focus').blur();
        v_p_user("form_pe_user");
        let _register_btn = document.getElementById('pu_a_save');
        if ($('#form_pe_user').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _register_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _register_valid = false;
        }
    }
}

function validateRegistration(element_name) {
    let birthday_rules = {
        required: true,
    };


    $.validator.addMethod("validpassword", function(value, element) {
        if (typeof formMode !== 'undefined') {
            if (formMode === "register")  {
                return this.optional(element) ||
                    /^.*(?=.*[A-Z])(?=.*[\d]).*$/.test(value);
            }
            else {
                return true;
            }
        }
        else {
            return this.optional(element) ||
                /^.*(?=.*[A-Z])(?=.*[\d]).*$/.test(value);
        }
        }, trans('validation.custom.validation.password.validpassword', {})
    );

    $('#' + element_name).validate({
        groups: {
            birth_date: "day-of-birth month-of-birth year-of-birth",
            terms_consent: "tos_agree privacy_policy_agree personal_info_agree min_age_agree"
        },
        rules: {
            name: {
                required: true,
                maxlength: 100
            },
            password: {
                required: true,
                minlength: 8,
                validpassword: function (element) {
                    return element.value;
                },
            },
            email: {
                required: true,
                maxlength: 100,
                email: true,
                remote: function () {
                    remote_data = {
                        url: window.location.origin + GLOBAL_URL_V2 + '/api/check/unique',
                        type: "post"
                    };
                    if (typeof formMode !== 'undefined') {
                        if (formMode === "register")  {
                            return remote_data;
                        }
                        else {
                            return true;
                        }
                    } 
                    else {
                        return remote_data;
                    }
                }
            },
            "day-of-birth": birthday_rules,
            "month-of-birth": birthday_rules,
            "year-of-birth": birthday_rules,
            tos_agree: {
                required: true
            },
            privacy_policy_agree: {
                required: true
            },
            personal_info_agree: {
                required: true
            },
            min_age_agree: {
                required: true
            },
        },

        messages: {
            name:
                {
                    required: trans('validation.custom.validation.name.required', {}),
                    maxlength: trans('validation.custom.validation.name.maxlength', {attribute: "name", max: 100})
                },
            email: {
                required: trans('validation.custom.validation.email.required', {}),
                maxlength: trans('validation.custom.validation.email.maxlength', {max: 100}),
                email: trans('validation.custom.validation.email.required', {}),
                remote: trans('validation.custom.validation.email.email_exists', {}),
            },
            password: {
                required: trans('validation.custom.validation.password.required', {}),
                minlength: trans('validation.custom.validation.password.minlength', {min: 8}),
                validpassword: trans('validation.custom.validation.password.validpassword', {})
            },
            "day-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            "month-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            "year-of-birth": {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            tos_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            privacy_policy_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            personal_info_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
            min_age_agree: {
                required: trans('validation.custom.validation.terms_consent.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "day-of-birth" || element.attr("name") == "month-of-birth" || element.attr("name") == "year-of-birth") {
                error.appendTo('#birthday_error');
            } else if (element.attr("name") == "name") {
                error.appendTo('#name_error');
            } else if (element.attr("name") == "password") {
                error.appendTo('#password_error');
            } else if (element.attr("name") == "email") {
                error.appendTo('#email_error');
            } else if (element.attr("name") == "tos_agree" || element.attr("name") == "privacy_policy_agree" || element.attr("name") == "personal_info_agree" ||  element.attr("name") == "min_age_agree") {
                error.appendTo('#required_checked_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function validateLogin(element_name) {
    $('#' + element_name).validate({
        rules: {
            password: {
                required: true,
                minlength: 8,
            },
            email: {
                required: true,
                email: true,
                maxlength: 100,
            },
        },
        messages: {
            email: {
                required: trans('validation.custom.validation.email.required', {}),
                email: trans('validation.custom.validation.email.required', {}),
                maxlength: trans('validation.custom.validation.email.maxlength', {max: 100}),
            },
            password: {
                required: trans('validation.custom.validation.password.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "password") {
                error.appendTo('#password_error');
            } else if (element.attr("name") == "email") {
                error.appendTo('#email_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_user(element_name) {
    let birthday_rules = {
        required: true,
    };

    let mindigits = 0;
    let maxdigits = 100;
    let allowz = true;

    if (userCountryId) {
        if (userCountryId == 1) {
            mindigits = 9;
            maxdigits = 10;
        } else if (userCountryId == 2 || userCountryId == 7) {
            mindigits = 8;
            maxdigits = 8;
        } else if (userCountryId == 8) {
            mindigits = 11;
            maxdigits = 11;
        } else if (userCountryId == 9) {
            mindigits = 9;
            maxdigits = 9;
        }

        if (userCountryId == 8) {
            allowz = true;
        }
        else {
            allowz = false;
        }
    }

    $.validator.addMethod(
        "allowzero",
        function(value,element) {
            return (value.charAt(0) != '0');
        }, trans('validation.custom.validation.contact_number.allowzero', {})
    );

    $('#' + element_name).validate({
        groups: {
            birth_date: "pu_e_birthday_day pu_e_birthday_month pu_e_birthday_year"
        },
        rules: {
            pu_e_password: {
                required: function () {
                    return $('#pu_e_password').val().length > 0;
                },
                minlength: function () {
                    if ($('#pu_e_password').val().length > 0) {
                        return 8;
                    }
                },
            },
            pu_e_email: {
                email: true,
                required: function () {
                    return $('#pu_e_email').val().length > 0;
                },
                maxlength: function () {
                    if ($('#pu_e_email').val().length > 100) {
                        return 100;
                    }
                },
            },
            pu_e_fullname: {
                required: true,
                maxlength: 100
            },
            pu_e_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            pu_e_birthday_day: birthday_rules,
            pu_e_birthday_month: birthday_rules,
            pu_e_birthday_year: birthday_rules
        },

        messages: {
            pu_e_fullname:
                {
                    required: trans('validation.custom.validation.name.required', {}),
                    maxlength: trans('validation.custom.validation.name.maxlength', {attribute: "name", max: 100})
                },
            pu_e_email: {
                email: trans('validation.custom.validation.email.email', {}),
                required: trans('validation.custom.validation.email.required', {}),
                maxlength: trans('validation.custom.validation.email.maxlength', {max: 100})
            },
            pu_e_password: {
                required: trans('validation.custom.validation.password.required', {}),
                minlength: trans('validation.custom.validation.password.minlength', {minlength: 8})
            },
            pu_e_phone: {
                required: trans('validation.custom.validation.contact_number.required', {}),
                minlength: trans('validation.custom.validation.contact_number.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.contact_number.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.contact_number.allowzero', {})
            },
            pu_e_birthday_day: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            pu_e_birthday_month: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
            pu_e_birthday_year: {
                required: trans('validation.custom.validation.birthday.required', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pu_e_birthday_day" || element.attr("name") == "pu_e_birthday_month" || element.attr("name") == "pu_e_birthday_year") {
                error.appendTo('#pu_e_birthday_error');
            } else if (element.attr("name") == "pu_e_fullname") {
                error.appendTo('#pu_e_fullname_error');
            } else if (element.attr("name") == "pu_e_email") {
                error.appendTo('#pu_e_email_error');
            }else if (element.attr("name") == "pu_e_phone") {
                error.appendTo('#pu_e_phone_error');
            } else if (element.attr("name") == "pu_e_password") {
                error.appendTo('#pu_e_password_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_delivery(element_name) {
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            pd_e_ssn: {
                required: true
            },
            pd_e_address: {
                required: true
            },
            pd_e_city: {
                required: true
            },
            pd_e_portalCode: {
                required: allowposcode,
                maxlength: 6
            },
            pd_e_state: {
                required: true
            },
        },

        messages: {
            pd_e_ssn: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            pd_e_address: {
                required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
            },
            pd_e_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            pd_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            pd_e_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pd_e_address") {
                error.appendTo('#pd_e_address_error');
            } else if (element.attr("name") == "pd_e_ssn") {
                error.appendTo('#pd_e_ssn_error');
            } else if (element.attr("name") == "pd_e_city") {
                error.appendTo('#pd_e_city_error');
            } else if (element.attr("name") == "pd_e_portalCode") {
                error.appendTo('#pd_e_portalCode_error');
            } else if (element.attr("name") == "pd_e_state") {
                error.appendTo('#pd_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_billing(element_name) {
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            pb_e_address: {
                required: true
            },
            pb_e_city: {
                required: true
            },
            pb_e_portalCode: {
                required: allowposcode,
                maxlength: 6
            },
            pb_e_state: {
                required: true
            },
        },

        messages: {
            pb_e_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
                },
            pb_e_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            pb_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            pb_e_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pb_e_address") {
                error.appendTo('#pb_e_address_error');
            } else if (element.attr("name") == "pb_e_city") {
                error.appendTo('#pb_e_city_error');
            } else if (element.attr("name") == "pb_e_portalCode") {
                error.appendTo('#pb_e_portalCode_error');
            } else if (element.attr("name") == "pb_e_state") {
                error.appendTo('#pb_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}


function v_e_o_delivery(element_name, count) {
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({

        errorPlacement: function (error, element) {
            // console.log(element.attr("name"))
            if (element.attr("name") == "pd_e_address_"+count) {
                error.appendTo('#pd_e_address_error_'+count);
            } else if (element.attr("name") == "pd_e_ssn_"+count) {
                error.appendTo('#pd_e_ssn_error_'+count);
            } else if (element.attr("name") == "pd_e_city_"+count) {
                error.appendTo('#pd_e_city_error_'+count);
            } else if (element.attr("name") == "pd_e_portalCode_"+count) {
                error.appendTo('#pd_e_portalCode_error_'+count);
            } else if (element.attr("name") == "pd_e_state_"+count) {
                error.appendTo('#pd_e_state_error_'+count);
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#pd_e_ssn_" + count).rules("add", {
        required: true,
        messages: { required: trans('validation.custom.validation.address.ssn.required', { attribute: "SSN" }), }
    });

    $("#pd_e_address_" + count).rules("add", {
        required: true,
        messages: { required: trans('validation.custom.validation.address.address.required', { attribute: "address" }), }
    });

    $("#pd_e_city_" + count).rules("add", {
        required: true,
        messages: { required: trans('validation.custom.validation.address.city.required', { attribute: "city" }), }
    });
    $("#pd_e_portalCode_" + count).rules("add", {
        required: allowposcode,
        maxlength: 6,
        messages: {
            required: trans('validation.custom.validation.address.portalCode.required', { attribute: "portalCode" }),
            maxlength: trans('validation.custom.validation.address.portalCode.maxlength', { length: 6, attribute: "portalCode" })
        }
    });
    $("#pd_e_state_" + count).rules("add", {
        required: true,
        messages: { required: trans('validation.custom.validation.address.state.required', { attribute: "state" }) }
    });

}


function v_p_delivery_popup(element_name) {
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            pd_e_ssn_popup: {
                required: true
            },
            pd_e_address_popup: {
                required: true
            },
            pd_e_city_popup: {
                required: true
            },
            pd_e_portalCode_popup: {
                required: allowposcode,
                maxlength: 6
            },
            pd_e_state_popup: {
                required: true
            },
        },

        messages: {
            pd_e_ssn_popup: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            pd_e_address_popup:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
                },
            pd_e_city_popup: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            pd_e_portalCode_popup: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            pd_e_state_popup: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pd_e_address_popup") {
                error.appendTo('#pd_e_address_popup_error');
            } else if (element.attr("name") == "pd_e_ssn_popup") {
                error.appendTo('#pd_e_ssn_popup_error');
            } else if (element.attr("name") == "pd_e_city_popup") {
                error.appendTo('#pd_e_city_popup_error');
            } else if (element.attr("name") == "pd_e_portalCode_popup") {
                error.appendTo('#pd_e_portalCode_popup_error');
            } else if (element.attr("name") == "pd_e_state_popup") {
                error.appendTo('#pd_e_state_popup_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_billing_popup(element_name) {
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            pb_e_address_popup: {
                required: true
            },
            pb_e_city_popup: {
                required: true
            },
            pb_e_portalCode_popup: {
                required: allowposcode,
                maxlength: 6
            },
            pb_e_state_popup: {
                required: true
            },
        },

        messages: {
            pb_e_address_popup:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
                },
            pb_e_city_popup: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            pb_e_portalCode_popup: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            pb_e_state_popup: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pb_e_address_popup") {
                error.appendTo('#pb_e_address_popup_error');
            } else if (element.attr("name") == "pb_e_city_popup") {
                error.appendTo('#pb_e_city_popup_error');
            } else if (element.attr("name") == "pb_e_portalCode_popup") {
                error.appendTo('#pb_e_portalCode_popup_error');
            } else if (element.attr("name") == "pb_e_state_popup") {
                error.appendTo('#pb_e_state_popup_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_p_cards(element_name) {
    $('#' + element_name).validate({
        rules: {
            pc_e_cardnumber_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#pc_e_cardnumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            pc_e_expiry: {
                required: true,
                minlength: 3
            },
            pc_e_cvv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            pc_e_cardnumber_masked:
                {
                    required: trans('validation.custom.validation.card.cardnumber.required', {}),
                    minlength: function() {
                        var cardInit = $("#pc_e_cardnumber").val().substring(0, 1);
                        if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                        }
                        else {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                        }
                    }
                },
            pc_e_expiry: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            pc_e_cvv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "pc_e_cardnumber_masked") {
                error.appendTo('#pc_e_cardnumber_error');
            } else if (element.attr("name") == "pc_e_expiry") {
                error.appendTo('#pc_e_expiry_error');
            } else if (element.attr("name") == "pc_e_cvv") {
                error.appendTo('#pc_e_cvv_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_cards(element_name) {
    // console.log(element_name);
    $('#' + element_name).validate({
        rules: {
            spc_e_cardNumber_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#spc_e_cardNumber").val().substring(0, 1);
                    if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            spc_e_expirydate: {
                required: true,
                minlength: 4
            },
            spc_e_ccv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            spc_e_cardNumber_masked:
                {
                    required: trans('validation.custom.validation.card.cardnumber.required', {}),
                    minlength: function() {
                        var cardInit = $("#spc_e_cardNumber").val().substring(0, 1);
                        if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                        }
                        else {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                        }
                    }
                },
            spc_e_expirydate: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            spc_e_ccv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "spc_e_cardNumber_masked") {
                error.appendTo('#spc_e_cardNumber_error');
            } else if (element.attr("name") == "spc_e_expirydate") {
                error.appendTo('#spc_e_expirydate_error');
            } else if (element.attr("name") == "spc_e_ccv") {
                error.appendTo('#spc_e_ccv_error2');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_delivery(element_name) {
 
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            spd_e_ssn: {
                required: true
            },
            spd_e_address: {
                required: true
            },
            spd_e_city: {
                required: true
            },
            spd_e_portalCode: {
                required: allowposcode,
                maxlength: 6
            },
            spd_e_state: {
                required: true
            },
        },

        messages: {
            spd_e_ssn: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            spd_e_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
                },
            spd_e_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            spd_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            spd_e_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "spd_e_address") {
                error.appendTo('#spd_e_address_error');
            } else if (element.attr("name") == "spd_e_ssn") {
                error.appendTo('#spd_e_ssn_error');
            } else if (element.attr("name") == "spd_e_city") {
                error.appendTo('#spd_e_city_error');
            } else if (element.attr("name") == "spd_e_portalCode") {
                error.appendTo('#spd_e_portalCode_error');
            } else if (element.attr("name") == "spd_e_state") {
                error.appendTo('#spd_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_sp_billing(element_name) {
    
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }
    $('#' + element_name).validate({
        rules: {
            spb_e_address: {
                required: true
            },
            spb_e_city: {
                required: true
            },
            spb_e_portalCode: {
                required: allowposcode,
                maxlength: 6
            },
            spb_e_state: {
                required: true
            },
        },

        messages: {
            spb_e_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "address"}),
                },
            spb_e_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "city"}),
            },
            spb_e_portalCode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "portalCode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "portalCode"
                })
            },
            spb_e_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "spb_e_address") {
                error.appendTo('#spb_e_address_error');
            } else if (element.attr("name") == "spb_e_city") {
                error.appendTo('#spb_e_city_error');
            } else if (element.attr("name") == "spb_e_portalCode") {
                error.appendTo('#spb_e_portalCode_error');
            } else if (element.attr("name") == "spb_e_state") {
                error.appendTo('#spb_e_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_checkout_address(element_name) {

    let mindigits = 0;
    let maxdigits = 100;
    let allowz = true;
    let allowposcode = true;
    if (userCountryId) {
        if (userCountryId == 1) {
            mindigits = 9;
            maxdigits = 10;
        } else if (userCountryId == 2 || userCountryId == 7) {
            mindigits = 8;
            maxdigits = 8;
        } else if (userCountryId == 8) {
            mindigits = 11;
            maxdigits = 11;
        } else if (userCountryId == 9) {
            mindigits = 9;
            maxdigits = 9;
        }

        if (userCountryId == 8) {
            allowz = true;
        }
        else {
            allowz = false;
        }
        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }

    $.validator.addMethod(
        "allowzero",
        function(value,element) {
            return (value.charAt(0) != '0');
        }, trans('validation.custom.validation.contact_number.allowzero', {})
    );

    $('#' + element_name).validate({
        rules: {
            delivery_name: {
                required: true
            },
            SSN: {
                required: true
            },
            delivery_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            delivery_address: {
                required: true
            },
            delivery_city: {
                required: true
            },
            delivery_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            delivery_state: {
                required: true
            },
            billing_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            billing_address: {
                required: true
            },
            billing_city: {
                required: true
            },
            billing_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            billing_state: {
                required: true
            },
        },

        messages: {
            delivery_name:
                {
                    required: trans('validation.custom.validation.address.name.required', {attribute: "recipient name"}),
                },
            SSN: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            delivery_phone: {
                required: trans('validation.custom.validation.address.phone.required', {attribute: "phone"}),
                minlength: trans('validation.custom.validation.contact_number.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.contact_number.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.contact_number.allowzero', {})
            },
            delivery_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "Unit No./Street/Area"}),
                },
            delivery_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "town"}),
            },
            delivery_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "postcode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "postcode"
                })
            },
            delivery_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            },
            billing_phone: {
                required: trans('validation.custom.validation.address.phone.required', {attribute: "phone"}),
                minlength: trans('validation.custom.validation.contact_number.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.contact_number.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.contact_number.allowzero', {})
            },
            billing_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "Unit No./Street/Area"}),
                },
            billing_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "town"}),
            },
            billing_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "postcode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "postcode"
                })
            },
            billing_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "delivery_name") {
                error.appendTo('#pd_name_error');
            } else if (element.attr("name") == "delivery_phone") {
                error.appendTo('#pd_phone_error');
            } else if (element.attr("name") == "delivery_address") {
                error.appendTo('#pd_address_error');
            } else if (element.attr("name") == "delivery_city") {
                error.appendTo('#pd_city_error');
            } else if (element.attr("name") == "delivery_postcode") {
                error.appendTo('#pd_portalCode_error');
            } else if (element.attr("name") == "delivery_state") {
                error.appendTo('#pd_state_error');
            } else if (element.attr("name") == "billing_phone") {
                error.appendTo('#pb_phone_error');
            } else if (element.attr("name") == "billing_address") {
                error.appendTo('#pb_address_error');
            } else if (element.attr("name") == "billing_city") {
                error.appendTo('#pb_city_error');
            } else if (element.attr("name") == "billing_postcode") {
                error.appendTo('#pb_portalCode_error');
            } else if (element.attr("name") == "billing_state") {
                error.appendTo('#pb_state_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_checkout_e_address(element_name) {

    let mindigits = 0;
    let maxdigits = 100;
    let allowz = true;
    let allowposcode = true;

    if (userCountryId) {
        if (userCountryId == 1) {
            mindigits = 9;
            maxdigits = 10;
        } else if (userCountryId == 2 || userCountryId == 7) {
            mindigits = 8;
            maxdigits = 8;
        } else if (userCountryId == 8) {
            mindigits = 11;
            maxdigits = 11;
        } else if (userCountryId == 9) {
            mindigits = 9;
            maxdigits = 9;
        }

        if (userCountryId == 8) {
            allowz = true;
        }
        else {
            allowz = false;
        }

        if (userCountryId == 2) {
            allowposcode = false;
        }
        else {
            allowposcode = true;
        }
    }

    $.validator.addMethod(
        "allowzero",
        function(value,element) {
            return (value.charAt(0) != '0');
        }, trans('validation.custom.validation.contact_number.allowzero', {})
    );

    $('#' + element_name).validate({
        rules: {
            delivery_name: {
                required: true
            },
            SSN: {
                required: true,
            },
            delivery_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            delivery_address: {
                required: true
            },
            delivery_city: {
                required: true
            },
            delivery_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            delivery_state: {
                required: true
            },
            billing_phone: {
                required: true,
                minlength: mindigits,
                maxlength: maxdigits,
                allowzero: function (element) {
                    if (!allowz) {
                        return element.value;
                    }
                },
            },
            billing_address: {
                required: true
            },
            billing_city: {
                required: true
            },
            billing_postcode: {
                required: allowposcode,
                maxlength: 6
            },
            billing_state: {
                required: true
            }
        },

        messages: {
            delivery_name:
                {
                    required: trans('validation.custom.validation.address.name.required', {attribute: "recipient name"}),
                },
            SSN: {
                required: trans('validation.custom.validation.address.ssn.required', {attribute: "SSN"}),
            },
            delivery_phone: {
                required: trans('validation.custom.validation.address.phone.required', {attribute: "phone"}),
                minlength: trans('validation.custom.validation.contact_number.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.contact_number.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.contact_number.allowzero', {})
            },
            delivery_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "Unit No./Street/Area"}),
                },
            delivery_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "town"}),
            },
            delivery_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "postcode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "postcode"
                })
            },
            delivery_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            },
            billing_phone: {
                required: trans('validation.custom.validation.address.phone.required', {attribute: "phone"}),
                minlength: trans('validation.custom.validation.contact_number.minlength', {min: mindigits}),
                maxlength: trans('validation.custom.validation.contact_number.maxlength', {max: maxdigits}),
                allowzero: trans('validation.custom.validation.contact_number.allowzero', {})
            },
            billing_address:
                {
                    required: trans('validation.custom.validation.address.address.required', {attribute: "Unit No./Street/Area"}),
                },
            billing_city: {
                required: trans('validation.custom.validation.address.city.required', {attribute: "town"}),
            },
            billing_postcode: {
                required: trans('validation.custom.validation.address.portalCode.required', {attribute: "postcode"}),
                maxlength: trans('validation.custom.validation.address.portalCode.maxlength', {
                    length: 6,
                    attribute: "postcode"
                })
            },
            billing_state: {
                required: trans('validation.custom.validation.address.state.required', {attribute: "state"})
            }
        }, errorPlacement: function (error, element) {
            if (element.attr("name") == "delivery_name") {
                error.appendTo('#pd_name_e_error');
            } else if (element.attr("name") == "delivery_phone") {
                error.appendTo('#pd_phone_e_error');
            } else if (element.attr("name") == "delivery_address") {
                error.appendTo('#pd_address_e_error');
            } else if (element.attr("name") == "delivery_city") {
                error.appendTo('#pd_city_e_error');
            } else if (element.attr("name") == "delivery_postcode") {
                error.appendTo('#pd_portalCode_e_error');
            } else if (element.attr("name") == "delivery_state") {
                error.appendTo('#pd_state_e_error');
            } else if (element.attr("name") == "billing_phone") {
                error.appendTo('#pb_phone_e_error');
            } else if (element.attr("name") == "billing_address") {
                error.appendTo('#pb_address_e_error');
            } else if (element.attr("name") == "billing_city") {
                error.appendTo('#pb_city_e_error');
            } else if (element.attr("name") == "billing_postcode") {
                error.appendTo('#pb_portalCode_e_error');
            } else if (element.attr("name") == "billing_state") {
                error.appendTo('#pb_state_e_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function v_rp_email(element_name) {
    $('#' + element_name).validate({
        rules: {
            email: {
                required: true,
                email: true
            },
        },

        messages: {
            email:
                {
                    required: trans('validation.custom.validation.email.required', {}),
                    email: trans('validation.custom.validation.email.email', {attribute: 'email'})
                },
        }, errorPlacement: function (error, element) {
            error.appendTo('#email_error');
        }
    });
}

function v_checkout_cards(element_name) {
    // console.log(element_name);
    $('#' + element_name).validate({
        rules: {
            card_number_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#card-number").val().substring(0, 1);
                    if (cardInit === "2" || cardInit === "3" || cardInit === "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            card_expiry_date: {
                required: true,
                minlength: 7
            },
            card_cvv: {
                required: true,
                minlength: 3
            },
        },

        messages: {
            card_number_masked:
                {
                    required: trans('validation.custom.validation.card.cardnumber.required', {}),
                    minlength: function() {
                        var cardInit = $("#card-number").val().substring(0, 1);
                        if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                        }
                        else {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                        }
                    }
                },
            card_expiry_date: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            card_cvv: {
                required: trans('validation.custom.validation.card.cvv.required', {}),
                minlength: trans('validation.custom.validation.card.cvv.minlength', {})
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "card_number_masked") {
                error.appendTo('#c_cardNumber_error');
            } else if (element.attr("name") == "card_expiry_date") {
                error.appendTo('#c_expirydate_error');
            } else if (element.attr("name") == "card_cvv") {
                error.appendTo('#c_ccv_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}


function v_checkout_cards_kr(element_name) {
    // console.log(element_name);
    $('#' + element_name).validate({
        rules: {
            card_number_masked: {
                required: true,
                minlength: function() {
                    var cardInit = $("#card-number").val().substring(0, 1);
                    if (cardInit === "2" || cardInit === "3" || cardInit === "6") {
                        return 17;
                    }
                    else {
                        return 19;
                    }
                }
            },
            card_expiry_date: {
                required: true,
                minlength: 9
            },
            card_birth: {
                required: true,
            },
            card_password: {
                required: true,
            },
        },

        messages: {
            card_number_masked:
                {
                    required: trans('validation.custom.validation.card.cardnumber.required', {}),
                    minlength: function() {
                        var cardInit = $("#card-number").val().substring(0, 1);
                        if (cardInit == "2" || cardInit == "3" || cardInit == "6") {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 14});
                        }
                        else {
                            return trans('validation.custom.validation.card.cardnumber.minlength', {minlength: 16});
                        }
                    }
                },
            card_expiry_date: {
                required: trans('validation.custom.validation.card.cardexpiry.required', {}),
                minlength: trans('validation.custom.validation.card.cardexpiry.minlength', {})
            },
            card_birth: {
                required: trans('validation.custom.validation.card.birth.required', {}),
            },
            card_password: {
                required: trans('validation.custom.validation.card.password.required', {}),
            },
        }, errorPlacement: function (error, element) {
            // console.log(error, element);
            if (element.attr("name") == "card_number_masked") {
                error.appendTo('#c_cardNumber_error');
            } else if (element.attr("name") == "card_expiry_date") {
                error.appendTo('#c_expirydate_error');
            } else if (element.attr("name") == "card_birth") {
                error.appendTo('#c_birth_error');
            } else if (element.attr("name") == "card_password") {
                error.appendTo('#c_password_error');
            } else {
                error.insertAfter(element);
            }
        }
    });
}

////////////////////////////////// ON LOAD FUNCTIONS ///////////////////////////////////
$(function () {

    // setTimeout(initialLandingItems, 1000);


    // validate normal registration
    $("#new_user_register").keyup(function () {
        validateRegistration("new_user_register");
        let _register_btn = document.getElementById('_register_btn');
        if ($('#new_user_register').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _register_checkout_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _register_checkout_valid = false;
        }
    })


    // validate user edit profile
    $("#form_pe_user").keyup(function () {
        v_p_user("form_pe_user");
        let _register_btn = document.getElementById('pu_a_save');
        if ($('#form_pe_user').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_valid = false;
        }
    })

    // validate user edit profile
    $("#form_pe_delivery").keyup(function () {
        let changes = $("#form_pe_delivery").trackChanges();
        let changes2 = $("#form_pe_delivery").isChanged()
        v_p_delivery("form_pe_delivery");
        let _register_btn = document.getElementById('pd_a_save');
        if ($('#form_pe_delivery').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_delivery_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_delivery_valid = false;
        }
    })

    // validate user edit profile
    $("#form_pe_billing").keyup(function () {
        v_p_billing("form_pe_billing");
        let _register_btn = document.getElementById('pb_a_save');
        if ($('#form_pe_billing').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_billing_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_billing_valid = false;
        }
    })

    // validate user edit profile
    $("#form_pe_delivery_popup").keyup(function () {
        // console.log("form_pe_delivery_popup");
        let changes = $("#form_pe_delivery_popup").trackChanges();
        let changes2 = $("#form_pe_delivery_popup").isChanged()
        v_p_delivery_popup("form_pe_delivery_popup");
        let _register_btn = document.getElementById('pd_a_save_popup');
        if ($('#form_pe_delivery_popup').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_delivery_valid_popup = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_delivery_valid_popup = false;
        }
    })

    // validate user edit profile
    $("#form_pe_billing_popup").keyup(function () {
        // console.log("form_pe_billing_popup");
        v_p_billing_popup("form_pe_billin_popupg");
        let _register_btn = document.getElementById('pb_a_save_popup');
        if ($('#form_pe_billing_popup').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_billing_valid_popup = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_billing_valid_popup = false;
        }
    })

    // validate user edit profile
    $("#form_pe_card").keyup(function () {
        v_p_cards("form_pe_card");
        let _register_btn = document.getElementById('pc_a_save');
        if ($('#form_pe_card').valid() === true) {
            // console.log("pass validation");
            _register_btn.removeAttribute("disabled");
            _user_profile_cards_valid = true;
        } else {
            // console.log("fail validation");
            _register_btn.setAttribute("disabled", null);
            _user_profile_cards_valid = false;
        }
    })
    $(document).ready(function () {
        v_rp_email("form_rp_email");
        $("#form_rp_email").change(function (event) {
            v_rp_email("form_rp_email");
        })
    });

});
