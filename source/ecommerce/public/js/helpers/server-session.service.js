
// Define methods
var SESSION_SET = "set";
var SESSION_GET = "get";
var SESSION_CLEAR = "clear";

// Define keys
var SESSION_SELECTION_TRIAL_PLAN = "selection_trial_plan";
var SESSION_CHECKOUT_TRIAL_PLAN = "checkout_trial_plan";
var SESSION_SELECTION_CUSTOM_PLAN = "selection_custom_plan";
var SESSION_CHECKOUT_CUSTOM_PLAN = "checkout_custom_plan";
var SESSION_CHECKOUT_ASK = "checkout_ask";
var SESSION_SELECTION_ASK = "checkout_selection_ask";
var SESSION_CHECKOUT_ALACARTE = "checkout_alacarte";
var SESSION_URL_PARAMETERS = "url_parameters";
var SESSION_UTM_PARAMETERS = "utm_parameters";
var SESSION_REFERRAL_PROGRAM_INFO = "referral_program_info";
var SESSION_LOGIN = "isLoggedIn";
var SESSION_EDIT_SHAVE_PLANS = "session_edit_shave_plans";
var SESSION_SELLER_INFO = "seller";
var STRIPE_PAYMENT_FAIL = "stripe_payment_fail";
function session(session_key, method, json_data) {
    return $.ajax({
        url: GLOBAL_URL_V2 + "/ajax/session",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        cache: false,
        data: {
            "session_key": session_key,
            "method": method,
            "data": json_data
        },
    })
}
