// copy to clipboard function
function copyToClipboard(type, element_name) {
    var copyText = ''
    var range = '';
    if (type == 'id') {
        copyText = document.getElementById(element_name);
        range = document.createRange();
        window.getSelection().removeAllRanges();
        range.selectNode(copyText);
        window.getSelection().addRange(range);
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
    } else if (type == 'class') {
        copyText = document.getElementsByClassName(element_name);
        range = document.createRange();
        window.getSelection().removeAllRanges();
        range.selectNode(copyText);
        window.getSelection().addRange(range);
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
    }

    Swal.fire({
        title: trans('website_contents.global.link_copied', {}),
        // heightAuto: false,
        icon: 'warning',
        backdrop: true,
        // background: 'white',
        // position: 'left',
        allowOutsideClick: true,
        allowEscapeKey: true,
        allowEnterKey: true,
        showCloseButton: true,
        showCancelButton: false,
        showConfirmButton: false,
        focusConfirm: false,
    })
}

// social sharing for mobile phones (supports Android Chrome, Safari)
function socialSharing(link) {
    console.log(link);
    if (navigator.share) {
        alert(link);
        navigator.share({
            title: 'Shaves2U',
            text: 'Referral Program',
            url: link,
        })
            // .then(() => console.log('Successful share'))
            // .catch((error) => console.log('Error sharing', error));
    } else {
        // console.log("not mobile");
    }
}

// get url params using link or address bar
function getUrlParam(link, parameter, defaultvalue) {
    if (link === null) {
        var urlparameter = defaultvalue;
        if (window.location.href.indexOf(parameter) > -1) {
            urlparameter = getUrlVars()[parameter];
        }
    } else {
        var urlparameter = defaultvalue;
        if (window.location.href.indexOf(parameter) > -1) {
            urlparameter = getLinkVars(link)[parameter];
        }
    }

    return urlparameter;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}
function getLinkVars(link) {
    var vars = {};
    var parts = link.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });

    // console.log(vars);
    return vars;
}

function AJAX(url, method, data) {
    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: method,
        cache: false,
        data: data,
    })
}

