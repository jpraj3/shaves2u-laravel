
function addNewAddress(type) {
    let _validation_result = '';
    let form_data = '';
    let template = '';
    $.ajax({
        type: 'POST',
        url: GLOBAL_URL_V2 + "/templates/popup",
        dataType: "html",
        data: { template_name: 'address', data: currentCountryData },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        async: false,
        cache: false,
        success: function (data) {
            template = data;
            if (template) {
                Swal.fire({
                    html: template,
                    width: '40%',
                    heightAuto: false,
                    backdrop: true,
                    background: 'white',
                    position: 'left',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: trans('website_contents.global.content.save'),
                    customClass: {
                        confirmButton: 'btn btn-load-more w-50 mb-3',
                    },
                    preConfirm: () => {
                        let _check = popup_address_Validation();
                        _validation_result = _check;
                        form_data = _check[2]
                        return _check[0];
                    }
                }).then((result) => {
                    // console.log(_validation_result);
                    if (result.value) {
                        if (_validation_result[1] !== null && _validation_result[1] === "delivery and shipping") {
                            p_address_actions_popup("save", "delivery and shipping", form_data);
                        } else if (_validation_result[1] !== null && _validation_result[1] === "delivery only") {

                            p_address_actions_popup("save", "delivery only", form_data);
                        } else {
                            return 0;
                        }

                        // Swal.fire(
                        //     'Address added!',
                        //     'success'
                        // )
                    }
                })
            }
        }
    });
}

function addNewAddressMobile(type) {
    let _validation_result = '';
    let form_data = '';
    let template = '';
    $.ajax({
        type: 'GET',
        url: GLOBAL_URL_V2 + "/templates/popup",
        dataType: "html",
        data: { template_name: 'address', data: null },
        async: false,
        cache: false,
        success: function (data) {
            template = data;
            if (template) {
                Swal.fire({
                    html: template,
                    width: '100%',
                    heightAuto: false,
                    backdrop: true,
                    background: 'white',
                    position: 'left',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: trans('website_contents.global.content.save'),
                    customClass: {
                        confirmButton: 'btn btn-load-more w-100 mb-3',
                    },
                    preConfirm: () => {
                        let _check = popup_address_Validation();
                        _validation_result = _check;
                        form_data = _check[2]
                        return _check[0];
                    }
                }).then((result) => {
                    // console.log(_validation_result);
                    if (result.value) {
                        if (_validation_result[1] !== null && _validation_result[1] === "delivery and shipping") {
                            p_address_actions_popup("save", "delivery and shipping", form_data);
                        } else if (_validation_result[1] !== null && _validation_result[1] === "delivery only") {

                            p_address_actions_popup("save", "delivery only", form_data);
                        } else {
                            return 0;
                        }

                        // Swal.fire(
                        //     'Address added!',
                        //     'success'
                        // )
                    }
                })
            }
        }
    });
}

function addNewAddressko(type) {
    let _validation_result = '';
    let form_data = '';
    let template = '';
    $.ajax({
        type: 'GET',
        url: GLOBAL_URL_V2 + "/templates/popupko",
        dataType: "html",
        data: { template_name: 'address', data: null },
        async: false,
        cache: false,
        success: function (data) {
            template = data;
            if (template) {
                Swal.fire({
                    html: template,
                    width: '40%',
                    heightAuto: false,
                    backdrop: true,
                    background: 'white',
                    position: 'left',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: trans('website_contents.global.content.save'),
                    customClass: {
                        confirmButton: 'btn btn-load-more w-50 mb-3',
                    },
                    preConfirm: () => {
                        let _check = popup_address_Validation();
                        _validation_result = _check;
                        form_data = _check[2]
                        return _check[0];
                    }
                }).then((result) => {
                    // console.log(_validation_result);
                    if (result.value) {
                        if (_validation_result[1] !== null && _validation_result[1] === "delivery and shipping") {
                            p_address_actions_popup("save", "delivery and shipping", form_data);
                        } else if (_validation_result[1] !== null && _validation_result[1] === "delivery only") {

                            p_address_actions_popup("save", "delivery only", form_data);
                        } else {
                            return 0;
                        }

                        // Swal.fire(
                        //     'Address added!',
                        //     'success'
                        // )
                    }
                })
            }
        }
    });
}

function addNewAddresskoMobile(type) {
    let _validation_result = '';
    let form_data = '';
    let template = '';
    $.ajax({
        type: 'GET',
        url: GLOBAL_URL_V2 + "/templates/popupko",
        dataType: "html",
        data: { template_name: 'address', data: null },
        async: false,
        cache: false,
        success: function (data) {
            template = data;
            if (template) {
                Swal.fire({
                    html: template,
                    width: '100%',
                    heightAuto: false,
                    backdrop: true,
                    background: 'white',
                    position: 'left',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    allowEnterKey: true,
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: true,
                    focusConfirm: false,
                    confirmButtonText: trans('website_contents.global.content.save'),
                    customClass: {
                        confirmButton: 'btn btn-load-more w-100 mb-3',
                    },
                    preConfirm: () => {
                        let _check = popup_address_Validation();
                        _validation_result = _check;
                        form_data = _check[2]
                        return _check[0];
                    }
                }).then((result) => {
                    // console.log(_validation_result);
                    if (result.value) {
                        if (_validation_result[1] !== null && _validation_result[1] === "delivery and shipping") {
                            p_address_actions_popup("save", "delivery and shipping", form_data);
                        } else if (_validation_result[1] !== null && _validation_result[1] === "delivery only") {

                            p_address_actions_popup("save", "delivery only", form_data);
                        } else {
                            return 0;
                        }

                        // Swal.fire(
                        //     'Address added!',
                        //     'success'
                        // )
                    }
                })
            }
        }
    });
}

function defaultcardupdated(data) {
    Swal.fire(
        '',
        trans('website_contents.global.card_action_messages.set_as_default', {}),
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function defaultaddressupdated(data) {
    Swal.fire(
        '',
        trans('website_contents.global.address_action_messages.set_as_default_shipment', {}),
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function carddeleted_popup(data) {
    Swal.fire(
        '',
        trans('website_contents.global.card_action_messages.card_deleted', {}),
        'success'
    ).then((result) => {
        window.location.reload();
    })
}

function addNewCard_Popup(data, success) {

    if (success == 1) {
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_added', {}),
            'success'
        ).then((result) => {
            window.location.reload();
        })
    } else if (success == 0) {
        Swal.fire(
            '',
            trans('website_contents.global.card_action_messages.card_add_failed', {}),
            'error'
        ).then((result) => {
            window.location.reload();
        })
    }

}


