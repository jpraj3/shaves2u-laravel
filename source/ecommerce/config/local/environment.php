<?php
$a = dirname(__FILE__);
$b = realpath($a);
$c = explode('\\', $b);
$d = end($c);
$DIR_NAME = $d;
$API_KEY_PATH = config('app.env') !== $DIR_NAME ? '' : 'E:/DevTools/laragon/www/s2u-laravel-config/config/' . config('app.appType') . '/config.' . config('app.env') . '.json';
$GET_CONFIG_CONTENTS = $API_KEY_PATH === '' ? '' : json_decode(file_get_contents($API_KEY_PATH), true);
return [
    'webUrl' => 'http://localhost:8000/',
    'apiUrl' => 'http://192.168.2.194:99/api',
    'emailUrl' => 'http://192.168.2.194:99/',
    'CUSTOM_CONFIG_PATH' => $API_KEY_PATH,
    'CUSTOM_CONFIG_DATA' => $GET_CONFIG_CONTENTS,
    'IpStack' => [
        'accesskey' => '2c878ac336a0a28ab63caed03dc92cc1',
        'url' => 'http://api.ipstack.com/',
        'example' => 'http://api.ipstack.com/175.138.185.1?access_key=2c878ac336a0a28ab63caed03dc92cc1',
    ],
    'aws' => [
        'settings' => [
            'keyPath' => 'E:/DevTools/laragon/www/s2u-laravel-config/config/aws-key.json',
            'userAvatarAlbum' => 'user-avatars',
            'version' => '2012-10-17',
            'bucketName' => 'shaves2u-dci',
            'testUpload' => 'shaves2u-laravel-test',
            'planPanelAlbum' => 'plan-panel',
            'productImagesAlbum' => 'product-images',
            'planImagesAlbum' => 'plan-images',
            'promotionImagesAlbum' => 'promotion-images',
            'filesUploadAlbum' => 'files-upload-local',
            'csvReportsUpload' => 'csv-reports-upload-local',
            'localDownloadFolder' => 'E:/DevTools/laragon/www/s2u-laravel-config/config/aws-output-files',
            'localCSVReportFolder' => 'E:/DevTools/laragon/www/s2u-laravel-config/config/aws-output-files/csv-report-files',
            'limitDownload' => 2000,
        ],
    ],

    'taxInvoice' => [
        'taxInvoicePath' => 'E:/DevTools/laragon/www/s2u-laravel-config/taxInvoice-upload-files',
    ],

    'warehouse-interface' => [
        'folderName' => 'warehouse-interface-',
        'orderCancel' => 'order-cancel-outbound',
        'orderConfirmation' => 'order-confirmation-outbound',
        'orderInbound' => 'order-inbound-outbound',
    ],

    'warehouse_paths' => [
        'MYS' => [
            'local_upload_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-my-upload-files',
            'local_download_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-my-output-files',
            // 'ftp_outbox_folder' => '/shaves2u-dci-my-ftp-test/outbox/new-site-test',
            // 'ftp_inbox_folder' => '/shaves2u-dci-my-ftp-test/inbox/new-site-test',
            // UAT Testing
            'ftp_outbox_folder' => '/shaves05-my-sftp-test/my/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-test/my/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'HKG' => [
            'local_upload_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-hk-upload-files',
            'local_download_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-hk-output-files',
            // 'ftp_outbox_folder' => '/shaves2u-dci-hk-ftp-test/outbox/new-site-test',
            // 'ftp_inbox_folder' => '/shaves2u-dci-hk-ftp-test/inbox/new-site-test',
            // UAT Testing
            'ftp_outbox_folder' => '/shaves05-my-sftp-test/hk/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-test/hk/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'SGP' => [
            'local_upload_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/urbanfox-upload-files',
            'local_download_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/urbanfox-output-files',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'KOR' => [
            'local_upload_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-kr-upload-files',
            'local_download_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-kr-output-files',
            // 'ftp_outbox_folder' => '/shaves2u-dci-kr-ftp-test/outbox/new-site-test',
            // 'ftp_inbox_folder' => '/shaves2u-dci-kr-ftp-test/inbox/new-site-test',
            // UAT Testing
            'ftp_outbox_folder' => '/shaves05-my-sftp-test/kr/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-test/kr/inbox',
            'pre_convert' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-kr-output-files/pre_convert',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'TWN' => [
            'local_upload_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-tw-upload-files',
            'local_download_folder' => 'E:/DevTools/laragon/www/s2u-laravel-config/warehouse-tw-output-files',
            // 'ftp_outbox_folder' => '/shaves2u-dci-tw-ftp-test/outbox/new-site-test',
            // 'ftp_inbox_folder' => '/shaves2u-dci-tw-ftp-test/inbox/new-site-test',
            // UAT Testing
            'ftp_outbox_folder' => '/shaves05-my-sftp-test/tw/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-test/tw/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
    ],

    'aftership' => [
        'trackingUrl' => 'https://s2ustaging.aftership.com',
    ],

    'urbanFox' => [
        'host' => 'https://ims.urbanfox.asia/graphiql',
        'trackingUrl' => 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number=',
    ],

    'deliverDateForKorea' => '2018-10-01',

    'trialPlan' => [
        'trial_first_delivery' => '13 days',
        'trial_first_delivery_hk' => '6 days',
        'baFirstDelivery' => '13 days',
        'webFirstDelivery' => '20 days',
        'startFirstDelivery' => '13 days',
        'followUpEmail1' => '10 days',
        'followUpEmail2' => '7 days',
        'followUpEmail5Days' => '5 days',
    ],

    'email-blasting' => [
        'user-email-update' => [
            'ALL' => [
                // 'chantal@dci-digital.com',
                'jason@dci-agency.com'
            ],
            'MY' => [
                'chantal@dci-digital.com'
            ],
            'HK' => [
                'chantal@dci-digital.com'
            ],
            'SG' => [
                'chantal@dci-digital.com'
            ],
            'KR' => [
                'chantal@dci-digital.com'
            ],
            'TW' => [
                'chantal@dci-digital.com'
            ],
        ],
        'ops-team-notications' => [
            'double-charge' => [
                'jason@dci-agency.com'
            ]
        ],
        'referral-cashouts' => [
            'lucas@dci-digital.com'
        ]
    ]

];
