<?php
$a = dirname(__FILE__);
$b = realpath($a);
$c = explode('/', $b);
$d = end($c);
$DIR_NAME = $d;
$API_KEY_PATH = config('app.env') !== $DIR_NAME ? '' : '/home/shaves2uV2/config-jobserver/config/' . config('app.appType') . '/config.' . config('app.env') . '.json';
$GET_CONFIG_CONTENTS = $API_KEY_PATH === '' ? '' : json_decode(file_get_contents($API_KEY_PATH), true);
return [
    'webUrl' => 'http://13.67.90.142:9003/',
    'apiUrl' => 'http://13.67.90.142:9003/api',
    'emailUrl' => 'https://shaves2u.com/',
    'CUSTOM_CONFIG_PATH' => $API_KEY_PATH,
    'CUSTOM_CONFIG_DATA' => $GET_CONFIG_CONTENTS,
    'IpStack' => [
        'accesskey' => '2c878ac336a0a28ab63caed03dc92cc1',
        'url' => 'http://api.ipstack.com/',
        'example' => 'http://api.ipstack.com/175.138.185.1?access_key=2c878ac336a0a28ab63caed03dc92cc1',
    ],
    'aws' => [
        'settings' => [
            'keyPath' => '/home/shaves2uV2/config-jobserver/aws-key.json',
            'userAvatarAlbum' => 'user-avatars',
            'version' => '2012-10-17',
            'bucketName' => 'shaves2u-dci',
            'testUpload' => 'shaves2u-laravel-test',
            'planPanelAlbum' => 'plan-panel',
            'productImagesAlbum' => 'product-images',
            'planImagesAlbum' => 'plan-images',
            'promotionImagesAlbum' => 'promotion-images',
            'filesUploadAlbum' => 'files-upload-production',
            'csvReportsUpload' => 'csv-reports-upload-production',
            'localDownloadFolder' => '/home/shaves2uV2/config-jobserver/aws-output-files',
            'localCSVReportFolder' => '/home/shaves2uV2/config-jobserver/aws-output-files/csv-report-files',
            'limitDownload' => 2000,
        ],
    ],

    'taxInvoice' => [
        'taxInvoicePath' => '/home/shaves2uV2/config-jobserver/taxInvoice-upload-files',
    ],

    'warehouse-interface' => [
        'folderName' => 'warehouse-interface-',
        'orderCancel' => 'order-cancel-outbound',
        'orderConfirmation' => 'order-confirmation-outbound',
        'orderInbound' => 'order-inbound-outbound',
    ],

    'warehouse_paths' => [
        'MYS' => [
            'local_upload_folder' => '/home/shaves2uV2/config-jobserver/warehouse-my-upload-files',
            'local_download_folder' => '/home/shaves2uV2/config-jobserver/warehouse-my-output-files',
            'ftp_outbox_folder' => '/shaves05-my-sftp-prod/my/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-prod/my/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'HKG' => [
            'local_upload_folder' => '/home/shaves2uV2/config-jobserver/warehouse-hk-upload-files',
            'local_download_folder' => '/home/shaves2uV2/config-jobserver/warehouse-hk-output-files',
            'ftp_outbox_folder' => '/shaves05-my-sftp-prod/hk/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-prod/hk/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'SGP' => [
            'local_upload_folder' => '/home/shaves2uV2/config-jobserver/urbanfox-upload-files',
            'local_download_folder' => '/home/shaves2uV2/config-jobserver/urbanfox-output-files',
        ],
        'KOR' => [
            'local_upload_folder' => '/home/shaves2uV2/config-jobserver/warehouse-kr-upload-files',
            'local_download_folder' => '/home/shaves2uV2/config-jobserver/warehouse-kr-output-files',
            'ftp_outbox_folder' => '/shaves05-my-sftp-prod/kr/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-prod/kr/inbox',
            'pre_convert' => '/home/shaves2uV2/config-jobserver/warehouse-kr-output-files/pre_convert',
            'retry' => 5,
            'maxFiles' => 10,
        ],
        'TWN' => [
            'local_upload_folder' => '/home/shaves2uV2/config-jobserver/warehouse-tw-upload-files',
            'local_download_folder' => '/home/shaves2uV2/config-jobserver/warehouse-tw-output-files',
            'ftp_outbox_folder' => '/shaves05-my-sftp-prod/tw/outbox',
            'ftp_inbox_folder' => '/shaves05-my-sftp-prod/tw/inbox',
            'retry' => 5,
            'maxFiles' => 10,
        ],
    ],

    'aftership' => [
        'trackingUrl' => 'https://shaves2u.aftership.com',
    ],

    'urbanFox' => [
        'host' => 'https://ims.urbanfox.asia/graphiql',
        'trackingUrl' => 'https://www.urbanfox.asia/courier-tracking/tracking/?tracking_number=',
    ],

    'deliverDateForKorea' => '2018-10-01',

    'trialPlan' => [
        'trial_first_delivery' => '13 days',
        'trial_first_delivery_hk' => '13 days',
        'baFirstDelivery' => '13 days',
        'webFirstDelivery' => '20 days',
        'startFirstDelivery' => '13 days',
        'followUpEmail1' => '10 days',
        'followUpEmail2' => '7 days',
        'followUpEmail5Days' => '5 days',
    ],

    'email-blasting' => [
        'user-email-update' => [
            'ALL' => [
                'help.my@shaves2u.com',
            ],
            'MY' => [
                'help.my@shaves2u.com',
            ],
            'HK' => [
                'help.hk@shaves2u.com',
            ],
            'SG' => [
                'help.sg@shaves2u.com',
            ],
            'KR' => [
                'help.kr@shaves2u.com',
            ],
            'TW' => [
                'help.tw@shaves2u.com',
            ],
        ],
        'operations' => [
            'ALL' => [
                'help.my@shaves2u.com',
            ],
            'MY' => [
                'help.my@shaves2u.com',
            ],
            'HK' => [
                'help.hk@shaves2u.com',
            ],
            'SG' => [
                'help.sg@shaves2u.com',
            ],
            'KR' => [
                'help.kr@shaves2u.com',
            ],
            'TW' => [
                'help.tw@shaves2u.com',
            ],
        ],
        'referral-cashouts' => [
            'accounts@shaves2u.com'
        ]
    ]
];
