<?php

$base_url = '';

if (env('APP_ENV') === "staging") {
    $base_url = '/shaves2u/ecommerce';
} else {
    $base_url = '';
}
// $serverHttp = (!empty(filter_input(INPUT_SERVER, "HTTPS")) && filter_input(INPUT_SERVER, "HTTPS") != "off") ? "https://" : "http://";
// $serverName = filter_input(INPUT_SERVER, "SERVER_NAME");
// $serverPort = (filter_input(INPUT_SERVER, "SERVER_PORT") === "80" || filter_input(INPUT_SERVER, "SERVER_PORT") === "443" || empty(filter_input(INPUT_SERVER, "SERVER_PORT"))) ?
// "" : ":" . filter_input(INPUT_SERVER, "SERVER_PORT");
// } else {
// $serverHttp = (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") ? "https://" : "http://";
// $serverName = $_SERVER["SERVER_NAME"];
// $serverPort = ($_SERVER["SERVER_PORT"] === "80" || $_SERVER["SERVER_PORT"] === "443" || empty($_SERVER["SERVER_PORT"])) ?
// "" : ":" . $_SERVER["SERVER_PORT"];
// }

// $bathUrl = $serverHttp . $serverName . $serverPort . "/";

return [
    'base_url' => $base_url,
    /*
    |--------------------------------------------------------------------------
    | Begin Default Values
    |--------------------------------------------------------------------------
    | This Section Covers the fallback values used for the site
    |
     */
    'default' => [
        'country' => [
            'id' => 1,
            'code' => 'MYS',
            'codeIso' => 'MY',
            'defaultLang' => 'en',
            'urlLang' => 'en',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | All Sites
    |--------------------------------------------------------------------------
    |
     */

    'all' => [
        'general_trial_price' => [
            'MY' => '6.50',
            'SG' => '5.00',
            'HK' => '46',
            'KR' => '4,000',
            'TW' => '175',
        ],
        'bulk_order_tax_invoice' => [
            'MY' => 'Y',
            'SG' => 'Y',
            'HK' => 'Y',
            'KR' => 'Y',
            'TW' => 'N',
        ],
        'handle_types' => [
            'skus' => ['H3'],
            'skusV2' => ['H3', 'H1', 'H2'],
            'trial' => [
                'skus' => ['H3', 'H1', 'H2'],
            ],
            'rubber' => [
                'skus' => ['H1'],
            ],
        ],
        'blade_types' => [
            'skus' => ['S3/2018', 'S5/2018', 'S6/2018', "F5"],
            'trial_blade_product' => [
                "S3/2018"  => "TK3/2018",
                "S5/2018"  => "TK5/2018",
                "S6/2018"  => "TK6/2018",
                "S3/2018_Swivel"  => "H3TK3/2018",
                "S5/2018_Swivel"  => "H3TK5/2018",
                "S6/2018_Swivel"  => "H3TK6/2018",
                "F5"  => "F5"
            ],
        ],
        'trial_types' => [
            'frontend_images_and_copy' => [
                'H1' => ['TK6/2018'],
                'H3' => ['H3TK6/2018'],
                'H2' => ['FK5/2019']
            ],
            'male' => [
                'combinations' => [
                    0 => ['H3', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018', 'S3/2018', 'S5/2018', 'S6/2018'],
                    1 => ['H1', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'S3/2018', 'S5/2018', 'S6/2018'],
                ],
                'trial_skus' => ['TK3/2018', 'TK5/2018', 'TK6/2018', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018'],
                'combine_skus' => [
                    'TK3/2018' => ['H1', 'TK3/2018', 'S3/2018'],
                    'TK5/2018' => ['H1', 'TK5/2018', 'S5/2018'],
                    'TK6/2018' => ['H1', 'TK6/2018', 'S6/2018'],
                    'H3TK3/2018' => ['H3', 'TK3/2018', 'S3/2018'],
                    'H3TK5/2018' => ['H3', 'TK5/2018', 'S5/2018'],
                    'H3TK6/2018' => ['H3', 'TK6/2018', 'S6/2018'],
                ],
            ],
            'female' => [
                'combinations' => [
                    0 => ['H2', 'FK3/2019', 'FK5/2019', 'FK6/2019', 'F3', 'F5', 'F6'],
                ],
                'trial_skus' => ['FK3/2019', 'FK5/2019', 'FK6/2019'],
                'combine_skus' => [
                    'FK3/2019' => ['H2', 'FK3/2019', 'F3'],
                    'FK5/2019' => ['H2', 'FK5/2019', 'F5'],
                    'FK6/2019' => ['H2', 'FK6/2019', 'F6'],
                ],
            ],
            'all' => [
                'handle' => ['H1', 'TK3/2018', 'TK5/2018', 'TK6/2018', 'S3/2018', 'H3', 'H3TK3/2018', 'H3TK5/2018', 'H3TK6/2018'],
                'blade' => ['S3/2018', 'S5/2018', 'S6/2018', 'F3', 'F5', 'F6']
            ]
        ],
        'shavecream_types' => [
            'skus' => ['A5'],
        ],
        'aftershavecream_types' => [
            'skus' => ['A2'],
        ],
        'pouch_types' => [
            'skus' => ['POUCH/2019'],
        ],
        'razor_potector' => [
            'skus' => ['PTC-HDL'],
        ],
        'blade_no' => [
            "S3/2018" => '3',
            "S5/2018" => '5',
            "S6/2018" => '6',
            "F5" => '5',
        ],
        'blade_no_v2' => [
            "S3/2018" => '3',
            "S5/2018" => '5',
            "S6/2018" => '6',
            "F5" => 'W 5',
        ],
        'plan_category_types' => [
            'men' => [
                'trial' => 'Trial Plan',
                'custom' => 'Custom Plan',
            ],
            'women' => [
                'trial' => 'Women Trial Plan',
                'custom' => 'Women Custom Plan',
            ],
        ],
        'product_category_types' => [
            'men' => [
                1,
                // 2,
                // 3,
                // 4,
                5,
                // 7,
                // 8,
                // 9,
                // 10,
                // 11,
                // 12,
                // 13,
                // 14
            ],
            'women' => [
                6,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
            ],
        ],
        'product_type_names' => [
            'men' => [
                'handles' => 'Handle Pack',
                'blades' => 'Cartridge Packs',
                'addons' => 'Skin Care',
                'ask' => 'Awesome Shave Kit',
            ],
            'women' => [
                'handles' => 'Womens Handle Pack',
                'blades' => 'Womens Cartridge Packs',
                'addons' => 'Womens Skin Care',
                'ask' => 'Womens Awesome Shave Kit',
            ],
        ],
        'product_based_on_plan_type_names' => [
            'men' => [
                'handles' => 'Trial Plan Handle',
                'blades' => 'Trial Plan Blade',
                'addons' => 'Trial Plan Addons',
            ],
            'women' => [
                'handles' => 'Womens Trial Plan Handle',
                'blades' => 'Womens Trial Plan Blade',
                'addons' => 'Womens Trial Plan Addons',
            ],
        ],
        'rebates' => [
            'referral' => [
                'invite_link' => '/en-my/shave-plans/trial-plan',
                'params' => [
                    'ref' => '',
                    'src' => '',
                ],
                'supported_countries' => ['MY'],
            ],
        ],
        'email-type' => [
            'cancellation_history_report',
            'goodbye',
            'order_cancelled',
            'subscription_recharge_email_report',
            'subscription_recharge_nicepay_email_report',
            'password_reset',
            'password_updated',
            'reactivate',
            'receipt_order_confirmation',
            'receipt_order_delivery',
            'receipt_order_completed',
            'receipt_subscription_payment_failure',
            'referral_active_credits',
            'referral_email_invite',
            'referral_inactive_credits',
            'referral_subscription_invite',
            'reminder_promo_7day',
            'reminder_promo_3day',
            'subscription_cancellation',
            'subscription_modified',
            'subscription_renewal',
            'subscription_renewal_annual',
            'welcome',
            'welcome_ask',
            'referral_cashout_request',
        ],

        'email-template' => [
            'my' => [
                'footer' => [
                    'links' => [
                        'instagram' => 'http://instagram.com/shaves2u',
                        'facebook' => 'http://facebook.com/shaves2u',
                        'youtube' => 'https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ',
                    ],
                    'company-name' => 'Shaves2U Sdn Bhd (1037174-T)',
                    'company-address' => 'Level 18, Axiata Tower, No. 9, Jalan Stesen Sentral 5, Kuala Lumpur Sentral, 50470 Kuala Lumpur, Malaysia.',
                ],
            ],
            'sg' => [
                'footer' => [
                    'links' => [
                        'instagram' => 'http://instagram.com/shaves2u',
                        'facebook' => 'http://facebook.com/shaves2u',
                        'youtube' => 'https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ',
                    ],
                    'company-name' => 'Shaves2U Pte Ltd (GST: 201732205Z)',
                    'company-address' => '120 Robinson Road, #07-01 Singapore 068913',
                ],
            ],
            'kr' => [
                'footer' => [
                    'links' => [
                        'instagram' => 'http://instagram.com/shaves2u',
                        'facebook' => 'http://facebook.com/shaves2u',
                        'youtube' => 'https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ',
                    ],
                    'company-name' => '세일즈웍스 코리아 유한회사<br>사업자등록번호 2648105684',
                    'company-address' => '서울특별시 강남구 논현로 419 (역삼동 피엠케이 빌딩). 메일 구독해지하기',
                ],
            ],
            'hk' => [
                'footer' => [
                    'links' => [
                        'instagram' => 'https://www.instagram.com/shaves2u_hktw/',
                        'facebook' => 'https://www.facebook.com/Shaves2u/',
                        'youtube' => 'https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ',
                    ],
                    'company-name' => 'Shaves2U HK Limited (2779733)',
                    'company-address' => 'Unit 2002, The Hennessy, 256 Hennessy Road, Wanchai, Hong Kong.',
                ],
            ],
            'tw' => [
                'footer' => [
                    'links' => [
                        'instagram' => 'https://www.instagram.com/shaves2u_hktw/',
                        'facebook' => 'https://www.facebook.com/Shaves2u/',
                        'youtube' => 'https://www.youtube.com/channel/UC4Hd_9mBGxhD6PRNy9scHqQ',
                    ],
                    'company-name' => 'Shaves2U HK Limited (2779733)',
                    'company-address' => 'Unit 2002, The Hennessy, 256 Hennessy Road, Wanchai, Hong Kong.',
                ],
            ],
        ],

        'stripe' => [
            "ignore_risk_error_codes" => [
                "insufficient_funds",
                "error_code_2",
                "error_code_3",
                "error_code_4",
                "error_code_5",
            ],
        ],

        'plans' => [
            'çustom_plan' => [
                'optional_products' => [1025, 34]
            ]
        ],

        'cancellation' => [
            'discount' => 20,
            'cancellation-report-recipient' => ""
        ],
        'ShaveCream' => 'A5',
        'AfterShaveCream' => 'A2',
        'MenSwivelHandle' => 'H3',
        'ToiletryBag' => 'POUCH/2019',
        'RubberSleeve' => 'PTC-HDL',
        'edit-shaves-plan' => [
            'all' => [
                'optional-product-ids' => [1026, 1025, 1019, 121,1027,1028,1007]
            ],
            'trial' => [
                'optional-product-ids' => [1026, 1025, 1019, 121,1027,1028,1007]
            ],
            'custom' => [
                'optional-product-ids' => [1026, 1025, 1019, 121,1027,1028,1007]
            ]
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Ecommerce
    |--------------------------------------------------------------------------
    |
     */

    'ecommerce' => [
        'default' => [
            'country' => [
                'id' => '1',
                'defaultLang' => 'EN',
            ],
        ],
        'checkout' => [
            'trial' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/3blades.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/5blades.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/6blades.png'
                    ]
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-4.png'
                    ]
                ]
            ],
            'trial-w' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/3blades-w.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/5blades-w.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/6blades-w.png'
                    ],
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-4.png'
                    ]
                ]
            ],
            'custom' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/3blades.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/5blades.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/6blades.png'
                    ]
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-4.png'
                    ]
                ]
            ]
        ],
        'cancellation' => [
            'blades_image_url' => [
                '3' => '/images/common/cancellation-journey/3blade.png',
                '5' => '/images/common/cancellation-journey/5blade.png',
                '6' => '/images/common/cancellation-journey/6blade.png'
            ],
            'blades_x4_image_url' => [
                '3' => '/images/common/cancellation-journey/3blade_x4.png',
                '5' => '/images/common/cancellation-journey/5blade_x4.png',
                '6' => '/images/common/cancellation-journey/6blade_x4.png'
            ],
            'free_blades_image_url' => [
                '3' => '/images/common/cancellation-journey/3blade_free.png',
                '5' => '/images/common/cancellation-journey/5blade_free.png',
                '6' => '/images/common/cancellation-journey/6blade_free.png'
            ]
        ],
        'TrialPlan' => [
            'TrialPlanType' => 'trial',
            'TrialPlanGroup' => 'Trial Plan',
            'TrialPlanHandle' => 'Trial Plan Handle',
            'TrialPlanBlade' => 'Trial Plan Blade',
            'TrialPlanAddOn' => 'Trial Plan Addons',
            'TrialPlanDuration' => [2, 3, 4, 6],
            'TrialPlanSku' => 'TP',
        ],
        'TrialPlan-W' => [
            'TrialPlanType' => 'trial',
            'TrialPlanGroup' => 'Women Trial Plan',
            'TrialPlanHandle' => 'Womens Trial Plan Handle',
            'TrialPlanBlade' => 'Womens Trial Plan Blade',
            'TrialPlanAddOn' => 'Womens Trial Plan Addons',
            'TrialPlanDuration' => [2, 3, 4, 6],
            'TrialPlanSku' => 'TP',
        ],
        'CustomPlan' => [
            'CustomPlanType' => 'custom',
            'CustomPlanGroup' => 'Custom Plan',
            'CustomPlanHandle' => 'HANDLE',
            'CustomPlanBlade' => 'BLADE',
            'CustomPlanBag' => 'BAG',
            'CustomPlanAddOn' => 'ADDON',
            'CustomPlanDuration' => [2, 3, 4, 6],
            'CustomPlanSku' => 'CP',
        ],
        'Campaign' => [
            'Type' => 'campaign',
            'PlanGroup' => 'Campaign Plan',
            'BladeGroup' => 'Campaign Blade',
            'ProductGroup' => 'Campaign Product',
            'Duration' => [6, 4, 3, 2],
            'Blade' => ["TK3", "TK5", "TK6"],
            'FirstDuration' => 2,
        ],
        'ShaveCream' => 'A5',
        'AfterShaveCream' => 'A2',
        'MenSwivelHandle' => 'H3',
        'ToiletryBag' => 'POUCH/2019',
        'RubberSleeve' => 'PTC-HDL',
    ],

    /*
    |--------------------------------------------------------------------------
    | BA-Website
    |--------------------------------------------------------------------------
    |
     */

    'baWebsite' => [
        'default' => [
            'country' => [
                'id' => '1',
                'defaultLang' => 'EN',
            ],
        ],
        'checkout' => [
            'trial' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/3blades.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/5blades.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/6blades.png'
                    ]
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-4.png'
                    ]
                ]
            ],
            'trial-w' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/3blades-w.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/5blades-w.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssetsWomen/6blades-w.png'
                    ],
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssetsWomen/icon-frequency-w-4.png'
                    ]
                ]
            ],
            'custom' => [
                'blade' => [
                    '3' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/3blades.png'
                    ],
                    '5' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/5blades.png'
                    ],
                    '6' => [
                        'smallBladeImageUrl' => '/images/common/checkoutAssets/6blades.png'
                    ]
                ],
                'frequency' => [
                    '2' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-1.png'
                    ],
                    '3' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-2.png'
                    ],
                    '4' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-3.png'
                    ],
                    '6' => [
                        'image' => '/images/common/checkoutAssets/icon-frequency-4.png'
                    ]
                ]
            ]
        ],
        'TrialPlan' => [
            'TrialPlanType' => 'trial',
            'TrialPlanGroup' => 'Trial Plan',
            'TrialPlanHandle' => 'Trial Plan Handle',
            'TrialPlanBlade' => 'Trial Plan Blade',
            'TrialPlanAddOn' => 'Trial Plan Addons',
            'TrialPlanDuration' => [2, 3, 4, 6],
            'TrialPlanSku' => 'TP',
        ],
        'TrialPlan-W' => [
            'TrialPlanType' => 'trial',
            'TrialPlanGroup' => 'Women Trial Plan',
            'TrialPlanHandle' => 'Womens Trial Plan Handle',
            'TrialPlanBlade' => 'Womens Trial Plan Blade',
            'TrialPlanAddOn' => 'Womens Trial Plan Addons',
            'TrialPlanDuration' => [2, 3, 4, 6],
            'TrialPlanSku' => 'TP',
        ],
        'CustomPlan' => [
            'CustomPlanType' => 'custom',
            'CustomPlanGroup' => 'Custom Plan',
            'CustomPlanHandle' => 'HANDLE',
            'CustomPlanBlade' => 'BLADE',
            'CustomPlanBag' => 'BAG',
            'CustomPlanAddOn' => 'ADDON',
            'CustomPlanDuration' => [2, 3, 4, 6],
            'CustomPlanSku' => 'CP',
        ],
        'ShaveCream' => 'A5',
        'AfterShaveCream' => 'A2',
        'MenSwivelHandle' => 'H3',
        'ToiletryBag' => 'POUCH/2019',
        'RubberSleeve' => 'PTC-HDL',
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    |
     */

    'admin' => [
        'testBadges' => [
            'TEST003',
            'TEST004',
            'TEST005',
            'TEST911',
            'TEST912',
            'TEST913',
            'TEST921',
            'TEST971'
        ],
    ],

];
